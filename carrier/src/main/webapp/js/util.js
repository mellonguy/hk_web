/**
 * 
 */
 
//1.  전체체크박스 선택/해제 
 	function checkAll(){

		if($('input:checkbox[id="checkAll"]').is(":checked")){
			$('input:checkbox[name="forBatch"]').each(function(index,element) {
				$(this).prop("checked","true");
			 });	
		}else{
			$('input:checkbox[name="forBatch"]').each(function(index,element) {
				$(this).prop("checked","");
			 });
		}
		getCheckBoxCnt();
	}	

//2. 체크박스 개수 구하기
	 function getCheckBoxCnt(){ 
			var checkboxCnt =$('input:checkbox[name="forBatch"]:checked').length;
			var checkbox = false;	
			if(checkboxCnt == 0){
				$("span[name=checkedChk_box]").text("");
			}
			for(var i=0; i<checkboxCnt; i++){
				//$("#checkedChk_box").text(checkboxCnt);	
				$("span[name=checkedChk_box]").text(checkboxCnt);
			}
		}

//3.  날짜 유효성 체크 코드
	
	function checkValidDate(value) {
		var result = true;
		var msg = "정확한 날짜 또는 yyyy-mm-dd 형식으로 입력해주세요.";
		try {
//		    var date = value.split("-");
//		    var y = parseInt(date[0], 10),
//		        m = parseInt(date[1], 10),
//		        d = parseInt(date[2], 10);
		    
//		    var dateRegex = /^(?=\d)(?:(?:31(?!.(?:0?[2469]|11))|(?:30|29)(?!.0?2)|29(?=.0?2.(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00)))(?:\x20|$))|(?:2[0-8]|1\d|0?[1-9]))([-.\/])(?:1[012]|0?[1-9])\1(?:1[6-9]|[2-9]\d)?\d\d(?:(?=\x20\d)\x20|$))?(((0?[1-9]|1[012])(:[0-5]\d){0,2}(\x20[AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$/;
		    var dateRegex = RegExp(/^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/);
		    result = dateRegex.test(value);
		    if(!result){
				alert(msg);
				return result;
			}else{
				return result;
			}
		} catch (err) {
			alert(msg);
			return false;
		}    
	}
	
	
//쿠키 set
function setCookie(c_name,value,exdays)
{
   var exdate=new Date();
   exdate.setDate(exdate.getDate() + exdays);
   var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
   document.cookie=c_name + "=" + c_value;
}

//쿠키 get
function getCookie(c_name)
{
   var i,x,y,ARRcookies=document.cookie.split(";");
   for (i=0;i<ARRcookies.length;i++)
   {
     x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
     y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
     x=x.replace(/^\s+|\s+$/g,"");
     if (x==c_name)
     {
       return unescape(y);
     }
   }
}		