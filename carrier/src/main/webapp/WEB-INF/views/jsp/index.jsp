<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%

%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>한국카캐리어</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="/img/favicon.ico">
<link rel="stylesheet" href="/css/bootstrap.min.css">
<link rel="stylesheet" href="/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="/fonts/NotoSans/notosanskr.css">
<link rel="stylesheet" href="/css/reset.css">
<link rel="stylesheet" href="/css/jquery.alert.css">
<link rel="stylesheet" href="/css/main.css">
<link rel="stylesheet" href="/css/main2.css">
<link rel="stylesheet" href="/css/responsive.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="/js/vendor/modernizr-2.8.3.min.js"></script>
<script src="/js/vendor/jquery-1.11.2.min.js"></script>
<script src="/js/89d51f385b.js"></script>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script type="text/javascript">

$(document).ready(function(){

	 
    var userInputId = getCookie("userInputId");
    $("#loginId").val(userInputId);  
    if($("#loginId").val() != ""){ 
        $("#rememberme").attr("checked", true); 
    }
   $("#rememberme").change(function(){ 
        if($("#rememberme").is(":checked")){ 
            var userInputId = $("#loginId").val();
            setCookie("userInputId", userInputId, 7); 
        }else{ 
            deleteCookie("userInputId");
        }
    });
   
    $("#loginId").keyup(function(){ 
        if($("#rememberme").is(":checked")){ 
            var userInputId = $("#loginId").val();
            setCookie("userInputId", userInputId, 7); 
        }
    });
   
   
    $("#toggler").click(function(){
        $(".modal").fadeIn();
    });
    $(".modal .btn-white").click(function(){
        $(".modal").fadeOut();
    });

    $('.urllist').on('click','.urlbtn.add',function(){
        $(this).prev().css('display','inline-block');
        $(' <tr>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="department">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="name">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="phoneNum">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="address">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="email">\
                                </div>\
                            </td>\
                            <td>\
                            <div class="inputdetails">\
                              <input type="text" name="etc">\
                            </div>\
                        </td>\
                            <td>\
                                <div class="buttons pull-left">\
                                    <span class="urlbtn del"></span>\
                                    <span class="urlbtn add iblock"></span>\
                                </div>\
                                \
                            </td>\
                        </tr>').appendTo('.urllist tbody');
        $(this).hide();
        $('.urllist tbody').find('tr:last-child .urlbtn.add').css('display','inline-block');
    });

    $('.urllist').on('click','.urlbtn.del',function(){
        $(this).parent().parent().parent().remove();
        console.log('a');
        $('.urllist tbody').find('tr:last-child .urlbtn.add').css('display','inline-block');
        var rowcount=0;
        $('.urllist tbody tr').each(function(){
            rowcount++;
        });
        if(rowcount==1){
            $('.urllist tbody').find('.urlbtn.del').css('display','none');
        }
    });
    
/*  	$("#phonePersonal").on('keyup', function(e){
        var trans_num = $(this).val().replace(/-/gi,'');
 		var k = e.keyCode;
	 	if(trans_num.length >= 11 && ((k >= 48 && k <=126) || (k >= 12592 && k <= 12687 || k==32 || k==229 || (k>=45032 && k<=55203)) )){
	   	    e.preventDefault();
	 	}
     }).on('blur', function(){ 
         if($(this).val() == '') return;
         var trans_num = $(this).val().replace(/-/gi,'');
         if(trans_num != null && trans_num != ''){
             if(trans_num.length==11 || trans_num.length==10){   
                 var regExp_ctn = /^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})([0-9]{3,4})([0-9]{4})$/;
                 if(regExp_ctn.test(trans_num)){
                     trans_num = trans_num.replace(/^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})-?([0-9]{3,4})-?([0-9]{4})$/, "$1-$2-$3");                  
                     $(this).val(trans_num);
                 }else{
                     alert("유효하지 않은 전화번호 입니다.");
                     $(this).val("");
                     $(this).focus();
                 }
             }else{
                 alert("유효하지 않은 전화번호 입니다.");
                 $(this).val("");
                 $(this).focus();
             }
       }
   });  */
    
/*  	$("#phoneWork").on('keyup', function(e){
        var trans_num = $(this).val().replace(/-/gi,'');
 		var k = e.keyCode;
	 	if(trans_num.length >= 11 && ((k >= 48 && k <=126) || (k >= 12592 && k <= 12687 || k==32 || k==229 || (k>=45032 && k<=55203)) )){
	   	    e.preventDefault();
	 	}
     }).on('blur', function(){ 
         if($(this).val() == '') return;
         var trans_num = $(this).val().replace(/-/gi,'');
         if(trans_num != null && trans_num != ''){
             if(trans_num.length==11 || trans_num.length==10){   
                 var regExp_ctn = /^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})([0-9]{3,4})([0-9]{4})$/;
                 if(regExp_ctn.test(trans_num)){
                     trans_num = trans_num.replace(/^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})-?([0-9]{3,4})-?([0-9]{4})$/, "$1-$2-$3");                  
                     $(this).val(trans_num);
                 }else{
                     alert("유효하지 않은 전화번호 입니다.");
                     $(this).val("");
                     $(this).focus();
                 }
             }else{
                 alert("유효하지 않은 전화번호 입니다.");
                 $(this).val("");
                 $(this).focus();
             }
       }
   });  */
    
/*  	$("#emailPersonal").on('keyup', function(e){
        
     }).on('blur', function(){ 
         if($(this).val() == '') return;
         //var trans_num = $(this).val().replace(/-/gi,'');
         var trans_num = $(this).val();
         if(trans_num != null && trans_num != ''){
                 var regExp_ctn = /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i;
                 if(regExp_ctn.test(trans_num)) {            
                	 $(this).val(trans_num);
		         }else{
		        	 alert("유효하지 않은 이메일 형식 입니다.");
		        	 $(this).val("");
	                 $(this).focus();
		         }
       }
   });  */
 	
});

function goMonthCheck(){ 



	﻿﻿$.confirm('입력하신 아이디는 보안이 만료되어 인증이 필요합니다.(한달에 한번 인증 요함)',{

    confirmButton : '문자로 인증',
    cancelButton : '주민등록번호 뒷자리로 인증',
    
   	callEvent:function(){ //문자

  	  if(﻿﻿confirm("가입하셨을 때 핸드폰번호로 인증하시겠습니까?")){
		
	
   	 $.ajax({ 
			type: 'post' ,
			url : "/baseinfo/monthCertNumForLogin.do" ,
			dataType : 'json' ,
			//async :true,
			data : {

				empId : $("#loginId").val(),
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				if(result == "0000"){

					$.prompt('인증번호를 전송하였습니다. 화면창에 입력해주세요',{
					    title:'인증번호 입력',
					    callEvent:function(val){
						    
					    	checkCertNum(val);
				            
					        },
					        
					    input:'text',
					    confirmButton:'인증하기'
					})
					
	
				}else if(result == "0001"){
					alert("실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		}); 

	  }else{
		  return false;
	  }
	  
    },


    
    cancelEvent:function(){ //주민번호


	  	  if(﻿﻿confirm("주민등록번호 뒷자리로 인증하시겠습니까?")){
	  		
	
	  		$.prompt('주민등록번호 (뒷자리)만 화면창에 입력해주세요',{
			    title:'인증번호 입력',
			    callEvent:function(val){
				    
			    	 registrationNumber(val);
			    	    
			        },
			        
			    input:'text',
			    confirmButton:'인증하기'
			})

  		  }else{
  			  return false;
  		  }

   	

        },
    
    
    
});
	
	
}

function checkCertNum(certNum){ //인증번호 비교 ajax


	 $.ajax({ 
			type: 'post' ,
			url : "/baseinfo/checkCertNumForPassWord.do" ,
			dataType : 'json' ,
			data : {
				
				empId : $("#loginId").val(),
				phoneCertNum : certNum,
				security : 'Y',
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				if(result == "0000"){
					alert("인증에 성공하였습니다. 홈페이지로 이동합니다.");
					//window.location.reload();
					loginProcess();
				}else if(result == "0001"){
					alert("인증 하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		}); 
	
}



function registrationNumber(registrationNumber){ //주민등록번호 뒷자리 비교 a.jax
	

	   	 $.ajax({ 
				type: 'post' ,
				url : "/baseinfo/registrationNumberCheck.do" ,
				dataType : 'json' ,
				data : {
	  				
					empId : $("#loginId").val(),
					registrationNumber :registrationNumber,
					
				},
				success : function(data, textStatus, jqXHR)
				{
					var result = data.resultCode;
					if(result == "0000"){
						alert("인증에 성공하였습니다. 홈페이지로 이동합니다.");
						//window.location.reload();
						loginProcess();
					}else if(result == "0001"){
						alert("인증 하는데 실패 하였습니다.");
					}
				} ,
				error : function(xhRequest, ErrorText, thrownError) {
				}
			}); 



	}



function setCookie(cookieName, value, exdays){
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var cookieValue = escape(value) + ((exdays==null) ? "" : "; expires=" + exdate.toGMTString());
    document.cookie = cookieName + "=" + cookieValue;
}

function deleteCookie(cookieName){
    var expireDate = new Date();
    expireDate.setDate(expireDate.getDate() - 1);
    document.cookie = cookieName + "= " + "; expires=" + expireDate.toGMTString();
}
 
function getCookie(cookieName) {
    cookieName = cookieName + '=';
    var cookieData = document.cookie;
    var start = cookieData.indexOf(cookieName);
    var cookieValue = '';
    if(start != -1){
        start += cookieName.length;
        var end = cookieData.indexOf(';', start);
        if(end == -1)end = cookieData.length;
        cookieValue = cookieData.substring(start, end);
    }
    return unescape(cookieValue);
}

function loginProcess(){


	
	//var test = prompt("ddddd");

	//alert(test);
	

	
	if($("#companyId").val() == ""){
		alert("회사가 선택 되지 않았습니다.");
		return false;
	}
	
	if($("#loginId").val() == ""){
		alert("아이디가 입력 되지 않았습니다.");
		return false;
	}
	if($("#loginPwd").val() == ""){
		alert("패스워드가 입력 되지 않았습니다.");
		return false;
	}
	
	$.ajax({ 
        type: 'post' ,
        url : 'loginPersist.do' ,
        dataType : 'json' ,
        data : $('#loginForm').serialize(), 
        
        success : function(jsonData, textStatus, jqXHR) {
	      
        	if(jsonData.resultCode =="0000"){
            	goMainPage();
            	/* $("#myphoto_path").css("background-image","url(/files/member"+ data.resultData.photo_path + ")"); */
        	}else if(jsonData.resultCode =="E001"){
        		alert("로그인에 실패 하였습니다. 관리자에게 문의 하세요.");
        	}else if(jsonData.resultCode =="E002"){
        		goMainPage();
        	}else if(jsonData.resultCode =="E003"){
        		alert("승인 대기중인 아이디 입니다.");
        	}else if(jsonData.resultCode =="E004"){
        		alert("휴직 상태인 아이디 입니다.");
        	}else if(jsonData.resultCode =="E005"){
        		alert("퇴직 상태인 아이디 입니다.");
        	}else if(jsonData.resultCode =="E006"){
        		goMonthCheck();
        		
        	}
        	
        } ,
        error : function(xhRequest, ErrorText, thrownError) {
          //  alertEx("시스템 에러");
        }
    }); 
}

function goMainPage(){
	document.location.href = "/allocation/combination.do";
}



function popup(obj,status){
	
	if(status == "employee"){
		$("#employeeInsertForm").css("display","block");
		$("#driverInsertForm").css("display","none");
	}else if(status == "driver"){
		$("#employeeInsertForm").css("display","none");
		$("#driverInsertForm").css("display","block");
	}
	
	$(".modal").fadeIn();
	
}

function popUpClose(obj){
	$(".modal").fadeOut();
}






function addEmployee(){
	
 	var file = document.getElementById("upload");
 	//alert(file.files.length);
 	for(var i = 0; i < file.files.length; i++){
		var ext = file.files[i].name.split(".").pop().toLowerCase();
		if($.inArray(ext, ["jpeg","jpg","gif","png","pdf"]) == -1){
			alert("업로드 할 수 없는 파일이 있습니다.");
			$("#upload").val("");
			return false;
		}
	}
 	

 	
        if($("#empId").val() == ""){
			alert("사원번호(아이디)가 입력되지 않았습니다.");
			return false;
		}	
        if($("empName").val() == ""){
			alert("성명이 입력되지 않았습니다.");
			return false;
		}	
       
        if($("#empPwd").val() == ""){
        	alert("패스워드가 입력되지 않았습니다.");
			return false;
        }
        
        /* var reg_pwd = /^.*(?=.{6,20})(?=.*[0-9])(?=.*[a-zA-Z]).*$/;
        if(!reg_pwd.test($("#empPwd").val())){
        	alert("패스워드는 영문과 숫자를 ");
         	return false;
        } */
        
        var pw = $("#empPwd").val();
        var num = pw.search(/[0-9]/g);
        var eng = pw.search(/[a-z]/ig);
        var spe = pw.search(/[`~!@@#$%^&*|₩₩₩'₩";:₩/?]/gi);

        if(pw.length < 6 || pw.length > 20){
         alert("패스워드는 6자리 이상이어야 합니다.");
         return false;
        }
        if(pw.search(/₩s/) != -1){
       	  alert("패스워드는 공백업이 입력해주세요.");
       	  return false;
       	 }
        if(num < 0 || eng < 0 || spe < 0 ){
          alert("영문,숫자, 특수문자를 혼합하여 입력해주세요.");
          return false;
         }
        
        if($("#empPwdChk").val() == ""){
        	alert("패스워드확인이 입력되지 않았습니다.");
			return false;
        }
        
        if($("#empPwd").val() != $("#empPwdChk").val()){
        	alert("패스워드와 패스워드 확인에 입력 한 값이 다릅니다.");
			return false;
        }
        
        if($("#birth").val() == ""){
        	alert("생년월일이 입력되지 않았습니다.");
			return false;
        }
        
        
        if($("#phonePersonal").val() == ""){
			alert("연락처 1이 입력되지 않았습니다.");
			return false;
		}else{
			if(!phoneChk($("#phonePersonal"))){
		  		alert("유효하지 않은 전화번호 입니다.");
		  		$("#phonePersonal").val("");
	            $("#phonePersonal").focus();
				return false;
			}
		}
        if($("#phoneWork").val() == ""){
			alert("연락처 2가 입력되지 않았습니다. 비상시 연락 가능한 번호를 작성 하세요.");
			return false;
		}else{
			if(!phoneChk($("#phoneWork"))){
		  		alert("유효하지 않은 전화번호 입니다.");
		  		$("#phoneWork").val("");
	            $("#phoneWork").focus();
				return false;
			}
		}
        
        if($("#address").val() == ""){
        	alert("주소가 입력되지 않았습니다.");
			return false;
        }
        if($("#addressDetail").val() == ""){
        	alert("상세주소가 입력되지 않았습니다.");
			return false;
        }
        
        if($("#emailPersonal").val() == ""){
			alert("이메일이 입력되지 않았습니다.");
			return false;
		}else{
			if(!mailChk($("#emailPersonal"))){
	 			alert("유효하지 않은 이메일 형식 입니다.");
	            $("#emailPersonal").val("");
	            $("#emailPersonal").focus();
	 			return false;
	 		}
		}
        
                
		if(confirm("등록 하시겠습니까?")){
			$("#employeeInsertForm").attr("action","/baseinfo/insert-employee.do?status=noLogin");
			$("#employeeInsertForm").submit();	
		}
	
}


function addDriver(){

	
	var file = document.getElementById("driverUpload");
 	//alert(file.files.length);
 	for(var i = 0; i < file.files.length; i++){
		var ext = file.files[i].name.split(".").pop().toLowerCase();
		if($.inArray(ext, ["jpeg","jpg","gif","png","pdf"]) == -1){
			alert("업로드 할 수 없는 파일이 있습니다.");
			$("#driverUpload").val("");
			return false;
		}
	}
	
	if($("#driverOwner").val() == ""){
		alert("차량소유주가 입력되지 않았습니다.");
		return false;
	}
	if($("#driverKind").val() == ""){
		alert("구분이 입력되지 않았습니다.");
		return false;
	}
	
	if($("#driverName").val() == ""){
		alert("이름이 입력되지 않았습니다.");
		return false;
	}
	
	if($("#driverId").val() == ""){
		alert("아이디가 입력되지 않았습니다.");
		return false;
	}	
	
	 if($("#driverPwd").val() == ""){
    	alert("패스워드가 입력되지 않았습니다.");
		return false;
    }
    
    /* var reg_pwd = /^.*(?=.{6,20})(?=.*[0-9])(?=.*[a-zA-Z]).*$/;
    if(!reg_pwd.test($("#empPwd").val())){
    	alert("패스워드는 영문과 숫자를 ");
     	return false;
    } */
    
    var pw = $("#driverPwd").val();
    var num = pw.search(/[0-9]/g);
    var eng = pw.search(/[a-z]/ig);
    var spe = pw.search(/[`~!@@#$%^&*|₩₩₩'₩";:₩/?]/gi);

    if(pw.length < 6 || pw.length > 20){
     alert("패스워드는 6자리 이상이어야 합니다.");
     return false;
    }
    if(pw.search(/₩s/) != -1){
   	  alert("패스워드는 공백업이 입력해주세요.");
   	  return false;
   	 }
    if(num < 0 || eng < 0 || spe < 0 ){
      alert("영문,숫자, 특수문자를 혼합하여 입력해주세요.");
      return false;
     }
    
    if($("#driverPwdChk").val() == ""){
    	alert("패스워드확인이 입력되지 않았습니다.");
		return false;
    }
    
    if($("#driverPwd").val() != $("#driverPwdChk").val()){
    	alert("패스워드와 패스워드 확인에 입력 한 값이 다릅니다.");
		return false;
    }
	
	
	
	if($("#phoneNum").val() == ""){
		alert("연락처가 입력되지 않았습니다.");
		return false;
	}else{
		if(!phoneChk($("#phoneNum"))){
	  		alert("유효하지 않은 전화번호 입니다.");
	  		$("#phoneNum").val("");
            $("#phoneNum").focus();
			return false;
		}
	}
	
	if($("#phoneNumA").val() == ""){
		alert("비상연락처1이 입력되지 않았습니다.");
		return false;
	}else{
		if(!phoneChk($("#phoneNumA"))){
	  		alert("유효하지 않은 전화번호 입니다.");
	  		$("#phoneNumA").val("");
            $("#phoneNumA").focus();
			return false;
		}
	}
	
	if($("#phoneNumB").val() == ""){
		alert("비상연락처2가 입력되지 않았습니다.");
		return false;
	}else{
		if(!phoneChk($("#phoneNumB"))){
	  		alert("유효하지 않은 전화번호 입니다.");
	  		$("#phoneNumB").val("");
            $("#phoneNumB").focus();
			return false;
		}
	}
	
	if($("#residentRegistrationNumber").val() == ""){
		alert("주민번호가 입력되지 않았습니다.");
		return false;
	}else{
		if(!residentChk($("#residentRegistrationNumber"))){
 			alert("유효하지 않은 주민번호 입니다.");
            $("#residentRegistrationNumber").val("");
            $("#residentRegistrationNumber").focus();
 			return false;
 		}
	}
	
	if($("#companyKind").val() == ""){
		alert("사업자 구분이 입력되지 않았습니다.");
		return false;
	}
	
	if($("#businessLicenseNumber").val() == ""){
		alert("사업자번호가 입력되지 않았습니다.");
		return false;
	}else{
		if(!businessNumChk($("#businessLicenseNumber"))){
	  		alert("유효하지 않은 사업자번호 입니다.");
	  		$("#businessLicenseNumber").val("");
            $("#businessLicenseNumber").focus();
			return false;
		}
	}
	
	if($("#driverAddress").val() == ""){
		alert("사업장 주소가 입력되지 않았습니다.");
		return false;
	}
	if($("#driverAddressDetail").val() == ""){
		alert("사업장 주소 상세가 입력되지 않았습니다.");
		return false;
	}
	if($("#carKind").val() == ""){
		alert("차종이 입력되지 않았습니다.");
		return false;
	}
	if($("#carAssignCompany").val() == ""){
		alert("차량 소속이 입력되지 않았습니다.");
		return false;
	}
	if($("#carNum").val() == ""){
		alert("차량번호가 입력되지 않았습니다.");
		return false;
	}
	/* if($("#carIdNum").val() == ""){
		alert("차대번호가 입력되지 않았습니다.");
		return false;
	} */
		
	if($("#carAssignCompany").val() == ""){
		alert("차량소속이 입력되지 않았습니다.");
		return false;
	}
	
	if($("#driverLicense").val() == ""){
		alert("자격증번호가 입력되지 않았습니다.");
		return false;
	}
	
	/* if($("#assignCompany").val() == ""){
		alert("소속이 입력되지 않았습니다.");
		return false;
	} */
	
	if($("#driverDeductionRate").val() == ""){
		alert("적용요율이 입력되지 않았습니다.");
		return false;
	}
	
    
	if(confirm("등록 하시겠습니까?")){
		$("#driverInsertForm").attr("action","/baseinfo/insert-driver.do?status=noLogin");
		$("#driverInsertForm").submit();	
	}
	
}



function jusoSearch(where,obj){
	
	new daum.Postcode({
	    oncomplete: function(data) {
	        // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

            // 각 주소의 노출 규칙에 따라 주소를 조합한다.
            // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
            var fullAddr = ''; // 최종 주소 변수
            var extraAddr = ''; // 조합형 주소 변수

            // 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
            if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                fullAddr = data.roadAddress;

            } else { // 사용자가 지번 주소를 선택했을 경우(J)
                fullAddr = data.jibunAddress;
            }

            // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
            if(data.userSelectedType === 'R'){
                //법정동명이 있을 경우 추가한다.
                if(data.bname !== ''){
                    extraAddr += data.bname;
                }
                // 건물명이 있을 경우 추가한다.
                if(data.buildingName !== ''){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
                fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
            }

            // 우편번호와 주소 정보를 해당 필드에 넣는다.
            //document.getElementById('sample6_postcode').value = data.zonecode; //5자리 새우편번호 사용
         	
            //$(obj).parent().children().eq(0).val(fullAddr);
            //$(obj).parent().parent().children().eq(0).children().val(data.sido);

            //alert(JSON.stringify(data));
            
            $(obj).val(fullAddr);
                        
            // 커서를 상세주소 필드로 이동한다.
            //document.getElementById('sample6_address2').focus();
        }
	    
	}).open();	
	
}



function new_pwd(){


	document.location.href = "/baseinfo/change-password.do";

	
	

	
	
}




</script>

</head>
            
<body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        
        
        
        
  <div class="modal">
        <div class="modal-con" style="">
	            
                <div class="dispatch-wrapper" style="width:1200px;">
            

      <section class="dispatch-bottom-content active" style=" margin:auto; width:100%;">
                
      <form id="employeeInsertForm" action="/baseinfo/insert-employee.do" method="post" enctype="multipart/form-data">
                   <h3 style=" font-size: 20px; text-align: center; margin-bottom:20px; font-weight: 700; font-family: NotoSansBold,sans-serif;">회원 가입(직원)</h3>
                   <input type="hidden" id="empRole" name="empRole" value="U">
                    <!-- <input type="hidden" name="support" id="support" value=""> -->
                    <!-- <input type="hidden" name="empStatus" id="empStatus" value="01"> -->
               <div class="form-con clearfix" style="max-width:1200px;">
                   <!-- <div class="column"> -->
                   <div class="column" style="margin-bottom:5px">
	                   	<%-- <table>
	                            <tbody>
	                                <tr>
	                                    <td>회사선택<span style="color:#F00;">&nbsp;*</span></td>
	                                    <td>
	                                            <div class="select-con">
											        <select class="dropdown" name="companyId" id="companyId">
											        	<option value="">회사를 선택 하세요.</option>
											        	<c:forEach var="data" items="${companyList}" varStatus="status">
															<option value="${data.company_id}">${data.company_name}</option>
														</c:forEach>
											        </select>
											        <span></span>
											    </div>
	                                    </td>
	                                </tr>
	                             </tbody>
	                        </table> --%>
	                       <table>
	                            <tbody>
	                                <tr>
	                                    <td>사원번호(아이디)<span style="color:#F00;">&nbsp;*</span></td>
	                                    <td class="widthAuto" style="">
	                                        <input type="text" placeholder="사원번호(아이디)" name="empId" id="empId">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>부서</td>
	                                    <td class="widthAuto" style="">
	                                        <input type="text" placeholder="부서" name="dept" id="dept">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>영문부서명</td>
	                                    <td class="widthAuto" style="">
	                                        <input type="text" placeholder="영문부서명" name="deptEngName" id="deptEngName">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>성명<span style="color:#F00;">&nbsp;*</span></td>
	                                    <td class="widthAuto" style="">
	                                        <input type="text" placeholder="성명" name="empName" id="empName">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>영문성명</td>
	                                    <td class="widthAuto" style="">
	                                        <input type="text" placeholder="영문성명" name="empEngName" id="empEngName">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>패스워드<span style="color:#F00;">&nbsp;*</span></td>
	                                    <td class="widthAuto" style="">
	                                        <input type="password" placeholder="패스워드(영문과 특수문자를 포함하여 6자리 이상으로 입력 하세요.)" name="empPwd" id="empPwd">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>패스워드 확인<span style="color:#F00;">&nbsp;*</span></td>
	                                    <td class="widthAuto" style="">
	                                        <input type="password" placeholder="패스워드 확인" name="empPwdChk" id="empPwdChk">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>생년월일<span style="color:#F00;">&nbsp;*</span></td>
	                                    <td class="widthAuto" style="">
	                                        <input class="datepick" type="text" placeholder="생년월일"  readonly="readonly" name="birth" id="birth">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>양력/음력</td>
	                                    <td class="widthAuto" style="">
	                                        <input type="radio" id="solarLunarS" name="solarLunar" value="S" checked>
									    	<label for="solarLunarS">양력</label>
									    	<input type="radio" id="solarLunarL" name="solarLunar" value="L">
									    	<label for="solarLunarL">음력</label>
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        
	                        
	                        
	                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>연락처 1<span style="color:#F00;">&nbsp;*</span></td>
	                                    <td class="widthAuto" style="">
	                                        <input type="text" placeholder="연락처 1" name="phonePersonal" id="phonePersonal">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>연락처 2<span style="color:#F00;">&nbsp;*</span></td>
	                                    <td class="widthAuto" style="">
	                                        <input type="text" placeholder="연락처 2" name="phoneWork" id="phoneWork">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        <!-- <table>
	                            <tbody>
	                                <tr>
	                                    <td>연락처 3</td>
	                                    <td class="widthAuto" style="">
	                                        <input type="text" placeholder="연락처 3" name="phoneWorkSub" id="phoneWorkSub">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table> -->
	                        <!-- <table>
                            <tbody>
                                <tr>
                                    <td>근무지원</td>
                                    <td class="widthAuto" style="">
	                                        <input type="text" placeholder="근무지원" name="support" id="support">
	                                </td>
                                </tr>
                             </tbody>
                            </table> -->
	                        
	                        
                        </div>
                        <div class="column" style="margin-bottom:5px">
                        <table>
                            <tbody>
                                <tr>
                                    <td>직급</td>
                                    <td class="widthAuto" style="">
                                        <input type="text" placeholder="직급" name="empPosition" id="empPosition">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
                            <tbody>
                                <tr>
                                    <td>영문직급명</td>
                                    <td class="widthAuto" style="">
                                        <input type="text" placeholder="영문직급명" name="empEngPosition" id="empEngPosition">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
                            <tbody>
                                <tr>
                                    <td>직책</td>
                                    <td class="widthAuto" style="">
                                        <input type="text" placeholder="직책" name="empGrade" id="empGrade">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>성별<span style="color:#F00;">&nbsp;*</span></td>
	                                    <td class="widthAuto" style="">
	                                        <input type="radio" id="empSexM" name="empSex" value="M" checked>
									    	<label for="empSexM">남성</label>
									    	<input type="radio" id="empSexF" name="empSex" value="F">
									    	<label for="empSexF">여성</label>
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>기혼/미혼</td>
	                                    <td class="widthAuto" style="">
	                                        <input type="radio" id="marriageYnN" name="marriageYn" value="N" checked>
									    	<label for="marriageYnN">미혼</label>
									    	<input type="radio" id="marriageYnY" name="marriageYn" value="Y">
									    	<label for="marriageYnY">기혼</label>
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>결혼기념일</td>
	                                    <td class="widthAuto" style="">
	                                        <input class="datepick" type="text" placeholder="결혼기념일"  readonly="readonly" name="weddingAnniversary" id="weddingAnniversary">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>주소<span style="color:#F00;">&nbsp;*</span></td>
	                                    <td class="widthAuto" style="">
	                                        <input type="text" placeholder="주소" name="address" id="address" onclick="jusoSearch('departure',this);">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>상세주소<span style="color:#F00;">&nbsp;*</span></td>
	                                    <td class="widthAuto" style="">
	                                        <input type="text" placeholder="상세주소" name="addressDetail" id="addressDetail">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
                        <!-- <table>
                            <tbody>
                                <tr>
                                    <td>수습여부</td>
                                    <td class="widthAuto" style="">
                                        <input type="radio" id="probationYnY" name="probationYn" value="Y">
								    	<label for="probationYnY">수습중</label>
								    	<input type="radio" id="probationYnN" name="probationYn" value="N" checked>
								    	<label for="probationYnN">수습아님</label>
                                    </td>
                                </tr>
                            </tbody>
                        </table> -->
                        <!-- <table>
		                    <tbody>
		                        <tr>
		                            <td>적용율</td>
		                            <td class="widthAuto" style="">
		                                <input type="text" placeholder="적용율(숫자만입력)" name="deductionRate" id="deductionRate">
		                            </td>
		                        </tr>
		                    </tbody>
		                </table>
                        <table>
                            <tbody>
                                <tr>
                                    <td>수습기간 시작일</td>
                                    <td class="widthAuto" style="">
                                        <input class="datepick" type="text" placeholder="수습기간 시작일"  readonly="readonly" name="probationStartDt" id="probationStartDt">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
                            <tbody>
                                <tr>
                                    <td>수습기간 종료일</td>
                                    <td class="widthAuto" style="">
                                        <input class="datepick" type="text" placeholder="수습기간 종료일" readonly="readonly" name="probationEndDt" id="probationEndDt">
                                    </td>
                                </tr>
                            </tbody>
                        </table> -->
                        
                        <!-- <table>
                            <tbody>
                                <tr>
                                    <td>입사일<span style="color:#F00;">&nbsp;*</span></td>
                                    <td class="widthAuto" style="">
                                        <input class="datepick" type="text" placeholder="입사일"  readonly="readonly" name="joinDt" id="joinDt">
                                    </td>
                                </tr>
                            </tbody>
                        </table> -->
                        <!-- <table>
                            <tbody>
                                <tr>
                                    <td>퇴사일</td>
                                    <td class="widthAuto" style="">
                                        <input class="datepick" type="text" placeholder="퇴사일" readonly="readonly" name="resignDt" id="resignDt">
                                    </td>
                                </tr>
                            </tbody>
                        </table> -->
                        
                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>이메일<span style="color:#F00;">&nbsp;*</span></td>
	                                    <td class="widthAuto" style="">
	                                        <input type="text" placeholder="외부 이메일" name="emailPersonal" id="emailPersonal">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        <!-- <table>
	                            <tbody>
	                                <tr>
	                                    <td>회사 이메일</td>
	                                    <td class="widthAuto" style="">
	                                        <input type="text" placeholder="업무용 휴대폰" name="emailWork" id="emailWork">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table> -->
                            <table>
	                            <tbody>
	                                <tr>
	                                    <td>파일첨부</td>
	                                    <td class="widthAuto" style="">
	                                        <input type="file" id="upload"  name="bbsFile" multiple>
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
                            <!-- <table>
	                            <tbody>
	                                <tr>
	                                    <td>비고</td>
	                                    <td class="widthAuto" style="">
	                                        <input type="text" placeholder="비고" name="note" id="note">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table> -->
	                      </div>  
                 
               		</div>
                
                    <div class="confirmation">
                        <div class="confirm">
                            <input type="button" value="회원가입" onclick="javascript:addEmployee();">
                        </div>
                        <div class="cancel">
                            <input type="button" value="취소" onclick="javascript:popUpClose(this);">
                        </div>
                    </div>
                </form>          
                
                <form id="driverInsertForm" action="/environment/insert-driver.do"  method="post" enctype="multipart/form-data">
                   <h3 style="font-size: 20px; text-align: center; margin-bottom:20px; font-weight: 700; font-family: NotoSansBold,sans-serif;">회원 가입(기사)</h3>
                
                <div class="form-con clearfix" style="max-width:1200px;">
                	<div class="column" style="margin-bottom:5px">
                
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>차량소유주<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="차량소유주" name="driverOwner" id="driverOwner">
	                            </td>
	                            
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>상호명</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="상호명" name="businessName" id="businessName">
	                            </td>
	                            
	                        </tr>
	                    </tbody>
	                </table>
	                <!-- <table>
	                    <tbody>
	                        <tr>
	                            <td>소속*</td>
	                            <td class="widthAuto" style=""> -->
	                                <!-- <input type="text" placeholder="소속" name="assignCompany" id="assignCompany"> -->
	                                <%-- <div class="select-con">
								        <select class="dropdown" name="assignCompany" id="assignCompany">
								        	<option value="">소속을 선택 하세요.</option>
								        	<c:forEach var="data" items="${companyList}" varStatus="status">
												<option value="${data.company_id}">${data.company_name}</option>
											</c:forEach>
											<option value="etc">소속없음</option>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table> --%>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>구분<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <!-- <input type="text" placeholder="소속" name="driverKind" id="driverKind"> -->
	                                <div class="select-con">
								        <select class="dropdown" name="driverKind" id="driverKind">
								        	<option value="">구분을 선택 하세요.</option>
								            <option value="00">직영</option>
								            <option value="03">직영-수수료</option>
								            <option value="01">지입</option>
								            <option value="02">외부</option>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
                            <tbody>
                                <tr>
                                    <td>아이디<span style="color:#F00;">&nbsp;*</span></td>
                                    <td class="widthAuto" style="">
                                        <input type="text" placeholder="아이디" name="driverId" id="driverId">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>이름<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="이름" name="driverName" id="driverName">
	                            </td>
	                            
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
                           <tbody>
                               <tr>
                                   <td>패스워드<span style="color:#F00;">&nbsp;*</span></td>
                                   <td class="widthAuto" style="">
                                       <input type="password" placeholder="패스워드(영문과 특수문자를 포함하여 6자리 이상으로 입력 하세요.)" name="driverPwd" id="driverPwd">
                                   </td>
                               </tr>
                           </tbody>
                       </table>
                       <table>
                           <tbody>
                               <tr>
                                   <td>패스워드 확인<span style="color:#F00;">&nbsp;*</span></td>
                                   <td class="widthAuto" style="">
                                       <input type="password" placeholder="패스워드 확인" name="driverPwdChk" id="driverPwdChk">
                                   </td>
                               </tr>
                           </tbody>
                       </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>연락처<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="연락처" name="phoneNum" id="phoneNum">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>비상연락처1<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="비상연락처1" name="phoneNumA" id="phoneNumA">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>비상연락처2<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="비상연락처2" name="phoneNumB" id="phoneNumB">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>주민번호<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="주민번호" name="residentRegistrationNumber" id="residentRegistrationNumber">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>사업자구분<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <!-- <input type="text" placeholder="사업자구분" name="companyKind" id="companyKind"> -->
	                                <div class="select-con">
								        <select class="dropdown" name="companyKind" id="companyKind">
								        	<option value="">사업자 구분을 선택 하세요.</option>
								            <option value="00">법인</option>
								            <option value="01">개인</option>
								            <option value="02">기타</option>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>사업자번호<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="사업자번호" name="businessLicenseNumber" id="businessLicenseNumber">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>업태</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="업태" name="businessCondition" id="businessCondition">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>종목</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="종목" name="businessKind" id="businessKind">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                
	                </div>
	                
	                <div class="column">
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>자격증번호<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="자격증번호" name="driverLicense" id="driverLicense">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>차종<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="차종" name="carKind" id="carKind">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table style="display:none;">
	                    <tbody>
	                        <tr>
	                            <td>약칭</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="약칭" name="carNickname" id="carNickname">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>차량소속<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <!-- <input type="text" placeholder="약칭" name="carNickname" id="carNickname"> -->
	                                <div class="select-con">
								        <select class="dropdown" name="carAssignCompany" id="carAssignCompany">
								        	<option value="">차량소속을 선택 하세요.</option>
								            <option value="S">셀프</option>
								            <option value="C">캐리어</option>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>차량번호<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="차량번호" name="carNum" id="carNum">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                
	                 <table>
	                    <tbody>
	                        <tr>
	                            <td>차대번호</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="차대번호" name="carIdNum" id="carIdNum">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>사업장주소<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="사업장주소" name="address" id="driverAddress"  onclick="jusoSearch('departure',this);">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>사업장주소상세<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="사업장주소상세" name="addressDetail" id="driverAddressDetail">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
		                <tbody>
			                <tr>
				                <td>보험가입여부</td>
				                <td class="widthAuto" style="">
				                	<input type="radio" id="insuranceY" name="insurance" value="Y">
									<label for="insuranceY">예</label>
					                <input type="radio" id="insuranceN" name="insurance" value="N" checked>
									<label for="insuranceN">아니오</label>
				                </td>
			                </tr>
		                </tbody>
	                </table>
	                
	                
	                <table>
		                <tbody>
			                <tr>
				                <td>보세차량</td>
				                <td class="widthAuto" style="">
				                	<input type="radio" id="bondedCarY" name="bondedCar" value="Y">
									<label for="bondedCarY">예</label>
					                <input type="radio" id="bondedCarN" name="bondedCar" value="N" checked>
									<label for="bondedCarN">아니오</label>
				                </td>
			                </tr>
		                </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>입사일</td>
	                            <td class="widthAuto" style="">
	                                <input class="datepick" type="text"  placeholder="입사일(YYYY-MM-DD)" readonly="readonly" name="joinDt" id="driverJoinDt">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <!-- <table>
	                    <tbody>
	                        <tr>
	                            <td>퇴사일</td>
	                            <td class="widthAuto" style="">
	                                <input class="datepick" type="text" readonly="readonly" placeholder="퇴사일(YYYY-MM-DD)" name="resignDt" id="driverResignDt">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table> -->
	                
	                <!-- <table>
	                    <tbody>
	                        <tr>
	                            <td>적용요율<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="적용요율(숫자만입력)" name="deductionRate" id="driverDeductionRate">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>지입료</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="지입료" name="driverBalance" id="driverBalance">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>예치금</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="예치금" name="driverDeposit" id="driverDeposit">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table> -->
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>은행명</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="은행명" name="bankName" id="bankName">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>예금주</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="예금주" name="depositor" id="depositor">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>계좌번호</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="계좌번호" name="accountNumber" id="accountNumber">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
                          <tbody>
                              <tr>
                                  <td>첨부파일</td>
                                  <td class="widthAuto" style="">
                                      <input type="file" id="driverUpload"  name="bbsFile" multiple>
                                  </td>
                              </tr>
                          </tbody>
                      </table>
	                <!-- <table>
	                    <tbody>
	                        <tr>
	                            <td>비고</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="비고" name="etc" id="etc">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table> -->
	                <!-- <table>
	                    <tbody>
	                        <tr>
	                            <td>차고지</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="차고지" name="carGarage" id="carGarage">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table> -->
	                
	                </div>
	            
                </div>
                
                
                <div class="confirmation">
	                <div class="confirm">
	                	<input type="button" value="회원가입" onclick="javascript:addDriver();">
	                </div>
	                <div class="cancel">
                            <input type="button" value="취소" onclick="javascript:popUpClose(this);">
                     </div>
                </div>
                </form>
                
                
                
                
                
                
                 </section>
        </div>
               
        </div>

    </div>      
    
        <section class="login">
            <div class="login-container">
                <div class="login-form">
                    <div class="top-content">
                        <div class="title">
                            <h3 class="t1">TEST</h3>
                            <h3 class="t2">Access</h3>
                        </div>
                    </div>
                    <div class="bottom-content">
                    	<form action="" id="loginForm">
                    		<div class="password d-table">
	                            <span class="d-tbc">회사선택</span>
	                            <div class="select-con">
									<select  class="dropdown" name="companyId" id="companyId" style="height:40px; width:200px;">
										<option value="">회사를 선택 하세요.</option>
											<c:forEach var="data" items="${companyList}" varStatus="status">
												<option value="${data.company_id}" <c:if test='${data.company_id == "company2"}'>selected</c:if>>${data.company_name}<c:if test='${data.company_kind == 00}'>(법인)</c:if><c:if test='${data.company_kind == 01}'>(개인)</c:if></option>
											</c:forEach>
									</select>
									<span></span>
								</div>
	                        </div>
	                        <div class="username d-table">
	                            <span class="d-tbc">아이디</span>
	                            <div>
	                                <input type="text" placeholder="사용자 아이디를 입력해 주세요" class="d-tbc" id="loginId" name="empId">
	                            </div>
	                        </div>
	                        <div class="password d-table">
	                            <span class="d-tbc">패스워드</span>
	                            <div>
	                                <input type="password" class="d-tbc" id="loginPwd" name="empPwd"   onkeypress="if(event.keyCode=='13') loginProcess();">
	                            </div>
	                        </div>
                        </form>
                        <div class="login-btn-container d-table">
                            <span class="d-tbc"></span>
                            <div>
                                <a style="cursor:pointer;" onclick="javascript:loginProcess();" class="login-btn d-tbc">로그인</a>
                            </div>
                        </div>
                        <!-- <div class="login-btn-container d-table">
                            <span class="d-tbc"></span>
                            <div>
                                <a style="cursor:pointer;" onclick="javascript:loginProcess();" class="login-btn d-tbc">회원가입</a>
                            </div>
                        </div> -->
                        
                        <div class="savepw d-table">
                            <span class="d-tbc"></span>
                            <div style="text-align:left;">
                                <label for="rememberme" class="d-tbc" style="width:400px;">
                                    <input type="checkbox" id="rememberme">
                                    <span class="checkimg">
                                        <i class="glyphicon glyphicon-ok" aria-hidden="true"></i>
                                    </span>
                                    <span class="checklabel" style="padding-left:0%;">아이디 저장 </span>
                                    <span class="checklabel" style="display:inline; margin-left:20px; margin-bottom: 20px;">회원가입(<a style="cursor:pointer; " href="javascript:popup(this,'employee');">직원</a>&nbsp;/&nbsp;<a style="cursor:pointer;" href="javascript:popup(this,'driver');">기사</a>)</span>
                                    <div id="pwbox" style="height:10px;"></div>
                                    <span class="checklabel" style="display:inline; margin-left:165px; "><a style="cursor:pointer; margin-top:20px; " href="javascript:new_pwd(this,'employee');">비밀번호 변경</a></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
        <script src="/js/vendor/bootstrap.min.js"></script>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="/js/moment.js"></script>
        <script src="/js/jquery.alert.min.js"></script>
        <script src="/js/pignose.calendar.full.js"></script>
        <script src="/js/dcalendar.picker.js"></script>
        <script src="/js/main.js"></script>
        
        
        
        
        
        
    </body>

</html>
