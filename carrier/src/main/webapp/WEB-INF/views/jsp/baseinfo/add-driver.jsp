<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%

%>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script type="text/javascript">
$(document).ready(function(){

});

function addDriver(){



	
	

	if($("#driverOwner").val() == ""){
		alert("소유주가 입력되지 않았습니다.");
		return false;
	}
	if($("#driverKind").val() == ""){
		alert("구분이 입력되지 않았습니다.");
		return false;
	}
	
	if($("#driverName").val() == ""){
		alert("이름이 입력되지 않았습니다.");
		return false;
	}
	
	if($("#phoneNum").val() == ""){
		alert("연락처가 입력되지 않았습니다.");
		return false;
	}
	
	if($("#residentRegistrationNumber").val() == ""){
		alert("주민번호가 입력되지 않았습니다.");
		return false;
	}
	
	if($("#companyKind").val() == ""){
		alert("사업자 구분이 입력되지 않았습니다.");
		return false;
	}
	
	if($("#businessLicenseNumber").val() == ""){
		alert("사업자 번호가 입력되지 않았습니다.");
		return false;
	}
	if($("#address").val() == ""){
		alert("사업장 주소가 입력되지 않았습니다.");
		return false;
	}
	if($("#addressDetail").val() == ""){
		alert("사업장 주소 상세가 입력되지 않았습니다.");
		return false;
	}
	if($("#carKind").val() == ""){
		alert("차종이 입력되지 않았습니다.");
		return false;
	}
	if($("#carAssignCompany").val() == ""){
		alert("차량 소속이 입력되지 않았습니다.");
		return false;
	}
	if($("#carNum").val() == ""){
		alert("차량번호가 입력되지 않았습니다.");
		return false;
	}
	if($("#carIdNum").val() == ""){
		alert("차대번호가 입력되지 않았습니다.");
		return false;
	}
		
	if($("#carAssignCompany").val() == ""){
		alert("차량소속이 입력되지 않았습니다.");
		return false;
	}
	
	if($("#driverLicense").val() == ""){
		alert("자격증번호가 입력되지 않았습니다.");
		return false;
	}
	
	/* if($("#assignCompany").val() == ""){
		alert("소속이 입력되지 않았습니다.");
		return false;
	} */
	
	if($("#deductionRate").val() == ""){
		alert("적용요율이 입력되지 않았습니다.");
		return false;
	}
	
	
    
	if(confirm("등록 하시겠습니까?")){
		$("#insertForm").attr("action","/baseinfo/insert-driver.do");
		$("#insertForm").submit();	
	}
	
}



function addPaymentDriver(){

	
	

	if($("#driverOwner").val() == ""){
		alert("소유주가 입력되지 않았습니다.");
		return false;
	}
	if($("#driverKind").val() == ""){
		alert("구분이 입력되지 않았습니다.");
		return false;
	}
	
	if($("#driverName").val() == ""){
		alert("이름이 입력되지 않았습니다.");
		return false;
	}
	
	if($("#phoneNum").val() == ""){
		alert("연락처가 입력되지 않았습니다.");
		return false;
	}
	
	if($("#residentRegistrationNumber").val() == ""){
		alert("주민번호가 입력되지 않았습니다.");
		return false;
	}
	
	if($("#companyKind").val() == ""){
		alert("사업자 구분이 입력되지 않았습니다.");
		return false;
	}
	
	if($("#businessLicenseNumber").val() == ""){
		alert("사업자 번호가 입력되지 않았습니다.");
		return false;
	}
	if($("#address").val() == ""){
		alert("사업장 주소가 입력되지 않았습니다.");
		return false;
	}
	if($("#addressDetail").val() == ""){
		alert("사업장 주소 상세가 입력되지 않았습니다.");
		return false;
	}
	if($("#carKind").val() == ""){
		alert("차종이 입력되지 않았습니다.");
		return false;
	}
	if($("#carAssignCompany").val() == ""){
		alert("차량 소속이 입력되지 않았습니다.");
		return false;
	}
	if($("#carNum").val() == ""){
		alert("차량번호가 입력되지 않았습니다.");
		return false;
	}
	if($("#carIdNum").val() == ""){
		alert("차대번호가 입력되지 않았습니다.");
		return false;
	}
		
	if($("#carAssignCompany").val() == ""){
		alert("차량소속이 입력되지 않았습니다.");
		return false;
	}
	
	if($("#driverLicense").val() == ""){
		alert("자격증번호가 입력되지 않았습니다.");
		return false;
	}
	
	/* if($("#assignCompany").val() == ""){
		alert("소속이 입력되지 않았습니다.");
		return false;
	} */
	
	if($("#deductionRate").val() == ""){
		alert("적용요율이 입력되지 않았습니다.");
		return false;
	}
	
	
    
	if(confirm("등록 하시겠습니까?")){
		$("#insertForm").attr("action","/baseinfo/insertPaymentDriver.do");
		$("#insertForm").submit();
	}
	
}


function jusoSearch(where,obj){
	
	new daum.Postcode({
	    oncomplete: function(data) {
	        // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

            // 각 주소의 노출 규칙에 따라 주소를 조합한다.
            // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
            var fullAddr = ''; // 최종 주소 변수
            var extraAddr = ''; // 조합형 주소 변수

            // 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
            if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                fullAddr = data.roadAddress;

            } else { // 사용자가 지번 주소를 선택했을 경우(J)
                fullAddr = data.jibunAddress;
            }

            // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
            if(data.userSelectedType === 'R'){
                //법정동명이 있을 경우 추가한다.
                if(data.bname !== ''){
                    extraAddr += data.bname;
                }
                // 건물명이 있을 경우 추가한다.
                if(data.buildingName !== ''){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
                fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
            }

            // 우편번호와 주소 정보를 해당 필드에 넣는다.
            //document.getElementById('sample6_postcode').value = data.zonecode; //5자리 새우편번호 사용
         	
            //$(obj).parent().children().eq(0).val(fullAddr);
            //$(obj).parent().parent().children().eq(0).children().val(data.sido);

            //alert(JSON.stringify(data));
            
            $(obj).val(fullAddr);
                        
            // 커서를 상세주소 필드로 이동한다.
            //document.getElementById('sample6_address2').focus();
        }
	    
	}).open();	
	
}



</script>
		<!-- <section class="side-nav">
            <ul>
                <li class=""><a href="/baseinfo/employee.do">직원관리</a></li>
                <li class="active"><a href="/baseinfo/driver.do">기사관리</a></li>
                <li class=""><a href="/baseinfo/customer.do">거래처관리</a></li>
                <li class=""><a href="/baseinfo/company.do">회사정보관리</a></li>
            </ul>
        </section> -->
     <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">HOME</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">기사 등록</a></li>
                </ul>
            </div>

            <div class="up-dl clearfix header-search">
                 <table>
                    <tbody>
                        <tr>
                            <td style="width: 145px;">
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">선택바
                                    <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">HTML</a></li>
                                        <li><a href="#">CSS</a></li>
                                        <li><a href="#">JavaScript</a></li>
                                    </ul>
                                </div>
                            </td>
                            <td style="width: 190px;">
                                <form action="">
                                    <input type="text" class="search-box">
                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
                                    <div class="search-result">
                                        <!-- No result -->
                                        <div class="no-result d-table">
                                            <div class="d-tbc">
                                                <i class="fa fa-exclamation-triangle fa-3x" aria-hidden="true"></i>
                                                <span>No results have been found.</span>
                                            </div>
                                        </div>

                                        <!-- Result type1 -->
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Campaigns</p>
                                            <a href="" class="result-sub"><span>네오퓨쳐</span></a>
                                            <p class="camp-type"><span>Campaign type:</span> <span>Powerlink</span></p>
                                            <p class="camp-id"><span>Campaign ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                        <!-- Result type2 -->
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Ad groups</p>
                                            <a href="" class="result-sub"><span>네오퓨쳐</span>_#0007</a>
                                            <p class="camp-type"><span>Campaign type:</span> <span>Powerlink</span></p>
                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
                                            <p class="adgroup-id"><span>Ad group ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                        <!-- Result type3 -->
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Ads - Powerlink</p>
                                            <div class="result-box">
                                                <p class="title">네오퓨쳐</p>
                                                <p class="content">일반 홈페이지에서 신개념 융복합 멀티미디어 홈피이지 제작, 견적상담 환영</p>

                                                <ul>
                                                    <li>View URL: http://www.neofss.com</li>
                                                    <li>Linked URL: http://www.neofss.com</li>
                                                </ul>
                                            </div>
                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
                                            <p class="adgroup"><span>Ad group:</span> <span class="red">네오퓨쳐</span>_#0003</p>
                                            <p class="adgroup-id"><span>Ad ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                        <!-- Result type4 -->
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Ads - PowerContents</p>
                                            <div class="result-box w-img">
                                                <div class="d-table">
                                                    <div class="d-tbc">
                                                        <img src="/img/ads-img.png" alt="">
                                                    </div>
                                                    <div class="d-tbc">
                                                        <p class="title">홈페이지제작 이러면 망한다!</p>
                                                        <p class="content">홈페이지제작! 대충 업체에 맡기면 된다는 생각으로 접근하면, 십중팔구는 실패하는 이유와 홈페이지 제작에 관한 최소한의 지식은 가지고 업체를 선정해야 돈과 시간을 낭비하는 일을 줄일수 있다.</p>
                                                    </div>
                                                </div>
                                                <ul>
                                                    <li>Content created: 2017-12-15</li>
                                                    <li>Brand Name: 네오퓨쳐</li>
                                                    <li>View URL: http://blog.naver.com/marujeen</li>
                                                    <li>Linked URL: http://blog.naver.com/marujeen</li>
                                                </ul>
                                            </div>
                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
                                            <p class="adgroup"><span>Ad group:</span> <span class="red">네오퓨쳐</span>_#0003</p>
                                            <p class="adgroup-id"><span>Ad ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                    </div>
                                </form>
                            </td>
                            <td>
                                <input type="button" id="btn-search" value="검색" class="btn-primary">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
<!-- 
            <div class="dispatch-btn-container">
               
                <div class="dispatch-btn">
                    <i class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div>
            </div> -->
        </section>

        <div class="dispatch-wrapper">

            <section class="dispatch-bottom-content active" style=" margin:auto;">
               <form id="insertForm" action="/environment/insert-driver.do"  method="post" enctype="multipart/form-data">
                   <h3 style="font-size: 20px; text-align: center; margin-bottom:20px; font-weight: 700; font-family: NotoSansBold,sans-serif;"><c:if test="${paramMap.driverForpayment eq 'Y'}">지입료 </c:if>기사정보 등록</h3>
                
                <div class="form-con clearfix" style="max-width:1200px;">
                	<div class="column-sub" style="margin-bottom:5px">
                
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>소유주<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="소유주" name="driverOwner" id="driverOwner">
	                            </td>
	                            
	                        </tr>
	                    </tbody>
	                </table>
	                <!-- <table>
	                    <tbody>
	                        <tr>
	                            <td>소속*</td>
	                            <td class="widthAuto" style=""> -->
	                                <!-- <input type="text" placeholder="소속" name="assignCompany" id="assignCompany"> -->
	                                <%-- <div class="select-con">
								        <select class="dropdown" name="assignCompany" id="assignCompany">
								        	<option value="">소속을 선택 하세요.</option>
								        	<c:forEach var="data" items="${companyList}" varStatus="status">
												<option value="${data.company_id}">${data.company_name}</option>
											</c:forEach>
											<option value="etc">소속없음</option>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table> --%>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>구분<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <!-- <input type="text" placeholder="소속" name="driverKind" id="driverKind"> -->
	                                <div class="select-con">
								        <select class="dropdown" name="driverKind" id="driverKind">
								        	<option value="">구분을 선택 하세요.</option>
								        	<c:if test="${paramMap.driverForpayment ne 'Y'}">
								            	<option value="00">직영</option>
								            	<option value="03">직영-수수료</option>
								            </c:if>
								            <option <c:if test="${paramMap.driverForpayment eq 'Y'}">selected</c:if> value="01">지입</option>
								            <option value="02">외부</option>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>이름<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="이름" name="driverName" id="driverName">
	                            </td>
	                            
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>연락처<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="연락처" name="phoneNum" id="phoneNum">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>주민번호<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="주민번호" name="residentRegistrationNumber" id="residentRegistrationNumber">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>사업자구분<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <!-- <input type="text" placeholder="사업자구분" name="companyKind" id="companyKind"> -->
	                                <div class="select-con">
								        <select class="dropdown" name="companyKind" id="companyKind">
								        	<option value="">사업자 구분을 선택 하세요.</option>
								            <option value="00">법인</option>
								            <option value="01">개인</option>
								            <option value="02">기타</option>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>사업자번호<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="사업자번호" name="businessLicenseNumber" id="businessLicenseNumber">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>사업장주소<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="사업장주소" name="address" id="address"  onclick="jusoSearch('departure',this);">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>사업장주소상세<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="사업장주소상세" name="addressDetail" id="addressDetail">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>차종<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="차종" name="carKind" id="carKind">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table style="display:none;">
	                    <tbody>
	                        <tr>
	                            <td>약칭</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="약칭" name="carNickname" id="carNickname">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>차량소속<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <!-- <input type="text" placeholder="약칭" name="carNickname" id="carNickname"> -->
	                                <div class="select-con">
								        <select class="dropdown" name="carAssignCompany" id="carAssignCompany">
								        	<option value="">차량소속을 선택 하세요.</option>
								            <option value="S">셀프</option>
								            <option value="C">캐리어</option>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>차량번호<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="차량번호" name="carNum" id="carNum">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                
	                 <table>
	                    <tbody>
	                        <tr>
	                            <td>차대번호<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="차대번호" name="carIdNum" id="carIdNum">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                
	                </div>
	                
	                <div class="column-sub">
	                
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>자격증번호<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="자격증번호" name="driverLicense" id="driverLicense">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>업태</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="업태" name="businessCondition" id="businessCondition">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>종목</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="종목" name="businessKind" id="businessKind">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
		                <tbody>
			                <tr>
				                <td>보세차량</td>
				                <td class="widthAuto" style="">
					                <input type="radio" id="bondedCarN" name="bondedCar" value="N" checked>
									<label for="bondedCarN">아니오</label>
									<input type="radio" id="bondedCarY" name="bondedCar" value="Y">
									<label for="bondedCarY">예</label>
				                </td>
			                </tr>
		                </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>입사일</td>
	                            <td class="widthAuto" style="">
	                                <input class="datepick" type="text"  placeholder="입사일(YYYY-MM-DD)" readonly="readonly" name="joinDt" id="joinDt">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>퇴사일</td>
	                            <td class="widthAuto" style="">
	                                <input class="datepick" type="text" readonly="readonly" placeholder="퇴사일(YYYY-MM-DD)" name="resignDt" id="resignDt">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>신고상태</td>
	                            <td class="widthAuto" style="">
	                                <input type="radio" id="registerStatusI" name="registerStatus" value="I">
									<label for="registerStatusI">입사신고</label>
									<input type="radio" id="registerStatusO" name="registerStatus" value="O">
									<label for="registerStatusO">퇴사신고</label>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                
	                <table>
                          <tbody>
                              <tr>
                                  <td>첨부파일</td>
                                  <td class="widthAuto" style="">
                                      <input type="file" id="upload"  name="bbsFile" multiple>
                                  </td>
                              </tr>
                          </tbody>
                      </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>비고</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="비고" name="etc" id="etc">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                
	                
	                </div>
	                
	                <div class="column-sub">
	                
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>적용요율<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="적용요율(숫자만입력)" name="deductionRate" id="deductionRate">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>지입료</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="지입료" name="driverBalance" id="driverBalance">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>예치금</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="예치금" name="driverDeposit" id="driverDeposit">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>은행명</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="은행명" name="bankName" id="bankName">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>예금주</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="예금주" name="depositor" id="depositor">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>계좌번호</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="계좌번호" name="accountNumber" id="accountNumber">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                         <table>
	                    <tbody>
	                        <tr>
	                            <td>기사 예치금 여부<!-- <span style="color:#F00;">&nbsp;*</span> --></td>
	                            <td class="widthAuto" style="">
	                                <!-- <input type="text" placeholder="약칭" name="carNickname" id="carNickname"> -->
	                                <div class="select-con">
								        <select class="dropdown" name="driverDepositYn" id="driverDepositYn">
								        	<option value="N" <c:if test='${driverMap.driver_deposit_yn eq "N" }'> selected="selected"</c:if>>미사용</option>
								            <option value="Y" <c:if test='${driverMap.driver_deposit_yn eq "Y" }'> selected="selected"</c:if>>사용</option>
							        	</select>
								        <span></span>
								    </div>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	               <table>
	                    <tbody>
	                        <tr>
	                            <td>기사 지입료 여부<!-- <span style="color:#F00;">&nbsp;*</span> --></td>
	                            <td class="widthAuto" style="">
	                                <!-- <input type="text" placeholder="약칭" name="carNickname" id="carNickname"> -->
	                                <div class="select-con">
								        <select class="dropdown" name="driverPaymentYn" id="driverPaymentYn">
								        	<option value="N" <c:if test='${driverMap.driver_payment_yn eq "N" }'> selected="selected"</c:if>>미사용</option>
								            <option value="Y" <c:if test='${driverMap.driver_payment_yn eq "Y" }'> selected="selected"</c:if>>사용</option>
							        	</select>
								        <span></span>
								    </div>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                
	                
	                <!-- <table>
	                    <tbody>
	                        <tr>
	                            <td>차고지</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="차고지" name="carGarage" id="carGarage">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table> -->
	                </div>
	                
	                
	                
	                
	               
	                
                
                </div>
                
                
                <div class="confirmation">
 	       <c:if test="${paramMap.driverForpayment ne 'Y'}">
	                <div class="confirm">
	                	<input type="button" value="기사 등록" onclick="javascript:addDriver();">
	                </div>
    		</c:if>
	                
	                 <c:if test="${paramMap.driverForpayment eq 'Y'}">
	                	<div class="confirm">
	                		<input type="button" value="지입 기사 등록" onclick="javascript:addPaymentDriver();">
	                	</div>
			                    </c:if>
                </div>
                </form>
                <!-- <div class="table-pagination text-center">
                    <ul class="pagination">
                        <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="curr-page"><a href="#">1</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
                    </ul>
                </div> -->
            </section>
        </div>
