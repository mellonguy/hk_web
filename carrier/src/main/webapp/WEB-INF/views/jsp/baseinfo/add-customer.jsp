<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%

%>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script type="text/javascript">
$(document).ready(function(){

	
	
});

function addCustomer(){

		var file = document.getElementById("upload");
	 	for(var i = 0; i < file.files.length; i++){
			var ext = file.files[i].name.split(".").pop().toLowerCase();
			if($.inArray(ext, ["jpeg","jpg","gif","png","pdf"]) == -1){
				alert("업로드 할 수 없는 파일이 있습니다.");
				$("#upload").val("");
				return false;
			}
		}
        
        if($("#customerName").val() ==""){
			alert("회사명이 입력되지 않았습니다.");
			return false;
		}else{
			$("#customerName").val($.trim($("#customerName").val()));
		}
        if($("#customerOwnerName").val() ==""){
			alert("대표자명이 입력되지 않았습니다.");
			return false;
		}
        if($("#customerKind").val() ==""){
			alert("회사구분이 입력되지 않았습니다.");
			return false;
		}
        
        if($("#businessLicenseNumber").val() ==""){
			alert("사업자번호가 입력되지 않았습니다.");
			return false;
		}

        if($("#paymentKind").val() ==""){
			alert("결제방법이 입력되지 않았습니다.");
			return false;
		}

        if($("#billingKind").val() ==""){
			alert("증빙구분이 입력되지 않았습니다.");
			return false;
		}
        
        insertCharge();
        insertDriver();
        insertPaymentCharge();
        
		if(confirm("등록 하시겠습니까?")){
			$("#insertForm").attr("action","/baseinfo/insert-customer.do");
			$("#insertForm").submit();	
		}
	
}



function jusoSearch(where,obj){
	
	new daum.Postcode({
	    oncomplete: function(data) {
	        // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

            // 각 주소의 노출 규칙에 따라 주소를 조합한다.
            // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
            var fullAddr = ''; // 최종 주소 변수
            var extraAddr = ''; // 조합형 주소 변수

            // 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
            if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                fullAddr = data.roadAddress;

            } else { // 사용자가 지번 주소를 선택했을 경우(J)
                fullAddr = data.jibunAddress;
            }

            // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
            if(data.userSelectedType === 'R'){
                //법정동명이 있을 경우 추가한다.
                if(data.bname !== ''){
                    extraAddr += data.bname;
                }
                // 건물명이 있을 경우 추가한다.
                if(data.buildingName !== ''){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
                fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
            }

            // 우편번호와 주소 정보를 해당 필드에 넣는다.
            //document.getElementById('sample6_postcode').value = data.zonecode; //5자리 새우편번호 사용
         	
            //$(obj).parent().children().eq(0).val(fullAddr);
            //$(obj).parent().parent().children().eq(0).children().val(data.sido);

            //alert(JSON.stringify(data));
            
            $(obj).val(fullAddr);
                        
            // 커서를 상세주소 필드로 이동한다.
            //document.getElementById('sample6_address2').focus();
        }
	    
	}).open();	
	
}



function insertCharge(){
	
	var chargeList= new Array();
	var size = $("#addLocation").find("tr").length;
	var result = "";
	 $("#addLocation").find("tr").each(function(index,element){
		 if($(this).children().eq(1).children().val() != "" || $(this).children().eq(2).children().val() != "" || $(this).children().eq(3).children().val() != "" || $(this).children().eq(4).children().val() != ""){
			 var chargeInfo= new Object();
			 chargeInfo.department = $(this).children().eq(1).children().val();
			 chargeInfo.name = $(this).children().eq(2).children().val();
			 chargeInfo.phoneNum = $(this).children().eq(3).children().val();
			 chargeInfo.address = $(this).children().eq(4).children().val();
			 chargeInfo.email = $(this).children().eq(5).children().val();
			 chargeInfo.etc = $(this).children().eq(6).children().val();
			 chargeList.push(chargeInfo);	 
		 }
      }); 
	
	 if(chargeList.length >= 1){
		 /* $("#personInCharge").val(result); */
		 $("#personInChargeInfo").val(JSON.stringify({chargeList : chargeList}));
	 }
	
}


function insertPaymentCharge(){
	
	var paymentChargeList= new Array();
	var size = $("#paymentAddLocation").find("tr").length;
	var result = "";
	 $("#paymentAddLocation").find("tr").each(function(index,element){
		 if($(this).children().eq(1).children().val() != "" || $(this).children().eq(2).children().val() != "" || $(this).children().eq(3).children().val() != "" || $(this).children().eq(4).children().val() != ""){
			 var paymentPersonInChargeInfo= new Object();
			 paymentPersonInChargeInfo.payment_person_in_charge_id = $(this).children().eq(1).attr("id");
			 paymentPersonInChargeInfo.payment_department = $(this).children().eq(1).children().val();
			 paymentPersonInChargeInfo.payment_name = $(this).children().eq(2).children().val();
			 paymentPersonInChargeInfo.payment_phone_num = $(this).children().eq(3).children().val();
			 paymentPersonInChargeInfo.address = $(this).children().eq(4).children().val();
			 paymentPersonInChargeInfo.email = $(this).children().eq(5).children().val();
			 paymentPersonInChargeInfo.etc = $(this).children().eq(6).children().val();
			 paymentChargeList.push(paymentPersonInChargeInfo);	 
		 }
      }); 
	
	 if(paymentChargeList.length >= 1){
		 $("#paymentPersonInChargeInfo").val(JSON.stringify({paymentChargeList : paymentChargeList}));
	 }
	
}






function insertDriver(){
	
	var driverList= new Array();
	var size = $("#driverAddLocation").find("tr").length;
	var result = "";
	 $("#driverAddLocation").find("tr").each(function(index,element){
		 if($(this).children().eq(1).children().val() != "" || $(this).children().eq(2).children().val() != "" || $(this).children().eq(3).children().val() != "" || $(this).children().eq(4).children().val() != ""){
			 var driverInfo= new Object();
			 driverInfo.driver_name = $(this).children().eq(1).children().val();
			 driverInfo.phone_num = $(this).children().eq(2).children().val();
			 driverInfo.car_kind = $(this).children().eq(3).children().val();
			 driverInfo.car_num = $(this).children().eq(4).children().val();
			 driverList.push(driverInfo);	 
		 }
      }); 
	
	 if(driverList.length >= 1){
		 $("#driverInfo").val(JSON.stringify({driverList : driverList}));
	 }
	
}


function addDriver(){
	
	var result = "";
	result += '<tr><td style="display:none;"></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="기사명" name=""></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="연락처" name=""></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="차종" name=""></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="차량번호" name=""></td>';
	result += '<td class="widthAuto" style=""><input style="display:inline; float:right; border:2px solid #eee;" type="button" onclick="javascript:deleteDriver(this);" value="삭제" class="btn-normal"></td></tr>';
	$("#driverRowspanCnt").attr("rowspan",Number($("#driverRowspanCnt").attr("rowspan"))+1);
	$("#driverAddLocation").append(result);
	
}

function deleteDriver(obj){
	
	$("#driverRowspanCnt").attr("rowspan",Number($("#driverRowspanCnt").attr("rowspan"))-1);
	$(obj).parent().parent().remove();
	
}




function addPersonInCharge(){
	
	var result = "";
	result += '<tr><td style="display:none;"></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="딤당자부서" name=""></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="담당자명" name=""></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="연락처" name=""></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="주소" name=""></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="이메일" name=""></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="비고" name=""></td>';
	result += '<td class="widthAuto" style=""><input style="display:inline; float:right; border:2px solid #eee;" type="button" onclick="javascript:deletePersonInCharge(this);" value="삭제" class="btn-normal"></td></tr>';
	$("#rowspanCnt").attr("rowspan",Number($("#rowspanCnt").attr("rowspan"))+1);
	$("#addLocation").append(result);

}




function addPaymentPersonInCharge(){
	
	var result = "";
	result += '<tr><td style="display:none;"></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="회계부서(거래처)" name=""></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="담당자명" name=""></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="연락처" name=""></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="주소" name=""></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="이메일" name=""></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="비고" name=""></td>';
	result += '<td class="widthAuto" style=""><input style="display:inline; float:right; border:2px solid #eee;" type="button" onclick="javascript:deletePersonInCharge(this);" value="삭제" class="btn-normal"></td></tr>';
	$("#paymentRowspanCnt").attr("rowspan",Number($("#paymentRowspanCnt").attr("rowspan"))+1);
	$("#paymentAddLocation").append(result);

}



function deletePersonInCharge(obj){
	
	$("#rowspanCnt").attr("rowspan",Number($("#rowspanCnt").attr("rowspan"))-1);
	$(obj).parent().parent().remove();
	
}

function deletePaymentPersonInCharge(obj){
	
	$("#paymentRowspanCnt").attr("rowspan",Number($("#paymentRowspanCnt").attr("rowspan"))-1);
	$(obj).parent().parent().remove();
	
}

</script>

<%-- <div class="modal">
        <div class="modal-con" style="width:1000px;">
	            <table class="urllist text-center iplist"> 
	                    <colgroup>
	                    </colgroup>
	                    <thead>
	                        <tr>
	                            <td>담당자부서</td>
	                            <td>담당자이름</td>
	                            <td>담당자연락처</td>
	                            <td>근무지주소</td>
	                            <td>담당자이메일</td>
	                            <td>비고</td>
	                        </tr>
	                        
	                    </thead>
	                    <tbody id="chargeList">
	                        <tr>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="department" >
	                                </div>
	                            </td>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="name" >
	                                </div>
	                            </td>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="phoneNum" >
	                                </div>
	                            </td>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="address" >
	                                </div>
	                            </td>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="email">
	                                </div>
	                            </td>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="etc">
	                                </div>
	                            </td>
	                            <td>
	                                <div class="buttons pull-left">
	                                    <span class="urlbtn del"></span>
	                                    <span class="urlbtn add iblock"></span>
	                                </div>
	                                
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
                <div class="confirm-buttons text-center"  style="margin-top:30px;">
                    <a class="btn w100" style="cursor:pointer;" id="confirmBtn" onclick="javascript:insertCharge();">확인</a>
                    <a class="btn w100 btn-white" href="#">취소</a>
                </div>
        </div>

    </div> --%>
		
<section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">HOME</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">거래처 등록</a></li>
                </ul>
            </div>

            <div class="up-dl clearfix header-search">
                 <table>
                    <tbody>
                        <tr>
                            <td style="width: 145px;">
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">선택바
                                    <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">HTML</a></li>
                                        <li><a href="#">CSS</a></li>
                                        <li><a href="#">JavaScript</a></li>
                                    </ul>
                                </div>
                            </td>
                            <td style="width: 190px;">
                                <form action="">
                                    <input type="text" class="search-box">
                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
                                    <div class="search-result">
                                        <!-- No result -->
                                        <div class="no-result d-table">
                                            <div class="d-tbc">
                                                <i class="fa fa-exclamation-triangle fa-3x" aria-hidden="true"></i>
                                                <span>No results have been found.</span>
                                            </div>
                                        </div>

                                        <!-- Result type1 -->
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Campaigns</p>
                                            <a href="" class="result-sub"><span>네오퓨쳐</span></a>
                                            <p class="camp-type"><span>Campaign type:</span> <span>Powerlink</span></p>
                                            <p class="camp-id"><span>Campaign ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                        <!-- Result type2 -->
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Ad groups</p>
                                            <a href="" class="result-sub"><span>네오퓨쳐</span>_#0007</a>
                                            <p class="camp-type"><span>Campaign type:</span> <span>Powerlink</span></p>
                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
                                            <p class="adgroup-id"><span>Ad group ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                        <!-- Result type3 -->
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Ads - Powerlink</p>
                                            <div class="result-box">
                                                <p class="title">네오퓨쳐</p>
                                                <p class="content">일반 홈페이지에서 신개념 융복합 멀티미디어 홈피이지 제작, 견적상담 환영</p>

                                                <ul>
                                                    <li>View URL: http://www.neofss.com</li>
                                                    <li>Linked URL: http://www.neofss.com</li>
                                                </ul>
                                            </div>
                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
                                            <p class="adgroup"><span>Ad group:</span> <span class="red">네오퓨쳐</span>_#0003</p>
                                            <p class="adgroup-id"><span>Ad ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                        <!-- Result type4 -->
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Ads - PowerContents</p>
                                            <div class="result-box w-img">
                                                <div class="d-table">
                                                    <div class="d-tbc">
                                                        <img src="/img/ads-img.png" alt="">
                                                    </div>
                                                    <div class="d-tbc">
                                                        <p class="title">홈페이지제작 이러면 망한다!</p>
                                                        <p class="content">홈페이지제작! 대충 업체에 맡기면 된다는 생각으로 접근하면, 십중팔구는 실패하는 이유와 홈페이지 제작에 관한 최소한의 지식은 가지고 업체를 선정해야 돈과 시간을 낭비하는 일을 줄일수 있다.</p>
                                                    </div>
                                                </div>
                                                <ul>
                                                    <li>Content created: 2017-12-15</li>
                                                    <li>Brand Name: 네오퓨쳐</li>
                                                    <li>View URL: http://blog.naver.com/marujeen</li>
                                                    <li>Linked URL: http://blog.naver.com/marujeen</li>
                                                </ul>
                                            </div>
                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
                                            <p class="adgroup"><span>Ad group:</span> <span class="red">네오퓨쳐</span>_#0003</p>
                                            <p class="adgroup-id"><span>Ad ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                    </div>
                                </form>
                            </td>
                            <td>
                                <input type="button" id="btn-search" value="검색" class="btn-primary">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
<!-- 
            <div class="dispatch-btn-container">
               
                <div class="dispatch-btn">
                    <i class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div>
            </div> -->
        </section>

        <div class="dispatch-wrapper">

            <section class="dispatch-bottom-content active" style=" margin:auto;">
               <form id="insertForm" action="/baseinfo/insert-customer.do"  method="post" enctype="multipart/form-data">
                   <h3 style="font-size: 20px; text-align: center; margin-bottom:20px; font-weight: 700; font-family: NotoSansBold,sans-serif;">거래처 등록</h3>
                   <input type="hidden" name="personInChargeInfo" id="personInChargeInfo">
                   <input type="hidden" name="paymentPersonInChargeInfo" id="paymentPersonInChargeInfo">
                   <input type="hidden" name="driverInfo" id="driverInfo">
               <div class="form-con clearfix" style="max-width:1200px;">    
                   
                <div class="column" style="margin-bottom:5px">   
                <table>
                    <tbody>
                        <tr>
                            <td>회사명<span style="color:#F00;">&nbsp;*</span></td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="회사명" name="customerName" id="customerName">
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <table>
                    <tbody>
                        <tr>
                            <td>대표자명<span style="color:#F00;">&nbsp;*</span></td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="대표자명" name="customerOwnerName" id="customerOwnerName">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>회사구분<span style="color:#F00;">&nbsp;*</span></td>
                            <td class="widthAuto" style="">
                                <!-- <input type="text" placeholder="약칭" name="carNickname" id="carNickname"> -->
                                <div class="select-con">
							        <select class="dropdown" name="customerKind" id="customerKind">
							        	<option value="">회사구분을 선택 하세요.</option>
							            <option value="00">법인</option>
							            <option value="01">개인</option>
							            <option value="02">외국인</option>
							            <option value="03">개인(주민번호)</option>
							        </select>
							        <span></span>
							    </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <table>
                    <tbody>
                        <tr>
                            <td>사업자번호<span style="color:#F00;">&nbsp;*</span></td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="사업자번호" name="businessLicenseNumber" id="businessLicenseNumber">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>법인등록번호</td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="법인등록번호" name="corporationRegistrationNumber" id="corporationRegistrationNumber">
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <table>
                    <tbody>
                        <tr>
                            <td>주소</td>
                            <td class="widthAuto" style="">
                            	<input type="text" placeholder="주소" name="address" id="address" onclick="jusoSearch('departure',this);">
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <table>
                    <tbody>
                        <tr>
                            <td>상세주소</td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="상세주소" name="addressDetail" id="addressDetail">
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <table>
                    <tbody>
                        <tr>
                            <td>업태</td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="업태" name="businessCondition" id="businessCondition">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>종목</td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="종목" name="businessKind" id="businessKind">
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <table>
                    <tbody>
                        <tr>
                            <td>대표전화</td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="대표전화" name="phone" id="phone">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>팩스</td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="팩스" name="faxNum" id="faxNum">
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <table>
                    <tbody>
                        <tr>
                            <td>결제방법</td>
                            <td class="widthAuto" style="">
                                <div class="select-con">
							        <select class="dropdown" name="paymentKind" id="paymentKind">
							        	<option value="">결제방법을 선택 하세요.</option>
							        	<c:forEach var="data" items="${paymentDivisionList}" varStatus="status">
							        		<option  value="${data.payment_division_cd}" <c:if test='${data.payment_division_cd eq "AT"}'>selected</c:if>>${data.payment_division}</option>
							        	</c:forEach>
							        </select>
							        <span></span>
							    </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>증빙구분</td>
                            <td class="widthAuto" style="">
                                <div class="select-con">
							        <select class="dropdown" name="billingKind" id="billingKind">
							        	<option value="">증빙구분을 선택 하세요.</option>
							        	<c:forEach var="data" items="${billingKindList}" varStatus="status">
							        		<option  value="${data.billing_division_id}" <c:if test='${data.billing_division_id eq "02"}'>selected</c:if>>${data.billing_division}</option>
							        	</c:forEach>
							        </select>
							        <span></span>
							    </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                     <table>
                    <tbody>
                        <tr>
                            <td>알림톡 또는 문자 사용여부</td>
                            <td class="widthAuto" style="">
                                <div class="select-con">
							         <select class="dropdown" name="alarmOrSmsYn" id="alarmOrSmsYn">
							        	<option value="">카카오톡/메세지 사용여부를 선택 하세요.</option>
							            <option value="A"  selected>알림톡 사용</option>
							            <option value="M">문자(SMS) 사용</option>
							        </select>
							        <span></span>
							    </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                </div>
                
                
                <div class="column" style="margin-bottom:5px">
                
                <table>
                    <tbody>
                        <tr>
                            <td>매출매입구분</td>
                            <td class="widthAuto" style="">
                                <!-- <input type="text" placeholder="약칭" name="carNickname" id="carNickname"> -->
                                <div class="select-con">
							        <select class="dropdown" name="salesPurchaseDivision" id="salesPurchaseDivision">
							        	<option value="">매출매입구분을 선택 하세요.</option>
							        	<c:forEach var="data" items="${salesPurchaseDivisionList}" varStatus="status">
							        		<option  value="${data.sales_purchase_division_cd}">${data.sales_purchase_division}</option>
							        	</c:forEach>
							        </select>
							        <span></span>
							    </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                
                <table>
                    <tbody>
                        <tr>
                            <td>세금계산서</td>
                            <td class="widthAuto" style="">
                                <!-- <input type="text" placeholder="약칭" name="carNickname" id="carNickname"> -->
                                <div class="select-con">
							        <select class="dropdown" name="billingDivision" id="billingDivision">
							        	<option value="">세금계산서구분을 선택 하세요.</option>
							        	<c:forEach var="data" items="${billingDivisionList}" varStatus="status">
							        		<c:if test='${data.billing_division_id eq "00" || data.billing_division_id eq "01"}'>
							        			<option  value="${data.billing_division_id}">${data.billing_division}</option>
							        		</c:if>
							        	</c:forEach>
							            <!-- <option value="00">미발행</option>
							            <option value="01">건별</option>
							            <option value="02">일괄</option>
							            <option value="03">카드</option>
							            <option value="04">현금영수증</option> -->
							        </select>
							        <span></span>
							    </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <table>
                    <tbody>
                        <tr>
                            <td>세금계산서이메일</td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="세금계산서이메일" name="billingEmail" id="billingEmail">
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <table>
                    <tbody>
                        <tr>
                            <td>은행명</td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="은행명" name="bankName" id="bankName">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>예금주</td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="예금주" name="depositor" id="depositor">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>계좌번호</td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="계좌번호" name="accountNumber" id="accountNumber">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>결제일</td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="결제일" name="paymentDt" id="paymentDt">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- <table>
                    <tbody>
                        <tr>
                            <td>회계담당자</td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="회계담당자" name="accounter" id="accounter">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>회계담당자연락처</td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="회계담당자연락처" name="phoneNum" id="phoneNum">
                            </td>
                        </tr>
                    </tbody>
                </table> -->
                               
                
                <table>
                    <tbody>
                        <tr>
                            <td>특이사항</td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="특이사항" name="significantData" id="significantData">
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <table>
                    <tbody>
                        <tr>
                            <td>첨부파일</td>
                            <td class="widthAuto" style="">
								<input style="width:400px;" type="file" id="upload"  name="bbsFile" multiple>
	                        </td>
                        </tr>
                    </tbody>
                </table>
                
                <table>
                    <tbody>
                        <tr>
                            <td>비고</td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="비고" name="etc" id="etc">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>알림톡 발송</td>
                            <td class="widthAuto" style="">
                                <div class="select-con">
							        <select class="dropdown" name="alarmTalkYn" id="alarmTalkYn">
							        	<option value="">발송여부를 선택 하세요.</option>
							            <option value="Y" selected>발송</option>
							            <option value="N">미발송</option>
							        </select>
							        <span></span>
							    </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>계좌정보 발송</td>
                            <td class="widthAuto" style="">
                                <div class="select-con">
							        <select class="dropdown" name="sendAccountInfoYn" id="sendAccountInfoYn">
							        	<option value="">발송여부를 선택 하세요.</option>
							            <option value="Y" selected>발송</option>
							            <option value="N">미발송</option>
							        </select>
							        <span></span>
							    </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>문자(SMS) 사용</td>
                            <td class="widthAuto" style="">
                                <div class="select-con">
							        <select class="dropdown" name="smsYn" id="smsYn">
							        	<option value="">사용여부를 선택 하세요.</option>
							            <option value="Y">사용</option>
							            <option value="N" selected>미사용</option>
							        </select>
							        <span></span>
							    </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <table>
                    <tbody>
                        <tr>
                            <td>블랙리스트</td>
                            <td class="widthAuto" style="">
                                <div class="select-con">
							        <select class="dropdown" name="blackListYn" id="blackListYn">
							        	<option value="">블랙리스트여부를 선택 하세요.</option>
							            <option value="Y">등록</option>
							            <option value="N" selected>미등록</option>
							        </select>
							        <span></span>
							    </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                </div>
                <table>
                           <tbody id="addLocation">
                               <tr>
                                   <td rowspan="1" id="rowspanCnt">담당자정보</td>
                                   <td class="widthAuto" style="">
                                   	<input type="text" placeholder="딤당자부서" name="">
                                   </td>
                                   <td class="widthAuto" style="">
                               		<input type="text" placeholder="담당자명" name="">
                                   </td>
                                   <td class="widthAuto" style="">
                                   	<input type="text" placeholder="연락처" name="">
                                   </td>
                                   <td class="widthAuto" style="">
                                   	<input type="text" placeholder="주소" name="">
                                   </td>
                                   <td class="widthAuto" style="">
                                   	<input type="text" placeholder="이메일" name="">
                                   </td>
                                   <td class="widthAuto" style="">
                                   	<input type="text" placeholder="비고" name="">
                                   </td>
                                   <td class="widthAuto" style="">
                                   	<input style="display:inline; float:right;" type="button" id="file_mod" onclick="javascript:addPersonInCharge();" value="추가" class="btn-primary">
                                   </td>
                               </tr>
                           </tbody>
                       </table>
                       
                            <table>
                           <tbody id="paymentAddLocation">
                               <tr>
                                   <td rowspan="1" id="paymentRowspanCnt">회계 담당자정보</td>
                                   <td class="widthAuto" style="">
                                   	<input type="text" placeholder="딤당자부서" name="">
                                   </td>
                                   <td class="widthAuto" style="">
                               		<input type="text" placeholder="담당자명" name="">
                                   </td>
                                   <td class="widthAuto" style="">
                                   	<input type="text" placeholder="연락처" name="">
                                   </td>
                                   <td class="widthAuto" style="">
                                   	<input type="text" placeholder="주소" name="">
                                   </td>
                                   <td class="widthAuto" style="">
                                   	<input type="text" placeholder="이메일" name="">
                                   </td>
                                   <td class="widthAuto" style="">
                                   	<input type="text" placeholder="비고" name="">
                                   </td>
                                   <td class="widthAuto" style="">
                                   	<input style="display:inline; float:right;" type="button" id="file_mod" onclick="javascript:addPaymentPersonInCharge();" value="추가" class="btn-primary">
                                   </td>
                               </tr>
                           </tbody>
                       </table>
                       
                       <table>
                           <tbody id="driverAddLocation">
                               <tr>
                                   <td rowspan="1" id="driverRowspanCnt">기사정보</td>
                                   <td class="widthAuto" style="">
                                   	<input type="text" placeholder="기사명" name="">
                                   </td>
                                   <td class="widthAuto" style="">
                                   	<input type="text" placeholder="연락처" name="">
                                   </td>
                                   <td class="widthAuto" style="">
                                   	<input type="text" placeholder="차종" name="">
                                   </td>
                                   <td class="widthAuto" style="">
                                   	<input type="text" placeholder="차량번호" name="">
                                   </td>
                                   <td class="widthAuto" style="">
                                   	<input style="display:inline; float:right;" type="button" id="file_mod" onclick="javascript:addDriver();" value="추가" class="btn-primary">
                                   </td>
                               </tr>
                           </tbody>
                       </table>
                
                </div>
                    <div class="confirmation">
                        <div class="confirm">
                            <input type="button" value="등록하기"  onclick="javascript:addCustomer();">
                        </div>
                        <div class="cancel">
	                        <input type="button" value="취소" onclick="javascript:history.go(-1); return false;">
	                    </div>
                    </div>
                </form>
            </section>
        </div>
