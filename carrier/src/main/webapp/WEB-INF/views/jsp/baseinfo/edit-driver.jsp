<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%

%>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script type="text/javascript">
$(document).ready(function(){


 	<c:if test ='${driverMap.driver_kind eq "03"}'>
 		$("#forTest").css("display","");
	</c:if>

 	<c:if test ='${driverMap.rate_discount eq "Y" }'>
		$("#textRateDiscountMinimum").css("display","");
		$("#textRateDiscountMinimum").css("display","inline");
	</c:if>

	
	
});

function updateDriver(){

	if($("#driverName").val() == ""){
		alert("이름이 입력되지 않았습니다.");
		return false;
	}	
	
	/* if($("#assignCompany").val() == ""){
		alert("소속이 입력되지 않았습니다.");
		return false;
	} */
	if($("#driverKind").val() == ""){
		alert("구분이 입력되지 않았습니다.");
		return false;
	}
	
	if($("#driverName").val() == ""){
		alert("이름이 입력되지 않았습니다.");
		return false;
	}
	
	if($("#phoneNum").val() == ""){
		alert("연락처가 입력되지 않았습니다.");
		return false;
	}else{
		if(!phoneChk($("#phoneNum"))){
	  		alert("유효하지 않은 전화번호 입니다.");
	  		$("#phoneNum").val("");
            $("#phoneNum").focus();
			return false;
		}
	}
	
	if($("#residentRegistrationNumber").val() == ""){
		alert("주민번호가 입력되지 않았습니다.");
		return false;
	}else{
		if(!residentChk($("#residentRegistrationNumber"))){
 			alert("유효하지 않은 주민번호 입니다.");
            $("#residentRegistrationNumber").val("");
            $("#residentRegistrationNumber").focus();
 			return false;
 		}
	}
	
	if($("#companyKind").val() == ""){
		alert("사업자 구분이 입력되지 않았습니다.");
		return false;
	}
	
	/* if($("#businessLicenseNumber").val() == ""){
		alert("사업자 번호가 입력되지 않았습니다.");
		return false;
	} */
	
	
	if($("#address").val() == ""){
		alert("사업장 주소가 입력되지 않았습니다.");
		return false;
	}
	if($("#addressDetail").val() == ""){
		alert("사업장 주소 상세가 입력되지 않았습니다.");
		return false;
	}
	if($("#carKind").val() == ""){
		alert("차종이 입력되지 않았습니다.");
		return false;
	}
	if($("#carAssignCompany").val() == ""){
		alert("차량 소속이 입력되지 않았습니다.");
		return false;
	}
	if($("#carNum").val() == ""){
		alert("차량번호가 입력되지 않았습니다.");
		return false;
	}
	if($("#carIdNum").val() == ""){
		alert("차대번호가 입력되지 않았습니다.");
		return false;
	}
	
	if($("#carAssignCompany").val() == ""){
		alert("차량소속이 입력되지 않았습니다.");
		return false;
	}
    
	
	if($("#deductionRate").val() == ""){
		alert("적용요율이 입력되지 않았습니다.");
		return false;
	}
	
	if($("#driverBalance").val() == ""){
		alert("지입료가 입력되지 않았습니다.");
		return false;
	}
	
	$("#driverBalance").val(getNumberOnly($("#driverBalance").val()));
	
		if(confirm("수정 하시겠습니까?")){
			$("#insertForm").attr("action","/baseinfo/update-driver.do");
			$("#insertForm").submit();	
		}
	
}


function getNumberOnly (str) {			//저장할때는 콤마 제거 후 저장한다.
    var len = str.length;
    var sReturn = "";

    for (var i=0; i<len; i++){
        if ( (str.charAt(i) >= "0") && (str.charAt(i) <= "9") ){
            sReturn += str.charAt(i);
        }
    }
    return sReturn;
}





function jusoSearch(where,obj){
	
	new daum.Postcode({
	    oncomplete: function(data) {
	        // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

            // 각 주소의 노출 규칙에 따라 주소를 조합한다.
            // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
            var fullAddr = ''; // 최종 주소 변수
            var extraAddr = ''; // 조합형 주소 변수

            // 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
            if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                fullAddr = data.roadAddress;

            } else { // 사용자가 지번 주소를 선택했을 경우(J)
                fullAddr = data.jibunAddress;
            }

            // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
            if(data.userSelectedType === 'R'){
                //법정동명이 있을 경우 추가한다.
                if(data.bname !== ''){
                    extraAddr += data.bname;
                }
                // 건물명이 있을 경우 추가한다.
                if(data.buildingName !== ''){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
                fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
            }

            // 우편번호와 주소 정보를 해당 필드에 넣는다.
            //document.getElementById('sample6_postcode').value = data.zonecode; //5자리 새우편번호 사용
         	
            //$(obj).parent().children().eq(0).val(fullAddr);
            //$(obj).parent().parent().children().eq(0).children().val(data.sido);

            //alert(JSON.stringify(data));
            
            $(obj).val(fullAddr);
                        
            // 커서를 상세주소 필드로 이동한다.
            //document.getElementById('sample6_address2').focus();
        }
	    
	}).open();	
	
}

function fileChange(){
	
	var file = document.getElementById("upload");
 	var result = "";
 	for(var i = 0; i < file.files.length; i++){
		var name = file.files[i].name;
		result += name + "\t";
	}
	
	//$("#file_info").val(result);
	$("#forFile").css("display","none");
	$("#upload").css("display","inline");
	
	

}

function driverfileChange(){
	
	var file = document.getElementById("driverupload");
 	var result = "";
 	for(var i = 0; i < file.files.length; i++){
		var name = file.files[i].name;
		result += name + "\t";
	}
	
	//$("#file_info").val(result);
	$("#driverforFile").css("display","none");
	$("#driverupload").css("display","inline");
	
	

}

function driverSealfileChange(){
	
	var file = document.getElementById("driverSealupload");
 	var result = "";
 	for(var i = 0; i < file.files.length; i++){
		var name = file.files[i].name;
		result += name + "\t";
	}
	
	//$("#file_info").val(result);
	$("#driverforSealFile").css("display","none");
	$("#driverSealupload").css("display","inline");
	
	

}




function fileModConfirm(cnt){
	
	if(confirm("현재"+cnt+" 개의 파일이 첨부되어 있습니다.\r\n파일 수정시 기존의 파일이 삭제 됩니다. \r\n정말 수정 하시겠습니까?")){
		$("#upload").trigger("click");
	}
	
}


function driverfileModConfirm(cnt){
	
	if(confirm("현재"+cnt+" 개의 파일이 첨부되어 있습니다.\r\n파일 수정시 기존의 파일이 삭제 됩니다. \r\n정말 수정 하시겠습니까?")){
		$("#driverupload").trigger("click");
	}
	
}


function driverSealfileModConfirm(cnt){
	
	if(confirm("현재"+cnt+" 개의 파일이 첨부되어 있습니다.\r\n파일 수정시 기존의 파일이 삭제 됩니다. \r\n정말 수정 하시겠습니까?")){
		$("#driverSealupload").trigger("click");
	}
	
}


function goShowTest(obj){

	
//alert( $(obj).val() );


		if($(obj).val() == "03"){
			
			$("#forTest").css("display","");
		}else{

			$("#forTest").css("display","none");
				
		}


	
}

function goShowRateDiscount(obj){
	
	if($(obj).val() == "Y"){
		
		$("#textRateDiscountMinimum").css("display","");
		$("#textRateDiscountMinimum").css("display","inline");
	}else{

		$("#textRateDiscountMinimum").css("display","none");
			
	}

	
}





</script>
		<!-- <section class="side-nav">
            <ul>
                <li class=""><a href="/baseinfo/employee.do">직원관리</a></li>
                <li class="active"><a href="/baseinfo/driver.do">기사관리</a></li>
                <li class=""><a href="/baseinfo/customer.do">거래처관리</a></li>
                <li class=""><a href="/baseinfo/company.do">회사정보관리</a></li>
            </ul>
        </section> -->
     <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">HOME</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">기사 정보 수정</a></li>
                </ul>
            </div>

            <div class="up-dl clearfix header-search">
                 <table>
                    <tbody>
                        <tr>
                            <td style="width: 145px;">
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">선택바
                                    <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">HTML</a></li>
                                        <li><a href="#">CSS</a></li>
                                        <li><a href="#">JavaScript</a></li>
                                    </ul>
                                </div>
                            </td>
                            <td style="width: 190px;">
                                <form action="">
                                    <input type="text" class="search-box">
                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
                                    <div class="search-result">
                                        <!-- No result -->
                                        <div class="no-result d-table">
                                            <div class="d-tbc">
                                                <i class="fa fa-exclamation-triangle fa-3x" aria-hidden="true"></i>
                                                <span>No results have been found.</span>
                                            </div>
                                        </div>

                                        <!-- Result type1 -->
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Campaigns</p>
                                            <a href="" class="result-sub"><span>네오퓨쳐</span></a>
                                            <p class="camp-type"><span>Campaign type:</span> <span>Powerlink</span></p>
                                            <p class="camp-id"><span>Campaign ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                        <!-- Result type2 -->
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Ad groups</p>
                                            <a href="" class="result-sub"><span>네오퓨쳐</span>_#0007</a>
                                            <p class="camp-type"><span>Campaign type:</span> <span>Powerlink</span></p>
                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
                                            <p class="adgroup-id"><span>Ad group ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                        <!-- Result type3 -->
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Ads - Powerlink</p>
                                            <div class="result-box">
                                                <p class="title">네오퓨쳐</p>
                                                <p class="content">일반 홈페이지에서 신개념 융복합 멀티미디어 홈피이지 제작, 견적상담 환영</p>

                                                <ul>
                                                    <li>View URL: http://www.neofss.com</li>
                                                    <li>Linked URL: http://www.neofss.com</li>
                                                </ul>
                                            </div>
                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
                                            <p class="adgroup"><span>Ad group:</span> <span class="red">네오퓨쳐</span>_#0003</p>
                                            <p class="adgroup-id"><span>Ad ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                        <!-- Result type4 -->
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Ads - PowerContents</p>
                                            <div class="result-box w-img">
                                                <div class="d-table">
                                                    <div class="d-tbc">
                                                        <img src="/img/ads-img.png" alt="">
                                                    </div>
                                                    <div class="d-tbc">
                                                        <p class="title">홈페이지제작 이러면 망한다!</p>
                                                        <p class="content">홈페이지제작! 대충 업체에 맡기면 된다는 생각으로 접근하면, 십중팔구는 실패하는 이유와 홈페이지 제작에 관한 최소한의 지식은 가지고 업체를 선정해야 돈과 시간을 낭비하는 일을 줄일수 있다.</p>
                                                    </div>
                                                </div>
                                                <ul>
                                                    <li>Content created: 2017-12-15</li>
                                                    <li>Brand Name: 네오퓨쳐</li>
                                                    <li>View URL: http://blog.naver.com/marujeen</li>
                                                    <li>Linked URL: http://blog.naver.com/marujeen</li>
                                                </ul>
                                            </div>
                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
                                            <p class="adgroup"><span>Ad group:</span> <span class="red">네오퓨쳐</span>_#0003</p>
                                            <p class="adgroup-id"><span>Ad ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                    </div>
                                </form>
                            </td>
                            <td>
                                <input type="button" id="btn-search" value="검색" class="btn-primary">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
<!-- 
            <div class="dispatch-btn-container">
               
                <div class="dispatch-btn">
                    <i class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div>
            </div> -->
        </section>

        <div class="dispatch-wrapper">

            <section class="dispatch-bottom-content active" style=" margin:auto;">
               <form id="insertForm" action="/environment/insert-driver.do" method="post" enctype="multipart/form-data">
                   <h3 style="font-size: 20px; text-align: center; margin-bottom:20px; font-weight: 700; font-family: NotoSansBold,sans-serif;">기사정보 수정</h3>
                   <input type="hidden" name="driverId" id="driverId" value="${driverMap.driver_id}">
               		<input type="hidden"name="driverStatus" id="driverStatus" value="${driverMap.driver_status}">
                   <div class="form-con clearfix" style="max-width:1200px;">
                	<div class="column-sub" style="margin-bottom:5px">
                
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>소유주<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="소유주" name="driverOwner" id="driverOwner" value="${driverMap.driver_owner}">
	                            </td>
	                            
	                        </tr>
	                    </tbody>
	                </table>
	                <!-- <table>
	                    <tbody>
	                        <tr>
	                            <td>소속*</td>
	                            <td class="widthAuto" style=""> -->
	                                <!-- <input type="text" placeholder="소속" name="assignCompany" id="assignCompany"> -->
	                                <%-- <div class="select-con">
								        <select class="dropdown" name="assignCompany" id="assignCompany">
								        	<option value="">소속을 선택 하세요.</option>
								        	<c:forEach var="data" items="${companyList}" varStatus="status">
												<option value="${data.company_id}">${data.company_name}</option>
											</c:forEach>
											<option value="etc">소속없음</option>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table> --%>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>구분<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <!-- <input type="text" placeholder="소속" name="driverKind" id="driverKind"> -->
	                                <div class="select-con">
								        <select class="dropdown" name="driverKind" id="driverKind" onchange="javascript:goShowTest(this);">
							        	<option value="" <c:if test='${driverMap.driver_kind eq "" }'> selected="selected"</c:if>>구분을 선택 하세요.</option>
							            <option value="00" <c:if test='${driverMap.driver_kind eq "00" }'> selected="selected"</c:if>>직영</option>
							            <option value="03" <c:if test='${driverMap.driver_kind eq "03" }'> selected="selected"</c:if>>직영-수수료</option>
							            <option value="01" <c:if test='${driverMap.driver_kind eq "01" }'> selected="selected"</c:if>>지입</option>
							            <option value="02" <c:if test='${driverMap.driver_kind eq "02" }'> selected="selected"</c:if>>외부</option>
							        </select>
								        <span></span>
								    </div>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>이름<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="이름" name="driverName" id="driverName" value="${driverMap.driver_name}">
	                            </td>
	                            
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>연락처<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="연락처" name="phoneNum" id="phoneNum" value="${driverMap.phone_num}">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>주민번호<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="주민번호" name="residentRegistrationNumber" id="residentRegistrationNumber" value="${driverMap.resident_registration_number}">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>사업자구분<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <!-- <input type="text" placeholder="사업자구분" name="companyKind" id="companyKind"> -->
	                                <div class="select-con">
								        <select class="dropdown" name="companyKind" id="companyKind" style="width:100%;" >
								        	<option value="" <c:if test='${driverMap.company_kind eq "" }'> selected="selected"</c:if>>사업자 구분 선택</option>
								            <option value="00" <c:if test='${driverMap.company_kind eq "00" }'> selected="selected"</c:if>>법인</option>
								            <option value="01" <c:if test='${driverMap.company_kind eq "01" }'> selected="selected"</c:if>>개인</option>
								            <option value="02" <c:if test='${driverMap.company_kind eq "02" }'> selected="selected"</c:if>>기타</option>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <!-- <td>사업자번호<span style="color:#F00;">&nbsp;*</span></td> -->
	                            <td>사업자번호</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="사업자번호" name="businessLicenseNumber" id="businessLicenseNumber" value="${driverMap.business_license_number}">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>사업장주소<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="사업장주소" name="address" id="address" value="${driverMap.address}"  onclick="jusoSearch('departure',this);">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>사업장주소상세<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="사업장주소" name="addressDetail" id="addressDetail"  value="${driverMap.address_detail}">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>차종<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="차종" name="carKind" id="carKind" value="${driverMap.car_kind}">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table style="display:none;">
	                    <tbody>
	                        <tr>
	                            <td>약칭</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="약칭" name="carNickname" id="carNickname" value="${driverMap.car_nickname}">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>차량소속<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <!-- <input type="text" placeholder="약칭" name="carNickname" id="carNickname"> -->
	                                <div class="select-con">
								        <select class="dropdown" name="carAssignCompany" id="carAssignCompany">
							        	<option value="" <c:if test='${driverMap.car_assign_company eq "" }'> selected="selected"</c:if>>차량소속을 선택 하세요.</option>
							            <option value="S" <c:if test='${driverMap.car_assign_company eq "S" }'> selected="selected"</c:if>>셀프</option>
							            <option value="C" <c:if test='${driverMap.car_assign_company eq "C" }'> selected="selected"</c:if>>캐리어</option>
							        	</select>
								        <span></span>
								    </div>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>차량번호<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="차량번호" name="carNum" id="carNum" value="${driverMap.car_num}">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                
	                 <table>
	                    <tbody>
	                        <tr>
	                            <td>차대번호<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="차대번호" name="carIdNum" id="carIdNum" value="${driverMap.car_id_num}">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                
	                </div>
	                
	                <div class="column-sub">
	                
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>자격증번호<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="자격증번호" name="driverLicense" id="driverLicense" value="${driverMap.driver_license}">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>상호명</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="상호명" name="businessName" id="businessName" value="${driverMap.business_name}">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>대표자명</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="대표자명" name="ownerName" id="ownerName" value="${driverMap.owner_name}">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>업태</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="업태" name="businessCondition" id="businessCondition" value="${driverMap.business_condition}">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>종목</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="종목" name="businessKind" id="businessKind" value="${driverMap.business_kind}">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                
	                <table>
		                <tbody>
			                <tr>
				                <td>보세차량</td>
				                <td class="widthAuto" style="">
					                <input type="radio" id="bondedCarN" name="bondedCar" value="N" <c:if test="${driverMap.bonded_car eq 'N'}">checked="checked"</c:if>>
									<label for="bondedCarN">아니오</label>
									<input type="radio" id="bondedCarY" name="bondedCar" value="Y" <c:if test="${driverMap.bonded_car eq 'Y'}">checked="checked"</c:if>>
									<label for="bondedCarY">예</label>
				                </td>
			                </tr>
		                </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>입사일</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="입사일(YYYY-MM-DD)" name="joinDt" id="joinDt" value="${driverMap.join_dt}">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>퇴사일</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="퇴사일(YYYY-MM-DD)" name="resignDt" id="resignDt" value="${driverMap.resign_dt}">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>신고상태</td>
	                            <td class="widthAuto" style="">
	                                <input type="radio" id="registerStatusI" name="registerStatus" value="I" <c:if test="${driverMap.register_status eq 'I'}">checked="checked"</c:if>>
									<label for="registerStatusI">입사신고</label>
									<input type="radio" id="registerStatusO" name="registerStatus" value="O" <c:if test="${driverMap.register_status eq 'O'}">checked="checked"</c:if>>
									<label for="registerStatusO">퇴사신고</label>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                            <tbody>
	                                <tr>
	                                    <td>첨부파일</td>
	                                    <td class="widthAuto" style="">
											<c:if test='${fn:length(driverMap.fileList) > 0 }'> 
	                                        	<input  style="display:none" type="file" id="upload"  name="bbsFile" multiple value=""  onchange="javascript:fileChange();"> 
	                                        	<%-- <input style="display:inline; width:82%;" id="file_info" type="text" placeholder="" readonly value="${fn:length(empMap.fileList)}개의 파일이 첨부 되어 있습니다."> --%>
	                                        	<div id="forFile"  style="display:inline;">
		                                        	<c:forEach var="fileData" items="${driverMap.fileList}" varStatus="status">
							                            <a target="_blank" href="/files/driver${fileData.file_path }">
							                            	<img style="width:30px; height:25px; margin-left:10px;" src="/files/driver${fileData.file_path }" alt="${fileData.file_nm}" title="${fileData.file_nm}" />
														</a>
													</c:forEach>
	                                        		<input style="display:inline; float:right;" type="button" id="file_mod" onclick="javascript:fileModConfirm('${fn:length(driverMap.fileList)}');" value="수정" class="btn-primary">
	                                        	</div>
	                                        </c:if>
	                                        <c:if test='${fn:length(driverMap.fileList) == 0 }'> 
	                                        	<input  style="display:inline; width:200px;" type="file" id="upload"  name="bbsFile" multiple value="" >
	                                        </c:if> 
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                
	                
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>비고</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="비고" name="etc" id="etc" value="${driverMap.etc}">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                            <tbody>
	                                <tr>
	                                    <td>사진</td>
	                                    <td class="widthAuto" style="">
											<c:if test='${fn:length(driverMap.driverfileList) > 0 }'> 
	                                        	<input  style="display:none" type="file" id="driverupload"  name="driverFile" multiple value=""  onchange="javascript:driverfileChange();"> 
	                                        	<%-- <input style="display:inline; width:82%;" id="file_info" type="text" placeholder="" readonly value="${fn:length(empMap.fileList)}개의 파일이 첨부 되어 있습니다."> --%>
	                                        	<div id="driverforFile"  style="display:inline;">
		                                        	<c:forEach var="fileData" items="${driverMap.driverfileList}" varStatus="status">
							                            <a target="_blank" href="/files/driver${fileData.file_path }">
							                            	<img style="width:30px; height:25px; margin-left:10px;" src="/files/driver${fileData.file_path }" alt="${fileData.file_nm}" title="${fileData.file_nm}" />
														</a>
													</c:forEach>
	                                        		<input style="display:inline; float:right;" type="button" id="file_mod" onclick="javascript:driverfileModConfirm('${fn:length(driverMap.driverfileList)}');" value="수정" class="btn-primary">
	                                        	</div>
	                                        </c:if>
	                                        <c:if test='${fn:length(driverMap.driverfileList) == 0 }'> 
	                                        	<input  style="display:inline; width:200px;" type="file" id="upload"  name="driverFile" multiple value="" >
	                                        </c:if> 
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        
	                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>직인</td>
	                                    <td class="widthAuto" style="">
											<c:if test='${fn:length(driverMap.driverSealfileList) > 0 }'> 
	                                        	<input  style="display:none" type="file" id="driverSealupload"  name="driverSealFile" multiple value=""  onchange="javascript:driverSealfileChange();"> 
	                                        	<%-- <input style="display:inline; width:82%;" id="file_info" type="text" placeholder="" readonly value="${fn:length(empMap.fileList)}개의 파일이 첨부 되어 있습니다."> --%>
	                                        	<div id="driverforSealFile"  style="display:inline;">
		                                        	<c:forEach var="fileData" items="${driverMap.driverSealfileList}" varStatus="status">
							                            <a target="_blank" href="/files/driverSeal${fileData.file_path }">
							                            	<img style="width:30px; height:25px; margin-left:10px;" src="/files/driverSeal${fileData.file_path }" alt="${fileData.file_nm}" title="${fileData.file_nm}" />
														</a>
													</c:forEach>
	                                        		<input style="display:inline; float:right;" type="button" id="file_mod" onclick="javascript:driverSealfileModConfirm('${fn:length(driverMap.driverSealfileList)}');" value="수정" class="btn-primary">
	                                        	</div>
	                                        </c:if>
	                                        <c:if test='${fn:length(driverMap.driverSealfileList) == 0 }'> 
	                                        	<input  style="display:inline; width:200px;" type="file" id="driverSealupload"  name="driverSealFile" multiple value="" >
	                                        </c:if> 
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                
	                </div>
	                
	                <div class="column-sub">
	                
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>적용요율<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="적용요율(숫자만입력)" name="deductionRate" id="deductionRate" value="${driverMap.deduction_rate}">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>지입료<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="지입료" name="driverBalance" id="driverBalance" value="${driverMap.driver_balance}">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>예치금</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="예치금" name="driverDeposit" id="driverDeposit" value="${driverMap.driver_deposit}">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>은행명</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="은행명" name="bankName" id="bankName" value="${driverMap.bank_name}">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>예금주</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="예금주" name="depositor" id="depositor" value="${driverMap.depositor}">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>계좌번호</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="계좌번호" name="accountNumber" id="accountNumber" value="${driverMap.account_number}">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>지급유형<!-- <span style="color:#F00;">&nbsp;*</span> --></td>
	                            <td class="widthAuto" style="">
	                                <!-- <input type="text" placeholder="약칭" name="carNickname" id="carNickname"> -->
	                                <div class="select-con">
								        <select class="dropdown" name="provideKind" id="provideKind">
								        	<option value="" <c:if test='${driverMap.provide_kind eq "" }'> selected="selected"</c:if>>지급유형을 선택 하세요.</option>
								        	<option value="G" <c:if test='${driverMap.provide_kind eq "G" }'> selected="selected"</c:if>>15일</option>
								        	<option value="H" <c:if test='${driverMap.provide_kind eq "H" }'> selected="selected"</c:if>>25일</option>
								            <option value="A" <c:if test='${driverMap.provide_kind eq "A" }'> selected="selected"</c:if>>30일</option>
								            <option value="B" <c:if test='${driverMap.provide_kind eq "B" }'> selected="selected"</c:if>>35일</option>
								            <option value="C" <c:if test='${driverMap.provide_kind eq "C" }'> selected="selected"</c:if>>37일</option>
								            <option value="D" <c:if test='${driverMap.provide_kind eq "D" }'> selected="selected"</c:if>>40일</option>
								            <option value="E" <c:if test='${driverMap.provide_kind eq "E" }'> selected="selected"</c:if>>45일</option>
								            <option value="F" <c:if test='${driverMap.provide_kind eq "F" }'> selected="selected"</c:if>>60일</option>
							        	</select>
								        <span></span>
								    </div>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>소속</td>
	                            <td class="widthAuto" style="">
	                                <div class="select-con">
								        <select class="dropdown" name="assignCompany" id="assignCompany">
								        	<option value="" <c:if test='${driverMap.assign_company eq "" }'> selected="selected"</c:if>>소속을 선택 하세요.</option>
								            <c:forEach var="data" items="${companyList}" varStatus="status" >
								        		<option  value="${data.company_id}" <c:if test='${driverMap.assign_company eq data.company_id }'> selected="selected" </c:if> >${data.company_name}</option>
											</c:forEach>
							        	</select>
								        <span></span>
								    </div>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>업무구분<!-- <span style="color:#F00;">&nbsp;*</span> --></td>
	                            <td class="widthAuto" style="">
	                                <!-- <input type="text" placeholder="약칭" name="carNickname" id="carNickname"> -->
	                                <div class="select-con">
								        <select class="dropdown" name="workKind" id="workKind">
								        	<option value="" <c:if test='${driverMap.work_kind eq "" }'> selected="selected"</c:if>>업무구분을 선택 하세요.</option>
								        	<option value="I" <c:if test='${driverMap.work_kind eq "I" }'> selected="selected"</c:if>>내부업무</option>
								            <option value="O" <c:if test='${driverMap.work_kind eq "O" }'> selected="selected"</c:if>>외부업무</option>
								            <option value="P" <c:if test='${driverMap.work_kind eq "P" }'> selected="selected"</c:if>>직영</option>
							        	</select>
								        <span></span>
								    </div>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                 <table>
	                    <tbody>
	                        <tr>
	                            <td>기사 예치금 여부<!-- <span style="color:#F00;">&nbsp;*</span> --></td>
	                            <td class="widthAuto" style="">
	                                <!-- <input type="text" placeholder="약칭" name="carNickname" id="carNickname"> -->
	                                <div class="select-con">
								        <select class="dropdown" name="driverDepositYn" id="driverDepositYn">
								        	<option value="N" <c:if test='${driverMap.driver_deposit_yn eq "N" }'> selected="selected"</c:if>>미사용</option>
								            <option value="Y" <c:if test='${driverMap.driver_deposit_yn eq "Y" }'> selected="selected"</c:if>>사용</option>
							        	</select>
								        <span></span>
								    </div>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                 <table>
	                    <tbody>
	                        <tr>
	                            <td>기사 지입료 여부<!-- <span style="color:#F00;">&nbsp;*</span> --></td>
	                            <td class="widthAuto" style="">
	                                <!-- <input type="text" placeholder="약칭" name="carNickname" id="carNickname"> -->
	                                <div class="select-con">
								        <select class="dropdown" name="driverPaymentYn" id="driverPaymentYn">
								        	<option value="N" <c:if test='${driverMap.driver_payment_yn eq "N" }'> selected="selected"</c:if>>미사용</option>
								            <option value="Y" <c:if test='${driverMap.driver_payment_yn eq "Y" }'> selected="selected"</c:if>>사용</option>
							        	</select>
								        <span></span>
								    </div>
	                            </td>
	                        </tr>
	                    </tbody>
	                 </table>
	              <%-- <c:if test ='${driverMap.driver_kind eq "03"}'> --%>
		              <table id="forTest" style="display:none;">
		                    <tbody>
		                        <tr>
		                            <td>직영 기사 수수료(%)</td>
		                            <td class="widthAuto" style="">
		                                <!-- <input type="text" placeholder="약칭" name="carNickname" id="carNickname"> -->
		                                <div class="select-con">
													<input type="text" placeholder="수수료 입력" name="commissionRate" id="commissionRate" value="${driverMap.commission_rate}">
									        <span></span>
									    </div>
		                            </td>
		                        </tr>
		                    </tbody>
		                 </table>
	                 <%-- </c:if> --%>
	                 <table id="" style="">
		                    <tbody>
		                        <tr>
		                            <td>적용 요율 할인 (%)</td>
		                            <td class="widthAuto" style="">
		                                <!-- <input type="text" placeholder="약칭" name="carNickname" id="carNickname"> -->
		                                <div class="select-con">
											<select class="dropdown" name="rateDiscount" id="rateDiscount" onchange="javascript:goShowRateDiscount(this);">
									        	<option value="N" <c:if test='${driverMap.rate_discount eq "N" }'> selected="selected"</c:if>>미사용</option>
									            <option value="Y" <c:if test='${driverMap.rate_discount eq "Y" }'> selected="selected"</c:if>>사용</option>
								        	</select>
								         	
									        	<input id="textRateDiscountMinimum" type="text" style="width:60%; display: none; " placeholder="적용요율 최소값 입력" name="rateDiscountMinimum" id="rateDiscountMinimum" value="${driverMap.rate_discount_minimum}">
									 
									        <span></span>
									    </div>
		                            </td>
		                        </tr>
		                    </tbody>
		                 </table>
	                
	                
	                <!-- <table>
	                    <tbody>
	                        <tr>
	                            <td>차고지</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="차고지" name="carGarage" id="carGarage">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table> -->
	                </div>
	                
	                
	                
	                
	               
	                
                
                </div>
               
                    <div class="confirmation">
                        <div class="confirm">
                            <input type="button" value="수정" onclick="javascript:updateDriver();">
                        </div>
                        <div class="cancel">
                        <input type="button" value="취소" onclick="javascript:history.go(-1); return false;">
                    </div>
                    </div>
                </form>
                <!-- <div class="table-pagination text-center">
                    <ul class="pagination">
                        <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="curr-page"><a href="#">1</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
                    </ul>
                </div> -->
            </section>
        </div>
