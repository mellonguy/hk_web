<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>
<%

%>

<script type="text/javascript">
$(document).ready(function(){
	
	
});

function editEmp(empId){
	
	document.location.href = "/baseinfo/edit-employee.do?empId="+empId;
}

function addEmp(){
	document.location.href = "/baseinfo/add-employee.do";
}


function search(){
	
	document.location.href = "/baseinfo/employee.do?&searchWord="+encodeURI($("#searchWord").val());
	
}


</script>
		<!-- <section class="side-nav"> -->
            <!-- <ul>
                <li class="active"><a href="/baseinfo/employee.do">직원관리</a></li>
                <li class=""><a href="/baseinfo/driver.do">기사관리</a></li>
                <li class=""><a href="/baseinfo/customer.do">거래처관리</a></li>
                <li class=""><a href="/baseinfo/company.do">회사정보관리</a></li>
            </ul> -->
        <!-- </section> -->

        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">HOME</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">직원관리</a></li>
                </ul>
            </div>

            <div class="up-dl clearfix header-search">
                 <table>
                    <tbody>
                        <tr>
                        	<td>
                                <!-- <input type="button" id="btn-search" onclick="javascript:addEmp();" value="직원등록" class="btn-primary"> -->
                            </td>
                            <%-- <td style="width: 145px;">
                                <div class="select-con">
							        <select class="dropdown" name="searchType" id="searchType">
							        	<option value="" <c:if test='${paramMap.searchType eq "" }'> selected="selected"</c:if>>조회구분을 선택 하세요.</option>
							            <option value="00" <c:if test='${paramMap.searchType eq "00" }'> selected="selected"</c:if>>아이디</option>
							            <option value="01" <c:if test='${paramMap.searchType eq "01" }'> selected="selected"</c:if>>이름</option>
							        </select>
							        <span></span>
							    </div>
                            </td> --%>
                            <td style="width: 190px;">
                                <input type="text" id="searchWord" name="searchWord" value="${paramMap.searchWord}"   onkeypress="if(event.keyCode=='13') search();">
                            </td>
                            <td>
                                <input type="button" id="btn-search" value="검색" class="btn-primary" onclick="javascript:search();">
                            </td>
                            <td>
                                
                            </td>
                            
                        </tr>
                    </tbody>
                </table>
            </div>
<!-- 
            <div class="dispatch-btn-container">
               
                <div class="dispatch-btn">
                    <i class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div>
            </div> -->
        </section>



            <section class="bottom-table" style="width:1600px; margin-left:290px;">
                
                <table class="article-table" style="margin-top:15px">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <td style="text-align:center;">사원번호</td>
                            <td style="text-align:center;">성명</td>
                            <td style="text-align:center;">연락처</td>
                            <td style="text-align:center;">입사일</td>
                            <td style="text-align:center;">퇴사일</td>
                            <td style="text-align:center;">직급</td>
                            <td style="text-align:center;">이메일</td>
                            <td style="text-align:center;">첨부파일</td>
                            <td style="text-align:center;">관리</td>
                            
                        </tr>
                    </thead>
                    <tbody id="">
                    	<c:forEach var="data" items="${listData}" varStatus="status">
							<tr class="ui-state-default"> 
	                            <td style="text-align:center;">${data.emp_id}</td>
	                            <td style="text-align:center;">${data.emp_name}</td>
	                            <td style="text-align:center;">
	                            	<c:if test="${data.phone_personal != null && data.phone_personal != ''}">
	                            		<div style="display:block;">연락처 1 : ${data.phone_personal}</div>
	                            	</c:if>
	                            	<c:if test="${data.phone_work != null && data.phone_work != '' && data.phone_work != data.phone_personal}">
	                            		<div style="display:block; margin-top:5px;">연락처 2 : ${data.phone_work}</div>
	                            	</c:if>
	                            </td>
	                            <td style="text-align:center;">${data.join_dt}</td>
	                            <td style="text-align:center;">${data.resign_dt}</td>
	                            <td style="text-align:center;">${data.emp_position}</td>
	                            <td style="text-align:center;">${data.email_work} </td>
	                            <%-- <td>${data.salary} </td> --%>
	                            <td style="text-align:center;">
	                            	<c:forEach var="fileData" items="${data.fileList}" varStatus="status">
			                            <a target="_blank" href="/files/employee${fileData.file_path }">
			                            	<%-- ${fileData.file_nm} --%>
			                            	<img style="width:40px; height:40px;" src="/files/employee${fileData.file_path }" alt="${fileData.file_nm}" title="${fileData.file_nm}" />
										</a>
									</c:forEach>
									<!-- <a target="_blank" href="/files/employee/2019/01/10/eb1e3bfe-89db-4beb-878a-8f8c0c57af96.jpg">
									  <img style="width:30px; height:30px;" src="/files/employee/2019/01/10/eb1e3bfe-89db-4beb-878a-8f8c0c57af96.jpg" alt=""> 
									</a> -->
	                            </td>
	                            <td style="text-align:center;">
	                            	<c:if test="${userSessionMap.emp_role == 'A' || userSessionMap.emp_id == data.emp_id}">
	                            		<a style="cursor:pointer;" onclick="javascript:editEmp('${data.emp_id}')" class="btn-edit">수정</a>
	                            	</c:if>
	                            </td>
                        	</tr>
						</c:forEach>
                        
                    </tbody>
                </table>
                <div class="table-pagination text-center">
                    <ul class="pagination">
                        <!-- <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="curr-page"><a href="#">1</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li> -->
                        <html:paging uri="/baseinfo/employee.do" frontYn="N" />
                    </ul>
                </div>
                <div class="confirmation">
                    <div class="confirm">
                        <!-- <a href="/baseinfo/add-employee.do">직원등록</a> -->
                    </div>
                </div>
                
            </section>
     
