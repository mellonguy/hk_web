<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator"
   prefix="decorator"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld"%>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/js/bootstable.js"></script>
<script type="text/javascript">

   var oldListOrder = "";
   var allocationId = "";
var test = "";
   $(document).ready(function() {
            	   
            	   
            	     $("#empId").focus();
					
            	  	/*if($("#phoneNum").val() == ""){
            	  		alert("직원 연락처가 입력되지 않았습니다.");
						return false;
                	  	}
            	  	
            	  	if($("#phoneCertNum").val() == ""){
            	  		alert("인증번호가 입력되지 않았습니다.");
						return false;
                	  	}
            	  	
            	  	if($("#newPassword").val()== ""){
            	  		alert("새로운 비밀번호가 입력되지 않았습니다.");
						return false;
                	  	}
            	  	
            		if($("#confirmpw").val()== ""){
            	  		alert("비밀번호가 입력되지 않았습니다.");
						return false;
                	  	}

            	  	*/
            	  	
            	     
            	   
                  //forOpen

                  /* $('#dataTable tr:odd td').css("backgroundColor",
                        "#f9f9f9"); */

                  $("#indate").datepicker({
                     dateFormat : "yy-mm-dd",
                     onSelect : function(date) {
                        //$("#endDt").focus();
                        setTimeout(function() {

                           //alert(this.toString());
                           
                           document.location.href = "/account/summary.do?&dateSearch="+date;
                           //$("#endDt").focus();
                        }, 50);
                     }
                  });

                  /* Gosummary */

                  $('#summary')
                        .click(
                              function() {

                                 var indate = document
                                       .getElementById('indate');

                                 var indateVal = $("#indate").val();

                                 if (indateVal == "") {

                                    alert("dddddddddddd");
                                 } else {

                                    document.location.href = "/account/summary.do?&dateSearch="
                                          + indateVal;

                                 }

                              });

                


                    });

   function updateAllocaion(row) {
      var inputDt = "";
      var carrierType = "";
      var distanceType = "";
      var departureDt = "";
      var departureTime = "";
      var customerName = "";
      var carKind = "";
      var carIdNum = "";
      var carNum = "";
      var departure = "";
      var arrival = "";
      var driverName = "";
      var carCnt = "";
      var amount = "";
      var paymentKind = "";
      var allocId = $(row).attr("allocationId");
      if (confirm("수정 하시겠습니까?")) {
         $(row).find("td").each(function(index, element) {
            if (index == 1) {
               inputDt = $(this).html();
            }
            if (index == 2) {
               if ($(this).html() == "셀프") {
                  carrierType = "S";
               } else if ($(this).html() == "캐리어") {
                  carrierType = "C";
               } else {
                  alert("배차구분을 확인 하세요.");
                  return false;
               }
            }
            if (index == 3) {
               if ($(this).html() == "시내") {
                  distanceType = "0";
               } else if ($(this).html() == "시외") {
                  distanceType = "1";
               } else {
                  alert("거리구분을 확인 하세요.");
                  return false;
               }
            }
            if (index == 4) {
               departureDt = $(this).html();
            }
            if (index == 5) {
               departureTime = $(this).html();
            }
            if (index == 6) {
               customerName = $(this).html();
            }
            if (index == 7) {
               carKind = $(this).html();
            }
            if (index == 8) {
               carIdNum = $(this).html();
            }
            if (index == 9) {
               carNum = $(this).html();
            }
            if (index == 10) {
               departure = $(this).html();
            }
            if (index == 11) {
               arrival = $(this).html();
            }
            if (index == 12) {
               driverName = $(this).html();
            }
            if (index == 13) {
               carCnt = $(this).html();
            }
            if (index == 14) {
               amount = $(this).html();
            }
            if (index == 15) {
               paymentKind = $(this).html();
            }
         });

         if (carrierType == "" || distanceType == "") {
            return false;
         }

         $.ajax({
            type : 'post',
            url : "/allocation/update-allocation.do",
            dataType : 'json',
            data : {
               inputDt : inputDt,
               carrierType : carrierType,
               distanceType : distanceType,
               departureDt : departureDt,
               departureTime : departureTime,
               customerName : customerName,
               carKind : carKind,
               carIdNum : carIdNum,
               carNum : carNum,
               departure : departure,
               arrival : arrival,
               driverName : driverName,
               carCnt : carCnt,
               amount : amount,
               paymentKind : paymentKind,
               allocationId : allocId
            },
            success : function(data, textStatus, jqXHR) {
               var result = data.resultCode;
               if (result == "0000") {
                  alert("변경 되었습니다.");
                  //         document.location.href = "/allocation/combination.do";
               } else if (result == "0001") {
                  alert("변경 하는데 실패 하였습니다.");
               }
            },
            error : function(xhRequest, ErrorText, thrownError) {
            }
         });

      }

   }

   function getListId(obj) {
      $('html').scrollTop(0);
      selectList($(obj).parent().parent().parent().attr("allocationId"));

   }

   var driverArray = new Array();
   function selectList(id) {

      var searchWord = encodeURI('${paramMap.searchWord}');
      document.location.href = "/allocation/allocation-view.do?&searchDateType=${paramMap.searchDateType}&startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&searchType=${paramMap.searchType}&cPage=${paramMap.cPage}&allocationStatus=${paramMap.allocationStatus}&location=${paramMap.location}&allocationId="
            + id + "&searchWord=" + searchWord;

   }

   function batchStatus(status) {

      var id = "";
      var total = $('input:checkbox[name="forBatch"]:checked').length;

      if (total == 0) {
         alert("수정할 목록이 선택 되지 않았습니다.");
         return false;
      } else {
         $('input:checkbox[name="forBatch"]:checked').each(
               function(index, element) {
                  if (this.checked) {//checked 처리된 항목의 값
                     id += $(this).attr("allocationId");
                     if (index < total - 1) {
                        id += ",";
                     }
                  }
               });
         updateAllocationStatus(status, id);
      }
   }

   function updateAllocationStatus(status, id) {

      var msg = "";
      if (status == "cancel") {
         msg = "취소";
      } else if (status == "complete") {
         msg = "완료";
      }

      if (confirm(msg + "하시겠습니까?")) {
         $.ajax({
            type : 'post',
            url : "/allocation/updateAllocationStatus.do",
            dataType : 'json',
            data : {
               status : status,
               allocationId : id
            },
            success : function(data, textStatus, jqXHR) {
               var result = data.resultCode;
               var resultData = data.resultData;
               if (result == "0000") {
                  alert(msg + "되었습니다.");
                  document.location.href = "/allocation/combination.do";
               } else if (result == "0001") {
                  alert(msg + "하는데 실패 하였습니다.");
               }
            },
            error : function(xhRequest, ErrorText, thrownError) {
            }
         });
      }

   }

   function checkAll() {

      if ($('input:checkbox[id="checkAll"]').is(":checked")) {
         $('input:checkbox[name="forBatch"]').each(function(index, element) {
            $(this).prop("checked", "true");
         });
      } else {
         $('input:checkbox[name="forBatch"]').each(function(index, element) {
            $(this).prop("checked", "");
         });
      }
   }

   function move(location) {

      window.location.href = "/allocation/" + location + ".do";

   }

   
   

	   
   

   
   



   
   function sendSocketMessage() {

      $.ajax({
         type : 'post',
         url : "http://52.78.153.148:8080/allocation/sendSocketMessage.do",
         dataType : 'json',
         data : {
            allocationId : 'ALO3df74bd0105046bfbdce51ab0e64927c',
            driverId : "sourcream"
         },
         success : function(data, textStatus, jqXHR) {
            var result = data.resultCode;
            var resultData = data.resultData;
            if (result == "0000") {
               //alert("성공");
            } else if (result == "0001") {

            }
         },
         error : function(xhRequest, ErrorText, thrownError) {
         }
      });

   }

   function viewBycustomerId(customerId, dateSearch) {

      document.location.href = "/account/viewBycustomerId.do?&dateSearch="+ dateSearch + "&customerId=" + customerId;

   }


function employeeClick(register_id,dateSearch){

   document.location.href = "/account/employeeBacthDetail.do?&dateSearch="+ dateSearch + "&register_id=" + register_id;      
   
}

function f_datepicker(obj){


$(obj).datepicker()+"date";
 



}

var searchYN = "N";
var dataYN = "N";




function getKeyword(){


	var resultStr = '';
	
	
	


		if($("#keyword").val() !=  ""){
  	  	var keyword = $("#keyword").val();
  	  	//$('input[name=empId]').attr('value',empId);

  		resultStr += '<option value="01" selected=selected>'+" 선택해주세요"+'</option>'
	  	  		  	  	
				$.ajax({
					type : 'post',
					url  : "/baseinfo/search-keyword.do",
				    dataType : 'json',
				       data : {
				    	   keyword : keyword,
						
				 },
				   success : function(data, textStatus, jqXHR)
				
				   {	
					  var resultData = data.resultData
					  var result = data.resultCode;
	
					   if(result == "0000"){
						   
					  	var customerAddressInfoListKeyword = resultData[0].keyword;
					  	resultStr +='<option value="01">' +customerAddressInfoListKeyword+ '</option>'
					  	dataYN = "Y";
					  	 }else if(result == "0001"){
							 resultStr += '<option value= "01"> 입력하신 키워드가 존재하지 않습니다.</option> '
							}
					   searchYN = "Y";
					   $("#choosekeyword").append(resultStr);
						
				  } ,
				  error :function(xhRequest, ErrorText, thrownError){
	                  alert(' @@@@error@@@@ ');
	                  alert(ErrorText);
	
					  }
	
				});
				
		}else{
			
			alert("키워드를 입력해주세요");
		}


	if(searchYN != "N"){
		//alert (searchYN);
		resultStr = "";
	}else{

			}


		
}




$(document).on("keyup", "#phoneNum", function() {
	$(this).val( $(this).val().replace(/[^0-9]/g, "").replace(/(^02|^0505|^1[0-9]{3}|^0[0-9]{2})([0-9]+)?([0-9]{4})$/,"$1-$2-$3").replace("--", "-") );
 });





function newAddKeyword(){

	if(searchYN == "Y"){
		
	
	var keyword = $("#keyword").val();
	var address = $("#address").val();
	var name = $("#name").val();
	var phoneNum = $("#phoneNum").val();

	var AddreesArray = new Array();
	

	    if(keyword == ""){
	     alert("키워드를 입력해주세요.");
	     return false;
	    }
	    if(address == ""){
	        alert("주소를 입력해주세요.");
	        return false;
	    }
	    if(name == "" ){
	      alert("담당자 이름을 입력해주세요.");
	      return false;
	    }
	    
	    if(phoneNum == "" ){
		    alert("담당자 연락처를 입력해주세요.");
		      return false;
		 }
		 
		 if(dataYN == "Y"){
			alert("현재 존재하고 있는 키워드입니다.");
			}else{

				

	  
	    //AddreesArray.push(keyword,address,name,phoneNum);

				$.ajax({
					type : 'post',
					url  : "/baseinfo/addForAddressKeyword.do",
				    dataType : 'json',
				       data : {

				    	   keyword : keyword,
				    	   address : address,
				    	   name : name,
				    	   phoneNum : phoneNum,
						},

				   success : function(data, textStatus, jqXHR)
				    {
					   var resultCode = data.resultCode;
					   if(resultCode == "0000"){
						alert("키워드 추가 완료되었습니다.");
							document.location.href="/baseinfo/customer-keywordList.do";
							}else {
						
						}
				   },
				  error :function(xhRequest, ErrorText, thrownError){
	                  alert(' @@@@error@@@@ ');
	                  alert(ErrorText);
					  }

				});

			}
			
	}else{
	alert("키워드 검색 버튼을 눌러주세요");
	 return false;
	}
	
}

function cansle(){


	document.location.href="/baseinfo/customer.do";
}


	$(document).keypress(function(e) { if (e.keyCode == 13) e.preventDefault(); });


</script>



<!-- <style>

  table {
  
    margin-left: auto;
    margin-right: auto;
  }

</style> -->

<div class="modal-field">
   <div class="modal-box">
      <h3 class="text-center">기사리스트</h3>
      <div class="modal-table-container">
         <table class="article-table">
            <colgroup>

            </colgroup>
            <thead>
               <tr>
                  <!-- <td>소유주</td> -->
                  <td>기사명</td>
                  <td>연락처</td>
                  <td>차량번호</td>
                  <td>차종</td>
               </tr>
            </thead>
            <tbody id="driverSelectList">
               <c:forEach var="data" items="${driverList}" varStatus="status">

                  <tr class="ui-state-default" style="cursor: pointer;"
                     driverId="${data.driver_id}"
                     onclick="javascript:setDriver('${data.driver_id}','${data.driver_name}','${data.car_kind}');">
                     <%-- <td>${data.driver_owner}</td> --%>
                     <td style="">${data.driver_name}</td>
                     <td>${data.phone_num}</td>
                     <td>${data.car_num}</td>
                     <td>${data.car_kind}</td>
                  </tr>
               </c:forEach>
            </tbody>
         </table>
      </div>
      <div class="confirmation">
         <div class="confirm">
            <input type="button" value="취소" name="">
         </div>


      </div>

      <div class="pickupbox">
         <div class="confirm">
            <input type="button" value="취소" name="">
         </div>
      </div>
   </div>
</div>
<%-- <section class="side-nav">
            <ul>
                <li class="<c:if test="${complete == ''}">active</c:if>"><a href="/allocation/combination.do">신규배차입력</a></li>
                <li><a href="/allocation/self.do">셀프 배차  </a></li>
                <li><a href="/allocation/self.do?reserve=N">셀프 예약</a></li>
                <li><a href="/allocation/self.do?reserve=Y">셀프 배차 현황</a></li>
                <li><a href="/allocation/carrier.do">캐리어 배차</a></li>
                <li><a href="/allocation/carrier.do?reserve=N">캐리어 예약</a></li>
                <li><a href="/allocation/carrier.do?reserve=Y">캐리어 배차 현황</a></li>
                <li class="<c:if test="${complete == 'Y'}">active</c:if>"><a href="/allocation/combination.do?forOpen=N&complete=Y">완료 배차</a></li>
            </ul>
        </section> --%>

<section class="dispatch-top-content">
   <div class="breadcrumbs clearfix">
      <ul>
         <li><a href="">HOME</a></li>
         <li><img src="/img/bc-arrow.png" alt=""></li>
         <li><a href="">비밀번호 변경</a></li>

      </ul>
   </div>
   <!-- 
<div class="upload-btn right" >
<input type="button"  onclick="javascript:excelDownload('S');" value="엑셀 다운로드">
</div>                
   --> <div class="up-dl clearfix header-search">
      
   </div>

   <div class="dispatch-btn-container">
      <!-- <div class="dispatch-btn">
                    <i id="downArrow" class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div> -->

      <%-- <c:if test="${complete == null || complete != 'Y'}"> --%>
      <%-- <c:if test="${forOpen == null || forOpen != 'N'}">
                  <div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">신규배차입력</div>
               </c:if> --%>
      <%-- <c:if test="${forOpen != null && forOpen == 'N'}"> --%>
      <p>
   
      <div
         style="width: 300px; margin: auto; text-align: center; font-weight: bold; font-size: 30px;">
       거래처 키워드 추가</div>
      <%-- </c:if>
            </c:if> --%>

      <%-- <c:if test="${complete != null && complete == 'Y'}">
               <div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">완료배차</div>               
            </c:if> --%>
            

   </div>
  
   
</section>

  <section class="bottom-table" style="width:1600px;">
<div class="dispatch-wrapper"  align="middle"  style="width:40% ; margin-left:200px;" >
<form id="forPassword"  onSubmit="return false;">
    <div class="container">
        <div class="row text-center" style="width:50%;">
            <div class="col-3"></div>
            <div class="col-6" id="joinbox" style="padding : 20px; margin:20px; " width="200px" height="500px" >
                    <div style="padding:20px;">
                    <br>
                     <br>
                    <div class="id">
                        <h4 class="text-warning left text-left" style="margin-bottom: 2px;"><em>키워드</em></h4>
                        <div style="display: flex">
                        <input type="text" class="form-control inputheight"  style ="float: right; width:75%;" id="keyword" name="keyword" value="">
                        <input type="button" style ="float: right;width:25%;" class="btn btn-outline-warning btn-sm bt" id="idck" value="키워드 검색" onclick="javascript:getKeyword(this)">
                        </div>
                        <div>

                        <br>
                        <br>
                        <select id ='choosekeyword'  name="choosekeyword"  onchange ="document.getElementById('phoneNum').value = this.options[this.selectedIndex].text"  style ="width: 300px; padding: .8em .5em; border: 1px solid #999; font-family: inherit; background: no-repeat 95% 50%; border-radius: 0px; -webkit-appearance: none; -moz-appearance: none; appearance: none;">
                    	 <option value=""  ></option>
                     
                        </select>
                        </div>
                    </div>
                    <br>
                     <br>
                     <h4 class="text-warning left text-left" style="margin-bottom: 2px;"><em>주소 </em></h4>
                    <div class="pw" style="display: flex">
                              <input type="text" class="form-control inputheight"  id="address" name="address" placeholder="주소를 입력해주세요">
                         
                    </div>
                      
                    <br>
                     <br>
                      <h4 class="text-warning left text-left" style="margin-bottom: 2px;"><em>담당자 이름</em></h4>
                      <div class="pw"style="display: flex">
                          <input type="text" class="form-control inputheight"  id="name" name="name" placeholder="담당자 이름을 입력해주세요">
                    </div>
                        <br>
                        <br>
                        
                    <div class="tel">
                            <h4 class="text-warning left text-left" style="margin-bottom: 2px;"><em>담당자 연락처</em></h4>
                            <input type="text" class="form-control inputheight"  id="phoneNum" name="phoneNum" placeholder=" '-' 빼고 입력해주세요">
                            
                     </div>
                     <br>
                   <br>
                     </div>
                    <br>
                    <br>
                    <div>
                        <input type="submit" class="btn btn-primary" value="추가 " onclick ="javascript:newAddKeyword(this)">
                    </div>
            </div>
        </div>
    </div>
</form>
   
   
   
   
   </div>
</div>

   </section>

<!-- <iframe style="width: 980px; height:10000px; border: none;" frameBorder="0" id="happyboxFrame" scrolling="no" src="https://www.happyalliance-happybox.org/Bridge?v=param"></iframe>     -->
<script>
   if ("${userMap.control_grade}" == "01") {
      $('#dataTable').SetEditable({
         columnsEd : "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15",
         onEdit : function(row) {
            updateAllocaion(row)
         },
         onDelete : function() {
         },
         onBeforeDelete : function() {
         },
         onAdd : function() {
         }
      });

   }
</script>