<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%

%>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script type="text/javascript">
$(document).ready(function(){

});

function updateCustomer(){

    if($("#customerName").val() ==""){
		alert("회사명이 입력되지 않았습니다.");
		return false;
	}
    if($("#customerOwnerName").val() ==""){
		alert("대표자명이 입력되지 않았습니다.");
		return false;
	}
    if($("#customerKind").val() ==""){
		alert("회사구분이 입력되지 않았습니다.");
		return false;
	}
    
    if($("#businessLicenseNumber").val() ==""){
		alert("사업자번호가 입력되지 않았습니다.");
		return false;
	}
	
    
    
    insertCharge();
    
    
    
	if(confirm("수정 하시겠습니까?")){
		$("#insertForm").attr("action","/baseinfo/update-customer.do");
		$("#insertForm").submit();	
	}
	
}




function jusoSearch(where,obj){
	
	new daum.Postcode({
	    oncomplete: function(data) {
	        // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

            // 각 주소의 노출 규칙에 따라 주소를 조합한다.
            // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
            var fullAddr = ''; // 최종 주소 변수
            var extraAddr = ''; // 조합형 주소 변수

            // 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
            if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                fullAddr = data.roadAddress;

            } else { // 사용자가 지번 주소를 선택했을 경우(J)
                fullAddr = data.jibunAddress;
            }

            // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
            if(data.userSelectedType === 'R'){
                //법정동명이 있을 경우 추가한다.
                if(data.bname !== ''){
                    extraAddr += data.bname;
                }
                // 건물명이 있을 경우 추가한다.
                if(data.buildingName !== ''){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
                fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
            }

            // 우편번호와 주소 정보를 해당 필드에 넣는다.
            //document.getElementById('sample6_postcode').value = data.zonecode; //5자리 새우편번호 사용
         	
            //$(obj).parent().children().eq(0).val(fullAddr);
            //$(obj).parent().parent().children().eq(0).children().val(data.sido);

            //alert(JSON.stringify(data));
            
            $(obj).val(fullAddr);
                        
            // 커서를 상세주소 필드로 이동한다.
            //document.getElementById('sample6_address2').focus();
        }
	    
	}).open();	
	
}

function insertCharge(){
	
	var chargeList= new Array();
	var size = $("#addLocation").find("tr").length;
	var result = "";
	 $("#addLocation").find("tr").each(function(index,element){
		 if($(this).children().eq(1).children().val() != "" || $(this).children().eq(2).children().val() != "" || $(this).children().eq(3).children().val() != "" || $(this).children().eq(4).children().val() != ""){
			 var chargeInfo= new Object();
			 chargeInfo.department = $(this).children().eq(1).children().val();
			 chargeInfo.name = $(this).children().eq(2).children().val();
			 chargeInfo.phoneNum = $(this).children().eq(3).children().val();
			 chargeInfo.address = $(this).children().eq(4).children().val();
			 chargeInfo.email = $(this).children().eq(5).children().val();
			 chargeInfo.etc = $(this).children().eq(6).children().val();
			 chargeList.push(chargeInfo);	 
		 }
      }); 
	
	 if(chargeList.length >= 1){
		 $("#personInChargeInfo").val(JSON.stringify({chargeList : chargeList}));
	 }
	
}


function addPersonInCharge(){
	
	var result = "";
	result += '<tr><td style="display:none;"></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="딤당자부서" name=""></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="담당자명" name=""></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="연락처" name=""></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="주소" name=""></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="이메일" name=""></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="비고" name=""></td>';
	result += '<td class="widthAuto" style=""><input style="display:inline; float:right; border:2px solid #eee;" type="button" onclick="javascript:deletePersonInCharge(this);" value="삭제" class="btn-normal"></td></tr>';
	$("#rowspanCnt").attr("rowspan",Number($("#rowspanCnt").attr("rowspan"))+1);
	$("#addLocation").append(result);

}


function deletePersonInCharge(obj){
	
	$("#rowspanCnt").attr("rowspan",Number($("#rowspanCnt").attr("rowspan"))-1);
	$(obj).parent().parent().remove();
	
}

function fileChange(){
	
	var file = document.getElementById("upload");
 	var result = "";
 	for(var i = 0; i < file.files.length; i++){
		var name = file.files[i].name;
		result += name + "\t";
	}
	
	//$("#file_info").val(result);
	$("#forFile").css("display","none");
	$("#upload").css("display","inline");
	
	

}

function fileModConfirm(cnt){
	
	if(confirm("현재"+cnt+" 개의 파일이 첨부되어 있습니다.\r\n파일 수정시 기존의 파일이 삭제 됩니다. \r\n정말 수정 하시겠습니까?")){
		$("#upload").trigger("click");
	}
	
}




</script>


	
<section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">HOME</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a>거래처 정보</a></li>
                </ul>
            </div>

            <!-- <div class="up-dl clearfix header-search">
                 <table>
                    <tbody>
                        <tr>
                            <td style="width: 145px;">
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">선택바
                                    <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">HTML</a></li>
                                        <li><a href="#">CSS</a></li>
                                        <li><a href="#">JavaScript</a></li>
                                    </ul>
                                </div>
                            </td>
                            <td style="width: 190px;">
                                <form action="">
                                    <input type="text" class="search-box">
                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
                                    <div class="search-result">
                                        No result
                                        <div class="no-result d-table">
                                            <div class="d-tbc">
                                                <i class="fa fa-exclamation-triangle fa-3x" aria-hidden="true"></i>
                                                <span>No results have been found.</span>
                                            </div>
                                        </div>

                                        Result type1
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Campaigns</p>
                                            <a href="" class="result-sub"><span>네오퓨쳐</span></a>
                                            <p class="camp-type"><span>Campaign type:</span> <span>Powerlink</span></p>
                                            <p class="camp-id"><span>Campaign ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                        Result type2
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Ad groups</p>
                                            <a href="" class="result-sub"><span>네오퓨쳐</span>_#0007</a>
                                            <p class="camp-type"><span>Campaign type:</span> <span>Powerlink</span></p>
                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
                                            <p class="adgroup-id"><span>Ad group ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                        Result type3
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Ads - Powerlink</p>
                                            <div class="result-box">
                                                <p class="title">네오퓨쳐</p>
                                                <p class="content">일반 홈페이지에서 신개념 융복합 멀티미디어 홈피이지 제작, 견적상담 환영</p>

                                                <ul>
                                                    <li>View URL: http://www.neofss.com</li>
                                                    <li>Linked URL: http://www.neofss.com</li>
                                                </ul>
                                            </div>
                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
                                            <p class="adgroup"><span>Ad group:</span> <span class="red">네오퓨쳐</span>_#0003</p>
                                            <p class="adgroup-id"><span>Ad ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                        Result type4
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Ads - PowerContents</p>
                                            <div class="result-box w-img">
                                                <div class="d-table">
                                                    <div class="d-tbc">
                                                        <img src="/img/ads-img.png" alt="">
                                                    </div>
                                                    <div class="d-tbc">
                                                        <p class="title">홈페이지제작 이러면 망한다!</p>
                                                        <p class="content">홈페이지제작! 대충 업체에 맡기면 된다는 생각으로 접근하면, 십중팔구는 실패하는 이유와 홈페이지 제작에 관한 최소한의 지식은 가지고 업체를 선정해야 돈과 시간을 낭비하는 일을 줄일수 있다.</p>
                                                    </div>
                                                </div>
                                                <ul>
                                                    <li>Content created: 2017-12-15</li>
                                                    <li>Brand Name: 네오퓨쳐</li>
                                                    <li>View URL: http://blog.naver.com/marujeen</li>
                                                    <li>Linked URL: http://blog.naver.com/marujeen</li>
                                                </ul>
                                            </div>
                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
                                            <p class="adgroup"><span>Ad group:</span> <span class="red">네오퓨쳐</span>_#0003</p>
                                            <p class="adgroup-id"><span>Ad ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                    </div>
                                </form>
                            </td>
                            <td>
                                <input type="button" id="btn-search" value="검색" class="btn-primary">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div> -->
<!-- 
            <div class="dispatch-btn-container">
               
                <div class="dispatch-btn">
                    <i class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div>
            </div> -->
        </section>

        <div class="dispatch-wrapper">

            <section class="dispatch-bottom-content active" style=" margin:auto;">
               <form id="insertForm" action="/baseinfo/insert-customer.do" method="post" enctype="multipart/form-data">
                   <h3 style="font-size: 20px; text-align: center; margin-bottom:20px; font-weight: 700; font-family: NotoSansBold,sans-serif;">거래처 정보</h3>
                   <input type="hidden" name="customerId" id="customerId" value="${customerMap.customer_id}">
                   <input type="hidden" placeholder="회사명" name="personInChargeInfo" id="personInChargeInfo">
                <div class="form-con clearfix" style="max-width:1200px;">    
                   
                <div class="column" style="margin-bottom:5px">   
                <table>
                    <tbody>
                        <tr>
                            <td>회사명<span style="color:#F00;">&nbsp;*</span></td>
                            <td class="widthAuto" style="">
                                ${customerMap.customer_name}
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <table>
                    <tbody>
                        <tr>
                            <td>대표자명<span style="color:#F00;">&nbsp;*</span></td>
                            <td class="widthAuto" style="">
                                ${customerMap.customer_owner_name}
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>회사구분<span style="color:#F00;">&nbsp;*</span></td>
                            <td class="widthAuto" style="">
                                <c:if test='${customerMap.customer_kind eq "" }'>회사구분을 선택 하세요.</c:if>
                            	<c:if test='${customerMap.customer_kind eq "00" }'>법인</c:if>
                            	<c:if test='${customerMap.customer_kind eq "01" }'>개인</c:if>
                            	<c:if test='${customerMap.customer_kind eq "02" }'>외국인</c:if>
                            	<c:if test='${customerMap.customer_kind eq "03" }'>개인(주민번호)</c:if>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <table>
                    <tbody>
                        <tr>
                            <td>사업자번호<span style="color:#F00;">&nbsp;*</span></td>
                            <td class="widthAuto" style="">
                                ${customerMap.business_license_number}
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>법인등록번호</td>
                            <td class="widthAuto" style="">
                                ${customerMap.corporation_registration_number}
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>주소</td>
                            <td class="widthAuto" style="">
                                ${customerMap.address}
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <table>
                    <tbody>
                        <tr>
                            <td>상세주소</td>
                            <td class="widthAuto" style="">
                                ${customerMap.address_detail}
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <table>
                    <tbody>
                        <tr>
                            <td>업태</td>
                            <td class="widthAuto" style="">
                                ${customerMap.business_condition}
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>종목</td>
                            <td class="widthAuto" style="">
                                ${customerMap.business_kind}
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                
                <table>
                    <tbody>
                        <tr>
                            <td>대표전화</td>
                            <td class="widthAuto" style="">
                                ${customerMap.phone}
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>팩스</td>
                            <td class="widthAuto" style="">
                                ${customerMap.fax_num}
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                
                <table>
                    <tbody>
                        <tr>
                            <td>결제방법<span style="color:#F00;">&nbsp;*</span></td>
                            <td class="widthAuto" style="">
                                <c:if test='${customerMap.payment_kind eq "" }'>-</c:if>
                            	<c:if test='${customerMap.payment_kind eq "AT" }'>계좌이체</c:if>
                            	<c:if test='${customerMap.payment_kind eq "CA" }'>현금</c:if>
                            	<c:if test='${customerMap.payment_kind eq "CR" }'>현금영수증</c:if>
                            	<c:if test='${customerMap.payment_kind eq "CD" }'>카드</c:if>
                            	<c:if test='${customerMap.payment_kind eq "DD" }'>현장수금</c:if>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>증빙구분<span style="color:#F00;">&nbsp;*</span></td>
                            <td class="widthAuto" style="">
                                <c:if test='${customerMap.billing_kind eq "" }'>-</c:if>
                            	<c:if test='${customerMap.billing_kind eq "00" }'>미발행</c:if>
                            	<c:if test='${customerMap.billing_kind eq "01" }'>발행</c:if>
                            	<c:if test='${customerMap.billing_kind eq "05" }'>신용카드</c:if>
                            	<c:if test='${customerMap.billing_kind eq "04" }'>계산서</c:if>
                            	<c:if test='${customerMap.billing_kind eq "02" }'>세금계산서</c:if>
                            	<c:if test='${customerMap.billing_kind eq "07" }'>현금영수증</c:if>
                            	<c:if test='${customerMap.billing_kind eq "08" }'>간이영수증</c:if>
                            </td>
                        </tr>
                    </tbody>
                </table>
                 <table>
                    <tbody>
                        <tr>
                            <td>인수증 자동발송</td>
                            <td class="widthAuto" style="">
                              <c:if test='${customerMap.auto_send_yn eq "Y" }'>발송</c:if>
                              <c:if test='${customerMap.auto_send_yn eq "N" }'>미발송</c:if>
                            </td>
                        </tr>
                    </tbody>
	                </table>
                
                   <table>
                    <tbody>
                        <tr>
                            <td> 거래처가 단가 입력 가능</td>
                            <td class="widthAuto" style="">
                              <c:if test='${customerMap.control_payment eq "Y" }'>활성화</c:if>
                              <c:if test='${customerMap.control_payment eq "N" }'>비활성화</c:if>
                            </td>
                        </tr>
                    </tbody>
	                </table>
                
                
                
                </div>
                
                <div class="column" style="margin-bottom:5px">
                
                <table>
                    <tbody>
                        <tr>
                            <td>매출매입구분</td>
                            <td class="widthAuto" style="">
                            	${customerMap.sales_purchase_division_name}
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                
                <table>
                    <tbody>
                        <tr>
                            <td>세금계산서</td>
                            <td class="widthAuto" style="">
                                ${customerMap.billing_division_name}
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <table>
                    <tbody>
                        <tr>
                            <td>세금계산서이메일</td>
                            <td class="widthAuto" style="">
                                ${customerMap.billing_email}
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <table>
                    <tbody>
                        <tr>
                            <td>은행명</td>
                            <td class="widthAuto" style="">
                                ${customerMap.bank_name}
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>예금주</td>
                            <td class="widthAuto" style="">
                                ${customerMap.depositor}
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>계좌번호</td>
                            <td class="widthAuto" style="">
                                ${customerMap.account_number}
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>결제일</td>
                            <td class="widthAuto" style="">
                                ${customerMap.payment_dt}
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>특이사항</td>
                            <td class="widthAuto" style="">
                                ${customerMap.significant_data}
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>첨부파일</td>
                            <td class="widthAuto" style="">
								<c:if test='${fn:length(customerMap.fileList) > 0 }'> 
                                     	<input  style="display:none" type="file" id="upload"  name="bbsFile" multiple value=""  onchange="javascript:fileChange();"> 
                                     	<div id="forFile"  style="display:inline;">
                                      	<c:forEach var="fileData" items="${customerMap.fileList}" varStatus="status">
				                            <a target="_blank" href="/files/customer${fileData.file_path }">
				                            	<img style="width:30px; height:25px; margin-left:10px;" src="/files/customer${fileData.file_path }" alt="${fileData.file_nm}" title="${fileData.file_nm}" />
											</a>
										</c:forEach>
                                     	</div>
                                     </c:if>
                                     <c:if test='${fn:length(customerMap.fileList) == 0 }'> 
                                     	첨부파일이 없습니다.
                                     </c:if> 
                                 </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>비고</td>
                            <td class="widthAuto" style="">
                                ${customerMap.etc}
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <table>
                    <tbody>
                        <tr>
                            <td>알림톡 발송<span style="color:#F00;">&nbsp;*</span></td>
                            <td class="widthAuto" style="">
                                <c:if test='${customerMap.alarm_talk_yn eq "" }'>-</c:if>
                            	<c:if test='${customerMap.alarm_talk_yn eq "Y" }'>발송</c:if>
                            	<c:if test='${customerMap.alarm_talk_yn eq "N" }'>미발송</c:if>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>계좌정보 발송<span style="color:#F00;">&nbsp;*</span></td>
                            <td class="widthAuto" style="">
                                <c:if test='${customerMap.send_account_info_yn eq "" }'>-</c:if>
                            	<c:if test='${customerMap.send_account_info_yn eq "Y" }'>발송</c:if>
                            	<c:if test='${customerMap.send_account_info_yn eq "N" }'>미발송</c:if>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>문자(SMS) 사용</td>
                            <td class="widthAuto" style="">
                                <c:if test='${customerMap.sms_yn eq "" }'>-</c:if>
                            	<c:if test='${customerMap.sms_yn eq "Y" }'>발송</c:if>
                            	<c:if test='${customerMap.sms_yn eq "N" }'>미발송</c:if>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <table>
                    <tbody>
                        <tr>
                            <td>블랙리스트</td>
                            <td class="widthAuto" style="">
                                <c:if test='${customerMap.black_list_yn eq "" }'>-</c:if>
                            	<c:if test='${customerMap.black_list_yn eq "Y" }'>등록</c:if>
                            	<c:if test='${customerMap.black_list_yn eq "N" }'>미등록</c:if>
                            </td>
                        </tr>
                    </tbody>
                </table>
                    <table>
                    <tbody>
                        <tr>
                            <td>알림톡 또는 문자 사용여부</td>
                            <td class="widthAuto" style="">
                                <c:if test='${customerMap.alarm_or_sms_yn eq "" }'>-</c:if>
                            	<c:if test='${customerMap.alarm_or_sms_yn eq "A" }'>알림톡 전송</c:if>
                            	<c:if test='${customerMap.alarm_or_sms_yn eq "M" }'>문자(SMS) 전송</c:if>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                
                
                </div>
                <table style="table-layout:fixed;">
                           <tbody id="addLocation">
                           		<c:if test='${fn:length(personInChargeList) > 0 }'>
		                           <c:forEach var="data" items="${personInChargeList}" varStatus="status">
		                               <tr>
		                                   <c:if test='${status.index == 0 }'>
		                                    	<td rowspan="${fn:length(personInChargeList)}" id="rowspanCnt">담당자정보</td>
		                                    </c:if>
		                                    <c:if test='${status.index > 0 }'>
		                                    	<td style="display:none;"></td>
		                                    </c:if>
		                                   <td class="widthAuto" style="">
		                                   	${data.department}
		                                   </td>
		                                   <td class="widthAuto" style="">
		                               		${data.name}
		                                   </td>
		                                   <td class="widthAuto" style="">
		                                   	${data.phone_num}
		                                   </td>
		                                   <td class="widthAuto" style="">
		                                   	${data.address}
		                                   </td>
		                                   <td class="widthAuto" style="">
		                                   	${data.email}
		                                   </td>
		                                   <td class="widthAuto" style="">
		                                   	${data.etc}
		                                   </td>
		                                    <td class="widthAuto" style="">
			                                    <c:if test ="${data.black_list_yn eq 'Y'}">
			                                    	블랙리스트
			                                   	</c:if>
			                                   	 <c:if test ="${data.black_list_yn eq 'N'}">
			                                    	일반
			                                   	</c:if>
		                                   </td>
		                               </tr>
	                               </c:forEach>
                               </c:if>
                               <c:if test='${fn:length(personInChargeList) == 0 }'>
	                               <tr>
	                                   <td rowspan="1" id="rowspanCnt">담당자정보</td>
	                                   <td class="widthAuto" style="">
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   </td>
	                               </tr>
                               </c:if>
                               
                           </tbody>
                       </table>
                       
                       
                           <table style="table-layout:fixed;">
                           <tbody id="addLocation">
                           		<c:if test='${fn:length(paymentPersonInChargeList) > 0 }'>
		                           <c:forEach var="data" items="${paymentPersonInChargeList}" varStatus="status">
		                               <tr>
		                                   <c:if test='${status.index == 0 }'>
		                                    	<td rowspan="${fn:length(paymentPersonInChargeList)}" id="rowspanCnt">회계 담당자정보</td>
		                                    </c:if>
		                                    <c:if test='${status.index > 0 }'>
		                                    	<td style="display:none;"></td>
		                                    </c:if>
		                                   <td class="widthAuto" style="">
		                                   	${data.payment_department}
		                                   </td>
		                                   <td class="widthAuto" style="">
		                               		${data.payment_name}
		                                   </td>
		                                   <td class="widthAuto" style="">
		                                   	${data.payment_phone_num}
		                                   </td>
		                                   <td class="widthAuto" style="">
		                                   	${data.address}
		                                   </td>
		                                   <td class="widthAuto" style="">
		                                   	${data.email}
		                                   </td>
		                                   <td class="widthAuto" style="">
		                                   	${data.etc}
		                                   </td>
		                               </tr>
	                               </c:forEach>
                               </c:if>
                               <c:if test='${fn:length(paymentPersonInChargeList) == 0 }'>
	                               <tr>
	                                   <td rowspan="1" id="rowspanCnt">회계 담당자정보</td>
	                                   <td class="widthAuto" style="">
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   </td>
	                               </tr>
                               </c:if>
                               
                           </tbody>
                       </table>
                       
                       
                       
                       <table style="table-layout:fixed;">
                           <tbody id="driverAddLocation">
                           	<c:if test='${fn:length(driverList) > 0 }'>
                           		<c:forEach var="data" items="${driverList}" varStatus="status">
                           			<tr>
	                     				<c:if test='${status.index == 0 }'>
	                                		<td rowspan="${fn:length(driverList)}" id="driverRowspanCnt">기사정보</td>
	                                	</c:if>
	                                	<c:if test='${status.index > 0 }'>
	                                		<td style="display:none;"></td>
	                                	</c:if>
	                                	<td class="widthAuto" style="">
	                                   		${data.driver_name}
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   		${data.phone_num}
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   		${data.car_kind}
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   		${data.car_num}
	                                   </td>
                                	</tr>
                           		</c:forEach>
                              </c:if>
                           		<c:if test='${fn:length(driverList) == 0 }'>
	                               <tr>
	                                   <td rowspan="1" id="driverRowspanCnt">기사정보</td>
	                                   <td class="widthAuto" style="">
	                                   		<!-- <input type="text" placeholder="기사명" name=""> -->
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   		<!-- <input type="text" placeholder="연락처" name=""> -->
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   		<!-- <input type="text" placeholder="차종" name=""> -->
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   		<!-- <input type="text" placeholder="차량번호" name=""> -->
	                                   </td>
	                               </tr>
                               </c:if>
                           </tbody>
                       </table>
                </div>
                
                    <div class="confirmation">
                        <div class="confirm">
                            <input type="button" value="확인"  onclick="javascript:history.go(-1); return false;">
                        </div>
                    </div>
                </form>
                <!-- <div class="table-pagination text-center">
                    <ul class="pagination">
                        <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="curr-page"><a href="#">1</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
                    </ul>
                </div> -->
            </section>
        </div>
