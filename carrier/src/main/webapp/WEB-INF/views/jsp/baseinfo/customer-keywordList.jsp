<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>

<%

%>
<script src="/js/vendor/clipboard.min.js"></script>
<script type="text/javascript">

var clipboard = new Clipboard('.clipboard'); 

$(document).ready(function(){
	
	$("#toggler").click(function(){
        $(".modal").fadeIn();
    });
    $(".modal .btn-white").click(function(){
        $(".modal").fadeOut();
    });

    $('.urllist').on('click','.urlbtn.add',function(){
        $(this).prev().css('display','inline-block');
        $(' <tr>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="buttons pull-left">\
                                    <span class="urlbtn del"></span>\
                                    <span class="urlbtn add iblock"></span>\
                                </div>\
                                \
                            </td>\
                        </tr>').appendTo('.urllist tbody');
        $(this).hide();
        $('.urllist tbody').find('tr:last-child .urlbtn.add').css('display','inline-block');
    });

    $('.urllist').on('click','.urlbtn.del',function(){
        $(this).parent().parent().parent().remove();
        console.log('a');
        $('.urllist tbody').find('tr:last-child .urlbtn.add').css('display','inline-block');
        var rowcount=0;
        $('.urllist tbody tr').each(function(){
            rowcount++;
        });
        if(rowcount==1){
            $('.urllist tbody').find('.urlbtn.del').css('display','none');
        }
    });
	
});

function editDriver(driverId){
	
	document.location.href = "/baseinfo/edit-driver.do?driverId="+driverId;
}


function insertIns(mode){
	
	var status = "등록";
	
	if(Number(mode) == 1){
		status = "수정";
	}
	
	if(confirm(status+" 하시겠습니까?")){
		$("#insertForm").attr("action","/driverInsurance/insert-driverInsurance.do");
		$("#insertForm").submit();	
	}
	
}

function fileUpload(fis){
	
	if(confirm("일괄 저장 하시겠습니까?")){
		$("#excelForm").attr("action","/baseinfo/driver-excel-insert.do");
		$("#excelForm").submit();	
	}
	
}


function getDriverInsurance(driverId,mode){
	
	$.ajax({ 
		type: 'post' ,
		url : "/driverInsurance/getDriverInsurance.do" ,
		dataType : 'json' ,
		data : {
			driverId : driverId
		},
		success : function(data, textStatus, jqXHR)
		{
			
			var result = data.resultData;
			$("#insuranceId").val(result.insurance_id);
			$("#deductDividedPayments").val(result.deduct_divided_payments);		
			$("#deductMaximum").val(result.deduct_maximum);
			$("#deductTotalPayments").val(result.deduct_total_payments);
			$("#deductionRate").val(result.deduction_rate);
			$("#expirationDt").val(result.expiration_dt);
			$("#insuranceFifthPaymentDt").val(result.insurance_fifth_payment_dt);
			$("#insuranceFifthPayments").val(result.insurance_fifth_payments);
			$("#insuranceFirstPaymentDt").val(result.insurance_first_payment_dt);
			$("#insuranceFirstPayments").val(result.insurance_first_payments);
			$("#insuranceFourthPaymentDt").val(result.insurance_fourth_payment_dt);
			$("#insuranceFourthPayments").val(result.insurance_fourth_payments);
			$("#insuranceSecondPaymentDt").val(result.insurance_second_payment_dt);
			$("#insuranceSecondPayments").val(result.insurance_second_payments);
			$("#insuranceSixthPaymentDt").val(result.insurance_sixth_payment_dt);
			$("#insuranceSixthPayments").val(result.insurance_sixth_payments);
			$("#insuranceThirdPaymentDt").val(result.insurance_third_payment_dt);
			$("#insuranceThirdPayments").val(result.insurance_third_payments);
			$("#luggageDividedPayments").val(result.luggage_divided_payments);
			$("#luggageMaximum").val(result.luggage_maximum);
			$("#luggageSelfPay").val(result.luggage_self_pay);
			$("#luggageSpecialContract").val(result.luggage_special_contract);
			$("#luggageTotalPayments").val(result.luggage_total_payments);
			$("#startDt").val(result.start_dt);
			
			if(Number(mode) == 0){
				$("#regStatus").html("등록");
				$("#regStatus").attr("onclick","javascript:insertIns(0);");
			}else if(Number(mode) == 1){
				$("#regStatus").html("수정");
				$("#regStatus").attr("onclick","javascript:insertIns(1);");
			} 
			
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
}

function editIns(driverId,mode){
	
	//0 : 작성 1: 수정
	$("#driverId").val(driverId);
	$("#mode").val(mode);
	
	if(Number(mode) == 1){
		getDriverInsurance(driverId,mode);
	}else{
		$("#insuranceId").val("");
		$("#deductDividedPayments").val("");		
		$("#deductMaximum").val("");
		$("#deductTotalPayments").val("");
		$("#deductionRate").val("");
		$("#expirationDt").val("");
		$("#insuranceFifthPaymentDt").val("");
		$("#insuranceFifthPayments").val("");
		$("#insuranceFirstPaymentDt").val("");
		$("#insuranceFirstPayments").val("");
		$("#insuranceFourthPaymentDt").val("");
		$("#insuranceFourthPayments").val("");
		$("#insuranceSecondPaymentDt").val("");
		$("#insuranceSecondPayments").val("");
		$("#insuranceSixthPaymentDt").val("");
		$("#insuranceSixthPayments").val("");
		$("#insuranceThirdPaymentDt").val("");
		$("#insuranceThirdPayments").val("");
		$("#luggageDividedPayments").val("");
		$("#luggageMaximum").val("");
		$("#luggageSelfPay").val("");
		$("#luggageSpecialContract").val("");
		$("#luggageTotalPayments").val("");
		$("#startDt").val("");
		$("#regStatus").html("등록");
		$("#regStatus").attr("onclick","javascript:insertIns(0);");
		
	}
	
	$(".modal").fadeIn();
	
}


function addDriver(){
	document.location.href = "/baseinfo/add-driver.do";
}

function excelDownload(){
	
	document.location.href = "/baseinfo/excel_download.do?mode=driver";
}

function addCompany(driverForpayment){

	document.location.href = "/baseinfo/add-driver.do?&driverForpayment="+driverForpayment;
																		
	
}

function search(){
	//searchGubun
	document.location.href = "/baseinfo/blacklist-customer.do?&searchWord="+encodeURI($("#searchWord").val());
	
}



function addDriverPayment(){


	if($("#driverName").val() == ""){
		alert("이름이 입력되지 않았습니다.");
		return false;
	}	
	
	
	if($("#driverOwner").val() == ""){
		alert("소유주가 입력 되지 않았습니다.");
		return false;
	}
	
	
	if($("#phoneNum").val() == ""){
		alert("연락처가 입력되지 않았습니다.");
		return false;
	}else{
		if(!phoneChk($("#phoneNum"))){
	  		alert("유효하지 않은 전화번호 입니다.");
	  		$("#phoneNum").val("");
            $("#phoneNum").focus();
			return false;
		}
	}


	if(confirm("해당 내용을 등록 하시겠습니까?")){
		$("#depositForm").attr("action","/baseinfo/insertPaymentDriver.do");
		$("#depositForm").submit();
	}
	

		

}


function viewCustomer(customerId){
	document.location.href = "/baseinfo/view-customer.do?customerId="+customerId;
}




function clipBoardCopy(driverId,obj){
	  
	  $.ajax({ 
			type: 'post' ,
			url : "/baseinfo/getDriverInfo.do" ,
			dataType : 'json' ,
			data : {
				driverId : driverId
			},
			success : function(data, textStatus, jqXHR)
			{
							
				var result = data.resultCode;
				var resultMsg = data.resultMsg;
				var resultData = data.resultData;
				if(result == "0000"){
					var t = document.createElement("textarea");
					t.setAttribute("id", "forClipboard");
					document.body.appendChild(t);		
					var clipBoardText = "";
					clipBoardText += ""+resultData.driver_name+"기사님"+" "+resultData.phone_num+"\r\n";
					clipBoardText += ""+resultData.car_num+"\r\n";

					var car_assign_company ="";
					if(resultData.car_assign_company == "S"){
						car_assign_company = "셀프";	
					}else{
						car_assign_company = "캐리어";
					}
					 					
					clipBoardText += ""+resultData.car_kind+" "+car_assign_company+"\r\n";
					var resident_registration_number = "";

					if(resultData.resident_registration_number.indexOf("-") < 0){
						resident_registration_number = resultData.resident_registration_number;
					}else{
						resident_registration_number = resultData.resident_registration_number.substring(0,resultData.resident_registration_number.indexOf("-")+2);						
					}
					clipBoardText += ""+resident_registration_number+"\r\n";
					t.value = clipBoardText;
					t.select();
					document.execCommand('copy');

					$(".clipboard").attr("data-clipboard-target","#forClipboard");
										
					document.body.removeChild(t);
					alert("기사 정보가 클립 보드에 복사 되었습니다.");
				}else{
					alert("기사 정보를 가져 오는데 실패 했습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
	  
}

function blackListCustomerChargeList(blackListYn){
	document.location.href ="/baseinfo/blacklist-customer.do?&cusotmerChargeBlackList="+blackListYn;
}

function addKeyword(){

	document.location.href ="/baseinfo/add-keyword.do";
}


function searchForKeyword(){

	document.location.href = "/baseinfo/customer-keywordList.do?&keyword="+$("#searchWord").val();
	
	
}

function deleteCustomerKeyword(addressInfoId){


		if(confirm("키워드 삭제 하시겠습니까?")){

			 $.ajax({ 
					type: 'post' ,
					url : "/baseinfo/deleteForAddressKeyword.do" ,
					dataType : 'json' ,
					data : {
						addressInfoId : addressInfoId
					},
					success : function(data, textStatus, jqXHR)
					{
									
						var result = data.resultCode;
						var resultMsg = data.resultMsg;
						var resultData = data.resultData;
						if(result == "0000"){
							alert("삭제 완료했습니다.")
							window.location.reload();
						}else{
							alert("삭제하는데 실패 했습니다.");
						}
					} ,
					error : function(xhRequest, ErrorText, thrownError) {
					}
				});

			
			



		}else{
			return false;



			}
		

		
		
	

	}




</script>



     <section class="dispatch-top-content">
   
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">HOME</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">블랙리스트 관리</a></li>
                </ul>
            </div>
			
    	<div class="dispatch-btn-container">
				<div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">거래처 주소 관리</div>
         </div>
            <div class="up-dl clearfix header-search">
                 <table>
                    <tbody>
                        <tr>
                       		 <td>
                             <!--   <input type="button" id="btn-search" value="블랙리스트 담당자"  onclick="javascript:blackListCustomerChargeList('C')" class="btn-primary"> --> 
                            </td>
                        
               
                             	<td>키워드 검색 :</td>
                            <td style="width: 190px;">
                                <input type="text" id="searchWord" name="searchWord" value=""   onkeypress="if(event.keyCode=='13') searchForKeyword();">
                            </td>
                            <td>
                                <input type="button" id="btn-search" value="검색" class="btn-primary" onclick="javascript:searchForKeyword();">
                               <!--  <form id="excelForm" style="display:inline-block;" name="excelForm" method="post" enctype="multipart/form-data">
                                	<input type="file" style="display:inline-block; width:300px;" id="upload" name="bbsFile" value="일괄입력" class="btn-primary" onchange="javascript:fileUpload(this);">
                                </form> -->
                            </td> 
                              <td>
                                <input type="button" id="btn-search" value="키워드 추가" class="btn-primary" onclick="javascript:addKeyword();">
                         	  </td>
                        <!--     <td>
                            <div class="upload-btn">
			                    <input type="button" onclick="javascript:excelDownload();" value="엑셀 다운로드">
			                </div>
                            </td> -->
                            
                            
                            
                            
                        </tr>
                    </tbody>
                </table>
                
            </div>
<!-- 
            <div class="dispatch-btn-container">
               
                <div class="dispatch-btn">
                    <i class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div>
            </div> -->
        </section>

			
			<a href="javascript:;" class="clipboard"  data-clipboard-target="#txt_MyLink"></a>

            <section class="bottom-table" style="width:1600px; margin-left:290px;">
           
                <table class="article-table" style="margin-top:15px; width:100%;" >
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <td style="text-align:center;">키워드</td>
                            <td style="text-align:center;">이름</td>
                            <td style="text-align:center;">주소</td>
                            <td style="text-align:center;">핸드폰번호</td>
                            <td style="text-align:center;">관리</td>
                        </tr>
                    </thead>
                    <tbody id="">
                    	<c:forEach var="data" items="${listData}" varStatus="status">
						<tr class="ui-state-default"> 
	                            <td style="text-align:center;">${data.keyword}</td>
	                            <td style="text-align:center;">${data.name}</td>
	                            <td style="text-align:center;">${data.address}</td>
	                            <td style="text-align:center;">${data.phone_num}</td>
	                            <td style="text-align:center;">
	                            	<!--<a style="cursor:pointer; width:50px;" onclick="" class="btn-edit">수정</a> -->
	                            	<a style="cursor:pointer; width:50px;" onclick="javascript:deleteCustomerKeyword('${data.address_info_id}');" class="btn-edit">삭제</a>
	                            </td>
	                       	</tr>
						</c:forEach>
                           
                    </tbody>
                </table>
         
            <!--     <div class="table-pagination text-center">
                    <ul class="pagination">
                    	<html:paging uri="/baseinfo/blacklist-customer.do" frontYn="N" />
                    </ul>
                </div>
                 -->
            </section>
        
         

