<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>
<%

%>
<script src="/js/vendor/clipboard.min.js"></script>
<script type="text/javascript">

var clipboard = new Clipboard('.clipboard'); 

$(document).ready(function(){
	
	$("#toggler").click(function(){
        $(".modal").fadeIn();
    });
    $(".modal .btn-white").click(function(){
        $(".modal").fadeOut();
    });

    $('.urllist').on('click','.urlbtn.add',function(){
        $(this).prev().css('display','inline-block');
        $(' <tr>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="buttons pull-left">\
                                    <span class="urlbtn del"></span>\
                                    <span class="urlbtn add iblock"></span>\
                                </div>\
                                \
                            </td>\
                        </tr>').appendTo('.urllist tbody');
        $(this).hide();
        $('.urllist tbody').find('tr:last-child .urlbtn.add').css('display','inline-block');
    });

    $('.urllist').on('click','.urlbtn.del',function(){
        $(this).parent().parent().parent().remove();
        console.log('a');
        $('.urllist tbody').find('tr:last-child .urlbtn.add').css('display','inline-block');
        var rowcount=0;
        $('.urllist tbody tr').each(function(){
            rowcount++;
        });
        if(rowcount==1){
            $('.urllist tbody').find('.urlbtn.del').css('display','none');
        }
    });
	
});

function editDriver(driverId){
	
	document.location.href = "/baseinfo/edit-driver.do?driverId="+driverId;
}


function insertIns(mode){
	
	var status = "등록";
	
	if(Number(mode) == 1){
		status = "수정";
	}
	
	if(confirm(status+" 하시겠습니까?")){
		$("#insertForm").attr("action","/driverInsurance/insert-driverInsurance.do");
		$("#insertForm").submit();	
	}
	
}

function fileUpload(fis){
	
	if(confirm("일괄 저장 하시겠습니까?")){
		$("#excelForm").attr("action","/baseinfo/driver-excel-insert.do");
		$("#excelForm").submit();	
	}
	
}


function getDriverInsurance(driverId,mode){
	
	$.ajax({ 
		type: 'post' ,
		url : "/driverInsurance/getDriverInsurance.do" ,
		dataType : 'json' ,
		data : {
			driverId : driverId
		},
		success : function(data, textStatus, jqXHR)
		{
			
			var result = data.resultData;
			$("#insuranceId").val(result.insurance_id);
			$("#deductDividedPayments").val(result.deduct_divided_payments);		
			$("#deductMaximum").val(result.deduct_maximum);
			$("#deductTotalPayments").val(result.deduct_total_payments);
			$("#deductionRate").val(result.deduction_rate);
			$("#expirationDt").val(result.expiration_dt);
			$("#insuranceFifthPaymentDt").val(result.insurance_fifth_payment_dt);
			$("#insuranceFifthPayments").val(result.insurance_fifth_payments);
			$("#insuranceFirstPaymentDt").val(result.insurance_first_payment_dt);
			$("#insuranceFirstPayments").val(result.insurance_first_payments);
			$("#insuranceFourthPaymentDt").val(result.insurance_fourth_payment_dt);
			$("#insuranceFourthPayments").val(result.insurance_fourth_payments);
			$("#insuranceSecondPaymentDt").val(result.insurance_second_payment_dt);
			$("#insuranceSecondPayments").val(result.insurance_second_payments);
			$("#insuranceSixthPaymentDt").val(result.insurance_sixth_payment_dt);
			$("#insuranceSixthPayments").val(result.insurance_sixth_payments);
			$("#insuranceThirdPaymentDt").val(result.insurance_third_payment_dt);
			$("#insuranceThirdPayments").val(result.insurance_third_payments);
			$("#luggageDividedPayments").val(result.luggage_divided_payments);
			$("#luggageMaximum").val(result.luggage_maximum);
			$("#luggageSelfPay").val(result.luggage_self_pay);
			$("#luggageSpecialContract").val(result.luggage_special_contract);
			$("#luggageTotalPayments").val(result.luggage_total_payments);
			$("#startDt").val(result.start_dt);
			
			if(Number(mode) == 0){
				$("#regStatus").html("등록");
				$("#regStatus").attr("onclick","javascript:insertIns(0);");
			}else if(Number(mode) == 1){
				$("#regStatus").html("수정");
				$("#regStatus").attr("onclick","javascript:insertIns(1);");
			} 
			
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
}

function editIns(driverId,mode){
	
	//0 : 작성 1: 수정
	$("#driverId").val(driverId);
	$("#mode").val(mode);
	
	if(Number(mode) == 1){
		getDriverInsurance(driverId,mode);
	}else{
		$("#insuranceId").val("");
		$("#deductDividedPayments").val("");		
		$("#deductMaximum").val("");
		$("#deductTotalPayments").val("");
		$("#deductionRate").val("");
		$("#expirationDt").val("");
		$("#insuranceFifthPaymentDt").val("");
		$("#insuranceFifthPayments").val("");
		$("#insuranceFirstPaymentDt").val("");
		$("#insuranceFirstPayments").val("");
		$("#insuranceFourthPaymentDt").val("");
		$("#insuranceFourthPayments").val("");
		$("#insuranceSecondPaymentDt").val("");
		$("#insuranceSecondPayments").val("");
		$("#insuranceSixthPaymentDt").val("");
		$("#insuranceSixthPayments").val("");
		$("#insuranceThirdPaymentDt").val("");
		$("#insuranceThirdPayments").val("");
		$("#luggageDividedPayments").val("");
		$("#luggageMaximum").val("");
		$("#luggageSelfPay").val("");
		$("#luggageSpecialContract").val("");
		$("#luggageTotalPayments").val("");
		$("#startDt").val("");
		$("#regStatus").html("등록");
		$("#regStatus").attr("onclick","javascript:insertIns(0);");
		
	}
	
	$(".modal").fadeIn();
	
}


function addDriver(){
	document.location.href = "/baseinfo/add-driver.do";
}

function excelDownload(){
	
	document.location.href = "/baseinfo/excel_download.do?mode=driver";
}


function search(){
	//searchGubun
	document.location.href = "/baseinfo/driver.do?searchType="+$("#searchType").val()+"&searchWord="+encodeURI($("#searchWord").val())+"&searchGubun="+$("#searchGubun").val();
	
}






function clipBoardCopy(driverId,obj){
	  
	  $.ajax({ 
			type: 'post' ,
			url : "/baseinfo/getDriverInfo.do" ,
			dataType : 'json' ,
			data : {
				driverId : driverId
			},
			success : function(data, textStatus, jqXHR)
			{
							
				var result = data.resultCode;
				var resultMsg = data.resultMsg;
				var resultData = data.resultData;
				if(result == "0000"){
					var t = document.createElement("textarea");
					t.setAttribute("id", "forClipboard");
					document.body.appendChild(t);		
					var clipBoardText = "";
					clipBoardText += ""+resultData.driver_name+"기사님"+" "+resultData.phone_num+"\r\n";
					clipBoardText += ""+resultData.car_num+"\r\n";

					var car_assign_company ="";
					if(resultData.car_assign_company == "S"){
						car_assign_company = "셀프";	
					}else{
						car_assign_company = "캐리어";
					}
					 					
					clipBoardText += ""+resultData.car_kind+" "+car_assign_company+"\r\n";
					var resident_registration_number = "";

					if(resultData.resident_registration_number.indexOf("-") < 0){
						resident_registration_number = resultData.resident_registration_number;
					}else{
						resident_registration_number = resultData.resident_registration_number.substring(0,resultData.resident_registration_number.indexOf("-")+2);						
					}
					clipBoardText += ""+resident_registration_number+"\r\n";
					t.value = clipBoardText;
					t.select();
					document.execCommand('copy');

					$(".clipboard").attr("data-clipboard-target","#forClipboard");
										
					document.body.removeChild(t);
					alert("기사 정보가 클립 보드에 복사 되었습니다.");
				}else{
					alert("기사 정보를 가져 오는데 실패 했습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
	  
}














</script>

<div class="modal">
        <div class="modal-con">
        	<form id="insertForm" action="/driverInsurance/insert-driverInsurance.do">
        		<input type="hidden" name="driverId" id="driverId">
        		<input type="hidden" name="mode" id="mode">
        		<input type="hidden" name="insuranceId" id="insuranceId">
	            <table class="urllist text-center iplist"> 
	                    <colgroup>
	                    </colgroup>
	                    <thead>
	                        <tr>
	                            <td colspan="23">보험</td>
	                            <td rowspan="3"></td>
	                        </tr>
	                        <tr>
	                            <td colspan="2">기초정보</td>
	                            <td colspan="4">공제 계약</td>
	                            <td colspan="5">적재물 계약</td>
	                            <td colspan="2">1차</td>
	                            <td colspan="2">2차</td>
	                            <td colspan="2">3차</td>
	                            <td colspan="2">4차</td>
	                            <td colspan="2">5차</td>
	                            <td colspan="2">6차</td>
	                            
	                        </tr>
	                        <tr>
	                            <td>시작일</td>
	                            <td>만기일</td>
	                            <td>적용요율</td>
	                            <td>대물 한도(원)</td>
	                            <td>분할구분</td>
	                            <td>총 납부금 (원)</td>
	                            <td>보상 한도 (원)</td>
	                            <td>자기부담금 (원)</td>
	                            <td>분할구분</td>
	                            <td>총 납부금 (원)</td>
	                            <td>특약사항</td>
	                            <td>납부일</td>
	                            <td>납부금 (원)</td>
	                            <td>납부일</td>
	                            <td>납부금 (원)</td>
	                            <td>납부일</td>
	                            <td>납부금 (원)</td>
	                            <td>납부일</td>
	                            <td>납부금 (원)</td>
	                            <td>납부일</td>
	                            <td>납부금 (원)</td>
	                            <td>납부일</td>
	                            <td>납부금 (원)</td>
	
	                        </tr>
	                        
	                    </thead>
	                    <tbody>
	                        <tr>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="startDt" id="startDt">
	                                </div>
	                            </td>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="expirationDt" id="expirationDt">
	                                </div>
	                            </td>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="deductionRate" id="deductionRate">
	                                </div>
	                            </td>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="deductMaximum" id="deductMaximum">
	                                </div>
	                            </td>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="deductDividedPayments" id="deductDividedPayments">
	                                </div>
	                            </td>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="deductTotalPayments" id="deductTotalPayments">
	                                </div>
	                            </td>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="luggageMaximum" id="luggageMaximum">
	                                </div>
	                            </td>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="luggageSelfPay" id="luggageSelfPay">
	                                </div>
	                            </td>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="luggageDividedPayments" id="luggageDividedPayments">
	                                </div>
	                            </td>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="luggageTotalPayments" id="luggageTotalPayments">
	                                </div>
	                            </td>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="luggageSpecialContract" id="luggageSpecialContract">
	                                </div>
	                            </td>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="insuranceFirstPaymentDt" id="insuranceFirstPaymentDt">
	                                </div>
	                            </td>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="insuranceFirstPayments" id="insuranceFirstPayments">
	                                </div>
	                            </td>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="insuranceSecondPaymentDt" id="insuranceSecondPaymentDt">
	                                </div>
	                            </td>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="insuranceSecondPayments" id="insuranceSecondPayments">
	                                </div>
	                            </td>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="insuranceThirdPaymentDt" id="insuranceThirdPaymentDt">
	                                </div>
	                            </td>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="insuranceThirdPayments" id="insuranceThirdPayments">
	                                </div>
	                            </td>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="insuranceFourthPaymentDt" id="insuranceFourthPaymentDt">
	                                </div>
	                            </td>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="insuranceFourthPayments" id="insuranceFourthPayments">
	                                </div>
	                            </td>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="insuranceFifthPaymentDt" id="insuranceFifthPaymentDt">
	                                </div>
	                            </td>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="insuranceFifthPayments" id="insuranceFifthPayments">
	                                </div>
	                            </td>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="insuranceSixthPaymentDt" id="insuranceSixthPaymentDt">
	                                </div>
	                            </td>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="insuranceSixthPayments" id="insuranceSixthPayments">
	                                </div>
	                            </td>
	                            <!-- <td>
	                                <div class="buttons pull-left">
	                                    <span class="urlbtn del"></span>
	                                    <span class="urlbtn add iblock"></span>
	                                </div>
	                                
	                            </td> -->
	                        </tr>
	                    </tbody>
	                </table>
                </form>
                <div class="confirm-buttons text-center" style="margin-top:30px;">
                    <a class="btn w100" style="cursor:pointer;" onclick="" id="regStatus">등록</a>
                    <a class="btn w100 btn-white" href="#">취소</a>
                </div>
        </div>

    </div>


		<!-- <section class="side-nav">
            <ul>
                <li class=""><a href="/baseinfo/employee.do">직원관리</a></li>
                <li class="active"><a href="/baseinfo/driver.do">기사관리</a></li>
                <li class=""><a href="/baseinfo/customer.do">거래처관리</a></li>
                <li class=""><a href="/baseinfo/company.do">회사정보관리</a></li>
            </ul>
        </section> -->
     <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">HOME</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">기사 관리</a></li>
                </ul>
            </div>

            <div class="up-dl clearfix header-search">
                 <table>
                    <tbody>
                        <tr>
                        	<!-- <td>
                                <input type="button" id="btn-search" value="기사등록"  onclick="javascript:addDriver();" class="btn-primary">
                            </td> -->
                            <td style="width: 145px;">
                                <div class="select-con">
							        <select class="dropdown" name="searchType" id="searchType">
							        	<option value="" <c:if test='${paramMap.searchType eq "" }'> selected="selected"</c:if>>조회구분을 선택 하세요.</option>
							            <option value="00" <c:if test='${paramMap.searchType eq "00" }'> selected="selected"</c:if>>이름</option>
							            <option value="01" <c:if test='${paramMap.searchType eq "01" }'> selected="selected"</c:if>>연락처</option>
							        </select>
							        <span></span>
							    </div>
                            </td>
                            <td style="width: 145px;">
                                <div class="select-con">
							        <select class="dropdown" name="searchGubun" id="searchGubun">
							        	<option value="" <c:if test='${paramMap.searchGubun eq "" }'> selected="selected"</c:if>>셀프/캐리어</option>
							            <option value="S" <c:if test='${paramMap.searchGubun eq "00" }'> selected="selected"</c:if>>셀프</option>
							            <option value="C" <c:if test='${paramMap.searchGubun eq "01" }'> selected="selected"</c:if>>캐리어</option>
							        </select>
							        <span></span>
							    </div>
                            </td>
                            <td style="width: 190px;">
                                <input type="text" id="searchWord" name="searchWord" value="${paramMap.searchWord}"   onkeypress="if(event.keyCode=='13') search();">
                            </td>
                            <td>
                                <input type="button" id="btn-search" value="검색" class="btn-primary" onclick="javascript:search();">
                                <form id="excelForm" style="display:inline-block;" name="excelForm" method="post" enctype="multipart/form-data">
                                	<input type="file" style="display:inline-block; width:300px;" id="upload" name="bbsFile" value="일괄입력" class="btn-primary" onchange="javascript:fileUpload(this);">
                                </form>
                            </td>
                            
                            <td>
                            <div class="upload-btn">
			                    <input type="button" onclick="javascript:excelDownload();" value="엑셀 다운로드">
			                </div>
                            </td>
                            
                            
                            
                            
                        </tr>
                    </tbody>
                </table>
            </div>
<!-- 
            <div class="dispatch-btn-container">
               
                <div class="dispatch-btn">
                    <i class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div>
            </div> -->
        </section>

			
			<a href="javascript:;" class="clipboard"  data-clipboard-target="#txt_MyLink"></a>

            <section class="bottom-table" style="width:1600px; margin-left:290px;">
            		    ※ 퇴사한 기사님은 <span style="color:#EE0D0D;"> 빨간색</span>으로 표시 됩니다.
                <table class="article-table" style="margin-top:15px; width:100%;" >
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <td style="text-align:center;">소유주</td>
                            <td style="text-align:center;">이름</td>
                            <td style="text-align:center;">연락처</td>
                            <!-- <td style="text-align:center;">주민번호</td> -->
                            <!-- <td>자격증번호</td> -->
                            <td style="text-align:center;">사업자구분</td>
                            <td style="text-align:center;">사업자번호</td>
                            <!-- <td>업태</td>
                            <td>종목</td> -->
                            <!-- <td style="text-align:center;">사업장주소</td> -->
                            <td style="text-align:center;">소속</td>
                            <td style="text-align:center;">구분</td>
                            <!-- <td>입사일</td>
                            <td>퇴사일</td>
                            <td>적용요율</td>
                            <td>지입료(원)</td>
                            <td>예치금(원)</td>
                            <td>계좌정보</td> -->
                            <td style="text-align:center;">차종</td>
                            <!-- <td style="text-align:center;">약칭</td> -->
                            <td style="text-align:center;">차량소속</td>
                            <td style="text-align:center;">차량번호</td>
                           <!--  <td>차고지</td> -->
                            <!-- <td style="text-align:center;">차대번호</td> -->
                            <!-- <td style="text-align:center;">보험정보</td> -->
                            <td style="text-align:center;">신고상태</td>
                            <td style="text-align:center;">관리</td>
                        </tr>
                    </thead>
                    <tbody id="">
                    	<c:forEach var="data" items="${listData}" varStatus="status">
							<tr class="ui-state-default"> 
	                            <td style="text-align:center;">${data.driver_owner}</td>
	                              <c:if test="${data.register_status == 'O'}">
	                           		<td style="text-align:center; color:#EE0D0D;">${data.driver_name}</td>
	                            </c:if>
	                              <c:if test="${data.register_status != 'O'}">
	                           		<td style="text-align:center;">${data.driver_name}</td>
	                            </c:if>
	                            
	                            <td style="text-align:center;">${data.phone_num}</td>
	                            <%-- <td style="text-align:center;">${data.resident_registration_number}</td> --%>
	                            <%-- <td>${data.driver_license}</td> --%>
	                           <td style="text-align:center;">
	                            <c:if test="${data.company_kind == '00'}">
	                            	법인
	                            </c:if>
	                            <c:if test="${data.company_kind == '01'}">
	                            	개인
	                            </c:if>
	                            <c:if test="${data.company_kind == '02'}">
	                            	기타
	                            </c:if>
	                            <c:if test="${data.company_kind == '' || data.company_kind == null}">
	                            	
	                            </c:if>
	                            </td>
	                            <td style="text-align:center;">${data.business_license_number} </td>
	                            <%-- <td>${data.business_condition}</td>
	                            <td>${data.business_kind}</td> --%>
	                            <%-- <td style="text-align:center;">${data.address}</td> --%>
	                            <td style="text-align:center;">${data.assign_company}</td>
	                            <td style="text-align:center;">
	                            <c:if test="${data.driver_kind == '00'}">
	                            	직영
	                            </c:if>
	                            <c:if test="${data.driver_kind == '01'}">
	                            	지입
	                            </c:if>
	                            <c:if test="${data.driver_kind == '02'}">
	                            	외부
	                            </c:if>
	                            <c:if test="${data.driver_kind == '03'}">
	                            	직영-수수료
	                            </c:if>
	                            <c:if test="${data.driver_kind == '' || data.driver_kind == null }">	                            	
	                            </c:if>
	                            </td>
	                            <%-- <td>${data.join_dt}</td>
	                            <td>${data.resign_dt}</td> 
	                            <td>${data.deduction_rate}%</td>
	                            <td>${data.driver_balance}</td>
	                            <td>${data.driver_deposit}</td>
	                            <td>${data.bank_name}/${data.depositor}/${data.account_number}</td> --%>
	                            <td style="text-align:center;">${data.car_kind}</td>
	                            <%-- <td style="text-align:center;">${data.car_nickname}</td> --%>
								<td style="text-align:center;">
	                            <c:if test="${data.car_assign_company == 'S'}">
	                            	셀프
	                            </c:if>
	                            <c:if test="${data.car_assign_company == 'C'}">
	                            	캐리어
	                            </c:if>
	                            <c:if test="${data.car_assign_company == '' || data.car_assign_company == null}">
	                            	<td style="text-align:center;"></td>
	                            </c:if>
	                            </td>
	                            <td style="text-align:center;">${data.car_num}</td>
	                            <%-- <td>${data.car_garage}</td> --%>
	                            <%-- <td style="text-align:center;">${data.car_id_num}</td> --%>
	                            <!-- 보험정보가 있는지 여부에 따라 구분 -->
	                            <%-- <c:if test="${data.ins_count == '0'}">
	                            	<td style="text-align:center;"><a style="cursor:pointer;" onclick="javascript:editIns('${data.driver_id}','0')" class="btn-edit">작성</a></td>
	                            </c:if>
	                            <c:if test="${data.ins_count != '0'}">
	                            	<td style="text-align:center;"><a style="cursor:pointer;" onclick="javascript:editIns('${data.driver_id}','1')" class="btn-edit">수정</a></td>
	                            </c:if> --%>
	                            <td style="text-align:center;">
	                            <c:if test="${data.register_status == 'N'}">
	                            	-
	                            </c:if>
	                            <c:if test="${data.register_status == 'I'}">
	                            	입사신고
	                            </c:if>
	                            <c:if test="${data.register_status == 'O'}">
	                            	퇴사신고
	                            </c:if>
	                            </td>
	                            <td style="width:140px;">
	                            	<a style="cursor:pointer;" onclick="javascript:editDriver('${data.driver_id}')" class="btn-edit">수정</a>
	                            	<span style="margin-left:5px;"><img style="width:15px; height:15px; cursor:pointer;" src="/img/check-icon.png" onclick="javascript:clipBoardCopy('${data.driver_id}',this);" alt=""  title='기사정보복사' ></span>
	                            </td>
                        	</tr>
						</c:forEach>
                    
                    
                    <!-- 
                    
                        <tr class="ui-state-default"> 
                            <td>1</td>
                            <td>2018년1월1일</td>
                            <td>17두3020</td>
                            <td>010-2222-3333</td>
                            <td>대우5톤</td>
                            <td>홍길동</td>
                            <td>
                                <a href="edit-article.html" class="btn-edit">수정</a>
                            </td>
                        </tr>
                        <tr class="ui-state-default"> 
                            <td>1</td>
                            <td>2018년1월1일</td>
                            <td>17두3020</td>
                            <td>010-2222-3333</td>
                            <td>대우5톤</td>
                            <td>홍길동</td>
                            <td>
                                <a href="edit-article.html" class="btn-edit">수정</a>
                            </td>
                        </tr>
                        <tr class="ui-state-default"> 
                            <td>1</td>
                            <td>2018년1월1일</td>
                            <td>17두3020</td>
                            <td>010-2222-3333</td>
                            <td>대우5톤</td>
                            <td>홍길동</td>
                            <td>
                                <a href="edit-article.html" class="btn-edit">수정</a>
                            </td>
                        </tr> -->
                       
                    </tbody>
                </table>
                <!-- <div class="confirmation">
                    
                </div> -->
                <!-- <div class="table-pagination text-center">
                    <ul class="pagination">
                        <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="curr-page"><a href="#">1</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
                    </ul>
                </div> -->
                <div class="table-pagination text-center">
                    <ul class="pagination">
                    	<html:paging uri="/baseinfo/driver.do" frontYn="N" />
                    </ul>
                </div>
            </section>
        
         
        <!-- <script src="/js/vendor/clipboard.min.js"></script> -->
           <!-- <script>
           $(document).ready(function(){

        		var clipboard = new Clipboard('.clipboard');



           });
        		    
            </script>  
 -->        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
