<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%

%>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script type="text/javascript">
$(document).ready(function(){

});

function addCompany(){
	
		
		if($("#companyName").val() == ""){
			alert("회사명이 입력되지 않았습니다.");
			return false;
		}
		
		if($("#companyKind").val() == ""){
			alert("회사구분이 입력되지 않았습니다.");
			return false;
		}
		
		if($("#businessLicenseNumber").val() == ""){
			alert("사업자번호가 입력되지 않았습니다.");
			return false;
		}else{
			if(!businessNumChk($("#businessLicenseNumber"))){
		  		alert("유효하지 않은 사업자번호 입니다.");
		  		$("#businessLicenseNumber").val("");
	            $("#businessLicenseNumber").focus();
				return false;
			}
		}
		
        if($("#companyOwnerName").val() == ""){
			alert("대표자명이 입력되지 않았습니다.");
			return false;
		}
        
        /* if($("#bankName").val() == ""){
			alert("은행명이 입력되지 않았습니다.");
			return false;
		}
        
        if($("#depositor").val() == ""){
			alert("예금주가 입력되지 않았습니다.");
			return false;
		}
        
        if($("#accountNumber").val() == ""){
			alert("계좌번호가 입력되지 않았습니다.");
			return false;
		}	
        
        if($("#businessCondition").val() == ""){
			alert("업태가 입력되지 않았습니다.");
			return false;
		}	
        
        if($("#businessKind").val() == ""){
			alert("종목이 입력되지 않았습니다.");
			return false;
		}	
        
        
        if($("#address").val() == ""){
			alert("주소가 입력되지 않았습니다.");
			return false;
		}	 */
		
        var accountList= new Array();
    	var size = $("#addLocation").find("tr").length;
    	 $("#addLocation").find("tr").each(function(index,element){
    		 if($(this).children().eq(1).children().val() != "" || $(this).children().eq(2).children().val() != "" || $(this).children().eq(3).children().val() != "" || $(this).children().eq(4).children().val() != ""){
    		 	var accountInfo= new Object();
    			 accountInfo.bankName = $(this).children().eq(1).children().val();
    			 accountInfo.depositor = $(this).children().eq(2).children().val();
    			 accountInfo.accountNumber = $(this).children().eq(3).children().val();
    			 accountInfo.accountName = $(this).children().eq(4).children().val();
    			 accountList.push(accountInfo);	 
    		 }
          }); 
    	
    	 if(accountList.length >= 1){
    		 $("#accountInfo").val(JSON.stringify({accountList : accountList}));
    	 }
		
	 	if(confirm("등록 하시겠습니까?")){
			$("#insertForm").attr("action","/baseinfo/insert-company.do");
			$("#insertForm").submit();	
		}
	
}

function jusoSearch(where,obj){
	
	new daum.Postcode({
	    oncomplete: function(data) {
	        // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

            // 각 주소의 노출 규칙에 따라 주소를 조합한다.
            // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
            var fullAddr = ''; // 최종 주소 변수
            var extraAddr = ''; // 조합형 주소 변수

            // 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
            if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                fullAddr = data.roadAddress;

            } else { // 사용자가 지번 주소를 선택했을 경우(J)
                fullAddr = data.jibunAddress;
            }

            // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
            if(data.userSelectedType === 'R'){
                //법정동명이 있을 경우 추가한다.
                if(data.bname !== ''){
                    extraAddr += data.bname;
                }
                // 건물명이 있을 경우 추가한다.
                if(data.buildingName !== ''){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
                fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
            }

            // 우편번호와 주소 정보를 해당 필드에 넣는다.
            //document.getElementById('sample6_postcode').value = data.zonecode; //5자리 새우편번호 사용
         	
            //$(obj).parent().children().eq(0).val(fullAddr);
            //$(obj).parent().parent().children().eq(0).children().val(data.sido);

            //alert(JSON.stringify(data));
            
            $(obj).val(fullAddr);
                        
            // 커서를 상세주소 필드로 이동한다.
            //document.getElementById('sample6_address2').focus();
        }
	    
	}).open();	
	
}

function addAccount(){
	
	var result = "";
	result += '<tr><td style="display:none;"></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="은행명" name="bankName"></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="예금주" name="depositor"></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="계좌번호" name="accountNumber"></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="계좌명칭" name="accountName"></td>';
	result += '<td class="widthAuto" style=""><input style="display:inline; float:right; border:2px solid #eee;" type="button" onclick="javascript:deleteAccount(this);" value="삭제" class="btn-normal"></td></tr>';
	
	$("#rowspanCnt").attr("rowspan",Number($("#rowspanCnt").attr("rowspan"))+1);
	$("#addLocation").append(result);

}


function deleteAccount(obj){
	
	$("#rowspanCnt").attr("rowspan",Number($("#rowspanCnt").attr("rowspan"))-1);
	$(obj).parent().parent().remove();
	
}



</script>

	<!-- <section class="side-nav">
            <ul>
                <li class=""><a href="/baseinfo/employee.do">직원관리</a></li>
                <li class=""><a href="/baseinfo/driver.do">기사관리</a></li>
                <li class=""><a href="/baseinfo/customer.do">거래처관리</a></li>
                <li class="active"><a href="/baseinfo/company.do">회사정보관리</a></li>
            </ul>
        </section> -->

 <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">HOME</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">회사 등록</a></li>
                </ul>
            </div>

            <div class="up-dl clearfix header-search">
                 <table>
                    <tbody>
                        <tr>
                            <td style="width: 145px;">
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">선택바
                                    <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">HTML</a></li>
                                        <li><a href="#">CSS</a></li>
                                        <li><a href="#">JavaScript</a></li>
                                    </ul>
                                </div>
                            </td>
                            <td style="width: 190px;">
                                <form action="">
                                    <input type="text" class="search-box">
                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
                                    <div class="search-result">
                                        <!-- No result -->
                                        <div class="no-result d-table">
                                            <div class="d-tbc">
                                                <i class="fa fa-exclamation-triangle fa-3x" aria-hidden="true"></i>
                                                <span>No results have been found.</span>
                                            </div>
                                        </div>

                                        <!-- Result type1 -->
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Campaigns</p>
                                            <a href="" class="result-sub"><span>네오퓨쳐</span></a>
                                            <p class="camp-type"><span>Campaign type:</span> <span>Powerlink</span></p>
                                            <p class="camp-id"><span>Campaign ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                        <!-- Result type2 -->
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Ad groups</p>
                                            <a href="" class="result-sub"><span>네오퓨쳐</span>_#0007</a>
                                            <p class="camp-type"><span>Campaign type:</span> <span>Powerlink</span></p>
                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
                                            <p class="adgroup-id"><span>Ad group ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                        <!-- Result type3 -->
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Ads - Powerlink</p>
                                            <div class="result-box">
                                                <p class="title">네오퓨쳐</p>
                                                <p class="content">일반 홈페이지에서 신개념 융복합 멀티미디어 홈피이지 제작, 견적상담 환영</p>

                                                <ul>
                                                    <li>View URL: http://www.neofss.com</li>
                                                    <li>Linked URL: http://www.neofss.com</li>
                                                </ul>
                                            </div>
                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
                                            <p class="adgroup"><span>Ad group:</span> <span class="red">네오퓨쳐</span>_#0003</p>
                                            <p class="adgroup-id"><span>Ad ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                        <!-- Result type4 -->
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Ads - PowerContents</p>
                                            <div class="result-box w-img">
                                                <div class="d-table">
                                                    <div class="d-tbc">
                                                        <img src="/img/ads-img.png" alt="">
                                                    </div>
                                                    <div class="d-tbc">
                                                        <p class="title">홈페이지제작 이러면 망한다!</p>
                                                        <p class="content">홈페이지제작! 대충 업체에 맡기면 된다는 생각으로 접근하면, 십중팔구는 실패하는 이유와 홈페이지 제작에 관한 최소한의 지식은 가지고 업체를 선정해야 돈과 시간을 낭비하는 일을 줄일수 있다.</p>
                                                    </div>
                                                </div>
                                                <ul>
                                                    <li>Content created: 2017-12-15</li>
                                                    <li>Brand Name: 네오퓨쳐</li>
                                                    <li>View URL: http://blog.naver.com/marujeen</li>
                                                    <li>Linked URL: http://blog.naver.com/marujeen</li>
                                                </ul>
                                            </div>
                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
                                            <p class="adgroup"><span>Ad group:</span> <span class="red">네오퓨쳐</span>_#0003</p>
                                            <p class="adgroup-id"><span>Ad ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                    </div>
                                </form>
                            </td>
                            <td>
                                <input type="button" id="btn-search" value="검색" class="btn-primary">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
<!-- 
            <div class="dispatch-btn-container">
               
                <div class="dispatch-btn">
                    <i class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div>
            </div> -->
        </section>

        <div class="dispatch-wrapper">
           <section class="dispatch-bottom-content active">
               <form id="insertForm" action="/baseinfo/insert-company.do"  method="post" enctype="multipart/form-data">
               		<h3 style="font-size: 20px; text-align: center; margin-bottom:20px; font-weight: 700; font-family: NotoSansBold,sans-serif;">회사정보 관리</h3>
               		<input type="hidden" name="accountInfo" id="accountInfo">
               <div class="form-con clearfix">
                   <div class="column" style="margin-bottom:5px">
                   		<table>
                            <tbody>
                                <tr>
                                    <td>회사명<span style="color:#F00;">&nbsp;*</span></td>
                                    <td class="widthAuto" style="">
                                        <input type="text" placeholder="회사명" name="companyName" id="companyName">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                       <table>
                            <tbody>
                                <tr>
                                    <td>구분<span style="color:#F00;">&nbsp;*</span></td>
                                    <td class="widthAuto" style="">
		                                <div class="select-con">
									        <select class="dropdown" name="companyKind" id="companyKind">
									        	<option value="">회사구분을 선택 하세요.</option>
									            <option value="00">법인</option>
									            <option value="01">개인</option>
									            <!-- <option value="02">외국인</option>
									            <option value="03">개인(주민번호)</option> -->
									        </select>
									        <span></span>
									    </div>
		                            </td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
                            <tbody>
                                <tr>
                                    <td>사업자번호<span style="color:#F00;">&nbsp;*</span></td>
                                    <td class="widthAuto" style="">
                                        <input type="text" placeholder="사업자번호" name="businessLicenseNumber" id="businessLicenseNumber">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
                            <tbody>
                                <tr>
                                    <td>대표자명<span style="color:#F00;">&nbsp;*</span></td>
                                    <td class="widthAuto" style="">
                                        <input type="text" placeholder="대표자명" name="companyOwnerName" id="companyOwnerName">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                   </div>
                   <div class="column">
                       <table>
                            <tbody>
                                <tr>
                                    <td>법인등록번호</td>
                                    <td class="widthAuto" style="">
                                        <input type="text" placeholder="법인등록번호" name="corporationRegistrationNumber" id="corporationRegistrationNumber">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
                            <tbody>
                                <tr>
                                    <td>설립년월일</td>
                                    <td class="widthAuto" style="">
                                        <input type="text" class="datepick" placeholder="설립년월일" name="establishDate" id="establishDate">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
                            <tbody>
                                <tr>
                                    <td>개업년월일</td>
                                    <td class="widthAuto" style="">
                                        <input type="text" class="datepick" placeholder="개업년월일" name="setUpDate" id="setUpDate">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
                            <tbody>
                                <tr>
                                    <td>대표전화</td>
                                    <td class="widthAuto" style="">
                                        <input type="text" placeholder="대표전화" name="phoneNum" id="phoneNum">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                   </div>
                   
                   <table>
                            <tbody>
                                <tr>
                                    <td>업태</td>
                                    <td class="widthAuto" style="">
                                        <input type="text" placeholder="업태" name="businessCondition" id="businessCondition">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
                            <tbody>
                                <tr>
                                    <td>종목</td>
                                    <td class="widthAuto" style="">
                                        <input type="text" placeholder="종목" name="businessKind" id="businessKind">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                      <table>
                            <tbody>
                                <tr>
                                    <td>주소</td>
                                    <td class="widthAuto" style="">
                                        <input type="text" placeholder="주소" name="address" id="address" onclick="jusoSearch('departure',this);">
                                    </td>
                                </tr>
                            </tbody>
                        </table>  
                      <table>
	                            <tbody>
	                                <tr>
	                                    <td>상세주소</td>
	                                    <td class="widthAuto" style="">
	                                        <input type="text" placeholder="상세주소" name="addressDetail" id="addressDetail">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>첨부파일</td>
	                                    <td class="widthAuto" style="">
	                                        <input type="file" id="upload"  name="bbsFile" multiple>
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        
	                        <table>
	                            <tbody id="addLocation">
	                                <tr>
	                                    <td rowspan="1" id="rowspanCnt">계좌정보</td>
	                                    <td class="widthAuto" style="">
	                                    	<input type="text" placeholder="은행명" name="bankName">
	                                    </td>
	                                    <td class="widthAuto" style="">
	                                		<input type="text" placeholder="예금주" name="depositor">
	                                    </td>
	                                    <td class="widthAuto" style="">
	                                    	<input type="text" placeholder="계좌번호" name="accountNumber">
	                                    </td>
	                                    <td class="widthAuto" style="">
	                                    	<input type="text" placeholder="계좌명칭" name="accountName">
	                                    </td>
	                                    <td class="widthAuto" style="">
	                                    	<input style="display:inline; float:right;" type="button" id="file_mod" onclick="javascript:addAccount();" value="추가" class="btn-primary">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        
	                        
	                          
                    <!-- <table>
                        <tbody>
                                <tr>
                                    <td>계좌번호</td>
                                    <td class="widthAuto" style="">
                                        <input type="text" placeholder="계좌번호" name="" id="">
                                    </td>
                                    <td>
                                        <input class="btn-edit" type="button" value="검색">
                                    </td>
                                    
                                </tr>
                            
                        </tbody>
                    </table> -->
               </div>
               
                <div class="confirmation">
                    <div class="confirm">
                        <input type="button" value="등록하기"  onclick="javascript:addCompany();">
                    </div>
                    <div class="cancel">
                        <input type="button" value="취소" onclick="javascript:history.go(-1); return false;">
                    </div>
                </div>
                    <!-- <script src="js/vendor/bootstrap-datepicker.min.js"></script> -->
                </form>
                <!-- <div class="table-pagination text-center">
                    <ul class="pagination">
                        <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="curr-page"><a href="#">1</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
                    </ul>
                </div> -->
            </section>
        </div>
