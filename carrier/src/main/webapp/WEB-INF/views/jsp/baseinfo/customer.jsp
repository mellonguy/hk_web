<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>
<%

%>

<script type="text/javascript">
$(document).ready(function(){
//	$('.car-modal-field').show();

$("#toggler").click(function(){
        $(".modal").fadeIn();
    });
    $(".modal .btn-white").click(function(){
        $(".modal").fadeOut();
    });

    $('.urllist').on('click','.urlbtn.add',function(){
        $(this).prev().css('display','inline-block');
        $(' <tr>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="department">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="name">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="phoneNum">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="address">\
                                </div>\
                            </td>\
                            <td>\
                                <div class="inputdetails">\
                                  <input type="text" name="email">\
                                </div>\
                            </td>\
                            <td>\
                            <div class="inputdetails">\
                              <input type="text" name="etc">\
                            </div>\
                        </td>\
                            <td>\
                                <div class="buttons pull-left">\
                                    <span class="urlbtn del"></span>\
                                    <span class="urlbtn add iblock"></span>\
                                </div>\
                                \
                            </td>\
                        </tr>').appendTo('.urllist tbody');
        $(this).hide();
        $('.urllist tbody').find('tr:last-child .urlbtn.add').css('display','inline-block');
    });

    $('.urllist').on('click','.urlbtn.del',function(){
        $(this).parent().parent().parent().remove();
        console.log('a');
        $('.urllist tbody').find('tr:last-child .urlbtn.add').css('display','inline-block');
        var rowcount=0;
        $('.urllist tbody tr').each(function(){
            rowcount++;
        });
        if(rowcount==1){
            $('.urllist tbody').find('.urlbtn.del').css('display','none');
        }
    });

});

function editCustomer(customerId){
	
	document.location.href = "/baseinfo/edit-customer.do?customerId="+customerId;
}


function insertCharge(mode){
	
	var status = "등록";
	if(Number(mode)==1){
		status = "수정";
	}
	
	var chargeList= new Array();
	var size = $("#chargeList").find("tr").length;
	
	 $("#chargeList").find("tr").each(function(index,element){
		 var chargeInfo= new Object();
		 
		 if($(this).children().eq(0).children().children().val() != "" || $(this).children().eq(1).children().children().val() != "" || $(this).children().eq(2).children().children().val() != "" || $(this).children().eq(3).children().children().val() != "" || $(this).children().eq(4).children().children().val() != "" ){
			 chargeInfo.department = $(this).children().eq(0).children().children().val();
			 chargeInfo.name = $(this).children().eq(1).children().children().val();
			 chargeInfo.phoneNum = $(this).children().eq(2).children().children().val();
			 chargeInfo.address = $(this).children().eq(3).children().children().val();
			 chargeInfo.email = $(this).children().eq(4).children().children().val();
			 chargeInfo.etc = $(this).children().eq(5).children().children().val();
			 chargeList.push(chargeInfo);	 
		 }
		 
		 /* if($(this).children().eq(0).children().children().val() == ""){
			 alert("담당자 부서가 입력 되지 않았습니다.");
			 status = false;
		 }else{
			 chargeInfo.department = $(this).children().eq(0).children().children().val();
		 }
		 if($(this).children().eq(1).children().children().val() == ""){
			 alert("담당자 이름이 입력 되지 않았습니다.");
			 status = false;
		 }else{
			 chargeInfo.name = $(this).children().eq(1).children().children().val();
		 }
		 if($(this).children().eq(2).children().children().val() == ""){
			 alert("담당자 연락처가 입력 되지 않았습니다.");
			 status = false;
		 }else{
			 chargeInfo.phoneNum = $(this).children().eq(2).children().children().val();
		 }
		 if($(this).children().eq(3).children().children().val() == ""){
			 alert("담당자 주소가 입력 되지 않았습니다.");
			 status = false;
		 }else{
			 chargeInfo.address = $(this).children().eq(3).children().children().val();
		 }
		 if($(this).children().eq(4).children().children().val() == ""){
			 alert("담당자 이메일이 입력 되지 않았습니다.");
			 status = false;
		 }else{
			 chargeInfo.email = $(this).children().eq(4).children().children().val();
		 } */
		 
			 
		 
      }); 
	
	 if(chargeList.length >= 1){
		 if(confirm(status+" 하시겠습니까?")){
			 $.ajaxSettings.traditional = true;
			 $.ajax({ 
					type: 'post' ,
					url : "/personInCharge/insertPersonInCharge.do" ,
					dataType : 'json' ,
					data : {
						customerId : $("#customerId").val(),
						chargeList : JSON.stringify({chargeList : chargeList})
					},
					success : function(data, textStatus, jqXHR)
					{
						alert("관리자를 "+status+" 하였습니다.");
						document.location.href="/baseinfo/customer.do";
					} ,
					error : function(xhRequest, ErrorText, thrownError) {

					}
				});	
			}		 

	 }else{
		 alert("담당자 정보가 입력 되지 않았습니다.");
		 return false;
	 }
	
}



function getChargeInfo(customerId,mode){
	
	$("#customerId").val(customerId);
	
	$.ajax({ 
		type: 'post' ,
		url : "/personInCharge/getPersonInChargeList.do" ,
		dataType : 'json' ,
		data : {
			customerId : customerId
		},
		success : function(data, textStatus, jqXHR)
		{
			
			var list = data.resultData;
			var result = "";
			$("#chargeList").html("");
			
       			for(var i=0; i<list.length; i++){
       				result += '<tr>'; 
       				result += '<td><div class="inputdetails"><input type="text" name="department" value=\''+list[i].department+'\'></div></td>'; 
       				result += '<td><div class="inputdetails"><input type="text" name="name" value=\''+list[i].name+'\'></div></td>';
       				result += '<td><div class="inputdetails"><input type="text" name="phoneNum" value=\''+list[i].phone_num+'\'></div></td>';
       				result += '<td><div class="inputdetails"><input type="text" name="address" value=\''+list[i].address+'\'></div></td>';
       				result += '<td><div class="inputdetails"><input type="text" name="email" value=\''+list[i].email+'\'></div></td>';
       				result += '<td><div class="inputdetails"><input type="text" name="etc" value=\''+list[i].etc+'\'></div></td>';
       				if(i == (list.length-1)){
       					result += '<td><div class="buttons pull-left"><span class="urlbtn del"></span><span class="urlbtn add iblock"></span></div></td>';	
       				}else{
       					result += '<td><div class="buttons pull-left"><span class="urlbtn del" style="display:inline-block;"></span></div></td>';
       				}
       				
       				
       				result += '</tr>';
       			}
			
			
			$("#chargeList").html(result);
			
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
	
	
}


function addCharge(customerId,mode){
	
	$("#customerId").val(customerId);
	$("#mode").val(mode);
	
	
	if(Number(mode) == 1){
		getChargeInfo(customerId,mode);
		
		$("#confirmBtn").html("수정");
		$("#confirmBtn").attr("onclick","javascript:insertCharge(1);");
	}else{
		var result = "";
		$("#chargeList").html("");
			result += '<tr>'; 
			result += '<td><div class="inputdetails"><input type="text" name="department"></div></td>'; 
			result += '<td><div class="inputdetails"><input type="text" name="name"></div></td>';
			result += '<td><div class="inputdetails"><input type="text" name="phoneNum"></div></td>';
			result += '<td><div class="inputdetails"><input type="text" name="address"></div></td>';
			result += '<td><div class="inputdetails"><input type="text" name="email"></div></td>';
			result += '<td><div class="inputdetails"><input type="text" name="etc"></div></td>';
			result += '<td><div class="buttons pull-left"><span class="urlbtn del"></span><span class="urlbtn add iblock"></span></div></td>';	
			result += '</tr>';
			$("#chargeList").html(result);
			$("#confirmBtn").html("등록");
			$("#confirmBtn").attr("onclick","javascript:insertCharge(0);");
	}
	
        $(".modal").fadeIn();
   
	
	
}

function fileUpload(fis){
	
	if(confirm("거래처를 일괄 저장 하시겠습니까?")){
		$("#excelForm").attr("action","/baseinfo/customer-excel-insert.do");
		$("#excelForm").submit();	
	}
	
}

function fileUploadCharge(fis){
	
	  if(confirm("담당자를 일괄 저장 하시겠습니까?")){
		$("#excelChargeForm").attr("action","/baseinfo/personInCharge-excel-insert.do");
		$("#excelChargeForm").submit();	
	}  
	
	 /* if(confirm("키워드를 일괄 저장 하시겠습니까?")){
		$("#excelChargeForm").attr("action","/baseinfo/keyWord-excel-insert.do");
		$("#excelChargeForm").submit();	
	} */
	
	 /* if(confirm("일괄 확정 하시겠습니까?")){
			$("#excelChargeForm").attr("action","/allocation/excelUploadForDecideStatus.do");
			$("#excelChargeForm").submit();	
		} */ 
	
	
}



function addCustomer(){
	document.location.href = "/baseinfo/add-customer.do";
}

function excelDownload(){
	
	document.location.href = "/baseinfo/excel_download.do?mode=customer";
}

function viewCustomer(customerId){
	document.location.href = "/baseinfo/view-customer.do?customerId="+customerId;
}


function search(){
	
	document.location.href = "/baseinfo/customer.do?searchType="+$("#searchType").val()+"&searchWord="+encodeURI($("#searchWord").val())+"&searchGubun="+$("#searchGubun").val();
	
}


</script>

<div class="modal">
        <div class="modal-con" style="width:1000px;">
        	<form id="insertForm" action="/personInCharge/insert-personInCharge.do">
        		<input type="hidden" name="customerId" id="customerId">
        		<input type="hidden" name="mode" id="mode">
	            <table class="urllist text-center iplist"> 
	                    <colgroup>
	                    </colgroup>
	                    <thead>
	                        <tr>
	                            <td>담당자부서</td>
	                            <td>담당자이름</td>
	                            <td>담당자연락처</td>
	                            <td>근무지주소</td>
	                            <td>담당자이메일</td>
	                            <td>비고</td>
	                        </tr>
	                        
	                    </thead>
	                    <tbody id="chargeList">
	                        <tr>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="department" >
	                                </div>
	                            </td>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="name" >
	                                </div>
	                            </td>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="phoneNum" >
	                                </div>
	                            </td>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="address" >
	                                </div>
	                            </td>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="email">
	                                </div>
	                            </td>
	                            <td>
	                                <div class="inputdetails">
	                                  <input type="text" name="etc">
	                                </div>
	                            </td>
	                            <td>
	                                <div class="buttons pull-left">
	                                    <span class="urlbtn del"></span>
	                                    <span class="urlbtn add iblock"></span>
	                                </div>
	                                
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
                </form>
                <div class="confirm-buttons text-center"  style="margin-top:30px;">
                    <a class="btn w100" style="cursor:pointer;" id="confirmBtn" onclick="javascript:insertCharge();">확인</a>
                    <a class="btn w100 btn-white" href="#">취소</a>
                </div>
        </div>

    </div>



		<!-- <section class="side-nav">
            <ul>
                <li class=""><a href="/baseinfo/employee.do">직원관리</a></li>
                <li class=""><a href="/baseinfo/driver.do">기사관리</a></li>
                <li class="active"><a href="/baseinfo/customer.do">거래처관리</a></li>
                <li class=""><a href="/baseinfo/company.do">회사정보관리</a></li>
            </ul>
        </section> -->

<section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">HOME</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">거래처 관리</a></li>
                </ul>
            </div>

            <div class="up-dl clearfix header-search">
                 <table>
                    <tbody>
                        <tr>
                        	<td>
                                <input type="button" id="btn-search" value="거래처등록"  onclick="javascript:addCustomer();" class="btn-primary">
                            </td>
                            <td style="width: 145px;">
                                <div class="select-con">
							        <select class="dropdown" name="searchType" id="searchType">
							        	<option value="" <c:if test='${paramMap.searchType eq "" }'> selected="selected"</c:if>>조회구분을 선택 하세요.</option>
							            <option value="00" <c:if test='${paramMap.searchType eq "00" }'> selected="selected"</c:if>>회사명</option>
							            <option value="01" <c:if test='${paramMap.searchType eq "01" }'> selected="selected"</c:if>>대표자명</option>
							            <option value="02" <c:if test='${paramMap.searchType eq "02" }'> selected="selected"</c:if>>담당자명</option>
							            <option value="03" <c:if test='${paramMap.searchType eq "03" }'> selected="selected"</c:if>>담당자연락처</option>
							            <option value="04" <c:if test='${paramMap.searchType eq "04" }'> selected="selected"</c:if>>기사명</option>
							        </select>
							        <span></span>
							    </div>
                            </td>
                            <td style="width: 145px;">
                                <div class="select-con">
							        <select class="dropdown" name="searchGubun" id="searchGubun">
							        	<option value="" <c:if test='${paramMap.searchGubun eq "" }'> selected="selected"</c:if>>회사구분</option>
							            <option value="00" <c:if test='${paramMap.searchGubun eq "00" }'> selected="selected"</c:if>>법인</option>
							            <option value="01" <c:if test='${paramMap.searchGubun eq "01" }'> selected="selected"</c:if>>개인</option>
							            <option value="02" <c:if test='${paramMap.searchGubun eq "02" }'> selected="selected"</c:if>>외국인</option>
							            <option value="03" <c:if test='${paramMap.searchGubun eq "03" }'> selected="selected"</c:if>>개인(주민번호)</option>
							        </select>
							        <span></span>
							    </div>
                            </td>
                            <td style="width: 190px;">
                                <input type="text" id="searchWord" name="searchWord" value="${paramMap.searchWord}"   onkeypress="if(event.keyCode=='13') search();">
                            </td>
                            <td>
                                <input type="button" id="btn-search" value="검색" class="btn-primary" onclick="javascript:search();">
                            </td>
                            <td>
	                            <div class="upload-btn">
	                            	<form id="excelForm" style="display:none" name="excelForm" method="post" enctype="multipart/form-data">
	                                	<input type="file" style="display:inline-block; width:300px;" id="upload" name="bbsFile" value="일괄입력" class="btn-primary" onchange="javascript:fileUpload(this);">
	                                </form>
				                    <input type="button" onclick="javascript:$('#upload').trigger('click'); return false;" value="거래처 엑셀 업로드">
				                </div>
                            </td>
                            <td>
	                            <div class="upload-btn">
	                            	<form id="excelChargeForm" style="display:none" name="excelForm" method="post" enctype="multipart/form-data">
	                                	<input type="file" style="display:inline-block; width:300px;" id="uploadCharge" name="bbsFile" value="일괄입력" class="btn-primary" onchange="javascript:fileUploadCharge(this);">
	                                </form>
				                    <input type="button" onclick="javascript:$('#uploadCharge').trigger('click'); return false;" value="담당자 엑셀 업로드">
				                </div>
                            </td>
                            <td>
	                            <div class="upload-btn">
				                    <input type="button" onclick="javascript:excelDownload();" value="엑셀 다운로드">
				                </div>
                            </td>
                            
                        </tr>
                    </tbody>
                </table>
            </div>
<!-- 
            <div class="dispatch-btn-container">
               
                <div class="dispatch-btn">
                    <i class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div>
            </div> -->
        </section>



            <section class="bottom-table" style="width:1600px; margin-left:290px;">
                
                <table class="article-table" style="margin-top:15px">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <td style="text-align:center;">회사명</td>
                            <td style="text-align:center;">대표자</td>
                            <td style="text-align:center;">회사구분</td>
                            <td style="text-align:center;">매출매입구분</td>
                            <td style="text-align:center;">세금계산서</td>
                            <td style="text-align:center; width:300px;">특이사항</td>
                            <td style="text-align:center; width:300px;">비고</td>
                            <td style="text-align:center;">수정일</td>
                            <td style="text-align:center;">수정한 담당자</td>
                            <!-- <td style="text-align:center;">담당자 정보 수정</td> -->
                            <td style="text-align:center;">거래처 정보 수정</td>
                            
                        </tr>
                    </thead>
                    <tbody id="">
                    <c:forEach var="data" items="${listData}" varStatus="status">
							<tr class="ui-state-default"> 
	                            <td style="text-align:center;  cursor:pointer;" onclick="javascript:viewCustomer('${data.customer_id}');">${data.customer_name}</td>
	                            <td style="text-align:center;">${data.customer_owner_name}</td>
	                            <c:if test="${data.customer_kind == '00'}">
	                            	<td style="text-align:center;">법인</td>
	                            </c:if>
	                            <c:if test="${data.customer_kind == '01'}">
	                            	<td style="text-align:center;">개인</td>
	                            </c:if>
	                            <c:if test="${data.customer_kind == '02'}">
	                            	<td style="text-align:center;">외국인</td>
	                            </c:if>
	                            <c:if test="${data.customer_kind == '03'}">
	                            	<td style="text-align:center;">개인(주민번호)</td>
	                            </c:if>
	                            <c:if test="${data.customer_kind == ''}">
	                            	<td style="text-align:center;"></td>
	                            </c:if>
	                            <td style="text-align:center;">${data.sales_purchase_division}</td>
	                            <td style="text-align:center;">${data.billing_division_name}</td>
	                            <%-- <td style="text-align:center;">${data.accounter}</td> --%>
	                            <td style="text-align:center;">${data.significant_data}</td>
	                            <td style="text-align:center;">${data.etc}</td>
	                            <td style="text-align:center;">${data.update_dt_str}&nbsp;&nbsp;${data.update_dt_si}</td>
	                            <td style="text-align:center;">${data.updater}</td>
	                            <%-- <c:if test="${data.person_in_charge_count == '0'}">
	                            	<td style="text-align:center;"><a style="cursor:pointer;" onclick="javascript:addCharge('${data.customer_id}','0')" class="btn-edit">등록</a></td>
	                            </c:if>
	                            <c:if test="${data.person_in_charge_count != '0'}">
	                            	<td style="text-align:center;"><a style="cursor:pointer;" onclick="javascript:addCharge('${data.customer_id}','1')" class="btn-edit">수정</a></td>
	                            </c:if> --%>
	                            <td><a style="cursor:pointer;" onclick="javascript:editCustomer('${data.customer_id}')" class="btn-edit">수정</a></td>
                        	</tr>
						</c:forEach>
                        <!-- <tr class="ui-state-default">
                            <td>1</td>
                            <td>2018년1월1일</td>
                            <td>개인</td>
                            <td>카캐리어</td>
                            <td>02-2000-2333</td>
                            <td>010-2222-2222</td>
                            <td>홍길동 </td>
                            <td>
                                <a href="edit-customer.html" class="btn-edit">수정</a>
                            </td>
                        </tr>
                        <tr class="ui-state-default">
                            <td>1</td>
                            <td>2018년1월1일</td>
                            <td>개인</td>
                            <td>카캐리어</td>
                            <td>02-2000-2333</td>
                            <td>010-2222-2222</td>
                            <td>홍길동 </td>
                            <td>
                                <a href="edit-customer.html" class="btn-edit">수정</a>
                            </td>
                        </tr>
                        <tr class="ui-state-default">
                            <td>1</td>
                            <td>2018년1월1일</td>
                            <td>개인</td>
                            <td>카캐리어</td>
                            <td>02-2000-2333</td>
                            <td>010-2222-2222</td>
                            <td>홍길동 </td>
                            <td>
                                <a href="edit-customer.html" class="btn-edit">수정</a>
                            </td>
                        </tr> -->
                       
                    </tbody>
                </table>
                <div class="table-pagination text-center">
                    <ul class="pagination">
                        <!-- <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="curr-page"><a href="#">1</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li> -->
                        <html:paging uri="/baseinfo/customer.do" frontYn="N" />
                    </ul>
                </div>
                <div class="confirmation">
                    <!-- <div class="confirm">
                        <a href="/baseinfo/add-customer.do">등록하기</a>
                    </div> -->
                </div>
                <!-- <div class="table-pagination text-center">
                    <ul class="pagination">
                        <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="curr-page"><a href="#">1</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
                    </ul>
                </div> -->
            </section>
        
