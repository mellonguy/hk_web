<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%

%>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script type="text/javascript">
$(document).ready(function(){

});

function updateCustomer(){

	$("#customerName").attr("disabled",false);

    if($("#customerName").val() ==""){
		alert("회사명이 입력되지 않았습니다.");
		return false;
	}
    if($("#customerOwnerName").val() ==""){
		alert("대표자명이 입력되지 않았습니다.");
		return false;
	}
    if($("#customerKind").val() ==""){
		alert("회사구분이 입력되지 않았습니다.");
		return false;
	}
    
    if($("#businessLicenseNumber").val() ==""){
		alert("사업자번호가 입력되지 않았습니다.");
		return false;
	}

    if($("#paymentKind").val() ==""){
		alert("결제방법이 입력되지 않았습니다.");
		return false;
	}

    if($("#billingKind").val() ==""){
		alert("증빙구분이 입력되지 않았습니다.");
		return false;
	}
    	
    insertCharge();
    insertDriver();
    insertPaymentCharge();


	if(confirm("수정 하시겠습니까?")){
		$("#insertForm").attr("action","/baseinfo/update-customer.do");
		$("#insertForm").submit();	
	}
	
}




function jusoSearch(where,obj){
	
	new daum.Postcode({
	    oncomplete: function(data) {
	        // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

            // 각 주소의 노출 규칙에 따라 주소를 조합한다.
            // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
            var fullAddr = ''; // 최종 주소 변수
            var extraAddr = ''; // 조합형 주소 변수

            // 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
            if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                fullAddr = data.roadAddress;

            } else { // 사용자가 지번 주소를 선택했을 경우(J)
                fullAddr = data.jibunAddress;
            }

            // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
            if(data.userSelectedType === 'R'){
                //법정동명이 있을 경우 추가한다.
                if(data.bname !== ''){
                    extraAddr += data.bname;
                }
                // 건물명이 있을 경우 추가한다.
                if(data.buildingName !== ''){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
                fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
            }

            // 우편번호와 주소 정보를 해당 필드에 넣는다.
            //document.getElementById('sample6_postcode').value = data.zonecode; //5자리 새우편번호 사용
         	
            //$(obj).parent().children().eq(0).val(fullAddr);
            //$(obj).parent().parent().children().eq(0).children().val(data.sido);

            //alert(JSON.stringify(data));
            
            $(obj).val(fullAddr);
                        
            // 커서를 상세주소 필드로 이동한다.
            //document.getElementById('sample6_address2').focus();
        }
	    
	}).open();	
	
}

function insertCharge(){
	
	var chargeList= new Array();
	var size = $("#addLocation").find("tr").length;
	var result = "";
	 $("#addLocation").find("tr").each(function(index,element){
		 if($(this).children().eq(1).children().val() != "" || $(this).children().eq(2).children().val() != "" || $(this).children().eq(3).children().val() != "" || $(this).children().eq(4).children().val() != ""){
			 var chargeInfo= new Object();
			 chargeInfo.person_in_charge_id = $(this).children().eq(1).attr("id");
			 chargeInfo.department = $(this).children().eq(1).children().val();
			 chargeInfo.name = $(this).children().eq(2).children().val();
			 chargeInfo.phoneNum = $(this).children().eq(3).children().val();
			 chargeInfo.address = $(this).children().eq(4).children().val();
			 chargeInfo.email = $(this).children().eq(5).children().val();
			 chargeInfo.etc = $(this).children().eq(6).children().val();
			 chargeInfo.black_list_yn = $(this).children().eq(7).children().val();
			 chargeList.push(chargeInfo);	 
		 }
      }); 
	
	 if(chargeList.length >= 1){
		 $("#personInChargeInfo").val(JSON.stringify({chargeList : chargeList}));
	 }
	
}


function insertPaymentCharge(){
	
	var paymentChargeList= new Array();
	var size = $("#paymentAddLocation").find("tr").length;
	var result = "";
	 $("#paymentAddLocation").find("tr").each(function(index,element){
		 if($(this).children().eq(1).children().val() != "" || $(this).children().eq(2).children().val() != "" || $(this).children().eq(3).children().val() != "" || $(this).children().eq(4).children().val() != ""){
			 var paymentPersonInChargeInfo= new Object();
			 paymentPersonInChargeInfo.payment_person_in_charge_id = $(this).children().eq(1).attr("id");
			 paymentPersonInChargeInfo.payment_department = $(this).children().eq(1).children().val();
			 paymentPersonInChargeInfo.payment_name = $(this).children().eq(2).children().val();
			 paymentPersonInChargeInfo.payment_phone_num = $(this).children().eq(3).children().val();
			 paymentPersonInChargeInfo.address = $(this).children().eq(4).children().val();
			 paymentPersonInChargeInfo.email = $(this).children().eq(5).children().val();
			 paymentPersonInChargeInfo.etc = $(this).children().eq(6).children().val();
			 paymentChargeList.push(paymentPersonInChargeInfo);	 
		 }
      }); 
	
	 if(paymentChargeList.length >= 1){
		 $("#paymentPersonInChargeInfo").val(JSON.stringify({paymentChargeList : paymentChargeList}));
	 }
	
}




function insertDriver(){
	
	var driverList= new Array();
	var size = $("#driverAddLocation").find("tr").length;
	var result = "";
	 $("#driverAddLocation").find("tr").each(function(index,element){
		 if($(this).children().eq(1).children().val() != "" || $(this).children().eq(2).children().val() != "" || $(this).children().eq(3).children().val() != "" || $(this).children().eq(4).children().val() != ""){
			 var driverInfo= new Object();
			 //alert($(this).children().eq(1).attr("id"));
			 driverInfo.driver_id = $(this).children().eq(1).attr("id");
			 driverInfo.driver_name = $(this).children().eq(1).children().val();
			 driverInfo.phone_num = $(this).children().eq(2).children().val();
			 driverInfo.car_kind = $(this).children().eq(3).children().val();
			 driverInfo.car_num = $(this).children().eq(4).children().val();
			 driverList.push(driverInfo);	 
		 }
      }); 
	
	 if(driverList.length >= 1){
		 $("#driverInfo").val(JSON.stringify({driverList : driverList}));
	 }
	
}

function addDriver(){
	
	var result = "";
	result += '<tr><td style="display:none;"></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="기사명" name=""></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="연락처" name=""></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="차종" name=""></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="차량번호" name=""></td>';
	result += '<td class="widthAuto" style=""><input style="display:inline; float:right; border:2px solid #eee;" type="button" onclick="javascript:deleteDriver(this);" value="삭제" class="btn-normal"></td></tr>';
	$("#driverRowspanCnt").attr("rowspan",Number($("#driverRowspanCnt").attr("rowspan"))+1);
	$("#driverAddLocation").append(result);
	
}

function deleteDriver(obj){
	
	$("#driverRowspanCnt").attr("rowspan",Number($("#driverRowspanCnt").attr("rowspan"))-1);
	$(obj).parent().parent().remove();
	
 }


function addPersonInCharge(){
	
	var result = "";
	result += '<tr><td style="display:none;"></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="딤당자부서" name=""></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="담당자명" name=""></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="연락처" name=""></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="주소" name=""></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="이메일" name=""></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="비고" name=""></td>';
	result += '<td class="widthAuto" style=""><select class="dropdown" name="personInchargeBlackListYn" id="personInchargeBlackListYn"><option value=""  selected="selected">블랙리스트여부를 선택 하세요.</option><option value="Y"  selected="selected">블랙리스트</option><option value="N"  selected="selected">일반</option></select></td>';
	result += '<td class="widthAuto" style=""><input style="display:inline; float:right; border:2px solid #eee;" type="button" onclick="javascript:deletePersonInCharge(this);" value="삭제" class="btn-normal"></td></tr>';

	$("#rowspanCnt").attr("rowspan",Number($("#rowspanCnt").attr("rowspan"))+1);
	$("#addLocation").append(result);

}

function addPaymentPersonInCharge(){
	
	var result = "";
	result += '<tr><td style="display:none;"></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="회계담당자(거래처)" name=""></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="담당자명" name=""></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="연락처" name=""></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="주소" name=""></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="이메일" name=""></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="비고" name=""></td>';
	result += '<td class="widthAuto" style=""><input style="display:inline; float:right; border:2px solid #eee;" type="button" onclick="javascript:deletePaymentPersonInCharge(this);" value="삭제" class="btn-normal"></td></tr>';
	$("#paymentRowspanCnt").attr("rowspan",Number($("#paymentRowspanCnt").attr("rowspan"))+1);
	$("#paymentAddLocation").append(result);

}


function deletePersonInCharge(obj){
	
	$("#rowspanCnt").attr("rowspan",Number($("#rowspanCnt").attr("rowspan"))-1);
	$(obj).parent().parent().remove();
	
}



function deletePaymentPersonInCharge(obj){
	
	$("#rowspanCnt").attr("rowspan",Number($("#rowspanCnt").attr("rowspan"))-1);
	$(obj).parent().parent().remove();
	
}



function fileChange(){
	
	var file = document.getElementById("upload");
 	var result = "";
 	for(var i = 0; i < file.files.length; i++){
		var name = file.files[i].name;
		result += name + "\t";
	}
	
	//$("#file_info").val(result);
	$("#forFile").css("display","none");
	$("#upload").css("display","inline");
	
	

}

function fileModConfirm(cnt){
	
	if(confirm("현재"+cnt+" 개의 파일이 첨부되어 있습니다.\r\n파일 수정시 기존의 파일이 삭제 됩니다. \r\n정말 수정 하시겠습니까?")){
		$("#upload").trigger("click");
	}
	
}




</script>


	
<section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">HOME</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">거래처 정보 수정</a></li>
                </ul>
            </div>

            <!-- <div class="up-dl clearfix header-search">
                 <table>
                    <tbody>
                        <tr>
                            <td style="width: 145px;">
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">선택바
                                    <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">HTML</a></li>
                                        <li><a href="#">CSS</a></li>
                                        <li><a href="#">JavaScript</a></li>
                                    </ul>
                                </div>
                            </td>
                            <td style="width: 190px;">
                                <form action="">
                                    <input type="text" class="search-box">
                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
                                    <div class="search-result">
                                        No result
                                        <div class="no-result d-table">
                                            <div class="d-tbc">
                                                <i class="fa fa-exclamation-triangle fa-3x" aria-hidden="true"></i>
                                                <span>No results have been found.</span>
                                            </div>
                                        </div>

                                        Result type1
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Campaigns</p>
                                            <a href="" class="result-sub"><span>네오퓨쳐</span></a>
                                            <p class="camp-type"><span>Campaign type:</span> <span>Powerlink</span></p>
                                            <p class="camp-id"><span>Campaign ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                        Result type2
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Ad groups</p>
                                            <a href="" class="result-sub"><span>네오퓨쳐</span>_#0007</a>
                                            <p class="camp-type"><span>Campaign type:</span> <span>Powerlink</span></p>
                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
                                            <p class="adgroup-id"><span>Ad group ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                        Result type3
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Ads - Powerlink</p>
                                            <div class="result-box">
                                                <p class="title">네오퓨쳐</p>
                                                <p class="content">일반 홈페이지에서 신개념 융복합 멀티미디어 홈피이지 제작, 견적상담 환영</p>

                                                <ul>
                                                    <li>View URL: http://www.neofss.com</li>
                                                    <li>Linked URL: http://www.neofss.com</li>
                                                </ul>
                                            </div>
                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
                                            <p class="adgroup"><span>Ad group:</span> <span class="red">네오퓨쳐</span>_#0003</p>
                                            <p class="adgroup-id"><span>Ad ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                        Result type4
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Ads - PowerContents</p>
                                            <div class="result-box w-img">
                                                <div class="d-table">
                                                    <div class="d-tbc">
                                                        <img src="/img/ads-img.png" alt="">
                                                    </div>
                                                    <div class="d-tbc">
                                                        <p class="title">홈페이지제작 이러면 망한다!</p>
                                                        <p class="content">홈페이지제작! 대충 업체에 맡기면 된다는 생각으로 접근하면, 십중팔구는 실패하는 이유와 홈페이지 제작에 관한 최소한의 지식은 가지고 업체를 선정해야 돈과 시간을 낭비하는 일을 줄일수 있다.</p>
                                                    </div>
                                                </div>
                                                <ul>
                                                    <li>Content created: 2017-12-15</li>
                                                    <li>Brand Name: 네오퓨쳐</li>
                                                    <li>View URL: http://blog.naver.com/marujeen</li>
                                                    <li>Linked URL: http://blog.naver.com/marujeen</li>
                                                </ul>
                                            </div>
                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
                                            <p class="adgroup"><span>Ad group:</span> <span class="red">네오퓨쳐</span>_#0003</p>
                                            <p class="adgroup-id"><span>Ad ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                    </div>
                                </form>
                            </td>
                            <td>
                                <input type="button" id="btn-search" value="검색" class="btn-primary">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div> -->
<!-- 
            <div class="dispatch-btn-container">
               
                <div class="dispatch-btn">
                    <i class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div>
            </div> -->
        </section>

        <div class="dispatch-wrapper">

            <section class="dispatch-bottom-content active" style=" margin:auto;">
               <form id="insertForm" action="/baseinfo/insert-customer.do" method="post" enctype="multipart/form-data">
                   <h3 style="font-size: 20px; text-align: center; margin-bottom:20px; font-weight: 700; font-family: NotoSansBold,sans-serif;">거래처정보 수정</h3>
                   <input type="hidden" name="customerId" id="customerId" value="${customerMap.customer_id}">
                   <input type="hidden" name="personInChargeInfo" id="personInChargeInfo">
                   <input type="hidden" name="paymentPersonInChargeInfo" id="paymentPersonInChargeInfo">
                   <input type="hidden" name="driverInfo" id="driverInfo">
                <div class="form-con clearfix" style="max-width:1200px;">    
                   
                <div class="column" style="margin-bottom:5px">   
                <table>
                    <tbody>
                        <tr>
                            <td>회사명<span style="color:#F00;">&nbsp;*</span></td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="회사명" name="customerName" id="customerName" <c:if test="${user.emp_id != 'hk0000' && user.emp_id != 'hk0001' && user.emp_id != 'hk0009' && user.emp_id != 'hk0028' && user.emp_id != 'hk0019'&& user.emp_id != 'hk0003' }">disabled</c:if>  value="${customerMap.customer_name}">
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <table>
                    <tbody>
                        <tr>
                            <td>대표자명<span style="color:#F00;">&nbsp;*</span></td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="대표자명" name="customerOwnerName" id="customerOwnerName" value="${customerMap.customer_owner_name}">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>회사구분<span style="color:#F00;">&nbsp;*</span></td>
                            <td class="widthAuto" style="">
                                <!-- <input type="text" placeholder="약칭" name="carNickname" id="carNickname"> -->
                                <div class="select-con">
							        <select class="dropdown" name="customerKind" id="customerKind">
							        
							        
							        	<option value="" <c:if test='${customerMap.customer_kind eq "" }'> selected="selected"</c:if>>회사구분을 선택 하세요.</option>
							            <option value="00" <c:if test='${customerMap.customer_kind eq "00" }'> selected="selected"</c:if>>법인</option>
							            <option value="01" <c:if test='${customerMap.customer_kind eq "01" }'> selected="selected"</c:if>>개인</option>
							            <option value="02" <c:if test='${customerMap.customer_kind eq "02" }'> selected="selected"</c:if>>외국인</option>
							            <option value="03" <c:if test='${customerMap.customer_kind eq "03" }'> selected="selected"</c:if>>개인(주민번호)</option>
							        </select>
							        <span></span>
							    </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <table>
                    <tbody>
                        <tr>
                            <td>사업자번호<span style="color:#F00;">&nbsp;*</span></td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="사업자번호" name="businessLicenseNumber" id="businessLicenseNumber" value="${customerMap.business_license_number}">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>법인등록번호</td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="법인등록번호" name="corporationRegistrationNumber" id="corporationRegistrationNumber" value="${customerMap.corporation_registration_number}">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>주소</td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="주소" name="address" id="address"  onclick="jusoSearch('departure',this);" value="${customerMap.address}">
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <table>
                    <tbody>
                        <tr>
                            <td>상세주소</td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="상세주소" name="addressDetail" id="addressDetail" value="${customerMap.address_detail}">
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <table>
                    <tbody>
                        <tr>
                            <td>업태</td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="업태" name="businessCondition" id="businessCondition" value="${customerMap.business_condition}">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>종목</td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="종목" name="businessKind" id="businessKind" value="${customerMap.business_kind}">
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                
                <table>
                    <tbody>
                        <tr>
                            <td>대표전화</td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="대표전화" name="phone" id="phone" value="${customerMap.phone}">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>팩스</td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="팩스" name="faxNum" id="faxNum" value="${customerMap.fax_num}">
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                
                
                <table>
                    <tbody>
                        <tr>
                            <td>결제방법</td>
                            <td class="widthAuto" style="">
                                <div class="select-con">
							        <select class="dropdown" name="paymentKind" id="paymentKind">
							        	<option value="">결제방법을 선택 하세요.</option>
							        	<c:forEach var="data" items="${paymentDivisionList}" varStatus="status">
							        		<option  value="${data.payment_division_cd}" <c:if test='${customerMap.payment_kind eq data.payment_division_cd}'> selected="selected"</c:if>>${data.payment_division}</option>
							        	</c:forEach>
							        </select>
							        <span></span>
							    </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                
                
                <table>
                    <tbody>
                        <tr>
                            <td>증빙구분</td>
                            <td class="widthAuto" style="">
                                <div class="select-con">
							        <select class="dropdown" name="billingKind" id="billingKind">
							        	<option value="">증빙구분을 선택 하세요.</option>
							        	<c:forEach var="data" items="${billingKindList}" varStatus="status">
							        		<c:if test='${data.billing_division_id ne "01"}'>
							        		<option  value="${data.billing_division_id}" <c:if test='${customerMap.billing_kind eq data.billing_division_id}'> selected="selected"</c:if>>${data.billing_division}</option>
							        		</c:if>
							        	</c:forEach>
							        </select>
							        <span></span>
							    </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>인수증 자동발송</td>
                            <td class="widthAuto" style="">
                                <div class="select-con">
                             <select class="dropdown" name="autoSendYn" id="autoSendYn">
                                <option value="" <c:if test='${customerMap.auto_send_yn eq "" }'> selected="selected"</c:if>>발송여부를 선택 하세요.</option>
                                 <option value="Y" <c:if test='${customerMap.auto_send_yn eq "Y" }'> selected="selected"</c:if>>발송</option>
                                 <option value="N" <c:if test='${customerMap.auto_send_yn eq "N" }'> selected="selected"</c:if>>미발송</option>
                             </select>
                             <span></span>
                         </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                
                  <table>
                    <tbody>
                        <tr>
                            <td>거래처가 단가 입력 가능</td>
                            <td class="widthAuto" style="">
                                <div class="select-con">
                             <select class="dropdown" name="controlPayment" id="controlPayment">
                                <option value="" <c:if test='${customerMap.control_payment eq "" }'> selected="selected"</c:if>>선택 하세요.</option>
                                 <option value="Y" <c:if test='${customerMap.control_payment eq "Y" }'> selected="selected"</c:if>>활성화됨</option>
                                 <option value="N" <c:if test='${customerMap.control_payment eq "N" }'> selected="selected"</c:if>>비활성화됨</option>
                             </select>
                             <span></span>
                         </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                
                
                </div>
                
                
                <div class="column" style="margin-bottom:5px">
                
                <table>
                    <tbody>
                        <tr>
                            <td>매출매입구분</td>
                            <td class="widthAuto" style="">
                                <div class="select-con">
							        <select class="dropdown" name="salesPurchaseDivision" id="salesPurchaseDivision">
							        	<c:forEach var="data" items="${salesPurchaseDivisionList}" varStatus="status">
							        		<option  value="${data.sales_purchase_division_cd}" <c:if test='${customerMap.sales_purchase_division eq data.sales_purchase_division_cd}'> selected="selected"</c:if>>${data.sales_purchase_division}</option>
							        	</c:forEach>
							        	
							        	
							            <%-- <option value="00"  <c:if test='${customerMap.sales_purchase_division eq "00" }'> selected="selected"</c:if>>매출</option>
							            <option value="01"  <c:if test='${customerMap.sales_purchase_division eq "01" }'> selected="selected"</c:if>>매입</option>
							            <option value="02"  <c:if test='${customerMap.sales_purchase_division eq "02" }'> selected="selected"</c:if>>기타</option> --%>
							        </select>
							        <span></span>
							    </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                
                <table>
                    <tbody>
                        <tr>
                            <td>세금계산서</td>
                            <td class="widthAuto" style="">
                                <!-- <input type="text" placeholder="약칭" name="carNickname" id="carNickname"> -->
                                <div class="select-con">
							        <select class="dropdown" name="billingDivision" id="billingDivision">
							        	<c:forEach var="data" items="${billingDivisionList}" varStatus="status">
							        		<c:if test='${data.billing_division_id eq "00" || data.billing_division_id eq "01"}'>
							        			<option  value="${data.billing_division_id}" <c:if test='${customerMap.billing_division eq data.billing_division_id}'> selected="selected"</c:if>>${data.billing_division}</option>
							        		</c:if>
							        	</c:forEach>
							        	<%-- <option value=""  <c:if test='${customerMap.billing_division eq "" }'> selected="selected"</c:if>>세금계산서구분을 선택 하세요.</option>
							            <option value="00" <c:if test='${customerMap.billing_division eq "00" }'> selected="selected"</c:if>>미발행</option>
							            <option value="01" <c:if test='${customerMap.billing_division eq "01" }'> selected="selected"</c:if>>건별</option>
							            <option value="02" <c:if test='${customerMap.billing_division eq "02" }'> selected="selected"</c:if>>일괄</option>
							            <option value="03" <c:if test='${customerMap.billing_division eq "03" }'> selected="selected"</c:if>>카드</option>
							            <option value="04" <c:if test='${customerMap.billing_division eq "04" }'> selected="selected"</c:if>>현금영수증</option> --%>
							        </select>
							        <span></span>
							    </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <table>
                    <tbody>
                        <tr>
                            <td>세금계산서이메일</td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="세금계산서이메일" name="billingEmail" id="billingEmail" value="${customerMap.billing_email}">
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <table>
                    <tbody>
                        <tr>
                            <td>은행명</td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="은행명" name="bankName" id="bankName" value="${customerMap.bank_name}">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>예금주</td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="예금주" name="depositor" id="depositor" value="${customerMap.depositor}">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>계좌번호</td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="계좌번호" name="accountNumber" id="accountNumber" value="${customerMap.account_number}">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>결제일</td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="결제일" name="paymentDt" id="paymentDt" value="${customerMap.payment_dt}">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <%-- <table>
                    <tbody>
                        <tr>
                            <td>회계담당자</td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="회계담당자" name="accounter" id="accounter" value="${customerMap.accounter}">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>회계담당자연락처</td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="회계담당자" name="phoneNum" id="phoneNum" value="${customerMap.phone_num}">
                            </td>
                        </tr>
                    </tbody>
                </table> --%>
                
                <table>
                    <tbody>
                        <tr>
                            <td>특이사항</td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="특이사항" name="significantData" id="significantData" value="${customerMap.significant_data}">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>첨부파일</td>
                            <td class="widthAuto" style="">
								<c:if test='${fn:length(customerMap.fileList) > 0 }'> 
                                     	<input  style="display:none" type="file" id="upload"  name="bbsFile" multiple value=""  onchange="javascript:fileChange();"> 
                                     	<%-- <input style="display:inline; width:82%;" id="file_info" type="text" placeholder="" readonly value="${fn:length(empMap.fileList)}개의 파일이 첨부 되어 있습니다."> --%>
                                     	<div id="forFile"  style="display:inline;">
                                      	<c:forEach var="fileData" items="${customerMap.fileList}" varStatus="status">
				                            <a target="_blank" href="/files/customer${fileData.file_path }">
				                            	<img style="width:30px; height:25px; margin-left:10px;" src="/files/customer${fileData.file_path }" alt="${fileData.file_nm}" title="${fileData.file_nm}" />
											</a>
										</c:forEach>
                                     		<input style="display:inline; float:right;" type="button" id="file_mod" onclick="javascript:fileModConfirm('${fn:length(customerMap.fileList)}');" value="수정" class="btn-primary">
                                     	</div>
                                     </c:if>
                                     <c:if test='${fn:length(customerMap.fileList) == 0 }'> 
                                     	<input  style="display:inline; width:400px;" type="file" id="upload"  name="bbsFile" multiple value="" >
                                     </c:if> 
                                 </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>비고</td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="비고" name="etc" id="etc" value="${customerMap.etc}">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>알림톡 발송</td>
                            <td class="widthAuto" style="">
                                <div class="select-con">
							        <select class="dropdown" name="alarmTalkYn" id="alarmTalkYn">
							        	<option value="" <c:if test='${customerMap.alarm_talk_yn eq "" }'> selected="selected"</c:if>>발송여부를 선택 하세요.</option>
							            <option value="Y" <c:if test='${customerMap.alarm_talk_yn eq "Y" }'> selected="selected"</c:if>>발송</option>
							            <option value="N" <c:if test='${customerMap.alarm_talk_yn eq "N" }'> selected="selected"</c:if>>미발송</option>
							        </select>
							        <span></span>
							    </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>계좌정보 발송</td>
                            <td class="widthAuto" style="">
                                <div class="select-con">
							        <select class="dropdown" name="sendAccountInfoYn" id="sendAccountInfoYn">
							        	<option value="" <c:if test='${customerMap.send_account_info_yn eq "" }'> selected="selected"</c:if>>발송여부를 선택 하세요.</option>
							            <option value="Y" <c:if test='${customerMap.send_account_info_yn eq "Y" }'> selected="selected"</c:if>>발송</option>
							            <option value="N" <c:if test='${customerMap.send_account_info_yn eq "N" }'> selected="selected"</c:if>>미발송</option>
							        </select>
							        <span></span>
							    </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <table>
                    <tbody>
                        <tr>
                            <td>문자(SMS) 사용</td>
                            <td class="widthAuto" style="">
                                <div class="select-con">
							        <select class="dropdown" name="smsYn" id="smsYn">
							        	<option value="" <c:if test='${customerMap.sms_yn eq "" }'> selected="selected"</c:if>>발송여부를 선택 하세요.</option>
							            <option value="Y" <c:if test='${customerMap.sms_yn eq "Y" }'> selected="selected"</c:if>>발송</option>
							            <option value="N" <c:if test='${customerMap.sms_yn eq "N" }'> selected="selected"</c:if>>미발송</option>
							        </select>
							        <span></span>
							    </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <table>
                    <tbody>
                        <tr>
                            <td>블랙리스트</td>
                            <td class="widthAuto" style="">
                                <div class="select-con">
							        <select class="dropdown" name="blackListYn" id="blackListYn">
							        	<option value="" <c:if test='${customerMap.black_list_yn eq "" }'> selected="selected"</c:if>>블랙리스트여부를 선택 하세요.</option>
							            <option value="Y" <c:if test='${customerMap.black_list_yn eq "Y" }'> selected="selected"</c:if>>등록</option>
							            <option value="N" <c:if test='${customerMap.black_list_yn eq "N" }'> selected="selected"</c:if>>미등록</option>
							        </select>
							        <span></span>
							    </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                   <table>
                    <tbody>
                        <tr>
                            <td>알림톡 또는 문자 사용여부</td>
                            <td class="widthAuto" style="">
                                <div class="select-con">
							         <select class="dropdown" name="alarmOrSmsYn" id="alarmOrSmsYn">
							        	<option value="" <c:if test='${customerMap.alarm_or_sms_yn eq "" }'> selected="selected"</c:if>>카카오톡/메세지 사용여부를 선택 하세요.</option>
							            <option value="A"  <c:if test='${customerMap.alarm_or_sms_yn eq "A" }'> selected="selected"</c:if>>알림톡 사용</option>
							            <option value="M" <c:if test='${customerMap.alarm_or_sms_yn eq "M" }'> selected="selected"</c:if>>문자(SMS) 사용</option>
							        </select>
							        <span></span>
							    </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                
                
                </div>
                <table>
                           <tbody id="addLocation">
                           		<c:if test='${fn:length(personInChargeList) > 0 }'>
		                           <c:forEach var="data" items="${personInChargeList}" varStatus="status">
		                               <tr>
		                                   <c:if test='${status.index == 0 }'>
		                                    	<td rowspan="${fn:length(personInChargeList)}" id="rowspanCnt">담당자 정보</td>
		                                    </c:if>
		                                    <c:if test='${status.index > 0 }'>
		                                    	<td style="display:none;"></td>
		                                    </c:if>
		                                   <td id="${data.person_in_charge_id}" class="widthAuto" style="">
		                                   	<input type="text" placeholder="딤당자부서" name="" value="${data.department}">
		                                   </td>
		                                   <td class="widthAuto" style="">
		                               		<input type="text" placeholder="담당자명" name="" value="${data.name}">
		                                   </td>
		                                   <td class="widthAuto" style="">
		                                   	<input type="text" placeholder="연락처" name="" value="${data.phone_num}">
		                                   </td>
		                                   <td class="widthAuto" style="">
		                                   	<input type="text" placeholder="주소" name="" value="${data.address}">
		                                   </td>
		                                   <td class="widthAuto" style="">
		                                   	<input type="text" placeholder="이메일" name="" value="${data.email}">
		                                   </td>
		                                   <td class="widthAuto" style="">
		                                   	<input type="text" placeholder="비고" name="" value="${data.etc}">
		                                   </td>
		                                   <td class="" style="">
		                                   	<!--   <input type="text" placeholder="블랙리스트여부" name="" value="${data.black_list_yn}">-->
				                              <select class="" name="personInchargeBlackListYn" id="personInchargeBlackListYn" style="">
									        	<option value="" <c:if test='${data.black_list_yn eq "" }'> selected="selected"</c:if>>블랙리스트여부를 선택 하세요.</option>
									            <option value="Y" <c:if test='${data.black_list_yn eq "Y" }'> selected="selected"</c:if>>블랙리스트</option>
									            <option value="N" <c:if test='${data.black_list_yn eq "N" }'> selected="selected"</c:if>>일반</option>
									        </select>
		                                   
		                                   </td>
		                                   <td class="widthAuto" style="">
		                                    	<c:if test='${status.index == 0 }'>
		                                    		<input style="display:inline; float:right;" type="button" id="" onclick="javascript:addPersonInCharge();" value="추가" class="btn-primary">
		                                    	</c:if>
		                                    	<c:if test='${status.index > 0 }'>
		                                    		<input style="display:inline; float:right; border:2px solid #eee;" type="button" onclick="javascript:deletePersonInCharge(this);" value="삭제" class="btn-normal">
		                                    	</c:if>
		                                    </td>
		                               </tr>
	                               </c:forEach>
                               </c:if>
                               <c:if test='${fn:length(personInChargeList) == 0 }'>
	                               <tr>
	                                   <td rowspan="1" id="rowspanCnt">담당자정보</td>
	                                   <td id="" class="widthAuto" style="">
	                                   	<input type="text" placeholder="딤당자부서" name="" value="">
	                                   </td>
	                                   <td class="widthAuto" style="">
	                               		<input type="text" placeholder="담당자명" name="" value="">
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   	<input type="text" placeholder="연락처" name="" value="">
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   	<input type="text" placeholder="주소" name="" value="">
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   	<input type="text" placeholder="이메일" name="" value="">
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   	<input type="text" placeholder="비고" name="" value="">
	                                   </td>
	                                   <td class="widthAuto" style="">
		                                   	<!--   <input type="text" placeholder="블랙리스트여부" name="" value="${data.black_list_yn}">-->
				                              <select class="" name="personInchargeBlackListYn" id="personInchargeBlackListYn" style="">
									        	<option value="" <c:if test='${data.black_list_yn eq "" }'> selected="selected"</c:if>>블랙리스트여부를 선택 하세요.</option>
									            <option value="Y" <c:if test='${data.black_list_yn eq "Y" }'> selected="selected"</c:if>>블랙리스트</option>
									            <option value="N" <c:if test='${data.black_list_yn eq "N" }'> selected="selected"</c:if>>일반</option>
									        </select>
		                                 </td>
	                                   <td class="widthAuto" style="">
	                                   	<input style="display:inline; float:right;" type="button" id="" onclick="javascript:addPersonInCharge();" value="추가" class="btn-primary">
	                                   </td>
	                               </tr>
                               </c:if>
                               
                           </tbody>
                       </table>
                       
                       
                       
                          <table>
                           <tbody id="paymentAddLocation">
                           		<c:if test='${fn:length(paymentPersonInChargeList) > 0 }'>
		                           <c:forEach var="data" items="${paymentPersonInChargeList}" varStatus="status">
		                               <tr>
		                                   <c:if test='${status.index == 0 }'>
		                                    	<td rowspan="${fn:length(paymentPersonInChargeList)}" id="paymentRowspanCnt">회계 담당자정보</td>
		                                    </c:if>
		                                    <c:if test='${status.index > 0 }'>
		                                    	<td style="display:none;"></td>
		                                    </c:if>
		                                   <td id="${data.payment_person_in_charge_id}" class="widthAuto" style="">
		                                   	<input type="text" placeholder="딤당자부서" name="" value="${data.payment_department}">
		                                   </td>
		                                   <td class="widthAuto" style="">
		                               		<input type="text" placeholder="담당자명" name="" value="${data.payment_name}">
		                                   </td>
		                                   <td class="widthAuto" style="">
		                                   	<input type="text" placeholder="연락처" name="" value="${data.payment_phone_num}">
		                                   </td>
		                                   <td class="widthAuto" style="">
		                                   	<input type="text" placeholder="주소" name="" value="${data.address}">
		                                   </td>
		                                   <td class="widthAuto" style="">
		                                   	<input type="text" placeholder="이메일" name="" value="${data.email}">
		                                   </td>
		                                   <td class="widthAuto" style="">
		                                   	<input type="text" placeholder="비고" name="" value="${data.etc}">
		                                   </td>
		                                   <td class="widthAuto" style="">
		                                    	<c:if test='${status.index == 0 }'>
		                                    		<input style="display:inline; float:right;" type="button" id="" onclick="javascript:addPaymentPersonInCharge();" value="추가" class="btn-primary">
		                                    	</c:if>
		                                    	<c:if test='${status.index > 0 }'>
		                                    		<input style="display:inline; float:right; border:2px solid #eee;" type="button" onclick="javascript:deletePaymentPersonInCharge(this);" value="삭제" class="btn-normal">
		                                    	</c:if>
		                                    </td>
		                               </tr>
	                               </c:forEach>
                               </c:if>
                               <c:if test='${fn:length(paymentPersonInChargeList) == 0 }'>
	                               <tr>
	                                   <td rowspan="1" id="paymentRowspanCnt">회계 담당자정보</td>
	                                   <td id="" class="widthAuto" style="">
	                                   	<input type="text" placeholder="회계담당자(거래처)" name="" value="">
	                                   </td>
	                                   <td class="widthAuto" style="">
	                               		<input type="text" placeholder="담당자명" name="" value="">
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   	<input type="text" placeholder="연락처" name="" value="">
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   	<input type="text" placeholder="주소" name="" value="">
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   	<input type="text" placeholder="이메일" name="" value="">
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   	<input type="text" placeholder="비고" name="" value="">
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   	<input style="display:inline; float:right;" type="button" id="" onclick="javascript:addPaymentPersonInCharge();" value="추가" class="btn-primary">
	                                   </td>
	                               </tr>
                               </c:if>
                               
                           </tbody>
                       </table>
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       <table>
                           <tbody id="driverAddLocation">
                           	<c:if test='${fn:length(driverList) > 0 }'>
                           		<c:forEach var="data" items="${driverList}" varStatus="status">
                           			<tr>
	                     				<c:if test='${status.index == 0 }'>
	                                		<td rowspan="${fn:length(driverList)}" id="driverRowspanCnt">기사정보</td>
	                                	</c:if>
	                                	<c:if test='${status.index > 0 }'>
	                                		<td style="display:none;"></td>
	                                	</c:if>
	                                	<td id="${data.driver_id}" class="widthAuto" style="">
	                                   	<input type="text" placeholder="기사명" name="" value="${data.driver_name}">
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   	<input type="text" placeholder="연락처" name="" value="${data.phone_num}">
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   	<input type="text" placeholder="차종" name="" value="${data.car_kind}">
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   	<input type="text" placeholder="차량번호" name="" value="${data.car_num}">
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   		<c:if test='${status.index == 0 }'>
		                                    	<input style="display:inline; float:right;" type="button" id="file_mod" onclick="javascript:addDriver();" value="추가" class="btn-primary">
		                                    </c:if>
		                                    <c:if test='${status.index > 0 }'>
		                                    	<input style="display:inline; float:right; border:2px solid #eee;" type="button" onclick="javascript:deleteDriver(this);" value="삭제" class="btn-normal">
		                                    </c:if>
	                                   </td>
                                	</tr>
                           		</c:forEach>
                              </c:if>
                           		<c:if test='${fn:length(driverList) == 0 }'>
	                               <tr>
	                                   <td rowspan="1" id="driverRowspanCnt">기사정보</td>
	                                   <td id="" class="widthAuto" style="">
	                                   	<input type="text" placeholder="기사명" name="" value="">
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   	<input type="text" placeholder="연락처" name="" value="">
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   	<input type="text" placeholder="차종" name="" value="">
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   	<input type="text" placeholder="차량번호" name="" value="">
	                                   </td>
	                                   <td class="widthAuto" style="">
	                                   	<input style="display:inline; float:right;" type="button" id="file_mod" onclick="javascript:addDriver();" value="추가" class="btn-primary">
	                                   </td>
	                               </tr>
                               </c:if>
                           </tbody>
                       </table>
                </div>
                
                
                
                    <div class="confirmation">
                        <div class="confirm">
                            <input class="btn-primary" type="button" value="수정"  onclick="javascript:updateCustomer();">
                        </div>
                        <div class="cancel">
	                        <input type="button" value="취소" onclick="javascript:history.go(-1); return false;">
	                    </div>
                    </div>
                </form>
                <!-- <div class="table-pagination text-center">
                    <ul class="pagination">
                        <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="curr-page"><a href="#">1</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
                    </ul>
                </div> -->
            </section>
        </div>
