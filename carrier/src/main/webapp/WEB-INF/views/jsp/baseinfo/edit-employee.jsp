<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%

%>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	 /* $(".dropdown-menu li a").click( function() {
	    var yourText = $(this).text();
	    $("#support").val(yourText);
	}); */ 
});

function updateEmployee(){

	
	var file = document.getElementById("upload");
 	//alert(file.files.length);
 	for(var i = 0; i < file.files.length; i++){
		var ext = file.files[i].name.split(".").pop().toLowerCase();
		if($.inArray(ext, ["jpeg","jpg","gif","png","pdf"]) == -1){
			alert("업로드 할 수 없는 파일이 있습니다.");
			$("#upload").val("");
			return false;
		}
	}
	
	 	if($("#companyId").val() == ""){
			alert("회사가 선택 되지 않았습니다.");
			return false;
		}
 	
        if($("#empId").val() == ""){
			alert("사원번호가 입력되지 않았습니다.");
			return false;
		}	
        if($("empName").val() == ""){
			alert("성명이 입력되지 않았습니다.");
			return false;
		}	
        if($("#phonePersonal").val() == ""){
			alert("연락처 1이 입력되지 않았습니다.");
			return false;
		}else{
			if(!phoneChk($("#phonePersonal"))){
		  		alert("유효하지 않은 전화번호 입니다.");
		  		$("#phonePersonal").val("");
	            $("#phonePersonal").focus();
				return false;
			}
		}
        
        if($("#phoneWork").val() == ""){
        	alert("연락처 2가 입력되지 않았습니다. 비상시 연락 가능한 번호를 작성 하세요.");
			return false;
		}else{
			if(!phoneChk($("#phoneWork"))){
		  		alert("유효하지 않은 전화번호 입니다.");
		  		$("#phoneWork").val("");
	            $("#phoneWork").focus();
				return false;
			}
		}
        
        if($("#emailPersonal").val() == ""){
			alert("외부 이메일이 입력되지 않았습니다.");
			return false;
		}else{
			if(!mailChk($("#emailPersonal"))){
	 			alert("유효하지 않은 이메일 형식 입니다.");
	            $("#emailPersonal").val("");
	            $("#emailPersonal").focus();
	 			return false;
	 		}
		}
        
        if($("#joinDt").val() == ""){
			alert("입사일이 입력되지 않았습니다.");
			return false;
		}
       	
		if(confirm("수정 하시겠습니까?")){
			$("#insertForm").attr("action","/baseinfo/update-employee.do");
			$("#insertForm").submit();	
		}
	
}

function jusoSearch(where,obj){
	
	new daum.Postcode({
	    oncomplete: function(data) {
	        // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

            // 각 주소의 노출 규칙에 따라 주소를 조합한다.
            // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
            var fullAddr = ''; // 최종 주소 변수
            var extraAddr = ''; // 조합형 주소 변수

            // 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
            if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                fullAddr = data.roadAddress;

            } else { // 사용자가 지번 주소를 선택했을 경우(J)
                fullAddr = data.jibunAddress;
            }

            // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
            if(data.userSelectedType === 'R'){
                //법정동명이 있을 경우 추가한다.
                if(data.bname !== ''){
                    extraAddr += data.bname;
                }
                // 건물명이 있을 경우 추가한다.
                if(data.buildingName !== ''){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
                fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
            }

            // 우편번호와 주소 정보를 해당 필드에 넣는다.
            //document.getElementById('sample6_postcode').value = data.zonecode; //5자리 새우편번호 사용
         	
            //$(obj).parent().children().eq(0).val(fullAddr);
            //$(obj).parent().parent().children().eq(0).children().val(data.sido);

            //alert(JSON.stringify(data));
            
            $(obj).val(fullAddr);
                        
            // 커서를 상세주소 필드로 이동한다.
            //document.getElementById('sample6_address2').focus();
        }
	    
	}).open();	
	
}

function fileChange(){
	
	var file = document.getElementById("upload");
 	var result = "";
 	for(var i = 0; i < file.files.length; i++){
		var name = file.files[i].name;
		result += name + "\t";
	}
	
	//$("#file_info").val(result);
	$("#forFile").css("display","none");
	$("#upload").css("display","inline");
	
	

}

function fileModConfirm(cnt){
	
	if(confirm("현재"+cnt+" 개의 파일이 첨부되어 있습니다.\r\n파일 수정시 기존의 파일이 삭제 됩니다. \r\n정말 수정 하시겠습니까?")){
		$("#upload").trigger("click");
	}
	
}




</script>
		<!-- <section class="side-nav">
            <ul>
                <li class="active"><a href="/baseinfo/employee.do">직원관리</a></li>
                <li class=""><a href="/baseinfo/driver.do">기사관리</a></li>
                <li class=""><a href="/baseinfo/customer.do">거래처관리</a></li>
                <li class=""><a href="/baseinfo/company.do">회사정보관리</a></li>
            </ul>
        </section> -->

       <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">HOME</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">직원 정보 수정</a></li>
                </ul>
            </div>

            <div class="up-dl clearfix header-search">
                 <table>
                    <tbody>
                        <tr>
                            <td style="width: 145px;">
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">선택바
                                    <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">HTML</a></li>
                                        <li><a href="#">CSS</a></li>
                                        <li><a href="#">JavaScript</a></li>
                                    </ul>
                                </div>
                            </td>
                            <td style="width: 190px;">
                                <form action="">
                                    <input type="text" class="search-box">
                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
                                    <div class="search-result">
                                        <!-- No result -->
                                        <div class="no-result d-table">
                                            <div class="d-tbc">
                                                <i class="fa fa-exclamation-triangle fa-3x" aria-hidden="true"></i>
                                                <span>No results have been found.</span>
                                            </div>
                                        </div>

                                        <!-- Result type1 -->
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Campaigns</p>
                                            <a href="" class="result-sub"><span>네오퓨쳐</span></a>
                                            <p class="camp-type"><span>Campaign type:</span> <span>Powerlink</span></p>
                                            <p class="camp-id"><span>Campaign ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                        <!-- Result type2 -->
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Ad groups</p>
                                            <a href="" class="result-sub"><span>네오퓨쳐</span>_#0007</a>
                                            <p class="camp-type"><span>Campaign type:</span> <span>Powerlink</span></p>
                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
                                            <p class="adgroup-id"><span>Ad group ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                        <!-- Result type3 -->
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Ads - Powerlink</p>
                                            <div class="result-box">
                                                <p class="title">네오퓨쳐</p>
                                                <p class="content">일반 홈페이지에서 신개념 융복합 멀티미디어 홈피이지 제작, 견적상담 환영</p>

                                                <ul>
                                                    <li>View URL: http://www.neofss.com</li>
                                                    <li>Linked URL: http://www.neofss.com</li>
                                                </ul>
                                            </div>
                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
                                            <p class="adgroup"><span>Ad group:</span> <span class="red">네오퓨쳐</span>_#0003</p>
                                            <p class="adgroup-id"><span>Ad ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                        <!-- Result type4 -->
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Ads - PowerContents</p>
                                            <div class="result-box w-img">
                                                <div class="d-table">
                                                    <div class="d-tbc">
                                                        <img src="/img/ads-img.png" alt="">
                                                    </div>
                                                    <div class="d-tbc">
                                                        <p class="title">홈페이지제작 이러면 망한다!</p>
                                                        <p class="content">홈페이지제작! 대충 업체에 맡기면 된다는 생각으로 접근하면, 십중팔구는 실패하는 이유와 홈페이지 제작에 관한 최소한의 지식은 가지고 업체를 선정해야 돈과 시간을 낭비하는 일을 줄일수 있다.</p>
                                                    </div>
                                                </div>
                                                <ul>
                                                    <li>Content created: 2017-12-15</li>
                                                    <li>Brand Name: 네오퓨쳐</li>
                                                    <li>View URL: http://blog.naver.com/marujeen</li>
                                                    <li>Linked URL: http://blog.naver.com/marujeen</li>
                                                </ul>
                                            </div>
                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
                                            <p class="adgroup"><span>Ad group:</span> <span class="red">네오퓨쳐</span>_#0003</p>
                                            <p class="adgroup-id"><span>Ad ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                    </div>
                                </form>
                            </td>
                            <td>
                                <input type="button" id="btn-search" value="검색" class="btn-primary">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
<!-- 
            <div class="dispatch-btn-container">
               
                <div class="dispatch-btn">
                    <i class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div>
            </div> -->
        </section>

        <div class="dispatch-wrapper">
            

            <section class="dispatch-bottom-content active" style=" margin:auto;">
               <form id="insertForm" action="/baseinfo/update-employee.do" method="post" enctype="multipart/form-data">
                   <h3 style=" font-size: 20px; text-align: center; margin-bottom:20px; font-weight: 700; font-family: NotoSansBold,sans-serif;">직원정보 수정</h3>
               <div class="form-con clearfix" style="max-width:1200px;">
               
               	<div class="column" style="margin-bottom:5px">
	                   	<table>
	                            <tbody>
	                                <tr>
	                                    <td>회사선택<span style="color:#F00;">&nbsp;*</span></td>
	                                    <td>
	                                            <div class="select-con">
											        <select class="dropdown" name="companyId" id="companyId">
											        	<option value="">회사를 선택 하세요.</option>
											        	<c:forEach var="data" items="${companyList}" varStatus="status">
											        		<option value="${data.company_id}" <c:if test='${empMap.company_id eq data.company_id }'> selected="selected" </c:if> >${data.company_name}</option>
														</c:forEach>
											        </select>
											        <span></span>
											    </div>
	                                    </td>
	                                </tr>
	                             </tbody>
	                        </table>
	                       <table>
	                            <tbody>
	                                <tr>
	                                    <td>사원번호(아이디)<span style="color:#F00;">&nbsp;*</span></td>
	                                    <td class="widthAuto" style="">
	                                        <input type="text" placeholder="사원번호(아이디)" name="empId" id="empId"  readonly="readonly" value="${empMap.emp_id}">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>부서</td>
	                                    <td class="widthAuto" style="">
	                                        <input type="text" placeholder="부서" name="dept" id="dept" value="${empMap.dept}">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>영문부서명</td>
	                                    <td class="widthAuto" style="">
	                                        <input type="text" placeholder="영문부서명" name="deptEngName" id="deptEngName" value="${empMap.dept_eng_name}">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>성명<span style="color:#F00;">&nbsp;*</span></td>
	                                    <td class="widthAuto" style="">
	                                        <input type="text" placeholder="성명" name="empName" id="empName" value="${empMap.emp_name}">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>영문성명</td>
	                                    <td class="widthAuto" style="">
	                                        <input type="text" placeholder="영문성명" name="empEngName" id="empEngName" value="${empMap.emp_eng_name}">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        <%-- <table>
	                            <tbody>
	                                <tr>
	                                    <td>배차담당자<span style="color:#F00;">&nbsp;*</span></td>
	                                    <td class="widthAuto" style="">
	                                        <input type="radio" id="allocationS" name="allocation" value="S" <c:if test="${empMap.allocation eq 'S'}">checked="checked"</c:if>>
									    	<label for="allocationS">셀프</label>
									    	<input type="radio" id="allocationC" name="allocation" value="C" <c:if test="${empMap.allocation eq 'C'}">checked="checked"</c:if>>
									    	<label for="allocationC">캐리어</label>
									    	<input type="radio" id="allocationN" name="allocation" value="N" <c:if test="${empMap.allocation eq 'N'}">checked="checked"</c:if>>
									    	<label for="allocationN">아님</label>
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table> --%>
	                        
	                        <%-- <c:if test="${userSessionMap.emp_role == 'A'}">
		                        <table>
		                            <tbody>
		                                <tr>
		                                    <td>권한</td>
		                                    <td class="widthAuto" style="">
		                                        <!-- <input type="text" placeholder="권한" name="empPosition" id="empPosition"> -->
		                                        <input type="radio" id="contactChoice1" name="empRole" value="A" <c:if test="${empMap.emp_role eq 'A'}">checked="checked"</c:if>>
										    	<label for="contactChoice1">관리자</label>
										    	<input type="radio" id="contactChoice2" name="empRole" value="U" <c:if test="${empMap.emp_role eq 'U'}">checked="checked"</c:if>>
										    	<label for="contactChoice2">사용자</label>
										    	<input type="radio" id="contactChoice3" name="empRole" value="E" <c:if test="${empMap.emp_role eq 'E'}">checked="checked"</c:if>>
										    	<label for="contactChoice3">기타</label>
		                                    </td>
		                                </tr>
		                            </tbody>
		                        </table>
	                        </c:if> --%>
	                        <%-- <c:if test="${userSessionMap.emp_role != 'A'}"> --%>
		                    	<input type="hidden" id="contactChoice1" name="empRole" value="${empMap.emp_role}">
	                        <%-- </c:if> --%>
	                        
	                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>생일</td>
	                                    <td class="widthAuto" style="">
	                                        <input class="datepick" type="text" placeholder="생일"  readonly="readonly" name="birth" id="birth"  value="${empMap.birth}">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>양력/음력</td>
	                                    <td class="widthAuto" style="">
	                                        <input type="radio" id="solarLunarS" name="solarLunar" value="S" <c:if test="${empMap.solar_lunar eq 'S'}">checked="checked"</c:if>>
									    	<label for="solarLunarS">양력</label>
									    	<input type="radio" id="solarLunarL" name="solarLunar" value="L" <c:if test="${empMap.solar_lunar eq 'L'}">checked="checked"</c:if>>
									    	<label for="solarLunarL">음력</label>
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        
	                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>기혼/미혼</td>
	                                    <td class="widthAuto" style="">
	                                        <input type="radio" id="marriageYnN" name="marriageYn" value="N" <c:if test="${empMap.marriage_yn eq 'N'}">checked="checked"</c:if>>
									    	<label for="marriageYnN">미혼</label>
									    	<input type="radio" id="marriageYnY" name="marriageYn" value="Y" <c:if test="${empMap.marriage_yn eq 'Y'}">checked="checked"</c:if>>
									    	<label for="marriageYnY">기혼</label>
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>결혼기념일</td>
	                                    <td class="widthAuto" style="">
	                                        <input class="datepick" type="text" placeholder="결혼기념일"  readonly="readonly" name="weddingAnniversary" id="weddingAnniversary" value="${empMap.wedding_anniversary}">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        
	                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>연락처 1<span style="color:#F00;">&nbsp;*</span></td>
	                                    <td class="widthAuto" style="">
	                                        <input type="text" placeholder="연락처 1" name="phonePersonal" id="phonePersonal" value="${empMap.phone_personal}">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>연락처 2<span style="color:#F00;">&nbsp;*</span></td>
	                                    <td class="widthAuto" style="">
	                                        <input type="text" placeholder="연락처 2" name="phoneWork" id="phoneWork" value="${empMap.phone_work}">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>연락처 3</td>
	                                    <td class="widthAuto" style="">
	                                        <input type="text" placeholder="연락처 3" name="phoneWorkSub" id="phoneWorkSub" value="${empMap.phone_work_sub}">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>외부 이메일<span style="color:#F00;">&nbsp;*</span></td>
	                                    <td class="widthAuto" style="">
	                                        <input type="text" placeholder="외부 이메일" name="emailPersonal" id="emailPersonal" value="${empMap.email_personal}">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>회사 이메일</td>
	                                    <td class="widthAuto" style="">
	                                        <input type="text" placeholder="업무용 휴대폰" name="emailWork" id="emailWork" value="${empMap.email_work}">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        
                        </div>
               
               
               			<div class="column" style="margin-bottom:5px">
                        <%-- <table>
	                            <tbody>
	                                <tr>
	                                    <td>재직상태</td>
	                                    <td class="widthAuto" style="">
	                                        <input type="radio" id="empStatus01" name="empStatus" value="01"  <c:if test="${empMap.emp_status eq '01'}">checked="checked"</c:if>>
									    	<label for="empStatus01">재직</label>
									    	<input type="radio" id="empStatus02" name="empStatus" value="02" <c:if test="${empMap.emp_status eq '02'}">checked="checked"</c:if>>
									    	<label for="empStatus02">휴직</label>
									    	<input type="radio" id="empStatus03" name="empStatus" value="03" <c:if test="${empMap.emp_status eq '03'}">checked="checked"</c:if>>
									    	<label for="empStatus03">퇴사</label>
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table> --%>
                        <table>
                            <tbody>
                                <tr>
                                    <td>직급</td>
                                    <td class="widthAuto" style="">
                                        <input type="text" placeholder="직급" name="empPosition" id="empPosition" value="${empMap.emp_position}">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
                            <tbody>
                                <tr>
                                    <td>영문직급명</td>
                                    <td class="widthAuto" style="">
                                        <input type="text" placeholder="영문직급명" name="empEngPosition" id="empEngPosition" value="${empMap.emp_eng_position}">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
                            <tbody>
                                <tr>
                                    <td>직책</td>
                                    <td class="widthAuto" style="">
                                        <input type="text" placeholder="직책" name="empGrade" id="empGrade" value="${empMap.emp_grade}">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>성별</td>
	                                    <td class="widthAuto" style="">
	                                        <input type="radio" id="empSexM" name="empSex" value="M" <c:if test="${empMap.emp_sex eq 'M'}">checked="checked"</c:if>>
									    	<label for="empSexM">남성</label>
									    	<input type="radio" id="empSexF" name="empSex" value="F" <c:if test="${empMap.emp_sex eq 'F'}">checked="checked"</c:if>>
									    	<label for="empSexF">여성</label>
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>주소</td>
	                                    <td class="widthAuto" style="">
	                                        <input type="text" placeholder="주소" name="address" id="address" onclick="jusoSearch('departure',this);"  value="${empMap.address}">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>상세주소</td>
	                                    <td class="widthAuto" style="">
	                                        <input type="text" placeholder="상세주소" name="addressDetail" id="addressDetail" value="${empMap.address_detail}">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
                        <table>
                            <tbody>
                                <tr>
                                    <td>수습여부</td>
                                    <td class="widthAuto" style="">
                                        <input type="radio" id="probationYn" name="probationYn" value="Y" <c:if test="${empMap.probation_yn eq 'Y'}">checked="checked"</c:if>>
								    	<label for="probationYnY">수습중</label>
								    	<input type="radio" id="probationYn" name="probationYn" value="N" <c:if test="${empMap.probation_yn eq 'N'}">checked="checked"</c:if>>
								    	<label for="probationYnN">수습아님</label>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
		                    <tbody>
		                        <tr>
		                            <td>적용율</td>
		                            <td class="widthAuto" style="">
		                                <input type="text" placeholder="적용율(숫자만입력)" name="deductionRate" id="deductionRate" value="${empMap.deduction_rate}">
		                            </td>
		                        </tr>
		                    </tbody>
		                </table>
                        <table>
                            <tbody>
                                <tr>
                                    <td>수습기간 시작일</td>
                                    <td class="widthAuto" style="">
                                        <input class="datepick" type="text" placeholder="수습기간 시작일"  readonly="readonly" name="probationStartDt" id="probationStartDt"  value="${empMap.probation_start_dt}">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
                            <tbody>
                                <tr>
                                    <td>수습기간 종료일</td>
                                    <td class="widthAuto" style="">
                                        <input class="datepick" type="text" placeholder="수습기간 종료일" readonly="readonly" name="probationEndDt" id="probationEndDt" value="${empMap.probation_end_dt}">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        
                        <table>
                            <tbody>
                                <tr>
                                    <td>입사일<span style="color:#F00;">&nbsp;*</span></td>
                                    <td class="widthAuto" style="">
                                        <input class="datepick" type="text" placeholder="입사일"  readonly="readonly" name="joinDt" id="joinDt" value="${empMap.join_dt}">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
                            <tbody>
                                <tr>
                                    <td>퇴사일</td>
                                    <td class="widthAuto" style="">
                                        <input class="datepick" type="text" placeholder="퇴사일" readonly="readonly" name="resignDt" id="resignDt" value="${empMap.resign_dt}">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <%-- <table>
                            <tbody>
                                <tr>
                                    <td>연봉</td>
                                    <td class="widthAuto" style="">
                                        <input type="text" placeholder="연봉" name="salary" id="salary" value="${empMap.salary}">
                                    </td>
                                </tr>
                            </tbody>
                        </table> --%>
                           <table>
                            <tbody>
                                <tr>
                                    <td>근무지원</td>
                                    <td class="widthAuto" style="">
	                                        <input type="text" placeholder="근무지원" name="support" id="support" value="${empMap.support}">
	                                    </td>
                                    <!-- <td style="width: 145px;">
                                        <div class="dropdown">
                                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">사업자
                                            <span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">HTML</a></li>
                                                <li><a href="#">CSS</a></li>
                                                <li><a href="#">JavaScript</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                    <td></td> -->
                                </tr>
                             </tbody>
                            </table>
                            
                            <table>
	                            <tbody>
	                                <tr>
	                                    <td>첨부파일</td>
	                                    <td class="widthAuto" style="">
											<c:if test='${fn:length(empMap.fileList) > 0 }'> 
	                                        	<input  style="display:none" type="file" id="upload"  name="bbsFile" multiple value=""  onchange="javascript:fileChange();"> 
	                                        	<%-- <input style="display:inline; width:82%;" id="file_info" type="text" placeholder="" readonly value="${fn:length(empMap.fileList)}개의 파일이 첨부 되어 있습니다."> --%>
	                                        	<div id="forFile"  style="display:inline;">
		                                        	<c:forEach var="fileData" items="${empMap.fileList}" varStatus="status">
							                            <a target="_blank" href="/files/employee${fileData.file_path }">
							                            	<img style="width:30px; height:25px; margin-left:10px;" src="/files/employee${fileData.file_path }" alt="${fileData.file_nm}" title="${fileData.file_nm}" />
														</a>
													</c:forEach>
	                                        		<input style="display:inline; float:right;" type="button" id="file_mod" onclick="javascript:fileModConfirm('${fn:length(empMap.fileList)}');" value="수정" class="btn-primary">
	                                        	</div>
	                                        </c:if>
	                                        <c:if test='${fn:length(empMap.fileList) == 0 }'> 
	                                        	<input  style="display:inline; width:400px;" type="file" id="upload"  name="bbsFile" multiple value="" >
	                                        </c:if> 
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
                            
                            <table>
	                            <tbody>
	                                <tr>
	                                    <td>비고</td>
	                                    <td class="widthAuto" style="">
	                                        <input type="text" placeholder="비고" name="note" id="note" value="${empMap.note}">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        
	                      </div> 
                            
               
                       <%-- <table>
                            <tbody>
                                <tr>
                                    <td>사원번호</td>
                                    <td class="widthAuto" style="">
                                        <input type="text" placeholder="사원번호" name="empId" id="empId" readonly="readonly" value="${empMap.emp_id}">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
                            <tbody>
                                <tr>
                                    <td>성명</td>
                                    <td class="widthAuto" style="">
                                        <input type="text" placeholder="성명" name="empName" id="empName" value="${empMap.emp_name}">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
                            <tbody>
                                <tr>
                                    <td>직급</td>
                                    <td class="widthAuto" style="">
                                        <input type="text" placeholder="직급" name="empPosition" id="empPosition" value="${empMap.emp_position}">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
                            <tbody>
                                <tr>
                                    <td>입사일</td>
                                    <td class="widthAuto" style="">
                                        <input class="datepick" type="text" placeholder="입사일"  readonly="readonly" name="joinDt" id="joinDt" value="${empMap.join_dt}">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
                            <tbody>
                                <tr>
                                    <td>퇴사일</td>
                                    <td class="widthAuto" style="">
                                        <input class="datepick" type="text" placeholder="퇴사일" readonly="readonly" name="resignDt" id="resignDt" value="${empMap.resign_dt}">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
                            <tbody>
                                <tr>
                                    <td>연봉</td>
                                    <td class="widthAuto" style="">
                                        <input type="text" placeholder="연봉" name="salary" id="salary" value="${empMap.salary}">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                           <table>
                            <tbody>
                                <tr>
                                    <td>근무지원</td>
                                    <td style="width: 145px;">
                                        <div class="dropdown">
                                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">사업자
                                            <span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">HTML</a></li>
                                                <li><a href="#">CSS</a></li>
                                                <li><a href="#">JavaScript</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                             </tbody>
                            </table>  --%>
                   
               </div>
                
                    <div class="confirmation">
                        <div class="confirm">
                            <input type="button" value="수정" onclick="javascript:updateEmployee();">
                        </div>
                        <div class="cancel">
                        <input type="button" value="취소" onclick="javascript:history.go(-1); return false;">
                    </div>
                    </div>
                    <!-- <script src="js/vendor/bootstrap-datepicker.min.js"></script> -->
                </form>
                <!-- <div class="table-pagination text-center">
                    <ul class="pagination">
                        <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="curr-page"><a href="#">1</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
                    </ul>
                </div> -->
            </section>
        </div>
