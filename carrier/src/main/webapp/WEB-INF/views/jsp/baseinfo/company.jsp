<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>
<%

%>

<script type="text/javascript">
$(document).ready(function(){

});

function editCompany(companyId){
	
	document.location.href = "/baseinfo/edit-company.do?companyId="+companyId;
}


function addCompany(){
	document.location.href = "/baseinfo/add-company.do";
}

function search(){
	
	document.location.href = "/baseinfo/company.do?searchType="+$("#searchType").val()+"&searchWord="+encodeURI($("#searchWord").val());
	
}



</script>


	<!-- <section class="side-nav">
            <ul>
                <li class=""><a href="/baseinfo/employee.do">직원관리</a></li>
                <li class=""><a href="/baseinfo/driver.do">기사관리</a></li>
                <li class=""><a href="/baseinfo/customer.do">거래처관리</a></li>
                <li class="active"><a href="/baseinfo/company.do">회사정보관리</a></li>
            </ul>
        </section> -->

 <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">HOME</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">회사 관리</a></li>
                </ul>
            </div>

            <div class="up-dl clearfix header-search">
                 <table>
                    <tbody>
                        <tr>
                        	<td>
                                <input type="button" id="btn-search" value="회사등록"  onclick="javascript:addCompany();" class="btn-primary">
                            </td>
                            <td style="width: 145px;">
                                <div class="select-con">
							        <select class="dropdown" name="searchType" id="searchType">
							        	<option value="" <c:if test='${paramMap.searchType eq "" }'> selected="selected"</c:if>>조회구분을 선택 하세요.</option>
							            <option value="00" <c:if test='${paramMap.searchType eq "00" }'> selected="selected"</c:if>>업체명</option>
							            <option value="01" <c:if test='${paramMap.searchType eq "01" }'> selected="selected"</c:if>>대표자명</option>
							        </select>
							        <span></span>
							    </div>
                            </td>
                            <td style="width: 190px;">
                                <input type="text" id="searchWord" name="searchWord" value="${paramMap.searchWord}"   onkeypress="if(event.keyCode=='13') search();">
                            </td>
                            <td>
                                <input type="button" id="btn-search" value="검색" class="btn-primary" onclick="javascript:search();">
                            </td>
                            <td>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
<!-- 
            <div class="dispatch-btn-container">
               
                <div class="dispatch-btn">
                    <i class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div>
            </div> -->
        </section>



            <section class="bottom-table" style="width:1600px; margin-left:290px;">
                
                <table class="article-table" style="table-layout:fixed; margin-top:15px">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <!-- <td style="text-align:center;">번호</td> -->
                            <td style="text-align:center; width:100px;">등록일</td>
                            <td style="text-align:center; width:60px;">구분</td>
                            <td style="text-align:center;">업체명</td>
                            <td style="text-align:center; width:150px;">사업자등록번호</td>
                            <!-- <td style="text-align:center;">은행명</td> -->
                            <td style="text-align:center; width:100px;">대표자</td>
                            <td style="text-align:center;">업태</td>
                            <td style="text-align:center;">종목</td>
                            <td style="text-align:center; width:400px;">주소</td>
                            <!-- <td style="text-align:center;">주소상세</td> -->
                            <td style="text-align:center; width:100px;">첨부파일</td>
                            <%-- <c:if test="${userSessionMap.emp_role == 'A'}"> --%>
                            	<td style="text-align:center; width:100px;">관리</td>
                            <%-- </c:if> --%>
                        </tr>
                    </thead>
                    <tbody id="">
                    	<c:forEach var="data" items="${listData}" varStatus="status">
							<tr class="ui-state-default"> 
	                            <%-- <td style="text-align:center;">${data.idx}</td> --%>
	                            <td style="text-align:center;">${data.reg_dt_str}</td>
	                            <c:if test="${data.company_kind == '00'}">
	                            	<td style="text-align:center;">법인</td>
	                            </c:if>
	                            <c:if test="${data.company_kind == '01'}">
	                            	<td style="text-align:center;">개인</td>
	                            </c:if>
	                            <td style="text-align:center;">${data.company_name}</td>
	                            <td style="text-align:center;">${data.business_license_number}</td>
	                            <%-- <td style="text-align:center;">${data.bank_name}</td> --%>
	                            <td style="text-align:center;">${data.company_owner_name}</td>
	                            <td style="text-align:center; text-overflow:ellipsis; overflow:hidden;"><nobr>${data.business_condition}</nobr></td>
	                            <td style="text-align:center; text-overflow:ellipsis; overflow:hidden;"><nobr>${data.business_kind}</nobr></td>
	                            <td style="text-align:center; text-overflow:ellipsis; overflow:hidden;" title="${data.address}${data.address_detail}"><nobr>${data.address}${data.address_detail}</nobr></td>
	                            <%-- <td style="text-align:center; text-overflow:ellipsis; overflow:hidden;"><nobr>${data.address_detail}</nobr></td> --%>
	                            <td style="text-align:center;">
	                            	<c:forEach var="fileData" items="${data.fileList}" varStatus="status">
			                            <a target="_blank" href="/files/company${fileData.file_path }">
			                            	<img style="width:40px; height:40px;" src="/files/company${fileData.file_path }" alt="${fileData.file_nm}" title="${fileData.file_nm}" />
										</a>
									</c:forEach>
	                            </td>
	                            <td><a style="cursor:pointer;" onclick="javascript:editCompany('${data.company_id}')" class="btn-edit">수정</a></td>
                        	</tr>
						</c:forEach>
                   
                    </tbody>
                </table>
                <div class="table-pagination text-center">
                    <ul class="pagination">
                        <!-- <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="curr-page"><a href="#">1</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li> -->
                        <html:paging uri="/baseinfo/company.do" frontYn="N" />
                    </ul>
                </div>
                <div class="confirmation">
                    <!-- <div class="confirm">
                        <a href="/baseinfo/add-company.do">등록하기</a>
                    </div> -->
                </div>
                <!-- <div class="table-pagination text-center">
                    <ul class="pagination">
                        <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="curr-page"><a href="#">1</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
                    </ul>
                </div> -->
            </section>
        
