<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator"
	prefix="decorator"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%
	
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>한국카캐리어</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="/img/favicon.ico">
<link rel="stylesheet" href="/css/bootstrap.min.css">
<link rel="stylesheet" href="/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="/fonts/NotoSans/notosanskr.css">
<link rel="stylesheet" href="/css/reset.css">
<link rel="stylesheet" href="/css/main.css">
<link rel="stylesheet" href="/css/main2.css">
<link rel="stylesheet" href="/css/responsive.css">
<!-- <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<link rel="stylesheet" href="/css/jquery-ui.css">
<link rel="stylesheet" href="/css/pignose.calendar.css">
<link rel="stylesheet" href="/css/dcalendar.picker.css">
<link rel="stylesheet" href="/css/jquery.timepicker.min.css">
<link rel="stylesheet" href="/css/jquery-confirm.css">
<link rel="stylesheet" href="/css/font-awesome-css.min.css">

<link rel="stylesheet" href="/css/MonthPicker.min.css">
<script src="/js/vendor/modernizr-2.8.3.min.js"></script>
<script src="/js/vendor/jquery-1.11.2.min.js"></script>
<script src="/js/vendor/jquery-ui.min.js"></script>
<!-- <script src="/js/89d51f385b.js"></script> -->



<script type="text/javascript">
	$(document).ready(function() {

		setTimeout(openSocket(), 5000);

		/*  $('#deadLine').click(function(){
			  
		       
		         if(confirm("당일 마감등록을 하시겠습니까?")){

		            $.ajax({
		               type : 'post',
		               url : "/deadLine.do",
		               dataType : 'json',
		               async : false,
		               data : {
		                     
		               },

		               success : function(data, textStatus, jqXHR) {

		               if(data.successCode == "0000"){
		                  alert("당일 마감 등록이 정상적으로 처리 되었습니다.");
		                    $('#deadLine').hide();
		                        //$('#deadLine').css("display","none");
		               }if(data.successCode == "0001"){
		                  alert("당일 마감 등록 하는데 실패 했습니다.");
		               }

		            },error:function() {
			            
		                 alert("에러");
		            
		                  }
		      
		                });
		         }else{
		            alert("취소됨");
		            return false;
		            }
		       });
		 */

	});

	var ws;

	function openSocket() {
		if (ws !== undefined && ws.readyState !== WebSocket.CLOSED) {
			writeResponse("WebSocket is already opened.");
			return;
		}
		//웹소켓 객체 만드는 코드

	 		/* ws = new WebSocket("ws://52.78.153.148:8080/echo-ws.do?empId=${user.emp_id}"); */  
	 		ws = new WebSocket("ws://www.hkai.co.kr/echo-ws.do?empId=${user.emp_id}");  

			 /* ws = new WebSocket("ws://localhost:8080/echo-ws.do?empId=${user.emp_id}"); */ 
		     /* ws=new WebSocket("ws://14.63.39.17:13201/echo-ws.do?empId=${user.emp_id}"); */   

		ws.onopen = function(event) {
			if (event.data === undefined) {
				return;
			}

			writeResponse(event.data);
		};
		ws.onmessage = function(event) {
			//alert(event.data);
			writeResponse(event.data);
		};
		ws.onclose = function(event) {
			writeResponse("Connection closed");
		}



		
	}
	

	function send() {
		var text = $("#messageinput").val() + "," + $("#sender").val();
		ws.send(text);
		//text="";
		$("#messageinput").val("");

	}

	function closeSocket() {
		ws.close();
	}

	function writeResponse(text) {

		//	var result = $("#messages").html();
		//	result = result+'<br/>'+text;
		//alert(text);

	
			if(text.indexOf("noteAlarm") != -1){

			 	 $.confirm({
					    title: '📧 한국카캐리어(주) 쪽지 알림',
					    content: '쪽지가 도착했습니다. \n 확인해주세요!',
					    buttons: {
					        
					    	cancel :{
					    		text: '확인',
						    	action: function () {
					            
						    	}
					        },
					        confirm : {
					            text: '쪽지 리스트로 바로가기',
					            btnClass: 'btn-orange',
					            keys: ['enter', 'shift'],
					            action: function(){
						            document.location.href="/environment/employeeMessage.do";
					                
					            }
					        }
					    }
					}); 
				

				}else{

					
			
		
		
			var test = text.split("&");
			if (test.length > 1) {
			
			

			var test1 = test[0].split("=");
			var allocationId = test1[1].split(",");

			//var allocationId= "ALOcfd86d2cf77649b4bedf71094aa4de30";

			//	jAlert("루키", "#RED");

			$.confirm({
						title : '👀 한국카캐리어(주) 알림',
						content : '어플 탁송 예약이 들어왔습니다. \n 확인해주세요!',
						buttons : {

							cancel : {
								text : '확인',
								action : function() {

								}
							},
							confirm : {
								text : '어플 예약 리스트로 바로가기',
								btnClass : 'btn-purple',
								keys : [ 'enter', 'shift' ],
								action : function() {
									document.location.href = "/allocation/combination.do?allocationStatus=W";
								}
							}
						}
					});



		} else {

		/* 	 	 $.confirm({
				    title: '📧 한국카캐리어(주) 쪽지 알림',
				    content: '쪽지가 도착했습니다. \n 확인해주세요!',
				    buttons: {
				        
				    	cancel :{
				    		text: '확인',
					    	action: function () {
				            
					    	}
				        },
				        confirm : {
				            text: '쪽지 리스트로 바로가기',
				            btnClass: 'btn-blue',
				            keys: ['enter', 'shift'],
				            action: function(){
				                
				            }
				        }
				    }
				}); 
 */
			/* 	var resultarr = text.split(",");
			var result = "";
			if(resultarr.length > 1){
				result += '<p style=" margin-top:10px;"><a style="cursor:pointer;" href="/allocation/allocation-view.do?allocationId='+resultarr[0]+'">';
				result += resultarr[2]+"기사님이 탁송을 완료 하였습니다.";
				result += '</a></p>';
				text = result;
			}
			
				$("#messages").append('<br/>');
			$("#messages").append(text); */

			}

		}
	}

	/*

	 function getNotificationPermission() {
	 // 브라우저 지원 여부 체크
	 if (!("Notification" in window)) {
	 alert("데스크톱 알림을 지원하지 않는 브라우저입니다.");
	 }
	 // 데스크탑 알림 권한 요청
	 Notification.requestPermission(function (result) {
	 // 권한 거절
	 if(result == 'denied') {
	 alert('알림을 차단하셨습니다.\n브라우저의 사이트 설정에서 변경하실 수 있습니다.');
	 return false;
	 }
	 });
	 }
	 */

	/*
	 function notify(msg) {
	 var options = {
	 body: msg
	 }
	
	 // 데스크탑 알림 요청
	 var notification = new Notification("ddddd", options);
	
	 // 5초뒤 알람 닫기
	 setTimeout(function(){
	 notification.close();
	 }, 5000);
	 }


	 */

	function logoutProcess() {

		if (confirm("로그아웃 하시겠습니까?")) {
			document.location.href = "../logout.do";
		}
	}

	function viewDriverLocation() {

		window.open("/allocation/viewDriverLocation.do", "_blank",
						"top=0,left=0,width=800,height=800,toolbar=0,status=0,scrollbars=1,resizable=0");

	}

	function viewCancelRequest() {

		//alert("");
		document.location.href = "/allocation/viewCancelRequest.do?allocationStatus=X";

	}

	function changeCompany(empId, obj) {

		//if(confirm("회사를 변경 하시겠습니까?")){
		$.ajax({
			type : 'post',
			url : "/updateSessionCompanyId.do",
			dataType : 'json',
			data : {
				companyId : $(obj).val()
			},
			success : function(data, textStatus, jqXHR) {
				var result = data.resultCode;
				var resultData = data.resultData;
				if (result == "0000") {
					alert("변경 되었습니다.");
					document.location.reload();
				} else if (result == "0002") {
					alert("변경 하는데 실패 하였습니다.");
				}
			},
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
		//}

	}

	function refreshPage(obj) {

		//
		//if(confirm(driverName+"기사의 "+decideMonth+"월 공제 내역서를 전송 하시겠습니까?")){

		$.ajax({
			type : 'post',
			url : "/environment/updateEmployeeListRowCnt.do",
			dataType : 'json',
			async : false,
			data : {
				listRowCnt : $(obj).val()
			},
			success : function(data, textStatus, jqXHR) {
				var result = data.resultCode;
				var resultData = data.resultData;
				if (result == "0000") {
					window.location.reload();
				} else if (result != "0000") {

				}
			},
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});

		//}

	}

	function tmapOpen() {

		window
				.open(
						"/tmapOpen.do",
						"_blank",
						"top=0,left=0,width=1200,height=1200,toolbar=0,status=0,scrollbars=1,resizable=0");

	}

	function goViewBatch(emp_id, obj) {
		document.location.href = "/account/viewBatchTeam.do";
	}

	function goWriteBoard() {

		$.ajax({
			type : 'post',
			url : "/environment/insertAlarmBoard.do",
			dataType : 'json',
			data : {

				textarea : $("textarea#textArea").val(),

			},
			success : function(data, textStatus, jqXHR) {
				var result = data.resultCode;
				var resultData = data.resultData;
				if (result == "0000") {
					window.location.reload();
				} else if (result != "0000") {

					alert("알 수 없는 오류입니다. 관리자에게 문의해주세요");
				}
			},
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});

	}

	function goEmployeeNote(empId, empName) {

		if (confirm(empName + "님께 쪽지를 보내시겠습니까?")) {

			$('#sendEmpNote').show();
			$('#empIdBox').val(empId);

		} else {

			return false;

		}

	}

	function goEmployeeSendMessage(empId){

	
		$.ajax({
			type : 'post',
			url : "/allocation/sendEmployeeMessage.do",
			dataType : 'json',
			data : {
				empId : empId,
				textarea : $("textarea#textArea").val(), 
			},
			success : function(data, textStatus, jqXHR) {
				var result = data.resultCode;
				var resultData = data.resultData;
				if (result == "0000") {
					alert("쪽지를 전송했습니다.");
					window.location.reload();
				}else if (result == "0001") {

				}
			},
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});

	}

	function goMessageHam() {

		document.location.href = "/environment/employeeMessage.do";

	}
</script>

</head>

<body>

	<!-- <section class="main-nav clearfix" style="position:fixed; top:0px;"> -->
<c:if test='${combination eq "Y"}'>
	<div class="modal-field" id="sendEmpNote">
		<div class="modal-box">
			<h3 class="text-center">쪽지 보내기</h3>
			<div class="modal-table-container">
				<table class="article-table">
					<colgroup>

					</colgroup>

					<br>
					<span style="color: #00f;"> 쪽지</span>를 작성하고 전송버튼을 눌러주세요.
					<br>
					<input type="hidden" id="empIdBox" value="">
					<br>
					<tr>
						<textarea id="textArea" rows="15" cols="100"
							placeholder="쪽지 내용을 입력해주세요." style="margin-left: 5%;"></textarea>
					</tr>
					<br>
					</thead>

				</table>
			</div>

			<div class="confirmation">
				<div class="cancel">
					<input type="button" value="취소" name="" >
				</div>
				<div class="confirm">
					<input type="button" value="전송" name="" onclick="javascript:goEmployeeSendMessage($('#empIdBox').val());">
				</div>
			</div>

		</div>
	</div>
</c:if>


	<section class="main-nav clearfix">
		<nav class="menu">
			<ul class="ul1" id="gnbList">
				<%-- <li><span><a href="/allocation/combination.do?startDt=${paramMap.startDt}&endDt=${paramMap.endDt}">관리자 시스템</a></span></li> --%>
				<li><span><a href="/allocation/combination.do">관리자
							시스템</a></span></li>

				<c:if test="${user.dept != 'sunbo'}">
					<li gnbId="baseinfo"><a>기초정보관리</a>
						<ul>
							<li class="active"><a href="/baseinfo/company.do">회사정보관리</a></li>
							<li class=""><a href="/baseinfo/employee.do">직원관리</a></li>
							<li class=""><a href="/baseinfo/driver.do">기사관리</a></li>
							<li class=""><a href="/baseinfo/customer.do">거래처관리</a></li>
							<li class=""><a href="/baseinfo/articles-driver.do">지입기사관리</a></li>
							<li class=""><a href="/baseinfo/blacklist-customer.do">블랙리스트관리</a></li>
							<li class=""><a href="/baseinfo/customer-keywordList.do">거래처주소관리</a></li>
						</ul></li>
				</c:if>


				<li gnbId="allocation">
					<%-- <a href="/allocation/combination.do?startDt=${paramMap.startDt}&endDt=${paramMap.endDt}">통합배차관리</a> --%>
					<a href="#" onclick="javascript:return false;">통합배차관리</a>

					<ul
						<c:if test="${user.dept == 'sunbo'}"> style="width:100%;" </c:if>>
						<c:if test="${user.dept != 'sunbo'}">

							<li><a href="/allocation/allocation-register.do">신규배차입력
							</a></li>

							<c:if
								test="${user.emp_id == 'hk0000' || user.emp_id == 'hk0001' || user.emp_id == 'hk0002' || user.emp_id == 'hk0003' || user.emp_id == 'hk0006'}">
								<c:if test="${user.emp_id != 'hk0001' && user.emp_role == 'A'}">
									<li class="<c:if test="${reserve == 'N'}">active</c:if>"><a
										href="javascript:viewDriverLocation();">기사 위치 확인</a></li>
								</c:if>
							</c:if>
							<c:if
								test="${user.emp_id == 'hk0000' || user.emp_id == 'hk0001' || user.emp_id == 'hk0002' || user.emp_id == 'hk0003' || user.emp_id == 'hk0006' || user.emp_role == 'A'}">
								<c:if test="${user.emp_id != 'hk0006'}">
									<li class="<c:if test="${reserve == 'N'}">active</c:if>"><a
										href="javascript:viewCancelRequest();">취소 대기</a></li>
								</c:if>
							</c:if>
						</c:if>
						<%-- <li style="clear:both;" class="<c:if test="${reserve == ''}">active</c:if>"><a href="/allocation/self.do?startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&allocationStatus=N">셀프 미배차  </a></li>
			                <li class="<c:if test="${reserve == 'N'}">active</c:if>"><a href="/allocation/self.do?startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&allocationStatus=Y&searchType=${paramMap.searchType}&searchWord=${paramMap.searchWord}">셀프 배차</a></li>
			                <li class="<c:if test="${reserve == 'Y'}">active</c:if>"><a href="/allocation/self.do?startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&allocationStatus=F&searchType=${paramMap.searchType}&searchWord=${paramMap.searchWord}">셀프 완료</a></li>
			                <li style="clear:both;"><a href="/allocation/carrier.do?startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&allocationStatus=N&searchType=${paramMap.searchType}&searchWord=${paramMap.searchWord}">캐리어 미배차</a></li>
			                <li><a href="/allocation/carrier.do?startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&allocationStatus=Y">캐리어 배차</a></li>
			                <li><a href="/allocation/carrier.do?startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&allocationStatus=F">캐리어 완료</a></li> --%>
						<li style="clear: both;"
							class="<c:if test="${reserve == ''}">active</c:if>"><a
							href="/allocation/self.do?startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&allocationStatus=N&searchDateType=${paramMap.searchDateType}">셀프
								미배차 </a></li>
						<li class="<c:if test="${reserve == 'N'}">active</c:if>"><a
							href="/allocation/self.do?startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&allocationStatus=Y&searchDateType=${paramMap.searchDateType}">셀프
								배차</a></li>
								<li class="<c:if test="${reserve == 'N'}">active</c:if>"><a href="/allocation/selfTable.do">셀프배차판</a></li>
						<li class="<c:if test="${reserve == 'Y'}">active</c:if>"><a
							href="/allocation/self.do?startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&allocationStatus=F&searchDateType=${paramMap.searchDateType}">셀프
								완료</a></li>
						<c:if test="${user.dept != 'sunbo'}">
							<li class="<c:if test="${reserve == 'Y'}">active</c:if>"><a
								href="/allocation/self.do?startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&allocationStatus=C&searchDateType=${paramMap.searchDateType}">셀프
									취소</a></li>
						</c:if>
						<li style="clear: both;"><a
							href="/allocation/carrier.do?startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&allocationStatus=N&searchDateType=${paramMap.searchDateType}">캐리어
								미배차</a></li>
						<li><a
							href="/allocation/carrier.do?startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&allocationStatus=Y&searchDateType=${paramMap.searchDateType}">캐리어
								배차</a></li>
						<li><a
							href="/allocation/carrier.do?startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&allocationStatus=F&searchDateType=${paramMap.searchDateType}">캐리어
								완료</a></li>
						<c:if test="${user.dept != 'sunbo'}">
							<li><a
								href="/allocation/carrier.do?startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&allocationStatus=C&searchDateType=${paramMap.searchDateType}">캐리어
									취소</a></li>
						</c:if>
						<c:if test="${user.dept != 'sunbo'}">
							<li style="clear: both;"><a
								href="/allocation/combination.do?&regType=A">모바일 배차건</a></li>
							<li><a href="/allocation/combination.do?allocationStatus=W">모바일
									탁송 예약대기중</a></li>
						</c:if>
						<!-- <li style="clear:both;"><a href="/allocation/combination.do?allocationStatus=F">완료 배차</a></li> -->
					</ul>
				</li>
				<c:if test="${user.dept != 'sunbo'}">
					<li><a>차량관리</a>
						<ul>
							<li class=""><a href="/carmanagement/driverDeposit.do">예치금
									관리</a></li>
							<li class=""><a href="/carmanagement/carinfo-list.do">지입료
									관리</a></li>
							<li class=""><a href="/carmanagement/carInsurance.do">차량보험
									관리</a></li>
							<li class=""><a href="/carmanagement/carInspectionManage.do">차량검사
									관리</a></li>
							<!-- <li class=""><a href="/carmanagement/driverLowViolation.do">과태료/범칙금 관리</a></li> -->
							<li class=""><a href="/carmanagement/lowViolation-reg.do">과태료/범칙금
									관리</a></li>
							<!-- <li class=""><a href="/carmanagement/accident.do">사고 관리</a></li> -->
							<li class=""><a href="/carmanagement/carMaintanance-list.do?&searchWord=01">차계부
									관리</a></li>
							<li class="<c:if test="${reserve == 'N'}">active</c:if>"><a href="/carmanagement/carNumberManage.do">번호판관리</a></li>

						</ul></li>
					<li><a>회계관리</a> 
						<ul>
							<li class=""><a href="/account/customerCal.do">거래처 운송비정산</a></li>
							<!-- <li class=""><a href="javascript:alert('준비중입니다.');">운송비정산</a></li> -->
							<!-- <li class="" ><a href="/account/driverList.do">공제내역</a></li> -->
							<li class=""><a href="/account/driverList.do">기사
									월별운송비관리</a></li>
							<!-- <li class=""><a href="/account/sales.do">매출/매입 관리</a></li> -->
							<li class=""><a href="/account/sales.do">매입/매출 관리</a></li>
							<li class=""><a href="/account/collectManage.do">거래처
									수금관리</a></li>
							<li class=""><a href="/account/summary.do">요약관리</a></li>
							<li class=""><a href="/account/receivable.do">미수/미지급 관리</a></li>

							<!-- <li class=""><a>미수/미지급 관리</a></li> -->
							<!-- <li class=""><a href="/account/customerList.do">매출관리</a></li> -->
							<!-- <li class=""><a>미수관리</a></li> -->
						</ul></li>
					<c:if test="${user.emp_role == 'A'}">
						<!-- 인사관리 항목은 관리자만 볼 수 있도록 한다. -->
						<li gnbId="humanResource"><a href="/humanResource/add-employee.do">인사관리</a>
							<ul>
								<li class="active"><a href="/humanResource/add-employee.do">신규사원등록</a></li>
								<li class="active"><a href="/humanResource/add-driver.do">신규기사등록</a></li>
								<li class=""><a href="/humanResource/employee.do">입퇴사자 현황</a></li>
								<li class=""><a href="/environment/evaluation-empList.do">인사 고과 평가</a></li>

							</ul>
						</li>
					</c:if>

					<li><a>견적서관리</a></li>
					<li><a href="/message/message.do">메시지관리</a>
						<ul>
							<li class="active"><a href="/message/message.do">메시지발송</a></li>
							<li class="active"><a href="/message/messageNew.do">메시지발송-신규</a></li>
						</ul>	
					</li>
					<c:if test="${user.emp_id == 'hk0000' || user.emp_id == 'hk0003'}">
						<li><a href="/election/getElectionList.do">온라인 투표</a></li> 
					</c:if>
					<li><a href="javascript:tmapOpen();">티맵</a></li>

					<%-- <c:if test="${(user.allocation == 'C' || user.allocation == 'S') && bool == false && user.emp_id=='hk0005'}" > --%>
					<c:if
						test="${(user.allocation == 'C' || user.allocation == 'S') && user.emp_id=='hk0005'}">
						<!-- <button id ="deadLine" style ="text-align:center; cursor:pointer; margin-top:12px; margin-left:15px;"  class="btn btn-primary">일일배차마감</button> -->
						<button id="deadLine"
							style="text-align: center; cursor: pointer; margin-top: 12px; margin-left: 15px;"
							class="btn btn-primary"
							onclick="javascript:goViewBatch('${user.emp_id}',this);">일일배차마감</button>
					</c:if>

				</c:if>

			</ul>



			<ul class="ul2">
				<li><span class="greeting"> <strong>${user.emp_name }</strong>


						<select class="dropdown" name="companyId" id="companyId"
						style="height: 40px; width: 200px; color: #000;"
						onchange="javascript:changeCompany('${user.emp_id}',this);">
							<c:forEach var="data" items="${companyList}" varStatus="status">
								<option style="color: #000;" value="${data.company_id}"
									<c:if test='${data.company_id == companyId}'>selected</c:if>>${data.company_name}</option>
							</c:forEach>
					</select> <span></span> <%--      ${companyName}  --%> &nbsp;님 안녕하세요
				</span></li>
				<c:if test="${user.emp_role == 'A'}">
					<!-- 환경설정은 관리자만 볼 수 있도록 한다. -->
					<li class="pref"><a
						href="/environment/user.do?searchType=ALL&searchWord="> <img
							src="/img/globe-icon.png" alt=""> <span>환경설정</span>
					</a>
						<ul>
							<li class="active" style="padding: 10px;"><a
								href="/environment/user.do?searchType=ALL&searchWord=">사용자
									관리</a></li>
							<li class="" style="padding: 10px;"><a
								href="/environment/driver.do">기사 관리</a></li>
							<li class="" style="padding: 10px;"><a
								href="/environment/user-role.do?searchType=ALL&searchWord=">사용자
									권한</a></li>

						</ul></li>

				</c:if>
				<li class="logout"><a style="cursor: pointer;"
					onclick="javascript:logoutProcess();"> <img
						src="/img/logout-icon.png" alt=""> <span>로그아웃</span>
				</a></li>
			</ul>
		</nav>
	</section>

	<section class="side-nav">


		<!-- <div class="calendar"></div> -->

		<table id="calendar-demo"></table>
		<input type="text" id="sender" value="${user.emp_id}"
			style="display: none;">
		<c:if test="${user.dept != 'sunbo'}">

			<!-- /account/billRequest.do -->

			<!-- alert(document.location.href); -->
			<div
				style="width: 100%; height: 50px; margin-top: 15px; text-align: right;">

				리스트 표시 : <select class="dropdown"
					style="width: 30%; margin-right: 10px;"
					onchange="javascript:refreshPage(this);">
					<option style="text-align: right;" value="10"
						<c:if test='${listRowCnt eq 10}'> selected="selected" </c:if>>10개</option>
					<option style="text-align: right;" value="15"
						<c:if test='${listRowCnt eq 15}'> selected="selected" </c:if>>15개</option>
					<option style="text-align: right;" value="20"
						<c:if test='${listRowCnt eq 20}'> selected="selected" </c:if>>20개</option>
					<option style="text-align: right;" value="30"
						<c:if test='${listRowCnt eq 30}'> selected="selected" </c:if>>30개</option>
					<option style="text-align: right;" value="40"
						<c:if test='${listRowCnt eq 40}'> selected="selected" </c:if>>40개</option>
					<option style="text-align: right;" value="50"
						<c:if test='${listRowCnt eq 50}'> selected="selected" </c:if>>50개</option>
					<option style="text-align: right;" value="100"
						<c:if test='${listRowCnt eq 100}'> selected="selected" </c:if>>100개</option>
				</select>
			</div>


			<!-- 알림창 테이블 -->
			<c:if test='${combination eq "Y"}'>
				<div
					style="width: 100%; height: 350px; margin-top: 5%; background-color: #E0ECF8; overflow-y: scroll; border: 2px solid #81BEF7;"
					id="messages">
					<span style="color: #0B0B61;"><Strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;💌한국카캐리어(주)
							알림창 💌</Strong></span><br>
					<c:forEach var="data" items="${socketAlarmTalkList}"
						varStatus="status">
						<p style="margin-top: 10px;">
							<c:if test='${data.status eq null || data.status eq "W"}'>
								<a style="cursor: pointer;"
									href="/allocation/combination.do?allocationStatus=W"><span
									style="">${data.text}</span></a>
							</c:if>
						</p>
					</c:forEach>

					<!--      		<div id ="textBox" style="margin-top:85%;" >
		    	     		<textarea id="textArea" rows="4" cols="22" placeholder ="공지 내용을 입력해주세요. "></textarea>
		    	    			 <input type ="button" value="입력" class="btn btn-info" style="margin-top:-16%;" onclick="javascript:goWriteBoard()">
		         		</div> -->

				</div>

			</c:if>


			<!-- 배차리스트 테이블 -->
			
			<!-- <c:if test='${combination eq "Y"}'>
				<div
					style="width: 100%; height: 260px; margin-top: 8%; background-color: #eee; overflow-y: scroll;"
					id="messages">
					<Strong><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;${today}&nbsp;배차
							리스트 &gt;</p></Strong><br>
					<c:forEach var="data" items="${allocationStatusListToday}"
						varStatus="status">
						<p style="margin-top: 10px;">
							<a style="cursor: pointer;"
								href="/allocation/combination.do?&allocationStatus=${data.allocation_status}&allocationStatistics=Y">
								&nbsp;${data.status} 건수 : &nbsp;&nbsp;<span
								style="color: #2E2EFE;">${data.count}</span>&nbsp;건
							</a>
						</p>

					</c:forEach>
				</div>
			</c:if> -->


			<!-- 세금계산서 발행 테이블 -->
			<c:if test='${combination eq "Y"}'>
				<c:if test='${billPublishRequestCount > 0}'>
					<div
						style="width: 100%; height: 85px; margin-top: 8%; background-color: #eee; text-align: right; cursor: pointer;">
						<p style="margin-right: 10px;"
							onclick="javascript:document.location.href='/account/billRequest.do'">세금계산서
							발행 요청 : ${billPublishRequestCount} 건</p>
						<br>
						<p style="margin-right: 10px;"
							onclick="javascript:document.location.href='/account/customerCal.do?&includeZero=&searchDateType=&searchType=&searchWord=&carrierType=A&startDt=&endDt=&decideStatus=Y'">
							세금계산서<span style="color: #FE2E2E"> 확정</span> 거래처 ${decideCountY}건
						</p>
						<br>
						<p style="margin-right: 10px;"
							onclick="javascript:document.location.href='/account/customerCal.do?&includeZero=N&searchDateType=D&searchType=00&searchWord=&carrierType=A&startDt=&endDt=&decideStatus=N'">
							세금계산서<span style="color: #00BFFF"> 미확정 </span> 거래처
							${decideCountN}건
						</p>
						<br>
					</div>
					<br>
				</c:if>
			</c:if>


			<!-- 기사 탁송 완료 테이블 -->

			<div
				style="width: 100%; height: 200px; background-color: #eee; overflow-y: scroll;"
				id="messages">
				<c:forEach var="data" items="${allocationFinishInfoList}"
					varStatus="status">
					<p style="margin-top: 10px;">
						<a style="cursor: pointer;"
							href="/allocation/allocation-view.do?allocationId=${data.allocation_id}">${data.driver_name}(${data.driver_cnt})기사님
							탁송 완료</a>
					</p>
					<p>하차지 : ${data.arrival} ${data.reg_dt_si}</p>
				</c:forEach>
			</div>

			<!-- 쪽지 보내기 테이블 -->
	<!-- 
		<c:if test='${combination eq "Y"}'>
			<div
				style="width: 100%; height: 350px; margin-top: 5%; background-color: #E6F8E0; overflow-y: scroll; border: 2px solid #2EFE64;"
				id="messages">
				<span style="color: #0B0B61;"><Strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;📧
						쪽지 보내기 📧 <Strong></span><br>
				<br>
				
				<c:forEach var="data" items="${employeeNoteList}" varStatus="status">
					<p style="margin-top: 10px;">
						<a style="cursor: pointer;"
							onclick="javascrtipt:goEmployeeNote('${data.emp_id}','${data.emp_name}');"><span
							style="">${data.emp_name} ${data.emp_position}님</span></a>
					</p>
				</c:forEach>
			</div>
			<div style="width: 80%; margin-left: 20%; padding:5%;">
				<a style="color: #31B404;" onclick="javascript:goMessageHam();"><Strong>
						📬  쪽지함으로 가기 <Strong></a>
			</div>
			</c:if> -->
			<%-- 	
		<div style="width:100%; height:50px; margin-top:15px; text-align:right;">
	         		
	         		날짜 선택 :
	         		<select class="dropdown" style="width:30%; margin-right:10px; " onchange="javascript:refreshPage(this);">
							        	<option style="text-align:right;" value="10" <c:if test='${listRowCnt eq 10}'> selected="selected" </c:if>></option>
							        	<option style="text-align:right;"  value="15" <c:if test='${listRowCnt eq 15}'> selected="selected" </c:if> >15개</option>
							        	<option style="text-align:right;"  value="20" <c:if test='${listRowCnt eq 20}'> selected="selected" </c:if> >20개</option>
							        	<option style="text-align:right;"  value="30" <c:if test='${listRowCnt eq 30}'> selected="selected" </c:if> >30개</option>
							        	<option style="text-align:right;"  value="40" <c:if test='${listRowCnt eq 40}'> selected="selected" </c:if> >40개</option>
							        	<option style="text-align:right;"  value="50" <c:if test='${listRowCnt eq 50}'> selected="selected" </c:if> >50개</option>
							        	<option style="text-align:right;"  value="100" <c:if test='${listRowCnt eq 100}'> selected="selected" </c:if> >100개</option>
							        </select>
	         		  <input style="width:30%; text-align:center;" id="selectMonth" name="selectMonth"  type="text" placeholder="조회월" readonly="readonly" value="">
	         		</div>  --%>




			<%-- <div style="width:100%; height:300px; background-color:#eee; overflow-y:scroll;" id="messages">
	         		<c:forEach var="data" items="${allocationFinishInfoList}" varStatus="status" >
	         			<p style=" margin-top:10px;"><a style="cursor:pointer;" href="/allocation/allocation-view.do?allocationId=${data.allocation_id}">${data.driver_name}(${data.driver_cnt})기사님 탁송 완료</a></p>
	         			<p>하차지 : ${data.arrival} ${data.reg_dt_si}</p>
	         		</c:forEach>
	         	</div> --%>


		</c:if>
		<!-- <input style="width:100%;" type="text" id="messageinput"> -->

		<div>

			<!-- <button type="button" onclick="openSocket();">Open</button>
		        <button type="button" onclick="send();">Send</button>
		        <button type="button" onclick="closeSocket();">Close</button> -->
		</div>






	</section>





	<decorator:body />


	<script>
		window.jQuery
				|| document
						.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')
	</script>
	<script src="/js/vendor/bootstrap.min.js"></script>
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="/js/moment.js"></script>
	<script src="/js/pignose.calendar.full.js"></script>
	<script src="/js/dcalendar.picker.js"></script>
	<script src="/js/MonthPicker.min.js"></script>
	<script src="/js/main.js"></script>
	<script src="/js/jquery-confirm.js"></script>
	<script src="/js/jquery.timepicker.min.js"></script>
	<script>
		var fixHelper = function(e, ui) {
			ui.children().each(function() {
				console.log(e);
				$(this).width($(this).width());
			});
			return ui;
		};

		$("#sortable").sortable({
			helper : fixHelper
		}).disableSelection();

		$(document).ready(function() {
			checkGnb();
		});

		function checkGnb() {
			var url = location.href;

			$("#gnbList").find("li").each(function() {
				$(this).removeClass("active");
			});

			$("#gnbList").find("li").each(function() {
				var gnbId = $(this).attr("gnbId");

				if (url.indexOf(gnbId) > -1) {
					$(this).addClass("active");
				}
			});

		}

		$(function() {
			//	 $('.calendar').pignoseCalender(); 	
			//$('#calendar-demo-sub').pignoseCalender();
			$('#calendar-demo')
					.dcalendar({
						theme : 'indigo'
					})
					.on(
							'dateselected',
							function(e) {

								//alert(e.obj);
								var selectedCalendarVal = e.date.split("-")[2];
								selectedCalendarVal = Number(selectedCalendarVal);
								var status = "";
								if (typeof '${paramMap.startDt}' != "undefined"
										&& '${paramMap.startDt}' == e.date) {
									if (typeof '${paramMap.searchDateType}' == "undefined"
											|| '${paramMap.searchDateType}' == ""
											|| '${paramMap.searchDateType}' == "S") {
										status = "D";
									} else {
										status = "S";
									}
								} else {
									status = "D";
								}

								var queryStr = "?&origin=calendar&selectedCalendarVal="
										+ selectedCalendarVal
										+ "&searchDateType="
										+ status
										+ "&startDt="
										+ e.date
										+ "&endDt="
										+ e.date
										+ "&searchWord="
										+ encodeURI('${paramMap.searchWord}')
										+ "&searchType=${paramMap.searchType}";

								if (typeof '${paramMap.allocationStatus}' != "undefined"
										&& '${paramMap.allocationStatus}' != "") {
									queryStr += "&allocationStatus=${paramMap.allocationStatus}"
								}

								var loc = $(location).attr('href').split("?")[0];
								document.location.href = $(location).attr(
										'href').split("?")[0]
										+ queryStr;

							});
		});

		if (typeof '${paramMap.selectedCalendarVal}' != "undefined"
				&& '${paramMap.selectedCalendarVal}' != "") {

			setTimeout(function() {
				$(".date").each(function(index, element) {
					$(this).removeClass("selected");
				});
				$("#${paramMap.selectedCalendarVal}").addClass("selected");
			}, 1);
		}
	</script>
</body>

</html>
