<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>한국카캐리어</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="/img/favicon.ico">
<link rel="stylesheet" href="/css/bootstrap.min.css">
<link rel="stylesheet" href="/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="/fonts/NotoSans/notosanskr.css">
<link rel="stylesheet" href="/css/reset.css">
<link rel="stylesheet" href="/css/main.css">
<link rel="stylesheet" href="/css/main2.css">
<link rel="stylesheet" href="/css/responsive.css">
<link rel="stylesheet" href="/css/jquery-ui.css">
<link rel="stylesheet" href="/css/font-awesome-css.min.css">
<link rel="stylesheet" type="text/css" href="https://topopen.tmap.co.kr/tmaplibv20/theme/default/style_effect.css">
<link rel="stylesheet" href="https://topopen.tmap.co.kr/tmaplibv20/theme/default/style.css" type="text/css">


<script src="https://apis.openapi.sk.com/tmap/js?version=1&format=javascript&appKey=${tmapAppKey}"></script>



<script src="https://topopen.tmap.co.kr/tmaplibv20/Core.js?vn=Release1.18.25"></script>
<script src="https://topopen.tmap.co.kr/tmaplibv20/Format.js?vn=Release1.18.25"></script>
<script src="https://topopen.tmap.co.kr/tmaplibv20/Add-on.js?vn=Release1.18.25"></script>



<script src="/js/vendor/modernizr-2.8.3.min.js"></script>
<script src="/js/vendor/jquery-1.11.2.min.js"></script>
<!-- <script src="/js/89d51f385b.js"></script> -->
<!-- <script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script> -->
<!-- <script type="text/javascript" src="/js/plugins/jquery.form.min.js"></script> -->
<script src="/js/vendor/jquery-1.11.2.min.js"></script>

<script type="text/javascript">

var map, markerLayer,marker,tData,vectorLayer,popup;
var zoomDepth = 17;
var searchStatus = true;

var mLastUrlString = ''; // 가장 최근에 불러온 메인 화면 URL 저장
var mIsNeedRefresh = false; // 새로고침 필요여부(검색페이지 로드 시 Anchor 가 변하지 않아 강제 새로고침 필요)

$(document).ready(function() {
	
	initTmap();

	loadMainContents($.urlAnchor()); // 메인 화면 불러오기
	$(window).on('hashchange', function(evt) {
		// URL #(앵커) 변경시 호출됨
		loadMainContents($.urlAnchor()); // 메인 화면 불러오기
	});
	$(document).on('click', 'a', function(evt) {
		// a Link 클릭시 호출됨
		var strHref = $(this).attr('href');
		if ('#' + $.urlAnchor() == strHref) {
			// URL 이 같으면 a 링크 클릭해도 hashchange 이벤트가 발생하지 않기 때문에 임의로 함수호출
			loadMainContents($.urlAnchor()); // 메인 화면 불러오기
		}
	});
	

	// 검색어 창 이벤트 설정
	$('#search_keyword').keyup(function() {
		searchMenuTitle($(this).val());
	});
	$('#search_keyword').keydown(function(evt) {
		if (evt.keyCode == 13) {
			$('#search_button').trigger('click');
		}
	});

	// 검색 버튼 이벤트 설정
	$('#search_button').click(function() {
		loadSearchIndex($('#search_keyword').val());
	});

	
});


//페이지가 로딩이 된 후 호출하는 함수입니다.
function initTmap(){

	map = new Tmap.Map({
		div:'map_div',
		width : "900px",
		height : "900px",
	});
	map.setCenter(new Tmap.LonLat("127.00118587318626", "37.457399").transform("EPSG:4326", "EPSG:3857"), zoomDepth);


	tData = new Tmap.TData();

	
	map.events.register("click", map, onClick);


	markerLayer = new Tmap.Layer.Markers();//마커 레이어 생성
	map.addLayer(markerLayer);//map에 마커 레이어 추가

	vectorLayer = new Tmap.Layer.Vector("vectorLayerID");
	map.addLayer(vectorLayer);

	/* markerLayer = new Tmap.Layer.Markers();
	map.addLayer(markerLayer);
	   
	var lonlat = new Tmap.LonLat( 127.02557976548207 , 37.457399).transform("EPSG:4326", "EPSG:3857");
	var size = new Tmap.Size(24, 38);
	var offset = new Tmap.Pixel(-(size.w / 2), -(size.h));
	var icon = new Tmap.Icon('http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_a.png',size, offset);
	
	marker = new Tmap.Marker(lonlat, icon);
	markerLayer.addMarker(marker); */

	
} 

var departure = new Object();
var arrival = new Object();

function onClick(e){


	var lonlat = map.getLonLatFromViewPortPx(e.xy).transform("EPSG:3857", "EPSG:4326");//클릭한 부분의 ViewPorPx를 LonLat로 변환합니다
	var result ='클릭한 위치의 좌표는'+lonlat+'입니다.';

	markerLayer.clearMarkers();
	markerLayer.removeMarker(marker); // 기존 마커 삭제
	
	if(map.getZoom() < 18){
		zoomDepth = 18
		map.setCenter(new Tmap.LonLat(lonlat.lon, lonlat.lat).transform("EPSG:4326", "EPSG:3857"), zoomDepth);

	}else{

		if(searchStatus){
			setLocation(lonlat,lonlat.lon,lonlat.lat);
		}
		
	}

	
	
		  
}



function searchStart(obj){

	

	$.ajax({
		method:"GET",
		url:"https://apis.openapi.sk.com/tmap/pois?version=1&format=xml&callback=result",// POI 통합검색 api 요청 url입니다.
		async:false,
		data:{
			"searchKeyword" : $(obj).val(),//검색 키워드
			"resCoordType" : "EPSG3857",//응답 좌표계 유형
			"reqCoordType" : "WGS84GEO",//요청 좌표계 유형
			"appKey" : "${tmapAppKey}",// 실행을 위한 키입니다. 발급받으신 AppKey(appKey)를 입력하세요.
			"count" : 10//페이지당 출력되는 개수를 지정
		},
		//데이터 로드가 성공적으로 완료되었을 때 발생하는 함수입니다.
		success:function(response){
			prtcl = response;
			
			// 2. 기존 마커, 팝업 제거
			if(markerLayer != null) {
				markerLayer.clearMarkers();
				map.removeAllPopup();
			}
			
			// 3. POI 마커 표시
			markerLayer = new Tmap.Layer.Markers();//마커 레이어 생성
			map.addLayer(markerLayer);//map에 마커 레이어 추가
			var size = new Tmap.Size(24, 38);//아이콘 크기 설정
			var offset = new Tmap.Pixel(-(size.w / 2), -(size.h));//아이콘 중심점 설정
			var maker;
			var popup;
			var prtclString = new XMLSerializer().serializeToString(prtcl);//xml to String	
			xmlDoc = $.parseXML( prtclString ),
			$xml = $( xmlDoc ),
			$intRate = $xml.find("poi");
			var innerHtml ="";
			$intRate.each(function(index, element) {
			   	var name = element.getElementsByTagName("name")[0].childNodes[0].nodeValue;
			   	var id = element.getElementsByTagName("id")[0].childNodes[0].nodeValue;
			   	var content ="<div style=' position: relative; border-bottom: 1px solid #dcdcdc; line-height: 18px; padding: 0 35px 2px 0;'>"+
							    "<div style='font-size: 12px; line-height: 15px;'>"+
							        "<span style='display: inline-block; width: 14px; height: 14px;  vertical-align: middle; margin-right: 5px;'></span>"+name+
							    "</div>"+
							 "</div>";
				var lon = element.getElementsByTagName("noorLon")[0].childNodes[0].nodeValue;
				var lat = element.getElementsByTagName("noorLat")[0].childNodes[0].nodeValue;
				var icon = new Tmap.Icon('http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_'+index+'.png',size, offset);//마커 아이콘 설정
				var lonlat = new Tmap.LonLat(lon, lat);//좌표설정

				var location = get4326LonLat(lon, lat);
				//alert(location.lon);
				//alert(location.lat);
				
			   	innerHtml+="<div style='cursor:pointer;'  onclick='javascript:setLocation(\""+location+"\",\""+location.lon+"\",\""+location.lat+"\")'><img src='http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_"+index+".png' style='vertical-align:middle'/><span>"+name+"</span></div>";
				
				marker = new Tmap.Marker(lonlat, icon);//마커생성
				markerLayer.addMarker(marker);//마커레이어에 마커 추가

				//marker.events.register("click", marker, onMarkerClick);
				
				popup = new Tmap.Popup("p1", lonlat, new Tmap.Size(120, 50), content, false);//마우스 오버 팝업
				popup.autoSize = true;//Contents 내용에 맞게 Popup창의 크기를 재조정할지 여부를 결정
				map.addPopup(popup);//map에 popup추가
				popup.hide();//마커에 마우스가 오버되기 전까진 popup을 숨김
				//마커 이벤트등록
			    marker.events.register("mouseover", popup, onOverMarker);
			    //마커에 마우스가 오버되었을 때 발생하는 이벤트 함수입니다.
			    function onOverMarker(evt) {
			      this.show(); //마커에 마우스가 오버되었을 때 팝업이 보입니다. 
			    }
			    //마커 이벤트 등록
			    marker.events.register("mouseout", popup, onOutMarker);
			    //마커에 마우스가 아웃되었을 때 발생하는 함수입니다.
			    function onOutMarker(evt) {
			      this.hide(); //마커에 마우스가 없을땐 팝업이 숨겨집니다.
			    }
		   });
		$("#searchResult").html(innerHtml);
			map.zoomToExtent(markerLayer.getDataExtent());//마커레이어의 영역에 맞게 map을 zoom합니다.
			zoomDepth = map.getZoom();
			
		},
		//요청 실패시 콘솔창에서 에러 내용을 확인할 수 있습니다.
		error:function(request,status,error){
			console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
		}
	});

	
}

function onMarkerClick(){

	
}


function searchInit(){


	if(confirm("초기화 하시겠습니까?")){

		searchStatus = true;
		//if(markerLayer != null) {
			markerLayer.clearMarkers();
			markerLayer.removeMarker(marker); // 기존 마커 삭제
		//}

		vectorLayer.removeAllFeatures();
		$("#searchKeyword").val('');
		$("#searchResult").html("");

		map.removeAllPopup();
		
		departure = new Object();
		arrival = new Object();
	}
	
}



function setLocation(lonlat,lon,lat){


	//markerLayer.removeMarker(marker); // 기존 마커 삭제

	
	map.setCenter(new Tmap.LonLat(lon, lat).transform("EPSG:4326", "EPSG:3857"), zoomDepth);	

	var size = new Tmap.Size(24, 38);//아이콘 크기 설정
	var offset = new Tmap.Pixel(-(size.w / 2), -(size.h));//아이콘 중심점 설정
	var iconD,iconA;

	iconD = new Tmap.Icon('/img/pin_b_m_d.png',size, offset);//마커 아이콘 설정
	iconA = new Tmap.Icon('/img/pin_b_m_a.png',size, offset);//마커 아이콘 설정
	
	
	
	setTimeout(function() {

		if(isEmpty(departure)){
			if(confirm("해당 위치를 출발지로 지정 하시겠습니까?")){

				markerLayer.clearMarkers();
				markerLayer.removeMarker(marker); // 기존 마커 삭제
				
				departure.lon = lon;
				departure.lat = lat;
				iconD = new Tmap.Icon('/img/pin_b_m_d.png',size, offset);//마커 아이콘 설정
				$("#searchKeyword").val("");
				$("#searchResult").html("");
				marker = new Tmap.Marker(new Tmap.LonLat(lon, lat).transform("EPSG:4326", "EPSG:3857"), iconD);//마커 생성
				markerLayer.addMarker(marker);
				
			}else{

			}

			
		}else if(!isEmpty(departure) && isEmpty(arrival)){
			if(confirm("해당 위치를 도착지로 지정 하시겠습니까?")){

				markerLayer.clearMarkers();
				markerLayer.removeMarker(marker); // 기존 마커 삭제

				arrival.lon = lon;
				arrival.lat = lat;

				var s_lonLat = new Tmap.LonLat(departure.lon, departure.lat); //시작 좌표입니다.   
				var e_lonLat = new Tmap.LonLat(arrival.lon, arrival.lat); //도착 좌표입니다.

				var optionObj = {
						reqCoordType:"WGS84GEO", //요청 좌표계 옵셥 설정입니다.
						resCoordType:"EPSG3857",  //응답 좌표계 옵셥 설정입니다.
						trafficInfo:"Y"
		             }
				
				tData.getRoutePlan(s_lonLat, e_lonLat, optionObj);//경로 탐색 데이터를 콜백 함수를 통해 XML로 리턴합니다.
				
				tData.events.register("onComplete", tData, onComplete);//데이터 로드가 성공적으로 완료되었을 때 발생하는 이벤트를 등록합니다.
				tData.events.register("onProgress", tData, onProgress);//데이터 로드중에 발생하는 이벤트를 등록합니다.
				tData.events.register("onError", tData, onError);//데이터 로드가 실패했을 떄 발생하는 이벤트를 등록합니다.
				$("#searchKeyword").val("");
				$("#searchResult").html("");

				
				marker = new Tmap.Marker(new Tmap.LonLat(departure.lon, departure.lat).transform("EPSG:4326", "EPSG:3857"), iconD);//마커 생성
				markerLayer.addMarker(marker);

				marker = new Tmap.Marker(new Tmap.LonLat(arrival.lon, arrival.lat).transform("EPSG:4326", "EPSG:3857"), iconA);//마커 생성
				markerLayer.addMarker(marker);
					
			}else{


			}
			
			
			
		}else if(!isEmpty(departure) && !isEmpty(arrival)){

			searchStatus = true;
			//초기화 하고 이 함수를 다시 호출 한다.
			//searchInit();
			if(markerLayer != null) {
				markerLayer.clearMarkers();
				markerLayer.removeMarker(marker); // 기존 마커 삭제
			}
	
			vectorLayer.removeAllFeatures();
			$("#searchKeyword").val("");
			$("#searchResult").html("");
			departure = new Object();
			arrival = new Object();
			
			setLocation(lonlat,lon,lat);
			
		}


		
	}, 200);






	
	

	
}

function onComplete(){


	searchStatus = false;
	
	
	console.log(this.responseXML); 
    
    var totalDistance = this.responseXML.getElementsByTagName("tmap:totalDistance")[0].firstChild.data;
    var totalTime = this.responseXML.getElementsByTagName("tmap:totalTime")[0].firstChild.data;

    var hour = parseInt(totalTime/3600);
    var min = parseInt((totalTime%3600)/60);
    var sec = totalTime%60;

    var test = "";
	if(hour > 0){
		test += hour + "시간"; 
	}
	if(min > 0){
		test += min + "분"; 
	}
	if(sec > 0){
		test += sec + "초"; 
	}

	totalDistance = totalDistance/1000;
	//alert(totalDistance);
	//alert(totalTime);
    
    var trafficColors = {
			extractStyles:true,
			trafficDefaultColor:"#000000", //교통 정보가 없을 때
			trafficType1Color:"#009900", //원할
			trafficType2Color:"#8E8111", //지체
			trafficType3Color:"#FF0000"  //정체

		};

	var kmlForm = new Tmap.Format.KML(trafficColors).readTraffic(this.responseXML);
	vectorLayer.addFeatures(kmlForm);
		  
	map.zoomToExtent(vectorLayer.getDataExtent());

	var center = map.getCenter().transform(PR_3857,PR_4326 );


	
	var content ="<div style=' position: relative; border-bottom: 1px solid #dcdcdc; line-height: 18px; padding: 0 35px 2px 0;'>"+
    "<div style='font-size: 12px; line-height: 15px;'>"+
        "<span style='display: inline-block; width: 14px; height: 14px; vertical-align: middle; margin-right: 5px;'>경로 정보</span>"+
    "</div>"+
 "</div>"+
 "<div style='position: relative; padding-top: 5px; display:inline-block'>"+
    "<div style='display:inline-block; margin-left:5px; vertical-align: top;'>"+
    	"<span style='font-size: 12px; margin-left:2px; margin-bottom:2px; display:block;'>총 거리 : "+totalDistance.toFixed(1)+"km</span>"+
    	/* "<span style='font-size: 12px; color:#888; margin-left:2px; margin-bottom:2px; display:block;'>(우)100-999  (지번)을지로2가 11</span>"+ */
    	/* "<span style='font-size: 12px; margin-left:2px;'><a href='https://developers.sktelecom.com/' target='blank'>개발자센터</a></span>"+ */
    	"<span style='font-size: 12px; margin-left:2px;'> 총 소요시간 : "+test+"</span>"+
    "</div>"+
 "</div>";
	popup = new Tmap.Popup("p1",
	                new Tmap.LonLat(center.lon, center.lat).transform("EPSG:4326", "EPSG:3857"),//Popup 이 표출될 맵 좌표
	                new Tmap.Size(180, 50),//Popup 크기
	                content,//Popup 표시될 text
	                //onPopupClose//close클릭시 실행할 콜백 함수
	                false
	                );
	popup.setBorder("1px solid #8d8d8d");//popup border 조절
	popup.autoSize=true;//popup 사이즈 자동 조절		                         
	map.addPopup(popup);//map에 popup 추가



	
   }



function onPopupClose(evt) {
	//select.unselectAll();
}


function onProgress(){

}

function onError(){
	alert("onError");
}





function isEmpty(obj) {

    for(var prop in obj){
         if(obj.hasOwnProperty(prop)){
        	 return false;
         }
    }
    return true;
}





var PR_3857 = new Tmap.Projection("EPSG:3857");		// Google Mercator 좌표계인 EPSG:3857
var PR_4326 = new Tmap.Projection("EPSG:4326");	

function get4326LonLat(x, y){
  	return new Tmap.LonLat(x, y).transform(PR_3857, PR_4326);
  }
 
  function get3857LonLat(x, y){
  	return new Tmap.LonLat(x, y).transform(PR_4326, PR_3857);
  }






//검색어 자동완성
function searchMenuTitle(searchKeyword) {
	var searchCategory = ''; // 검색범위(Web, WebV2 Android, iOS, WebService)

	if (searchKeyword && searchKeyword != '') {
		searchCategory = $('#side_search .dropdown_wrapper a').html()
				.trim(); // 검색범위(Web, WebV2, Android, iOS, WebService)

		// 검색창에 검색 범위목록 복제
		$('#search_result dl').html(
				$('#tit_' + searchCategory.replace(" ", "_"))
						.siblings('ul').clone().addClass('filetree')
						.wrapAll("<div/>").parent().html());
		$('#search_result ul, #search_result li').css('display', 'none');

		// 검색어가 존재하는 항목만 표시
		$('#search_result a').each(
				function() {
					var aTagText = $(this).text();
					if (aTagText.toLowerCase().indexOf(
							searchKeyword.toLowerCase()) != -1) {
						$(this).parents('ul, li').css('display', 'block');
					}
				});
		$('#search_result').show();
	} else {
		// 자동 검색창 숨기기
		$('#search_result').hide();
		$('#search_result dl').html("");
	}
}

// Index 검색결과 페이지 메인에 로드
function loadSearchIndex(searchKeyword) {
	var searchCategory = '';

	var resultHtml = '';

	if (searchKeyword && searchKeyword != '') {
		searchCategory = $('#side_search .dropdown_wrapper a').html()
				.trim(); // 검색범위(Web, WebV2 Android, iOS, WebService)

		setSelectedMenuCSS('search_result');
		$('#contents')
				.load(
						'/search_result.html',
						function(response, status, xhr) {
							var resultLinkGuide = '';
							var resultLinkSample = '';
							var resultLinkDocs = '';
							var resultLinkUsecase = '';

							var resultCnt = 0;
							var eachCnt = 0;
							if (status == "success") {
								// 메인 페이지 로드 성공 시 작업

								// 검색 페이지 로드시 Anchor 가 변하지 않아 임의로 페이지 새로고침 필요
								mIsNeedRefresh = true;

								// 카테고리 표시
								$('#divIndexCategory').html(searchCategory);
								$('#divIndexCategory').addClass(
										searchCategory.replace(/\s/gi, ""));

								// 각 메뉴 복제하여 로드
								$('#indexResultGuide')
										.html(
												$(
														'#tit_'
																+ searchCategory
																		.replace(
																				" ",
																				"_"))
														.siblings('ul')
														.find(
																'li > span:contains("Guide")')
														.siblings('ul')
														.clone().addClass(
																'filetree')
														.wrapAll("<div/>")
														.parent().html());
								$('#indexResultSample')
										.html(
												$(
														'#tit_'
																+ searchCategory
																		.replace(
																				" ",
																				"_"))
														.siblings('ul')
														.find(
																'li > span:contains("Sample")')
														.siblings('ul')
														.clone().addClass(
																'filetree')
														.wrapAll("<div/>")
														.parent().html());
								$('#indexResultDocs')
										.html(
												$(
														'#tit_'
																+ searchCategory
																		.replace(
																				" ",
																				"_"))
														.siblings('ul')
														.find(
																'li > span:contains("Docs")')
														.siblings('ul')
														.clone().addClass(
																'filetree')
														.wrapAll("<div/>")
														.parent().html());
								$('#indexResultUsecase')
										.html(
												$(
														'#tit_'
																+ searchCategory
																		.replace(
																				" ",
																				"_"))
														.siblings('ul')
														.find(
																'li > span:contains("Use case")')
														.siblings('ul')
														.clone().addClass(
																'filetree')
														.wrapAll("<div/>")
														.parent().html());
								$('#tbodyResult ul, #tbodyResult li').css(
										'display', 'none');

								// Guide 검색 결과 생성
								eachCnt = 0;
								$('#indexResultGuide a')
										.each(
												function() {
													var aTagText = $(this)
															.text();
													if (aTagText
															.toLowerCase()
															.indexOf(
																	searchKeyword
																			.toLowerCase()) != -1) {
														//resultHtml += '<dd>' + $(this).parent().html() + '</dd>';
														$(this)
																.parents(
																		'ul, li')
																.css(
																		'display',
																		'block');
														resultCnt++;
														eachCnt++;
													}
												});
								if (eachCnt == 0) {
									$('#search_guide').css('display',
											'none');
									$('#search_guide_list').css('display',
											'none');
								}

								// Sample 검색 결과 생성
								eachCnt = 0;
								$('#indexResultSample a')
										.each(
												function() {
													var aTagText = $(this)
															.text();
													if (aTagText
															.toLowerCase()
															.indexOf(
																	searchKeyword
																			.toLowerCase()) != -1) {
														//resultHtml += '<dd>' + $(this).parent().html() + '</dd>';
														$(this)
																.parents(
																		'ul, li')
																.css(
																		'display',
																		'block');
														resultCnt++;
														eachCnt++;
													}
												});
								if (eachCnt == 0) {
									$('#search_sample').css('display',
											'none');
									$('#search_sample_list').css('display',
											'none');
								}

								// Docs 검색 결과 생성
								eachCnt = 0;
								$('#indexResultDocs a')
										.each(
												function() {
													var aTagText = $(this)
															.text();
													if (aTagText
															.toLowerCase()
															.indexOf(
																	searchKeyword
																			.toLowerCase()) != -1) {
														//resultHtml += '<dd>' + $(this).parent().html() + '</dd>';
														$(this)
																.parents(
																		'ul, li')
																.css(
																		'display',
																		'block');
														resultCnt++;
														eachCnt++;
													}
												});
								if (eachCnt == 0) {
									$('#search_docs')
											.css('display', 'none');
									$('#search_docs_list').css('display',
											'none');
								}

								// Use case 검색 결과 생성
								if (searchCategory == "Web"
										|| searchCategory == "WebV2") {
									eachCnt = 0;
									$('#indexResultUsecase a')
											.each(
													function() {
														var aTagText = $(
																this)
																.text();
														if (aTagText
																.toLowerCase()
																.indexOf(
																		searchKeyword
																				.toLowerCase()) != -1) {
															//resultHtml += '<dd>' + $(this).parent().html() + '</dd>';
															$(this)
																	.parents(
																			'ul, li')
																	.css(
																			'display',
																			'block');
															resultCnt++;
															eachCnt++;
														}
													});
									if (eachCnt == 0) {
										$('#search_usecase').css('display',
												'none');
										$('#search_usecase_list').css(
												'display', 'none');
									}
								}

								if (resultCnt > 0) {
									// 검색결과가 존재할 경우
									// Index 결과검색 테마 적용
									$('#tbodyResult a').addClass(
											'h_underline');
									$('#tbodyResult a')
											.each(
													function() {
														$(this)
																.html(
																		'· <span>'
																				+ $(
																						this)
																						.html()
																				+ '</span>');
													});
								} else {
									$('#tbodyResult')
											.html(
													'<td style="padding:100px 20px">검색 결과가 없습니다.</td>');
								}

								// 스크롤 맨 위로 이동
								$('main').animate({
									scrollTop : 0
								}, 100);
							}
						});
	}
}

// 메인페이지 로드 함수
function loadMainContents(urlAnchorString) {
	var urlString = null; // (URL?Prameter) : .html 없음
	var urlWithHtml = null; // 메인 페이지에 띄울 실제 요청 주소
	var anchorIndex = -1; // 앵커 인덱스
	var anchorString = null; // 앵커 스트링

	// 자동 검색창 초기화 - START
	$('#search_keyword').val('');
	$('#search_result').hide();
	$('#search_result dl').html("");
	// 자동 검색창 초기화 - END

	if (urlAnchorString) {
		// URL 과 책갈피 정보 파싱 - START
		anchorIndex = urlAnchorString.indexOf('.'); // 페이지 URL 과 책갈피 정보는 "." 으로  구분됨
		if (anchorIndex != -1) {
			// 책갈피 정보가 있는 경우
			urlString = urlAnchorString.substring(0, anchorIndex);
			anchorString = urlAnchorString.substring(anchorIndex + 1,
					urlAnchorString.length);
		} else {
			// 책갈피 정보가 없는 경우
			urlString = urlAnchorString;
		}
		// URL 과 책갈피 정보 파싱 - END

		if (mIsNeedRefresh || mLastUrlString != urlString) {
			// 새로고침이 필요하거나 새로운 URL 일 경우 작업

			// 새로고침 필요여부 초기화
			mIsNeedRefresh = false;

			// 실제 요청 주소 만들기
			if (urlString.indexOf('?') != -1) {
				// 파라미터가 있을 경우
				urlWithHtml = "/"
						+ urlString.substring(0, urlString.indexOf('?'))
						+ ".html"
						+ urlString.substring(urlString.indexOf('?'),
								urlString.length); // ?(파라미터 구분자) 앞에 .html 끼워 넣음
			} else {
				// 파라미터가 없을 경우
				urlWithHtml = "/" + urlString + ".html";
			}

			$('#contents')
					.load(
							urlWithHtml,
							function(response, status, xhr) {
								if (status == "success") {
									// 메인 페이지 로드 성공 시 작업

									// 최근 페이지 URL 저장( 중복 로드 방지 )
									mLastUrlString = urlString;

									// 선택된 메뉴 표시
									setSelectedMenuCSS(urlString,
											anchorString);

									// 책갈피 위치로 이동
									if (anchorString) {
										$.moveScrollById(anchorString);
									} else {
										$('#contents.main').animate({
											scrollTop : 0
										}, 500, 'swing', function() {
											$('#top_btn').fadeOut();
										});
									}
								} else {
									// 기본 페이지 로드
									location.href = "/main.html#web/sample/TotalSample";
									// 			            setSelectedMenuCSS('web/sample/TotalSample');
									// 			            $('#contents').load('/web/sample/TotalSample.html');
								}
							});
		} else {
			// 기존 URL 일 경우 작업

			// 선택된 메뉴 표시
			setSelectedMenuCSS(urlString, anchorString);

			// 책갈피 위치로 이동
			if (anchorString) {
				$.moveScrollById(anchorString);
			}
		}
	}
}

// 좌측 메뉴 선택된 항목 설정
function setSelectedMenuCSS(urlString, anchorString) {
	var htmlIndex = -1;
	var menuId = '';

	if (urlString) {

		// 메뉴 아이디 구하기
		if (urlString.indexOf("?") != -1) {
			// 파라미터가 있을 경우
			menuId = urlString.substring(0, urlString.indexOf("?"))
					.replace(/\//gi, "_"); // 슬레시를 언더바로 치환
		} else {
			// 파라미터가 없을 경우
			menuId = urlString.replace(/\//gi, "_"); // 슬레시를 언더바로 치환
		}

		// 앵커가 존재한다면 메뉴 아이디 뒤에 붙임
		if (anchorString) {
			menuId = menuId + "_" + anchorString; // anchor 에서 # 제거하고 메뉴 아이디 뒤에 붙이기
		}

		$('#' + menuId).closest('li').parents('li').removeClass(
				'expandable');
		$('#' + menuId).closest('li').parents('li').addClass('collapsable');

		$('#' + menuId).parents('ul').css('display', 'block');
		$('#' + menuId).parents('ul').each(function(index, element) {
			$(element).siblings('div').removeClass('expandable-hitarea');
			$(element).siblings('div').addClass('collapsable-hitarea');
		});

		$('#tree_wrap a').removeClass("on");
		$('#' + menuId).addClass("on");

		//메뉴 스크롤 이동 (스크롤이 펼쳐지는 시간이 클라이언트마다 다르기에 1초 후 작동)
		setTimeout(function() {
			$("#tree_wrap").mCustomScrollbar("scrollTo", '#' + menuId);
		}, 1000);

	}
}

// Html id 로 위치를 찾아서 스크롤
$.moveScrollById = function(id) {
	setTimeout(function() {
		var position = $('#' + id).offset(); // 위치값

		if (position) {
			$('main').animate({
				scrollTop : (position.top + $('main').scrollTop())
			}, 100); // 이동
		}
	}, 300);//처음 로딩시 offset의 위치값이 달라지므로 0.3초 딜레이
}

// URL 에서 앵커 추출
$.urlAnchor = function() {
	var sharpIndex = window.location.href.indexOf('#');
	var result = null;
	if (sharpIndex != -1) {
		result = window.location.href.substring(sharpIndex + 1,
				window.location.href.length);
	}
	if (result == null) {
		return null;
	} else {
		return result || '';
	}
}


</script>
	
</head>
		
	<body style="width:100%;">
	
	<!-- <input style=" margin-top:30px;" type="text" placeholder="검색" name="cAcqDate" onkeypress="javascript:if(event.keyCode=='13'){searchStart(this);};"> -->
	
	
	<!-- <div style="height:50px; overflow-y:scroll; " id="searchResult" name="searchResult">검색결과</div> -->
	
	<!-- <div id="map_div"></div> -->
		
	
	
	<div class="sample_area">
		<div class="ft_area" style="">
			<div class="ft_select_wrap">
				<div class="text_div_custom2">
					<input type="text" class="text_custom" id="searchKeyword" name="searchKeyword" value="" onkeypress="javascript:if(event.keyCode=='13'){searchStart(this);};">
					<input type="button" style=""  value="초기화" onclick="javascript:searchInit();">
				</div>
				<!-- <a class="btn_select" id="btn_select" name="btn_select">
					<span class="map_ico">적용하기</span>
				</a> -->
			</div>

			<div class="clear"></div>
		</div>
		<div class="search_rst_wrap" style=" ">
			<div class="search_rst" style="width: 25%; float:left;">
								<div class="title"><strong>Search</strong> Results</div>
								<div class="rst_wrap">
									<div class="rst mCustomScrollbar _mCS_2 mCS_no_scrollbar"><div id="mCSB_2" class="mCustomScrollBox mCS-light mCSB_vertical mCSB_inside" tabindex="0"><div id="mCSB_2_container" class="mCSB_container mCS_y_hidden mCS_no_scrollbar_y" style="position:relative; top:0; left:0;" dir="ltr">
										<ul id="searchResult" name="searchResult">
											<li>검색결과</li>
										</ul>
									</div><div id="mCSB_2_scrollbar_vertical" class="mCSB_scrollTools mCSB_2_scrollbar mCS-light mCSB_scrollTools_vertical" style="display: none;"><a href="#" class="mCSB_buttonUp" oncontextmenu="return false;"></a><div class="mCSB_draggerContainer"><div id="mCSB_2_dragger_vertical" class="mCSB_dragger" style="position: absolute; min-height: 30px; height: 0px; top: 0px;" oncontextmenu="return false;"><div class="mCSB_dragger_bar" style="line-height: 30px;"></div></div><div class="mCSB_draggerRail"></div></div><a href="#" class="mCSB_buttonDown" oncontextmenu="return false;"></a></div></div></div>
								</div>
			</div>
			<div id="map_div" class="map_wrap tmMap" style="float: left; width: 70%;"><!-- <div id="Tmap_Map_7_Tmap_ViewPort" class="tmMapViewport tmControlDragPanActive tmControlZoomBoxActive tmControlPinchZoomActive tmControlNavigationActive" style="position: relative; overflow: hidden; width: 100%; height: 100%;"><div id="Tmap_Map_7_Tmap_Container" style="position: absolute; z-index: 749; left: -24px; top: 104px;"><div id="Tmap_Layer_TMS_31" dir="ltr" class="tmLayerDiv tmLayerGrid" style="position: absolute; width: 100%; height: 100%; z-index: 100;"><img class="tmTileImage" src="http://topopentile1.tmap.co.kr/tms/1.0.0/hd_tile/15/27942/20078.png?bizAppId=5c88a4e4-0f6d-4002-9989-f9e35e5257fe?v=Release1.18.25" style="visibility: inherit; opacity: 1; position: absolute; left: 194px; top: 76px; width: 256px; height: 256px;"><img class="tmTileImage" src="http://topopentile2.tmap.co.kr/tms/1.0.0/hd_tile/15/27941/20078.png?bizAppId=5c88a4e4-0f6d-4002-9989-f9e35e5257fe?v=Release1.18.25" style="visibility: inherit; opacity: 1; position: absolute; left: -62px; top: 76px; width: 256px; height: 256px;"><img class="tmTileImage" src="http://topopentile3.tmap.co.kr/tms/1.0.0/hd_tile/15/27942/20079.png?bizAppId=5c88a4e4-0f6d-4002-9989-f9e35e5257fe?v=Release1.18.25" style="visibility: inherit; opacity: 1; position: absolute; left: 194px; top: -180px; width: 256px; height: 256px;"><img class="tmTileImage" src="http://topopentile3.tmap.co.kr/tms/1.0.0/hd_tile/15/27942/20077.png?bizAppId=5c88a4e4-0f6d-4002-9989-f9e35e5257fe?v=Release1.18.25" style="visibility: inherit; opacity: 1; position: absolute; left: 194px; top: 332px; width: 256px; height: 256px;"><img class="tmTileImage" src="http://topopentile3.tmap.co.kr/tms/1.0.0/hd_tile/15/27943/20078.png?bizAppId=5c88a4e4-0f6d-4002-9989-f9e35e5257fe?v=Release1.18.25" style="visibility: inherit; opacity: 1; position: absolute; left: 450px; top: 76px; width: 256px; height: 256px;"><img class="tmTileImage" src="http://topopentile2.tmap.co.kr/tms/1.0.0/hd_tile/15/27941/20079.png?bizAppId=5c88a4e4-0f6d-4002-9989-f9e35e5257fe?v=Release1.18.25" style="visibility: inherit; opacity: 1; position: absolute; left: -62px; top: -180px; width: 256px; height: 256px;"><img class="tmTileImage" src="http://topopentile3.tmap.co.kr/tms/1.0.0/hd_tile/15/27941/20077.png?bizAppId=5c88a4e4-0f6d-4002-9989-f9e35e5257fe?v=Release1.18.25" style="visibility: inherit; opacity: 1; position: absolute; left: -62px; top: 332px; width: 256px; height: 256px;"><img class="tmTileImage" src="http://topopentile1.tmap.co.kr/tms/1.0.0/hd_tile/15/27943/20079.png?bizAppId=5c88a4e4-0f6d-4002-9989-f9e35e5257fe?v=Release1.18.25" style="visibility: inherit; opacity: 1; position: absolute; left: 450px; top: -180px; width: 256px; height: 256px;"><img class="tmTileImage" src="http://topopentile1.tmap.co.kr/tms/1.0.0/hd_tile/15/27943/20077.png?bizAppId=5c88a4e4-0f6d-4002-9989-f9e35e5257fe?v=Release1.18.25" style="visibility: inherit; opacity: 1; position: absolute; left: 450px; top: 332px; width: 256px; height: 256px;"><img class="tmTileImage" src="http://topopentile2.tmap.co.kr/tms/1.0.0/hd_tile/15/27944/20078.png?bizAppId=5c88a4e4-0f6d-4002-9989-f9e35e5257fe?v=Release1.18.25" style="visibility: inherit; opacity: 1; position: absolute; left: 706px; top: 76px; width: 256px; height: 256px;"><img class="tmTileImage" src="http://topopentile1.tmap.co.kr/tms/1.0.0/hd_tile/15/27944/20079.png?bizAppId=5c88a4e4-0f6d-4002-9989-f9e35e5257fe?v=Release1.18.25" style="visibility: inherit; opacity: 1; position: absolute; left: 706px; top: -180px; width: 256px; height: 256px;"><img class="tmTileImage" src="http://topopentile1.tmap.co.kr/tms/1.0.0/hd_tile/15/27944/20077.png?bizAppId=5c88a4e4-0f6d-4002-9989-f9e35e5257fe?v=Release1.18.25" style="visibility: inherit; opacity: 1; position: absolute; left: 706px; top: 332px; width: 256px; height: 256px;"></div></div><div id="Tmap_Control_ZoomBar_3" class="tmControlZoomBar tmControlNoSelect" unselectable="on" style="position: absolute; left: 4px; top: 4px; z-index: 1005;"><div id="Tmap_Control_ZoomBar_3_zoomin" class="tmButton" style="position: absolute; left: 4px; top: 4px; width: 30px; height: 29px; cursor: pointer;"><img id="Tmap_Control_ZoomBar_3_zoomin_innerImage" class="olAlphaImg" src="https://topopen.tmap.co.kr/tmaplibv20/img/zm_zoom-plus-mini.png" style="position: relative; width: 30px; height: 29px;"></div><div id="Tmap_Control_PanZoomBar_ZoombarTmap_Map_7" class="tmButton" style="background-image: url(&quot;https://topopen.tmap.co.kr/tmaplibv20/img/zm_zoombar.png&quot;); left: 4px; top: 33px; width: 30px; height: 200px; position: absolute; cursor: pointer; visibility: hidden;"></div><div id="Tmap_Control_ZoomBar_3_Tmap_Map_7" style="position: absolute; left: 8px; top: 71px; width: 22px; height: 9px; cursor: move; visibility: hidden;"><img id="Tmap_Control_ZoomBar_3_Tmap_Map_7_innerImage" class="olAlphaImg" src="https://topopen.tmap.co.kr/tmaplibv20/img/zm_slider.png" style="position: relative; width: 22px; height: 9px;"></div><div id="Tmap_Control_PanZoomBar_LevelGuideTmap_Map_7" style="background-image: url(&quot;https://topopen.tmap.co.kr/tmaplibv20/img/zm_btn_lable.png&quot;); left: 34px; top: 63px; width: 29px; height: 134px; position: absolute; visibility: hidden;"></div><div id="Tmap_Control_ZoomBar_3_zoomout" class="tmButton" style="position: absolute; left: 4px; top: 33px; width: 30px; height: 29px; cursor: pointer;"><img id="Tmap_Control_ZoomBar_3_zoomout_innerImage" class="olAlphaImg" src="https://topopen.tmap.co.kr/tmaplibv20/img/zm_zoom-minus-mini.png" style="position: relative; width: 30px; height: 29px;"></div></div><div id="Tmap_Control_ScaleLine_4" class="tmControlScaleLine tmControlNoSelect" unselectable="on" style="position: absolute; z-index: 1005;"><div class="tmControlScaleLineTop" style="visibility: visible; width: 53px;">200 m</div><div class="tmControlScaleLineBottom" style="visibility: hidden;"></div></div><div id="Tmap_Control_ArgParser_5" class="tmControlArgParser tmControlNoSelect" unselectable="on" style="position: absolute; z-index: 1005;"></div><div id="Tmap_Control_Attribution_6" class="tmControlAttribution tmControlNoSelect" unselectable="on" style="position: absolute; z-index: 1005;"><img src="https://topopen.tmap.co.kr/tmaplib/img/powered-by-T-map_comms.png"></div></div> --></div>
		</div>
		<!-- <div>
			<img src="/resources/images/sample/WebSamplePoi.png"/>
		</div> -->
	</div>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	</body>
</html>

