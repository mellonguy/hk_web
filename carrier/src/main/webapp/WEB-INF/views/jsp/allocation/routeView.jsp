<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>한국카캐리어</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="/img/favicon.ico">
<link rel="stylesheet" href="/css/bootstrap.min.css">
<link rel="stylesheet" href="/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="/fonts/NotoSans/notosanskr.css">
<link rel="stylesheet" href="/css/reset.css">
<link rel="stylesheet" href="/css/main.css">
<link rel="stylesheet" href="/css/main2.css">
<link rel="stylesheet" href="/css/responsive.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="/js/vendor/modernizr-2.8.3.min.js"></script>
<script src="/js/vendor/jquery-1.11.2.min.js"></script>
<script src="/js/89d51f385b.js"></script>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<!-- <script type="text/javascript" src="/js/plugins/jquery.form.min.js"></script> -->
<script src="/js/vendor/jquery-1.11.2.min.js"></script>
<script type="text/javascript">


</script>
	
</head>
		
	<body style="width:1600px;">
	
	<div style="float:left;">
		<div id="map" style="width:800px; height:800px;">
		</div>
	</div>
	<div style="float:right;">
		<div id="roadview" style="width:800px; height:800px; float:right;">
		</div>
	</div>	
	<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=196301395a31adf3319a7ee5f66f17da"></script>
	<script>
	var container = document.getElementById('map'); //지도를 담을 영역의 DOM 레퍼런스
	var options = { //지도를 생성할 때 필요한 기본 옵션
		center: new daum.maps.LatLng('${map.center_x}','${map.center_y}'), //지도의 중심좌표.
		level: 9 //지도의 레벨(확대, 축소 정도)
	};

	var map = new daum.maps.Map(container, options);
	//map.addOverlayMapTypeId(daum.maps.MapTypeId.ROADVIEW);
	// 지도 레벨은 지도의 확대 수준을 의미합니다
	// 지도 레벨은 1부터 14레벨이 있으며 숫자가 작을수록 지도 확대 수준이 높습니다
	function zoomIn() {        
	    // 현재 지도의 레벨을 얻어옵니다
	    var level = map.getLevel();
	    
	    // 지도를 1레벨 내립니다 (지도가 확대됩니다)
	    map.setLevel(level - 1);
	    
	}    

	function zoomOut() {    
	    // 현재 지도의 레벨을 얻어옵니다
	    var level = map.getLevel(); 
	
	    map.setLevel(level + 1);
	 
	} 
	
	
	daum.maps.event.addListener(map, 'bounds_changed', function() {             
	    
	    // 지도 영역정보를 얻어옵니다 
	    var bounds = map.getBounds();
	    
	    // 영역정보의 남서쪽 정보를 얻어옵니다 
	    var swLatlng = bounds.getSouthWest();
	    
	    // 영역정보의 북동쪽 정보를 얻어옵니다 
	    var neLatlng = bounds.getNorthEast();
	    
	    var message = '<p>영역좌표는 남서쪽 위도, 경도는  ' + swLatlng.toString() + '이고 <br>'; 
	    message += '북동쪽 위도, 경도는  ' + neLatlng.toString() + '입니다 </p>'; 
	    
	});

	var points = [
	    new daum.maps.LatLng('${map.departure_x}','${map.departure_y}'),
	    new daum.maps.LatLng('${map.arrival_x}','${map.arrival_y}'),
	    //new daum.maps.LatLng('37.4540237','127.0008785')				//폰에서 가져온 좌표
	];
	
	var bounds = new daum.maps.LatLngBounds();    

	var i, marker;

	
	
	for (i = 0; i < points.length; i++) {
	    // 배열의 좌표들이 잘 보이게 마커를 지도에 추가합니다
	    marker =     new daum.maps.Marker({ position : points[i] });
	    marker.setMap(map);

	    markerTest(points[i]);
		
	   // LatLngBounds 객체에 좌표를 추가합니다
	    bounds.extend(points[i]); 
	    	
   	
	}



	function markerTest(position){

		var marker = new kakao.maps.Marker({
	        map: map,
	        position: position
	    });
	    
		daum.maps.event.addListener(marker, 'click', function() {

			showRoadView(position);
			
		});

		
	}


	var x_departure = "";
	var y_departure = "";
			
	var x = "";
	var y = "";
	
	var linePath = new Array();
	 <c:forEach var="item" items="${posList}" varStatus="status">
		linePath.push(new daum.maps.LatLng('${item.x}','${item.y}'));
		<c:if test='${status.index == 0}'>
			x_departure = '${item.x}';
			y_departure = '${item.y}';
		</c:if>
	 	<c:if test='${status.index == fn:length(posList)-1}'>
	 		x = '${item.x}';
	 		y = '${item.y}';
		</c:if>
	</c:forEach> 
	// 지도에 표시할 선을 생성합니다
	var polyline = new daum.maps.Polyline({
	    path: linePath, // 선을 구성하는 좌표배열 입니다
	    strokeWeight: 3, // 선의 두께 입니다
	    strokeColor: '#FF0000', // 선의 색깔입니다
	    //strokeColor: '#8B00FF', // 선의 색깔입니다
	    strokeOpacity: 1, // 선의 불투명도 입니다 1에서 0 사이의 값이며 0에 가까울수록 투명합니다
	    strokeStyle: 'solid' // 선의 스타일입니다
	});
	
	 map.setBounds(bounds);
	 polyline.setMap(map);
	 
	 var roadviewContainer = document.getElementById('roadview'); //로드뷰를 표시할 div
	 var roadview = new daum.maps.Roadview(roadviewContainer); //로드뷰 객체
	 var roadviewClient = new daum.maps.RoadviewClient(); //좌표로부터 로드뷰 파노ID를 가져올 로드뷰 helper객체

	 var position = new daum.maps.LatLng(x, y);
	 var departurePosition = new daum.maps.LatLng(x_departure, y_departure);


	 function showRoadView(departurePosition){

		 
	 // 특정 위치의 좌표와 가까운 로드뷰의 panoId를 추출하여 로드뷰를 띄운다.
		 roadviewClient.getNearestPanoId(departurePosition, 80, function(panoId) {
	     roadview.setPanoId(panoId, departurePosition); //panoId와 중심좌표를 통해 로드뷰 실행
		
	     
	 	});

	 }
		

//	var iwContent = '<div style="padding:5px;">Hello World! <br><a href="http://map.daum.net/link/map/Hello World!,33.450701,126.570667" style="color:blue" target="_blank">큰지도보기</a> <a href="http://map.daum.net/link/to/Hello World!,33.450701,126.570667" style="color:blue" target="_blank">길찾기</a></div>', // 인포윈도우에 표출될 내용으로 HTML 문자열이나 document element가 가능합니다
//    iwPosition = new daum.maps.LatLng(37.4540237, 127.0008785); //인포윈도우 표시 위치입니다

/* 
// 인포윈도우를 생성합니다
var infowindow = new daum.maps.InfoWindow({
    position : iwPosition, 
    content : iwContent 
});

infowindow.open(map, marker); 
 */	
		 
	
	$(document).ready(function(){

		
		showRoadView(position);
		 
		 
	});
	
	
	</script>
	
	
</body>
</html>

