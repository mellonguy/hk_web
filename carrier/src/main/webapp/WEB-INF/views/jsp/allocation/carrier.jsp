<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>
<%
String listOrder = request.getAttribute("listOrder").toString();
String allocationId = request.getAttribute("allocationId").toString();
%>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/js/bootstable.js"></script>
<script src="/js/vendor/clipboard.min.js"></script>
<script type="text/javascript"> 
var oldListOrder = "";
var allocationId = "";

var sortStatus = false;

var clipboard = new Clipboard('.clipboard'); 

$(document).ready(function(){
	//alert('<%=listOrder%>');
	oldListOrder = "<%=listOrder%>";
	allocationId = "<%=allocationId%>";
	$('#dataTable tr:odd td').css("backgroundColor","#f9f9f9");
	
	$("#startDt").datepicker({
		dateFormat : "yy-mm-dd",
		onSelect : function(date){
			//$("#endDt").focus();
			setTimeout(function(){
				$("#endDt").focus();  
	        }, 50);
		}
	});
	
    $( "#sortable" ).sortable({
    	disabled: true,
  	   start:function(){
  	   }
  	   ,stop:function(){
         	$('#dataTable tr:odd td').css("backgroundColor","#f9f9f9");
         	$('#dataTable tr:even td').css("backgroundColor","#fff");
  	   }
  	  });
    
    $("#customerList").css('display','none');
    $("#personInChargeList").css('display','none');
    
});

        var fixHelper = function(e, ui) {  
          ui.children().each(function() {  
          console.log(e);
            $(this).width($(this).width());  
          });  
          return ui;  
        };

        $("#sortable").sortable({  
            helper: fixHelper  
        }).disableSelection();
 
        
        function listOrderChangePossible(obj){
        	
        	if(sortStatus){
        		sortStatus = false
        		$(obj).val("리스트 순서 활성화");
        		  $( "#sortable" ).sortable({
        		    	disabled: true 
        		 	  });
        		$(obj).next().css("display","none");
        	}else{
        		sortStatus = true;
        		$(obj).val("리스트 순서 비활성화");
        		  $( "#sortable" ).sortable({
        		    	disabled: false 
        		 	  });
        		$(obj).next().css("display","block");	
        	}
        	
        }       
                       
        
        
	function listOrderChange(){
        var listOrder = ""
        var total = $("#sortable").find("tr").length;
       	$("#sortable").find("tr").each(function(index,element){		
       		listOrder +=$(this).attr("list-order");
	       	 if (index < total-1 ) { 
	       		listOrder += ","; 
	         } 
      	});		
        
     	if(oldListOrder == listOrder){
       		alert("리스트의 순서가 변경되지 않았습니다.");
       		return false;
       	}
       	
       	if(confirm("변경 하시겠습니까?")){
   			$.ajax({ 
   				type: 'post' ,
   				url : "/allocation/update-order.do" ,
   				dataType : 'json' ,
   				data : {
   					oldListOrder : oldListOrder,
   					newlistOrder : listOrder,
   					allocationId : allocationId
   				},
   				success : function(data, textStatus, jqXHR)
   				{
   					var result = data.resultCode;
   					if(result == "0000"){
   						alert("변경 되었습니다.");
   						document.location.href = "/allocation/carrier.do";
   					}else if(result == "0002"){
   						alert("변경 하는데 실패 하였습니다.");
   					}else if(result == "0003"){
   						alert("리스트가 삭제되어 변경 하는데 실패 하였습니다.");
   					}else if(result == "0004"){
   						alert("리스트가 이미 변경 되어서 변경 하는데 실패 했습니다.");
   					}
   				} ,
   				error : function(xhRequest, ErrorText, thrownError) {
   				}
   			});	
   		} 	
        	
	}
	
	
	function updateAllocaion(row){
		var inputDt = "";
        var carrierType = "";
        var distanceType = "";
        var departureDt = "";
        var departureTime = "";
        var customerName = "";
        var carKind = "";
        var carIdNum = "";
        var carNum  = "";
        var departure = "";
        var arrival  = "";
        var driverName = "";
        var carCnt  = "";
        var amount   = "";
        var paymentKind = "";
		var allocId = $(row).attr("allocationId");
		if(confirm("수정 하시겠습니까?")){
			$(row).find("td").each(function(index,element){
				if(index==1){
					inputDt = $(this).html();
				}
				if(index==2){
					if($(this).html() == "셀프"){
						carrierType = "S";	
					}else if($(this).html() == "캐리어"){
						carrierType = "C";
					}else{
						alert("배차구분을 확인 하세요.");
						return false;
					}
				}
				if(index==3){
					if($(this).html() == "시내"){
						distanceType = "0";	
					}else if($(this).html() == "시외"){
						distanceType = "1";
					}else{
						alert("거리구분을 확인 하세요.");
						return false;
					}
				}
				if(index==4){
					departureDt = $(this).html();
				}
				if(index==5){
					departureTime = $(this).html();
				}
				if(index==6){
					customerName = $(this).html();
				}
				if(index==7){
					carKind = $(this).html();
				}
				if(index==8){
					carIdNum = $(this).html();
				}
				if(index==9){
					carNum = $(this).html();
				}
				if(index==10){
					departure = $(this).html();
				}
				if(index==11){
					arrival = $(this).html();
				}
				if(index==12){
					driverName = $(this).html();
				}
				if(index==13){
					carCnt = $(this).html();
				}
				if(index==14){
					amount = $(this).html();
				}
				if(index==15){
					paymentKind = $(this).html();
				}
	      	});
	
			if(carrierType == "" || distanceType == ""){
				return false;	
			}
			
			$.ajax({ 
   				type: 'post' ,
   				url : "/allocation/update-allocation.do" ,
   				dataType : 'json' ,
   				data : {
   					inputDt : inputDt,
   					carrierType : carrierType,
   					distanceType : distanceType,
   					departureDt : departureDt,
   					departureTime : departureTime,
   					customerName : customerName,
   					carKind : carKind,
   					carIdNum : carIdNum,
   					carNum : carNum,
   					departure : departure,
   					arrival : arrival,
   					driverName : driverName,
   					carCnt : carCnt,
   					amount : amount,
   					paymentKind : paymentKind,
   					allocationId : allocId
   				},
   				success : function(data, textStatus, jqXHR)
   				{
   					var result = data.resultCode;
   					if(result == "0000"){
   						alert("변경 되었습니다.");
   					//	document.location.href = "/allocation/carrier.do";
   					}else if(result == "0001"){
   						alert("변경 하는데 실패 하였습니다.");
   					}
   				} ,
   				error : function(xhRequest, ErrorText, thrownError) {
   				}
   			});
			
		}
	
	}
	
	
	var selectCustomerObj = new Object();
	
	function selectPersonInCharge(customerId,customerName,obj){
		
		$.ajax({ 
			type: 'post' ,
			url : "/baseinfo/getDriverList.do" ,
			dataType : 'json' ,
			data : {
				customerId : customerId
			},
			success : function(data, textStatus, jqXHR)
			{
				
				$("#driverList").css('display','none');
				$("#customerList").css('display','none');
				$("#personInChargeList").css('display','');
				var list = data.resultData;
				var result = "";
				$("#personInChargeLocation").html("");
				if(list.length > 0){
	       			for(var i=0; i<list.length; i++){
	       				result += '<tr class="ui-state-default" personInChargeId="" >'; 
	       				result += '<td class="showToggle"><input type="checkbox" name="driver" driverId="\''+list[i].driver_id+'\'"></td>';
	       				result += '<td style="cursor:pointer;" onclick="javascript:setDriver(\''+list[i].driver_id+'\',\''+list[i].driver_name+'\',\'\',this);">'+list[i].driver_name+'</td>';
	       				result += '<td style="cursor:pointer;" onclick="javascript:setDriver(\''+list[i].driver_id+'\',\''+list[i].driver_name+'\',\'\',this);">'+list[i].phone_num+'</td>';
	       				result += '<td style="cursor:pointer;" onclick="javascript:setDriver(\''+list[i].driver_id+'\',\''+list[i].driver_name+'\',\'\',this);">'+list[i].car_num+'</td>';
	       				result += '<td><select class="dropdown" style="width:100%;" >';
	       				result += '<option  value="" selected>회차 선택</option>';
	       				result += '<c:forEach var="val" begin="1" end="10" step="1" varStatus="status">';
	       				result += '<option value="${val}">${val}회차</option>';
	       				result += '</c:forEach>';
	       				result += '</select></td>';
	       				result += '</tr>';

	       				$("#searchForList").css('display','none');
	       			}
				}else if(list.length == 1){
	       				//getPersonInChargeInfo(list[0].person_in_charge_id,list[0].name,list[0].phone_num,list[0].address);
				}else{
					//getPersonInChargeInfo(id,name,phone_num,address);
				}
				
				selectCustomerObj.customerId=customerId;
				selectCustomerObj.customerName=customerName;
				$("#personInChargeLocation").html(result);
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
		
	}

	
	
	function getListId(obj){
		$('html').scrollTop(0);
		selectList($(obj).parent().parent().parent().attr("allocationId"));
	}
	
	//var driverArray = new Array();
	function selectList(id){
		
		var searchWord = encodeURI('${paramMap.searchWord}');
		document.location.href = "/allocation/allocation-view.do?&searchDateType=${paramMap.searchDateType}&startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&searchType=${paramMap.searchType}&cPage=${paramMap.cPage}&allocationStatus=${paramMap.allocationStatus}&location=${paramMap.location}&allocationId="+id+"&searchWord="+searchWord;
	}	

	var selectedList = new Array();
	function batchDriver(){
		
		var id = "";
		var total = $('input:checkbox[name="forBatch"]:checked').length;

		var returnStatus = false;

		
		if(total == 0){
			alert("기사를 지정할 목록이 선택 되지 않았습니다.");
			return false;
		}else{
			$("#addPlace").html("");
			selectedList.length = 0;
			$('input:checkbox[name="forBatch"]:checked').each(function(index,element) {
			      if(this.checked){	//checked 처리된 항목의 값
			    	  var selectedObj = new Object();
			    	  id+=$(this).attr("allocationId");
			    	  if(index<total-1){
			    		  id += ","; 
				         } 
			    	  //selectedObj.carrier_type = $(this).parent().next().next().html();
			    	  
			    	  if($(this).parent().parent().attr("carrierType") == "C"){
			    		  selectedObj.carrier_type = "캐리어";

				      }else{
				    	 alert((index+1)+"번째 배차건의 배차 구분이 입력 되지 않았습니다.");
				    	 returnStatus = true;
				    	 return false;
					  }
			    	  selectedObj.customer_name = $(this).parent().next().next().html();
			    	  selectedObj.departure_dt = $(this).parent().next().next().next().next().html();
			    	  selectedObj.car_kind = $(this).parent().next().next().next().next().next().next().html();
			    	  selectedObj.car_id_num = $(this).parent().next().next().next().next().next().next().next().html();
			    	  selectedObj.car_id = $(this).parent().next().next().next().next().next().next().next().next().html();
			    	  selectedList.push(selectedObj);
			      }
			 });	

			if(!returnStatus){

				var result = "";
				for(var i = 0; i < selectedList.length; i++){
					result += '<tr class="ui-state-default">';
					result += '<td>'+selectedList[i].carrier_type+'</td>';
					result += '<td>'+selectedList[i].customer_name+'</td>';
					result += '<td>'+selectedList[i].departure_dt+'</td>';
					result += '<td>'+selectedList[i].car_kind+'</td>';
					result += '<td>'+selectedList[i].car_id_num+'</td>';
					result += '<td>'+selectedList[i].car_id+'</td>';
					result += '</tr>';
				}
				
				$("#addPlace").html(result);
				$(".car-modal-field").fadeIn();

			}
			
			//updateAllocationStatus(status,id);
		}
		
		
		
	}
	
	
	function settingDriver(){
		
		var total = $('input:checkbox[name="driver"]:checked').length;
		
		if(total == 0){
			alert("기사가 선택 되지 않았습니다.\r\n 기사명을 클릭해도 기사 선택이 가능 합니다.");
			return false;
		}else if(total > 1){
			alert("기사 선택은 1명만 가능 합니다.");
			return false;
		}else{
			$('input:checkbox[name="driver"]:checked').each(function(index,element){
				$(this).parent().next().trigger("click");
	      	});
		}
		
	}
	
	
	function driverSelect(){
		
		if(confirm(selectedList.length+"개의 목록에 기사를 지정 하시겠습니까?")){
			
			$(".car-modal-field").fadeOut();
			$(".modal-field").fadeIn();
			
		}else{
			selectedList.length = 0;
			alert("기사 지정이 취소 되었습니다.");
			$(".car-modal-field").fadeOut();
		}
		
	}
	
	function changeAllocationStatus(allocationId,val){
		
		updateAllocationStatus(val,allocationId);
		
	}
	
	
	
	function batchStatus(status){
		
		var id = "";
		var total = $('input:checkbox[name="forBatch"]:checked').length;
		
		if(total == 0){
			alert("수정할 목록이 선택 되지 않았습니다.");
			return false;
		}else{

			var inputString="";

			if(status == "X"){
				inputString = prompt('취소 사유를 작성 해 주세요.', '');

				if(inputString == null){
					return false;
				}else if( inputString == "" || inputString == undefined || ( inputString != null && typeof inputString == "object" && !Object.keys(inputString).length ) ){
					alert("취소 사유가 작성 되지 않았습니다.");
					return false;
				}else{	
					$('input:checkbox[name="forBatch"]:checked').each(function(index,element) {
					      if(this.checked){//checked 처리된 항목의 값
					    	  id+=$(this).attr("allocationId");
					    	  if(index<total-1){
					    		  id += ","; 
						         } 
					      }
					 });	
					updateAllocationStatus(status,id,inputString);
				}
				
			}else{

				$('input:checkbox[name="forBatch"]:checked').each(function(index,element) {
				      if(this.checked){//checked 처리된 항목의 값
				    	  id+=$(this).attr("allocationId");
				    	  if(index<total-1){
				    		  id += ","; 
					         } 
				      }
				 });	
				updateAllocationStatus(status,id,inputString);
			
			}


		}
	}
	
	
	function updateAllocationStatus(status,id,addMsg){
		
		var msg = "";
		if(status == "C"){
			msg = "취소";
		}else if(status == "F"){
			msg = "완료";
		}else if(status == "U"){
			msg = "미확정으로 변경";
		}else if(status == "N"){
			msg = "미배차로 변경";
		}else if(status == "X"){
			msg = "취소 요청";
		}
		
		if(confirm(msg+"하시겠습니까?")){
			$.ajax({ 
				type: 'post' ,
				url : "/allocation/updateAllocationStatus.do" ,
				dataType : 'json' ,
				data : {
					allocationStatus : status,
					allocationId : id,
					cancelReason : addMsg
				},
				success : function(data, textStatus, jqXHR)
				{
					var result = data.resultCode;
					var resultData = data.resultData;
					if(result == "0000"){
						alert(msg+"되었습니다.");
						var searchWord = encodeURI('${paramMap.searchWord}');
						document.location.href = "/allocation/carrier.do?&cPage=${paramMap.cPage}&startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&allocationStatus=${paramMap.allocationStatus}&searchType=${paramMap.searchType}&searchWord="+searchWord;
					}else if(result == "0001"){
						//alert(msg+"하는데 실패 하였습니다.");

						if(status == "X"){
							alert("계산서 발행 또는 발행 요청된 건에 대해서 취소 할 수 없습니다.");
						}else{
							alert(msg+"하는데 실패 하였습니다.");	
						}

						
					}
				} ,
				error : function(xhRequest, ErrorText, thrownError) {
				}
			});	
		}
		
	}	
			
	
	function checkAll(){

		if($('input:checkbox[id="checkAll"]').is(":checked")){
			$('input:checkbox[name="forBatch"]').each(function(index,element) {
				$(this).prop("checked","true");
			 });	
		}else{
			$('input:checkbox[name="forBatch"]').each(function(index,element) {
				$(this).prop("checked","");
			 });
		}
	}
	
	
	
	function move(location){
		
		
		window.location.href = "/allocation/"+location+".do";
		
	}	
	


function showModal(allocationId,obj){

		if($(obj).parent().parent().attr("carrierType") == "N" || $(obj).parent().parent().attr("carrierType") == ""){
			alert("배차 구분이 선택 되지 않았습니다.")
			return false;
		} 
		$('.modal-field').attr("allocationId",allocationId);
		$("#forCustomer").css("display","none");
		$('.modal-field').show();

}

	function setDriver(driverId,drivername,carKind,obj){
		
		
		if($(obj).parent().children().last().children().eq(0).val() != ""){
			var driverCnt = $(obj).parent().children().last().children().eq(0).val();
			var allocationId = $('.modal-field').attr("allocationId");
			if(allocationId == "" && selectedList.length > 0){					//일괄기사 지정을 하는 경우
				var id = "";
				var total = $('input:checkbox[name="forBatch"]:checked').length;
				$('input:checkbox[name="forBatch"]:checked').each(function(index,element) {
				      if(this.checked){	//checked 처리된 항목의 값
				    	  var selectedObj = new Object();
				    	  id+=$(this).attr("allocationId");
				    	  if(index<total-1){
				    		  id += ","; 
					         } 
				      }
				 });
				allocationId = id; 
			}
			//alert(selectedList.length);
			
			selectDriver(allocationId,driverId,drivername,driverCnt);
			$('.modal-field').attr("allocationId","");
			$('.modal-field').hide();	
		}else{
			alert(" 선택한 기사의 회차가 지정되지 않았습니다.");
			return false;
		}
		
	}	

	function selectDriver(allocationId,driverId,drivername,driverCnt){
		
		
		if(confirm(drivername+"기사를 지정 하시겠습니까?")){
			$.ajax({ 
				type: 'post' ,
				url : "/allocation/setDriver.do" ,
				dataType : 'json' ,
				data : {
					allocationId : allocationId,
					driverId : driverId,
					driverCnt : driverCnt,
					customerInfo : JSON.stringify(selectCustomerObj)
				},
				success : function(data, textStatus, jqXHR)
				{
					var result = data.resultCode;
					var resultMsg = data.resultMsg;
					var resultData = data.resultData;
					if(result == "0000"){
						//alert("기사를 지정 하였습니다.");
						alert("기사를 지정 하였습니다.\r\n 결제 정보(업체청구액,기사지급액)가 작성되지 않은경우 미배차 상태에서 진행 할 수 없습니다.");
						var searchWord = encodeURI('${paramMap.searchWord}');
						document.location.href = "/allocation/carrier.do?cPage=${paramMap.cPage}&searchDateType=${paramMap.searchDateType}&startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&allocationStatus=${paramMap.allocationStatus}&searchType=${paramMap.searchType}&searchWord="+searchWord;
					}else{
						alert(resultMsg);
					}
				} ,
				error : function(xhRequest, ErrorText, thrownError) {
				}
			});	
		}else{
			selectCustomerObj = new Object();	
		}
		
		
	}
	
	
	
	var order = "${order}";
	function sortby(gubun){

		if(order == "" || order == "desc"){
			order = "asc";
		}else{
			order = "desc";
		}
		
		var loc = document.location.href;
		var str = "";
		if(loc.indexOf("?") > -1){
			//forOrder 가 있는경우 ㅎㅎ
			if(loc.indexOf("forOrder") > -1){
				var queryString = loc.split("?");
				var query = queryString[1].split("&");
				
				for(var i = 0; i < query.length; i++){
					if(query[i].indexOf("forOrder") > -1){
						query[i] = "forOrder="+gubun+"%5E"+order;
					}
				}
				for(var i = 0; i < query.length; i++){
					if(query[i] != ""){
						str += "&"+query[i];	
					}
				}
				document.location.href = queryString[0]+"?"+str;
			}else{
				str="&forOrder="+gubun+"%5E"+order;
				document.location.href = loc+str;
			}
			
		}else{
			str="?&forOrder="+gubun+"%5E"+order;
			document.location.href = loc+str;
		}
		
	}	
	
	function view(gubun){
		
		selectCustomerObj = new Object();
		
		$("#forCustomer").css("display","none");
		
		if(gubun == "driver"){
			$("#driverList").css("display","");
			$("#customerList").css("display","none");
			$("#personInChargeList").css("display","none");
		}else{
			$("#driverList").css("display","none");
			$("#customerList").css("display","");
			$("#personInChargeList").css("display","none");
			$("#forCustomer").css("display","");
		}
		
	}
	

	
	function clipBoardCopy(allocationId,obj){
		  
		  $.ajax({ 
				type: 'post' ,
				url : "/allocation/selectAllocationInfo.do" ,
				dataType : 'json' ,
				data : {
					allocationId : allocationId
				},
				success : function(data, textStatus, jqXHR)
				{
								
					var result = data.resultCode;
					var resultMsg = data.resultMsg;
					var resultData = data.resultData;
					var resultDataSub = data.resultDataSub;
					if(result == "0000"){
						var t = document.createElement("textarea");
						t.setAttribute("id", "forClipboard");
						document.body.appendChild(t);		
						
						
						var clipBoardText = "";
						var departure_time = "";

						if(resultDataSub.departure_time != undefined){
							departure_time = resultDataSub.departure_time; 
						}
						
						if (typeof resultData.department == "undefined") {
							clipBoardText += ""+resultDataSub.departure_dt_str+"("+resultDataSub.departure_dt_d_o_w+") "+departure_time+"      "+resultData.customer_name+"\r\n";	
						}else{
							clipBoardText += ""+resultDataSub.departure_dt_str+"("+resultDataSub.departure_dt_d_o_w+") "+departure_time+"      "+resultData.department+" "+resultData.customer_name+"\r\n";	
						}
						
						if (typeof resultData.charge_name != "undefined") {
							clipBoardText += ""+resultData.charge_name+" "+resultData.charge_phone+"\r\n";	
						}
						
						clipBoardText += ""+resultDataSub.car_kind+" "+resultDataSub.car_id_num+" "+resultDataSub.car_num+"\r\n";
						
						//alert(resultDataSub.departure_addr);
						
						clipBoardText += "상차 "+resultDataSub.departure+" "+resultDataSub.departure_addr+"\r\n";
						if(resultDataSub.departure_person_in_charge != "" || resultDataSub.departure_phone != ""){
							clipBoardText += ""+resultDataSub.departure_person_in_charge+" "+resultDataSub.departure_phone+"\r\n";	
						}
						clipBoardText += "하차 "+resultDataSub.arrival+" "+resultDataSub.arrival_addr+"\r\n";
						if(resultDataSub.arrival_person_in_charge != "" || resultDataSub.arrival_phone != ""){
							clipBoardText += ""+resultDataSub.arrival_person_in_charge+" "+resultDataSub.arrival_phone+"\r\n";	
						}
						
						clipBoardText += ""+resultDataSub.etc
						t.value = clipBoardText;
						t.select();
						document.execCommand('copy');
						$(".clipboard").attr("data-clipboard-target","#forClipboard");
						document.body.removeChild(t);
						alert("배차 정보가 클립 보드에 복사 되었습니다.");
					}else{
						alert("배차 정보를 가져 오는데 실패 했습니다.");
					}
				} ,
				error : function(xhRequest, ErrorText, thrownError) {
				}
			});
		  
	}
	
	
	function viewDriverLocation(){
		
		window.open("/allocation/viewDriverLocation.do","_blank","top=0,left=0,width=800,height=800,toolbar=0,status=0,scrollbars=1,resizable=0");
		
	}
	
	function excelDownload(carrierType){
		
		document.location.href = "/allocation/excel_download.do?allocationStatus=${paramMap.allocationStatus}&carrierType="+carrierType+"&startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val()+"&searchDateType="+$("#searchDateType").val()+"&searchWord="+encodeURI($("#searchWord").val());
		//document.location.href = "/allocation/excel_download.do?carrierType="+carrierType;
	}	




	function searchForModify(searchWord) { 

		$("#textWord").val("");
	 		
		
			   $.ajax({

			       type:'post',
			       url :'/allocation/searchForModify.do',
			       dataType :'json',
			       data : {

			    	   searchWord : searchWord

			          },
			          success : function(data, textStatus, jqXHR){

			         
	                 $("#driverList").css('display','none');
	                 $("#customerList").css('display','none');
	                 $("#personInChargeList").css('display','none');
	                 $("#searchForList").css('display','');
					 $('.modal-field').show(); 
				            
			            	 var result = data.resultCode;
						     var list = data.resultData;

			            	$("#searchText").html("");
			            	
			                if(list.length > 0){
			                    for(var i=0; i<list.length; i++){
			                    	result += '<tr class="ui-state-default" searchForListId="" >'; 
			           				result += '<td class="showToggle"><input type="checkbox" name="forBatch" driverId="\''+list[i].driver_id+'\'"></td>';
			           				result += '<td style="cursor:pointer;" onclick="javascript:selectPersonInCharge(\''+list[i].customer_id+'\',\''+list[i].customer_name+'\',\'\',this);">'+list[i].customer_name+'</td>';
			           				result += '<td style="cursor:pointer;" onclick="javascript:selectPersonInCharge(\''+list[i].customer_id+'\',\''+list[i].customer_name+'\',\'\',this);">'+list[i].customer_owner_name+'</td>';
			           				result += '<td style="cursor:pointer;" onclick="javascript:selectPersonInCharge(\''+list[i].customer_id+'\',\''+list[i].customer_name+'\',\'\',this);">'+list[i].phone+'</td>';
			           				result += '</td>';
			           				result += '</tr>';
				       			     
			                    }
			                }else if(list.length == 1){

				            }else {

									alert("없는 이름입니다. 다시입력해주세요");
									
					            }
							$("#searchText").html(result);
			     
			          },error :function(xhRequest, ErrorText, thrownError){
			                  alert(ErrorText);
			               }
			         });
			 
			}

	

	 function press_enter(){

		if(window.event.keyCode==13){	
			$("#searchBtn").click();
				}	
		 }

	 function getCheckBoxCnt(){ //체크박스 개수 구하기


			var checkboxCnt =$('input:checkbox[name="forBatch"]:checked').length;
			var checkbox = false;	
			//var test2 = $("#checkedChk_box").get(0);
			for(var i=0; i<checkboxCnt; i++){
				$("#checkedChk_box").text(checkboxCnt);			
			}

			
		}
	 

	
 </script>
<div class="modal-field" allocationId="">
            <div class="modal-box">
                <h3 class="text-center">리스트</h3>
                    <div style="text-align:left;margin-leftt:30px; " id="forCustomer">
             		 <input type ="text" placeholder="거래처명/대표자명/연락처" id="textWord" onkeypress="press_enter()"> 
            		  <input type="button" id="searchBtn" value="검색" class="btn btn-primary" onclick ="javascript:searchForModify($(this).prev().val())"> 
           		     </div>
                <div style="text-align:right; margin-right:30px;"><a onclick="javascript:view('driver');" style="cursor:pointer;">기사선택</a>&nbsp;/&nbsp;<a onclick="javascript:view('customer');" style="cursor:pointer;">거래처선택</a></div>
            <div class="modal-table-container">
                <table class="article-table" id="driverList">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <td class="showToggle"><input type="checkbox" id="" onclick="" ></td>
                            <td>기사명</td>
                            <td>연락처</td>
                            <td>차량번호</td>
                            <td>차종</td>
                        </tr>
                    </thead>
                    <tbody id="">
                    <c:forEach var="data" items="${driverList}" varStatus="status" >
                    		<tr class="ui-state-default" driverId="${data.driver_id}"> 
                    			<td class="showToggle"><input type="checkbox" name="driver" driverId="${data.driver_id}"></td>
	                            <td style="cursor:pointer;" onclick="javascript:setDriver('${data.driver_id}','${data.driver_name}','${data.car_kind}',this);" >${data.driver_name}</td>
	                            <td style="cursor:pointer;" onclick="javascript:setDriver('${data.driver_id}','${data.driver_name}','${data.car_kind}',this);" >${data.phone_num}</td>
	                            <td style="cursor:pointer;" onclick="javascript:setDriver('${data.driver_id}','${data.driver_name}','${data.car_kind}',this);" >${data.car_num}</td>
	                            <td style="cursor:pointer;" onclick="javascript:setDriver('${data.driver_id}','${data.driver_name}','${data.car_kind}',this);" >${data.car_kind}</td>
	                            <td><select class="dropdown" style="width:100%;" >
							        	<option  value="" selected>회차 선택</option>
							        	<c:forEach var="val" begin="1" end="10" step="1" varStatus="status">
										    	<option value="${val}"  <c:if test='${data.driver_cnt == val}'>selected</c:if>>${val}회차</option>
										</c:forEach>
							        </select>
							     </td>
                        	</tr>
						</c:forEach>
                    </tbody>
                </table>
                <table class="article-table" id="customerList">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <!-- <td>소유주</td> -->
                            <td>거래처명</td>
                            <td>대표자명</td>
                            <td>연락처</td>
                        </tr>
                    </thead>
                    <tbody id="">
                    	<c:forEach var="data" items="${customerList}" varStatus="status" >
                    		<tr class="ui-state-default" customerId="${data.customer_id}" > 
	                            <%-- <td>${data.driver_owner}</td> --%>
	                            <td style="cursor:pointer;" onclick="javascript:selectPersonInCharge('${data.customer_id}','${data.customer_name}',this);">${data.customer_name}</td>
	                            <td style="cursor:pointer;" onclick="javascript:selectPersonInCharge('${data.customer_id}','${data.customer_name}',this);">${data.customer_owner_name}</td>
	                            <td style="cursor:pointer;" onclick="javascript:selectPersonInCharge('${data.customer_id}','${data.customer_name}',this);">${data.phone}</td>
                        	</tr>
						</c:forEach>
                    </tbody>
                </table>
                
                <table class="article-table" id="personInChargeList">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                        	<td class="showToggle"><input type="checkbox" id="" onclick="" ></td>
                            <td>기사명</td>
                            <td>연락처</td>
                            <td>차량번호</td>
                            <td>회차</td>
                        </tr>
                    </thead>
                    <tbody id="personInChargeLocation">
                    </tbody>
                </table>
                
                
                        <table class="article-table"  id="searchForList">
                    <colgroup>
                        
                    </colgroup>
                     <thead>
                        <tr>
                        	<td class="showToggle"><input type="checkbox" id="checkAll" onclick="javascript:checkAll();" ></td>
                            <td>거래처명</td>
                            <td>대표자명</td>
                            <td>연락처</td>
                        </tr>
                    </thead>
                    <tbody id="searchText">
          
                    </tbody>
                </table>
                
                
                
                
                </div>
                <div class="confirmation">
                	<div class="confirm">
                        <input type="button" onclick="javascript:settingDriver();" value="확인" >
                    </div>
                    <div class="cancel">
                        <input type="button" value="취소" name="" onclick="javascript:$('.modal-field').attr('allocationId',''); $('.modal-field').fadeOut(); return false;">
                    </div>
                </div>
            </div>
        </div>

<div class="car-modal-field" allocationId="">
            <div class="car-modal-box">
                <h3 class="text-center">기사 지정 리스트</h3>
            <div class="car-modal-table-container">
                <table class="article-table">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <td>배차구분</td>
                            <td>고객명</td>
                            <td>출발일</td>
                            <td>차종</td>
                            <td>차대번호</td>
                            <td>차량번호</td>
                        </tr>
                    </thead>
                    <tbody id="addPlace">
                   		<tr class="ui-state-default"> 
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                       	</tr>
                    </tbody>
                </table>
                </div>
                <div class="confirmation">
                    <div class="confirm">
                        <input type="button" value="확인" name="" onclick="javascript:driverSelect();">
                    </div>
                    <div class="cancel">
                        <input type="button" value="취소" name="" onclick="javascript: selectedList.length = 0; selectCustomerObj = new Object(); $('.car-modal-field').fadeOut(); return false;">
                    </div>
                </div>
            </div>
        </div>
 		
 		
 		<%-- <section class="side-nav">
            <ul>
                <li><a href="/allocation/combination.do">신규배차입력 </a></li>
                <li><a href="/allocation/self.do">셀프 배차  </a></li>
                <li><a href="/allocation/self.do?reserve=N">셀프 예약</a></li>
                <li><a href="/allocation/self.do?reserve=Y">셀프 배차 현황</a></li>
                <li class="<c:if test="${reserve == ''}">active</c:if>"><a href="/allocation/carrier.do">캐리어 배차</a></li>
                <li class="<c:if test="${reserve == 'N'}">active</c:if>"><a href="/allocation/carrier.do?reserve=N">캐리어 예약</a></li>
                <li class="<c:if test="${reserve == 'Y'}">active</c:if>"><a href="/allocation/carrier.do?reserve=Y">캐리어 배차 현황</a></li>
                <li><a href="/allocation/combination.do?forOpen=N&complete=Y">완료 배차</a></li>
            </ul>
        </section> --%>

		<a href="javascript:;" class="clipboard"  data-clipboard-target="#txt_MyLink"></a>


        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">HOME</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">캐리어 배차</a></li>
                </ul>
            </div>

            <div class="up-dl clearfix">
                <form name="searchForm" method="get" action="carrier.do">
                	<div class="date-picker">
                		<select name="searchDateType" id="searchDateType">
							<option value="" <c:if test='${paramMap.searchDateType eq "" }'> selected="selected"</c:if> >선택</option>
							<option value="S" <c:if test='${paramMap.searchDateType eq "S" }'> selected="selected"</c:if> >의뢰일</option>
							<option value="D" <c:if test='${paramMap.searchDateType eq "D" }'> selected="selected"<</c:if> >출발일</option>
						</select>
	                	<input style="width:150px;" autocomplete="off" id="startDt" placeholder="검색 시작일" name="startDt" type="text" class="datepick" value="${paramMap.startDt}"> ~ <input style="width:150px;" autocomplete="off" id="endDt" name="endDt"  placeholder="검색 종료일" type="text" class="datepick" value="${paramMap.endDt}">
	                </div>
					<div class="btns-submit">	
						<div class="right">
							<select name="searchType">
								<option value="" <c:if test='${paramMap.searchType eq "" }'> selected="selected"</c:if> >선택</option>
								<option value="total" <c:if test='${paramMap.searchType eq "total" }'> selected="selected"</c:if> >통합검색</option>
								<%-- <option value="distance" <c:if test='${paramMap.searchType eq "distance" }'> selected="selected"<</c:if> >운행구분</option> --%>
								<option value="customer" <c:if test='${paramMap.searchType eq "customer" }'> selected="selected"<</c:if> >고객명</option>
								<option value="driver" <c:if test='${paramMap.searchType eq "driver" }'> selected="selected"<</c:if> >기사명</option>
								<option value="carIdNum" <c:if test='${paramMap.searchType eq "carIdNum" }'> selected="selected"<</c:if> >차대번호</option>
								<option value="carNum" <c:if test='${paramMap.searchType eq "carNum" }'> selected="selected"<</c:if> >차량번호</option>
								<option value="contractNum" <c:if test='${paramMap.searchType eq "contractNum" }'> selected="selected"<</c:if> >계약번호</option>			
								<option value="buyer" <c:if test='${paramMap.searchType eq "buyer" }'> selected="selected"<</c:if> >매입처명</option>
							</select>
							<input type="hidden" name="allocationStatus" value="${paramMap.allocationStatus}" />
							<input type="text" name="searchWord" value="${paramMap.searchWord}"  placeholder="검색어 입력"  onkeypress="if(event.keyCode=='13'){ encodeURI($(this).val()); document.searchForm.submit();}" />
							<a class="btn-gray" href="javascript:" onclick=" encodeURI($(this).prev().val()); document.searchForm.submit()"><input type="button" value="검색"></a>
							<c:if test="${user.dept != 'sunbo'}">
								<div class="upload-btn right" style="float:right;">
				                    <input type="button"  onclick="javascript:excelDownload('C');" value="엑셀 다운로드">
				                    <c:if test="${order == null || order == ''}">
			                    		<input type="button" value="리스트 순서 변경 활성화" onclick="javascript:listOrderChangePossible(this);">
			                    		<input type="button" value="리스트 순서 변경" onclick="javascript:listOrderChange();" style="display:none;">
			                    	</c:if>
				                </div>
			                </c:if>
						</div>
						
					</div>
								
				</form>
            </div>

            <div class="dispatch-btn-container">
                <!-- <div class="dispatch-btn">
                    <i class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div> -->
                 <c:if test="${paramMap.allocationStatus == 'N'}">
					<div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">캐리어 미배차</div>					
				</c:if>
                <c:if test="${paramMap.allocationStatus == 'Y'}">
					<div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">캐리어 배차</div>					
				</c:if>
                <c:if test="${paramMap.allocationStatus == 'F'}">
					<div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">캐리어 완료</div>					
				</c:if>
            </div>
            
            	   ※ 경기사업자 배차건들은 <span style="color:#9A2EFE;"> 보라색</span>으로 표시 됩니다. <br>
            	 ※ 탁송완료된 배차건중에 미결제된 배차건은 상태에<span style="color:#EE0D0D;"> 빨간색</span>으로 표시 됩니다.
        </section>

        <div class="dispatch-wrapper">
            <%-- <jsp:include page="common-form.jsp"></jsp:include> --%>
            <section class="dispatch-bottom-content">
			</section>
            <section class="bottom-table" style="width:1600px; margin-left:10px;">
                <table class="article-table forToggle" id="dataTable" style="width:100%; table-layout:fixed;">
                    <colgroup>
                         <col width="40px;">
                        <col width="120px;">
                        <col width="150px;">
                        <col width="auto">
                        <col width="auto">
                        <col width="auto">
                        <col width="auto">
                        <col width="auto">
                        <col width="auto">
                        <col width="auto">
                        <col width="180px;">
                        <col width="auto">
                        <col width="auto">
                        <col width="auto">
                        <col width="100px;">
                        <col width="100px;">
                    </colgroup>
                    <thead>
                        <tr>
                            <td class="showToggle"><input type="checkbox" id="checkAll" onclick="javascript:checkAll();" ></td>
                            <!-- <td class="showToggle">번호</td> -->
                            <td style="cursor:pointer;" onclick="javascript:sortby('A');">의뢰일 </td>
                            <!-- <td style="cursor:pointer;" onclick="javascript:sortby('B');">배차구분</td>
                            <td style="cursor:pointer;" onclick="javascript:sortby('C');">운행구분</td> -->
                            <td style="cursor:pointer;" onclick="javascript:sortby('D');">고객명</td>
                            <td style="cursor:pointer;">담당자명</td>
                            <c:if test="${user.dept == 'sunbo'}">
                            	<td onclick="">고객구분</td>
                            </c:if>
                            <td style="cursor:pointer;" onclick="javascript:sortby('E');">출발일</td>
                            <td style="cursor:pointer;" onclick="javascript:sortby('F');">출발시간</td>
                            <td style="cursor:pointer;" onclick="javascript:sortby('G');">차종</td>
                            <td style="cursor:pointer;" onclick="javascript:sortby('H');">차대번호</td>
                            <td style="cursor:pointer;" onclick="javascript:sortby('I');">차량번호</td>
                            <td style="cursor:pointer;">계약번호</td>
                            <td style="cursor:pointer;" onclick="javascript:sortby('J');">출발지</td>
                            <td style="cursor:pointer;" onclick="javascript:sortby('K');">하차지</td>
                            <c:if test="${user.dept != 'sunbo'}">
                            <td style="cursor:pointer;" onclick="javascript:sortby('L');">비고</td>
                            	<td>업체청구금액</td>
                            	<c:if test="${paramMap.allocationStatus == 'F'}">
                            		<td>기사지급액</td>
                            	</c:if>
                            	<td><c:if test="${paramMap.allocationStatus == 'N'}">기사지정</c:if><c:if test="${paramMap.allocationStatus != 'N'}">기사명(회차)</c:if></td>
                            </c:if>
                            <td style="cursor:pointer;" onclick="javascript:sortby('M');">상태</td>
                        </tr>
                    </thead>
                    <tbody id="sortable">
                    	<c:forEach var="data" items="${listData}" varStatus="status">
							<tr class="ui-state-default" list-order="${data.list_order}" allocationId="${data.allocation_id}"  carrierType="${data.carrier_type}">
                            <td class="showToggle"><input type="checkbox" name="forBatch" allocationId="${data.allocation_id}" onclick="javascript:getCheckBoxCnt();"></td>
                            <%-- <td class="showToggle">${data.list_order}</td> --%>
                            <td style="<c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>">${data.input_dt}</td>
                            <%-- <td>${data.allocation_division}</td>
                            <td>${data.run_division}</td> --%>
                            <td style="cursor:pointer; <c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>" onclick="javascript:selectList('${data.allocation_id}');">
                            
                               <c:if test ="${data.company_id eq 'company2'}">
	                            ${data.customer_name} 
	                            </c:if>
	                         
	                           <c:if test ="${data.company_id eq 'company1'}">
	                         	<span style="font-weight: bold; color: #9A2EFE;" >   ${data.customer_name} </span>
	                            </c:if>
                            
                            
                            </td>
                            <td style="cursor:pointer; <c:if test="${data.batch_status == 'P'}">color:#00f;</c:if>" onclick="javascript:selectList('${data.allocation_id}');">${data.charge_name}</td>
                            <c:if test="${user.dept == 'sunbo'}">
                            	<td>${data.charge_dept}</td>
                            </c:if>
                            <td style="<c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>">${data.departure_dt}</td>
                            <td style="<c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>">${data.departure_time}</td>
                            <td style="<c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>">${data.car_kind}</td>
                           	<c:if test="${data.driver_mod_yn == 'Y'}">
                            	<td style="color:#F00;">${data.car_id_num}</td>
                            </c:if>
                            <c:if test="${data.driver_mod_yn == 'N' && data.mod_emp != null && data.mod_emp != ''}">
                            	<td style="color:#00F;">${data.car_id_num}</td>
                            </c:if>
                            <c:if test="${data.driver_mod_yn == 'N' && (data.mod_emp == null || data.mod_emp == '')}">
                            	<td>${data.car_id_num}</td>
                            </c:if>
                            <%-- <td style="<c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>">${data.car_id_num}</td> --%>
                            <td style="<c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>">${data.car_num}</td>
                            <td style="<c:if test="${data.batch_status == 'P'}">color:#F00;</c:if> white-space:nowrap; text-overflow:ellipsis; overflow:hidden;" >${data.contract_num}</td>
                            <td style="<c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>">${data.departure}</td>
                            <td style="<c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>">${data.arrival}</td>
                            <%-- <td>${data.amount}</td> --%>
                            <c:if test="${user.dept != 'sunbo'}">
                            	<td style=" text-overflow:ellipsis; overflow:hidden; <c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>"><nobr>${data.etc}</nobr></td>
                            	<%-- <td style="<c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>"><fmt:formatNumber value="${data.amount}" groupingUsed="true"/></td> --%>
                            	<td style="<c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>">
                           			 <c:if test="${user.hidePrice == 'Y' }">
	                            	</c:if>
                            		<c:if test="${user.hidePrice != 'Y' }">
	                            		${data.sales_total}
	                            	</c:if>
                            	</td>
                            	<c:if test="${paramMap.allocationStatus == 'F'}">
                            		<td style="<c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>">${data.driver_amount}</td>
                            	</c:if>
                            	<td style="text-align:center;">
	                            	<c:if test="${paramMap.allocationStatus == 'N' or paramMap.allocationStatus == 'U'}"><a style="cursor:pointer; width:50px;" onclick="javascript:showModal('${data.allocation_id}',this);" class="btn-edit">지정</a></c:if>
	                            	<c:if test="${paramMap.allocationStatus != 'N' and paramMap.allocationStatus != 'U'}">${data.driver_status}${data.driver_name}(${data.driver_cnt})</c:if>
	                            </td>
                            </c:if>
                            
                            <%-- <td>${data.car_cnt}</td> --%>
                            <%-- <td>${data.amount}</td> --%>
                            <%-- <td>${data.driver_amount}</td> --%>
                            <%-- <c:if test="${data.payment == '00'}">
									<td>결제</td>
								</c:if>
	                            <c:if test="${data.payment == '01'}">
									<td>미결제</td>
								</c:if>
								<c:if test="${data.payment != '01' and data.payment != '00'}">
									<td></td>
								</c:if>
                            <td class="showToggle">${data.billing_division}</td> --%>
                            <td>
                            <c:if test="${user.dept == 'sunbo'}">
                            	${data.allocation_status_name}
                            </c:if>
                            <c:if test="${user.dept != 'sunbo'}">
                            	<c:if test="${data.allocation_status == 'N' or data.allocation_status == 'U'}">
	                            	<select name="forAllocationStatus" onchange="javascript:changeAllocationStatus('${data.allocation_id}',this.value)">
										<option value="U" <c:if test='${data.allocation_status eq "U" }'> selected="selected"</c:if> >미확정</option>
										<option value="N" <c:if test='${data.allocation_status eq "N" }'> selected="selected"</c:if> >미배차</option>
									</select>
								</c:if>
								<c:if test="${data.allocation_status != 'N' and data.allocation_status != 'U'}">
									  <c:if test="${data.allocation_status == 'F'}">
									  	<c:if test="${data.payment == 'Y'}">
												${data.allocation_status_name}
										</c:if>
											<c:if test="${data.payment != 'Y'}">
											 	<c:if test="${data.payment_kind != 'DD'}">
													<span style="color:#EE0D0D;">${data.allocation_status_name}</span>
												</c:if>
											
												<c:if test="${data.payment_kind == 'DD'}">
														${data.allocation_status_name}
												</c:if>
											</c:if>
										</c:if>
											
									<c:if test="${data.allocation_status != 'F'}">
										${data.allocation_status_name}
									</c:if>
									
								</c:if>
								<span><img style="width:15px; height:15px; cursor:pointer;" src="/img/question-icon.png" onclick="javascript:clipBoardCopy('${data.allocation_id}',this);" alt=""  title='비고 : ${data.etc}&#10;' ></span>
                            </c:if>
							</td>
                        </tr>
						</c:forEach>

                    </tbody>
                </table>
                <!-- <div style="color:#8B0000;">※차대번호는 기사님이 입력한 경우에는 붉은색으로 표시 되고 배차 직원이 수정 한 경우에는 파란색으로 표시 됩니다.</div> -->
                <div style="">※차대번호는 기사님이 입력한 경우에는 <span style="color:#f00;">붉은색</span>으로 표시 되고 배차 직원이 수정 한 경우에는  <span style="color:#00f;">파란색</span>으로 표시 됩니다.</div>
                <div class="table-pagination text-center">
                    <ul class="pagination">
                    	<html:paging uri="/allocation/carrier.do" forGroup="&startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&searchDateType=${paramMap.searchDateType}&allocationStatus=${paramMap.allocationStatus}&forOrder=${paramMap.forOrder}%5E${paramMap.order}" frontYn="N" />
                    </ul>
                </div>
            </section>
            <c:if test="${user.dept != 'sunbo' && paramMap.allocationStatus != 'F'}">
            	<div class="confirmation">
                    <div class="cancel">
                        <a style="cursor:pointer;" onclick="javascript:batchStatus('X');">취소요청</a>
                    </div>
                    <div class="confirm">
                    	<c:if test="${paramMap.allocationStatus == 'N'}"><a style="cursor:pointer;" onclick="javascript:batchDriver();">일괄기사지정</a></c:if>
                    	<c:if test="${paramMap.allocationStatus != 'N'}"><a style="cursor:pointer;" onclick="javascript:batchStatus('F');">일괄완료</a></c:if>
                        
                    </div>
                </div>
                	<div>체크된 항목의 갯수 :
	                		<span id ="checkedChk_box" ></span>   
	                </div>
             </c:if>
        </div>           
		<script>
		
		if("${userMap.control_grade}" == "01"){
			$('#dataTable').SetEditable({
            	columnsEd: "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15",
            	 onEdit: function(row) {updateAllocaion(row)},  
            	 onDelete: function() {},  
            	 onBeforeDelete: function() {}, 
            	 onAdd: function() {}  
            });
		}
                       
            
            </script>
