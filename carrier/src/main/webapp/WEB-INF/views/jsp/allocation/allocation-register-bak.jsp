<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>

<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/js/bootstable.js"></script>
<script type="text/javascript"> 
var addInfo = "";
var firstPayment = "";
var secondPayment = "";
var thirdPayment = "";
$(document).ready(function(){
	
	addInfo = $("#forAddAllocation").html();
	paymentInfoSetting();
		
	$("#companyInfoMenu li a").click( function() {
	    $("#companyInfo").val($(this).text());
	});
	/* $("#carCntMenu li a").click( function() {
	    $("#carCnt").val($(this).text());
	}); */
	
	
	/* $("#carrierTypeMenu li a").click( function() {
	    $("#carrierType").val($(this).text());
	});
	$("#distanceTypeMenu li a").click( function() {
	    $("#distanceType").val($(this).text());
	});
	$("#driverInfoMenu li a").click( function() {
	    $("#driverName").val($(this).text());
	}); */
	
	
	$("#paymentKindMenu li a").click( function() {
	    $("#paymentKind").val($(this).text());
	});
	$("#paymentMenu li a").click( function() {
	    $("#payment").val($(this).text());
	});
	$("#paymentAccountNumberMenu li a").click( function() {
	    $("#paymentAccountNumber").val($(this).text());
	});
	$("#driverPaymentKindMenu li a").click( function() {
	    $("#driverPaymentKind").val($(this).text());
	});
	$("#driverPaymentMenu li a").click( function() {
	    $("#driverPayment").val($(this).text());
	});
	$("#companyPaymentKindMenu li a").click( function() {
	    $("#companyPaymentKind").val($(this).text());
	});
	$("#companyPaymentMenu li a").click( function() {
	    $("#companyPayment").val($(this).text());
	});
	
	
	$("#departureDtf").val("${paramMap.startDt}");
	
	
	
	
	$('#datepicker1').click(function(){
		$( "#datepicker1" ).val("");   	
	});

	$('.fromdate').click(function(){
		$( "#datepicker1" ).mousedown();   	
		$( "#datepicker1" ).focus();   	
	});
	
	
	 $("#datepicker1").datepicker('setDate', "${paramMap.startDt}");
	
	var maxDate = new Date();
    $( "#datepicker1" ).datepicker({
  	  dateFormat : "yy-mm-dd",
  	  maxDate : maxDate,
  	  onClose: function(e) {    
  		var date = new Date($("#datepicker1").datepicker({ dateFormat: 'yy-mm-dd' }).val()),	week = new Array('일', '월', '화', '수', '목', '금', '토');
  		if (week[date.getDay()]!= undefined){
    		$("#datepicker1").val($("#datepicker1").val()+" "+"("+(week[date.getDay()])+")"); 
    	}
        } 
    });
    
    $(document).find("input[name=cAcqDate]").removeClass('hasDatepicker').datepicker({
    	dateFormat : "yy-mm-dd",
    	  maxDate : maxDate,
    	  onClose: function( selectedDate ) {    

          } 
    });
	
    //$("#companyList").focus();
    
    
});

var driverArray = new Array();
var carInfoList = new Array();
var paymentInfoList = new Array();
var driverList = new Array();
var rgx1 = /\D/g;  // /[^0-9]/g 와 같은 표현
var rgx2 = /(\d+)(\d{3})/; 

function getNumberSec(value){
	$("#profitf").val(setComma(value));
}

function getNumberTh(value){
	$("#billForPaymentf").val(setComma(value));
}

function getNumber(obj){
	
     var num01;
     var num02;
     num01 = obj.value;
     num02 = num01.replace(rgx1,"");
     num01 = setComma(num02);
     obj.value =  num01;
     if($("#amountf").val() != "" && $("#companyBillForPaymentf").val() != "" && $("#billForPaymentf").val() != ""){
    	var pay1 = $("#amountf").val().replace(/[^0-9]/gi,"");
    	var pay2 = $("#companyBillForPaymentf").val().replace(/[^0-9]/gi,"");
    	var pay3 = $("#billForPaymentf").val().replace(/[^0-9]/gi,"");
    	$("#profitf").val(Number(pay1)-Number(pay2)-Number(pay3));
    	getNumberSec($("#profitf").val());
     }
     if($("#amountPaidf").val() != "" && $("#deductionRatef").val()){
    	var pay1 = $("#amountPaidf").val().replace(/[^0-9]/gi,"");
     	var pay2 = $("#deductionRatef").val().replace(/[^0-9]/gi,"");
     	$("#billForPaymentf").val(Number(pay1)-((Number(pay1)/100)*Number(pay2)));
     	getNumberTh($("#billForPaymentf").val());
     }
     
/*      if($(obj).attr("id") == "carCntf"){
    	 if(Number($(obj).val()) > 1){
    		 $("#carKindf").val("");
    		 $("#carIdNumf").val("");
    		 $("#carNumf").val("");
    		 $("#contractNumf").val("");
    		 $("#carKindf").attr("disabled",true);
    		 $("#carIdNumf").attr("disabled",true);
    		 $("#carNumf").attr("disabled",true);
    		 $("#contractNumf").attr("disabled",true);
    	 }else{
    		 $("#carKindf").attr("disabled",false);
    		 $("#carIdNumf").attr("disabled",false);
    		 $("#carNumf").attr("disabled",false);
    		 $("#contractNumf").attr("disabled",false);
    	 }    	 
     }  */ 
     
}

function setComma(inNum){
     
     var outNum;
     outNum = inNum; 
     while (rgx2.test(outNum)) {
          outNum = outNum.replace(rgx2, '$1' + ',' + '$2');
      }
     return outNum;

}


function paymentInfoSetting(){
	
	
	firstPayment += '<tr payment_division="01">'; 
    firstPayment += '<td class="notFirst widthAuto"  style="width:100px;">';
    firstPayment += '<form action="">';																
    firstPayment += '<input type="text" class="search-box" placeholder="매출처" name="findCustomerForPayment" id="">'; 		
    firstPayment += '<input type="hidden" name="paymentCustomerId">';
    firstPayment += '<i class="fa fa-spinner fa-spin fa-fw"></i>';
    firstPayment += '<div class="search-result" id="searchResult">';
    firstPayment += '</div>';
    firstPayment += '</form>';
    firstPayment += '</td>';
    firstPayment += '<td style="width: 145px;">';
	firstPayment += '<div class="select-con">';
	firstPayment += '<select class="dropdown">';
	firstPayment += '<option  value=""  >결제방법</option>';
	firstPayment += '<option  value="00"  >계좌이체</option>';
	firstPayment += '<option  value="01"  >현금</option>';
	firstPayment += '<option  value="02"  >현금영수증</option>';
	firstPayment += '<option  value="03"  >카드</option>';
	firstPayment += '</select>';
	firstPayment += '<span></span>';
	firstPayment += '</div>';
    firstPayment += '</td>';
    firstPayment += '<td style="width: 145px;">';
    firstPayment += '<div class="select-con">';
	firstPayment += '<select class="dropdown">';
	firstPayment += '<option  value=""  >증빙구분</option>';
	firstPayment += '<c:forEach var="data" items="${billingDivisionList}" varStatus="status" >';
	firstPayment += '<option  value="${data.billing_division_id}"  >${data.billing_division}</option>';
	firstPayment += '</c:forEach>';
	firstPayment += '</select>';
	firstPayment += '<span></span>';
	firstPayment += '</div>';
    firstPayment += '</td>';
    firstPayment += '<td style="width: 145px;">';
	firstPayment += '<div class="select-con">';
	firstPayment += '<select class="dropdown">';
	firstPayment += '<option  value=""  >결제여부</option>';
	firstPayment += '<option  value="00"  >결제</option>';
	firstPayment += '<option  value="01">미결제</option>';
	firstPayment += '</select>';
	firstPayment += '<span></span>';
	firstPayment += '</div>';
    firstPayment += '</td>';
    firstPayment += '<td style="width:130px;"><input type="text"  placeholder="입금일" name="cAcqDate"></td>';
    firstPayment += '<td style="width:130px;"  class="notFirst widthAuto"><input type="text"  style="display:inline-block;" placeholder="계산서발행일자" name="cAcqDate" ></td>';
    firstPayment += '<td style="width:130px;"><input type="text" onkeyup="javascript:getNumber(this);" placeholder="업체청구액" name="amountf" id="amountf"></td>';
    firstPayment += '<td style="" colspan="2"><input type="text"  style="display:inline-block;" placeholder="비고" name="" id=""></td>';
    firstPayment += '<td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="추가" onclick="javascript:addPayment(0,this);"></td>';
    firstPayment += '<td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="삭제" onclick="javascript:delPayment(0,this);"></td>';
	firstPayment += '</tr>';
	
	
	//alert($("#paymentInfo").children().eq(0).html());
	
	
	//alert($("#paymentInfo").children().eq(0).clone().wrapAll("<div/>").parent().html());
	//firstPayment = $("#paymentInfo").children().eq(0).clone().wrapAll("<div/>").parent().html()
	
	//firstPayment = "<tr payment_division='01'>"+$("#paymentInfo").children().eq(0).html()+"</tr>";
	secondPayment = "<tr payment_division='02'>"+$("#paymentInfo").children().eq(1).html()+"</tr>";
	thirdPayment = "<tr payment_division='03'>"+$("#paymentInfo").children().eq(2).html()+"</tr>";
	
}

function setCarInfo(){
	
	carInfoList.length = 0;
	var returnVal = true;
	$("#allocationInfo").find("tbody").each(function(index,element){
		var carInfo= new Object();
		$(this).find("tr").each(function(trnum,element){
			$(this).find("td").each(function(tdnum,element){
				
				if(trnum == 0 && tdnum==0){
					//alert($(this).html());					//타이틀
				}
				if(trnum == 0 && tdnum==1){
					if($(this).find("div").find("select").val() == ""){
						alert("배차구분은 필수 입력 사항 입니다.");
						returnVal = false;
					}else{
						carInfo.carrier_type = $(this).find("div").find("select").val();			//배차구분	
					}
				}
				if(trnum == 0 && tdnum==2){
					carInfo.distance_type = $(this).find("div").find("select").val();			//운행구분
				}
				if(trnum == 0 && tdnum==3){
					carInfo.departure_dt = $(this).find("input").val();			//출발일
				}
				if(trnum == 0 && tdnum==4){
					carInfo.departure_time = $(this).find("input").val();			//출발시간
				}
				if(trnum == 0 && tdnum==5){
					carInfo.driver_id = $(this).find("input").next().val();			//기사선택
				//	alert($(this).find("div").find("select").html());
				}
				if(trnum == 0 && tdnum==6){
					//사고유무
					if($(this).find("input").prop("checked")){
						carInfo.accident_yn = 01;	
					}else{
						carInfo.accident_yn = 00;
					}
				}
				if(trnum == 0 && tdnum==7){
					carInfo.etc = $(this).find("textarea").val();			//비고	
				}
				if(trnum == 1 && tdnum==0){
					carInfo.car_kind = $(this).find("input").val();			//차종
				} 
				if(trnum == 1 && tdnum==1){
					carInfo.car_id_num = $(this).find("input").val();			//차대
				}
				if(trnum == 1 && tdnum==2){
					carInfo.car_num = $(this).find("input").val();			//차량번호
				}
				if(trnum == 1 && tdnum==3){
					carInfo.contract_num = $(this).find("input").val();			//계약번호
				}
				if(trnum == 1 && tdnum==4){
					carInfo.tow_distance = $(this).find("input").val();			//견인거리
				}
				if(trnum == 1 && tdnum==5){
					//사고유무
				}
				if(trnum == 2 && tdnum==0){
					carInfo.departure = $(this).find("input").val();			//출발지약식주소
				}
				if(trnum == 2 && tdnum==1){
					carInfo.departure_addr = $(this).find("input").val();			//출발지상세주소
				}
				if(trnum == 2 && tdnum==2){
					carInfo.departure_person_in_charge = $(this).find("input").val();			//출발담당자명
				}
				if(trnum == 2 && tdnum==3){
					carInfo.departure_phone = $(this).find("input").val();			//출발연락처
				}
				if(trnum == 3 && tdnum==0){
					carInfo.arrival = $(this).find("input").val();			//도착지약식주소
				}
				if(trnum == 3 && tdnum==1){
					carInfo.arrival_addr = $(this).find("input").val();			//도착지상세주소
				}
				if(trnum == 3 && tdnum==2){
					carInfo.arrival_person_in_charge = $(this).find("input").val();			//도착담당자명
				}
				if(trnum == 3 && tdnum==3){
					carInfo.arrival_phone = $(this).find("input").val();			//도착연락처
				}
	      	});
      	});	
		carInfoList.push(carInfo);
  	});
	
	$("#carInfoVal").val(JSON.stringify({carInfoList : carInfoList}))
	return returnVal;
}

function setPaymentInfo(){
	
	paymentInfoList.length = 0; 
	
	$("#paymentInfo").find("tr").each(function(index,element){
		var paymentInfo= new Object();
		
			paymentInfo.payment_division = $(this).attr("payment_division");
		
			$(this).find("td").each(function(tdnum,element){
				if(index == 0){
					if(tdnum == 1){
						paymentInfo.payment_partner = $(this).find("form").find("input").val();
						paymentInfo.payment_partner_id = $(this).find("form").find("input").next().val();
					}
					if(tdnum == 2){
						paymentInfo.payment_kind = $(this).find("div").find("select").val();
					}
					if(tdnum == 3){
						paymentInfo.billing_division = $(this).find("div").find("select").val();
					}
					if(tdnum == 4){
						paymentInfo.payment = $(this).find("div").find("select").val();
					}
					if(tdnum == 5){
						paymentInfo.payment_dt =  $(this).find("input").val();
					}
					if(tdnum == 6){
						paymentInfo.billing_dt = $(this).find("input").val();
					}
					if(tdnum == 7){
						paymentInfo.amount = $(this).find("input").val();
					}
					if(tdnum == 9){
						paymentInfo.etc = $(this).find("input").val();
						paymentInfo.deduction_rate = "";
						paymentInfo.bill_for_payment = "";
					}	
				}else{
					if(Number(paymentInfo.payment_division) == 02){
						if(tdnum == 0){
							paymentInfo.payment_partner_id = $(this).find("div").find("select").val();
							paymentInfo.payment_partner = $(this).find("div").find("select option:selected").text();
						//	alert(paymentInfo.payment_partner);
						}
						if(tdnum == 1){
							paymentInfo.payment_kind = $(this).find("div").find("select").val();
						}
						if(tdnum == 2){
							paymentInfo.billing_division = $(this).find("div").find("select").val();
						}
						if(tdnum == 3){
							paymentInfo.payment = $(this).find("div").find("select").val();
						}
						if(tdnum == 4){
							paymentInfo.payment_dt =  $(this).find("input").val();
						}
						if(tdnum == 5){
							paymentInfo.billing_dt = $(this).find("input").val();
						}
						if(tdnum == 6){
							paymentInfo.amount = $(this).find("input").val();
						}
						
						if(paymentInfo.payment_division != "02"){
							if(tdnum == 8){
								paymentInfo.etc = $(this).find("input").val();
							}	
							paymentInfo.deduction_rate = "";
							paymentInfo.bill_for_payment = "";
						}else{
							paymentInfo.etc = "";
							if(tdnum == 8){
								paymentInfo.deduction_rate = $(this).find("input").val();
							}
							if(tdnum == 9){
								paymentInfo.bill_for_payment = $(this).find("input").val();
							}
						}
					}else if(Number(paymentInfo.payment_division) != 02){
						if(tdnum == 0){
							paymentInfo.payment_partner = $(this).find("form").find("input").val();
							paymentInfo.payment_partner_id = $(this).find("form").find("input").next().val();
						}
						if(tdnum == 1){
							paymentInfo.payment_kind = $(this).find("div").find("select").val();
						}
						if(tdnum == 2){
							paymentInfo.billing_division = $(this).find("div").find("select").val();
						}
						if(tdnum == 3){
							paymentInfo.payment = $(this).find("div").find("select").val();
						}
						if(tdnum == 4){
							paymentInfo.payment_dt =  $(this).find("input").val();
						}
						if(tdnum == 5){
							paymentInfo.billing_dt = $(this).find("input").val();
						}
						if(tdnum == 6){
							paymentInfo.amount = $(this).find("input").val();
						}
						
						if(paymentInfo.payment_division != "02"){
							if(tdnum == 8){
								paymentInfo.etc = $(this).find("input").val();
							}	
							paymentInfo.deduction_rate = "";
							paymentInfo.bill_for_payment = "";
						}else{
							paymentInfo.etc = "";
							if(tdnum == 8){
								paymentInfo.deduction_rate = $(this).find("input").val();
							}
							if(tdnum == 9){
								paymentInfo.bill_for_payment = $(this).find("input").val();
							}
						}
					}
				}
				
	      	});
      	
		paymentInfoList.push(paymentInfo);
  	});
	
	$("#paymentInfoVal").val(JSON.stringify({paymentInfoList : paymentInfoList}))
	
}


function insertAllocation(){

	//첫줄시작
	 $("#inputDt").val($("#datepicker1").val());
	 $("#profit").val($("#profitf").val());
	//첫줄끝
	//2
	 $("#comment").val($("#commentf").val());
	//2
	//3
	
	if($("#customerNamef").val() == ""){
		alert("고객정보(업체명) 는 필수 입력 사항 입니다.");
		return false;
	}else{
		$("#customerName").val($("#customerNamef").val());	
	}
	
	 
	 $("#chargeName").val($("#chargeNamef").val());
	 $("#chargePhone").val($("#chargePhonef").val());
	 $("#chargeAddr").val($("#chargeAddrf").val());
	 $("#customerSignificantData").val($("#customerSignificantDataf").val());
	 $("#carCnt").val($("#carCntf").val());
	 //3
	 
	 
	 //4
	 if(!setCarInfo()){
		// alert(setCarInfo());
		 return false;
	 }
	 
	 //5
	 setPaymentInfo();
	 
	 $("#memo").val($("#memof").val());
	if($("#customerName").val() == ""){
		alert("고객명이 입력되지 않았습니다.");
		return false;
	}
	
	if($("#carrierType").val() == ""){
		alert("배차구분이 입력되지 않았습니다.");
		return false;
	}


 	if(confirm("등록 하시겠습니까?")){
		$("#insertForm").attr("action","/allocation/insert-allocation.do");
		$("#insertForm").submit();	
	} 
	
}

function updateAllocation(){
	
	//첫줄시작
	 $("#inputDt").val($("#datepicker1").val());
	 $("#profit").val($("#profitf").val());
	//첫줄끝
	//2
	 $("#comment").val($("#commentf").val());
	//2
	//3
	 $("#customerName").val($("#customerNamef").val());
	 $("#chargeName").val($("#chargeNamef").val());
	 $("#chargePhone").val($("#chargePhonef").val());
	 $("#chargeAddr").val($("#chargeAddrf").val());
	 $("#customerSignificantData").val($("#customerSignificantDataf").val());
	 $("#carCnt").val($("#carCntf").val());
	 //3
	 
	 //4
	 //setCarInfo();
	 if(!setCarInfo()){
			// alert(setCarInfo());
			 return false;
	}
	 
	 //5
	 setPaymentInfo();
	 
	 $("#memo").val($("#memof").val());
	 
	if($("#customerName").val() == ""){
		alert("고객명이 입력되지 않았습니다.");
		return false;
	}
	/* if($("#customerPhoneNum").val() == ""){
		alert("고객 연락처가 입력되지 않았습니다.");
		return false;
	} */

	if($("#carrierType").val() == ""){
		alert("배차구분이 입력되지 않았습니다.");
		return false;
	}

	var currentLocation = $(location).attr('href');
	var locationStr = "";
	if(currentLocation.indexOf("combination.do") != -1){
		locationStr = "combination.do";
	}else if(currentLocation.indexOf("self.do") != -1){
		locationStr = "self.do";
	}else if(currentLocation.indexOf("carrier.do") != -1){
		locationStr = "carrier.do";
	}
	
	if(confirm("수정 하시겠습니까?")){
		$("#insertForm").attr("action","/allocation/update-allocationAllData.do?returnPage="+locationStr);
		$("#insertForm").submit();	
	}
	
}



function jusoSearch(where,obj){
	
	new daum.Postcode({
	    oncomplete: function(data) {
	        // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

            // 각 주소의 노출 규칙에 따라 주소를 조합한다.
            // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
            var fullAddr = ''; // 최종 주소 변수
            var extraAddr = ''; // 조합형 주소 변수

            // 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
            if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                fullAddr = data.roadAddress;

            } else { // 사용자가 지번 주소를 선택했을 경우(J)
                fullAddr = data.jibunAddress;
            }

            // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
            if(data.userSelectedType === 'R'){
                //법정동명이 있을 경우 추가한다.
                if(data.bname !== ''){
                    extraAddr += data.bname;
                }
                // 건물명이 있을 경우 추가한다.
                if(data.buildingName !== ''){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
                fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
            }

            // 우편번호와 주소 정보를 해당 필드에 넣는다.
            //document.getElementById('sample6_postcode').value = data.zonecode; //5자리 새우편번호 사용
         	
            $(obj).parent().children().eq(0).val(fullAddr);
            $(obj).parent().parent().children().eq(0).children().val(data.sido+" "+data.sigungu);
            // 커서를 상세주소 필드로 이동한다.
            //document.getElementById('sample6_address2').focus();
        }
	    
	}).open();	
	
}

function selectCompany(companyId){
	
	$.ajax({ 
			type: 'post' ,
			url : "/baseinfo/getCompanyInfo.do" ,
			dataType : 'json' ,
			data : {
				companyId : companyId
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					$("#paymentAccountNumberf").val(resultData.account_number);					
					$("#companyId").val(companyId);
				}else if(result == "0001"){
					alert("변경 하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
	
}

var selectDriverObj;

function showModal(obj){
	selectDriverObj =obj; 
	$('.modal-field').show();
}


function setDriver(driverId,drivername,carKind){
	
	if(carKind != ""){
		$(selectDriverObj).val(drivername+" / "+carKind);	
	}else{
		$(selectDriverObj).val(drivername);
	}
	$(selectDriverObj).next().val(driverId);
	selectDriver(driverId,selectDriverObj);
	$('.modal-field').hide();
}

function selectDriver(driverId,obj){
	
	$.ajax({ 
			type: 'post' ,
			url : "/baseinfo/getDriverInfo.do" ,
			dataType : 'json' ,
			data : {
				driverId : driverId
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					var driverInfo= new Object();
					driverInfo.driver_id = resultData.driver_id;
					driverInfo.driver_name = resultData.driver_name;
					driverInfo.deduction_rate = resultData.deduction_rate;
					driverInfo.car_kind = resultData.car_kind;
					driverInfo.car_num = resultData.car_num;
					driverList.push(driverInfo);
					var add = '<option  value="'+driverInfo.driver_id+'">'+driverInfo.driver_name+'</option>';
					$('select[name="driverInfo"]').each(function(index,element){
						$(this).append(add);
						if($("#allocationInfo").children().first().get(0) == $(obj).parent().parent().parent().get(0)){
							if(index == 0){
								$(this).val(driverInfo.driver_id).prop("selected", true);	
								setDriverDeduct(driverInfo.driver_id,this);
							}
						}
			      	});
					
				}else if(result == "0001"){
					alert("변경 하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
	
}


function getPersonInChargeInfo(id,name,phone_num,address){
	
	//$('#searchInput').trigger("click");
	
	$("#searchResult").removeClass('active');
	$("#chargeNamef").val(name);
	$("#chargePhonef").val(phone_num);
	$("#chargeAddrf").val(address);
	
}





function getCustomerInfo(id,name,phone_num,significant,forPayment,address){
	
	//alert(forPayment);
	if(forPayment == "findCustomer"){
		$("#customerNamef").val(name);
		$("#customerSignificantDataf").val(significant);
		$.ajax({ 
			type: 'post' ,
			url : "/personInCharge/getPersonInChargeList.do" ,
			dataType : 'json' ,
			data : {
				customerId : id
			},
			success : function(data, textStatus, jqXHR)
			{
				
				$("#paymentInfo").find("tr").each(function(index,element){
					$(this).find("td").each(function(tdnum,element){
						if(index == 0){
							if(tdnum == 1){
								$(this).find("form").find("input").val(name);
								$(this).find("form").find("input").next().val(id);
							//	$(this).find("form").find("input").parent().find("div").removeClass('active');
							}
						}
					});
			      });
    
				var list = data.resultData;
				var result = "";
				$("#searchResult").html("");
				//$(obj).parent().find("div").html("");
			
				if(list.length > 1){
	       			for(var i=0; i<list.length; i++){
	       				result += '<div class="Wresult">';
	       				result += '<p class="result-title">고객정보</p>';
	       				result += '<a style="cursor:pointer;"   onclick="javascript:getPersonInChargeInfo(\''+list[i].person_in_charge_id+'\',\''+list[i].name+'\',\''+list[i].phone_num+'\',\''+list[i].address+'\');" class="result-sub"><span>'+list[i].name+'</span></a>';
	       				result += '<p class="camp-type"><span>담당자 주소:</span> <span>'+list[i].address+'</span></p>';
	       				result += '<p class="camp-id"><span>Tel:</span> <span>'+list[i].phone+'</span></p>';
	       				result += '</div>';
	       			}
				}else if(list.length == 1){
	       				getPersonInChargeInfo(list[0].person_in_charge_id,list[0].name,list[0].phone_num,list[0].address);
				}else{
					/* result += '<div class="no-result d-table">';
					result += '<div class="d-tbc">';
					result += '<i class="fa fa-exclamation-triangle fa-3x" aria-hidden="true"></i>';
					result += '<span>No results have been found.</span>';
					result += '</div></div>'; */
					
					//180823  기존에 담당자가 없을시 한국카캐리어가 담당자가 되는것을 거래처 정보로 변경
					//getPersonInChargeInfo("${companyList[0].company_id}","${companyList[0].company_name}","${companyList[0].phone_num}","${companyList[0].address}");
					getPersonInChargeInfo(id,name,phone_num,address);
				}
				
				$("#searchResult").html(result);
				//$(obj).parent().find("div").html(result);
			
				
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
	}else{
		$(test).val(name);
		$(test).next().val(id);
		$(test).parent().find("div").removeClass('active');
	}
	
	
	

}

var test;
function getAjaxData(val,obj,forPayment){
	
	test = obj;
	
	$.ajax({ 
		type: 'post' ,
		url : "/baseinfo/getCustomerList.do" ,
		dataType : 'json' ,
		data : {
			customerName : val
		},
		success : function(data, textStatus, jqXHR)
		{
			
			var list = data.resultData;
			var result = "";
			//$("#searchResult").html("");
			$(obj).parent().find("div").html("");
			if(list.length > 0){
       			for(var i=0; i<list.length; i++){
       				result += '<div class="Wresult">';
       				/* result += '<a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>'; */
       				result += '<p class="result-title">고객정보</p>';
       				var strJsonText = JSON.stringify(obj);
    	   		    strJsonText = strJsonText.replace(/\"/gi, "'");
       				result += '<a style="cursor:pointer;"   onclick="javascript:getCustomerInfo(\''+list[i].customer_id+'\',\''+list[i].customer_name+'\',\''+list[i].phone+'\',\''+list[i].significant_data+'\',\''+forPayment+'\',\''+list[i].address+'\');" class="result-sub"><span>'+list[i].customer_name+'</span></a>';
       				
       				if(list[i].customer_kind == "00"){
       					result += '<p class="camp-type"><span>회사 구분:</span> <span>법인</span></p>';	
       				}else if(list[i].customer_kind == "01"){
       					result += '<p class="camp-type"><span>회사 구분:</span> <span>개인</span></p>';	
       				}else if(list[i].customer_kind == "02"){
       					result += '<p class="camp-type"><span>회사 구분:</span> <span>외국인</span></p>';	
       				}else if(list[i].customer_kind == "03"){
       					result += '<p class="camp-type"><span>회사 구분:</span> <span>개인(주민번호)</span></p>';	
       				}
       				
       				result += '<p class="camp-id"><span>Tel:</span> <span>'+list[i].phone+'</span></p>';
       				result += '</div>';
       			}
			}else{
				result += '<div class="no-result d-table">';
				result += '<div class="d-tbc">';
				result += '<i class="fa fa-exclamation-triangle fa-3x" aria-hidden="true"></i>';
				result += '<span>No results have been found.</span>';
				result += '</div></div>';
			}
			
			//$("#searchResult").html(result);
			$(obj).parent().find("div").html(result);
			
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});

	
}


var initBody; 
function beforePrint() 
{ 
    initBody = document.body.innerHTML; 
    document.body.innerHTML = $("#printlayout").html(); 
} 
function afterPrint() 
{ 
    document.body.innerHTML = initBody; 
} 
function pageprint() 
{ 
    window.onbeforeprint = beforePrint; 
    window.onafterprint = afterPrint;    
 /*    
	//웹 브라우저 컨트롤 생성
    var webBrowser = '<OBJECT ID="previewWeb" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
    //웹 페이지에 객체 삽입
    document.body.insertAdjacentHTML('beforeEnd', webBrowser);
    //ExexWB 메쏘드 실행 (7 : 미리보기 , 8 : 페이지 설정 , 6 : 인쇄하기(대화상자))
    previewWeb.ExecWB(7, 1);
    //객체 해제
    previewWeb.outerHTML = ""; 
*/
    window.print(); 
} 

function carInfoInput(){
	
	var carCnt = $("#carCntf").val();
	
	if(carCnt == "" || Number(carCnt) == 0){
		alert("대수를 입력해 주세요.");
		return false;
	}else if(Number(carCnt) == 1){
		alert("1대인경우 운행정보의 입력란에 입력 하세요");
		return false;
	}else if(Number(carCnt) > 10){
		alert("최대 10대까지 입력 할 수 있습니다.");
		return false;
	}else{
		$("#carInfo").html("");
		var result = "";
		var cnt = 0;
		if(carInfoList.length != 0){
			if(Number(carCnt) >= carInfoList.length){
				for(var i = 0; i < carInfoList.length; i++){
					result += '<tr class="ui-state-default">';
					result += '<td style="width:60px;">'+(cnt+1)+'</td>';
					result += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="차종" name="" id="" value="'+carInfoList[i].carKind+'"></td>';
					result += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="차대번호" name="" id="" value="'+carInfoList[i].carIdNum+'"></td>';
					result += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="차량번호" name="" id="" value="'+carInfoList[i].carNum+'"></td>';
					result += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="계약번호" name="" id="" value="'+carInfoList[i].contractNum+'"></td>';
					result += '</tr>';
					cnt++;
				}
				for(var i = 0; i < Number(carCnt)-carInfoList.length; i++){
					result += '<tr class="ui-state-default">';
					result += '<td style="width:60px;">'+(cnt+1)+'</td>';
					result += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="차종" name="" id="" value=""></td>';
					result += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="차대번호" name="" id="" value=""></td>';
					result += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="차량번호" name="" id="" value=""></td>';
					result += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="계약번호" name="" id="" value=""></td>';
					result += '</tr>';
					cnt++;
				}
			}else{
				for(var i = 0; i < Number(carCnt); i++){
					result += '<tr class="ui-state-default">';
					result += '<td style="width:60px;">'+(cnt+1)+'</td>';
					result += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="차종" name="" id="" value="'+carInfoList[i].carKind+'"></td>';
					result += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="차대번호" name="" id="" value="'+carInfoList[i].carIdNum+'"></td>';
					result += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="차량번호" name="" id="" value="'+carInfoList[i].carNum+'"></td>';
					result += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="계약번호" name="" id="" value="'+carInfoList[i].contractNum+'"></td>';
					result += '</tr>';
					cnt++;
				}
			}
			
		}else{
			for(var i = 0; i < Number(carCnt); i++){
				result += '<tr class="ui-state-default">';
				result += '<td style="width:60px;">'+(i+1)+'</td>';
				result += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="차종" name="" id=""></td>';
				result += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="차대번호" name="" id=""></td>';
				result += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="차량번호" name="" id=""></td>';
				result += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="계약번호" name="" id=""></td>';
				result += '</tr>';
			}	
		}
		
		$("#carInfo").html(result);
		$('.car-modal-field').show();
	}

	
}

function carInfoConfirm(){
	
	//확인을 누르면 셋트를 먼처 추가 하여 자동으로 입력 될 수 있도록 한다.
	for(var i = 0; i < Number($("#carInfo").find("tr").length)-1; i++){		//세트가 하나 있으므로 총 대수에서 한대 뺌.
		$("#allocationAddForm").append(addInfo);
	}
	
	//alert($("#allocationAddForm").find("input[name=carKindf]").length);
	/* carKindf
    carIdNumf
    carNumf
    contractNumf */
	
    carInfoList.length = 0;
	$("#carInfo").find("tr").each(function(index,element){
		var carInfo= new Object();
		$(this).find("td").each(function(num,element){
			if(num==1){
				carInfo.carKind = $(this).children().val();
			}
			if(num==2){
				carInfo.carIdNum = $(this).children().val();
			}
			if(num==3){
				carInfo.carNum = $(this).children().val();
			}
			if(num==4){
				carInfo.contractNum = $(this).children().val();
			}
      	});	
		carInfoList.push(carInfo);
  	});
    
	$("#allocationAddForm").find("input[name=carKindf]").each(function(index,element){
			$(this).val(carInfoList[index].carKind);
     });
	$("#allocationAddForm").find("input[name=carIdNumf]").each(function(index,element){
		$(this).val(carInfoList[index].carIdNum);
 });
	$("#allocationAddForm").find("input[name=carNumf]").each(function(index,element){
		$(this).val(carInfoList[index].carNum);
 });
	$("#allocationAddForm").find("input[name=contractNumf]").each(function(index,element){
		$(this).val(carInfoList[index].contractNum);
 });
	
	$('.car-modal-field').hide();
		/* carInfoList.length = 0;
		$("#carInfo").find("tr").each(function(index,element){
			var carInfo= new Object();
			$(this).find("td").each(function(num,element){
				if(num==1){
					carInfo.carKind = $(this).children().val();
				}
				if(num==2){
					carInfo.carIdNum = $(this).children().val();
				}
				if(num==3){
					carInfo.carNum = $(this).children().val();
				}
				if(num==4){
					carInfo.contractNum = $(this).children().val();
				}
	      	});	
			carInfoList.push(carInfo);
      	});
		//alert(JSON.stringify({carInfoList : carInfoList}));
		$("#carInfoVal").val(JSON.stringify({carInfoList : carInfoList}))
		 */	
	
}


function jusoCopy(status,obj){
	
	 
	 	if(status == "departure"){
	 
	 		var location = $("#allocationAddForm").find("input[name=departuref]:eq(0)").val();		
	 		var address = $("#allocationAddForm").find("input[name=departureAddrf]:eq(0)").val();
	 		
	 		$(obj).prev().val(location);
	 		$(obj).parent().next().children().eq(0).val(address);
	 		
	 	}else if(status == "arrival"){
	 		
	 		var location = $("#allocationAddForm").find("input[name=arrivalf]:eq(0)").val();		
	 	 	var address = $("#allocationAddForm").find("input[name=arrivalAddrf]:eq(0)").val();
	 	 	$(obj).prev().val(location);
	 		$(obj).parent().next().children().eq(0).val(address);
	 		
	 	}
	 	
}




function carInfoCancel(){
	$('.car-modal-field').hide();	
}

function deleteAllocationInfo(obj){
	
	
 	if($("#allocationInfo").children().first().get(0) == $(obj).parent().parent().parent().get(0)){
		alert("운행정보를 삭제 할 수 없습니다.");
		return false;
	}else{
		$(obj).parent().parent().parent().remove();
	} 
	
	
}


function addAllocationInfo(){
	
	$("#allocationAddForm").append(addInfo);
	
}

function addPayment(index,obj){
	

//	firstPayment = $("#paymentInfo").children().eq(0).parent().html();
//	secondPayment = $("#paymentInfo").children().eq(1).parent().html();
//	thirdPayment = $("#paymentInfo").children().eq(2).parent().html();
	
/* 	alert(firstPayment);*/
	
	//var rowspan = $("#forPaymentAdd").attr("rowspan");
	
	/* var rowspan = $(obj).parent().parent().parent().children.eq(0).attr("rowspan"); */
	
	var rowspan = $(obj).parent().parent().parent().children().eq(0).children().eq(0).attr("rowspan");
	
	//alert(rowspan);
	
	if(Number(index) == 0){
		$(obj).parent().parent().after(firstPayment);
	}else if(Number(index) == 1){
		//secondPayment = "<tr payment_division='02'>"+$("#paymentInfo > tr[payment_division='02']").first().html()+"</tr>";
		//alert($("#paymentInfo > tr[payment_division='02']").find("td").first().html());
		$(obj).parent().parent().after(secondPayment);
		var newElement = $(obj).parent().parent().next();
	alert($(newElement).html());
	//	$(newElement).find("td").first().html($("#paymentInfo > tr[payment_division='02']").find("td").first().html());
		
	}else if(Number(index) == 2){
		$(obj).parent().parent().after(thirdPayment);
	}
	
	$(obj).parent().parent().parent().children().eq(0).children().eq(0).attr("rowspan",rowspan+1);

	  $('.search-box').on("click", function(){
	        $(this).val("");
	        $(this).parent().find("div").removeClass('active');
	    });
	    
	    $('.search-box').keyup(function (e) {
	        var SearchBoxVal = $(this).val();
	        var obj = this;
	        if (SearchBoxVal.length >= 2) {
	        	$(this).parent().find("i").stop().css("display", "block");
	            setTimeout( function() {
	            	getAjaxData(SearchBoxVal,obj);
	            //	alert(this);
	            	$(obj).parent().find("i").css('display','none');
	            	$(obj).parent().find("div").addClass('active');
	            	
	           //     $(".dispatch-bottom-content table tbody tr td form i.fa-spinner").css('display','none');
	            //    $(".dispatch-bottom-content table tbody tr td form .search-result").addClass('active');
	            }, 1000);
	        }
	        else {
	        	
	        	$(this).parent().find(".search-result").removeClass('active');
	            //$(".dispatch-bottom-content table tbody tr td form .search-result").removeClass('active');
	        };
	    }); 
	  
	     
		var maxDate = new Date();
		  $(document).find("input[name=cAcqDate]").removeClass('hasDatepicker').datepicker({
		    	dateFormat : "yy-mm-dd",
		    	  maxDate : maxDate,
		    	  onClose: function( selectedDate ) {    

		          } 
		    }); 
	    
}

function delPayment(index,obj){
	
		if($("#paymentInfo > tr[payment_division='"+$(obj).parent().parent().attr('payment_division')+"']").first().get(0) == $(obj).parent().parent().get(0)){
			alert("삭제 할 수 없습니다.");
			return false;
		}else{
			$(obj).parent().parent().remove();
		}
}

function setDriverDeduct(driverId,obj){
	
	
	for(var i = 0; i < driverList.length; i++){
		if(driverId == driverList[i].driver_id){
			
			$(obj).parent().parent().parent().children().eq(8).find("input").val(driverList[i].deduction_rate+"%");
			
		}	
	}
	
}
var order = "${order}";
function sortby(gubun){

	if(order == "" || order == "desc"){
		order = "asc";
	}else{
		order = "desc";
	}
	
	var loc = document.location.href;
	var str = "";
	if(loc.indexOf("?") > -1){
		//forOrder 가 있는경우 ㅎㅎ
		if(loc.indexOf("forOrder") > -1){
			var queryString = loc.split("?");
			var query = queryString[1].split("&");
			
			for(var i = 0; i < query.length; i++){
				if(query[i].indexOf("forOrder") > -1){
					query[i] = "forOrder="+gubun+"^"+order;
				}
			}
			for(var i = 0; i < query.length; i++){
				if(query[i] != ""){
					str += "&"+query[i];	
				}
			}
			document.location.href = queryString[0]+"?"+str;
		}else{
			str="&forOrder="+gubun+"^"+order;
			document.location.href = loc+str;
		}
		
	}else{
		str="?&forOrder="+gubun+"^"+order;
		document.location.href = loc+str;
	}
	
}
	
 </script>


<div class="modal-field">
            <div class="modal-box">
                <h3 class="text-center">기사리스트</h3>
            <div class="modal-table-container">
                <table class="article-table">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <!-- <td>소유주</td> -->
                            <td>기사명</td>
                            <td>연락처</td>
                            <td>차량번호</td>
                            <td>차종</td>
                        </tr>
                    </thead>
                    <tbody id="driverSelectList">
                    	<c:forEach var="data" items="${driverList}" varStatus="status" >
                    		<tr class="ui-state-default" style="cursor:pointer;" driverId="${data.driver_id}" onclick="javascript:setDriver('${data.driver_id}','${data.driver_name}','${data.car_kind}');"> 
	                            <%-- <td>${data.driver_owner}</td> --%>
	                            <td>${data.driver_name}</td>
	                            <td>${data.phone_num}</td>
	                            <td>${data.car_num}</td>
	                            <td>${data.car_kind}</td>
                        	</tr>
						</c:forEach>
                    </tbody>
                </table>
                </div>
                <div class="confirmation">
                    <div class="confirm">
                        <input type="button" value="취소" name="">
                    </div>
                </div>
            </div>
        </div>
 		
        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">HOME</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">신규 배차 입력</a></li>
                </ul>
            </div>
            <div class="up-dl clearfix">
                <div class="date-picker">
                    <a href="" class="prev-date">
                        <img src="img/date-arrow-prev.png" alt="">
                    </a>
                    <div class="date-display">
                        <span>2018.01.01</span> ~ <span>2018.01.31</span>
                    </div>
                    <a href="" class="next-date">
                        <img src="img/date-arrow-next.png" alt="">
                    </a>
                </div>

                <div class="upload-btn">
                    <input type="button" value="엑셀 다운로드">
                </div> 
                
                
            </div>

            <div class="dispatch-btn-container">
				<div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">신규배차입력</div>					
            </div>
        </section>

        <div class="dispatch-wrapper">
            <div class="car-modal-field">
            <div class="car-modal-box" style="width:800px;">
                <h3 class="text-center">차량정보 입력</h3>
            <div class="car-modal-table-container">
                <table class="article-table">
                    <colgroup>
                    	<col>
						<col>
						<col>
						<col>
						<col>
                    </colgroup>
                    <thead>
                        <tr>
                        	<td style="width:60px;">번호</td>
                            <td>차종</td>
                            <td>차대번호</td>
                            <td>차량번호</td>
                            <td>계약번호</td>
                        </tr>
                    </thead>
                    <tbody id="carInfo">
                    </tbody>
                </table>
                </div>
                <div class="confirmation">
                    <div class="confirm">
                        <input type="button" value="확인" name="" onclick="javascript:carInfoConfirm();">
                    </div>
                    <div class="confirm">
                        <input type="button" value="취소" name="" onclick="javascript:carInfoCancel();">
                    </div>
                </div>
            </div>
        </div>

             <input type="button" id="printButton" value="인쇄" onclick="javascript:pageprint()">
			<section class="dispatch-bottom-content" id="printlayout" class="active">
				<form id="insertForm" action="/allocation/insert-allocation.do">
					<input type="hidden" name="allocationId" id="allocationId" value="">
					<input type="hidden" name="companyInfo" id="companyInfo" value="">
					<input type="hidden" name="paymentKind" id="paymentKind" value="">
					<input type="hidden" name="payment" id="payment" value="">
					<input type="hidden"  name="inputDt" id="inputDt">
					<input type="hidden"  name="comment" id="comment">
					<input type="hidden"  name="customerName" id="customerName">
					<input type="hidden"  name="chargeName" id="chargeName">
					<input type="hidden"  name="chargePhone" id="chargePhone">
					<input type="hidden"  name="chargeAddr" id="chargeAddr">
					<input type="hidden"  name="customerSignificantData" id="customerSignificantData">
					<input type="hidden"  name="customerPhoneNum" id="customerPhoneNum">
					<input type="hidden" name="carCnt" id="carCnt" value="">
					<input type="hidden" name="departureDt" id="departureDt" value="">
					<input type="hidden" name="departureTime" id="departureTime" value="">
					<input type="hidden" name="towDistance" id="towDistance" value="">
					<input type="hidden" name="departure" id="departure" value="">
					<input type="hidden" name="departureAddr" id="departureAddr" value="">
					<input type="hidden" name="departurePersonInCharge" id="departurePersonInCharge" value="">
					<input type="hidden" name="departurePhone" id="departurePhone" value="">
					<input type="hidden" name="etc" id="etc" value="">			
					<input type="hidden" name="arrival" id="arrival" value="">
					<input type="hidden" name="arrivalAddr" id="arrivalAddr" value="">
					<input type="hidden" name="arrivalPersonInCharge" id="arrivalPersonInCharge" value="">
					<input type="hidden" name="arrivalPhone" id="arrivalPhone" value="">
					<input type="hidden" name="amount" id="amount" value="">
					<input type="hidden" name="billForPayment" id="billForPayment" value="">
					<input type="hidden" name="paymentAccountNumber" id="paymentAccountNumber" value="">
					<input type="hidden" name="memo" id="memo" value="">
					<input type="hidden" name="cancelYn" id="cancelYn" value="N">
					<input type="hidden" name="completeYn" id="completeYn" value="N">
					<input type="hidden" name="companyId" id="companyId" value="">
					<input type="hidden" name="driverId" id="driverId" value="">
					<input type="hidden" name="driverPaymentKind" id="driverPaymentKind" value="">
					<input type="hidden" name="companyPaymentKind" id="companyPaymentKind" value="">
					<input type="hidden" name="driverPayment" id="driverPayment" value="">
					<input type="hidden" name="companyPayment" id="companyPayment" value="">
					<input type="hidden" name="driverPaymentAccountNumber" id="driverPaymentAccountNumber" value="">
					<input type="hidden" name="companyPaymentAccountNumber" id="companyPaymentAccountNumber" value="">
					<input type="hidden" name="companyBillForPayment" id="companyBillForPayment" value="">
					<input type="hidden" name="paymentDt" id="paymentDt" value="">
					<input type="hidden" name="driverPaymentDt" id="driverPaymentDt" value="">
					<input type="hidden" name="companyPaymentDt" id="companyPaymentDt" value="">
					<input type="hidden" name="billingDtMain" id="billingDtMain" value="">
					<input type="hidden" name="billingDtSec" id="billingDtSec" value="">
					<input type="hidden" name="billingDtThird" id="billingDtThird" value="">
					<input type="hidden" name="profit" id="profit" value=""><!--순이익  -->
					<input type="hidden" name="deductionRate" id="deductionRate" value="">	<!-- 공제율 -->
					<input type="hidden" name="amountPaid" id="amountPaid" value=""><!-- 기본지급액 -->
					<input type="hidden" name="carInfoVal" id="carInfoVal" value="">
					<input type="hidden" name="paymentInfoVal" id="paymentInfoVal" value="">
				</form>
                <table>
                    <tbody>
                        <tr>
                            <td style="width:100px;" >사업자정보</td>
                            <td style="width: 145px;">
                            	<div class="select-con">
							        <select class="dropdown" style="width:100%;" onchange="javascript:selectCompany(this.value);">
							        	<option  value=""  >사업자 선택</option>
							        	<c:forEach var="data" items="${companyList}" varStatus="status" >
							        		<option  value="${data.company_id}"  >${data.company_name}</option>
										</c:forEach>
							        </select>
							        <span></span>
							    </div>
                            </td>
                            <td class="form-title">
                                	의뢰일 
                            </td>
                            <td style="width:190px">
                                <input  type="text" id="datepicker1" class="date-range" value="${paramMap.startDt}">
                            </td>
                            <td class="form-title">등록자</td>
                            <td style="width:190px">
                            	<input type="hidden" name="registerId" id="registerId" value="${userMap.emp_id}">
                                <input type="text" placeholder="등록자" readonly="readonly" name="register" id="register" value="${userMap.emp_name }">
                            </td>
                            <td class="widthAuto"></td>
                            <td class="form-title">순이익</td>
                            <td style="width:190px">
                                <input type="text" placeholder="순이익" readonly="readonly" name="profitf" id="profitf">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                        <tr>
                            <td style="width:100px;" >고객정보</td>
                            <td style="width: 190px;">
                                <form action="">																
                                	<input type="text" class="search-box" name="findCustomer" id="" style="margin-top:5px;"> 		
                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
                                    <div class="search-result" id="searchResult">
                                    </div>
                                </form>
                            </td>
                            <td style="width: 190px;"><input type="text" placeholder="업체명" name="customerNamef" id="customerNamef"></td>
                            <td style="width: 190px;"><input type="text" placeholder="담당자 이름" name="chargeNamef" id="chargeNamef"></td>
                            <td style="width: 190px;"><input type="text" placeholder="담당자 연락처" name="chargePhonef" id="chargePhonef"></td>
                            <td style="width: 190px;"><input type="text" placeholder="담당자 주소" name="chargeAddrf" id="chargeAddrf"></td>
                            <td style="width: 190px;"><input type="text" placeholder="특이사항" name="customerSignificantDataf" id="customerSignificantDataf"></td>
                            <td class="notFirst widthAuto" style="width: 300px;">
                            	<input type="text" style="display:inline-block; width:63%;"  onkeyup="javascript:getNumber(this);" placeholder="차량대수(1~100)" name="carCntf" id="carCntf"  onkeypress="if(event.keyCode=='13') carInfoInput();">
                            	<input type="button" style="width:35%;" class="btn-primary" value="차량정보 입력" onclick="javascript:carInfoInput();">
                            </td>
                        </tr>
                </table>
                
                <div id="allocationAddForm" style="margin-bottom:5px;">
                <table data-name="allocationInfo" style="margin-bottom:0px;">
                		<tbody>
	                        <tr>
	                            <td style="width:100px;" rowspan="4" class="vaTop">운행정보</td>
	                            <td>
	                            <div class="select-con">
							        <select class="dropdown" style="width:100%;">
							        	<option  value=""  >배차구분</option>
							        	<c:forEach var="data" items="${allocationDivisionList}" varStatus="status" >
							        		<option  value="${data.allocation_division_cd}"  >${data.allocation_division}</option>
										</c:forEach>
							        </select>
							        <span></span>
							    </div>
	                            
	                            </td>
	                            <td style="width: 145px;">
	                            	<div class="select-con">
								        <select class="dropdown" style="width:100%;" >
								        	<option  value=""  >운행구분</option>
								        	<c:forEach var="data" items="${runDivisionList}" varStatus="status" >
								        		<option  value="${data.run_division_cd}"  >${data.run_division}</option>
											</c:forEach>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                            <td style="width:150px;"><input type="text"  placeholder="출발일(ex:01/01)" name="departureDtf" id=""></td>
	                            <td style="width:150px;"><input type="text" placeholder="출발시간(ex:10:00)" name="departureTimef" id=""></td>
	                            <td>
	                            	<input type="text" placeholder="기사선택" name="" id="" onclick="javascript:showModal(this);">
	                            	<input type="hidden" value="">
	                            </td>
	                            <td style="width:150px;"><input type="checkbox" style="margin-left:15px;" ><div style="margin-left:10px;  display:inline-block;">사고유무</div></td>
	                            <td style="padding: 10px 10px; width:300px;" colspan =3 rowspan=2>
	                            		<textarea class="remarks" style="height:100px;" name="etcf" id="" placeholder="비고"></textarea>
	                            </td>
	                            <!-- <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="추가" onclick="javascript:addAllocationInfo();"></td> -->
	                        </tr>
							<tr>
	                            <td  class="notFirst widthAuto" colspan=2><input type="text" placeholder="차종" name="carKindf" id=""></td>
	                            <td><input type="text" placeholder="차대" name="carIdNumf" id=""></td>
	                            <td><input type="text" placeholder="차량번호" name="carNumf" id=""></td>
	                            <td><input type="text" placeholder="계약번호" name="contractNumf" id=""></td>
	                            <td style="width:150px;"><input type="text" placeholder="견인거리(ex:20.5)" name="towDistancef" id=""></td>
	                           <!-- <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="삭제" onclick="javascript:deleteAllocationInfo(this);"></td> -->
	                        </tr>
	
	                        <tr>
	                            <td  colspan=2 class="notFirst widthAuto">
	                                <input type="text" placeholder="출발지" name="departuref" id="">
	                            </td>
	                            <td colspan="5" class="notFirst widthAuto text-center">
	                                <input type="text" placeholder="Search" style="width: 89%; display:inline-block;" name="departureAddrf" id="">
	                                <input type="button" class="btn-primary" value="검색" onclick="jusoSearch('departure',this);">
	                            </td>
	                            <td><input type="text" placeholder="담당자명" name="departurePersonInChargef" id=""></td>
	                            <td><input type="text" placeholder="연락처" name="departurePhonef" id=""></td>
	                            <!-- <td></td> -->
	                        </tr>
	                        <tr>
	                            <td  colspan=2 class="notFirst widthAuto">
	                                <input type="text" placeholder="도착지" name="arrivalf" id="">
	                            </td>
	                            <td colspan="5" class="notFirst widthAuto text-center">
	                                <input type="text" placeholder="Search" style="width: 89%; display:inline-block;" name="arrivalAddrf" id="">
	                                <input type="button" class="btn-primary" value="검색" onclick="jusoSearch('arrival',this);">
	                            </td>
	                            <td><input type="text" placeholder="담당자명" name="arrivalPersonInChargef" id=""></td>
	                            <td><input type="text" placeholder="연락처" name="arrivalPhonef" id=""></td>
	                            <!-- <td></td> -->
	                        </tr>
                        </tbody>
                </table>
				<table data-name="paymentInfo">
						<tbody>
	                        <tr payment_division="01">
	                            <td style="width:100px;"  rowspan="3" class="vaTop" id="forPaymentAdd">결제정보</td>
	                            <td style="width:130px;">
	                                <form action="">																
	                                	<input type="text" class="search-box" placeholder="매출처" name="findCustomerForPayment" id="">
	                                	<input type="hidden" name="paymentCustomerId"> 		
	                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
	                                    <div class="search-result" id="">
	                                    </div>
	                                </form>
	                            </td>
	                            <td style="width: 145px;">
	                                <div class="select-con">
								        <select class="dropdown">
								        	<option  value=""  >결제방법</option>
								            <c:forEach var="data" items="${paymentDivisionList}" varStatus="status" >
								        		<option  value="${data.payment_division_cd}"  >${data.payment_division}</option>
											</c:forEach>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                            <td style="width: 145px;">
	                            	<div class="select-con">
								        <select class="dropdown">
								        	<option  value=""  >증빙구분</option>
								             <c:forEach var="data" items="${billingDivisionList}" varStatus="status" >
								        		<option  value="${data.billing_division_id}"  >${data.billing_division}</option>
											</c:forEach>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                            <td style="width: 145px;">
	                                <div class="select-con">
								        <select class="dropdown">
								        	<option  value=""  >결제여부</option>
								            <c:forEach var="data" items="${payDivisionList}" varStatus="status" >
								        		<option  value="${data.pay_division_cd}"  >${data.pay_division}</option>
											</c:forEach>
								        </select>
								        <span></span>
								    </div> 
	                            </td> 
	                            <td style="width:130px;"><input type="text"  placeholder="입금일" name="cAcqDate" ></td>
	                            <td style="width:130px;"  class="notFirst widthAuto"><input type="text"  style="display:inline-block;" placeholder="계산서발행일자" name="cAcqDate" ></td>
	                            <td style="width:130px;"><input type="text" onkeyup="javascript:getNumber(this);" placeholder="업체청구액" name="amountf" id=""></td>
	                            <!-- <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="수정" onclick="javascript:notyet();"></td> -->
	                            <td style="" colspan="2"><input type="text"  style="display:inline-block;" placeholder="비고" name="" id=""></td>
	                            <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="추가" onclick="javascript:addPayment(0,this);"></td>
	                            <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="삭제" onclick="javascript:delPayment(0,this);"></td>
	                        </tr>
	                        
	                        <tr payment_division="02">
	                            <td class="notFirst widthAuto">
	                            	<div class="select-con">
								        <select class="dropdown" name="driverInfo" onchange="javascript:setDriverDeduct(this.value,this);">
								        	<option  value=""  >기사선택</option>
								        </select>
								        <input type="hidden" name="paymentCustomerId">
								        <span></span>
								    </div>
	                            </td>
	                            <td style="width: 145px;">
	                                <div class="select-con">
								        <select class="dropdown">
								        	<option  value=""  >결제방법</option>
								            <option  value="00"  >계좌이체</option>
								            <option  value="01"  >현금</option>
								            <option  value="02"  >현금영수증</option>
								            <option  value="03"  >카드</option>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                            <td style="width: 145px;">
	                                <div class="select-con">
								        <select class="dropdown">
								        	<option  value=""  >증빙구분</option>
								             <c:forEach var="data" items="${billingDivisionList}" varStatus="status" >
							        		<option  value="${data.billing_division_id}"  >${data.billing_division}</option>
										</c:forEach>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                            <td style="width: 145px;">
	                                <div class="select-con">
								        <select class="dropdown">
								        	<option  value=""  >결제여부</option>
								            <option  value="00"  >결제</option>
								            <option  value="01">미결제</option>
								        </select>
								        <span></span>
								    </div>
	                            </td> 
	                            <td style="width:130px;"><input type="text" placeholder="지급일" name="cAcqDate" ></td>
	                            <td style="width:130px;"  class="notFirst widthAuto"><input type="text"  style="display:inline-block;" placeholder="계산서발행일자" name="cAcqDate" ></td>
	                            <td  class="notFirst widthAuto"><input type="text"  onkeyup="javascript:getNumber(this);" style="display:inline-block;" placeholder="기사지급액" name="amountPaidf" id=""></td>
	                            <!-- <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="수정" onclick="javascript:notyet();"></td> -->
	                            <td style="width:130px;"><input type="text"  style="display:inline-block;" placeholder="공제율" name="deductionRatef" id=""></td>
	                            <td style="width:130px;"><input type="text" placeholder="최종지급액" readonly="readonly" name="billForPaymentf" id=""></td>
	                            <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="추가" onclick="javascript:addPayment(1,this);"></td>
	                            <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="삭제" onclick="javascript:delPayment(1,this);"></td>
	                        </tr>
	                        <tr payment_division="03">
	                            <td class="notFirst widthAuto">
	                                <form action="">																
	                                	<input type="text" class="search-box" placeholder="매입처" name="findCustomerForPayment" id=""> 		
	                                	<input type="hidden" name="paymentCustomerId">
	                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
	                                    <div class="search-result" id="">
	                                    </div>
	                                </form>
	                            </td>
	                            <td style="width: 145px;">
	                                <div class="select-con">
								        <select class="dropdown">
								        	<option  value=""  >결제방법</option>
								            <option  value="00"  >계좌이체</option>
								            <option  value="01"  >현금</option>
								            <option  value="02"  >현금영수증</option>
								            <option  value="03"  >카드</option>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                            <td style="width: 145px;">
	                                <div class="select-con">
								        <select class="dropdown">
								        	<option  value=""  >증빙구분</option>
								             <c:forEach var="data" items="${billingDivisionList}" varStatus="status" >
							        		<option  value="${data.billing_division_id}"  >${data.billing_division}</option>
										</c:forEach>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                            <td style="width: 145px;">
	                                <div class="select-con">
								        <select class="dropdown">
								        	<option  value=""  >결제여부</option>
								            <option  value="00"  >결제</option>
								            <option  value="01">미결제</option>
								        </select>
								        <span></span>
								    </div>
	                            </td> 
	                            <td style="width:130px;"><input type="text" placeholder="지급일" name="cAcqDate" ></td>
	                            <td style="width:130px;" class="notFirst widthAuto"><input type="text"  style="display:inline-block;" placeholder="계산서발행일자" name="cAcqDate" ></td>
	                            <td style="width:130px;"><input type="text" onkeyup="javascript:getNumber(this);" placeholder="업체지급액" name="companyBillForPaymentf" id=""></td>
	                            <!-- <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="수정" onclick="javascript:notyet();"></td> -->
	                            <td style="" colspan="2"><input type="text"  style="display:inline-block;" placeholder="비고" name="" id=""></td>
	                            <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="추가" onclick="javascript:addPayment(2,this);"></td>
	                            <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="삭제" onclick="javascript:delPayment(2,this);"></td>
	                        </tr>
	                        </tbody>
                    	</table>
                    </div>
                    	
                <table>
                        <tr>
                            <td style="width:100px;"  class="vaTop">메모</td>
                            <td style="padding: 5.5px;"><textarea name="memof" id=""></textarea></td>
                        </tr>
                </table>

                <div class="confirmation">
                    <div class="confirm">
                        <input type="button" id="insertButton" value="확인" onclick="javascript:insertAllocation()">
                        <input type="button" id="modButton" value="수정" onclick="javascript:updateAllocation()" style="display:none;">
                    </div>
                    <div class="cancel">
                        <input type="button" value="취소" onclick="javascript:history.go(-1); return false;">
                    </div>
                </div>
               
            </section>
			
			
			<div id="forAddAllocation" style="display:none;">
			<table data-name="allocationInfo" style="margin-bottom:0px;">
                		<tbody>
	                        <tr>
	                            <td style="width:100px;" rowspan="4" class="vaTop">운행정보</td>
	                            <td>
	                            <div class="select-con">
							        <select class="dropdown" style="width:100%;">
							        	<option  value=""  >배차구분</option>
							        	<c:forEach var="data" items="${allocationDivisionList}" varStatus="status" >
							        		<option  value="${data.allocation_division_cd}"  >${data.allocation_division}</option>
										</c:forEach>
							        </select>
							        <span></span>
							    </div>
	                            
	                            </td>
	                            <td style="width: 145px;">
	                            	<div class="select-con">
								        <select class="dropdown" style="width:100%;" >
								        	<option  value=""  >운행구분</option>
								        	<c:forEach var="data" items="${runDivisionList}" varStatus="status" >
								        		<option  value="${data.run_division_cd}"  >${data.run_division}</option>
											</c:forEach>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                            <td style="width:150px;"><input type="text"  placeholder="출발일(ex:01/01)" name="departureDtf" id=""></td>
	                            <td style="width:150px;"><input type="text" placeholder="출발시간(ex:10:00)" name="departureTimef" id=""></td>
	                            <td>
	                            	<input type="text" placeholder="기사선택" name="" id="" onclick="javascript:showModal(this);">
	                            	<input type="hidden" value="">
	                            </td>
	                            <td style="width:150px;"><input type="checkbox" style="margin-left:15px;" ><div style="margin-left:10px;  display:inline-block;">사고유무</div></td>
	                            <td style="padding: 10px 10px; width:300px;" colspan =3 rowspan=2>
	                            		<textarea class="remarks" style="height:100px;" name="etcf" id="" placeholder="비고"></textarea>
	                            </td>
	                            <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="추가" onclick="javascript:addAllocationInfo();"></td>
	                        </tr>
							<tr>
	                            <td  class="notFirst widthAuto" colspan=2><input type="text" placeholder="차종" name="carKindf" id=""></td>
	                            <td><input type="text" placeholder="차대" name="carIdNumf" id=""></td>
	                            <td><input type="text" placeholder="차량번호" name="carNumf" id=""></td>
	                            <td><input type="text" placeholder="계약번호" name="contractNumf" id=""></td>
	                            <td style="width:150px;"><input type="text" placeholder="견인거리(ex:20.5)" name="towDistancef" id=""></td>
	                           <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="삭제" onclick="javascript:deleteAllocationInfo(this);"></td>
	                        </tr>
	
	                        <tr>
	                            <td  colspan=2 class="notFirst widthAuto">
	                                <input type="text" placeholder="출발지" name="departuref" id="" style="width: 75%; display:inline-block;">
	                                <input type="button" class="btn-primary" value="복사" onclick="jusoCopy('departure',this);">
	                            </td>
	                            <td colspan="5" class="notFirst widthAuto text-center">
	                                <input type="text" placeholder="Search" style="width: 88%; display:inline-block;" name="departureAddrf" id="">
	                                <input type="button" class="btn-primary" value="검색" onclick="jusoSearch('departure',this);">
	                            </td>
	                            <td><input type="text" placeholder="담당자명" name="departurePersonInChargef" id=""></td>
	                            <td><input type="text" placeholder="연락처" name="departurePhonef" id=""></td>
	                            <td></td>
	                        </tr>
	                        <tr>
	                            <td  colspan=2 class="notFirst widthAuto">
	                                <input type="text" placeholder="도착지" name="arrivalf" id="" style="width: 75%; display:inline-block;">
	                                <input type="button" class="btn-primary" value="복사" onclick="jusoCopy('arrival',this);">
	                            </td>
	                            <td colspan="5" class="notFirst widthAuto text-center">
	                                <input type="text" placeholder="Search" style="width: 88%; display:inline-block;" name="arrivalAddrf" id="">
	                                <input type="button" class="btn-primary" value="검색" onclick="jusoSearch('arrival',this);">
	                            </td>
	                            <td><input type="text" placeholder="담당자명" name="arrivalPersonInChargef" id=""></td>
	                            <td><input type="text" placeholder="연락처" name="arrivalPhonef" id=""></td>
	                            <td></td>
	                        </tr>
                        </tbody>
                </table>
				<table data-name="paymentInfo">
						<tbody>
	                        <tr payment_division="01">
	                            <td style="width:100px;"  rowspan="3" class="vaTop" id="">결제정보</td>
	                            <td style="width:130px;">
	                                <form action="">																
	                                	<input type="text" class="search-box" placeholder="매출처" name="findCustomerForPayment" id="">
	                                	<input type="hidden" name="paymentCustomerId"> 		
	                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
	                                    <div class="search-result" id="">
	                                    </div>
	                                </form>
	                            </td>
	                            <td style="width: 145px;">
	                                <div class="select-con">
								        <select class="dropdown">
								        	<option  value=""  >결제방법</option>
								            <c:forEach var="data" items="${paymentDivisionList}" varStatus="status" >
								        		<option  value="${data.payment_division_cd}"  >${data.payment_division}</option>
											</c:forEach>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                            <td style="width: 145px;">
	                            	<div class="select-con">
								        <select class="dropdown">
								        	<option  value=""  >증빙구분</option>
								             <c:forEach var="data" items="${billingDivisionList}" varStatus="status" >
								        		<option  value="${data.billing_division_id}"  >${data.billing_division}</option>
											</c:forEach>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                            <td style="width: 145px;">
	                                <div class="select-con">
								        <select class="dropdown">
								        	<option  value=""  >결제여부</option>
								            <c:forEach var="data" items="${payDivisionList}" varStatus="status" >
								        		<option  value="${data.pay_division_cd}"  >${data.pay_division}</option>
											</c:forEach>
								        </select>
								        <span></span>
								    </div> 
	                            </td> 
	                            <td style="width:130px;"><input type="text"  placeholder="입금일" name="cAcqDate" ></td>
	                            <td style="width:130px;"  class="notFirst widthAuto"><input type="text"  style="display:inline-block;" placeholder="계산서발행일자" name="cAcqDate" ></td>
	                            <td style="width:130px;"><input type="text" onkeyup="javascript:getNumber(this);" placeholder="업체청구액" name="amountf" id=""></td>
	                            <!-- <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="수정" onclick="javascript:notyet();"></td> -->
	                            <td style="" colspan="2"><input type="text"  style="display:inline-block;" placeholder="비고" name="" id=""></td>
	                            <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="추가" onclick="javascript:addPayment(0,this);"></td>
	                            <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="삭제" onclick="javascript:delPayment(0,this);"></td>
	                        </tr>
	                        
	                        <tr payment_division="02">
	                            <td class="notFirst widthAuto">
	                            	<div class="select-con">
								        <select class="dropdown" name="driverInfo" onchange="javascript:setDriverDeduct(this.value,this);">
								        	<option  value=""  >기사선택</option>
								        </select>
								        <input type="hidden" name="paymentCustomerId">
								        <span></span>
								    </div>
	                            </td>
	                            <td style="width: 145px;">
	                                <div class="select-con">
								        <select class="dropdown">
								        	<option  value=""  >결제방법</option>
								            <option  value="00"  >계좌이체</option>
								            <option  value="01"  >현금</option>
								            <option  value="02"  >현금영수증</option>
								            <option  value="03"  >카드</option>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                            <td style="width: 145px;">
	                                <div class="select-con">
								        <select class="dropdown">
								        	<option  value=""  >증빙구분</option>
								             <c:forEach var="data" items="${billingDivisionList}" varStatus="status" >
							        		<option  value="${data.billing_division_id}"  >${data.billing_division}</option>
										</c:forEach>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                            <td style="width: 145px;">
	                                <div class="select-con">
								        <select class="dropdown">
								        	<option  value=""  >결제여부</option>
								            <option  value="00"  >결제</option>
								            <option  value="01">미결제</option>
								        </select>
								        <span></span>
								    </div>
	                            </td> 
	                            <td style="width:130px;"><input type="text" placeholder="지급일" name="cAcqDate" ></td>
	                            <td style="width:130px;"  class="notFirst widthAuto"><input type="text"  style="display:inline-block;" placeholder="계산서발행일자" name="cAcqDate" ></td>
	                            <td  class="notFirst widthAuto"><input type="text"  onkeyup="javascript:getNumber(this);" style="display:inline-block;" placeholder="기사지급액" name="amountPaidf" id=""></td>
	                            <!-- <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="수정" onclick="javascript:notyet();"></td> -->
	                            <td style="width:130px;"><input type="text"  style="display:inline-block;" placeholder="공제율" name="deductionRatef" id=""></td>
	                            <td style="width:130px;"><input type="text" placeholder="최종지급액" readonly="readonly" name="billForPaymentf" id=""></td>
	                            <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="추가" onclick="javascript:addPayment(1,this);"></td>
	                            <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="삭제" onclick="javascript:delPayment(1,this);"></td>
	                        </tr>
	                        <tr payment_division="03">
	                            <td class="notFirst widthAuto">
	                                <form action="">																
	                                	<input type="text" class="search-box" placeholder="매입처" name="findCustomerForPayment" id=""> 		
	                                	<input type="hidden" name="paymentCustomerId">
	                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
	                                    <div class="search-result" id="">
	                                    </div>
	                                </form>
	                            </td>
	                            <td style="width: 145px;">
	                                <div class="select-con">
								        <select class="dropdown">
								        	<option  value=""  >결제방법</option>
								            <option  value="00"  >계좌이체</option>
								            <option  value="01"  >현금</option>
								            <option  value="02"  >현금영수증</option>
								            <option  value="03"  >카드</option>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                            <td style="width: 145px;">
	                                <div class="select-con">
								        <select class="dropdown">
								        	<option  value=""  >증빙구분</option>
								             <c:forEach var="data" items="${billingDivisionList}" varStatus="status" >
							        		<option  value="${data.billing_division_id}"  >${data.billing_division}</option>
										</c:forEach>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                            <td style="width: 145px;">
	                                <div class="select-con">
								        <select class="dropdown">
								        	<option  value=""  >결제여부</option>
								            <option  value="00"  >결제</option>
								            <option  value="01">미결제</option>
								        </select>
								        <span></span>
								    </div>
	                            </td> 
	                            <td style="width:130px;"><input type="text" placeholder="지급일" name="cAcqDate" ></td>
	                            <td style="width:130px;" class="notFirst widthAuto"><input type="text"  style="display:inline-block;" placeholder="계산서발행일자" name="cAcqDate" ></td>
	                            <td style="width:130px;"><input type="text" onkeyup="javascript:getNumber(this);" placeholder="업체지급액" name="companyBillForPaymentf" id=""></td>
	                            <!-- <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="수정" onclick="javascript:notyet();"></td> -->
	                            <td style="" colspan="2"><input type="text"  style="display:inline-block;" placeholder="비고" name="" id=""></td>
	                            <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="추가" onclick="javascript:addPayment(2,this);"></td>
	                            <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="삭제" onclick="javascript:delPayment(2,this);"></td>
	                        </tr>
	                        </tbody>
                    	</table>
			</div>
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
        </div>        
       <script>
		     
       </script>

