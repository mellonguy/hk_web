<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/js/bootstable.js"></script>
<script type="text/javascript"> 
var oldListOrder = "";
var allocationId = "";

$(document).ready(function(){
	//forOpen

	
	$('#dataTable tr:odd td').css("backgroundColor","#f9f9f9");
	
});

function updateAllocaion(row){
	var inputDt = "";
    var carrierType = "";
    var distanceType = "";
    var departureDt = "";
    var departureTime = "";
    var customerName = "";
    var carKind = "";
    var carIdNum = "";
    var carNum  = "";
    var departure = "";
    var arrival  = "";
    var driverName = "";
    var carCnt  = "";
    var amount   = "";
    var paymentKind = "";
	var allocId = $(row).attr("allocationId");
	if(confirm("수정 하시겠습니까?")){
		$(row).find("td").each(function(index,element){
			if(index==1){
				inputDt = $(this).html();
			}
			if(index==2){
				if($(this).html() == "셀프"){
					carrierType = "S";	
				}else if($(this).html() == "캐리어"){
					carrierType = "C";
				}else{
					alert("배차구분을 확인 하세요.");
					return false;
				}
			}
			if(index==3){
				if($(this).html() == "시내"){
					distanceType = "0";	
				}else if($(this).html() == "시외"){
					distanceType = "1";
				}else{
					alert("거리구분을 확인 하세요.");
					return false;
				}
			}
			if(index==4){
				departureDt = $(this).html();
			}
			if(index==5){
				departureTime = $(this).html();
			}
			if(index==6){
				customerName = $(this).html();
			}
			if(index==7){
				carKind = $(this).html();
			}
			if(index==8){
				carIdNum = $(this).html();
			}
			if(index==9){
				carNum = $(this).html();
			}
			if(index==10){
				departure = $(this).html();
			}
			if(index==11){
				arrival = $(this).html();
			}
			if(index==12){
				driverName = $(this).html();
			}
			if(index==13){
				carCnt = $(this).html();
			}
			if(index==14){
				amount = $(this).html();
			}
			if(index==15){
				paymentKind = $(this).html();
			}
      	});

		if(carrierType == "" || distanceType == ""){
			return false;	
		}
		
		 $.ajax({ 
				type: 'post' ,
				url : "/allocation/update-allocation.do" ,
				dataType : 'json' ,
				data : {
					inputDt : inputDt,
					carrierType : carrierType,
					distanceType : distanceType,
					departureDt : departureDt,
					departureTime : departureTime,
					customerName : customerName,
					carKind : carKind,
					carIdNum : carIdNum,
					carNum : carNum,
					departure : departure,
					arrival : arrival,
					driverName : driverName,
					carCnt : carCnt,
					amount : amount,
					paymentKind : paymentKind,
					allocationId : allocId
				},
				success : function(data, textStatus, jqXHR)
				{
					var result = data.resultCode;
					if(result == "0000"){
						alert("변경 되었습니다.");
			//			document.location.href = "/allocation/combination.do";
					}else if(result == "0001"){
   						alert("변경 하는데 실패 하였습니다.");
   					}
				} ,
				error : function(xhRequest, ErrorText, thrownError) {
				}
			}); 
		
	}

}

function getListId(obj){
	$('html').scrollTop(0);
	selectList($(obj).parent().parent().parent().attr("allocationId"));
	
}

var driverArray = new Array();
function selectList(id){
	
	
	$(".dispatch-bottom-content").addClass('active');
    $("downArrow").addClass('active');
	
	$.ajax({ 
		type: 'post' ,
		url : "/allocation/selectAllocationInfo.do" ,
		dataType : 'json' ,
		data : {
			allocationId : id
		},
		success : function(data, textStatus, jqXHR)
		{
			var result = data.resultCode;
			var resultData = data.resultData;
			var resultDataSub = data.resultDataSub;
			var resultDataThird = data.resultDataThird;
			if(result == "0000"){
				$("#allocationId").val(resultData.allocation_id);
				$("#companyList").val(resultData.company_id).prop("selected", true);
				 $("#datepicker1").val(resultData.input_dt);
				 $("#commentf").val(resultData.comment);
				 $("#customerNamef").val(resultData.customer_name);
				 $("#profitf").val(resultData.profit);
				 $("#commentf").val(resultData.comment);
				 $("#customerNamef").val(resultData.customer_name);
				 $("#chargeNamef").val(resultData.charge_name);
				 $("#chargePhonef").val(resultData.charge_phone);
				 $("#chargeAddrf").val(resultData.charge_addr);
				 $("#customerSignificantDataf").val(resultData.customer_significant_data);
				 $("#carCntf").val(resultData.car_cnt);
				 $("#memof").val(resultData.memo);
				 $("#insertButton").css("display","none");
				 $("#modButton").css("display","");
				 
				 $("#allocationInfo").html("");
				
				 for(var i = 0; i < resultDataSub.length; i++){
					 var carInfoResult = "";
					carInfoResult += '<tbody>';
					carInfoResult += '<tr>';
					carInfoResult += '<td rowspan="4" class="vaTop"  style="width:100px;">운행정보</td>';
					carInfoResult += '<td>';
					carInfoResult += '<div class="select-con">';
					carInfoResult += '<select class="dropdown" style="width:100%;">';
					if(resultDataSub[i].carrier_type == ""){
						carInfoResult += '<option  value=""  selected="selected" >배차구분</option>';	
					}else{
						carInfoResult += '<option  value="" >배차구분</option>';
					}
					if(resultDataSub[i].carrier_type == "S"){
						carInfoResult += '<option  value="S"   selected="selected" >셀프</option>';		
					}else{
						carInfoResult += '<option  value="S"  >셀프</option>';
					}
					if(resultDataSub[i].carrier_type == "C"){
						carInfoResult += '<option  value="C"   selected="selected" >캐리어</option>';		
					}else{
						carInfoResult += '<option  value="C"  >캐리어</option>';
					}
					if(resultDataSub[i].carrier_type == "A"){
						carInfoResult += '<option  value="A"   selected="selected" >A</option>';		
					}else{
						carInfoResult += '<option  value="A"  >A</option>';
					}
					carInfoResult += '</select>';
					carInfoResult += '<span></span>';
					carInfoResult += '</div>';
					carInfoResult += '</td>';
					carInfoResult += '<td style="width: 145px;">';
					carInfoResult += '<div class="select-con">';
					carInfoResult += '<select class="dropdown" style="width:100%;" >';
					
					if(resultDataSub[i].distance_type == ""){
						carInfoResult += '<option  value=""   selected="selected" >운행구분</option>';	
					}else{
						carInfoResult += '<option  value=""  >운행구분</option>';
					}
					if(resultDataSub[i].distance_type == "00"){
						carInfoResult += '<option  value="00"   selected="selected" >시내</option>';		
					}else{
						carInfoResult += '<option  value="00"  >시내</option>';
					}
					if(resultDataSub[i].distance_type == "01"){
						carInfoResult += '<option  value="01"   selected="selected" >상행</option>';		
					}else{
						carInfoResult += '<option  value="01"  >상행</option>';
					}
					if(resultDataSub[i].distance_type == "02"){
						carInfoResult += '<option  value="02"   selected="selected" >하행</option>';		
					}else{
						carInfoResult += '<option  value="02"  >하행</option>';
					}
					if(resultDataSub[i].distance_type == "03"){
						carInfoResult += '<option  value="03"   selected="selected" >픽업</option>';		
					}else{
						carInfoResult += '<option  value="03"  >픽업</option>';
					}
					carInfoResult += '</select>';
					carInfoResult += '<span></span>';
					carInfoResult += '</div>';
					carInfoResult += '</td>';
					carInfoResult += '<td style="width:150px;"><input type="text"  placeholder="출발일(ex:01/01)" name="departureDtf" id="departureDtf" value="'+resultDataSub[i].departure_dt+'"></td>';
					carInfoResult += '<td style="width:150px;"><input type="text" placeholder="출발시간(ex:10:00)" name="departureTimef" id="departureTimef"  value="'+resultDataSub[i].departure_time+'"></td>';
					carInfoResult += '<td>';
					/*
					carInfoResult += '<div class="select-con">';
					carInfoResult += '<select id="sel'+resultDataSub[i].driver_id+'" class="dropdown" onchange="javascript:selectDriver(this.value);">';
					carInfoResult += '<option  value=""  >기사선택</option>';
					carInfoResult += '<c:forEach var="data" items="${driverList}" varStatus="status" >';
					carInfoResult += '<option  value="${data.driver_id}">${data.driver_name}/${data.car_kind}</option>';
					carInfoResult += '</c:forEach>';
					carInfoResult += '</select>';
					carInfoResult += '<span></span>';
					carInfoResult += '</div>';
					*/
					if(typeof(resultDataSub[i].driver_name) != "undefined"){
						carInfoResult += '<input type="text" placeholder="기사선택" name="" id="" onclick="javascript:showModal(this);" value="'+resultDataSub[i].driver_name+'">';
						carInfoResult += '<input type="hidden" value="'+resultDataSub[i].driver_id+'">';	
					}else{
						carInfoResult += '<input type="text" placeholder="기사선택" name="" id="" onclick="javascript:showModal(this);" value="">';
						carInfoResult += '<input type="hidden" value="">';	
					}
					carInfoResult += '</td>';
					if(Number(resultDataSub[i].accident_yn) == 1){
						carInfoResult += '<td style="width:150px;"><input type="checkbox" style="margin-left:15px;" checked ><div style="margin-left:10px;  display:inline-block;">사고유무</div></td>';	
					}else{
						carInfoResult += '<td style="width:150px;"><input type="checkbox" style="margin-left:15px;"><div style="margin-left:10px;  display:inline-block;">사고유무</div></td>';
					}
					carInfoResult += '<td style="padding: 10px 10px;" colspan =3 rowspan=2>';
					carInfoResult += '<textarea class="remarks" height="100px" name="etcf" id="etcf" placeholder="비고">'+resultDataSub[i].etc+'</textarea>';
					carInfoResult += '</td>';
					carInfoResult += '<td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="추가" onclick="javascript:addAllocationInfo();"></td>';
					carInfoResult += '</tr>';
					carInfoResult += '<tr>';
					carInfoResult += '<td  class="notFirst widthAuto"  colspan=2><input type="text" placeholder="차종" name="carKindf" id="carKindf" value="'+resultDataSub[i].car_kind+'"></td>';
					carInfoResult += '<td><input type="text" placeholder="차대" name="carIdNumf" id="carIdNumf" value="'+resultDataSub[i].car_id_num+'"></td>';
					carInfoResult += '<td><input type="text" placeholder="차량번호" name="carNumf" id="carNumf" value="'+resultDataSub[i].car_num+'"></td>';
					carInfoResult += '<td><input type="text" placeholder="계약번호" name="contractNumf" id="contractNumf" value="'+resultDataSub[i].contract_num+'"></td>';
					carInfoResult += '<td style="width:150px;"><input type="text" placeholder="견인거리(ex:20.5)" name="towDistancef" id="towDistancef" value="'+resultDataSub[i].tow_distance+'"></td>';
					carInfoResult += '<td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="삭제" onclick="javascript:deleteAllocationInfo(this);"></td>';
					carInfoResult += '</tr>';
					carInfoResult += '<tr>';
					carInfoResult += '<td  colspan=2  class="notFirst widthAuto">';
					carInfoResult += '<input type="text" placeholder="출발지" name="departuref" id="departuref" value="'+resultDataSub[i].departure+'">';
					carInfoResult += '</td>';
					carInfoResult += '<td colspan="5" class="notFirst widthAuto text-center">';
					carInfoResult += '<input type="text" placeholder="Search" style="width: 89%; display:inline-block;" name="departureAddrf" id="departureAddrf" value="'+resultDataSub[i].departure_addr+'">';
					carInfoResult += '<input type="button" class="btn-primary" value="검색" onclick="jusoSearch(\'departure\',this);">';
					carInfoResult += '</td>';
					carInfoResult += '<td><input type="text" placeholder="담당자명" name="departurePersonInChargef" id="departurePersonInChargef" value="'+resultDataSub[i].departure_person_in_charge+'"></td>';
					carInfoResult += '<td><input type="text" placeholder="연락처" name="departurePhonef" id="departurePhonef" value="'+resultDataSub[i].departure_phone+'"></td>';
					carInfoResult += '<td></td>';
					carInfoResult += '</tr>';
					carInfoResult += '<tr>';
					carInfoResult += '<td  colspan=2 class="notFirst widthAuto">';
					carInfoResult += '<input type="text" placeholder="도착지" name="arrivalf" id="arrivalf" value="'+resultDataSub[i].arrival+'">';
					carInfoResult += '</td>';
					carInfoResult += '<td colspan="5" class="notFirst widthAuto text-center">';
					carInfoResult += '<input type="text" placeholder="Search" style="width: 89%; display:inline-block;" name="arrivalAddrf" id="arrivalAddrf" value="'+resultDataSub[i].arrival_addr+'">';
					carInfoResult += '<input type="button" class="btn-primary" value="검색" onclick="jusoSearch(\'arrival\',this);">';
					carInfoResult += '</td>';
					carInfoResult += '<td><input type="text" placeholder="담당자명" name="arrivalPersonInChargef" id="arrivalPersonInChargef" value="'+resultDataSub[i].arrival_person_in_charge+'"></td>';
					carInfoResult += '<td><input type="text" placeholder="연락처" name="arrivalPhonef" id="arrivalPhonef" value="'+resultDataSub[i].arrival_phone+'"></td>';
					carInfoResult += '<td></td>';
					carInfoResult += '</tr>';
					carInfoResult += '</tbody>';
					$("#allocationInfo").append(carInfoResult);
					//$("#sel"+resultDataSub[i].driver_id).val(resultDataSub[i].driver_id).prop("selected", true);
					if(resultDataSub[i].driver_id != ""){
						selectDriver(resultDataSub[i].driver_id);	
					}
				 }
				 
				 $("#paymentInfo").html("");
				 for(var i = 0; i < resultDataThird.length; i++){
					 var paymentResult = "";					 	
					paymentResult += '<tr payment_division="'+resultDataThird[i].payment_division+'">';
					if(i == 0){
						paymentResult += '<td style="width:115px;" rowspan="'+resultDataThird.length+'" class="vaTop" id="forPaymentAdd">결제정보</td>';
						paymentResult += '<td style="width:130px;">';
					}else{
						paymentResult += '<td  class="notFirst widthAuto" style="width:130px;">';
					}
			
					if(resultDataThird[i].payment_division == "01"){
						paymentResult += '<form action="">';																
						paymentResult += '<input type="text" class="search-box" placeholder="매출처" name="findCustomerForPayment" value="'+resultDataThird[i].payment_partner+'">';
						paymentResult += '<input type="hidden" name="paymentCustomerId" value="'+resultDataThird[i].payment_partner_id+'">';
						paymentResult += '<i class="fa fa-spinner fa-spin fa-fw"></i>';
						paymentResult += '<div class="search-result" id="searchResult">';
						paymentResult += '</div>';
						paymentResult += '</form>';		
					}else if(resultDataThird[i].payment_division == "02"){
						var driverObject = new Object();
						driverObject.payment_partner_id = resultDataThird[i].payment_partner_id;
						driverArray.push(driverObject);
						paymentResult += '<div class="select-con">';
						paymentResult += '<select id="pay'+resultDataThird[i].payment_partner_id+'" class="dropdown" name="driverInfo" onchange="javascript:setDriverDeduct(this.value,this);">';
						paymentResult += '<option  value=""  >기사선택</option>';
						paymentResult += '</select>';
						paymentResult += '<input type="hidden" name="paymentCustomerId" value="'+resultDataThird[i].payment_partner_id+'">';
						paymentResult += '<span></span>';
						paymentResult += '</div>';
					}else if(resultDataThird[i].payment_division == "03"){
						paymentResult += '<form action="">';																
						paymentResult += '<input type="text" class="search-box" placeholder="매입처" name="findCustomerForPayment" value="'+resultDataThird[i].payment_partner+'">'; 		
						paymentResult += '<input type="hidden" name="paymentCustomerId" value="'+resultDataThird[i].payment_partner_id+'">';
						paymentResult += '<i class="fa fa-spinner fa-spin fa-fw"></i>';
						paymentResult += '<div class="search-result" id="searchResult">';
						paymentResult += '</div>';
						paymentResult += '</form>';
					}
					
					paymentResult += '</td>';
					paymentResult += '<td style="width: 145px;">';
					paymentResult += '<div class="select-con">';
					paymentResult += '<select class="dropdown">';
					
					
					if(resultDataThird[i].payment_kind == ""){
						paymentResult += '<option  value=""   selected="selected"  >결제방법</option>';	
					}else{
						paymentResult += '<option  value=""  >선택</option>';
					}
					if(resultDataThird[i].payment_kind == "00"){
						paymentResult += '<option  value="00"    selected="selected" >계좌이체</option>';		
					}else{
						paymentResult += '<option  value="00"  >계좌이체</option>';
					}
					if(resultDataThird[i].payment_kind == "01"){
						paymentResult += '<option  value="01"   selected="selected"  >현금</option>';		
					}else{
						paymentResult += '<option  value="01"  >현금</option>';
					}
					if(resultDataThird[i].payment_kind == "02"){
						paymentResult += '<option  value="02"   selected="selected"  >현금영수증</option>';		
					}else{
						paymentResult += '<option  value="02"  >현금영수증</option>';
					}
					if(resultDataThird[i].payment_kind == "03"){
						paymentResult += '<option  value="03"   selected="selected"  >카드</option>';		
					}else{
						paymentResult += '<option  value="03"  >카드</option>';
					}
					paymentResult += '</select>';
					paymentResult += '<span></span>';
					paymentResult += '</div>';
					paymentResult += '</td>';
					paymentResult += '<td style="width: 145px;">';
					paymentResult += '<div class="select-con">';
					paymentResult += '<select  id="sel'+resultDataThird[i].billing_division+'" class="dropdown">';
					paymentResult += '<option  value=""  >증빙구분</option>';
					paymentResult += '<c:forEach var="data" items="${billingDivisionList}" varStatus="status" >';
					paymentResult += '<option  value="${data.billing_division_id}"  >${data.billing_division}</option>';
					paymentResult += '</c:forEach>';
					paymentResult += '</select>';
					paymentResult += '<span></span>';
					paymentResult += '</div>';
					paymentResult += '</td>';
					paymentResult += '<td style="width: 145px;">';
					paymentResult += '<div class="select-con">';
					paymentResult += '<select class="dropdown">';
					
					if(resultDataThird[i].payment == ""){
						paymentResult += '<option  value=""  selected="selected" >결제여부</option>';	
					}else{
						paymentResult += '<option  value=""  >결제여부</option>';
					}
					if(resultDataThird[i].payment == "00"){
						paymentResult += '<option  value="00" selected="selected"  >결제</option>';		
					}else{
						paymentResult += '<option  value="00"  >결제</option>';
					}
					if(resultDataThird[i].payment == "01"){
						paymentResult += '<option  value="01" selected="selected">미결제</option>';		
					}else{
						paymentResult += '<option  value="01">미결제</option>';
					}
					paymentResult += '</select>';
					paymentResult += '<span></span>';
					paymentResult += '</div>';
					paymentResult += '</td>';
					paymentResult += '<td style="width:130px;"><input type="text"  placeholder="지급일"  name="cAcqDate"  value="'+resultDataThird[i].payment_dt+'"></td>';
					paymentResult += '<td style="width:130px;"  class="notFirst widthAuto"><input type="text"  style="display:inline-block;" placeholder="계산서발행일자"  name="cAcqDate"  value="'+resultDataThird[i].billing_dt+'"></td>';
					
					var maxDate = new Date();
					  $(document).find("input[name=cAcqDate]").removeClass('hasDatepicker').datepicker({
					    	dateFormat : "mm/dd",
					    	  maxDate : maxDate,
					    	  onClose: function( selectedDate ) {    

					          } 
					    }); 
					
					if(resultDataThird[i].payment_division == "01"){
						paymentResult += '<td style="width:130px;"><input type="text" onkeyup="javascript:getNumber(this);" placeholder="업체청구액" name="amountf" id="amountf" value="'+resultDataThird[i].amount+'"></td>';	
					}else if(resultDataThird[i].payment_division == "02"){
						paymentResult += '<td style="width:130px;"><input type="text" onkeyup="javascript:getNumber(this);" placeholder="기본지급액" name="amountf" id="amountf" value="'+resultDataThird[i].amount+'"></td>';
					}else if(resultDataThird[i].payment_division == "03"){
						paymentResult += '<td style="width:130px;"><input type="text" onkeyup="javascript:getNumber(this);" placeholder="업체지급액" name="amountf" id="amountf" value="'+resultDataThird[i].amount+'"></td>';
					}
					
					paymentResult += '<td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="수정" onclick="javascript:notyet();" ></td>';
					if(resultDataThird[i].payment_division == "02"){
						paymentResult += '<td style="width:130px;"><input type="text"  style="display:inline-block;" placeholder="공제율" name="deductionRatef" id="deductionRatef"value="'+resultDataThird[i].deduction_rate+'"></td>';
						paymentResult += '<td style="width:130px;"><input type="text" placeholder="기사지급액" readonly="readonly" name="billForPaymentf" id="billForPaymentf"value="'+resultDataThird[i].bill_for_payment+'"></td>';
					}else{
						paymentResult += '<td style="" colspan="2"><input type="text"  style="display:inline-block;" placeholder="비고" name="" id="" value="'+resultDataThird[i].etc+'"></td>';	
					}
					
					if(resultDataThird[i].payment_division == "01"){
						paymentResult += '<td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="추가" onclick="javascript:addPayment(0,this);" ></td>';
						paymentResult += '<td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="삭제" onclick="javascript:delPayment(0,this);" ></td>';	
					}else if(resultDataThird[i].payment_division == "02"){
						paymentResult += '<td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="추가" onclick="javascript:addPayment(1,this);" ></td>';
						paymentResult += '<td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="삭제" onclick="javascript:delPayment(1,this);" ></td>';	
					}else if(resultDataThird[i].payment_division == "03"){
						paymentResult += '<td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="추가" onclick="javascript:addPayment(2,this);" ></td>';
						paymentResult += '<td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="삭제" onclick="javascript:delPayment(2,this);" ></td>';	
					}
					
					paymentResult += '</tr>';					 
					$("#paymentInfo").append(paymentResult);
					$("#sel"+resultDataThird[i].billing_division).val(resultDataThird[i].billing_division).prop("selected", true);
					//$("#pay"+resultDataThird[i].payment_partner_id).val(resultDataThird[i].payment_partner_id).prop("selected", true);
					$('.search-box').on("click", function(){
				        $(this).val("");
				        $(this).parent().find("div").removeClass('active');
				    });
				    $('.search-box').keyup(function (e) {
				        var SearchBoxVal = $(this).val();
				        var obj = this;
				        if (SearchBoxVal.length >= 2) {
				        	$(this).parent().find("i").stop().css("display", "block");
				            setTimeout( function() {
				            	getAjaxData(SearchBoxVal,obj);
				            	$(obj).parent().find("i").css('display','none');
				            	$(obj).parent().find("div").addClass('active');
				            }, 1000);
				        }
				        else {
				        	$(this).parent().find(".search-result").removeClass('active');
				        };
				    }); 
				 }
				 setTimeout( function() {
					 for(var i = 0; i < driverArray.length; i++){
							$("#pay"+driverArray[i].payment_partner_id).val(driverArray[i].payment_partner_id).prop("selected", true);
						}
		            }, 100);
				
			}else if(result == "0001"){
				alert("변경 하는데 실패 하였습니다.");
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
	
}	
	
function batchStatus(status){
	
	var id = "";
	var total = $('input:checkbox[name="forBatch"]:checked').length;
	
	if(total == 0){
		alert("수정할 목록이 선택 되지 않았습니다.");
		return false;
	}else{
		$('input:checkbox[name="forBatch"]:checked').each(function(index,element) {
		      if(this.checked){//checked 처리된 항목의 값
		    	  id+=$(this).attr("allocationId");
		    	  if(index<total-1){
		    		  id += ","; 
			         } 
		      }
		 });	
		updateAllocationStatus(status,id);
	}
}	

function updateAllocationStatus(status,id){
	
	var msg = "";
	if(status == "cancel"){
		msg = "취소";
	}else if(status == "complete"){
		msg = "완료";
	}
	
	if(confirm(msg+"하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : "/allocation/updateAllocationStatus.do" ,
			dataType : 'json' ,
			data : {
				status : status,
				allocationId : id
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert(msg+"되었습니다.");
					document.location.href = "/allocation/combination.do";
				}else if(result == "0001"){
					alert(msg+"하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
}	
	
function checkAll(){

	if($('input:checkbox[id="checkAll"]').is(":checked")){
		$('input:checkbox[name="forBatch"]').each(function(index,element) {
			$(this).prop("checked","true");
		 });	
	}else{
		$('input:checkbox[name="forBatch"]').each(function(index,element) {
			$(this).prop("checked","");
		 });
	}
}
	
	
	
function move(location){
	
	
	window.location.href = "/allocation/"+location+".do";
	
}	
	
	
 </script>


<div class="modal-field">
            <div class="modal-box">
                <h3 class="text-center">기사리스트</h3>
            <div class="modal-table-container">
                <table class="article-table">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <!-- <td>소유주</td> -->
                            <td>기사명</td>
                            <td>연락처</td>
                            <td>차량번호</td>
                            <td>차종</td>
                        </tr>
                    </thead>
                    <tbody id="driverSelectList">
                    	<c:forEach var="data" items="${driverList}" varStatus="status" >
                    		<tr class="ui-state-default" style="cursor:pointer;" driverId="${data.driver_id}" onclick="javascript:setDriver('${data.driver_id}','${data.driver_name}','${data.car_kind}');"> 
	                            <%-- <td>${data.driver_owner}</td> --%>
	                            <td>${data.driver_name}</td>
	                            <td>${data.phone_num}</td>
	                            <td>${data.car_num}</td>
	                            <td>${data.car_kind}</td>
                        	</tr>
						</c:forEach>
                    </tbody>
                </table>
                </div>
                <div class="confirmation">
                    <div class="confirm">
                        <input type="button" value="취소" name="">
                    </div>
                </div>
            </div>
        </div>
 		<%-- <section class="side-nav">
            <ul>
                <li class="<c:if test="${complete == ''}">active</c:if>"><a href="/allocation/combination.do">신규배차입력</a></li>
                <li><a href="/allocation/self.do">셀프 배차  </a></li>
                <li><a href="/allocation/self.do?reserve=N">셀프 예약</a></li>
                <li><a href="/allocation/self.do?reserve=Y">셀프 배차 현황</a></li>
                <li><a href="/allocation/carrier.do">캐리어 배차</a></li>
                <li><a href="/allocation/carrier.do?reserve=N">캐리어 예약</a></li>
                <li><a href="/allocation/carrier.do?reserve=Y">캐리어 배차 현황</a></li>
                <li class="<c:if test="${complete == 'Y'}">active</c:if>"><a href="/allocation/combination.do?forOpen=N&complete=Y">완료 배차</a></li>
            </ul>
        </section> --%>

        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">HOME</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">배차 현황</a></li>
                </ul>
            </div>

            <div class="up-dl clearfix">
                <div class="date-picker">
                    <a href="" class="prev-date">
                        <img src="img/date-arrow-prev.png" alt="">
                    </a>
                    <div class="date-display">
                        <span>2018.01.01</span> ~ <span>2018.01.31</span>
                    </div>
                    <a href="" class="next-date">
                        <img src="img/date-arrow-next.png" alt="">
                    </a>
                </div>

                <div class="upload-btn">
                    <!-- <input type="button" value="엑셀 업로드"> -->
                    <input type="button" value="엑셀 다운로드">
                </div><!-- 
                <div class="download-btn">
                   
                </div> -->
                
                <form name="searchForm" method="get" action="combination.do?forOpen=N">
                	<input type="hidden" name="forOpen" value="N" />
					<div class="btns-submit">	
						<div class="right">
							<select name="searchType">
								<option value="" <c:if test='${paramMap.searchType eq "all" }'> selected="selected"</c:if> >선택</option>
								<option value="carrier" <c:if test='${paramMap.searchType eq "carrier" }'> selected="selected"</c:if> >배차구분</option>
								<option value="distance" <c:if test='${paramMap.searchType eq "distance" }'> selected="selected"<</c:if> >거리구분</option>
								<option value="customer" <c:if test='${paramMap.searchType eq "customer" }'> selected="selected"<</c:if> >고객명</option>
								<option value="driver" <c:if test='${paramMap.searchType eq "driver" }'> selected="selected"<</c:if> >기사명</option>						
							</select>
							<input type="text" name="searchWord" value="${paramMap.searchWord}" />
							<a class="btn-gray" href="javascript:" onclick="document.searchForm.submit()"><input type="button" value="검색"></a>
						</div>
					</div>			
				</form>
                
                
            </div>

            <div class="dispatch-btn-container">
                <!-- <div class="dispatch-btn">
                    <i id="downArrow" class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div> -->
                
                <%-- <c:if test="${complete == null || complete != 'Y'}"> --%>
                	<%-- <c:if test="${forOpen == null || forOpen != 'N'}">
						<div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">신규배차입력</div>
					</c:if> --%>
					<%-- <c:if test="${forOpen != null && forOpen == 'N'}"> --%>
						<div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">배차 현황</div>
					<%-- </c:if>
				</c:if> --%>
                
                <%-- <c:if test="${complete != null && complete == 'Y'}">
					<div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">완료배차</div>					
				</c:if> --%>
                
            </div>
        </section>

        <div class="dispatch-wrapper">
            <%-- <jsp:include page="common-form.jsp"></jsp:include> --%>
            <section class="dispatch-bottom-content">
			</section>
			<div id="bottom-table">
	            <section  class="bottom-table" style="width:1635px;">
	                <table class="article-table forToggle" id="dataTable" style="width:100%;">
	                    <colgroup>
	                       <col width="auto">
	                        <col width="150px;">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="150px;">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <%-- <c:if test="${user.control_grade == '01' }">
	                        	<col width="auto">
	                        	<col width="auto">
	                        </c:if> --%>
	                    </colgroup>
	                    <thead>
	                        <tr>
	                            <td class="showToggle"><input type="checkbox" id="checkAll" onclick="javascript:checkAll();" ></td>
	                            <!-- <td class="showToggle">번호</td> -->
	                            <td>의뢰일 </td>
	                            <td>배차구분</td>
	                            <td>운행구분</td>
	                            <td>고객명</td>
	                            <td>출발일</td>
	                            <td>출발시간</td>
	                            <td>차종</td>
	                            <td>등록구분</td>
	                            <td>차대번호</td>
	                            <td>차량번호</td>
	                            <td>출발지</td>
	                            <td>하차지</td>
	                            <td>기사명</td>
	                            <td>상태</td>
	                         	<%-- <c:if test="${user.control_grade == '01' }">   
	                            	<td class="showToggle"></td>
	                            	<td>수정</td>
	                            </c:if> --%>
	                        </tr>
	                    </thead>
	                    <tbody id="rowData">
	                    	<c:forEach var="data" items="${listData}" varStatus="status">
								<tr class="ui-state-default" list-order="${data.list_order}" allocationId="${data.allocation_id}">
	                            <td class="showToggle"><input type="checkbox" name="forBatch" allocationId="${data.allocation_id}"></td>
	                            <%-- <td class="showToggle">${data.list_order}</td> --%>
	                            <td>${data.input_dt}</td>
								<td>${data.allocation_division}</td>
								<td>${data.run_division}</td>
	                            <td style="cursor:pointer;" onclick="javascript:selectList('${data.allocation_id}');">${data.customer_name}</td>
	                            <td>${data.departure_dt}</td>
	                            <td>${data.departure_time}</td>
	                            <td>${data.car_kind}</td>
	                            <td>${data.batch_status_name}</td>
	                            <td>${data.car_id_num}</td>
	                            <td>${data.car_num}</td>
	                            <td>${data.departure}</td>
	                            <td>${data.arrival}</td>
	                            <td>${data.driver_name}</td>
	                            <td>${data.allocation_status}</td>
	                            <%-- <c:if test="${user.control_grade == '01' }">
		                            <td class="showToggle">
		                                <a href="#" class="table-driver-btn" title="아이콘 설명 1"><i class="fa fa-truck" aria-hidden="true"></i></a>
		                                <a href="#" class="table-btn" title="아이콘 설명 2"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
		                                <a style="cursor:pointer" title="취소" onclick="javascript:updateAllocationStatus('cancel','${data.allocation_id}');" class="table-x">
		                                    <img src="/img/x-icon.png" alt="">
		                                </a>
		                                <a style="cursor:pointer"  title="완료" onclick="javascript:updateAllocationStatus('complete','${data.allocation_id}');" class="table-check">
		                                    <img src="/img/check-icon.png" alt="">
		                                </a>
		                            </td>
	                            </c:if> --%>
	                        </tr>
							</c:forEach>
	                    
	                        <!-- <tr class="ui-state-default">
	                            <td>
	                                <input type="checkbox">
	                            </td>
	                            <td>1</td>
	                            <td>01/09</td>
	                            <td>셀프</td>
	                            <td>시내</td>
	                            <td>01/09</td>
	                            <td>10:00</td>
	                            <td><input class="table-input" type="text"></td>
	                            <td>캐딜락</td>
	                            <td>40두4010</td>
	                            <td>20두2010</td>
	                            <td>성남</td>
	                            <td>하남</td>
	                            <td>
	                                <form action="">
	                                    <input type="text" class="search-box">
	                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
	                                    <div class="search-result">
	                                        No result
	                                        <div class="no-result d-table">
	                                            <div class="d-tbc">
	                                                <i class="fa fa-exclamation-triangle fa-3x" aria-hidden="true"></i>
	                                                <span>No results have been found.</span>
	                                            </div>
	                                        </div>
	
	                                        Result type1
	                                        <div class="Wresult">
	                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
	                                            <p class="result-title">Campaigns</p>
	                                            <a href="" class="result-sub"><span>네오퓨쳐</span></a>
	                                            <p class="camp-type"><span>Campaign type:</span> <span>Powerlink</span></p>
	                                            <p class="camp-id"><span>Campaign ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
	                                        </div>
	
	                                        Result type2
	                                        <div class="Wresult">
	                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
	                                            <p class="result-title">Ad groups</p>
	                                            <a href="" class="result-sub"><span>네오퓨쳐</span>_#0007</a>
	                                            <p class="camp-type"><span>Campaign type:</span> <span>Powerlink</span></p>
	                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
	                                            <p class="adgroup-id"><span>Ad group ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
	                                        </div>
	
	                                        Result type3
	                                        <div class="Wresult">
	                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
	                                            <p class="result-title">Ads - Powerlink</p>
	                                            <div class="result-box">
	                                                <p class="title">네오퓨쳐</p>
	                                                <p class="content">일반 홈페이지에서 신개념 융복합 멀티미디어 홈피이지 제작, 견적상담 환영</p>
	
	                                                <ul>
	                                                    <li>View URL: http://www.neofss.com</li>
	                                                    <li>Linked URL: http://www.neofss.com</li>
	                                                </ul>
	                                            </div>
	                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
	                                            <p class="adgroup"><span>Ad group:</span> <span class="red">네오퓨쳐</span>_#0003</p>
	                                            <p class="adgroup-id"><span>Ad ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
	                                        </div>
	
	                                        Result type4
	                                        <div class="Wresult">
	                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
	                                            <p class="result-title">Ads - PowerContents</p>
	                                            <div class="result-box w-img">
	                                                <div class="d-table">
	                                                    <div class="d-tbc">
	                                                        <img src="/img/ads-img.png" alt="">
	                                                    </div>
	                                                    <div class="d-tbc">
	                                                        <p class="title">홈페이지제작 이러면 망한다!</p>
	                                                        <p class="content">홈페이지제작! 대충 업체에 맡기면 된다는 생각으로 접근하면, 십중팔구는 실패하는 이유와 홈페이지 제작에 관한 최소한의 지식은 가지고 업체를 선정해야 돈과 시간을 낭비하는 일을 줄일수 있다.</p>
	                                                    </div>
	                                                </div>
	                                                <ul>
	                                                    <li>Content created: 2017-12-15</li>
	                                                    <li>Brand Name: 네오퓨쳐</li>
	                                                    <li>View URL: http://blog.naver.com/marujeen</li>
	                                                    <li>Linked URL: http://blog.naver.com/marujeen</li>
	                                                </ul>
	                                            </div>
	                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
	                                            <p class="adgroup"><span>Ad group:</span> <span class="red">네오퓨쳐</span>_#0003</p>
	                                            <p class="adgroup-id"><span>Ad ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
	                                        </div>
	
	                                    </div>
	                                </form>
	                            </td>
	                            <td>3대</td>
	                            <td>150,000</td>
	                            <td>현금</td>
	                            <td>세금</td>
	                            <td>
	                                <a href="#" class="table-driver-btn"><i class="fa fa-truck" aria-hidden="true"></i></a>
	                                <a href="" class="table-save-btn"><i class="fa fa-floppy-o" aria-hidden="true"></i></a>
	                                <a href="" class="table-btn"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
	                                <a href="" class="table-x">
	                                    <img src="/img/x-icon.png" alt="">
	                                </a>
	                                <a href="" class="table-check">
	                                    <img src="/img/check-icon.png" alt="">
	                                </a>
	                            </td>
	                        </tr>
	                        <tr class="ui-state-default">
	                            <td>
	                                <input type="checkbox">
	                            </td>
	                            <td>1</td>
	                            <td>01/09</td>
	                            <td>셀프</td>
	                            <td>시내</td>
	                            <td>01/09</td>
	                            <td>10:00</td>
	                            <td><input class="table-input" type="text"></td>
	                            <td>캐딜락</td>
	                            <td>40두4010</td>
	                            <td>20두2010</td>
	                            <td>성남</td>
	                            <td>하남</td>
	                            <td>
	                                <form action="">
	                                    <input type="text" class="search-box">
	                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
	                                    <div class="search-result">
	                                        No result
	                                        <div class="no-result d-table">
	                                            <div class="d-tbc">
	                                                <i class="fa fa-exclamation-triangle fa-3x" aria-hidden="true"></i>
	                                                <span>No results have been found.</span>
	                                            </div>
	                                        </div>
	
	                                        Result type1
	                                        <div class="Wresult">
	                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
	                                            <p class="result-title">Campaigns</p>
	                                            <a href="" class="result-sub"><span>네오퓨쳐</span></a>
	                                            <p class="camp-type"><span>Campaign type:</span> <span>Powerlink</span></p>
	                                            <p class="camp-id"><span>Campaign ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
	                                        </div>
	
	                                        Result type2
	                                        <div class="Wresult">
	                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
	                                            <p class="result-title">Ad groups</p>
	                                            <a href="" class="result-sub"><span>네오퓨쳐</span>_#0007</a>
	                                            <p class="camp-type"><span>Campaign type:</span> <span>Powerlink</span></p>
	                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
	                                            <p class="adgroup-id"><span>Ad group ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
	                                        </div>
	
	                                        Result type3
	                                        <div class="Wresult">
	                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
	                                            <p class="result-title">Ads - Powerlink</p>
	                                            <div class="result-box">
	                                                <p class="title">네오퓨쳐</p>
	                                                <p class="content">일반 홈페이지에서 신개념 융복합 멀티미디어 홈피이지 제작, 견적상담 환영</p>
	
	                                                <ul>
	                                                    <li>View URL: http://www.neofss.com</li>
	                                                    <li>Linked URL: http://www.neofss.com</li>
	                                                </ul>
	                                            </div>
	                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
	                                            <p class="adgroup"><span>Ad group:</span> <span class="red">네오퓨쳐</span>_#0003</p>
	                                            <p class="adgroup-id"><span>Ad ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
	                                        </div>
	
	                                        Result type4
	                                        <div class="Wresult">
	                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
	                                            <p class="result-title">Ads - PowerContents</p>
	                                            <div class="result-box w-img">
	                                                <div class="d-table">
	                                                    <div class="d-tbc">
	                                                        <img src="/img/ads-img.png" alt="">
	                                                    </div>
	                                                    <div class="d-tbc">
	                                                        <p class="title">홈페이지제작 이러면 망한다!</p>
	                                                        <p class="content">홈페이지제작! 대충 업체에 맡기면 된다는 생각으로 접근하면, 십중팔구는 실패하는 이유와 홈페이지 제작에 관한 최소한의 지식은 가지고 업체를 선정해야 돈과 시간을 낭비하는 일을 줄일수 있다.</p>
	                                                    </div>
	                                                </div>
	                                                <ul>
	                                                    <li>Content created: 2017-12-15</li>
	                                                    <li>Brand Name: 네오퓨쳐</li>
	                                                    <li>View URL: http://blog.naver.com/marujeen</li>
	                                                    <li>Linked URL: http://blog.naver.com/marujeen</li>
	                                                </ul>
	                                            </div>
	                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
	                                            <p class="adgroup"><span>Ad group:</span> <span class="red">네오퓨쳐</span>_#0003</p>
	                                            <p class="adgroup-id"><span>Ad ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
	                                        </div>
	
	                                    </div>
	                                </form>
	                            </td>
	                            <td>3대</td>
	                            <td>150,000</td>
	                            <td>현금</td>
	                            <td>세금</td>
	                            <td>
	                                <a href="#" class="table-driver-btn"><i class="fa fa-truck" aria-hidden="true"></i></a>
	                                <a href="" class="table-save-btn"><i class="fa fa-floppy-o" aria-hidden="true"></i></a>
	                                <a href="" class="table-btn"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
	                                <a href="" class="table-x">
	                                    <img src="/img/x-icon.png" alt="">
	                                </a>
	                                <a href="" class="table-check">
	                                    <img src="/img/check-icon.png" alt="">
	                                </a>
	                            </td>
	                        </tr>
	                        <tr class="ui-state-default">
	                            <td>
	                                <input type="checkbox">
	                            </td>
	                            <td>1</td>
	                            <td>01/09</td>
	                            <td>셀프</td>
	                            <td>시내</td>
	                            <td>01/09</td>
	                            <td>10:00</td>
	                            <td><input class="table-input" type="text"></td>
	                            <td>캐딜락</td>
	                            <td>40두4010</td>
	                            <td>20두2010</td>
	                            <td>성남</td>
	                            <td>하남</td>
	                            <td>
	                                <form action="">
	                                    <input type="text" class="search-box">
	                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
	                                    <div class="search-result">
	                                        No result
	                                        <div class="no-result d-table">
	                                            <div class="d-tbc">
	                                                <i class="fa fa-exclamation-triangle fa-3x" aria-hidden="true"></i>
	                                                <span>No results have been found.</span>
	                                            </div>
	                                        </div>
	
	                                        Result type1
	                                        <div class="Wresult">
	                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
	                                            <p class="result-title">Campaigns</p>
	                                            <a href="" class="result-sub"><span>네오퓨쳐</span></a>
	                                            <p class="camp-type"><span>Campaign type:</span> <span>Powerlink</span></p>
	                                            <p class="camp-id"><span>Campaign ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
	                                        </div>
	
	                                        Result type2
	                                        <div class="Wresult">
	                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
	                                            <p class="result-title">Ad groups</p>
	                                            <a href="" class="result-sub"><span>네오퓨쳐</span>_#0007</a>
	                                            <p class="camp-type"><span>Campaign type:</span> <span>Powerlink</span></p>
	                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
	                                            <p class="adgroup-id"><span>Ad group ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
	                                        </div>
	
	                                        Result type3
	                                        <div class="Wresult">
	                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
	                                            <p class="result-title">Ads - Powerlink</p>
	                                            <div class="result-box">
	                                                <p class="title">네오퓨쳐</p>
	                                                <p class="content">일반 홈페이지에서 신개념 융복합 멀티미디어 홈피이지 제작, 견적상담 환영</p>
	
	                                                <ul>
	                                                    <li>View URL: http://www.neofss.com</li>
	                                                    <li>Linked URL: http://www.neofss.com</li>
	                                                </ul>
	                                            </div>
	                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
	                                            <p class="adgroup"><span>Ad group:</span> <span class="red">네오퓨쳐</span>_#0003</p>
	                                            <p class="adgroup-id"><span>Ad ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
	                                        </div>
	
	                                        Result type4
	                                        <div class="Wresult">
	                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
	                                            <p class="result-title">Ads - PowerContents</p>
	                                            <div class="result-box w-img">
	                                                <div class="d-table">
	                                                    <div class="d-tbc">
	                                                        <img src="/img/ads-img.png" alt="">
	                                                    </div>
	                                                    <div class="d-tbc">
	                                                        <p class="title">홈페이지제작 이러면 망한다!</p>
	                                                        <p class="content">홈페이지제작! 대충 업체에 맡기면 된다는 생각으로 접근하면, 십중팔구는 실패하는 이유와 홈페이지 제작에 관한 최소한의 지식은 가지고 업체를 선정해야 돈과 시간을 낭비하는 일을 줄일수 있다.</p>
	                                                    </div>
	                                                </div>
	                                                <ul>
	                                                    <li>Content created: 2017-12-15</li>
	                                                    <li>Brand Name: 네오퓨쳐</li>
	                                                    <li>View URL: http://blog.naver.com/marujeen</li>
	                                                    <li>Linked URL: http://blog.naver.com/marujeen</li>
	                                                </ul>
	                                            </div>
	                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
	                                            <p class="adgroup"><span>Ad group:</span> <span class="red">네오퓨쳐</span>_#0003</p>
	                                            <p class="adgroup-id"><span>Ad ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
	                                        </div>
	
	                                    </div>
	                                </form>
	                            </td>
	                            <td>3대</td>
	                            <td>150,000</td>
	                            <td>현금</td>
	                            <td>세금</td>
	                            <td>
	                                <a href="#" class="table-driver-btn"><i class="fa fa-truck" aria-hidden="true"></i></a>
	                                <a href="" class="table-save-btn"><i class="fa fa-floppy-o" aria-hidden="true"></i></a>
	                                <a href="" class="table-btn"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
	                                <a href="" class="table-x">
	                                    <img src="/img/x-icon.png" alt="">
	                                </a>
	                                <a href="" class="table-check">
	                                    <img src="/img/check-icon.png" alt="">
	                                </a>
	                            </td>
	                        </tr>
	                        <tr class="evenTr ui-state-default">
	                            <td>
	                                <input type="checkbox">
	                            </td>
	                            <td>1</td>
	                            <td>01/09</td>
	                            <td>셀프</td>
	                            <td>시내</td>
	                            <td>01/09</td>
	                            <td>10:00</td>
	                            <td><input class="table-input" type="text"></td>
	                            <td>캐딜락</td>
	                            <td>40두4010</td>
	                            <td>20두2010</td>
	                            <td>성남</td>
	                            <td>하남</td>
	                            <td>
	                                <form action="">
	                                    <input type="text" class="search-box">
	                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
	                                    <div class="search-result">
	                                        No result
	                                        <div class="no-result d-table">
	                                            <div class="d-tbc">
	                                                <i class="fa fa-exclamation-triangle fa-3x" aria-hidden="true"></i>
	                                                <span>No results have been found.</span>
	                                            </div>
	                                        </div>
	
	                                        Result type1
	                                        <div class="Wresult">
	                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
	                                            <p class="result-title">Campaigns</p>
	                                            <a href="" class="result-sub"><span>네오퓨쳐</span></a>
	                                            <p class="camp-type"><span>Campaign type:</span> <span>Powerlink</span></p>
	                                            <p class="camp-id"><span>Campaign ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
	                                        </div>
	
	                                        Result type2
	                                        <div class="Wresult">
	                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
	                                            <p class="result-title">Ad groups</p>
	                                            <a href="" class="result-sub"><span>네오퓨쳐</span>_#0007</a>
	                                            <p class="camp-type"><span>Campaign type:</span> <span>Powerlink</span></p>
	                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
	                                            <p class="adgroup-id"><span>Ad group ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
	                                        </div>
	
	                                        Result type3
	                                        <div class="Wresult">
	                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
	                                            <p class="result-title">Ads - Powerlink</p>
	                                            <div class="result-box">
	                                                <p class="title">네오퓨쳐</p>
	                                                <p class="content">일반 홈페이지에서 신개념 융복합 멀티미디어 홈피이지 제작, 견적상담 환영</p>
	
	                                                <ul>
	                                                    <li>View URL: http://www.neofss.com</li>
	                                                    <li>Linked URL: http://www.neofss.com</li>
	                                                </ul>
	                                            </div>
	                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
	                                            <p class="adgroup"><span>Ad group:</span> <span class="red">네오퓨쳐</span>_#0003</p>
	                                            <p class="adgroup-id"><span>Ad ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
	                                        </div>
	
	                                        Result type4
	                                        <div class="Wresult">
	                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
	                                            <p class="result-title">Ads - PowerContents</p>
	                                            <div class="result-box w-img">
	                                                <div class="d-table">
	                                                    <div class="d-tbc">
	                                                        <img src="/img/ads-img.png" alt="">
	                                                    </div>
	                                                    <div class="d-tbc">
	                                                        <p class="title">홈페이지제작 이러면 망한다!</p>
	                                                        <p class="content">홈페이지제작! 대충 업체에 맡기면 된다는 생각으로 접근하면, 십중팔구는 실패하는 이유와 홈페이지 제작에 관한 최소한의 지식은 가지고 업체를 선정해야 돈과 시간을 낭비하는 일을 줄일수 있다.</p>
	                                                    </div>
	                                                </div>
	                                                <ul>
	                                                    <li>Content created: 2017-12-15</li>
	                                                    <li>Brand Name: 네오퓨쳐</li>
	                                                    <li>View URL: http://blog.naver.com/marujeen</li>
	                                                    <li>Linked URL: http://blog.naver.com/marujeen</li>
	                                                </ul>
	                                            </div>
	                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
	                                            <p class="adgroup"><span>Ad group:</span> <span class="red">네오퓨쳐</span>_#0003</p>
	                                            <p class="adgroup-id"><span>Ad ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
	                                        </div>
	
	                                    </div>
	                                </form>
	                            </td>
	                            <td>3대</td>
	                            <td>150,000</td>
	                            <td>현금</td>
	                            <td>세금</td>
	                            <td>
	                                <a href="#" class="table-driver-btn"><i class="fa fa-truck" aria-hidden="true"></i></a>
	                                <a href="" class="table-save-btn"><i class="fa fa-floppy-o" aria-hidden="true"></i></a>
	                                <a href="" class="table-btn"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
	                                <a href="" class="table-x">
	                                    <img src="/img/x-icon.png" alt="">
	                                </a>
	                                <a href="" class="table-check">
	                                    <img src="/img/check-icon.png" alt="">
	                                </a>
	                            </td>
	                        </tr>
	                        <tr class="ui-state-default">
	                            <td>
	                                <input type="checkbox">
	                            </td>
	                            <td>1</td>
	                            <td>01/09</td>
	                            <td>셀프</td>
	                            <td>시내</td>
	                            <td>01/09</td>
	                            <td>10:00</td>
	                            <td><input class="table-input" type="text"></td>
	                            <td>캐딜락</td>
	                            <td>40두4010</td>
	                            <td>20두2010</td>
	                            <td>성남</td>
	                            <td>하남</td>
	                            <td>
	                                <form action="">
	                                    <input type="text" class="search-box">
	                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
	                                    <div class="search-result">
	                                        No result
	                                        <div class="no-result d-table">
	                                            <div class="d-tbc">
	                                                <i class="fa fa-exclamation-triangle fa-3x" aria-hidden="true"></i>
	                                                <span>No results have been found.</span>
	                                            </div>
	                                        </div>
	
	                                        Result type1
	                                        <div class="Wresult">
	                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
	                                            <p class="result-title">Campaigns</p>
	                                            <a href="" class="result-sub"><span>네오퓨쳐</span></a>
	                                            <p class="camp-type"><span>Campaign type:</span> <span>Powerlink</span></p>
	                                            <p class="camp-id"><span>Campaign ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
	                                        </div>
	
	                                        Result type2
	                                        <div class="Wresult">
	                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
	                                            <p class="result-title">Ad groups</p>
	                                            <a href="" class="result-sub"><span>네오퓨쳐</span>_#0007</a>
	                                            <p class="camp-type"><span>Campaign type:</span> <span>Powerlink</span></p>
	                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
	                                            <p class="adgroup-id"><span>Ad group ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
	                                        </div>
	
	                                        Result type3
	                                        <div class="Wresult">
	                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
	                                            <p class="result-title">Ads - Powerlink</p>
	                                            <div class="result-box">
	                                                <p class="title">네오퓨쳐</p>
	                                                <p class="content">일반 홈페이지에서 신개념 융복합 멀티미디어 홈피이지 제작, 견적상담 환영</p>
	
	                                                <ul>
	                                                    <li>View URL: http://www.neofss.com</li>
	                                                    <li>Linked URL: http://www.neofss.com</li>
	                                                </ul>
	                                            </div>
	                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
	                                            <p class="adgroup"><span>Ad group:</span> <span class="red">네오퓨쳐</span>_#0003</p>
	                                            <p class="adgroup-id"><span>Ad ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
	                                        </div>
	
	                                        Result type4
	                                        <div class="Wresult">
	                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
	                                            <p class="result-title">Ads - PowerContents</p>
	                                            <div class="result-box w-img">
	                                                <div class="d-table">
	                                                    <div class="d-tbc">
	                                                        <img src="/img/ads-img.png" alt="">
	                                                    </div>
	                                                    <div class="d-tbc">
	                                                        <p class="title">홈페이지제작 이러면 망한다!</p>
	                                                        <p class="content">홈페이지제작! 대충 업체에 맡기면 된다는 생각으로 접근하면, 십중팔구는 실패하는 이유와 홈페이지 제작에 관한 최소한의 지식은 가지고 업체를 선정해야 돈과 시간을 낭비하는 일을 줄일수 있다.</p>
	                                                    </div>
	                                                </div>
	                                                <ul>
	                                                    <li>Content created: 2017-12-15</li>
	                                                    <li>Brand Name: 네오퓨쳐</li>
	                                                    <li>View URL: http://blog.naver.com/marujeen</li>
	                                                    <li>Linked URL: http://blog.naver.com/marujeen</li>
	                                                </ul>
	                                            </div>
	                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
	                                            <p class="adgroup"><span>Ad group:</span> <span class="red">네오퓨쳐</span>_#0003</p>
	                                            <p class="adgroup-id"><span>Ad ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
	                                        </div>
	
	                                    </div>
	                                </form>
	                            </td>
	                            <td>3대</td>
	                            <td>150,000</td>
	                            <td>현금</td>
	                            <td>세금</td>
	                            <td>
	                                <a href="#" class="table-driver-btn"><i class="fa fa-truck" aria-hidden="true"></i></a>
	                                <a href="" class="table-save-btn"><i class="fa fa-floppy-o" aria-hidden="true"></i></a>
	                                <a href="" class="table-btn"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
	                                <a href="" class="table-x">
	                                    <img src="/img/x-icon.png" alt="">
	                                </a>
	                                <a href="" class="table-check">
	                                    <img src="/img/check-icon.png" alt="">
	                                </a>
	                            </td>
	                        </tr>
	                        <tr class="evenTr ui-state-default">
	                            <td>
	                                <input type="checkbox">
	                            </td>
	                            <td>1</td>
	                            <td>01/09</td>
	                            <td>셀프</td>
	                            <td>시내</td>
	                            <td>01/09</td>
	                            <td>10:00</td>
	                            <td><input class="table-input" type="text"></td>
	                            <td>캐딜락</td>
	                            <td>40두4010</td>
	                            <td>20두2010</td>
	                            <td>성남</td>
	                            <td>하남</td>
	                            <td>
	                                <form action="">
	                                    <input type="text" class="search-box">
	                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
	                                    <div class="search-result">
	                                        No result
	                                        <div class="no-result d-table">
	                                            <div class="d-tbc">
	                                                <i class="fa fa-exclamation-triangle fa-3x" aria-hidden="true"></i>
	                                                <span>No results have been found.</span>
	                                            </div>
	                                        </div>
	
	                                        Result type1
	                                        <div class="Wresult">
	                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
	                                            <p class="result-title">Campaigns</p>
	                                            <a href="" class="result-sub"><span>네오퓨쳐</span></a>
	                                            <p class="camp-type"><span>Campaign type:</span> <span>Powerlink</span></p>
	                                            <p class="camp-id"><span>Campaign ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
	                                        </div>
	
	                                        Result type2
	                                        <div class="Wresult">
	                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
	                                            <p class="result-title">Ad groups</p>
	                                            <a href="" class="result-sub"><span>네오퓨쳐</span>_#0007</a>
	                                            <p class="camp-type"><span>Campaign type:</span> <span>Powerlink</span></p>
	                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
	                                            <p class="adgroup-id"><span>Ad group ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
	                                        </div>
	
	                                        Result type3
	                                        <div class="Wresult">
	                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
	                                            <p class="result-title">Ads - Powerlink</p>
	                                            <div class="result-box">
	                                                <p class="title">네오퓨쳐</p>
	                                                <p class="content">일반 홈페이지에서 신개념 융복합 멀티미디어 홈피이지 제작, 견적상담 환영</p>
	
	                                                <ul>
	                                                    <li>View URL: http://www.neofss.com</li>
	                                                    <li>Linked URL: http://www.neofss.com</li>
	                                                </ul>
	                                            </div>
	                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
	                                            <p class="adgroup"><span>Ad group:</span> <span class="red">네오퓨쳐</span>_#0003</p>
	                                            <p class="adgroup-id"><span>Ad ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
	                                        </div>
	
	                                        Result type4
	                                        <div class="Wresult">
	                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
	                                            <p class="result-title">Ads - PowerContents</p>
	                                            <div class="result-box w-img">
	                                                <div class="d-table">
	                                                    <div class="d-tbc">
	                                                        <img src="/img/ads-img.png" alt="">
	                                                    </div>
	                                                    <div class="d-tbc">
	                                                        <p class="title">홈페이지제작 이러면 망한다!</p>
	                                                        <p class="content">홈페이지제작! 대충 업체에 맡기면 된다는 생각으로 접근하면, 십중팔구는 실패하는 이유와 홈페이지 제작에 관한 최소한의 지식은 가지고 업체를 선정해야 돈과 시간을 낭비하는 일을 줄일수 있다.</p>
	                                                    </div>
	                                                </div>
	                                                <ul>
	                                                    <li>Content created: 2017-12-15</li>
	                                                    <li>Brand Name: 네오퓨쳐</li>
	                                                    <li>View URL: http://blog.naver.com/marujeen</li>
	                                                    <li>Linked URL: http://blog.naver.com/marujeen</li>
	                                                </ul>
	                                            </div>
	                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
	                                            <p class="adgroup"><span>Ad group:</span> <span class="red">네오퓨쳐</span>_#0003</p>
	                                            <p class="adgroup-id"><span>Ad ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
	                                        </div>
	
	                                    </div>
	                                </form>
	                            </td>
	                            <td>3대</td>
	                            <td>150,000</td>
	                            <td>현금</td>
	                            <td>세금</td>
	                            <td>
	                                <a href="#" class="table-driver-btn"><i class="fa fa-truck" aria-hidden="true"></i></a>
	                                <a href="" class="table-save-btn"><i class="fa fa-floppy-o" aria-hidden="true"></i></a>
	                                <a href="" class="table-btn"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
	                                <a href="" class="table-x">
	                                    <img src="/img/x-icon.png" alt="">
	                                </a>
	                                <a href="" class="table-check">
	                                    <img src="/img/check-icon.png" alt="">
	                                </a>
	                            </td>
	                        </tr> -->
	
	
	                    </tbody>
	                </table>
	                <div class="table-pagination text-center">
	                    <ul class="pagination">
	                    	<html:paging uri="/allocation/combination.do" frontYn="N" />
	                       <!--  <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
	                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
	                        <li class="curr-page"><a href="#">1</a></li>
	                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
	                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li> -->
	                    </ul>
	                </div>                
	            </section>
            
            	<div class="confirmation">
                    <div class="cancel">
                        <a style="cursor:pointer;" onclick="javascript:batchStatus('cancel');">일괄취소</a>
                    </div>
                    <div class="confirm">
                        <a style="cursor:pointer;" onclick="javascript:batchStatus('complete');">일괄완료</a>
                    </div>
                </div>
            </div>
        </div>        
        <!-- <iframe style="width: 980px; height:10000px; border: none;" frameBorder="0" id="happyboxFrame" scrolling="no" src="https://www.happyalliance-happybox.org/Bridge?v=param"></iframe>​    -->
       <script>
		if("${userMap.control_grade}" == "01"){
			$('#dataTable').SetEditable({
            	columnsEd: "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15",
            	 onEdit: function(row) {updateAllocaion(row)},  
            	 onDelete: function() {},  
            	 onBeforeDelete: function() {}, 
            	 onAdd: function() {}  
            });
			
		}          
            </script>

