<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/js/bootstable.js"></script>
<script type="text/javascript"> 
var oldListOrder = "";
var allocationId = "";




$(document).ready(function(){
	//forOpen

	
	$('#dataTable tr:odd td').css("backgroundColor","#f9f9f9");
	
	$("#startDt").datepicker({
		dateFormat : "yy-mm-dd",
		onSelect : function(date){
			//$("#endDt").focus();
			setTimeout(function(){
				$("#endDt").focus();  
	        }, 50);
		}
	});
	
	
	
});

function updateAllocaion(row){
	var inputDt = "";
    var carrierType = "";
    var distanceType = "";
    var departureDt = "";
    var departureTime = "";
    var customerName = "";
    var carKind = "";
    var carIdNum = "";
    var carNum  = "";
    var departure = "";
    var arrival  = "";
    var driverName = "";
    var carCnt  = "";
    var amount   = "";
    var paymentKind = "";
	var allocId = $(row).attr("allocationId");
	if(confirm("수정 하시겠습니까?")){
		$(row).find("td").each(function(index,element){
			if(index==1){
				inputDt = $(this).html();
			}
			if(index==2){
				if($(this).html() == "셀프"){
					carrierType = "S";	
				}else if($(this).html() == "캐리어"){
					carrierType = "C";
				}else{
					alert("배차구분을 확인 하세요.");
					return false;
				}
			}
			if(index==3){
				if($(this).html() == "시내"){
					distanceType = "0";	
				}else if($(this).html() == "시외"){
					distanceType = "1";
				}else{
					alert("거리구분을 확인 하세요.");
					return false;
				}
			}
			if(index==4){
				departureDt = $(this).html();
			}
			if(index==5){
				departureTime = $(this).html();
			}
			if(index==6){
				customerName = $(this).html();
			}
			if(index==7){
				carKind = $(this).html();
			}
			if(index==8){
				carIdNum = $(this).html();
			}
			if(index==9){
				carNum = $(this).html();
			}
			if(index==10){
				departure = $(this).html();
			}
			if(index==11){
				arrival = $(this).html();
			}
			if(index==12){
				driverName = $(this).html();
			}
			if(index==13){
				carCnt = $(this).html();
			}
			if(index==14){
				amount = $(this).html();
			}
			if(index==15){
				paymentKind = $(this).html();
			}
      	});

		if(carrierType == "" || distanceType == ""){
			return false;	
		}
		
		 $.ajax({ 
				type: 'post' ,
				url : "/allocation/update-allocation.do" ,
				dataType : 'json' ,
				data : {
					inputDt : inputDt,
					carrierType : carrierType,
					distanceType : distanceType,
					departureDt : departureDt,
					departureTime : departureTime,
					customerName : customerName,
					carKind : carKind,
					carIdNum : carIdNum,
					carNum : carNum,
					departure : departure,
					arrival : arrival,
					driverName : driverName,
					carCnt : carCnt,
					amount : amount,
					paymentKind : paymentKind,
					allocationId : allocId
				},
				success : function(data, textStatus, jqXHR)
				{
					var result = data.resultCode;
					if(result == "0000"){
						alert("변경 되었습니다.");
			//			document.location.href = "/allocation/combination.do";
					}else if(result == "0001"){
   						alert("변경 하는데 실패 하였습니다.");
   					}
				} ,
				error : function(xhRequest, ErrorText, thrownError) {
				}
			}); 
		
	}

}

function getListId(obj){
	$('html').scrollTop(0);
	selectList($(obj).parent().parent().parent().attr("allocationId"));
	
}


function clipBoardCopy(allocationId,obj){
	  
	  $.ajax({ 
			type: 'post' ,
			url : "/allocation/selectAllocationInfo.do" ,
			dataType : 'json' ,
			data : {
				allocationId : allocationId
			},
			success : function(data, textStatus, jqXHR)
			{
							
				var result = data.resultCode;
				var resultMsg = data.resultMsg;
				var resultData = data.resultData;
				var resultDataSub = data.resultDataSub;
				if(result == "0000"){
					var t = document.createElement("textarea");
					t.setAttribute("id", "forClipboard");
					document.body.appendChild(t);		
					var clipBoardText = "";
					var departure_time = "";

					if(resultDataSub.departure_time != undefined){
						departure_time = resultDataSub.departure_time; 
					}
					
					clipBoardText += ""+resultDataSub.departure_dt_str+"("+resultDataSub.departure_dt_d_o_w+") "+departure_time+"      "+resultData.department+" "+resultData.customer_name+"\r\n";
					clipBoardText += ""+resultData.charge_name+" "+resultData.charge_phone+"\r\n";
					clipBoardText += ""+resultDataSub.car_kind+" "+resultDataSub.car_id_num+" "+resultDataSub.car_num+"\r\n";
					
					clipBoardText += "상차 "+resultDataSub.departure+" "+resultDataSub.departure_addr+"\r\n";
					if(resultDataSub.departure_person_in_charge != "" || resultDataSub.departure_phone != ""){
						clipBoardText += ""+resultDataSub.departure_person_in_charge+" "+resultDataSub.departure_phone+"\r\n";	
					}
					clipBoardText += "하차 "+resultDataSub.arrival+" "+resultDataSub.arrival_addr+"\r\n";
					if(resultDataSub.arrival_person_in_charge != "" || resultDataSub.arrival_phone != ""){
						clipBoardText += ""+resultDataSub.arrival_person_in_charge+" "+resultDataSub.arrival_phone+"\r\n";	
					}

					clipBoardText += ""+resultDataSub.etc
					t.value = clipBoardText;
					t.select();
					document.execCommand('copy');
					$(".clipboard").attr("data-clipboard-target","#forClipboard");
					
					document.body.removeChild(t);
					alert("배차 정보가 클립 보드에 복사 되었습니다.");
				}else{
					alert("배차 정보를 가져 오는데 실패 했습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
	  
}




var driverArray = new Array();
function selectList(id){
	
	var searchWord = encodeURI('${paramMap.searchWord}');
	document.location.href="/allocation/allocation-view.do?&searchDateType=${paramMap.searchDateType}&startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&searchType=${paramMap.searchType}&cPage=${paramMap.cPage}&allocationStatus=${paramMap.allocationStatus}&location=${paramMap.location}&allocationId="+id+"&searchWord="+searchWord;
	
}	
	
function batchStatus(status){
	
	var id = "";
	var total = $('input:checkbox[name="forBatch"]:checked').length;
	
	if(total == 0){
		alert("목록이 선택 되지 않았습니다.");
		return false;
	}else{

		var inputString="";

		if(status == "X"){
			inputString = prompt('취소 사유를 작성 해 주세요.', '');

			if(inputString == null){
				return false;
			}else if( inputString == "" || inputString == undefined || ( inputString != null && typeof inputString == "object" && !Object.keys(inputString).length ) ){
				alert("취소 사유가 작성 되지 않았습니다.");
				return false;
			}else{	
				$('input:checkbox[name="forBatch"]:checked').each(function(index,element) {
				      if(this.checked){//checked 처리된 항목의 값
				    	  id+=$(this).attr("allocationId");
				    	  if(index<total-1){
				    		  id += ","; 
					         } 
				      }
				 });	
				updateAllocationStatus(status,id,inputString);
			}
			
		}else{
			$('input:checkbox[name="forBatch"]:checked').each(function(index,element) {
			      if(this.checked){//checked 처리된 항목의 값
			    	  id+=$(this).attr("allocationId");
			    	  if(index<total-1){
			    		  id += ","; 
				         } 
			      }
			 });	
			updateAllocationStatus(status,id,inputString);

		}

		
	}
}	


function updateAllocationStatus(status,id,addMsg){
	
	var msg = "";
	if(status == "C"){
		msg = "취소";
	}else if(status == "F"){
		msg = "완료";
	}else if(status == "U"){
		msg = "미확정으로 변경";
	}else if(status == "N"){
		msg = "미배차로 변경";
	}else if(status == "X"){
		msg = "취소 요청";
	}

	if(confirm(msg+"하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : "/allocation/updateAllocationStatus.do" ,
			dataType : 'json' ,
			data : {
				allocationStatus : status,
				allocationId : id,
				cancelReason : addMsg
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert(msg+"되었습니다.");
					var searchWord = encodeURI('${paramMap.searchWord}');
					document.location.href = "/allocation/combination.do?&cPage=${paramMap.cPage}&startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&allocationStatus=${paramMap.allocationStatus}&searchType=${paramMap.searchType}&searchWord="+searchWord;
				}else if(result == "0001"){
					alert(msg+"하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
}		
	
function checkAll(){

	if($('input:checkbox[id="checkAll"]').is(":checked")){
		$('input:checkbox[name="forBatch"]').each(function(index,element) {
			$(this).prop("checked","true");
		 });	
	}else{
		$('input:checkbox[name="forBatch"]').each(function(index,element) {
			$(this).prop("checked","");
		 });
	}
}
	
	
	
function move(location){
	
	
	window.location.href = "/allocation/"+location+".do";
	
}	
	
	
function excelDownload(carrierType){
	
	
	if(confirm("다운로드 하시겠습니까?")){
		document.location.href = "/allocation/excel_download.do?&carrierType="+carrierType+"&searchDateType="+$("#searchDateType").val()+"&startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val()+"&searchType="+$("#searchType").val()+"&searchWord="+encodeURI($("#searchWord").val());		
	}
	
	
}	
	
	


function sendSocketMessage(){
	
	$.ajax({ 
		type: 'post' ,
		url : "http://52.78.153.148:8080/allocation/sendSocketMessage.do" ,
		dataType : 'json' ,
		data : {
			allocationId : 'ALO3df74bd0105046bfbdce51ab0e64927c',
			driverId : "sourcream"
		},
		success : function(data, textStatus, jqXHR)
		{
			var result = data.resultCode;
			var resultData = data.resultData;
			if(result == "0000"){
				//alert("성공");
			}else if(result == "0001"){
			
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
}
	


var order = "${order}";
function sortby(gubun){

	if(order == ""){
		order = "asc";
	}else if(order == "asc"){
		order = "desc";
	}else if(order == "desc"){
		order = "";
	}
	
	var loc = document.location.href;
	var str = "";
	if(loc.indexOf("?") > -1){
		//forOrder 가 있는경우 ㅎㅎ
		if(loc.indexOf("forOrder") > -1){
			var queryString = loc.split("?");
			var query = queryString[1].split("&");
			
			for(var i = 0; i < query.length; i++){
				if(query[i].indexOf("forOrder") > -1){
					query[i] = "forOrder="+gubun+"%5E"+order;
				}
			}
			for(var i = 0; i < query.length; i++){
				if(query[i] != ""){
					str += "&"+query[i];	
				}
			}
			document.location.href = queryString[0]+"?"+str;
		}else{
			str="&forOrder="+gubun+"%5E"+order;
			document.location.href = loc+str;
		}
		
	}else{
		str="?&forOrder="+gubun+"%5E"+order;
		document.location.href = loc+str;
	}
	
}



function pickUpValidation(status){


	var allocationIdArr = new Array();
	
	var total = $('input:checkbox[name="forBatch"]:checked').length;
	var validationStatus = true;

	if(total == 0){
		alert("목록이 선택 되지 않았습니다.");
		return false;
	}else if(total == 1){
		alert("1개 이상의 배차건이 선택 되어야 합니다.");
		return false;
	}else if(total > 3){
		alert("4개 이상의 배차건에 대한 픽업은 관리자에게 문의 하세요.");
		return false;
	}
	
	if(status == "P"){

		$('input:checkbox[name="forBatch"]:checked').each(function(index,element) {

			if($(this).attr("batchStatus") != "N"){
				alert((index+1)+"번째 선택된 배차건은 픽업 또는 일괄배차로 이미 등록 되어 있습니다.")
				validationStatus = false;
				return false;
			}else{
				allocationIdArr.push($(this).attr("allocationId"))
			}
		});

	}else{

		$('input:checkbox[name="forBatch"]:checked').each(function(index,element) {

			if($(this).attr("batchStatus") == "N"){
				alert((index+1)+"번째 선택된 배차건은 이미 개별로 등록 되어 있습니다.");
				validationStatus = false;
				return false;
			}else{
				allocationIdArr.push($(this).attr("allocationId"))
				
			}

		});

	}

	if(validationStatus){
		setPickup(allocationIdArr,status);
	}
	
}




function setPickup(allocationIdArr,status){


	if(confirm("해당 배차를 픽업으로 설정 하시겠습니까?")){

		$.ajax({ 
			type: 'post' ,
			url : "/allocation/setPickup.do" ,
			dataType : 'json' ,
			traditional : true,
			data : {
				allocationIdArr : allocationIdArr,
				batchStatus : status
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				if(result == "0000"){
					alert("픽업으로 설정 되었습니다.");
					window.location.reload();
				}else if(result == "0001"){
					alert("픽업으로 설정 하는데 실패 하였습니다. 관리자에게 문의 하세요.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});

	}else{

		alert("취소 되었습니다.");
		return false;
	}

	
}






function setConfirmation(status){


	var allocationIdArr = new Array();
	
	var total = $('input:checkbox[name="forBatch"]:checked').length;
	var validationStatus = true;

	if(total == 0){
		alert("목록이 선택 되지 않았습니다.");
		return false;
	}

	if(status == "N"){
		

		$('input:checkbox[name="forBatch"]:checked').each(function(index,element) {

			if($(this).attr("allocationStatus") == "N"){
				alert((index+1)+"번째 선택된 배차건은 이미 확정 처리 되어 있습니다.")
				validationStatus = false;
				return false;
			}else{
				allocationIdArr.push($(this).attr("allocationId"))
			}
		});

	}else{
		
	/* 
		$('input:checkbox[name="forConfirm"]:checked').each(function(index,element) {

			if($(this).attr("allocationStatus") == "N"){
				alert((index+1)+"번째 선택된 배차건은 이미 개별로 등록 되어 있습니다.");
				validationStatus = false;
				return false;
			}else{
				allocationIdArr.push($(this).attr("allocationId"))
				
			}

		}); */

	}

	if(validationStatus){
		confirmAllocation(allocationIdArr,status);
	}
	
}

function goAllocationRegisterPage(){

	if(confirm("신규 배차 입력 페이지로 이동하시겠습니까?")){

			document.location.href="/allocation/allocation-register.do";
			
	}else{

		return false;
	}
	
}



function confirmAllocation(allocationIdArr,status){


			if(confirm("해당 예약배차를 확정으로 처리 하시겠습니까?")){
		
				$.ajax({ 
					type: 'post' ,
					url : "/allocation/confirmAllocation.do" ,
					dataType : 'json' ,
					traditional : true,
					data : {
						allocationIdArr : allocationIdArr,
						 allocationStatus : status 
					},
					success : function(data, textStatus, jqXHR)
					{
						var result = data.resultCode;
						if(result == "0000"){
							alert("탁송 예약 확정으로 설정 되었습니다.");
							window.location.reload();
						}else if(result == "0001"){
							alert("확정으로 설정 하는데 실패 하였습니다. 관리자에게 문의 하세요.");
						}
					} ,
					error : function(xhRequest, ErrorText, thrownError) {
					}
				});
		
			}else{
		
				alert("취소 되었습니다.");
				return false;
			}
		
}


function goHkcarrierAppAllocatinoList(){



	if(confirm("고객용 어플로 들어온 배차리스트로 이동하시겠습니까?")){

		document.location.href="/allocation/combination.do?&regType=A";

	}else{

		alert("취소됨");
		return false;
		}

}


/*
function goInsertAllocationUpdate(){

		var allocationIdArr = new Array();

		var checkBox = $('input:checkbox[name="forBatch"]:checked').length;
		
		var insertStatus = true;
		
			if(checkBox == 0){
				alert("목록이 선택 되지 않았습니다.");
				insertStatus =false;
				return false;
				
			}else{

				allocationIdArr.push($(this).attr("allocationId"))
				insertStatus = true;

				}

				if(insertStatus){
				goUpdateAllocation(allocationIdArr);

			}
	
}

*/

/*

	function goUpdateAllocation(allocationIdArr){

	if(confirm("해당 배차건들 덮어쓰기하시겠습니까?")){


		
		$.ajax({ 
			type: 'post' ,
			url : "/allocation/updateDupAllocation.do" ,
			dataType : 'json' ,
			traditional : true,
			data : {
				allocationIdArr : allocationIdArr,
				fileId : fileId,

			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				if(result == "0000"){
					alert("배차가 등록되었습니다.");
					window.location.reload();
				}else if(result == "0001"){
					alert("");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
		

			
	}else{
	alert("취소됨");
	return false;
	
		}
	
	}

*/


function goInsertAllocationUpdate(){

	var allocationIdArr = new Array();

	var checkBox = $('input:checkbox[name="forBatch"]:checked').length;
//	var allocationId= $('input:checkbox[name="forBatch"]:checked').attr("allocationId");
	var allocationId = "";
	
	var fileId ="${paramMap.fileId}";
	
	var insertStatus = true;
	
		if(checkBox == 0){
			alert("목록이 선택 되지 않았습니다.");
			insertStatus =false;
			return false;
			
		}else{

			$('input:checkbox[name="forBatch"]:checked').each(function(index,element) {
				//allocationId += $(this).attr("allocationId")+",";
				allocationIdArr.push($(this).attr("allocationId"));
			});
			
			//allocationIdArr.push(allocationId);
			insertStatus = true;
		
			}

			if(insertStatus){
			goUpdateAllocation(allocationIdArr,fileId,checkBox);
		
		}

}


function goUpdateAllocation(allocationIdArr,fileId,checkBox){

//$('input:checkbox[name="forBatch"]:checked').each(function(index,element) {
	
	if(confirm("덮어쓰기 할 배차 건 "+checkBox+"건 이대로 진행하시겠습니까?")){

		$.ajax({ 
			type: 'post' ,
			url : "/allocation/updateDupAllocation.do" ,
			dataType : 'json' ,
			traditional : true,
			data : {
				allocationIdArr : allocationIdArr,
				fileId : fileId,
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				if(result == "0000"){
					alert("배차가 등록되었습니다.");
					document.location.href ="/allocation/combination.do";	
							
				}else if(result == "0001"){
					alert("알수없는 오류입니다. 관리자에게 문의해주세요.");
					return false;
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
		
		}else{
			alert("취소됨");
			return false;
		
			}

//	});
	
}


 </script>


<div class="modal-field">
            <div class="modal-box">
                <h3 class="text-center">기사리스트</h3>
            <div class="modal-table-container">
                <table class="article-table">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <!-- <td>소유주</td> -->
                            <td>기사명</td>
                            <td>연락처</td>
                            <td>차량번호</td>
                            <td>차종</td>
                        </tr>
                    </thead>
                    <tbody id="driverSelectList">
                    	<c:forEach var="data" items="${driverList}" varStatus="status" >
                    		<tr class="ui-state-default" style="cursor:pointer;" driverId="${data.driver_id}" onclick="javascript:setDriver('${data.driver_id}','${data.driver_name}','${data.car_kind}');"> 
	                            <%-- <td>${data.driver_owner}</td> --%>
	                            <td>${data.driver_name}</td>
	                            <td>${data.phone_num}</td>
	                            <td>${data.car_num}</td>
	                            <td>${data.car_kind}</td>
                        	</tr>
						</c:forEach>
                    </tbody>
                </table>
                </div>
                <div class="confirmation">
                    <div class="confirm">
                        <input type="button" value="취소" name="">
                    </div>
                </div>
            </div>
        </div>
 		<%-- <section class="side-nav">
            <ul>
                <li class="<c:if test="${complete == ''}">active</c:if>"><a href="/allocation/combination.do">신규배차입력</a></li>
                <li><a href="/allocation/self.do">셀프 배차  </a></li>
                <li><a href="/allocation/self.do?reserve=N">셀프 예약</a></li>
                <li><a href="/allocation/self.do?reserve=Y">셀프 배차 현황</a></li>
                <li><a href="/allocation/carrier.do">캐리어 배차</a></li>
                <li><a href="/allocation/carrier.do?reserve=N">캐리어 예약</a></li>
                <li><a href="/allocation/carrier.do?reserve=Y">캐리어 배차 현황</a></li>
                <li class="<c:if test="${complete == 'Y'}">active</c:if>"><a href="/allocation/combination.do?forOpen=N&complete=Y">완료 배차</a></li>
            </ul>
        </section> --%>

        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">HOME</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">배차 중복 리스트 현황</a></li>
                </ul>
            </div>

      <%--       <div class="up-dl clearfix">
                
                <form name="searchForm" method="get" action="combination.do">
                	<div class="date-picker">
                		<select name="searchDateType" id="searchDateType">
							<option value="" <c:if test='${paramMap.searchDateType eq "" }'> selected="selected"</c:if> >선택</option>
							<option value="S" <c:if test='${paramMap.searchDateType eq "S" }'> selected="selected"</c:if> >의뢰일</option>
							<option value="D" <c:if test='${paramMap.searchDateType eq "D" }'> selected="selected"<</c:if> >출발일</option>
						</select>
	                	<input style="width:150px;" autocomplete="off" id="startDt" placeholder="검색 시작일" name="startDt" type="text" class="datepick" value="${paramMap.startDt}"> ~ <input style="width:150px;" autocomplete="off" id="endDt" placeholder="검색 종료일" name="endDt"  type="text" class="datepick" value="${paramMap.endDt}">
	                </div>
	                <div class="upload-btn" style="float:right;">
	                    <!-- <input type="button" value="엑셀 업로드"> -->
	                    <c:if test="${user.dept != 'sunbo'}">
	                    	<input type="button"  onclick="javascript:excelDownload('');" value="엑셀 다운로드">
	                    </c:if>
	                    <c:if test="${order == null || order == ''}">
			                    		<input type="button" value="리스트 순서 변경 활성화" onclick="javascript:listOrderChangePossible(this);">
			                    		<input type="button" value="리스트 순서 변경" onclick="javascript:listOrderChange();" style="display:none;">
						</c:if>
						 <br>
							<!-- <div class ="appAllocationList" style ="margin-top:20px;">	                    
		                    	<input type="button"  onclick="javascript:goHkcarrierAppAllocatinoList('');" value="APP 배차건 보기">
		                    </div> -->
	                </div>
	                    <!-- <input type="button"  onclick="javascript:sendSocketMessage();" value="소켓 메세지"> -->
	                   
					<div class="btns-submit">	
						<div class="right">
							<select name="searchType"  id="searchType">
								<option value="" <c:if test='${paramMap.searchType eq "" }'> selected="selected"</c:if> >선택</option>
								<c:if test="${user.emp_id == 'hk0000' || user.emp_id == 'hk0009'}">
								<option value="total" <c:if test='${paramMap.searchType eq "total" }'> selected="selected"</c:if> >통합검색</option>
								</c:if>
								<option value="carrier" <c:if test='${paramMap.searchType eq "carrier" }'> selected="selected"</c:if> >배차구분</option>
								<option value="distance" <c:if test='${paramMap.searchType eq "distance" }'> selected="selected"<</c:if> >운행구분</option>
								<option value="customer" <c:if test='${paramMap.searchType eq "customer" }'> selected="selected"<</c:if> >고객명</option>
								<option value="driver" <c:if test='${paramMap.searchType eq "driver" }'> selected="selected"<</c:if> >기사명</option>		
								<option value="carIdNum" <c:if test='${paramMap.searchType eq "carIdNum" }'> selected="selected"<</c:if> >차대번호</option>
								<option value="carNum" <c:if test='${paramMap.searchType eq "carNum" }'> selected="selected"<</c:if> >차량번호</option>
								<option value="contractNum" <c:if test='${paramMap.searchType eq "contractNum" }'> selected="selected"<</c:if> >계약번호</option>				
							</select>
							<input type="text" id="searchWord" name="searchWord" value="${paramMap.searchWord}" placeholder="검색어 입력"  onkeypress="if(event.keyCode=='13'){encodeURI($(this).val()); document.searchForm.submit();}" />
							<a class="btn-gray" href="javascript:" onclick=" encodeURI($(this).prev().val()); document.searchForm.submit()"><input type="button" value="검색"></a>
							<c:if test="${user.emp_id == 'hk0000' || user.emp_id == 'HK0008' || user.emp_id == 'hk0021' || user.emp_id == 'hk0029' || user.emp_id == 'hk0030'}">
							<c:if test="${user.dept != 'sunbo'}">
								<select class="dropdown" style="margin-left:20px;" name="allocationStatus" onchange="javascript:document.searchForm.submit();">
									<option  value="">모든상태</option>
									<c:forEach var="data" items="${allocationStatusList}" varStatus="status" >
										<c:if test="${data.allocation_status_cd != 'C' && data.allocation_status_cd != 'M' && data.allocation_status_cd != 'U' && data.allocation_status_cd != 'D' && data.allocation_status_cd != 'P' && data.allocation_status_cd != 'B' && data.allocation_status_cd != 'X'  && data.allocation_status_cd != 'C'  && data.allocation_status_cd != 'A' && data.allocation_status_cd != 'R' && data.allocation_status_cd != 'S' && data.allocation_status_cd != 'I'}">
											<option  value="${data.allocation_status_cd}" <c:if test='${data.allocation_status_cd eq paramMap.allocationStatus}'> selected="selected"</c:if> >${data.allocation_status}</option>
										</c:if>
									</c:forEach>
								</select>
							</c:if>
							<c:if test="${user.dept != 'sunbo'}">
		                    	<input type="button" style="margin-left:20px;"  onclick="javascript:goHkcarrierAppAllocatinoList('');" value="APP 배차건 보기">
		                    </c:if>
						</div>
					</div>			
				</form>
                
                
            </div> --%>

            <div class="dispatch-btn-container">
                <!-- <div class="dispatch-btn">
                    <i id="downArrow" class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div> -->
                
                <%-- <c:if test="${complete == null || complete != 'Y'}"> --%>
                	<%-- <c:if test="${forOpen == null || forOpen != 'N'}">
						<div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">신규배차입력</div>
					</c:if> --%>
					<%-- <c:if test="${forOpen != null && forOpen == 'N'}"> --%>
						<div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">배차 중복 리스트 현황</div>
						   
						   ※ 체크박스를 선택하시고 <span style="color:#01A9DB;"> 덮어쓰기 </span>버튼을 누르시면 배차가 등록됩니다.
						 <br>
					  <!--  ※ 경기사업자 배차건들은 <span style="color:#9A2EFE;"> 보라색</span>으로 표시 됩니다. --> 
					<%-- </c:if>
				</c:if> --%>
                
                <%-- <c:if test="${complete != null && complete == 'Y'}">
					<div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">완료배차</div>					
				</c:if> --%>
                
            </div>
        </section>

        <div class="dispatch-wrapper">
            <%-- <jsp:include page="common-form.jsp"></jsp:include> --%>
            <section class="dispatch-bottom-content">
			</section>
			<div id="bottom-table">
	            <section  class="bottom-table" style="width:1600px; margin-left:10px;">
	            	<!-- <p>차대번호를 기사님이 입력한 경우에는 붉은색으로 표시 되고 배차 직원이 입력 한 경우에는 파란색으로 표시 됩니다. </p> -->
	            	<!-- <div style="color:#8B0000;">※차대번호를 기사님이 입력한 경우에는 붉은색으로 표시 되고 배차 직원이 입력 한 경우에는 파란색으로 표시 됩니다.</div> -->
	                <table class="article-table forToggle" id="dataTable" style="width:100%;">
	                    <colgroup>
	                       <col width="auto">
	                        <col width="130px;">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="260px;">
	                        <col width="100px;">
	                        <col width="100px;">
	                        <%-- <col width="auto"> --%>
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="100px;">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <%-- <c:if test="${user.control_grade == '01' }">
	                        	<col width="auto">
	                        	<col width="auto">
	                        </c:if> --%>
	                    </colgroup>
	                    <thead>
	                        <tr>
	                            <td class="showToggle"><input type="checkbox" id="checkAll" onclick="javascript:checkAll();" ></td>
	                            <!-- <td class="showToggle">번호</td> -->
	                            <td>의뢰일 </td>
	                            <td>배차구분</td>
	                            <td>운행구분</td>
	                            <td>고객명 /담당자명 </td>
	                            <c:if test="${user.dept == 'sunbo'}">
	                            	<td>고객구분</td>
	                            </c:if>
	                            <td style="cursor:pointer;" onclick="javascript:sortby('E');">출발일<c:if test='${paramMap.forOrder eq "E" }'><c:if test='${paramMap.order eq "asc" }'><img style="margin-left:10px;" src="/img/arrow-up.png" alt=""></c:if><c:if test='${paramMap.order eq "desc" }'><img style="margin-left:10px;" src="/img/arrow-down.png" alt=""></c:if></c:if></td>
	                            <td>출발시간</td>
	                            <!-- <td>비고</td> -->
	                            <td>차종</td>
	                            <!-- <td>등록구분</td> -->
	                            <td>차대번호</td>
	                            <td>차량번호</td>
	                            <td>출발지</td>
	                            <td>하차지</td>
	                            <td>기사명(회차)</td>
	                            <c:if test="${user.dept != 'sunbo'}">
	                            	<td>매출액</td>
	                            </c:if>
	                            <c:if test="${user.dept != 'sunbo'}">
		                            <td>기사지급액</td>
	                            </c:if>
	                            <td>상태</td>
	                         	<%-- <c:if test="${user.control_grade == '01' }">   
	                            	<td class="showToggle"></td>
	                            	<td>수정</td>
	                            </c:if> --%>
	                        </tr>
	                    </thead>
	                    <tbody id="">
	                    	<c:forEach var="data" items="${listData}" varStatus="status">
								<tr class="ui-state-default" list-order="${data.list_order}" allocationId="${data.allocation_id}">
	                            <td class="showToggle"><input type="checkbox" name="forBatch" allocationId="${data.allocation_id}"  batchStatus="${data.batch_status}"> 
								<td>${data.input_dt}</td>
								<td>${data.allocation_division}</td>
								<td>${data.run_division}</td>
	                            <td style="cursor:pointer;" onclick="javascript:selectList('${data.allocation_id}');">
	                            <c:if test="${data.missing_send =='F'}"><div style ="background-color: red;  border-radius:4px; width:8px; height:8px; display: inline-block; margin-right:5px;" ></div></c:if>
	                            <c:if test="${data.missing_send == 'Y'}"><div style ="background-color: #00FF00;  border-radius:4px; width:10px; height:10px; display: inline-block; margin-right:5px;"></div></c:if>
	                          
	                           <c:if test ="${data.company_id eq 'company2'}">
	                            ${data.customer_name} / ${data.charge_name}
	                            </c:if>
	                         
	                           <c:if test ="${data.company_id eq 'company1'}">
	                         	<span style="font-weight: bold; color: #9A2EFE;" >   ${data.customer_name} / ${data.charge_name}  </span>
	                            </c:if>
	                         
	                            </td>
	                            <c:if test="${user.dept == 'sunbo'}">
	                            	<td>${data.charge_dept}</td>
	                            </c:if>
	                            <td>${data.departure_dt}</td>
	                            <td>${data.departure_time}</td>
	                            <%-- <td>${data.etc}</td> --%>
	                            <td><span style="font-weight: bold; color: #FE2E2E" >${data.car_kind}</span></td>
	                            <%-- <td>${data.batch_status_name}</td> --%>
	                            <c:if test="${data.driver_mod_yn == 'Y'}">
	                            	<td style="color:#F00;"><span style="font-weight: bold; color: #FE2E2E" >${data.car_id_num}</span></td>
	                            </c:if>
	                            <c:if test="${data.driver_mod_yn == 'N' && data.mod_emp != null && data.mod_emp != ''}">
	                            	<td style="color:#00F;"><span style="font-weight: bold; color: #FE2E2E" >${data.car_id_num}</span></td>
	                            </c:if>
	                            <c:if test="${data.driver_mod_yn == 'N' && data.mod_emp == null || data.mod_emp == ''}">
	                            	<td><span style="font-weight: bold; color: #FE2E2E" >${data.car_id_num}</span></td>
	                            </c:if>
	                            <td><span style="font-weight: bold; color: #FE2E2E" >${data.car_num}</span></td>
	                            <td>${data.departure}</td>
	                            <td>${data.arrival}</td>
	                            <%-- <td>${data.driver_status}${data.driver_name}<c:if test="${data.allocation_status != 'N'}">(${data.driver_cnt})</c:if></td> --%>
	                            <td>${data.driver_status}${data.driver_name}<c:if test="${data.driver_name != null && data.driver_name != ''}">(${data.driver_cnt})</c:if></td>
	                            <c:if test="${user.dept != 'sunbo'}">
	                            	<td>${data.sales_total}</td>
	                            </c:if>	
	                            <c:if test="${user.dept != 'sunbo'}">                           
		                            <td>${data.driver_amount}</td>
	                            </c:if>
	                            <c:if test="${data.allocation_status != 'C'}">
	                            	<td>${data.allocation_status_name}
	                            	<span><img style="width:15px; height:15px; cursor:pointer;" src="/img/question-icon.png" onclick="javascript:clipBoardCopy('${data.allocation_id}',this);" alt=""  title='비고 : ${data.etc}&#10;' ></span>
	                            	</td>
	                            </c:if>
	                            <c:if test="${data.allocation_status == 'C'}">
	                            	<td style="color:#F0F;">${data.allocation_status_name}
	                            	<span><img style="width:15px; height:15px; cursor:pointer;" src="/img/question-icon.png" onclick="javascript:clipBoardCopy('${data.allocation_id}',this);" alt=""  title='비고 : ${data.etc}&#10;' ></span>
	                            	</td>
	                            </c:if>
	                            
	                            <%-- <c:if test="${user.control_grade == '01' }">
		                            <td class="showToggle">
		                                <a href="#" class="table-driver-btn" title="아이콘 설명 1"><i class="fa fa-truck" aria-hidden="true"></i></a>
		                                <a href="#" class="table-btn" title="아이콘 설명 2"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
		                                <a style="cursor:pointer" title="취소" onclick="javascript:updateAllocationStatus('cancel','${data.allocation_id}');" class="table-x">
		                                    <img src="/img/x-icon.png" alt="">
		                                </a>
		                                <a style="cursor:pointer"  title="완료" onclick="javascript:updateAllocationStatus('complete','${data.allocation_id}');" class="table-check">
		                                    <img src="/img/check-icon.png" alt="">
		                                </a>
		                            </td>
	                            </c:if> --%>
	                        </tr>
							</c:forEach>
	                    
	                        
	
	
	                    </tbody>
	                </table>
	                <!-- <div style="color:#8B0000;">※차대번호는 기사님이 입력한 경우에는 붉은색으로 표시 되고 배차 직원이 수정 한 경우에는 파란색으로 표시 됩니다.</div> -->
	                <!-- <div style="">※차대번호는 기사님이 입력한 경우에는 <span style="color:#f00;">붉은색</span>으로 표시 되고 배차 직원이 수정 한 경우에는  <span style="color:#00f;">파란색</span>으로 표시 됩니다.</div> -->
	                <div class="table-pagination text-center">
	                    <ul class="pagination">
	                    	<html:paging uri="/allocation/combination.do" forGroup="&startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&searchDateType=${paramMap.searchDateType}&forOrder=${paramMap.forOrder}%5E${paramMap.order}&allocationStatus=${paramMap.allocationStatus}" frontYn="N" />
	                       <!--  <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
	                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
	                        <li class="curr-page"><a href="#">1</a></li>
	                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
	                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li> -->
	                    </ul>
	                </div>                
	                
	            </section>
            
            	<div class="confirmation">
                    <!-- <div class="cancel">
                        <a style="cursor:pointer;" onclick="javascript:pickUpValidation('N');">픽업해제</a>
                    </div> -->
                    <div class="confirm">
	                        <a style="cursor:pointer;" onclick="javascript:goAllocationRegisterPage();">취소</a>
	                    
	                       
                    </div>
                    <div class="cancel">
	                	<a style="cursor:pointer;" onclick="javascript:goInsertAllocationUpdate()">덮어쓰기 및 업데이트</a>
	                </div>
                </div>
            </div>
        </div>        
        <!-- <iframe style="width: 980px; height:10000px; border: none;" frameBorder="0" id="happyboxFrame" scrolling="no" src="https://www.happyalliance-happybox.org/Bridge?v=param"></iframe>​    -->
       <script>
		if("${userMap.control_grade}" == "01"){
			$('#dataTable').SetEditable({
            	columnsEd: "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15",
            	 onEdit: function(row) {updateAllocaion(row)},  
            	 onDelete: function() {},  
            	 onBeforeDelete: function() {}, 
            	 onAdd: function() {}  
            });
			
		}          
            </script>

