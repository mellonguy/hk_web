<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>

<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/js/bootstable.js"></script>
<script type="text/javascript"> 
var addInfo = "";
var firstPayment = "";
var secondPayment = "";
var thirdPayment = "";
$(document).ready(function(){
	
	addInfo = $("#forAddAllocation").html();
	paymentInfoSetting();
	
	
	$('#datepicker1').click(function(){
		$( "#datepicker1" ).val("");   	
	});

	$("#datepicker1").datepicker('setDate', "${paramMap.startDt}");
	
	//var maxDate = new Date();
    $( "#datepicker1" ).datepicker({
  	  dateFormat : "yy-mm-dd",
  	  /* maxDate : maxDate, */
  	  onClose: function(e) {    
  		var date = new Date($("#datepicker1").datepicker({ dateFormat: 'yy-mm-dd' }).val()),	week = new Array('일', '월', '화', '수', '목', '금', '토');
  		if (week[date.getDay()]!= undefined){
    		$("#datepicker1").val($("#datepicker1").val()+" "+"("+(week[date.getDay()])+")"); 
    	}
        } 
    });
    
    $(document).find("input[name=cAcqDate]").removeClass('hasDatepicker').attr("id","").datepicker({
    	dateFormat : "yy-mm-dd",
    	  /* maxDate : maxDate, */
    	  onClose: function( selectedDate ) {    

          } 
    });
	
    //$("#companyList").focus();

   	$(".timepicker").timepicker({
	    timeFormat: 'h:mm p',
	    interval: 30,
	    minTime: '10',
	    dropdown: true,
	    scrollbar: true
	});
	
       
    $("#customerList").css('display','none');
    $("#personInChargeList").css('display','none');
    
    
    
});

var driverArray = new Array();
var carInfoList = new Array();
var paymentInfoList = new Array();
var driverList = new Array();
var rgx1 = /\D/g;  // /[^0-9]/g 와 같은 표현
var rgx2 = /(\d+)(\d{3})/; 

function getNumberSec(value){
	$("#profitf").val(setComma(value));
}

function getNumberTh(value){
	$("#billForPaymentf").val(setComma(value));
}

function getNumberOnly (str) {			//저장할때는 콤마 제거 후 저장한다.
    var len = str.length;
    var sReturn = "";

    for (var i=0; i<len; i++){
        if ( (str.charAt(i) >= "0") && (str.charAt(i) <= "9") ){
            sReturn += str.charAt(i);
        }
    }
    return sReturn;
}

function comma(str) {
    str = String(str);
    return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
}

function getNumber(obj){
	
     var num01;
     var num02;
     num01 = obj.value;
     num02 = num01.replace(rgx1,"");
     num01 = setComma(num02);
     obj.value =  num01;
     
     //alert($(obj).parent().parent().attr("payment_division"));
     
     if($(obj).parent().parent().attr("payment_division") == "02"){
    	 if($(obj).parent().next().children().eq(0).val() != ""){
    		 var result = num02-(Number($(obj).parent().next().children().eq(0).val())*num02/100)
    		 $(obj).parent().next().next().children().eq(0).val(comma(result));
    	 }
     }
     
   
     var income = 0;
      $("input[name=income]").each(function(index,element){
    	  var value = 0; 
    		if($(this).val() != ""){
    			value = getNumberOnly($(this).val());
    		}
    		income += Number(value);
 		});
      $("input[name=amountPaidf]").each(function(index,element){
    	  var value = 0; 
	  		if($(this).val() != ""){
	  			value = getNumberOnly($(this).val());
	  		}
	  		income -= Number(value);
	 	});
      $("input[name=companyBillForPaymentf]").each(function(index,element){
    	  var value = 0; 
	  		if($(this).val() != ""){
	  			value = getNumberOnly($(this).val());
	  		}
	  		income -= Number(value);
 		});
     
      	$("#profitf").val(comma(income));
     
    /*  if($("#amountf").val() != "" && $("#companyBillForPaymentf").val() != "" && $("#billForPaymentf").val() != ""){
    	var pay1 = $("#amountf").val().replace(/[^0-9]/gi,"");
    	var pay2 = $("#companyBillForPaymentf").val().replace(/[^0-9]/gi,"");
    	var pay3 = $("#billForPaymentf").val().replace(/[^0-9]/gi,"");
    	$("#profitf").val(Number(pay1)-Number(pay2)-Number(pay3));
    	getNumberSec($("#profitf").val());
     }
     if($("#amountPaidf").val() != "" && $("#deductionRatef").val()){
    	var pay1 = $("#amountPaidf").val().replace(/[^0-9]/gi,"");
     	var pay2 = $("#deductionRatef").val().replace(/[^0-9]/gi,"");
     	$("#billForPaymentf").val(Number(pay1)-((Number(pay1)/100)*Number(pay2)));
     	getNumberTh($("#billForPaymentf").val());
     } */
     

     
}

function setComma(inNum){
     
     var outNum;
     outNum = inNum; 
     while (rgx2.test(outNum)) {
          outNum = outNum.replace(rgx2, '$1' + ',' + '$2');
      }
     return outNum;

}

function timeValidation(obj){
	
	var timeRegExp = /^([0-2]{1}[0-9]{1})([0-5]{1}[0-9]{1})$/;
	var str = String($(obj).val());
	
	 if(str.length == 2){
		 if(Number(str)<=24){
			 $(obj).val(str+":");	 
		 }else{
			alert("출발 시간을 확인 해 주세요."); 
		 }
	}
	
	if(str.length == 5){
		if(timeRegExp.test(str)){
			str = str.replace(/([0-2]{1}[0-9]{1})([0-5]{1}[0-9]{1})/g, '$1:$2');
			$(obj).val(str);
		}	
	}else{
		
	}
	
	
	
	
	
	
	/*  while(timeRegExp.test(str)){
		str = str.replace(/([0-2]{1}[0-9]{1})([0-5]{1}[0-9]{1})/g, '$1:$2')
		$(obj).val(str);
	} */ 
	
	
}



function paymentInfoSetting(){
	
	firstPayment += '<tr payment_division="01">'; 
    firstPayment += '<td class="notFirst widthAuto"  style="width:100px;">';
    firstPayment += '<form action="">';																
    firstPayment += '<input type="text" class="search-box" placeholder="매출처" name="findCustomerForPayment" id="">'; 		
    firstPayment += '<input type="hidden" name="paymentCustomerId">';
    firstPayment += '<i class="fa fa-spinner fa-spin fa-fw"></i>';
    firstPayment += '<div class="search-result" id="searchResult">';
    firstPayment += '</div>';
    firstPayment += '</form>';
    firstPayment += '</td>';
    firstPayment += '<td style="width: 145px;">';
	firstPayment += '<div class="select-con">';
	firstPayment += '<select class="dropdown">';
	firstPayment += '<option  value=""  >결제방법</option>';
	firstPayment += '<c:forEach var="data" items="${paymentDivisionList}" varStatus="status" >';
	firstPayment += '<option  value="${data.payment_division_cd}"  >${data.payment_division}</option>';
	firstPayment += '</c:forEach>';
	firstPayment += '</select>';
	firstPayment += '<span></span>';
	firstPayment += '</div>';
    firstPayment += '</td>';
    firstPayment += '<td style="width: 145px;">';
    firstPayment += '<div class="select-con">';
	firstPayment += '<select class="dropdown" onchange="javascript:billingDtToggle(this);">';
	firstPayment += '<option  value=""  >증빙구분</option>';
	firstPayment += '<c:forEach var="data" items="${billingDivisionList}" varStatus="status" >';
	firstPayment += '<option  value="${data.billing_division_id}"  >${data.billing_division}</option>';
	firstPayment += '</c:forEach>';
	firstPayment += '</select>';
	firstPayment += '<span></span>';
	firstPayment += '</div>';
    firstPayment += '</td>';
    firstPayment += '<td style="width: 145px;">';
	firstPayment += '<div class="select-con">';
	firstPayment += '<select class="dropdown">';
	/* firstPayment += '<option  value=""  >결제여부</option>'; */
	firstPayment += '<c:forEach var="data" items="${payDivisionList}" varStatus="status" >';
	firstPayment += '<option  value="${data.pay_division_cd}"  >${data.pay_division}</option>';
	firstPayment += '</c:forEach>';
	firstPayment += '</select>';
	firstPayment += '<span></span>';
	firstPayment += '</div>';
    firstPayment += '</td>';
    firstPayment += '<td style="width:130px;"><input type="text"  placeholder="입금일" name="cAcqDate"></td>';
    firstPayment += '<td style="width:130px;"  class="notFirst widthAuto"><input type="text"  style="display:inline-block;" placeholder="계산서발행일자" name="cAcqDate" ></td>';
    firstPayment += '<td style="width:130px;"><input type="text" onfocusout="javascript:setAmountForNotYet(this);" onkeyup="javascript:getNumber(this);" placeholder="업체청구액" name="amountf" id="amountf"></td>';
    firstPayment += '<td style="" colspan="4"><input type="text"  style="display:inline-block;" placeholder="비고" name="" id=""></td>';
    /* firstPayment += '<td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="추가" onclick="javascript:addPayment(0,this);"></td>'; */
    firstPayment += '<td  class="notFirst widthAuto"><input type="button"  class="btn-normal" value="삭제" onclick="javascript:delPayment(0,this);"></td>';
	firstPayment += '</tr>';
	
	secondPayment +='<tr payment_division="02">';
	secondPayment +='<td class="notFirst widthAuto">';
	secondPayment +='<div class="select-con">';
	secondPayment +='<select class="dropdown" name="driverInfo" onchange="javascript:setDriverDeduct(this.value,this);">';
	secondPayment +='<option  value=""  >기사선택</option>';
	secondPayment +='</select>';
	secondPayment +='<input type="hidden" name="paymentCustomerId">';
	secondPayment +='<span></span>';
	secondPayment +='</div>';
	secondPayment +='</td>';
	secondPayment +='<td style="width: 145px;">';
	secondPayment +='<div class="select-con">';
	secondPayment +='<select class="dropdown">';
	secondPayment +='<option  value=""  >결제방법</option>';
	secondPayment +='<c:forEach var="data" items="${paymentDivisionList}" varStatus="status" >';
	secondPayment +='<option  value="${data.payment_division_cd}"  >${data.payment_division}</option>';
	secondPayment +='</c:forEach>';
	secondPayment +='</select>';
	secondPayment +='<span></span>';
	secondPayment +='</div>';
	secondPayment +='</td>';
	secondPayment +='<td style="width: 145px;">';
	secondPayment +='<div class="select-con">';
	secondPayment +='<select class="dropdown" onchange="javascript:billingDtToggle(this);">';
	secondPayment +='<option  value=""  >증빙구분</option>';
	secondPayment +='<c:forEach var="data" items="${billingDivisionList}" varStatus="status" >';
	secondPayment +='<option  value="${data.billing_division_id}"  >${data.billing_division}</option>';
	secondPayment +='</c:forEach>';
	secondPayment +='</select>';
	secondPayment +='<span></span>';
	secondPayment +='</div>';
	secondPayment +='</td>';
	secondPayment +='<td style="width: 145px;">';
	secondPayment +='<div class="select-con">';
	secondPayment +='<select class="dropdown">';
	/* secondPayment +='<option  value=""  >결제여부</option>'; */
	secondPayment +='<c:forEach var="data" items="${payDivisionList}" varStatus="status" >';
	secondPayment +='<option  value="${data.pay_division_cd}"  >${data.pay_division}</option>';
	secondPayment +='</c:forEach>';
	secondPayment +='</select>';
	secondPayment +='<span></span>';
	secondPayment +='</div>';
	secondPayment +='</td>';
	secondPayment +='<td style="width:130px;"><input type="text" placeholder="지급일" name="cAcqDate" ></td>';
	secondPayment +='<td style="width:130px;"  class="notFirst widthAuto"><input type="text"  style="display:inline-block;" placeholder="계산서발행일자" name="cAcqDate" ></td>';
	secondPayment +='<td  class="notFirst widthAuto"><input type="text"  onkeyup="javascript:getNumber(this);" style="display:inline-block;" placeholder="기사지급액" name="amountPaidf" id=""></td>';
	/* secondPayment +='<td style="width:130px;" colspan="2"><input type="text"  style="display:inline-block;" readonly="readonly" placeholder="공제율" name="deductionRatef" id=""></td>'; */
	secondPayment +='<td style="width:130px;" colspan="2"><input type="text"  style="display:inline-block; width:85%;" readonly="readonly" placeholder="공제율" name="deductionRatef" id=""><span style="margin-left:5px;">%</span></td>';
	secondPayment +='<td style="width:130px;" colspan="2"><input type="text" placeholder="최종지급액" readonly="readonly" name="billForPaymentf" id=""></td>';
	/* secondPayment +='<td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="추가" onclick="javascript:addPayment(1,this);"></td>'; */
	secondPayment +='<td  class="notFirst widthAuto"><input type="button"  class="btn-normal" value="삭제" onclick="javascript:delPayment(1,this);"></td>';
	secondPayment +='</tr>';
	
	thirdPayment +='<tr payment_division="03">';
	thirdPayment +='<td class="notFirst widthAuto">';
	thirdPayment +='<form action="">';											
	thirdPayment +='<input type="text" class="search-box" placeholder="매입처" name="findCustomerForPayment" id="">';
	thirdPayment +='<input type="hidden" name="paymentCustomerId">';
	thirdPayment +='<i class="fa fa-spinner fa-spin fa-fw"></i>';
	thirdPayment +='<div class="search-result" id="">';
	thirdPayment +='</div>';
	thirdPayment +='</form>';
	thirdPayment +='</td>';
	thirdPayment +='<td style="width: 145px;">';
	thirdPayment +='<div class="select-con">';
	thirdPayment +='<select class="dropdown">';
	thirdPayment +='<option  value=""  >결제방법</option>';
	thirdPayment +='<c:forEach var="data" items="${paymentDivisionList}" varStatus="status" >';
	thirdPayment +='<option  value="${data.payment_division_cd}"  >${data.payment_division}</option>';
	thirdPayment +='</c:forEach>';
	thirdPayment +='</select>';
	thirdPayment +='<span></span>';
	thirdPayment +='</div>';
	thirdPayment +='</td>';
	thirdPayment +='<td style="width: 145px;">';
	thirdPayment +='<div class="select-con">';
	thirdPayment +='<select class="dropdown" onchange="javascript:billingDtToggle(this);">';
	thirdPayment +='<option  value=""  >증빙구분</option>';
	thirdPayment +='<c:forEach var="data" items="${billingDivisionList}" varStatus="status" >';
	thirdPayment +='<option  value="${data.billing_division_id}"  >${data.billing_division}</option>';
	thirdPayment +='</c:forEach>';
	thirdPayment +='</select>';
	thirdPayment +='<span></span>';
	thirdPayment +='</div>';
	thirdPayment +='</td>';
	thirdPayment +='<td style="width: 145px;">';
	thirdPayment +='<div class="select-con">';
	thirdPayment +='<select class="dropdown">';
	/* thirdPayment +='<option  value=""  >결제여부</option>'; */
	thirdPayment +='<c:forEach var="data" items="${payDivisionList}" varStatus="status" >';
	thirdPayment +='<option  value="${data.pay_division_cd}"  >${data.pay_division}</option>';
	thirdPayment +='</c:forEach>';
	thirdPayment +='</select>';
	thirdPayment +='<span></span>';
	thirdPayment +='</div>';
	thirdPayment +='</td> ';
	thirdPayment +='<td style="width:130px;"><input type="text" placeholder="지급일" name="cAcqDate" ></td>';
	thirdPayment +='<td style="width:130px;" class="notFirst widthAuto"><input type="text"  style="display:inline-block;" placeholder="계산서발행일자" name="cAcqDate" ></td>';
	thirdPayment +='<td style="width:130px;"><input type="text" onkeyup="javascript:getNumber(this);" placeholder="업체지급액" name="companyBillForPaymentf" id=""></td>';
	thirdPayment +='<td style="" colspan="4"><input type="text"  style="display:inline-block;" placeholder="비고" name="" id=""></td>';
	/* thirdPayment +='<td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="추가" onclick="javascript:addPayment(2,this);"></td>'; */
	thirdPayment +='<td  class="notFirst widthAuto"><input type="button"  class="btn-normal" value="삭제" onclick="javascript:delPayment(2,this);"></td>';
	thirdPayment +='</tr>';
	
	//alert($("#paymentInfo").children().eq(0).html());
	
	
	//alert($("#paymentInfo").children().eq(0).clone().wrapAll("<div/>").parent().html());
	//firstPayment = $("#paymentInfo").children().eq(0).clone().wrapAll("<div/>").parent().html()
	
	//firstPayment = "<tr payment_division='01'>"+$("#paymentInfo").children().eq(0).html()+"</tr>";
	//secondPayment = "<tr payment_division='02'>"+$("#paymentInfo").children().eq(1).html()+"</tr>";
	//thirdPayment = "<tr payment_division='03'>"+$("#paymentInfo").children().eq(2).html()+"</tr>";
	
}

function setCarInfo(){
	
	carInfoList.length = 0;
	var returnVal = true;
	
	//alert($("#allocationAddForm").find("table[data-name=allocationInfo]").length);
	
	$("#allocationAddForm").find("table[data-name=allocationInfo]").find("tbody").each(function(index,element){
		var carInfo= new Object();
		carInfo.index = index;
		$(this).find("tr").each(function(trnum,element){
			$(this).find("td").each(function(tdnum,element){
				if(trnum == 0 && tdnum==0){
					//alert($(this).html());					//타이틀
				}
				if(trnum == 0 && tdnum==1){
					if($(this).find("div").find("select").val() == ""){
						alert("배차구분은 필수 입력 사항 입니다.");
						returnVal = false;
					}else{
						carInfo.carrier_type = $(this).find("div").find("select").val();			//배차구분	
					}
				}
				if(trnum == 0 && tdnum==2){
					carInfo.distance_type = $(this).find("div").find("select").val();			//운행구분
				}
				if(trnum == 0 && tdnum==3){
					carInfo.departure_dt = $(this).find("input").val();			//출발일
				}
				if(trnum == 0 && tdnum==4){
					carInfo.departure_time = $(this).find("input").val();			//출발시간
				}
				if(trnum == 0 && tdnum==5){
					
					//getNumberOnly($(this).find("input").val())
					//carInfo.sales_total = $(this).find("input").val();			//db에 금액 저장시 ,를 제거한 후에 저장 한다.
					carInfo.sales_total = getNumberOnly($(this).find("input").val());			//매출액
				}
				if(trnum == 0 && tdnum==6){
					carInfo.driver_name = $(this).find("input").val();			//기사명
					carInfo.driver_id = $(this).find("input").next().val();			//기사아이디
					carInfo.driver_cnt = $(this).find("input").next().next().val();			//기사차수
				//	alert($(this).find("div").find("select").html());
				}
				if(trnum == 0 && tdnum==7){
					//사고유무
					if($(this).find("input").prop("checked")){
						carInfo.accident_yn = "Y";	
					}else{
						carInfo.accident_yn = "N";
					}
				}
				if(trnum == 0 && tdnum==8){
					carInfo.etc = $(this).find("textarea").val();			//비고	
				}
				if(trnum == 1 && tdnum==0){
					carInfo.car_kind = $(this).find("input").val();			//차종
				} 
				if(trnum == 1 && tdnum==1){
					carInfo.car_id_num = $(this).find("input").val();			//차대
				}
				if(trnum == 1 && tdnum==2){
					carInfo.car_num = $(this).find("input").val();			//차량번호
				}
				if(trnum == 1 && tdnum==3){
					carInfo.contract_num = $(this).find("input").val();			//계약번호
				}
				if(trnum == 1 && tdnum==4){
					carInfo.tow_distance = $(this).find("input").val();			//운행거리
				}
				/* if(trnum == 1 && tdnum==5){
					if($(this).children().eq(0).val() != "" && $(this).children().eq(1).val() != ""){
						carInfo.require_pic_cnt = $(this).children().eq(0).val();			//사진제한
						carInfo.require_dn_pic_cnt = $(this).children().eq(1).val();			//사진제한
					}else{
						alert("상차 또는 하차 사진 제한이 입력 되지 않았습니다.");
						returnVal = false;
					}
				}
				if(trnum == 1 && tdnum==6){
					//인수증전송
					if($(this).find("input").prop("checked")){
						carInfo.receipt_send_yn = "Y";	
					}else{
						carInfo.receipt_send_yn = "N";
					}
				} */
				if(trnum == 2 && tdnum==0){
					carInfo.departure = $(this).find("input").val();			//출발지약식주소
				}
				if(trnum == 2 && tdnum==1){
					carInfo.departure_addr = $(this).find("input").val();			//출발지상세주소
				}
				if(trnum == 2 && tdnum==3){
					carInfo.departure_person_in_charge = $(this).find("input").val();			//출발담당자명
				}
				if(trnum == 2 && tdnum==4){
					carInfo.departure_phone = $(this).find("input").val();			//출발연락처
				}
				if(trnum == 3 && tdnum==0){
					carInfo.arrival = $(this).find("input").val();			//도착지약식주소
				}
				if(trnum == 3 && tdnum==1){
					carInfo.arrival_addr = $(this).find("input").val();			//도착지상세주소
				}
				if(trnum == 3 && tdnum==2){
					carInfo.arrival_person_in_charge = $(this).find("input").val();			//도착담당자명
				}
				if(trnum == 3 && tdnum==3){
					carInfo.arrival_phone = $(this).find("input").val();			//도착연락처
				}
				
	      	});
      	});	
		$("#allocationAddForm").find("table[data-name=applicationInfo]").find("tbody").each(function(applicationIndex,element){
			if(applicationIndex == index){
				
				//alert("applicationIndex = "+applicationIndex);
				//alert($(this).find("tr").find("td").length);
				
				if($(this).find("tr").find("td").eq(1).children().eq(0).val() != "" && $(this).find("tr").find("td").eq(1).children().eq(1).val() != ""){
					carInfo.require_pic_cnt = $(this).find("tr").find("td").eq(1).children().eq(0).val();			//사진제한
					carInfo.require_dn_pic_cnt = $(this).find("tr").find("td").eq(1).children().eq(1).val();			//사진제한
				}else{
					alert("상차 또는 하차 사진 제한이 입력 되지 않았습니다.");
					returnVal = false;
				}
				carInfo.receipt_name = $(this).find("tr").find("td").eq(2).children().eq(0).val();
				carInfo.receipt_email = $(this).find("tr").find("td").eq(3).children().eq(0).val();
				carInfo.receipt_phone = $(this).find("tr").find("td").eq(4).children().eq(0).val();
				if($(this).find("tr").find("td").eq(5).find("input").prop("checked")){
					carInfo.receipt_send_yn = "Y";	
				}else{
					carInfo.receipt_send_yn = "N";
				}
			}
			
		});
		carInfoList.push(carInfo);
  	});
	
	$("#carInfoVal").val(JSON.stringify({carInfoList : carInfoList}))
	//alert($("#carInfoVal").val());
	return returnVal;
}

function setPaymentInfo(){
	
	paymentInfoList.length = 0;
	var rtnObj = new Object();
	rtnObj.rtnStatus = "00";
	rtnObj.rtnResult = true;
	$("#allocationAddForm").find("table[data-name=paymentInfo]").find("tbody").each(function(index,element){
		$(this).find("tr").each(function(trnum,element){
			var paymentInfo= new Object();
			paymentInfo.index = index;
			paymentInfo.payment_division = $(this).attr("payment_division");
			$(this).find("td").each(function(tdnum,element){
				if(trnum == 0){
					if(tdnum == 1){
						paymentInfo.payment_partner = $(this).find("form").find("input").val();
						paymentInfo.payment_partner_id = $(this).find("form").find("input").next().val();
					}
					if(tdnum == 2){
						paymentInfo.payment_kind = $(this).find("div").find("select").val();
						if($(this).find("div").find("select").val() == ""){
							rtnObj.rtnResult = false;
						}
					}
					if(tdnum == 3){
						paymentInfo.billing_division = $(this).find("div").find("select").val();
						if($(this).find("div").find("select").val() == ""){
							rtnObj.rtnResult = false;
						}
					}
					if(tdnum == 4){
						paymentInfo.payment = $(this).find("div").find("select").val();
					}
					if(tdnum == 5){
						paymentInfo.payment_dt =  $(this).find("input").val();
					}
					if(tdnum == 6){
						paymentInfo.billing_dt = $(this).find("input").val();
					}
					if(tdnum == 7){
						if($(this).find("input").val() == ""){
							rtnObj.rtnResult = false;
						}
						//paymentInfo.amount = $(this).find("input").val();
						paymentInfo.amount = getNumberOnly($(this).find("input").val());	
					}
					if(tdnum == 8){
						paymentInfo.etc = $(this).find("input").val();
						paymentInfo.deduction_rate = "";
						paymentInfo.bill_for_payment = "";
					}	
				}else{
					if(Number(paymentInfo.payment_division) == 02){
						if(tdnum == 0){
							paymentInfo.payment_partner_id = $(this).find("div").find("select").val();
							paymentInfo.payment_partner = $(this).find("div").find("select option:selected").text();
						//	alert(paymentInfo.payment_partner);
						}
						if(tdnum == 1){
							paymentInfo.payment_kind = $(this).find("div").find("select").val();
						}
						if(tdnum == 2){
							paymentInfo.billing_division = $(this).find("div").find("select").val();
						}
						if(tdnum == 3){
							paymentInfo.payment = $(this).find("div").find("select").val();
						}
						if(tdnum == 4){
							paymentInfo.payment_dt =  $(this).find("input").val();
						}
						if(tdnum == 5){
							paymentInfo.billing_dt = $(this).find("input").val();
						}
						if(tdnum == 6){
							if($(this).find("input").val() == ""){
								rtnObj.rtnResult = false;
							}
							//paymentInfo.amount = $(this).find("input").val();
							paymentInfo.amount = getNumberOnly($(this).find("input").val());
						}
						
						if(paymentInfo.payment_division != "02"){
							if(tdnum == 7){
								paymentInfo.etc = $(this).find("input").val();
							}	
							paymentInfo.deduction_rate = "";
							paymentInfo.bill_for_payment = "";
						}else{
							paymentInfo.etc = "";
							if(tdnum == 7){
								paymentInfo.deduction_rate = $(this).find("input").val();
							}
							if(tdnum == 8){
								/* paymentInfo.bill_for_payment = $(this).find("input").val(); */
								paymentInfo.bill_for_payment = getNumberOnly($(this).find("input").val());
							}
						}
					}else if(Number(paymentInfo.payment_division) != 02){
						if(tdnum == 0){
							paymentInfo.payment_partner = $(this).find("form").find("input").val();
							paymentInfo.payment_partner_id = $(this).find("form").find("input").next().val();
						}
						if(tdnum == 1){
							paymentInfo.payment_kind = $(this).find("div").find("select").val();
						}
						if(tdnum == 2){
							paymentInfo.billing_division = $(this).find("div").find("select").val();
						}
						if(tdnum == 3){
							paymentInfo.payment = $(this).find("div").find("select").val();
						}
						if(tdnum == 4){
							paymentInfo.payment_dt =  $(this).find("input").val();
						}
						if(tdnum == 5){
							paymentInfo.billing_dt = $(this).find("input").val();
						}
						if(tdnum == 6){
							if(Number(paymentInfo.payment_division) == 01 || Number(paymentInfo.payment_division) == 02){
								if($(this).find("input").val() == ""){
									rtnObj.rtnResult = false;
								}	
							}
							//paymentInfo.amount = $(this).find("input").val();
							paymentInfo.amount = getNumberOnly($(this).find("input").val());
						}
						
						if(paymentInfo.payment_division != "02"){
							if(tdnum == 7){
								paymentInfo.etc = $(this).find("input").val();
							}	
							paymentInfo.deduction_rate = "";
							paymentInfo.bill_for_payment = "";
						}else{
							paymentInfo.etc = "";
							if(tdnum == 7){
								paymentInfo.deduction_rate = $(this).find("input").val();
							}
							if(tdnum == 8){
								//paymentInfo.bill_for_payment = $(this).find("input").val();
								paymentInfo.bill_for_payment = getNumberOnly($(this).find("input").val());
							}
						}
					}
				}
				
	      	});	
			paymentInfoList.push(paymentInfo);	
		});
			
  	});
	
	$("#paymentInfoVal").val(JSON.stringify({paymentInfoList : paymentInfoList}));
	
	
	//alert($("#paymentInfoVal").val());
	return rtnObj;
	
	
}


function insertAllocation(){

	
	//getDistance();
	
	//첫줄시작
	if($("#companyId").val() == ""){			//사업자 정보
		alert("사업자가 선택 되지 않았습니다.");
		return false;
	}

	$("#batchStatus").val($("input:radio[name=batchStatusf]:checked").val());
	$("#inputDt").val($("#datepicker1").val());		//의뢰일
	$("#profit").val(getNumberOnly($("#profitf").val()));					//순이익
	//첫줄끝
	//2	한줄입력삭제
	// $("#comment").val($("#commentf").val());
	//2
	
	
	
	//2
	if($("#customerNamef").val() == ""){
		alert("고객정보(업체명) 는 필수 입력 사항 입니다.");
		return false;
	}else{
		$("#customerName").val($("#customerNamef").val());
		$("#customerId").val($("#customerIdf").val());
		$("#chargeName").val($("#chargeNamef").val());
		$("#chargeId").val($("#chargeIdf").val());
		$("#chargePhone").val($("#chargePhonef").val());
		$("#chargeAddr").val($("#chargeAddrf").val());
		$("#customerSignificantData").val($("#customerSignificantDataf").val());
		 $("#carCnt").val($("#carCntf").val());
	}
	 //2
	 
	 
	 //3 운행정보 셋팅
	 if(!setCarInfo()){
		// alert(setCarInfo());
		 return false;
	 }
	 //3

	  //4	결제정보 셋팅
	 var rtnObj = setPaymentInfo();
	 var regMsg = "";
	 if(!rtnObj.rtnResult){
		 //alert("결제 정보가 입력 되지 않았습니다. ");
		 /* regMsg = "결제 정보(업체청구액,기사지급액)가 작성되지 않은경우 미배차 상태에서 진행 할 수 없습니다."; */
		 regMsg = "결제방법,증빙구분,결제 정보(업체청구액,기사지급액)가 작성되지 않았습니다.";
		 $("#paymentInputStatus").val("N"); //작성 안됨
		 alert(regMsg);
		 return false;
	 }else{
		 $("#paymentInputStatus").val("Y");//작성 됨
	 }
	 //4
	 
	 //5 메모 셋팅
	 $("#memo").val($("#memof").val());
	//5
	
	var batchStatus = "";
	
	if($("#batchStatus").val() == "Y"){
		batchStatus = "일괄";
	}else if($("#batchStatus").val() == "N"){
		batchStatus = "개별";
	}else if($("#batchStatus").val() == "P"){
		batchStatus = "픽업";
	}
	
 	if(confirm(batchStatus+"등록 하시겠습니까?"+"\r\n"+regMsg)){
		$("#insertForm").attr("action","/allocation/insert-allocation.do");
		$("#insertForm").submit();	
	} 
	
}

function updateAllocation(){
	
	//첫줄시작
	 $("#inputDt").val($("#datepicker1").val());
	 $("#profit").val($("#profitf").val());
	//첫줄끝
	//2
	 $("#comment").val($("#commentf").val());
	//2
	//3
	 $("#customerName").val($("#customerNamef").val());
	 $("#chargeName").val($("#chargeNamef").val());
	 $("#chargePhone").val($("#chargePhonef").val());
	 $("#chargeAddr").val($("#chargeAddrf").val());
	 $("#customerSignificantData").val($("#customerSignificantDataf").val());
	 $("#carCnt").val($("#carCntf").val());
	 //3
	 
	 //4
	 //setCarInfo();
	 if(!setCarInfo()){
			// alert(setCarInfo());
			 return false;
	}
	 
	 //5
	 setPaymentInfo();
	 
	 $("#memo").val($("#memof").val());
	 
	if($("#customerName").val() == ""){
		alert("고객명이 입력되지 않았습니다.");
		return false;
	}
	/* if($("#customerPhoneNum").val() == ""){
		alert("고객 연락처가 입력되지 않았습니다.");
		return false;
	} */

	if($("#carrierType").val() == ""){
		alert("배차구분이 입력되지 않았습니다.");
		return false;
	}

	var currentLocation = $(location).attr('href');
	var locationStr = "";
	if(currentLocation.indexOf("combination.do") != -1){
		locationStr = "combination.do";
	}else if(currentLocation.indexOf("self.do") != -1){
		locationStr = "self.do";
	}else if(currentLocation.indexOf("carrier.do") != -1){
		locationStr = "carrier.do";
	}
	
	if(confirm("수정 하시겠습니까?")){
		$("#insertForm").attr("action","/allocation/update-allocationAllData.do?returnPage="+locationStr);
		$("#insertForm").submit();	
	}
	
}



function jusoSearch(where,obj){
	
	new daum.Postcode({
	    oncomplete: function(data) {
	        // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

            // 각 주소의 노출 규칙에 따라 주소를 조합한다.
            // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
            var fullAddr = ''; // 최종 주소 변수
            var extraAddr = ''; // 조합형 주소 변수

            // 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
            if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                fullAddr = data.roadAddress;

            } else { // 사용자가 지번 주소를 선택했을 경우(J)
                fullAddr = data.jibunAddress;
            }

            // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
            if(data.userSelectedType === 'R'){
                //법정동명이 있을 경우 추가한다.
                if(data.bname !== ''){
                    extraAddr += data.bname;
                }
                // 건물명이 있을 경우 추가한다.
                if(data.buildingName !== ''){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
                fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
            }

            // 우편번호와 주소 정보를 해당 필드에 넣는다.
            //document.getElementById('sample6_postcode').value = data.zonecode; //5자리 새우편번호 사용
         	
            $(obj).parent().children().eq(0).val(fullAddr);
            $(obj).parent().parent().children().eq(0).children().find("input").eq(0).val(data.sido+" "+data.sigungu);
            // 커서를 상세주소 필드로 이동한다.
            //document.getElementById('sample6_address2').focus();
            
            if(where == "departure"){		//출발지 정보를 입력 하고 있고
            	if($(obj).parent().children().eq(0).val() != "" && $(obj).parent().parent().next().children().eq(1).children().eq(0).val() != ""){		//출발지와 도착지 정보 모두 입력 되어 있으면
            		getDistance($(obj).parent().parent().next().children().eq(1).children().eq(0).val(),$(obj).parent().children().eq(0).val(),$(obj).parent().parent().next().children().eq(1).children().eq(1));
            	}
            }
            
            if(where == "arrival"){		//도착지 정보를 입력 하고 있고
            	if($(obj).parent().children().eq(0).val() != "" && $(obj).parent().parent().prev().children().eq(1).children().eq(0).val() != ""){		//출발지와 도착지 정보 모두 입력 되어 있으면
            		getDistance($(obj).parent().parent().prev().children().eq(1).children().eq(0).val(),$(obj).parent().children().eq(0).val(),obj);
            	}
            }
            
            
            
            
            
        }
	    
	}).open();	
	
}

function getDistance(departureAddr,arrivalAddr,obj){
	
	$.ajax({ 
		type: 'post' ,
		url : "/allocation/getDistance.do" ,
		dataType : 'json' ,
		async : false,
		data : {
			departureAddr :departureAddr, 
			arrivalAddr : arrivalAddr
		},
		success : function(data, textStatus, jqXHR)
		{
			var result = data.resultCode;
			var resultData = data.resultData;
			if(result == "0000"){
				$(obj).parent().parent().prev().children().eq(1).children().eq(1).val(resultData.departure);
				$(obj).parent().children().eq(1).val(resultData.arrival);
				$(obj).parent().parent().prev().prev().children().eq(4).children().eq(0).val(resultData.distance+"km");
			}else if(result == "0001"){
				alert("조회 하는데 실패 하였습니다.");
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
}

function routeView(obj){
	
	
	//alert($(obj).parent().parent().children().eq(1).children().eq(1).val());
	//alert($(obj).parent().parent().next().children().eq(1).children().eq(1).val());
	
	//getDistance($(obj).parent().parent().next().children().eq(1).children().eq(0).val(),$(obj).parent().children().eq(0).val(),$(obj).parent().parent().next().children().eq(1).children().eq(1));
	
	if($(obj).parent().parent().children().eq(1).children().eq(0).val() != "" && $(obj).parent().parent().next().children().eq(1).children().eq(0).val() != ""){
		getDistance($(obj).parent().parent().children().eq(1).children().eq(0).val(),$(obj).parent().parent().next().children().eq(1).children().eq(0).val(),$(obj).parent().parent().next().children().eq(1).children().eq(2));
	}
	
	
	if($(obj).parent().parent().children().eq(1).children().eq(1).val() == ""){
		alert("주소 정보가 입력 되지 않았습니다.");
		return false;
	}else if($(obj).parent().parent().next().children().eq(1).children().eq(1).val() == ""){
		alert("주소 정보가 입력 되지 않았습니다.");
		return false;
	}else{
		window.open("/allocation/routeView.do?&departure="+$(obj).parent().parent().children().eq(1).children().eq(1).val()+"&arrival="+$(obj).parent().parent().next().children().eq(1).children().eq(1).val(),"_blank","top=0,left=0,width=1600,height=800,toolbar=0,status=0,scrollbars=1,resizable=0");
		//window.open("/allocation/routeView.do?&departure="+$(obj).parent().parent().children().eq(1).children().eq(0).val()+"&arrival="+$(obj).parent().parent().next().children().eq(1).children().eq(0).val(),"_blank","top=0,left=0,width=1000,height=800,toolbar=0,status=0,scrollbars=1,resizable=0");	
	}
	
	
	//alert($(obj).parent().parent().children().eq(1).children().eq(1).val());
	//alert($(obj).parent().parent().next().children().eq(1).children().eq(1).val());
	
	
}




function selectCompany(companyId){
	
	$.ajax({ 
			type: 'post' ,
			url : "/baseinfo/getCompanyInfo.do" ,
			dataType : 'json' ,
			data : {
				companyId : companyId
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					//$("#paymentAccountNumberf").val(resultData.account_number);
					$("#companyId").val(companyId);
					$("#companyName").val(resultData.company_name);
				}else if(result == "0001"){
					alert("변경 하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
	
}

var selectDriverObj;

function showModal(obj){
	
	$(obj).val("");
	
	selectDriverObj =obj;
	$("#driverList").css('display','');
	$("#customerList").css('display','none');
	$("#personInChargeList").css('display','none');	
	$('.modal-field').show();
}

function setDriverCustomer(customerId,customername,obj){
	
	//alert($(obj).parent().children().last().children().eq(0).val());
	
		$(selectDriverObj).val(customername);
		
		$(selectDriverObj).next().val(customerId);
		$(selectDriverObj).next().next().val($(obj).parent().children().last().children().eq(0).val());
		//$(selectDriverObj).next().next().next().val("Y");
		
		selectPersonInCharge(customerId,selectDriverObj);
	
}

function selectPersonInCharge(customerId,obj){
	
	$.ajax({ 
		type: 'post' ,
		url : "/baseinfo/getDriverList.do" ,
		dataType : 'json' ,
		data : {
			customerId : customerId
		},
		success : function(data, textStatus, jqXHR)
		{
			
			$("#driverList").css('display','none');
			$("#customerList").css('display','none');
			$("#personInChargeList").css('display','');
			var list = data.resultData;
			var result = "";
			$("#personInChargeLocation").html("");
			if(list.length > 0){
       			for(var i=0; i<list.length; i++){
       				result += '<tr class="ui-state-default" personInChargeId="" >'; 
       				result += '<td class="showToggle"><input type="checkbox" name="forBatch" driverId="\''+list[i].driver_id+'\'"></td>';
       				/* result += '<td style="cursor:pointer;" onclick="javascript:setExternalDriver(\''+list[i].driver_id+'\',\''+list[i].driver_name+'\',\'\',this);">'+list[i].driver_name+'</td>';
       				result += '<td style="cursor:pointer;" onclick="javascript:setExternalDriver(\''+list[i].driver_id+'\',\''+list[i].driver_name+'\',\'\',this);">'+list[i].phone_num+'</td>';
       				result += '<td style="cursor:pointer;" onclick="javascript:setExternalDriver(\''+list[i].driver_id+'\',\''+list[i].driver_name+'\',\'\',this);">'+list[i].car_num+'</td>'; */
       				result += '<td style="cursor:pointer;" onclick="javascript:setDriver(\''+list[i].driver_id+'\',\''+list[i].driver_name+'\',\'\',this);">'+list[i].driver_name+'</td>';
       				result += '<td style="cursor:pointer;" onclick="javascript:setDriver(\''+list[i].driver_id+'\',\''+list[i].driver_name+'\',\'\',this);">'+list[i].phone_num+'</td>';
       				result += '<td style="cursor:pointer;" onclick="javascript:setDriver(\''+list[i].driver_id+'\',\''+list[i].driver_name+'\',\'\',this);">'+list[i].car_num+'</td>';
       				result += '<td><select class="dropdown" style="width:100%;" >';
       				result += '<option  value="" selected>회차 선택</option>';
       				result += '<c:forEach var="val" begin="1" end="10" step="1" varStatus="status">';
       				result += '<option value="${val}">${val}회차</option>';
       				result += '</c:forEach>';
       				result += '</select></td>';
       				result += '</tr>';
       			}
			}else if(list.length == 1){
       				//getPersonInChargeInfo(list[0].person_in_charge_id,list[0].name,list[0].phone_num,list[0].address);
			}else{
				//getPersonInChargeInfo(id,name,phone_num,address);
			}
			$("#personInChargeLocation").html(result);
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
}


function setPersonInCharge(driverId,drivername,carKind,obj){
	
		$(selectDriverObj).val(drivername);
		$(selectDriverObj).next().val(driverId);
		$(selectDriverObj).next().next().val("1");
		//selectDriver(driverId,selectDriverObj);
		$('.modal-field').hide();
}




function driverSelect(){
	
	var total = $('input:checkbox[name="forBatch"]:checked').length;
	
	if(total == 0){
		alert("기사가 선택 되지 않았습니다.\r\n 기사명을 클릭해도 기사 선택이 가능 합니다.");
		return false;
	}else if(total > 1){
		alert("기사 선택은 1명만 가능 합니다.");
		return false;
	}else{
		$('input:checkbox[name="forBatch"]:checked').each(function(index,element){
			$(this).parent().next().trigger("click");
      	});
	}
	
}


function setDriver(driverId,drivername,carKind,obj){
	
	//alert($(obj).parent().children().last().children().eq(0).val());
	if($(obj).parent().children().last().children().eq(0).val() != ""){
		if(carKind != ""){
			$(selectDriverObj).val(drivername+" / "+carKind);	
		}else{
			$(selectDriverObj).val(drivername);
		}
		$(selectDriverObj).next().val(driverId);
		$(selectDriverObj).next().next().val($(obj).parent().children().last().children().eq(0).val());
		$(selectDriverObj).next().next().next().val("N");		
		selectDriver(driverId,selectDriverObj);
		$('.modal-field').hide();
	}else{
		alert(" 선택한 기사의 회차가 지정되지 않았습니다.");
		return false;
	}
}


function setExternalDriver(driverId,drivername,carKind,obj){
	
	//alert($(obj).parent().children().last().children().eq(0).val());
	if($(obj).parent().children().last().children().eq(0).val() != ""){
		if(carKind != ""){
			$(selectDriverObj).val(drivername+" / "+carKind);	
		}else{
			$(selectDriverObj).val(drivername);
		}
		$(selectDriverObj).next().val(driverId);
		$(selectDriverObj).next().next().val($(obj).parent().children().last().children().eq(0).val());
		$(selectDriverObj).next().next().next().val("N");		
		selectExternalDriver(driverId,selectDriverObj);
		$('.modal-field').hide();
	}else{
		alert(" 선택한 기사의 회차가 지정되지 않았습니다.");
		return false;
	}
}

function selectExternalDriver(driverId,obj){
	
	$.ajax({ 
			type: 'post' ,
			url : "/baseinfo/getDriverInfo.do" ,
			dataType : 'json' ,
			data : {
				driverId : driverId
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					var driverInfo= new Object();
					driverInfo.driver_id = resultData.driver_id;
					driverInfo.driver_name = resultData.driver_name;
					driverInfo.deduction_rate = resultData.deduction_rate;
					driverInfo.car_kind = resultData.car_kind;
					driverInfo.car_num = resultData.car_num;
					driverList.push(driverInfo);
					//var add = '<option  value="'+driverInfo.driver_id+'">'+driverInfo.driver_name+'</option>';
					
					$(obj).parent().parent().parent().parent().next().next().children().find("tr").each(function(index,element){
						if($(this).attr("payment_division") == "03"){
							$(this).children().eq(0).find("input").eq(0).val(driverInfo.driver_name);
							$(this).children().eq(0).find("input").eq(1).val(driverInfo.driver_id);
						}
					});
					
					
					/* $("input[name=findCustomerForPayment]").each(function(index,element){
						if($(this).parent().parent().parent().attr("payment_division") == "03"){
							$(this).val(name);
							$(this).next().val(id);
						}
					}); */
					
					
					/* $('select[name="driverInfo"]').each(function(index,element){
						$(this).append(add);
						var compareObj = $(this);
						$(obj).parent().parent().parent().parent().next().next().children().eq(0).children().each(function(secIndex,secEle){
							if($(this).children().eq(0).children().eq(0).children().eq(0).get(0) == compareObj.get(0)){
								compareObj.val(driverInfo.driver_id).prop("selected", true);
								setDriverDeduct(driverInfo.driver_id,compareObj);
							} 
						});
			      	}); */
					
				}else if(result == "0001"){
					alert("변경 하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
	
}






function selectDriver(driverId,obj){
	
	$.ajax({ 
			type: 'post' ,
			url : "/baseinfo/getDriverInfo.do" ,
			dataType : 'json' ,
			data : {
				driverId : driverId
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					var driverInfo= new Object();
					driverInfo.driver_id = resultData.driver_id;
					driverInfo.driver_name = resultData.driver_name;
					driverInfo.deduction_rate = resultData.deduction_rate;
					driverInfo.car_kind = resultData.car_kind;
					driverInfo.car_num = resultData.car_num;
					driverList.push(driverInfo);
					var add = '<option  value="'+driverInfo.driver_id+'">'+driverInfo.driver_name+'</option>';
					$('select[name="driverInfo"]').each(function(index,element){
						$(this).append(add);
						var compareObj = $(this);
						$(obj).parent().parent().parent().parent().next().next().children().eq(0).children().each(function(secIndex,secEle){
							if($(this).children().eq(0).children().eq(0).children().eq(0).get(0) == compareObj.get(0)){
								compareObj.val(driverInfo.driver_id).prop("selected", true);
								setDriverDeduct(driverInfo.driver_id,compareObj);
							} 
						});
						/* if($("#allocationInfo").children().first().get(0) == $(obj).parent().parent().parent().get(0)){
							if(index == 0){
								$(this).val(driverInfo.driver_id).prop("selected", true);	
								setDriverDeduct(driverInfo.driver_id,this);
							}
						} */
			      	});
					
				}else if(result == "0001"){
					alert("변경 하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
	
}


function getPersonInChargeInfo(id,name,phone_num,address){
	
	//$('#searchInput').trigger("click");
	
	$("#searchResult").removeClass('active');
	$("#chargeNamef").val(name);
	$("#chargeIdf").val(id);
	$("#chargePhonef").val(phone_num);
	$("#chargeAddrf").val(address);
	
}


var customer = new Object();

function getCustomerInfo(id,name,phone_num,significant,forPayment,address){
	
	customer.id = id;
	customer.name = name;
	 $("input[name=findCustomerForPayment]").each(function(index,element){
		if($(this).parent().parent().parent().attr("payment_division") == "01"){
			$(this).val(name);
			$(this).next().val(id);
		}
	});
	
	
	//alert(forPayment);
	if(forPayment == "findCustomer"){
		$("#customerIdf").val(id);
		$("#customerNamef").val(name);
		$("#customerSignificantDataf").val(significant);		
/* 		 $(document).find("input[name=findCustomerForPayment]").each(function(index,element){
			$(this).val(name);
		});
		$(document).find("input[name=paymentCustomerId]").each(function(index,element){
			$(this).val(id);
		}); */
		$.ajax({ 
			type: 'post' ,
			url : "/personInCharge/getPersonInChargeList.do" ,
			dataType : 'json' ,
			data : {
				customerId : id
			},
			success : function(data, textStatus, jqXHR)
			{
				
				$("#paymentInfo").find("tr").each(function(index,element){
					$(this).find("td").each(function(tdnum,element){
						if(index == 0){
							if(tdnum == 1){
								$(this).find("form").find("input").val(name);
								$(this).find("form").find("input").next().val(id);
							//	$(this).find("form").find("input").parent().find("div").removeClass('active');
							}
						}
					});
			      });
    
				var list = data.resultData;
				var result = "";
				$("#searchResult").html("");
				//$(obj).parent().find("div").html("");
			
				if(list.length > 1){
	       			for(var i=0; i<list.length; i++){
	       				result += '<div class="Wresult">';
	       				result += '<p class="result-title">고객정보</p>';
	       				result += '<a style="cursor:pointer;"   onclick="javascript:getPersonInChargeInfo(\''+list[i].person_in_charge_id+'\',\''+list[i].name+'\',\''+list[i].phone_num+'\',\''+list[i].address+'\');" class="result-sub"><span>'+list[i].name+'</span></a>';
	       				result += '<p class="camp-type"><span>담당자 주소:</span> <span>'+list[i].address+'</span></p>';
	       				result += '<p class="camp-id"><span>Tel:</span> <span>'+list[i].phone_num+'</span></p>';
	       				result += '</div>';
	       			}
				}else if(list.length == 1){
	       				getPersonInChargeInfo(list[0].person_in_charge_id,list[0].name,list[0].phone_num,list[0].address);
				}else{
					/* result += '<div class="no-result d-table">';
					result += '<div class="d-tbc">';
					result += '<i class="fa fa-exclamation-triangle fa-3x" aria-hidden="true"></i>';
					result += '<span>No results have been found.</span>';
					result += '</div></div>'; */
					
					//180823  기존에 담당자가 없을시 한국카캐리어가 담당자가 되는것을 거래처 정보로 변경
					//getPersonInChargeInfo("${companyList[0].company_id}","${companyList[0].company_name}","${companyList[0].phone_num}","${companyList[0].address}");
					getPersonInChargeInfo(id,name,phone_num,address);
				}
				
				$("#searchResult").html(result);
				//$(obj).parent().find("div").html(result);
			
				
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
	}else{
		$(test).val(name);
		$(test).next().val(id);
		$(test).parent().find("div").removeClass('active');
	}
	
	
	

}

var test;
function getAjaxData(val,obj,forPayment){
	
	test = obj;
	
	
	//findLocation
	
	//alert(forPayment);
	
	if(forPayment != "departuref" && forPayment != "arrivalf" ){
		$.ajax({ 
			type: 'post' ,
			url : "/baseinfo/getCustomerList.do" ,
			dataType : 'json' ,
			data : {
				customerName : val
			},
			success : function(data, textStatus, jqXHR)
			{
				
				var list = data.resultData;
				var result = "";
				//$("#searchResult").html("");
				$(obj).parent().find("div").html("");
				if(list.length > 0){
	       			for(var i=0; i<list.length; i++){
	       				result += '<div class="Wresult">';
	       				/* result += '<a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>'; */
	       				result += '<p class="result-title">고객정보</p>';
	       				var strJsonText = JSON.stringify(obj);
	    	   		    strJsonText = strJsonText.replace(/\"/gi, "'");
	       				result += '<a style="cursor:pointer;"   onclick="javascript:getCustomerInfo(\''+list[i].customer_id+'\',\''+list[i].customer_name+'\',\''+list[i].phone+'\',\''+list[i].significant_data+'\',\''+forPayment+'\',\''+list[i].address+'\');" class="result-sub"><span>'+list[i].customer_name+'</span></a>';
	       				
	       				if(list[i].customer_kind == "00"){
	       					result += '<p class="camp-type"><span>회사 구분:</span> <span>법인</span></p>';	
	       				}else if(list[i].customer_kind == "01"){
	       					result += '<p class="camp-type"><span>회사 구분:</span> <span>개인</span></p>';	
	       				}else if(list[i].customer_kind == "02"){
	       					result += '<p class="camp-type"><span>회사 구분:</span> <span>외국인</span></p>';	
	       				}else if(list[i].customer_kind == "03"){
	       					result += '<p class="camp-type"><span>회사 구분:</span> <span>개인(주민번호)</span></p>';	
	       				}
	       				
	       				result += '<p class="camp-id"><span>Tel:</span> <span>'+list[i].phone+'</span></p>';
	       				result += '</div>';
	       			}
				}else{
					result += '<div class="no-result d-table">';
					result += '<div class="d-tbc">';
					result += '<i class="fa fa-exclamation-triangle fa-3x" aria-hidden="true"></i>';
					result += '<span>No results have been found.</span>';
					result += '</div></div>';
				}
				
				//$("#searchResult").html(result);
				$(obj).parent().find("div").html(result);
				
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
	}else{
		
		$.ajax({ 
			type: 'post' ,
			url : "/allocation/getAddressList.do" ,
			dataType : 'json' ,
			data : {
				keyword : val
			},
			success : function(data, textStatus, jqXHR)
			{
				
				var list = data.resultData;
				var result = "";
				//$("#searchResult").html("");
				$(obj).parent().find("div").html("");
				if(list.length > 0){
	       			for(var i=0; i<list.length; i++){
	       				result += '<div class="Wresult">';
	       				/* result += '<a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>'; */
	       				result += '<p class="result-title">주소정보</p>';
	       				var strJsonText = JSON.stringify(obj);
	    	   		    strJsonText = strJsonText.replace(/\"/gi, "'");
	       				result += '<a style="cursor:pointer;"   onclick="javascript:setAddressInfo(\''+list[i].keyword+'\',\''+list[i].address+'\',\''+list[i].name+'\',\''+list[i].phone_num+'\',\''+forPayment+'\');" class="result-sub"><span>'+list[i].keyword+'</span></a>';
	       				result += '<p class="camp-type"><span>주소:</span> <span>'+list[i].address+'</span></p>';	
	       				result += '<p class="camp-id"><span>Tel:</span> <span>'+list[i].phone_num+'</span></p>';
	       				result += '</div>';
	       			}
				}else{
					$(test).parent().find("div").removeClass('active');
				}
				
				//$("#searchResult").html(result);
				$(obj).parent().find("div").html(result);
				
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
		
		
	}
	
		
}


function setAddressInfo(keyword,address,name,phone_num){
	
	
	//$("#allocationInfo").children().first().get(0) == $(obj).parent().parent().parent().get(0)
	 
	//alert($(test).parent().parent().parent().children().length);
	
	if($(test).parent().parent().parent().children().length == 6){
		$(test).val(keyword);
		$(test).parent().parent().next().children().eq(0).val(address);
		$(test).parent().parent().next().next().next().children().eq(0).val(name);
		$(test).parent().parent().next().next().next().next().children().eq(0).val(phone_num);
	}else if($(test).parent().parent().parent().children().length == 5){
		
		//alert($(test).parent().parent().parent().children().eq($(test).parent().parent().parent().children().length-1).find("input").length);
		
		if($(test).parent().parent().parent().children().eq($(test).parent().parent().parent().children().length-1).find("input").length == 1){
			$(test).val(keyword);
			$(test).parent().parent().next().children().eq(0).val(address);
			$(test).parent().parent().next().next().next().children().eq(0).val(name);
			$(test).parent().parent().next().next().next().next().children().eq(0).val(phone_num);	
		}else{
			$(test).val(keyword);
			$(test).parent().parent().next().children().eq(0).val(address);
			$(test).parent().parent().next().next().children().eq(0).val(name);
			$(test).parent().parent().next().next().next().children().eq(0).val(phone_num);
		}
		
	}else if($(test).parent().parent().parent().children().length == 4){
		$(test).val(keyword);
		$(test).parent().parent().next().children().eq(0).val(address);
		$(test).parent().parent().next().next().children().eq(0).val(name);
		$(test).parent().parent().next().next().next().children().eq(0).val(phone_num);
	}
	
	if($(test).parent().parent().parent().get(0) == $(test).parent().parent().parent().parent().children().eq(3).get(0)){
		//alert("도착지");
		$(test).parent().parent().parent().parent().parent().next().find("tr").children().eq(2).children().eq(0).val(name);
		$(test).parent().parent().parent().parent().parent().next().find("tr").children().eq(4).children().eq(0).val(phone_num);
	}else{
		//alert("출발지");
	}
	
	$(test).parent().find("div").removeClass('active');
	
}


var initBody; 
function beforePrint() 
{ 
    initBody = document.body.innerHTML; 
    document.body.innerHTML = $("#printlayout").html(); 
} 
function afterPrint() 
{ 
    document.body.innerHTML = initBody; 
} 
function pageprint() 
{ 
    window.onbeforeprint = beforePrint; 
    window.onafterprint = afterPrint;    
 /*    
	//웹 브라우저 컨트롤 생성
    var webBrowser = '<OBJECT ID="previewWeb" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
    //웹 페이지에 객체 삽입
    document.body.insertAdjacentHTML('beforeEnd', webBrowser);
    //ExexWB 메쏘드 실행 (7 : 미리보기 , 8 : 페이지 설정 , 6 : 인쇄하기(대화상자))
    previewWeb.ExecWB(7, 1);
    //객체 해제
    previewWeb.outerHTML = ""; 
*/
    window.print(); 
} 

function carInfoInput(){
	
	var carCnt = $("#carCntf").val();
	
	if(carCnt == "" || Number(carCnt) == 0){
		alert("대수를 입력해 주세요.");
		return false;
	}/* else if(Number(carCnt) == 1){
		alert("1대인경우 운행정보의 입력란에 입력 하세요");
		return false;
	} */else if(Number(carCnt) > 10){
		alert("최대 10대까지 입력 할 수 있습니다.");
		return false;
	}else{
		$("#carInfo").html("");
		var result = "";
		var cnt = 0;
		if(carInfoList.length != 0){
			if(Number(carCnt) >= carInfoList.length){
				for(var i = 0; i < carInfoList.length; i++){
					result += '<tr class="ui-state-default">';
					result += '<td style="width:60px;">'+(cnt+1)+'</td>';
					result += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="차종" name="" id="" value="'+carInfoList[i].carKind+'"></td>';
					result += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="차대번호" name="" id="" value="'+carInfoList[i].carIdNum+'"></td>';
					result += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="차량번호" name="" id="" value="'+carInfoList[i].carNum+'"></td>';
					result += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="계약번호" name="" id="" value="'+carInfoList[i].contractNum+'"></td>';
					result += '</tr>';
					cnt++;
				}
				for(var i = 0; i < Number(carCnt)-carInfoList.length; i++){
					result += '<tr class="ui-state-default">';
					result += '<td style="width:60px;">'+(cnt+1)+'</td>';
					result += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="차종" name="" id="" value=""></td>';
					result += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="차대번호" name="" id="" value=""></td>';
					result += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="차량번호" name="" id="" value=""></td>';
					result += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="계약번호" name="" id="" value=""></td>';
					result += '</tr>';
					cnt++;
				}
			}else{
				for(var i = 0; i < Number(carCnt); i++){
					result += '<tr class="ui-state-default">';
					result += '<td style="width:60px;">'+(cnt+1)+'</td>';
					result += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="차종" name="" id="" value="'+carInfoList[i].carKind+'"></td>';
					result += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="차대번호" name="" id="" value="'+carInfoList[i].carIdNum+'"></td>';
					result += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="차량번호" name="" id="" value="'+carInfoList[i].carNum+'"></td>';
					result += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="계약번호" name="" id="" value="'+carInfoList[i].contractNum+'"></td>';
					result += '</tr>';
					cnt++;
				}
			}
			
		}else{
			for(var i = 0; i < Number(carCnt); i++){
				result += '<tr class="ui-state-default">';
				result += '<td style="width:60px;">'+(i+1)+'</td>';
				result += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="차종" name="" id=""></td>';
				result += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="차대번호" name="" id=""></td>';
				result += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="차량번호" name="" id=""></td>';
				result += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="계약번호" name="" id=""></td>';
				result += '</tr>';
			}	
		}
		
		$("#carInfo").html(result);
		$('.car-modal-field').show();
	}

	
}

function carInfoConfirm(){
	
	//확인을 누르면 셋트를 먼처 추가 하여 자동으로 입력 될 수 있도록 한다.
	for(var i = 0; i < Number($("#carInfo").find("tr").length)-1; i++){		//세트가 하나 있으므로 총 대수에서 한대 뺌.
		addInfo = $("#forAddAllocation").html();
		$("#allocationAddForm").append(addInfo);
	}
	
	//alert($("#allocationAddForm").find("input[name=carKindf]").length);
	
    carInfoList.length = 0;
	$("#carInfo").find("tr").each(function(index,element){
		var carInfo= new Object();
		$(this).find("td").each(function(num,element){
			if(num==1){
				carInfo.carKind = $(this).children().val();
			}
			if(num==2){
				carInfo.carIdNum = $(this).children().val();
			}
			if(num==3){
				carInfo.carNum = $(this).children().val();
			}
			if(num==4){
				carInfo.contractNum = $(this).children().val();
			}
      	});	
		carInfoList.push(carInfo);
  	});
    
	$("#allocationAddForm").find("input[name=carKindf]").each(function(index,element){
			$(this).val(carInfoList[index].carKind);
     });
	$("#allocationAddForm").find("input[name=carIdNumf]").each(function(index,element){
		$(this).val(carInfoList[index].carIdNum);
 });
	$("#allocationAddForm").find("input[name=carNumf]").each(function(index,element){
		$(this).val(carInfoList[index].carNum);
 });
	$("#allocationAddForm").find("input[name=contractNumf]").each(function(index,element){
		$(this).val(carInfoList[index].contractNum);
 });
	
	$('.car-modal-field').hide();
	
	
	$('.search-box').on("click", function(){
        $(this).val("");
        $(this).parent().find("div").removeClass('active');
    });
    
    $('.search-box').keyup(function (e) {
        var SearchBoxVal = $(this).val();
        var obj = this;
        var forPayment = $(obj).attr('name');
        if (SearchBoxVal.length >= 2) {
        	$(this).parent().find("i").stop().css("display", "block");
            setTimeout( function() {
            	getAjaxData(SearchBoxVal,obj,forPayment);
            //	alert(this);
            	$(obj).parent().find("i").css('display','none');
            	$(obj).parent().find("div").addClass('active');
            	
           //     $(".dispatch-bottom-content table tbody tr td form i.fa-spinner").css('display','none');
            //    $(".dispatch-bottom-content table tbody tr td form .search-result").addClass('active');
            }, 1000);
        }
        else {
        	
        	$(this).parent().find(".search-result").removeClass('active');
            //$(".dispatch-bottom-content table tbody tr td form .search-result").removeClass('active');
        };
    }); 
  

	/* var maxDate = new Date(); */
	
 	  $(document).find("input[name=cAcqDate]").removeClass('hasDatepicker').attr("id","").datepicker({
	    	dateFormat : "yy-mm-dd",
	    	  /* maxDate : maxDate, */
	    	  onClose: function( selectedDate ) {    

	          } 
	    }); 
	
		
	
		/* carInfoList.length = 0;
		$("#carInfo").find("tr").each(function(index,element){
			var carInfo= new Object();
			$(this).find("td").each(function(num,element){
				if(num==1){
					carInfo.carKind = $(this).children().val();
				}
				if(num==2){
					carInfo.carIdNum = $(this).children().val();
				}
				if(num==3){
					carInfo.carNum = $(this).children().val();
				}
				if(num==4){
					carInfo.contractNum = $(this).children().val();
				}
	      	});	
			carInfoList.push(carInfo);
      	});
		//alert(JSON.stringify({carInfoList : carInfoList}));
		$("#carInfoVal").val(JSON.stringify({carInfoList : carInfoList}))
		 */	
		 
	  $("input[name=findCustomerForPayment]").each(function(index,element){
			if($(this).parent().parent().parent().attr("payment_division") == "01"){
				$(this).val(customer.name);
				$(this).next().val(customer.id);
			}
		}); 
		 
	   	$(".timepicker").timepicker({
		    timeFormat: 'h:mm p',
		    interval: 30,
		    minTime: '10',
		    dropdown: true,
		    scrollbar: true
		});
		
	
}


function jusoCopy(status,obj){
	
	 
	 	if(status == "departure"){
	 
	 		var location = $("#allocationAddForm").find("input[name=departuref]:eq(0)").val();		
	 		var address = $("#allocationAddForm").find("input[name=departureAddrf]:eq(0)").val();
	 		var personInCharge = $("#allocationAddForm").find("input[name=departurePersonInChargef]:eq(0)").val();		
	 		var phone = $("#allocationAddForm").find("input[name=departurePhonef]:eq(0)").val();
	 		$(obj).prev().children().eq(0).val(location);
	 		$(obj).parent().next().children().eq(0).val(address);
	 		$(obj).parent().next().next().next().children().eq(0).val(personInCharge);
	 		$(obj).parent().next().next().next().next().children().eq(0).val(phone);
	 		
	 	}else if(status == "arrival"){
	 		
	 		var location = $("#allocationAddForm").find("input[name=arrivalf]:eq(0)").val();		
	 	 	var address = $("#allocationAddForm").find("input[name=arrivalAddrf]:eq(0)").val();
	 	 	var personInCharge = $("#allocationAddForm").find("input[name=arrivalPersonInChargef]:eq(0)").val();		
	 		var phone = $("#allocationAddForm").find("input[name=arrivalPhonef]:eq(0)").val();
	 		$(obj).prev().children().eq(0).val(location);
	 		$(obj).parent().next().children().eq(0).val(address);
	 		$(obj).parent().next().next().children().eq(0).val(personInCharge);
	 		$(obj).parent().next().next().next().children().eq(0).val(phone);
	 	}
	 	
}


function carInfoCancel(){
	$('.car-modal-field').hide();	
}

function deleteAllocationInfo(obj){
	
	
	if(confirm("삭제 하시겠습니까?")){
		/* if($("#allocationInfo").children().first().get(0) == $(obj).parent().parent().parent().get(0)){
		alert("운행정보를 삭제 할 수 없습니다.");
		return false;
	}else{ */
		$("#carCntf").val(Number($("#carCntf").val())-1);
		$(obj).parent().parent().parent().parent().next().remove();
		$(obj).parent().parent().parent().parent().remove();
		alert("삭제 되었습니다.");
	/* } */	
	}
	
}


function addAllocationInfo(){
	
	$("#carCntf").val(Number($("#carCntf").val())+1);
	addInfo = $("#forAddAllocation").html();
	$("#allocationAddForm").append(addInfo);
	
	$("input[name=findCustomerForPayment]").each(function(index,element){
		if($(this).parent().parent().parent().attr("payment_division") == "01"){
			$(this).val(customer.name);
			$(this).next().val(customer.id);
		}
	});
	
	/* var maxDate = new Date(); */
	  $(document).find("input[name=cAcqDate]").removeClass('hasDatepicker').attr("id","").datepicker({
	    	dateFormat : "yy-mm-dd",
	    	  /* maxDate : maxDate, */
	    	  onClose: function( selectedDate ) {    

	          } 
	    }); 
	
}

function addPayment(index,obj){

	var rowspan = $(obj).parent().parent().parent().children().eq(0).children().eq(0).attr("rowspan");
	
	if(Number(index) == 0){
		$(obj).parent().parent().after(firstPayment);
	}else if(Number(index) == 1){
		$(obj).parent().parent().after(secondPayment);
		//var newElement = $(obj).parent().parent().next();
	}else if(Number(index) == 2){
		$(obj).parent().parent().after(thirdPayment);
	}
	
	$(obj).parent().parent().parent().children().eq(0).children().eq(0).attr("rowspan",rowspan+1);

	  $('.search-box').on("click", function(){
	        $(this).val("");
	        $(this).parent().find("div").removeClass('active');
	    });
	    
	    $('.search-box').keyup(function (e) {
	        var SearchBoxVal = $(this).val();
	        var obj = this;
	        var forPayment = $(obj).attr('name');
	        if (SearchBoxVal.length >= 2) {
	        	$(this).parent().find("i").stop().css("display", "block");
	            setTimeout( function() {
	            	getAjaxData(SearchBoxVal,obj,forPayment);
	            //	alert(this);
	            	$(obj).parent().find("i").css('display','none');
	            	$(obj).parent().find("div").addClass('active');
	            	
	           //     $(".dispatch-bottom-content table tbody tr td form i.fa-spinner").css('display','none');
	            //    $(".dispatch-bottom-content table tbody tr td form .search-result").addClass('active');
	            }, 1000);
	        }
	        else {
	        	
	        	$(this).parent().find(".search-result").removeClass('active');
	            //$(".dispatch-bottom-content table tbody tr td form .search-result").removeClass('active');
	        };
	    }); 
	  
	     
		/* var maxDate = new Date(); */
		  $(document).find("input[name=cAcqDate]").removeClass('hasDatepicker').attr("id","").datepicker({
		    	dateFormat : "yy-mm-dd",
		    	  /* maxDate : maxDate, */
		    	  onClose: function( selectedDate ) {    

		          } 
		    }); 
	    
}

function delPayment(index,obj){
	
		/* if($("#paymentInfo > tr[payment_division='"+$(obj).parent().parent().attr('payment_division')+"']").first().get(0) == $(obj).parent().parent().get(0)){
			alert("삭제 할 수 없습니다.");
			return false;
		}else{ */
			$(obj).parent().parent().remove();
		/* } */
}

function setDriverDeduct(driverId,obj){
	
	for(var i = 0; i < driverList.length; i++){
		if(driverId == driverList[i].driver_id){
			if($(obj).parent().parent().parent().children().eq(6).find("input").val() != ""){
				var amount = getNumberOnly($(obj).parent().parent().parent().children().eq(6).find("input").val());				
				$(obj).parent().parent().parent().children().eq(8).find("input").val(comma((Number(amount))-(Number(driverList[i].deduction_rate)*Number(amount)/100)));
			}
			$(obj).parent().parent().parent().children().eq(7).find("input").val(driverList[i].deduction_rate);
		}	
	}
	
}
var order = "${order}";
function sortby(gubun){

	if(order == "" || order == "desc"){
		order = "asc";
	}else{
		order = "desc";
	}
	
	var loc = document.location.href;
	var str = "";
	if(loc.indexOf("?") > -1){
		//forOrder 가 있는경우 ㅎㅎ
		if(loc.indexOf("forOrder") > -1){
			var queryString = loc.split("?");
			var query = queryString[1].split("&");
			
			for(var i = 0; i < query.length; i++){
				if(query[i].indexOf("forOrder") > -1){
					query[i] = "forOrder="+gubun+"^"+order;
				}
			}
			for(var i = 0; i < query.length; i++){
				if(query[i] != ""){
					str += "&"+query[i];	
				}
			}
			document.location.href = queryString[0]+"?"+str;
		}else{
			str="&forOrder="+gubun+"^"+order;
			document.location.href = loc+str;
		}
		
	}else{
		str="?&forOrder="+gubun+"^"+order;
		document.location.href = loc+str;
	}
	
}


function fileUpload(fis){
	
	if(confirm("배차 일괄 등록 하시겠습니까?")){
		 $("#excelForm").attr("action","/allocation/allocation-excel-insert.do");
		/* $("#excelForm").attr("action","/allocation/allocation-excel-insert-finish.do"); */
		$("#excelForm").submit();	
	}
	
}

function view(gubun){
	
	if(gubun == "driver"){
		$("#driverList").css("display","");
		$("#customerList").css("display","none");
		$("#personInChargeList").css("display","none");
	}else{
		$("#driverList").css("display","none");
		$("#customerList").css("display","");
		$("#personInChargeList").css("display","none");
	}
	
}


function setAmount(obj){
	
	$(obj).parent().parent().parent().parent().next().next().children().children().each(function(index,element){
			if($(this).attr("payment_division") == "01"){
				
				if(index == 0){
					$(this).children().eq(7).find("input").val($(obj).val());
				}else{
					$(this).children().eq(6).find("input").val($(obj).val());
				}
			}
      	});
	
	
}


function billingDtToggle(obj){
	
	if($(obj).val()=="00"){
		//$(obj).parent().parent().next().next().next().children().find("input").attr("disabled",true);
		$(obj).parent().parent().next().next().next().find("input").attr("disabled",true);
		//$(obj).parent().parent().next().next().next().children().find("input").val("ddddddddddddddd");
	}else{
		$(obj).parent().parent().next().next().next().find("input").attr("disabled",false);
	}
	
}


function setAmountForNotYet(obj){

	
	var total = Number(getNumberOnly($(obj).parent().parent().parent().parent().prev().prev().children().eq(0).children().eq(0).children().eq(5).find("input").val()));
	
//	if(total > 0 && total != getNumberOnly($(obj).val())){
		

		
		var customerId = $(obj).parent().parent().parent().children().eq(0).children().eq(1).find("input").eq(1).val();
		var customerName = $(obj).parent().parent().parent().children().eq(0).children().eq(1).find("input").eq(0).val();
		
		var income = 0;
		$(obj).parent().parent().parent().each(function(index,element){
			$(this).find("input[name=amountf]").each(function(index,element){
				var value = 0; 
				if($(this).val() != ""){
					value = getNumberOnly($(this).val());
				}
				income += Number(value);
			});
		});
		
		
		if(total > income){
			$(obj).parent().prev().prev().prev().find("select").val("P");
			//$(obj).parent().next().next().find("input").trigger("click");
			//$(obj).parent().parent().parent().children().eq(0).children().eq(9).find("input").trigger("click");
			var forAddObj = 	$(obj).parent().parent().parent().children().eq(0).children().eq(9).find("input");
			var rowspan = $(forAddObj).parent().parent().parent().children().eq(0).children().eq(0).attr("rowspan");
			$(obj).parent().parent().after(firstPayment);
			$(forAddObj).parent().parent().parent().children().eq(0).children().eq(0).attr("rowspan",rowspan+1);
			$('.search-box').on("click", function(){
		        $(this).val("");
		        $(this).parent().find("div").removeClass('active');
		    });
		    
		    $('.search-box').keyup(function (e) {
		        var SearchBoxVal = $(this).val();
		        var obj = this;
		        var forPayment = $(obj).attr('name');
		        if (SearchBoxVal.length >= 2) {
		        	$(this).parent().find("i").stop().css("display", "block");
		            setTimeout( function() {
		            	getAjaxData(SearchBoxVal,obj,forPayment);
		            //	alert(this);
		            	$(obj).parent().find("i").css('display','none');
		            	$(obj).parent().find("div").addClass('active');
		            	
		           //     $(".dispatch-bottom-content table tbody tr td form i.fa-spinner").css('display','none');
		            //    $(".dispatch-bottom-content table tbody tr td form .search-result").addClass('active');
		            }, 1000);
		        }
		        else {
		        	
		        	$(this).parent().find(".search-result").removeClass('active');
		            //$(".dispatch-bottom-content table tbody tr td form .search-result").removeClass('active');
		        };
		    }); 
			/* var maxDate = new Date(); */
			  $(document).find("input[name=cAcqDate]").removeClass('hasDatepicker').attr("id","").datepicker({
			    	dateFormat : "yy-mm-dd",
			    	  /* maxDate : maxDate, */
			    	  onClose: function( selectedDate ) {    

			          } 
			    });
			
		}else if(total == income){
			$(obj).parent().prev().prev().prev().find("select").val("Y");
		}
		
		var count = 0;
		$(obj).parent().parent().parent().children().each(function(index,element){
			if($(this).attr("payment_division") == "01"){
				 count++;
			}	
		}); 
		
		if(count > 1){
			$(obj).parent().parent().parent().children().each(function(index,element){
				if(index == count-1){
					 if($(this).attr("payment_division") == "01"){
						$(this).children().eq(0).find("input").eq(0).val(customerName);
						$(this).children().eq(0).find("input").eq(1).val(customerId);
						$(this).children().eq(3).find("select").val("N");
						$(this).children().eq(6).find("input").val(comma(total-income));
					}	
				}
			});
		}
		
	//}
	
	

}


 </script>


<div class="modal-field">
            <div class="modal-box">
                <h3 class="text-center" style="text-align:center; ">리스트</h3>
                <div style="text-align:right; margin-right:30px;"><a onclick="javascript:view('driver');" style="cursor:pointer;">기사선택</a>&nbsp;/&nbsp;<a onclick="javascript:view('customer');" style="cursor:pointer;">거래처선택</a></div>
            <div class="modal-table-container" style="margin-top:15px;">
                <table class="article-table" id="driverList">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <td class="showToggle"><input type="checkbox" id="checkAll" onclick="javascript:checkAll();" ></td>
                            <td>기사명</td>
                            <td>연락처</td>
                            <td>차량번호</td>
                            <td>차종</td>
                            <td>회차</td>
                        </tr>
                    </thead>
                    <tbody id="driverSelectList">
                    	<c:forEach var="data" items="${driverList}" varStatus="status" >
                    		<tr class="ui-state-default" driverId="${data.driver_id}" > 
	                            <td class="showToggle"><input type="checkbox" name="forBatch" driverId="${data.driver_id}"></td>
	                            <td style="cursor:pointer;" onclick="javascript:setDriver('${data.driver_id}','${data.driver_name}','${data.car_kind}',this);">${data.driver_name}</td>
	                            <td style="cursor:pointer;" onclick="javascript:setDriver('${data.driver_id}','${data.driver_name}','${data.car_kind}',this);">${data.phone_num}</td>
	                            <td style="cursor:pointer;" onclick="javascript:setDriver('${data.driver_id}','${data.driver_name}','${data.car_kind}',this);">${data.car_num}</td>
	                            <td style="cursor:pointer;" onclick="javascript:setDriver('${data.driver_id}','${data.driver_name}','${data.car_kind}',this);">${data.car_kind}</td>
	                            <td><select class="dropdown" style="width:100%;" >
							        	<option  value="" selected>회차 선택</option>
							        	<c:forEach var="val" begin="1" end="10" step="1" varStatus="status">
										    	<option value="${val}" <c:if test='${data.driver_cnt == val}'>selected</c:if>>${val}회차</option>
										</c:forEach>
							        </select>
							     </td>
                        	</tr>
						</c:forEach>
                    </tbody>
                </table>
                <table class="article-table" id="customerList">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <!-- <td>소유주</td> -->
                            <td>거래처명</td>
                            <td>대표자명</td>
                            <td>연락처</td>
                        </tr>
                    </thead>
                    <tbody id="">
                    	<c:forEach var="data" items="${customerList}" varStatus="status" >
                    		<tr class="ui-state-default" customerId="${data.customer_id}" > 
	                            <%-- <td>${data.driver_owner}</td> --%>
	                            <td style="cursor:pointer;" onclick="javascript:setDriverCustomer('${data.customer_id}','${data.customer_name}',this);">${data.customer_name}</td>
	                            <td style="cursor:pointer;" onclick="javascript:setDriverCustomer('${data.customer_id}','${data.customer_name}',this);">${data.customer_owner_name}</td>
	                            <td style="cursor:pointer;" onclick="javascript:setDriverCustomer('${data.customer_id}','${data.customer_name}',this);">${data.phone}</td>
                        	</tr>
						</c:forEach>
                    </tbody>
                </table>
                
                <table class="article-table" id="personInChargeList">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                        	<td class="showToggle"><input type="checkbox" id="checkAll" onclick="javascript:checkAll();" ></td>
                            <td>기사명</td>
                            <td>연락처</td>
                            <td>차량번호</td>
                            <td>회차</td>
                        </tr>
                    </thead>
                    <tbody id="personInChargeLocation">
                    </tbody>
                </table>
                
                
                
                </div>
                <div class="confirmation">
                    <div class="confirm">
                        <input type="button" onclick="javascript:driverSelect();" value="확인" >
                    </div>
                    <div class="cancel">
                        <input type="button" value="취소" onclick="">
                    </div>
                </div>
            </div>
        </div>
 		
        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">HOME</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">신규 배차 입력</a></li>
                </ul>
            </div>
            <div class="up-dl clearfix">
                <div class="upload-btn" style="float:left;">
                	<form id="excelForm" style="display:none" name="excelForm" method="post" enctype="multipart/form-data">
                    	<input type="file" style="display:inline-block; width:300px;" id="upload" name="bbsFile" value="일괄입력" class="btn-primary" onchange="javascript:fileUpload(this);">
                    </form>
	            	<input type="button" onclick="javascript:$('#upload').trigger('click'); return false;" value="엑셀 업로드">
	            </div>
            </div>

            <div class="dispatch-btn-container">
				<div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">신규배차입력</div>					
            </div>
        </section>

        <div class="dispatch-wrapper">
            <div class="car-modal-field">
            <div class="car-modal-box" style="width:800px;">
                <h3 class="text-center">차량정보 입력</h3>
            <div class="car-modal-table-container">
                <table class="article-table">
                    <colgroup>
                    	<col>
						<col>
						<col>
						<col>
						<col>
                    </colgroup>
                    <thead>
                        <tr>
                        	<td style="width:60px;">번호</td>
                            <td>차종</td>
                            <td>차대번호</td>
                            <td>차량번호</td>
                            <td>계약번호</td>
                        </tr>
                    </thead>
                    <tbody id="carInfo">
                    </tbody>
                </table>
                </div>
                <div class="confirmation">
                    <div class="confirm">
                        <input type="button" value="확인" name="" onclick="javascript:carInfoConfirm();">
                    </div>
                    <div class="confirm">
                        <input type="button" value="취소" name="" onclick="javascript:carInfoCancel();">
                    </div>
                </div>
            </div>
        </div>

             <input type="button" id="printButton" value="인쇄" onclick="javascript:pageprint()">
			<section class="dispatch-bottom-content" id="printlayout" class="active">
				<form id="insertForm" action="/allocation/insert-allocation.do">
					<input type="hidden" name="allocationId" id="allocationId" value="">
					<input type="hidden" name="companyId" id="companyId" value="">
					<input type="hidden" name="companyName" id="companyName" value="">
					<input type="hidden"  name="inputDt" id="inputDt">
					<input type="hidden" name="registerId" id="registerId" value="${userMap.emp_id}">
					<input type="hidden" name="registerName" id="registerName" value="${userMap.emp_name }">
					<input type="hidden" name="profit" id="profit" value=""><!--순이익  -->
					<input type="hidden"  name="customerName" id="customerName">
					<input type="hidden"  name="customerId" id="customerId">
					<input type="hidden"  name="chargeName" id="chargeName">
					<input type="hidden"  name="chargeId" id="chargeId">
					<input type="hidden"  name="chargePhone" id="chargePhone">
					<input type="hidden"  name="chargeAddr" id="chargeAddr">
					<input type="hidden"  name="customerSignificantData" id="customerSignificantData">
					<input type="hidden" name="carCnt" id="carCnt" value="">
					<input type="hidden" name="allocationStatus" id="allocationStatus" value="N">
					<input type="hidden" name="batchStatus" id="batchStatus">
					<input type="hidden" name="paymentInputStatus" id="paymentInputStatus"><!-- 배차정보 입력,수정시 결제정보(업체청구액,기사지급액)가 입력 되어 있지 않은경우 기사가 지정 되어도 미배차 상태에서 진행 할 수 없도록 한다.  -->
					
					
					<!-- 한줄입력 삭제 -->
					<!-- <input type="hidden"  name="comment" id="comment"> -->
					<input type="hidden" name="memo" id="memo" value="">
					<input type="hidden" name="carInfoVal" id="carInfoVal" value="">
					<input type="hidden" name="paymentInfoVal" id="paymentInfoVal" value="">
				</form>
                <table>
                    <tbody>
                        <tr>
                            <td style="width:100px;" >사업자정보</td>
                            <td style="width: 145px;">
                            	<div class="select-con">
							        <select class="dropdown" style="width:100%;" onchange="javascript:selectCompany(this.value);">
							        	<option  value=""  >사업자 선택</option>
							        	<c:forEach var="data" items="${companyList}" varStatus="status" >
							        		<option  value="${data.company_id}"  >${data.company_name}</option>
										</c:forEach>
							        </select>
							        <span></span>
							    </div>
                            </td>
                            <td class="form-title">
                                	등록 구분 
                            </td>
                            <td style="width:150px">
                                <input type="radio" id="batchStatusN" name="batchStatusf" value="N" checked>
							    <label for="batchStatusN">개별</label>
							    <input type="radio" id="batchStatusY" name="batchStatusf" value="Y">
							    <label for="batchStatusY">일괄</label>
							    <input type="radio" id="batchStatusP" name="batchStatusf" value="P">
							    <label for="batchStatusP">픽업</label>
                            </td>
                            <td class="form-title">
                                	의뢰일 
                            </td>
                            <td style="width:150px">
                                <input  type="text" id="datepicker1" class="date-range" value="${paramMap.today}">
                            </td>
                            <td class="form-title">등록자</td>
                            <td style="width:150px">
                            	<input type="hidden" name="registerIdf" id="registerIdf" value="${userMap.emp_id}">
                                <input type="text" placeholder="등록자" readonly="readonly" name="registerf" id="registerf" value="${userMap.emp_name }">
                            </td>
                            <td class="form-title">순이익</td>
                            <td style="width:150px">
                                <input type="text" placeholder="순이익" readonly="readonly" name="profitf" id="profitf">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                        <tr>
                            <td style="width:100px;" >고객정보</td>
                            <td style="width: 190px;">
                                <form action="">																
                                	<input type="text" class="search-box" name="findCustomer" id="" style="margin-top:5px;"> 		
                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
                                    <div class="search-result" id="searchResult">
                                    </div>
                                </form>
                            </td>
                            <td style="width: 190px;">
                            	<input type="text" placeholder="업체명" name="customerNamef" id="customerNamef">
                            	<input type="hidden" name="customerIdf" id="customerIdf">
                            </td>
                            <td style="width: 190px;">
                            	<input type="text" placeholder="담당자 이름" name="chargeNamef" id="chargeNamef">
                            	<input type="hidden" name="chargeIdf" id="chargeIdf">
                            </td>
                            <td style="width: 190px;"><input type="text" placeholder="담당자 연락처" name="chargePhonef" id="chargePhonef"></td>
                            <td style="width: 190px;"><input type="text" placeholder="담당자 주소" name="chargeAddrf" id="chargeAddrf"></td>
                            <td style="width: 190px;"><input type="text" placeholder="특이사항" name="customerSignificantDataf" id="customerSignificantDataf"></td>
                            <td class="notFirst widthAuto" style="width: 300px;">
                            	<input type="text" style="display:inline-block; width:63%;"  onkeyup="javascript:getNumber(this);" placeholder="차량대수(1~10)" name="carCntf" id="carCntf"  onkeypress="if(event.keyCode=='13') carInfoInput();" value="1">
                            	<input type="button" style="width:35%;" class="btn-primary" value="차량정보 입력" onclick="javascript:carInfoInput();">
                            </td>
                        </tr>
                </table>
                
                <div id="allocationAddForm" style="margin-bottom:5px;">
                <table data-name="allocationInfo" style="margin-bottom:0px;">
                		<tbody>
	                        <tr>
	                            <td style="width:100px;" rowspan="4" class="vaTop">운행정보</td>
	                            <td style="width: 120px;">
	                            <div class="select-con">
							        <select class="dropdown" style="width:80%;">
							        	<option  value=""  >배차구분</option>
							        	<c:forEach var="data" items="${allocationDivisionList}" varStatus="status" >
							        		<option  value="${data.allocation_division_cd}"  >${data.allocation_division}</option>
										</c:forEach>
							        </select>
							        <span></span>
							    </div>
	                            
	                            </td>
	                            <td style="width: 120px;">
	                            	<div class="select-con">
								        <select class="dropdown" style="width:100%;" >
								        	<option  value=""  >운행구분</option>
								        	<c:forEach var="data" items="${runDivisionList}" varStatus="status" >
								        		<option  value="${data.run_division_cd}"  >${data.run_division}</option>
											</c:forEach>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                            <td style="width:150px;"><input type="text"  placeholder="출발일(ex:2019-01-01)" name="departureDtf" id="" class="datepick"></td>
	                            <!-- <td style="width:150px;"><input type="text" placeholder="출발시간(ex:10:00)" name="departureTimef" readonly class="timepicker" id=""></td> -->
	                            <!-- <td style="width:150px;"><input type="text" placeholder="출발시간(ex:10:00)" name="departureTimef" class="" id="" onkeyup="javascript:if(event.keyCode=='13') alert(event.keyCode); timeValidation(this);"></td> -->
	                            
	                            <td style="width:150px;"><input type="text" placeholder="출발시간(ex:10:00)" name="departureTimef" class="" id="" onkeyup="javascript:if(event.keyCode!='8') timeValidation(this);"></td>
	                            <td>
	                            	<input type="text" placeholder="매출액" name="income" id="" onkeyup="javascript:getNumber(this);"  onfocusout="javascript:setAmount(this);">
	                            </td>
	                            <td>
	                            	<input type="text" placeholder="기사선택" name="" id="" onclick="javascript:showModal(this);">
	                            	<input type="hidden" value="">
	                            	<input type="hidden" value="">
	                            </td>
	                            <td style="width:150px;"><input type="checkbox" style="margin-left:15px;" ><div style="margin-left:10px;  display:inline-block;">사고유무</div></td>
	                            <td style="padding: 10px 10px; width:300px;" colspan =4 rowspan=2>
	                            		<textarea class="remarks" style="height:100px;" name="etcf" id="" placeholder="비고"></textarea>
	                            </td>
	                            <!-- <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="추가" onclick="javascript:addAllocationInfo();"></td> -->
	                        </tr>
							<tr>
	                            <td  class="notFirst widthAuto" style="width:150px;"><input type="text" placeholder="차종" name="carKindf" id=""></td>
	                            <td><input type="text" placeholder="차대번호" name="carIdNumf" id=""></td>
	                            <td><input type="text" placeholder="차량번호" name="carNumf" id=""></td>
	                            <td><input type="text" placeholder="계약번호" name="contractNumf" id=""></td>
	                            <td ><input type="text" placeholder="운행거리(ex:20.5)" name="towDistancef" id=""></td>
	                            <td style="width:180px;">
	                            	<!-- <input style="width:48%; display:inline-block;" type="text" placeholder="상차제한" name="requirePicCntf" id="" onkeyup="javascript:getNumber(this);">
	                            	<input style="width:48%; display:inline-block;" type="text" placeholder="하차제한" name="requireDnPicCntf" id="" onkeyup="javascript:getNumber(this);"> -->
	                            </td>
	                           <td  class="notFirst widthAuto">
	                           		<!-- <input type="checkbox" style="margin-left:15px;" checked ><div style="margin-left:10px;  display:inline-block;">인수증전송</div> -->
	                           </td>
	                        </tr>
	
	                        <tr>
	                            <td  colspan=2 class="notFirst widthAuto">
	                                <!-- <input type="text" placeholder="출발지" name="departuref" id=""> -->
	                                <form action="">																
	                                	<input type="text" placeholder="출발지" class="search-box" name="departuref" id="" style="margin-top:5px;"> 		
	                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
	                                    <div class="search-result">
	                                    </div>
	                                </form>
	                            </td>
	                            <td colspan="5" class="notFirst widthAuto text-center">
	                                <input type="text" placeholder="Search" style="width: 89%; display:inline-block;" name="departureAddrf" id="">
	                                <input type="hidden" value="">
	                                <input type="button" class="btn-primary" value="검색" onclick="jusoSearch('departure',this);">
	                            </td>
	                            <td rowspan="2" class="notFirst widthAuto text-center">
	                                <input type="button" style="height:50px;" class="btn-primary" value="경로보기" onclick="routeView(this);">
	                            </td>
	                            <td><input type="text" placeholder="담당자명" name="departurePersonInChargef" id=""></td>
	                            <td><input type="text" placeholder="연락처" name="departurePhonef" id=""></td>
	                            <!-- <td></td> -->
	                        </tr>
	                        <tr>
	                            <td  colspan=2 class="notFirst widthAuto">
	                                <!-- <input type="text" placeholder="도착지" name="arrivalf" id=""> -->
	                                <form action="">																
	                                	<input type="text" placeholder="도착지" class="search-box" name="arrivalf" id="" style="margin-top:5px;"> 		
	                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
	                                    <div class="search-result">
	                                    </div>
	                                </form>
	                            </td>
	                            <td colspan="5" class="notFirst widthAuto text-center">
	                                <input type="text" placeholder="Search" style="width: 89%; display:inline-block;" name="arrivalAddrf" id="">
	                                <input type="hidden" value="">
	                                <input type="button" class="btn-primary" value="검색" onclick="jusoSearch('arrival',this);">
	                            </td>
	                            <td><input type="text" placeholder="담당자명" name="arrivalPersonInChargef" id=""></td>
	                            <td><input type="text" placeholder="연락처" name="arrivalPhonef" id=""></td>
	                            <!-- <td></td> -->
	                        </tr>
                        </tbody>
                </table>
                <table  data-name="applicationInfo"  style="margin-bottom:0px;">
                        <tr>
                            <td style="width:100px;" >앱정보</td>
                            <td style="width: 300px;">
                                <input style="width:48%; display:inline-block;" type="text" placeholder="상차사진제한" name="requirePicCntf" id="" onkeyup="javascript:getNumber(this);" value="1">
	                            <input style="width:48%; display:inline-block;" type="text" placeholder="하차사진제한" name="requireDnPicCntf" id="" onkeyup="javascript:getNumber(this);" value="1">
                            </td>
                            <td style="width: 190px;">
                            	<input type="text" placeholder="고객명" name="" id="">
                            </td>
                            <td style="width: 190px;">
                            	<input type="text" placeholder="이메일" name="" id="">
                            </td>
                            <td style="width: 190px;"><input type="text" placeholder="연락처" name="" id=""></td>
                            <td>
                            	<input type="checkbox" style="margin-left:15px;" ><div style="margin-left:10px;  display:inline-block;">인수증전송여부</div>
                            </td>
                        </tr>
                </table>
                
				<table data-name="paymentInfo">
						<tbody>
	                        <tr payment_division="01">
	                            <td style="width:100px;"  rowspan="3" class="vaTop" id="forPaymentAdd">결제정보</td>
	                            <td style="width:130px;">
	                                <form action="">																
	                                	<input type="text" class="search-box" placeholder="매출처" name="findCustomerForPayment" id="">
	                                	<input type="hidden" name="paymentCustomerId"> 		
	                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
	                                    <div class="search-result" id="">
	                                    </div>
	                                </form>
	                            </td>
	                            <td style="width: 145px;">
	                                <div class="select-con">
								        <select class="dropdown">
								        	<option  value=""  >결제방법</option>
								            <c:forEach var="data" items="${paymentDivisionList}" varStatus="status" >
								            	<c:if test='${data.payment_division_cd ne "CR"}'>
								        			<option  value="${data.payment_division_cd}"  >${data.payment_division}</option>
								        		</c:if>
											</c:forEach>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                            <td style="width: 145px;">
	                            	<div class="select-con">
								        <select class="dropdown" onchange="javascript:billingDtToggle(this);">
								        	<option  value=""  >증빙구분</option>
								             <c:forEach var="data" items="${billingDivisionList}" varStatus="status" >
								             	<c:if test='${data.billing_division_id ne "01"}'>
								        			<option  value="${data.billing_division_id}"  >${data.billing_division}</option>
								        		</c:if>
											</c:forEach>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                            <td style="width: 145px;">
	                                <div class="select-con">
								        <select class="dropdown">
								        	<!-- <option  value=""  >결제여부</option> -->
								            <c:forEach var="data" items="${payDivisionList}" varStatus="status" >
								        		<option  value="${data.pay_division_cd}"  >${data.pay_division}</option>
											</c:forEach>
								        </select>
								        <span></span>
								    </div> 
	                            </td> 
	                            <td style="width:130px;"><input type="text"  placeholder="입금일" name="cAcqDate" ></td>
	                            <td style="width:130px;"  class="notFirst widthAuto"><input type="text"  style="display:inline-block;" placeholder="계산서발행일자" name="cAcqDate" ></td>
	                            <td style="width:130px;"><input type="text"  onfocusout="javascript:setAmountForNotYet(this);" onkeyup="javascript:getNumber(this);" placeholder="업체청구액" name="amountf" id=""></td>
	                            <!-- <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="수정" onclick="javascript:notyet();"></td> -->
	                            <td style="" colspan="4"><input type="text"  style="display:inline-block;" placeholder="비고" name="" id=""></td>
	                            <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="추가" onclick="javascript:addPayment(0,this);"></td>
	                            <!-- <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="삭제" onclick="javascript:delPayment(0,this);"></td> -->
	                        </tr>
	                        
	                        <tr payment_division="02">
	                            <td class="notFirst widthAuto">
	                            	<div class="select-con">
								        <select class="dropdown" name="driverInfo" onchange="javascript:setDriverDeduct(this.value,this);">
								        	<option  value=""  >기사선택</option>
								        </select>
								        <input type="hidden" name="paymentCustomerId">
								        <span></span>
								    </div>
	                            </td>
	                            <td style="width: 145px;">
	                                <div class="select-con">
								        <select class="dropdown">
								        	<option  value=""  >결제방법</option>
								            <c:forEach var="data" items="${paymentDivisionList}" varStatus="status" >
								            	<c:if test='${data.payment_division_cd ne "CR"}'>
													<option  value="${data.payment_division_cd}"  >${data.payment_division}</option>
												</c:if>
												</c:forEach>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                            <td style="width: 145px;">
	                                <div class="select-con">
								        <select class="dropdown" onchange="javascript:billingDtToggle(this);">
								        	<option  value=""  >증빙구분</option>
								             <c:forEach var="data" items="${billingDivisionList}" varStatus="status" >
								             	<c:if test='${data.billing_division_id ne "01"}'>
							        				<option  value="${data.billing_division_id}"  >${data.billing_division}</option>
							        			</c:if>
											</c:forEach>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                            <td style="width: 145px;">
	                                <div class="select-con">
								        <select class="dropdown">
								        	<!-- <option  value=""  >결제여부</option> -->
								            <c:forEach var="data" items="${payDivisionList}" varStatus="status" >
												<option  value="${data.pay_division_cd}"  >${data.pay_division}</option>
												</c:forEach>
								        </select>
								        <span></span>
								    </div>
	                            </td> 
	                            <td style="width:130px;"><input type="text" placeholder="지급일" name="cAcqDate" ></td>
	                            <td style="width:130px;"  class="notFirst widthAuto"><input type="text"  style="display:inline-block;" placeholder="계산서발행일자" name="cAcqDate" ></td>
	                            <td  class="notFirst widthAuto"><input type="text"  onkeyup="javascript:getNumber(this);" style="display:inline-block;" placeholder="기사지급액" name="amountPaidf" id=""></td>
	                            <!-- <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="수정" onclick="javascript:notyet();"></td> -->
	                            <td style="width:130px;" colspan="2"><input type="text"  style="display:inline-block; width:85%;" readonly="readonly" placeholder="공제율" name="deductionRatef" id=""><span style="margin-left:5px;">%</span></td>
	                            <td style="width:130px;" colspan="2"><input type="text" placeholder="최종지급액" readonly="readonly" name="billForPaymentf" id=""></td>
	                            <td  class="notFirst widthAuto" ><input type="button"  class="btn-primary" value="추가" onclick="javascript:addPayment(1,this);"></td>
	                            <!-- <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="삭제" onclick="javascript:delPayment(1,this);"></td> -->
	                        </tr>
	                        <tr payment_division="03">
	                            <td class="notFirst widthAuto">
	                                <form action="">																
	                                	<input type="text" class="search-box" placeholder="매입처" name="findCustomerForPayment" id=""> 		
	                                	<input type="hidden" name="paymentCustomerId">
	                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
	                                    <div class="search-result" id="">
	                                    </div>
	                                </form>
	                            </td>
	                            <td style="width: 145px;">
	                                <div class="select-con">
								        <select class="dropdown">
								        	<option  value=""  >결제방법</option>
								            <c:forEach var="data" items="${paymentDivisionList}" varStatus="status" >
								            	<c:if test='${data.payment_division_cd ne "CR"}'>
													<option  value="${data.payment_division_cd}"  >${data.payment_division}</option>
												</c:if>
												</c:forEach>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                            <td style="width: 145px;">
	                                <div class="select-con">
								        <select class="dropdown" onchange="javascript:billingDtToggle(this);">
								        	<option  value=""  >증빙구분</option>
								             <c:forEach var="data" items="${billingDivisionList}" varStatus="status" >
								             	<c:if test='${data.billing_division_id ne "01"}'>
								        			<option  value="${data.billing_division_id}"  >${data.billing_division}</option>
								        		</c:if>
											</c:forEach>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                            <td style="width: 145px;">
	                                <div class="select-con">
								        <select class="dropdown">
								        	<!-- <option  value=""  >결제여부</option> -->
								            <c:forEach var="data" items="${payDivisionList}" varStatus="status" >
												<option  value="${data.pay_division_cd}"  >${data.pay_division}</option>
												</c:forEach>
								        </select>
								        <span></span>
								    </div>
	                            </td> 
	                            <td style="width:130px;"><input type="text" placeholder="지급일" name="cAcqDate" ></td>
	                            <td style="width:130px;" class="notFirst widthAuto"><input type="text"  style="display:inline-block;" placeholder="계산서발행일자" name="cAcqDate" ></td>
	                            <td style="width:130px;"><input type="text" onkeyup="javascript:getNumber(this);" placeholder="업체지급액" name="companyBillForPaymentf" id=""></td>
	                            <!-- <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="수정" onclick="javascript:notyet();"></td> -->
	                            <td style="" colspan="4"><input type="text"  style="display:inline-block;" placeholder="비고" name="" id=""></td>
	                            <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="추가" onclick="javascript:addPayment(2,this);"></td>
	                            <!-- <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="삭제" onclick="javascript:delPayment(2,this);"></td> -->
	                        </tr>
	                        </tbody>
                    	</table>
                    </div>
                    	
                <table>
                        <tr>
                            <td style="width:100px;"  class="vaTop">메모</td>
                            <td style="padding: 5.5px;"><textarea name="memof" id="memof"></textarea></td>
                        </tr>
                </table>

                <div class="confirmation">
                    <div class="confirm">
                        <input type="button" id="insertButton" value="확인" onclick="javascript:insertAllocation()">
                        <input type="button" id="modButton" value="수정" onclick="javascript:updateAllocation()" style="display:none;">
                    </div>
                    <div class="cancel">
                        <input type="button" value="취소" onclick="javascript:history.go(-1); return false;">
                    </div>
                </div>
               
            </section>
			
			
			<div id="forAddAllocation" style="display:none;">
			<table data-name="allocationInfo" style="margin-bottom:0px;">
                		<tbody>
	                        <tr>
	                            <td style="width:100px;" rowspan="4" class="vaTop">운행정보</td>
	                            <td style="width: 120px;">
	                            <div class="select-con">
							        <select class="dropdown" style="width:100%;">
							        	<option  value=""  >배차구분</option>
							        	<c:forEach var="data" items="${allocationDivisionList}" varStatus="status" >
							        		<option  value="${data.allocation_division_cd}"  >${data.allocation_division}</option>
										</c:forEach>
							        </select>
							        <span></span>
							    </div>
	                            
	                            </td>
	                            <td style="width: 120px;">
	                            	<div class="select-con">
								        <select class="dropdown" style="width:100%;" >
								        	<option  value=""  >운행구분</option>
								        	<c:forEach var="data" items="${runDivisionList}" varStatus="status" >
								        		<option  value="${data.run_division_cd}"  >${data.run_division}</option>
											</c:forEach>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                            <!-- <td style="width:150px;"><input type="text"  placeholder="출발일(ex:2019-01-01)" name="departureDtf" id="" class="datepick"></td> -->
	                            <td style="width:150px;"><input type="text"  placeholder="출발일(ex:2019-01-01)" name="cAcqDate" id="" class=""></td>
	                            <!-- <td style="width:150px;"><input type="text" placeholder="출발시간(ex:10:00)" name="departureTimef" readonly class="timepicker" id=""></td> -->
	                            <td style="width:150px;"><input type="text" placeholder="출발시간(ex:10:00)" name="departureTimef" class="" id="" onkeyup="javascript:if(event.keyCode!='8') timeValidation(this);"></td>
	                            <td>
	                            	<input type="text" placeholder="매출액" name="income" id="" onkeyup="javascript:getNumber(this);" onfocusout="javascript:setAmount(this);">
	                            </td>
	                            <td>
	                            	<input type="text" placeholder="기사선택" name="" id="" onclick="javascript:showModal(this);">
	                            	<input type="hidden" value="">
	                            	<input type="hidden" value="">
	                            </td>
	                            <td style="width:150px;"><input type="checkbox" style="margin-left:15px;" ><div style="margin-left:10px;  display:inline-block;">사고유무</div></td>
	                            <td style="padding: 10px 10px; width:300px;" colspan =4 rowspan=2>
	                            		<textarea class="remarks" style="height:100px;" name="etcf" id="" placeholder="비고"></textarea>
	                            </td>
	                            <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="추가" onclick="javascript:addAllocationInfo();"></td>
	                        </tr>
							<tr>
	                            <td  class="notFirst widthAuto" style="width:150px;"><input type="text" placeholder="차종" name="carKindf" id=""></td>
	                            <td><input type="text" placeholder="차대번호" name="carIdNumf" id=""></td>
	                            <td><input type="text" placeholder="차량번호" name="carNumf" id=""></td>
	                            <td><input type="text" placeholder="계약번호" name="contractNumf" id=""></td>
	                            <td ><input type="text" placeholder="운행거리(ex:20.5)" name="towDistancef" id=""></td>
	                            <td style="width:180px;">
	                            	<!-- <input style="width:48%; display:inline-block;" type="text" placeholder="상차제한" name="requirePicCntf" id="" onkeyup="javascript:getNumber(this);">
	                            	<input style="width:48%; display:inline-block;" type="text" placeholder="하차제한" name="requireDnPicCntf" id="" onkeyup="javascript:getNumber(this);"> -->
	                            </td>
	                            <td  class="notFirst widthAuto">
	                            	<!-- <input type="checkbox" style="margin-left:15px;" checked ><div style="margin-left:10px;  display:inline-block;">인수증전송</div> -->
	                            </td>
	                           <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="삭제" onclick="javascript:deleteAllocationInfo(this);"></td>
	                        </tr>
	
	                        <tr>
	                            <td  colspan=2 class="notFirst widthAuto">
	                                <!-- <input type="text" placeholder="출발지" name="departuref" id="" style="width: 65%; display:inline-block;"> -->
	                                <form action="" style=" width: 65%; display:inline-block;">																
	                                	<input type="text" placeholder="출발지" class="search-box" name="departuref" id="" style="margin-top:5px;"> 		
	                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
	                                    <div class="search-result">
	                                    </div>
	                                </form>
	                                <input type="button" class="btn-primary" value="복사" onclick="jusoCopy('departure',this);">
	                            </td>
	                            <td colspan="5" class="notFirst widthAuto text-center">
	                                <input type="text" placeholder="Search" style="width: 88%; display:inline-block;" name="departureAddrf" id="">
	                                <input type="hidden" value="">
	                                <input type="button" class="btn-primary" value="검색" onclick="jusoSearch('departure',this);">
	                            </td>
	                            <td rowspan="2" class="notFirst widthAuto text-center">
	                                <input type="button" style="height:50px;" class="btn-primary" value="경로보기" onclick="routeView(this);">
	                            </td>
	                            <td><input type="text" placeholder="담당자명" name="departurePersonInChargef" id=""></td>
	                            <td colspan=2><input type="text" placeholder="연락처" name="departurePhonef" id=""></td>
	                            <td></td>
	                        </tr>
	                        <tr>
	                            <td  colspan=2 class="notFirst widthAuto">
	                                <!-- <input type="text" placeholder="도착지" name="arrivalf" id="" style="width: 65%; display:inline-block;"> -->
	                                <form action="" style=" width: 65%; display:inline-block;">																
	                                	<input type="text" placeholder="도착지" class="search-box" name="arrivalf" id="" style="margin-top:5px;"> 		
	                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
	                                    <div class="search-result">
	                                    </div>
	                                </form>
	                                <input type="button" class="btn-primary" value="복사" onclick="jusoCopy('arrival',this);">
	                            </td>
	                            <td colspan="5" class="notFirst widthAuto text-center">
	                                <input type="text" placeholder="Search" style="width: 88%; display:inline-block;" name="arrivalAddrf" id="">
	                                <input type="hidden" value="">
	                                <input type="button" class="btn-primary" value="검색" onclick="jusoSearch('arrival',this);">
	                            </td>
	                            <td><input type="text" placeholder="담당자명" name="arrivalPersonInChargef" id=""></td>
	                            <td colspan=2><input type="text" placeholder="연락처" name="arrivalPhonef" id=""></td>
	                            <td></td>
	                        </tr>
                        </tbody>
                </table>
                
                <table  data-name="applicationInfo"  style="margin-bottom:0px;">
                        <tr>
                            <td style="width:100px;" >앱정보</td>
                            <td style="width: 300px;">
                                <input style="width:48%; display:inline-block;" type="text" placeholder="상차사진제한" name="requirePicCntf" id="" onkeyup="javascript:getNumber(this);" value="1">
	                            <input style="width:48%; display:inline-block;" type="text" placeholder="하차사진제한" name="requireDnPicCntf" id="" onkeyup="javascript:getNumber(this);" value="1">
                            </td>
                            <td style="width: 190px;">
                            	<input type="text" placeholder="고객명" name="" id="">
                            </td>
                            <td style="width: 190px;">
                            	<input type="text" placeholder="이메일" name="" id="">
                            </td>
                            <td style="width: 190px;"><input type="text" placeholder="연락처" name="" id=""></td>
                            <td>
                            	<input type="checkbox" style="margin-left:15px;"  ><div style="margin-left:10px;  display:inline-block;">인수증전송여부</div>
                            </td>
                        </tr>
                </table>
                
                
				<table data-name="paymentInfo">
						<tbody>
	                        <tr payment_division="01">
	                            <td style="width:100px;"  rowspan="3" class="vaTop" id="">결제정보</td>
	                            <td style="width:130px;">
	                                <form action="">																
	                                	<input type="text" class="search-box" placeholder="매출처" name="findCustomerForPayment" id="">
	                                	<input type="hidden" name="paymentCustomerId"> 		
	                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
	                                    <div class="search-result" id="">
	                                    </div>
	                                </form>
	                            </td>
	                            <td style="width: 145px;">
	                                <div class="select-con">
								        <select class="dropdown">
								        	<option  value=""  >결제방법</option>
								            <c:forEach var="data" items="${paymentDivisionList}" varStatus="status" >
								        		<option  value="${data.payment_division_cd}"  >${data.payment_division}</option>
											</c:forEach>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                            <td style="width: 145px;">
	                            	<div class="select-con">
								        <select class="dropdown" onchange="javascript:billingDtToggle(this);">
								        	<option  value=""  >증빙구분</option>
								             <c:forEach var="data" items="${billingDivisionList}" varStatus="status" >
								             	<c:if test='${data.billing_division_id ne "01"}'>
								        			<option  value="${data.billing_division_id}"  >${data.billing_division}</option>
								        		</c:if>
											</c:forEach>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                            <td style="width: 145px;">
	                                <div class="select-con">
								        <select class="dropdown">
								        	<!-- <option  value=""  >결제여부</option> -->
								            <c:forEach var="data" items="${payDivisionList}" varStatus="status" >
								        		<option  value="${data.pay_division_cd}"  >${data.pay_division}</option>
											</c:forEach>
								        </select>
								        <span></span>
								    </div> 
	                            </td> 
	                            <td style="width:130px;"><input type="text"  placeholder="입금일" name="cAcqDate" ></td>
	                            <td style="width:130px;"  class="notFirst widthAuto"><input type="text"  style="display:inline-block;" placeholder="계산서발행일자" name="cAcqDate" ></td>
	                            <td style="width:130px;"><input type="text"  onfocusout="javascript:setAmountForNotYet(this);" onkeyup="javascript:getNumber(this);" placeholder="업체청구액" name="amountf" id=""></td>
	                            <!-- <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="수정" onclick="javascript:notyet();"></td> -->
	                            <td style="" colspan="4"><input type="text"  style="display:inline-block;" placeholder="비고" name="" id=""></td>
	                            <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="추가" onclick="javascript:addPayment(0,this);"></td>
	                            <!-- <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="삭제" onclick="javascript:delPayment(0,this);"></td> -->
	                        </tr>
	                        
	                        <tr payment_division="02">
	                            <td class="notFirst widthAuto">
	                            	<div class="select-con">
								        <select class="dropdown" name="driverInfo" onchange="javascript:setDriverDeduct(this.value,this);">
								        	<option  value=""  >기사선택</option>
								        </select>
								        <input type="hidden" name="paymentCustomerId">
								        <span></span>
								    </div>
	                            </td>
	                            <td style="width: 145px;">
	                                <div class="select-con">
								        <select class="dropdown">
								        	<option  value=""  >결제방법</option>
								            <c:forEach var="data" items="${paymentDivisionList}" varStatus="status" >
												<option  value="${data.payment_division_cd}"  >${data.payment_division}</option>
											</c:forEach>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                            <td style="width: 145px;">
	                                <div class="select-con">
								        <select class="dropdown" onchange="javascript:billingDtToggle(this);">
								        	<option  value=""  >증빙구분</option>
								             <c:forEach var="data" items="${billingDivisionList}" varStatus="status" >
								             	<c:if test='${data.billing_division_id ne "01"}'>
								        			<option  value="${data.billing_division_id}"  >${data.billing_division}</option>
								        		</c:if>
											</c:forEach>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                            <td style="width: 145px;">
	                                <div class="select-con">
								        <select class="dropdown">
								        	<!-- <option  value=""  >결제여부</option> -->
								            <c:forEach var="data" items="${payDivisionList}" varStatus="status" >
												<option  value="${data.pay_division_cd}"  >${data.pay_division}</option>
											</c:forEach>
								        </select>
								        <span></span>
								    </div>
	                            </td> 
	                            <td style="width:130px;"><input type="text" placeholder="지급일" name="cAcqDate" ></td>
	                            <td style="width:130px;"  class="notFirst widthAuto"><input type="text"  style="display:inline-block;" placeholder="계산서발행일자" name="cAcqDate" ></td>
	                            <td  class="notFirst widthAuto"><input type="text"  onkeyup="javascript:getNumber(this);" style="display:inline-block;" placeholder="기사지급액" name="amountPaidf" id=""></td>
	                            <!-- <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="수정" onclick="javascript:notyet();"></td> -->
	                            <td style="width:130px;" colspan="2"><input type="text"  style="display:inline-block; width:85%;" readonly="readonly" placeholder="공제율" name="deductionRatef" id=""><span style="margin-left:5px;">%</span></td>
	                            <td style="width:130px;" colspan="2"><input type="text" placeholder="최종지급액" readonly="readonly" name="billForPaymentf" id=""></td>
	                            <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="추가" onclick="javascript:addPayment(1,this);"></td>
	                            <!-- <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="삭제" onclick="javascript:delPayment(1,this);"></td> -->
	                        </tr>
	                        <tr payment_division="03">
	                            <td class="notFirst widthAuto">
	                                <form action="">																
	                                	<input type="text" class="search-box" placeholder="매입처" name="findCustomerForPayment" id=""> 		
	                                	<input type="hidden" name="paymentCustomerId">
	                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
	                                    <div class="search-result" id="">
	                                    </div>
	                                </form>
	                            </td>
	                            <td style="width: 145px;">
	                                <div class="select-con">
								        <select class="dropdown">
								        	<option  value=""  >결제방법</option>
								            <c:forEach var="data" items="${paymentDivisionList}" varStatus="status" >
												<option  value="${data.payment_division_cd}"  >${data.payment_division}</option>
											</c:forEach>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                            <td style="width: 145px;">
	                                <div class="select-con">
								        <select class="dropdown" onchange="javascript:billingDtToggle(this);">
								        	<option  value=""  >증빙구분</option>
								             <c:forEach var="data" items="${billingDivisionList}" varStatus="status" >
								             	<c:if test='${data.billing_division_id ne "01"}'>
								        			<option  value="${data.billing_division_id}"  >${data.billing_division}</option>
								        		</c:if>
											</c:forEach>
								        </select>
								        <span></span>
								    </div>
	                            </td>
	                            <td style="width: 145px;">
	                                <div class="select-con">
								        <select class="dropdown">
								        	<!-- <option  value=""  >결제여부</option> -->
								            <c:forEach var="data" items="${payDivisionList}" varStatus="status" >
												<option  value="${data.pay_division_cd}"  >${data.pay_division}</option>
											</c:forEach>
								        </select>
								        <span></span>
								    </div>
	                            </td> 
	                            <td style="width:130px;"><input type="text" placeholder="지급일" name="cAcqDate" ></td>
	                            <td style="width:130px;" class="notFirst widthAuto"><input type="text"  style="display:inline-block;" placeholder="계산서발행일자" name="cAcqDate" ></td>
	                            <td style="width:130px;"><input type="text" onkeyup="javascript:getNumber(this);" placeholder="업체지급액" name="companyBillForPaymentf" id=""></td>
	                            <!-- <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="수정" onclick="javascript:notyet();"></td> -->
	                            <td style="" colspan="4"><input type="text"  style="display:inline-block;" placeholder="비고" name="" id=""></td>
	                            <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="추가" onclick="javascript:addPayment(2,this);"></td>
	                            <!-- <td  class="notFirst widthAuto"><input type="button"  class="btn-primary" value="삭제" onclick="javascript:delPayment(2,this);"></td> -->
	                        </tr>
	                        </tbody>
                    	</table>
			</div>
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
        </div>        
        
       <script>
		     
       
       
       

       
       
       
       
       
       
       
       
       
       
       
       
       
       
       </script>

