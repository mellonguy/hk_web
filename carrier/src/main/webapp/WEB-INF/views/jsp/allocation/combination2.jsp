<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld"%>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/js/bootstable.js"></script>
<script type="text/javascript"> 
var oldListOrder = "";
var allocationId = "";

 </script>

<div id="combination2" class="dispatch-wrapper">
	<%-- <jsp:include page="common-form.jsp"></jsp:include> --%>
	<section class="dispatch-bottom-content"></section>
	<div id="bottom-table">
		<section class="bottom-table"
			style="width: 1600px; margin-left: 10px;">
			<!-- <p>차대번호를 기사님이 입력한 경우에는 붉은색으로 표시 되고 배차 직원이 입력 한 경우에는 파란색으로 표시 됩니다. </p> -->
			<!-- <div style="color:#8B0000;">※차대번호를 기사님이 입력한 경우에는 붉은색으로 표시 되고 배차 직원이 입력 한 경우에는 파란색으로 표시 됩니다.</div> -->
			<table class="article-table forToggle" id="dataTable"
				style="width: 100%;">
				<colgroup>
					<col width="auto">
					<col width="120px;">
					<col width="auto">
					<col width="auto">
					<col width="230px;">
					<col width="90px;">
					<col width="70px;">
					<%-- <col width="auto"> --%>
					<col width="auto">
					<col width="auto">

					<col width="130px;">
					<col width="130px;">

					<col width="auto">
					<col width="100px;">
					<col width="auto">
					<col width="auto">
					<col width="auto">
					<col width="auto">
					<%-- <c:if test="${user.control_grade == '01' }">
	                        	<col width="auto">
	                        	<col width="auto">
	                        </c:if> --%>
				</colgroup>
				<thead>
					<tr>
						<td ><input type="checkbox" id="checkAll" onclick="javascript:checkAll();"></td>
						<!-- <td class="showToggle">번호</td> -->
						<td>의뢰일</td>
						<td>배차구분</td>
						<td>운행구분</td>
						<td>고객명 /담당자명</td>
						<c:if test="${user.dept == 'sunbo'}">
							<td>고객구분</td>
						</c:if>
						<td style="cursor: pointer;" onclick="javascript:sortby('E');">출발일<c:if
								test='${paramMap.forOrder eq "E" }'>
								<c:if test='${paramMap.order eq "asc" }'>
									<img style="margin-left: 10px;" src="/img/arrow-up.png" alt="">
								</c:if>
								<c:if test='${paramMap.order eq "desc" }'>
									<img style="margin-left: 10px;" src="/img/arrow-down.png"
										alt="">
								</c:if>
							</c:if></td>
						<td>출발시간</td>
						<!-- <td>비고</td> -->
						<td>차종</td>
						<!-- <td>등록구분</td> -->
						<td>차대번호</td>
						<td>차량번호</td>
						<td>출발지</td>
						<td>하차지</td>
						<td>기사명(회차)</td>
						<c:if test="${user.dept != 'sunbo'}">
							<td>매출액</td>
						</c:if>
						<c:if test="${user.dept != 'sunbo'}">
							<td>기사지급액</td>
						</c:if>
						<td>상태</td>
						<%-- <c:if test="${user.control_grade == '01' }">   
	                            	<td class="showToggle"></td>
	                            	<td>수정</td>
	                            </c:if> --%>
					</tr>
				</thead>
				<tbody id="">
					<c:forEach var="data" items="${listData}" varStatus="status">
						<tr class="ui-state-default" list-order="${data.list_order}"
							allocationId="${data.allocation_id}">
							<td class="showToggle"><input type="checkbox"
								name="forBatch" allocationId="${data.allocation_id}"
								batchStatus="${data.batch_status}"
								onclick="javascript:getCheckBoxCnt();">
							<td>${data.input_dt}</td>
							<td>${data.allocation_division}</td>
							<td>${data.run_division}</td>
							<td style="cursor: pointer;"
								onclick="javascript:selectList('${data.allocation_id}');">
								<c:if test="${data.missing_send =='F'}">
									<div
										style="background-color: red; border-radius: 4px; width: 8px; height: 8px; display: inline-block; margin-right: 5px;"></div>
								</c:if> <c:if test="${data.missing_send == 'Y'}">
									<div
										style="background-color: #00FF00; border-radius: 4px; width: 10px; height: 10px; display: inline-block; margin-right: 5px;"></div>
								</c:if> <c:if test="${data.company_id eq 'company2'}">
	                            ${data.customer_name} / ${data.charge_name}
	                            </c:if> <c:if
									test="${data.company_id eq 'company1'}">
									<span style="font-weight: bold; color: #9A2EFE;">
										${data.customer_name} / ${data.charge_name} </span>
								</c:if> <c:if
									test="${data.company_id eq 'COMa0f004a1336143078015c4be8f292e92'}">
									<span style="font-weight: bold; color: #964b00;">
										${data.customer_name} / ${data.charge_name} </span>
								</c:if>

							</td>
							<c:if test="${user.dept == 'sunbo'}">
								<td>${data.charge_dept}</td>
							</c:if>
							<td>${data.departure_dt}</td>
							<td>${data.departure_time}</td>
							<%-- <td>${data.etc}</td> --%>
							<td>${data.car_kind}</td>
							<%-- <td>${data.batch_status_name}</td> --%>
							<c:if test="${data.driver_mod_yn == 'Y'}">
								<td style="color: #F00;">${data.car_id_num}</td>
							</c:if>
							<c:if
								test="${data.driver_mod_yn == 'N' && data.mod_emp != null && data.mod_emp != ''}">
								<td style="color: #00F;">${data.car_id_num}</td>
							</c:if>
							<c:if
								test="${data.driver_mod_yn == 'N' && (data.mod_emp == null || data.mod_emp == '')}">
								<td>${data.car_id_num}</td>
							</c:if>
							<td>${data.car_num}</td>
							<td>${data.departure}</td>
							<td>${data.arrival}</td>
							<%-- <td>${data.driver_status}${data.driver_name}<c:if test="${data.allocation_status != 'N'}">(${data.driver_cnt})</c:if></td> --%>
							<td>${data.driver_status}${data.driver_name}<c:if
									test="${data.driver_name != null && data.driver_name != ''}">(${data.driver_cnt})</c:if></td>
							<c:if test="${user.dept != 'sunbo'}">
								<c:if test="${user.hidePrice == 'Y'}">
									<td></td>
								</c:if>
								<c:if test="${user.hidePrice != 'Y'}">
									<td>${data.sales_total}</td>
								</c:if>
							</c:if>
							<c:if test="${user.dept != 'sunbo'}">
								<td>${data.driver_amount}</td>
							</c:if>

							<c:if test="${data.allocation_status != 'C'}">
								<c:if test="${data.allocation_status == 'F'}">
									<c:if test="${data.payment == 'Y'}">
										<td>${data.allocation_status_name}<span><img
												style="width: 15px; height: 15px; cursor: pointer;"
												src="/img/question-icon.png"
												onclick="javascript:clipBoardCopy('${data.allocation_id}',this);"
												alt="" title='비고 : ${data.etc}&#10;'></span>
										</td>
									</c:if>
									<c:if test="${data.payment != 'Y'}">
										<c:if test="${data.payment_kind != 'DD'}">
											<td style="color: #EE0D0D;">${data.allocation_status_name}
												<span><img
													style="width: 15px; height: 15px; cursor: pointer;"
													src="/img/question-icon.png"
													onclick="javascript:clipBoardCopy('${data.allocation_id}',this);"
													alt="" title='비고 : ${data.etc}&#10;'></span>
											</td>
										</c:if>
										<c:if test="${data.payment_kind == 'DD'}">
											<td>${data.allocation_status_name}<span><img
													style="width: 15px; height: 15px; cursor: pointer;"
													src="/img/question-icon.png"
													onclick="javascript:clipBoardCopy('${data.allocation_id}',this);"
													alt="" title='비고 : ${data.etc}&#10;'></span>
											</td>
										</c:if>
									</c:if>
								</c:if>
								<c:if test="${data.allocation_status != 'F'}">
									<td>${data.allocation_status_name}<span><img
											style="width: 15px; height: 15px; cursor: pointer;"
											src="/img/question-icon.png"
											onclick="javascript:clipBoardCopy('${data.allocation_id}',this);"
											alt="" title='비고 : ${data.etc}&#10;'></span>
									</td>
								</c:if>
							</c:if>



							<c:if test="${data.allocation_status == 'C'}">
								<td style="color: #F0F;">${data.allocation_status_name}<span><img
										style="width: 15px; height: 15px; cursor: pointer;"
										src="/img/question-icon.png"
										onclick="javascript:clipBoardCopy('${data.allocation_id}',this);"
										alt="" title='비고 : ${data.etc}&#10;'></span>
								</td>
							</c:if>




							<%-- <c:if test="${user.control_grade == '01' }">
		                            <td class="showToggle">
		                                <a href="#" class="table-driver-btn" title="아이콘 설명 1"><i class=	"fa fa-truck" aria-hidden="true"></i></a>
		                                <a href="#" class="table-btn" title="아이콘 설명 2"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
		                                <a style="cursor:pointer" title="취소" onclick="javascript:updateAllocationStatus('cancel','${data.allocation_id}');" class="table-x">
		                                    <img src="/img/x-icon.png" alt="">
		                                </a>
		                                <a style="cursor:pointer"  title="완료" onclick="javascript:updateAllocationStatus('complete','${data.allocation_id}');" class="table-check">
		                                    <img src="/img/check-icon.png" alt="">
		                                </a>
		                            </td>
	                            </c:if> --%>
						</tr>
					</c:forEach>

				</tbody>
			</table>




			<!-- <div style="color:#8B0000;">※차대번호는 기사님이 입력한 경우에는 붉은색으로 표시 되고 배차 직원이 수정 한 경우에는 파란색으로 표시 됩니다.</div> -->
			<div style="">
				※차대번호는 기사님이 입력한 경우에는 <span style="color: #f00;">붉은색</span>으로 표시 되고
				배차 직원이 수정 한 경우에는 <span style="color: #00f;">파란색</span>으로 표시 됩니다.
			</div>
			<div class="table-pagination text-center">
				<ul class="pagination">
					<html:paging uri="/allocation/combination.do"
						forGroup="&startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&searchDateType=${paramMap.searchDateType}&forOrder=${paramMap.forOrder}%5E${paramMap.order}&allocationStatus=${paramMap.allocationStatus}"
						frontYn="N" />
					<!--  <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
	                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
	                        <li class="curr-page"><a href="#">1</a></li>
	                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
	                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li> -->
				</ul>
			</div>

		</section>

		<div class="confirmation">
			<!-- <div class="cancel">
                        <a style="cursor:pointer;" onclick="javascript:pickUpValidation('N');">픽업해제</a>
                    </div> -->
			<div class="confirm">
				<c:if test="${paramMap.allocationStatus != 'W'}">
					<a style="cursor: pointer;"
						onclick="javascript:pickUpValidation('P');">픽업설정</a>
				</c:if>
				<c:if test="${paramMap.allocationStatus == 'W'}">
					<a style="cursor: pointer;"
						onclick="javascript:setConfirmation('N');">예약확정처리</a>
				</c:if>


			</div>
			<div class="cancel">
				<a style="cursor: pointer;" onclick="javascript:batchStatus('X');">취소요청</a>
			</div>
		</div>
		<div>
			체크된 항목의 갯수 : <span id="checkedChk_box"></span>
		</div>
	</div>
</div>


