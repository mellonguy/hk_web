<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>한국카캐리어</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="/img/favicon.ico">
<link rel="stylesheet" href="/css/bootstrap.min.css">
<link rel="stylesheet" href="/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="/fonts/NotoSans/notosanskr.css">
<link rel="stylesheet" href="/css/reset.css">
<link rel="stylesheet" href="/css/main.css">
<link rel="stylesheet" href="/css/main2.css">
<link rel="stylesheet" href="/css/responsive.css">
<link rel="stylesheet" href="/css/jquery-ui.css">
<link rel="stylesheet" href="/css/font-awesome-css.min.css">
<script src="/js/vendor/modernizr-2.8.3.min.js"></script>
<script src="/js/vendor/jquery-1.11.2.min.js"></script>
<!-- <script src="/js/89d51f385b.js"></script> -->
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<!-- <script type="text/javascript" src="/js/plugins/jquery.form.min.js"></script> -->
<script src="/js/vendor/jquery-1.11.2.min.js"></script>
<script type="text/javascript">


</script>
	
</head>
		
	<body style="width:800px;">
	
	<div style="float:left;">
		<div id="map" style="width:800px; height:800px;">
		</div>
	</div>
		
	<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=196301395a31adf3319a7ee5f66f17da"></script>
	<script>
	
	$(document).ready(function(){
	
	var container = document.getElementById('map'); //지도를 담을 영역의 DOM 레퍼런스
	var options = { //지도를 생성할 때 필요한 기본 옵션
		center: new daum.maps.LatLng('36.250701','127.870667'), //지도의 중심좌표.
		//center : new daum.maps.LatLng('37.4540097','127.0008677'),
		level: 12 //지도의 레벨(확대, 축소 정도)
		//level : 1
	};
	
	var map = new daum.maps.Map(container, options);
	var points = new Array();
	<c:forEach var="item" items="${driverList}" varStatus="status">
		var obj = new Object();
		obj.content = '<div style="padding:5px; text-align:center; width:190px;">${item.driver_name}/${item.car_num}</div>';
		obj.point = new daum.maps.LatLng('${item.lat}','${item.lng}');
		obj.driverId = '${item.driver_id}';
		points.push(obj);
	</c:forEach>
	
	var i, marker;
	for (i = 0; i < points.length; i++) {
	    	marker =     new daum.maps.Marker({ position : points[i].point });
		    marker.setMap(map);
		    
		    var  infowindow = new daum.maps.InfoWindow({
		  	  content : points[i].content
		  	});
		 
		   infowindow.open(map, marker);
		   //daum.maps.event.addListener(marker, 'mouseover', makeOverListener(map, marker, infowindow));
		   
		   var driverId = points[i].driverId;
		   //alert(driverId);
		   viewDriverRoute(marker,driverId);
		   
		   
	}

});
	
	
	function viewDriverRoute(marker,driverId){
		
		daum.maps.event.addListener(marker, 'click', function() {
			   
			window.open("/allocation/viewDriverRoute.do?driverId="+encodeURI(driverId),"_blank","top=0,left=0,width=800,height=800,toolbar=0,status=0,scrollbars=1,resizable=0");     
		       
		});
		
		
	}
	
	
	
	
	
	</script>
	
	
</body>
</html>

