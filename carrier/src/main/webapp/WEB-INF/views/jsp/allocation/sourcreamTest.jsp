<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>
<%

%>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/js/bootstable.js"></script>
<script type="text/javascript"> 
var oldListOrder = "";
var allocationId = "";

$(document).ready(function(){
	$(".dispatch-bottom-content").addClass('active');
    $("downArrow").addClass('active');
   // alert("${userMap.control_grade}");
});

function updateAllocaion(row){
	var inputDt = "";
    var carrierType = "";
    var distanceType = "";
    var departureDt = "";
    var departureTime = "";
    var customerName = "";
    var carKind = "";
    var carIdNum = "";
    var carNum  = "";
    var departure = "";
    var arrival  = "";
    var driverName = "";
    var carCnt  = "";
    var amount   = "";
    var paymentKind = "";
	var allocId = $(row).attr("allocationId");
	if(confirm("수정 하시겠습니까?")){
		$(row).find("td").each(function(index,element){
			if(index==1){
				inputDt = $(this).html();
			}
			if(index==2){
				if($(this).html() == "셀프"){
					carrierType = "S";	
				}else if($(this).html() == "캐리어"){
					carrierType = "C";
				}else{
					alert("배차구분을 확인 하세요.");
					return false;
				}
			}
			if(index==3){
				if($(this).html() == "시내"){
					distanceType = "0";	
				}else if($(this).html() == "시외"){
					distanceType = "1";
				}else{
					alert("거리구분을 확인 하세요.");
					return false;
				}
			}
			if(index==4){
				departureDt = $(this).html();
			}
			if(index==5){
				departureTime = $(this).html();
			}
			if(index==6){
				customerName = $(this).html();
			}
			if(index==7){
				carKind = $(this).html();
			}
			if(index==8){
				carIdNum = $(this).html();
			}
			if(index==9){
				carNum = $(this).html();
			}
			if(index==10){
				departure = $(this).html();
			}
			if(index==11){
				arrival = $(this).html();
			}
			if(index==12){
				driverName = $(this).html();
			}
			if(index==13){
				carCnt = $(this).html();
			}
			if(index==14){
				amount = $(this).html();
			}
			if(index==15){
				paymentKind = $(this).html();
			}
      	});

		if(carrierType == "" || distanceType == ""){
			return false;	
		}
		
		 $.ajax({ 
				type: 'post' ,
				url : "/allocation/update-allocation.do" ,
				dataType : 'json' ,
				data : {
					inputDt : inputDt,
					carrierType : carrierType,
					distanceType : distanceType,
					departureDt : departureDt,
					departureTime : departureTime,
					customerName : customerName,
					carKind : carKind,
					carIdNum : carIdNum,
					carNum : carNum,
					departure : departure,
					arrival : arrival,
					driverName : driverName,
					carCnt : carCnt,
					amount : amount,
					paymentKind : paymentKind,
					allocationId : allocId
				},
				success : function(data, textStatus, jqXHR)
				{
					var result = data.resultCode;
					if(result == "0000"){
						alert("변경 되었습니다.");
			//			document.location.href = "/allocation/combination.do";
					}else if(result == "0001"){
   						alert("변경 하는데 실패 하였습니다.");
   					}
				} ,
				error : function(xhRequest, ErrorText, thrownError) {
				}
			}); 
		
	}

}


function selectList(id){
	
	$(".dispatch-bottom-content").addClass('active');
    $("downArrow").addClass('active');
	
	$.ajax({ 
		type: 'post' ,
		url : "/allocation/selectAllocationInfo.do" ,
		dataType : 'json' ,
		data : {
			allocationId : id
		},
		success : function(data, textStatus, jqXHR)
		{
			var result = data.resultCode;
			var resultData = data.resultData;
			var resultDataSub = data.resultDataSub;
			if(result == "0000"){
				$("#allocationId").val(resultData.allocation_id);
				 $("#inputDtf").val(resultData.input_dt);
				 $("#commentf").val(resultData.comment);
				 $("#customerNamef").val(resultData.customer_name);
				 $("#customerPhoneNumf").val(resultData.customer_phone_num);
				 $("#carCntf").val(resultData.car_cnt);
				 $("#departureDtf").val(resultData.departure_dt);
				 $("#departureTimef").val(resultData.departure_time);
				 /* $("#carKindf").val(resultData.car_kind);
				 $("#carIdNumf").val(resultData.car_id_num);
				 $("#carNumf").val(resultData.car_num);
				 $("#contractNumf").val(resultData.contract_num); */
				 $("#driverNamef").val(resultData.driver_name);
				 $("#towDistancef").val(resultData.tow_distance);
				 $("#departuref").val(resultData.departure);
				 $("#departureAddrf").val(resultData.departure_addr);
				 $("#departurePersonInChargef").val(resultData.departure_person_in_charge);
				 $("#departurePhonef").val(resultData.departure_phone);
				 $("#etcf").val(resultData.etc);
				 $("#arrivalf").val(resultData.arrival);
				 $("#arrivalAddrf").val(resultData.arrival_addr);
				 $("#arrivalPersonInChargef").val(resultData.arrival_person_in_charge);
				 $("#arrivalPhonef").val(resultData.arrival_phone);
				 $("#amountf").val(resultData.amount);
				 $("#billForPaymentf").val(resultData.bill_for_payment);
				 $("#paymentAccountNumberf").val(resultData.payment_account_number);
				 $("#memof").val(resultData.memo);
				 $("#insertButton").css("display","none");
				 $("#modButton").css("display","");
				 $("#driverPaymentAccountNumberf").val(resultData.driver_payment_account_number);
				 $("#companyPaymentAccountNumberf").val(resultData.company_payment_account_number);
				 $("#companyBillForPaymentf").val(resultData.company_bill_for_payment);
				 $("#paymentDtf").val(resultData.payment_dt);
				 $("#driverPaymentDtf").val(resultData.driver_payment_dt);
				 $("#companyPaymentDtf").val(resultData.company_payment_dt);
				 $("#billingDtMainf").val(resultData.billing_dt_main);
				 $("#billingDtSecf").val(resultData.billing_dt_sec);
				 $("#billingDtThirdf").val(resultData.billing_dt_third);
				 $("#companyId").val(resultData.company_id);
				 $("#driverId").val(resultData.driver_id);
				 
				var carCnt = $("#carCntf").val();
				$("#carInfo").html("");
				var resultSub = "";
				if(resultDataSub.length != 0){
					if(resultDataSub.length == 1){
						$("#carKindf").attr("disabled",false);
			    		 $("#carIdNumf").attr("disabled",false);
			    		 $("#carNumf").attr("disabled",false);
			    		 $("#contractNumf").attr("disabled",false);
						 $("#carKindf").val(resultDataSub[0].car_kind);
						 $("#carIdNumf").val(resultDataSub[0].car_id_num);
						 $("#carNumf").val(resultDataSub[0].car_num);
						 $("#contractNumf").val(resultDataSub[0].contract_num);
					}else{
						for(var i = 0; i < resultDataSub.length; i++){
							
							var carInfo= new Object();
							carInfo.carKind = resultDataSub[i].car_kind;
							carInfo.carIdNum = resultDataSub[i].car_id_num;
							carInfo.carNum = resultDataSub[i].car_num;
							carInfo.contractNum = resultDataSub[i].contract_num;
							resultSub += '<tr class="ui-state-default">';
							resultSub += '<td style="width:60px;">'+(i+1)+'</td>';
							resultSub += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="차종" name="" id="" value="'+resultDataSub[i].car_kind+'"></td>';
							resultSub += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="차대번호" name="" id="" value="'+resultDataSub[i].car_id_num+'"></td>';
							resultSub += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="차량번호" name="" id="" value="'+resultDataSub[i].car_num+'"></td>';
							resultSub += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="계약번호" name="" id="" value="'+resultDataSub[i].contract_num+'"></td>';
							resultSub += '</tr>';
							carInfoList.push(carInfo);
						}	
						$("#carKindf").val("");
			    		 $("#carIdNumf").val("");
			    		 $("#carNumf").val("");
			    		 $("#contractNumf").val("");
			    		 $("#carKindf").attr("disabled",true);
			    		 $("#carIdNumf").attr("disabled",true);
			    		 $("#carNumf").attr("disabled",true);
			    		 $("#contractNumf").attr("disabled",true);
					}
				}else{
					for(var i = 0; i < Number(carCnt); i++){
						resultSub += '<tr class="ui-state-default">';
						resultSub += '<td style="width:60px;">'+(i+1)+'</td>';
						resultSub += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="차종" name="" id=""></td>';
						resultSub += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="차대번호" name="" id=""></td>';
						resultSub += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="차량번호" name="" id=""></td>';
						resultSub += '<td style="width:100px;"><input style="width:100%;" type="text" placeholder="계약번호" name="" id=""></td>';
						resultSub += '</tr>';
					}	
				}
				$("#carInfo").html(resultSub);
				 
			}else if(result == "0001"){
				alert("변경 하는데 실패 하였습니다.");
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
	
}	
	
	
function batchStatus(status){
	
	var id = "";
	var total = $('input:checkbox[name="forBatch"]:checked').length;
	
	if(total == 0){
		alert("수정할 목록이 선택 되지 않았습니다.");
		return false;
	}else{
		$('input:checkbox[name="forBatch"]:checked').each(function(index,element) {
		      if(this.checked){//checked 처리된 항목의 값
		    	  id+=$(this).attr("allocationId");
		    	  if(index<total-1){
		    		  id += ","; 
			         } 
		      }
		 });	
		updateAllocationStatus(status,id);
	}
}	

function updateAllocationStatus(status,id){
	
	var msg = "";
	if(status == "cancel"){
		msg = "취소";
	}else if(status == "complete"){
		msg = "완료";
	}
	
	if(confirm(msg+"하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : "/allocation/updateAllocationStatus.do" ,
			dataType : 'json' ,
			data : {
				status : status,
				allocationId : id
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert(msg+"되었습니다.");
					document.location.href = "/allocation/combination.do";
				}else if(result == "0001"){
					alert(msg+"하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
}	
	
function checkAll(){

	if($('input:checkbox[id="checkAll"]').is(":checked")){
		$('input:checkbox[name="forBatch"]').each(function(index,element) {
			$(this).prop("checked","true");
		 });	
	}else{
		$('input:checkbox[name="forBatch"]').each(function(index,element) {
			$(this).prop("checked","");
		 });
	}
}
	
 </script>


<embed src="http://18.217.110.213/data/file/song/us/2950003115_VOR2FolH_0de7454c3f8a7c311e2446feb869474a9c1f81f0.mp3">


<!-- <embed  type="audio/mpeg" autoplay="false"  src="http://18.217.110.213/data/file/song/us/2950003115_VOR2FolH_0de7454c3f8a7c311e2446feb869474a9c1f81f0.mp3" > -->

  


<!-- <audio controls="controls">
  Your browser does not support the &lt;audio&gt; tag.
  <source src="http://18.217.110.213/data/file/song/us/2950003115_VOR2FolH_0de7454c3f8a7c311e2446feb869474a9c1f81f0.mp3" type="audio/mpeg" />
</audio> -->




 
        
       <script>
		if("${userMap.control_grade}" == "01"){
			$('#dataTable').SetEditable({
            	columnsEd: "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15",
            	 onEdit: function(row) {updateAllocaion(row)},  
            	 onDelete: function() {},  
            	 onBeforeDelete: function() {}, 
            	 onAdd: function() {}  
            });
		}          
            </script>

