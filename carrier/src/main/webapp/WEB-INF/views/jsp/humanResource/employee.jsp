<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>
<%

%>

<script type="text/javascript">
$(document).ready(function(){
	
	
});

function editEmp(empId){
	
	document.location.href = "/baseinfo/edit-employee.do?empId="+empId;
}

function addEmp(){
	document.location.href = "/baseinfo/add-employee.do";
}


function searchStatusChange(searchStatus,obj){
	
	
	if($("#startDt").val() != ""){
		if($("#endDt").val() == ""){
			alert("조회 종료일이 지정되지 않았습니다.");
			return false;
		}
	}
	
	if($("#endDt").val() != ""){
		if($("#startDt").val() == ""){
			alert("조회 시작일이 지정되지 않았습니다.");
			return false;
		}
	}
	
	document.location.href = "/humanResource/employee.do?searchStatus="+searchStatus+"&startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val();
	
	
	
	/* $('input[name="searchStatus"]').each(function(index,element) {
		$(this).removeClass("btn-primary");
		$(this).addClass("btn-normal");
	 });
	$(obj).removeClass("btn-normal");
	$(obj).addClass("btn-primary"); */

//	
	
	/* $.ajax({ 
		type: 'post' ,
		url : "/humanResource/getEmpData.do" ,
		dataType : 'json' ,
		data : {
			searchStatus : searchStatus,
			startDt : $("#startDt").val(),
			endDt : $("#endDt").val()
		},
		success : function(data, textStatus, jqXHR)
		{
			var resultCode = data.resultCode;
			var resultData = data.resultData;
			var resultDataSub = data.resultDataSub;
			$("#dataArea").html("");
			if(resultCode == "0000"){
				var result = "";
				 for(var i = 0; i < resultData.length; i++){
					 result += '<tr class="ui-state-default">';
					 result += '<td style="text-align:center;">'+resultData[i].emp_id+'</td>';
					 result += '<td style="text-align:center;">'+resultData[i].emp_name+'</td>';
					 result += '<td style="text-align:center;">';
					 if(resultData[i].phone_work != null && resultData[i].phone_work != ""){
						 result += '<div style="display:block;">업무용 : '+resultData[i].phone_work+'</div>';	 
					 }
					 if(resultData[i].phone_personal != null && resultData[i].phone_personal != ""){
						 result += '<div style="display:block; margin-top:5px;">개인 : '+resultData[i].phone_personal+'</div>';	 
					 }
					 result += '</td>';
					 result += '<td style="text-align:center;">'+resultData[i].join_dt+'</td>';
					 result += '<td style="text-align:center;">'+resultData[i].resign_dt+'</td>';
					 result += '<td style="text-align:center;">'+resultData[i].emp_position+'</td>';
					 result += '<td style="text-align:center;">'+resultData[i].email_work+' </td>';
					 result += '</tr>';
					  
				 }
				 $("#dataArea").html(result);
				 $("#startDt").val(resultDataSub.startDt);
				 $("#endDt").val(resultDataSub.endDt);
			}else if(resultCode == "0002"){
				alert("");
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	}); */	
	
	
	

	
}




</script>
		<!-- <section class="side-nav"> -->
            <!-- <ul>
                <li class="active"><a href="/baseinfo/employee.do">직원관리</a></li>
                <li class=""><a href="/baseinfo/driver.do">기사관리</a></li>
                <li class=""><a href="/baseinfo/customer.do">거래처관리</a></li>
                <li class=""><a href="/baseinfo/company.do">회사정보관리</a></li>
            </ul> -->
        <!-- </section> -->

        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">HOME</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">직원관리</a></li>
                </ul>
            </div>

            <div class="up-dl clearfix header-search">
                 <table>
                    <tbody>
                        <tr>
                        	<td>조회기간 :</td>
				            <td class="widthAuto" style="width:300px;">
				                <input style="width:40%; text-align:center;" class="datepick" type="text" placeholder="조회시작일" onclick="javascript:$(this).val('');"  readonly="readonly" name="startDt" id="startDt" value="${paramMap.startDt}">&nbsp;~&nbsp;
				                <input style="width:40%; text-align:center;" class="datepick" type="text" placeholder="조회종료일" onclick="javascript:$(this).val('');" readonly="readonly" name="endDt" id="endDt"  value="${paramMap.endDt}">
				            </td>
                        	<td>
                                <input type="button" id="searchStatusU" name="searchStatus" value="재직자 현황 조회" class="<c:if test="${paramMap.searchStatus == 'U'}">btn-primary</c:if><c:if test="${paramMap.searchStatus != 'U'}">btn-normal</c:if>" onclick="javascript:searchStatusChange('U',this);">
                            </td>
                        	<%-- <td>					
                                <input type="button" id="searchStatusI" name="searchStatus" value="입사자 현황 조회" class="<c:if test="${paramMap.searchStatus == 'I'}">btn-primary</c:if><c:if test="${paramMap.searchStatus != 'I'}">btn-normal</c:if>" onclick="javascript:searchStatusChange('I',this);">
                            </td> --%>
                            <td>
                                <input type="button" id="searchStatusO" name="searchStatus" value="퇴사자 현황 조회" class="<c:if test="${paramMap.searchStatus == 'O'}">btn-primary</c:if><c:if test="${paramMap.searchStatus != 'O'}">btn-normal</c:if>" onclick="javascript:searchStatusChange('O',this);">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </section>

            <section class="bottom-table" style="width:1600px; margin-left:290px;">
            	                                        
                <table class="article-table" style="margin-top:15px">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <td style="text-align:center;">사원번호</td>
                            <td style="text-align:center;">성명</td>
                            <td style="text-align:center;">연락처</td>
                            <td style="text-align:center;">입사일</td>
                            <td style="text-align:center;">퇴사일</td>
                            <td style="text-align:center;">직급</td>
                            <td style="text-align:center;">이메일</td>
                            <!-- <td style="text-align:center;">첨부파일</td> -->
                        </tr>
                    </thead>
                    <tbody id="dataArea">
                    	<c:forEach var="data" items="${listData}" varStatus="status">
							<tr class="ui-state-default"> 
	                            <td style="text-align:center;">${data.emp_id}</td>
	                            <td style="text-align:center;">${data.emp_name}</td>
	                            <td style="text-align:center; width:15%;">
	                            	<c:if test="${data.phone_personal != null && data.phone_personal != ''}">
	                            		<div style="display:block;">연락처 1 : ${data.phone_personal}</div>
	                            	</c:if>
	                            	<c:if test="${data.phone_work != null && data.phone_work != '' && data.phone_work != data.phone_personal}">
	                            		<div style="display:block; margin-top:5px;">연락처 2 : ${data.phone_work}</div>
	                            	</c:if>
	                            </td>
	                            <td style="text-align:center;">${data.join_dt}</td>
	                            <td style="text-align:center;">${data.resign_dt}</td>
	                            <td style="text-align:center;">${data.emp_position}</td>
	                            <td style="text-align:center;">${data.email_work} </td>
	                            <%-- <td style="text-align:center;">
	                            	<c:forEach var="fileData" items="${data.fileList}" varStatus="status">
			                            <a target="_blank" href="/files/employee${fileData.file_path }">
			                            	<img style="width:40px; height:40px;" src="/files/employee${fileData.file_path }" alt="${fileData.file_nm}" title="${fileData.file_nm}" />
										</a>
									</c:forEach>
	                            </td> --%>
                        	</tr>
						</c:forEach>
                        
                    </tbody>
                </table>
                <div class="table-pagination text-center">
                    <ul class="pagination">
                        <!-- <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="curr-page"><a href="#">1</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li> -->
                        <html:paging uri="/baseinfo/employee.do" frontYn="N" />
                    </ul>
                </div>
                <div class="confirmation">
                    <div class="confirm">
                        <!-- <a href="/baseinfo/add-employee.do">직원등록</a> -->
                    </div>
                </div>
                
            </section>
     
