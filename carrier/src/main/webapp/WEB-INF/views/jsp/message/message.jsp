<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%

%>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	
	
	
});

$(function(){
    var responseMessage = "<c:out value="${msg}" />";
    if(responseMessage != ""){
        alert(responseMessage);
    }
}) 

// 신규메시지등록
function addMessage(){
	
 	var file = document.getElementById("upload");
 	//alert(file.files.length);
 	for(var i = 0; i < file.files.length; i++){
		var ext = file.files[i].name.split(".").pop().toLowerCase();
		if($.inArray(ext, ["jpeg","jpg","gif","png","pdf"]) == -1){
			alert("업로드 할 수 없는 파일이 있습니다.");
			$("#upload").val("");
			return false;
		}
	}

 	if($("#message").val() == ""){
		alert("본문내용이 없습니다");
		return false;
	}
 	
 	if($("#customer_phone").val() == ""){
		alert("연락처가 입력되지 않았습니다.");
		return false;
	}
	
 	if(!phoneChk($("#customer_phone"))){
  		alert("유효하지 않은 전화번호 입니다.");
  		$("#customer_phone").val("");
           $("#customer_phone").focus();
		return false;
	}

       
	if(confirm("등록 하시겠습니까?")){
		
		var url = "/message/sendMessage.do";
		var formData = new FormData($("#insertForm")[0]);
		
		$.ajax({ 
			type: 'post' ,
			url : url ,
			dataType : 'json' ,
			data : formData,
			contentType : false,
			processData : false,
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultMsg = data.resultMsg;
				if(result == "0000"){
					alert(resultMsg);
				}else if(result == "0001"){
					alert(resultMsg);
				}
				location.reload();
			},
			error : function(xhRequest, ErrorText, thrownError) {
				location.reload();
			}
		});
		
	}
}



//신규메시지등록 - new api
function addMessageNew(){
	
 	var file = document.getElementById("upload");
 	   
	if(confirm("등록 하시겠습니까?")){
		
		var url = "/message/sendMessageNew.do";
		var formData = new FormData($("#insertForm")[0]);
		
		$.ajax({ 
			type: 'post' ,
			url : url ,
			dataType : 'json' ,
			data : formData,
			contentType : false,
			processData : false,
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultMsg = data.resultMsg;
				if(result == "0000"){
					alert(resultMsg);
				}else if(result == "0001"){
					alert(resultMsg);
				}
				location.reload();
			},
			error : function(xhRequest, ErrorText, thrownError) {
				location.reload();
			}
		});
		
	}
}




</script>
		<!-- <section class="side-nav">
            <ul>
                <li class="active"><a href="/baseinfo/employee.do">직원관리</a></li>
                <li class=""><a href="/baseinfo/driver.do">기사관리</a></li>
                <li class=""><a href="/baseinfo/customer.do">거래처관리</a></li>
                <li class=""><a href="/baseinfo/company.do">회사정보관리</a></li>
            </ul>
        </section> -->

       <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">HOME</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">메시지방송등록</a></li>
                </ul>
            </div>
        </section>

        <div class="dispatch-wrapper">

            <section class="dispatch-bottom-content active" style=" margin:auto;">
               <form id="insertForm" method="post" enctype="multipart/form-data">
                   <h3 style=" font-size: 20px; text-align: center; margin-bottom:20px; font-weight: 700; font-family: NotoSansBold,sans-serif;">신규메시지등록</h3>
                    <!-- <input type="hidden" name="support" id="support" value=""> -->
                    <!-- <input type="hidden" name="empStatus" id="empStatus" value="01"> -->
               <div class="form-con clearfix" style="max-width:1200px;">
                   <!-- <div class="column"> -->
                   <div class="column" style="margin-bottom:5px">
                    	<table>
	                            <tbody>
	                                <tr>
	                                    <td>메시지 종류<span style="color:#F00;">&nbsp;*</span></td>
	                                    <td class="widthAuto" style="">
	                                        <input type="checkbox" id="alarmtalk" name="alarmtalk" value="Y"/>
							    			<label >알람톡</label>
							    			<input type="checkbox" id="sms" name="sms" value="Y" />
							    			<label >SMS</label>
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>연락처<span style="color:#F00;">&nbsp;*</span></td>
	                                    <td class="widthAuto" style="">
	                                        <input type="text" placeholder="연락처" name="customer_phone" id="customer_phone">
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	              
	                        <table>
	                            <tbody>
	                                <tr>
	                                    <td>본문내용</td>
	                                    <td class="widthAuto" style="">
	                                        <textarea rows="5" cols="30" placeholder="본문내용" name="message" id="message"></textarea>
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>

                            <table>
	                            <tbody>
	                                <tr>
	                                    <td>첨부파일</td>
	                                    <td class="widthAuto" style="">
	                                        <input type="file" id="upload"  name="bbsFile" multiple>
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
                            
               </div>
                
                    <div class="confirmation">
                        <div class="confirm">
                            <input type="button" value="등록" onclick="javascript:addMessage();">
                        </div>
                        <div class="confirm">
                            <input type="button" value="등록-신규버전" onclick="javascript:addMessageNew();">
                        </div>
                        <div class="cancel">
                        <input type="button" value="취소" onclick="javascript:history.go(-1); return false;">
                    </div>
                    </div>
                    <!-- <script src="js/vendor/bootstrap-datepicker.min.js"></script> -->
                </form>
                <!-- <div class="table-pagination text-center">
                    <ul class="pagination">
                        <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="curr-page"><a href="#">1</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
                    </ul>
                </div> -->
            </section>
        </div>
