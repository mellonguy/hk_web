<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>
<%

%>


<script type="text/javascript">
$(document).ready(function(){
	
	$("#startDt").MonthPicker({ 
		Button: false
		,MonthFormat: 'yy-mm'	
		,OnAfterChooseMonth: function() { 
	        //alert($(this).val());
	        document.location.href = "/carmanagement/lowViolation-reg.do?&selectMonth="+$(this).val();
	    }
	});
	
});


var rgx1 = /\D/g;  // /[^0-9]/g 와 같은 표현
var rgx2 = /(\d+)(\d{3})/; 

function getNumber(obj){
	
    var num01;
    var num02;
    num01 = obj.value;
    num02 = num01.replace(rgx1,"");
    num01 = setComma(num02);
    obj.value =  num01;
      
}

function setComma(inNum){
    
    var outNum;
    outNum = inNum; 
    while (rgx2.test(outNum)) {
         outNum = outNum.replace(rgx2, '$1' + ',' + '$2');
     }
    return outNum;

}



function search(searchStatus,obj){
	
	document.location.href = "/carmanagement/lowViolation-reg.do?selectMonth="+$("#startDt").val();
}

function addEmp(){
	document.location.href = "/baseinfo/add-employee.do";
}

function getNumberOnly (str) {			//저장할때는 콤마 제거 후 저장한다.
    var len = str.length;
    var sReturn = "";

    for (var i=0; i<len; i++){
        if ( (str.charAt(i) >= "0") && (str.charAt(i) <= "9") ){
            sReturn += str.charAt(i);
        }
    }
    return sReturn;
}


function addDriverDeduct(driverId,obj){
	
	var deductInfo = new Object();
	
	if($("#occurrenceDt").val() == ""){
		alert("날짜가 입력 되지 않았습니다.");
		return false;
	}else{
		deductInfo.occurrence_dt = $("#occurrenceDt").val();	
	}	
	if($("#item").val() == ""){
		alert("항목이 입력 되지 않았습니다.");
		return false;
	}else{
		deductInfo.item = $("#item").next().val();	
	}
	if($("#division").val() == ""){
		alert("구분이 입력 되지 않았습니다.");
		return false;
	}else{
		deductInfo.division = $("#division").next().val();	
	}
	if($("#amount").val() == ""){
		alert("금액이 입력 되지 않았습니다.");
		return false;
	}else{
		deductInfo.amount = getNumberOnly($("#amount").val());
	}
	deductInfo.etc = encodeURI($("#etc").val());
	
	if(confirm("공제 내역을 등록 하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : "/account/insert-driver-deduct.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				deductInfo : JSON.stringify({deductInfo : deductInfo}),
				driverId : "${driver.driver_id}",
				selectMonth : "${paramMap.selectMonth}"
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					document.location.reload();
				}else if(result == "E000"){
					alert("날짜가 잘못 입력 되었습니다.");
					return false;
				}else if(result == "0001"){
					alert("조회 하는데 실패 하였습니다.");
					return false;
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
}



function addLowViolation(obj){

	//작성되어야 하는 내용이 전부 작성 되었는지 확인하는 로직 추가.....

	
	
	if($("#carNum").val() == ""){
		alert("차량번호가 입력 되지 않았습니다.");
		return false;
	}

	if($("#driverId").val() == ""){
		if($("#phoneNum").val() == ""){
			alert("연락처가 입력 되지 않았습니다.");
			return false;
		}
	}
	
	if($("#occurrenceDt").val() == ""){
		alert("위반일자가 입력 되지 않았습니다.");
		return false;
	}
	if($("#occurrenceTime").val() == ""){
		alert("위반시간이 입력 되지 않았습니다.");
		return false;
	}
	if($("#placeAddress").val() == ""){
		alert("위반장소가 입력 되지 않았습니다.");
		return false;
	}
	if($("#summary").val() == ""){
		alert("위반내용이 입력 되지 않았습니다.");
		return false;
	}
	if($("#amount").val() == ""){
		alert("금액이 입력 되지 않았습니다.");
		return false;
	}
	if($("#endDt").val() == ""){
		alert("납부마감기간이 입력 되지 않았습니다.");
		return false;
	}
	
	if($("#bbsFile").val() == ""){

		if(confirm("파일이 등록되지 않았습니다. 파일 없이 등록 하시겠습니까?")){
			
		}else{
			alert("취소 되었습니다.");
			return false;
		}
	}

	if(confirm("해당 내용을 등록 하시겠습니까?")){
		$("#excelForm").attr("action","/carmanagement/insertLowViolation.do");
		$("#excelForm").submit();
	}
	
		
	/* $('#excelForm').ajaxForm({
		url: "/carmanagement/insertLowViolation.do",
		enctype: "multipart/form-data", 
	    type: "POST",
		dataType: "json",		
		data : {
			driverId : driverId
	    },
		success: function(data, response, status) {
			var status = data.resultCode;
			if(status == '0000'){

				alert();
				
			}else if(status == '1111'){
						
			}
					
		},
		error: function() {
			alert("이미지 등록중 오류가 발생하였습니다.");
		}                               
	}); */


	



	
}










function deleteLowViolation(lowViolationId,driverId,obj){
	
	
 	if(confirm("공제 내역을 삭제 하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : "/carmanagement/delete-lowViolation.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				driverId : "${driver.driver_id}",
				lowViolationId : lowViolationId
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("삭제 되었습니다.");
					document.location.reload();
				}else if(result == "0001"){
					alert("조회 하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
	
}


function viewElection(electionId){


	document.location.href = "/election/viewElection.do?electionId="+electionId;
	
}



function editLowViolation(lowViolationId,driverId,obj){
	
	$(obj).parent().parent().find(".normal").css("display","none");
	$(obj).parent().parent().find(".mod").css("display","");
	$(obj).css("display","none");
	$(obj).next().css("display","");
	
}


function setLowViolation(lowViolationId,driverId,obj){
	
	
	var lowViolationInfo = new Object();
	
	if($(obj).parent().parent().find(".mod").eq(0).find("input").val() == ""){
		alert("위반일자가 입력 되지 않았습니다.");
		return false;
	}else{
		lowViolationInfo.occurrence_dt = $(obj).parent().parent().find(".mod").eq(0).find("input").val();	
	}	
	if($(obj).parent().parent().find(".mod").eq(1).find("input").val() == ""){
		alert("위반시간이 입력 되지 않았습니다.");
		return false;
	}else{
		lowViolationInfo.occurrence_time = $(obj).parent().parent().find(".mod").eq(1).find("input").val();	
	}

	if($(obj).parent().parent().find(".mod").eq(2).find("input").val() == ""){
		alert("차량번호가 입력 되지 않았습니다.");
		return false;
	}else{
		lowViolationInfo.car_num = $(obj).parent().parent().find(".mod").eq(2).find("input").val();	
	}

	if($(obj).parent().parent().find(".mod").eq(3).find("input").val() == ""){
		alert("위반장소가 입력 되지 않았습니다.");
		return false;
	}else{
		lowViolationInfo.place_address = $(obj).parent().parent().find(".mod").eq(3).find("input").val();	
	}
	if($(obj).parent().parent().find(".mod").eq(4).find("input").val() == ""){
		alert("위반내용이 입력 되지 않았습니다.");
		return false;
	}else{
		lowViolationInfo.summary = $(obj).parent().parent().find(".mod").eq(4).find("input").val();	
	}

	if($(obj).parent().parent().find(".mod").eq(5).find("input").val() == ""){
		alert("금액이 입력 되지 않았습니다.");
		return false;
	}else{
		lowViolationInfo.amount = getNumberOnly($(obj).parent().parent().find(".mod").eq(5).find("input").val());
	}

	if($(obj).parent().parent().find(".mod").eq(6).find("input").val() == ""){
		alert("납부마감기간이 입력 되지 않았습니다.");
		return false;
	}else{
		lowViolationInfo.end_dt = $(obj).parent().parent().find(".mod").eq(6).find("input").val();	
	}

	
 	if(confirm("과태료 관리를 수정 하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : "/carmanagement/update-low-violation.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				lowViolationInfo : JSON.stringify({lowViolationInfo : lowViolationInfo}),
				driverId : "${driver.driver_id}",
				lowViolationId : lowViolationId
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("수정 되었습니다.");
					document.location.reload();
				}else if(result == "0001"){
					alert("조회 하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
	/* $(obj).parent().parent().find(".normal").css("display","");
	$(obj).parent().parent().find(".mod").css("display","none");
	$(obj).css("display","none");
	$(obj).prev().css("display",""); */
	
}



/* <td style="text-align:center;"><input type="text" placeholder="항목" name="item" id="item" onkeyup="javascript:setItem(this);"></td>
<td style="text-align:center;"><input type="text" placeholder="구분" name="division" id="division" onkeyup="javascript:setDivision(this);"></td> */

function setItem(obj){
	var val = $(obj).val();
	var num01;
    var num02;
    num01 = obj.value;
    num02 = num01.replace(rgx1,"");
    
    if(num02 != ""){
    	if(obj.value == "1"){
    		$(obj).next().val(obj.value);
    		num02 = "청구 내역 공제";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "2"){
    		$(obj).next().val(obj.value);
    		num02 = "대납 내역 공제";
    		obj.value =  num02;
    		$(obj).blur();
    	}else{
    		alert("항목 입력이 잘못 되었습니다. \r\n 1 : 청구 내역 공제, 2 : 대납 내역 공제");
    		obj.value =  "";
    	}
    }else{
    	alert("항목 입력이 잘못 되었습니다. \r\n 1 : 청구 내역 공제, 2 : 대납 내역 공제");
    	obj.value =  num02;
    }
    
    
	
}

function setDivision(obj){
	
	var val = $(obj).val();
	var num01;
    var num02;
    num01 = obj.value;
    num02 = num01.replace(rgx1,"");
    if(num02 != ""){
    	if(obj.value == "1"){
    		$(obj).next().val(obj.value);
    		num02 = "보험료";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "2"){
    		$(obj).next().val(obj.value);
    		num02 = "차량할부";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "3"){
    		$(obj).next().val(obj.value);
    		num02 = "과태료";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "4"){
    		$(obj).next().val(obj.value);
    		num02 = "미납통행료";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "5"){
    		$(obj).next().val(obj.value);
    		num02 = "사고대납금";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "6"){
    		$(obj).next().val(obj.value);
    		num02 = "기타";
    		obj.value =  num02;
    		$(obj).blur();
    	}else{
    		alert("구분 입력이 잘못 되었습니다. \r\n 1 : 보험료, 2 : 차량할부, 3 : 과태료, 4 : 미납통행료, 5 : 사고대납금, 6 : 기타");
    		obj.value =  "";
    	}
    }else{
    	alert("구문 입력이 잘못 되었습니다. \r\n 1 : 보험료, 2 : 차량할부, 3 : 과태료, 4 : 미납통행료, 5 : 사고대납금, 6 : 기타");
    	obj.value =  num02;
    }
	
}


function setDriver(driverId,driverName,carNum,phoneNum,obj){

	
	if(driverId != ""){

		$("#findDriver").val(driverName);
		
		$("#driverId").val(driverId);
		$("#driverName").val(driverName);
		$("#findCarNum").val(carNum);
		$("#carNum").val(carNum);
		$("#phoneNum").val(phoneNum);
		$("td[name=phoneNumParent]").css("display","none");
	}else{

		$("td[name=phoneNumParent]").css("display","");
		
		$("#driverId").val(driverId);
		$("#driverName").val($("#findDriver").val());
		$("#findCarNum").val(carNum);
		$("#carNum").val(carNum);
		$("#phoneNum").val(phoneNum);
	}

	$(obj).parent().parent().removeClass("active");
	
}


function setDriverByCarNum(driverId,driverName,carNum,phoneNum,obj){


	if(driverId != ""){

		$("#findDriver").val(driverName);
		
		$("#driverId").val(driverId);
		$("#driverName").val(driverName);
		$("#findCarNum").val(carNum);
		$("#carNum").val(carNum);
		$("#phoneNum").val(phoneNum);
		$("td[name=phoneNumParent]").css("display","none");
	}else{

		$("td[name=phoneNumParent]").css("display","");
		
		$("#driverId").val(driverId);
		//$("#driverName").val($("#findCarNum").val());
		//$("#findCarNum").val($("#findCarNum").val());
		$("#carNum").val($("#findCarNum").val());
		$("#phoneNum").val(phoneNum);
	}

	$(obj).parent().parent().removeClass("active");

	
}




var test;
function getAjaxData(val,obj,forPayment){

	test = obj;


	if(forPayment == "findDriver"){

			
		
			$.ajax({ 
				type: 'post' ,
				url : "/baseinfo/getDriverListAll.do" ,
				dataType : 'json' ,
				async : false,
				data : {
					driverName : val
				},
				success : function(data, textStatus, jqXHR)
				{
					
					var list = data.resultData;
					var result = "";
					//$("#searchResult").html("");
					$(obj).parent().find("div").html("");
					if(list.length > 0){
		       			for(var i=0; i<list.length; i++){
		       				result += '<div class="Wresult">';
		       				/* result += '<a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>'; */
		       				result += '<p class="result-title">기사정보</p>';
		       				var strJsonText = JSON.stringify(obj);
		    	   		    strJsonText = strJsonText.replace(/\"/gi, "'");
		       				result += '<a style="cursor:pointer;"   onclick="javascript:setDriver(\''+list[i].driver_id+'\',\''+list[i].driver_name+'\',\''+list[i].car_num+'\',\''+list[i].phone_num+'\',this);" class="result-sub"><span>'+list[i].driver_name+'</span></a>';
		       				result += '<p class="camp-type"><span>차량번호:</span> <span>'+list[i].car_num+'</span></p>';	
		       				/* result += '<p class="camp-type"><span>주소:</span> <span>'+list[i].address+'</span></p>'; */	
		       				result += '<p class="camp-id"><span>Tel:</span> <span>'+list[i].phone_num+'</span></p>';
		       				result += '</div>';
		       			}
		       			result += '<div class="Wresult">';
	       				/* result += '<a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>'; */
	       				/* result += '<a style="cursor:pointer; margin-bottom:10px;" class="result-title result-sub">기사정보 직접입력</a>'; */
	       				/* result += '<a style="cursor:pointer; margin-bottom:10px;" onclick="javascript:$(this).parent().parent().removeClass(\'active\');" class="result-title result-sub">기사정보 직접입력</a>'; */
	       				result += '<a style="cursor:pointer; margin-bottom:10px;" onclick="javascript:setDriver(\'\',\'\',\'\',\'\',this);" class="result-title result-sub">기사정보 직접입력</a>';
	       				var strJsonText = JSON.stringify(obj);
	    	   		    strJsonText = strJsonText.replace(/\"/gi, "'");
	       				/* result += '<a style="cursor:pointer;"   onclick="javascript:setAddressInfo();" class="result-sub"><span></span></a>';
	       				result += '<p class="camp-type"><span>차량번호:</span> <span></span></p>';	
	       				result += '<p class="camp-type"><span>주소:</span> <span></span></p>';	
	       				result += '<p class="camp-id"><span>Tel:</span> <span></span></p>'; */
	       				result += '</div>';

					}else{

						result += '<div class="Wresult">';
	       				/* result += '<a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>'; */
	       				/* result += '<p class="result-title">기사정보 직접입력</p>'; */
	       				
	       				result += '<a style="cursor:pointer; margin-bottom:10px;" onclick="javascript:setDriver(\'\',\'\',\'\',\'\',this);" class="result-title result-sub">기사정보 직접입력</a>';
	       				
	       				/* result += '<a style="cursor:pointer; margin-bottom:10px;" onclick="javascript:$(this).parent().parent().removeClass(\'active\');" class="result-title result-sub">기사정보 직접입력</a>'; */
	       				var strJsonText = JSON.stringify(obj);
	    	   		    strJsonText = strJsonText.replace(/\"/gi, "'");
	       				result += '<a style="cursor:pointer;"   onclick="javascript:setAddressInfo();" class="result-sub"><span></span></a>';
	       				/* result += '<p class="camp-type"><span>차량번호:</span> <span></span></p>';	
	       				result += '<p class="camp-type"><span>주소:</span> <span></span></p>';	
	       				result += '<p class="camp-id"><span>Tel:</span> <span></span></p>'; */
	       				result += '</div>';
						//$(test).parent().find("div").removeClass('active');
					}
					
					//$("#searchResult").html(result);
					$(obj).parent().find("div").html(result);
					
				} ,
				error : function(xhRequest, ErrorText, thrownError) {
				}
			});



	}else if(forPayment == "findCarNum"){

		$.ajax({ 
			type: 'post' ,
			url : "/baseinfo/getDriverListAllByCarNum.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				carNum : val
			},
			success : function(data, textStatus, jqXHR)
			{
				
				var list = data.resultData;
				var result = "";
				//$("#searchResult").html("");
				$(obj).parent().find("div").html("");
				if(list.length > 0){
	       			for(var i=0; i<list.length; i++){
	       				result += '<div class="Wresult">';
	       				/* result += '<a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>'; */
	       				result += '<p class="result-title">기사정보</p>';
	       				var strJsonText = JSON.stringify(obj);
	    	   		    strJsonText = strJsonText.replace(/\"/gi, "'");
	       				result += '<a style="cursor:pointer;"   onclick="javascript:setDriverByCarNum(\''+list[i].driver_id+'\',\''+list[i].driver_name+'\',\''+list[i].car_num+'\',\''+list[i].phone_num+'\',this);" class="result-sub"><span>'+list[i].driver_name+'</span></a>';
	       				result += '<p class="camp-type"><span>차량번호:</span> <span>'+list[i].car_num+'</span></p>';	
	       				/* result += '<p class="camp-type"><span>주소:</span> <span>'+list[i].address+'</span></p>'; */	
	       				result += '<p class="camp-id"><span>Tel:</span> <span>'+list[i].phone_num+'</span></p>';
	       				result += '</div>';
	       			}
	       			result += '<div class="Wresult">';
       				/* result += '<a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>'; */
       				/* result += '<a style="cursor:pointer; margin-bottom:10px;" class="result-title result-sub">기사정보 직접입력</a>'; */
       				/* result += '<a style="cursor:pointer; margin-bottom:10px;" onclick="javascript:$(this).parent().parent().removeClass(\'active\');" class="result-title result-sub">기사정보 직접입력</a>'; */
       				result += '<a style="cursor:pointer; margin-bottom:10px;" onclick="javascript:setDriverByCarNum(\'\',\'\',\'\',\'\',this);" class="result-title result-sub">기사정보 직접입력</a>';
       				var strJsonText = JSON.stringify(obj);
    	   		    strJsonText = strJsonText.replace(/\"/gi, "'");
       				/* result += '<a style="cursor:pointer;"   onclick="javascript:setAddressInfo();" class="result-sub"><span></span></a>';
       				result += '<p class="camp-type"><span>차량번호:</span> <span></span></p>';	
       				result += '<p class="camp-type"><span>주소:</span> <span></span></p>';	
       				result += '<p class="camp-id"><span>Tel:</span> <span></span></p>'; */
       				result += '</div>';

				}else{

					result += '<div class="Wresult">';
       				/* result += '<a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>'; */
       				/* result += '<p class="result-title">기사정보 직접입력</p>'; */
       				
       				result += '<a style="cursor:pointer; margin-bottom:10px;" onclick="javascript:setDriverByCarNum(\'\',\'\',\'\',\'\',this);" class="result-title result-sub">기사정보 직접입력</a>';
       				
       				/* result += '<a style="cursor:pointer; margin-bottom:10px;" onclick="javascript:$(this).parent().parent().removeClass(\'active\');" class="result-title result-sub">기사정보 직접입력</a>'; */
       				var strJsonText = JSON.stringify(obj);
    	   		    strJsonText = strJsonText.replace(/\"/gi, "'");
       				result += '<a style="cursor:pointer;"   onclick="javascript:setAddressInfo();" class="result-sub"><span></span></a>';
       				/* result += '<p class="camp-type"><span>차량번호:</span> <span></span></p>';	
       				result += '<p class="camp-type"><span>주소:</span> <span></span></p>';	
       				result += '<p class="camp-id"><span>Tel:</span> <span></span></p>'; */
       				result += '</div>';
					//$(test).parent().find("div").removeClass('active');
				}
				
				//$("#searchResult").html(result);
				$(obj).parent().find("div").html(result);
				
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});




	}









	
}




function addElection(){
	document.location.href = "/election/add-election.do";
}


</script>

		<!-- <section class="side-nav"> -->
            <!-- <ul>
                <li class="active"><a href="/baseinfo/employee.do">직원관리</a></li>
                <li class=""><a href="/baseinfo/driver.do">기사관리</a></li>
                <li class=""><a href="/baseinfo/customer.do">거래처관리</a></li>
                <li class=""><a href="/baseinfo/company.do">회사정보관리</a></li>
            </ul> -->
        <!-- </section> -->

        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">온라인 투표</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">투표 목록</a></li>
                </ul>
            </div>

            <div class="up-dl clearfix header-search">
            
            	<table>
                    <tbody>
                        <tr>
                        	<td>
                        		<c:if test="${user.emp_id == 'hk0000' || user.emp_id == 'hk0001' || user.emp_id == 'hk0002' || user.emp_id == 'hk0003'}">
                                	<input type="button" id="btn-search" value="선거등록"  onclick="javascript:addElection();" class="btn-primary">
                                </c:if>
                            </td>
                            <td style="width: 145px;">
                                <div class="select-con">
							        <select class="dropdown" name="searchType" id="searchType">
							        	<option value="" <c:if test='${paramMap.searchType eq "" }'> selected="selected"</c:if>>조회구분을 선택 하세요.</option>
							            <option value="00" <c:if test='${paramMap.searchType eq "00" }'> selected="selected"</c:if>>선거명</option>
							            <%-- <option value="01" <c:if test='${paramMap.searchType eq "01" }'> selected="selected"</c:if>>대표자명</option> --%>
							        </select>
							        <span></span>
							    </div>
                            </td>
                            <td style="width: 190px;">
                                <input type="text" id="searchWord" name="searchWord" value="${paramMap.searchWord}"   onkeypress="if(event.keyCode=='13') search();">
                            </td>
                            <td>
                                <input type="button" id="btn-search" value="검색" class="btn-primary" onclick="javascript:search();">
                            </td>
                            <td>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </section>

            <section class="bottom-table" style="width:1600px; margin-left:290px;">
            	 <!--<div style="color:#8B0000;">*항목코드 1 : 청구내역공제,2 : 대납내역공제</div>
            	 <div style="color:#8B0000;">*구분코드 1 : 보험료,2 : 차량할부,3 : 과태료,4 : 미납통행료,5 : 사고대납금,6 : 기타</div> -->
                
                
                <table class="article-table" style="margin-top:15px">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <!-- <td style="text-align:center;">기사아이디</td> -->
                            <td style="text-align:center; width:50px;"><input type="checkbox" id="checkAll" onclick="javascript:checkAll();" ></td>
                            <td style="text-align:center;">선거제목</td>
                            <td style="text-align:center;">선거내용</td>
                            <td style="text-align:center;">참여구분</td>
                            <td style="text-align:center;">후보구분</td>
                            <td style="text-align:center;">선거시작일</td>
                            <td style="text-align:center;">선거종료일</td>
                            <td style="text-align:center;">참여인원</td>
                            <td style="text-align:center;">등록자</td>
                        </tr>
                    </thead>
                    <tbody id="">
                    	<c:forEach var="data" items="${listData}" varStatus="status">
							<tr class="ui-state-default" style=" cursor:pointer;" onclick="javascript:viewElection('${data.election_id}');"> 
	                            <td style="text-align:center;"><input type="checkbox" name="forDeductStatus" electionId="${data.election_id}"></td>
	                            <td style="text-align:center;">${data.election_title}</td>
	                            <td style="text-align:center;">${data.election_content}</td>
	                            <td style="text-align:center;">
	                            	<c:if test='${data.distribute_division eq "A" }'>
	                            		전체
	                            	</c:if>
	                            	<c:if test='${data.distribute_division eq "O" }'>
	                            		사무실 전체
	                            	</c:if>
	                            	<c:if test='${data.distribute_division eq "D" }'>
	                            		기사 전체
	                            	</c:if>
	                            	<c:if test='${data.distribute_division eq "S" }'>
	                            		셀프
	                            	</c:if>
	                            	<c:if test='${data.distribute_division eq "C" }'>
	                            		캐리어
	                            	</c:if>
	                            </td>
	                            <td style="text-align:center;">
	                            	<c:if test='${data.candidate_division eq "A" }'>
	                            		전체
	                            	</c:if>
	                            	<c:if test='${data.candidate_division eq "O" }'>
	                            		사무실 전체
	                            	</c:if>
	                            	<c:if test='${data.candidate_division eq "D" }'>
	                            		기사 전체
	                            	</c:if>
	                            	<c:if test='${data.candidate_division eq "S" }'>
	                            		셀프
	                            	</c:if>
	                            	<c:if test='${data.candidate_division eq "C" }'>
	                            		캐리어
	                            	</c:if>
	                            </td>
	                            <td style="text-align:center;">${data.start_dt}</td>
	                            <td style="text-align:center;">${data.end_dt}</td>
	                            <td style="text-align:center;">${data.join_count}</td>
	                            <td style="text-align:center;">${data.register_name}</td>
                        	</tr>
						</c:forEach>
                        
                    </tbody>
                </table>
                
                
                <div class="table-pagination text-center">
                    <ul class="pagination">
                        <!-- <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="curr-page"><a href="#">1</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li> -->
                        <html:paging uri="/election/getElectionList.do" forGroup="&occurrenceDt=${paramMap.occurrenceDt}" />
                    </ul>
                </div>
                <div class="confirmation">
                    <div class="confirm">
                        <!-- <a href="/baseinfo/add-employee.do">직원등록</a> -->
                    </div>
                </div>
                
            </section>
     
