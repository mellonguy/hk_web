<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%

%>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script type="text/javascript">
$(document).ready(function(){

});

function addElection(){

		
		if($("#electionTitle").val() == ""){
			alert("선거명이 입력되지 않았습니다.");
			return false;
		}

		if($("#startDt").val() == ""){
			alert("선거 시작일이 입력되지 않았습니다.");
			return false;
		}

		if($("#distributeDivision").val() == ""){
			alert("참여구분이 입력되지 않았습니다.");
			return false;
		}
		
		if($("#endDt").val() == ""){
			alert("선거 종료일이 입력되지 않았습니다.");
			return false;
		}

		if($("#candidateDivision").val() == ""){
			alert("후보구분이 입력되지 않았습니다.");
			return false;
		}


		if($("#electionContent").val() == ""){
			alert("선거내용이 입력되지 않았습니다.");
			return false;
		}

		
	 	if(confirm("등록 하시겠습니까?")){
			$("#insertForm").attr("action","/election/insert-election.do");
			$("#insertForm").submit();	
		}
	
}

function jusoSearch(where,obj){
	
	new daum.Postcode({
	    oncomplete: function(data) {
	        // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

            // 각 주소의 노출 규칙에 따라 주소를 조합한다.
            // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
            var fullAddr = ''; // 최종 주소 변수
            var extraAddr = ''; // 조합형 주소 변수

            // 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
            if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                fullAddr = data.roadAddress;

            } else { // 사용자가 지번 주소를 선택했을 경우(J)
                fullAddr = data.jibunAddress;
            }

            // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
            if(data.userSelectedType === 'R'){
                //법정동명이 있을 경우 추가한다.
                if(data.bname !== ''){
                    extraAddr += data.bname;
                }
                // 건물명이 있을 경우 추가한다.
                if(data.buildingName !== ''){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
                fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
            }

            // 우편번호와 주소 정보를 해당 필드에 넣는다.
            //document.getElementById('sample6_postcode').value = data.zonecode; //5자리 새우편번호 사용
         	
            //$(obj).parent().children().eq(0).val(fullAddr);
            //$(obj).parent().parent().children().eq(0).children().val(data.sido);

            //alert(JSON.stringify(data));
            
            $(obj).val(fullAddr);
                        
            // 커서를 상세주소 필드로 이동한다.
            //document.getElementById('sample6_address2').focus();
        }
	    
	}).open();	
	
}

function addAccount(){
	
	var result = "";
	result += '<tr><td style="display:none;"></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="은행명" name="bankName"></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="예금주" name="depositor"></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="계좌번호" name="accountNumber"></td>';
	result += '<td class="widthAuto" style=""><input type="text" placeholder="계좌명칭" name="accountName"></td>';
	result += '<td class="widthAuto" style=""><input style="display:inline; float:right; border:2px solid #eee;" type="button" onclick="javascript:deleteAccount(this);" value="삭제" class="btn-normal"></td></tr>';
	
	$("#rowspanCnt").attr("rowspan",Number($("#rowspanCnt").attr("rowspan"))+1);
	$("#addLocation").append(result);

}


function deleteAccount(obj){
	
	$("#rowspanCnt").attr("rowspan",Number($("#rowspanCnt").attr("rowspan"))-1);
	$(obj).parent().parent().remove();
	
}



</script>

	<!-- <section class="side-nav">
            <ul>
                <li class=""><a href="/baseinfo/employee.do">직원관리</a></li>
                <li class=""><a href="/baseinfo/driver.do">기사관리</a></li>
                <li class=""><a href="/baseinfo/customer.do">거래처관리</a></li>
                <li class="active"><a href="/baseinfo/company.do">회사정보관리</a></li>
            </ul>
        </section> -->

 <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">선거관리</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">선거 등록</a></li>
                </ul>
            </div>

            <div class="up-dl clearfix header-search">
                 
            </div>
<!-- 
            <div class="dispatch-btn-container">
               
                <div class="dispatch-btn">
                    <i class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div>
            </div> -->
        </section>

        <div class="dispatch-wrapper">
           <section class="dispatch-bottom-content active">
               <form id="insertForm" action="/baseinfo/insert-company.do"  method="post" enctype="multipart/form-data">
               		<h3 style="font-size: 20px; text-align: center; margin-bottom:20px; font-weight: 700; font-family: NotoSansBold,sans-serif;">선거 등록</h3>
               		<input type="hidden" name="accountInfo" id="accountInfo">
               <div class="form-con clearfix">
                   <div class="column" style="margin-bottom:5px">
                   		<table>
                            <tbody>
                                <tr>
                                    <td>선거명<span style="color:#F00;">&nbsp;*</span></td>
                                    <td class="widthAuto" style="">
                                        <input type="text" placeholder="선거명을 입력 하세요." name="electionTitle" id="electionTitle">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                       <table>
                            <tbody>
                                <tr>
                                    <td>참여구분<span style="color:#F00;">&nbsp;*</span></td>
                                    <td class="widthAuto" style="">
		                                <div class="select-con">
									        <select class="dropdown" name="distributeDivision" id="distributeDivision">
									        	<option value="">참여구분을 선택 하세요.</option>
									            <option value="A">전체</option>
									            <option value="O">사무실전체</option>
									            <option value="D">기사전체</option>
									            <option value="S">셀프</option>
									            <option value="C">캐리어</option>
									        </select>
									        <span></span>
									    </div>
		                            </td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
                            <tbody>
                                <tr>
                                    <td>후보구분<span style="color:#F00;">&nbsp;*</span></td>
                                    <td class="widthAuto" style="">
		                                <div class="select-con">
									        <select class="dropdown" name="candidateDivision" id="candidateDivision">
									        	<option value="">후보구분을 선택 하세요.</option>
									            <option value="A">전체</option>
									            <option value="O">사무실전체</option>
									            <option value="D">기사전체</option>
									            <option value="S">셀프</option>
									            <option value="C">캐리어</option>
									        </select>
									        <span></span>
									    </div>
		                            </td>
                                </tr>
                            </tbody>
                        </table>
                        
                   </div>
                   <div class="column">
                       <table>
                            <tbody>
                                <tr>
                                    <td>선거시작일<span style="color:#F00;">&nbsp;*</span></td>
                                    <td class="widthAuto" style="">
                                        <input type="text" class="datepick" placeholder="선거시작일을 입력하세요" name="startDt" id="startDt" autocomplete="off">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
                            <tbody>
                                <tr>
                                    <td>선거종료일<span style="color:#F00;">&nbsp;*</span></td>
                                    <td class="widthAuto" style="">
                                        <input type="text" class="datepick" placeholder="선거종료일을 입력 하세요" name="endDt" id="endDt" autocomplete="off">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- <table>
                            <tbody>
                                <tr>
                                    <td>개업년월일</td>
                                    <td class="widthAuto" style="">
                                        <input type="text" class="datepick" placeholder="개업년월일" name="setUpDate" id="setUpDate">
                                    </td>
                                </tr>
                            </tbody>
                        </table> -->
                        
                   </div>
                   
                   <table>
                       <tbody>
                           <tr>
                               <td>선거 내용<span style="color:#F00;">&nbsp;*</span></td>
                               <td class="widthAuto" style="height:200px;">
                                   <textarea style="height:180px;" placeholder="선거내용을 입력 하세요" name="electionContent" id="electionContent" autocomplete="off"></textarea>
                               </td>
                           </tr>
                       </tbody>
                   </table>
                        
	                        
	                        
	                          
                    <!-- <table>
                        <tbody>
                                <tr>
                                    <td>계좌번호</td>
                                    <td class="widthAuto" style="">
                                        <input type="text" placeholder="계좌번호" name="" id="">
                                    </td>
                                    <td>
                                        <input class="btn-edit" type="button" value="검색">
                                    </td>
                                    
                                </tr>
                            
                        </tbody>
                    </table> -->
               </div>
               
                <div class="confirmation">
                    <div class="confirm">
                        <input type="button" value="등록하기"  onclick="javascript:addElection();">
                    </div>
                    <div class="cancel">
                        <input type="button" value="취소" onclick="javascript:history.go(-1); return false;">
                    </div>
                </div>
                    <!-- <script src="js/vendor/bootstrap-datepicker.min.js"></script> -->
                </form>
                <!-- <div class="table-pagination text-center">
                    <ul class="pagination">
                        <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="curr-page"><a href="#">1</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
                    </ul>
                </div> -->
            </section>
        </div>
