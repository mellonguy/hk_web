<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SYSTEM </title>
<c:set var="prefix" value=""/>
<script type="text/javascript">
<c:if test="${!empty scriptMsg}">
alert('${scriptMsg}');
</c:if>

/* <c:if test="${!empty redirectUrl}">
location.href = '${pageContext.request.contextPath}${prefix}${redirectUrl}';
</c:if> */
<c:choose>
<c:when test="${fn:contains(redirectUrl, '/account/viewMonthDriverDeduct.do' )}">
	opener.window.location.reload();
  self.close();
</c:when>

<c:otherwise>
  location.href = '${pageContext.request.contextPath}${prefix}${redirectUrl}';
</c:otherwise>
</c:choose>

</script>
</head>
<body></body>
</html>
