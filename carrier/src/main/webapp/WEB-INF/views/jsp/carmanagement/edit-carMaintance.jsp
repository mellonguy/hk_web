<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%

%>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script type="text/javascript">
$(document).ready(function(){


 	<c:if test ='${driverMap.driver_kind eq "03"}'>
 		$("#forTest").css("display","");
	</c:if>

});

function updateCarDetail(){


	if($("#carNum").val() == ""){
		alert("차량번호가 입력되지 않았습니다.");
		return false;
	}
	if($("#carIdNum").val() == ""){
		alert("차대번호가 입력되지 않았습니다.");
		return false;
	}
	
	if($("#driverName").val() == ""){
		alert("기사명이 입력되지 않았습니다.");
		return false;
	}

	if($("#carKind").val() == ""){
		alert("차종이 입력되지 않았습니다.");
		return false;
	}	

	if($("#carModelYear").val() == ""){
		alert("연식이 입력되지 않았습니다.");
		return false;
	}	
	if($("#carWeight").val() == ""){
		alert("총중량이 입력되지 않았습니다.");
		return false;
	}

	if($("#carOwnerName").val() == ""){
		alert("차량번호 소유자가 입력되지 않았습니다.");
		return false;
	}



		if($("#searchYN").val() == "N"){
			if(confirm("기존에 등록되지 않는 기사입니다 새로운 기사명으로 등록하시겠습니까?")){
				if(confirm("수정 하시겠습니까?")){
					$("#carDetailForm").attr("action","/carmanagement/update-carDetail.do");
					$("#carDetailForm").submit();	
				}
			}
		}else{
	
			if(confirm("수정 하시겠습니까?")){
				$("#carDetailForm").attr("action","/carmanagement/update-carDetail.do");
				$("#carDetailForm").submit();	
			}
			
		}
	}
		




function getNumberOnly (str) {			//저장할때는 콤마 제거 후 저장한다.
    var len = str.length;
    var sReturn = "";

    for (var i=0; i<len; i++){
        if ( (str.charAt(i) >= "0") && (str.charAt(i) <= "9") ){
            sReturn += str.charAt(i);
        }
    }
    return sReturn;
}




function fileChange(){
	
	var file = document.getElementById("upload");
 	var result = "";
 	for(var i = 0; i < file.files.length; i++){
		var name = file.files[i].name;
		result += name + "\t";
	}
	
	//$("#file_info").val(result);
	$("#forFile").css("display","none");
	$("#upload").css("display","inline");
	
	

}

function driverfileChange(){
	
	var file = document.getElementById("driverupload");
 	var result = "";
 	for(var i = 0; i < file.files.length; i++){
		var name = file.files[i].name;
		result += name + "\t";
	}
	
	//$("#file_info").val(result);
	$("#driverforFile").css("display","none");
	$("#driverupload").css("display","inline");
	
	

}

function driverSealfileChange(){
	
	var file = document.getElementById("driverSealupload");
 	var result = "";
 	for(var i = 0; i < file.files.length; i++){
		var name = file.files[i].name;
		result += name + "\t";
	}
	
	//$("#file_info").val(result);
	$("#driverforSealFile").css("display","none");
	$("#driverSealupload").css("display","inline");
	
	

}




function fileModConfirm(cnt){
	
	if(confirm("현재"+cnt+" 개의 파일이 첨부되어 있습니다.\r\n파일 수정시 기존의 파일이 삭제 됩니다. \r\n정말 수정 하시겠습니까?")){
		$("#upload").trigger("click");
	}
	
}


function driverfileModConfirm(cnt){
	
	if(confirm("현재"+cnt+" 개의 파일이 첨부되어 있습니다.\r\n파일 수정시 기존의 파일이 삭제 됩니다. \r\n정말 수정 하시겠습니까?")){
		$("#driverupload").trigger("click");
	}
	
}


function driverSealfileModConfirm(cnt){
	
	if(confirm("현재"+cnt+" 개의 파일이 첨부되어 있습니다.\r\n파일 수정시 기존의 파일이 삭제 됩니다. \r\n정말 수정 하시겠습니까?")){
		$("#driverSealupload").trigger("click");
	}
	
}



var searchYN ="N";


function goSearchDriverName(){

	var resultStr = ""; 



resultStr += '<option value="00" selected=selected>'+"기사명을 선택해주세요"+'</option>'	

	$.ajax({ 
		type: 'post' ,
		url : "/carmanagement/searchForDriverNameList.do" ,
		dataType : 'json' ,
		data : {
			driverName : $("#driverName").val()
		},
		success : function(data, textStatus, jqXHR)
		{
			var result = data.resultCode;
			var resultData = data.resultData;
			if(result == "0000"){

			var driverId = resultData.driver_id;
			
			 if(resultData != ""){

					for(var i = 0; i < resultData.length; i++){
					var driverName = resultData[i].driver_name;
					var driverId = resultData[i].driver_id;


						 resultStr += '<option value="01" >'+driverName+'</option>'

						  $("#driverId").val(driverId);

						 searchYN ="Y";
						 
						}	

				}
				$("#chooseDriver").append(resultStr);
			 
				
			}else if(result != "0000"){



				
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
}




function goShowTest(obj){

	
//alert( $(obj).val() );


		if($(obj).val() == "03"){
			
			$("#forTest").css("display","");
		}else{

			$("#forTest").css("display","none");
				
		}


	
}


function insertCarDetail(){



	if($("#carIdNum").val() == ""){
		alert("차대번호가 입력되지 않았습니다.");
		return false;
	}	

	if($("#carNum").val() == ""){
		alert("차량번호가 입력되지 않았습니다.");
		return false;
	}	

	if($("#driverName").val() == ""){
		alert("기사명이 입력되지 않았습니다.");
		return false;
	}
	
	if($("#carKind").val() == ""){
		alert("차종이 입력되지 않았습니다.");
		return false;
	}	

	if($("#carModelYear").val() == ""){
		alert("연식이 입력되지 않았습니다.");
		return false;
	}	
	if($("#carWeight").val() == ""){
		alert("총중량이 입력되지 않았습니다.");
		return false;
	}
	
	if($("#carOwnerName").val() == ""){
		alert("차량번호 소유자가 입력되지 않았습니다.");
		return false;
	}


	if($("#searchYN").val() == "N"){
		if(confirm("기존에 등록되지 않는 기사입니다 새로운 기사명으로 등록하시겠습니까?")){
			if(confirm("해당 내용을 등록 하시겠습니까?")){
				$("#carDetailForm").attr("action","/carmanagement/insert-carDetail.do");
				$("#carDetailForm").submit();
			
				}
		}
	}else{

		if(confirm("해당 내용을 등록 하시겠습니까?")){
			$("#carDetailForm").attr("action","/carmanagement/insert-carDetail.do");
			$("#carDetailForm").submit();
		
		
	}
		}

//	$("#carDetailForm").submit()






}



</script>
		<!-- <section class="side-nav">
            <ul>
                <li class=""><a href="/baseinfo/employee.do">직원관리</a></li>
                <li class="active"><a href="/baseinfo/driver.do">기사관리</a></li>
                <li class=""><a href="/baseinfo/customer.do">거래처관리</a></li>
                <li class=""><a href="/baseinfo/company.do">회사정보관리</a></li>
            </ul>
        </section> -->
     <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">HOME</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">차량 정보</a></li>
                </ul>
            </div>

            <div class="up-dl clearfix header-search">
                 <table>
                    <tbody>
                        <tr>
                            <td style="width: 145px;">
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">선택바
                                    <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">HTML</a></li>
                                        <li><a href="#">CSS</a></li>
                                        <li><a href="#">JavaScript</a></li>
                                    </ul>
                                </div>
                            </td>
                            <td style="width: 190px;">
                                <form action="">
                                    <input type="text" class="search-box">
                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
                                    <div class="search-result">
                                        <!-- No result -->
                                        <div class="no-result d-table">
                                            <div class="d-tbc">
                                                <i class="fa fa-exclamation-triangle fa-3x" aria-hidden="true"></i>
                                                <span>No results have been found.</span>
                                            </div>
                                        </div>

                                        <!-- Result type1 -->
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Campaigns</p>
                                            <a href="" class="result-sub"><span>네오퓨쳐</span></a>
                                            <p class="camp-type"><span>Campaign type:</span> <span>Powerlink</span></p>
                                            <p class="camp-id"><span>Campaign ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                        <!-- Result type2 -->
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Ad groups</p>
                                            <a href="" class="result-sub"><span>네오퓨쳐</span>_#0007</a>
                                            <p class="camp-type"><span>Campaign type:</span> <span>Powerlink</span></p>
                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
                                            <p class="adgroup-id"><span>Ad group ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                        <!-- Result type3 -->
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Ads - Powerlink</p>
                                            <div class="result-box">
                                                <p class="title">네오퓨쳐</p>
                                                <p class="content">일반 홈페이지에서 신개념 융복합 멀티미디어 홈피이지 제작, 견적상담 환영</p>

                                                <ul>
                                                    <li>View URL: http://www.neofss.com</li>
                                                    <li>Linked URL: http://www.neofss.com</li>
                                                </ul>
                                            </div>
                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
                                            <p class="adgroup"><span>Ad group:</span> <span class="red">네오퓨쳐</span>_#0003</p>
                                            <p class="adgroup-id"><span>Ad ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                        <!-- Result type4 -->
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Ads - PowerContents</p>
                                            <div class="result-box w-img">
                                                <div class="d-table">
                                                    <div class="d-tbc">
                                                        <img src="/img/ads-img.png" alt="">
                                                    </div>
                                                    <div class="d-tbc">
                                                        <p class="title">홈페이지제작 이러면 망한다!</p>
                                                        <p class="content">홈페이지제작! 대충 업체에 맡기면 된다는 생각으로 접근하면, 십중팔구는 실패하는 이유와 홈페이지 제작에 관한 최소한의 지식은 가지고 업체를 선정해야 돈과 시간을 낭비하는 일을 줄일수 있다.</p>
                                                    </div>
                                                </div>
                                                <ul>
                                                    <li>Content created: 2017-12-15</li>
                                                    <li>Brand Name: 네오퓨쳐</li>
                                                    <li>View URL: http://blog.naver.com/marujeen</li>
                                                    <li>Linked URL: http://blog.naver.com/marujeen</li>
                                                </ul>
                                            </div>
                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
                                            <p class="adgroup"><span>Ad group:</span> <span class="red">네오퓨쳐</span>_#0003</p>
                                            <p class="adgroup-id"><span>Ad ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                    </div>
                                </form>
                            </td>
                            <td>
                                <input type="button" id="btn-search" value="검색" class="btn-primary">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
<!-- 
            <div class="dispatch-btn-container">
               
                <div class="dispatch-btn">
                    <i class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div>
            </div> -->
        </section>

        <div class="dispatch-wrapper">

            <section class="dispatch-bottom-content active" style=" margin:auto;">
               <form id="carDetailForm" action="/carmanagement/insert-carDetail.do" method="post" enctype="multipart/form-data" >
                   <h3 style="font-size: 20px; text-align: center; margin-bottom:20px; font-weight: 700; font-family: NotoSansBold,sans-serif;">차량 정보</h3>
               		<input type="hidden"name="driverStatus" id="driverStatus" value="${driverMap.driver_status}">
               		      <c:if test="${paramMap.code eq 'A'}">
               					<input type="hidden"name="driverId" id="driverId" value="">
               		</c:if>
               		     <c:if test="${paramMap.code ne 'A'}">
               					<input type="hidden"name="driverId" id="driverId" value="${selectCarDetailEMap.driver_id}">
               		</c:if>
                   <div class="form-con clearfix" style="max-width:1528px;">
                	<div class="column-sub" style="margin-bottom:5px">
                 
	                </div>
	                
	                <div class="column-sub">
	                
	                <table>
	                    <tbody>
	                        <tr>
	                            <td >차대번호<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                               <c:if test="${paramMap.code eq 'A'}">
	                               	 <input type="text" placeholder="차대번호" name="carIdNum" id="carIdNum" value="">
	                               </c:if>	 
	                               <c:if test="${paramMap.code ne 'A'}">
	                              	  <input type="text" placeholder="차대번호" name="carIdNum" id="carIdNum" value="${selectCarDetailEMap.car_id_num}">
	                              	  <input type="hidden"name="carDetailId" id="carDetailId" value="${selectCarDetailEMap.car_detail_id}">
	                               </c:if>
	                                
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                           <td >차량번호</td>
	                            <td class="widthAuto" style="">
	                             <c:if test="${paramMap.code eq 'A'}">
	                                <input type="text" placeholder="차량번호" name="carNum" id="carNum" value="">
	                           </c:if>
	                             <c:if test="${paramMap.code ne 'A'}">
	                            	 <input type="text" placeholder="차량번호" name="carNum" id="carNum" value="${selectCarDetailEMap.car_num}">
	                           	</c:if>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                    <table>
	                    <tbody>
	                        <tr>
	                           <td >기사명</td>
	                            <td class="widthAuto" style="">
	                                <c:if test="${paramMap.code eq 'A'}">
	                              <input type="text" placeholder="기사명" name="driverName" id="driverName" value="" style="width: 310px; display:inline;">
	                                </c:if>
	                                         <c:if test="${paramMap.code ne 'A'}">
	                                         	 <input type="text" placeholder="기사명" name="driverName" id="driverName" value="${selectCarDetailEMap.driver_name}" style="width: 310px; display:inline;">
	                                         	</c:if>
	                            <!--  
	                             <c:if test="${paramMap.code eq 'A'}">
	                                <input type="text" placeholder="기사명" name="driverName" id="driverName" value="" style="width: 235px; display:inline;">
	                               		<button type="button" style="display:inline;" class="btn btn-primary" onclick="javasciript:goSearchDriverName();">조회</button>
	                               		     <select id ='chooseDriver'  name=""  onchange ="document.getElementById('driverName').value = this.options[this.selectedIndex].text"  style ="width: 300px; padding: .8em .5em; height: 43px; border: 1px solid #999; font-family: inherit; background: no-repeat 95% 50%; border-radius: 0px; -webkit-appearance: none; -moz-appearance: none; appearance: none;">
                    							 <option value=""  ></option>
                    						 </select>
	                           </c:if>
	                             <c:if test="${paramMap.code ne 'A'}">
	                            	 <input type="text" placeholder="기사명" name="driverName" id="driverName" value="${selectCarDetailEMap.driver_name}" style="width: 235px; display:inline;">
	                           				<button type="button" style="display:inline;" class="btn btn-primary" onclick="javasciript:goSearchDriverName();">조회</button>
	                               		     <select id ='chooseDriver'  name=""  onchange ="document.getElementById('driverName').value = this.options[this.selectedIndex].text"  style ="width: 300px; padding: .8em .5em; height: 43px; border: 1px solid #999; font-family: inherit; background: no-repeat 95% 50%; border-radius: 0px; -webkit-appearance: none; -moz-appearance: none; appearance: none;">
                    							 <option value=""  ></option>
                    						 </select>
	                           	</c:if>
	                           	-->
	                           	
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>차종</td>
	                            <td class="widthAuto" style="">
	                             <c:if test="${paramMap.code eq 'A'}">    
	                                <input type="text" placeholder="차종" name="carKind" id="carKind" value="">
	                           	</c:if>
	                             <c:if test="${paramMap.code ne 'A'}">
	                             <input type="text" placeholder="차종" name="carKind" id="carKind" value="${selectCarDetailEMap.car_kind}">
	                             </c:if> 
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>연식</td>
	                            <td class="widthAuto" style="">
	                             <c:if test="${paramMap.code eq 'A'}">    
	                                <input type="text" placeholder="연식" name="carModelYear" id="carModelYear" value="">
	                           </c:if> 
	                            <c:if test="${paramMap.code ne 'A'}">
	                            	<input type="text" placeholder="연식" name="carModelYear" id="carModelYear" value="${selectCarDetailEMap.car_model_year}">
	                            </c:if>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                
	                <table>
		                <tbody>
			                <tr>
				                <td>최대 적재량</td>
				                <td class="widthAuto" style="">
					            <c:if test="${paramMap.code eq 'A'}">    
					                 <input type="text" placeholder="총중량" name="carWeight" id="carWeight" value="">
				                </c:if>
				                <c:if test="${paramMap.code ne 'A'}">
				                	<input type="text" placeholder="총중량" name="carWeight" id="carWeight" value="${selectCarDetailEMap.car_weight}">
				                </c:if>
				                </td>
			                </tr>
		                </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>차량번호 소유자</td>
	                            <td class="widthAuto" style="">
	               		        <c:if test="${paramMap.code eq 'A'}">  
	               		           <input type="text" placeholder="차량번호 소유자" name="carOwnerName" id="carOwnerName" value="">
	                          	</c:if>
	                          	<c:if test="${paramMap.code ne 'A'}">
	                          		<input type="text" placeholder="차량번호 소유자" name="carOwnerName" id="carOwnerName" value="${selectCarDetailEMap.car_owner_name}">
	                          	</c:if>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	         
	                <table>
	                            <tbody>
	                                <tr>
	                                    <td>차량등록증</td>
	                                    <td class="widthAuto" style="">
											<c:if test='${fn:length(selectCarDetailEMap.fileList) > 0 }'> 
	                                        	<input  style="display:none" type="file" id="upload"  name="bbsFile" multiple value=""  onchange="javascript:fileChange();"> 
	                                        	<%-- <input style="display:inline; width:82%;" id="file_info" type="text" placeholder="" readonly value="${fn:length(empMap.fileList)}개의 파일이 첨부 되어 있습니다."> --%>
	                                        	<div id="forFile"  style="display:inline;">
		                                        	<c:forEach var="fileData" items="${selectCarDetailEMap.fileList}" varStatus="status">
							                            <a target="_blank" href="/files/driver${selectCarDetailEMap.file_path }">
							                            	<img style="width:30px; height:25px; margin-left:10px;" src="/files/driver${selectCarDetailEMap.file_path }" alt="${fileData.file_nm}" title="${fileData.file_nm}" />
														</a>
													</c:forEach>
	                                        		<input style="display:inline; float:right;" type="button" id="file_mod" onclick="javascript:fileModConfirm('${fn:length(selectCarDetailEMap.fileList)}');" value="수정" class="btn-primary">
	                                        	</div>
	                                        </c:if>
	                                        <c:if test='${fn:length(selectCarDetailEMap.fileList) == 0 }'> 
	                                        	<input  style="display:inline; width:200px;" type="file" id="upload"  name="bbsFile" multiple value="" >
	                                        </c:if> 
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>비고</td>
	                            <td class="widthAuto" style="">
		                         	<c:if test="${paramMap.code eq 'A'}">
		                                <input type="text" placeholder="비고" name="carText" id="carText" value="">
		                            </c:if>
		                            <c:if test="${paramMap.code ne 'A'}">
		                               <input type="text" placeholder="비고" name="carText" id="carText" value="${selectCarDetailEMap.car_text}">
		                            </c:if>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	            <!--     <table>
	                            <tbody>
	                                <tr>
	                                    <td>사진</td>
	                                    <td class="widthAuto" style="">
											<c:if test='${fn:length(driverMap.driverfileList) > 0 }'> 
	                                        	<input  style="display:none" type="file" id="driverupload"  name="driverFile" multiple value=""  onchange="javascript:driverfileChange();"> 
	                                        	<%-- <input style="display:inline; width:82%;" id="file_info" type="text" placeholder="" readonly value="${fn:length(empMap.fileList)}개의 파일이 첨부 되어 있습니다."> --%>
	                                        	<div id="driverforFile"  style="display:inline;">
		                                        	<c:forEach var="fileData" items="${driverMap.driverfileList}" varStatus="status">
							                            <a target="_blank" href="/files/driver${fileData.file_path }">
							                            	<img style="width:30px; height:25px; margin-left:10px;" src="/files/driver${fileData.file_path }" alt="${fileData.file_nm}" title="${fileData.file_nm}" />
														</a>
													</c:forEach>
	                                        		<input style="display:inline; float:right;" type="button" id="file_mod" onclick="javascript:driverfileModConfirm('${fn:length(driverMap.driverfileList)}');" value="수정" class="btn-primary">
	                                        	</div>
	                                        </c:if>
	                                        <c:if test='${fn:length(driverMap.driverfileList) == 0 }'> 
	                                        	<input  style="display:inline; width:200px;" type="file" id="upload"  name="driverFile" multiple value="" >
	                                        </c:if> 
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                         -->
	                     
	                
	                </div>
	                
	                <div class="column-sub">
	                
	         
	              
	      
	
	                

	                </div>
	                
	                
	                
	                
	               
	                
                
                </div>
               
                    <div class="confirmation">
                        <div class="confirm">
	                        <c:if test="${paramMap.code eq 'A'}">
	                            <input type="button" value="등록" onclick="javascript:insertCarDetail();">
	                         </c:if> 
	                      	<c:if test="${paramMap.code ne 'A'}">
                         	   <input type="button" value="수정" onclick="javascript:updateCarDetail();">
                           </c:if>    
                        </div>
                        <div class="cancel">
                        <input type="button" value="취소" onclick="javascript:history.go(-1); return false;">
                    </div>
                    </div>
                </form>
                <!-- <div class="table-pagination text-center">
                    <ul class="pagination">
                        <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="curr-page"><a href="#">1</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
                    </ul>
                </div> -->
            </section>
        </div>
