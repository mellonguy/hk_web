<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>
<%

%>

<script type="text/javascript">
$(document).ready(function(){
	
	$("#selectMonth").MonthPicker({ 
		Button: false
		,MonthFormat: 'yy-mm'	
		,OnAfterChooseMonth: function() { 
	        //alert($(this).val());
	        document.location.href = "/carmanagement/driverDeposit.do?&searchWord="+encodeURI($("#searchWord").val())+"&selectMonth="+$(this).val();
	    }
	});
	
});

function editDriverDeduct(driverId,selectMonth){
	
	document.location.href = "/account/driverCal.do?driverId="+driverId+"&selectMonth="+selectMonth;
}

/*
function downloadDriverDeduct(driverId,selectMonth){
	
	document.location.href = "/account/downloadDriverDeductInfo.do?driverId="+driverId+"&selectMonth="+selectMonth;
}
*/


function addEmp(){
	document.location.href = "/baseinfo/add-employee.do";
}


function search(){
	
	document.location.href = "/carmanagement/carMaintanance-reg.do?&searchType="+encodeURI($("#searchType").val())+"&startDt="+$('#startDt').val()+"&endDt="+$('#endDt').val()+"&driverId="+"${paramMap.driverId}";
	
}

function viewMonthDriverDeduct(driverId){
	
	document.location.href = "/account/viewMonthDriverDeduct.do?driverId="+driverId;
	
}




function checkAll(){

	if($('input:checkbox[id="checkAll"]').is(":checked")){
		$('input:checkbox[name="forDeductStatus"]').each(function(index,element) {
			$(this).prop("checked","true");
		 });	
	}else{
		$('input:checkbox[name="forDeductStatus"]').each(function(index,element) {
			$(this).prop("checked","");
		 });
	}
}








function viewTotalSales(driverId){
	document.location.href = "/account/driverListDetail.do?&paymentPartnerId="+driverId+"&decideMonth="+$("#selectMonth").val();
}


function downloadDriverDeduct(driverId,selectMonth){
	
	window.open("/account/viewDriverDeductInfo.do?driverId="+driverId+"&selectMonth="+selectMonth,"_blank","top=0,left=0,width=780,height=1000,toolbar=0,status=0,scrollbars=0,resizable=0");
	
}




function viewDriverBillingFile(driverId,selectMonth){
	
	window.open("/account/viewDriverBillingFile.do?driverId="+driverId+"&selectMonth="+selectMonth,"_blank","top=0,left=0,width=1000,height=700,toolbar=0,status=0,scrollbars=0,resizable=0");
	
}

function viewLowViolation(driverId,occurrenceDt){


	document.location.href = "/carmanagement/viewLowViolation.do?driverId="+driverId+"&occurrenceDt="+occurrenceDt;
	
}


function editCarMaintanance(carMaintananceId,driverId,obj){

	$(obj).parent().parent().find(".normal").css("display","none");
	$(obj).parent().parent().find(".mod").css("display","");
	$(obj).css("display","none");
	$(obj).next().css("display","");
}


var rgx1 = /\D/g;  // /[^0-9]/g 와 같은 표현
var rgx2 = /(\d+)(\d{3})/; 

function getNumber(obj){
	
    var num01;
    var num02;
    num01 = obj.value;
    num02 = num01.replace(rgx1,"");
    num01 = setComma(num02);
    obj.value =  num01;
    
}

function setComma(inNum){
    
    var outNum;
    outNum = inNum; 
    while (rgx2.test(outNum)) {
         outNum = outNum.replace(rgx2, '$1' + ',' + '$2');
     }
 
    return outNum;

}



function setCarMaintanance(carMaintananceId,driverId,obj){
	
	
	var carMaintananceInfo = new Object();

	if($(obj).parent().parent().find(".mod").eq(0).find("input").val() == ""){
		alert("일자가 입력 되지 않았습니다.");
		return false;
	}else{
		$("#carMaintananceRegdt").val($(obj).parent().parent().find(".mod").eq(0).find("input").eq(0).val());
		//$("#phoneNum").val($(obj).parent().parent().find(".mod").eq(0).find("input").eq(1).val());
		
	}	
	
	if($(obj).parent().parent().find(".mod").eq(1).find("input").val() == ""){
		alert("기사명이 입력 되지 않았습니다.");
		return false;
	}else{
		$("#driverName").val($(obj).parent().parent().find(".mod").eq(1).find("input").val());
	
	}

	if($(obj).parent().parent().find(".mod").eq(2).find("input").val() == ""){
		alert("차량번호가 입력 되지 않았습니다.");
		return false;
	}else{
		$("#carNum").val($(obj).parent().parent().find(".mod").eq(2).find("input").val());
	
	}

	if($(obj).parent().parent().find(".mod").eq(3).find("input").val() == ""){
		alert("분류 입력 되지 않았습니다.");
		return false;
	}else{
		$("#carDivision").val($(obj).parent().parent().find(".mod").eq(3).find("select").val());
		
	}
	if($(obj).parent().parent().find(".mod").eq(4).find("input").val() == ""){
		alert("항목이 입력 되지 않았습니다.");
		return false;
	}else{
		$("#carCategory").val($(obj).parent().parent().find(".mod").eq(4).find("input").val());
	
	}

	if($(obj).parent().parent().find(".mod").eq(5).find("input").val() == ""){
		alert("비용이 입력 되지 않았습니다.");
		return false;
	}else{
		$("#carPayment").val($(obj).parent().parent().find(".mod").eq(5).find("input").val());
		
	}
	
	if($(obj).parent().parent().find(".mod").eq(6).find("input").val() == ""){
	
	}else{
		$("#carSubsidy").val($(obj).parent().parent().find(".mod").eq(6).find("input").val());
		
	}
	if($(obj).parent().parent().find(".mod").eq(7).find("input").val() == ""){
		
	}else{
		$("#carRepair").val($(obj).parent().parent().find(".mod").eq(7).find("input").val());
		
	}

	if($(obj).parent().parent().find(".mod").eq(8).find("input").val() == ""){
	
	}else{
		$("#carDistance").val($(obj).parent().parent().find(".mod").eq(8).find("input").val());
		
	}

	if($(obj).parent().parent().find(".mod").eq(9).find("input").val() == ""){

	}else{
		$("#carGas").val($(obj).parent().parent().find(".mod").eq(9).find("input").val());
		
	}
	
	if($(obj).parent().parent().find(".mod").eq(10).find("input").val() == ""){

	}else{
		//$("#carText").val($(obj).parent().parent().find(".mod").eq(10).find("input").val());
		
	}

	if($(obj).parent().parent().find(".mod").eq(11).find("input").val() == ""){

	}else{
		$("#carText").val($(obj).parent().parent().find(".mod").eq(11).find("input").val());
		
	}
	

	$("#carMaintananceId").val(carMaintananceId);
	$("#driverId").val(driverId);

	if(confirm("차계부 관리를 수정 하시겠습니까?")){
		$("#carMaintananceForm").attr("action","/carmanagement/updateCarMaintanance.do");
		$("#carMaintananceForm").submit();
	}


	
}

function deleteCarMaintanance(carMaintananceId,driverId,obj){
	
	
 	if(confirm("차계부 내역을 삭제 하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : "/carmanagement/delete-carMaintanance.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				driverId : "${paramMap.driverId}",
				carMaintananceId : carMaintananceId
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("삭제 되었습니다.");
					document.location.reload();
				}else if(result == "0001"){
					alert("삭제 하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
	
}







function addcarMaintance(driverId){


	if($("#carMaintananceRegdt").val() == ""){
		alert("일자가 입력되지 않았습니다.");
		return false;
	}	
	
	
	if($("#carDivision").val() == ""){
		alert("분류가 입력 되지 않았습니다.");
		return false;
	}

	if($("#driverName").val() == ""){
		alert("기사가 입력되지 않았습니다.");
		return false;
	}	

	if($("#carCategory").val() == ""){
		alert("항목이 입력 되지 않았습니다.");
		return false;
	}

	if($("#carPayment").val() == ""){
		alert("비용 입력 되지 않았습니다.");
		return false;
	}
		

	if($("#carSubsidy").val() == ""){
		alert("보조금이 입력 되지 않았습니다.");
		return false;
	}
	

	if($("#carRepair").val() == ""){
		alert("정비/주유소가 입력 되지 않았습니다.");
		return false;
	}


	
	if(confirm("해당 내용을 등록 하시겠습니까?")){
		$("#carMaintananceForm").attr("action","/carmanagement/insertCarMaintanance.do?&driverId="+driverId);
		$("#carMaintananceForm").submit();
	}
	

		

}


</script>
		<!-- <section class="side-nav"> -->
            <!-- <ul>
                <li class="active"><a href="/baseinfo/employee.do">직원관리</a></li>
                <li class=""><a href="/baseinfo/driver.do">기사관리</a></li>
                <li class=""><a href="/baseinfo/customer.do">거래처관리</a></li>
                <li class=""><a href="/baseinfo/company.do">회사정보관리</a></li>
            </ul> -->
        <!-- </section> -->

        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">차량관리</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">차계부 등록</a></li>
                </ul>
            </div>
            
             <div class="dispatch-btn-container">
				<div style="width:700px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">기사명 : ${paramMap.driverName} /  차량번호 : ${paramMap.carNum} 의 차계부</div>
            </div>

            <div class="up-dl clearfix header-search">
                 <table>
                    <tbody>
                        <tr>
         		            <td class="widthAuto" style="width:570px;">
				            <!--  -->	
				           		    <select class="dropdown" name="searchType" id="searchType" style="height: 63%;">
							        	<option value=""  selected="selected" >전체</option>
							        	<option value="00" <c:if test='${paramMap.searchType eq "00" }'> selected="selected" </c:if>>운행비</option>
							        	<option value="01"  <c:if test='${paramMap.searchType eq "01" }'> selected="selected" </c:if>>수리비</option>
							        	<option value="02"  <c:if test='${paramMap.searchType eq "02" }'> selected="selected" </c:if>>사고비</option>
							        	<option value="03"  <c:if test='${paramMap.searchType eq "03" }'> selected="selected" </c:if>>세금기타</option>
							        </select>
				                <input style="width:40%; text-align:center;" class="datepick" type="text" placeholder="조회시작일" onclick="javascript:$(this).val('');"  readonly="readonly" name="startDt" id="startDt" value="${paramMap.startDt}">&nbsp;~&nbsp;
				                <input style="width:40%; text-align:center;" class="datepick" type="text" placeholder="조회종료일" onclick="javascript:$(this).val('');" readonly="readonly" name="endDt" id="endDt"  value="${paramMap.endDt}">
				            </td>
				         	<td>
                        		<input type="button" id="searchStatusU" name="" value="검색" class="btn-primary" onclick="javascript:search();">
                            </td>
                            
                        </tr>
                    </tbody>
                </table>
            </div>
<!-- 
            <div class="dispatch-btn-container">
               
                <div class="dispatch-btn">
                    <i class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div>
            </div> -->
            	
            
        </section>



            <section class="bottom-table" style="width:1600px; margin-left:290px;">
                   <br>
              <table class="article-table" style="margin-top:15px; table-layout:fixed;">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <td style="text-align:center; width:100px;">일자(필수)</td>
                        	<td style="text-align:center; width:100px;">분류(필수)</td>
                        	<td style="text-align:center; width:100px;">기사명</td>
                            <td style="text-align:center; width:100px;">항목(필수)</td>
                            <td style="text-align:center; width:100px;">비용(필수)</td>
                            <td style="text-align:center; width:100px;">보조금(필수)</td>
                            <td style="text-align:center; width:100px;">정비/주유소(필수)</td>
                            <td style="text-align:center; width:100px;">주행거리(선택)</td>
                            <td style="text-align:center; width:100px;">주유량(선택)</td>
                           <!--  <td style="text-align:center; width:100px;">연비(선택)</td> -->
                            <td style="text-align:center; width:100px;">비고</td>
                            <td style="text-align:center; width:100px;">작성자</td>
                            <td style="text-align:center; width:100px;">관리</td>
                        </tr>
                    </thead>
                    <tbody id="dataArea">
	                   	
    	                	<tr class="ui-state-default">
	                    		
	                    		<form id="carMaintananceForm" style="display:none" name="excelForm" method="post" enctype="multipart/form-data" autocomplete="false">
	                    			
			                    	<td style="text-align:center;"><input style="width:80%;"class="datepick" type="text" placeholder="일자" name="carMaintananceRegdt" id="carMaintananceRegdt"></td>
			                    	<td style="text-align:center;">
			                    	
			                    	  <input style="width:80%;"  type="hidden" name="searchType" id="searchType" value="${paramMap.searchType}"> 
			                    	  <input style="width:80%;"   type="hidden" name="carDetailCode" id="carDetailCode" value="${paramMap.carDetailCode}"> 
			                    	
			                    	  
			                    	 <select class="dropdown" name="carDivision" id="carDivision" style="width: 85%;">
							        	<option value=""  selected="selected">선택</option>
							        	<option value="00" >운행비</option>
							        	<option value="01" >수리비</option>
							        	<option value="02" >사고비</option>
							        	<option value="03" >세금기타</option>
							        </select>
			                    	
			                    	</td>
			                    	<td style="text-align:center;"><input style="width:80%;" type="text" placeholder="기사명" name="driverName" id="driverName" ></td>
			                    	<td style="text-align:center;"><input style="width:80%;" type="text" placeholder="항목" name="carCategory" id="carCategory" ></td>
			                    	<td style="text-align:center;"><input style="width:80%;" type="text" placeholder="비용" name="carPayment" id="carPayment" onkeyup="getNumber(this);" ></td>
			                    	<td style="text-align:center;"><input style="width:80%;" type="text" placeholder="보조금" name="carSubsidy" id="carSubsidy" onkeyup="getNumber(this);" ></td>
			                    	<td style="text-align:center;"><input style="width:80%;" type="text" placeholder="정비/주유소" name="carRepair" id="carRepair" ></td>
			                    	<td style="text-align:center;"><input style="width:80%;" type="text" placeholder="주행거리" name="carDistance" id="carDistance" onkeyup="getNumber(this);" ></td>
			                    	<td style="text-align:center;"><input style="width:80%;" type="text" placeholder="주유량" name="carGas" id="carGas" ></td>
			                    	<!-- <td style="text-align:center;"><input style="width:80%;" type="text" placeholder="연비" name="carMileage" id="carMileage" onkeyup="getNumber(this);" ></td> -->
			                    	<td style="text-align:center;"><input style="width:80%;" type="text" placeholder="비고" name="carText" id="carText" ></td>
			                    	<td style="text-align:center;">${user.emp_name}</td>
			                    	<td style="text-align:center;">
			                    		<a style="cursor:pointer; width:60px;" onclick="javascript:addcarMaintance('${paramMap.driverId}')" class="btn-edit">등록</a>
			                    	</td>
			                    	<input style="width:80%;" type="hidden"  name="driverId" value="${paramMap.driverId}" id="driverId">
			                    	<input style="width:80%;" type="hidden"  name="carNum" value="${paramMap.carNum}" >
			                    	<input style="width:80%;" type="hidden"  id="carMaintananceId" name="carMaintananceId" value="" >
			                    	<input style="width:80%;" type="hidden"  name="carText" value="" >			                    	
		                    	</form>
	                    	</tr>
                    	
                    </tbody>
                </table>    
                
                
                
                <table class="article-table" style="margin-top:15px">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                    
                        <tr>
                            <!-- <td style="text-align:center;">기사아이디</td> -->
                            <td style="text-align:center; width:50px;"><input type="checkbox" id="checkAll" onclick="javascript:checkAll();" ></td>
                            <td style="text-align:center;">일자</td>
                            <td style="text-align:center;">기사명</td>
                            <td style="text-align:center;">차량번호</td>
                            <td style="text-align:center;">분류</td>
                            <td style="text-align:center;">항목</td>
                            <td style="text-align:center;">비용</td>
                            <td style="text-align:center;">보조금</td>
                            <td style="text-align:center;">정비/주유소</td>
                            <td style="text-align:center;">주행거리</td>
                            <td style="text-align:center;">주유량</td>
                       		 <td style="text-align:center;">연비</td>
                            <td style="text-align:center;">비고</td>
                            <td style="text-align:center;">관리</td>
                          
                        </tr>
                    </thead>
                    
                    
                    
                    <tbody id="">
                    		<c:set var="compareMonth" value="" />
                    		<c:set var ="paymentTotal"  value ="0"/>
               			   	<c:set var ="subsidyTotal"  value ="0"/>
               			   	<c:set var ="distanceTotal"  value ="0"/>
               			   	<c:set var ="gasTempTotal"  value ="0"/>

               			   	<c:set var ="total"  value ="0"/>
               			   	
               			   <c:set var ="finalTotal"  value ="0"/>
               			   
               			   
               			   
               			  
               			<c:forEach var="data" items="${listData}" varStatus="status">
               		
                    	  	<c:if test='${status.index == 0}'>
                    	  
                    	        <c:set var="compareMonth" value="${fn:substring(data.car_maintanance_reg_dt,0,7) }" />
                    	 		<c:set var ="finalTotal"  value ="${data.car_distance}"/>
                    	 		
                    		</c:if>
                    	
                    		<c:if test='${status.index > 0 && compareMonth != fn:substring(data.car_maintanance_reg_dt,0,7)}'>
                    			
                    			
								<c:set var ="distanceFinal"  value ="${fn:replace(listData[status.index-1].car_distance,',','')-fn:replace(finalTotal, ',','')}"/> 
	                   			<!-- 연비 -->
                    			<c:set var ="total"  value ="${fn:replace(distanceFinal,',','') / fn:replace(gasTempTotal, ',','')}"/> 
                    		
                    			
                    			<c:set var="compareMonth" value="${fn:substring(data.car_maintanance_reg_dt,0,7) }" />
                        			<tr class="ui-state-default" style="cursor:pointer;"> 
		                            <td style="text-align:center; background:#DEDEDE;"></td> 
		                            <td style="text-align:left; background:#DEDEDE;" >
		                            	[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;월&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;계&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]
		                            </td>
		                            <td style="text-align:right; background:#DEDEDE;"><fmt:formatNumber value="" groupingUsed="true"/></td>
		                            <td style="text-align:right; background:#DEDEDE;"><fmt:formatNumber value="" groupingUsed="true"/></td>
		                            <td style="text-align:right; background:#DEDEDE;"><fmt:formatNumber value="" groupingUsed="true"/></td>
		                            <td style="text-align:right; background:#DEDEDE;"><fmt:formatNumber value="" groupingUsed="true"/></td>
		                            <td style="text-align:right; background:#DEDEDE;"><fmt:formatNumber value="${paymentTotal}" groupingUsed="true"/></td>
		                            <td style="text-align:center; background:#DEDEDE;"><fmt:formatNumber value="${subsidyTotal}" groupingUsed="true"/></td>
		                            <td style="text-align:center; background:#DEDEDE;"></td>
		                            <!-- <td style="text-align:center; background:#DEDEDE;"><fmt:formatNumber value="${fn:replace(data.car_distance,',','')-fn:replace(finalTotal, ',','')}" groupingUsed="true"/>km</td>-->
		                            <td style="text-align:center; background:#DEDEDE;"><fmt:formatNumber value="${distanceFinal}" groupingUsed="true"/>km</td>
		                            <td style="text-align:center; background:#DEDEDE;"><fmt:formatNumber value="${gasTempTotal}" groupingUsed="true"/>L</td>
		                            <td style="text-align:center; background:#DEDEDE;"><fmt:formatNumber value= "${total}" groupingUsed="true"/></td>
		                            <td style="text-align:center; background:#DEDEDE;"><fmt:formatNumber value= "" groupingUsed="true"/></td>
		                            <td style="text-align:center; background:#DEDEDE;"><fmt:formatNumber value= "" groupingUsed="true"/></td>
		                   
	                        	</tr>
	                        	
		                        <c:set var ="paymentTotal"  value ="0"/>
	               			   	<c:set var ="subsidyTotal"  value ="0"/>
	               			   	<c:set var ="distanceTotal"  value ="0"/>
	               			   	<c:set var ="gasTempTotal"  value ="0"/>
	           
	               			   	<c:set var ="total"  value ="0"/>
	               			   	<c:set var ="finalTotal"  value ="${listData[status.index-1].car_distance}"/>
	               			   	
                        </c:if>		
                       
							<tr class="ui-state-default" style=" cursor:pointer;"> 
	                            <td style="text-align:center;"><input type="checkbox" name="forDeductStatus" driverId="${data.driver_id}"></td>
		                            
		                            <td style="text-align:center;">
		                            	<div class="normal">
			                            	${data.car_maintanance_reg_dt}
		                            	</div>
		                            	<div class="mod" style="display:none;">
		                            		<input style=" width:80%;" type="text" placeholder="일자" value="${data.car_maintanance_reg_dt}" >
		                    			</div>
		                            </td>
		                              <td style="text-align:center;">
		                            	<div class="normal">
			                            	${data.driver_name}
		                            	</div>
		                            	<div class="mod" style="display:none;">
		                            		<input style=" width:80%;" type="text" placeholder="기사명" value="${data.driver_name}" >
		                    			</div>
		                            </td> 
		                              <td style="text-align:center;">
		                            	<div class="normal">
			                            	${data.car_num}
		                            	</div>
		                            	<div class="mod" style="display:none;">
		                            		<input style=" width:80%;" type="text" placeholder="차량번호" value="${data.car_num}" >
		                    			</div>
		                            </td> 
		                             
		                            
		                              
		                            <td style="text-align:center;">
		                            	<div class="normal">
		                            	    <c:if test='${data.car_division eq "00" }'>운행비</c:if>
								            <c:if test='${data.car_division eq "01" }'>수리비</c:if>
								            <c:if test='${data.car_division eq "02" }'>사고비</c:if>
								            <c:if test='${data.car_division eq "03" }'>세금기타</c:if>
			                            	
		                            	</div>
		                            	<div class="mod" style="display:none;">
		                            		<!--<input style=" width:80%;" type="text" placeholder="분류" value="${data.car_division}" >  -->
									       <select class="dropdown" name="carDivision" id="carDivision">
									            <option value="00" <c:if test='${data.car_division eq "00" }'> selected="selected"</c:if>>운행비</option>
									            <option value="01" <c:if test='${data.car_division eq "01" }'> selected="selected"</c:if>>수리비</option>
									            <option value="02" <c:if test='${data.car_division eq "02" }'> selected="selected"</c:if>>사고비</option>
									            <option value="03" <c:if test='${data.car_division eq "03" }'> selected="selected"</c:if>>세금기타</option>
								      	  </select>
		                    			</div>
		                            </td> 
		                            
		                              
		                            <td style="text-align:center;">
		                            	<div class="normal">
			                            	${data.car_category}
		                            	</div>
		                            	<div class="mod" style="display:none;">
		                            		<input style=" width:80%;" type="text" placeholder="항목" value="${data.car_category}" >
		                    			</div>
		                            </td> 
		                              
		                            <td style="text-align:center;">
		                            	<div class="normal">
			                            	${data.car_payment}
		                            	</div>
		                            	<div class="mod" style="display:none;">
		                            		<input style=" width:80%;" type="text" placeholder="비용" value="${data.car_payment}"  onkeyup="getNumber(this);" >
		                    			</div>
		                            </td> 
		                              
		                            <td style="text-align:center;">
		                            	<div class="normal">
			                            	${data.car_subsidy}
		                            	</div>
		                            	<div class="mod" style="display:none;">
		                            		<input style=" width:80%;" type="text" placeholder="보조금" value="${data.car_subsidy}"  onkeyup="getNumber(this);">
		                    			</div>
		                            </td> 
		                              
		                            <td style="text-align:center;">
		                            	<div class="normal">
			                            	${data.car_repair}
		                            	</div>
		                            	<div class="mod" style="display:none;">
		                            		<input style=" width:80%;" type="text" placeholder="정비/주유소" value="${data.car_repair}" >
		                    			</div>
		                            </td> 
		                              
		                            <td style="text-align:center;">
		                            	<div class="normal">
			                            	<c:if test="${data.car_distance eq ''}">0km</c:if>
			                            	
			                            	<c:if test="${data.car_distance ne '' && data.car_distance ne null}">
			                            		
			                            		${data.car_distance}km
			                            		
			                           	</c:if>
				                           	                                    	
		                            	</div>
		                            	<div class="mod" style="display:none;">
		                            		<input style=" width:80%;" type="text" placeholder="주행거리(km)" value="${data.car_distance}"  onkeyup="getNumber(this);">
		                    			</div>
		                            </td> 
		                            
		                              <td style="text-align:center;">
		                            	<div class="normal">
		                            		${data.car_gas}
		                            	</div>
		                            	<div class="mod" style="display:none;">
		                            		<input style=" width:80%;" type="text" placeholder="주유량 "  value="${data.car_gas}" >
		                    			</div>
		                            </td> 
		                              
		                            <td style="text-align:center;">
		                            	<div class="normal">
			                            	${data.car_mileage}
		                            	</div>
		                            	<div class="mod" style="display:none;">
		                            		<input style=" width:80%;" type="text" placeholder="연비" value="${data.car_mileage}" onkeyup="getNumber(this);" >
		                    			</div>
		                            </td> 
		                              
		                          
		                            <td style="text-align:center;">
		                            	<div class="normal">
			                            	${data.car_text}
		                            	</div>
		                            	<div class="mod" style="display:none;">
		                            		<input style=" width:80%;" type="text" placeholder="비고" value="${data.car_text}" >
		                    			</div>
		                            </td> 
		                     
		                          <td style="text-align:center;">
	                            		<a style="cursor:pointer; width:50px;" onclick="javascript:editCarMaintanance('${data.car_maintanance_id}','${data.driver_id}',this)" class="btn-edit">수정</a>
	                            	 	<a style="display:none; cursor:pointer; width:50px;" onclick="javascript:setCarMaintanance('${data.car_maintanance_id}','${data.driver_id}',this)" class="btn-edit">완료</a> 
	                            		<a style="cursor:pointer; width:50px;" onclick="javascript:deleteCarMaintanance('${data.car_maintanance_id}','${data.driver_id}',this)" class="btn-edit">삭제</a>
	                            </td>
		                     
                        	</tr> 
                        	
               			   	<c:set var ="paymentTotal"  value ="${fn:replace(paymentTotal,',','') + fn:replace(data.car_payment, ',','') }"/>
               			   	<c:set var ="subsidyTotal"  value ="${fn:replace(subsidyTotal,',','') + fn:replace(data.car_subsidy, ',','') }"/>
               			   	<c:set var ="distanceTotal"  value ="${fn:replace(distanceTotal,',','') + fn:replace(data.car_distance, ',','') }"/>
               			   	<c:set var ="gasTempTotal"  value ="${fn:replace(gasTempTotal,',','') + fn:replace(data.car_gas, ',','') }"/>
               			

               			   	<!-- 연비 -->
               			  
               			    <c:if test='${status.index == fn:length(listData)-1}'>
                    			<c:set var="compareMonth" value="${fn:substring(data.car_maintanance_reg_dt,0,7) }" />
                        		<c:set var ="distanceFinal"  value ="${fn:replace(data.car_distance,',','')-fn:replace(finalTotal, ',','')}"/>
	               			   	<c:set var ="total"  value ="${fn:replace(distanceFinal,',','') / fn:replace(gasTempTotal, ',','')}"/> 
                        	
                    				<c:set var ="DistanceTemp"  value ="${data.car_distance}"/>
                    				<c:set var ="smallDistance"  value ="${fn:replace(distanceTotal,',','')-fn:replace(DistanceTemp, ',','')}"/> 
                    				<c:set var ="bigDistance"  value ="${fn:replace(DistanceTemp,',','')-fn:replace(smallDistance, ',','')}"/> 
                    				
                        	  		<tr class="ui-state-default" style="cursor:pointer;"> 
		                            <td style="text-align:center; background:#DEDEDE;"></td>
		                           
		                            <td style="text-align:left; background:#DEDEDE;" >
		                            	[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;월&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;계&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]
		                            </td>
		                            <td style="text-align:right; background:#DEDEDE;"><fmt:formatNumber value="" groupingUsed="true"/></td>
		                            <td style="text-align:right; background:#DEDEDE;"><fmt:formatNumber value="" groupingUsed="true"/></td>
		                            <td style="text-align:right; background:#DEDEDE;"><fmt:formatNumber value="" groupingUsed="true"/></td>
		                            <td style="text-align:right; background:#DEDEDE;"><fmt:formatNumber value="" groupingUsed="true"/></td>
		                            <td style="text-align:right; background:#DEDEDE;"><fmt:formatNumber value="${paymentTotal}" groupingUsed="true"/></td>
		                            <td style="text-align:center; background:#DEDEDE;"><fmt:formatNumber value="${subsidyTotal}" groupingUsed="true"/></td>
		                            <td style="text-align:center; background:#DEDEDE;"><fmt:formatNumber value="" groupingUsed="true"/></td>	 
		                            <td style="text-align:center; background:#DEDEDE;"><fmt:formatNumber value="${distanceFinal}" groupingUsed="true"/>km</td>
		                            <td style="text-align:center; background:#DEDEDE;"><fmt:formatNumber value= "${gasTempTotal}"  groupingUsed="true"/>L</td>
		                            <td style="text-align:center; background:#DEDEDE;"><fmt:formatNumber value= "${total}" groupingUsed="true"/></td>
		                            <td style="text-align:center; background:#DEDEDE;"><fmt:formatNumber value= "" groupingUsed="true"/></td>
		                            <td style="text-align:center; background:#DEDEDE;"><fmt:formatNumber value= "" groupingUsed="true"/></td>
	
	                        	</tr>
                        </c:if>	
                        	
						</c:forEach>
						
						
				                   
                    </tbody>
                </table>
                <div class="table-pagination text-center">
                    <ul class="pagination">
                        <html:paging uri="/carmanagement/carMaintanance-list.do" forGroup="" />
                    </ul>
                </div>
                <div class="confirmation">
                    <div class="confirm">
                        <%-- <a href="/carnamagement/driverDepositList_download.do?selectMonth=${paramMap.selectMonth}">엑셀다운로드</a> --%>
                    </div>
                    <%-- <div class="confirm">
                        <a href="/account/driverList_download.do?selectMonth=${paramMap.selectMonth}"></a>
                    </div> --%>
                </div>
                
            </section>
     
