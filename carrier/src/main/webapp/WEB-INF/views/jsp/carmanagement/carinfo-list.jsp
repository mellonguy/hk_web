<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>
<%

%>

<script type="text/javascript">
$(document).ready(function(){
	
	$("#selectMonth").MonthPicker({ 
		Button: false
		,MonthFormat: 'yy-mm'	
		,OnAfterChooseMonth: function() { 
	        //alert($(this).val());
	        document.location.href = "/carmanagement/driverDeposit.do?&searchWord="+encodeURI($("#searchWord").val())+"&selectMonth="+$(this).val();
	    }
	});
	
});

function editDriverDeduct(driverId,selectMonth){
	
	document.location.href = "/account/driverCal.do?driverId="+driverId+"&selectMonth="+selectMonth;
}

/*
function downloadDriverDeduct(driverId,selectMonth){
	
	document.location.href = "/account/downloadDriverDeductInfo.do?driverId="+driverId+"&selectMonth="+selectMonth;
}
*/


function addEmp(){
	document.location.href = "/baseinfo/add-employee.do";
}


/* function search(){
	
	document.location.href = "/carmanagement/driverList.do?&searchWord="+encodeURI($("#searchWord").val())+"&selectMonth="+$("#selectMonth").val();
	
} */

function viewMonthDriverDeduct(driverId){
	
	document.location.href = "/account/viewMonthDriverDeduct.do?driverId="+driverId;
	
}



function search(){
	
	document.location.href = "/carmanagement/carinfo-list.do?&searchDateType=${paramMap.searchDateType}"+"&startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val();

	
}


function editDriver(location,driverId){

	if(location == "driver"){
		document.location.href = "/baseinfo/edit-driver.do?driverId="+driverId+"&location="+location;
	}else{
		document.location.href = "/baseinfo/edit-payment-driver.do?driverId="+driverId+"&location="+location;
	}
	
}


function addDriverPayment(){



	
	if($("#carNum").val() == ""){
		alert("차량번호가 입력 되지 않았습니다.");
		return false;
	}
	
	if($("#driverPayment").val() == ""){
		alert("예치금이 입력 되지 않았습니다.");
		return false;
	}




	if(confirm("해당 내용을 등록 하시겠습니까?")){
		$("#paymentForm").attr("action","/carmanagement/insertPaymentDriver.do");
		$("#paymentForm").submit();

		}
	
		

}

function insertDriver(){


	document.location.href = "/baseinfo/add-driver.do?&driverForpayment=Y";

	
}



function insertDriverDeductStatus(driverName,driverId,selectMonth){
	
	
	if(confirm(driverName+" 기사님의 "+selectMonth+"월의 공제내역을 확정 하시겠습니까?")){
		
		$.ajax({ 
			type: 'post' ,
			url : "/account/insertDriverDeductStatus.do" ,
			dataType : 'json' ,
			data : {
				driverName : driverName,
				driverId : driverId,
				decideMonth : selectMonth
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("확정 되었습니다.");
					window.location.reload();
				}else if(result != "0000"){
					alert("확정 하는데 실패 했습니다. 관리자에게 문의 하세요.");
					window.location.reload();
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
		
		
	}else{
		
		
		
	}
	
	
}

function checkAll(){

	if($('input:checkbox[id="checkAll"]').is(":checked")){
		$('input:checkbox[name="forDeductStatus"]').each(function(index,element) {
			$(this).prop("checked","true");
		 });	
	}else{
		$('input:checkbox[name="forDeductStatus"]').each(function(index,element) {
			$(this).prop("checked","");
		 });
	}
}


function insertAllDriverDeductStatus(selectMonth){
	
	var total = $('input:checkbox[name="forDeductStatus"]:checked').length;
	var driverId = "";
	
	if(total == 0){
		alert("확정할 목록이 선택 되지 않았습니다.");
		return false;
	}else{
		$('input:checkbox[name="forDeductStatus"]:checked').each(function(index,element) {
		      if(this.checked){	//checked 처리된 항목의 값
		    	  var selectedObj = new Object();
		    	  driverId+=$(this).attr("driverId");
		    	  if(index<total-1){
		    		  driverId += ","; 
			         } 
		      }
		 });
		
		if(driverId != ""){
			insertDeductStatus(driverId,total,selectMonth);
		}
			

	}
	
	
	/* if(confirm(selectMonth+"월의 공제내역을 일괄확정 하시겠습니까?")){
		
		$.ajax({ 
			type: 'post' ,
			url : "/account/insertAllDriverDeductStatus.do" ,
			dataType : 'json' ,
			data : {
				selectMonth : selectMonth,
				searchType : "${paramMap.searchType}",
				searchWord : "${paramMap.searchWord}"
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("확정 되었습니다.");
					window.location.reload();
				}else if(result != "0000"){
					alert("확정 하는데 실패 했습니다. 관리자에게 문의 하세요.");
					window.location.reload();
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
		
		
	}else{
		
	} */
	
	
}



function insertDeductStatus(driverId,total,selectMonth){
	
	
	if(confirm(total+"명의 공제내역을 확정 하시겠습니까?")){
		
		$.ajax({ 
			type: 'post' ,
			url : "/account/insertAllDriverDeductStatus.do" ,
			dataType : 'json' ,
			data : {
				selectMonth : selectMonth,
				driverId : driverId
				//searchWord : "${paramMap.searchWord}"
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("확정 되었습니다.");
					window.location.reload();
				}else if(result != "0000"){
					alert("확정 하는데 실패 했습니다. 관리자에게 문의 하세요.");
					window.location.reload();
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
		
		
	}else{
		
	}
}



function viewTotalSales(driverId){
	document.location.href = "/account/driverListDetail.do?&paymentPartnerId="+driverId+"&decideMonth="+$("#selectMonth").val();
}


function downloadDriverDeduct(driverId,selectMonth){
	
	window.open("/account/viewDriverDeductInfo.do?driverId="+driverId+"&selectMonth="+selectMonth,"_blank","top=0,left=0,width=780,height=1000,toolbar=0,status=0,scrollbars=0,resizable=0");
	
}


/* function viewDriverPaymentMiddle(location,driverId){

	document.location.href = "/carmanagement/carinfo-middle.do?&driverId="+driverId+"&location="+location;
	
} */


function viewDriverPaymentMiddle(location,businessLicenseNumber){

	document.location.href = "/carmanagement/carinfo-middle.do?&businessLicenseNumber="+businessLicenseNumber+"&location="+location;
	
}




function viewDriverBillingFile(driverId,selectMonth){
	
	window.open("/account/viewDriverBillingFile.do?driverId="+driverId+"&selectMonth="+selectMonth,"_blank","top=0,left=0,width=1000,height=700,toolbar=0,status=0,scrollbars=0,resizable=0");
	
}

function viewLowViolation(driverId,occurrenceDt){


	document.location.href = "/carmanagement/viewLowViolation.do?driverId="+driverId+"&occurrenceDt="+occurrenceDt;
	
}



function updateAnotherDecideFinalForMakeExcel(){
	
	
	if(confirm($("#selectMonth").val()+"월분 운송비 정산 내역서를 일괄 성성 하시겠습니까?")){
	
	$.ajax({ 
		type: 'post' ,
		url : "/account/updateAnotherDecideFinalForMakeExcel.do" ,
		dataType : 'json' ,
		data : {
			decideMonth : $("#selectMonth").val()
		},
		success : function(data, textStatus, jqXHR)
		{
			var result = data.resultCode;
			var resultData = data.resultData;
			if(result == "0000"){
				alert("일괄 생성 되었습니다.");
			}else if(result == "0001"){
				alert("일괄 생성 하는데 실패 하였습니다.");
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
	}
	
	
}


</script>
		<!-- <section class="side-nav"> -->
            <!-- <ul>
                <li class="active"><a href="/baseinfo/employee.do">직원관리</a></li>
                <li class=""><a href="/baseinfo/driver.do">기사관리</a></li>
                <li class=""><a href="/baseinfo/customer.do">거래처관리</a></li>
                <li class=""><a href="/baseinfo/company.do">회사정보관리</a></li>
            </ul> -->
        <!-- </section> -->

        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">차량관리</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">예치금 관리</a></li>
                </ul>
            </div>
            	<br>
 		 <div class="dispatch-btn-container">
				<div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">기사 지입료 관리</div>
            </div>
           			<!-- <div class="upload-btn" style="float:right;">
						<input type="button"  onclick="javascript:excelDownload('S');" value="엑셀 다운로드"> 
					</div>  
 	 -->
 	<div class="up-dl clearfix header-search">
                 <table>
                    <tbody>
                        <tr>
                        	<td>조회 기간 :</td>
				                 <td class="widthAuto" style="width:500px;">
				            
				                <input style="width:40%; text-align:center;" class="datepick" type="text" placeholder="조회시작일" onclick="javascript:$(this).val('');"  readonly="readonly" name="startDt" id="startDt" value="${paramMap.startDt}">&nbsp;~&nbsp;
				                <input style="width:40%; text-align:center;" class="datepick" type="text" placeholder="조회종료일" onclick="javascript:$(this).val('');" readonly="readonly" name="endDt" id="endDt"  value="${paramMap.endDt}">
				            </td>
                            <td>
                                <input type="button" id="btn-search" value="검색" class="btn-primary" onclick="javascript:search();">
                            </td>
                            
                            <!-- <td style="">
                			        <input type="button" id="btn-search" value="지입료 기사 등록" class="btn-primary" onclick="javascript:insertDriver();">        
                            </td> -->
                            
                        </tr>
                        
                    </tbody>
                    
                </table>
                
            </div>
          
        </section>
  

            <section class="bottom-table" style="width:1600px; margin-left:290px;">
                
                <table class="article-table" style="margin-top:15px">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <!-- <td style="text-align:center;">기사아이디</td> -->
                            <td style="text-align:center; width:30px;"><input type="checkbox" id="checkAll" onclick="javascript:checkAll();" ></td>
                            <td style="text-align:center; width:150px;">소유주</td>
                            <td style="text-align:center; width:150px;">사업자등록번호</td>
                            <td style="text-align:center; width:150px;">기초잔액</td>
                            <td style="text-align:center; width:150px;">청구액</td>
                            <td style="text-align:center; width:150px;">입금액</td>
                            <td style="text-align:center; width:150px;">잔액</td>
                            <td style="text-align:center; width:150px;">비고</td>
                            <!-- <td style="text-align:center; width:70px;">관리</td> -->
                        </tr>
                    </thead>
                    <tbody id="">
                    	<c:forEach var="data" items="${listData}" varStatus="status">
							<tr class="ui-state-default" style=" cursor:pointer;"> 
	                            <td style="text-align:center;"><input type="checkbox" name="forDeductStatus" driverId="${data.driver_owner}"></td>
	                            <td style="text-align:center;" onclick="javascript:viewDriverPaymentMiddle('${data.location}','${data.business_license_number}');">${data.driver_owner}</td>
	                            <td style="text-align:center;" onclick="javascript:viewDriverPaymentMiddle('${data.location}','${data.business_license_number}');">${data.business_license_number}</td>
	                            <td style="text-align:center;" onclick="javascript:viewDriverPaymentMiddle('${data.location}','${data.business_license_number}');">${data.driver_balance}</td>
	                            <td style="text-align:center;" onclick="javascript:viewDriverPaymentMiddle('${data.location}','${data.business_license_number}');">${data.resign_dt}</td>	                            
	                            <td style="text-align:center;" onclick="javascript:viewDriverPaymentMiddle('${data.location}','${data.business_license_number}');">${data.driver_payment}</td>
	                            <td style="text-align:center;" onclick="javascript:viewDriverPaymentMiddle('${data.location}','${data.business_license_number}');">${data.driver_payment}</td>
	                            <td style="text-align:center;" onclick="javascript:viewDriverPaymentMiddle('${data.location}','${data.business_license_number}');">${data.etc}</td>
	                            <%-- <td style="text-align:center;">
	                            	<a style="cursor:pointer; width:60px;" onclick="javascript:editDriver('${data.location}','${data.driver_id}')" class="btn-edit">수정</a>
	                            </td> --%>
                        	</tr>
                        	
                        	 <tr>      
                           		<c:set var="driverDeposit" value="${fn:replace(driverDeposit,',','')+fn:replace(data.driver_deposit, ',','')}" />
                                <c:set var="inputDeposit" value="${fn:replace(inputDeposit,',','')+fn:replace(data.input_deposit, ',','')}" />
                                
                                
                                 <c:if test="${fn:indexOf(data.receivable_deposit, '-') == -1}">
                                <c:set var="receivableDeposit" value="${fn:replace(receivableDeposit,',','')+fn:replace(data.receivable_deposit, ',','')}" />
                                </c:if>
                                
                                <c:if test="${fn:indexOf(data.receivable_deposit, '-') != -1}">
                                <c:set var="receivableDeposit0" value="" />
                                </c:if>
                                
                                <c:set var="driver_payment" value="${fn:replace(driver_payment,',','')+fn:replace(data.driver_payment, ',','')}" />
                                
               			   </tr>
                        	
                        	
                        	
						</c:forEach>
                          <tr>
                        
                          <td style="text-align: center; background: #E1E1E1; ">                                  
                                      <fmt:formatNumber value="" groupingUsed="true"/>
                                      </td>
                          
                                 <td style="text-align: center; background: #E1E1E1; ">                                
                                      <c:out value =" 합     계"/>
                                      </td>

                                <td style="text-align: center; background: #E1E1E1; ">                                  
                                      <fmt:formatNumber value="" groupingUsed="true"/>
                                      </td>
                             
                                   <td style="text-align: center; background: #E1E1E1; ">                                  
                                      <fmt:formatNumber value="" groupingUsed="true"/>
                                      </td>
                                   
                                     <td style="text-align: center; background: #E1E1E1; ">                                  
                                      <fmt:formatNumber value="" groupingUsed="true"/>
                                      </td>
                                        <td style="text-align: center; background: #E1E1E1; ">                                  
                                      <fmt:formatNumber value="" groupingUsed="true"/>
                                      </td>
                                <td style="text-align: center; background: #E1E1E1; ">                                  
                                      <fmt:formatNumber value="" groupingUsed="true"/>
                                      </td>
                                      
                       
                                      
                                  <td style="text-align: center; background: #E1E1E1; ">                                  
                                      <fmt:formatNumber value="${driver_payment}" groupingUsed="true"/>
                                      </td>
                                  
                                  <!-- <td style="text-align: center; background: #E1E1E1; "></td> -->
                                      
                                   </tr>   
                    </tbody>
                </table>
                
                	
                
                
                <div class="table-pagination text-center">
                    <ul class="pagination">
                        <html:paging uri="/carmanagement/carinfo-list.do" forGroup="" />
                    </ul>
                </div> 
                <div class="confirmation">
                    <div class="confirm">
                        <%-- <a href="/carnamagement/driverDepositList_download.do?selectMonth=${paramMap.selectMonth}">엑셀다운로드</a> --%>
                    </div>
                    <%-- <div class="confirm">
                        <a href="/account/driverList_download.do?selectMonth=${paramMap.selectMonth}"></a>
                    </div> --%>
                </div>
                
            </section>
            
            
            
     
