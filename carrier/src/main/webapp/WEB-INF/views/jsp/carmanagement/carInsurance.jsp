<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>
<%

%>

<script type="text/javascript">
$(document).ready(function(){
	
	$("#selectMonth").MonthPicker({ 
		Button: false
		,MonthFormat: 'yy-mm'	
		,OnAfterChooseMonth: function() { 
	        //alert($(this).val());
	        document.location.href = "/carmanagement/driverList.do?&searchWord="+encodeURI($("#searchWord").val())+"&selectMonth="+$(this).val();
	    }
	});



	
});

function editDriverDeduct(driverId,selectMonth){
	
	document.location.href = "/account/driverCal.do?driverId="+driverId+"&selectMonth="+selectMonth;
}



function addEmp(){
	document.location.href = "/baseinfo/add-employee.do";
}


function search(){
	
	document.location.href = "/carmanagement/carInsurance.do?&searchWord="+encodeURI($("#searchWord").val());
	
}

function viewMonthDriverDeduct(driverId){
	
	document.location.href = "/account/viewMonthDriverDeduct.do?driverId="+driverId;
	
}


function insertDriverDeductStatus(driverName,driverId,selectMonth){
	
	
	if(confirm(driverName+" 기사님의 "+selectMonth+"월의 공제내역을 확정 하시겠습니까?")){
		
		$.ajax({ 
			type: 'post' ,
			url : "/account/insertDriverDeductStatus.do" ,
			dataType : 'json' ,
			data : {
				driverName : driverName,
				driverId : driverId,
				decideMonth : selectMonth
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("확정 되었습니다.");
					window.location.reload();
				}else if(result != "0000"){
					alert("확정 하는데 실패 했습니다. 관리자에게 문의 하세요.");
					window.location.reload();
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
		
		
	}else{
		
		
		
	}
	
	
}

function checkAll(){

	if($('input:checkbox[id="checkAll"]').is(":checked")){
		$('input:checkbox[name="forDeductStatus"]').each(function(index,element) {
			$(this).prop("checked","true");
		 });	
	}else{
		$('input:checkbox[name="forDeductStatus"]').each(function(index,element) {
			$(this).prop("checked","");
		 });
	}
}


function insertAllDriverDeductStatus(selectMonth){
	
	var total = $('input:checkbox[name="forDeductStatus"]:checked').length;
	var driverId = "";
	
	if(total == 0){
		alert("확정할 목록이 선택 되지 않았습니다.");
		return false;
	}else{
		$('input:checkbox[name="forDeductStatus"]:checked').each(function(index,element) {
		      if(this.checked){	//checked 처리된 항목의 값
		    	  var selectedObj = new Object();
		    	  driverId+=$(this).attr("driverId");
		    	  if(index<total-1){
		    		  driverId += ","; 
			         } 
		      }
		 });
		
		if(driverId != ""){
			insertDeductStatus(driverId,total,selectMonth);
		}
			

	}
	
	
	/* if(confirm(selectMonth+"월의 공제내역을 일괄확정 하시겠습니까?")){
		
		$.ajax({ 
			type: 'post' ,
			url : "/account/insertAllDriverDeductStatus.do" ,
			dataType : 'json' ,
			data : {
				selectMonth : selectMonth,
				searchType : "${paramMap.searchType}",
				searchWord : "${paramMap.searchWord}"
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("확정 되었습니다.");
					window.location.reload();
				}else if(result != "0000"){
					alert("확정 하는데 실패 했습니다. 관리자에게 문의 하세요.");
					window.location.reload();
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
		
		
	}else{
		
	} */
	
	
}



function insertDeductStatus(driverId,total,selectMonth){
	
	
	if(confirm(total+"명의 공제내역을 확정 하시겠습니까?")){
		
		$.ajax({ 
			type: 'post' ,
			url : "/account/insertAllDriverDeductStatus.do" ,
			dataType : 'json' ,
			data : {
				selectMonth : selectMonth,
				driverId : driverId
				//searchWord : "${paramMap.searchWord}"
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("확정 되었습니다.");
					window.location.reload();
				}else if(result != "0000"){
					alert("확정 하는데 실패 했습니다. 관리자에게 문의 하세요.");
					window.location.reload();
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
		
		
	}else{
		
	}
}



function viewTotalSales(driverId){
	document.location.href = "/account/driverListDetail.do?&paymentPartnerId="+driverId+"&decideMonth="+$("#selectMonth").val();
}


function downloadDriverDeduct(driverId,selectMonth){
	
	window.open("/account/viewDriverDeductInfo.do?driverId="+driverId+"&selectMonth="+selectMonth,"_blank","top=0,left=0,width=780,height=1000,toolbar=0,status=0,scrollbars=0,resizable=0");
	
}



function viewDriverBillingFile(driverId,selectMonth){
	
	window.open("/account/viewDriverBillingFile.do?driverId="+driverId+"&selectMonth="+selectMonth,"_blank","top=0,left=0,width=1000,height=700,toolbar=0,status=0,scrollbars=0,resizable=0");
	
}

function viewCarInsurnceDetail(carNum){


	document.location.href = "/carmanagement/carInsuranceDetail.do?&gongCarNum="+carNum;
	
}



function updateAnotherDecideFinalForMakeExcel(){
	
	
	if(confirm($("#selectMonth").val()+"월분 운송비 정산 내역서를 일괄 성성 하시겠습니까?")){
	
	$.ajax({ 
		type: 'post' ,
		url : "/account/updateAnotherDecideFinalForMakeExcel.do" ,
		dataType : 'json' ,
		data : {
			decideMonth : $("#selectMonth").val()
		},
		success : function(data, textStatus, jqXHR)
		{
			var result = data.resultCode;
			var resultData = data.resultData;
			if(result == "0000"){
				alert("일괄 생성 되었습니다.");
			}else if(result == "0001"){
				alert("일괄 생성 하는데 실패 하였습니다.");
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
	}
	
	
}


function goAddInsurance(){


	document.location.href ="/carmanagement/add-insurance.do";
}


function paymentComplate(insuranceSubId,roundOfEvents,insuranceAymentInInstallment){   //납부완료 눌렀을때 


var total = Number(roundOfEvents)+1;


var monthCount = Number(insuranceAymentInInstallment);
var cnt = 0;

	if(monthCount == 6){
		
		cnt = 2;

	}else if(monthCount == 2){
		
			cnt = 6;
	}else if(monthCount == 12){

		alert("일시납");
	}	

	if(total > cnt ){

		alert("납부가 완료된 보험입니다.");
		
	}else{

		
		if(confirm(total+"회차 납부를 완료하시겠습니까?")){
	
			$.ajax({ 
				type: 'post' ,
				url : "/carmanagement/updateInsuracneRountOfEvents.do" ,
				dataType : 'json' ,
				data : {
					insuranceSubId : insuranceSubId,
					roundOfEvents : roundOfEvents,
					
				},
				success : function(data, textStatus, jqXHR)
				{
					var result = data.resultCode;
					var resultData = data.resultData;
					if(result == "0000"){
						
						alert("납부완료되었습니다.");
						window.location.reload();
					}else if(result =="A001"){
						alert("납부가 완료된 보험입니다.");
					}else if(result =="0001"){
						alert("알수 없는 에러입니다 관리자에게 문의해주세요.");
					}
				} ,
				error : function(xhRequest, ErrorText, thrownError) {
				}
			});
		
		}

	}
}




var rgx1 = /\D/g;  // /[^0-9]/g 와 같은 표현
var rgx2 = /(\d+)(\d{3})/; 

function getNumber(obj){
	
    var num01;
    var num02;
    num01 = obj.value;
    num02 = num01.replace(rgx1,"");
    num01 = setComma(num02);
    obj.value =  num01;
    
}

function setComma(inNum){
    
    var outNum;
    outNum = inNum; 
    while (rgx2.test(outNum)) {
         outNum = outNum.replace(rgx2, '$1' + ',' + '$2');
     }
 
    return outNum;

}














</script>
		<!-- <section class="side-nav"> -->
            <!-- <ul>
                <li class="active"><a href="/baseinfo/employee.do">직원관리</a></li>
                <li class=""><a href="/baseinfo/driver.do">기사관리</a></li>
                <li class=""><a href="/baseinfo/customer.do">거래처관리</a></li>
                <li class=""><a href="/baseinfo/company.do">회사정보관리</a></li>
            </ul> -->
        <!-- </section> -->

        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">차량관리</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">차량보험 관리</a></li>
                </ul>
            </div>
            
               <div class="dispatch-btn-container">
				<div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">차량보험 관리</div>
            </div>
            
  			※ 납부일 7일전은 <span style="color:#0000FF;">파란색 ,</span> 2일전은 <span style="color:#FF8000;">주황색,</span> 당일은 <span style="color:#DF0101;">빨간색</span> 으로 표시됩니다.<br>
			<div class="up-dl clearfix header-search">
                 <table>
                    <tbody>
                        <tr>
                        	<td>납부일 :</td>
				            <td class="widthAuto" style="width:400px;">
							     <div class="date-picker">
			                		<input style="width:150px;" autocomplete="off" id="startDt" placeholder="검색 시작일" name="startDt" type="text" class="datepick" value="${paramMap.startDt}"> ~ <input style="width:150px;" autocomplete="off" id="endDt" placeholder="검색 종료일" name="endDt"  type="text" class="datepick" value="${paramMap.endDt}">
				                </div>
				            </td>
				            <td>차량번호 :</td>
	                            <td style="width: 190px;">
	                                <input type="text" id="searchWord" name="searchWord" value="${paramMap.searchWord}"   placeholder="차량 번호 검색" onkeypress="if(event.keyCode=='13') search();">
	                            </td>
                            <td>
                                <input type="button" id="btn-search" value="검색" class="btn-primary" onclick="javascript:search();">
                            </td>
                             <td>
                                <input type="button" id="btn-search" value="차량보험 등록" class="btn-primary" onclick="javascript:goAddInsurance();">
                            </td>
                            
                        </tr>
                    </tbody>
                </table>
            </div>
<!-- 
            <div class="dispatch-btn-container">
               
                <div class="dispatch-btn">
                    <i class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div>
            </div> -->
        </section>
		
		
            <section class="bottom-table" style="width:1600px; margin-left:290px;">
   
		<!--  공차 아닌 테이블 -->
                <table class="article-table" style="margin-top:15px">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <!-- <td style="text-align:center;">기사아이디</td> -->
                            <td style="text-align:center; width:50px;"><input type="checkbox" id="checkAll" onclick="javascript:checkAll();" ></td>
                            <td style="text-align:center;">차량번호</td>
                            <td style="text-align:center;">기사명</td>
                            <td style="text-align:center;">최대적재량</td>
                            <td style="text-align:center;">종류</td>
                            <td style="text-align:center;">요율</td>
                            <td style="text-align:center;">자부담금</td>
                            <td style="text-align:center;">가입금액</td>
                            <td style="text-align:center;">총보험료</td>
                            <td style="text-align:center;">회차</td>
                            <td style="text-align:center;">보험료</td>
                            <td style="text-align:center;">납부일</td>
                            <td style="text-align:center;">납부상태</td>
                           
                        </tr>
                    </thead>
                    <tbody id="">
                    	<c:forEach var="data" items="${listData}" varStatus="status">
	                    		
		<!--<c:set var ="paymentTotal"  value ="${fn:replace(data.insurance_application_contribution_first,',','') + fn:replace(data.insurance_application_contribution_next, ',','') }"/> -->	                    		
                    			<tr class="ui-state-default" > 
							
	                            <td style="text-align:center;" rowspan="${data.sub_cnt}" align = "center"><input type="checkbox" name="forDeductStatus" driverId="${data.car_insurance_id}"></td>
	                            <td style="text-align:center; cursor:pointer;" rowspan="${data.sub_cnt} "  align = "center" onclick ="viewCarInsurnceDetail('${data.car_num}')">${data.car_num}</td> <!--  차량번호-->
	                            <td style="text-align:center;" rowspan="${data.sub_cnt}" align = "center">${data.driver_name}</td> <!-- 기사명 -->
	                            <td style="text-align:center;" rowspan="${data.sub_cnt}" align = "center">${data.car_maximum_load_capacity}kg</td><!-- 최대적재량 -->
								<td style="text-align:center;">통합</td>
									<c:forEach var="subdata" items="${carInsuranceSubList}" varStatus="substatus"> 
									
									<!-- 보험료-->
									<c:set var ="paymentTotal"  value ="${fn:replace(subdata.insurance_application_contribution_first,',','') * fn:replace(subdata.round_of_events, ',','')}"/>	      
										
										<c:if test='${data.car_insurance_id eq subdata.car_insurance_id}'>
										    <c:if test='${subdata.insurance_kind eq "T" }'>	                            
												<!--  년도-->
												<c:set var="afterYearDate" value="${subdata.finishInsuranceDate}" /> <!-- 가입일로부터 1년 날짜-->
													
												<fmt:parseDate value="${subdata.payment_closing_dt}" var="strPlanDate" pattern="yyyy-MM-dd"/>
												<fmt:parseNumber value="${strPlanDate.time / (1000*60*60*24)}" integerOnly="true" var="strDate"></fmt:parseNumber> <!-- 납부일 -->
												<fmt:parseDate value="${paramMap.nowDate}" var="endPlanDate" pattern="yyyy-MM-dd"/>
												<fmt:parseNumber value="${endPlanDate.time / (1000*60*60*24)}" integerOnly="true" var="endDate"></fmt:parseNumber> <!-- 현재날짜 -->
												
												<td style="text-align:center;">${subdata.rate}%</td> 	                            
												<td style="text-align:center;">300,000</td> 	                            
												<td style="text-align:center;">${subdata.barrier_reward_distribution}</td> <!-- 가입금액 -->                         
												<td style="text-align:center;">	${subdata.totalInsurance} </td> <!--총보험료  -->
												<td style="text-align:center;">${subdata.round_of_events}</td> <!-- 회차 -->
												<td style="text-align:center;">${paymentTotal}</td> <!-- 보험료 -->
												
												<c:if test ="${strDate-endDate>'7'}">
													<td style="text-align:center;">${subdata.payment_closing_dt}</td>  <!-- 검정색-->
												 </c:if>
												 <c:if test ="${'3'<= strDate-endDate && strDate-endDate <='7'}">
													<td style="text-align:center;">${subdata.payment_closing_dt}</td>     <!-- 검정색-->
												 </c:if>							 
												<c:if test ="${'1'<=strDate - endDate && strDate-endDate<'3'}">
													<td style="text-align:center; color:#FF8000; ">${subdata.payment_closing_dt}</td> <!-- 주황색--> 
												 </c:if>
												 <c:if test ="${strDate - endDate<'1'}">
													<td style="text-align:center; color: #DF0101; ">${subdata.payment_closing_dt}</td>  <!--빨강색-->
												 </c:if>			
												 												 
																						 
												 												 
												 <td style="width:140px;">
												 	 <a style="cursor:pointer;" onclick="javascript:paymentComplate('${subdata.car_insurance_sub_id}','${subdata.round_of_events}','${subdata.insurance_ayment_in_installment}')" class="btn-edit">납부완료</a>
					                           </td>
									     
									     	</c:if>
			                       		</c:if>
		                       		</c:forEach> 	                           
							 </tr>
	                         <c:if test='${data.sub_cnt eq "2" }'>
	                        <tr class="ui-state-default" >
	                            <td style="text-align:center;">적재물</td>  <!--  종류-->
	                              <c:forEach var="subdata" items="${carInsuranceSubList}" varStatus="status">
	                              
	                            	<!-- 보험료-->
									<c:set var ="paymentJTotal"  value ="${fn:replace(subdata.insurance_application_contribution_first,',','') * fn:replace(subdata.round_of_events, ',','')}"/>	      
											<fmt:parseDate value="${subdata.payment_closing_dt}" var="strPlanDate" pattern="yyyy-MM-dd"/>
											<fmt:parseNumber value="${strPlanDate.time / (1000*60*60*24)}" integerOnly="true" var="strDate"></fmt:parseNumber> <!-- 납부일 -->
											<fmt:parseDate value="${paramMap.nowDate}" var="endPlanDate" pattern="yyyy-MM-dd"/>
											<fmt:parseNumber value="${endPlanDate.time / (1000*60*60*24)}" integerOnly="true" var="endDate"></fmt:parseNumber> <!-- 현재날짜 -->
												
		                            	<c:if test='${data.car_insurance_id eq subdata.car_insurance_id}'>
		                            	
			                            	<c:if test='${subdata.insurance_kind eq "J" }'>
					                            <td style="text-align:center;">100%</td> <!-- 요율 -->
					                            <td style="text-align:center;">${subdata.self_payments}</td> <!--자부담금  -->
					                            <td style="text-align:center;">${subdata.compensation_limit}</td> <!-- 가입금액 -->
					                            <td style="text-align:center;">${subdata.totalInsurance}</td><!--총보험료  -->
					                            <td style="text-align:center;">${subdata.round_of_events}</td> <!-- 회차 -->
					                            <td style="text-align:center;">${paymentJTotal}</td> <!-- 보험료 -->
					                            <td style="text-align:center;">	${subdata.payment_closing_dt} </td> <!--납부일-->
					                            
					                             <td style="width:140px;" >
					                             	<a style="cursor:pointer;" onclick="javascript:paymentComplate('${data.car_insurance_sub_id}','${data.round_of_events}','${subdata.insurance_ayment_in_installment}')" class="btn-edit">납부완료</a>
					                            </td>   
	                     					</c:if>
			                     		</c:if>
		                     		</c:forEach>
	                              
                        	</tr>
                        	</c:if>
                        	
                        
						</c:forEach>
          
                    </tbody>
                </table>
       
                
                
           
               

                
                <div class="table-pagination text-center">
                    <ul class="pagination">
                        <html:paging uri="/carmanagement/driverList.do" forGroup="&selectMonth=${paramMap.selectMonth}" />
                    </ul>
                </div>
          <%--      <div class="confirmation">
                    <div class="confirm">
                        <a href="/account/driverList_download.do?selectMonth=${paramMap.selectMonth}">엑셀다운로드</a>
                    </div>--%>
                    <%-- <div class="confirm">
                        <a href="/account/driverList_download.do?selectMonth=${paramMap.selectMonth}"></a>
                    </div> 
                </div>--%>
                
            </section>
     
