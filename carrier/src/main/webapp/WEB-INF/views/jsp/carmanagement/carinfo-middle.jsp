<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>
<%

%>


<script type="text/javascript">
$(document).ready(function(){
	
	$("#startDt").MonthPicker({ 
		Button: false
		,MonthFormat: 'yy-mm'	
		,OnAfterChooseMonth: function() { 
	        //alert($(this).val());
	        document.location.href = "/carmanagement/lowViolation-reg.do?&selectMonth="+$(this).val();
	    }
	});
	
});


var rgx1 = /\D/g;  // /[^0-9]/g 와 같은 표현
var rgx2 = /(\d+)(\d{3})/; 

function getNumber(obj){
	
    var num01;
    var num02;
    num01 = obj.value;
    num02 = num01.replace(rgx1,"");
    num01 = setComma(num02);
    obj.value =  num01;
      
}

function setComma(inNum){
    
    var outNum;
    outNum = inNum; 
    while (rgx2.test(outNum)) {
         outNum = outNum.replace(rgx2, '$1' + ',' + '$2');
     }
    return outNum;

}



function search(searchStatus,obj){
	
	document.location.href = "/carmanagement/lowViolation-reg.do?selectMonth="+$("#startDt").val();
}

function addEmp(){
	document.location.href = "/baseinfo/add-employee.do";
}

function getNumberOnly (str) {			//저장할때는 콤마 제거 후 저장한다.
    var len = str.length;
    var sReturn = "";

    for (var i=0; i<len; i++){
        if ( (str.charAt(i) >= "0") && (str.charAt(i) <= "9") ){
            sReturn += str.charAt(i);
        }
    }
    return sReturn;
}


function addDriverDeduct(driverId,obj){
	
	var deductInfo = new Object();
	
	if($("#occurrenceDt").val() == ""){
		alert("날짜가 입력 되지 않았습니다.");
		return false;
	}else{
		deductInfo.occurrence_dt = $("#occurrenceDt").val();	
	}	
	if($("#item").val() == ""){
		alert("항목이 입력 되지 않았습니다.");
		return false;
	}else{
		deductInfo.item = $("#item").next().val();	
	}
	if($("#division").val() == ""){
		alert("구분이 입력 되지 않았습니다.");
		return false;
	}else{
		deductInfo.division = $("#division").next().val();	
	}
	if($("#amount").val() == ""){
		alert("금액이 입력 되지 않았습니다.");
		return false;
	}else{
		deductInfo.amount = getNumberOnly($("#amount").val());
	}
	deductInfo.etc = encodeURI($("#etc").val());
	
	if(confirm("공제 내역을 등록 하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : "/account/insert-driver-deduct.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				deductInfo : JSON.stringify({deductInfo : deductInfo}),
				driverId : "${driver.driver_id}",
				selectMonth : "${paramMap.selectMonth}"
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					document.location.reload();
				}else if(result == "E000"){
					alert("날짜가 잘못 입력 되었습니다.");
					return false;
				}else if(result == "0001"){
					alert("조회 하는데 실패 하였습니다.");
					return false;
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
}



function addDriverPaymentMain(obj){

	//작성되어야 하는 내용이 전부 작성 되었는지 확인하는 로직 추가.....

	
	
	if($("#carNum").val() == ""){
		alert("차량번호가 입력 되지 않았습니다.");
		return false;
	}
	
	if($("#carKind").val() == ""){
		alert("차종이 입력 되지 않았습니다.");
		return false;
	}

	/* if($("#carIdNum").val() == ""){
		alert("차대번호가 입력 되지 않았습니다.");
		return false;
	} */
	
	
	if($("#etc").val() == ""){

		if(confirm("비고가 등록되지 않았습니다. 비고 없이 등록 하시겠습니까?")){
			
		}else{
			alert("취소 되었습니다.");
			return false;
		}
	}

	if(confirm("해당 내용을 등록 하시겠습니까?")){
		$("#excelForm").attr("action","/carmanagement/insertDriverPaymentMain.do");
		$("#excelForm").submit();
	}
	
		
	/* $('#excelForm').ajaxForm({
		url: "/carmanagement/insertLowViolation.do",
		enctype: "multipart/form-data", 
	    type: "POST",
		dataType: "json",		
		data : {
			driverId : driverId
	    },
		success: function(data, response, status) {
			var status = data.resultCode;
			if(status == '0000'){

				alert();
				
			}else if(status == '1111'){
						
			}
					
		},
		error: function() {
			alert("이미지 등록중 오류가 발생하였습니다.");
		}                               
	}); */


	



	
}










function deleteDriverPayment(driverPaymentId,obj){
	
	
 	if(confirm("지입료정보를 삭제 하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : "/carmanagement/deleteDriverPaymentMain.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				driverPaymentId : driverPaymentId
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("삭제 되었습니다.");
					document.location.reload();
				}else if(result == "0001"){
					alert("조회 하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
	
}


function viewLowViolation(driverId,selectMonth){


	document.location.href = "/carmanagement/viewLowViolation.do?driverId="+driverId+"&selectMonth="+selectMonth;
	
}


function viewDriverPaymentLast(driverId){

	document.location.href = "/carmanagement/carinfo-reg.do?&driverId="+driverId;
	
}



function viewPaymentDriverList(driverId,location){

	document.location.href = "/carmanagement/driverPaymentList.do?&driverId="+driverId+"&location="+location;
	
}





function editDriverPaymentMain(driverPaymentId,paymentDriverId,obj){
	
	$(obj).parent().parent().find(".normal").css("display","none");
	$(obj).parent().parent().find(".mod").css("display","");
	$(obj).css("display","none");
	$(obj).next().css("display","");
	
}


function setDriverPaymentMain(driverPaymentId,paymentDriverId,obj){
	
	
	var driverPaymentMainInfo = new Object();
	
	if($(obj).parent().parent().find(".mod").eq(0).find("input").val() == ""){
		alert("차량번호가 입력 되지 않았습니다.");
		return false;
	}else{
		driverPaymentMainInfo.car_num = $(obj).parent().parent().find(".mod").eq(0).find("input").val();	
	}	

	//alert(driverPaymentMainInfo.car_num);
	
	if($(obj).parent().parent().find(".mod").eq(1).find("input").val() == ""){
		alert("계약일이 입력 되지 않았습니다.");
		return false;
	}else{
		driverPaymentMainInfo.join_dt = $(obj).parent().parent().find(".mod").eq(1).find("input").val();	
	}

	//alert(driverPaymentMainInfo.car_kind);
	if($(obj).parent().parent().find(".mod").eq(2).find("input").val() == ""){
		if(confirm("계약종료일 입력되지 않았습니다. 그래도 진행하시겠습니까?")){
			driverPaymentMainInfo.resign_dt = $(obj).parent().parent().find(".mod").eq(2).find("input").val();
		}else{
			alert("취소 되었습니다.");
			return false;
		}
		
	}else{
		driverPaymentMainInfo.resign_dt = $(obj).parent().parent().find(".mod").eq(2).find("input").val();	
	}	
	


	//alert(driverPaymentMainInfo.car_id_num);
	if($(obj).parent().parent().find(".mod").eq(3).find("input").val() == ""){
		alert("해당월이 입력 되지 않았습니다.");
		return false;
	}else{
		driverPaymentMainInfo.payment_month = $(obj).parent().parent().find(".mod").eq(3).find("input").val();	
	}
	
	if($(obj).parent().parent().find(".mod").eq(4).find("input").val() == ""){
		alert("지입료입금액이 입력 되지 않았습니다.");
		return false;
	}else{
		driverPaymentMainInfo.driver_payment = $(obj).parent().parent().find(".mod").eq(4).find("input").val();	
	}

	if($(obj).parent().parent().find(".mod").eq(5).find("option").val() == ""){
		if(confirm("납부여부를 선택해주세요.")){
			driverPaymentMainInfo.payment_yn = $(obj).parent().parent().find(".mod").eq(5).find("option").val();
		}else{
			alert("취소 되었습니다.");
			return false;
		}
		
	}else{
		driverPaymentMainInfo.payment_yn = $(obj).parent().parent().find(".mod").eq(5).find("input").val();	
	}	
		
	
	if($(obj).parent().parent().find(".mod").eq(6).find("input").val() == ""){
		if(confirm("비고가 등록되지 않았습니다. 비고 없이 수정 하시겠습니까?")){
			driverPaymentMainInfo.etc = $(obj).parent().parent().find(".mod").eq(6).find("input").val();
		}else{
			alert("취소 되었습니다.");
			return false;
		}
		
	}else{
		driverPaymentMainInfo.etc = $(obj).parent().parent().find(".mod").eq(6).find("input").val();	
	}	

	//alert(driverPaymentMainInfo.etc);
	
 	if(confirm("차량정보를 수정 하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : "/carmanagement/updateDriverPaymentMain.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				driverPaymentMainInfo : JSON.stringify({driverPaymentMainInfo : driverPaymentMainInfo}),
				driverPaymentId : driverPaymentId,
				paymentDriverId : paymentDriverId
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("수정 되었습니다.");
					document.location.reload();
				}else if(result == "0001"){
					alert("조회 하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
	/* $(obj).parent().parent().find(".normal").css("display","");
	$(obj).parent().parent().find(".mod").css("display","none");
	$(obj).css("display","none");
	$(obj).prev().css("display",""); */
	
}



/* <td style="text-align:center;"><input type="text" placeholder="항목" name="item" id="item" onkeyup="javascript:setItem(this);"></td>
<td style="text-align:center;"><input type="text" placeholder="구분" name="division" id="division" onkeyup="javascript:setDivision(this);"></td> */

function setItem(obj){
	var val = $(obj).val();
	var num01;
    var num02;
    num01 = obj.value;
    num02 = num01.replace(rgx1,"");
    
    if(num02 != ""){
    	if(obj.value == "1"){
    		$(obj).next().val(obj.value);
    		num02 = "청구 내역 공제";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "2"){
    		$(obj).next().val(obj.value);
    		num02 = "대납 내역 공제";
    		obj.value =  num02;
    		$(obj).blur();
    	}else{
    		alert("항목 입력이 잘못 되었습니다. \r\n 1 : 청구 내역 공제, 2 : 대납 내역 공제");
    		obj.value =  "";
    	}
    }else{
    	alert("항목 입력이 잘못 되었습니다. \r\n 1 : 청구 내역 공제, 2 : 대납 내역 공제");
    	obj.value =  num02;
    }
    
    
	
}

function setDivision(obj){
	
	var val = $(obj).val();
	var num01;
    var num02;
    num01 = obj.value;
    num02 = num01.replace(rgx1,"");
    if(num02 != ""){
    	if(obj.value == "1"){
    		$(obj).next().val(obj.value);
    		num02 = "보험료";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "2"){
    		$(obj).next().val(obj.value);
    		num02 = "차량할부";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "3"){
    		$(obj).next().val(obj.value);
    		num02 = "과태료";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "4"){
    		$(obj).next().val(obj.value);
    		num02 = "미납통행료";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "5"){
    		$(obj).next().val(obj.value);
    		num02 = "사고대납금";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "6"){
    		$(obj).next().val(obj.value);
    		num02 = "기타";
    		obj.value =  num02;
    		$(obj).blur();
    	}else{
    		alert("구분 입력이 잘못 되었습니다. \r\n 1 : 보험료, 2 : 차량할부, 3 : 과태료, 4 : 미납통행료, 5 : 사고대납금, 6 : 기타");
    		obj.value =  "";
    	}
    }else{
    	alert("구문 입력이 잘못 되었습니다. \r\n 1 : 보험료, 2 : 차량할부, 3 : 과태료, 4 : 미납통행료, 5 : 사고대납금, 6 : 기타");
    	obj.value =  num02;
    }
	
}


function setDriver(driverId,driverName,carNum,phoneNum,obj){

	
	if(driverId != ""){

		$("#findDriver").val(driverName);
		
		$("#driverId").val(driverId);
		$("#driverName").val(driverName);
		$("#findCarNum").val(carNum);
		$("#carNum").val(carNum);
		$("#phoneNum").val(phoneNum);
		$("td[name=phoneNumParent]").css("display","none");
	}else{

		$("td[name=phoneNumParent]").css("display","");
		
		$("#driverId").val(driverId);
		$("#driverName").val($("#findDriver").val());
		$("#findCarNum").val(carNum);
		$("#carNum").val(carNum);
		$("#phoneNum").val(phoneNum);
	}

	$(obj).parent().parent().removeClass("active");
	
}


function setDriverByCarNum(driverId,driverName,carNum,phoneNum,obj){


	if(driverId != ""){

		$("#findDriver").val(driverName);
		
		$("#driverId").val(driverId);
		$("#driverName").val(driverName);
		$("#findCarNum").val(carNum);
		$("#carNum").val(carNum);
		$("#phoneNum").val(phoneNum);
		$("td[name=phoneNumParent]").css("display","none");
	}else{

		$("td[name=phoneNumParent]").css("display","");
		
		$("#driverId").val(driverId);
		//$("#driverName").val($("#findCarNum").val());
		//$("#findCarNum").val($("#findCarNum").val());
		$("#carNum").val($("#findCarNum").val());
		$("#phoneNum").val(phoneNum);
	}

	$(obj).parent().parent().removeClass("active");

	
}


function goDetailPage(driverPaymentMainId){

	document.location.href = "/carmanagement/driverPayment.do?driverPaymentMainId="+driverPaymentMainId;
	
}






var test;
function getAjaxData(val,obj,forPayment){

	test = obj;


	if(forPayment == "findDriver"){

			
		
			$.ajax({ 
				type: 'post' ,
				url : "/baseinfo/getDriverListAll.do" ,
				dataType : 'json' ,
				async : false,
				data : {
					driverName : val
				},
				success : function(data, textStatus, jqXHR)
				{
					
					var list = data.resultData;
					var result = "";
					//$("#searchResult").html("");
					$(obj).parent().find("div").html("");
					if(list.length > 0){
		       			for(var i=0; i<list.length; i++){
		       				result += '<div class="Wresult">';
		       				/* result += '<a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>'; */
		       				result += '<p class="result-title">기사정보</p>';
		       				var strJsonText = JSON.stringify(obj);
		    	   		    strJsonText = strJsonText.replace(/\"/gi, "'");
		       				result += '<a style="cursor:pointer;"   onclick="javascript:setDriver(\''+list[i].driver_id+'\',\''+list[i].driver_name+'\',\''+list[i].car_num+'\',\''+list[i].phone_num+'\',this);" class="result-sub"><span>'+list[i].driver_name+'</span></a>';
		       				result += '<p class="camp-type"><span>기사명:</span> <span>'+list[i].car_num+'</span></p>';	
		       				result += '<p class="camp-type"><span>차량번호:</span> <span>'+list[i].car_num+'</span></p>';	

		       				/* result += '<p class="camp-type"><span>주소:</span> <span>'+list[i].address+'</span></p>'; */	
		       				result += '<p class="camp-id"><span>Tel:</span> <span>'+list[i].phone_num+'</span></p>';
		       				result += '</div>';
		       			}
		       			result += '<div class="Wresult">';
	       				/* result += '<a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>'; */
	       				/* result += '<a style="cursor:pointer; margin-bottom:10px;" class="result-title result-sub">기사정보 직접입력</a>'; */
	       				/* result += '<a style="cursor:pointer; margin-bottom:10px;" onclick="javascript:$(this).parent().parent().removeClass(\'active\');" class="result-title result-sub">기사정보 직접입력</a>'; */
	       				result += '<a style="cursor:pointer; margin-bottom:10px;" onclick="javascript:setDriver(\'\',\'\',\'\',\'\',this);" class="result-title result-sub">기사정보 직접입력</a>';
	       				var strJsonText = JSON.stringify(obj);
	    	   		    strJsonText = strJsonText.replace(/\"/gi, "'");
	       				/* result += '<a style="cursor:pointer;"   onclick="javascript:setAddressInfo();" class="result-sub"><span></span></a>';
	       				result += '<p class="camp-type"><span>차량번호:</span> <span></span></p>';	
	       				result += '<p class="camp-type"><span>주소:</span> <span></span></p>';	
	       				result += '<p class="camp-id"><span>Tel:</span> <span></span></p>'; */
	       				result += '</div>';

					}else{

						result += '<div class="Wresult">';
	       				/* result += '<a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>'; */
	       				/* result += '<p class="result-title">기사정보 직접입력</p>'; */
	       				
	       				result += '<a style="cursor:pointer; margin-bottom:10px;" onclick="javascript:setDriver(\'\',\'\',\'\',\'\',this);" class="result-title result-sub">기사정보 직접입력</a>';
	       				
	       				/* result += '<a style="cursor:pointer; margin-bottom:10px;" onclick="javascript:$(this).parent().parent().removeClass(\'active\');" class="result-title result-sub">기사정보 직접입력</a>'; */
	       				var strJsonText = JSON.stringify(obj);
	    	   		    strJsonText = strJsonText.replace(/\"/gi, "'");
	       				result += '<a style="cursor:pointer;"   onclick="javascript:setAddressInfo();" class="result-sub"><span></span></a>';
	       				/* result += '<p class="camp-type"><span>차량번호:</span> <span></span></p>';	
	       				result += '<p class="camp-type"><span>주소:</span> <span></span></p>';	
	       				result += '<p class="camp-id"><span>Tel:</span> <span></span></p>'; */
	       				result += '</div>';
						//$(test).parent().find("div").removeClass('active');
					}
					
					//$("#searchResult").html(result);
					$(obj).parent().find("div").html(result);
					
				} ,
				error : function(xhRequest, ErrorText, thrownError) {
				}
			});



	}else if(forPayment == "findCarNum"){

		$.ajax({ 
			type: 'post' ,
			url : "/baseinfo/getDriverListAllByCarNum.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				carNum : val
			},
			success : function(data, textStatus, jqXHR)
			{
				
				var list = data.resultData;
				var result = "";
				//$("#searchResult").html("");
				$(obj).parent().find("div").html("");
				if(list.length > 0){
	       			for(var i=0; i<list.length; i++){
	       				result += '<div class="Wresult">';
	       				/* result += '<a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>'; */
	       				result += '<p class="result-title">기사정보</p>';
	       				var strJsonText = JSON.stringify(obj);
	    	   		    strJsonText = strJsonText.replace(/\"/gi, "'");
	       				result += '<a style="cursor:pointer;"   onclick="javascript:setDriverByCarNum(\''+list[i].driver_id+'\',\''+list[i].driver_name+'\',\''+list[i].car_num+'\',\''+list[i].phone_num+'\',this);" class="result-sub"><span>'+list[i].driver_name+'</span></a>';
	       				result += '<p class="camp-type"><span>차량번호:</span> <span>'+list[i].car_num+'</span></p>';	
	       				/* result += '<p class="camp-type"><span>주소:</span> <span>'+list[i].address+'</span></p>'; */	
	       				result += '<p class="camp-id"><span>Tel:</span> <span>'+list[i].phone_num+'</span></p>';
	       				result += '</div>';
	       			}
	       			result += '<div class="Wresult">';
       				/* result += '<a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>'; */
       				/* result += '<a style="cursor:pointer; margin-bottom:10px;" class="result-title result-sub">기사정보 직접입력</a>'; */
       				/* result += '<a style="cursor:pointer; margin-bottom:10px;" onclick="javascript:$(this).parent().parent().removeClass(\'active\');" class="result-title result-sub">기사정보 직접입력</a>'; */
       				result += '<a style="cursor:pointer; margin-bottom:10px;" onclick="javascript:setDriverByCarNum(\'\',\'\',\'\',\'\',this);" class="result-title result-sub">기사정보 직접입력</a>';
       				var strJsonText = JSON.stringify(obj);
    	   		    strJsonText = strJsonText.replace(/\"/gi, "'");
       				/* result += '<a style="cursor:pointer;"   onclick="javascript:setAddressInfo();" class="result-sub"><span></span></a>';
       				result += '<p class="camp-type"><span>차량번호:</span> <span></span></p>';	
       				result += '<p class="camp-type"><span>주소:</span> <span></span></p>';	
       				result += '<p class="camp-id"><span>Tel:</span> <span></span></p>'; */
       				result += '</div>';

				}else{

					result += '<div class="Wresult">';
       				/* result += '<a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>'; */
       				/* result += '<p class="result-title">기사정보 직접입력</p>'; */
       				
       				result += '<a style="cursor:pointer; margin-bottom:10px;" onclick="javascript:setDriverByCarNum(\'\',\'\',\'\',\'\',this);" class="result-title result-sub">기사정보 직접입력</a>';
       				
       				/* result += '<a style="cursor:pointer; margin-bottom:10px;" onclick="javascript:$(this).parent().parent().removeClass(\'active\');" class="result-title result-sub">기사정보 직접입력</a>'; */
       				var strJsonText = JSON.stringify(obj);
    	   		    strJsonText = strJsonText.replace(/\"/gi, "'");
       				result += '<a style="cursor:pointer;"   onclick="javascript:setAddressInfo();" class="result-sub"><span></span></a>';
       				/* result += '<p class="camp-type"><span>차량번호:</span> <span></span></p>';	
       				result += '<p class="camp-type"><span>주소:</span> <span></span></p>';	
       				result += '<p class="camp-id"><span>Tel:</span> <span></span></p>'; */
       				result += '</div>';
					//$(test).parent().find("div").removeClass('active');
				}
				
				//$("#searchResult").html(result);
				$(obj).parent().find("div").html(result);
				
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});




	}









	
}


function addDriverPayment(obj,driverId,remainAmount,location){

	//작성되어야 하는 내용이 전부 작성 되었는지 확인하는 로직 추가.....

	
	//입금일 입력여부 확인
	
	if($(obj).parent().prev().prev().prev().find("input").val() == ""){
		alert("입금일이 입력 되지 않았습니다.");
		return false;
	}
	
	//입금액 입력여부 확인
	if($(obj).parent().prev().prev().find("input").val() == ""){
		alert("입금액이 입력 되지 않았습니다.");
		return false;
	}	

	//alert("잔액은="+getNumberOnly($(obj).parent().prev().prev().find("input").val()));
	//alert("입금액은="+getNumberOnly(remainAmount));

	
	//잔액보다 입금액이 크면 경고
	if(Number(getNumberOnly($(obj).parent().prev().prev().find("input").val())) > Number(getNumberOnly(remainAmount))){
		alert("입금액이 잔액보다 클 수 없습니다.");
		return false;
	}

	var paymentInfo = new Object();
	paymentInfo.driverId = driverId;
	paymentInfo.paymentDt = $(obj).parent().prev().prev().prev().find("input").val();
	paymentInfo.driverPaymentI = getNumberOnly($(obj).parent().prev().prev().find("input").val());
	paymentInfo.paymentBalance = getNumberOnly(remainAmount);

	if(confirm("지입료 내역을 등록 하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : "/carmanagement/insertDriverPayment.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				paymentInfo : JSON.stringify({paymentInfo : paymentInfo}),
				businessLicenseNumber : "${paramMap.businessLicenseNumber}",
				location : "${paramMap.location}",
				driverId : driverId
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("등록 되었습니다.");
					document.location.reload();
				}else if(result == "E000"){
					alert("날짜가 잘못 입력 되었습니다.");
					return false;
				}else if(result == "0001"){
					alert("조회 하는데 실패 하였습니다.");
					return false;
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}


	
	/* if(confirm("해당 내용을 등록 하시겠습니까?")){
		$("#excelForm").attr("action","/carmanagement/insertDriverPaymentMain.do");
		$("#excelForm").submit();
	} */
	

}
















</script>

		<!-- <section class="side-nav"> -->
            <!-- <ul>
                <li class="active"><a href="/baseinfo/employee.do">직원관리</a></li>
                <li class=""><a href="/baseinfo/driver.do">기사관리</a></li>
                <li class=""><a href="/baseinfo/customer.do">거래처관리</a></li>
                <li class=""><a href="/baseinfo/company.do">회사정보관리</a></li>
            </ul> -->
        <!-- </section> -->

        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">차량정보 관리</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">등록</a></li>
                </ul>
            </div>
 		<br>
 		<div class="dispatch-btn-container">
				<div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">(지입료) 차량 관리</div>
            </div>
        
        <div class="up-dl clearfix header-search">
                 <table>
                    <tbody>
                        <tr>
                        	<td>조회 기간 :</td>
				                 <td class="widthAuto" style="width:500px;">
				            
				                <input style="width:40%; text-align:center;" class="datepick" type="text" placeholder="조회시작일" onclick="javascript:$(this).val('');"  readonly="readonly" name="startDt" id="startDt" value="${paramMap.startDt}">&nbsp;~&nbsp;
				                <input style="width:40%; text-align:center;" class="datepick" type="text" placeholder="조회종료일" onclick="javascript:$(this).val('');" readonly="readonly" name="endDt" id="endDt"  value="${paramMap.endDt}">
				            </td>
                            <td>
                                <input type="button" id="btn-search" value="검색" class="btn-primary" onclick="javascript:search();">
                            </td>
                            
                            
                        </tr>
                    </tbody>
                </table>
            </div>

        </section>

            <section class="bottom-table" style="width:1600px; margin-left:290px;">
            <%-- <table class="article-table" style="margin-top:15px; table-layout:fixed;">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <td style="text-align:center; width:100px;">차대번호</td>
                        	<td style="text-align:center; width:100px;">기사명</td>
                            <td style="text-align:center; width:100px;">계약일</td>
                            <td style="text-align:center; width:100px;">월지입료</td>
                            <td style="text-align:center; width:100px;">기초잔액</td>
                            <td style="text-align:center; width:100px;">청구액</td>
                            <td style="text-align:center; width:100px;">입금액</td>
                            <td style="text-align:center; width:100px;">잔액</td>
                            <td style="text-align:center; width:100px;">입금일</td>
                            <td style="text-align:center; width:100px;">입금액</td>
                            <td style="text-align:center; width:100px;">등록</td>
                        </tr>
                    </thead>
                    <tbody id="dataArea">
	                   	
    	                	<tr class="ui-state-default">
	                    		
	                    		<form id="excelForm" style="display:none" name="excelForm" method="post" enctype="multipart/form-data" autocomplete="false">
			                   		<input type="hidden" style=""  name="driverId" value="${driver.driver_id}">
			                    	<input type="hidden" style=""  name="driverName" value="${driver.driver_name}">
			                    	<td style="text-align:center;">${driver.car_id_num}</td>
			                    	<td style="text-align:center;">${driver.driver_name}</td>
			                    	<td style="text-align:center;">${driver.join_dt}</td>
			                    	<td style="text-align:center;">${driver.driver_balance}</td>
			                    	<td style="text-align:center;">${driver.driver_balance}</td>
			                    	<td style="text-align:center;">청구액</td>
			                    	<td style="text-align:center;">입금액</td>
			                    	<td style="text-align:center;">잔액</td>
			                    	<td style="text-align:center;"><input style="width:80%;" class="datepick" type="text" placeholder="입금일" name="etc"></td>
			                    	<td style="text-align:center;"><input style="width:80%;" type="text" placeholder="입금액" name="etc"></td>
			                    	<td style="text-align:center;">
			                    		<a style="cursor:pointer; width:60px;" onclick="javascript:addDriverPaymentMain(this)" class="btn-edit">등록</a>
			                    	</td>
		                    	</form>
	                    	</tr>
                    	
                    
                    </tbody>
                </table> --%>
                
                <table class="article-table" style="margin-top:15px">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <!-- <td style="text-align:center;">기사아이디</td> -->
                            <!-- <td style="text-align:center; width:50px;"><input type="checkbox" id="checkAll" onclick="javascript:checkAll();" ></td> -->
                            <td style="text-align:center; width:100px;">소유자명</td>
                            <td style="text-align:center; width:100px;">차량번호</td>
                            <td style="text-align:center; width:100px;">차대번호</td>
                            <td style="text-align:center; width:100px;">기사이름</td>
	                        <td style="text-align:center; width:100px;">계약 일</td>
                            <td style="text-align:center; width:100px;">월 지입료</td>
                            <td style="text-align:center; width:100px;">기초잔액</td>
                            <td style="text-align:center; width:100px;">청구액</td>
                            <td style="text-align:center; width:100px;">잔액</td>
                            <td style="text-align:center; width:100px;">입금일</td>
                            <td style="text-align:center; width:100px;">입금액</td>
                            <td style="text-align:center; width:50px;">관리</td>
                            <td style="text-align:center; width:50px;">관리</td>
                        </tr>
                    </thead>
                    <tbody id="">
                    	<c:forEach var="data" items="${listData}" varStatus="status">
							<tr class="ui-state-default" > 
							     	
	                            <td style="text-align:center; cursor:pointer;" onclick="javascript:viewPaymentDriverList('${data.driver_id}','${data.location}');">
		                            	${data.driver_owner}
	                            </td>
	                            
	                            <td style="text-align:center; cursor:pointer;" onclick="javascript:viewPaymentDriverList('${data.driver_id}','${data.location}');">
		                            	${data.car_num}
	                            </td>	
	                           <td style="text-align:center; cursor:pointer;" onclick="javascript:viewPaymentDriverList('${data.driver_id}','${data.location}');">
		                            	${data.car_id_num}
	                            </td>
	                            
	                            <td style="text-align:center; cursor:pointer;" onclick="javascript:viewPaymentDriverList('${data.driver_id}','${data.location}');">
		                            	${data.driver_name}
	                            </td>
	                       
	                            <td style="text-align:center; cursor:pointer;" onclick="javascript:viewPaymentDriverList('${data.driver_id}','${data.location}');">
		                            	${data.join_dt}
	                            </td>
	                            
	                            <td style="text-align:center; cursor:pointer;" onclick="javascript:viewPaymentDriverList('${data.driver_id}','${data.location}');">
		                            	${data.driver_balance}
	                            </td>
	                       
	                          <td style="text-align:center; cursor:pointer;" onclick="javascript:viewPaymentDriverList('${data.driver_id}','${data.location}');">
		                            	${data.driver_balance}
	                            </td>
	                            
	                              <td style="text-align:center; cursor:pointer;" onclick="javascript:viewPaymentDriverList('${data.driver_id}','${data.location}');">
		                            	${data.total_amount}
	                            </td>
	                    
	                            <td style="text-align:center; cursor:pointer;" onclick="javascript:viewPaymentDriverList('${data.driver_id}','${data.location}');">
	                            
	                         	${data.remain_amount}
	                            </td>
	                    
	                         <td style="text-align:center;">
	                        <input style=" width:80%;" class="datepick" type="text" placeholder="입금일" value="" onclick="javascript:$(this).val('');">
	                            </td>
	                                 <td style="text-align:center;">
	                   				<input style=" width:80%;" type="text" onkeyup="javascript:getNumber(this);" placeholder="입금액" value="" onclick="javascript:$(this).val('');">
	                            </td>
	                            
	                            
	                             <td style="text-align:center; cursor:pointer;">
		                            	${user.emp_name}	
	                            </td>
	                                    
	                           <%--  <td style="text-align:center;">
	                            	<a style="cursor:pointer; width:50px;" onclick="javascript:editDriverPaymentMain('${data.driver_payment_id}','${data.payment_driver_id}',this)" class="btn-edit">수정</a>
	                            	<a style="display:none; cursor:pointer; width:50px;" onclick="javascript:setDriverPaymentMain('${data.driver_payment_id}','${data.payment_driver_id}',this)"  class="btn-edit">완료</a>
	                            	<a style="cursor:pointer; width:50px;" onclick="javascript:deleteDriverPayment('${data.driver_payment_id}',this)" class="btn-edit">삭제</a>
	                            </td> --%>
	                            
	                              	<td style="text-align:center;">
			                    		<a style="cursor:pointer; width:60px;" onclick="javascript:addDriverPayment(this,'${data.driver_id}','${data.remain_amount}','${data.location}')" class="btn-edit">등록</a>
			                    	</td>
                        	</tr>
						</c:forEach>
                        
                    </tbody>
                </table>
                
                
                <div class="table-pagination text-center">
                    <ul class="pagination">
                        <!-- <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="curr-page"><a href="#">1</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li> -->
                        <html:paging uri="/carmanagement/carinfo-middle.do" forGroup="" />
                    </ul>
                </div>
                <div class="confirmation">
                    <div class="confirm">
                        <!-- <a href="/baseinfo/add-employee.do">직원등록</a> -->
                    </div>
                </div>
                
            </section>
     
