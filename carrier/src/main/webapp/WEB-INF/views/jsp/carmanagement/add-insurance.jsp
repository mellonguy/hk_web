<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%

%>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script type="text/javascript">
$(document).ready(function(){

});

function addInsurance(){



		
	
		if($("#driverName").val() == ""){
			alert("기사명이 입력되지 않았습니다.");
			return false;
		}
		if($("#carNum").val() == ""){
			alert("차량번호가 입력되지 않았습니다.");
			return false;
		}
		
		if($("#carIdNum").val() == ""){
			alert("차대번호가 입력되지 않았습니다.");
			return false;
		}
		
		if($("#carMaximumLoadCapacity").val() == ""){
			alert("최대적재량이 입력되지 않았습니다.");
			return false;
		}
		
		if($("#integratedInsuranceRegisterDt").val() == ""){
			alert("통합보험 가입일이 입력되지 않았습니다.");
			return false;
		}
		
		if($("#rate").val() == ""){
			alert("요율이 입력되지 않았습니다.");
			return false;
		}
		
		if($("#insuranceApplicationContributionFirst").val() == ""){
			alert("통합보험 적용분담금_1회가 입력되지 않았습니다.");
			return false;
		}
	
		if($("#barrierRewardDistribution").val() == ""){
			alert("대물배상한도가 입력되지 않았습니다.");
			return false;
		}
		if($("#selfAccident").val() == ""){
			alert("자기신체사고 구분이 입력되지 않았습니다.");
			return false;
		}
		if($("#selfVehicleDamaget").val() == ""){
			alert("자기차량손해 구분이 입력되지 않았습니다.");
			return false;
		}
		if($("#legalExpensesSupportSpecial").val() == ""){
			alert("법률비용지원특약 구분이 입력되지 않았습니다.");
			return false;
		}
		if($("#urgentDispatch").val() == ""){
			alert("긴급출동 구분이 입력되지 않았습니다.");
			return false;
		}
			
		if($("#integratedInsuranceAymentInInstallment").val() == ""){
			alert("통합보험 분할납입특약 구분이 입력되지 않았습니다.");
			return false;
		}

			var loadInsurance = "N";
		
			if($("#loadInsuranceRegisterDt").val() != ""){
				
				loadInsurance = "Y";
			}

	
			$.ajax({ 
				type: 'post' ,
				url : "/carmanagement/insert-Tolerance.do" ,
				dataType : 'json' ,	
				data : {
					gongCarNum :$("#carNum").val(),
			
				},
				success : function(data, textStatus, jqXHR)
				{
					var result = data.resultCode;
					var resultData = data.resultData;
					if(result == "0000"){
		
						goInsertFormSubmit('U', loadInsurance);

					}else if(result == "E001"){

						goInsertFormSubmit('A', loadInsurance);
		
					}else if(result == "E002"){

						alert("알 수 없는 에러입니다 관리자에게 문의 해주세요.");
					}
				} ,
				error : function(xhRequest, ErrorText, thrownError) {
				}
			});
	
		}
		



function goInsertFormSubmit(newORaddInsurance,loadInsurance){

	if(newORaddInsurance == "U"){
	
		if(confirm("입력하신 차량번호가 등록되어 있습니다. 계속해서 등록 하시겠습니까?")){
			$("#insertForm").attr("action","/carmanagement/insert-add-insurance.do?&newORaddInsurance="+newORaddInsurance+"&loadInsurance="+loadInsurance);
			$("#insertForm").submit();	

		}

	}else{
		
		
		if(confirm("등록 하시겠습니까?")){
			$("#insertForm").attr("action","/carmanagement/insert-add-insurance.do?&newORaddInsurance="+newORaddInsurance+"&loadInsurance="+loadInsurance);
			$("#insertForm").submit();	

		}

	}

	
}



//공차등록
function goInsertInsurance(gongCarNum,gongStartRegisterDate,gongFininshRegisterDate){


	var Tolerance ="Y";
	
	document.location.href ="/carmanagement/insert-add-insurance.do?&gongCarNum="+gongCarNum+"&gongStartRegisterDate="+gongStartRegisterDate+"&gongFininshRegisterDate="+gongFininshRegisterDate+"&Tolerance="+Tolerance;
	
	
}







</script>
		<!-- <section class="side-nav">
            <ul>
                <li class=""><a href="/baseinfo/employee.do">직원관리</a></li>
                <li class="active"><a href="/baseinfo/driver.do">기사관리</a></li>
                <li class=""><a href="/baseinfo/customer.do">거래처관리</a></li>
                <li class=""><a href="/baseinfo/company.do">회사정보관리</a></li>
            </ul>
        </section> -->
     <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">HOME</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">차량보험 등록</a></li>
                </ul>
            </div>

            <div class="up-dl clearfix header-search">
                 <table>
                    <tbody>
                        <tr>
                            <td style="width: 145px;">
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">선택바
                                    <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">HTML</a></li>
                                        <li><a href="#">CSS</a></li>
                                        <li><a href="#">JavaScript</a></li>
                                    </ul>
                                </div>
                            </td>
                            <td style="width: 190px;">
                               <form id="" action=""  method="post" enctype="multipart/form-data">
                                    <input type="text" class="search-box">
                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
                                    <div class="search-result">
                                        <!-- No result -->
                                        <div class="no-result d-table">
                                            <div class="d-tbc">
                                                <i class="fa fa-exclamation-triangle fa-3x" aria-hidden="true"></i>
                                                <span>No results have been found.</span>
                                            </div>
                                        </div>

                                        <!-- Result type1 -->
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Campaigns</p>
                                            <a href="" class="result-sub"><span>네오퓨쳐</span></a>
                                            <p class="camp-type"><span>Campaign type:</span> <span>Powerlink</span></p>
                                            <p class="camp-id"><span>Campaign ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                        <!-- Result type2 -->
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Ad groups</p>
                                            <a href="" class="result-sub"><span>네오퓨쳐</span>_#0007</a>
                                            <p class="camp-type"><span>Campaign type:</span> <span>Powerlink</span></p>
                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
                                            <p class="adgroup-id"><span>Ad group ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                        <!-- Result type3 -->
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Ads - Powerlink</p>
                                            <div class="result-box">
                                                <p class="title">네오퓨쳐</p>
                                                <p class="content">일반 홈페이지에서 신개념 융복합 멀티미디어 홈피이지 제작, 견적상담 환영</p>

                                                <ul>
                                                    <li>View URL: http://www.neofss.com</li>
                                                    <li>Linked URL: http://www.neofss.com</li>
                                                </ul>
                                            </div>
                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
                                            <p class="adgroup"><span>Ad group:</span> <span class="red">네오퓨쳐</span>_#0003</p>
                                            <p class="adgroup-id"><span>Ad ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                        <!-- Result type4 -->
                                        <div class="Wresult">
                                            <a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            <p class="result-title">Ads - PowerContents</p>
                                            <div class="result-box w-img">
                                                <div class="d-table">
                                                    <div class="d-tbc">
                                                        <img src="/img/ads-img.png" alt="">
                                                    </div>
                                                    <div class="d-tbc">
                                                        <p class="title">홈페이지제작 이러면 망한다!</p>
                                                        <p class="content">홈페이지제작! 대충 업체에 맡기면 된다는 생각으로 접근하면, 십중팔구는 실패하는 이유와 홈페이지 제작에 관한 최소한의 지식은 가지고 업체를 선정해야 돈과 시간을 낭비하는 일을 줄일수 있다.</p>
                                                    </div>
                                                </div>
                                                <ul>
                                                    <li>Content created: 2017-12-15</li>
                                                    <li>Brand Name: 네오퓨쳐</li>
                                                    <li>View URL: http://blog.naver.com/marujeen</li>
                                                    <li>Linked URL: http://blog.naver.com/marujeen</li>
                                                </ul>
                                            </div>
                                            <p class="camp"><span>Campaign:</span> <span class="red">네오퓨쳐</span></p>
                                            <p class="adgroup"><span>Ad group:</span> <span class="red">네오퓨쳐</span>_#0003</p>
                                            <p class="adgroup-id"><span>Ad ID:</span> <span>cmp-m002-01-00000003990602131232321331232131</span></p>
                                        </div>

                                    </div>
                                </form>
                            </td>
                            <td>
                                <input type="button" id="btn-search" value="검색" class="btn-primary">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
<!-- 
            <div class="dispatch-btn-container">
               
                <div class="dispatch-btn">
                    <i class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div>
            </div> -->
        </section>

        <div class="dispatch-wrapper">

            <section class="dispatch-bottom-content active" style=" margin:auto;">
               <form id="insertForm" action="/carmanagement/insert-add-insurance.do"  method="post" enctype="multipart/form-data">
                   <h3 style="font-size: 20px; text-align: center; margin-bottom:20px; font-weight: 700; font-family: NotoSansBold,sans-serif;">차량보험 등록</h3>
                
                <div class="form-con clearfix" style="max-width:1200px;">
                	<div class="column-sub" style="margin-bottom:5px">
                
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>차량번호<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="차량번호" name="carNum" id="carNum">
	                            </td>
	                            
	                        </tr>
	                    </tbody>
	                </table>
	       
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>기사명<span style="color:#F00;">&nbsp;*</span></td>
	                   		      <td class="widthAuto" style="">
	                                <input type="text" placeholder="기사명" name="driverName" id="driverName">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>차대번호<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="차대번호" name="carIdNum" id="carIdNum">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>최대적재량<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="최대적재량" name="carMaximumLoadCapacity" id="carMaximumLoadCapacity">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                 
	                </div>
	                
	                <div class="column-sub">
	                
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>통합 보험 가입일<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text"  class="datepick" placeholder="통합 보험 가입일" name="insuranceRegisterDt" id="integratedInsuranceRegisterDt">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>요율<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="요율" name="rate" id="rate">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>적용분담금 1회<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="적용부담금 1회" name="integratedInsuranceApplicationContributionFirst" id="integratedInsuranceApplicationContributionFirst">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
		                <tbody>
			                <tr>
				                <td>적용분담금 2회~6회</td>
				                <td class="widthAuto" style="">
	                                <input type="text" placeholder="적용부담금 2회" name="integratedInsuranceApplicationContributionNext" id="integratedInsuranceApplicationContributionNext">
	                            </td>
			                </tr>
		                </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>대물배상한도<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                <input type="text"  placeholder="대물배상한도"  name="barrierRewardDistribution" id="barrierRewardDistribution">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>자기신체사고<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                  <div class="select-con">
								        <select class="dropdown" name="selfAccident" id="selfAccident">
								        	<option value="N" >미가입</option>
								            <option value="Y" >가입</option>
							        	</select>
								        <span></span>
								    </div>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>자기차량손해<span style="color:#F00;">&nbsp;*</span></td>
	                            <td class="widthAuto" style="">
	                                   <div class="select-con">
								        <select class="dropdown" name="selfVehicleDamaget" id="selfVehicleDamaget">
								        	<option value="N" >미가입</option>
								            <option value="Y" >가입</option>
							        	</select>
								        <span></span>
								    </div>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                
	                <table>
                          <tbody>
                              <tr>
                                  <td>법률비용지원특약<span style="color:#F00;">&nbsp;*</span></td>
                                     <td class="widthAuto" style="">
	                                   <div class="select-con">
								        <select class="dropdown" name="legalExpensesSupportSpecial" id="legalExpensesSupportSpecial">
								        	<option value="N" >미가입</option>
								            <option value="Y" >가입</option>
							        	</select>
								        <span></span>
								    </div>
	                            </td>
                              </tr>
                          </tbody>
                      </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>긴급출동<span style="color:#F00;">&nbsp;*</span></td>
	                                 <td class="widthAuto" style="">
	                                   <div class="select-con">
								        <select class="dropdown" name="urgentDispatch" id="urgentDispatch">
								        	<option value="N" >미가입</option>
								            <option value="Y" >가입</option>
							        	</select>
								        <span></span>
								    </div>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>분할납입특약<span style="color:#F00;">&nbsp;*</span></td>
	                                 <td class="widthAuto" style="">
	                                   <div class="select-con">
								        <select class="dropdown" name="integratedInsuranceAymentInInstallment" id="integratedInsuranceAymentInInstallment">
								        	<option value="12" >일시납</option>
								            <option value="6" >2회납</option>
								            <option value="2" >6회납</option>
							        	</select>
								        <span></span>
								    </div>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                
	                </div>
	                
	                <div class="column-sub">
	                
	           <table>
	                    <tbody>
	                        <tr>
	                            <td>적재물 보험 가입일</td>
	                            <td class="widthAuto" style="">
	                                <input type="text"  class="datepick" placeholder="적재물 보험 가입일" name="insuranceRegisterDt2" id="loadInsuranceRegisterDt">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>보상한도</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="보상한도" name="compensationLimit" id="compensationLimit">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>적용분담금 1회</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="적용분담금 1회" name="insuranceApplicationContributionFirst2" id="loadInsuranceApplicationContributionFirst">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>적용분담금 2회~6회</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="적용분담금 2회~6회" name="insuranceApplicationContributionNext2" id="loadInsuranceApplicationContributionNext">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>특약사항</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="특약사항" name="selfPayments" id="selfPayments">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <table>
	                    <tbody>
	                        <tr>
	                            <td>자기부담금</td>
	                            <td class="widthAuto" style="">
	                                <input type="text" placeholder="자기부담금" name="specialOffers" id="specialOffers">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                     <table>
	                    <tbody>
	                        <tr>
	                            <td>분할납입특약</td>
	                                 <td class="widthAuto" style="">
	                                   <div class="select-con">
								        <select class="dropdown" name="insuranceAymentInInstallment2" id="loadInsuranceAymentInInstallment">
								        	<option value="12" >일시납</option>
								            <option value="6" >2회납</option>
								            <option value="2" >6회납</option>
							        	</select>
								        <span></span>
								    </div>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	            
	           
	                </div>
	                
	               
	                
                
                </div>
                
                
                <div class="confirmation">

	                <div class="confirm">
	                	<input type="button" value="차량보험 등록" onclick="javascript:addInsurance();">
	                </div>
    		
	                
				 <!-- 
	                	<div class="confirm">
	                		<input type="button" value="공차 등록" onclick="javascript:addInsurance('Y');">
	                	</div>
			       -->
			            
                </div>
                </form>
                <!-- <div class="table-pagination text-center">
                    <ul class="pagination">
                        <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="curr-page"><a href="#">1</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
                    </ul>
                </div> -->
            </section>
        </div>
