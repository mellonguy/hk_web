<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>
<%

%>


<script type="text/javascript">
$(document).ready(function(){
	
/* 	$("#selectMonth").MonthPicker({ 
		Button: false
		,MonthFormat: 'yy-mm',	
	}); */
	
});


var rgx1 = /\D/g;  // /[^0-9]/g 와 같은 표현
var rgx2 = /(\d+)(\d{3})/; 

function getNumber(obj){
	
    var num01;
    var num02;
    num01 = obj.value;
    num02 = num01.replace(rgx1,"");
    num01 = setComma(num02);
    obj.value =  num01;
      
}

function setComma(inNum){
    
    var outNum;
    outNum = inNum; 
    while (rgx2.test(outNum)) {
         outNum = outNum.replace(rgx2, '$1' + ',' + '$2');
     }
    return outNum;

}



function search(searchStatus,obj){

	
	// document.location.href = "/carmanagement/viewLowViolation.do?selectMonth="+$("#selectMonth").val()+"&driverId=${paramMap.driverId}";
	document.location.href = "/carmanagement/viewLowViolation.do?driverId="+"${paramMap.driverId}"+"&startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val();
}

function addEmp(){
	document.location.href = "/baseinfo/add-employee.do";
}

function getNumberOnly (str) {			//저장할때는 콤마 제거 후 저장한다.
    var len = str.length;
    var sReturn = "";

    for (var i=0; i<len; i++){
        if ( (str.charAt(i) >= "0") && (str.charAt(i) <= "9") ){
            sReturn += str.charAt(i);
        }
    }
    return sReturn;
}


function addDriverDeduct(driverId,obj){
	
	var deductInfo = new Object();
	
	if($("#occurrenceDt").val() == ""){
		alert("날짜가 입력 되지 않았습니다.");
		return false;
	}else{
		deductInfo.occurrence_dt = $("#occurrenceDt").val();	
	}	
	if($("#item").val() == ""){
		alert("항목이 입력 되지 않았습니다.");
		return false;
	}else{
		deductInfo.item = $("#item").next().val();	
	}
	if($("#division").val() == ""){
		alert("구분이 입력 되지 않았습니다.");
		return false;
	}else{
		deductInfo.division = $("#division").next().val();	
	}
	if($("#amount").val() == ""){
		alert("금액이 입력 되지 않았습니다.");
		return false;
	}else{
		deductInfo.amount = getNumberOnly($("#amount").val());
	}
	deductInfo.etc = encodeURI($("#etc").val());
	
	if(confirm("공제 내역을 등록 하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : "/account/insert-driver-deduct.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				deductInfo : JSON.stringify({deductInfo : deductInfo}),
				driverId : "${driver.driver_id}",
				selectMonth : "${paramMap.selectMonth}"
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					document.location.reload();
				}else if(result == "E000"){
					alert("날짜가 잘못 입력 되었습니다.");
					return false;
				}else if(result == "0001"){
					alert("조회 하는데 실패 하였습니다.");
					return false;
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
}



function addLowViolation(driverId,obj){

	//작성되어야 하는 내용이 전부 작성 되었는지 확인하는 로직 추가.....

	
	if($("#occurrenceDt").val() == ""){
		alert("위반일자가 입력 되지 않았습니다.");
		return false;
	}
	if($("#occurrenceTime").val() == ""){
		alert("위반시간이 입력 되지 않았습니다.");
		return false;
	}
	if($("#carNum").val() == ""){
		alert("차량번호가 입력 되지 않았습니다.");
		return false;
	}
	if($("#placeAddress").val() == ""){
		alert("위반장소가 입력 되지 않았습니다.");
		return false;
	}
	if($("#summary").val() == ""){
		alert("위반내용이 입력 되지 않았습니다.");
		return false;
	}
	if($("#amount").val() == ""){
		alert("금액이 입력 되지 않았습니다.");
		return false;
	}
	if($("#endDt").val() == ""){
		alert("납부마감기간이 입력 되지 않았습니다.");
		return false;
	}
	
	if($("#bbsFile").val() == ""){

		if(confirm("파일이 등록되지 않았습니다. 파일 없이 등록 하시겠습니까?")){
			
		}else{
			alert("취소 되었습니다.");
			return false;
		}
	}

	if(confirm("해당 내용을 등록 하시겠습니까?")){
		$("#excelForm").attr("action","/carmanagement/insertLowViolation.do");
		$("#excelForm").submit();
	}
	
		
	/* $('#excelForm').ajaxForm({
		url: "/carmanagement/insertLowViolation.do",
		enctype: "multipart/form-data", 
	    type: "POST",
		dataType: "json",		
		data : {
			driverId : driverId
	    },
		success: function(data, response, status) {
			var status = data.resultCode;
			if(status == '0000'){

				alert();
				
			}else if(status == '1111'){
						
			}
					
		},
		error: function() {
			alert("이미지 등록중 오류가 발생하였습니다.");
		}                               
	}); */



	



	
}










function deleteLowViolation(lowViolationId,driverId,obj){
	
	
 	if(confirm("공제 내역을 삭제 하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : "/carmanagement/delete-lowViolation.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				driverId : "${paramMap.driverId}",
				lowViolationId : lowViolationId
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("삭제 되었습니다.");
					document.location.reload();
				}else if(result == "0001"){
					alert("조회 하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
	
}



function editLowViolation(lowViolationId,driverId,obj){
	
	$(obj).parent().parent().find(".normal").css("display","none");
	$(obj).parent().parent().find(".mod").css("display","");
	$(obj).css("display","none");
	$(obj).next().css("display","");
	
}


function setLowViolation(lowViolationId,driverId,obj){
	
	
	var lowViolationInfo = new Object();

	if($(obj).parent().parent().find(".mod").eq(0).find("input").val() == ""){
		alert("위반일자가 입력 되지 않았습니다.");
		return false;
	}else{
		//lowViolationInfo.occurrence_dt = $(obj).parent().parent().find(".mod").eq(0).find("input").eq(0).val();	
		//lowViolationInfo.phone_num = $(obj).parent().parent().find(".mod").eq(0).find("input").eq(1).val();
		$("#occurrenceDt").val($(obj).parent().parent().find(".mod").eq(0).find("input").eq(0).val());
		$("#phoneNum").val($(obj).parent().parent().find(".mod").eq(0).find("input").eq(1).val());
		
	}	
	if($(obj).parent().parent().find(".mod").eq(1).find("input").val() == ""){
		alert("위반시간이 입력 되지 않았습니다.");
		return false;
	}else{
		//lowViolationInfo.occurrence_time = $(obj).parent().parent().find(".mod").eq(1).find("input").val();	
		$("#occurrenceTime").val($(obj).parent().parent().find(".mod").eq(1).find("input").val());
	
	}

	if($(obj).parent().parent().find(".mod").eq(2).find("input").val() == ""){
		alert("차량번호가 입력 되지 않았습니다.");
		return false;
	}else{
		//lowViolationInfo.car_num = $(obj).parent().parent().find(".mod").eq(2).find("input").val();	
		$("#carNum").val($(obj).parent().parent().find(".mod").eq(2).find("input").val());
	
	}

	if($(obj).parent().parent().find(".mod").eq(3).find("input").val() == ""){
		alert("위반장소가 입력 되지 않았습니다.");
		return false;
	}else{
		//lowViolationInfo.place_address = $(obj).parent().parent().find(".mod").eq(3).find("input").val();	
		$("#placeAddress").val($(obj).parent().parent().find(".mod").eq(3).find("input").val());
		
	}
	if($(obj).parent().parent().find(".mod").eq(4).find("input").val() == ""){
		alert("위반내용이 입력 되지 않았습니다.");
		return false;
	}else{
		//lowViolationInfo.summary = $(obj).parent().parent().find(".mod").eq(4).find("input").val();	
		$("#summary").val($(obj).parent().parent().find(".mod").eq(4).find("input").val());
	
	}

	if($(obj).parent().parent().find(".mod").eq(5).find("input").val() == ""){
		alert("금액이 입력 되지 않았습니다.");
		return false;
	}else{
		//lowViolationInfo.amount = getNumberOnly($(obj).parent().parent().find(".mod").eq(5).find("input").val());
		$("#amount").val(getNumberOnly($(obj).parent().parent().find(".mod").eq(5).find("input").val()));
		
	}
	
	if($(obj).parent().parent().find(".mod").eq(6).find("input").val() == ""){
		alert("납부정보가 입력 되지 않았습니다.");
		return false;
	}else{
		//lowViolationInfo.amount = getNumberOnly($(obj).parent().parent().find(".mod").eq(5).find("input").val());
		$("#accountInfo").val($(obj).parent().parent().find(".mod").eq(6).find("input").val());
		
	}

	

	if($(obj).parent().parent().find(".mod").eq(7).find("input").val() == ""){
		alert("납부마감기간이 입력 되지 않았습니다.");
		return false;
	}else{
		
		//lowViolationInfo.end_dt = $(obj).parent().parent().find(".mod").eq(6).find("input").val();
		$("#endDt").val($(obj).parent().parent().find(".mod").eq(7).find("input").val());
		
	}


	$("#lowViolationId").val(lowViolationId);
	$("#driverId").val(driverId);

	if(confirm("과태료 관리를 수정 하시겠습니까?")){
		$("#excelForm").attr("action","/carmanagement/updateLowViolation.do");
		$("#excelForm").submit();
	}




	
/*	
 	if(confirm("과태료 관리를 수정 하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : "/carmanagement/update-low-violation.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				lowViolationInfo : JSON.stringify({lowViolationInfo : lowViolationInfo}),
				driverId : "${driver.driver_id}",
				lowViolationId : lowViolationId
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("수정 되었습니다.");
					document.location.reload();
				}else if(result == "0001"){
					alert("조회 하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}

*/

	/* $(obj).parent().parent().find(".normal").css("display","");
	$(obj).parent().parent().find(".mod").css("display","none");
	$(obj).css("display","none");
	$(obj).prev().css("display",""); */
	
}



/* <td style="text-align:center;"><input type="text" placeholder="항목" name="item" id="item" onkeyup="javascript:setItem(this);"></td>
<td style="text-align:center;"><input type="text" placeholder="구분" name="division" id="division" onkeyup="javascript:setDivision(this);"></td> */

function setItem(obj){
	var val = $(obj).val();
	var num01;
    var num02;
    num01 = obj.value;
    num02 = num01.replace(rgx1,"");
    
    if(num02 != ""){
    	if(obj.value == "1"){
    		$(obj).next().val(obj.value);
    		num02 = "청구 내역 공제";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "2"){
    		$(obj).next().val(obj.value);
    		num02 = "대납 내역 공제";
    		obj.value =  num02;
    		$(obj).blur();
    	}else{
    		alert("항목 입력이 잘못 되었습니다. \r\n 1 : 청구 내역 공제, 2 : 대납 내역 공제");
    		obj.value =  "";
    	}
    }else{
    	alert("항목 입력이 잘못 되었습니다. \r\n 1 : 청구 내역 공제, 2 : 대납 내역 공제");
    	obj.value =  num02;
    }
    
    
	
}

function setDivision(obj){
	
	var val = $(obj).val();
	var num01;
    var num02;
    num01 = obj.value;
    num02 = num01.replace(rgx1,"");
    if(num02 != ""){
    	if(obj.value == "1"){
    		$(obj).next().val(obj.value);
    		num02 = "보험료";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "2"){
    		$(obj).next().val(obj.value);
    		num02 = "차량할부";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "3"){
    		$(obj).next().val(obj.value);
    		num02 = "과태료";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "4"){
    		$(obj).next().val(obj.value);
    		num02 = "미납통행료";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "5"){
    		$(obj).next().val(obj.value);
    		num02 = "사고대납금";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "6"){
    		$(obj).next().val(obj.value);
    		num02 = "기타";
    		obj.value =  num02;
    		$(obj).blur();
    	}else{
    		alert("구분 입력이 잘못 되었습니다. \r\n 1 : 보험료, 2 : 차량할부, 3 : 과태료, 4 : 미납통행료, 5 : 사고대납금, 6 : 기타");
    		obj.value =  "";
    	}
    }else{
    	alert("구문 입력이 잘못 되었습니다. \r\n 1 : 보험료, 2 : 차량할부, 3 : 과태료, 4 : 미납통행료, 5 : 사고대납금, 6 : 기타");
    	obj.value =  num02;
    }
	
}

var selectedObj;
function fileModConfirm(obj){
	
	if(confirm("현재 파일이 첨부되어 있습니다.\r\n파일 수정시 기존의 파일이 삭제 됩니다. \r\n정말 수정 하시겠습니까?")){
		//$(obj).next().trigger("click");
		selectedObj = obj;
		$("#bbsFile").trigger("click");
		
	}
	
}

$(function() {

	  $("#bbsFile").change(function(e){
	 		$(selectedObj).html('선택완료');
	    });

	});


</script>

		<!-- <section class="side-nav"> -->
            <!-- <ul>
                <li class="active"><a href="/baseinfo/employee.do">직원관리</a></li>
                <li class=""><a href="/baseinfo/driver.do">기사관리</a></li>
                <li class=""><a href="/baseinfo/customer.do">거래처관리</a></li>
                <li class=""><a href="/baseinfo/company.do">회사정보관리</a></li>
            </ul> -->
        <!-- </section> -->

        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">과태료/범칙금 관리</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">상세내역</a></li>
                </ul>
            </div>

            <div class="up-dl clearfix header-search">
                 <table>
                    <tbody>
                        <tr>
                        	<td>조회기간 :</td>
                        	     <td class="widthAuto" style="width:330px;">
                       			 	<input style="width:150px;" autocomplete="off" id="startDt" placeholder="조회 시작일" name="startDt" type="text" class="datepick" value="${paramMap.startDt}">&nbsp;~&nbsp;
                       			 	<input style="width:150px;" autocomplete="off" id="endDt" placeholder="조회 종료일" name="endDt"  type="text" class="datepick" value="${paramMap.endDt}">
                       			 </td>
                       			 <td>
                       			 <input type="button" id="searchStatusU" name="searchStatus" value="조회" class="btn-primary" onclick="javascript:search('U',this);">
                       			 </td>
                        	<%-- <td>조회월 :</td>
				            <td class="widthAuto" style="width:300px;">
				                <input style="width:100%; text-align:center;" id="selectMonth" type="text" placeholder="조회월" onclick="javascript:$(this).val('');"  readonly="readonly" name="startDt" id="startDt" value="${paramMap.selectMonth}">
				                <input style="width:40%; text-align:center;" class="datepick" type="text" placeholder="조회종료일" onclick="javascript:$(this).val('');" readonly="readonly" name="endDt" id="endDt"  value="${paramMap.endDt}">
				            </td>
                        	<td>
                        		<input type="button" id="searchStatusU" name="searchStatus" value="조회" class="btn-primary" onclick="javascript:search('U',this);">
                                <input type="button" id="searchStatusU" name="searchStatus" value="매출처 정산" class="<c:if test="${paramMap.searchStatus == 'U'}">btn-primary</c:if><c:if test="${paramMap.searchStatus != 'U'}">btn-normal</c:if>" onclick="javascript:searchStatusChange('U',this);">
                            </td>
                        	<td>					
                                <input type="button" id="searchStatusI" name="searchStatus" value="매입처 정산" class="<c:if test="${paramMap.searchStatus == 'I'}">btn-primary</c:if><c:if test="${paramMap.searchStatus != 'I'}">btn-normal</c:if>" onclick="javascript:searchStatusChange('I',this);">
                            </td>
                            <td>
                                <input type="button" id="searchStatusO" name="searchStatus" value="기사 정산" class="<c:if test="${paramMap.searchStatus == 'O'}">btn-primary</c:if><c:if test="${paramMap.searchStatus != 'O'}">btn-normal</c:if>" onclick="javascript:searchStatusChange('O',this);">
                            </td> --%>
                        </tr>
                    </tbody>
                </table>
            </div>

        </section>

            <section class="bottom-table" style="width:1600px; margin-left:290px;">
            	 
            <form id="excelForm" style="display:none" name="excelForm" method="post" enctype="multipart/form-data">
			    <input type="hidden" name="lowViolationId" id="lowViolationId" value="">
			    <input type="hidden" name="driverId" id="driverId" value="">
				<input type="hidden" name="driverName" id="driverName" value="">
				<input type="hidden" name="occurrenceDt" id="occurrenceDt" >
				<input type="hidden" name="occurrenceTime" id="occurrenceTime" >
				<input type="hidden" name="carNum" id="carNum" value="">
				<input type="hidden" name="placeAddress" id="placeAddress" >
				<input type="hidden" name="summary" id="summary" >
				<input type="hidden" name="amount" id="amount">
				<input type="hidden" name="endDt" id="endDt">
				<input type="hidden" name="phoneNum" id="phoneNum">
				<input type="hidden" style=""  name="selectMonth" id="selectMonth" value="${paramMap.selectMonth}">
				
				<input style="display:none;" type="file" placeholder="" name="bbsFile" id="bbsFile" >
		    </form>
            	 
            	 
                <table class="article-table" style="margin-top:15px; table-layout:fixed;">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                        	<td style="text-align:center; width:70px;">기사명</td>
                            <td style="text-align:center; width:130px;">위반일자</td>
                            <td style="text-align:center; width:130px;">시간</td>
                            <td style="text-align:center; width:150px;">차량번호</td>
                            <td style="text-align:center; width:70px;" >위반장소</td>
                            <td style="text-align:center; width:70px;">위반내용</td>
                            <td style="text-align:center; width:150px;">금액</td>
                            <td style="text-align:center; width:200px;">납부정보</td>
                            <td style="text-align:center; width:130px;">납부마감기간</td>
                            <td style="text-align:center; width:120px;">파일</td>
                            <td style="text-align:center; width:80px;">확인여부</td>
                            <td style="text-align:center; width:80px;">납부여부</td>
                            <td style="text-align:center; width:90px;">작성자</td>
                            <td style="text-align:center; width:150px;">관리</td>
                        </tr>
                    </thead>
                    <tbody id="dataArea">
                    	
	                   	
                    	<c:forEach var="data" items="${listData}" varStatus="status">
							<tr class="ui-state-default"> 
								<td style="text-align:center;">${data.driver_name}</td>
	                            <td style="text-align:center;">
	                            <div class="normal">${data.occurrence_dt}</div>
	                            <div class="mod" style="display:none;">
	                            	<input style=" width:80%;" class="datepick" type="text" placeholder="위반일자" value="${data.occurrence_dt}" onclick="javascript:$(this).val('');">
	                            	<input style=" width:80%;" type="hidden"  value="${data.phone_num}">
	                            </div>
	                            </td>
	                            <td style="text-align:center;">
	                            	<div class="normal">
	                            		${data.occurrence_time}
	                            	</div>
	                            	<div class="mod" style="display:none;">
	                            		<input style=" width:80%;" type="text" placeholder="위반시간" value="${data.occurrence_time}" onclick="javascript:$(this).val('');">
	                    			</div>
	                            </td>
	                            <td style="text-align:center;">
	                            	<div class="normal">
		                            	${data.car_num }
	                            	</div>
	                            	<div class="mod" style="display:none;">
	                            		<input style=" width:80%;" type="text" placeholder="차량번호" value="${data.car_num}" onclick="javascript:$(this).val('');">
	                    			</div>
	                            </td>
	                            
	                            
	                            <td style="text-align:center;">
	                            	<div class="normal">
		                            	${data.place_address }
	                            	</div>
	                            	<div class="mod" style="display:none;">
	                            		<input style=" width:80%;" type="text" placeholder="위반장소" value="${data.place_address}" onclick="javascript:$(this).val('');">
	                    			</div>
	                            </td>
	                            <td style="text-align:center;">
	                            	<div class="normal">
		                            	${data.summary }
	                            	</div>
	                            	<div class="mod" style="display:none;">
	                            		<input style=" width:80%;" type="text" placeholder="위반내용" value="${data.summary}" onclick="javascript:$(this).val('');">
	                    			</div>
	                            </td>
	                            
	                            <td style="text-align:center;">
	                            	<div class="normal">
	                            		${data.amount}
	                            	</div>
	                            	<div class="mod"  style="display:none;">
	                            		<input style=" width:80%;" type="text" placeholder="금액" value="${data.amount}" onkeyup="javascript:getNumber(this);">
	                            	</div>
	                            </td>
	                              <td style="text-align:center;">
	                            	<div class="normal">
	                            		${data.account_info}
	                            	</div>
	                            	<div class="mod" style="display:none;">
	                            		<input style=" width:80%;" type="text" placeholder="납부정보" value="${data.account_info}" onclick="javascript:$(this).val('');">
	                            	</div>
	                            </td>
	                            <td style="text-align:center;">
	                            	<div class="normal">
	                            		${data.end_dt}
	                            	</div>
	                            	<div class="mod" style="display:none;">
	                            		<input style=" width:80%;" class="datepick" type="text" placeholder="납부마감기간" value="${data.end_dt}"  onclick="javascript:$(this).val('');">
	                            	</div>
	                            </td>
	                            <td style="text-align:center;">
	                            	<%-- <div class="normal">
	                            		${data.end_dt}
	                            	</div>
	                            	<div class="mod" style="display:none;">
	                            		<input style=" width:80%;" type="text" placeholder="납부마감기간" value="${data.end_dt}" >
	                            	</div> --%>
	                            	
	                            	<c:if test="${data.file_path != '-'}">
	                            		<div class="normal">
			                            	<a target="_blank" href="/files/lowViolation${data.file_path }">
									             <img style="width:30px; height:25px; margin-left:10px;" src="/files/lowViolation${data.file_path }" alt="${data.file_nm}" title="${data.file_nm}" />
											</a>
										</div>
										<div class="mod"  style="display:none;">
											<a style="cursor:pointer; width:50px;" onclick="javascript:fileModConfirm(this)" class="btn-edit">수정</a>
											<input style="display:none;" type="file" placeholder="" name="bbsFile" id=""  onchange="javascript:$(this).prev().html('선택완료');" >
										</div>
	                            	</c:if>
	                            	<c:if test="${data.file_path == '-'}">
	                            		파일없음
	                            	</c:if>
	                            	
	                            </td>
	                            
	                            <td style="text-align:center;">
	                            	<c:if test="${data.read_yn == 'Y'}">읽음</c:if>
	                            	<c:if test="${data.read_yn == 'N'}">읽지않음</c:if>
	                            </td>
	                            <td style="text-align:center;">
	                            	<c:if test="${data.payment_yn == 'Y'}">납부완료</c:if>
	                            	<c:if test="${data.payment_yn == 'N'}">미납</c:if>
	                            </td>
	                            <td style="text-align:center;">
	                            	<!-- <div class="normal"> -->
	                            		${data.register_name}
	                            	<%-- </div>
	                            	<div class="mod" style="display:none;">
	                            		<input type="text" placeholder="작성자" value="${data.register_name}" >
	                            	</div> --%>
	                            </td>
	                            <td style="text-align:center;">
	                            	<c:if test="${data.payment_yn == 'N'}">
	                            		<a style="cursor:pointer; width:50px;" onclick="javascript:editLowViolation('${data.low_violation_id}','${data.driver_id}',this)" class="btn-edit">수정</a>
	                            	</c:if>
	                            	<a style="display:none; cursor:pointer; width:50px;" onclick="javascript:setLowViolation('${data.low_violation_id}','${data.driver_id}',this)" class="btn-edit">완료</a>
	                            	<a style="cursor:pointer; width:50px;" onclick="javascript:deleteLowViolation('${data.low_violation_id}','${data.driver_id}',this)" class="btn-edit">삭제</a>
	                            </td>
                        	</tr>
						</c:forEach>
                        
                    </tbody>
                </table>
                <div class="table-pagination text-center">
                    <ul class="pagination">
                        <!-- <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="curr-page"><a href="#">1</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li> -->
                        <html:paging uri="/account/driverCal.do" forGroup="&selectMonth=${paramMap.selectMonth}&driverId=${paramMap.driverId}" />
                    </ul>
                </div>
                <div class="confirmation">
                    <div class="confirm">
                        <!-- <a href="/baseinfo/add-employee.do">직원등록</a> -->
                    </div>
                </div>
                
            </section>
     
