<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>한국카캐리어</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="/img/favicon.ico">
<link rel="stylesheet" href="/css/bootstrap.min.css">
<link rel="stylesheet" href="/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="/fonts/NotoSans/notosanskr.css">
<link rel="stylesheet" href="/css/reset.css">
<link rel="stylesheet" href="/css/main.css">
<link rel="stylesheet" href="/css/main2.css">
<link rel="stylesheet" href="/css/responsive.css">
<link rel="stylesheet" href="/css/jquery-ui.css">
<link rel="stylesheet" href="/css/font-awesome-css.min.css">

<script src="/js/vendor/modernizr-2.8.3.min.js"></script>
<script src="/js/vendor/jquery-1.11.2.min.js"></script>
<!-- <script src="/js/89d51f385b.js"></script> -->
<!-- <script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script> -->
<!-- <script type="text/javascript" src="/js/plugins/jquery.form.min.js"></script> -->
<script src="/js/vendor/jquery-1.11.2.min.js"></script>

<script type="text/javascript">


$(document).ready(function() {
	


	changeViewFile("http://52.78.153.148:8080/files/driverDeposit${fileList[0].file_path}");

	
	
});


function changeViewFile(filePath){



	var ext = filePath.substr(filePath.lastIndexOf('.') + 1);
	
	if(ext == "pdf"){
		$("#viewLocationPng").css("display","none");
		$("#viewLocationPdf").css("display","");
		$("#viewLocationPdf").attr("src","http://docs.google.com/gview?embedded=true&url="+filePath);
	}else{
		$("#viewLocationPng").css("display","");
		$("#viewLocationPdf").css("display","none");
		$("#viewLocationPng").attr("src",filePath);

	}

	
	

	
} 



</script>
	
</head>
		
	<body style="width:100%;">
	
	<!-- <input style=" margin-top:30px;" type="text" placeholder="검색" name="cAcqDate" onkeypress="javascript:if(event.keyCode=='13'){searchStart(this);};"> -->
	
	
	<!-- <div style="height:50px; overflow-y:scroll; " id="searchResult" name="searchResult">검색결과</div> -->
	
	<!-- <div id="map_div"></div> -->
		
	
	
	<div class="sample_area" >
		
		
			<c:forEach var="data" items="${fileList}" varStatus="status">
			
			<c:set var="fileName" value="${fn:split(data.file_path, '.')}" />
			
			 <c:if test="${fileName[fn:length(fileName)-1] ne 'pdf'}">
					<img style="width:50px; cursor:pointer; height:50px; margin-left:15px; border:1px solid #fff;" src="http://52.78.153.148:8080/files/driverDeposit${data.file_path}" alt="${data.file_nm}" title="${data.file_nm}" onclick="javascript:changeViewFile('http://52.78.153.148:8080/files/driverDeposit${data.file_path}')" /> 
				</c:if> 
				<c:if test="${fileName[fn:length(fileName)-1] eq 'pdf'}">
					<a onclick="javascript:changeViewFile('${data.file_path}')">
					<img src="/img/pdf-logo.jpg" style="width:50px; height:50px; cursor:pointer;" onclick="javascript:changeViewFile('http://52.78.153.148:8080/files/driverDeposit${data.file_path}')" />
						<iframe style="" src="http://docs.google.com/gview?embedded=true&url=http://52.78.153.148:8080/files/driverDeposit${data.file_path}" width="100px;" height="100px;"></iframe>
					</a>
				</c:if> 
				
				<%--  <img style="width:200px; cursor:pointer; height:200px; margin-left:15px; border:1px solid #fff;" src="http://52.78.153.148:8080/files/driverDeposit${data.file_path}" alt="${data.file_nm}" title="${data.file_nm}" onclick="javascript:changeViewFile('http://52.78.153.148:8080/files/driverDeposit${data.file_path}')" /> --%> 
				 
			</c:forEach>
			<img id="viewLocationPng" style="display:none; width:100%; height:100%; margin-top:50px; border:3px solid #fff;" src="" />	
		
        	<%-- <img id="viewLocationPng" style="display:none; width:100%; height:100%; margin-top:50px; border:3px solid #fff;" src="http://52.78.153.148:8080/files/driverDeposit${fileList[0].file_path }" />	
			<iframe id="viewLocationPdf" style="display:none;" src="http://docs.google.com/gview?embedded=true&url=http://52.78.153.148:8080/files/driverDeposit${fileList[0].file_path}" width="100%;"  height="1000px;" ></iframe> --%>
		
			<iframe id="viewLocationPdf" style="display:none;" src="" width="100%;"  height="1000px;" ></iframe>
		
		
	</div>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	</body>
</html>

