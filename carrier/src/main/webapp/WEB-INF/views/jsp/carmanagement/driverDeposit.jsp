<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>
<%

%>
<script src="/js/util.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	
	$("#selectMonth").MonthPicker({ 
		Button: false
		,MonthFormat: 'yy-mm'	
		,OnAfterChooseMonth: function() { 
	        //alert($(this).val());
	        document.location.href = "/carmanagement/driverDeposit.do?&searchWord="+encodeURI($("#searchWord").val())+"&selectMonth="+$(this).val();
	    }
	});
	
	$("#resignDt").datepicker({
		dateFormat : "yy-mm-dd"
	});
	
	

});

function updateDepositDriver(datetype , driverType, driverName, driverId, obj){
	
	if (datetype == 'resigndate'){
		//퇴사일 input 항목 신규로 입력 할 때 
		var resignDate = obj.trim();
		if(resignDate != "" && resignDate != null){
			
			// 날짜 형식 체크
			if(checkValidDate(resignDate)){
				
				if(confirm(driverName+" 기사님의 퇴사일을"+ obj+"로 등록 하시겠습니까?")){
					
					$.ajax({ 
						type: 'post' ,
						url : "/carmanagement/updateDepositDriver.do" ,
						dataType : 'json' ,
						data : {
							type : datetype,
							driverName : driverName,
							driverId : driverId,
							resignDate : resignDate,
							driverType : driverType,
							paymentDate : ''
						},
						success : function(data, textStatus, jqXHR)
						{
							var result = data.resultCode;
							var resultData = data.resultData;
							if(result == "0000"){
								alert("확정 되었습니다.");
								window.location.reload();
							}else if(result != "0000"){
								alert("확정 하는데 실패 했습니다. 관리자에게 문의 하세요.");
								window.location.reload();
							}
						} ,
						error : function(xhRequest, ErrorText, thrownError) {
						}
					});
					
					
				}else{
					
					
					return true;
				}
			} 

		}
	}else{
		//지급일자 input 항목 신규로 입력 할 때 
		var paymentDate = obj.trim();
		if(paymentDate != "" && paymentDate != null){
			
			// 날짜 형식 체크
			if(checkValidDate(paymentDate)){
				
				if(confirm(driverName+" 기사님의 지급일을"+ obj+"로 등록 하시겠습니까?")){
					
					$.ajax({ 
						type: 'post' ,
						url : "/carmanagement/updateDepositDriver.do" ,
						dataType : 'json' ,
						data : {
							type : datetype,
							driverName : driverName,
							driverId : driverId,
							resignDate : '',
							driverType : driverType,
							paymentDate : paymentDate
						},
						success : function(data, textStatus, jqXHR)
						{
							var result = data.resultCode;
							var resultData = data.resultData;
							if(result == "0000"){
								alert("확정 되었습니다.");
								window.location.reload();
							}else if(result != "0000"){
								alert("확정 하는데 실패 했습니다. 관리자에게 문의 하세요.");
								window.location.reload();
							}
						} ,
						error : function(xhRequest, ErrorText, thrownError) {
						}
					});
					
				}else{
					return true;
				}
			} 

		}
	}
	
}


function search(){
	
	var completeType = $("#completeType").val();
	

	document.location.href = "/carmanagement/driverDeposit.do?&searchWord="+encodeURI($("#searchWord").val())+"&completeType="+ completeType;
			
	
}

function viewDriverDeposit(driverId){

	document.location.href = "/carmanagement/driverDepositDetail.do?&driverId="+driverId;
	
}

function addDriverDeposit(){


	if($("#driverName").val() == ""){
		alert("이름이 입력되지 않았습니다.");
		return false;
	}	
	
	
	if($("#carNum").val() == ""){
		alert("차량번호가 입력 되지 않았습니다.");
		return false;
	}
	
	
	if($("#phoneNum").val() == ""){
		alert("연락처가 입력되지 않았습니다.");
		return false;
	}else{
		if(!phoneChk($("#phoneNum"))){
	  		alert("유효하지 않은 전화번호 입니다.");
	  		$("#phoneNum").val("");
            $("#phoneNum").focus();
			return false;
		}
	}


	if(confirm("해당 내용을 등록 하시겠습니까?")){
		$("#depositForm").attr("action","/carmanagement/insertDepositDriver.do");
		$("#depositForm").submit();
	}
	

		

}


</script>
		<!-- <section class="side-nav"> -->
            <!-- <ul>
                <li class="active"><a href="/baseinfo/employee.do">직원관리</a></li>
                <li class=""><a href="/baseinfo/driver.do">기사관리</a></li>
                <li class=""><a href="/baseinfo/customer.do">거래처관리</a></li>
                <li class=""><a href="/baseinfo/company.do">회사정보관리</a></li>
            </ul> -->
        <!-- </section> -->

        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">차량관리</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">예치금 관리</a></li>
                </ul>
            </div>
            
             <div class="dispatch-btn-container">
				<div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">기사 예치금 관리</div>
            </div>

            <div class="up-dl clearfix header-search">
                 <table>
                    <tbody>
                        <tr>
                        	<td>기사 검색 :</td>
				            <%-- <td class="widthAuto" style="width:200px;">
				                <input style="width:100%; text-align:center;" id="selectMonth" type="text" placeholder="조회월" onclick="javascript:$(this).val('');"  readonly="readonly" value="${paramMap.occurrenceDt}">
				                <input style="width:40%; text-align:center;" class="datepick" type="text" placeholder="조회종료일" onclick="javascript:$(this).val('');" readonly="readonly" name="endDt" id="endDt"  value="${paramMap.endDt}">
				            </td>
                             --%> 
                             <td style="width: 190px;">
                                <input type="text" id="searchWord" name="searchWord" value="${paramMap.searchWord}"   onkeypress="if(event.keyCode=='13') search();">
                            </td> 
                            <td>
	                            <select name="completeType" id="completeType" onchange="search()">
									<option value="ALL" <c:if test='${paramMap.completeType eq "ALL" }'> selected="selected"</c:if> >전체</option>
									<option value="Y" <c:if test='${paramMap.completeType eq "Y" }'> selected="selected"</c:if> >지급완료</option>
									<option value="N" <c:if test='${paramMap.completeType eq "N" }'> selected="selected"<</c:if> >미지급</option>
								</select>
							</td> 
                            <td>
                                <input type="button" id="btn-search" value="검색" class="btn-primary" onclick="javascript:search();">
                            </td>
                            
                            
                        </tr>
                    </tbody>
                </table>
            </div>
<!-- 
            <div class="dispatch-btn-container">
               
                <div class="dispatch-btn">
                    <i class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div>
            </div> -->
            	
            
        </section>



            <section class="bottom-table" style="width:1600px; margin-left:290px;">
                   <br>
                   <table class="article-table" style="margin-top:15px; table-layout:fixed;">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                        	<td style="text-align:center; width:100px;">기사명</td>
                            <td style="text-align:center; width:100px;">차량번호</td>
                            <td style="text-align:center; width:100px;">연락처</td>
                            <td style="text-align:center; width:100px;">예치금 </td>
                            <td style="text-align:center; width:50px;">작성자</td>
                            <td style="text-align:center; width:100px;">관리</td>
                        </tr>
                    </thead>
                    <tbody id="dataArea">
    	                	<tr class="ui-state-default">
	                    		<form id="depositForm" style="display:none" name="excelForm" method="post" enctype="multipart/form-data" autocomplete="false">
			                    	<td style="text-align:center;"><input style="width:80%;" type="text" placeholder="기사명" name="driverName" id="carNum"></td>
			                    	<td style="text-align:center;"><input style="width:80%;" autocomplete=”false”  type="text" placeholder="차량번호" name="carNum" id="carNum"></td>
			                    	<td style="text-align:center;"><input style="width:80%;" type="text" placeholder="연락처" name="phoneNum" id="phoneNum" ></td>
			                    	<td style="text-align:center;"><input style="width:80%;" type="text" placeholder="예치금" name="driverDeposit" id="driverDeposit" ></td>
			                    	<td style="text-align:center;">${user.emp_name}</td>
			                    	<td style="text-align:center;">
			                    		<a style="cursor:pointer; width:60px;" onclick="javascript:addDriverDeposit()" class="btn-edit">등록</a>
			                    	</td>
		                    	</form>
	                    	</tr>
                    </tbody>
                </table>
							   ※ 어플 사용 일반 등록 기사님은 <span style="color:#9A2EFE;"> 보라색</span>으로 표시 됩니다.  퇴사일 지정은 관리자가 기사관리 메뉴에서 처리 합니다. <br>
							   ※ 지급예정일로부터 6개월이 지난 날짜는 <span style="color:#EE0D0D;"> 빨간색</span>으로 표시 됩니다. <br>
                
                <table class="article-table" style="margin-top:15px">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <!-- <td style="text-align:center;">기사아이디</td> -->
                            <td style="text-align:center; width:10%;">기사명</td>
                            <td style="text-align:center; width:10%;">차량번호</td>
                            <td style="text-align:center; width:10%;">퇴사일</td>
                            <td style="text-align:center; width:10%;">지급예정일</td>
                            <td style="text-align:center; width:10%;">지급일</td>
                            <td style="text-align:center; width:10%;">예치금</td>
                            <td style="text-align:center; width:10%;">예치금 입금액</td>
                            <td style="text-align:center; width:10%;">예치금 미입금액</td>
                            <td style="text-align:center; width:10%;">예치금 출금액</td>
                            <td style="text-align:center; width:10%;">예치금 잔액</td>
                        </tr>
                    </thead>
                    <tbody id="">
                    <jsp:useBean id="now" class="java.util.Date" />
					<fmt:formatDate value="${now}" pattern="yyyy-MM-dd" var="nowDate" />								

                    
                    	<c:forEach var="data" items="${listData}" varStatus="status">
							<tr class="ui-state-default" > 
	                            <td style="cursor:pointer; text-align:center;" onclick="javascript:viewDriverDeposit('${data.driver_id}');" > <!-- 기사명 --> 
	                           <c:choose>
	                            	<c:when test='${data.driverType eq "noDepositDriver"}'>
	                            		<span style="color:#9A2EFE;">${data.driver_name}</span>
	                            	</c:when>
	                            	<c:otherwise>
	                            		${data.driver_name}
	                            	</c:otherwise>
	                            </c:choose>
	                            </td>
	                            
	                            <td style="text-align:center;">${data.car_num}</td> <!-- 차량번호 --> 
	                            
	                            <td style="text-align:center;"> <!-- 퇴사일 --> 
	                            <c:choose>
	                            	<c:when test='${data.resign_dt eq "" || data.resign_dt eq null }'>
	                            		<c:choose>
		                            		<c:when test='${data.driverType ne "noDepositDriver"}'> <!-- 일반등록 기사면 예치금 화면에서 퇴사일을 지정할 수 없다, 기사정보화면에서 퇴사일을 지정할 수 있도록 한다  -->
		                            			<input style="width:100%; text-align:center;" id="resignDate" type="text" placeholder="퇴사일(ex:2019-01-01)" id="" class="datepick" 
		                            			onchange='updateDepositDriver("resigndate","${data.driverType}","${data.driver_name}", "${data.driver_id}",this.value)'
		                            			onkeypress='if(event.keyCode=="13") updateDepositDriver("resigndate","${data.driverType}","${data.driver_name}", "${data.driver_id}",this.value);'
		                            			>
		                            		</c:when>
		                            		<c:otherwise>
		                            			${data.resign_dt}
		                            		</c:otherwise>
	                            		</c:choose>
	                            	</c:when>
	                            	<c:otherwise>
	                            		${data.resign_dt}
	                            	</c:otherwise>
	                            </c:choose>
	                            </td>
	                            
	                            <td style="text-align:center;"> <!-- 지급예정일 -->
								<span <c:if test='${data.payment_exp_dt < nowDate && (data.payment_dt eq "" || data.payment_dt eq null) }'> style="color:#EE0D0D; font-weight:bold;"</c:if>>${data.payment_exp_dt}</span>

	                            </td> 
	                            
	                            
	                             <td style="text-align:center;">
	                             <c:choose>
		                            	<c:when test='${ (data.payment_dt eq "" || data.payment_dt eq null) && (data.payment_exp_dt ne "" && data.payment_exp_dt ne null) }'>
		                            		<input style="width:100%; text-align:center;" id="paymentDate" type="text" placeholder="지급일 (ex:2019-01-01)" id="" class="datepick" 
		                            		onchange='updateDepositDriver("paymentdate","${data.driverType}","${data.driver_name}", "${data.driver_id}",this.value)'
		                            		onkeypress='if(event.keyCode=="13") updateDepositDriver("resigndate","${data.driverType}","${data.driver_name}", "${data.driver_id}",this.value);'
		                            		>
		                            	</c:when>
		                            	<c:otherwise>
		                            		${data.payment_dt}
		                            	</c:otherwise>
		                         </c:choose>
		                             
	                             </td>
	                            <td style="text-align:center;">${data.driver_deposit}</td>
	                            <td style="text-align:center;">${data.input_deposit}</td>
	                            <td style="text-align:center;">
	                            	<c:if test="${fn:indexOf(data.receivable_deposit, '-') != -1}">0</c:if>
		                            <c:if test="${fn:indexOf(data.receivable_deposit, '-') == -1}">${data.receivable_deposit}</c:if>
	                            </td>
	                            
	                            
	                            <td style="text-align:center;">${data.output_deposit}</td> 
	                            <td style="text-align:center;">${data.total}</td>
                        	</tr>
                        	
                        	 <tr>      
                           		<c:set var="driverDeposit" value="${fn:replace(driverDeposit,',','')+fn:replace(data.driver_deposit, ',','')}" />
                                <c:set var="inputDeposit" value="${fn:replace(inputDeposit,',','')+fn:replace(data.input_deposit, ',','')}" />
                                
                                
                                 <c:if test="${fn:indexOf(data.receivable_deposit, '-') == -1}">
                                <c:set var="receivableDeposit" value="${fn:replace(receivableDeposit,',','')+fn:replace(data.receivable_deposit, ',','')}" />
                                </c:if>
                                
                                <c:if test="${fn:indexOf(data.receivable_deposit, '-') != -1}">
                                <c:set var="receivableDeposit0" value="0" />
                                </c:if>
                                
                                <c:set var="outputDeposit" value="${fn:replace(outputDeposit,',','')+fn:replace(data.output_deposit, ',','')}" />
                                <c:set var="total" value="${fn:replace(total,',','')+fn:replace(data.total, ',','')}" />
                                
               			   </tr>
                        	
                        	
                        	
						</c:forEach>
                          <tr>
                        	<td style="text-align: center; background: #E1E1E1; "><c:out value =" 합     계"/></td>
                            <td style="text-align: center; background: #E1E1E1; "><fmt:formatNumber value="" groupingUsed="true"/></td>
                            <td style="text-align: center; background: #E1E1E1; "><fmt:formatNumber value="" groupingUsed="true"/></td>
                            <td style="text-align: center; background: #E1E1E1; "><fmt:formatNumber value="" groupingUsed="true"/></td>
                            <td style="text-align: center; background: #E1E1E1; "><fmt:formatNumber value="" groupingUsed="true"/></td>
                            <td style="text-align: center; background: #E1E1E1; "><fmt:formatNumber value="${driverDeposit}" groupingUsed="true"/></td>
                            <td style="text-align: center; background: #E1E1E1; "><fmt:formatNumber value="${inputDeposit}" groupingUsed="true"/></td>
                            <td style="text-align: center; background: #E1E1E1; "><fmt:formatNumber value="${receivableDeposit}" groupingUsed="true"/></td>
                            <td style="text-align: center; background: #E1E1E1; "><fmt:formatNumber value="${outputDeposit}" groupingUsed="true"/></td>
                            <td style="text-align: center; background: #E1E1E1; "><fmt:formatNumber value="${total}" groupingUsed="true"/></td>
                          </tr>   
                    </tbody>
                </table>
                <div class="table-pagination text-center">
                    <ul class="pagination">
                        <html:paging uri="/carmanagement/driverDeposit.do" forGroup='&searchWord="+encodeURI($("#searchWord").val())+"&completeType="+ completeType' frontYn="N"/>                        
                    </ul>
                </div>
                <div class="confirmation">
                    <div class="confirm">
                        <%-- <a href="/carnamagement/driverDepositList_download.do?selectMonth=${paramMap.selectMonth}">엑셀다운로드</a> --%>
                    </div>
                    <%-- <div class="confirm">
                        <a href="/account/driverList_download.do?selectMonth=${paramMap.selectMonth}"></a>
                    </div> --%>
                </div>
                
            </section>
     

     