<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>
<%

%>


<script type="text/javascript">
$(document).ready(function(){
	
	$("#selectMonth").MonthPicker({ 
		Button: false
		,MonthFormat: 'yy-mm',	
	});
	
});


var rgx1 = /\D/g;  // /[^0-9]/g 와 같은 표현
var rgx2 = /(\d+)(\d{3})/; 

function getNumber(obj){
	
    var num01;
    var num02;
    num01 = obj.value;
    num02 = num01.replace(rgx1,"");
    num01 = setComma(num02);
    obj.value =  num01;
      
}

function setComma(inNum){
    
    var outNum;
    outNum = inNum; 
    while (rgx2.test(outNum)) {
         outNum = outNum.replace(rgx2, '$1' + ',' + '$2');
     }
    return outNum;

}



function search(searchStatus,obj){
	
	document.location.href = "/carmanagement/viewLowViolation.do?selectMonth="+$("#selectMonth").val()+"&driverId=${paramMap.driverId}";
}

function addEmp(){
	document.location.href = "/baseinfo/add-employee.do";
}

function getNumberOnly (str) {			//저장할때는 콤마 제거 후 저장한다.
    var len = str.length;
    var sReturn = "";

    for (var i=0; i<len; i++){
        if ( (str.charAt(i) >= "0") && (str.charAt(i) <= "9") ){
            sReturn += str.charAt(i);
        }
    }
    return sReturn;
}


function addDriverDeduct(driverId,obj){
	
	var deductInfo = new Object();
	
	if($("#occurrenceDt").val() == ""){
		alert("날짜가 입력 되지 않았습니다.");
		return false;
	}else{
		deductInfo.occurrence_dt = $("#occurrenceDt").val();	
	}	
	if($("#item").val() == ""){
		alert("항목이 입력 되지 않았습니다.");
		return false;
	}else{
		deductInfo.item = $("#item").next().val();	
	}
	if($("#division").val() == ""){
		alert("구분이 입력 되지 않았습니다.");
		return false;
	}else{
		deductInfo.division = $("#division").next().val();	
	}
	if($("#amount").val() == ""){
		alert("금액이 입력 되지 않았습니다.");
		return false;
	}else{
		deductInfo.amount = getNumberOnly($("#amount").val());
	}
	deductInfo.etc = encodeURI($("#etc").val());
	
	if(confirm("공제 내역을 등록 하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : "/account/insert-driver-deduct.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				deductInfo : JSON.stringify({deductInfo : deductInfo}),
				driverId : "${driver.driver_id}",
				selectMonth : "${paramMap.selectMonth}"
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					document.location.reload();
				}else if(result == "E000"){
					alert("날짜가 잘못 입력 되었습니다.");
					return false;
				}else if(result == "0001"){
					alert("조회 하는데 실패 하였습니다.");
					return false;
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
}



function addDriverDeposit(driverId,obj){

	//작성되어야 하는 내용이 전부 작성 되었는지 확인하는 로직 추가.....
	
	if($("#ioStatus").val() == ""){
		alert("입출금 구분이 입력 되지 않았습니다.");
		return false;
	}
	/* if($("#guideDt").val() == ""){
		alert("안내일자가 입력 되지 않았습니다.");
		return false;
	} */
	if($("#ioDt").val() == ""){
		alert("입출금일이 입력 되지 않았습니다.");
		return false;
	}
	if($("#driverDeposit").val() == ""){
		alert("입출금액이 입력 되지 않았습니다.");
		return false;
	}

	if($("#ioAccount").val() == ""){
		alert("계좌가 입력 되지 않았습니다.");
		return false;
	}
	
	if($("#summary").val() == ""){
		alert("내용이 입력 되지 않았습니다.");
		return false;
	}

	if(confirm("해당 내용을 등록 하시겠습니까?")){
		$("#excelForm").attr("action","/carmanagement/insertDriverDeposit.do");
		$("#excelForm").submit();
	}
	
		
	/* $('#excelForm').ajaxForm({
		url: "/carmanagement/insertLowViolation.do",
		enctype: "multipart/form-data", 
	    type: "POST",
		dataType: "json",		
		data : {
			driverId : driverId
	    },
		success: function(data, response, status) {
			var status = data.resultCode;
			if(status == '0000'){

				alert();
				
			}else if(status == '1111'){
						
			}
					
		},
		error: function() {
			alert("이미지 등록중 오류가 발생하였습니다.");
		}                               
	}); */


	



	
}



function openPopup(depositId){



	
	window.open("/carmanagement/viewDepositFile.do?categoryType=driverDeposit&driverDepositId="+depositId,"_blank","top=240,left=660,width=600,height=700,toolbar=0,status=0,scrollbars=1,resizable=0");



	
}







function deleteDriverDeposit(driverDepositId,driverId,obj){
	
	
 	if(confirm("예치금 내역을 삭제 하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : "/carmanagement/deleteDriverDeposit.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				driverId : "${driver.driver_id}",
				driverDepositId : driverDepositId
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("삭제 되었습니다.");
					document.location.reload();
				}else if(result == "0001"){
					alert("조회 하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
	
}



function editDriverDeposit(driverDepositId,driverId,obj){
	
	$(obj).parent().parent().find(".normal").css("display","none");
	$(obj).parent().parent().find(".mod").css("display","");
	$(obj).css("display","none");
	$(obj).next().css("display","");
	
}


function setDriverDeposit(driverDepositId,driverId,obj){


	$("#updateForm").children().eq(0).val(driverDepositId);
	
	var driverDepositInfo = new Object();
	
	if($(obj).parent().parent().find(".mod").eq(0).find("select").val() == ""){
		alert("입출금구분이 입력 되지 않았습니다.");
		return false;
	}else{
		driverDepositInfo.io_status = $(obj).parent().parent().find(".mod").eq(0).find("select").val();	
		$("#updateForm").children().eq(1).val($(obj).parent().parent().find(".mod").eq(0).find("select").val());
	}	

	if($(obj).parent().parent().find(".mod").eq(1).find("input").val() == ""){

		$("#updateForm").children().eq(2).val($(obj).parent().parent().find(".mod").eq(1).find("input").val());
		
		//alert("안내일자가 입력 되지 않았습니다.");
		//return false;
	}else{
		driverDepositInfo.guide_dt = $(obj).parent().parent().find(".mod").eq(1).find("input").val();	
		$("#updateForm").children().eq(2).val($(obj).parent().parent().find(".mod").eq(1).find("input").val());
	}

	if($(obj).parent().parent().find(".mod").eq(2).find("input").val() == ""){
		alert("입/출금일이 입력 되지 않았습니다.");
		return false;
	}else{
		driverDepositInfo.io_dt = $(obj).parent().parent().find(".mod").eq(2).find("input").val();	
		$("#updateForm").children().eq(3).val($(obj).parent().parent().find(".mod").eq(2).find("input").val());
	}

	if($(obj).parent().parent().find(".mod").eq(3).find("input").val() == ""){
		alert("입/출금액이 입력 되지 않았습니다.");
		return false;
	}else{
		driverDepositInfo.driver_deposit = getNumberOnly($(obj).parent().parent().find(".mod").eq(3).find("input").val());	
		$("#updateForm").children().eq(4).val(getNumberOnly($(obj).parent().parent().find(".mod").eq(3).find("input").val()));
	}
	
	if($(obj).parent().parent().find(".mod").eq(4).find("input").val() == ""){
		alert("계좌가 입력 되지 않았습니다.");
		return false;
	}else{
		driverDepositInfo.io_account = $(obj).parent().parent().find(".mod").eq(4).find("input").val();
		$("#updateForm").children().eq(5).val($(obj).parent().parent().find(".mod").eq(4).find("input").val());	
	}
		
	if($(obj).parent().parent().find(".mod").eq(5).find("input").val() == ""){
		alert("내용이 입력 되지 않았습니다.");
		return false;
	}else{
		driverDepositInfo.summary = $(obj).parent().parent().find(".mod").eq(5).find("input").val();
		$("#updateForm").children().eq(6).val($(obj).parent().parent().find(".mod").eq(5).find("input").val());
	}


	if(confirm("예치금 내역을 수정 하시겠습니까?")){
		$("#updateForm").attr("action","/carmanagement/updateDriverDeposit.do");
		$("#updateForm").submit();
	}

	
/*	
 	if(confirm("예치금 내역을 수정 하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : "/carmanagement/updateDriverDeposit.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				driverDepositInfo : JSON.stringify({driverDepositInfo : driverDepositInfo}),
				driverId : "${driver.driver_id}",
				driverDepositId : driverDepositId
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("수정 되었습니다.");
					document.location.reload();
				}else if(result == "0001"){
					alert("조회 하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}else{

		document.location.reload();
	}

*/

	
}



/* <td style="text-align:center;"><input type="text" placeholder="항목" name="item" id="item" onkeyup="javascript:setItem(this);"></td>
<td style="text-align:center;"><input type="text" placeholder="구분" name="division" id="division" onkeyup="javascript:setDivision(this);"></td> */

function setItem(obj){
	var val = $(obj).val();
	var num01;
    var num02;
    num01 = obj.value;
    num02 = num01.replace(rgx1,"");
    
    if(num02 != ""){
    	if(obj.value == "1"){
    		$(obj).next().val(obj.value);
    		num02 = "청구 내역 공제";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "2"){
    		$(obj).next().val(obj.value);
    		num02 = "대납 내역 공제";
    		obj.value =  num02;
    		$(obj).blur();
    	}else{
    		alert("항목 입력이 잘못 되었습니다. \r\n 1 : 청구 내역 공제, 2 : 대납 내역 공제");
    		obj.value =  "";
    	}
    }else{
    	alert("항목 입력이 잘못 되었습니다. \r\n 1 : 청구 내역 공제, 2 : 대납 내역 공제");
    	obj.value =  num02;
    }
    
    
	
}

function setDivision(obj){
	
	var val = $(obj).val();
	var num01;
    var num02;
    num01 = obj.value;
    num02 = num01.replace(rgx1,"");
    if(num02 != ""){
    	if(obj.value == "1"){
    		$(obj).next().val(obj.value);
    		num02 = "보험료";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "2"){
    		$(obj).next().val(obj.value);
    		num02 = "차량할부";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "3"){
    		$(obj).next().val(obj.value);
    		num02 = "과태료";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "4"){
    		$(obj).next().val(obj.value);
    		num02 = "미납통행료";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "5"){
    		$(obj).next().val(obj.value);
    		num02 = "사고대납금";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "6"){
    		$(obj).next().val(obj.value);
    		num02 = "기타";
    		obj.value =  num02;
    		$(obj).blur();
    	}else{
    		alert("구분 입력이 잘못 되었습니다. \r\n 1 : 보험료, 2 : 차량할부, 3 : 과태료, 4 : 미납통행료, 5 : 사고대납금, 6 : 기타");
    		obj.value =  "";
    	}
    }else{
    	alert("구문 입력이 잘못 되었습니다. \r\n 1 : 보험료, 2 : 차량할부, 3 : 과태료, 4 : 미납통행료, 5 : 사고대납금, 6 : 기타");
    	obj.value =  num02;
    }
	
}

var selectedObj;
function fileModConfirm(obj){
	
	if(confirm("현재 파일이 첨부되어 있습니다.\r\n파일 수정시 기존의 파일이 삭제 됩니다. \r\n정말 수정 하시겠습니까?")){
		//$(obj).next().trigger("click");
		selectedObj = obj;
		//$(obj).next().trigger("click");
		$("#updateForm").children().eq(8).trigger("click");
		
	}
	
}


function fileSelected(obj){

	$(selectedObj).html('선택완료');
	
}


$(function() {

	/*
	  $("#bbsFile").change(function(e){
	 		$(selectedObj).html('선택완료');
	    });
*/



});


</script>

		<!-- <section class="side-nav"> -->
            <!-- <ul>
                <li class="active"><a href="/baseinfo/employee.do">직원관리</a></li>
                <li class=""><a href="/baseinfo/driver.do">기사관리</a></li>
                <li class=""><a href="/baseinfo/customer.do">거래처관리</a></li>
                <li class=""><a href="/baseinfo/company.do">회사정보관리</a></li>
            </ul> -->
        <!-- </section> -->

        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">예치금 관리</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">상세내역</a></li>
                </ul>
            </div>

            <div class="up-dl clearfix header-search">
                 <table>
                    <tbody>
                        <tr>
                        	<td>조회월 :</td>
				            <td class="widthAuto" style="width:300px;">
				                <input style="width:100%; text-align:center;" id="selectMonth" type="text" placeholder="조회월" onclick="javascript:$(this).val('');"  readonly="readonly" name="startDt" id="startDt" value="${paramMap.selectMonth}">
				                <%-- <input style="width:40%; text-align:center;" class="datepick" type="text" placeholder="조회종료일" onclick="javascript:$(this).val('');" readonly="readonly" name="endDt" id="endDt"  value="${paramMap.endDt}"> --%>
				            </td>
                        	<td>
                        		<input type="button" id="searchStatusU" name="searchStatus" value="조회" class="btn-primary" onclick="javascript:search('U',this);">
                                <%-- <input type="button" id="searchStatusU" name="searchStatus" value="매출처 정산" class="<c:if test="${paramMap.searchStatus == 'U'}">btn-primary</c:if><c:if test="${paramMap.searchStatus != 'U'}">btn-normal</c:if>" onclick="javascript:searchStatusChange('U',this);"> --%>
                            </td>
                        	<td>					
                                <%-- <input type="button" id="searchStatusI" name="searchStatus" value="매입처 정산" class="<c:if test="${paramMap.searchStatus == 'I'}">btn-primary</c:if><c:if test="${paramMap.searchStatus != 'I'}">btn-normal</c:if>" onclick="javascript:searchStatusChange('I',this);"> --%>
                            </td>
                            <td>
                                <%-- <input type="button" id="searchStatusO" name="searchStatus" value="기사 정산" class="<c:if test="${paramMap.searchStatus == 'O'}">btn-primary</c:if><c:if test="${paramMap.searchStatus != 'O'}">btn-normal</c:if>" onclick="javascript:searchStatusChange('O',this);"> --%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </section>

            <section class="bottom-table" style="width:1600px; margin-left:290px;">
            	 
            <form id="updateForm" style="display:none" name="updateForm" method="post" enctype="multipart/form-data">
			    <input type="hidden" name="driverDepositId" value="">
			    <input type="hidden" name="ioStatus"  value="">
			    <input type="hidden" name="guideDt"  >
			    <input type="hidden" name="ioDt"  >
				<input type="hidden" name="driverDeposit" >
				<input type="hidden" name="ioAccount"  >
				<input type="hidden" name="summary" >
				<input type="hidden" name="returnTargetYn" >
				<input style="display:none;" type="file" placeholder="" name="bbsFile" onchange="javascript:fileSelected(this);">
				<input type="hidden" name="driverId" value="${driver.driver_id}"  >
		    </form>
            	 
                <table class="article-table" style="margin-top:15px; table-layout:fixed;">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                        	<td style="text-align:center; width:70px;">기사명</td>
                        	<td style="text-align:center; width:130px;">입/출금 구분</td>
                            <td style="text-align:center; width:130px;">안내일자</td>
                            <td style="text-align:center; width:130px;">입/출금일</td>
                            <td style="text-align:center; width:150px;">입/출금액</td>
                            <td style="text-align:center;">계좌</td>
                            <td style="text-align:center;">내용</td>
                            <td style="text-align:center; width:150px;">파일</td>
                            <td style="text-align:center; width:90px;">작성자</td>
                            <td style="text-align:center; width:150px;">관리</td>
                        </tr>
                    </thead>
                    <tbody id="dataArea">
                    	
	                   	<form id="excelForm" style="display:none" name="excelForm" method="post" enctype="multipart/form-data" autocomplete="false">
	                   		<input type="hidden" style=""  name="driverId" id="driverId" value="${driver.driver_id}">
	                   		<input type="hidden" style=""  name="driverName" id="driverName" value="${driver.driver_name}">
    	                	<tr class="ui-state-default">
	                    		<td style="text-align:center;">${driver.driver_name}</td>
	                    		
	                    		<td style="text-align:center;">
		                    		<select class="dropdown" name="ioStatus" id="ioStatus">
							        	<option value="" selected="selected">입/출금 구분</option>
							            <option value="I">입금</option>
							            <option value="O">출금</option>
							        </select>
		                    	</td>
	                    		
		                    	<td style="text-align:center;"><input style="width:80%;" autocomplete="false"class="datepick" type="text" placeholder="안내일자" name="guideDt" id="guideDt" onclick="javascript:$(this).val('');"></td>
		                    	<td style="text-align:center;">
		                    		<input style="width:80%;" autocomplete="false" class="datepick" type="text" placeholder="입/출금일" name="ioDt" id="ioDt" onclick="javascript:$(this).val('');">
		                    	</td>
		                    	<td style="text-align:center;">
		                    		<input type="text" style="width:80%;" placeholder="입/출금액" name="driverDeposit" id="driverDeposit" onkeyup="javascript:getNumber(this);" value="">
		                    	</td>
		                    	<td style="text-align:center;">
		                    		<input type="text" style="width:80%;" placeholder="계좌" name="ioAccount" id="ioAccount"  >
		                    	</td>
		                    	<td style="text-align:center;">
		                    		<input type="text" style="width:80%;" placeholder="내용" name="summary" id="summary"  >
		                    	</td>
		                    	<td style="text-align:center;">
			                    	<a style="cursor:pointer; width:60px;" onclick="javascript:$(this).next().trigger('click');" class="btn-edit">파일선택</a>
			                    	<input style="display:none;" type="file" multiple placeholder="" name="bbsFile" id="bbsFile" onchange="javascript:$(this).prev().html('선택완료');" >
			                    </td>
		                    	<td style="text-align:center;">${user.emp_name}</td>
		                    	<td style="text-align:center;">
		                    		<a style="cursor:pointer; width:60px;" onclick="javascript:addDriverDeposit('${data.driver_id}',this)" class="btn-edit">등록</a>
		                    	</td>
	                    	</tr>
                    	</form>
                    	<c:forEach var="data" items="${listData}" varStatus="status">
							<tr class="ui-state-default"> 
								<td style="text-align:center;">${data.driver_name}</td>
	                            <td style="text-align:center;">
		                            <div class="normal">
		                            	<c:if test="${data.io_status == 'I'}">
		                            		입금	
		                            	</c:if>
		                            	<c:if test="${data.io_status == 'O'}">
		                            		출금
		                            	</c:if>
		                            </div>
		                            <div class="mod" style="display:none;">
		                            	<select class="dropdown" name="ioStatus" id="ioStatus">
								        	<option value="" <c:if test="${data.io_status == ''}">selected="selected"</c:if>>입/출금 구분</option>
								            <option value="I" <c:if test="${data.io_status == 'I'}">selected="selected"</c:if>>입금</option>
								            <option value="O" <c:if test="${data.io_status == 'O'}">selected="selected"</c:if>>출금</option>
								        </select>
		                            </div>
	                            </td>
	                            <td style="text-align:center;">
	                            	<div class="normal">
	                            		${data.guide_dt}
	                            	</div>
	                            	<div class="mod" style="display:none;">
	                            		<input style=" width:80%;" type="text" placeholder="안내일자" class="datepick" value="${data.guide_dt}" onclick="javascript:$(this).val('');">
	                    			</div>
	                            </td>
	                            <td style="text-align:center;">
	                            	<div class="normal">
		                            	${data.io_dt }
	                            	</div>
	                            	<div class="mod" style="display:none;">
	                            		<input style=" width:80%;" type="text" placeholder="입/출금일" class="datepick" value="${data.io_dt}" onclick="javascript:$(this).val('');">
	                    			</div>
	                            </td>
	                            <td style="text-align:center;">
	                            	<div class="normal">
		                            	${data.driver_deposit }
	                            	</div>
	                            	<div class="mod" style="display:none;">
	                            		<input style=" width:80%;" type="text" placeholder="입출금액" value="${data.driver_deposit}" onkeyup="javascript:getNumber(this);" >
	                    			</div>
	                            </td>
	                            <td style="text-align:center;">
	                            	<div class="normal">
		                            	${data.io_account }
	                            	</div>
	                            	<div class="mod" style="display:none;">
	                            		<input style=" width:80%;" type="text" placeholder="계좌" value="${data.io_account}" >
	                    			</div>
	                            </td>  
	                            
	                            <td style="text-align:center;">
	                            	<div class="normal">
		                            	${data.summary }
	                            	</div>
	                            	<div class="mod" style="display:none;">
	                            		<input style=" width:80%;" type="text" placeholder="내용" value="${data.summary}" >
	                    			</div>
	                            </td>
	                            <td style="text-align:center;">
	                            
		                            <c:if test="${data.file_count != 0}">
	                            		<div class="normal">
			                            	<a style="cursor:pointer; width:100px;" onclick="javascript:openPopup('${data.driver_deposit_id}')" class="btn-edit">눌러서 파일보기</a>
										</div>
										<div class="mod"  style="display:none;">
											<a style="cursor:pointer; width:50px;" onclick="javascript:fileModConfirm(this)" class="btn-edit">수정</a>
											<input style="display:none;" type="file" placeholder="" name="bbsFile" id=""  onchange="javascript:$(this).prev().html('선택완료');" >
										</div>
	                            	</c:if>
	                            	
	                            	<c:if test="${data.file_count == 0}">
										<a style="cursor:pointer; width:50px;" onclick="javascript:fileModConfirm(this)" class="btn-edit">등록</a>
										<input style="display:none;" type="file" placeholder="" name="bbsFile" id=""  onchange="javascript:$(this).prev().html('선택완료');" >
	                            	</c:if>
	                            	
	                            </td>
	                        
	                            <td style="text-align:center;">
	                            		${data.register_name}
	                            </td>
	                            <td style="text-align:center;">
	                            	<a style="cursor:pointer; width:50px;" onclick="javascript:editDriverDeposit('${data.driver_deposit_id}','${data.driver_id}',this)" class="btn-edit">수정</a>
	                            	<a style="display:none; cursor:pointer; width:50px;" onclick="javascript:setDriverDeposit('${data.driver_deposit_id}','${data.driver_id}',this)" class="btn-edit">완료</a>
	                            	<a style="cursor:pointer; width:50px;" onclick="javascript:deleteDriverDeposit('${data.driver_deposit_id}','${data.driver_id}',this)" class="btn-edit">삭제</a>
	                            </td>
                        	</tr>
						</c:forEach>
                        
                    </tbody>
                </table>
                <div class="table-pagination text-center">
                    <ul class="pagination">
                        <!-- <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="curr-page"><a href="#">1</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li> -->
                        <html:paging uri="/account/driverCal.do" forGroup="&selectMonth=${paramMap.selectMonth}&driverId=${paramMap.driverId}" />
                    </ul>
                </div>
                <div class="confirmation">
                    <div class="confirm">
                        <a href="/carmanagement/driverDeposit.do">목록으로</a>
                    </div>
                </div>
                
            </section>
     
