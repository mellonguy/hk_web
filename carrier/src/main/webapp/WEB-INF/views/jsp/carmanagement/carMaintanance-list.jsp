<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>
<%

%>

<script type="text/javascript">
$(document).ready(function(){
	
	$("#selectMonth").MonthPicker({ 
		Button: false
		,MonthFormat: 'yy-mm'	
		,OnAfterChooseMonth: function() { 
	        //alert($(this).val());
	        document.location.href = "/carmanagement/driverDeposit.do?&searchWord="+encodeURI($("#searchWord").val())+"&selectMonth="+$(this).val();
	    }
	});
	
});

function editDriverDeduct(driverId,selectMonth){
	
	document.location.href = "/account/driverCal.do?driverId="+driverId+"&selectMonth="+selectMonth;
}

/*
function downloadDriverDeduct(driverId,selectMonth){
	
	document.location.href = "/account/downloadDriverDeductInfo.do?driverId="+driverId+"&selectMonth="+selectMonth;
}
*/


function addEmp(){
	document.location.href = "/baseinfo/add-employee.do";
}


function search(){
	
	document.location.href = "/carmanagement/carMaintanance-list.do?&searchWord="+encodeURI($("#searchWord").val())+"&searchType="+encodeURI($("#searchType").val())+"&startDt="+$('#startDt').val()+"&endDt="+$('#endDt').val()+"&driverName="+$("#driverSearchName").val();
	//document.location.href = "/carmanagement/carMaintanance-list.do?&searchWord="+"${paramMap.searchWord}";
	
}

function viewMonthDriverDeduct(driverId){
	
	document.location.href = "/account/viewMonthDriverDeduct.do?driverId="+driverId;
	
}


function insertDriverDeductStatus(driverName,driverId,selectMonth){
	
	
	if(confirm(driverName+" 기사님의 "+selectMonth+"월의 공제내역을 확정 하시겠습니까?")){
		
		$.ajax({ 
			type: 'post' ,
			url : "/account/insertDriverDeductStatus.do" ,
			dataType : 'json' ,
			data : {
				driverName : driverName,
				driverId : driverId,
				decideMonth : selectMonth
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("확정 되었습니다.");
					window.location.reload();
				}else if(result != "0000"){
					alert("확정 하는데 실패 했습니다. 관리자에게 문의 하세요.");
					window.location.reload();
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
		
		
	}else{
		
		
		
	}
	
	
}

function checkAll(){

	if($('input:checkbox[id="checkAll"]').is(":checked")){
		$('input:checkbox[name="forDeductStatus"]').each(function(index,element) {
			$(this).prop("checked","true");
		 });	
	}else{
		$('input:checkbox[name="forDeductStatus"]').each(function(index,element) {
			$(this).prop("checked","");
		 });
	}
}


function insertAllDriverDeductStatus(selectMonth){
	
	var total = $('input:checkbox[name="forDeductStatus"]:checked').length;
	var driverId = "";
	
	if(total == 0){
		alert("확정할 목록이 선택 되지 않았습니다.");
		return false;
	}else{
		$('input:checkbox[name="forDeductStatus"]:checked').each(function(index,element) {
		      if(this.checked){	//checked 처리된 항목의 값
		    	  var selectedObj = new Object();
		    	  driverId+=$(this).attr("driverId");
		    	  if(index<total-1){
		    		  driverId += ","; 
			         } 
		      }
		 });
		
		if(driverId != ""){
			insertDeductStatus(driverId,total,selectMonth);
		}
			

	}
	
	
	/* if(confirm(selectMonth+"월의 공제내역을 일괄확정 하시겠습니까?")){
		
		$.ajax({ 
			type: 'post' ,
			url : "/account/insertAllDriverDeductStatus.do" ,
			dataType : 'json' ,
			data : {
				selectMonth : selectMonth,
				searchType : "${paramMap.searchType}",
				searchWord : "${paramMap.searchWord}"
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("확정 되었습니다.");
					window.location.reload();
				}else if(result != "0000"){
					alert("확정 하는데 실패 했습니다. 관리자에게 문의 하세요.");
					window.location.reload();
				}	
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
		
		
	}else{
		
	} */
	
	
}



function insertDeductStatus(driverId,total,selectMonth){
	
	
	if(confirm(total+"명의 공제내역을 확정 하시겠습니까?")){
		
		$.ajax({ 
			type: 'post' ,
			url : "/account/insertAllDriverDeductStatus.do" ,
			dataType : 'json' ,
			data : {
				selectMonth : selectMonth,
				driverId : driverId
				//searchWord : "${paramMap.searchWord}"
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("확정 되었습니다.");
					window.location.reload();
				}else if(result != "0000"){
					alert("확정 하는데 실패 했습니다. 관리자에게 문의 하세요.");
					window.location.reload();
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
		
		
	}else{
		
	}
}



function viewTotalSales(driverId){
	document.location.href = "/account/driverListDetail.do?&paymentPartnerId="+driverId+"&decideMonth="+$("#selectMonth").val();
}


function downloadDriverDeduct(driverId,selectMonth){
	
	window.open("/account/viewDriverDeductInfo.do?driverId="+driverId+"&selectMonth="+selectMonth,"_blank","top=0,left=0,width=780,height=1000,toolbar=0,status=0,scrollbars=0,resizable=0");
	
}


function viewCarMaintanance(driverId,carNum,searchType,carDetailCode){

	document.location.href = "/carmanagement/carMaintanance-reg.do?&driverId="+driverId+"&carNum="+carNum+"&searchType="+"${paramMap.searchType}"+"&carDetailCode="+carDetailCode;
	
}



function viewDriverBillingFile(driverId,selectMonth){
	
	window.open("/account/viewDriverBillingFile.do?driverId="+driverId+"&selectMonth="+selectMonth,"_blank","top=0,left=0,width=1000,height=700,toolbar=0,status=0,scrollbars=0,resizable=0");
	
}

function viewLowViolation(driverId,occurrenceDt){


	document.location.href = "/carmanagement/viewLowViolation.do?driverId="+driverId+"&occurrenceDt="+occurrenceDt;
	
}




var rgx1 = /\D/g;  // /[^0-9]/g 와 같은 표현
var rgx2 = /(\d+)(\d{3})/; 

function getNumber(obj){
	
    var num01;
    var num02;
    num01 = obj.value;
    num02 = num01.replace(rgx1,"");
    num01 = setComma(num02);
    obj.value =  num01;
    
}

function setComma(inNum){
    
    var outNum;
    outNum = inNum; 
    while (rgx2.test(outNum)) {
         outNum = outNum.replace(rgx2, '$1' + ',' + '$2');
     }
 
    return outNum;

}




function deleteCarDetail(carDetailId){

	if(confirm("선택한 차량을 삭제 하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : "/carmanagement/delete-carDeatail.do" ,
			dataType : 'json' ,
			async : false,
			data : {
			
				carDetailId : carDetailId
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("삭제 되었습니다.");
					document.location.reload();
				}else if(result == "0001"){
					alert("삭제 하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}

	
}



function updateAnotherDecideFinalForMakeExcel(){
	
	
	if(confirm($("#selectMonth").val()+"월분 운송비 정산 내역서를 일괄 성성 하시겠습니까?")){
	
	$.ajax({ 
		type: 'post' ,
		url : "/account/updateAnotherDecideFinalForMakeExcel.do" ,
		dataType : 'json' ,
		data : {
			decideMonth : $("#selectMonth").val()
		},
		success : function(data, textStatus, jqXHR)
		{
			var result = data.resultCode;
			var resultData = data.resultData;
			if(result == "0000"){
				alert("일괄 생성 되었습니다.");
			}else if(result == "0001"){
				alert("일괄 생성 하는데 실패 하였습니다.");
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
	}
	
	
}
function addDriverDeposit(){


	if($("#driverName").val() == ""){
		alert("이름이 입력되지 않았습니다.");
		return false;
	}	
	
	
	if($("#carNum").val() == ""){
		alert("차량번호가 입력 되지 않았습니다.");
		return false;
	}
	
	
	if($("#phoneNum").val() == ""){
		alert("연락처가 입력되지 않았습니다.");
		return false;
	}else{
		if(!phoneChk($("#phoneNum"))){
	  		alert("유효하지 않은 전화번호 입니다.");
	  		$("#phoneNum").val("");
            $("#phoneNum").focus();
			return false;
		}
	}


	if(confirm("해당 내용을 등록 하시겠습니까?")){
		$("#depositForm").attr("action","/carmanagement/insertDepositDriver.do");
		$("#depositForm").submit();
	}
	

		

}

function editCarMaintanance(code,carDetailId){

/*	$(obj).parent().parent().find(".normal").css("display","none");
	$(obj).parent().parent().find(".mod").css("display","");
	$(obj).css("display","none");
	$(obj).next().css("display","");
*/

		if(code == 'A'){
		
			document.location.href="/carmanagement/edit-carMaintance.do?&code="+code;
		}else{
		
			document.location.href="/carmanagement/edit-carMaintance.do?&carDetailId="+carDetailId;
		}

	



}


function showModal(obj){
	selectDriverObj =obj; 
	$('.modal-field').show();
}

function addcarDetail(){



	if($("#carIdNum").val() == ""){
		alert("차대번호가 입력되지 않았습니다.");
		return false;
	}	

	if($("#carIdNum").val() == ""){
		alert("차애번호가 입력되지 않았습니다.");
		return false;
	}	

	if($("#carIdNum").val() == ""){
		alert("차애번호가 입력되지 않았습니다.");
		return false;
	}	

	if($("#carIdNum").val() == ""){
		alert("차애번호가 입력되지 않았습니다.");
		return false;
	}	

	

	
}






</script>
		<!-- <section class="side-nav"> -->
            <!-- <ul>
                <li class="active"><a href="/baseinfo/employee.do">직원관리</a></li>
                <li class=""><a href="/baseinfo/driver.do">기사관리</a></li>
                <li class=""><a href="/baseinfo/customer.do">거래처관리</a></li>
                <li class=""><a href="/baseinfo/company.do">회사정보관리</a></li>
            </ul> -->
        <!-- </section> -->
	
        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">차량관리</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">차계부 관리</a></li>
                </ul>
            </div>
            
             <div class="dispatch-btn-container">
				<div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">차계부 관리</div>
            </div>

            <div class="up-dl clearfix header-search">
            
                 <table>
                    <tbody>
                        <tr>
         		            <td class="widthAuto" style="width:500px;">
				            	<select name="searchWord"  id="searchWord">
				            		<option value=""  selected="selected">전체</option>
									<option value="01"<c:if test='${paramMap.searchWord eq "01" }'>selected="selected"</c:if>>재직중</option>
									<option value="02"<c:if test='${paramMap.searchWord eq "02" }'>selected="selected"</c:if>>휴직중</option>
									<option value="03"<c:if test='${paramMap.searchWord eq "03" }'>selected="selected"</c:if>>퇴사</option>
								</select>
				                <input style="width:40%; text-align:center;" class="datepick" type="text" placeholder="조회시작일" onclick="javascript:$(this).val('');"  readonly="readonly" name="startDt" id="startDt" value="${paramMap.startDt}">&nbsp;~&nbsp;
				                <input style="width:40%; text-align:center;" class="datepick" type="text" placeholder="조회종료일" onclick="javascript:$(this).val('');" readonly="readonly" name="endDt" id="endDt"  value="${paramMap.endDt}">
				            </td>
				            
				               <td style="width: 190px;">
                                <input type="text" id="driverSearchName" name="driverName" value="${paramMap.driverName}" placeholder="기사명">
                            </td>
				            
				               <td style="width: 145px;">
                                <div class="select-con">
							        <select class="dropdown" name="searchType" id="searchType">
							        	<option value=""  selected="selected">전체</option>
							        	<option value="00" <c:if test='${paramMap.searchType eq "00" }'> selected="selected" </c:if>>운행비</option>
							        	<option value="01"  <c:if test='${paramMap.searchType eq "01" }'> selected="selected" </c:if>>수리비</option>
							        	<option value="02"  <c:if test='${paramMap.searchType eq "02" }'> selected="selected" </c:if>>사고비</option>
							        	<option value="03"  <c:if test='${paramMap.searchType eq "03" }'> selected="selected" </c:if>>세금기타</option>
							        </select>
							    </div>
							    
                            </td>
				            
				            
				         	<td>
                        		<input type="button" id="searchStatusU" name="" value="검색" class="btn-primary" onclick="javascript:search();">
                            </td>
                            	<td>
                        		<input type="button" id="searchStatusU" name="" value="차량등록" class="btn-primary" onclick="javascript:editCarMaintanance('A');">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
<!-- 
            <div class="dispatch-btn-container">
               
                <div class="dispatch-btn">
                    <i class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div>
            </div> -->
            	
            
        </section>



            <section class="bottom-table" style="width:1600px; margin-left:290px;">
                   <br>
               <!--     <table class="article-table" style="margin-top:15px; table-layout:fixed;">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <td style="text-align:center; width:100px;">차대번호</td>
                        	<td style="text-align:center; width:100px;">차량번호</td>
                            <td style="text-align:center; width:100px;">차종</td>
                            <td style="text-align:center; width:100px;">연식</td>
                            <td style="text-align:center; width:100px;">총중량</td>
                            <td style="text-align:center; width:100px;">차량번호 소유자</td>
                            <td style="text-align:center; width:100px;">비고</td>
                            <td style="text-align:center; width:100px;">작성자</td>
                            <td style="text-align:center; width:100px;">관리</td>
                        </tr>
                    </thead>
                    <tbody id="dataArea">
	                   	
    	                	<tr class="ui-state-default">
	                    		
	                    		<form id="carDetailForm" style="display:none" name="carDetailForm" method="post" enctype="multipart/form-data" autocomplete="false">
	                    			
			                    	<td style="text-align:center;"><input style="width:80%;"class="text" type="text" placeholder="차대번호" name="carIdNum" id="carIdNum"></td>
			                    	<td style="text-align:center;"><input style="width:80%;"class="text" type="text" placeholder="차량번호" name="carNum" id="carNum"></td>
			                    	<td style="text-align:center;"><input style="width:80%;" type="text" placeholder="차종" name="carKind" id="carKind" ></td>
			                    	<td style="text-align:center;"><input style="width:80%;" type="text" placeholder="연식" name="carModelYear" id="carModelYear" onkeyup="getNumber(this);" ></td>
			                    	<td style="text-align:center;"><input style="width:80%;" type="text" placeholder="총중량" name="carWeight" id="carWeight" onkeyup="getNumber(this);" ></td>
			                    	<td style="text-align:center;"><input style="width:80%;" type="text" placeholder="차량번호 소유자" name="carOwnerName" id="carOwnerName" ></td>
			                    	<td style="text-align:center;"><input style="width:80%;" type="text" placeholder="비고" name="carText" id="carText" ></td>
			                    	<td style="text-align:center;">${user.emp_name}</td>
			                    	<td style="text-align:center;">
			                    		<a style="cursor:pointer; width:60px;" onclick="javascript:addcarDetail()" class="btn-edit">등록</a>
			                    	</td>
			                    	<input style="width:80%;" type="hidden"  id="carMaintananceId" name="carMaintananceId" value="" >
		                    	</form>
	                    	</tr>
                    	
                    </tbody>
                </table>   -->
                <table class="article-table" style="margin-top:15px">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <!-- <td style="text-align:center;">기사아이디</td> -->
                            <td style="text-align:center; width:50px;"><input type="checkbox" id="checkAll" onclick="javascript:checkAll();" ></td>
                            <td style="text-align:center; width:100px;">차량번호</td>
                            <td style="text-align:center; width:100px;" >기사명</td>
                            <td style="text-align:center; width:100px;" >차대번호</td>
                            <td style="text-align:center; width:100px;">차종</td>
                            <td style="text-align:center; width:100px;">총 비용(원)</td>
                            <td style="text-align:center; width:100px;">연비 (km/L)</td>
                            <td style="text-align:center; width:100px;">비고</td>
                            <td style="text-align:center; width:100px;">관리</td>
                          
                        </tr>
                    </thead>
               <tbody id="">
                  		 
                    	<c:forEach var="data" items="${listData}" varStatus="status">
                    	
							<c:set var ="oilTotal"  value ="${data.oiltotal}"/>
							
							<tr class="ui-state-default" > 
	                            <td style="text-align:center;"><input type="checkbox" name="forDeductStatus" driverId="${data.driver_id}" ></td>
	                            <td style="text-align:center; cursor:pointer;" onclick="javascript:viewCarMaintanance('${data.driver_id}','${data.car_num}','${paramMap.searchType}','N');" >${data.car_num}</td>
	                            <td style="text-align:center;">${data.driver_name}</td>
	                            <td style="text-align:center;">${data.car_id_num}</td>
	                            <td style="text-align:center;">
	                            		<c:if test="${data.car_assign_company eq 'C'}">캐리어</c:if>
	                            		<c:if test="${data.car_assign_company eq 'S'}">셀프</c:if></td>
	                            <td style="text-align:center;">${data.car_payment}</td>
	                            <td style="text-align:center;">${data.oiltest}</td> 
	                            <td style="text-align:center;">
		                           <div class="normal">
			                        	${data.car_text}
		                        	</div>
		                        	<div class="mod" style="display:none;">
		                        		<input style=" width:80%;" type="text" placeholder="비고" value="${data.car_text}" >
		                    			</div>
		                          </td> 
	                                  
		                          <td style="text-align:center;">
	                            		<a style="cursor:pointer; width:50px;" onclick="javascript:editCarMaintanance('E','${data.car_detail_id}')" class="btn-edit">수정</a>
	                            		<a style="cursor:pointer; width:50px;" onclick="javascript:deleteCarDetail('${data.car_detail_id}',this)" class="btn-edit">삭제</a>
	                            
	                            </td>
	                            
                        	</tr>
              
                        	
						</c:forEach>
                  
                                   
                    </tbody> 
                 <!--     <tbody id="">
                  		 
                    	<c:forEach var="data" items="${detailListData}" varStatus="status">
                    		
							<tr class="ui-state-default" > 
	                            <td style="text-align:center;"><input type="checkbox" name="forDeductStatus" driverId="${data.driver_id}" ></td>
	                            <td style="text-align:center; cursor:pointer;" onclick="javascript:viewCarMaintanance('${data.driver_id}','${data.car_num}','${paramMap.searchType}','Y');" >${data.car_num}</td>
	                            <td style="text-align:center;">
	                            <c:if test ="${data.driver_name eq '' }">
	                            		공차
	                            </c:if>
	                             <c:if test ="${data.driver_name ne '' }">
	                            		${data.driver_name}
	                            </c:if>
	                            </td>
	                            <td style="text-align:center;">${data.car_id_num}</td>
	                            <td style="text-align:center;">${data.car_kind}</td>
	                            <td style="text-align:center;">${data.car_payment}</td>
	                            <td style="text-align:center;">${data.oiltest}</td> 
	                            <td style="text-align:center;">
		                           <div class="normal">
			                        	${data.car_text}
		                        	</div>
		                        	<div class="mod" style="display:none;">
		                        		<input style=" width:80%;" type="text" placeholder="비고" value="${data.car_text}" >
		                    			</div>
		                          </td> 
	                                  
		                          <td style="text-align:center;">
	                            		<a style="cursor:pointer; width:50px;" onclick="javascript:editCarMaintanance('E','${data.driver_id}')" class="btn-edit">수정</a>
	                            	 
	                            	<a style="cursor:pointer; width:50px;" onclick="javascript:deleteCarDetail('${data.car_detail_id}',this)" class="btn-edit">삭제</a> 
	                            </td>
	                            
                        	</tr>
            
                        	
						</c:forEach>
                                   
                    </tbody> -->
                    
                </table>
              <!-- <div class="table-pagination text-center">
                    <ul class="pagination">
                        <html:paging uri="/carmanagement/carMaintanance-list.do" forGroup="" />
                    </ul>
                </div> -->  
                <div class="confirmation">
                    <div class="confirm">
                        <%-- <a href="/carnamagement/driverDepositList_download.do?selectMonth=${paramMap.selectMonth}">엑셀다운로드</a> --%>
                    </div>
                    <%-- <div class="confirm">
                        <a href="/account/driverList_download.do?selectMonth=${paramMap.selectMonth}"></a>
                    </div> --%>
                </div>
                
            </section>
     
