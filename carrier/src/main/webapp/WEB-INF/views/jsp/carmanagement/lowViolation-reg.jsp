<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>
<%

%>


<script type="text/javascript">
$(document).ready(function(){
	
/* 	$("#startDt").MonthPicker({ 
		Button: false
		,MonthFormat: 'yy-mm'	
		,OnAfterChooseMonth: function() { 
	        //alert($(this).val());
	        document.location.href = "/carmanagement/lowViolation-reg.do?&selectMonth="+$(this).val();
	    }
	});
*/
	 
});


var rgx1 = /\D/g;  // /[^0-9]/g 와 같은 표현
var rgx2 = /(\d+)(\d{3})/; 

function getNumber(obj){
	
    var num01;
    var num02;
    num01 = obj.value;
    num02 = num01.replace(rgx1,"");
    num01 = setComma(num02);
    obj.value =  num01;
      
}

function setComma(inNum){
    
    var outNum;
    outNum = inNum; 
    while (rgx2.test(outNum)) {
         outNum = outNum.replace(rgx2, '$1' + ',' + '$2');
     }
    return outNum;

}



function search(searchStatus,obj){

	if(searchStatus == 'U'){

		document.location.href = "/carmanagement/lowViolation-reg.do?startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val();

		}else{

			document.location.href = "/carmanagement/lowViolation-reg.do?startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val()+"&searchWord="+$("#driverFName").val();
			
		}
	

	
	
	
	
}



function addEmp(){
	document.location.href = "/baseinfo/add-employee.do";
}

function getNumberOnly (str) {			//저장할때는 콤마 제거 후 저장한다.
    var len = str.length;
    var sReturn = "";

    for (var i=0; i<len; i++){
        if ( (str.charAt(i) >= "0") && (str.charAt(i) <= "9") ){
            sReturn += str.charAt(i);
        }
    }
    return sReturn;
}


function addDriverDeduct(driverId,obj){
	
	var deductInfo = new Object();
	
	if($("#occurrenceDt").val() == ""){
		alert("날짜가 입력 되지 않았습니다.");
		return false;
	}else{
		deductInfo.occurrence_dt = $("#occurrenceDt").val();	
	}	
	if($("#item").val() == ""){
		alert("항목이 입력 되지 않았습니다.");
		return false;
	}else{
		deductInfo.item = $("#item").next().val();	
	}
	if($("#division").val() == ""){
		alert("구분이 입력 되지 않았습니다.");
		return false;
	}else{
		deductInfo.division = $("#division").next().val();	
	}
	if($("#amount").val() == ""){
		alert("금액이 입력 되지 않았습니다.");
		return false;
	}else{
		deductInfo.amount = getNumberOnly($("#amount").val());
	}
	deductInfo.etc = encodeURI($("#etc").val());
	
	if(confirm("공제 내역을 등록 하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : "/account/insert-driver-deduct.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				deductInfo : JSON.stringify({deductInfo : deductInfo}),
				driverId : "${driver.driver_id}",
				selectMonth : "${paramMap.selectMonth}"
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					document.location.reload();
				}else if(result == "E000"){
					alert("날짜가 잘못 입력 되었습니다.");
					return false;
				}else if(result == "0001"){
					alert("조회 하는데 실패 하였습니다.");
					return false;
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
}



function addLowViolation(obj){

	//작성되어야 하는 내용이 전부 작성 되었는지 확인하는 로직 추가.....

	
	
	if($("#driverName").val() == ""){
		alert("기사명이 입력 되지 않았습니다.");
		return false;
	}
	
	if($("#carNum").val() == ""){
		alert("차량번호가 입력 되지 않았습니다.");
		return false;
	}

	if($("#driverId").val() == ""){
		if($("#phoneNum").val() == ""){
			alert("연락처가 입력 되지 않았습니다.");
			return false;
		}
	}
	
	if($("#occurrenceDt").val() == ""){
		alert("위반일자가 입력 되지 않았습니다.");
		return false;
	}
	if($("#occurrenceTime").val() == ""){
		alert("위반시간이 입력 되지 않았습니다.");
		return false;
	}
	if($("#placeAddress").val() == ""){
		alert("위반장소가 입력 되지 않았습니다.");
		return false;
	}
	if($("#summary").val() == ""){
		alert("위반내용이 입력 되지 않았습니다.");
		return false;
	}
	if($("#amount").val() == ""){
		alert("금액이 입력 되지 않았습니다.");
		return false;
	}
	if($("#accountInfo").val() == ""){
		alert("계좌정보가 입력 되지 않았습니다.");
		return false;
	}
	if($("#deadLindDt").val() == ""){
		alert("납부마감기간이 입력 되지 않았습니다.");
		return false;
	}
	
	if($("#bbsFile").val() == ""){

		if(confirm("파일이 등록되지 않았습니다. 파일 없이 등록 하시겠습니까?")){
			
		}else{
			alert("취소 되었습니다.");
			return false;
		}
	}

	if(confirm("해당 내용을 등록 하시겠습니까?")){
		$("#excelForm").attr("action","/carmanagement/insertLowViolation.do");
		$("#excelForm").submit();
	}
	
		
	/* $('#excelForm').ajaxForm({
		url: "/carmanagement/insertLowViolation.do",
		enctype: "multipart/form-data", 
	    type: "POST",
		dataType: "json",		
		data : {
			driverId : driverId
	    },
		success: function(data, response, status) {
			var status = data.resultCode;
			if(status == '0000'){

				alert();
				
			}else if(status == '1111'){
						
			}
					
		},
		error: function() {
			alert("이미지 등록중 오류가 발생하였습니다.");
		}                               
	}); */


	



	
}










function deleteLowViolation(lowViolationId,driverId,obj){
	
	
 	if(confirm("공제 내역을 삭제 하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : "/carmanagement/delete-lowViolation.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				driverId : "${driver.driver_id}",
				lowViolationId : lowViolationId
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("삭제 되었습니다.");
					document.location.reload();
				}else if(result == "0001"){
					alert("조회 하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
	
}


function viewLowViolation(driverId){


	//document.location.href = "/carmanagement/viewLowViolation.do?driverId="+driverId+"&selectMonth="+selectMonth;
	//"&startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val();
	document.location.href = "/carmanagement/viewLowViolation.do?driverId="+driverId+"&startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val();
	
}



function editLowViolation(lowViolationId,driverId,obj){
	
	$(obj).parent().parent().find(".normal").css("display","none");
	$(obj).parent().parent().find(".mod").css("display","");
	$(obj).css("display","none");
	$(obj).next().css("display","");
	
}


function setLowViolation(lowViolationId,driverId,obj){
	
	
	var lowViolationInfo = new Object();
	
	if($(obj).parent().parent().find(".mod").eq(0).find("input").val() == ""){
		alert("위반일자가 입력 되지 않았습니다.");
		return false;
	}else{
		lowViolationInfo.occurrence_dt = $(obj).parent().parent().find(".mod").eq(0).find("input").val();	
	}	
	if($(obj).parent().parent().find(".mod").eq(1).find("input").val() == ""){
		alert("위반시간이 입력 되지 않았습니다.");
		return false;
	}else{
		lowViolationInfo.occurrence_time = $(obj).parent().parent().find(".mod").eq(1).find("input").val();	
	}

	if($(obj).parent().parent().find(".mod").eq(2).find("input").val() == ""){
		alert("차량번호가 입력 되지 않았습니다.");
		return false;
	}else{
		lowViolationInfo.car_num = $(obj).parent().parent().find(".mod").eq(2).find("input").val();	
	}

	if($(obj).parent().parent().find(".mod").eq(3).find("input").val() == ""){
		alert("위반장소가 입력 되지 않았습니다.");
		return false;
	}else{
		lowViolationInfo.place_address = $(obj).parent().parent().find(".mod").eq(3).find("input").val();	
	}
	if($(obj).parent().parent().find(".mod").eq(4).find("input").val() == ""){
		alert("위반내용이 입력 되지 않았습니다.");
		return false;
	}else{
		lowViolationInfo.summary = $(obj).parent().parent().find(".mod").eq(4).find("input").val();	
	}

	if($(obj).parent().parent().find(".mod").eq(5).find("input").val() == ""){
		alert("금액이 입력 되지 않았습니다.");
		return false;
	}else{
		lowViolationInfo.amount = getNumberOnly($(obj).parent().parent().find(".mod").eq(5).find("input").val());
	}

	if($(obj).parent().parent().find(".mod").eq(6).find("input").val() == ""){
		alert("납부마감기간이 입력 되지 않았습니다.");
		return false;
	}else{
		lowViolationInfo.end_dt = $(obj).parent().parent().find(".mod").eq(6).find("input").val();	
	}

	
 	if(confirm("과태료 관리를 수정 하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : "/carmanagement/update-low-violation.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				lowViolationInfo : JSON.stringify({lowViolationInfo : lowViolationInfo}),
				driverId : "${driver.driver_id}",
				lowViolationId : lowViolationId
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("수정 되었습니다.");
					document.location.reload();
				}else if(result == "0001"){
					alert("조회 하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
	/* $(obj).parent().parent().find(".normal").css("display","");
	$(obj).parent().parent().find(".mod").css("display","none");
	$(obj).css("display","none");
	$(obj).prev().css("display",""); */
	
}



/* <td style="text-align:center;"><input type="text" placeholder="항목" name="item" id="item" onkeyup="javascript:setItem(this);"></td>
<td style="text-align:center;"><input type="text" placeholder="구분" name="division" id="division" onkeyup="javascript:setDivision(this);"></td> */

function setItem(obj){
	var val = $(obj).val();
	var num01;
    var num02;
    num01 = obj.value;
    num02 = num01.replace(rgx1,"");
    
    if(num02 != ""){
    	if(obj.value == "1"){
    		$(obj).next().val(obj.value);
    		num02 = "청구 내역 공제";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "2"){
    		$(obj).next().val(obj.value);
    		num02 = "대납 내역 공제";
    		obj.value =  num02;
    		$(obj).blur();
    	}else{
    		alert("항목 입력이 잘못 되었습니다. \r\n 1 : 청구 내역 공제, 2 : 대납 내역 공제");
    		obj.value =  "";
    	}
    }else{
    	alert("항목 입력이 잘못 되었습니다. \r\n 1 : 청구 내역 공제, 2 : 대납 내역 공제");
    	obj.value =  num02;
    }
    
    
	
}

function setDivision(obj){
	
	var val = $(obj).val();
	var num01;
    var num02;
    num01 = obj.value;
    num02 = num01.replace(rgx1,"");
    if(num02 != ""){
    	if(obj.value == "1"){
    		$(obj).next().val(obj.value);
    		num02 = "보험료";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "2"){
    		$(obj).next().val(obj.value);
    		num02 = "차량할부";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "3"){
    		$(obj).next().val(obj.value);
    		num02 = "과태료";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "4"){
    		$(obj).next().val(obj.value);
    		num02 = "미납통행료";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "5"){
    		$(obj).next().val(obj.value);
    		num02 = "사고대납금";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "6"){
    		$(obj).next().val(obj.value);
    		num02 = "기타";
    		obj.value =  num02;
    		$(obj).blur();
    	}else{
    		alert("구분 입력이 잘못 되었습니다. \r\n 1 : 보험료, 2 : 차량할부, 3 : 과태료, 4 : 미납통행료, 5 : 사고대납금, 6 : 기타");
    		obj.value =  "";
    	}
    }else{
    	alert("구문 입력이 잘못 되었습니다. \r\n 1 : 보험료, 2 : 차량할부, 3 : 과태료, 4 : 미납통행료, 5 : 사고대납금, 6 : 기타");
    	obj.value =  num02;
    }
	
}


function setDriver(driverId,driverName,carNum,phoneNum,obj){

	
	if(driverId != ""){

		$("#findDriver").val(driverName);
		
		$("#driverId").val(driverId);
		$("#driverName").val(driverName);
		$("#findCarNum").val(carNum);
		$("#carNum").val(carNum);
		$("#phoneNum").val(phoneNum);
		$("td[name=phoneNumParent]").css("display","none");
	}else{

		$("td[name=phoneNumParent]").css("display","");
		
		$("#driverId").val(driverId);
		$("#driverName").val($("#findDriver").val());
		$("#findCarNum").val(carNum);
		$("#carNum").val(carNum);
		$("#phoneNum").val(phoneNum);
	}

	$(obj).parent().parent().removeClass("active");
	
}


function setDriverByCarNum(driverId,driverName,carNum,phoneNum,obj){


	if(driverId != ""){

		$("#findDriver").val(driverName);
		
		$("#driverId").val(driverId);
		$("#driverName").val(driverName);
		$("#findCarNum").val(carNum);
		$("#carNum").val(carNum);
		$("#phoneNum").val(phoneNum);
		$("td[name=phoneNumParent]").css("display","none");
	}else{

		$("td[name=phoneNumParent]").css("display","");
		
		$("#driverId").val(driverId);
		//$("#driverName").val($("#findCarNum").val());
		//$("#findCarNum").val($("#findCarNum").val());
		$("#carNum").val($("#findCarNum").val());
		$("#phoneNum").val(phoneNum);
	}

	$(obj).parent().parent().removeClass("active");

	
}




var test;
function getAjaxData(val,obj,forPayment){

	test = obj;


	if(forPayment == "findDriver"){

			
		
			$.ajax({ 
				type: 'post' ,
				url : "/baseinfo/getDriverListAll.do" ,
				dataType : 'json' ,
				async : false,
				data : {
					driverName : val
				},
				success : function(data, textStatus, jqXHR)
				{
					
					var list = data.resultData;
					var result = "";
					//$("#searchResult").html("");
					$(obj).parent().find("div").html("");
					if(list.length > 0){
		       			for(var i=0; i<list.length; i++){
		       				result += '<div class="Wresult">';
		       				/* result += '<a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>'; */
		       				result += '<p class="result-title">기사정보</p>';
		       				var strJsonText = JSON.stringify(obj);
		    	   		    strJsonText = strJsonText.replace(/\"/gi, "'");
		       				result += '<a style="cursor:pointer;"   onclick="javascript:setDriver(\''+list[i].driver_id+'\',\''+list[i].driver_name+'\',\''+list[i].car_num+'\',\''+list[i].phone_num+'\',this);" class="result-sub"><span>'+list[i].driver_name+'</span></a>';
		       				result += '<p class="camp-type"><span>차량번호:</span> <span>'+list[i].car_num+'</span></p>';	
		       				/* result += '<p class="camp-type"><span>주소:</span> <span>'+list[i].address+'</span></p>'; */	
		       				result += '<p class="camp-id"><span>Tel:</span> <span>'+list[i].phone_num+'</span></p>';
		       				result += '</div>';
		       			}
		       			result += '<div class="Wresult">';
	       				/* result += '<a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>'; */
	       				/* result += '<a style="cursor:pointer; margin-bottom:10px;" class="result-title result-sub">기사정보 직접입력</a>'; */
	       				/* result += '<a style="cursor:pointer; margin-bottom:10px;" onclick="javascript:$(this).parent().parent().removeClass(\'active\');" class="result-title result-sub">기사정보 직접입력</a>'; */
	       				result += '<a style="cursor:pointer; margin-bottom:10px;" onclick="javascript:setDriver(\'\',\'\',\'\',\'\',this);" class="result-title result-sub">기사정보 직접입력</a>';
	       				var strJsonText = JSON.stringify(obj);
	    	   		    strJsonText = strJsonText.replace(/\"/gi, "'");
	       				/* result += '<a style="cursor:pointer;"   onclick="javascript:setAddressInfo();" class="result-sub"><span></span></a>';
	       				result += '<p class="camp-type"><span>차량번호:</span> <span></span></p>';	
	       				result += '<p class="camp-type"><span>주소:</span> <span></span></p>';	
	       				result += '<p class="camp-id"><span>Tel:</span> <span></span></p>'; */
	       				result += '</div>';

					}else{

						result += '<div class="Wresult">';
	       				/* result += '<a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>'; */
	       				/* result += '<p class="result-title">기사정보 직접입력</p>'; */
	       				
	       				result += '<a style="cursor:pointer; margin-bottom:10px;" onclick="javascript:setDriver(\'\',\'\',\'\',\'\',this);" class="result-title result-sub">기사정보 직접입력</a>';
	       				
	       				/* result += '<a style="cursor:pointer; margin-bottom:10px;" onclick="javascript:$(this).parent().parent().removeClass(\'active\');" class="result-title result-sub">기사정보 직접입력</a>'; */
	       				var strJsonText = JSON.stringify(obj);
	    	   		    strJsonText = strJsonText.replace(/\"/gi, "'");
	       				result += '<a style="cursor:pointer;"   onclick="javascript:setAddressInfo();" class="result-sub"><span></span></a>';
	       				/* result += '<p class="camp-type"><span>차량번호:</span> <span></span></p>';	
	       				result += '<p class="camp-type"><span>주소:</span> <span></span></p>';	
	       				result += '<p class="camp-id"><span>Tel:</span> <span></span></p>'; */
	       				result += '</div>';
						//$(test).parent().find("div").removeClass('active');
					}
					
					//$("#searchResult").html(result);
					$(obj).parent().find("div").html(result);
					
				} ,
				error : function(xhRequest, ErrorText, thrownError) {
				}
			});



	}else if(forPayment == "findCarNum"){

		$.ajax({ 
			type: 'post' ,
			url : "/baseinfo/getDriverListAllByCarNum.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				carNum : val
			},
			success : function(data, textStatus, jqXHR)
			{
				
				var list = data.resultData;
				var result = "";
				//$("#searchResult").html("");
				$(obj).parent().find("div").html("");
				if(list.length > 0){
	       			for(var i=0; i<list.length; i++){
	       				result += '<div class="Wresult">';
	       				/* result += '<a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>'; */
	       				result += '<p class="result-title">기사정보</p>';
	       				var strJsonText = JSON.stringify(obj);
	    	   		    strJsonText = strJsonText.replace(/\"/gi, "'");
	       				result += '<a style="cursor:pointer;"   onclick="javascript:setDriverByCarNum(\''+list[i].driver_id+'\',\''+list[i].driver_name+'\',\''+list[i].car_num+'\',\''+list[i].phone_num+'\',this);" class="result-sub"><span>'+list[i].driver_name+'</span></a>';
	       				result += '<p class="camp-type"><span>차량번호:</span> <span>'+list[i].car_num+'</span></p>';	
	       				/* result += '<p class="camp-type"><span>주소:</span> <span>'+list[i].address+'</span></p>'; */	
	       				result += '<p class="camp-id"><span>Tel:</span> <span>'+list[i].phone_num+'</span></p>';
	       				result += '</div>';
	       			}
	       			result += '<div class="Wresult">';
       				/* result += '<a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>'; */
       				/* result += '<a style="cursor:pointer; margin-bottom:10px;" class="result-title result-sub">기사정보 직접입력</a>'; */
       				/* result += '<a style="cursor:pointer; margin-bottom:10px;" onclick="javascript:$(this).parent().parent().removeClass(\'active\');" class="result-title result-sub">기사정보 직접입력</a>'; */
       				result += '<a style="cursor:pointer; margin-bottom:10px;" onclick="javascript:setDriverByCarNum(\'\',\'\',\'\',\'\',this);" class="result-title result-sub">기사정보 직접입력</a>';
       				var strJsonText = JSON.stringify(obj);
    	   		    strJsonText = strJsonText.replace(/\"/gi, "'");
       				/* result += '<a style="cursor:pointer;"   onclick="javascript:setAddressInfo();" class="result-sub"><span></span></a>';
       				result += '<p class="camp-type"><span>차량번호:</span> <span></span></p>';	
       				result += '<p class="camp-type"><span>주소:</span> <span></span></p>';	
       				result += '<p class="camp-id"><span>Tel:</span> <span></span></p>'; */
       				result += '</div>';

				}else{

					result += '<div class="Wresult">';
       				/* result += '<a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>'; */
       				/* result += '<p class="result-title">기사정보 직접입력</p>'; */
       				
       				result += '<a style="cursor:pointer; margin-bottom:10px;" onclick="javascript:setDriverByCarNum(\'\',\'\',\'\',\'\',this);" class="result-title result-sub">기사정보 직접입력</a>';
       				
       				/* result += '<a style="cursor:pointer; margin-bottom:10px;" onclick="javascript:$(this).parent().parent().removeClass(\'active\');" class="result-title result-sub">기사정보 직접입력</a>'; */
       				var strJsonText = JSON.stringify(obj);
    	   		    strJsonText = strJsonText.replace(/\"/gi, "'");
       				result += '<a style="cursor:pointer;"   onclick="javascript:setAddressInfo();" class="result-sub"><span></span></a>';
       				/* result += '<p class="camp-type"><span>차량번호:</span> <span></span></p>';	
       				result += '<p class="camp-type"><span>주소:</span> <span></span></p>';	
       				result += '<p class="camp-id"><span>Tel:</span> <span></span></p>'; */
       				result += '</div>';
					//$(test).parent().find("div").removeClass('active');
				}
				
				//$("#searchResult").html(result);
				$(obj).parent().find("div").html(result);
				
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});




	}









	
}

 
</script>

		<!-- <section class="side-nav"> -->
            <!-- <ul>
                <li class="active"><a href="/baseinfo/employee.do">직원관리</a></li>
                <li class=""><a href="/baseinfo/driver.do">기사관리</a></li>
                <li class=""><a href="/baseinfo/customer.do">거래처관리</a></li>
                <li class=""><a href="/baseinfo/company.do">회사정보관리</a></li>
            </ul> -->
        <!-- </section> -->

        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">과태료/범칙금 관리</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">등록</a></li>
                </ul>
            </div>

            <div class="up-dl clearfix header-search">
                 <table>
                    <tbody>
                        <tr>
                        	<td>조회기간 :</td>
                        	     <td class="widthAuto" style="width:330px;">
                       			 	<input style="width:150px;" autocomplete="off" id="startDt" placeholder="조회 시작일" name="startDt" type="text" class="datepick" value="${paramMap.startDt}">&nbsp;~&nbsp;
                       			 	<input style="width:150px;" autocomplete="off" id="endDt" placeholder="조회 종료일" name="endDt"  type="text" class="datepick" value="${paramMap.endDt}">
                       			 </td>
                       			 <td>
                       			 <input type="button" id="searchStatusU" name="searchStatus" value="조회" class="btn-primary" onclick="javascript:search('U',this);">
                       			 </td>
                        <%-- 
                        	<td>조회월 :</td>
				            <td class="widthAuto" style="width:300px;">
				                <input style="width:100%; text-align:center;"  type="text" placeholder="조회월" onclick="javascript:$(this).val('');"  readonly="readonly" name="startDt" id="startDt" value="${paramMap.selectMonth}">
				                <input style="width:40%; text-align:center;" class="datepick" type="text" placeholder="조회종료일" onclick="javascript:$(this).val('');" readonly="readonly" name="endDt" id="endDt"  value="${paramMap.endDt}">
				            </td>
                        	<td>
                        		<input type="button" id="searchStatusU" name="searchStatus" value="조회" class="btn-primary" onclick="javascript:search('U',this);">
                                <input type="button" id="searchStatusU" name="searchStatus" value="매출처 정산" class="<c:if test="${paramMap.searchStatus == 'U'}">btn-primary</c:if><c:if test="${paramMap.searchStatus != 'U'}">btn-normal</c:if>" onclick="javascript:searchStatusChange('U',this);">
                            </td> --%>
                            
                        	<td>			
                        	<td>기사명  :</td>
				            <td class="widthAuto" style="width:300px;">
				                <input style="width:100%; text-align:center;"  id='driverFName' type="text" placeholder="기사이름" onclick="javascript:$(this).val('');"  value="${paramMap.searchWord}">
				               
                            </td>
                            <td>
                        		<input type="button" id="searchStatusU" name="searchStatus" value="검색" class="btn-primary" onclick="javascript:search('D',this);">
                            </td>
                            <td>
                                <%-- <input type="button" id="searchStatusO" name="searchStatus" value="기사 정산" class="<c:if test="${paramMap.searchStatus == 'O'}">btn-primary</c:if><c:if test="${paramMap.searchStatus != 'O'}">btn-normal</c:if>" onclick="javascript:searchStatusChange('O',this);"> --%>
                            </td>
                        </tr>
                    </tbody>
                </table>
      <%--           	<input type="text" id="searchWord" name="searchWord" value="${paramMap.searchWord}" placeholder="검색어 입력"  onkeypress="if(event.keyCode=='13'){encodeURI($(this).val()); document.searchForm.submit();}" />
							<a class="btn-gray" href="javascript:" onclick=" encodeURI($(this).prev().val()); document.searchForm.submit()"><input type="button" value="검색"></a> --%>
            </div>

        </section>

            <section class="bottom-table" style="width:1600px; margin-left:290px;">
            	 <!--<div style="color:#8B0000;">*항목코드 1 : 청구내역공제,2 : 대납내역공제</div>
            	 <div style="color:#8B0000;">*구분코드 1 : 보험료,2 : 차량할부,3 : 과태료,4 : 미납통행료,5 : 사고대납금,6 : 기타</div> -->
                <table class="article-table" style="margin-top:15px; table-layout:fixed;">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                        	<td style="text-align:center; width:120px;">기사명</td>
                            <td style="text-align:center; width:160px;">차량번호</td>
                            <td style="text-align:center; width:150px; display:none;" name="phoneNumParent">연락처</td>
                            <td style="text-align:center; width:130px;">위반일자</td>
                            <td style="text-align:center; width:130px;">시간</td>
                            <td style="text-align:center;">위반장소</td>
                            <td style="text-align:center;">위반내용</td>
                            <td style="text-align:center; width:100px;">금액</td>
                            <td style="text-align:center; width:200px;">납부정보</td>
                            <td style="text-align:center; width:130px;">납부마감기간</td>
                            <td style="text-align:center; width:100px;">파일</td>
                            <td style="text-align:center; width:50px;">확인여부</td>
                            <td style="text-align:center; width:50px;">납부여부</td>
                            <td style="text-align:center; width:70px;">작성자</td>
                            <td style="text-align:center; width:100px;">관리</td>
                        </tr>
                    </thead>
                    <tbody id="dataArea">
	                   	
    	                	<tr class="ui-state-default">
	                    		<td style="text-align:center;">
	                    			<form action="">																
	                                	<input type="text" placeholder="기사명" class="search-box" id="findDriver" name="findDriver" id="" style="margin-top:5px; height:26px;"> 		
	                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
	                                    <div class="search-result">
	                                    </div>
	                                </form>
	                    		</td>
	                    		
	                    		<td style="text-align:center;">
	                    			<form action="">
	                                	<input type="text" placeholder="차량번호" class="search-box" id="findCarNum" name="findCarNum" id="" onclick="javascript:$(this).val('');" style="margin-top:5px; height:26px;"> 		
	                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
	                                    <div class="search-result">
	                                    </div>
	                                </form>
			                    </td>
	                    		
	                    		<form id="excelForm" style="display:none" name="excelForm" method="post" enctype="multipart/form-data" autocomplete="false">
		                    		<input type="hidden" style=""  name="driverId" id="driverId" value="">
			                   		<input type="hidden" style=""  name="driverName" id="driverName" value="">
			                   		<input type="hidden" style=""  name=selectMonth id="selectMonth" value="${paramMap.selectMonth}">
			                   		<input type="hidden" style=""  name="carNum" id="carNum" value="">
			                   		<!-- <td style="text-align:center;">
			                    		<input type="text" style="width:80%;" placeholder="차량번호" name="carNum" id="carNum"  onclick="javascript:$(this).val('');" value="">
			                    	</td> -->
			                    	<td style="text-align:center; display:none;" id="phoneNumParent" name="phoneNumParent">
			                    		<input type="text" style="width:80%;" placeholder="연락처" name="phoneNum" id="phoneNum"  onclick="javascript:$(this).val('');" value="">
			                    	</td>
			                    	<td style="text-align:center;"><input style="width:80%;" autocomplete="false"class="datepick" type="text" placeholder="위반일자" name="occurrenceDt" id="occurrenceDt" onclick="javascript:$(this).val('');" onfocus=></td>
			                    	<td style="text-align:center;">
			                    		<input type="text" style="width:80%;" placeholder="시간" name="occurrenceTime" id="occurrenceTime"  >
			                    	</td>
			                    	
			                    	<td style="text-align:center;">
			                    		<input type="text" style="width:80%;" placeholder="위반장소" name="placeAddress" id="placeAddress" >
			                    	</td>
			                    	<td style="text-align:center;">
			                    		<input type="text" style="width:80%;" placeholder="위반내용" name="summary" id="summary">
			                    	</td>
			                    	<td style="text-align:center;"><input style="width:80%;" type="text" placeholder="금액" name="amount" id="amount" onkeyup="javascript:getNumber(this);"></td>
			                    	
			                    	<td style="text-align:center;"><input style="width:100%;" type="text" placeholder="은행/예금주/납부계좌번호" name="accountInfo" id="accountInfo"></td>
			                    	
			                    	<td style="text-align:center;"><input style="width:80%;" autocomplete=”false” class="datepick" type="text" placeholder="납부마감기간" name="endDt" id="deadLindDt" onclick="javascript:$(this).val('');"></td>
			                    	<td style="text-align:center;">
			                    		<a style="cursor:pointer; width:60px;" onclick="javascript:$(this).next().trigger('click');" class="btn-edit">파일선택</a>
			                    		<input style="display:none;" type="file" placeholder="" name="bbsFile" id="bbsFile" onchange="javascript:$(this).prev().html('선택완료');" >
			                    	</td>
			                    	<td style="text-align:center;"></td>
			                    	<td style="text-align:center;"></td>
			                    	<td style="text-align:center;">${user.emp_name}</td>
			                    	<td style="text-align:center;">
			                    		<a style="cursor:pointer; width:60px;" onclick="javascript:addLowViolation(this)" class="btn-edit">등록</a>
			                    	</td>
		                    	</form>
	                    	</tr>
                    	
                    	<%-- 
                    	<c:forEach var="data" items="${listData}" varStatus="status">
							<tr class="ui-state-default"> 
								<td style="text-align:center;">${data.driver_name}</td>
	                            <td style="text-align:center;">
	                            <div class="normal">${data.occurrence_dt}</div>
	                            <div class="mod" style="display:none;">
	                            	<input style=" width:80%;" class="datepick" type="text" placeholder="위반일자" value="${data.occurrence_dt}" onclick="javascript:$(this).val('');">
	                            </div>
	                            </td>
	                            <td style="text-align:center;">
	                            	<div class="normal">
	                            		${data.occurrence_time}
	                            	</div>
	                            	<div class="mod" style="display:none;">
	                            		<input style=" width:80%;" type="text" placeholder="위반시간" value="${data.occurrence_time}" onclick="javascript:$(this).val('');">
	                    			</div>
	                            </td>
	                            <td style="text-align:center;">
	                            	<div class="normal">
		                            	${data.car_num }
	                            	</div>
	                            	<div class="mod" style="display:none;">
	                            		<input style=" width:80%;" type="text" placeholder="차량번호" value="${data.car_num}" onclick="javascript:$(this).val('');">
	                    			</div>
	                            </td>
	                            <td style="text-align:center;">
	                            	<div class="normal">
		                            	${data.place_address }
	                            	</div>
	                            	<div class="mod" style="display:none;">
	                            		<input style=" width:80%;" type="text" placeholder="위반장소" value="${data.place_address}" onclick="javascript:$(this).val('');">
	                    			</div>
	                            </td>
	                            <td style="text-align:center;">
	                            	<div class="normal">
		                            	${data.summary }
	                            	</div>
	                            	<div class="mod" style="display:none;">
	                            		<input style=" width:80%;" type="text" placeholder="위반내용" value="${data.summary}" onclick="javascript:$(this).val('');">
	                    			</div>
	                            </td>
	                            
	                            <td style="text-align:center;">
	                            	<div class="normal">
	                            		${data.amount}
	                            	</div>
	                            	<div class="mod"  style="display:none;">
	                            		<input style=" width:80%;" type="text" placeholder="금액" value="${data.amount}" onkeyup="javascript:getNumber(this);">
	                            	</div>
	                            </td>
	                            <td style="text-align:center;">
	                            	<div class="normal">
	                            		${data.end_dt}
	                            	</div>
	                            	<div class="mod" style="display:none;">
	                            		<input style=" width:80%;" class="datepick" type="text" placeholder="납부마감기간" value="${data.end_dt}" >
	                            	</div>
	                            </td>
	                            <td style="text-align:center;">
	                            	<c:if test="${data.file_path != '-'}">
		                            	<a target="_blank" href="/files/lowViolation${data.file_path }">
								             	<img style="width:30px; height:25px; margin-left:10px;" src="/files/lowViolation${data.file_path }" alt="${data.file_nm}" title="${data.file_nm}" />
										</a>
	                            	</c:if>
	                            	<c:if test="${data.file_path == '-'}">
	                            		파일없음
	                            	</c:if>
	                            	
	                            </td>
	                            
	                            <td style="text-align:center;">
	                            	<c:if test="${data.read_yn == 'Y'}">읽음</c:if>
	                            	<c:if test="${data.read_yn == 'N'}">읽지않음</c:if>
	                            </td>
	                            <td style="text-align:center;">
	                            	<c:if test="${data.payment_yn == 'Y'}">납부완료</c:if>
	                            	<c:if test="${data.payment_yn == 'N'}">미납</c:if>
	                            </td>
	                            <td style="text-align:center;">
	                            		${data.register_name}
	                            </td>
	                            <td style="text-align:center;">
	                            	<c:if test="${data.payment_yn == 'N'}">
	                            		<a style="cursor:pointer; width:50px;" onclick="javascript:editLowViolation('${data.low_violation_id}','${data.driver_id}',this)" class="btn-edit">수정</a>
	                            	</c:if>
	                            	<a style="display:none; cursor:pointer; width:50px;" onclick="javascript:setLowViolation('${data.low_violation_id}','${data.driver_id}',this)" class="btn-edit">완료</a>
	                            	<a style="cursor:pointer; width:50px;" onclick="javascript:deleteLowViolation('${data.low_violation_id}','${data.driver_id}',this)" class="btn-edit">삭제</a>
	                            </td>
                        	</tr>
						</c:forEach>
                         --%>
                    </tbody>
                </table>
                
                <table class="article-table" style="margin-top:15px">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <!-- <td style="text-align:center;">기사아이디</td> -->
                            <td style="text-align:center; width:50px;"><input type="checkbox" id="checkAll" onclick="javascript:checkAll();" ></td>
                            <td style="text-align:center;">기사명</td>
                            <td style="text-align:center;">위반건</td>
                            <td style="text-align:center;">위반금액</td>
                            <td style="text-align:center;">읽은건</td>
                            <td style="text-align:center;">납부금액</td>
                        </tr>
                    </thead>
                    <tbody id="">
                    	<c:forEach var="data" items="${listData}" varStatus="status">
							<tr class="ui-state-default" style=" cursor:pointer;" onclick="javascript:viewLowViolation('${data.driver_id}');"> 
	                            <td style="text-align:center;"><input type="checkbox" name="forDeductStatus" driverId="${data.driver_id}"></td>
	                            <td style="text-align:center;" >${data.driver_name}</td>
	                            <td style="text-align:center;">${data.violation_cnt}</td>
	                            <td style="text-align:center;">${data.amount}</td>
	                            <td style="text-align:center;">${data.read_cnt}</td>
	                            <td style="text-align:center;">${data.payment_amount}</td>
                        	</tr>
						</c:forEach>
                        
                    </tbody>
                </table>
                
                
                <div class="table-pagination text-center">
                    <ul class="pagination">
                        <!-- <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="curr-page"><a href="#">1</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li> -->
                        <html:paging uri="/carmanagement/lowViolation-reg.do" forGroup="&occurrenceDt=${paramMap.occurrenceDt}" />
                    </ul>
                </div>
                <div class="confirmation">
                    <div class="confirm">
                        <!-- <a href="/baseinfo/add-employee.do">직원등록</a> -->
                    </div>
                </div>
                
            </section>
     
