<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>

<!-- 테이블 컨트롤시 사용한다. -->
<!-- <link rel="stylesheet" href="https://cdn.datatables.net/t/bs-3.3.6/jqc-1.12.0,dt-1.10.11/datatables.min.css"/>  -->
<!-- <script src="https://cdn.datatables.net/t/bs-3.3.6/jqc-1.12.0,dt-1.10.11/datatables.min.js"></script> -->

<script src="/js/main.js"></script>


<script>
//테이블 컨트롤시 사용한다 
//     jQuery(function($){
//         $("#dataTable").DataTable({
//         	"ordering": false, //칼럼별 정렬기능
// 			"lengthMenu": [ 10, 30, 50, 100 ], // 표시 건수를 10건 단위로 설정
// 			"displayLength": 50, // 기본 표시 건수를 100건으로 설정
// 			"info": false, // 정보 표시 숨기기
// 			"paging": false, // 페이징 기능 숨기기
// 			"lengthChange": false, //표시건수기능
// 			"searching": false, //검색
// 			"columnDefs": [
// 			    { "orderable": false, "targets": 0}
// 			    ]
//     });
</script>

<script src="/js/util.js"></script>
<script type="text/javascript">
$(document).ready(function(){
//  	$("#dataTable_paginate").css("display","none"); // DataTable CSS에서 paging만 숨기고 싶을때
// 	$('#dataTable tr:odd td').css("backgroundColor","#f9f9f9");
 	
	//pagetotal 카우트 처리에 필요한 전처리
	pageTotalCount = $('input:checkbox[name="forBatch"]').length;
	$("span[name=pageTotalCount]").text(pageTotalCount);
	
	$("#selectMonth").MonthPicker({ 
		Button: false
		,MonthFormat: 'yy-mm'	
		,OnAfterChooseMonth: function() { 
	        //alert($(this).val());
	        document.location.href = "/carmanagement/driverList.do?&searchWord="+encodeURI($("#searchWord").val())+"&selectMonth="+$(this).val();
	    }
	});



	
});


//등록
function goAdd(){
	
	$("#insertForm #carNum").val('');
	$("#insertForm #driverName").val('');
	$("#insertForm #bizNum").val('');
	$("#insertForm #startDate").val('');
	$("#insertForm #endDate").val('');
	$("#insertForm #inspectionDate").val('');	
	
	$("#insertForm").css("display","block");
	$("#modForm").css("display","none");
	$(".modal").fadeIn();
	//document.location.href ="/carmanagement/add-insurance.do";
}

//수정
function goMod(car_num,driver_name,business_license_number,start_date,end_date,inspectionDate, inspection_possible_yn){

	$("#insertForm").css("display","none");
	$("#modForm").css("display","block");
	
	$("#modForm #carNum_mod").val('');
	$("#modForm #driverName_mod").val('');
	$("#modForm #bizNum_mod").val('');
	$("#modForm #startDate_mod").val('');
	$("#modForm #endDate_mod").val('');
	$("#modForm #inspectionDate_mod").val('');	
		
	$("#modForm #carNum_mod").val(car_num);
	$("#modForm #driverName_mod").val(driver_name);
	$("#modForm #bizNum_mod").val(business_license_number);
	$("#modForm #startDate_mod").val(start_date);
	$("#modForm #endDate_mod").val(end_date);
	$("#modForm #inspectionDate_mod").val(inspectionDate);
	
	
	if (inspection_possible_yn =='Y'){
		$('input:radio[name="inspectionPossibleYn_mod"]:input[value="Y"]').prop("checked", true);
	}else	if (inspection_possible_yn =='N'){
		$('input:radio[name="inspectionPossibleYn_mod"]:input[value="N"]').prop("checked", true);
	}
		
	$(".modal").fadeIn();
	//document.location.href ="/carmanagement/add-insurance.do";
}

function popUpClose(obj){
	$(".modal").fadeOut();
}


function search(){
	
	document.location.href = "/carmanagement/carInspectionManage.do?&searchWord="+encodeURI($("#searchWord").val())+"&startDt="+encodeURI($("#startDt").val())+"&endDt="+encodeURI($("#endDt").val());
	
}




var rgx1 = /\D/g;  // /[^0-9]/g 와 같은 표현
var rgx2 = /(\d+)(\d{3})/; 

function getNumber(obj){
	
    var num01;
    var num02;
    num01 = obj.value;
    num02 = num01.replace(rgx1,"");
    num01 = setComma(num02);
    obj.value =  num01;
    
}

function setComma(inNum){
    
    var outNum;
    outNum = inNum; 
    while (rgx2.test(outNum)) {
         outNum = outNum.replace(rgx2, '$1' + ',' + '$2');
     }
 
    return outNum;

}



function addInspection(){
	
        if($("#carNum").val() == ""){
			alert("차량번호가 입력되지 않았습니다.");
			return false;
		}	
        if($("#startDate").val() == ""){
			alert("검사기간 시작일이 입력되지 않았습니다.");
			return false;
		}
        if($("#driverName").val() == ""){
			alert("기사명이 입력되지 않았습니다.");
			return false;
		}
        if($("#endDate").val() == ""){
			alert("검사기간 종료일이 입력되지 않았습니다.");
			return false;
		}	
       
        if($("#inspectionDate").val() == ""){
        	alert("검사일이 입력되지 않았습니다.");
			return false;
        }
        if($("#inspectionDate").val() == ""){
        	alert("검사일이 입력되지 않았습니다.");
			return false;
        }
        
    	if($("#bizNum").val() == ""){
    		alert("사업자번호가 입력되지 않았습니다.");
    		return false;
    	}else{
    		if(!businessNumChk($("#bizNum"))){
    	  		alert("유효하지 않은 사업자번호 입니다.");
    	  		$("#bizNum").val("");
                $("#bizNum").focus();
    			return false;
    		}
    	}

        if($("#inspectionPossibleYn").val() == ""){
        	alert("검사가능여부가 입력되지 않았습니다.");
			return false;
        }
        
		if(confirm("등록 하시겠습니까?")){
			//$("#insertForm").attr("action","/baseinfo/insert-employee.do?status=noLogin");
			$("#insertForm").submit();	
		}
	
}


function modInspection(){
	
        
        if($("#startDate_mod").val() == ""){
			alert("검사기간 시작일이 입력되지 않았습니다.");
			return false;
		}
        if($("#endDate_mod").val() == ""){
			alert("검사기간 종료일이 입력되지 않았습니다.");
			return false;
		}	
        if($("#driverName_mod").val() == ""){
			alert("기사명이 입력되지 않았습니다.");
			return false;
		}       
        if($("#inspectionDate_mod").val() == ""){
        	alert("검사일이 입력되지 않았습니다.");
			return false;
        }
        
    	if($("#bizNum_mod").val() == ""){
    		alert("사업자번호가 입력되지 않았습니다.");
    		return false;
    	}else{
    		if(!businessNumChk($("#bizNum_mod"))){
    	  		alert("유효하지 않은 사업자번호 입니다.");
    	  		$("#bizNum_mod").val("");
                $("#bizNum_mod").focus();
    			return false;
    		}
    	}
        if($("#inspectionPossibleYn_mod").val() == ""){
        	alert("검사가능여부가 입력되지 않았습니다.");
			return false;
        }

		if(confirm("수정 하시겠습니까?")){
			$("#modForm").submit();	
		}
	
}



</script>
        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">차량관리</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">차량검사관리</a></li>
                </ul>
            </div>
            
               <div class="dispatch-btn-container">
				<div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">차량검사관리</div>
				 <table width="100%">
				 		<tr>
							<td>
								<div class="left-box">
								 	※남은 검사일이 한 달 남은 차량은 <span style="color:#f00;">붉은색</span>으로 표시됩니다.
								</div>							
							</td>
						<tr>
					</table>
            </div>
            
  			<div class="up-dl clearfix header-search">
                 <table>
                    <tbody>
                        <tr>
                        	<td>검사일 :</td>
				            <td class="widthAuto" style="width:400px;">
							     <div class="date-picker">
			                		<input style="width:150px;" autocomplete="off" id="startDt" placeholder="검색 시작일" name="startDt" type="text" class="datepick" value="${paramMap.startDt}"> ~ <input style="width:150px;" autocomplete="off" id="endDt" placeholder="검색 종료일" name="endDt"  type="text" class="datepick" value="${paramMap.endDt}">
				                </div>
				            </td>
				            <td>차량번호 :</td>
	                            <td style="width: 190px;">
	                                <input type="text" id="searchWord" name="searchWord" value="${paramMap.searchWord}"   placeholder="차량 번호 검색" onkeypress="if(event.keyCode=='13') search();">
	                            </td>
                            <td>
                                <input type="button" id="btn-search" value="검색" class="btn-primary" onclick="javascript:search();">                                
                            </td>
                            <td>
								<input type="button" id="btn-add" value="등록" class="btn-primary" onclick="javascript:goAdd();">
							</td>
                            
                        </tr>
                    </tbody>
                </table>
            </div>
        </section>
		
		
            <section class="bottom-table" style="width:1600px; margin-left:290px;">
   
		<!--  공차 아닌 테이블 -->
							
                <table class="article-table" id="dataTable" style="margin-top:15px">
                    <colgroup>
                         <col width="auto">
                         <col width="auto">
                         <col width="auto">
                         <col width="auto">
                         <col width="auto">
                         <col width="auto">
                    </colgroup>
                    <thead>
                        <tr>
                            <!-- <td style="text-align:center;">기사아이디</td> -->
<!--                             <td class="showToggle"><input type="checkbox" id="checkAll" onclick="javascript:checkAll();" ></td> -->
                            <td style="text-align:center;">차량번호</td>
                            <td style="text-align:center;">기사명</td>
                            <td style="text-align:center;">사업자등록번호</td>
                            <td style="text-align:center;">검사기간</td>
                            <td style="text-align:center;">검사일</td>
                            <td style="text-align:center;">검사가능여부</td>
                            <td style="text-align:center;"></td>
                        </tr>
                    </thead>
                    <tbody id="">
                    	<c:forEach var="data" items="${listData}" varStatus="status">
                   			<tr class="ui-state-default" > 
<!-- 							<td class="showToggle"><input type="checkbox" name="forBatch"  onclick="javascript:getCheckBoxCnt();"> </td> -->
                            <td style="text-align:center;"  align = "center">${data.car_num}</td> <!--  차량번호-->
                            <td style="text-align:center;"  align = "center">${data.driver_name}</td> <!-- 기사명 -->
                            <td style="text-align:center;"  align = "center">${data.business_license_number}</td><!-- 사업자등록번호 -->
							<td style="text-align:center;"> ${data.start_date} ~ ${data.end_date}</td> <!-- 검사기간 -->
							<td style="text-align:center;"> <span <c:if test="${data.checkMonth eq 'Y'}"> style="color:#f00;" </c:if>>${data.inpection_date} </span></td> <!-- 검사일 -->							
							<td style="text-align:center;">
								  <input type="radio" value="Y" <c:if test ="${data.inspection_possible_yn == 'Y' }" > checked </c:if> >
								  <label for="inspectionPossibleYn">Y</label>
								  &nbsp;&nbsp;
								  <input type="radio" value="N" <c:if test ="${data.inspection_possible_yn == 'N' }" > checked </c:if> >
								  <label for="inspectionPossibleYn">N</label>							
							</td> <!-- 검사가능여부 -->									 												 
							<td style="width:100px;">
							 	 <a style="cursor:pointer;" onclick="javascript:goMod('${data.car_num}','${data.driver_name}','${data.business_license_number}','${data.start_date}','${data.end_date}','${data.inpection_date}','${data.inspection_possible_yn}');" class="btn-edit">수정</a>
                           </td>
						 </tr>
						</c:forEach>
          
                    </tbody>
                </table>
                
                <div class="table-pagination text-center">
                    <ul class="pagination">
                        <html:paging uri="/carmanagement/driverList.do" forGroup="&selectMonth=${paramMap.selectMonth}" />
                    </ul>
                </div>
          <%--      <div class="confirmation">
                    <div class="confirm">
                        <a href="/account/driverList_download.do?selectMonth=${paramMap.selectMonth}">엑셀다운로드</a>
                    </div>--%>
                    <%-- <div class="confirm">
                        <a href="/account/driverList_download.do?selectMonth=${paramMap.selectMonth}"></a>
                    </div> 
                </div>--%>
                
            </section>
     
     
      <div class="modal">
        <div class="modal-con2" style="">
			<div class="dispatch-wrapper" style="width:500px;">
           		<section class="dispatch-bottom-content active" style=" margin:auto; width:100%;">
           		
		<!-- -------------------------------------- 등록 FORM ------------------------------------------------------	                 -->           		
	                <form id="insertForm" action="/carmanagement/insertInspection.do"  method="post" enctype="multipart/form-data">
	                   <h3 style="font-size: 20px; text-align: center; margin-bottom:20px; font-weight: 700; font-family: NotoSansBold,sans-serif;">차량 검사 등록</h3>
		                <div class="form-con clearfix" style="max-width:500px;">
		                	<div style="margin-bottom:5px">
			                    <table>
				                    <tbody>
				                        <tr>
				                            <td>차량번호<span style="color:#F00;">&nbsp;*</span></td>
				                            <td class="widthAuto" >
				                                <input type="text" placeholder="차량번호" name="carNum" id="carNum">
				                            </td>
										</tr>
										<tr>
				                            <td>기사명<span style="color:#F00;">&nbsp;*</span></td>
				                            <td class="widthAuto" >
				                                <input type="text" placeholder="기사명" name="driverName" id="driverName">
				                            </td>										
										</tr>
				                        <tr>
				                            <td>사업자등록번호<span style="color:#F00;">&nbsp;*</span></td>
				                            <td class="widthAuto" >
				                                <input type="text" placeholder="사업자등록번호" name="bizNum" id="bizNum">
				                            </td>				                        
				                        </tr>
				                        <tr>
											<td>검사기간<span style="color:#F00;">&nbsp;*</span></td>
				                            <td class="widthAuto">
				                                <input type="text" placeholder="시작" class="datepick" name="startDate" id="startDate">
				                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;~
				                                <input type="text" placeholder="종료" class="datepick" name="endDate" id="endDate">
				                            </td>				                        
				                        </tr>
				                        <tr>
				                            <td>검사일<span style="color:#F00;">&nbsp;*</span></td>
				                            <td class="widthAuto" >
				                                <input type="text" placeholder="검사일" class="datepick" name="inspectionDate" id="inspectionDate">
				                            </td>				                        
				                        </tr> 
				                        <tr>
											<td>검사가능여부<span style="color:#F00;">&nbsp;*</span></td>
				                            <td class="widthAuto" >
				                                <div>
												  <input type="radio" id="inspectionPossibleYn" name="inspectionPossibleYn" value="Y">
												  <label for="inspectionPossibleYn">Y</label>
												  &nbsp;&nbsp;
												  <input type="radio" id="inspectionPossibleYn" name="inspectionPossibleYn" value="N">
												  <label for="inspectionPossibleYn">N</label>
												</div>
				                            </td>				           				                        
				                        </tr>
				                    </tbody>
				                </table>
			                </div>
		                </div>
		                <div class="confirmation">
			                <div class="confirm">
			                	<input type="button" value="등록" onclick="javascript:addInspection();">
			                </div>
			                <div class="cancel">
		                            <input type="button" value="취소" onclick="javascript:popUpClose(this);">
		                     </div>
		                </div>
	                </form>
			<!-- -------------------------------------- 등록 FORM ------------------------------------------------------	                 -->
		
			<!-- -------------------------------------- 수정 FORM ------------------------------------------------------	                 -->
	                 <form id="modForm" action="/carmanagement/updateInspection.do"  method="post" enctype="multipart/form-data">
	                   <h3 style="font-size: 20px; text-align: center; margin-bottom:20px; font-weight: 700; font-family: NotoSansBold,sans-serif;">차량 검사 관리 수정</h3>
		                <div class="form-con clearfix" style="max-width:500px;">
		                	<div style="margin-bottom:5px">
			                    <table>
				                    <tbody>
				                        <tr>
				                            <td>차량번호<span style="color:#F00;">&nbsp;*</span></td>
				                            <td class="widthAuto" >
				                                <input type="text" placeholder="차량번호" name="carNum_mod" id="carNum_mod" readonly style="border: 0;box-shadow: none; background-color: gray;">
				                            </td>
										</tr>
										<tr>
				                            <td>기사명<span style="color:#F00;">&nbsp;*</span></td>
				                            <td class="widthAuto" >
				                                <input type="text" placeholder="기사명" name="driverName_mod" id="driverName_mod">
				                            </td>										
										</tr>
				                        <tr>
				                            <td>사업자등록번호<span style="color:#F00;">&nbsp;*</span></td>
				                            <td class="widthAuto" >
				                                <input type="text" placeholder="사업자등록번호" name="bizNum_mod" id="bizNum_mod">
				                            </td>				                        
				                        </tr>
				                        <tr>
											<td>검사기간<span style="color:#F00;">&nbsp;*</span></td>
				                            <td class="widthAuto">
				                                <input type="text" placeholder="시작" class="datepick" name="startDate_mod" id="startDate_mod">
				                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;~
				                                <input type="text" placeholder="종료" class="datepick" name="endDate_mod" id="endDate_mod">
				                            </td>				                        
				                        </tr>
				                        <tr>
				                            <td>검사일<span style="color:#F00;">&nbsp;*</span></td>
				                            <td class="widthAuto" >
				                                <input type="text" placeholder="검사일" class="datepick" name="inspectionDate_mod" id="inspectionDate_mod">
				                            </td>				                        
				                        </tr> 
				                        <tr>
											<td>검사가능여부<span style="color:#F00;">&nbsp;*</span></td>
				                            <td class="widthAuto" >
				                                <div>
												  <input type="radio" value="Y" id="inspectionPossibleYn_mod" name="inspectionPossibleYn_mod" >
												  <label for="inspectionPossibleYn_mod">Y</label>
												  &nbsp;&nbsp;
												  <input type="radio" value="N" id="inspectionPossibleYn_mod" name="inspectionPossibleYn_mod" >
												  <label for="inspectionPossibleYn_mod">N</label>
												</div>
				                            </td>				           				                        
				                        </tr>
				                    </tbody>
				                </table>
			                </div>
		                </div>
		                <div class="confirmation">
			                <div class="confirm">
			                	<input type="button" value="수정" onclick="javascript:modInspection();">
			                </div>
			                <div class="cancel">
		                            <input type="button" value="취소" onclick="javascript:popUpClose(this);">
		                     </div>
		                </div>
	                </form>
	                <!-- -------------------------------------- 수정 FORM ------------------------------------------------------	                 -->
	                
				</section>
        	</div> 
		</div>
    </div>     
