<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>
<%

%>

<script type="text/javascript">
$(document).ready(function(){
	
	$("#selectMonth").MonthPicker({ 
		Button: false
		,MonthFormat: 'yy-mm'	
		,OnAfterChooseMonth: function() { 
	        //alert($(this).val());
	        document.location.href = "/carmanagement/driverDeposit.do?&searchWord="+encodeURI($("#searchWord").val())+"&selectMonth="+$(this).val();
	    }
	});
	
});


var rgx1 = /\D/g;  // /[^0-9]/g 와 같은 표현
var rgx2 = /(\d+)(\d{3})/; 

function getNumber(obj){
	
    var num01;
    var num02;
    num01 = obj.value;
    num02 = num01.replace(rgx1,"");
    num01 = setComma(num02);
    obj.value =  num01;
      
}

function setComma(inNum){
    
    var outNum;
    outNum = inNum; 
    while (rgx2.test(outNum)) {
         outNum = outNum.replace(rgx2, '$1' + ',' + '$2');
     }
    return outNum;

}



function search(searchStatus,obj){
	
	document.location.href = "/carmanagement/lowViolation-reg.do?selectMonth="+$("#startDt").val();
}

function addEmp(){
	document.location.href = "/baseinfo/add-employee.do";
}

function getNumberOnly (str) {			//저장할때는 콤마 제거 후 저장한다.
    var len = str.length;
    var sReturn = "";

    for (var i=0; i<len; i++){
        if ( (str.charAt(i) >= "0") && (str.charAt(i) <= "9") ){
            sReturn += str.charAt(i);
        }
    }
    return sReturn;
}






function editDriverDeduct(driverId,selectMonth){
	
	document.location.href = "/account/driverCal.do?driverId="+driverId+"&selectMonth="+selectMonth;
}

/*
function downloadDriverDeduct(driverId,selectMonth){
	
	document.location.href = "/account/downloadDriverDeductInfo.do?driverId="+driverId+"&selectMonth="+selectMonth;
}
*/


function addEmp(){
	document.location.href = "/baseinfo/add-employee.do";
}


function search(){
	
	document.location.href = "/carmanagement/driverList.do?&searchWord="+encodeURI($("#searchWord").val())+"&selectMonth="+$("#selectMonth").val();
	
}

function viewMonthDriverDeduct(driverId){
	
	document.location.href = "/account/viewMonthDriverDeduct.do?driverId="+driverId;
	
}


function insertDriverDeductStatus(driverName,driverId,selectMonth){
	
	
	if(confirm(driverName+" 기사님의 "+selectMonth+"월의 공제내역을 확정 하시겠습니까?")){
		
		$.ajax({ 
			type: 'post' ,
			url : "/account/insertDriverDeductStatus.do" ,
			dataType : 'json' ,
			data : {
				driverName : driverName,
				driverId : driverId,
				decideMonth : selectMonth
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("확정 되었습니다.");
					window.location.reload();
				}else if(result != "0000"){
					alert("확정 하는데 실패 했습니다. 관리자에게 문의 하세요.");
					window.location.reload();
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
		
		
	}else{
		
		
		
	}
	
	
}

function checkAll(){

	if($('input:checkbox[id="checkAll"]').is(":checked")){
		$('input:checkbox[name="forDeductStatus"]').each(function(index,element) {
			$(this).prop("checked","true");
		 });	
	}else{
		$('input:checkbox[name="forDeductStatus"]').each(function(index,element) {
			$(this).prop("checked","");
		 });
	}
}


function insertAllDriverDeductStatus(selectMonth){
	
	var total = $('input:checkbox[name="forDeductStatus"]:checked').length;
	var driverId = "";
	
	if(total == 0){
		alert("확정할 목록이 선택 되지 않았습니다.");
		return false;
	}else{
		$('input:checkbox[name="forDeductStatus"]:checked').each(function(index,element) {
		      if(this.checked){	//checked 처리된 항목의 값
		    	  var selectedObj = new Object();
		    	  driverId+=$(this).attr("driverId");
		    	  if(index<total-1){
		    		  driverId += ","; 
			         } 
		      }
		 });
		
		if(driverId != ""){
			insertDeductStatus(driverId,total,selectMonth);
		}
			

	}
	
	
	/* if(confirm(selectMonth+"월의 공제내역을 일괄확정 하시겠습니까?")){
		
		$.ajax({ 
			type: 'post' ,
			url : "/account/insertAllDriverDeductStatus.do" ,
			dataType : 'json' ,
			data : {
				selectMonth : selectMonth,
				searchType : "${paramMap.searchType}",
				searchWord : "${paramMap.searchWord}"
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("확정 되었습니다.");
					window.location.reload();
				}else if(result != "0000"){
					alert("확정 하는데 실패 했습니다. 관리자에게 문의 하세요.");
					window.location.reload();
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
		
		
	}else{
		
	} */
	
	
}



function addDriverPayment(obj){

	//작성되어야 하는 내용이 전부 작성 되었는지 확인하는 로직 추가.....

	
	
	if($("#driverName").val() == ""){
		alert("기사명이 입력 되지 않았습니다.");
		return false;
	}

	
	if($("#paymentStartDt").val() == ""){
		alert("시작일이 입력 되지 않았습니다.");
		return false;
	}

	if($("#assignDivision").val() == ""){
		alert("구분이 입력 되지 않았습니다.");
		return false;
	}

	if($("#workKind").val() == ""){
		alert("업무구분이 입력 되지 않았습니다.");
		return false;
	}

	if($("#paymentYn").val() == ""){
		alert("납부여부가 입력 되지 않았습니다.");
		return false;
	}
	
	
	if($("#etc").val() == ""){

		if(confirm("비고가 등록되지 않았습니다. 비고 없이 등록 하시겠습니까?")){
			
		}else{
			alert("취소 되었습니다.");
			return false;
		}
	}

	if(confirm("해당 내용을 등록 하시겠습니까?")){
		$("#excelForm").attr("action","/carmanagement/insertDriverPayment.do");
		$("#excelForm").submit();
	}
	

	
}









function insertDeductStatus(driverId,total,selectMonth){
	
	
	if(confirm(total+"명의 공제내역을 확정 하시겠습니까?")){
		
		$.ajax({ 
			type: 'post' ,
			url : "/account/insertAllDriverDeductStatus.do" ,
			dataType : 'json' ,
			data : {
				selectMonth : selectMonth,
				driverId : driverId
				//searchWord : "${paramMap.searchWord}"
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("확정 되었습니다.");
					window.location.reload();
				}else if(result != "0000"){
					alert("확정 하는데 실패 했습니다. 관리자에게 문의 하세요.");
					window.location.reload();
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
		
		
	}else{
		
	}
}



function viewTotalSales(driverId){
	document.location.href = "/account/driverListDetail.do?&paymentPartnerId="+driverId+"&decideMonth="+$("#selectMonth").val();
}


function downloadDriverDeduct(driverId,selectMonth){
	
	window.open("/account/viewDriverDeductInfo.do?driverId="+driverId+"&selectMonth="+selectMonth,"_blank","top=0,left=0,width=780,height=1000,toolbar=0,status=0,scrollbars=0,resizable=0");
	
}



function viewDriverBillingFile(driverId,selectMonth){
	
	window.open("/account/viewDriverBillingFile.do?driverId="+driverId+"&selectMonth="+selectMonth,"_blank","top=0,left=0,width=1000,height=700,toolbar=0,status=0,scrollbars=0,resizable=0");
	
}

function viewLowViolation(driverId,occurrenceDt){


	document.location.href = "/carmanagement/viewLowViolation.do?driverId="+driverId+"&occurrenceDt="+occurrenceDt;
	
}


function setDriver(driverId,driverName,carNum,phoneNum,obj){

	
	if(driverId != ""){

		$("#findDriver").val(driverName);
		
		$("#driverId").val(driverId);
		$("#driverName").val(driverName);
		
	}else{

		
		$("#driverId").val(driverId);
		$("#driverName").val($("#findDriver").val());
		
	}

	$(obj).parent().parent().removeClass("active");
	
}



function updateAnotherDecideFinalForMakeExcel(){
	
	
	if(confirm($("#selectMonth").val()+"월분 운송비 정산 내역서를 일괄 성성 하시겠습니까?")){
	
	$.ajax({ 
		type: 'post' ,
		url : "/account/updateAnotherDecideFinalForMakeExcel.do" ,
		dataType : 'json' ,
		data : {
			decideMonth : $("#selectMonth").val()
		},
		success : function(data, textStatus, jqXHR)
		{
			var result = data.resultCode;
			var resultData = data.resultData;
			if(result == "0000"){
				alert("일괄 생성 되었습니다.");
			}else if(result == "0001"){
				alert("일괄 생성 하는데 실패 하였습니다.");
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
	}
	
	
}


var test;
function getAjaxData(val,obj,forPayment){

	test = obj;


	if(forPayment == "findDriver"){

			
		
			$.ajax({ 
				type: 'post' ,
				url : "/baseinfo/getDriverListAllForDriverPayment.do" ,
				dataType : 'json' ,
				async : false,
				data : {
					driverName : val
				},
				success : function(data, textStatus, jqXHR)
				{
					
					var list = data.resultData;
					var result = "";
					//$("#searchResult").html("");
					$(obj).parent().find("div").html("");
					if(list.length > 0){
		       			for(var i=0; i<list.length; i++){
		       				result += '<div class="Wresult">';
		       				/* result += '<a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>'; */
		       				result += '<p class="result-title">기사정보</p>';
		       				var strJsonText = JSON.stringify(obj);
		    	   		    strJsonText = strJsonText.replace(/\"/gi, "'");
		       				result += '<a style="cursor:pointer;"   onclick="javascript:setDriver(\''+list[i].driver_id+'\',\''+list[i].driver_name+'\',\''+list[i].car_num+'\',\''+list[i].phone_num+'\',this);" class="result-sub"><span>'+list[i].driver_name+'</span></a>';
		       				result += '<p class="camp-type"><span>차량번호:</span> <span>'+list[i].car_num+'</span></p>';	
		       				/* result += '<p class="camp-type"><span>주소:</span> <span>'+list[i].address+'</span></p>'; */	
		       				result += '<p class="camp-id"><span>Tel:</span> <span>'+list[i].phone_num+'</span></p>';
		       				result += '</div>';
		       			}
		       			result += '<div class="Wresult">';
	       				/* result += '<a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>'; */
	       				/* result += '<a style="cursor:pointer; margin-bottom:10px;" class="result-title result-sub">기사정보 직접입력</a>'; */
	       				/* result += '<a style="cursor:pointer; margin-bottom:10px;" onclick="javascript:$(this).parent().parent().removeClass(\'active\');" class="result-title result-sub">기사정보 직접입력</a>'; */
	       				result += '<a style="cursor:pointer; margin-bottom:10px;" onclick="javascript:setDriver(\'\',\'\',\'\',\'\',this);" class="result-title result-sub">기사정보 직접입력</a>';
	       				var strJsonText = JSON.stringify(obj);
	    	   		    strJsonText = strJsonText.replace(/\"/gi, "'");
	       				/* result += '<a style="cursor:pointer;"   onclick="javascript:setAddressInfo();" class="result-sub"><span></span></a>';
	       				result += '<p class="camp-type"><span>차량번호:</span> <span></span></p>';	
	       				result += '<p class="camp-type"><span>주소:</span> <span></span></p>';	
	       				result += '<p class="camp-id"><span>Tel:</span> <span></span></p>'; */
	       				result += '</div>';

					}else{

						result += '<div class="Wresult">';
	       				/* result += '<a href="" class="view-all">View all<i class="fa fa-external-link" aria-hidden="true"></i></a>'; */
	       				/* result += '<p class="result-title">기사정보 직접입력</p>'; */
	       				
	       				result += '<a style="cursor:pointer; margin-bottom:10px;" onclick="javascript:setDriver(\'\',\'\',\'\',\'\',this);" class="result-title result-sub">기사정보 직접입력</a>';
	       				
	       				/* result += '<a style="cursor:pointer; margin-bottom:10px;" onclick="javascript:$(this).parent().parent().removeClass(\'active\');" class="result-title result-sub">기사정보 직접입력</a>'; */
	       				var strJsonText = JSON.stringify(obj);
	    	   		    strJsonText = strJsonText.replace(/\"/gi, "'");
	       				result += '<a style="cursor:pointer;"   onclick="javascript:setAddressInfo();" class="result-sub"><span></span></a>';
	       				/* result += '<p class="camp-type"><span>차량번호:</span> <span></span></p>';	
	       				result += '<p class="camp-type"><span>주소:</span> <span></span></p>';	
	       				result += '<p class="camp-id"><span>Tel:</span> <span></span></p>'; */
	       				result += '</div>';
						//$(test).parent().find("div").removeClass('active');
					}
					
					//$("#searchResult").html(result);
					$(obj).parent().find("div").html(result);
					
				} ,
				error : function(xhRequest, ErrorText, thrownError) {
				}
			});



	}



	
}



function deleteDriverPayment(driverPaymentId,obj){
	
	
 	if(confirm("차량정보를 삭제 하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : "/carmanagement/deleteDriverPayment.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				driverPaymentId : driverPaymentId
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("삭제 되었습니다.");
					document.location.reload();
				}else if(result == "0001"){
					alert("조회 하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
	
}


function editDriverPayment(driverPaymentmainId,obj){
	
	$(obj).parent().parent().find(".normal").css("display","none");
	$(obj).parent().parent().find(".mod").css("display","");
	$(obj).css("display","none");
	$(obj).next().css("display","");
	
}


function setDriverPayment(driverPaymentId,obj){
	
	
	var driverPaymentInfo = new Object();
	
	if($(obj).parent().parent().find(".mod").eq(0).find("input").val() == ""){
		alert("시작일이 입력 되지 않았습니다.");
		return false;
	}else{
		driverPaymentInfo.payment_start_dt = $(obj).parent().parent().find(".mod").eq(0).find("input").val();	
	}	

	//alert(driverPaymentInfo.payment_start_dt);
	
	/* if($(obj).parent().parent().find(".mod").eq(1).find("input").val() == ""){
		alert("종료일이 입력 되지 않았습니다.");
		return false;
	}else{ */
		driverPaymentInfo.payment_end_dt = $(obj).parent().parent().find(".mod").eq(1).find("input").val();	
	/* } */

	//alert(driverPaymentInfo.payment_end_dt);
	
	if($(obj).parent().parent().find(".mod").eq(2).find("input").val() == ""){
		alert("지입금액이 입력 되지 않았습니다.");
		return false;
	}else{
		driverPaymentInfo.driver_payment = $(obj).parent().parent().find(".mod").eq(2).find("input").val();	
	}

	//alert(driverPaymentInfo.driver_payment);


	if($(obj).parent().parent().find(".mod").eq(3).find("select").val() == ""){
		alert("구분이 입력 되지 않았습니다.");
		return false;
	}else{
		driverPaymentInfo.assign_division = $(obj).parent().parent().find(".mod").eq(3).find("select").val();	
	}

	//alert(driverPaymentInfo.assign_division);

	
	if($(obj).parent().parent().find(".mod").eq(4).find("select").val() == ""){
		alert("업무구분이 입력 되지 않았습니다.");
		return false;
	}else{
		driverPaymentInfo.work_kind = $(obj).parent().parent().find(".mod").eq(4).find("select").val();	
	}

	//alert(driverPaymentInfo.work_kind);

	if($(obj).parent().parent().find(".mod").eq(5).find("select").val() == ""){
		alert("납부여부가 입력 되지 않았습니다.");
		return false;
	}else{
		driverPaymentInfo.payment_yn = $(obj).parent().parent().find(".mod").eq(5).find("select").val();	
	}

	//alert(driverPaymentInfo.payment_yn);
	
	
	if($(obj).parent().parent().find(".mod").eq(6).find("input").val() == ""){
		if(confirm("비고가 등록되지 않았습니다. 비고 없이 수정 하시겠습니까?")){
			driverPaymentInfo.etc = $(obj).parent().parent().find(".mod").eq(6).find("input").val();
		}else{
			alert("취소 되었습니다.");
			return false;
		}
		
	}else{
		driverPaymentInfo.etc = $(obj).parent().parent().find(".mod").eq(6).find("input").val();	
	}	

	//alert(driverPaymentInfo.etc);
	
 	if(confirm("지입료정보를 수정 하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : "/carmanagement/updateDriverPayment.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				driverPaymentInfo : JSON.stringify({driverPaymentInfo : driverPaymentInfo}),
				driverPaymentId : driverPaymentId
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("수정 되었습니다.");
					document.location.reload();
				}else if(result == "0001"){
					alert("조회 하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
	/* $(obj).parent().parent().find(".normal").css("display","");
	$(obj).parent().parent().find(".mod").css("display","none");
	$(obj).css("display","none");
	$(obj).prev().css("display",""); */
	
}








</script>
		<!-- <section class="side-nav"> -->
            <!-- <ul>
                <li class="active"><a href="/baseinfo/employee.do">직원관리</a></li>
                <li class=""><a href="/baseinfo/driver.do">기사관리</a></li>
                <li class=""><a href="/baseinfo/customer.do">거래처관리</a></li>
                <li class=""><a href="/baseinfo/company.do">회사정보관리</a></li>
            </ul> -->
        <!-- </section> -->

        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">차량관리</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">지입료 관리</a></li>
                </ul>
            </div>

            <div class="up-dl clearfix header-search">
                 <table>
                    <tbody>
                        <tr>
                        	<td>조회월 :</td>
				            <td class="widthAuto" style="width:200px;">
				                <input style="width:100%; text-align:center;" id="selectMonth" type="text" placeholder="조회월" onclick="javascript:$(this).val('');"  readonly="readonly" value="${paramMap.occurrenceDt}">
				                <%-- <input style="width:40%; text-align:center;" class="datepick" type="text" placeholder="조회종료일" onclick="javascript:$(this).val('');" readonly="readonly" name="endDt" id="endDt"  value="${paramMap.endDt}"> --%>
				            </td>
                            <td style="width: 190px;">
                                <input type="text" id="searchWord" name="searchWord" value="${paramMap.searchWord}"   onkeypress="if(event.keyCode=='13') search();">
                            </td>
                            <td>
                                <input type="button" id="btn-search" value="검색" class="btn-primary" onclick="javascript:search();">
                            </td>
                            
                            
                        </tr>
                    </tbody>
                </table>
            </div>
<!-- 
            <div class="dispatch-btn-container">
               
                <div class="dispatch-btn">
                    <i class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div>
            </div> -->
        </section>



            <section class="bottom-table" style="width:1600px; margin-left:290px;">
                
                <table class="article-table" style="margin-top:15px">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <!-- <td style="text-align:center;">기사아이디</td> -->
                            <!-- <td style="text-align:center; width:50px;"><input type="checkbox" id="checkAll" onclick="javascript:checkAll();" ></td> -->
                            <td style="text-align:center; width:120px;">차량번호</td>
                            <td style="text-align:center; width:120px;">차종</td>
                            <td style="text-align:center; width:120px;">차대번호</td>
                            <td style="text-align:center; width:120px;">기사명</td>
                            <td style="text-align:center; width:120px;">시작일</td>
                            <td style="text-align:center; width:120px;">종료일</td>
                            <td style="text-align:center; width:120px;">지입금액</td>
                            <!-- <td style="text-align:center; width:120px;">구분</td> -->
                            <td style="text-align:center; width:120px;">업무구분</td>
                            <td style="text-align:center; width:120px;">납부여부</td>
                            <td style="text-align:center; width:120px;">비고</td>
                            <td style="text-align:center; width:120px;">등록자</td>
                            <td style="text-align:center; width:150px;">관리</td>
                        </tr>
                    </thead>
                    <tbody id="">
                    	<tr class="ui-state-default">
	                    			
			                    	<td style="text-align:center;">${driverPaymentMain.car_num}</td>
			                    	<td style="text-align:center;">${driverPaymentMain.car_kind}</td>
			                    	<td style="text-align:center;">${driverPaymentMain.car_id_num}</td>
			                    	<td style="text-align:center;">
		                    			<form action="">																
		                                	<input type="text" placeholder="기사명" class="search-box" id="findDriver" name="findDriver" id="" style="margin-top:5px; height:26px;"> 		
		                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
		                                    <div class="search-result">
		                                    </div>
		                                </form>
		                    		</td>
			                    	
			                    <form id="excelForm" style="display:none" name="excelForm" method="post" enctype="multipart/form-data" autocomplete="false">
			                    	<input style="width:80%;" type="hidden" name="registerId" id="registerId" value="${userSessionMap.emp_id}" >
			                    	<input style="width:80%;" type="hidden" name="registerName" id="registerName" value="${userSessionMap.emp_name}" >
			                    	<input style="width:80%;" type="hidden" name="driverId" id="driverId" >
			                    	<input style="width:80%;" type="hidden" name="driverName" id="driverName" >
			                    	<input style="width:80%;" type="hidden" name="driverPaymentMainId" id="driverPaymentMainId" value="${paramMap.driverPaymentMainId}" >
			                    
			                    	
			                    	<td style="text-align:center;"><input style="width:80%;" type="text" autocomplete="off" class="datepick" placeholder="시작일" name="paymentStartDt" id="paymentStartDt" ></td>
			                    	<td style="text-align:center;"><input style="width:80%;" type="text" autocomplete="off" class="datepick" placeholder="종료일" name="paymentEndDt" id="paymentEndDt" ></td>
			                    	<td style="text-align:center;"><input style="width:80%;" type="text" placeholder="지입금액" name="driverPayment" id="driverPayment" onkeyup="javascript:getNumber(this);" ></td>
			                    	<!-- <td style="text-align:center;">
			                    		<select class="dropdown" name="assignDivision" id="assignDivision">
								        	<option value="" selected="selected">직영/지입 구분</option>
								            <option value="00">직영</option>
								            <option value="01">지입</option>
								            <option value="02">외부</option>
								        </select>
									</td> -->
			                    	<td style="text-align:center;">
			                    		<select class="dropdown" name="workKind" id="workKind">
								        	<option value="" selected="selected">업부구문</option>
								            <option value="I">내부업무</option>
								            <option value="O">외부업무</option>
								        </select>
			                    	</td>
			                    	<td style="text-align:center;">
			                    		<select class="dropdown" name="paymentYn" id="paymentYn">
								        	<option value="" selected="selected">납부여부</option>
								            <option value="N">미납</option>
								            <option value="Y">납부완료</option>
								        </select>
									</td>
			                    	<td style="text-align:center;"><input style="width:80%;" autocomplete="off" type="text" placeholder="비고" name="etc" id="etc" ></td>
			                    	<td style="text-align:center;">${user.emp_name}</td>
			                    	<td style="text-align:center;">
			                    		<a style="cursor:pointer; width:60px;" onclick="javascript:addDriverPayment(this)" class="btn-edit">등록</a>
			                    	</td>
		                    	</form>
	                    	</tr>
                     </tbody>
                </table>
                    
                    
                    <table class="article-table" style="margin-top:15px">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <!-- <td style="text-align:center;">기사아이디</td> -->
                            <!-- <td style="text-align:center; width:50px;"><input type="checkbox" id="checkAll" onclick="javascript:checkAll();" ></td> -->
                            <td style="text-align:center; width:120px;">차량번호</td>
                            <td style="text-align:center; width:120px;">차종</td>
                            <td style="text-align:center; width:120px;">차대번호</td>
                            <td style="text-align:center; width:120px;">기사명</td>
                            <td style="text-align:center; width:120px;">시작일</td>
                            <td style="text-align:center; width:120px;">종료일</td>
                            <td style="text-align:center; width:120px;">지입금액</td>
                            <!-- <td style="text-align:center; width:120px;">구분</td> -->
                            <td style="text-align:center; width:120px;">업무구분</td>
                            <td style="text-align:center; width:120px;">납부여부</td>
                            <td style="text-align:center; width:120px;">비고</td>
                            <td style="text-align:center; width:120px;">등록자</td>
                            <td style="text-align:center; width:150px;">관리</td>
                        </tr>
                    </thead>
                    <tbody id="">
                    	<c:forEach var="data" items="${listData}" varStatus="status">
							<tr class="ui-state-default"> 
	                            <td style="text-align:center; cursor:pointer;" >
	                            	${data.car_num}
	                            </td>
	                            <td style="text-align:center; cursor:pointer;" >
		                            ${data.car_kind }
	                            </td>
	                            <td style="text-align:center; cursor:pointer;" >
		                            ${data.car_id_num }
	                            </td>
	                            <td style="text-align:center; cursor:pointer;" >
		                           	${data.driver_name}
	                            </td>
	                            
	                            <td style="text-align:center; cursor:pointer;" >
	                            	<div class="normal">
		                            	${data.payment_start_dt }
	                            	</div>
	                            	<div class="mod" style="display:none;">
	                            		<input style=" width:80%;" class="datepick" type="text" placeholder="시작일" value="${data.payment_start_dt}" >
	                    			</div>
	                            </td>
	                            <td style="text-align:center; cursor:pointer;" >
	                            	<div class="normal">
		                            	${data.payment_end_dt }
	                            	</div>
	                            	<div class="mod" style="display:none;">
	                            		<input style=" width:80%;" class="datepick" type="text" placeholder="종료일" value="${data.payment_end_dt}" >
	                    			</div>
	                            </td>
	                            
	                            <td style="text-align:center; cursor:pointer;" >
	                            	<div class="normal">
		                            	${data.driver_payment }
	                            	</div>
	                            	<div class="mod" style="display:none;">
	                            		<input style=" width:80%;" type="text" placeholder="지입금액" value="${data.driver_payment}"  onkeyup="javascript:getNumber(this);">
	                    			</div>
	                            </td>
	                            
	                            <%-- <td style="text-align:center; cursor:pointer;" >
	                            	<div class="normal">
	                            		<c:if test="${data.assign_division == ''}"></c:if>
	                            		<c:if test="${data.assign_division == '01'}">직영</c:if>
	                            		<c:if test="${data.assign_division == '02'}">지입</c:if>
	                            		<c:if test="${data.assign_division == '03'}">외부</c:if>
	                            	
	                            	</div>
	                            	<div class="mod" style="display:none;">
	                            		<select class="dropdown" name="" id="">
								        	<option value="" <c:if test="${data.assign_division == ''}">selected="selected"</c:if>>직영/지입 구분</option>
								            <option value="00" <c:if test="${data.assign_division == '00'}">selected="selected"</c:if>>직영</option>
								            <option value="01" <c:if test="${data.assign_division == '01'}">selected="selected"</c:if>>지입</option>
								            <option value="02" <c:if test="${data.assign_division == '02'}">selected="selected"</c:if>>외부</option>
								        </select>
	                    			</div>
	                            </td> --%>
	                            <td style="text-align:center; cursor:pointer;" >
	                            	<div class="normal">
	                            		<c:if test="${data.work_kind == ''}"></c:if>
		                            	<c:if test="${data.work_kind == 'I'}">내부업무</c:if>
	                            		<c:if test="${data.work_kind == 'O'}">외부업무</c:if>
	                            	</div>
	                            	<div class="mod" style="display:none;">
	                            		<select class="dropdown" name="" id="">
								        	<option value="" <c:if test="${data.work_kind == ''}">selected="selected"</c:if>>업부구문</option>
								            <option value="I" <c:if test="${data.work_kind == 'I'}">selected="selected"</c:if>>내부업무</option>
								            <option value="O" <c:if test="${data.work_kind == 'O'}">selected="selected"</c:if>>외부업무</option>
								        </select>
	                    			</div>
	                            </td>
	                            <td style="text-align:center; cursor:pointer;">
	                            	<div class="normal">
	                            		<c:if test="${data.payment_yn == ''}"></c:if>
		                            	<c:if test="${data.payment_yn == 'N'}">미납</c:if>
	                            		<c:if test="${data.payment_yn == 'Y'}">납부완료</c:if>
	                            	</div>
	                            	<div class="mod" style="display:none;">
	                            		<select class="dropdown" name="" id="">
								        	<option value="" <c:if test="${data.payment_yn == ''}">selected="selected"</c:if>>납부여부</option>
								            <option value="N" <c:if test="${data.payment_yn == 'N'}">selected="selected"</c:if>>미납</option>
								            <option value="Y" <c:if test="${data.payment_yn == 'Y'}">selected="selected"</c:if>>납부완료</option>
								        </select>
	                    			</div>
	                            </td>
	                           
	                            <td style="text-align:center; cursor:pointer;">
	                            	<div class="normal">
		                            	${data.etc }
	                            	</div>
	                            	<div class="mod" style="display:none;">
	                            		<input style=" width:80%;" type="text" placeholder="비고" value="${data.etc}" >
	                    			</div>
	                            </td>
	                            
	                            
	                           
	                            <td style="text-align:center; cursor:pointer;">
	                            	<!-- <div class="normal"> -->
	                            		${data.register_name}
	                            	<%-- </div>
	                            	<div class="mod" style="display:none;">
	                            		<input type="text" placeholder="작성자" value="${data.register_name}" >
	                            	</div> --%>
	                            </td>
	                            <td style="text-align:center;">
	                            	<a style="cursor:pointer; width:50px;" onclick="javascript:editDriverPayment('${data.driver_payment_id}',this)" class="btn-edit">수정</a>
	                            	<a style="display:none; cursor:pointer; width:50px;" onclick="javascript:setDriverPayment('${data.driver_payment_id}',this)" class="btn-edit">완료</a>
	                            	<a style="cursor:pointer; width:50px;" onclick="javascript:deleteDriverPayment('${data.driver_payment_id}',this)" class="btn-edit">삭제</a>
	                            </td>
                        	</tr>
						</c:forEach>
					
                    </tbody>
                </table>
                    
                    
                    
                    
                    
                    	
                        
                        
                        
                        
                        
                        
                        
                        
                    </tbody>
                </table>
                <div class="table-pagination text-center">
                    <ul class="pagination">
                        <html:paging uri="/carmanagement/driverList.do" forGroup="&selectMonth=${paramMap.selectMonth}" />
                    </ul>
                </div>
                <div class="confirmation">
                    <div class="confirm">
                        <%-- <a href="/account/driverList_download.do?selectMonth=${paramMap.selectMonth}">엑셀다운로드</a> --%>
                    </div>
                    <%-- <div class="confirm">
                        <a href="/account/driverList_download.do?selectMonth=${paramMap.selectMonth}"></a>
                    </div> --%>
                </div>
                
            </section>
     
