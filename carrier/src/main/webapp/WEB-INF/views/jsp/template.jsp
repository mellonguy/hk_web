<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%

%>

            



        <script>
            var hallOfFame = $('.archive-page-halloffame-slider');
            hallOfFame.owlCarousel({
                loop:true,
                dots: true,
                items:5,
                responsive : {
                    1900: {
                        margin: 77
                    },
                    1800 : {
                        margin : 73
                    },
                    1700 : {
                        margin : 65
                    },
                    1600 : {
                        margin : 60
                    },
                    1500 : {
                        margin : 55
                    },
                    1366 : {
                        margin : 45,
                        items: 3
                    }
                }
            });
        </script>

