<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/js/bootstable.js"></script>
<script type="text/javascript"> 
var oldListOrder = "";
var allocationId = "";

$(document).ready(function(){
	//forOpen

	
	$('#dataTable tr:odd td').css("backgroundColor","#f9f9f9");
	
	$("#startDt").datepicker({
		dateFormat : "yy-mm-dd",
		onSelect : function(date){
			//$("#endDt").focus();
			setTimeout(function(){
				$("#endDt").focus();  
	        }, 50);
		}
	});
	
	$("#selectMonth").MonthPicker({ 

		Button: false
		,MonthFormat: 'yy-mm'	
		,OnAfterChooseMonth: function() { 
	        //alert($(this).val());
	        document.location.href = "/account/collectManage.do?&searchWord="+$(this).val()+"&companyType="+$("#companyType").val();
	        
	     }
	});
	
});






function updateAllocaion(row){
	var inputDt = "";
    var carrierType = "";
    var distanceType = "";
    var departureDt = "";
    var departureTime = "";
    var customerName = "";
    var carKind = "";
    var carIdNum = "";
    var carNum  = "";
    var departure = "";
    var arrival  = "";
    var driverName = "";
    var carCnt  = "";
    var amount   = "";
    var paymentKind = "";
	var allocId = $(row).attr("allocationId");
	if(confirm("수정 하시겠습니까?")){
		$(row).find("td").each(function(index,element){
			if(index==1){
				inputDt = $(this).html();
			}
			if(index==2){
				if($(this).html() == "셀프"){
					carrierType = "S";	
				}else if($(this).html() == "캐리어"){
					carrierType = "C";
				}else{
					alert("배차구분을 확인 하세요.");
					return false;
				}
			}
			if(index==3){
				if($(this).html() == "시내"){
					distanceType = "0";	
				}else if($(this).html() == "시외"){
					distanceType = "1";
				}else{
					alert("거리구분을 확인 하세요.");
					return false;
				}
			}
			if(index==4){
				departureDt = $(this).html();
			}
			if(index==5){
				departureTime = $(this).html();
			}
			if(index==6){
				customerName = $(this).html();
			}
			if(index==7){
				carKind = $(this).html();
			}
			if(index==8){
				carIdNum = $(this).html();
			}
			if(index==9){
				carNum = $(this).html();
			}
			if(index==10){
				departure = $(this).html();
			}
			if(index==11){
				arrival = $(this).html();
			}
			if(index==12){
				driverName = $(this).html();
			}
			if(index==13){
				carCnt = $(this).html();
			}
			if(index==14){
				amount = $(this).html();
			}
			if(index==15){
				paymentKind = $(this).html();
			}
      	});

		if(carrierType == "" || distanceType == ""){
			return false;	
		}
		
		 $.ajax({ 
				type: 'post' ,
				url : "/allocation/update-allocation.do" ,
				dataType : 'json' ,
				data : {
					inputDt : inputDt,
					carrierType : carrierType,
					distanceType : distanceType,
					departureDt : departureDt,
					departureTime : departureTime,
					customerName : customerName,
					carKind : carKind,
					carIdNum : carIdNum,
					carNum : carNum,
					departure : departure,
					arrival : arrival,
					driverName : driverName,
					carCnt : carCnt,
					amount : amount,
					paymentKind : paymentKind,
					allocationId : allocId
				},
				success : function(data, textStatus, jqXHR)
				{
					var result = data.resultCode;
					if(result == "0000"){
						alert("변경 되었습니다.");
			//			document.location.href = "/allocation/combination.do";
					}else if(result == "0001"){
   						alert("변경 하는데 실패 하였습니다.");
   					}
				} ,
				error : function(xhRequest, ErrorText, thrownError) {
				}
			}); 
		
	}

}

function getListId(obj){
	$('html').scrollTop(0);
	selectList($(obj).parent().parent().parent().attr("allocationId"));
	
}


function clipBoardCopy(allocationId,obj){
	  
	  $.ajax({ 
			type: 'post' ,
			url : "/allocation/selectAllocationInfo.do" ,
			dataType : 'json' ,
			data : {
				allocationId : allocationId
			},
			success : function(data, textStatus, jqXHR)
			{
							
				var result = data.resultCode;
				var resultMsg = data.resultMsg;
				var resultData = data.resultData;
				var resultDataSub = data.resultDataSub;
				if(result == "0000"){
					var t = document.createElement("textarea");
					t.setAttribute("id", "forClipboard");
					document.body.appendChild(t);		
					var clipBoardText = "";
					var departure_time = "";

					if(resultDataSub.departure_time != undefined){
						departure_time = resultDataSub.departure_time; 
					}
					
					clipBoardText += ""+resultDataSub.departure_dt_str+"("+resultDataSub.departure_dt_d_o_w+") "+departure_time+"      "+resultData.department+" "+resultData.customer_name+"\r\n";
					clipBoardText += ""+resultData.charge_name+" "+resultData.charge_phone+"\r\n";
					clipBoardText += ""+resultDataSub.car_kind+" "+resultDataSub.car_id_num+" "+resultDataSub.car_num+"\r\n";
					
					clipBoardText += "상차 "+resultDataSub.departure+" "+resultDataSub.departure_addr+"\r\n";
					if(resultDataSub.departure_person_in_charge != "" || resultDataSub.departure_phone != ""){
						clipBoardText += ""+resultDataSub.departure_person_in_charge+" "+resultDataSub.departure_phone+"\r\n";	
					}
					clipBoardText += "하차 "+resultDataSub.arrival+" "+resultDataSub.arrival_addr+"\r\n";
					if(resultDataSub.arrival_person_in_charge != "" || resultDataSub.arrival_phone != ""){
						clipBoardText += ""+resultDataSub.arrival_person_in_charge+" "+resultDataSub.arrival_phone+"\r\n";	
					}

					clipBoardText += ""+resultDataSub.etc
					t.value = clipBoardText;
					t.select();
					document.execCommand('copy');
					$(".clipboard").attr("data-clipboard-target","#forClipboard");
					
					document.body.removeChild(t);
					alert("배차 정보가 클립 보드에 복사 되었습니다.");
				}else{
					alert("배차 정보를 가져 오는데 실패 했습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
	  
}




var driverArray = new Array();
function selectList(paymentDt,paymentKind){
	
	
	document.location.href="/account/collectManageMiddle.do?paymentDt="+paymentDt+"&paymentKind="+paymentKind+"&companyType="+$("#companyType").val();
	
}	
	
function batchStatus(status){
	
	var id = "";
	var total = $('input:checkbox[name="forBatch"]:checked').length;
	
	if(total == 0){
		alert("목록이 선택 되지 않았습니다.");
		return false;
	}else{

		var inputString="";

		if(status == "X"){
			inputString = prompt('취소 사유를 작성 해 주세요.', '');

			if(inputString == null){
				return false;
			}else if( inputString == "" || inputString == undefined || ( inputString != null && typeof inputString == "object" && !Object.keys(inputString).length ) ){
				alert("취소 사유가 작성 되지 않았습니다.");
				return false;
			}else{	
				$('input:checkbox[name="forBatch"]:checked').each(function(index,element) {
				      if(this.checked){//checked 처리된 항목의 값
				    	  id+=$(this).attr("allocationId");
				    	  if(index<total-1){
				    		  id += ","; 
					         } 
				      }
				 });	
				updateAllocationStatus(status,id,inputString);
			}
			
		}else{
			$('input:checkbox[name="forBatch"]:checked').each(function(index,element) {
			      if(this.checked){//checked 처리된 항목의 값
			    	  id+=$(this).attr("allocationId");
			    	  if(index<total-1){
			    		  id += ","; 
				         } 
			      }
			 });	
			updateAllocationStatus(status,id,inputString);

		}

		
	}
}	


function updateAllocationStatus(status,id,addMsg){
	
	var msg = "";
	if(status == "C"){
		msg = "취소";
	}else if(status == "F"){
		msg = "완료";
	}else if(status == "U"){
		msg = "미확정으로 변경";
	}else if(status == "N"){
		msg = "미배차로 변경";
	}else if(status == "X"){
		msg = "취소 요청";
	}

	if(confirm(msg+"하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : "/allocation/updateAllocationStatus.do" ,
			dataType : 'json' ,
			data : {
				allocationStatus : status,
				allocationId : id,
				cancelReason : addMsg
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert(msg+"되었습니다.");
					var searchWord = encodeURI('${paramMap.searchWord}');
					document.location.href = "/allocation/combination.do?&cPage=${paramMap.cPage}&startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&allocationStatus=${paramMap.allocationStatus}&searchType=${paramMap.searchType}&searchWord="+searchWord;
				}else if(result == "0001"){
					alert(msg+"하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
}		
	
function checkAll(){

	if($('input:checkbox[id="checkAll"]').is(":checked")){
		$('input:checkbox[name="forBatch"]').each(function(index,element) {
			$(this).prop("checked","true");
		 });	
	}else{
		$('input:checkbox[name="forBatch"]').each(function(index,element) {
			$(this).prop("checked","");
		 });
	}
}
	
	
	
function move(location){
	
	
	window.location.href = "/allocation/"+location+".do";
	
}	
	
	
function excelDownload(carrierType){
	
	
	if(confirm("다운로드 하시겠습니까?")){
		document.location.href = "/allocation/excel_download.do?&carrierType="+carrierType+"&searchDateType="+$("#searchDateType").val()+"&startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val()+"&searchType="+$("#searchType").val()+"&searchWord="+encodeURI($("#searchWord").val());		
	}
	
	
}	
	
	

function goQuarterly(yearType,quarterType){
	
	var year = $("#forYear").val();
	
		  document.location.href="/account/collectManage.do?&yearType="+year+"&quarterType="+quarterType+"&companyType="+$("#companyType").val();   

	}


function sendSocketMessage(){
	
	$.ajax({ 
		type: 'post' ,
		url : "http://52.78.153.148:8080/allocation/sendSocketMessage.do" ,
		dataType : 'json' ,
		data : {
			allocationId : 'ALO3df74bd0105046bfbdce51ab0e64927c',
			driverId : "sourcream"
		},
		success : function(data, textStatus, jqXHR)
		{
			var result = data.resultCode;
			var resultData = data.resultData;
			if(result == "0000"){
				//alert("성공");
			}else if(result == "0001"){
			
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
}
	


var order = "${order}";
function sortby(gubun){

	if(order == ""){
		order = "asc";
	}else if(order == "asc"){
		order = "desc";
	}else if(order == "desc"){
		order = "";
	}
	
	var loc = document.location.href;
	var str = "";
	if(loc.indexOf("?") > -1){
		//forOrder 가 있는경우 ㅎㅎ
		if(loc.indexOf("forOrder") > -1){
			var queryString = loc.split("?");
			var query = queryString[1].split("&");
			
			for(var i = 0; i < query.length; i++){
				if(query[i].indexOf("forOrder") > -1){
					query[i] = "forOrder="+gubun+"%5E"+order;
				}
			}
			for(var i = 0; i < query.length; i++){
				if(query[i] != ""){
					str += "&"+query[i];	
				}
			}
			document.location.href = queryString[0]+"?"+str;
		}else{
			str="&forOrder="+gubun+"%5E"+order;
			document.location.href = loc+str;
		}
		
	}else{
		str="?&forOrder="+gubun+"%5E"+order;
		document.location.href = loc+str;
	}
	
}



function pickUpValidation(status){


	var allocationIdArr = new Array();
	
	var total = $('input:checkbox[name="forBatch"]:checked').length;
	var validationStatus = true;

	if(total == 0){
		alert("목록이 선택 되지 않았습니다.");
		return false;
	}else if(total == 1){
		alert("1개 이상의 배차건이 선택 되어야 합니다.");
		return false;
	}else if(total > 3){
		alert("4개 이상의 배차건에 대한 픽업은 관리자에게 문의 하세요.");
		return false;
	}
	
	if(status == "P"){

		$('input:checkbox[name="forBatch"]:checked').each(function(index,element) {

			if($(this).attr("batchStatus") != "N"){
				alert((index+1)+"번째 선택된 배차건은 픽업 또는 일괄배차로 이미 등록 되어 있습니다.")
				validationStatus = false;
				return false;
			}else{
				allocationIdArr.push($(this).attr("allocationId"))
			}
		});

	}else{

		$('input:checkbox[name="forBatch"]:checked').each(function(index,element) {

			if($(this).attr("batchStatus") == "N"){
				alert((index+1)+"번째 선택된 배차건은 이미 개별로 등록 되어 있습니다.");
				validationStatus = false;
				return false;
			}else{
				allocationIdArr.push($(this).attr("allocationId"))
				
			}

		});

	}

	if(validationStatus){
		setPickup(allocationIdArr,status);
	}
	
}











function setPickup(allocationIdArr,status){


	if(confirm("해당 배차를 픽업으로 설정 하시겠습니까?")){

		$.ajax({ 
			type: 'post' ,
			url : "/allocation/setPickup.do" ,
			dataType : 'json' ,
			traditional : true,
			data : {
				allocationIdArr : allocationIdArr,
				batchStatus : status
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				if(result == "0000"){
					alert("픽업으로 설정 되었습니다.");
					window.location.reload();
				}else if(result == "0001"){
					alert("픽업으로 설정 하는데 실패 하였습니다. 관리자에게 문의 하세요.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});

	}else{

		alert("취소 되었습니다.");
		return false;
	}

	
}

	
 </script>




        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">HOME</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">수금 관리</a></li>
                </ul>
            </div>

            <div class="up-dl clearfix">
                
                <form name="searchForm" method="get" action="/account/collectManage.do">
              
                            
                           <div class="select-con">
                        		 &nbsp;소속 선택 :&nbsp;
							        <select class="dropdown" name="companyType" id="companyType">
							        	<option value="" selected="selected">전체</option>
							            <option value="company1" <c:if test='${paramMap.companyType eq "company1"}'> selected="selected"</c:if>>한국캐리어</option>
							            <option value="company2" <c:if test='${paramMap.companyType eq "company2" }'> selected="selected"</c:if>>한국카캐리어(주)</option>
							        </select>
							    </div>	
							    
							    
                       <br>
                 
                	<div class="date-picker">
	                	입금일 : <input style="width:150px;" autocomplete="off" id="startDt" placeholder="조회 시작일" name="startDt" type="text" class="datepick" 
	                	
	                	value="<c:if test="${paramMap.searchWord == '' }">${paramMap.startDt}</c:if>"> ~ 
	                	<input style="width:150px;" autocomplete="off" id="endDt" placeholder="조회 종료일" name="endDt"  type="text" class="datepick" value="<c:if test="${paramMap.searchWord == ''}">${paramMap.endDt}</c:if>">
	             
	                </div>
	                
	      
	                <div class="upload-btn" style="float:right;">
	                    
	                </div>
				</form>
					<div class="btns-submit">	
						<div class="right">
							<%-- <input type="text" id="searchWord" name="searchWord" value="${paramMap.searchWord}" placeholder="검색어 입력"  onkeypress="if(event.keyCode=='13'){encodeURI($(this).val()); document.searchForm.submit();}" /> --%>
							<a class="btn-gray" href="javascript:" onclick=" encodeURI($(this).prev().val()); document.searchForm.submit()"><input type="button" value="검색"></a>
							    <%--   &nbsp;
							      &nbsp;
							      &nbsp;
							      월별 조회  :
							                  <input style="width:150px; text-align:center;" id="selectMonth" name="selectMonth"  type="text" placeholder="조회월" readonly="readonly" value="<c:if test="${paramMap.searchType != null && paramMap.searchType == 'month'}">${paramMap.searchWord}</c:if>">
	
	
						 --%>
						 
 <div class="up-dl clearfix header-search" style="margin-left :850px;">
      <table>
         <tbody>
            <tr>
               <td>월별  :</td>
               <td class="widthAuto" style="width: 200px;">
                  <input style="width:100%; text-align:center;" id="selectMonth" name="selectMonth"  type="text" placeholder="월별 입금일" readonly="readonly" value="<c:if test="${paramMap.searchWord != null}">${paramMap.searchWord}</c:if>">
               </td>
            </tr>
         </tbody>
      </table>
   </div>
	<div class="up-dl clearfix header-search" style="margin-left : 1100px;">
      <table>
         <tbody>
            <tr>
               <td>시즌 별 :
               
               					<%-- <select name ="forYear" id ="forYear">
								
									<option value="${paramMap.yearType[0].yearType}">${paramMap.yearType[0].yearType}</option>
									<option value="${paramMap.yearType[1].yearType}">${paramMap.yearType[1].yearType}</option>
													
								</select> --%>
								 
								<select name ="forYear" id ="forYear">
									<option value="" selected>년도</option>
									<c:forEach var="data" items="${yearList}" varStatus="status">
										<option value="${data.yearType}" <c:if test='${paramMap.yearType eq data.yearType }'>selected</c:if>> ${data.yearType}</option>
									</c:forEach>
								</select> 
				
						<%--  <input type ="text"  style="width:40px; text-align:center;" id="yearType" name ="yearType" value="${paramMap.yearType}">년 --%>
           		    <td class="widthAuto" style="width: 70px;">
              <%--     <input style="width:100%; text-align:center;" id="quarterly" name="quarterly"  type="text" placeholder="조회분기" readonly="readonly" value="${paramMap.searchWord}"> --%>
           							
           					<select name="quarterly"  id="quarterly">
 								
									<option value=" " <c:if test='${paramMap.quarterType eq "" }'>selected</c:if> >선택</option>
									<option value="1" <c:if test='${paramMap.quarterType eq "1"}'> selected</c:if> >1분기</option>
									<option value="2" <c:if test='${paramMap.quarterType eq "2"}'> selected</c:if> >2분기</option>
									<option value="3" <c:if test='${paramMap.quarterType eq "3"}'> selected</c:if> >3분기</option>
									<option value="4" <c:if test='${paramMap.quarterType eq "4"}'> selected</c:if> >4분기</option>
									<option value="5" <c:if test='${paramMap.quarterType eq "5"}'> selected</c:if> >상반기</option>
									<option value="6" <c:if test='${paramMap.quarterType eq "6"}'> selected</c:if> >하반기</option>
									
											
								</select>
               </td>	 
               		<td>
                      	<input type="button" value="검색"  onclick="javascript:goQuarterly( $('#forYear').val(),$('#quarterly').val() )"> 
                     	<%-- <input type="button" value="검색"  onclick="javascript:goQuarterly('yearType[i]','${paramMap.numberType}')"> --%>
 						                        
               </td>

            </tr>
         </tbody>
      </table>
   </div>
   
						
						</div>
					</div>		
                
                
            </div>

            <div class="dispatch-btn-container">
				<div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">수금 관리</div>
            </div>
        </section>

        <div class="dispatch-wrapper">
            
            <section class="dispatch-bottom-content">
			</section>
			<div id="bottom-table">
	            <section  class="bottom-table" style="width:1600px; margin-left:10px;">
	            	
	                <table class="article-table forToggle" id="dataTable" style="width:100%;">
	                    <colgroup>
	                       <col width="100px;">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                    </colgroup>
	                    <thead>
	                        <tr>
	                            <td style="text-align:center;">입금일 </td>
	                            <td style="text-align:center;">계좌이체</td>
	                            <td style="text-align:center;">현금</td>
	                            <td style="text-align:center;">카드</td>
	                            <td style="text-align:center;">현장수금</td>
	                            <td style="text-align:center;">합계</td>
	                        </tr>
	                    </thead>
	                    <tbody id="rowData">
	                    	<c:set var="atSum" value="0" />
	                    	<c:set var="caSum" value="0" />
	                    	<c:set var="cdSum" value="0" />
	                    	<c:set var="ddSum" value="0" />
                        	<c:set var="totalSum" value="0" />
	                    	<c:forEach var="data" items="${listData}" varStatus="status">
								<tr class="ui-state-default">
		                            <td style="text-align:right;">${data.payment_dt}</td>
									<td style="text-align:right; cursor:pointer;" onclick="javascript:selectList('${data.payment_dt}','AT');">${data.at_sum}</td>
									<td style="text-align:right; cursor:pointer;" onclick="javascript:selectList('${data.payment_dt}','CA');">${data.ca_sum}</td>
		                            <td style="text-align:right; cursor:pointer;" onclick="javascript:selectList('${data.payment_dt}','CD');">${data.cd_sum}</td>
		                            <td style="text-align:right; cursor:pointer;" onclick="javascript:selectList('${data.payment_dt}','DD');">${data.dd_sum}</td>
		                            <td style="text-align:right;">${data.total_sum}</td>
	                        	</tr>
	                        	<c:set var="atSum" value="${fn:replace(atSum,',','')+fn:replace(data.at_sum, ',','')}" />
	                        	<c:set var="caSum" value="${fn:replace(caSum,',','')+fn:replace(data.ca_sum, ',','')}" />
	                        	<c:set var="cdSum" value="${fn:replace(cdSum,',','')+fn:replace(data.cd_sum, ',','')}" />
	                        	<c:set var="ddSum" value="${fn:replace(ddSum,',','')+fn:replace(data.dd_sum, ',','')}" />
	                        	<c:set var="totalSum" value="${fn:replace(totalSum,',','')+fn:replace(data.total_sum, ',','')}" />
							</c:forEach>
	                    	<tr class="ui-state-default">
		                    	<td style="text-align:right; background:#DEDEDE; font-weight:bold;">[&nbsp;&nbsp;&nbsp;합&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;계&nbsp;&nbsp;&nbsp;]</td>
								<td style="text-align:right; background:#DEDEDE; font-weight:bold;"><fmt:formatNumber value="${atSum}" groupingUsed="true"/></td>
								<td style="text-align:right; background:#DEDEDE; font-weight:bold;"><fmt:formatNumber value="${caSum}" groupingUsed="true"/></td>
		                    	<td style="text-align:right; background:#DEDEDE; font-weight:bold;"><fmt:formatNumber value="${cdSum}" groupingUsed="true"/></td>
		                    	<td style="text-align:right; background:#DEDEDE; font-weight:bold;"><fmt:formatNumber value="${ddSum}" groupingUsed="true"/></td>
		                    	<td style="text-align:right; background:#DEDEDE; font-weight:bold;"><fmt:formatNumber value="${totalSum}" groupingUsed="true"/></td>
	                        </tr>
	
	                    </tbody>
	                </table>
	
	
	                <%-- <div class="table-pagination text-center">
	                    <ul class="pagination">
	                    	<html:paging uri="/allocation/combination.do" forGroup="&startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&searchDateType=${paramMap.searchDateType}&forOrder=${paramMap.forOrder}%5E${paramMap.order}&allocationStatus=${paramMap.allocationStatus}" frontYn="N" />
	                    </ul>
	                </div> --%>                
	                
	            </section>
            
            	<!-- <div class="confirmation">
                    <div class="confirm">
                        <a style="cursor:pointer;" onclick="javascript:pickUpValidation('P');">픽업설정</a>
                    </div>
                    <div class="cancel">
	                	<a style="cursor:pointer;" onclick="javascript:batchStatus('X');">취소요청</a>
	                </div>
                </div> -->
            </div>
        </div>        
        <!-- <iframe style="width: 980px; height:10000px; border: none;" frameBorder="0" id="happyboxFrame" scrolling="no" src="https://www.happyalliance-happybox.org/Bridge?v=param"></iframe>​    -->
       <script>
		if("${userMap.control_grade}" == "01"){
			$('#dataTable').SetEditable({
            	columnsEd: "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15",
            	 onEdit: function(row) {updateAllocaion(row)},  
            	 onDelete: function() {},  
            	 onBeforeDelete: function() {}, 
            	 onAdd: function() {}  
            });
			
		}          
            </script>

