<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>
<%

%>

<script type="text/javascript">
$(document).ready(function(){
	
	
});

function editEmp(empId){
	
	document.location.href = "/baseinfo/edit-employee.do?empId="+empId;
}

function addEmp(){
	document.location.href = "/baseinfo/add-employee.do";
}



function go_BillRequest(){

	//document.location.href='/account/billRequest.do?&companyType='+$("#companyType").val();
	document.location.href='/account/billRequest.do?';
}

function go_BillRequest_list(){

	document.location.href="/account/billRequestList.do";
	
}



function searchStatusChange(searchStatus,obj){
	
	
	if($("#startDt").val() != ""){
		if($("#endDt").val() == ""){
			alert("조회 종료일이 지정되지 않았습니다.");
			return false;
		}
	}
	
	if($("#endDt").val() != ""){
		if($("#startDt").val() == ""){
			alert("조회 시작일이 지정되지 않았습니다.");
			return false;
		}
	}
	
	document.location.href = "/humanResource/employee.do?searchStatus="+searchStatus+"&startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val();
	
	
	
	/* $('input[name="searchStatus"]').each(function(index,element) {
		$(this).removeClass("btn-primary");
		$(this).addClass("btn-normal");
	 });
	$(obj).removeClass("btn-normal");
	$(obj).addClass("btn-primary"); */

//	
	
	/* $.ajax({ 
		type: 'post' ,
		url : "/humanResource/getEmpData.do" ,
		dataType : 'json' ,
		data : {
			searchStatus : searchStatus,
			startDt : $("#startDt").val(),
			endDt : $("#endDt").val()
		},
		success : function(data, textStatus, jqXHR)
		{
			var resultCode = data.resultCode;
			var resultData = data.resultData;
			var resultDataSub = data.resultDataSub;
			$("#dataArea").html("");
			if(resultCode == "0000"){
				var result = "";
				 for(var i = 0; i < resultData.length; i++){
					 result += '<tr class="ui-state-default">';
					 result += '<td style="text-align:center;">'+resultData[i].emp_id+'</td>';
					 result += '<td style="text-align:center;">'+resultData[i].emp_name+'</td>';
					 result += '<td style="text-align:center;">';
					 if(resultData[i].phone_work != null && resultData[i].phone_work != ""){
						 result += '<div style="display:block;">업무용 : '+resultData[i].phone_work+'</div>';	 
					 }
					 if(resultData[i].phone_personal != null && resultData[i].phone_personal != ""){
						 result += '<div style="display:block; margin-top:5px;">개인 : '+resultData[i].phone_personal+'</div>';	 
					 }
					 result += '</td>';
					 result += '<td style="text-align:center;">'+resultData[i].join_dt+'</td>';
					 result += '<td style="text-align:center;">'+resultData[i].resign_dt+'</td>';
					 result += '<td style="text-align:center;">'+resultData[i].emp_position+'</td>';
					 result += '<td style="text-align:center;">'+resultData[i].email_work+' </td>';
					 result += '</tr>';
					  
				 }
				 $("#dataArea").html(result);
				 $("#startDt").val(resultDataSub.startDt);
				 $("#endDt").val(resultDataSub.endDt);
			}else if(resultCode == "0002"){
				alert("");
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	}); */	
	
	
	

	
}


function inputDate(){
	
	$.MessageBox({
			  input    : true,
			  buttonDone: "확인",
			  message  : "계산서 발행일자를 입력 해 주세요(ex:20190401)"
			}).done(function(data){
			  if ($.trim(data)) {
				 //전화번호만 입력
				phoneNumber = data;
			  } else {
				$.alert("인수증 전송이 취소 되었습니다. \r\n 메일 또는 전화번호 하나는 반드시 입력 되어야 합니다.",function(a){
             	});   
			  }
			});
	
	
}


function getNumberOnly (str) {			//저장할때는 콤마 제거 후 저장한다.
    var len = str.length;
    var sReturn = "";

    for (var i=0; i<len; i++){
        if ( (str.charAt(i) >= "0") && (str.charAt(i) <= "9") ){
            sReturn += str.charAt(i);
        }
    }
    return sReturn;
}


function billPublishRequest(){
	
	var total = $('input:checkbox[name="forDecideStatus"]:checked').length;
	var customerId = "";
	
		if(total == 0){
			alert("거래처가 선택 되지 않았습니다.");
			return false;
		}else{
			var billPublishInfoList = new Array();
			var requestDtChk = true;
			var summaryChk = true;
			$('input:checkbox[name="forDecideStatus"]:checked').each(function(index,element) {
					if(this.checked){
						var billPublishInfo= new Object();
						billPublishInfo.index = index;	
						billPublishInfo.customerId=$(this).attr("customerId");
						billPublishInfo.customerName=$(this).parent().next().html();
						billPublishInfo.amount=Number(getNumberOnly($(this).parent().next().next().next().html()));
						billPublishInfo.requestDt=$(this).parent().next().next().next().next().next().find("input").val();
						billPublishInfo.summary=$(this).parent().next().next().next().next().next().next().find("input").val();
						billPublishInfo.comment=$(this).parent().next().next().next().next().next().next().next().find("input").val();
						if($(this).parent().next().next().next().next().next().find("input").val() == ""){
							requestDtChk = false;
						}
						if($(this).parent().next().next().next().next().next().next().find("input").val() == ""){
							summaryChk = false;
						}
						billPublishInfoList.push(billPublishInfo);
					}
			 });

			if(!requestDtChk){
				alert("세금계산서 발행일이 입력 되지 않았습니다.");
				return false;
			}else if(!summaryChk){
				alert("적요가 입력 되지 않았습니다. 적요는 필수 입력 사항 입니다.");
				return false;
			}else{
				if(confirm("선택된 "+total+"건에 대하여 세금계산서 발행 요청을 하시겠습니까? 세금계산서 미발행건은 포함되지 않습니다.")){
					//alert(JSON.stringify({billPublishInfoList : billPublishInfoList}));
					$.ajax({ 
						type: 'post' ,
						url : "/account/billPublishRequest.do" ,
						dataType : 'json' ,
						data : {
							billPublishInfoList : JSON.stringify({billPublishInfoList : billPublishInfoList}),
							includeZero : "${paramMap.includeZero}",
							searchDateType : "${paramMap.searchDateType}",
							searchType : "${paramMap.searchType}",
							searchWord : "${paramMap.searchWord}",
							carrierType : "${paramMap.carrierType}",
							decideStatus : "${paramMap.decideStatus}",
							startDt : "${paramMap.startDt}",
							endDt : "${paramMap.endDt}"
							
						},
						success : function(data, textStatus, jqXHR)
						{
							var resultCode = data.resultCode;
							var resultData = data.resultData;
							
							if(resultCode == "0000"){
								alert("계산서 발행 요청에 성공 했습니다.");
								document.location.reload();
							}else if(resultCode == "0002"){
								alert(resultData+"거래처의 계산서 발행 요청에 실패 했습니다.");
								document.location.reload();
							}else if(resultCode == "E001"){
								alert("증빙구분이 입력되지 않았습니다.");
								document.location.reload();
							}
						} ,
						error : function(xhRequest, ErrorText, thrownError) {
							ErrorText
						}
					});
						
				}
					
			}
			
		}
	
	
}

function checkAll(){

	if($('input:checkbox[id="checkAll"]').is(":checked")){
		$('input:checkbox[name="forDecideStatus"]').each(function(index,element) {
			$(this).prop("checked","true");
		 });	
	}else{
		$('input:checkbox[name="forDecideStatus"]').each(function(index,element) {
			$(this).prop("checked","");
		 });
	}
}


function excelDownload(){
	
	
	var total = $('input:checkbox[name="forDecideStatus"]:checked').length;
	var customerId = "";
	
		if(total == 0){
			alert("거래처가 선택 되지 않았습니다.");
			return false;
		}else if(total > 1){
			alert("한개의 거래처만 선택 할 수 있습니다.");
			return false;
		}else{
			$('input:checkbox[name="forDecideStatus"]:checked').each(function(index,element) {
			      if(this.checked){//checked 처리된 항목의 값
			    	  customerId+=$(this).attr("customerId");
			      }
			 });

			var includeZero = "";
			
			if($("#includeZero").is(":checked")){
				includeZero = "Y";
			}else{
				includeZero = "N";
			}
			document.location.href = "/account/excel_download.do?&decideStatus=${paramMap.decideStatus}&customerId="+customerId+"&includeZero="+includeZero+"&searchDateType="+$("#searchDateType").val()+"&searchType="+$("#searchType").val()+"&searchWord="+encodeURI($("#searchWord").val())+"&carrierType="+$("#carrierType").val()+"&startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val();

		}
	
	
}

function sendMail(){
	
	
	var total = $('input:checkbox[name="forDecideStatus"]:checked').length;
	var customerId = "";
	
		if(total == 0){
			alert("거래처가 선택 되지 않았습니다.");
			return false;
		}else{
			
			if(confirm("선택한 거래처로 메일을 보내시겠습니까?")){

				if(confirm("확인을 누르면 메일을 보냅니다. 정말 메일을 보내시겠습니까?")){
				
				$('input:checkbox[name="forDecideStatus"]:checked').each(function(index,element) {
				      if(this.checked){//checked 처리된 항목의 값
				    	  customerId+=$(this).attr("customerId");
				      }
				      if(index<total-1){
				    	  customerId += ","; 
				      } 
				 });
				var includeZero = "";
				if($("#includeZero").is(":checked")){
					includeZero = "Y";
				}else{
					includeZero = "N";
				}
				$.ajax({ 
					type: 'post' ,
					url : "/account/sendMail.do" ,
					dataType : 'json' ,
					data : {
						decideStatus : "${paramMap.decideStatus}",
						customerId : customerId,
						includeZero : includeZero,
						searchDateType : $("#searchDateType").val(),
						searchType : $("#searchType").val(),
						searchWord : encodeURI($("#searchWord").val()),
						carrierType : $("#carrierType").val(),
						startDt : $("#startDt").val(),
						endDt : $("#endDt").val()
					},
					success : function(data, textStatus, jqXHR)
					{
						var resultCode = data.resultCode;
						var resultData = data.resultData;
						
						if(resultCode == "0000"){
							alert("메일을 전송 하였습니다.");
						}else if(resultCode == "0002"){
							/* if(confirm("거래처의 세금계산서 이메일 주소가 저장되어 있지 않습니다. 세금계산서 이메일을 입력 하시겠습니까?")){
								document.location.href = "/baseinfo/edit-customer.do?customerId="+resultData;
							}else{
								return false;	
							} */
							alert(resultData);
							
						}
					} ,
					error : function(xhRequest, ErrorText, thrownError) {
					}
				});


				}


				
			}		
			
		
	
			
			
			
			
		}
	
	
}





function search(){//searchDateType
	
	
	var includeZero = "";
	
	if($("#includeZero").is(":checked")){
		includeZero = "Y";
	}else{
		includeZero = "N";
	}
	document.location.href = "/account/customerCal.do?&decideStatus=${paramMap.decideStatus}&includeZero="+includeZero+"&searchDateType="+$("#searchDateType").val()+"&searchType="+$("#searchType").val()+"&searchWord="+encodeURI($("#searchWord").val())+"&carrierType="+$("#carrierType").val()+"&startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val();
	
}


function viewTotalSales(customerId){
	document.location.href = "/account/customerCalDetail.do?&decideStatus=${paramMap.decideStatus}&searchDateType="+$("#searchDateType").val()+"&customerId="+customerId+"&carrierType=${paramMap.carrierType}&startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val();
}


function decideStatus(status){
	
	document.location.href = "/account/customerCal.do?&includeZero=${paramMap.includeZero}&searchDateType="+$("#searchDateType").val()+"&searchType="+$("#searchType").val()+"&searchWord="+encodeURI($("#searchWord").val())+"&carrierType="+$("#carrierType").val()+"&startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val()+"&decideStatus="+status;
}

function goSearch(obj){
	
	var includeZero = "";
	
	if($("#includeZero").is(":checked")){
		includeZero = "Y";
	}else{
		includeZero = "N";
	}
	document.location.href = "/account/customerCal.do?&decideStatus=${paramMap.decideStatus}&includeZero="+includeZero+"&searchDateType="+$("#searchDateType").val()+"&searchType="+$("#searchType").val()+"&searchWord="+encodeURI($("#searchWord").val())+"&carrierType="+$(obj).val()+"&startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val();
	
//	alert($(obj).val());
	
	
}


function customerCheck(status){
	
	var msg = "확정";
	if(status == "N"){
		msg = "수정";
	}
	if(confirm("선택하신 거래처의 매출액을 일괄 "+msg+" 하시겠습니까?")){
		
		var customerId = "";
		var total = $('input:checkbox[name="forDecideStatus"]:checked').length;
		
		//if('${paramMap.carrierType}' != 'A'){
			if(total == 0){
				alert("일괄 "+msg+"할 거래처가 선택 되지 않았습니다.");
				return false;
			}else{
				$('input:checkbox[name="forDecideStatus"]:checked').each(function(index,element) {
				      if(this.checked){//checked 처리된 항목의 값
				    	  customerId+=$(this).attr("customerId");
				    	  if(index<total-1){
				    		  customerId += ","; 
					         } 
				      }
				 });	
			
			}	
		//}else{
		//	alert("");
		//	return false;
	//	}
	
			updateDecideStatus('${paramMap.carrierType}','${paramMap.searchDateType}','${paramMap.startDt}','${paramMap.endDt}','${paramMap.searchType}','${paramMap.searchWord}',customerId,status);
			
	}
	
	
	
}



function updateDecideStatus(carrierType,searchDateType,startDt,endDt,searchType,searchWord,customerId,status){
	
		
	var msg = "확정";
	if(status == "N"){
		msg = "수정";
	}
	
	$.ajax({ 
		type: 'post' ,
		url : "/account/updateDecideStatus.do" ,
		dataType : 'json' ,
		data : {
			customerId : customerId,
			carrierType : carrierType,
			searchDateType : searchDateType,
			startDt : startDt,
			endDt : endDt,
			searchType : searchType,
			searchWord : encodeURI(searchWord),
			currentDecideStatus : "${paramMap.decideStatus}",
			setDecideStatus : status
		},
		success : function(data, textStatus, jqXHR)
		{
			var result = data.resultCode;
			var resultData = data.resultData;
			if(result == "0000"){
				alert(msg+"되었습니다.");
				var searchWord = encodeURI('${paramMap.searchWord}');
				document.location.href = "/account/customerCal.do?&decideStatus=${paramMap.decideStatus}&includeZero=${paramMap.includeZero}&cPage=${paramMap.cPage}&searchDateType=${paramMap.searchDateType}&carrierType=${paramMap.carrierType}&startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&searchType=${paramMap.searchType}&searchWord="+searchWord;
			}else if(result == "0001"){
				alert(msg+"하는데 실패 하였습니다.");
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
	

	
}




var order = "${paramMap.order}";
var forOrder = "${paramMap.forOrder}";
function sortBy(gubun){

	
	//alert(order);
	//alert(forOrder);

	//alert(gubun);
	
	if(gubun == "A" && (order == "asc" && forOrder == "regDt")){
		//alert(1);
		gubun = "A";
		order = "asc";
	}else if(gubun == "A" && (order == "asc" && forOrder == "A")){
		//alert(2);
		gubun = "A";
		order = "desc";
	}else if(gubun == "A" && (order == "desc" && forOrder == "A")){
		//alert(3);
		gubun = "regDt";
		order = "asc";
	}else if(gubun == "B" && (order == "asc" && forOrder == "regDt")){
		//alert(4);
		gubun = "B";
		order = "asc";
	}else if(gubun == "B" && (order == "asc" && forOrder == "B")){
		//alert(5);
		gubun = "B";
		order = "desc";
	}else if(gubun == "B" && (order == "desc" && forOrder == "B")){
		//alert(6);
		gubun = "regDt";
		order = "asc";
	}else if(gubun == "C" && (order == "asc" && forOrder == "regDt")){
		gubun = "C";
		order = "asc";
	}else if(gubun == "C" && (order == "asc" && forOrder == "C")){
		gubun = "C";
		order = "desc";
	}else if(gubun == "C" && (order == "desc" && forOrder == "C")){
		gubun = "regDt";
		order = "asc";
	}else if(gubun == "D" && (order == "asc" && forOrder == "regDt")){
		gubun = "D";
		order = "asc";
	}else if(gubun == "D" && (order == "asc" && forOrder == "D")){
		gubun = "D";
		order = "desc";
	}else if(gubun == "D" && (order == "desc" && forOrder == "D")){
		gubun = "regDt";
		order = "asc";
	}
	
	
	
/* 	if(order == "" || (forOrder == "A" && order == "desc")){
		gubun = "A"
		order = "asc";
	}else if(forOrder == "A" && order == "asc"){
		gubun = "A"
			order = "desc";
		} */
	
	
	
	
	
	
	/* if((forOrder == "A" || forOrder == "B") && order == "desc"){
		gubun = "regDt"
		order = "asc";
	}else{
		if(order == "" || order == "desc"){
			order = "asc";
		}else{
			order = "desc";
		}
	} */
			
	/* if(order == "" || order == "desc"){
		order = "asc";
	}else{
		order = "desc";
	} */
	
	var loc = document.location.href;
	var str = "";
	if(loc.indexOf("?") > -1){
		//forOrder 가 있는경우 ㅎㅎ
		if(loc.indexOf("forOrder") > -1){
			var queryString = loc.split("?");
			var query = queryString[1].split("&");
			
			for(var i = 0; i < query.length; i++){
				if(query[i].indexOf("forOrder") > -1){
					query[i] = "forOrder="+gubun+"%5E"+order;
				}
			}
			for(var i = 0; i < query.length; i++){
				if(query[i] != ""){
					str += "&"+query[i];	
				}
			}
			document.location.href = queryString[0]+"?"+str;
		}else{
			str="&forOrder="+gubun+"%5E"+order;
			document.location.href = loc+str;
		}
		
	}else{
		str="?&forOrder="+gubun+"%5E"+order;
		document.location.href = loc+str;
	}
	
}

function fileUpload(fis){

	if(confirm("업체 청구액을 일괄 수정 하시겠습니까?")){
		$("#excelForm").attr("action","/allocation/excelUploadForAmountMod.do");
		$("#excelForm").submit();
			
	}


	
}



function getCheckBoxCnt(){ //체크박스 개수 구하기


	var checkboxCnt =$('input:checkbox[name="forDecideStatus"]:checked').length;
	var checkbox = false;	
	//var test2 = $("#checkedChk_box").get(0);
	for(var i=0; i<checkboxCnt; i++){
		$("#checkedChk_box").text(checkboxCnt);			
	}

	
}
</script>
		<!-- <section class="side-nav"> -->
            <!-- <ul>
                <li class="active"><a href="/baseinfo/employee.do">직원관리</a></li>
                <li class=""><a href="/baseinfo/driver.do">기사관리</a></li>
                <li class=""><a href="/baseinfo/customer.do">거래처관리</a></li>
                <li class=""><a href="/baseinfo/company.do">회사정보관리</a></li>
            </ul> -->
        <!-- </section> -->

        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">운송비 정산</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">매출처 정산</a></li>
                </ul>
            </div>
            
            <div class="up-dl clearfix header-search">
            
                 <table>
                 
                    <tbody>
                    
                        <tr>
                      
                          <%--       <td style="width: 149px;">
                                <div class="select-con">
							        <select class="dropdown" name="companyType" id="companyType">
							        	<option value="" selected="selected">소속선택(전체)</option>
							            <option value="company1" <c:if test='${paramMap.companyType eq "company1"}'> selected="selected"</c:if>>한국캐리어</option>
							            <option value="company2" <c:if test='${paramMap.companyType eq "company2" }'> selected="selected"</c:if>>한국카캐리어(주)</option>
							        </select>
							    </div>
                            </td> --%>
                       <!--      <td>
                                <input type="button" id="btn-company" value="검색" onclick="javascript:companySearch($('#companyType').val());">
                            </td> -->
                        	<!-- <td>조회기간 :</td> -->
				            <td class="widthAuto" style="width:500px;">
				            	<select name="searchDateType"  id="searchDateType">
									<option value="" <c:if test='${paramMap.searchDateType eq "" }'> selected="selected"</c:if> >선택</option>
									<option value="S" <c:if test='${paramMap.searchDateType eq "S" }'> selected="selected"</c:if> >의뢰일</option>
									<option value="D" <c:if test='${paramMap.searchDateType eq "D" }'> selected="selected"<</c:if> >출발일</option>
								</select>
				                <input style="width:40%; text-align:center;" class="datepick" type="text" placeholder="조회시작일" onclick="javascript:$(this).val('');"  readonly="readonly" name="startDt" id="startDt" value="${paramMap.startDt}">&nbsp;~&nbsp;
				                <input style="width:40%; text-align:center;" class="datepick" type="text" placeholder="조회종료일" onclick="javascript:$(this).val('');" readonly="readonly" name="endDt" id="endDt"  value="${paramMap.endDt}">
				            </td>
				            <td style="width: 145px;">
                                <div class="select-con">
							        <select class="dropdown" name="searchType" id="searchType">
							        	<option value="" <c:if test='${paramMap.searchType eq "" }'> selected="selected"</c:if>>조회구분 선택</option>
							            <option value="00" <c:if test='${paramMap.searchType eq "00" }'> selected="selected"</c:if>>업체명</option>
							            <option value="01" <c:if test='${paramMap.searchType eq "01" }'> selected="selected"</c:if>>대표자명</option>
							        </select>
							        <span></span>
							    </div>
                            </td>
                            <td>
                                <input  style="width: 150px;" type="text" id="searchWord" name="searchWord" value="${paramMap.searchWord}" placeholder="검색어 입력"   onkeypress="if(event.keyCode=='13') search();">
                            </td>
				            <td>
				            	<div class="select-con">
							        <select style="width:100px;" class="dropdown" name="carrierType" id="carrierType" onchange="javascript:goSearch(this);">
							        	<option value="A" <c:if test='${paramMap.carrierType eq "A" }'> selected="selected"</c:if>>전체</option>
							            <option value="S" <c:if test='${paramMap.carrierType eq "S" }'> selected="selected"</c:if>>셀프</option>
							            <option value="C" <c:if test='${paramMap.carrierType eq "C" }'> selected="selected"</c:if>>캐리어</option>
							        </select>
							        <span></span>
							    </div>
				            </td>
				            <td>
                        		<input type="checkbox" style="width:15px;" id="includeZero" <c:if test='${paramMap.includeZero eq "Y" }'> checked</c:if> class="btn-primary" onclick="javascript:search();">
                            </td>
                            <td>
                        		<div>0건매출처포함</div>
                            </td>
                        	<td>
                        		<input type="button" id="searchStatusU" name="" value="매출처 정산" class="btn-primary" onclick="javascript:search();">
                                <!-- <input type="button" id="searchStatusU" name="" value="매출처 정산" class="btn-primary" onclick="javascript:searchStatusChange('U',this);"> -->
                            </td>
                        	<td>					
                        		<input type="button" id="searchStatusI" name="" value="매입처 정산" class="btn-normal" onclick="javascript:document.location.href='/account/anotherCal.do'">
                                <!-- <input type="button" id="searchStatusI" name="" value="매입처 정산" class="btn-normal" onclick="javascript:searchStatusChange('I',this);"> -->
                            </td>
                            <td>
                            	<!-- <input type="button" id="searchStatusO" name="" value="기사 정산" class="btn-normal" onclick="javascript:searchStatusChange('O',this);"> -->
                                <!-- <input type="button" id="searchStatusO" name="" value="기사 정산" class="btn-normal" onclick="javascript:searchStatusChange('O',this);"> -->
                            </td>
                              
                       
                            
                            
                        </tr>
                    </tbody>
                </table>
            </div>

        </section>

			<div class="confirmation" style="min-height:0px; text-align:left; margin-top:-10px; margin-left:290px; margin-bottom:10px;">
				<div class="<c:if test='${paramMap.decideStatus eq "A" }'> confirm</c:if><c:if test='${paramMap.decideStatus eq "Y" }'> cancel</c:if><c:if test='${paramMap.decideStatus eq "N" }'> cancel</c:if>">
	            	<a style="cursor:pointer;" onclick="javascript:decideStatus('A');">전체</a>
	            </div>
	            <div class="<c:if test='${paramMap.decideStatus eq "A" }'> cancel</c:if><c:if test='${paramMap.decideStatus eq "Y" }'> cancel</c:if><c:if test='${paramMap.decideStatus eq "N" }'> confirm</c:if>">
	            	<a style="cursor:pointer;" onclick="javascript:decideStatus('N');">미확정</a>
	            </div>
	            <div class="<c:if test='${paramMap.decideStatus eq "A" }'> cancel</c:if><c:if test='${paramMap.decideStatus eq "Y" }'> confirm</c:if><c:if test='${paramMap.decideStatus eq "N" }'> cancel</c:if>">
	            	<a style="cursor:pointer;" onclick="javascript:decideStatus('Y');">확정</a>
	            </div>
	            <div class="cancel">
	            	<!-- <a style="cursor:pointer;" onclick="javascript:document.location.href='/account/billRequest.do';">세금계산서 발행요청</a> 
	            	document.location.href='/account/billRequest.do
	            	-->
	            	<a style="cursor:pointer;" onclick="javascript:go_BillRequest()">발행요청</a>
	            	
	            </div>
	               <div class="cancel">
		            	<a style="cursor:pointer;" onclick="javascript:go_BillRequest_list()">세금계산서 발행 리스트</a>
	            	</div>
	            	
	            
            </div>
            
            <section class="bottom-table" style="width:1600px; margin-left:290px;">
            	                                        
                <table class="article-table" style="margin-top:25px; table-layout:fixed;">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                        	<td style="width:40px;"><input type="checkbox" id="checkAll" onclick="javascript:checkAll();" ></td>
                        	
                            <!-- <td style="width:250px; text-align:center; ">소속</td> -->
                            <td style="width:auto; text-align:center; cursor:pointer;" onclick="javascript:sortBy('C');">회사명<c:if test='${paramMap.forOrder eq "C" }'><c:if test='${paramMap.order eq "asc" }'><img style="margin-left:10px;" src="/img/arrow-up.png" alt=""></c:if><c:if test='${paramMap.order eq "desc" }'><img style="margin-left:10px;" src="/img/arrow-down.png" alt=""></c:if></c:if></td>
                            <td style="text-align:center; cursor:pointer;" onclick="">세금계산서 발행 구분<c:if test='${paramMap.forOrder eq "D" }'><c:if test='${paramMap.order eq "asc" }'><img style="margin-left:10px;" src="/img/arrow-up.png" alt=""></c:if><c:if test='${paramMap.order eq "desc" }'><img style="margin-left:10px;" src="/img/arrow-down.png" alt=""></c:if></c:if></td>
                            <td style="width:auto; text-align:center; cursor:pointer;" onclick="javascript:sortBy('A');">매출액 <c:if test='${paramMap.forOrder eq "A" }'><c:if test='${paramMap.order eq "asc" }'><img style="margin-left:10px;" src="/img/arrow-up.png" alt=""></c:if><c:if test='${paramMap.order eq "desc" }'><img style="margin-left:10px;" src="/img/arrow-down.png" alt=""></c:if></c:if></td>
                            <td style="width:auto; text-align:center; cursor:pointer;" onclick="javascript:sortBy('B');">매출(픽업)건수 <c:if test='${paramMap.forOrder eq "B" }'><c:if test='${paramMap.order eq "asc" }'><img style="margin-left:10px;" src="/img/arrow-up.png" alt=""></c:if><c:if test='${paramMap.order eq "desc" }'><img style="margin-left:10px;" src="/img/arrow-down.png" alt=""></c:if></c:if></td>
                            <c:if test='${paramMap.decideStatus eq "Y" }'>
                            	<td style="text-align:center;">세금계산서발행일</td>
                            	<td style="text-align:center;">적요</td>
                            	<td style="text-align:center;">비고</td>
                            		
                            </c:if>
                        </tr>
                    </thead>
                    <tbody id="dataArea">
                    		<%-- <tr class="ui-state-default"> 
								<td>${totalCount}</td>
	                            <td style="text-align:center;"></td>
	                            <td style="text-align:center;"></td>
	                            <td style="text-align:center;"></td>
	                            <td style="text-align:center;"></td>
                        	</tr> --%>
                        	
                        	
                        	
                    	<c:forEach var="data" items="${listData}" varStatus="status">
							<tr class="ui-state-default"> 
								<td><input type="checkbox" name="forDecideStatus" customerId="${data.customer_id}" onclick="javascript:getCheckBoxCnt();"></td>
								
	                         <%--    <td style="text-align:center;"><c:if test ='${data.companyType eq "company1"}'>한국캐리어</c:if>
	                            											<c:if test ='${data.companyType eq "company2"}'>한국카캐리어(주)</c:if></td> --%>
	                     
	                            <td style="text-align:center;  cursor:pointer;" onclick="javascript:viewTotalSales('${data.customer_id}');">${data.customer_name}</td>
	                            <td style="text-align:center;">&nbsp;발행&nbsp;:&nbsp;${data.billing_cnt}&nbsp;/&nbsp;미발행&nbsp;:&nbsp;${data.cash_cnt}&nbsp;/&nbsp;총매출건&nbsp;:&nbsp;${data.allocation_cnt}</td>
	                            <td style="text-align:center;">${data.total_sales}</td>
	                            <td style="text-align:center;">${data.cnt}<c:if test='${data.pickup_cnt > 0}'>(${data.pickup_cnt})</c:if>건</td>
	                            <c:if test='${paramMap.decideStatus eq "Y" }'>
	                            	<!-- <td style="text-align:center;"><input style="text-align:center; width:75%;" class="datepick" type="text" placeholder="발행일 입력" onclick="javascript:$(this).val('');" readonly="readonly" value=""></td> -->
	                            	<td style="text-align:center;"><input style="text-align:center; width:75%;" class="datepick" type="text" placeholder="발행일 입력"  value=""></td>
	                            	<td style="text-align:center;"><input style="text-align:center; width:75%;" type="text" placeholder="적요 입력"  value=""></td>
	                            	<td style="text-align:center;"><input style="text-align:center; width:75%;" type="text" placeholder="비고" value=""></td>
	                            </c:if>
                        	</tr>
						</c:forEach>
						<tr class="ui-state-default"> 
								<c:if test='${paramMap.decideStatus ne "A" }'>
									<td style="background:#e1e1e1;"></td>
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;">통&nbsp;&nbsp;&nbsp;계</td>
		                           <!--  <td style="text-align:center; background:#e1e1e1; font-weight:bold;"></td> -->
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;"><c:if test='${paramMap.decideStatus eq "Y" }'> 확정&nbsp;</c:if><c:if test='${paramMap.decideStatus eq "N" }'> 미확정&nbsp;</c:if>거래처 수 : ${totalCount}</td>
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;"><c:if test='${paramMap.decideStatus eq "Y" }'> 확정&nbsp;</c:if><c:if test='${paramMap.decideStatus eq "N" }'> 미확정&nbsp;</c:if>매출 : ${totalStatistics.statistics_price}원</td>
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;"><c:if test='${paramMap.decideStatus eq "Y" }'> 확정&nbsp;</c:if><c:if test='${paramMap.decideStatus eq "N" }'> 미확정&nbsp;</c:if>건 수 : ${totalStatistics.total_cnt}건</td>
		                            <c:if test='${paramMap.decideStatus eq "Y" }'>
		                             <td style="text-align:center; background:#e1e1e1; font-weight:bold;"></td>
		                            	<td style="background:#e1e1e1;"></td>
		                            	<td style="background:#e1e1e1;"></td>
		                            </c:if>
	                            </c:if>
	                            <c:if test='${paramMap.decideStatus eq "A" }'>
									<td style="background:#e1e1e1;"></td>
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;">통&nbsp;&nbsp;&nbsp;계</td>
		                             <!-- <td style="text-align:center; background:#e1e1e1; font-weight:bold;"></td> -->
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;"> 총 거래처 수 : ${totalCount}&nbsp;/&nbsp; 미확정 거래처 수 : ${totalStatisticsN.statistics_cnt}&nbsp;/&nbsp; 확정 거래처 수 : ${totalStatisticsY.statistics_cnt}</td>
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;"> 총 매출 : ${totalStatistics.statistics_price}원&nbsp;/&nbsp; 미확정 : ${totalStatisticsN.statistics_price}원&nbsp;/&nbsp; 확정 : ${totalStatisticsY.statistics_price}원</td>
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;"> 총 건 수 : ${totalStatistics.total_cnt}건&nbsp;/&nbsp; 미확정 건 수 : ${totalStatisticsN.total_cnt}건&nbsp;/&nbsp; 확정 건 수 : ${totalStatisticsY.total_cnt}건</td>
	                            </c:if>
                        	</tr>
                    </tbody>
                </table>
                <div class="table-pagination text-center">
                    <ul class="pagination">
                        <!-- <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="curr-page"><a href="#">1</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li> -->
                        <html:paging uri="/account/customerCal.do" forGroup="&decideStatus=${paramMap.decideStatus}&forOrder=${paramMap.forOrder}%5E${paramMap.order}&includeZero=${paramMap.includeZero}&carrierType=${paramMap.carrierType}&startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&searchType=${paramMap.searchType}&searchWord=${paramMap.searchWord}&searchDateType=${paramMap.searchDateType}&customerId=${paramMap.customerId}" frontYn="N" />
                    </ul>
                </div>
                <div class="confirmation">
                	<c:if test='${paramMap.decideStatus eq "N" }'>
	                   <div class="confirm">
	                    	<a style="cursor:pointer;" onclick="javascript:customerCheck('Y')">일괄확정</a>
	                    </div>
	                    <!-- <div class="cancel">
	                    	<a style="cursor:pointer;" onclick="javascript:sendMail();">메일전송</a>
	                    </div> -->
	                    <div class="cancel">
	                    	<a style="cursor:pointer;" onclick="javascript:excelDownload();">엑셀다운로드</a>
	                    </div>
                    </c:if>
                    <c:if test='${paramMap.decideStatus eq "Y" }'>
	                   <div class="confirm">
	                    	<a style="cursor:pointer;" onclick="javascript:customerCheck('N')">수정/확정취소</a>
	                    </div>
	                    <div class="cancel">
	                    	<a style="cursor:pointer;" onclick="javascript:sendMail();">메일전송</a>
	                    </div>
	                    <div class="cancel">
	                    	<a style="cursor:pointer;" onclick="javascript:excelDownload();">엑셀다운로드</a>
	                    </div>
	                    <div class="cancel">
	                    	<a style="cursor:pointer;" onclick="javascript:billPublishRequest();">세금계산서 발행 및 청구요청</a>
	                    </div>
                    </c:if>
                    <c:if test='${paramMap.decideStatus eq "A" }'>
	                    <div class="confirm">
	                    	<a style="cursor:pointer;" onclick="javascript:excelDownload();">엑셀다운로드</a>
	                    </div>
                    </c:if>
                    
                     <div class="confirm">
	                        <!-- 청구액 엑셀 업로드 -->
                   		
                   			 	<form id="excelForm" style="display:none" name="excelForm" method="post" enctype="multipart/form-data">
                    				<input type="file" style="display:inline-block; width:300px;" id="upload" name="bbsFile" value="일괄입력" class="btn-primary" onchange="javascript:fileUpload(this);">
                   				 </form>
	            					<input type="button"class="btn-primary" onclick="javascript:$('#upload').trigger('click'); return false;" value="청구액 엑셀 업로드">
                 
	                    </div>
                    
                    
                </div>
                 <div>체크된 항목의 갯수 :
	                <span id ="checkedChk_box" ></span>   
	             </div>
                <div style="color:#8B0000;">※각 컬럼을 클릭 하여 오름차순 또는 내림차순으로 정렬 할 수 있으며 오름차순->내림차순->초기화 순으로 동작 합니다.</div>
               
                
                
                
            </section>
     
