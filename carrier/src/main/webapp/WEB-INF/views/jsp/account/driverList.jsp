<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>
<%

%>

<script type="text/javascript">
$(document).ready(function(){
	
	$("#selectMonth").MonthPicker({ 
		Button: false
		,MonthFormat: 'yy-mm'	
		,OnAfterChooseMonth: function() { 
	        //alert($(this).val());
	        document.location.href = "/account/driverList.do?&searchWord="+encodeURI($("#searchWord").val())+"&selectMonth="+$(this).val();
	    }
	});
	
});

function editDriverDeduct(driverId,selectMonth){
	
	document.location.href = "/account/driverCal.do?driverId="+driverId+"&selectMonth="+selectMonth;
}

function companySearch(companyType){
	      
	   document.location.href = "/account/driverList.do?&companyType="+companyType;
	   
}



/*
function downloadDriverDeduct(driverId,selectMonth){
	
	document.location.href = "/account/downloadDriverDeductInfo.do?driverId="+driverId+"&selectMonth="+selectMonth;
}
*/


function addEmp(){
	document.location.href = "/baseinfo/add-employee.do";
}


function search(){
	
	document.location.href = "/account/driverList.do?&searchWord="+encodeURI($("#searchWord").val())+"&selectMonth="+$("#selectMonth").val()+"&companyType="+$('#companyType').val();
	
}

function viewMonthDriverDeduct(driverId){
	
	document.location.href = "/account/viewMonthDriverDeduct.do?driverId="+driverId;
	
}


function insertDriverDeductStatus(driverName,driverId,selectMonth){
	
	
	if(confirm(driverName+" 기사님의 "+selectMonth+"월의 공제내역을 확정 하시겠습니까?")){
		
		$.ajax({ 
			type: 'post' ,
			url : "/account/insertDriverDeductStatus.do" ,
			dataType : 'json' ,
			data : {
				driverName : driverName,
				driverId : driverId,
				decideMonth : selectMonth
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("확정 되었습니다.");
					window.location.reload();
				}else if(result != "0000"){
					alert("확정 하는데 실패 했습니다. 관리자에게 문의 하세요.");
					window.location.reload();
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
		
		
	}else{
		
		
		
	}
	
	
}

function checkAll(){

	if($('input:checkbox[id="checkAll"]').is(":checked")){
		$('input:checkbox[name="forDeductStatus"]').each(function(index,element) {
			$(this).prop("checked","true");
		 });	
	}else{
		$('input:checkbox[name="forDeductStatus"]').each(function(index,element) {
			$(this).prop("checked","");
		 });
	}
}


function insertAllDriverDeductStatus(selectMonth){
	
	var total = $('input:checkbox[name="forDeductStatus"]:checked').length;
	var driverId = "";
		
	if(total == 0){
		alert("확정할 목록이 선택 되지 않았습니다.");
		return false;
	}else{
		$('input:checkbox[name="forDeductStatus"]:checked').each(function(index,element) {
		      if(this.checked){	//checked 처리된 항목의 값
		    	  var selectedObj = new Object();
		    	  driverId+=$(this).attr("driverId");
		    	  if(index<total-1){
		    		  driverId += ","; 
			         } 
		      }
		 });
		
		if(driverId != ""){
			insertDeductStatus(driverId,total,selectMonth);
		}
			

	}
	
	
	/* if(confirm(selectMonth+"월의 공제내역을 일괄확정 하시겠습니까?")){
		
		$.ajax({ 
			type: 'post' ,
			url : "/account/insertAllDriverDeductStatus.do" ,
			dataType : 'json' ,
			data : {
				selectMonth : selectMonth,
				searchType : "${paramMap.searchType}",
				searchWord : "${paramMap.searchWord}"
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("확정 되었습니다.");
					window.location.reload();
				}else if(result != "0000"){
					alert("확정 하는데 실패 했습니다. 관리자에게 문의 하세요.");
					window.location.reload();
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
		
		
	}else{
		
	} */
	
	
}



function insertDeductStatus(driverId,total,selectMonth){
	
	
	if(confirm(total+"명의 공제내역을 확정 하시겠습니까?")){
		
		$.ajax({ 
			type: 'post' ,
			url : "/account/insertAllDriverDeductStatus.do" ,
			dataType : 'json' ,
			data : {
				selectMonth : selectMonth,
				driverId : driverId
				//searchWord : "${paramMap.searchWord}"
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("확정 되었습니다.");
					window.location.reload();
				}else if(result != "0000"){
					alert("확정 하는데 실패 했습니다. 관리자에게 문의 하세요.");
					window.location.reload();
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
		
		
	}else{
		
	}
}



function viewTotalSales(driverId){
	document.location.href = "/account/driverListDetail.do?&paymentPartnerId="+driverId+"&decideMonth="+$("#selectMonth").val();
}


function downloadDriverDeduct(driverId,selectMonth){
	
	window.open("/account/viewDriverDeductInfo.do?driverId="+driverId+"&selectMonth="+selectMonth,"_blank","top=0,left=0,width=780,height=1000,toolbar=0,status=0,scrollbars=0,resizable=0");
	
}



function viewDriverBillingFile(driverId,selectMonth){
	
	window.open("/account/viewDriverBillingFile.do?driverId="+driverId+"&selectMonth="+selectMonth,"_blank","top=0,left=0,width=1000,height=700,toolbar=0,status=0,scrollbars=0,resizable=0");
	
}




function updateAnotherDecideFinalForMakeExcel(){
	
	
	if(confirm($("#selectMonth").val()+"월분 운송비 정산 내역서를 일괄 성성 하시겠습니까?")){
	
	$.ajax({ 
		type: 'post' ,
		url : "/account/updateAnotherDecideFinalForMakeExcel.do" ,
		dataType : 'json' ,
		data : {
			decideMonth : $("#selectMonth").val()
		},
		success : function(data, textStatus, jqXHR)
		{
			var result = data.resultCode;
			var resultData = data.resultData;
			if(result == "0000"){
				alert("일괄 생성 되었습니다.");
			}else if(result == "0001"){
				alert("일괄 생성 하는데 실패 하였습니다.");
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
	}
	
	
}

function fileUpload(fis){


	 
 	if(confirm("기사 지급금 일괄 수정 하시겠습니까?")){
		$("#excelForm").attr("action","/allocation/excelUploadForALLDriverAmount.do");
		$("#excelForm").submit();			
	}  

}


var order = "${order}";
function sortby(gubun){

	if(order == "" || order == "desc"){
		order = "asc";
	}else{
		order = "desc";
	}
	
	var loc = document.location.href;
	var str = "";
	if(loc.indexOf("?") > -1){
		//forOrder 가 있는경우 ㅎㅎ
		if(loc.indexOf("forOrder") > -1){
			var queryString = loc.split("?");
			var query = queryString[1].split("&");
			
			for(var i = 0; i < query.length; i++){
				if(query[i].indexOf("forOrder") > -1){
					query[i] = "forOrder="+gubun+"%5E"+order;
				}
			}
			for(var i = 0; i < query.length; i++){
				if(query[i] != ""){
					str += "&"+query[i];	
				}
			}
			document.location.href = queryString[0]+"?"+str;
		}else{
			str="&forOrder="+gubun+"%5E"+order;
			document.location.href = loc+str;
		}
		
	}else{
		str="?&forOrder="+gubun+"%5E"+order;
		document.location.href = loc+str;
	}
	
}





</script>
		<!-- <section class="side-nav"> -->
            <!-- <ul>
                <li class="active"><a href="/baseinfo/employee.do">직원관리</a></li>
                <li class=""><a href="/baseinfo/driver.do">기사관리</a></li>
                <li class=""><a href="/baseinfo/customer.do">거래처관리</a></li>
                <li class=""><a href="/baseinfo/company.do">회사정보관리</a></li>
            </ul> -->
        <!-- </section> -->

        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">HOME</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">월별운송비관리</a></li>
                </ul>
            </div>

            <div class="up-dl clearfix header-search">
                 <table>
                    <tbody>
                        <tr>
                            <td>
                        		 &nbsp;소속 선택 :&nbsp;
                            </td>
                                <td style="width: 149px;">
                                <div class="select-con">
							        <select class="dropdown" name="companyType" id="companyType">
							        	<option value="" selected="selected">전체</option>
							            <option value="company1" <c:if test='${paramMap.companyType eq "company1"}'> selected="selected"</c:if>>한국캐리어</option>
							            <option value="company2" <c:if test='${paramMap.companyType eq "company2" }'> selected="selected"</c:if>>한국카캐리어(주)</option>
							        </select>
							    </div>
                            </td>
                           <!--  <td>
                                <input type="button" id="btn-company" value="검색" class="btn-primary" onclick="javascript:companySearch($('#companyType').val());">
                            </td>
                         -->
                        
                        	<td>조회월 :</td>
				            <td class="widthAuto" style="width:200px;">
				                <input style="width:100%; text-align:center;" id="selectMonth" type="text" placeholder="조회월" onclick="javascript:$(this).val('');"  readonly="readonly" value="${paramMap.selectMonth}">
				                <%-- <input style="width:40%; text-align:center;" class="datepick" type="text" placeholder="조회종료일" onclick="javascript:$(this).val('');" readonly="readonly" name="endDt" id="endDt"  value="${paramMap.endDt}"> --%>
				            </td>
                            <td style="width: 190px;">
                                <input type="text" id="searchWord" name="searchWord" value="${paramMap.searchWord}"   onkeypress="if(event.keyCode=='13') search();">
                            </td>
                            <td>
                                <input type="button" id="btn-search" value="검색" class="btn-primary" onclick="javascript:search();">
                            </td>
                            <td>
                                <!-- <input type="button" id="btn-search" value="정산내역서생성" class="btn-primary" onclick="javascript:updateAnotherDecideFinalForMakeExcel();"> -->
                                <input type="button" id="btn-search" value="일괄 확정" class="btn-primary" onclick="javascript:insertAllDriverDeductStatus('${paramMap.selectMonth}');">
                            </td>
                            
                            <!-- 기사 지급액 엑셀 업로드 -->
                   			 <td>
                   			 	<form id="excelForm" style="display:none" name="excelForm" method="post" enctype="multipart/form-data">
                    				<input type="file" style="display:inline-block; width:300px;" id="upload" name="bbsFile" value="일괄입력" class="btn-primary" onchange="javascript:fileUpload(this);">
                   				 </form>
	            					<input type="button"class="btn-primary" onclick="javascript:$('#upload').trigger('click'); return false;" value="지급액 엑셀 업로드">
                            </td>
                    	
                        </tr>
                        
                    </tbody>
                </table>
            </div>
            
<!-- 
            <div class="dispatch-btn-container">
               
                <div class="dispatch-btn">
                    <i class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div>
            </div> -->
        </section>



            <section class="bottom-table" style="width:1600px; margin-left:290px;">
                
                <table class="article-table" style="margin-top:15px">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <!-- <td style="text-align:center;">기사아이디</td> -->
                            <td style="text-align:center; width:50px;"><input type="checkbox" id="checkAll" onclick="javascript:checkAll();" ></td>
                            <td style="text-align:center;">기사명</td>
                            <td style="text-align:center; cursor:pointer;" onclick="javascript:sortby('A');">차량소속</td>
                            <td style="text-align:center;">총매입건수</td>
                            <td style="text-align:center;">총매입금액</td>
                            <td style="text-align:center;">현장수금건수</td>
                            <td style="text-align:center;">현장수금액</td>
                            <td style="text-align:center;">공제건수</td>
                            <td style="text-align:center;">청구공제</td>
                            <td style="text-align:center;">대납공제</td>
                            <td style="text-align:center;">최종지급액</td>
                            <td style="text-align:center;">확정여부</td>
                            <td style="text-align:center; width:100px;">내역서생성여부</td>
                            <td style="text-align:center;">내역서전송여부</td>
                            <!-- <td style="text-align:center;">연락처</td> -->
                            <!-- <td style="text-align:center; width:250px;">관리</td> -->
                            <td style="text-align:center; width:250px;">상세</td>
                            
                        </tr>
                    </thead>
                    <tbody id="">
                    	<c:forEach var="data" items="${listData}" varStatus="status">
							<tr class="ui-state-default"> 
	                            <td style="text-align:center;"><input type="checkbox" name="forDeductStatus" driverId="${data.driver_id}"></td>
	                            <td style="text-align:center; cursor:pointer;" onclick="javascript:viewTotalSales('${data.driver_id}');">${data.driver_name}</td>
	                            <td style="text-align:center;">${data.carrier_type_name}</td>
	                            <td style="text-align:center;">${data.another_cnt}</td>
	                            <td style="text-align:center;">${data.another_sum}</td>
	                            <td style="text-align:center;">${data.dd_cnt}</td>
	                            <td style="text-align:center;">${data.dd_sales}</td>
	                            <td style="text-align:center;">${data.deduct_cnt}</td>
	                            
	                            <td style="text-align:center;">${data.deduct_amount_one}</td>
	                            <td style="text-align:center;">${data.deduct_amount_two}</td>
	                            
	                            <c:if test="${data.final_val != '0'}">
	                            	<td style="text-align:center;">${data.final_val}</td>
	                            </c:if>
	                            <c:if test="${data.final_val == '0'}">
	                            	<td style="text-align:center;">확정진행중</td>
	                            </c:if>
	                            <td style="text-align:center;">
		                            <c:if test="${data.deduct_status == 'N'}">
		                            	미확정
		                            </c:if>
		                            <c:if test="${data.deduct_status != 'N'}">
		                            	확정
		                            </c:if>
	                            </td>
	                            <td style="text-align:center;">
	                            	<c:if test="${data.deduct_file != 'N'}">
		                            	<a style="cursor:pointer;" onclick="javascript:downloadDriverDeduct('${data.driver_id}','${paramMap.selectMonth}');" class="btn-edit">내역서확인</a>
		                            </c:if>
									<c:if test="${data.deduct_file == 'N'}">
		                            	생성안됨
		                            </c:if>
								</td>
								<td style="text-align:center;">
	                            	<c:if test="${data.driver_view_flag != 'N'}">
		                            	<p style="color:#F00;">전송 완료</p>
		                            </c:if>
									<c:if test="${data.driver_view_flag == 'N'}">
		                            	<p>전송 대기중</p>
		                            </c:if>
								</td>
								
	                            <td style="text-align:center;">
	                            
	                            	<c:if test="${data.deduct_status == 'N'}">
	                            		<a style="cursor:pointer; width:70px;" onclick="javascript:insertDriverDeductStatus('${data.driver_name}','${data.driver_id}','${paramMap.selectMonth}');" class="btn-edit">공제확정</a>
	                            	</c:if>
	                            	<%-- <a style="cursor:pointer;" onclick="javascript:downloadDriverDeduct('${data.driver_id}','${paramMap.selectMonth}');" class="btn-edit">운송내역서</a> --%>
	                            	<a style="cursor:pointer; width:70px;" onclick="javascript:editDriverDeduct('${data.driver_id}','${paramMap.selectMonth}');" class="btn-edit">공제내역</a>
	                            	<a style="cursor:pointer; width:70px;" onclick="javascript:viewMonthDriverDeduct('${data.driver_id}');" class="btn-edit">상세보기</a>
	                            	<c:if test="${data.driver_billing_status == 'Y' && data.driver_billing_file_path != 'N'}">
	                            		<a style="cursor:pointer; width:70px;" onclick="javascript:viewDriverBillingFile('${data.driver_id}','${paramMap.selectMonth}');" class="btn-edit">세금계산서</a>
	                            	</c:if>
	                            	<%-- <a style="cursor:pointer;" onclick="javascript:editDriverDeduct('${data.driver_id}','${paramMap.selectMonth}');" class="btn-edit">공제내역</a> --%>
	                            </td>
                        	</tr>
						</c:forEach>
                        <tr class="ui-state-default"> 
									<td style="background:#e1e1e1;"></td>
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;">통&nbsp;&nbsp;&nbsp;계</td>
		                            <td style="background:#e1e1e1;"></td>
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;">${driverTotalStatistics.another_cnt}</td>
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;">${driverTotalStatistics.another_sum}</td>
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;">${driverTotalStatistics.dd_cnt}</td>
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;">${driverTotalStatistics.dd_sales}</td>
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;">${driverTotalStatistics.deduct_cnt}</td>
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;">${driverTotalStatistics.item_deduct_amount_one}</td>
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;">${driverTotalStatistics.item_deduct_amount_two}</td>
		                            <%-- <td style="text-align:center; background:#e1e1e1; font-weight:bold;">${driverTotalStatistics.bill_for_payment}</td> --%>
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;">${driverTotalStatistics.final_val}</td>
		                            <td style="background:#e1e1e1;"></td>
		                            <td style="background:#e1e1e1;"></td>
		                            <td style="background:#e1e1e1;"></td>
		                            <td style="background:#e1e1e1;"></td>
		                            
                        	</tr>
                        
                        
                        
                        
                        
                        
                        
                    </tbody>
                </table>
                <%-- <div class="table-pagination text-center">
                    <ul class="pagination">
                        <html:paging uri="/account/driverList.do" forGroup="&selectMonth=${paramMap.selectMonth}" />
                    </ul>
                </div> --%>
                <div class="confirmation">
                    <div class="confirm">
                        <a href="/account/driverList_download.do?selectMonth=${paramMap.selectMonth}">엑셀다운로드</a>
                    </div>
                    <%-- <div class="confirm">
                        <a href="/account/driverList_download.do?selectMonth=${paramMap.selectMonth}"></a>
                    </div> --%>
                </div>
                
            </section>
     
