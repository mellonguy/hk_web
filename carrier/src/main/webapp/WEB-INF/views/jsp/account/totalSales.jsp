<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/js/bootstable.js"></script>
<script type="text/javascript"> 
var oldListOrder = "";
var allocationId = "";

$(document).ready(function(){
	//forOpen

	
	$('#dataTable tr:odd td').css("backgroundColor","#f9f9f9");
	
	$("#startDt").datepicker({
		dateFormat : "yy-mm-dd",
		onSelect : function(date){
			$("#endDt").focus();
		}
	});
	
	
	
});

function updateAllocaion(row){
	var inputDt = "";
    var carrierType = "";
    var distanceType = "";
    var departureDt = "";
    var departureTime = "";
    var customerName = "";
    var carKind = "";
    var carIdNum = "";
    var carNum  = "";
    var departure = "";
    var arrival  = "";
    var driverName = "";
    var carCnt  = "";
    var amount   = "";
    var paymentKind = "";
	var allocId = $(row).attr("allocationId");
	if(confirm("수정 하시겠습니까?")){
		$(row).find("td").each(function(index,element){
			if(index==1){
				inputDt = $(this).html();
			}
			if(index==2){
				if($(this).html() == "셀프"){
					carrierType = "S";	
				}else if($(this).html() == "캐리어"){
					carrierType = "C";
				}else{
					alert("배차구분을 확인 하세요.");
					return false;
				}
			}
			if(index==3){
				if($(this).html() == "시내"){
					distanceType = "0";	
				}else if($(this).html() == "시외"){
					distanceType = "1";
				}else{
					alert("거리구분을 확인 하세요.");
					return false;
				}
			}
			if(index==4){
				departureDt = $(this).html();
			}
			if(index==5){
				departureTime = $(this).html();
			}
			if(index==6){
				customerName = $(this).html();
			}
			if(index==7){
				carKind = $(this).html();
			}
			if(index==8){
				carIdNum = $(this).html();
			}
			if(index==9){
				carNum = $(this).html();
			}
			if(index==10){
				departure = $(this).html();
			}
			if(index==11){
				arrival = $(this).html();
			}
			if(index==12){
				driverName = $(this).html();
			}
			if(index==13){
				carCnt = $(this).html();
			}
			if(index==14){
				amount = $(this).html();
			}
			if(index==15){
				paymentKind = $(this).html();
			}
      	});

		if(carrierType == "" || distanceType == ""){
			return false;	
		}
		
		 $.ajax({ 
				type: 'post' ,
				url : "/allocation/update-allocation.do" ,
				dataType : 'json' ,
				data : {
					inputDt : inputDt,
					carrierType : carrierType,
					distanceType : distanceType,
					departureDt : departureDt,
					departureTime : departureTime,
					customerName : customerName,
					carKind : carKind,
					carIdNum : carIdNum,
					carNum : carNum,
					departure : departure,
					arrival : arrival,
					driverName : driverName,
					carCnt : carCnt,
					amount : amount,
					paymentKind : paymentKind,
					allocationId : allocId
				},
				success : function(data, textStatus, jqXHR)
				{
					var result = data.resultCode;
					if(result == "0000"){
						alert("변경 되었습니다.");
			//			document.location.href = "/allocation/combination.do";
					}else if(result == "0001"){
   						alert("변경 하는데 실패 하였습니다.");
   					}
				} ,
				error : function(xhRequest, ErrorText, thrownError) {
				}
			}); 
		
	}

}

function getListId(obj){
	$('html').scrollTop(0);
	selectList($(obj).parent().parent().parent().attr("allocationId"));
	
}

var driverArray = new Array();
function selectList(id){
	
	document.location.href="/allocation/allocation-view.do?location=${paramMap.location}&allocationId="+id;
	
}	
	
function batchStatus(status){
	
	var id = "";
	var total = $('input:checkbox[name="forBatch"]:checked').length;
	
	if(total == 0){
		alert("수정할 목록이 선택 되지 않았습니다.");
		return false;
	}else{
		$('input:checkbox[name="forBatch"]:checked').each(function(index,element) {
		      if(this.checked){//checked 처리된 항목의 값
		    	  id+=$(this).attr("allocationId");
		    	  if(index<total-1){
		    		  id += ","; 
			         } 
		      }
		 });	
		updateAllocationStatus(status,id);
	}
}	

function updateAllocationStatus(status,id){
	
	var msg = "";
	if(status == "cancel"){
		msg = "취소";
	}else if(status == "complete"){
		msg = "완료";
	}
	
	if(confirm(msg+"하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : "/allocation/updateAllocationStatus.do" ,
			dataType : 'json' ,
			data : {
				status : status,
				allocationId : id
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert(msg+"되었습니다.");
					document.location.href = "/allocation/combination.do";
				}else if(result == "0001"){
					alert(msg+"하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
}	
	
function checkAll(){

	if($('input:checkbox[id="checkAll"]').is(":checked")){
		$('input:checkbox[name="forBatch"]').each(function(index,element) {
			$(this).prop("checked","true");
		 });	
	}else{
		$('input:checkbox[name="forBatch"]').each(function(index,element) {
			$(this).prop("checked","");
		 });
	}
}
	
	
	
function move(location){
	
	
	window.location.href = "/allocation/"+location+".do";
	
}	
	
	
function excelDownload(carrierType){
	
	document.location.href = "/allocation/excel_download.do?carrierType="+carrierType;
}	
	
	


function sendSocketMessage(){
	
	$.ajax({ 
		type: 'post' ,
		url : "http://52.78.153.148:8080/allocation/sendSocketMessage.do" ,
		dataType : 'json' ,
		data : {
			allocationId : 'ALO3df74bd0105046bfbdce51ab0e64927c',
			driverId : "sourcream"
		},
		success : function(data, textStatus, jqXHR)
		{
			var result = data.resultCode;
			var resultData = data.resultData;
			if(result == "0000"){
				//alert("성공");
			}else if(result == "0001"){
			
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
}
	
	
	
 </script>




        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">회계관리</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">배차 현황</a></li>
                </ul>
            </div>

            <div class="up-dl clearfix">
                
                <form name="searchForm" method="get" action="combination.do">
                	<div class="date-picker">
                		<select name="searchDateType">
							<option value="" <c:if test='${paramMap.searchDateType eq "" }'> selected="selected"</c:if> >선택</option>
							<option value="S" <c:if test='${paramMap.searchDateType eq "S" }'> selected="selected"</c:if> >의뢰일</option>
							<option value="D" <c:if test='${paramMap.searchDateType eq "D" }'> selected="selected"<</c:if> >출발일</option>
						</select>
	                	<input style="width:150px;" id="startDt" placeholder="검색 시작일" name="startDt" type="text" class="datepick" value="${paramMap.startDt}"> ~ <input style="width:150px;" id="endDt" placeholder="검색 종료일" name="endDt"  type="text" class="datepick" value="${paramMap.endDt}">
	                </div>
	                <div class="upload-btn" style="float:right;">
	                    <!-- <input type="button" value="엑셀 업로드"> -->
	                    <input type="button"  onclick="javascript:excelDownload('');" value="엑셀 다운로드">
	                    
	                    <!-- <input type="button"  onclick="javascript:sendSocketMessage();" value="소켓 메세지"> -->
	                    
	                    
	                </div>
					<div class="btns-submit">	
						<div class="right">
							<select name="searchType">
								<option value="" <c:if test='${paramMap.searchType eq "" }'> selected="selected"</c:if> >선택</option>
								<option value="carrier" <c:if test='${paramMap.searchType eq "carrier" }'> selected="selected"</c:if> >배차구분</option>
								<option value="distance" <c:if test='${paramMap.searchType eq "distance" }'> selected="selected"<</c:if> >운행구분</option>
								<option value="customer" <c:if test='${paramMap.searchType eq "customer" }'> selected="selected"<</c:if> >고객명</option>
								<option value="driver" <c:if test='${paramMap.searchType eq "driver" }'> selected="selected"<</c:if> >기사명</option>						
							</select>
							<input type="text" name="searchWord" value="${paramMap.searchWord}" placeholder="검색어 입력"  onkeypress="if(event.keyCode=='13'){encodeURI($(this).val()); document.searchForm.submit();}" />
							<a class="btn-gray" href="javascript:" onclick=" encodeURI($(this).prev().val()); document.searchForm.submit()"><input type="button" value="검색"></a>
						</div>
					</div>			
				</form>
                
                
            </div>

            <div class="dispatch-btn-container">
                <!-- <div class="dispatch-btn">
                    <i id="downArrow" class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div> -->
                
                <%-- <c:if test="${complete == null || complete != 'Y'}"> --%>
                	<%-- <c:if test="${forOpen == null || forOpen != 'N'}">
						<div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">신규배차입력</div>
					</c:if> --%>
					<%-- <c:if test="${forOpen != null && forOpen == 'N'}"> --%>
						<div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">배차 현황</div>
					<%-- </c:if>
				</c:if> --%>
                
                <%-- <c:if test="${complete != null && complete == 'Y'}">
					<div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">완료배차</div>					
				</c:if> --%>
                
            </div>
        </section>

        <div class="dispatch-wrapper">
            <%-- <jsp:include page="common-form.jsp"></jsp:include> --%>
            <section class="dispatch-bottom-content">
			</section>
			<div id="bottom-table">
	            <section  class="bottom-table" style="width:1600px; margin-left:10px;">
	                <table class="article-table forToggle" id="dataTable" style="width:100%;">
	                    <colgroup>
	                       <%-- <col width="auto"> --%>
	                        <col width="130px;">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="150px;">
	                        <col width="auto">
	                        <%-- <col width="auto"> --%>
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <%-- <c:if test="${user.control_grade == '01' }">
	                        	<col width="auto">
	                        	<col width="auto">
	                        </c:if> --%>
	                    </colgroup>
	                    <thead>
	                        <tr>
	                            <!-- <td class="showToggle"><input type="checkbox" id="checkAll" onclick="javascript:checkAll();" ></td> -->
	                            <!-- <td class="showToggle">번호</td> -->
	                            <td>출발일</td>
	                            <td>출발시간</td>
	                            <td>고객명</td>
	                            <td>차종</td>
	                            <td>차대번호</td>
	                            <td>차량번호</td>
	                            <td>출발지</td>
	                            <td>하차지</td>
	                            <td>금액</td>
	                            <td>비고</td>
	                         	<%-- <c:if test="${user.control_grade == '01' }">   
	                            	<td class="showToggle"></td>
	                            	<td>수정</td>
	                            </c:if> --%>
	                        </tr>
	                    </thead>
	                    <tbody id="rowData">
	                    	<c:forEach var="data" items="${listData}" varStatus="status">
								<tr class="ui-state-default" list-order="${data.list_order}" allocationId="${data.allocation_id}">
	                            <%-- <td class="showToggle"><input type="checkbox" name="forBatch" allocationId="${data.allocation_id}"></td> --%>
	                            <%-- <td class="showToggle">${data.list_order}</td> --%>
	                            <td>${data.departure_dt}</td>
	                            <td>${data.departure_time}</td>
	                            <td style="cursor:pointer;" onclick="javascript:selectList('${data.allocation_id}');">${data.customer_name}</td>
	                            <td>${data.car_kind}</td>
	                            <td>${data.car_id_num}</td>
	                            <td>${data.car_num}</td>
	                            <td>${data.departure}</td>
	                            <td>${data.arrival}</td>
	                            <td>${data.sales_total}</td>
	                            <td>${data.etc}</td>
	                            
	                        </tr>
							</c:forEach>
	                    
	                       
	
	                    </tbody>
	                </table>
	                <div class="table-pagination text-center">
	                    <ul class="pagination">
	                    	<html:paging uri="/account/totalSales.do" forGroup="&startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&searchDateType=${paramMap.searchDateType}&customerId=${paramMap.customerId}" frontYn="N" />
	                       <!--  <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
	                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
	                        <li class="curr-page"><a href="#">1</a></li>
	                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
	                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li> -->
	                    </ul>
	                </div>                
	            </section>
            
            	<!-- <div class="confirmation">
                    <div class="cancel">
                        <a style="cursor:pointer;" onclick="javascript:batchStatus('cancel');">일괄취소</a>
                    </div>
                    <div class="confirm">
                        <a style="cursor:pointer;" onclick="javascript:batchStatus('complete');">일괄완료</a>
                    </div>
                </div> -->
            </div>
        </div>        
        <!-- <iframe style="width: 980px; height:10000px; border: none;" frameBorder="0" id="happyboxFrame" scrolling="no" src="https://www.happyalliance-happybox.org/Bridge?v=param"></iframe>​    -->
       <script>
		if("${userMap.control_grade}" == "01"){
			$('#dataTable').SetEditable({
            	columnsEd: "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15",
            	 onEdit: function(row) {updateAllocaion(row)},  
            	 onDelete: function() {},  
            	 onBeforeDelete: function() {}, 
            	 onAdd: function() {}  
            });
			
		}          
            </script>

