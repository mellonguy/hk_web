<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>
<%

%>

<script type="text/javascript">
$(document).ready(function(){
	
	$("#selectMonth").MonthPicker({ 
		Button: false
		,MonthFormat: 'yy-mm'	
		,OnAfterChooseMonth: function() { 
	        //alert($(this).val());
	        document.location.href = "/account/driverList.do?&searchWord="+encodeURI($("#searchWord").val())+"&selectMonth="+$(this).val();
	    }
	});
	
});

function editDriverDeduct(driverId,selectMonth){
	
	document.location.href = "/account/driverCal.do?driverId="+driverId+"&selectMonth="+selectMonth;
}


/*
function downloadDriverDeduct(driverId,selectMonth){
	
	//window.open("/account/downloadDriverDeductInfo.do?driverId="+driverId+"&selectMonth="+selectMonth,"_blank","top=0,left=0,width=1000,height=1000,toolbar=0,status=0,scrollbars=0,resizable=0");
	
	document.location.href = "/account/downloadDriverDeductInfo.do?driverId="+driverId+"&selectMonth="+selectMonth;
}
*/


function downloadDriverDeduct(driverId,selectMonth){
	
	window.open("/account/viewDriverDeductInfo.do?driverId="+driverId+"&selectMonth="+selectMonth,"_blank","top=0,left=0,width=1000,height=1000,toolbar=0,status=0,scrollbars=0,resizable=0");
	
}



function addEmp(){
	document.location.href = "/baseinfo/add-employee.do";
}


function search(){
	
	document.location.href = "/account/driverList.do?&searchWord="+encodeURI($("#searchWord").val())+"&selectMonth="+$("#selectMonth").val();
	
}

function viewMonthDriverDeduct(driverId){
	
	document.location.href = "/account/viewMonthDriverDeduct.do?driverId="+driverId;
	
}

function viewDetailList(driverId,decideMonth){

	document.location.href="/account/driverListDetail.do?&paymentPartnerId="+driverId+"&decideMonth="+decideMonth;
	
}



</script>
		<!-- <section class="side-nav"> -->
            <!-- <ul>
                <li class="active"><a href="/baseinfo/employee.do">직원관리</a></li>
                <li class=""><a href="/baseinfo/driver.do">기사관리</a></li>
                <li class=""><a href="/baseinfo/customer.do">거래처관리</a></li>
                <li class=""><a href="/baseinfo/company.do">회사정보관리</a></li>
            </ul> -->
        <!-- </section> -->

        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">HOME</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">공제내역</a></li>
                </ul>
            </div>

            <div class="up-dl clearfix header-search">
                 <table>
                    <tbody>
                        <tr>
                        	<td>조회월 :</td>
				            <td class="widthAuto" style="width:200px;">
				                <input style="width:100%; text-align:center;" id="selectMonth" type="text" placeholder="조회월" onclick="javascript:$(this).val('');"  readonly="readonly" name="startDt" id="startDt" value="${paramMap.selectMonth}">
				                <%-- <input style="width:40%; text-align:center;" class="datepick" type="text" placeholder="조회종료일" onclick="javascript:$(this).val('');" readonly="readonly" name="endDt" id="endDt"  value="${paramMap.endDt}"> --%>
				            </td>
                            <td style="width: 190px;">
                                <%-- <input type="text" id="searchWord" name="searchWord" value="${paramMap.searchWord}"   onkeypress="if(event.keyCode=='13') search();"> --%>
                                <input type="text" id="searchWord" name="searchWord" value="${paramMap.searchWord}"   onkeypress="">
                            </td>
                            <td>
                                <!-- <input type="button" id="btn-search" value="검색" class="btn-primary" onclick="javascript:search();"> -->
                                <input type="button" id="btn-search" value="검색" class="btn-primary" onclick="">
                            </td>
                            <td>
                                
                            </td>
                            
                        </tr>
                    </tbody>
                </table>
            </div>
<!-- 
            <div class="dispatch-btn-container">
               
                <div class="dispatch-btn">
                    <i class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div>
            </div> -->
        </section>



            <section class="bottom-table" style="width:1600px; margin-left:290px;">
                
                <table class="article-table" style="margin-top:15px">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <!-- <td style="text-align:center;">기사아이디</td> -->
                            <td style="text-align:center;">매입월</td>
                            <td style="text-align:center;">기사명</td>
                            <td style="text-align:center;">총매입건수</td>
                            <td style="text-align:center;">총매입금액</td>
                            <td style="text-align:center;">현장수금건수</td>
                            <td style="text-align:center;">현장수금액</td>
                            <td style="text-align:center;">공제건수</td>
                            <td style="text-align:center;">공제금액</td>
                            <!-- <td style="text-align:center;">연락처</td> -->
                            <td style="text-align:center; width:250px;">관리</td>
                            
                        </tr>
                    </thead>
                    <tbody id="">
                    	<c:forEach var="data" items="${listData}" varStatus="status">
							<tr class="ui-state-default"> 
	                            <%-- <td style="text-align:center;">${data.driver_id}</td> --%>
	                            <td style="text-align:center;">${data.year_month_a}</td>
	                            <td style="text-align:center; cursor:pointer;" onclick="javascript:viewDetailList('${data.driver_id}','${data.year_month_a}');">${data.driver_name}</td>
	                            <td style="text-align:center;">${data.another_cnt}</td>
	                            <td style="text-align:center;">${data.another_sum}</td>
	                            <td style="text-align:center;">${data.dd_cnt}</td>
	                            <td style="text-align:center;">${data.dd_sales}</td>
	                            <td style="text-align:center;">${data.deduct_cnt}</td>
	                            <td style="text-align:center;">${data.deduct_amount}</td>
	                            <%-- <td style="text-align:center;">${data.phone_num}</td> --%>
	                            <td style="text-align:center;">
	                            	<a style="cursor:pointer;" onclick="javascript:downloadDriverDeduct('${data.driver_id}','${data.year_month_a}');" class="btn-edit">운송내역서</a>
	                            	<a style="cursor:pointer;" onclick="javascript:editDriverDeduct('${data.driver_id}','${data.year_month_a}');" class="btn-edit">공제내역</a>
	                            </td>
                        	</tr>
						</c:forEach>
                        
                    </tbody>
                </table>
                <div class="table-pagination text-center">
                    <ul class="pagination">
                        <!-- <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="curr-page"><a href="#">1</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li> -->
                        <html:paging uri="/account/viewMonthDriverDeduct.do" forGroup="&driverId=${paramMap.driverId}" />
                    </ul>
                </div>
                <div class="confirmation">
                    <%-- <div class="confirm">
                        <a href="/account/driverList_download.do?selectMonth=${paramMap.selectMonth}">엑셀다운로드</a>
                    </div> --%>
                </div>
                
            </section>
     
