<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator"
   prefix="decorator"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld"%>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/js/bootstable.js"></script>
<script src="/js/vendor/html2canvas.min.js"></script>
<script src="/js/vendor/es6-promise.auto.js"></script>
<script type="text/javascript">

   var oldListOrder = "";
   var allocationId = "";

   $(document).ready(function() {
                  //forOpen

                  /* $('#dataTable tr:odd td').css("backgroundColor",
                        "#f9f9f9"); */

					var selectMonth = $("#selectMonth").val();
    					
                   $("#indate").datepicker({
          
                     dateFormat : "yy-mm-dd",
                     onSelect : function(date) {
                        //$("#endDt").focus();
                        setTimeout(function() {
                           //alert(this.toString());
                    
 							
                           document.location.href = "/account/summary.do?&searchWord="+date+"&searchType=date"+"&companyType="+$("#companyType").val();
                           //$("#endDt").focus();
                        }, 50);
                     }
                  }); 

                   
                  /* Gosummary */

                  $('#summary').click(function() {

                	  					
                                 var indate = document.getElementById('indate');
                                 var indateVal = $("#indate").val();
                                 if (indateVal == "") {
                       
                                 } else {

                                    document.location.href = "/account/summary.do?&searchWord="+indateVal+"&searchType=date"+"&companyType="+$("#companyType").val();

                                 }

                              });
 

                  
                     /* $("#indate").click(function(){

                                                
                         var indateVal = $("#indate").datepicker();
                          document.location.href = "/account/summary.do?&dateSearch="+ indateVal;

                        }); */

               	$("#selectMonth").MonthPicker({ 

            		Button: false
            		,MonthFormat: 'yy-mm'	
            		,OnAfterChooseMonth: function() { 
            	        //alert($(this).val());
            	        document.location.href = "/account/summary.do?&searchWord="+$(this).val()+"&searchType=month"+"&companyType="+$("#companyType").val();
            	        
            	     }
            	});


      
                            
               });


   
   function updateAllocaion(row) {
      var inputDt = "";
      var carrierType = "";
      var distanceType = "";
      var departureDt = "";
      var departureTime = "";
      var customerName = "";
      var carKind = "";
      var carIdNum = "";
      var carNum = "";
      var departure = "";
      var arrival = "";
      var driverName = "";
      var carCnt = "";
      var amount = "";
      var paymentKind = "";
      var allocId = $(row).attr("allocationId");
      if (confirm("수정 하시겠습니까?")) {
         $(row).find("td").each(function(index, element) {
            if (index == 1) {
               inputDt = $(this).html();
            }
            if (index == 2) {
               if ($(this).html() == "셀프") {
                  carrierType = "S";
               } else if ($(this).html() == "캐리어") {
                  carrierType = "C";
               } else {
                  alert("배차구분을 확인 하세요.");
                  return false;
               }
            }
            if (index == 3) {
               if ($(this).html() == "시내") {
                  distanceType = "0";
               } else if ($(this).html() == "시외") {
                  distanceType = "1";
               } else {
                  alert("거리구분을 확인 하세요.");
                  return false;
               }
            }
            if (index == 4) {
               departureDt = $(this).html();
            }
            if (index == 5) {
               departureTime = $(this).html();
            }
            if (index == 6) {
               customerName = $(this).html();
            }
            if (index == 7) {
               carKind = $(this).html();
            }
            if (index == 8) {
               carIdNum = $(this).html();
            }
            if (index == 9) {
               carNum = $(this).html();
            }
            if (index == 10) {
               departure = $(this).html();
            }
            if (index == 11) {
               arrival = $(this).html();
            }
            if (index == 12) {
               driverName = $(this).html();
            }
            if (index == 13) {
               carCnt = $(this).html();
            }
            if (index == 14) {
               amount = $(this).html();
            }
            if (index == 15) {
               paymentKind = $(this).html();
            }
         });

         if (carrierType == "" || distanceType == "") {
            return false;
         }

         $.ajax({
            type : 'post',
            url : "/allocation/update-allocation.do",
            dataType : 'json',
            data : {
               inputDt : inputDt,
               carrierType : carrierType,
               distanceType : distanceType,
               departureDt : departureDt,
               departureTime : departureTime,
               customerName : customerName,
               carKind : carKind,
               carIdNum : carIdNum,
               carNum : carNum,
               departure : departure,
               arrival : arrival,
               driverName : driverName,
               carCnt : carCnt,
               amount : amount,
               paymentKind : paymentKind,
               allocationId : allocId
            },
            success : function(data, textStatus, jqXHR) {
               var result = data.resultCode;
               if (result == "0000") {
                  alert("변경 되었습니다.");
                  //         document.location.href = "/allocation/combination.do";
               } else if (result == "0001") {
                  alert("변경 하는데 실패 하였습니다.");
               }
            },
            error : function(xhRequest, ErrorText, thrownError) {
            }
         });

      }

   }

   function getListId(obj) {
      $('html').scrollTop(0);
      selectList($(obj).parent().parent().parent().attr("allocationId"));

   }

   var driverArray = new Array();
   function selectList(id) {

      var searchWord = encodeURI('${paramMap.searchWord}');
      document.location.href = "/allocation/allocation-view.do?&searchDateType=${paramMap.searchDateType}&startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&searchType=${paramMap.searchType}&cPage=${paramMap.cPage}&allocationStatus=${paramMap.allocationStatus}&location=${paramMap.location}&allocationId="
            + id + "&searchWord=" + searchWord;

   }

   function batchStatus(status) {

      var id = "";
      var total = $('input:checkbox[name="forBatch"]:checked').length;

      if (total == 0) {
         alert("수정할 목록이 선택 되지 않았습니다.");
         return false;
      } else {
         $('input:checkbox[name="forBatch"]:checked').each(
               function(index, element) {
                  if (this.checked) {//checked 처리된 항목의 값
                     id += $(this).attr("allocationId");
                     if (index < total - 1) {
                        id += ",";
                     }
                  }
               });
         updateAllocationStatus(status, id);
      }
   }

   function updateAllocationStatus(status, id) {

      var msg = "";
      if (status == "cancel") {
         msg = "취소";
      } else if (status == "complete") {
         msg = "완료";
      }

      if (confirm(msg + "하시겠습니까?")) {
         $.ajax({
            type : 'post',
            url : "/allocation/updateAllocationStatus.do",
            dataType : 'json',
            data : {
               status : status,
               allocationId : id
            },
            success : function(data, textStatus, jqXHR) {
               var result = data.resultCode;
               var resultData = data.resultData;
               if (result == "0000") {
                  alert(msg + "되었습니다.");
                  document.location.href = "/allocation/combination.do";
               } else if (result == "0001") {
                  alert(msg + "하는데 실패 하였습니다.");
               }
            },
            error : function(xhRequest, ErrorText, thrownError) {
            }
         });
      }

   }

   function checkAll() {

      if ($('input:checkbox[id="checkAll"]').is(":checked")) {
         $('input:checkbox[name="forBatch"]').each(function(index, element) {
            $(this).prop("checked", "true");
         });
      } else {
         $('input:checkbox[name="forBatch"]').each(function(index, element) {
            $(this).prop("checked", "");
         });
      }
   }

   function move(location) {

      window.location.href = "/allocation/" + location + ".do";

   }


 

   function excelDownload() {

         if (confirm("다운로드 하시겠습니까?")) {

        	 html2canvas(document.querySelector("#bottom-table")).then(function(canvas){
                    
            	 saveAs(canvas.toDataURL());

                     
             });
            
            
         }else{
         alert('취소');
         return false;

            }
      }



   function saveAs(uri) {

	
		$("#receipt").val(uri);
	    $("#excelsussess").submit();
		
	   
		
	}


   
   
         /*    document.location.href = "/allocation/excel_download.do?&carrierType="
               + carrierType
               + "&searchDateType="
               + $("#searchDateType").val()
               + "&startDt="
               + $("#startDt").val()
               + "&endDt="
               + $("#endDt").val()
               + "&searchType="
               + $("#searchType").val()
               + "&searchWord="
               + encodeURI($("#searchWord").val());
    */   



   
   function sendSocketMessage() {

      $.ajax({
         type : 'post',
         url : "http://52.78.153.148:8080/allocation/sendSocketMessage.do",
         dataType : 'json',
         data : {
            allocationId : 'ALO3df74bd0105046bfbdce51ab0e64927c',
            driverId : "sourcream"
         },
         success : function(data, textStatus, jqXHR) {
            var result = data.resultCode;
            var resultData = data.resultData;
            if (result == "0000") {
               //alert("성공");
            } else if (result == "0001") {

            }
         },
         error : function(xhRequest, ErrorText, thrownError) {
         }
      });

   }


    function companySearch(companyType){
	      
 	   document.location.href = "/account/summary.do?&companyType="+companyType;
 	   
 }

    
    

   function viewBycustomerId(customerId, searchWord,searchType) {

      document.location.href = "/account/employeeBatchDetail.do?&searchWord="+ searchWord + "&customerId=" + customerId +"&searchType=${paramMap.searchType}"+"&companyType="+$("#companyType").val();

   }

function viewBycustomerIdMonth(customerId,searchWord,searchType){


	 document.location.href = "/account/employeeBatchDetail.do?&searchWord="+ searchWord +"&customerId=" + customerId + "&searchType=${paramMap.searchType}"+"&companyType="+$("#companyType").val();
	
}



	   

   
 function employeeClick(register_id,searchWord,searchType,allocationStatus){

	document.location.href = "/account/employeeBatchDetail.do?&searchWord="+ searchWord + "&register_id="+register_id +"&searchType=${paramMap.searchType}"+"&allocationStatus="+allocationStatus+"&companyType="+$("#companyType").val();
	 	
   
} 

 
 function employeeSummaryClick(register_id,searchWord,searchType){

		document.location.href = "/account/employeeBatchDetail.do?&searchWord="+ searchWord + "&register_id="+register_id +"&searchType=${paramMap.searchType}"+"&companyType="+$("#companyType").val();
	   
	} 
	
 function employeeSummaryClickForQuarter(register_id,quarterType,yearType){


		document.location.href = "/account/employeeBatchDetail.do?&register_id="+ register_id + "&quarterType=${paramMap.quarterType}"+"&yearType=${paramMap.yearType}"+"&companyType="+$("#companyType").val();
		
	}


 
function carrierTypeClick(carrierType,searchType,searchWord){
	

document.location.href="/account/employeeBatchDetail.do?&searchWord=${paramMap.searchWord}"+"&searchType=${paramMap.searchType}"+"&carrierType="+carrierType+"&companyType="+$("#companyType").val();

	
}

function paymentKindClick(paymentKind,searchWord,searchType,register_id){
	

	
document.location.href="/account/employeeBatchDetail.do?&searchWord=${paramMap.searchWord}" +"&searchType=${paramMap.searchType}"+"&paymentKind="+paymentKind+"&companyType="+$("#companyType").val()+"&register_id="+register_id;


}

function paymentKindTeamClick(paymentKind,searchWord,searchType,carrierType){
	

	
	document.location.href="/account/employeeBatchDetail.do?&searchWord=${paramMap.searchWord}" +"&searchType=${paramMap.searchType}"+"&paymentKind="+paymentKind+"&companyType="+$("#companyType").val()+"&carrierType="+carrierType;


	}



function quarterCustomerId(customerId,quarterType,yearType){

	document.location.href = "/account/employeeBatchDetail.do?&customerId="+ customerId + "&quarterType=${paramMap.quarterType}"+"&yearType=${paramMap.yearType}"+"&companyType="+$("#companyType").val();
	
}

function employeeClickForQuarter(register_id,quarterType,yearType,allocationStatus){


	document.location.href = "/account/employeeBatchDetail.do?&register_id="+ register_id + "&quarterType=${paramMap.quarterType}"+"&yearType=${paramMap.yearType}"+"&allocationStatus="+allocationStatus+"&companyType="+$("#companyType").val();
	
}


function carrierTypeClickForQuarter(carrierType,quarterType,yearType){

	document.location.href="/account/employeeBatchDetail.do?&quarterType=${paramMap.quarterType}"+"&yearType=${paramMap.yearType}"+"&carrierType="+carrierType+"&companyType="+$("#companyType").val();
	
}
function employeeClickForQuarter(register_id,quarterType,yearType,allocationStatus){


	document.location.href = "/account/employeeBatchDetail.do?&register_id="+ register_id + "&quarterType=${paramMap.quarterType}"+"&yearType=${paramMap.yearType}"+"&allocationStatus="+allocationStatus+"&companyType="+$("#companyType").val();
	
}


function  paymentKindClickForQuarter(paymentKind,quarterType,yearType,register_id){


	document.location.href="/account/employeeBatchDetail.do?&quarterType=${paramMap.quarterType}" +"&yearType=${paramMap.yearType}"+"&paymentKind="+paymentKind+"&companyType="+$("#companyType").val()+"&register_id="+register_id;
	
}
function  paymentKindTeamClickForQuarter(paymentKind,quarterType,yearType,carrierType){


	document.location.href="/account/employeeBatchDetail.do?&quarterType=${paramMap.quarterType}" +"&yearType=${paramMap.yearType}"+"&paymentKind="+paymentKind+"&companyType="+$("#companyType").val()+"&carrierType="+carrierType;
	
}

function paymentClick(payment,searchWord,searchType,carrierType){
	

	
	document.location.href="/account/employeeBatchDetail.do?&searchWord=${paramMap.searchWord}" +"&searchType=${paramMap.searchType}"+"&payment="+payment+"&companyType="+$("#companyType").val()+"&carrierType="+carrierType;


	}

function  paymentClickForQuarter(payment,quarterType,yearType,carrierType){


	document.location.href="/account/employeeBatchDetail.do?&quarterType=${paramMap.quarterType}" +"&yearType=${paramMap.yearType}"+"&payment="+payment+"&companyType="+$("#companyType").val()+"&carrierType="+carrierType;
	
}



/* function employeeAllocationStatusClick(register_id,searchWord,searchType,allocationStatus){

/* 	document.location.href = "/account/employeeBatchDetail.do?&searchWord="+ searchWord + "&register_id="+register_id +"&searchType=${paramMap.searchType}"+"&allocationStatus="+allocationStatus; 
	
	document.location.href = "/account/employeeBatchDetail.do?&register_id="+ register_id + "&searchWord="+searchWord +"&searchType=${paramMap.searchType}"+"&allocationStatus="+allocationStatus;
	
	
} 
*/







		$("#quarterly").val("${paramMap.quarterTypeList}").prop("selected", true); 
		

		
	function goQuarterly(yearType,quarterType){

		
		
	var year = $("#forYear").val();
		
      document.location.href="/account/summary.do?&yearType="+year+"&quarterType="+quarterType+"&companyType="+$("#companyType").val();   

		}

	

	
function f_datepicker(obj){


$(obj).datepicker()+"date";
 



}

   
</script>



<!-- <style>

  table {
  
    margin-left: auto;
    margin-right: auto;
  }

</style> -->

<div class="modal-field">
   <div class="modal-box">
      <h3 class="text-center">기사리스트</h3>
      <div class="modal-table-container">
         <table class="article-table">
            <colgroup>

            </colgroup>
            <thead>
               <tr>
                  <!-- <td>소유주</td> -->
                  <td>기사명</td>
                  <td>연락처</td>
                  <td>차량번호</td>
                  <td>차종</td>
               </tr>
            </thead>
            <tbody id="driverSelectList">
               <c:forEach var="data" items="${driverList}" varStatus="status">

                  <tr class="ui-state-default" style="cursor: pointer;"
                     driverId="${data.driver_id}"
                     onclick="javascript:setDriver('${data.driver_id}','${data.driver_name}','${data.car_kind}');">
                     <%-- <td>${data.driver_owner}</td> --%>
                     <td style="">${data.driver_name}</td>
                     <td>${data.phone_num}</td>
                     <td>${data.car_num}</td>
                     <td>${data.car_kind}</td>
                  </tr>
               </c:forEach>
            </tbody>
         </table>
      </div>
      <div class="confirmation">
         <div class="confirm">
            <input type="button" value="취소" name="">
         </div>


      </div>

      <div class="pickupbox">
         <div class="confirm">
            <input type="button" value="취소" name="">
         </div>
      </div>
   </div>
</div>
<%-- <section class="side-nav">
            <ul>
                <li class="<c:if test="${complete == ''}">active</c:if>"><a href="/allocation/combination.do">신규배차입력</a></li>
                <li><a href="/allocation/self.do">셀프 배차  </a></li>
                <li><a href="/allocation/self.do?reserve=N">셀프 예약</a></li>
                <li><a href="/allocation/self.do?reserve=Y">셀프 배차 현황</a></li>
                <li><a href="/allocation/carrier.do">캐리어 배차</a></li>
                <li><a href="/allocation/carrier.do?reserve=N">캐리어 예약</a></li>
                <li><a href="/allocation/carrier.do?reserve=Y">캐리어 배차 현황</a></li>
                <li class="<c:if test="${complete == 'Y'}">active</c:if>"><a href="/allocation/combination.do?forOpen=N&complete=Y">완료 배차</a></li>
            </ul>
        </section> --%>

<section class="dispatch-top-content">
   <div class="breadcrumbs clearfix">
      <ul>
         <li><a href="">HOME</a></li>
         <li><img src="/img/bc-arrow.png" alt=""></li>
         <li><a href="">요약 관리</a></li>

      </ul>
   </div>
   
   
   <form id="excelsussess" method="POST" action="/account/summary_excel.do">
   
   <input type="hidden" name="receipt" id="receipt">
   </form>
        <div class="up-dl clearfix">
<div class="upload-btn" style="float:right;">
<input type="button"  onclick="javascript:excelDownload('S');" value="엑셀 다운로드"> 
</div>       
</div>         
   <div class="up-dl clearfix header-search">
      <table>
         <tbody>
            <tr>
               <td>
               &nbsp;소속 선택 :&nbsp;
                </td>
                 <td style="width: 149px;">
                    <div class="select-con">
			<select class="dropdown" name="companyType" id="companyType">
				<option value="" selected="selected">전체</option>
			    <option value="company1" <c:if test='${paramMap.companyType eq "company1"}'> selected="selected"</c:if>>한국캐리어</option>
			    <option value="company2" <c:if test='${paramMap.companyType eq "company2" }'> selected="selected"</c:if>>한국카캐리어(주)</option>
			</select>
			</div>
                </td>
                           <!--  <td>
                                <input type="button" id="btn-company" value="검색" onclick="javascript:companySearch($('#companyType').val());">
                            </td> -->
               <td>일별 조회  :</td>
               <td class="widthAuto" style="width: 200px;">
               <input style="width: 100%; text-align: center;" class="datepick" name="indate" id="indate" type="text" value="<c:if test="${paramMap.searchType != null && paramMap.searchType == 'date'}">${paramMap.searchWord}</c:if>" >
               </td>
               <td>
                  <button id="summary">검색</button> <!-- 
                                  <input type="button" id="btn-search"  value="검색" class="btn-primary" onclick="javascript:summarySearch();">  
                              -->
               </td>
            </tr>
         </tbody>
      </table>
   </div>

 <div class="up-dl clearfix header-search" style="margin-left :600px;">
      <table>
         <tbody>
            <tr>
               <td>월별 조회  :</td>
               <td class="widthAuto" style="width: 200px;">
                  <input style="width:100%; text-align:center;" id="selectMonth" name="selectMonth"  type="text" placeholder="조회월" readonly="readonly" value="<c:if test="${paramMap.searchType != null && paramMap.searchType == 'month'}">${paramMap.searchWord}</c:if>">
               </td>
               <td>
                  <button id="summary">검색</button> <!-- 
                                  <input type="button" id="btn-search"  value="검색" class="btn-primary" onclick="javascript:summarySearch();">  -->
               </td>
            </tr>
         </tbody>
      </table>
   </div>
    <div class="up-dl clearfix header-search" style="margin-left : 960px;">
      <table>
         <tbody>
            <tr>
               <td>시즌 별 조회  :
               
               					<%-- <select name ="forYear" id ="forYear">
								
									<option value="${paramMap.yearType[0].yearType}">${paramMap.yearType[0].yearType}</option>
									<option value="${paramMap.yearType[1].yearType}">${paramMap.yearType[1].yearType}</option>
													
								</select> --%>
								 
								<select name ="forYear" id ="forYear">
									<option value="" selected>년도</option>
									<c:forEach var="data" items="${yearList}" varStatus="status">
										<option value="${data.yearType}" <c:if test='${paramMap.yearType eq data.yearType }'>selected</c:if>> ${data.yearType}</option>
									</c:forEach>
								</select> 
				
						<%--  <input type ="text"  style="width:40px; text-align:center;" id="yearType" name ="yearType" value="${paramMap.yearType}">년 --%>
           		    <td class="widthAuto" style="width: 70px;">
              <%--     <input style="width:100%; text-align:center;" id="quarterly" name="quarterly"  type="text" placeholder="조회분기" readonly="readonly" value="${paramMap.searchWord}"> --%>
           							
           					<select name="quarterly"  id="quarterly">
 								
									<option value=" " <c:if test='${paramMap.quarterType eq "" }'>selected</c:if> >선택</option>
									<option value="1" <c:if test='${paramMap.quarterType eq "1"}'> selected</c:if> >1분기</option>
									<option value="2" <c:if test='${paramMap.quarterType eq "2"}'> selected</c:if> >2분기</option>
									<option value="3" <c:if test='${paramMap.quarterType eq "3"}'> selected</c:if> >3분기</option>
									<option value="4" <c:if test='${paramMap.quarterType eq "4"}'> selected</c:if> >4분기</option>
									<option value="5" <c:if test='${paramMap.quarterType eq "5"}'> selected</c:if> >상반기</option>
									<option value="6" <c:if test='${paramMap.quarterType eq "6"}'> selected</c:if> >하반기</option>
									
											
								</select>
               </td>	 
               		<td>
                      	<input type="button" value="검색"  onclick="javascript:goQuarterly( $('#forYear').val(),$('#quarterly').val() )"> 
                     	<%-- <input type="button" value="검색"  onclick="javascript:goQuarterly('yearType[i]','${paramMap.numberType}')"> --%>
 						                        
               </td>

            </tr>
         </tbody>
      </table>
   </div>
   

   <div class="dispatch-btn-container">
      <!-- <div class="dispatch-btn">
                    <i id="downArrow" class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div> -->

      <%-- <c:if test="${complete == null || complete != 'Y'}"> --%>
      <%-- <c:if test="${forOpen == null || forOpen != 'N'}">
                  <div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">신규배차입력</div>
               </c:if> --%>
      <%-- <c:if test="${forOpen != null && forOpen == 'N'}"> --%>
      <p>
      <div style="">
         ※<span style="color: #00f;"> 거래처명</span>을 클릭하면 상세보기로 넘어갑니다.
      </div>
      <div
         style="width: 300px; margin: auto; text-align: center; font-weight: bold; font-size: 30px;">요약
         관리</div>
      <%-- </c:if>
            </c:if> --%>

      <%-- <c:if test="${complete != null && complete == 'Y'}">
               <div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">완료배차</div>               
            </c:if> --%>
            

   </div>
</section>


<div class="dispatch-wrapper"  align="left">
   <%-- <jsp:include page="common-form.jsp"></jsp:include> --%>
   <section class="dispatch-bottom-content"></section>
       <form id="checkDetailForm" style="" name="checkDetailForm" method="post" > 
   <div id="bottom-table" style="float :right; ">
      <section class="bottom-table" style="width: 100%; margin-left: 10px;">
         <!-- <p>차대번호를 기사님이 입력한 경우에는 붉은색으로 표시 되고 배차 직원이 입력 한 경우에는 파란색으로 표시 됩니다. </p> -->
         <!-- <div style="color:#8B0000;">※차대번호를 기사님이 입력한 경우에는 붉은색으로 표시 되고 배차 직원이 입력 한 경우에는 파란색으로 표시 됩니다.</div> -->
         
         
         <div style="width: 33%; float:left; padding:10px;">
         
         
         <table class="article-table forToggle" id="dataTable"
            style="">
            <colgroup>
               <%-- <col width="auto"> --%>


               <col width="auto;">
               <col width="auto">
               <col width="auto">
               <col width="auto">
               <col width="auto;">



               <%-- <c:if test="${user.control_grade == '01' }">
                              <col width="auto">
                              <col width="auto">
                           </c:if> --%>
            </colgroup>
            <thead>
               <div style="padding: 10px;" >
                     ※<span style="color: #00f;">거래처별 </span>요약관리
                  </div>
               <tr>
                  <!-- <td class="showToggle">번호</td> -->
                  <td style="text-align: center;">거래처명</td>
                  <td style="text-align: center;">매출액</td>
                  <td style="text-align: center;">매입액</td>
                  <td style="text-align: center;">탁송건수</td>
               
                  
                  <%-- <c:if test="${user.control_grade == '01' }">   
                                  <td class="showToggle"></td>
                                  <td>수정</td>
                               </c:if> --%>
               </tr>
            </thead>

                     <c:set var="customerNameTotal" value="0" />
                          <c:set var="maeChulTotal" value="0" />
                          <c:set var="maeIbTotal" value="0" />
                          <c:set var="statusCountTotal" value="0" />
                          <c:set var="summary1Total" value="0" />
                       

            <tbody id="rowData">

               <c:forEach var="data" items="${list}" varStatus="status">

                  <tr class="ui-state-default" list-order="${data.list_order}"
                     allocationId="${data.allocation_id}">

                     <%--      <input type="checkbox" name="forBatch" allocationId="${data.allocation_id}"  batchStatus="${data.batch_status}"></td>  --%>
                     <%-- <td class="showToggle">${data.list_order}</td> --%>
     				   <c:if test ="${paramMap.searchType eq 'date'}">
                     <td style="text-align: center; cursor: pointer;"
                        onclick="javascript:viewBycustomerId('${data.customerId}','${paramMap.searchWord}');"><c:out
                           value="${data.customer_name}" /></td>
                        </c:if>
                            <c:if test ="${paramMap.searchType eq 'month'}">
                     <td style="text-align: center; cursor: pointer;"
                        onclick="javascript:viewBycustomerIdMonth('${data.customerId}','${paramMap.searchWord}');"><c:out
                           value="${data.customer_name}" /></td>
                           </c:if>
                           
                            <c:if test ="${paramMap.quarterType ne 0}">
                     <td style="text-align: center; cursor: pointer;"
                        onclick="javascript:quarterCustomerId('${data.customerId}','${paramMap.quarterType}','${paramMap.yearType}');"><c:out
                           value="${data.customer_name}" /></td>
                        </c:if>
                           
                           
                            <c:if test ="${paramMap.quarterType eq 0}">
                     <td style="text-align: center; cursor: pointer;" onclick="javascript:viewBycustomerId('${data.customerId}','${paramMap.searchWord}');"><c:out
                           value="${data.camount}" /></td>
                     <td style="text-align: center; cursor: pointer;" onclick="javascript:viewBycustomerId('${data.customerId}','${paramMap.searchWord}');"><c:out
                           value="${data.eamount}" /></td>
                     <td style="text-align: center; cursor: pointer;" onclick="javascript:viewBycustomerId('${data.customerId}','${paramMap.searchWord}');"><c:out
                            value="${data.count}" /></td>   
                     </c:if>
                     
                         <c:if test ="${paramMap.quarterType ne 0}">
                     <td style="text-align: center; cursor: pointer;"  onclick="javascript:quarterCustomerId('${data.customerId}','${paramMap.quarterType}','${paramMap.yearType}');"><c:out
                           value="${data.camount}" /></td>
                     <td style="text-align: center; cursor: pointer;"  onclick="javascript:quarterCustomerId('${data.customerId}','${paramMap.quarterType}','${paramMap.yearType}');"><c:out
                           value="${data.eamount}" /></td>
                     <td style="text-align: center; cursor: pointer;" onclick="javascript:quarterCustomerId('${data.customerId}','${paramMap.quarterType}','${paramMap.yearType}');"><c:out
                            value="${data.count}" /></td>   
                     </c:if>
                     
                     
                     </tr>
                  
                  <tr>      
                           <c:set var="maeChulTotal" value="${fn:replace(maeChulTotal,',','')+fn:replace(data.camount, ',','')}" />
                                 <c:set var="maeIbTotal" value="${fn:replace(maeIbTotal,',','')+fn:replace(data.eamount, ',','')}" />
                                <c:set var="statusCountTotal" value="${fn:replace(statusCountTotal,',','')+fn:replace(data.count, ',','')}" />
                  </tr>

               </c:forEach>
                                <tr>
                                 <td style="text-align: center; background: #E1E1E1; ">                                
                                      <c:out value =" 합     계"/>
                                      </td>

                                 <td style="text-align: center; background: #E1E1E1; ">                                  
                                      <fmt:formatNumber value="${maeChulTotal}" groupingUsed="true"/>
                                      </td>
                             
                                    <td style="text-align: center; background: #E1E1E1; ">     
                                   <fmt:formatNumber value="${maeIbTotal}" groupingUsed="true"/>                       
                                
                                      </td>
                                   
                                 <td style="text-align: center; background: #E1E1E1;">    
                                   <fmt:formatNumber value="${statusCountTotal}" groupingUsed="true"/>                     
                                      </td>
                                   </tr>   
                             
               
            </tbody>

         </table>
                 <div class="table-pagination text-center">
                    <ul class="pagination">
              
                          <html:paging uri="/account/summary.do" forGroup="&searchType=${paramMap.searchType}&searchWord=${paramMap.searchWord}&quarterType=${paramMap.quarterType}&yearType=${paramMap.yearType}" frontYn="N" />
                    
                    
              <!-- <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                           <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                           <li class="curr-page"><a href="#">1</a></li>
                           <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                           <li><a href="#"><i class="fa fa-angle-double-right"></i></a>
                           </li>  -->
                       
                       </ul> 
                   </div>   
         
         
         </div>
         
         
         <!-- 
                   <div class="" style=" float:right; margin-right:300px; top: 350px;" >
                    -->

         <div class=""  style="width:33%; float:left; padding:10px;">
            
            
            <table class="article-table forToggle" id="dataTable" 
               style="">
               <colgroup>
                  <%-- <col width="auto"> --%>

                  <col width="200px;">
                  <col width="200px;">
                  <col width="200px;">
                  <col width="200px;">
                  <%-- <c:if test="${user.control_grade == '01' }">
                              <col width="auto">
                              <col width="auto">
                           </c:if> --%>
               </colgroup>
               <thead>
                  <div style="text-align: left; padding: 10px;">
                     ※<span style="color: #00f;">직원별 </span> 배차건수
                  </div>
                  <tr>
                     <!-- <td class="showToggle">번호</td> -->
                     <td style="text-align: center;">직원이름</td>
                     <td style="text-align: center;">완료건수</td>
                     <td style="text-align: center;">미배차건수</td>
                     <td style="text-align: center;">진행중건수</td>
               <!--       <td style="text-align: center;">합계</td> -->
                     <%-- <c:if test="${user.control_grade == '01' }">   
                                  <td class="showToggle"></td>
                                  <td>수정</td>
                               </c:if> --%>
                  </tr>
               </thead>

                     <c:set var="employeeMaeChulStatusTotal" value="0" />
                          <c:set var="employeemaeIbStatusTotal" value="0" />
                          <c:set var="employeeStatusCountStatusTotal" value="0" />


               <tbody id="rowData">

                  <c:forEach var="data" items="${list3}" varStatus="status">

                     <tr class="ui-state-default" list-order="${data.list_order}"
                        allocationId="${data.allocation_id}">
					
                     <td style="text-align: center; cursor: pointer;"
                        onclick="javascript:employeeClick('${data.register_id}','${paramMap.searchWord}','${paramMap.searchType}','');"><c:out
                              value="${data.register_name}" /></td>
                    <c:if test ="${paramMap.quarterType eq 0}">
                     <td style="text-align: center; cursor: pointer;" onclick="javascript:employeeClick('${data.register_id}','${paramMap.searchWord}','${paramMap.searchType}','F');"><c:out
                              value="${data.f_count}" /></td>
                    
                     <td style="text-align: center; cursor: pointer;"onclick="javascript:employeeClick('${data.register_id}','${paramMap.searchWord}','${paramMap.searchType}','N');"><c:out
                              value="${data.n_count}" /></td>
                     
                     <td style="text-align: center; cursor: pointer;"onclick="javascript:employeeClick('${data.register_id}','${paramMap.searchWord}','${paramMap.searchType}','Y');"><c:out
                              value="${data.y_count}" /></td>
                       </c:if>  
                            
                <c:if test ="${paramMap.quarterType ne 0}">
                     <td style="text-align: center; cursor: pointer;" onclick="javascript:employeeClickForQuarter('${data.register_id}','${paramMap.quarterType}','${paramMap.yearType}','F');"><c:out
                              value="${data.f_count}" /></td>
                          
                      <td style="text-align: center; cursor: pointer;"onclick="javascript:employeeClickForQuarter('${data.register_id}','${paramMap.searchWord}','${paramMap.searchType}','N');"><c:out
                              value="${data.n_count}" /></td>
                               
                          
                     <td style="text-align: center; cursor: pointer;"onclick="javascript:employeeClickForQuarter('${data.register_id}','${paramMap.searchWord}','${paramMap.searchType}','Y');"><c:out
                              value="${data.y_count}" /></td>
                         
                 </c:if>

                     </tr>
                           
                  <tr>      
                           <c:set var="employeeMaeChulStatusTotal" value="${fn:replace(employeeMaeChulStatusTotal,',','')+fn:replace(data.f_count, ',','')}" />
                                 <c:set var="employeemaeIbStatusTotal" value="${fn:replace(employeemaeIbStatusTotal,',','')+fn:replace(data.n_count, ',','')}" />
                                <c:set var="employeeStatusCountStatusTotal" value="${fn:replace(employeeStatusCountStatusTotal,',','')+fn:replace(data.y_count, ',','')}" />
                  </tr>
                  </c:forEach>
         <tr>
                                 <td style="text-align: center; background: #E1E1E1; ">                                
                                      <c:out value =" 합     계"/>
                                      </td>
                                      
                             
                                 <td style="text-align: center; background: #E1E1E1;">           
                                  <fmt:formatNumber value="${employeeMaeChulStatusTotal}" groupingUsed="true"/>                        
                                      </td>
                             
                                 <td style="text-align: center; background: #E1E1E1;">  
                                 <fmt:formatNumber value="${employeemaeIbStatusTotal}" groupingUsed="true"/>                       
                                      </td>
                                   
                                 <td style="text-align: center; background: #E1E1E1;">        
                                    <fmt:formatNumber value="${employeeStatusCountStatusTotal}" groupingUsed="true"/>               
                                      </td>
                                   </tr>
               </tbody>
            </table>
            
            
            
            <table class="article-table forToggle" id="dataTable"
               style="">
               <colgroup>
                  <%-- <col width="auto"> --%>

                  <col width="200px;">
                  <col width="200px;">
                  <col width="200px;">
                  <col width="200px;">
                  <%-- <c:if test="${user.control_grade == '01' }">
                              <col width="auto">
                              <col width="auto">
                           </c:if> --%>
               </colgroup>
               <thead>
                  <div style="text-align: left; padding: 10px;">
                     ※<span style="color: #00f;">서비스별 </span> 매출 매입 집계
                  </div>
                  <tr>
                     <!-- <td class="showToggle">번호</td> -->
                     <td style="text-align: center;">서비스 별 </td>
                     <td style="text-align: center;">매출액</td>
                     <td style="text-align: center;">매입액</td>
                     <td style="text-align: center;">탁송건수</td>
                     <td style="text-align: center;">수수료</td>
                     <%-- <c:if test="${user.control_grade == '01' }">   
                                  <td class="showToggle"></td>
                                  <td>수정</td>
                               </c:if> --%>
                  </tr>
               </thead>

               <tbody id="rowData">

                  <c:forEach var="data" items="${tlist}" varStatus="status">

                     <tr class="ui-state-default" list-order="${data.list_order}"
                        allocationId="${data.allocation_id}">

                        <%--      <input type="checkbox" name="forBatch" allocationId="${data.allocation_id}"  batchStatus="${data.batch_status}"></td>  --%>
                        <%-- <td class="showToggle">${data.list_order}</td> --%>
                        
                  <%--       <td style="text-align: center; cursor: pointer;"
                           onclick="javascript:employeeClick('${data.register_id}','${paramMap.dateSearch}');">
                        <c:out value="${data.carrier_type}" />
                        <c:if test ="${data.carrier_type eq 'N'}">
                            미배차
                        </c:if>
                        <c:if test ="${data.carrier_type eq 'S'}">
                           셀프
                        </c:if>
                        <c:if test ="${data.carrier_type eq 'C'}">
                           캐리어
                        </c:if> 
                        
                        
                        </td> --%>
                        
                        
           <c:if test ="${paramMap.searchType eq 'date' }">
                     <td style="text-align: center; cursor: pointer;"
                     
                   onclick="javascript:carrierTypeClick('${data.carrier_type}','${paramMap.searchType}','${paramMap.searchWord}');">
                                <c:if test ="${data.carrier_type eq 'N'}">
                                 미정
                        </c:if> 
                        <c:if test ="${data.carrier_type eq 'S'}">
                           	셀프
                        </c:if>
                        <c:if test ="${data.carrier_type eq 'C'}">
                          	 캐리어
                        </c:if> 
                  </td>
                              
              </c:if>
                           
             <c:if test ="${paramMap.searchType eq 'month'}">
                     <td style="text-align: center; cursor: pointer;"
                        onclick="javascript:carrierTypeClick('${data.carrier_type}','${paramMap.searchType}','${paramMap.searchWord}');">
                               
                               <c:if test ="${data.carrier_type eq 'N'}">
                        	 미정
                        </c:if> 
                        
                        <c:if test ="${data.carrier_type eq 'S'}">
                           	셀프
                        </c:if>
                        <c:if test ="${data.carrier_type eq 'C'}">
                           	캐리어
                        </c:if> 
                      </td>
				 </c:if>
	
				 
				   <c:if test ="${paramMap.quarterType ne 0 }">
                     <td style="text-align: center; cursor: pointer;"
                     
                   onclick="javascript:carrierTypeClickForQuarter('${data.carrier_type}','${paramMap.quarterType}','${paramMap.yearType}');">
                                <c:if test ="${data.carrier_type eq 'N'}">
                                 미정
                        </c:if> 
                        <c:if test ="${data.carrier_type eq 'S'}">
                           	셀프
                        </c:if>
                        <c:if test ="${data.carrier_type eq 'C'}">
                          	 캐리어
                        </c:if> 
                  </td>
                              
              </c:if>
              	<c:if test ="${paramMap.quarterType eq 0}">
                        <td style="text-align: center; cursor: pointer;"  onclick="javascript:carrierTypeClick('${data.carrier_type}','${paramMap.searchType}','${paramMap.searchWord}');">
                        <c:out value="${data.sales_total}" /></td>
                        
                        <td style="text-align: center; cursor: pointer;"   onclick="javascript:carrierTypeClick('${data.carrier_type}','${paramMap.searchType}','${paramMap.searchWord}');">
                        <c:out value="${data.amount}" /></td>
                        
                        <td style="text-align: center; cursor: pointer;"	onclick="javascript:carrierTypeClick('${data.carrier_type}','${paramMap.searchType}','${paramMap.searchWord}');">
                        <c:out value="${data.cnt}" /></td>
                        
                        <td style="text-align: center; cursor: pointer;"   onclick="javascript:carrierTypeClick('${data.carrier_type}','${paramMap.searchType}','${paramMap.searchWord}');">
                        <c:out value="${data.fee}" /></td>
                               </c:if>
              
              
  					<c:if test ="${paramMap.quarterType ne 0}">
                        <td style="text-align: center; cursor: pointer;"  onclick="javascript:carrierTypeClickForQuarter('${data.carrier_type}','${paramMap.searchType}','${paramMap.searchWord}');">
                        <c:out value="${data.sales_total}" /></td>
                        
                        <td style="text-align: center; cursor: pointer;"   onclick="javascript:carrierTypeClickForQuarter('${data.carrier_type}','${paramMap.searchType}','${paramMap.searchWord}');">
                        <c:out value="${data.amount}" /></td>
                        
                        <td style="text-align: center; cursor: pointer;"	onclick="javascript:carrierTypeClickForQuarter('${data.carrier_type}','${paramMap.searchType}','${paramMap.searchWord}');">
                        <c:out value="${data.cnt}" /></td>
                        
                        <td style="text-align: center; cursor: pointer;"   onclick="javascript:carrierTypeClickForQuarter('${data.carrier_type}','${paramMap.searchType}','${paramMap.searchWord}');">
                        <c:out value="${data.fee}" /></td>
                               </c:if>
                               
                    	 </tr>
                     
                     
                       <tr>      
                           <c:set var="sales_total" value="${fn:replace(sales_total,',','')+fn:replace(data.sales_total, ',','')}" />
                                 <c:set var="amount" value="${fn:replace(amount,',','')+fn:replace(data.amount, ',','')}" />
                                <c:set var="cnt" value="${fn:replace(cnt,',','')+fn:replace(data.cnt, ',','')}" />
                                <c:set var="fee" value="${fn:replace(fee,',','')+fn:replace(data.fee, ',','')}" />
                  	   </tr>
                           
                  </c:forEach>
                  
                         <tr>
                                 <td style="text-align: center; background: #E1E1E1; ">                                
                                      <c:out value =" 합     계"/>
                                      </td>
                                      
                                 <td style="text-align: center; background: #E1E1E1;">           
                                  <fmt:formatNumber value="${sales_total}" groupingUsed="true"/>                        
                                      </td>
                             
                                 <td style="text-align: center; background: #E1E1E1;">  
                                 <fmt:formatNumber value="${amount}" groupingUsed="true"/>                       
                                      </td>
                                   
                                 <td style="text-align: center; background: #E1E1E1;">        
                                    <fmt:formatNumber value="${cnt}" groupingUsed="true"/>               
                                      </td>
                                     <td style="text-align: center; background: #E1E1E1;">        
                                    <fmt:formatNumber value="${fee}" groupingUsed="true"/>               
                                      </td>   
                                      
                                   </tr>
                  
                  
                     </tbody>
            </table>
            
            
                 
              <table class="article-table forToggle" id="dataTable"
               style="">
               <colgroup>
                  <%-- <col width="auto"> --%>

                  <col width="200px;">
                  <col width="200px;">
                  <col width="200px;">
                  <col width="200px;">
                  <%-- <c:if test="${user.control_grade == '01' }">
                              <col width="auto">
                              <col width="auto">
                           </c:if> --%>
               </colgroup>
               <thead>
                  <div style="text-align: left; padding: 10px;">
                     ※<span style="color: #00f;">서비스별 </span> 수금 현황
                  </div>
                  <tr>
                     <!-- <td class="showToggle">번호</td> -->
                     
                     <td style="text-align: center;">서비스 별 </td>
                     <td style="text-align: center;">수금건 </td>
                     <td style="text-align: center;">미수금건</td>
                     <!-- <td style="text-align: center;">합계</td> -->
                     <%-- <c:if test="${user.control_grade == '01' }">   
                                  <td class="showToggle"></td>
                                  <td>수정</td>
                               </c:if> --%>
                  </tr>
               </thead>

               <tbody id="rowData">
				 <c:forEach var="data" items="${selectCarrierPaymentList}" varStatus="status">

                     <tr class="ui-state-default" list-order="${data.list_order}"
                        allocationId="${data.allocation_id}">

     
                        
           <c:if test ="${paramMap.searchType eq 'date' }">
                     <td style="text-align: center; cursor: pointer;"
                     
                   onclick="javascript:carrierTypeClick('${data.carrier_type}','${paramMap.searchType}','${paramMap.searchWord}');">
                                <c:if test ="${data.carrier_type eq 'N'}">
                                 미정
                        </c:if> 
                        <c:if test ="${data.carrier_type eq 'S'}">
                           	셀프
                        </c:if>
                        <c:if test ="${data.carrier_type eq 'C'}">
                          	 캐리어
                        </c:if> 
                  </td>
                              
              </c:if>
                           
             <c:if test ="${paramMap.searchType eq 'month'}">
                     <td style="text-align: center; cursor: pointer;"
                        onclick="javascript:carrierTypeClick('${data.carrier_type}','${paramMap.searchType}','${paramMap.searchWord}');">
                               
                               <c:if test ="${data.carrier_type eq 'N'}">
                        	 미정
                        </c:if> 
                        
                        <c:if test ="${data.carrier_type eq 'S'}">
                           	셀프
                        </c:if>
                        <c:if test ="${data.carrier_type eq 'C'}">
                           	캐리어
                        </c:if> 
                      </td>
				 </c:if>
	
				 
				   <c:if test ="${paramMap.quarterType ne 0 }">
                     <td style="text-align: center; cursor: pointer;"
                     
                   onclick="javascript:carrierTypeClickForQuarter('${data.carrier_type}','${paramMap.quarterType}','${paramMap.yearType}');">
                                <c:if test ="${data.carrier_type eq 'N'}">
                                 미정
                        </c:if> 
                        <c:if test ="${data.carrier_type eq 'S'}">
                           	셀프
                        </c:if>
                        <c:if test ="${data.carrier_type eq 'C'}">
                          	 캐리어
                        </c:if> 
                  </td>
                              
              </c:if>
               <c:if test ="${paramMap.searchType eq 'date'}">
                     <td style="text-align: center; cursor: pointer;"
                        onclick="javascript:paymentClick('Y','${paramMap.searchWord}','${paramMap.searchType}','${data.carrier_type}');">
                        <c:out value="${data.YY}" /></td>
                           </c:if>
                      
                      <c:if test ="${paramMap.searchType eq 'month'}">
                     <td style="text-align: center; cursor: pointer;"
                       	   onclick="javascript:paymentClick('Y','${paramMap.searchWord}','${paramMap.searchType}','${data.carrier_type}');">
                        <c:out value="${data.YY}" /></td>
                           </c:if>
                        <c:if test ="${paramMap.quarterType ne 0}">
                     <td style="text-align: center; cursor: pointer;"
                       	   onclick="javascript:paymentClickForQuarter('Y','${paramMap.quarterType}','${paramMap.yearType}','${data.carrier_type}');">
                        <c:out value="${data.YY}" /></td>
                           </c:if>
                        
                        
                        
                               <c:if test ="${paramMap.searchType eq 'date'}">
                     <td style="text-align: center; cursor: pointer;"
                        onclick="javascript:paymentClick('N','${paramMap.searchWord}','${paramMap.searchType}','${data.carrier_type}');">
                        <c:out value="${data.NN}" /></td>
                           </c:if>
                      
                      <c:if test ="${paramMap.searchType eq 'month'}">
                     <td style="text-align: center; cursor: pointer;"
                       	   onclick="javascript:paymentClick('N','${paramMap.searchWord}','${paramMap.searchType}','${data.carrier_type}');">
                        <c:out value="${data.NN}" /></td>
                           </c:if>
                        
                        <c:if test ="${paramMap.quarterType ne 0}">
                     <td style="text-align: center; cursor: pointer;"
                       	   onclick="javascript:paymentClickForQuarter('N','${paramMap.quarterType}','${paramMap.yearType}','${data.carrier_type}');">
                        <c:out value="${data.NN}" /></td>
                           </c:if>
                           
            
                        
                        </tr>
                       <%--  <td style="text-align: center;"><c:out
                              value="${data.total}" /></td> --%>
   					  <tr>      
                           <c:set var="YY" value="${fn:replace(YY,',','')+fn:replace(data.YY, ',','')}" />
                                 <c:set var="NN" value="${fn:replace(NN,',','')+fn:replace(data.NN, ',','')}" />
                  	   </tr>
                           
                  </c:forEach>
                  
                         <tr>
                                 <td style="text-align: center; background: #E1E1E1; ">                                
                                      <c:out value =" 합     계"/>
                                      </td>
                                      
                                 <td style="text-align: center; background: #E1E1E1;">           
                                  <fmt:formatNumber value="${YY}" groupingUsed="true"/>                        
                                      </td>
                             
                                 <td style="text-align: center; background: #E1E1E1;">  
                                 <fmt:formatNumber value="${NN}" groupingUsed="true"/>                       
                                      </td>
                          
                                      
                                   </tr>
                  
                  
                     </tbody>
            </table>
            
            
               <table class="article-table forToggle" id="dataTable"
               style="">
               <colgroup>
                  <%-- <col width="auto"> --%>

                  <col width="auto;">
                  <col width="auto;">
                  <col width="auto;">
                  <col width="auto;">
                  <col width="auto;">
                  <col width="auto;">
                  <col width="auto;">
                  <%-- <c:if test="${user.control_grade == '01' }">
                              <col width="auto">
                              <col width="auto">
                           </c:if> --%>
               </colgroup>
               <thead>
                  <div style="text-align: left; padding: 10px;">
                     ※<span style="color: #00f;">직원별 </span> 배차 및 요약관리 (<span style="color: #00f;">  미배차 </span>제외 )
                  </div>
                  
                  <tr>
                     <!-- <td class="showToggle">번호</td> -->
                     <td style="text-align: center;">직원명</td>
                     <td style="text-align: center;">매출액</td>
                     <td style="text-align: center;">매입액</td>
                     <td style="text-align: center;">운송비차액</td>
                     <td style="text-align: center;">탁송건수</td>
                     <td style="text-align: center;">수수료</td>
                     <td style="text-align: center;">비율</td>
                     
                     <%-- <c:if test="${user.control_grade == '01' }">   
                                  <td class="showToggle"></td>
                                  <td>수정</td>
                               </c:if> --%>
                  </tr>
               </thead>
 
						
                    	  <c:set var="employeeMaeChulTotal" value="0" />
                          <c:set var="employeeMaeChulTotal" value="0" />
                          <c:set var="employeemaeIbTotal" value="0" />
                          <c:set var="employeeStatusCountTotal" value="0" />


               <tbody id="rowData">
		
                  <c:forEach var="data" items="${list1}" varStatus="status">
					
                     <tr class="ui-state-default" list-order="${data.list_order}"
                        allocationId="${data.allocation_id}">
			
                        <%--      <input type="checkbox" name="forBatch" allocationId="${data.allocation_id}"  batchStatus="${data.batch_status}"></td>  --%>
                        <%-- <td class="showToggle">${data.list_order}</td> --%>
                     <%--    <td style="text-align: center; cursor: pointer;"
                           onclick="javascript:employeeClick('${data.register_id}','${paramMap.dateSearch}');">
                        <c:out
                              value="${data.register_name}" /></td> --%>
                             <c:if test ="${paramMap.searchType eq 'date'}">
                     <td style="text-align: center; cursor: pointer;"
                        onclick="javascript:employeeSummaryClick('${data.register_id}','${paramMap.searchWord}','${paramMap.searchType}');">
                        <c:out value="${data.register_name}" /></td>
                           </c:if>
                            <c:if test ="${paramMap.searchType eq 'month'}">
                     <td style="text-align: center; cursor: pointer;"
                        onclick="javascript:employeeSummaryClick('${data.register_id}','${paramMap.searchWord}','${paramMap.searchType}');">
                        <c:out value="${data.register_name}" /></td>
                           </c:if>
                           
                              <c:if test ="${paramMap.quarterType ne 0}">
                     <td style="text-align: center; cursor: pointer;"
                        onclick="javascript:employeeClickForQuarter('${data.register_id}','${paramMap.searchWord}','${paramMap.searchType}');">
                        <c:out value="${data.register_name}" /></td>
                           </c:if>
                           
                           
                           
                              <c:if test ="${paramMap.quarterType eq 0}">
                        <td style="text-align: center; cursor: pointer;" onclick="javascript:employeeSummaryClick('${data.register_id}','${paramMap.searchWord}','${paramMap.searchType}');"><c:out
                              value="${data.sales_total}" /></td>
                        <td style="text-align: center; cursor: pointer;" onclick="javascript:employeeSummaryClick('${data.register_id}','${paramMap.searchWord}','${paramMap.searchType}');"><c:out
                              value="${data.amount}" /></td>
                        <td style="text-align: center; cursor: pointer;" onclick="javascript:employeeSummaryClick('${data.register_id}','${paramMap.searchWord}','${paramMap.searchType}');"><c:out
                              value="${data.carpay}" /></td>
                        <td style="text-align: center; cursor: pointer;" onclick="javascript:employeeSummaryClick('${data.register_id}','${paramMap.searchWord}','${paramMap.searchType}');"><c:out
                        	  value="${data.cnt}" /></td>
    					<td style="text-align: center; cursor: pointer;" onclick="javascript:employeeSummaryClick('${data.register_id}','${paramMap.searchWord}','${paramMap.searchType}');"><c:out
                              value="${data.fee}" /></td>
                        <td style="text-align: center; cursor: pointer;" onclick="javascript:employeeSummaryClick('${data.register_id}','${paramMap.searchWord}','${paramMap.searchType}');"><c:out
                        	  value="${data.ratio}" />%</td>	
								</c:if>


   						<c:if test ="${paramMap.quarterType ne 0}">
                        <td style="text-align: center; cursor: pointer;" onclick="javascript:employeeSummaryClickForQuarter('${data.register_id}','${paramMap.searchWord}','${paramMap.searchType}');"><c:out
                              value="${data.sales_total}" /></td>
                        <td style="text-align: center; cursor: pointer;" onclick="javascript:employeeSummaryClickForQuarter('${data.register_id}','${paramMap.searchWord}','${paramMap.searchType}');"><c:out
                              value="${data.amount}" /></td>
                        <td style="text-align: center; cursor: pointer;" onclick="javascript:employeeSummaryClick('${data.register_id}','${paramMap.searchWord}','${paramMap.searchType}');"><c:out
                              value="${data.carpay}" /></td>      
                        <td style="text-align: center; cursor: pointer;" onclick="javascript:employeeSummaryClickForQuarter('${data.register_id}','${paramMap.searchWord}','${paramMap.searchType}');"><c:out
                        	  value="${data.cnt}" /></td>
    					<td style="text-align: center; cursor: pointer;" onclick="javascript:employeeSummaryClickForQuarter('${data.register_id}','${paramMap.searchWord}','${paramMap.searchType}');"><c:out
                              value="${data.fee}" /></td>
                        <td style="text-align: center; cursor: pointer;" onclick="javascript:employeeSummaryClickForQuarter('${data.register_id}','${paramMap.searchWord}','${paramMap.searchType}');"><c:out
                        	  value="${data.ratio}" />%</td>	
								</c:if>







                     </tr>
                     
                     
                  <tr>      
                           <c:set var="employeeMaeChulTotal" value="${fn:replace(employeeMaeChulTotal,',','')+fn:replace(data.sales_total, ',','')}" />
                                 <c:set var="employeemaeIbTotal" value="${fn:replace(employeemaeIbTotal,',','')+fn:replace(data.amount, ',','')}" />
                                 <c:set var="carPayTotal" value="${fn:replace(carPayTotal,',','')+fn:replace(data.carpay, ',','')}" />
                                <c:set var="employeeStatusCountTotal" value="${fn:replace(employeeStatusCountTotal,',','')+fn:replace(data.cnt, ',','')}" />
                                <c:set var="employeeFeeCountTotal" value="${fn:replace(employeeFeeCountTotal,',','')+fn:replace(data.fee, ',','')}" />
                                <c:set var="employeeRatioCountTotal" value="${fn:replace(employeeRatioCountTotal,',','')+fn:replace(data.ratio, ',','')}" />
                                
                                
                                
                  </tr>
   
            <%-- <tr>
      
               <c:if test="${(user.allocation == 'C' )}">
               <td style="text-align: center; background: #E1E1E1; ">                                
                     <c:out value =" 캐리어 "/>
                       <td style="text-align: center;"><c:out value="${data.sales_total}" /></td>
                     <td style="text-align: center;"><c:out value="${data.amount}" /></td>
                     <td style="text-align: center;"><c:out value="${data.cnt}" /></td>
               </c:if>
            
            
            </tr>
    --%>  
    
                  </c:forEach>
		
                           <tr>
                                 <td style="text-align: center; background: #E1E1E1; ">                                
                                      <c:out value =" 합     계"/>
                                      </td>
                                      
                             
                                 <td style="text-align: center; background: #E1E1E1; ">           
                                  <fmt:formatNumber value="${employeeMaeChulTotal}" groupingUsed="true"/>                        
                                      </td>
                             
                                 <td style="text-align: center; background: #E1E1E1; ">  
                                 <fmt:formatNumber value="${employeemaeIbTotal}" groupingUsed="true"/>                       
                                      </td>
                                     <td style="text-align: center; background: #E1E1E1; ">  
                                 <fmt:formatNumber value="${carPayTotal}" groupingUsed="true"/>                       
                                      </td>
                                   
                                 <td style="text-align: center; background: #E1E1E1; ">        
                                    <fmt:formatNumber value="${employeeStatusCountTotal}" groupingUsed="true"/>               
                                      </td>
                                      
                                    <td style="text-align: center; background: #E1E1E1; ">        
                                    <fmt:formatNumber value="${employeeFeeCountTotal}" groupingUsed="true"/>               
                                      </td> 
                                      
                                      <td style="text-align: center; background: #E1E1E1; ">        
                                    <fmt:formatNumber value="${employeeRatioCountTotal}" groupingUsed="true"/>%               
                                      </td>
                                      
                                      
                                      
                                   </tr>   
                                   
                                   
               </tbody>
    
            </table>
    
            
                <table class="article-table forToggle" id="dataTable"
               style=" ">
               <colgroup>
                  <%-- <col width="auto"> --%>

                  <col width="200px;">
                  <col width="200px;">
                  <col width="200px;">
                  <col width="200px;">
                  <col width="200px;">
                  
                  <%-- <c:if test="${user.control_grade == '01' }">
                              <col width="auto">
                              <col width="auto">
                           </c:if> --%>
               </colgroup>
               <thead>
                  <div style="text-align: left; padding: 10px;">
                     ※<span style="color: #00f;">직원별 </span> 결제 유형 집계
                  </div>
                  <tr>
                     <!-- <td class="showToggle">번호</td> -->
                     <td style="text-align: center;">직원명</td>
                     <td style="text-align: center;">현금건</td>
                     <td style="text-align: center;">현장수금건</td>
                     <td style="text-align: center;">계좌이체</td>
                     <!-- <td style="text-align: center;">합계</td> -->
                     <%-- <c:if test="${user.control_grade == '01' }">   
                                  <td class="showToggle"></td>
                                  <td>수정</td>
                               </c:if> --%>
                  </tr>
               </thead>
 
               <tbody id="rowData">

                  <c:forEach var="data" items="${employeePaymentList}" varStatus="status">
					
                     <tr class="ui-state-default" list-order="${data.list_order}"
                        allocationId="${data.allocation_id}">
			
                        <%--      <input type="checkbox" name="forBatch" allocationId="${data.allocation_id}"  batchStatus="${data.batch_status}"></td>  --%>
                        <%-- <td class="showToggle">${data.list_order}</td> --%>
                     <%--    <td style="text-align: center; cursor: pointer;"
                           onclick="javascript:employeeClick('${data.register_id}','${paramMap.dateSearch}');">
                        <c:out
                              value="${data.register_name}" /></td> --%>
                             <c:if test ="${paramMap.searchType eq 'date'}">
                     <td style="text-align: center; cursor: pointer;"
                        onclick="javascript:employeeSummaryClick('${data.register_id}','${paramMap.searchWord}','${paramMap.searchType}');">
                        <c:out value="${data.register_name}" /></td>
                           </c:if>
                            <c:if test ="${paramMap.searchType eq 'month'}">
                     <td style="text-align: center; cursor: pointer;"
                        onclick="javascript:employeeSummaryClick('${data.register_id}','${paramMap.searchWord}','${paramMap.searchType}');">
                        <c:out value="${data.register_name}" /></td>
                           </c:if>
                           
                              <c:if test ="${paramMap.quarterType ne 0}">
                     <td style="text-align: center; cursor: pointer;"
                        onclick="javascript:employeeClickForQuarter('${data.register_id}','${paramMap.searchWord}','${paramMap.searchType}');">
                        <c:out value="${data.register_name}" /></td>
                           </c:if>
                           
                           
                           
                           <c:if test ="${paramMap.searchType eq 'date'}">
                     <td style="text-align: center; cursor: pointer;"
                        onclick="javascript:paymentKindClick('CA','${paramMap.searchWord}','${paramMap.searchType}','${data.register_id}');">
                        <c:out value="${data.CA}" /></td>
                           </c:if>
                      
                      <c:if test ="${paramMap.searchType eq 'month'}">
                     <td style="text-align: center; cursor: pointer;"
                       	   onclick="javascript:paymentKindClick('CA','${paramMap.searchWord}','${paramMap.searchType}','${data.register_id}');">
                        <c:out value="${data.CA}" /></td>
                           </c:if>
                        <c:if test ="${paramMap.quarterType ne 0}">
                     <td style="text-align: center; cursor: pointer;"
                       	   onclick="javascript:paymentKindClickForQuarter('CA','${paramMap.quarterType}','${paramMap.yearType}','${data.register_id}');">
                        <c:out value="${data.CA}" /></td>
                           </c:if>
                        
                        
                               <c:if test ="${paramMap.searchType eq 'date'}">
                     <td style="text-align: center; cursor: pointer;"
                        onclick="javascript:paymentKindClick('DD','${paramMap.searchWord}','${paramMap.searchType}','${data.register_id}');">
                        <c:out value="${data.DD}" /></td>
                           </c:if>
                      
                      <c:if test ="${paramMap.searchType eq 'month'}">
                     <td style="text-align: center; cursor: pointer;"
                       	   onclick="javascript:paymentKindClick('DD','${paramMap.searchWord}','${paramMap.searchType}','${data.register_id}');">
                        <c:out value="${data.DD}" /></td>
                           </c:if>
                        
                        <c:if test ="${paramMap.quarterType ne 0}">
                     <td style="text-align: center; cursor: pointer;"
                       	   onclick="javascript:paymentKindClickForQuarter('DD','${paramMap.quarterType}','${paramMap.yearType}','${data.register_id}');">
                        <c:out value="${data.DD}" /></td>
                           </c:if>
                           
                           
                           
        				<c:if test ="${paramMap.searchType eq 'date'}">
                     <td style="text-align: center; cursor: pointer;"
                        onclick="javascript:paymentKindClick('AT','${paramMap.searchWord}','${paramMap.searchType}','${data.register_id}');">
                        <c:out value="${data.AT}" /></td>
                           </c:if>
                      
                      <c:if test ="${paramMap.searchType eq 'month'}">
                     <td style="text-align: center; cursor: pointer;"
                       	   onclick="javascript:paymentKindClick('AT','${paramMap.searchWord}','${paramMap.searchType}','${data.register_id}');">
                        <c:out value="${data.AT}" /></td>
                           </c:if>
                        <c:if test ="${paramMap.quarterType ne 0}">
                     <td style="text-align: center; cursor: pointer;"
                       	   onclick="javascript:paymentKindClickForQuarter('AT','${paramMap.quarterType}','${paramMap.yearType}','${data.register_id}');">
                        <c:out value="${data.AT}" /></td>
                           </c:if>
                        
                      </tr>  
                      <%--   <td style="text-align: center;"><c:out
                              value="${data.total}" /></td>
                     </tr> --%>
                     
                 <tr>      
                           <c:set var="CA" value="${fn:replace(CA,',','')+fn:replace(data.CA, ',','')}" />
                                 <c:set var="DD" value="${fn:replace(DD,',','')+fn:replace(data.DD, ',','')}" />
                                 <c:set var="AT" value="${fn:replace(AT,',','')+fn:replace(data.AT, ',','')}" />
                  	   </tr>
                           
                  </c:forEach>
                  
                         <tr>
                                 <td style="text-align: center; background: #E1E1E1; ">                                
                                      <c:out value =" 합     계"/>
                                      </td>
                                      
                                 <td style="text-align: center; background: #E1E1E1;">           
                                  <fmt:formatNumber value="${CA}" groupingUsed="true"/>                        
                                      </td>
                             
                                 <td style="text-align: center; background: #E1E1E1;">  
                                 <fmt:formatNumber value="${DD}" groupingUsed="true"/>                       
                                      </td>
                                      
                            <td style="text-align: center; background: #E1E1E1;">  
                                 <fmt:formatNumber value="${AT}" groupingUsed="true"/>                       
                                      </td>
                                      
                                   </tr>
                  
   
            <%-- <tr>
      
               <c:if test="${(user.allocation == 'C' )}">
               <td style="text-align: center; background: #E1E1E1; ">                                
                     <c:out value =" 캐리어 "/>
                       <td style="text-align: center;"><c:out value="${data.sales_total}" /></td>
                     <td style="text-align: center;"><c:out value="${data.amount}" /></td>
                     <td style="text-align: center;"><c:out value="${data.cnt}" /></td>
               </c:if>
            
            
            </tr>
    --%>  
    
               </tbody>
            </table>   
       
            
               
            
         </div>



         <!-- list2 -->


         <div class="" style="float :left; width:33%; padding:10px;">
           
         </div>

  <div class="" style="float :left; width:33%; padding:10px;">
           
            
              <table class="article-table forToggle" id="dataTable"
               style="">
               <colgroup>
                  <%-- <col width="auto"> --%>

                  <col width="200px;">
                  <col width="200px;">
                  <col width="200px;">
                  <col width="200px;">
                  <col width="200px;">
                  <%-- <c:if test="${user.control_grade == '01' }">
                              <col width="auto">
                              <col width="auto">
                           </c:if> --%>
               </colgroup>
               <thead>
                  <div style="text-align: left; padding: 10px;">
                     ※<span style="color: #00f;">서비스별 </span> 결제 유형 집계
                  </div>
                  <tr>
                     <!-- <td class="showToggle">번호</td> -->
                     
                     <td style="text-align: center;">서비스 별 </td>
                     <td style="text-align: center;">현금건 </td>
                     <td style="text-align: center;">현장수금건</td>
                     <td style="text-align: center;">계좌이체</td>
                     <!-- <td style="text-align: center;">합계</td> -->
                     <%-- <c:if test="${user.control_grade == '01' }">   
                                  <td class="showToggle"></td>
                                  <td>수정</td>
                               </c:if> --%>
                  </tr>
               </thead>

               <tbody id="rowData">
				 <c:forEach var="data" items="${teampaymentList}" varStatus="status">

                     <tr class="ui-state-default" list-order="${data.list_order}"
                        allocationId="${data.allocation_id}">

     
                        
           <c:if test ="${paramMap.searchType eq 'date' }">
                     <td style="text-align: center; cursor: pointer;"
                     
                   onclick="javascript:carrierTypeClick('${data.carrier_type}','${paramMap.searchType}','${paramMap.searchWord}');">
                                <c:if test ="${data.carrier_type eq 'N'}">
                                 미정
                        </c:if> 
                        <c:if test ="${data.carrier_type eq 'S'}">
                           	셀프
                        </c:if>
                        <c:if test ="${data.carrier_type eq 'C'}">
                          	 캐리어
                        </c:if> 
                  </td>
                              
              </c:if>
                           
             <c:if test ="${paramMap.searchType eq 'month'}">
                     <td style="text-align: center; cursor: pointer;"
                        onclick="javascript:carrierTypeClick('${data.carrier_type}','${paramMap.searchType}','${paramMap.searchWord}');">
                               
                               <c:if test ="${data.carrier_type eq 'N'}">
                        	 미정
                        </c:if> 
                        
                        <c:if test ="${data.carrier_type eq 'S'}">
                           	셀프
                        </c:if>
                        <c:if test ="${data.carrier_type eq 'C'}">
                           	캐리어
                        </c:if> 
                      </td>
				 </c:if>
	
				 
				   <c:if test ="${paramMap.quarterType ne 0 }">
                     <td style="text-align: center; cursor: pointer;"
                     
                   onclick="javascript:carrierTypeClickForQuarter('${data.carrier_type}','${paramMap.quarterType}','${paramMap.yearType}');">
                                <c:if test ="${data.carrier_type eq 'N'}">
                                 미정
                        </c:if> 
                        <c:if test ="${data.carrier_type eq 'S'}">
                           	셀프
                        </c:if>
                        <c:if test ="${data.carrier_type eq 'C'}">
                          	 캐리어
                        </c:if> 
                  </td>
                              
              </c:if>
               <c:if test ="${paramMap.searchType eq 'date'}">
                     <td style="text-align: center; cursor: pointer;"
                        onclick="javascript:paymentKindTeamClick('CA','${paramMap.searchWord}','${paramMap.searchType}','${data.carrier_type}');">
                        <c:out value="${data.CA}" /></td>
                           </c:if>
                      
                      <c:if test ="${paramMap.searchType eq 'month'}">
                     <td style="text-align: center; cursor: pointer;"
                       	   onclick="javascript:paymentKindTeamClick('CA','${paramMap.searchWord}','${paramMap.searchType}','${data.carrier_type}');">
                        <c:out value="${data.CA}" /></td>
                           </c:if>
                        <c:if test ="${paramMap.quarterType ne 0}">
                     <td style="text-align: center; cursor: pointer;"
                       	   onclick="javascript:paymentKindTeamClickForQuarter('CA','${paramMap.quarterType}','${paramMap.yearType}','${data.carrier_type}');">
                        <c:out value="${data.CA}" /></td>
                           </c:if>
                        
                        
                        
                               <c:if test ="${paramMap.searchType eq 'date'}">
                     <td style="text-align: center; cursor: pointer;"
                        onclick="javascript:paymentKindTeamClick('DD','${paramMap.searchWord}','${paramMap.searchType}','${data.carrier_type}');">
                        <c:out value="${data.DD}" /></td>
                           </c:if>
                      
                      <c:if test ="${paramMap.searchType eq 'month'}">
                     <td style="text-align: center; cursor: pointer;"
                       	   onclick="javascript:paymentKindTeamClick('DD','${paramMap.searchWord}','${paramMap.searchType}','${data.carrier_type}');">
                        <c:out value="${data.DD}" /></td>
                           </c:if>
                        
                        <c:if test ="${paramMap.quarterType ne 0}">
                     <td style="text-align: center; cursor: pointer;"
                       	   onclick="javascript:paymentKindTeamClickForQuarter('DD','${paramMap.quarterType}','${paramMap.yearType}','${data.carrier_type}');">
                        <c:out value="${data.DD}" /></td>
                           </c:if>
                           
                           
                           
        				<c:if test ="${paramMap.searchType eq 'date'}">
                     <td style="text-align: center; cursor: pointer;"
                        onclick="javascript:paymentKindTeamClick('AT','${paramMap.searchWord}','${paramMap.searchType}','${data.carrier_type}');">
                        <c:out value="${data.AT}" /></td>
                           </c:if>
                      
                      <c:if test ="${paramMap.searchType eq 'month'}">
                     <td style="text-align: center; cursor: pointer;"
                       	   onclick="javascript:paymentKindTeamClick('AT','${paramMap.searchWord}','${paramMap.searchType}','${data.carrier_type}');">
                        <c:out value="${data.AT}" /></td>
                           </c:if>
                        <c:if test ="${paramMap.quarterType ne 0}">
                     <td style="text-align: center; cursor: pointer;"
                       	   onclick="javascript:paymentKindTeamClickForQuarter('AT','${paramMap.quarterType}','${paramMap.yearType}','${data.carrier_type}');">
                        <c:out value="${data.AT}" /></td>
                           </c:if>
                        
                        
                       <%--  <td style="text-align: center;"><c:out
                              value="${data.total}" /></td> --%>
</tr>
                           
                  <tr>      
                           <c:set var="CA" value="${fn:replace(CA,',','')+fn:replace(data.CA, ',','')}" />
                                 <c:set var="DD" value="${fn:replace(DD,',','')+fn:replace(data.DD, ',','')}" />
                                 <c:set var="AT" value="${fn:replace(AT,',','')+fn:replace(data.AT, ',','')}" />
                  	   </tr>
                           
                  </c:forEach>
                  
                         <tr>
                                 <td style="text-align: center; background: #E1E1E1; ">                                
                                      <c:out value =" 합     계"/>
                                      </td>
                                      
                                 <td style="text-align: center; background: #E1E1E1;">           
                                  <fmt:formatNumber value="${CA}" groupingUsed="true"/>                        
                                      </td>
                             
                                 <td style="text-align: center; background: #E1E1E1;">  
                                 <fmt:formatNumber value="${DD}" groupingUsed="true"/>                       
                                      </td>
                                      
                            <td style="text-align: center; background: #E1E1E1;">  
                                 <fmt:formatNumber value="${AT}" groupingUsed="true"/>                       
                                      </td>
                                      
                                   </tr>
                  
                  
                  
                     </tbody>
            </table>
            
            
             <table class="article-table forToggle" id="dataTable"
               style=" ">
               <colgroup>

                  <col width="200px;">
                  <col width="200px;">
                  <col width="200px;">
                  <col width="200px;">
                  
       
       
               </colgroup>
               <thead>
                  <div style="text-align: left; padding: 10px;">
                     ※<span style="color: #00f;">총 </span> 결제유형 집계
                  </div>
                  <tr>
                     <!-- <td class="showToggle">번호</td> -->
                     <td style="text-align: center;">현금건</td>
                     <td style="text-align: center;">현장수금건</td>
                     <td style="text-align: center;">계좌이체</td>
                     <td style="text-align: center;">합계</td>
                
                  </tr>
               </thead>
 
               <tbody id="rowData">

                  <c:forEach var="data" items="${list2}" varStatus="status">

                     <tr class="ui-state-default" list-order="${data.list_order}"
                        allocationId="${data.allocation_id}">

          <%--                    <input type="checkbox" name="forBatch" allocationId="${data.allocation_id}"  batchStatus="${data.batch_status}"></td> 
                        <td class="showToggle">${data.list_order}</td> --%>
                        
                        <c:if test ="${paramMap.searchType eq 'date'}">
                     <td style="text-align: center; cursor: pointer;"
                        onclick="javascript:paymentKindClick('CA','${paramMap.searchWord}','${paramMap.searchType}');">
                        <c:out value="${data.CA}" /></td>
                           </c:if>
                      
                      <c:if test ="${paramMap.searchType eq 'month'}">
                     <td style="text-align: center; cursor: pointer;"
                       	   onclick="javascript:paymentKindClick('CA','${paramMap.searchWord}','${paramMap.searchType}');">
                        <c:out value="${data.CA}" /></td>
                           </c:if>
                        <c:if test ="${paramMap.quarterType ne 0}">
                     <td style="text-align: center; cursor: pointer;"
                       	   onclick="javascript:paymentKindClickForQuarter('CA','${paramMap.quarterType}','${paramMap.yearType}');">
                        <c:out value="${data.CA}" /></td>
                           </c:if>
                        
                        
                        
                               <c:if test ="${paramMap.searchType eq 'date'}">
                     <td style="text-align: center; cursor: pointer;"
                        onclick="javascript:paymentKindClick('DD','${paramMap.searchWord}','${paramMap.searchType}');">
                        <c:out value="${data.DD}" /></td>
                           </c:if>
                      
                      <c:if test ="${paramMap.searchType eq 'month'}">
                     <td style="text-align: center; cursor: pointer;"
                       	   onclick="javascript:paymentKindClick('DD','${paramMap.searchWord}','${paramMap.searchType}');">
                        <c:out value="${data.DD}" /></td>
                           </c:if>
                        
                        <c:if test ="${paramMap.quarterType ne 0}">
                     <td style="text-align: center; cursor: pointer;"
                       	   onclick="javascript:paymentKindClickForQuarter('DD','${paramMap.quarterType}','${paramMap.yearType}');">
                        <c:out value="${data.DD}" /></td>
                           </c:if>
                           
                           
                           
        				<c:if test ="${paramMap.searchType eq 'date'}">
                     <td style="text-align: center; cursor: pointer;"
                        onclick="javascript:paymentKindClick('AT','${paramMap.searchWord}','${paramMap.searchType}');">
                        <c:out value="${data.AT}" /></td>
                           </c:if>
                      
                      <c:if test ="${paramMap.searchType eq 'month'}">
                     <td style="text-align: center; cursor: pointer;"
                       	   onclick="javascript:paymentKindClick('AT','${paramMap.searchWord}','${paramMap.searchType}');">
                        <c:out value="${data.AT}" /></td>
                           </c:if>
                        <c:if test ="${paramMap.quarterType ne 0}">
                     <td style="text-align: center; cursor: pointer;"
                       	   onclick="javascript:paymentKindClickForQuarter('AT','${paramMap.quarterType}','${paramMap.yearType}');">
                        <c:out value="${data.AT}" /></td>
                           </c:if>
                        
                        
                        <td style="text-align: center;"><c:out
                              value="${data.total}" /></td>

                     </tr>
                     <tr class="ui-state-default" list-order="${data.list_order}"
                        allocationId="${data.allocation_id}">

           <%--                   <input type="checkbox" name="forBatch" allocationId="${data.allocation_id}"  batchStatus="${data.batch_status}"></td> 
                        <td class="showToggle">${data.list_order}</td> --%>
                        <td style="text-align: center;"><c:out value="${data.CA_sum}" /></td>
                        <td style="text-align: center;"><c:out value="${data.DD_sum}" /></td>
                        <td style="text-align: center;"><c:out value="${data.AT_sum}" /></td>
                        <td style="text-align: center;"><c:out
                              value="${data.all_total}" /></td>

                     </tr>                     
                  </c:forEach>

               </tbody>
            </table> 
            
            
            
            
            
         </div>


         <!--  </div> -->


         <!-- <div style="color:#8B0000;">※차대번호는 기사님이 입력한 경우에는 붉은색으로 표시 되고 배차 직원이 수정 한 경우에는 파란색으로 표시 됩니다.</div> -->

         <%-- <div class="table-pagination text-center">
                       <ul class="pagination">
                          <html:paging uri="/allocation/combination.do" forGroup="&startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&searchDateType=${paramMap.searchDateType}&forOrder=${paramMap.forOrder}%5E${paramMap.order}" frontYn="N" />
                          <!--   <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                           <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                           <li class="curr-page"><a href="#">1</a></li>
                           <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                           <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li> -->
                       </ul>
                   </div>  --%>

      </section>



      <!--     <div class="confirmation">
                    <div class="cancel">
                        <a style="cursor:pointer;" onclick="javascript:batchStatus('cancel');">일괄취소</a>
                    </div>
                    <div class="confirm">
                        <a style="cursor:pointer;" onclick="javascript:batchStatus('complete');">일괄완료</a>
                    </div>
                </div>   -->
   </div>
   </form>
</div>





<!-- <iframe style="width: 980px; height:10000px; border: none;" frameBorder="0" id="happyboxFrame" scrolling="no" src="https://www.happyalliance-happybox.org/Bridge?v=param"></iframe>     -->
<script>
   if ("${userMap.control_grade}" == "01") {
      $('#dataTable').SetEditable({
         columnsEd : "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15",
         onEdit : function(row) {
            updateAllocaion(row)
         },
         onDelete : function() {
         },
         onBeforeDelete : function() {
         },
         onAdd : function() {
         }
      });

   }
</script>
