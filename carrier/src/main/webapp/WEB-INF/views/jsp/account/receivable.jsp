<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator"
   prefix="decorator"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld"%>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/js/bootstable.js"></script>
<script src="/js/vendor/html2canvas.min.js"></script>
<script src="/js/vendor/es6-promise.auto.js"></script>
<script type="text/javascript">

   var oldListOrder = "";
   var allocationId = "";

   $(document).ready(function() {
                  //forOpen

                  /* $('#dataTable tr:odd td').css("backgroundColor",
                        "#f9f9f9"); */

					var selectMonth = $("#selectMonth").val();
    					
                   $("#indate").datepicker({
          
                     dateFormat : "yy-mm-dd",
                     onSelect : function(date) {
                        //$("#endDt").focus();
                        setTimeout(function() {
                           //alert(this.toString());
                    
 							
                           document.location.href = "/account/receivable.do?&searchWord="+date+"&searchType=date"+"&companyType="+$("#companyType").val();
                           //$("#endDt").focus();
                        }, 50);
                     }
                  }); 

                   
                  /* Gosummary */

                  $('#summary').click(function() {

                	  					
                                 var indate = document.getElementById('indate');
                                 var indateVal = $("#indate").val();
                                 if (indateVal == "") {
                       
                                 } else {

                                    document.location.href = "/account/receivable.do?&searchWord="+indateVal+"&searchType=date"+"&companyType="+$("#companyType").val();

                                 }

                              });
 

                  
                     /* $("#indate").click(function(){

                                                
                         var indateVal = $("#indate").datepicker();
                          document.location.href = "/account/summary.do?&dateSearch="+ indateVal;

                        }); */

               	$("#selectMonth").MonthPicker({ 

            		Button: false
            		,MonthFormat: 'yy-mm'	
            		,OnAfterChooseMonth: function() { 
            	        //alert($(this).val());
            	        document.location.href = "/account/receivable.do?&searchWord="+$(this).val()+"&searchType=month"+"&companyType="+$("#companyType").val();
            	        
            	     }
            	});


               });


   
   function updateAllocaion(row) {
      var inputDt = "";
      var carrierType = "";
      var distanceType = "";
      var departureDt = "";
      var departureTime = "";
      var customerName = "";
      var carKind = "";
      var carIdNum = "";
      var carNum = "";
      var departure = "";
      var arrival = "";
      var driverName = "";
      var carCnt = "";
      var amount = "";
      var paymentKind = "";
      var allocId = $(row).attr("allocationId");
      if (confirm("수정 하시겠습니까?")) {
         $(row).find("td").each(function(index, element) {
            if (index == 1) {
               inputDt = $(this).html();
            }
            if (index == 2) {
               if ($(this).html() == "셀프") {
                  carrierType = "S";
               } else if ($(this).html() == "캐리어") {
                  carrierType = "C";
               } else {
                  alert("배차구분을 확인 하세요.");
                  return false;
               }
            }
            if (index == 3) {
               if ($(this).html() == "시내") {
                  distanceType = "0";
               } else if ($(this).html() == "시외") {
                  distanceType = "1";
               } else {
                  alert("거리구분을 확인 하세요.");
                  return false;
               }
            }
            if (index == 4) {
               departureDt = $(this).html();
            }
            if (index == 5) {
               departureTime = $(this).html();
            }
            if (index == 6) {
               customerName = $(this).html();
            }
            if (index == 7) {
               carKind = $(this).html();
            }
            if (index == 8) {
               carIdNum = $(this).html();
            }
            if (index == 9) {
               carNum = $(this).html();
            }
            if (index == 10) {
               departure = $(this).html();
            }
            if (index == 11) {
               arrival = $(this).html();
            }
            if (index == 12) {
               driverName = $(this).html();
            }
            if (index == 13) {
               carCnt = $(this).html();
            }
            if (index == 14) {
               amount = $(this).html();
            }
            if (index == 15) {
               paymentKind = $(this).html();
            }
         });

         if (carrierType == "" || distanceType == "") {
            return false;
         }

         $.ajax({
            type : 'post',
            url : "/allocation/update-allocation.do",
            dataType : 'json',
            data : {
               inputDt : inputDt,
               carrierType : carrierType,
               distanceType : distanceType,
               departureDt : departureDt,
               departureTime : departureTime,
               customerName : customerName,
               carKind : carKind,
               carIdNum : carIdNum,
               carNum : carNum,
               departure : departure,
               arrival : arrival,
               driverName : driverName,
               carCnt : carCnt,
               amount : amount,
               paymentKind : paymentKind,
               allocationId : allocId
            },
            success : function(data, textStatus, jqXHR) {
               var result = data.resultCode;
               if (result == "0000") {
                  alert("변경 되었습니다.");
                  //         document.location.href = "/allocation/combination.do";
               } else if (result == "0001") {
                  alert("변경 하는데 실패 하였습니다.");
               }
            },
            error : function(xhRequest, ErrorText, thrownError) {
            }
         });

      }

   }

   function getListId(obj) {
      $('html').scrollTop(0);
      selectList($(obj).parent().parent().parent().attr("allocationId"));

   }

   var driverArray = new Array();
   function selectList(id) {

      var searchWord = encodeURI('${paramMap.searchWord}');
      document.location.href = "/allocation/allocation-view.do?&searchDateType=${paramMap.searchDateType}&startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&searchType=${paramMap.searchType}&cPage=${paramMap.cPage}&allocationStatus=${paramMap.allocationStatus}&location=${paramMap.location}&allocationId="
            + id + "&searchWord=" + searchWord;

   }

   function batchStatus(status) {

      var id = "";
      var total = $('input:checkbox[name="forBatch"]:checked').length;

      if (total == 0) {
         alert("수정할 목록이 선택 되지 않았습니다.");
         return false;
      } else {
         $('input:checkbox[name="forBatch"]:checked').each(
               function(index, element) {
                  if (this.checked) {//checked 처리된 항목의 값
                     id += $(this).attr("allocationId");
                     if (index < total - 1) {
                        id += ",";
                     }
                  }
               });
         updateAllocationStatus(status, id);
      }
   }

   function updateAllocationStatus(status, id) {

      var msg = "";
      if (status == "cancel") {
         msg = "취소";
      } else if (status == "complete") {
         msg = "완료";
      }

      if (confirm(msg + "하시겠습니까?")) {
         $.ajax({
            type : 'post',
            url : "/allocation/updateAllocationStatus.do",
            dataType : 'json',
            data : {
               status : status,
               allocationId : id
            },
            success : function(data, textStatus, jqXHR) {
               var result = data.resultCode;
               var resultData = data.resultData;
               if (result == "0000") {
                  alert(msg + "되었습니다.");
                  document.location.href = "/allocation/combination.do";
               } else if (result == "0001") {
                  alert(msg + "하는데 실패 하였습니다.");
               }
            },
            error : function(xhRequest, ErrorText, thrownError) {
            }
         });
      }

   }

   function checkAll() {

      if ($('input:checkbox[id="checkAll"]').is(":checked")) {
         $('input:checkbox[name="forBatch"]').each(function(index, element) {
            $(this).prop("checked", "true");
         });
      } else {
         $('input:checkbox[name="forBatch"]').each(function(index, element) {
            $(this).prop("checked", "");
         });
      }
   }

   function move(location) {

      window.location.href = "/allocation/" + location + ".do";

   }


 

   function excelDownload() {

         if (confirm("다운로드 하시겠습니까?")) {

        	 html2canvas(document.querySelector("#bottom-table")).then(function(canvas){
                    
            	 saveAs(canvas.toDataURL());

                     
             });
            
            
         }else{
         alert('취소');
         return false;

            }
      }



   function saveAs(uri) {

	
		$("#receipt").val(uri);
	    $("#excelsussess").submit();
		
	   
		
	}


   
   
         /*    document.location.href = "/allocation/excel_download.do?&carrierType="
               + carrierType
               + "&searchDateType="
               + $("#searchDateType").val()
               + "&startDt="
               + $("#startDt").val()
               + "&endDt="
               + $("#endDt").val()
               + "&searchType="
               + $("#searchType").val()
               + "&searchWord="
               + encodeURI($("#searchWord").val());
    */   



   
   function sendSocketMessage() {

      $.ajax({
         type : 'post',
         url : "http://52.78.153.148:8080/allocation/sendSocketMessage.do",
         dataType : 'json',
         data : {
            allocationId : 'ALO3df74bd0105046bfbdce51ab0e64927c',
            driverId : "sourcream"
         },
         success : function(data, textStatus, jqXHR) {
            var result = data.resultCode;
            var resultData = data.resultData;
            if (result == "0000") {
               //alert("성공");
            } else if (result == "0001") {

            }
         },
         error : function(xhRequest, ErrorText, thrownError) {
         }
      });

   }


    function companySearch(companyType){
	      
 	   document.location.href = "/account/receivable.do?&companyType="+companyType;
 	   
 }

    
    

   function viewBycustomerId(customerId, searchWord,searchType) {

      document.location.href = "/account/receivable_list.do?&searchWord="+ searchWord + "&customerId=" + customerId +"&searchType=${paramMap.searchType}"+"&companyType="+$("#companyType").val()+"&unpaid=${paramMap.unpaid}";

   }

function viewBycustomerIdMonth(customerId,searchWord,searchType){


	 document.location.href = "/account/receivable_list.do?&searchWord="+ searchWord +"&customerId=" + customerId + "&searchType=${paramMap.searchType}"+"&companyType="+$("#companyType").val()+"&unpaid=${paramMap.unpaid}";
	
}



	   






function quarterCustomerId(customerId,quarterType,yearType){

	document.location.href = "/account/receivable_list.do?&customerId="+ customerId + "&quarterType=${paramMap.quarterType}"+"&yearType=${paramMap.yearType}"+"&companyType="+$("#companyType").val()+"&unpaid=${paramMap.unpaid}";
	
}


		$("#quarterly").val("${paramMap.quarterTypeList}").prop("selected", true); 
		

		
	function goQuarterly(yearType,quarterType){

		
		
	var year = $("#forYear").val();
		
      document.location.href="/account/receivable.do?&yearType="+year+"&quarterType="+quarterType+"&companyType="+$("#companyType").val()+"&unpaid=${paramMap.unpaid}";   

		}

	

	
function f_datepicker(obj){


$(obj).datepicker()+"date";
 

}


function receivable(){

	document.location.href ="/account/receivable.do";

	
}


function receivableNoMoney(){

	document.location.href ="/account/receivable.do?&unpaid=N";

	
}



</script>



<!-- <style>

  table {
  
    margin-left: auto;
    margin-right: auto;
  }

</style> -->

<div class="modal-field">
   <div class="modal-box">
      <h3 class="text-center">기사리스트</h3>
      <div class="modal-table-container">
         <table class="article-table">
            <colgroup>

            </colgroup>
            <thead>
               <tr>
                  <!-- <td>소유주</td> -->
                  <td>기사명</td>
                  <td>연락처</td>
                  <td>차량번호</td>
                  <td>차종</td>
               </tr>
            </thead>
            <tbody id="driverSelectList">
               <c:forEach var="data" items="${driverList}" varStatus="status">

                  <tr class="ui-state-default" style="cursor: pointer;"
                     driverId="${data.driver_id}"
                     onclick="javascript:setDriver('${data.driver_id}','${data.driver_name}','${data.car_kind}');">
                     <%-- <td>${data.driver_owner}</td> --%>
                     <td style="">${data.driver_name}</td>
                     <td>${data.phone_num}</td>
                     <td>${data.car_num}</td>
                     <td>${data.car_kind}</td>
                  </tr>
               </c:forEach>
            </tbody>
         </table>
      </div>
      <div class="confirmation">
         <div class="confirm">
            <input type="button" value="취소" name="">
         </div>


      </div>

      <div class="pickupbox">
         <div class="confirm">
            <input type="button" value="취소" name="">
         </div>
      </div>
   </div>
</div>
<%-- <section class="side-nav">
            <ul>
                <li class="<c:if test="${complete == ''}">active</c:if>"><a href="/allocation/combination.do">신규배차입력</a></li>
                <li><a href="/allocation/self.do">셀프 배차  </a></li>
                <li><a href="/allocation/self.do?reserve=N">셀프 예약</a></li>
                <li><a href="/allocation/self.do?reserve=Y">셀프 배차 현황</a></li>
                <li><a href="/allocation/carrier.do">캐리어 배차</a></li>
                <li><a href="/allocation/carrier.do?reserve=N">캐리어 예약</a></li>
                <li><a href="/allocation/carrier.do?reserve=Y">캐리어 배차 현황</a></li>
                <li class="<c:if test="${complete == 'Y'}">active</c:if>"><a href="/allocation/combination.do?forOpen=N&complete=Y">완료 배차</a></li>
            </ul>
        </section> --%>

<section class="dispatch-top-content">
   <div class="breadcrumbs clearfix">
      <ul>
         <li><a href="">HOME</a></li>
         <li><img src="/img/bc-arrow.png" alt=""></li>
         <li><a href="">미수/미수금 관리</a></li>

      </ul>
   </div>
   
   
   <form id="excelsussess" method="POST" action="/account/summary_excel.do">
   
   <input type="hidden" name="receipt" id="receipt">
   </form>
        <div class="up-dl clearfix">
<div class="upload-btn" style="float:right;">
<input type="button"  onclick="javascript:excelDownload('S');" value="엑셀 다운로드"> 
</div>       
</div>         
   <div class="up-dl clearfix header-search">
      <table>
         <tbody>
            <tr>
               <td>
               &nbsp;소속 선택 :&nbsp;
                </td>
                 <td style="width: 149px;">
                    <div class="select-con">
			<select class="dropdown" name="companyType" id="companyType">
				<option value="" selected="selected">전체</option>
			    <option value="company1" <c:if test='${paramMap.companyType eq "company1"}'> selected="selected"</c:if>>한국캐리어</option>
			    <option value="company2" <c:if test='${paramMap.companyType eq "company2" }'> selected="selected"</c:if>>한국카캐리어(주)</option>
			</select>
			</div>
                </td>
                           <!--  <td>
                                <input type="button" id="btn-company" value="검색" onclick="javascript:companySearch($('#companyType').val());">
                            </td> -->
               <td>일별 조회  :</td>
               <td class="widthAuto" style="width: 200px;">
               <input style="width: 100%; text-align: center;" class="datepick" name="indate" id="indate" type="text" value="<c:if test="${paramMap.searchType != null && paramMap.searchType == 'date'}">${paramMap.searchWord}</c:if>" >
               </td>
               <td>
                  <button id="summary">검색</button> <!-- 
                                  <input type="button" id="btn-search"  value="검색" class="btn-primary" onclick="javascript:summarySearch();">  
                              -->
               </td>
            </tr>
         </tbody>
      </table>
   </div>

 <div class="up-dl clearfix header-search" style="margin-left :600px;">
      <table>
         <tbody>
            <tr>
               <td>월별 조회  :</td>
               <td class="widthAuto" style="width: 200px;">
                  <input style="width:100%; text-align:center;" id="selectMonth" name="selectMonth"  type="text" placeholder="조회월" readonly="readonly" value="<c:if test="${paramMap.searchType != null && paramMap.searchType == 'month'}">${paramMap.searchWord}</c:if>">
               </td>
               <td>
                  <button id="summary">검색</button> <!-- 
                                  <input type="button" id="btn-search"  value="검색" class="btn-primary" onclick="javascript:summarySearch();">  -->
               </td>
            </tr>
         </tbody>
      </table>
   </div>
    <div class="up-dl clearfix header-search" style="margin-left : 960px;">
      <table>
         <tbody>
            <tr>
               <td>시즌 별 조회  :
               
               					<%-- <select name ="forYear" id ="forYear">
								
									<option value="${paramMap.yearType[0].yearType}">${paramMap.yearType[0].yearType}</option>
									<option value="${paramMap.yearType[1].yearType}">${paramMap.yearType[1].yearType}</option>
													
								</select> --%>
								 
								<select name ="forYear" id ="forYear">
									<option value="" selected>년도</option>
									<c:forEach var="data" items="${yearList}" varStatus="status">
										<option value="${data.yearType}" <c:if test='${paramMap.yearType eq data.yearType }'>selected</c:if>> ${data.yearType}</option>
									</c:forEach>
								</select> 
				
						<%--  <input type ="text"  style="width:40px; text-align:center;" id="yearType" name ="yearType" value="${paramMap.yearType}">년 --%>
           		    <td class="widthAuto" style="width: 70px;">
              <%--     <input style="width:100%; text-align:center;" id="quarterly" name="quarterly"  type="text" placeholder="조회분기" readonly="readonly" value="${paramMap.searchWord}"> --%>
           							
           					<select name="quarterly"  id="quarterly">
 								
									<option value=" " <c:if test='${paramMap.quarterType eq "" }'>selected</c:if> >선택</option>
									<option value="1" <c:if test='${paramMap.quarterType eq "1"}'> selected</c:if> >1분기</option>
									<option value="2" <c:if test='${paramMap.quarterType eq "2"}'> selected</c:if> >2분기</option>
									<option value="3" <c:if test='${paramMap.quarterType eq "3"}'> selected</c:if> >3분기</option>
									<option value="4" <c:if test='${paramMap.quarterType eq "4"}'> selected</c:if> >4분기</option>
									<option value="5" <c:if test='${paramMap.quarterType eq "5"}'> selected</c:if> >상반기</option>
									<option value="6" <c:if test='${paramMap.quarterType eq "6"}'> selected</c:if> >하반기</option>
									
											
								</select>
               </td>	 
               		<td>
                      	<input type="button" value="검색"  onclick="javascript:goQuarterly( $('#forYear').val(),$('#quarterly').val() )"> 
                     	<%-- <input type="button" value="검색"  onclick="javascript:goQuarterly('yearType[i]','${paramMap.numberType}')"> --%>
 						                        
               </td>

            </tr>
         </tbody>
      </table>
   </div>
   

   <div class="dispatch-btn-container">
      <!-- <div class="dispatch-btn">
                    <i id="downArrow" class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div> -->

      <%-- <c:if test="${complete == null || complete != 'Y'}"> --%>
      <%-- <c:if test="${forOpen == null || forOpen != 'N'}">
                  <div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">신규배차입력</div>
               </c:if> --%>
      <%-- <c:if test="${forOpen != null && forOpen == 'N'}"> --%>
      <p>
      <div style="">
         ※<span style="color: #00f;"> 거래처명</span>을 클릭하면 상세보기로 넘어갑니다.
      </div>
      <div
         style="width: 300px; margin: auto; text-align: center; font-weight: bold; font-size: 30px;">미수/미지급
         관리</div>
      <%-- </c:if>
            </c:if> --%>

      <%-- <c:if test="${complete != null && complete == 'Y'}">
               <div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">완료배차</div>               
            </c:if> --%>
            

   </div>
</section>


<div class="dispatch-wrapper"  align="left">
   <%-- <jsp:include page="common-form.jsp"></jsp:include> --%>
      <div class="confirmation" style="min-height:0px; text-align:left; margin-top:-10px; margin-left:30px; margin-bottom:10px;">
	            <div class="<c:if test='${paramMap.unpaid ne "N" }'> confirm</c:if><c:if test='${paramMap.unpaid eq "N" }'> cancel</c:if>">
	            	<a style="cursor:pointer;" onclick="javascript:receivable();">미수</a>
	            </div>
	            <div class="<c:if test='${paramMap.unpaid eq "N" }'> confirm</c:if><c:if test='${paramMap.unpaid ne "N" }'> cancel</c:if>">
	            	<a style="cursor:pointer;" onclick="javascript:receivableNoMoney()">미지급</a>
	              </div>
	            
            </div>
      <section class="bottom-table" style="width: 100%; margin-left: 10px;">
  
         <div style="width: 100%; padding:10px;">
         
         
         <table class="article-table forToggle" id="dataTable"
            style="">
            <colgroup>
               <%-- <col width="auto"> --%>
 

               <col width="auto;">
               <col width="auto">
               <col width="auto">
               <col width="auto">
               <col width="auto;">



               <%-- <c:if test="${user.control_grade == '01' }">
                              <col width="auto">
                              <col width="auto">
                           </c:if> --%>
            </colgroup>
            <thead>
          <!--      <div style="padding: 10px;" >
                     ※<span style="color: #00f;">거래처별 </span>요약관리
                  </div> -->
               <tr>
                  <!-- <td class="showToggle">번호</td> -->
                  <td style="text-align: center;">거래처명</td>
                  <c:if test='${paramMap.unpaid ne "N" }'>
                  	<td style="text-align: center;">미수액</td>
      			  </c:if>
                      <c:if test='${paramMap.unpaid eq "N" }'>
                  	<td style="text-align: center;">미지급액</td>
      			  </c:if>
                  <%-- <c:if test="${user.control_grade == '01' }">   
                                  <td class="showToggle"></td>
                                  <td>수정</td>
                               </c:if> --%>
               </tr>
            </thead>

                     <c:set var="customerNameTotal" value="0" />
                          <c:set var="maeChulTotal" value="0" />
                          <c:set var="maeIbTotal" value="0" />
                          <c:set var="statusCountTotal" value="0" />
                          <c:set var="summary1Total" value="0" />
                       

            <tbody id="rowData">

               <c:forEach var="data" items="${list}" varStatus="status">

                  <tr class="ui-state-default" list-order="${data.list_order}"
                     allocationId="${data.allocation_id}">

                     <%--      <input type="checkbox" name="forBatch" allocationId="${data.allocation_id}"  batchStatus="${data.batch_status}"></td>  --%>
                     <%-- <td class="showToggle">${data.list_order}</td> --%>
     				   <c:if test ="${paramMap.searchType eq 'date'}">
                     <td style="text-align: center; cursor: pointer;"
                        onclick="javascript:viewBycustomerId('${data.customer_id}','${paramMap.searchWord}');"><c:out
                           value="${data.customer_name}" /></td>
                        </c:if>
                            <c:if test ="${paramMap.searchType eq 'month'}">
                     <td style="text-align: center; cursor: pointer;"
                        onclick="javascript:viewBycustomerIdMonth('${data.customer_id}','${paramMap.searchWord}');"><c:out
                           value="${data.customer_name}" /></td>
                           </c:if>
                           
                            <c:if test ="${paramMap.quarterType ne 0}">
                     <td style="text-align: center; cursor: pointer;"
                        onclick="javascript:quarterCustomerId('${data.customer_id}','${paramMap.quarterType}','${paramMap.yearType}');"><c:out
                           value="${data.customer_name}" /></td>
                        </c:if>
                           
                           
                         <c:if test ="${paramMap.quarterType eq 0}">
                     	<td style="text-align: center; cursor: pointer;" onclick="javascript:viewBycustomerId('${data.customer_id}','${paramMap.searchWord}');">
                     	<fmt:formatNumber value="${data.total}" groupingUsed="true"/> 원</td> 
                     	<%-- <c:out  value="${data.total}" /></td> --%>
               
                     </c:if>
                     
                         <c:if test ="${paramMap.quarterType ne 0}">
                     <td style="text-align: center; cursor: pointer;"  onclick="javascript:quarterCustomerId('${data.customer_id}','${paramMap.quarterType}','${paramMap.yearType}');">
                     <%-- <c:out value="${data.total}" /></td> --%>
                   	<fmt:formatNumber value="${data.total}" groupingUsed="true"/> 원</td> 
                     </c:if>
                     
                     
                     </tr>
                  
                  <tr>      
                           <c:set var="maeChulTotal" value="${fn:replace(maeChulTotal,',','')+fn:replace(data.total, ',','')}" />
                  </tr>

               </c:forEach>
                                <tr>
                                 <td style="text-align: center; background: #E1E1E1; ">                                
                                      <c:out value =" 합     계"/>
                                      </td>

                                 <td style="text-align: center; background: #E1E1E1; ">                                  
                                      <fmt:formatNumber value="${maeChulTotal} " groupingUsed="true"/> 원
                                      </td>
                                  
                                   </tr>   
                             
            </tbody>

         </table>
                 <div class="table-pagination text-center">
                    <ul class="pagination">
              
                          <html:paging uri="/account/receivable.do" forGroup="&searchType=${paramMap.searchType}&searchWord=${paramMap.searchWord}&quarterType=${paramMap.quarterType}&yearType=${paramMap.yearType}" frontYn="N" />
                    
                    
              <!-- <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                           <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                           <li class="curr-page"><a href="#">1</a></li>
                           <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                           <li><a href="#"><i class="fa fa-angle-double-right"></i></a>
                           </li>  -->
                       
                       </ul> 
                   </div>   
         
         
         </div>
         
     



      </section>



      <!--     <div class="confirmation">
                    <div class="cancel">
                        <a style="cursor:pointer;" onclick="javascript:batchStatus('cancel');">일괄취소</a>
                    </div>
                    <div class="confirm">
                        <a style="cursor:pointer;" onclick="javascript:batchStatus('complete');">일괄완료</a>
                    </div>
                </div>   -->
   </div>
   </form>
</div>





<!-- <iframe style="width: 980px; height:10000px; border: none;" frameBorder="0" id="happyboxFrame" scrolling="no" src="https://www.happyalliance-happybox.org/Bridge?v=param"></iframe>     -->
<script>
   if ("${userMap.control_grade}" == "01") {
      $('#dataTable').SetEditable({
         columnsEd : "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15",
         onEdit : function(row) {
            updateAllocaion(row)
         },
         onDelete : function() {
         },
         onBeforeDelete : function() {
         },
         onAdd : function() {
         }
      });

   }
</script>
