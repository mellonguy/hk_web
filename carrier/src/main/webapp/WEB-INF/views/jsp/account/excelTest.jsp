<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import ="java.io.*,java.text.*,java.util.*"%>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title></title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/css/bootstrap.min.css">
<link rel="stylesheet" href="/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="/fonts/NotoSans/notosanskr.css">
<link rel="stylesheet" href="/css/reset.css">
<link rel="stylesheet" href="/css/main.css">
<link rel="stylesheet" href="/css/main2.css">
<link rel="stylesheet" href="/css/responsive.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="/js/vendor/modernizr-2.8.3.min.js"></script>
<script src="/js/vendor/jquery-1.11.2.min.js"></script>
<script src="/js/89d51f385b.js"></script>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<!-- <script type="text/javascript" src="/js/plugins/jquery.form.min.js"></script> -->
<script src="/js/vendor/jquery-1.11.2.min.js"></script>
<script type="text/javascript">


</script>


<style type="text/css">
#btn-search {
	width: 120px;
    height: 40px;
    background: linear-gradient(#4383de,#3f7dd4);
    border-radius: 3px;
    color: #fff;
    font-size: 13px;
    font-family: NotoSansRegular, sans-serif;
    font-weight: 400;
    border: 0;
    outline: none;
    margin-top:15px;
    margin-left:90px;
}
</style>	
	
</head>
	
<script type="text/javascript">	



function updateDriverViewFlag(driverName,driverId,decideMonth){
	
	
	if(confirm(driverName+"기사의 "+decideMonth+"월 공제 내역서를 전송 하시겠습니까?")){
		
		$.ajax({ 
			type: 'post' ,
			url : "/account/updateDriverViewFlag.do" ,
			dataType : 'json' ,
			data : {
				driverId : driverId,
				decideMonth : decideMonth
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("전송하는데 성공했습니다.");
					//self.close();
					opener.window.location.reload();
					window.location.reload();
					self.close();
				}else if(result != "0000"){
					alert("전송 하는데 실패 했습니다. 관리자에게 문의 하세요.");
					window.location.reload();
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
		
		
	}
	
	
}



</script>
	<body style="width:100%; height:100%;">
		
	<c:if test="${map.driverDeductFile.driver_view_flag == 'N'}">
		<input type="button" id="btn-search" value="보내기" class="btn-primary" onclick="javascript:updateDriverViewFlag('${map.driver.driver_name}','${map.driverId}','${map.decideMonth}');">
	</c:if>
	
	<iframe style="" src="http://docs.google.com/gview?&d=v&embedded=true&url=http://14.63.39.17:13200/files/deduct_list/2019/10/15/f74eaf76-8c88-4e01-a637-18a83f274f43.xlsx" width="50%;"  height="1000px;" ></iframe>
	
	<script>
	
	
	
	$(document).ready(function(){
		
		
		 
	});
	
	
	</script>
	
	
</body>
</html>

