<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/js/bootstable.js"></script>
<script type="text/javascript"> 
var oldListOrder = "";
var allocationId = "";

$(document).ready(function(){
	//forOpen

	
	$('#dataTable tr:odd td').css("backgroundColor","#f9f9f9");
	
	$("#startDt").datepicker({
		dateFormat : "yy-mm-dd",
		onSelect : function(date){
			$("#endDt").focus();
		}
	});
	
	
	
});

var rgx1 = /\D/g;  // /[^0-9]/g 와 같은 표현
var rgx2 = /(\d+)(\d{3})/;

function getNumberOnly (str) {			//저장할때는 콤마 제거 후 저장한다.
    var len = str.length;
    var sReturn = "";

    for (var i=0; i<len; i++){
        if ( (str.charAt(i) >= "0") && (str.charAt(i) <= "9") ){
            sReturn += str.charAt(i);
        }
    }
    return sReturn;
}

function comma(str) {
    str = String(str);
    return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
}

function getNumber(obj,gubun){
	
     var num01;
     var num02;
     num01 = obj.value;
     num02 = num01.replace(rgx1,"");
     num01 = setComma(num02);
     obj.value =  num01;
        
     if(gubun == "1"){
    	if($(obj).parent().prev().find("input").is(":checked")){
    		var price = Number(getNumberOnly($(obj).val()));
     		var vat = Math.round(Number(price/10)); 
     		$(obj).parent().next().find("input").val(comma(vat));
     		var total = price+vat;
     		$(obj).parent().next().next().find("input").val(comma(total));
    	}else{
    		var price = Number(getNumberOnly($(obj).val()));
    		$(obj).parent().next().next().find("input").val(comma(price));
    	}
     }else if(gubun == "2"){
    	 //if($(obj).parent().prev().prev().find("input").is(":checked")){
    		var price = Number(getNumberOnly($(obj).parent().prev().find("input").val()));
    	  	var vat = Number(getNumberOnly($(obj).val())); 
    	  	var total = price+vat;
    	  	$(obj).parent().next().find("input").val(comma(total));	 
    	 //}
     }
     
}

function setComma(inNum){
     
     var outNum;
     outNum = inNum; 
     while (rgx2.test(outNum)) {
          outNum = outNum.replace(rgx2, '$1' + ',' + '$2');
      }
     return outNum;

}

var beforeValue = "";

function saveAmount(obj){
	beforeValue = $(obj).val();
}


function updateAmount(obj,allocationId,paymentDivision,gubun){
	
	
	
	if($(obj).val() == ""){
		$(obj).val(0); 
		//return;
	}
	
	var amount = "";
	var vat = "";
	var price = "";
	var vatIncludeYn = "";
	var vatExcludeYn = "";
	
	if(gubun == "A"){
		amount = getNumberOnly($(obj).val());
		vat = getNumberOnly($(obj).parent().next().find("input").val());
		price = getNumberOnly($(obj).parent().next().next().find("input").val());
		if($(obj).parent().prev().find("input").eq(0).is(":checked")){
			vatIncludeYn = "Y";
		}else{
			vatIncludeYn = "N";
		}
		if($(obj).parent().prev().find("input").eq(1).is(":checked")){
			vatExcludeYn = "Y";
		}else{
			vatExcludeYn = "N";
		}
	}else if(gubun == "V"){
		amount = getNumberOnly($(obj).parent().prev().find("input").val());
		vat = getNumberOnly($(obj).val());
		price = getNumberOnly($(obj).parent().next().find("input").val());
		if($(obj).parent().prev().prev().find("input").eq(0).is(":checked")){
			vatIncludeYn = "Y";
		}else{
			vatIncludeYn = "N";
		}
		if($(obj).parent().prev().prev().find("input").eq(1).is(":checked")){
			vatExcludeYn = "Y";
		}else{
			vatExcludeYn = "N";
		}
	}
	
	if(beforeValue != $(obj).val()){
	//	if(confirm("해당 배차의 업체 청구 금액을 수정 하시겠습니까?")){
			
			$.ajax({ 
				type: 'post' ,
				url : "/allocation/updateAmount.do" ,
				dataType : 'json' ,
				async : false,
				data : {
					allocationId :allocationId, 
					paymentDivision : paymentDivision,
					amount :  amount,
					vat :  vat,
					price :  price,
					vatIncludeYn : vatIncludeYn,
					vatExcludeYn : vatExcludeYn
				},
				success : function(data, textStatus, jqXHR)
				{
					var result = data.resultCode;
					var resultData = data.resultData;
					if(result == "0000"){
						//alert("수정 되었습니다.");
						//window.location.reload();
					}else if(result == "0001"){
						alert("수정 하는데 실패 하였습니다.");
					}
				} ,
				error : function(xhRequest, ErrorText, thrownError) {
				}
			});
			
	//	}else{
	//		$(obj).val(beforeValue);
	//	}	
	}
	
}



function updateAllocaion(row){
	var inputDt = "";
    var carrierType = "";
    var distanceType = "";
    var departureDt = "";
    var departureTime = "";
    var customerName = "";
    var carKind = "";
    var carIdNum = "";
    var carNum  = "";
    var departure = "";
    var arrival  = "";
    var driverName = "";
    var carCnt  = "";
    var amount   = "";
    var paymentKind = "";
	var allocId = $(row).attr("allocationId");
	if(confirm("수정 하시겠습니까?")){
		$(row).find("td").each(function(index,element){
			if(index==1){
				inputDt = $(this).html();
			}
			if(index==2){
				if($(this).html() == "셀프"){
					carrierType = "S";	
				}else if($(this).html() == "캐리어"){
					carrierType = "C";
				}else{
					alert("배차구분을 확인 하세요.");
					return false;
				}
			}
			if(index==3){
				if($(this).html() == "시내"){
					distanceType = "0";	
				}else if($(this).html() == "시외"){
					distanceType = "1";
				}else{
					alert("거리구분을 확인 하세요.");
					return false;
				}
			}
			if(index==4){
				departureDt = $(this).html();
			}
			if(index==5){
				departureTime = $(this).html();
			}
			if(index==6){
				customerName = $(this).html();
			}
			if(index==7){
				carKind = $(this).html();
			}
			if(index==8){
				carIdNum = $(this).html();
			}
			if(index==9){
				carNum = $(this).html();
			}
			if(index==10){
				departure = $(this).html();
			}
			if(index==11){
				arrival = $(this).html();
			}
			if(index==12){
				driverName = $(this).html();
			}
			if(index==13){
				carCnt = $(this).html();
			}
			if(index==14){
				amount = $(this).html();
			}
			if(index==15){
				paymentKind = $(this).html();
			}
      	});

		if(carrierType == "" || distanceType == ""){
			return false;	
		}
		
		 $.ajax({ 
				type: 'post' ,
				url : "/allocation/update-allocation.do" ,
				dataType : 'json' ,
				data : {
					inputDt : inputDt,
					carrierType : carrierType,
					distanceType : distanceType,
					departureDt : departureDt,
					departureTime : departureTime,
					customerName : customerName,
					carKind : carKind,
					carIdNum : carIdNum,
					carNum : carNum,
					departure : departure,
					arrival : arrival,
					driverName : driverName,
					carCnt : carCnt,
					amount : amount,
					paymentKind : paymentKind,
					allocationId : allocId
				},
				success : function(data, textStatus, jqXHR)
				{
					var result = data.resultCode;
					if(result == "0000"){
						alert("변경 되었습니다.");
			//			document.location.href = "/allocation/combination.do";
					}else if(result == "0001"){
   						alert("변경 하는데 실패 하였습니다.");
   					}
				} ,
				error : function(xhRequest, ErrorText, thrownError) {
				}
			}); 
		
	}

}

function getListId(obj){
	$('html').scrollTop(0);
	selectList($(obj).parent().parent().parent().attr("allocationId"));
	
}

var driverArray = new Array();
function selectList(id){
	
	document.location.href="/allocation/allocation-view.do?location=${paramMap.location}&allocationId="+id;
	
}	


function billPublishRequest(){
	
	
	 var total = $('input:checkbox[name="forDecide"]:checked').length;
	var customerId = "";
	
		if(total == 0){
			alert("목록이 선택 되지 않았습니다.");
			return false;
		}else{
			var billPublishInfoList = new Array();
			var requestDt = "";
			var requestDtChk = true;
			var requestDtChkSub = true;
			$('input:checkbox[name="forDecide"]:checked').each(function(index,element) {
					if(this.checked){
						var billPublishInfo= new Object();
						billPublishInfo.index = index;	
						billPublishInfo.allocationId=$(this).attr("allocationId");
						billPublishInfo.requestDt=$(this).parent().parent().children().eq(13).find("input").val();
						if(index == 0){
							requestDt = $(this).parent().parent().children().eq(13).find("input").val();	
						}else{
							if(requestDt != $(this).parent().parent().children().eq(13).find("input").val()){
								requestDtChkSub = false;
							}
						}
						if($(this).parent().parent().children().eq(13).find("input").val() == ""){
							requestDtChk = false;
						}
						billPublishInfoList.push(billPublishInfo);
					}
			 });

			if(!requestDtChk){
				alert("세금계산서 발행일이 입력 되지 않았습니다.");
				return false;
			}else if(!requestDtChkSub){
				alert("세금계산서 발행일이 다릅니다.");
				return false;
			}else{
				if(confirm("선택된 "+total+"건에 대하여 세금계산서 발행 요청을 하시겠습니까?")){
					$.ajax({ 
						type: 'post' ,
						url : "/account/billPublishRequestByAllocationId.do" ,
						dataType : 'json' ,
						data : {
							billPublishInfoList : JSON.stringify({billPublishInfoList : billPublishInfoList}),
							customerId : "${paramMap.customerId}"
						},
						success : function(data, textStatus, jqXHR)
						{
							var resultCode = data.resultCode;
							var resultData = data.resultData;
							
							if(resultCode == "0000"){
								alert("계산서 발행 요청에 성공 했습니다.");
								document.location.reload();
							}else if(resultCode == "0002"){
								alert(resultData+"계산서 발행 요청에 실패 했습니다.");
								document.location.reload();
							}else if(resultCode == "E001"){
								alert("증빙구분이 입력되지 않았습니다.");
								document.location.reload();
							}
						} ,
						error : function(xhRequest, ErrorText, thrownError) {
						}
					});
						
				}
					
			}
			
		} 
	
	
}


















	
function excelDownload(){
	
	
	var total = $('input:checkbox[name="forDecide"]:checked').length;
	var id = "";
	
		if(total == 0){
			alert("목록이 선택 되지 않았습니다.");
			return false;
		}else{
			$('input:checkbox[name="forDecide"]:checked').each(function(index,element) {
			      if(this.checked){//checked 처리된 항목의 값
			    	  id+=$(this).attr("allocationId");
			    	  if(index<total-1){
			    		  id += ","; 
				         } 
			      }
			 });

			//alert(id);
			document.location.href = "/account/excel_download_by_id.do?&decideStatus=${paramMap.decideStatus}&customerId=${paramMap.customerId}&searchDateType="+$("#searchDateType").val()+"&carrierType="+$("#carrierType").val()+"&startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val()+"&allocationId="+id;

		}
	
	
}	
	
	
function batchDecide(status){
	
	var msg = "확정";
	if(status == "N"){
		msg = "수정";
	}
	
	var id = "";
	var total = $('input:checkbox[name="forDecide"]:checked').length;
	
	if(total == 0){
		alert(msg+"할 목록이 선택 되지 않았습니다.");
		return false;
	}else{
		$('input:checkbox[name="forDecide"]:checked').each(function(index,element) {
		      if(this.checked){//checked 처리된 항목의 값
		    	  id+=$(this).attr("allocationId");
		    	  if(index<total-1){
		    		  id += ","; 
			         } 
		      }
		 });
		
		//alert(id);
		updateDecide(id,status);
	}
	
}	

function updateDecide(id,status){
	
	var msg = "확정";
	if(status == "N"){
		msg = "수정";
	}
	
	if(confirm("일괄"+msg+" 하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : "/account/updateCarInfoDecideStatus.do" ,
			dataType : 'json' ,
			data : {
				allocationId : id,
				currentDecideStatus : "${paramMap.decideStatus}",
				setDecideStatus : status
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert(msg+" 되었습니다.");
					//document.location.href = "/allocation/combination.do";
					document.location.reload();
				}else if(result == "0001"){
					alert(msg+" 하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
}	

	
function batchStatus(status){
	
	var id = "";
	var total = $('input:checkbox[name="forBatch"]:checked').length;
	
	if(total == 0){
		alert("수정할 목록이 선택 되지 않았습니다.");
		return false;
	}else{
		$('input:checkbox[name="forBatch"]:checked').each(function(index,element) {
		      if(this.checked){//checked 처리된 항목의 값
		    	  id+=$(this).attr("allocationId");
		    	  if(index<total-1){
		    		  id += ","; 
			         } 
		      }
		 });	
		updateAllocationStatus(status,id);
	}
}	

function updateAllocationStatus(status,id){
	
	var msg = "";
	if(status == "cancel"){
		msg = "취소";
	}else if(status == "complete"){
		msg = "완료";
	}
	
	if(confirm(msg+"하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : "/allocation/updateAllocationStatus.do" ,
			dataType : 'json' ,
			data : {
				status : status,
				allocationId : id
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert(msg+"되었습니다.");
					document.location.href = "/allocation/combination.do";
				}else if(result == "0001"){
					alert(msg+"하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
}	
	
	
	
function checkIncludeAll(){

	var vat_include_yn = "";
	var vat_exclude_yn = "";
	if($('input:checkbox[id="checkIncludeAll"]').is(":checked")){
		$('input:checkbox[name="forIncludeVAT"]').each(function(index,element) {
			
			if($(this).next().is(":checked")){
				$(this).next().prop("checked","");
			}
			
			if($(this).is(":checked")){
				
			}else{
				$(this).prop("checked","true");
				
				
				vat_include_yn = "Y";
				vat_exclude_yn = "N"
				var allocationId = $(this).attr("allocationId");
				var paymentDivision = "01";
				
				//개별건인경우
				if($(this).parent().attr("batchStatus") != "P"){
					var amount = Math.round(Number(getNumberOnly($(this).parent().next().find("input").val()))/1.1);
					$(this).parent().next().find("input").val(comma(amount));
					var vat = Math.round(Number(amount/10)); 
					$(this).parent().next().next().find("input").val(comma(vat));
					var price = amount+vat;
					if(price%10 != 0){
						price = price-(price%10);
					}
					$(this).parent().next().next().next().find("input").val(comma(price));
					
					$.ajax({ 
						type: 'post' ,
						url : "/allocation/updateAmount.do" ,
						dataType : 'json' ,
						async : false,
						data : {
							allocationId :allocationId, 
							paymentDivision : paymentDivision,
							amount :  amount,
							vat :  vat,
							price :  price,
							vatIncludeYn : vat_include_yn,
							vatExcludeYn : vat_exclude_yn
						},
						success : function(data, textStatus, jqXHR)
						{
							var result = data.resultCode;
							var resultData = data.resultData;
							if(result == "0000"){
								//window.location.reload();
							}else if(result == "0001"){
								alert("수정 하는데 실패 하였습니다.");
							}
						} ,
						error : function(xhRequest, ErrorText, thrownError) {
						}
					});
					
				}else{
					
					$.ajax({ 
						type: 'post' ,
						url : "/allocation/updateAmountForPickup.do" ,
						dataType : 'json' ,
						async : false,
						data : {
							allocationId :allocationId, 
							paymentDivision : paymentDivision,
							vatIncludeYn : vat_include_yn,
							vatExcludeYn : vat_exclude_yn
						},
						success : function(data, textStatus, jqXHR)
						{
							var result = data.resultCode;
							var resultData = data.resultData;
							if(result == "0000"){
								//window.location.reload();
							}else if(result == "0001"){
								alert("수정 하는데 실패 하였습니다.");
							}
						} ,
						error : function(xhRequest, ErrorText, thrownError) {
						}
					});
					
				}
				
			}
			
		 });	
	}else{
		$('input:checkbox[name="forIncludeVAT"]').each(function(index,element) {
			
			if($(this).is(":checked")){
				$(this).prop("checked","");
				vat_include_yn = "N";
				vat_exclude_yn = "N";
				var allocationId = $(this).attr("allocationId");
				var paymentDivision = "01";
				
				$(this).parent().next().find("input").val($(this).parent().next().next().next().find("input").val());		
				$(this).parent().next().next().find("input").val(0);
				var amount = Number(getNumberOnly($(this).parent().next().find("input").val()));
				var vat = $(this).parent().next().next().find("input").val();
				var price = amount;
				
				if($(this).parent().attr("batchStatus") != "P"){
					$.ajax({ 
						type: 'post' ,
						url : "/allocation/updateAmount.do" ,
						dataType : 'json' ,
						async : false,
						data : {
							allocationId :allocationId, 
							paymentDivision : paymentDivision,
							amount :  amount,
							vat :  vat,
							price :  price,
							vatIncludeYn : vat_include_yn,
							vatExcludeYn : vat_exclude_yn
						},
						success : function(data, textStatus, jqXHR)
						{
							var result = data.resultCode;
							var resultData = data.resultData;
							if(result == "0000"){
								//window.location.reload();
							}else if(result == "0001"){
								alert("수정 하는데 실패 하였습니다.");
							}
						} ,
						error : function(xhRequest, ErrorText, thrownError) {
						}
					});
				}else{
					
					$.ajax({ 
						type: 'post' ,
						url : "/allocation/updateAmountForPickup.do" ,
						dataType : 'json' ,
						async : false,
						data : {
							allocationId :allocationId, 
							paymentDivision : paymentDivision,
							vatIncludeYn : vat_include_yn,
							vatExcludeYn : vat_exclude_yn
						},
						success : function(data, textStatus, jqXHR)
						{
							var result = data.resultCode;
							var resultData = data.resultData;
							if(result == "0000"){
								//window.location.reload();
							}else if(result == "0001"){
								alert("수정 하는데 실패 하였습니다.");
							}
						} ,
						error : function(xhRequest, ErrorText, thrownError) {
						}
					});
					
				}
				
			
			}else{
				
			}
			
		 });
	}
	
	window.location.reload();
	
}
	
	
	
function toggleIncludeVat(obj,allocationId,paymentDivision){
	
	var vat_include_yn = "";
	var vat_exclude_yn = "";
	
	if($(obj).is(":checked")){
		
		if($(obj).next().is(":checked")){
			$(obj).next().prop("checked","");
		}
		
		//alert($(obj).parent().attr("batchStatus"));
		
		if($(obj).parent().attr("batchStatus") != "P"){
			var price = Math.round(Number(getNumberOnly($(obj).parent().next().find("input").val()))/1.1);
			$(obj).parent().next().find("input").val(comma(price))
			var vat = Math.round(Number(price/10)); 
			$(obj).parent().next().next().find("input").val(comma(vat));
			var total = price+vat;
			if(total%10 != 0){
				total = total-(total%10);
			}
			$(obj).parent().next().next().next().find("input").val(comma(total));	
		}
		
		vat_include_yn = "Y";
		vat_exclude_yn = "N";
	}else{
		if($('input:checkbox[id="checkIncludeAll"]').is(":checked")){
			$('input:checkbox[id="checkIncludeAll"]').prop("checked","");
		}
		
		if($(obj).parent().attr("batchStatus") != "P"){
			$(obj).parent().next().find("input").val($(obj).parent().next().next().next().find("input").val());		
			$(obj).parent().next().next().find("input").val(0);	
		}
		
		vat_include_yn = "N";
		vat_exclude_yn = "N";
	}
	
	if($(obj).parent().attr("batchStatus") != "P"){
		$.ajax({ 
			type: 'post' ,
			url : "/allocation/updateAmount.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				allocationId :allocationId, 
				paymentDivision : paymentDivision,
				amount :  getNumberOnly($(obj).parent().next().find("input").val()),
				vat :  getNumberOnly($(obj).parent().next().next().find("input").val()),
				price :  getNumberOnly($(obj).parent().next().next().next().find("input").val()),
				vatIncludeYn : vat_include_yn,
				vatExcludeYn : vat_exclude_yn
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					//window.location.reload();
				}else if(result == "0001"){
					alert("수정 하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
	}else{
		$.ajax({ 
			type: 'post' ,
			url : "/allocation/updateAmountForPickup.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				allocationId :allocationId, 
				paymentDivision : paymentDivision,
				vatIncludeYn : vat_include_yn,
				vatExcludeYn : vat_exclude_yn
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					window.location.reload();
				}else if(result == "0001"){
					alert("수정 하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
	}
	
	
	
	
	
}
	
	
	
function checkExcludeAll(){

	var vat_exclude_yn = "";
	var vat_include_yn = "";
	if($('input:checkbox[id="checkExcludeAll"]').is(":checked")){
		$('input:checkbox[name="forExcludeVAT"]').each(function(index,element) {
			
			//부가세 포함 상태 였는지 확인
			if($(this).prev().is(":checked")){
				//포함 상태 였으면 미포함 상태로 바꿔 준 후 진행한다.
				$(this).prev().prop("checked","");
				$(this).parent().next().find("input").val($(this).parent().next().next().next().find("input").val());
			}
			
			if($(this).is(":checked")){
				
			}else{
				
				
				vat_exclude_yn = "Y";
				vat_include_yn = "N";
				var allocationId = $(this).attr("allocationId");
				var paymentDivision = "01";
				
				if($(this).parent().attr("batchStatus") != "P"){
					
					$(this).prop("checked","true");
					var amount = Number(getNumberOnly($(this).parent().next().find("input").val()));
					var vat = Math.round(Number(amount/10)); 
					$(this).parent().next().next().find("input").val(comma(vat));
					var price = amount+vat;
					$(this).parent().next().next().next().find("input").val(comma(price));
					
					$.ajax({ 
						type: 'post' ,
						url : "/allocation/updateAmount.do" ,
						dataType : 'json' ,
						async : false,
						data : {
							allocationId :allocationId, 
							paymentDivision : paymentDivision,
							amount :  amount,
							vat :  vat,
							price :  price,
							vatExcludeYn : vat_exclude_yn,
							vatIncludeYn : vat_include_yn
						},
						success : function(data, textStatus, jqXHR)
						{
							var result = data.resultCode;
							var resultData = data.resultData;
							if(result == "0000"){
								//window.location.reload();
							}else if(result == "0001"){
								alert("수정 하는데 실패 하였습니다.");
							}
						} ,
						error : function(xhRequest, ErrorText, thrownError) {
						}
					});
					
					
				}else{
					
					$.ajax({ 
						type: 'post' ,
						url : "/allocation/updateAmountForPickup.do" ,
						dataType : 'json' ,
						async : false,
						data : {
							allocationId :allocationId, 
							paymentDivision : paymentDivision,
							vatIncludeYn : vat_include_yn,
							vatExcludeYn : vat_exclude_yn
						},
						success : function(data, textStatus, jqXHR)
						{
							var result = data.resultCode;
							var resultData = data.resultData;
							if(result == "0000"){
								//window.location.reload();
							}else if(result == "0001"){
								alert("수정 하는데 실패 하였습니다.");
							}
						} ,
						error : function(xhRequest, ErrorText, thrownError) {
						}
					});
					
					
				}
				
				
			}
			
			
		 });	
	}else{
		$('input:checkbox[name="forExcludeVAT"]').each(function(index,element) {
			
			if($(this).is(":checked")){
				
				vat_exclude_yn = "N";
				vat_include_yn = "N";
				var allocationId = $(this).attr("allocationId");
				var paymentDivision = "01";
				
				if($(this).parent().attr("batchStatus") != "P"){
					
					$(this).prop("checked","");
					$(this).parent().next().next().find("input").val(0);
					$(this).parent().next().next().next().find("input").val($(this).parent().next().find("input").val());	
					
					var amount = Number(getNumberOnly($(this).parent().next().find("input").val()));
					var vat = $(this).parent().next().next().find("input").val();
					//var price = $(this).parent().next().next().next().find("input").val();
					var price = amount;
					
					$.ajax({ 
						type: 'post' ,
						url : "/allocation/updateAmount.do" ,
						dataType : 'json' ,
						async : false,
						data : {
							allocationId :allocationId, 
							paymentDivision : paymentDivision,
							amount :  amount,
							vat :  vat,
							price :  price,
							vatExcludeYn : vat_exclude_yn,
							vatIncludeYn : vat_include_yn
						},
						success : function(data, textStatus, jqXHR)
						{
							var result = data.resultCode;
							var resultData = data.resultData;
							if(result == "0000"){
								//window.location.reload();
							}else if(result == "0001"){
								alert("수정 하는데 실패 하였습니다.");
							}
						} ,
						error : function(xhRequest, ErrorText, thrownError) {
						}
					});
				}else{
					
					$.ajax({ 
						type: 'post' ,
						url : "/allocation/updateAmountForPickup.do" ,
						dataType : 'json' ,
						async : false,
						data : {
							allocationId :allocationId, 
							paymentDivision : paymentDivision,
							vatIncludeYn : vat_include_yn,
							vatExcludeYn : vat_exclude_yn
						},
						success : function(data, textStatus, jqXHR)
						{
							var result = data.resultCode;
							var resultData = data.resultData;
							if(result == "0000"){
								//window.location.reload();
							}else if(result == "0001"){
								alert("수정 하는데 실패 하였습니다.");
							}
						} ,
						error : function(xhRequest, ErrorText, thrownError) {
						}
					});
					
				}
				
				
			}else{
				
			}
			
		 });
	}
	
	window.location.reload();
}
	
	
	
function toggleExcludeVat(obj,allocationId,paymentDivision){
	
	var vat_exclude_yn = "";
	var vat_include_yn = "";
	if($(obj).is(":checked")){
		
		if($(obj).parent().attr("batchStatus") != "P"){
			//부가세 포함 상태 였는지 확인
			if($(obj).prev().is(":checked")){
				//포함 상태 였으면 미포함 상태로 바꿔 준 후 진행한다.
				$(obj).prev().prop("checked","");
				$(obj).parent().next().find("input").val($(obj).parent().next().next().next().find("input").val());
			}
			var price = Number(getNumberOnly($(obj).parent().next().find("input").val()));
			var vat = Math.round(Number(price/10)); 
			$(obj).parent().next().next().find("input").val(comma(vat));
			var total = price+vat;
			$(obj).parent().next().next().next().find("input").val(comma(total));	
		}
		
		vat_include_yn = "N";
		vat_exclude_yn = "Y";
	}else{
		if($('input:checkbox[id="checkExcludeAll"]').is(":checked")){
			$('input:checkbox[id="checkExcludeAll"]').prop("checked","");
		}
		
		if($(obj).parent().attr("batchStatus") != "P"){
			$(obj).parent().next().next().find("input").val(0);
			$(obj).parent().next().next().next().find("input").val($(obj).parent().next().find("input").val());	
		}
		
		vat_include_yn = "N";
		vat_exclude_yn = "N";
	}
	
	
	
	if($(obj).parent().attr("batchStatus") != "P"){
		$.ajax({ 
			type: 'post' ,
			url : "/allocation/updateAmount.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				allocationId :allocationId, 
				paymentDivision : paymentDivision,
				amount :  getNumberOnly($(obj).parent().next().find("input").val()),
				vat :  getNumberOnly($(obj).parent().next().next().find("input").val()),
				price :  getNumberOnly($(obj).parent().next().next().next().find("input").val()),
				vatIncludeYn : vat_include_yn,
				vatExcludeYn : vat_exclude_yn
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					//window.location.reload();
				}else if(result == "0001"){
					alert("수정 하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
	}else{
		
		$.ajax({ 
			type: 'post' ,
			url : "/allocation/updateAmountForPickup.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				allocationId :allocationId, 
				paymentDivision : paymentDivision,
				vatIncludeYn : vat_include_yn,
				vatExcludeYn : vat_exclude_yn
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					window.location.reload();
				}else if(result == "0001"){
					alert("수정 하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
		
		
	}
	
	
}
	
	
function checkAllDecide(){
	
	if($('input:checkbox[id="checkForDecide"]').is(":checked")){
		$('input:checkbox[name="forDecide"]').each(function(index,element) {
			$(this).prop("checked","true");
		});
	}else{
		$('input:checkbox[name="forDecide"]').each(function(index,element) {
			$(this).prop("checked","");
		});
	}
	
}
	
	
	
	
function move(location){
	
	
	window.location.href = "/allocation/"+location+".do";
	
}	
	
	
	

function decideStatus(status){
	
	document.location.href = "/account/customerCalDetail.do?&carrierType=${paramMap.carrierType}&startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&searchDateType=${paramMap.searchDateType}&customerId=${paramMap.customerId}&searchType=${paramMap.searchType}&searchWord=${paramMap.searchWord}&decideStatus="+status;
}


function sendSocketMessage(){
	
	$.ajax({ 
		type: 'post' ,
		url : "http://52.78.153.148:8080/allocation/sendSocketMessage.do" ,
		dataType : 'json' ,
		data : {
			allocationId : 'ALO3df74bd0105046bfbdce51ab0e64927c',
			driverId : "sourcream"
		},
		success : function(data, textStatus, jqXHR)
		{
			var result = data.resultCode;
			var resultData = data.resultData;
			if(result == "0000"){
				//alert("성공");
			}else if(result == "0001"){
			
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
}
	


function goListPage(){
	document.location.href = "/account/customerCal.do";
}


var order = "${paramMap.order}";
var forOrder = "${paramMap.forOrder}";
function sortBy(gubun){

			
	 if(order == "" || order == "desc"){
		order = "asc";
	}else{
		order = "desc";
	}
	
	var loc = document.location.href;
	var str = "";
	if(loc.indexOf("?") > -1){
		//forOrder 가 있는경우 ㅎㅎ
		if(loc.indexOf("forOrder") > -1){
			var queryString = loc.split("?");
			var query = queryString[1].split("&");
			
			for(var i = 0; i < query.length; i++){
				if(query[i].indexOf("forOrder") > -1){
					query[i] = "forOrder="+gubun+"%5E"+order;
				}
			}
			for(var i = 0; i < query.length; i++){
				if(query[i] != ""){
					str += "&"+query[i];	
				}
			}
			document.location.href = queryString[0]+"?"+str;
		}else{
			str="&forOrder="+gubun+"%5E"+order;
			document.location.href = loc+str;
		}
		
	}else{
		str="?&forOrder="+gubun+"%5E"+order;
		document.location.href = loc+str;
	}
	
}


function goDeleteDebtorCreditor(debtorCreditorId){

	if (confirm("매출 상세 내역이 없습니다. 삭제하시겠습니까?")) {
		$.ajax({ 
			type: 'post' ,
			url : "/account/deleteDebtorCreditorId.do" ,
			dataType : 'json' ,
			data : {

				debtorCreditorId : debtorCreditorId,		
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("삭제되었습니다.");
					history.go(-1);
					//alert("성공");
				}else if(result == "0001"){
				
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
	}else{

		}

	
}



	
 </script>




        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">매출 매입관리</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">매출상세내역</a></li>
                </ul>
            </div>

            <div class="up-dl clearfix">
                
                <%-- <form name="searchForm" method="get" action="customerCalDetail.do">
                	<div class="date-picker">
                		<select name="searchDateType"  id="searchDateType">
							<option value="" <c:if test='${paramMap.searchDateType eq "" }'> selected="selected"</c:if> >선택</option>
							<option value="S" <c:if test='${paramMap.searchDateType eq "S" }'> selected="selected"</c:if> >의뢰일</option>
							<option value="D" <c:if test='${paramMap.searchDateType eq "D" }'> selected="selected"<</c:if> >출발일</option>
						</select>
	                	<input style="width:150px;" id="startDt" placeholder="검색 시작일" name="startDt" type="text" class="datepick" value="${paramMap.startDt}"> ~ <input style="width:150px;" id="endDt" placeholder="검색 종료일" name="endDt"  type="text" class="datepick" value="${paramMap.endDt}">
	                	<input type="hidden" name="customerId" value="${paramMap.customerId}">
	                </div>
	                <div class="upload-btn" style="float:right;">
	                    
	                </div>
					<div class="btns-submit">	
						<div class="right">
								
							<a class="btn-gray" href="javascript:" onclick=" encodeURI($(this).prev().val()); document.searchForm.submit()"><input type="button" value="검색"></a>
						</div>
					</div>			
				</form> --%>
                
                
            </div>

            <div class="dispatch-btn-container">
                <!-- <div class="dispatch-btn">
                    <i id="downArrow" class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div> -->
                
                <%-- <c:if test="${complete == null || complete != 'Y'}"> --%>
                	<%-- <c:if test="${forOpen == null || forOpen != 'N'}">
						<div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">신규배차입력</div>
					</c:if> --%>
					<%-- <c:if test="${forOpen != null && forOpen == 'N'}"> --%>
						<div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">매출 상세 내역</div>
					<%-- </c:if>
				</c:if> --%>
                
                <%-- <c:if test="${complete != null && complete == 'Y'}">
					<div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">완료배차</div>					
				</c:if> --%>
                
            </div>
        </section>

        <div class="dispatch-wrapper">
            <%-- <jsp:include page="common-form.jsp"></jsp:include> --%>
            <section class="dispatch-bottom-content">
			</section>
			
			<!-- <div class="confirmation" style="min-height:0px; text-align:left; margin-top:-20px; margin-left:10px; margin-bottom:30px;">
                    <div class="confirm">
                    	<a style="cursor:pointer;" onclick="javascript:batchDecide();">일괄확정</a>
                    </div>
                    <div class="confirm">
                    	<a style="cursor:pointer;" onclick="javascript:billPublishRequest();">세금계산서 발행 요청</a>
                    </div>
                    <div class="confirm">
                    	<a style="cursor:pointer;" onclick="javascript:sendMail();">메일전송</a>
                    </div>
                </div> -->
			
			
			<div id="bottom-table">
	            <section  class="bottom-table" style="width:1600px; margin-left:10px;">
	                <table class="article-table forToggle" id="dataTable" style="width:100%;">
	                    <colgroup>
	                        <col width="100px;">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="150px;">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                    </colgroup>
	                    <thead>
	                        <tr>
	                        	<!-- <td style="text-align:center;"><input type="checkbox" id="checkForDecide" onclick="javascript:checkAllDecide();" ></td> -->
	                            <td>출발일</td>
	                            <td>고객명</td>
	                            <td>배차구분</td>
	                            <!-- <td>결제방법</td> -->
	                            <td style="cursor:pointer;" onclick="javascript:sortBy('B');">출발지<c:if test='${paramMap.forOrder eq "B" }'><c:if test='${paramMap.order eq "asc" }'><img style="margin-left:10px;" src="/img/arrow-up.png" alt=""></c:if><c:if test='${paramMap.order eq "desc" }'><img style="margin-left:10px;" src="/img/arrow-down.png" alt=""></c:if></c:if></td>
	                            <td style="cursor:pointer;" onclick="javascript:sortBy('C');">하차지<c:if test='${paramMap.forOrder eq "C" }'><c:if test='${paramMap.order eq "asc" }'><img style="margin-left:10px;" src="/img/arrow-up.png" alt=""></c:if><c:if test='${paramMap.order eq "desc" }'><img style="margin-left:10px;" src="/img/arrow-down.png" alt=""></c:if></c:if></td>
	                            <td>결제방법</td>
	                            <td>증빙구분</td>
	                            <td>차종</td>
	                            <td style="cursor:pointer;" onclick="javascript:sortBy('A');">차대번호<c:if test='${paramMap.forOrder eq "A" }'><c:if test='${paramMap.order eq "asc" }'><img style="margin-left:10px;" src="/img/arrow-up.png" alt=""></c:if><c:if test='${paramMap.order eq "desc" }'><img style="margin-left:10px;" src="/img/arrow-down.png" alt=""></c:if></c:if></td>
	                            <td>차량번호</td>
	                            <!-- <td style="text-align:center;">부가세&nbsp;<input type="checkbox" id="checkIncludeAll" onclick="javascript:checkIncludeAll();" >포함&nbsp;&nbsp;<input type="checkbox" id="checkExcludeAll" onclick="javascript:checkExcludeAll();" >별도</td> -->
	                            <td>공급가액</td>
	                            <td>부가세액</td>
	                            <td>총액</td>
	                        </tr>
	                    </thead>
	                    <tbody id="rowData">
	                    
	                    	<c:forEach var="data" items="${listData}" varStatus="status">
								<tr class="ui-state-default" list-order="${data.list_order}" allocationId="${data.allocation_id}">
	                            <%-- <td class="showToggle"><input type="checkbox" name="forBatch" allocationId="${data.allocation_id}"></td> --%>
	                            <%-- <td style="text-align:center;"><input type="checkbox" name="forDecide" allocationId="${data.allocation_id}"></td> --%>
	                            <td style="<c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>">${data.departure_dt}</td>
	                            <td style="cursor:pointer; <c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>" onclick="javascript:selectList('${data.allocation_id}');">${data.customer_name}</td>
	                            <td style="<c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>"><c:if test="${data.batch_status == 'P'}">픽업</c:if><c:if test="${data.batch_status != 'P'}"><c:if test="${data.carrier_type == 'S'}">셀프</c:if><c:if test="${data.carrier_type == 'C'}">캐리어</c:if></c:if></td>
	                            <%-- <td style="<c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>">${data.payment_division}</td> --%>
	                            <td style="<c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>">${data.departure}</td>
	                            <td style="<c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>">${data.arrival}</td>
	                            <td style="<c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>">${data.payment_kind}</td>
	                            <td style="<c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>">${data.billing_division}</td>
	                            <td style="<c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>">${data.car_kind}</td>
	                            <td style="<c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>">${data.car_id_num}</td>
	                            <td style="<c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>">${data.car_num}</td>
	                            <%-- <td style="text-align:right;" batchStatus ="${data.batch_status}"><input type="checkbox"  <c:if test='${data.vat_include_yn eq "Y" }'> checked</c:if> onclick="javascript:toggleIncludeVat(this,'${data.allocation_id}','01');" name="forIncludeVAT" allocationId="${data.allocation_id}">포함&nbsp;&nbsp;<input type="checkbox" <c:if test='${data.vat_exclude_yn eq "Y" }'> checked</c:if> onclick="javascript:toggleExcludeVat(this,'${data.allocation_id}','01');" name="forExcludeVAT" allocationId="${data.allocation_id}">별도</td> --%>
	                            
		                        <td style="text-align:right;">${data.sales_total}</td>
		                        <td style="text-align:right;">${data.vat}</td>
		                        <td style="text-align:right;">${data.price}</td>
	                            
	                        </tr>
							</c:forEach>
	                    </tbody>
	                </table>
	                <div class="table-pagination text-center">
	                    <ul class="pagination">
	                    	<html:paging uri="/account/debtorCreditorDetail.do" forGroup="&debtorCreditorId=${paramMap.debtorCreditorId}" frontYn="N" />
	                       <!--  <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
	                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
	                        <li class="curr-page"><a href="#">1</a></li>
	                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
	                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li> -->
	                    </ul>
	                </div>                
	            </section>
            	<div class="confirmation">
            	<!--goListPage()  -->
            		<c:if test='${paramMap.decideStatus eq "N" }'>
            			<div class="confirm">
	                    	<a style="cursor:pointer;" onclick="javascript:goListPage();">목록으로</a>
	                    </div>
	                   <div class="confirm">
	                    	<a style="cursor:pointer;" onclick="javascript:batchDecide('Y');">일괄확정</a>
	                    </div>
	                    <div class="cancel">
	                    	<a style="cursor:pointer;" onclick="javascript:sendMail();">메일전송</a>
	                    </div>
	                    <div class="cancel">
	                    	<a style="cursor:pointer;" onclick="javascript:excelDownload();">엑셀다운로드</a>
	                    </div>
                    </c:if>
                    <c:if test='${paramMap.decideStatus eq "Y" }'>
                    	<div class="confirm">
	                    	<a style="cursor:pointer;" onclick="javascript:goListPage();">목록으로</a>
	                    </div>
                    	<div class="confirm">
	                    	<a style="cursor:pointer;" onclick="javascript:batchDecide('N');">수정/확정취소</a>
	                    </div>
	                    <div class="cancel">
	                    	<a style="cursor:pointer;" onclick="javascript:billPublishRequest();">세금계산서 발행 요청</a>
	                    </div>
                    </c:if>
                    <c:if test='${paramMap.decideStatus eq "A" }'>
                    	<div class="confirm">
	                    	<a style="cursor:pointer;" onclick="javascript:goListPage();">목록으로</a>
	                    </div>
	                    <div class="confirm">
	                    	<a style="cursor:pointer;" onclick="javascript:excelDownload();">엑셀다운로드</a>
	                    </div>
                    </c:if>
                    
                    
                    <c:if test="${deleteYN eq 'Y'}">
	                    <div class="confirm">
		                   	<a style="cursor:pointer;" onclick="javascript:goDeleteDebtorCreditor('${paramMap.debtorCreditorId}');">삭제</a>
		                </div>
	                </c:if>
	                
	                
                  </div>
            
            
            
            	<!-- <div class="confirmation">
                    <div class="cancel">
                        <a style="cursor:pointer;" onclick="javascript:batchStatus('cancel');">일괄취소</a>
                    </div>
                    <div class="confirm">
                        <a style="cursor:pointer;" onclick="javascript:batchStatus('complete');">일괄완료</a>
                    </div>
                </div> -->
            </div>
        </div>        
        <!-- <iframe style="width: 980px; height:10000px; border: none;" frameBorder="0" id="happyboxFrame" scrolling="no" src="https://www.happyalliance-happybox.org/Bridge?v=param"></iframe>​    -->
       <script>
		if("${userMap.control_grade}" == "01"){
			$('#dataTable').SetEditable({
            	columnsEd: "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15",
            	 onEdit: function(row) {updateAllocaion(row)},  
            	 onDelete: function() {},  
            	 onBeforeDelete: function() {}, 
            	 onAdd: function() {}  
            });
			y
		}          
            </script>

