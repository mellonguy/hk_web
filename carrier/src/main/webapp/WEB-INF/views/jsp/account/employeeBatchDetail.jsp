<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/js/bootstable.js"></script>
<script type="text/javascript"> 
var oldListOrder = "";
var allocationId = "";

$(document).ready(function(){
   //forOpen

   
   $('#dataTable tr:odd td').css("backgroundColor","#f9f9f9");
   
   $("#startDt").datepicker({
      dateFormat : "yy-mm-dd",
      onSelect : function(date){
         //$("#endDt").focus();
         setTimeout(function(){
            $("#endDt").focus();  
           }, 50);
      }
   });
   

   
});

function updateAllocaion(row){
   var inputDt = "";
    var carrierType = "";
    var distanceType = "";
    var departureDt = "";
    var departureTime = "";
    var customerName = "";
    var carKind = "";
    var carIdNum = "";
    var carNum  = "";
    var departure = "";
    var arrival  = "";
    var driverName = "";
    var carCnt  = "";
    var amount   = "";
    var paymentKind = "";
   var allocId = $(row).attr("allocationId");
   if(confirm("수정 하시겠습니까?")){
      $(row).find("td").each(function(index,element){
         if(index==1){
            inputDt = $(this).html();
         }
         if(index==2){
            if($(this).html() == "셀프"){
               carrierType = "S";   
            }else if($(this).html() == "캐리어"){
               carrierType = "C";
            }else{
               alert("배차구분을 확인 하세요.");
               return false;
            }
         }
         if(index==3){
            if($(this).html() == "시내"){
               distanceType = "0";   
            }else if($(this).html() == "시외"){
               distanceType = "1";
            }else{
               alert("거리구분을 확인 하세요.");
               return false;
            }
         }
         if(index==4){
            departureDt = $(this).html();
         }
         if(index==5){
            departureTime = $(this).html();
         }
         if(index==6){
            customerName = $(this).html();
         }
         if(index==7){
            carKind = $(this).html();
         }
         if(index==8){
            carIdNum = $(this).html();
         }
         if(index==9){
            carNum = $(this).html();
         }
         if(index==10){
            departure = $(this).html();
         }
         if(index==11){
            arrival = $(this).html();
         }
         if(index==12){
            driverName = $(this).html();
         }
         if(index==13){
            carCnt = $(this).html();
         }
         if(index==14){
            amount = $(this).html();
         }
         if(index==15){
            paymentKind = $(this).html();
         }
         });

      if(carrierType == "" || distanceType == ""){
         return false;   
      }
      
       $.ajax({ 
            type: 'post' ,
            url : "/allocation/update-allocation.do" ,
            dataType : 'json' ,
            data : {
               inputDt : inputDt,
               carrierType : carrierType,
               distanceType : distanceType,
               departureDt : departureDt,
               departureTime : departureTime,
               customerName : customerName,
               carKind : carKind,
               carIdNum : carIdNum,
               carNum : carNum,
               departure : departure,
               arrival : arrival,
               driverName : driverName,
               carCnt : carCnt,
               amount : amount,
               paymentKind : paymentKind,
               allocationId : allocId
            },
            success : function(data, textStatus, jqXHR)
            {
               var result = data.resultCode;
               if(result == "0000"){
                  alert("변경 되었습니다.");
         //         document.location.href = "/allocation/combination.do";
               }else if(result == "0001"){
                     alert("변경 하는데 실패 하였습니다.");
                  }
            } ,
            error : function(xhRequest, ErrorText, thrownError) {
            }
         }); 
      
   }

}

function getListId(obj){
   $('html').scrollTop(0);
   selectList($(obj).parent().parent().parent().attr("allocationId"));
   
}

var driverArray = new Array();
function selectList(id){
   
   var searchWord = encodeURI('${paramMap.searchWord}');
   document.location.href="/allocation/allocation-view.do?&searchDateType=${paramMap.searchDateType}&startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&searchType=${paramMap.searchType}&cPage=${paramMap.cPage}&allocationStatus=${paramMap.allocationStatus}&location=${paramMap.location}&allocationId="+id+"&searchWord="+searchWord;
   
}   
   
function batchStatus(status){
   
   var id = "";
   var total = $('input:checkbox[name="forBatch"]:checked').length;
   
   if(total == 0){
      alert("수정할 목록이 선택 되지 않았습니다.");
      return false;
   }else{
      $('input:checkbox[name="forBatch"]:checked').each(function(index,element) {
            if(this.checked){//checked 처리된 항목의 값
               id+=$(this).attr("allocationId");
               if(index<total-1){
                  id += ","; 
                  } 
            }
       });   
      updateAllocationStatus(status,id);
   }
}   

function updateAllocationStatus(status,id){
   
   var msg = "";
   if(status == "cancel"){
      msg = "취소";
   }else if(status == "complete"){
      msg = "완료";
   }
   
   if(confirm(msg+"하시겠습니까?")){
      $.ajax({ 
         type: 'post' ,
         url : "/allocation/updateAllocationStatus.do" ,
         dataType : 'json' ,
         data : {
            status : status,
            allocationId : id
         },
         success : function(data, textStatus, jqXHR)
         {
            var result = data.resultCode;
            var resultData = data.resultData;
            if(result == "0000"){
               alert(msg+"되었습니다.");
               document.location.href = "/allocation/combination.do";
            }else if(result == "0001"){
               alert(msg+"하는데 실패 하였습니다.");
            }
         } ,
         error : function(xhRequest, ErrorText, thrownError) {
         }
      });   
   }
   
}   
   
function checkAll(){

   if($('input:checkbox[id="checkAll"]').is(":checked")){
      $('input:checkbox[name="forBatch"]').each(function(index,element) {
         $(this).prop("checked","true");
       });   
   }else{
      $('input:checkbox[name="forBatch"]').each(function(index,element) {
         $(this).prop("checked","");
       });
   }
}
   
   
   
function move(location){
   
   
   window.location.href = "/allocation/"+location+".do";
   
}   
   
   
function excelDownload(carrierType){
   
   
   if(confirm("다운로드 하시겠습니까?")){
      document.location.href = "/allocation/excel_download.do?&carrierType="+carrierType+"&searchDateType="+$("#searchDateType").val()+"&startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val()+"&searchType="+$("#searchType").val()+"&searchWord="+encodeURI($("#searchWord").val());      
   }
   
   
}   
   
   


function sendSocketMessage(){
   
   $.ajax({ 
      type: 'post' ,
      url : "http://52.78.153.148:8080/allocation/sendSocketMessage.do" ,
      dataType : 'json' ,
      data : {
         allocationId : 'ALO3df74bd0105046bfbdce51ab0e64927c',
         driverId : "sourcream"
      },
      success : function(data, textStatus, jqXHR)
      {
         var result = data.resultCode;
         var resultData = data.resultData;
         if(result == "0000"){
            //alert("성공");
         }else if(result == "0001"){
         
         }
      } ,
      error : function(xhRequest, ErrorText, thrownError) {
      }
   });
   
}





   function pickUpSet(status){

   
      alert("ddddddd");

      //var allocationId=$ ('input:checkbox[name="forBatch"]:checked').attr("allocationId");
      
      var allocationIdArr = new Array(); 
      
                        
      var bStatus = true;
      var choose = $('input:checkbox[name="forBatch"]:checked').length;
      
      if(choose ==0){
         alert("체크박스를 눌러서 선택해주세요");
         return false;
   
      }else if (choose ==1){
         alert("하나를 더 선택해주세요");
         return false;
      
      }else if (choose > 3){
         alert("관라자에게 문의하세요");
         return false;
      }

      if(status =="P"){
      
         $('input:checkbox[name="forBatch"]:checked').each(function(index,element) {

             if ($(this).attr('batchStatus') != "N" ) {
               alert("이미 등록된 배차건입니다.");
               bStatus  = false;
               
            }else {
               allocationIdArr.push($(this).attr("allocationId"));
               
/* 
               allocationId  += $(this).attr("allocationId");
               if(index<choose-1){
                    allocationId += ",";
               }
 */            
   
            } 

         });

         alert("아이디는 "+allocationIdArr + "상태는" +status );
      }
      
       if(bStatus){
          Test04(allocationIdArr,status); 
      }
      
      

   
}


   

   function Test04(allocationIdArr,status){

      if(confirm("픽업으로 묶으시겠습니까??")){
         
   $.ajax({
      
      type: 'post' ,
      url : "/Test04/Test04.do" ,
      traditional : true,
      dataType : 'json' ,
      data : {
         
         allocationIdArr : allocationIdArr,
         batchStatus : status
         
      },
      
      success : function(data, textStatus, jqXHR)
      
      {
         var result = data.resultCode;
         if(result == "0000"){
            alert("픽업으로 설정되었습니다/");
         }else if(result == "0001"){
             alert("실패하였습니다. 관리자에게 문의하세요 ")
         }
      } ,
      error : function(xhRequest, ErrorText, thrownError) {
      }
   });

      }else{
         alert("취소됨");
         
         return false;
      }   
}



var order = "${order}";
function sortby(gubun){

   if(order == ""){
      order = "asc";
   }else if(order == "asc"){
      order = "desc";
   }else if(order == "desc"){
      order = "";
   }
   
   var loc = document.location.href;
   var str = "";
   if(loc.indexOf("?") > -1){
      //forOrder 가 있는경우 ㅎㅎ
      if(loc.indexOf("forOrder") > -1){
         var queryString = loc.split("?");
         var query = queryString[1].split("&");
         
         for(var i = 0; i < query.length; i++){
            if(query[i].indexOf("forOrder") > -1){
               query[i] = "forOrder="+gubun+"%5E"+order;
            }
         }
         for(var i = 0; i < query.length; i++){
            if(query[i] != ""){
               str += "&"+query[i];   
            }
         }
         document.location.href = queryString[0]+"?"+str;
      }else{
         str="&forOrder="+gubun+"%5E"+order;
         document.location.href = loc+str;
      }
      
   }else{
      str="?&forOrder="+gubun+"%5E"+order;
      document.location.href = loc+str;
   }
   
}





function allocationpage(allocation_id){


   document.location.href ="/allocation/allocation-view.do?&allocationId="+allocation_id;

}



   
 </script>


<div class="modal-field">
            <div class="modal-box">
                <h3 class="text-center">기사리스트</h3>
            <div class="modal-table-container">
                <table class="article-table">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <!-- <td>소유주</td> -->
                            <td>기사명</td>
                            <td>연락처</td>
                            <td>차량번호</td>
                            <td>차종</td>
                        </tr>
                    </thead>
                    <tbody id="driverSelectList">
                       <c:forEach var="data" items="${driverList}" varStatus="status" >
                          
                          <tr class="ui-state-default" style="cursor:pointer;" driverId="${data.driver_id}" onclick="javascript:setDriver('${data.driver_id}','${data.driver_name}','${data.car_kind}');"> 
                               <%-- <td>${data.driver_owner}</td> --%>
                               <td>${data.driver_name}</td>
                               <td>${data.phone_num}</td>
                               <td>${data.car_num}</td>
                               <td>${data.car_kind}</td>
                           </tr>
                  </c:forEach>
                    </tbody>
                </table>
                </div>
                <div class="confirmation">
                    <div class="confirm">
                        <input type="button" value="취소" name="">
                    </div>
                    
                    
                    
                </div>
                
                    <div class="pickupbox">
                    <div class="confirm">
                        <input type="button" value="취소" name="">
                    </div>
                </div>
            </div>
        </div>
       <%-- <section class="side-nav">
            <ul>
                <li class="<c:if test="${complete == ''}">active</c:if>"><a href="/allocation/combination.do">신규배차입력</a></li>
                <li><a href="/allocation/self.do">셀프 배차  </a></li>
                <li><a href="/allocation/self.do?reserve=N">셀프 예약</a></li>
                <li><a href="/allocation/self.do?reserve=Y">셀프 배차 현황</a></li>
                <li><a href="/allocation/carrier.do">캐리어 배차</a></li>
                <li><a href="/allocation/carrier.do?reserve=N">캐리어 예약</a></li>
                <li><a href="/allocation/carrier.do?reserve=Y">캐리어 배차 현황</a></li>
                <li class="<c:if test="${complete == 'Y'}">active</c:if>"><a href="/allocation/combination.do?forOpen=N&complete=Y">완료 배차</a></li>
            </ul>
        </section> --%>

        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">HOME</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">통합 배차 현황</a></li>
                </ul>
            </div>

        

            <div class="dispatch-btn-container">
                <!-- <div class="dispatch-btn">
                    <i id="downArrow" class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div> -->
                
                <%-- <c:if test="${complete == null || complete != 'Y'}"> --%>
                   <%-- <c:if test="${forOpen == null || forOpen != 'N'}">
                  <div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">신규배차입력</div>
               </c:if> --%>
               <%-- <c:if test="${forOpen != null && forOpen == 'N'}"> --%>
               <div style="">※<span style="color:#00f;"> 회사명</span>을 클릭하면 배차 상태 정보를 볼 수 있습니다.</div>
                  <div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">상세 보기</div>
               <%-- </c:if>
            </c:if> --%>
                
                <%-- <c:if test="${complete != null && complete == 'Y'}">
               <div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">완료배차</div>               
            </c:if> --%>
                
            </div>
        </section>

        <div class="dispatch-wrapper">
            <%-- <jsp:include page="common-form.jsp"></jsp:include> --%>
            <section class="dispatch-bottom-content">
         </section>
         <div id="bottom-table">
               <section  class="bottom-table" style="width:1600px; margin-left:10px;">
                  <!-- <p>차대번호를 기사님이 입력한 경우에는 붉은색으로 표시 되고 배차 직원이 입력 한 경우에는 파란색으로 표시 됩니다. </p> -->
                  <!-- <div style="color:#8B0000;">※차대번호를 기사님이 입력한 경우에는 붉은색으로 표시 되고 배차 직원이 입력 한 경우에는 파란색으로 표시 됩니다.</div> -->
                   <table class="article-table forToggle" id="dataTable" style="width:100%;">
                       <colgroup>
                          <%-- <col width="auto"> --%>
                           
                           <col width="auto;">
                           <col width="auto;">
                           <col width="auto;">
                           <col width="auto;">
                           <col width="auto;">
                           <col width="auto;">
                           <col width="auto;">
                           <col width="auto;">
                           <col width="auto;">
                           <col width="auto;">
                           <col width="auto;">
                           <col width="auto;">
                           
                         
                        
                        
                           <%-- <c:if test="${user.control_grade == '01' }">
                              <col width="auto">
                              <col width="auto">
                           </c:if> --%>
                       </colgroup>
                       <thead>
                           <tr>
                               <!-- <td class="showToggle">번호</td> -->
                               <td style ="text-align:center;">출발일</td>
                               <td style ="text-align:center;">거래처명</td>
                               <td style ="text-align:center;">등록자</td>
                               <td style ="text-align:center;">배차구분</td>
                               <td style ="text-align:center;" onclick="javascript:sortBy('B');">출발지</td>
                               <td style ="text-align:center;" onclick="javascript:sortBy('C');">하차지</td>
                               <td style ="text-align:center;">결제방법</td>
                               <td style ="text-align:center;">증빙구분</td>
                               <td style ="text-align:center;">차종</td>
                               <td style ="text-align:center;"onclick="javascript:sortBy('A');"> 차대번호 </td>
                               <td style ="text-align:center;">차량번호</td>
                               <td style ="text-align:center;">매출액</td>
                               <%-- <c:if test="${user.control_grade == '01' }">   
                                  <td class="showToggle"></td>
                                  <td>수정</td>
                               </c:if> --%>
                           </tr>
                       </thead>
                       
                       <tbody id="rowData">
                       
                          <c:forEach var="data" items="${employeelist}" varStatus="status">
                          
                        <tr class="ui-state-default" list-order="${data.list_order}"  allocationId="${data.allocation_id}">
                             
                             
                                <td style= "text-align:center;"<c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>>${data.departure_dt}</td>
                               <td style ="text-align:center;  cursor:pointer;" onclick="javascript:allocationpage('${data.allocation_id}')">${data.customer_name}</td>
                               <td style ="text-align:center;">${data.emp_name}</td>
                               <td style="text-align:center;"<c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>><c:if test="${data.batch_status == 'P'}">픽업</c:if><c:if test="${data.batch_status != 'P'}"><c:if test="${data.carrier_type == 'S'}">셀프</c:if><c:if test="${data.carrier_type == 'C'}">캐리어</c:if></c:if></td>
                                
                               <td style="text-align:center;"<c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>>${data.departure}</td>
                               <td style="text-align:center;"<c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>>${data.arrival}</td>
                             <td style="text-align:center;"<c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>>${data.payment_division}</td> 
                               <td style="text-align:center;"<c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>>${data.billing_division}</td>
                               
                               <td style="text-align:center;"<c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>>${data.car_kind}</td>
                               <td style="text-align:center;"<c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>>${data.car_id_num}</td>
                               <td style="text-align:center;"<c:if test="${data.batch_status == 'P'}">color:#F00;</c:if>>${data.car_num}</td>
                                <td style ="text-align:center;">${data.camount}</td>
                              
                              
                               
                        
                               <%-- <c:if test="${user.control_grade == '01' }">
                                  <td class="showToggle">
                                      <a href="#" class="table-driver-btn" title="아이콘 설명 1"><i class="fa fa-truck" aria-hidden="true"></i></a>
                                      <a href="#" class="table-btn" title="아이콘 설명 2"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
                                      <a style="cursor:pointer" title="취소" onclick="javascript:updateAllocationStatus('cancel','${data.allocation_id}');" class="table-x">
                                          <img src="/img/x-icon.png" alt="">
                                      </a>
                                      <a style="cursor:pointer"  title="완료" onclick="javascript:updateAllocationStatus('complete','${data.allocation_id}');" class="table-check">
                                          <img src="/img/check-icon.png" alt="">
                                      </a>
                                  </td>
                               </c:if> --%>
                           </tr>
                     </c:forEach>
                       
                           
   
   
                       </tbody>
                   </table>
                                   <!-- <div style="color:#8B0000;">※차대번호는 기사님이 입력한 경우에는 붉은색으로 표시 되고 배차 직원이 수정 한 경우에는 파란색으로 표시 됩니다.</div> -->
                <!--    <div style="">※차대번호는 기사님이 입력한 경우에는 <span style="color:#f00;">붉은색</span>으로 표시 되고 배차 직원이 수정 한 경우에는  <span style="color:#00f;">파란색</span>으로 표시 됩니다.</div> -->
            <%--        <div class="table-pagination text-center">
                    <ul class="pagination">
                          <html:paging uri="/account/employeeBatchDetail.do" forGroup="&customerId=${paramMap.customerId}&dateSearch=${paramMap.dateSearch}" frontYn="N" />
                   <html:paging uri="/account/viewBycustomerId.do" forGroup="&customerId=${paramMap.customerId}&dateSearch=${paramMap.dateSearch}" frontYn="N" /> 
                     
                     
                        <!-- <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                           <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                           <li class="curr-page"><a href="#">1</a></li>
                           <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                           <li><a href="#"><i class="fa fa-angle-double-right"><
                           /i></a></li>  -->
                       
                       
                       </ul> 
                   </div>    --%>      
                      <div class="table-pagination text-center">
                    <ul class="pagination">
                    	
                          <html:paging uri="/account/employeeBatchDetail.do" forGroup="&searchType=${paramMap.searchType}&searchWord=${paramMap.searchWord}&carrierType=${paramMap.carrierType}&paymentKind=${paramMap.paymentKind}&customerId=${paramMap.customerId}&register_id=${paramMap.register_id}&quarterType=${paramMap.quarterType}&yearType=${paramMap.yearType}&allocationStatus=${paramMap.allocationStatus}&payment=${paramMap.payment}" frontYn="N" />
                       
                       
                        
                       
              <!-- <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                           <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                           <li class="curr-page"><a href="#">1</a></li>
                           <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                           <li><a href="#"><i class="fa fa-angle-double-right"></i></a>
                           </li>  -->
                       
                       </ul> 
                   </div>         
                   
               </section>
            
                
            <!--     <div class="confirmation">
                    <div class="cancel">
                        <a style="cursor:pointer;" onclick="javascript:batchStatus('cancel');">일괄취소</a>
                    </div>
                    <div class="confirm">
                        <a style="cursor:pointer;" onclick="javascript:batchStatus('complete');">일괄완료</a>
                    </div>
                </div>   -->
            </div>
        </div>    
       
       
        
        <!-- <iframe style="width: 980px; height:10000px; border: none;" frameBorder="0" id="happyboxFrame" scrolling="no" src="https://www.happyalliance-happybox.org/Bridge?v=param"></iframe>     -->
       <script>
      if("${userMap.control_grade}" == "01"){
         $('#dataTable').SetEditable({
               columnsEd: "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15",
                onEdit: function(row) {updateAllocaion(row)},  
                onDelete: function() {},  
                onBeforeDelete: function() {}, 
                onAdd: function() {}  
            });
         
      }          
            </script>
