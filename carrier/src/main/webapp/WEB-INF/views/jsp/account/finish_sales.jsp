<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>
<%

%>

<script type="text/javascript">
$(document).ready(function(){
	
	$("#selectMonth").MonthPicker({ 
		Button: false
		,MonthFormat: 'yy-mm'	
		,OnAfterChooseMonth: function() { 
	        //alert($(this).val());
	        document.location.href = "/account/finish_sales.do?&searchWord="+encodeURI($("#searchWord").val())+"&selectMonth="+$(this).val()+"&searchType="+$("#searchType").val();
	    }
	});
	
});

function editEmp(empId){
	
	document.location.href = "/baseinfo/edit-employee.do?empId="+empId;
}

function addEmp(){
	document.location.href = "/baseinfo/add-employee.do";
}


function searchStatusChange(searchStatus,obj){
	
	
	if($("#startDt").val() != ""){
		if($("#endDt").val() == ""){
			alert("조회 종료일이 지정되지 않았습니다.");
			return false;
		}
	}
	
	if($("#endDt").val() != ""){
		if($("#startDt").val() == ""){
			alert("조회 시작일이 지정되지 않았습니다.");
			return false;
		}
	}
	
	document.location.href = "/humanResource/employee.do?searchStatus="+searchStatus+"&startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val();
	
	
	
	/* $('input[name="searchStatus"]').each(function(index,element) {
		$(this).removeClass("btn-primary");
		$(this).addClass("btn-normal");
	 });
	$(obj).removeClass("btn-normal");
	$(obj).addClass("btn-primary"); */

//	
	
	/* $.ajax({ 
		type: 'post' ,
		url : "/humanResource/getEmpData.do" ,
		dataType : 'json' ,
		data : {
			searchStatus : searchStatus,
			startDt : $("#startDt").val(),
			endDt : $("#endDt").val()
		},
		success : function(data, textStatus, jqXHR)
		{
			var resultCode = data.resultCode;
			var resultData = data.resultData;
			var resultDataSub = data.resultDataSub;
			$("#dataArea").html("");
			if(resultCode == "0000"){
				var result = "";
				 for(var i = 0; i < resultData.length; i++){
					 result += '<tr class="ui-state-default">';
					 result += '<td style="text-align:center;">'+resultData[i].emp_id+'</td>';
					 result += '<td style="text-align:center;">'+resultData[i].emp_name+'</td>';
					 result += '<td style="text-align:center;">';
					 if(resultData[i].phone_work != null && resultData[i].phone_work != ""){
						 result += '<div style="display:block;">업무용 : '+resultData[i].phone_work+'</div>';	 
					 }
					 if(resultData[i].phone_personal != null && resultData[i].phone_personal != ""){
						 result += '<div style="display:block; margin-top:5px;">개인 : '+resultData[i].phone_personal+'</div>';	 
					 }
					 result += '</td>';
					 result += '<td style="text-align:center;">'+resultData[i].join_dt+'</td>';
					 result += '<td style="text-align:center;">'+resultData[i].resign_dt+'</td>';
					 result += '<td style="text-align:center;">'+resultData[i].emp_position+'</td>';
					 result += '<td style="text-align:center;">'+resultData[i].email_work+' </td>';
					 result += '</tr>';
					  
				 }
				 $("#dataArea").html(result);
				 $("#startDt").val(resultDataSub.startDt);
				 $("#endDt").val(resultDataSub.endDt);
			}else if(resultCode == "0002"){
				alert("");
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	}); */	
	
	
	

	
}


function inputDate(){
	
	$.MessageBox({
			  input    : true,
			  buttonDone: "확인",
			  message  : "계산서 발행일자를 입력 해 주세요(ex:20190401)"
			}).done(function(data){
			  if ($.trim(data)) {
				 //전화번호만 입력
				phoneNumber = data;
			  } else {
				$.alert("인수증 전송이 취소 되었습니다. \r\n 메일 또는 전화번호 하나는 반드시 입력 되어야 합니다.",function(a){
             	});   
			  }
			});
	
	
}


function getNumberOnly (str) {			//저장할때는 콤마 제거 후 저장한다.
    var len = str.length;
    var sReturn = "";

    for (var i=0; i<len; i++){
        if ( (str.charAt(i) >= "0") && (str.charAt(i) <= "9") ){
            sReturn += str.charAt(i);
        }
    }
    return sReturn;
}


function publishDone(id){
	
	
	if(confirm("세금계산서 발행을 완료 하시겠습니까?")){
		
		$.ajax({ 
			type: 'post' ,
			url : "/account/updateBillPublishRequest.do" ,
			dataType : 'json' ,
			data : {
				billPublishRequestId : id,
				status : "Y"
			},
			success : function(data, textStatus, jqXHR)
			{
				var resultCode = data.resultCode;
				var resultData = data.resultData;
				
				if(resultCode == "0000"){
					alert("세금계산서 발행을 완료 했습니다.");
					document.location.reload();
				}else{
					alert(resultData+"세금계산서 발행을 실패 했습니다.");
					document.location.reload();
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
			
	}
	
}



function publishCancel(id){
	
	
	if(confirm("세금계산서 발행요청을 반려 하시겠습니까?")){
		
		$.ajax({ 
			type: 'post' ,
			url : "/account/updateBillPublishRequest.do" ,
			dataType : 'json' ,
			data : {
				billPublishRequestId : id,
				status : "N"
			},
			success : function(data, textStatus, jqXHR)
			{
				var resultCode = data.resultCode;
				var resultData = data.resultData;
				
				if(resultCode == "0000"){
					alert("반려 하였습니다.");
					document.location.reload();
				}else{
					alert(resultData+"반려하는데 실패 했습니다.");
					document.location.reload();
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
			
	}
	
}





function billPublishRequest(){
	
	var total = $('input:checkbox[name="forDecideStatus"]:checked').length;
	var customerId = "";
	
		if(total == 0){
			alert("거래처가 선택 되지 않았습니다.");
			return false;
		}else{
			var billPublishInfoList = new Array();
			var requestDtChk = true;
			$('input:checkbox[name="forDecideStatus"]:checked').each(function(index,element) {
					if(this.checked){
						var billPublishInfo= new Object();
						billPublishInfo.index = index;	
						billPublishInfo.customerId=$(this).attr("customerId");
						billPublishInfo.customerName=$(this).parent().next().html();
						billPublishInfo.amount=Number(getNumberOnly($(this).parent().next().next().next().html()));
						billPublishInfo.requestDt=$(this).parent().next().next().next().next().next().find("input").val();
						
						if($(this).parent().next().next().next().next().next().find("input").val() == ""){
							requestDtChk = false;
						}
						billPublishInfoList.push(billPublishInfo);
					}
			 });

			if(!requestDtChk){
				alert("세금계산서 발행일이 입력 되지 않았습니다.");
				return false;
			}else{
				if(confirm("선택된 "+total+"건에 대하여 세금계산서 발행 요청을 하시겠습니까?")){
					//alert(JSON.stringify({billPublishInfoList : billPublishInfoList}));
					$.ajax({ 
						type: 'post' ,
						url : "/account/billPublishRequest.do" ,
						dataType : 'json' ,
						data : {
							billPublishInfoList : JSON.stringify({billPublishInfoList : billPublishInfoList}),
							includeZero : "${paramMap.includeZero}",
							searchDateType : "${paramMap.searchDateType}",
							searchType : "${paramMap.searchType}",
							searchWord : "${paramMap.searchWord}",
							carrierType : "${paramMap.carrierType}",
							decideStatus : "${paramMap.decideStatus}",
							startDt : "${paramMap.startDt}",
							endDt : "${paramMap.endDt}"
							
						},
						success : function(data, textStatus, jqXHR)
						{
							var resultCode = data.resultCode;
							var resultData = data.resultData;
							
							if(resultCode == "0000"){
								alert("계산서 발행 요청에 성공 했습니다.");
								document.location.reload();
							}else if(resultCode == "0002"){
								alert(resultData+"거래처의 계산서 발행 요청에 실패 했습니다.");
								document.location.reload();
							}
						} ,
						error : function(xhRequest, ErrorText, thrownError) {
						}
					});
						
				}
					
			}
			
		}
	
	
}

function checkAll(){

	if($('input:checkbox[id="checkAll"]').is(":checked")){
		$('input:checkbox[name="forDecideStatus"]').each(function(index,element) {
			$(this).prop("checked","true");
		 });	
	}else{
		$('input:checkbox[name="forDecideStatus"]').each(function(index,element) {
			$(this).prop("checked","");
		 });
	}
}


function excelDownload(){

	if(confirm("${paramMap.selectMonth}월의 마감내역을 다운로드 하시겠습니까?")){
		document.location.href = "/account/finish_sales_download.do?selectMonth=${paramMap.selectMonth}";
	}
	
}

function sendMail(){
	
	
	var total = $('input:checkbox[name="forDecideStatus"]:checked').length;
	var customerId = "";
	
		if(total == 0){
			alert("거래처가 선택 되지 않았습니다.");
			return false;
		}else{
			
			if(confirm("선택한 거래처로 메일을 보내시겠습니까?")){
				$('input:checkbox[name="forDecideStatus"]:checked').each(function(index,element) {
				      if(this.checked){//checked 처리된 항목의 값
				    	  customerId+=$(this).attr("customerId");
				      }
				      if(index<total-1){
				    	  customerId += ","; 
				      } 
				 });
				var includeZero = "";
				if($("#includeZero").is(":checked")){
					includeZero = "Y";
				}else{
					includeZero = "N";
				}
				$.ajax({ 
					type: 'post' ,
					url : "/account/sendMail.do" ,
					dataType : 'json' ,
					data : {
						decideStatus : "${paramMap.decideStatus}",
						customerId : customerId,
						includeZero : includeZero,
						searchDateType : $("#searchDateType").val(),
						searchType : $("#searchType").val(),
						searchWord : encodeURI($("#searchWord").val()),
						carrierType : $("#carrierType").val(),
						startDt : $("#startDt").val(),
						endDt : $("#endDt").val()
					},
					success : function(data, textStatus, jqXHR)
					{
						var resultCode = data.resultCode;
						var resultData = data.resultData;
						
						if(resultCode == "0000"){
							alert("메일을 전송 하였습니다.");
						}else if(resultCode == "0002"){
							/* if(confirm("거래처의 세금계산서 이메일 주소가 저장되어 있지 않습니다. 세금계산서 이메일을 입력 하시겠습니까?")){
								document.location.href = "/baseinfo/edit-customer.do?customerId="+resultData;
							}else{
								return false;	
							} */
							alert(resultData);
							
						}
					} ,
					error : function(xhRequest, ErrorText, thrownError) {
					}
				});
			}		
			
		
	
			
			
			
			
		}
	
	
}





function search(){//searchDateType
	
	document.location.href = "/account/purchase.do?&searchType="+$("#searchType").val()+"&searchWord="+encodeURI($("#searchWord").val())+"&decideMonth="+$("#selectMonth").val();
	
}


function viewTotalSales(paymentPartnerId){
	document.location.href = "/account/purchaseDetail.do?&paymentPartnerId="+paymentPartnerId+"&decideMonth="+$("#selectMonth").val();
}


function decideStatus(status){
	
	document.location.href = "/account/purchase.do?&includeZero=${paramMap.includeZero}&searchDateType="+$("#searchDateType").val()+"&searchType="+$("#searchType").val()+"&searchWord="+encodeURI($("#searchWord").val())+"&carrierType="+$("#carrierType").val()+"&startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val()+"&decideStatus="+status;
}

function goSearch(obj){
	
	var includeZero = "";
	
	if($("#includeZero").is(":checked")){
		includeZero = "Y";
	}else{
		includeZero = "N";
	}
	document.location.href = "/account/purchase.do?&decideStatus=${paramMap.decideStatus}&includeZero="+includeZero+"&searchDateType="+$("#searchDateType").val()+"&searchType="+$("#searchType").val()+"&searchWord="+encodeURI($("#searchWord").val())+"&carrierType="+$(obj).val()+"&startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val();
	
//	alert($(obj).val());
	
	
}


function customerCheck(status){
	
	var msg = "확정";
	if(status == "N"){
		msg = "수정";
	}
	if(confirm("선택하신 거래처의 매출액을 일괄 "+msg+" 하시겠습니까?")){
		
		var customerId = "";
		var total = $('input:checkbox[name="forDecideStatus"]:checked').length;
		
		//if('${paramMap.carrierType}' != 'A'){
			if(total == 0){
				alert("일괄 "+msg+"할 거래처가 선택 되지 않았습니다.");
				return false;
			}else{
				$('input:checkbox[name="forDecideStatus"]:checked').each(function(index,element) {
				      if(this.checked){//checked 처리된 항목의 값
				    	  customerId+=$(this).attr("customerId");
				    	  if(index<total-1){
				    		  customerId += ","; 
					         } 
				      }
				 });	
			
			}	
		//}else{
		//	alert("");
		//	return false;
	//	}
	
			updateDecideStatus('${paramMap.carrierType}','${paramMap.searchDateType}','${paramMap.startDt}','${paramMap.endDt}','${paramMap.searchType}','${paramMap.searchWord}',customerId,status);
			
	}
	
	
	
}



function updateDecideStatus(carrierType,searchDateType,startDt,endDt,searchType,searchWord,customerId,status){
	
		
	var msg = "확정";
	if(status == "N"){
		msg = "수정";
	}
	
	$.ajax({ 
		type: 'post' ,
		url : "/account/updateDecideStatus.do" ,
		dataType : 'json' ,
		data : {
			customerId : customerId,
			carrierType : carrierType,
			searchDateType : searchDateType,
			startDt : startDt,
			endDt : endDt,
			searchType : searchType,
			searchWord : encodeURI(searchWord),
			currentDecideStatus : "${paramMap.decideStatus}",
			setDecideStatus : status
		},
		success : function(data, textStatus, jqXHR)
		{
			var result = data.resultCode;
			var resultData = data.resultData;
			if(result == "0000"){
				alert(msg+"되었습니다.");
				var searchWord = encodeURI('${paramMap.searchWord}');
				document.location.href = "/account/customerCal.do?&decideStatus=${paramMap.decideStatus}&includeZero=${paramMap.includeZero}&cPage=${paramMap.cPage}&searchDateType=${paramMap.searchDateType}&carrierType=${paramMap.carrierType}&startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&searchType=${paramMap.searchType}&searchWord="+searchWord;
			}else if(result == "0001"){
				alert(msg+"하는데 실패 하였습니다.");
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
	

	
}




var order = "${paramMap.order}";
var forOrder = "${paramMap.forOrder}";
function sortBy(gubun){

	
	//alert(order);
	//alert(forOrder);

	//alert(gubun);
	
	if(gubun == "A" && (order == "asc" && forOrder == "regDt")){
		//alert(1);
		gubun = "A";
		order = "asc";
	}else if(gubun == "A" && (order == "asc" && forOrder == "A")){
		//alert(2);
		gubun = "A";
		order = "desc";
	}else if(gubun == "A" && (order == "desc" && forOrder == "A")){
		//alert(3);
		gubun = "regDt";
		order = "asc";
	}else if(gubun == "B" && (order == "asc" && forOrder == "regDt")){
		//alert(4);
		gubun = "B";
		order = "asc";
	}else if(gubun == "B" && (order == "asc" && forOrder == "B")){
		//alert(5);
		gubun = "B";
		order = "desc";
	}else if(gubun == "B" && (order == "desc" && forOrder == "B")){
		//alert(6);
		gubun = "regDt";
		order = "asc";
	}else if(gubun == "C" && (order == "asc" && forOrder == "regDt")){
		gubun = "C";
		order = "asc";
	}else if(gubun == "C" && (order == "asc" && forOrder == "C")){
		gubun = "C";
		order = "desc";
	}else if(gubun == "C" && (order == "desc" && forOrder == "C")){
		gubun = "regDt";
		order = "asc";
	}else if(gubun == "D" && (order == "asc" && forOrder == "regDt")){
		gubun = "D";
		order = "asc";
	}else if(gubun == "D" && (order == "asc" && forOrder == "D")){
		gubun = "D";
		order = "desc";
	}else if(gubun == "D" && (order == "desc" && forOrder == "D")){
		gubun = "regDt";
		order = "asc";
	}
	
	
	
/* 	if(order == "" || (forOrder == "A" && order == "desc")){
		gubun = "A"
		order = "asc";
	}else if(forOrder == "A" && order == "asc"){
		gubun = "A"
			order = "desc";
		} */
	
	
	
	
	
	
	/* if((forOrder == "A" || forOrder == "B") && order == "desc"){
		gubun = "regDt"
		order = "asc";
	}else{
		if(order == "" || order == "desc"){
			order = "asc";
		}else{
			order = "desc";
		}
	} */
			
	/* if(order == "" || order == "desc"){
		order = "asc";
	}else{
		order = "desc";
	} */
	
	var loc = document.location.href;
	var str = "";
	if(loc.indexOf("?") > -1){
		//forOrder 가 있는경우 ㅎㅎ
		if(loc.indexOf("forOrder") > -1){
			var queryString = loc.split("?");
			var query = queryString[1].split("&");
			
			for(var i = 0; i < query.length; i++){
				if(query[i].indexOf("forOrder") > -1){
					query[i] = "forOrder="+gubun+"%5E"+order;
				}
			}
			for(var i = 0; i < query.length; i++){
				if(query[i] != ""){
					str += "&"+query[i];	
				}
			}
			document.location.href = queryString[0]+"?"+str;
		}else{
			str="&forOrder="+gubun+"%5E"+order;
			document.location.href = loc+str;
		}
		
	}else{
		str="?&forOrder="+gubun+"%5E"+order;
		document.location.href = loc+str;
	}
	
}




function viewPublishRequestAllocation(billPublishRequestId,status){
	
	
	document.location.href = "/account/billRequestDetail.do?billPublishRequestId="+billPublishRequestId+"&billingStatus="+status;
	
}





function selectList(id){
	
	var searchWord = encodeURI('${paramMap.searchWord}');
	document.location.href="/allocation/allocation-view.do?&searchDateType=${paramMap.searchDateType}&startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&searchType=${paramMap.searchType}&cPage=${paramMap.cPage}&allocationStatus=${paramMap.allocationStatus}&location=${paramMap.location}&allocationId="+id+"&searchWord="+searchWord;
	
}




</script>
		<!-- <section class="side-nav"> -->
            <!-- <ul>
                <li class="active"><a href="/baseinfo/employee.do">직원관리</a></li>
                <li class=""><a href="/baseinfo/driver.do">기사관리</a></li>
                <li class=""><a href="/baseinfo/customer.do">거래처관리</a></li>
                <li class=""><a href="/baseinfo/company.do">회사정보관리</a></li>
            </ul> -->
        <!-- </section> -->

        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">회계관리</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">마감월별 매출 관리</a></li>
                </ul>
            </div>
            <div class="up-dl clearfix header-search">
            	
            	<table>
                    <tbody>
                        <tr>
                        	<td>조회월 :</td>
				            <td class="widthAuto" style="width:200px;">
				                <input style="width:100%; text-align:center;" id="selectMonth" type="text" placeholder="조회월" onclick="javascript:$(this).val('');"  readonly="readonly" value="${paramMap.selectMonth}">
				            </td>
                        	
				            <td style="width: 145px;">
                                <div class="select-con">
							        <select class="dropdown" name="searchType" id="searchType">
							        	<option value="" <c:if test='${paramMap.searchType eq "" }'> selected="selected"</c:if>>조회구분 선택</option>
							            <option value="00" <c:if test='${paramMap.searchType eq "00" }'> selected="selected"</c:if>>업체명</option>
							            <%-- <option value="01" <c:if test='${paramMap.searchType eq "01" }'> selected="selected"</c:if>>대표자명</option> --%>
							        </select>
							        <span></span>
							    </div>
                            </td>
                            <td>
                                <input  style="width: 150px;" type="text" id="searchWord" name="searchWord" value="${paramMap.searchWord}" placeholder="검색어 입력"   onkeypress="if(event.keyCode=='13') search();">
                            </td>
				            
				            
                        	<td>					
                        	<a class="btn-gray" href="javascript:search();"><input type="button" value="검색"></a>	
                            </td>
                            <td>
                            	
                            </td>
                        </tr>
                    </tbody>
                </table>
            	
            	
                 <%-- <table>
                    <tbody>
                        <tr>
                        	<td>조회월 :</td>
				            <td class="widthAuto" style="width:200px;">
				                <input style="width:100%; text-align:center;" id="selectMonth" type="text" placeholder="조회월" onclick="javascript:$(this).val('');"  readonly="readonly" value="${paramMap.decideMonth}">
				            </td>
				            <td style="width: 145px;">
                                <div class="select-con">
							        <select class="dropdown" name="searchType" id="searchType">
							        	<option value="" <c:if test='${paramMap.searchType eq "" }'> selected="selected"</c:if>>조회구분 선택</option>
							            <option value="00" <c:if test='${paramMap.searchType eq "00" }'> selected="selected"</c:if>>매입처명</option>
							            <option value="01" <c:if test='${paramMap.searchType eq "01" }'> selected="selected"</c:if>>대표자명</option>
							        </select>
							        <span></span>
							    </div>
                            </td>
                            <td>
                                <input  style="width: 150px;" type="text" id="searchWord" name="searchWord" value="${paramMap.searchWord}" placeholder="검색어 입력"   onkeypress="if(event.keyCode=='13') search();">
                            </td>
				            
				            <td>
				            	<a class="btn-gray" href="javascript:search();"><input type="button" value="검색"></a>
                        		
                            </td>
                            <td>
                        		
                            </td>
                        	<td>
                        		
                            </td>
                        	<td>					
                        		
                            </td>
                            <td>
                            	
                            </td>
                        </tr>
                    </tbody>
                </table> --%>
            </div>

			<div class="dispatch-btn-container">
                <!-- <div class="dispatch-btn">
                    <i id="downArrow" class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div> -->
                
                <%-- <c:if test="${complete == null || complete != 'Y'}"> --%>
                	<%-- <c:if test="${forOpen == null || forOpen != 'N'}">
						<div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">신규배차입력</div>
					</c:if> --%>
					<%-- <c:if test="${forOpen != null && forOpen == 'N'}"> --%>
						<div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">마감월별 매출 관리</div>
					<%-- </c:if>
				</c:if> --%>
                
                <%-- <c:if test="${complete != null && complete == 'Y'}">
					<div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">완료배차</div>					
				</c:if> --%>
                
            </div>

        </section>

			<div class="confirmation" style="min-height:0px; text-align:left; margin-top:-10px; margin-left:290px; margin-bottom:10px;">
	            <div class="cancel">
	            	<a style="cursor:pointer;" onclick="javascript:document.location.href='/account/sales.do';">매출관리</a>
	            </div>
	            <div class="confirm">
	            	<a style="cursor:pointer;" onclick="javascript:document.location.href='/account/finish_sales.do';">마감월별 매출관리</a>
	            	<!-- <a style="cursor:pointer;" onclick="">매입관리</a> -->
	            </div>
	            <div class="cancel">
	            	<a style="cursor:pointer;" onclick="javascript:document.location.href='/account/purchase.do';">매입관리</a>
	            	<!-- <a style="cursor:pointer;" onclick="">매입관리</a> -->
	            </div>
	            <%-- <div class="<c:if test='${paramMap.decideStatus eq "A" }'> confirm</c:if><c:if test='${paramMap.decideStatus eq "Y" }'> cancel</c:if><c:if test='${paramMap.decideStatus eq "N" }'> cancel</c:if>">
	            	<a style="cursor:pointer;" onclick="javascript:decideStatus('A');">전체</a>
	            </div> --%>
            </div>

            <section class="bottom-table" style="width:1600px; margin-left:290px;">
            	                                        
                <table class="article-table forToggle" id="dataTable" style="width:100%; margin-top:25px;">
	                    <colgroup>
	                        <col width="130px;">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="100px;">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                        <col width="auto">
	                    </colgroup>
	                    <thead>
	                        <tr>
	                            <td>의뢰일 </td>
	                            <td>배차구분</td>
	                            <td>운행구분</td>
	                            <td>고객명</td>
	                            <td>출발일</td>
	                            <td>출발시간</td>
	                            <td>차종</td>
	                            <td>차대번호</td>
	                            <td>차량번호</td>
	                            <td>출발지</td>
	                            <td>하차지</td>
	                            <td>기사명</td>
	                            <td>적용율</td>
	                            <td>기사형태</td>
	                            <td>발행여부</td>
	                            <td>매출액</td>
	                            <td>기사지급액</td>
	                            <td>상태</td>
	                        </tr>
	                    </thead>
	                    <tbody id="rowData">
	                    	<c:forEach var="data" items="${listData}" varStatus="status">
								<tr class="ui-state-default" list-order="${data.list_order}" allocationId="${data.allocation_id}">
	                            <%-- <td class="showToggle"><input type="checkbox" name="forBatch" allocationId="${data.allocation_id}"></td> --%>
	                            <%-- <td class="showToggle">${data.list_order}</td> --%>
	                            <td>${data.input_dt}</td>
								<td>${data.allocation_division}</td>
								<td>${data.run_division}</td>
	                            <td style="cursor:pointer;" onclick="javascript:selectList('${data.allocation_id}');">${data.customer_name}</td>
	                            <td>${data.departure_dt}</td>
	                            <td>${data.departure_time}</td>
	                            <%-- <td>${data.etc}</td> --%>
	                            <td>${data.car_kind}</td>
	                            <%-- <td>${data.batch_status_name}</td> --%>
	                            <c:if test="${data.driver_mod_yn == 'Y'}">
	                            	<td style="color:#F00;">${data.car_id_num}</td>
	                            </c:if>
	                            <td>${data.car_id_num}</td>
	                            <td>${data.car_num}</td>
	                            <td>${data.departure}</td>
	                            <td>${data.arrival}</td>
	                            <td>${data.driver_name}</td>
	                            <td>${data.deduction_rate}%</td>
	                            <td>${data.driver_kind}</td>
	                            <td>${data.publish}</td>
	                            <td>${data.price}</td>
		                        <td>${data.driver_amount}</td>
	                            <td>${data.allocation_status_name}</td>
	                            <%-- <c:if test="${user.control_grade == '01' }">
		                            <td class="showToggle">
		                                <a href="#" class="table-driver-btn" title="아이콘 설명 1"><i class="fa fa-truck" aria-hidden="true"></i></a>
		                                <a href="#" class="table-btn" title="아이콘 설명 2"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
		                                <a style="cursor:pointer" title="취소" onclick="javascript:updateAllocationStatus('cancel','${data.allocation_id}');" class="table-x">
		                                    <img src="/img/x-icon.png" alt="">
		                                </a>
		                                <a style="cursor:pointer"  title="완료" onclick="javascript:updateAllocationStatus('complete','${data.allocation_id}');" class="table-check">
		                                    <img src="/img/check-icon.png" alt="">
		                                </a>
		                            </td>
	                            </c:if> --%>
	                        </tr>
							</c:forEach>
	                    
	                        
	
	
	                    </tbody>
	                </table>
                <div class="table-pagination text-center">
                    <ul class="pagination">
                        <!-- <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="curr-page"><a href="#">1</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li> -->
                        <html:paging uri="/account/finish_sales.do" forGroup="&searchWord=${paramMap.searchWord}&selectMonth=${paramMap.selectMonth}&searchType=${paramMap.searchType}" frontYn="N" />
                    </ul>
                </div>
                <div class="confirmation">
                	<%-- <c:if test='${paramMap.decideStatus eq "N" }'>
	                   <div class="confirm">
	                    	<a style="cursor:pointer;" onclick="javascript:customerCheck('Y')">일괄확정</a>
	                    </div>
	                    <div class="cancel">
	                    	<a style="cursor:pointer;" onclick="javascript:sendMail();">메일전송</a>
	                    </div>
	                    <div class="cancel">
	                    	<a style="cursor:pointer;" onclick="javascript:excelDownload();">엑셀다운로드</a>
	                    </div>
                    </c:if>
                    <c:if test='${paramMap.decideStatus eq "Y" }'>
	                   <div class="confirm">
	                    	<a style="cursor:pointer;" onclick="javascript:customerCheck('N')">수정/확정취소</a>
	                    </div>
	                    <div class="cancel">
	                    	<a style="cursor:pointer;" onclick="javascript:excelDownload();">엑셀다운로드</a>
	                    </div>
	                    <div class="cancel">
	                    	<a style="cursor:pointer;" onclick="javascript:billPublishRequest();">세금계산서 발행 요청</a>
	                    </div>
                    </c:if>
                    <c:if test='${paramMap.decideStatus eq "A" }'>
	                    <div class="confirm">
	                    	<a style="cursor:pointer;" onclick="javascript:excelDownload();">엑셀다운로드</a>
	                    </div>
                    </c:if> --%>
                    
                    <div class="confirm">
	                    	<a style="cursor:pointer;" onclick="javascript:excelDownload();">엑셀다운로드</a>
	                </div>
                    
                    
                </div>
                
                <!-- <div style="color:#8B0000;">※각 컬럼을 클릭 하여 오름차순 또는 내림차순으로 정렬 할 수 있으며 오름차순->내림차순->초기화 순으로 동작 합니다.</div> -->
                
                
                
                
            </section>
     
