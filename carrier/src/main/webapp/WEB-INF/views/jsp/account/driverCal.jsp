<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>
<%

%>

<script type="text/javascript">
$(document).ready(function(){
	
	$("#selectMonth").MonthPicker({ 
		Button: false
		,MonthFormat: 'yy-mm',	
	});
	
});


var rgx1 = /\D/g;  // /[^0-9]/g 와 같은 표현
var rgx2 = /(\d+)(\d{3})/; 

function getNumber(obj){
	
    var num01;
    var num02;
    num01 = obj.value;
    num02 = num01.replace(rgx1,"");
    num01 = setComma(num02);
    obj.value =  num01;
      
}

function setComma(inNum){
    
    var outNum;
    outNum = inNum; 
    while (rgx2.test(outNum)) {
         outNum = outNum.replace(rgx2, '$1' + ',' + '$2');
     }
    return outNum;

}



function search(searchStatus,obj){
	
	document.location.href = "/account/driverCal.do?selectMonth="+$("#selectMonth").val()+"&driverId=${paramMap.driverId}";
}

function addEmp(){
	document.location.href = "/baseinfo/add-employee.do";
}

function getNumberOnly (str) {			//저장할때는 콤마 제거 후 저장한다.
    var len = str.length;
    var sReturn = "";

    for (var i=0; i<len; i++){
        if ( (str.charAt(i) >= "0") && (str.charAt(i) <= "9") ){
            sReturn += str.charAt(i);
        }
    }
    return sReturn;
}


function addDriverDeduct(driverId,obj){
	
	var deductInfo = new Object();
	
	if($("#occurrenceDt").val() == ""){
		alert("날짜가 입력 되지 않았습니다.");
		return false;
	}else{
		deductInfo.occurrence_dt = $("#occurrenceDt").val();	
	}	
	if($("#item").val() == ""){
		alert("항목이 입력 되지 않았습니다.");
		return false;
	}else{
		deductInfo.item = $("#item").next().val();	
	}
	if($("#division").val() == ""){
		alert("구분이 입력 되지 않았습니다.");
		return false;
	}else{
		deductInfo.division = $("#division").next().val();	
	}
	if($("#amount").val() == ""){
		alert("금액이 입력 되지 않았습니다.");
		return false;
	}else{
		deductInfo.amount = getNumberOnly($("#amount").val());
	}
	deductInfo.etc = encodeURI($("#etc").val());
	
	if(confirm("공제 내역을 등록 하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : "/account/insert-driver-deduct.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				deductInfo : JSON.stringify({deductInfo : deductInfo}),
				driverId : "${driver.driver_id}"
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					document.location.reload();
				}else if(result == "E000"){
					alert("날짜가 잘못 입력 되었습니다.");
					return false;
				}else if(result == "0001"){
					alert("조회 하는데 실패 하였습니다.");
					return false;
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
}


function deleteDriverDeduct(deductInfoId,driverId,obj){
	
	
 	if(confirm("공제 내역을 삭제 하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : "/account/delete-driver-deduct.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				driverId : "${driver.driver_id}",
				deductInfoId : deductInfoId
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("삭제 되었습니다.");
					document.location.reload();
				}else if(result == "0001"){
					alert("조회 하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
	
	
	
	
}



function editDriverDeduct(driverDeductId,driverId,obj){
	
	$(obj).parent().parent().find(".normal").css("display","none");
	$(obj).parent().parent().find(".mod").css("display","");
	$(obj).css("display","none");
	$(obj).next().css("display","");
	
}


function setDriverDeduct(deductInfoId,driverId,obj){
	
	
	var deductInfo = new Object();
	
	if($(obj).parent().parent().find(".mod").eq(0).find("input").val() == ""){
		alert("날짜가 입력 되지 않았습니다.");
		return false;
	}else{
		deductInfo.occurrence_dt = $(obj).parent().parent().find(".mod").eq(0).find("input").val();	
	}	
	if($(obj).parent().parent().find(".mod").eq(1).find("input").eq(0).val() == ""){
		alert("항목이 입력 되지 않았습니다.");
		return false;
	}else{
		deductInfo.item = $(obj).parent().parent().find(".mod").eq(1).find("input").eq(1).val();	
	}
	if($(obj).parent().parent().find(".mod").eq(2).find("input").eq(0).val() == ""){
		alert("구분이 입력 되지 않았습니다.");
		return false;
	}else{
		deductInfo.division = $(obj).parent().parent().find(".mod").eq(2).find("input").eq(1).val();	
	}
	if($(obj).parent().parent().find(".mod").eq(3).find("input").val() == ""){
		alert("금액이 입력 되지 않았습니다.");
		return false;
	}else{
		deductInfo.amount = getNumberOnly($(obj).parent().parent().find(".mod").eq(3).find("input").val());
	}
	deductInfo.etc = encodeURI($(obj).parent().parent().find(".mod").eq(4).find("input").val());
	
 	if(confirm("공제 내역을 수정 하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : "/account/update-driver-deduct.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				deductInfo : JSON.stringify({deductInfo : deductInfo}),
				driverId : "${driver.driver_id}",
				deductInfoId : deductInfoId
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("수정 되었습니다.");
					document.location.reload();
				}else if(result == "0001"){
					alert("조회 하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
	/* $(obj).parent().parent().find(".normal").css("display","");
	$(obj).parent().parent().find(".mod").css("display","none");
	$(obj).css("display","none");
	$(obj).prev().css("display",""); */
	
}



/* <td style="text-align:center;"><input type="text" placeholder="항목" name="item" id="item" onkeyup="javascript:setItem(this);"></td>
<td style="text-align:center;"><input type="text" placeholder="구분" name="division" id="division" onkeyup="javascript:setDivision(this);"></td> */

function setItem(obj){
	var val = $(obj).val();
	var num01;
    var num02;
    num01 = obj.value;
    num02 = num01.replace(rgx1,"");
    
    if(num02 != ""){
    	if(obj.value == "1"){
    		$(obj).next().val(obj.value);
    		num02 = "청구 내역 공제";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "2"){
    		$(obj).next().val(obj.value);
    		num02 = "대납 내역 공제";
    		obj.value =  num02;
    		$(obj).blur();
    	}else{
    		alert("항목 입력이 잘못 되었습니다. \r\n 1 : 청구 내역 공제, 2 : 대납 내역 공제");
    		obj.value =  "";
    	}
    }else{
    	alert("항목 입력이 잘못 되었습니다. \r\n 1 : 청구 내역 공제, 2 : 대납 내역 공제");
    	obj.value =  num02;
    }
    
    
	
}

function setDivision(obj){
	
	var val = $(obj).val();
	var num01;
    var num02;
    num01 = obj.value;
    num02 = num01.replace(rgx1,"");
    if(num02 != ""){
    	if(obj.value == "1"){
    		$(obj).next().val(obj.value);
    		num02 = "보험료";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "2"){
    		$(obj).next().val(obj.value);
    		num02 = "차량할부";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "3"){
    		$(obj).next().val(obj.value);
    		num02 = "과태료";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "4"){
    		$(obj).next().val(obj.value);
    		num02 = "미납통행료";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "5"){
    		$(obj).next().val(obj.value);
    		num02 = "사고대납금";
    		obj.value =  num02;
    		$(obj).blur();
    	}else if(obj.value == "6"){
    		$(obj).next().val(obj.value);
    		num02 = "기타";
    		obj.value =  num02;
    		$(obj).blur();
    	}else{
    		alert("구분 입력이 잘못 되었습니다. \r\n 1 : 보험료, 2 : 차량할부, 3 : 과태료, 4 : 미납통행료, 5 : 사고대납금, 6 : 기타");
    		obj.value =  "";
    	}
    }else{
    	alert("구문 입력이 잘못 되었습니다. \r\n 1 : 보험료, 2 : 차량할부, 3 : 과태료, 4 : 미납통행료, 5 : 사고대납금, 6 : 기타");
    	obj.value =  num02;
    }
	
}


</script>
		<!-- <section class="side-nav"> -->
            <!-- <ul>
                <li class="active"><a href="/baseinfo/employee.do">직원관리</a></li>
                <li class=""><a href="/baseinfo/driver.do">기사관리</a></li>
                <li class=""><a href="/baseinfo/customer.do">거래처관리</a></li>
                <li class=""><a href="/baseinfo/company.do">회사정보관리</a></li>
            </ul> -->
        <!-- </section> -->

        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">공제내역</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">공제목록</a></li>
                </ul>
            </div>

            <div class="up-dl clearfix header-search">
                 <table>
                    <tbody>
                        <tr>
                        	<td>조회월 :</td>
				            <td class="widthAuto" style="width:300px;">
				                <input style="width:100%; text-align:center;" id="selectMonth" type="text" placeholder="조회월" onclick="javascript:$(this).val('');"  readonly="readonly" name="startDt" id="startDt" value="${paramMap.selectMonth}">
				                <%-- <input style="width:40%; text-align:center;" class="datepick" type="text" placeholder="조회종료일" onclick="javascript:$(this).val('');" readonly="readonly" name="endDt" id="endDt"  value="${paramMap.endDt}"> --%>
				            </td>
                        	<td>
                        		<input type="button" id="searchStatusU" name="searchStatus" value="조회" class="btn-primary" onclick="javascript:search('U',this);">
                                <%-- <input type="button" id="searchStatusU" name="searchStatus" value="매출처 정산" class="<c:if test="${paramMap.searchStatus == 'U'}">btn-primary</c:if><c:if test="${paramMap.searchStatus != 'U'}">btn-normal</c:if>" onclick="javascript:searchStatusChange('U',this);"> --%>
                            </td>
                        	<td>					
                                <%-- <input type="button" id="searchStatusI" name="searchStatus" value="매입처 정산" class="<c:if test="${paramMap.searchStatus == 'I'}">btn-primary</c:if><c:if test="${paramMap.searchStatus != 'I'}">btn-normal</c:if>" onclick="javascript:searchStatusChange('I',this);"> --%>
                            </td>
                            <td>
                                <%-- <input type="button" id="searchStatusO" name="searchStatus" value="기사 정산" class="<c:if test="${paramMap.searchStatus == 'O'}">btn-primary</c:if><c:if test="${paramMap.searchStatus != 'O'}">btn-normal</c:if>" onclick="javascript:searchStatusChange('O',this);"> --%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </section>

            <section class="bottom-table" style="width:1600px; margin-left:290px;">
            	 <div style="color:#8B0000;">*항목코드 1 : 청구내역공제,2 : 대납내역공제</div>
            	 <div style="color:#8B0000;">*구분코드 1 : 보험료,2 : 차량할부,3 : 과태료,4 : 미납통행료,5 : 사고대납금,6 : 기타</div>
                <table class="article-table" style="margin-top:15px; table-layout:fixed;">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                        	<td style="text-align:center;">기사명</td>
                            <td style="text-align:center;">날짜</td>
                            <td style="text-align:center;">항목</td>
                            <td style="text-align:center;">구분</td>
                            <td style="text-align:center;">금액</td>
                            <td style="text-align:center;">비고</td>
                            <td style="text-align:center; width:90px;">작성자</td>
                            <td style="text-align:center;">관리</td>
                        </tr>
                    </thead>
                    <tbody id="dataArea">
                    	<tr class="ui-state-default">
                    		<td style="text-align:center;">${driver.driver_name }</td>
	                    	<td style="text-align:center;"><input style="width:80%;" class="datepick" type="text" placeholder="사유발생일" name="occurrenceDt" id="occurrenceDt" onclick="javascript:$(this).val('');"></td>
	                    	<td style="text-align:center;">
	                    		<input type="text" style="width:80%;" placeholder="항목" name="item" id="item" onkeyup="javascript:setItem(this);" onclick="javascript:$(this).val('');">
	                    		<input type="hidden">
	                    	</td>
	                    	<td style="text-align:center;">
	                    		<input type="text" style="width:80%;" placeholder="구분" name="division" id="division" onkeyup="javascript:setDivision(this);" onclick="javascript:$(this).val('');">
	                    		<input type="hidden">
	                    	</td>
	                    	<td style="text-align:center;"><input style="width:80%;" type="text" placeholder="금액" name="amount" id="amount" onkeyup="javascript:getNumber(this);"></td>
	                    	<td style="text-align:center;"><input style="width:80%;" type="text" placeholder="비고" name="etc" id="etc"></td>
	                    	<td style="text-align:center;">${user.emp_name}</td>
	                    	<td style="text-align:center;">
	                    		<a style="cursor:pointer;" onclick="javascript:addDriverDeduct('${data.driver_id}',this)" class="btn-edit">등록</a>
	                    	</td>
                    	</tr>
                    	<c:forEach var="data" items="${listData}" varStatus="status">
							<tr class="ui-state-default"> 
								<td style="text-align:center;">${data.driver_name}</td>
	                            <td style="text-align:center;">
	                            <div class="normal">${data.occurrence_dt}</div>
	                            <div class="mod" style="display:none;">
	                            	<input class="datepick" type="text" placeholder="사유발생일" value="${data.occurrence_dt}" onclick="javascript:$(this).val('');">
	                            </div>
	                            </td>
	                            <td style="text-align:center;">
	                            	<div class="normal">
	                            		<c:if test='${data.item eq "1" }'>청구 내역 공제</c:if>
	                            		<c:if test='${data.item eq "2" }'>대납 내역 공제</c:if>
	                            	</div>
	                            	<div class="mod" style="display:none;">
	                            		<input type="text" placeholder="항목" value="<c:if test='${data.item eq "1" }'>청구 내역 공제</c:if><c:if test='${data.item eq "2" }'>대납 내역 공제</c:if>" onkeyup="javascript:setItem(this);" onclick="javascript:$(this).val('');">
	                    				<input type="hidden" value="${data.item}">
	                    			</div>
	                            </td>
	                            <td style="text-align:center;">
	                            	<div class="normal">
		                            	<c:if test='${data.division eq "1" }'>보험료</c:if>
		                            	<c:if test='${data.division eq "2" }'>차량할부</c:if>
		                            	<c:if test='${data.division eq "3" }'>과태료</c:if>
		                            	<c:if test='${data.division eq "4" }'>미납통행료</c:if>
		                            	<c:if test='${data.division eq "5" }'>사고대납금</c:if>
		                            	<c:if test='${data.division eq "6" }'>기타</c:if>
	                            	</div>
	                            	<div class="mod" style="display:none;">
	                            		<input type="text" placeholder="구분" value="<c:if test='${data.division eq "1" }'>보험료</c:if><c:if test='${data.division eq "2" }'>차량할부</c:if><c:if test='${data.division eq "3" }'>과태료</c:if><c:if test='${data.division eq "4" }'>미납통행료</c:if><c:if test='${data.division eq "5" }'>사고대납금</c:if><c:if test='${data.division eq "6" }'>기타</c:if>" onkeyup="javascript:setDivision(this);" onclick="javascript:$(this).val('');">
	                    				<input type="hidden" value="${data.division}">
	                    			</div>
	                            </td>
	                            <td style="text-align:center;">
	                            	<div class="normal">
	                            		${data.amount}
	                            	</div>
	                            	<div class="mod"  style="display:none;">
	                            		<input type="text" placeholder="금액" value="${data.amount}" onkeyup="javascript:getNumber(this);">
	                            	</div>
	                            </td>
	                            <td style="text-align:center;">
	                            	<div class="normal">
	                            		${data.etc}
	                            	</div>
	                            	<div class="mod" style="display:none;">
	                            		<input type="text" placeholder="비고" value="${data.etc}" >
	                            	</div>
	                            </td>
	                            <td style="text-align:center;">
	                            	<div class="normal">
	                            		${data.register_name}
	                            	</div>
	                            	<div class="mod" style="display:none;">
	                            		<input type="text" placeholder="작성자" value="${data.register_name}" >
	                            	</div>
	                            </td>
	                            <td style="text-align:center;">
	                            	<a style="cursor:pointer;" onclick="javascript:editDriverDeduct('${data.deduct_info_id}','${data.driver_id}',this)" class="btn-edit">수정</a>
	                            	<a style="display:none; cursor:pointer;" onclick="javascript:setDriverDeduct('${data.deduct_info_id}','${data.driver_id}',this)" class="btn-edit">완료</a>
	                            	<a style="cursor:pointer;" onclick="javascript:deleteDriverDeduct('${data.deduct_info_id}','${data.driver_id}',this)" class="btn-edit">삭제</a>
	                            </td>
                        	</tr>
						</c:forEach>
                        
                    </tbody>
                </table>
                <div class="table-pagination text-center">
                    <ul class="pagination">
                        <!-- <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="curr-page"><a href="#">1</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li> -->
                        <html:paging uri="/account/driverCal.do" forGroup="&selectMonth=${paramMap.selectMonth}&driverId=${paramMap.driverId}" />
                    </ul>
                </div>
                <div class="confirmation">
                    <div class="confirm">
                        <!-- <a href="/baseinfo/add-employee.do">직원등록</a> -->
                    </div>
                </div>
                
            </section>
     
