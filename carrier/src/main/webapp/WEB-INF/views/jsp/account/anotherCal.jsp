<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>
<%

%>

<script type="text/javascript">
$(document).ready(function(){
	
	
});

function editEmp(empId){
	
	document.location.href = "/baseinfo/edit-employee.do?empId="+empId;
}

function addEmp(){
	document.location.href = "/baseinfo/add-employee.do";
}


function searchStatusChange(searchStatus,obj){
	
	
	if($("#startDt").val() != ""){
		if($("#endDt").val() == ""){
			alert("조회 종료일이 지정되지 않았습니다.");
			return false;
		}
	}
	
	if($("#endDt").val() != ""){
		if($("#startDt").val() == ""){
			alert("조회 시작일이 지정되지 않았습니다.");
			return false;
		}
	}
	
	document.location.href = "/humanResource/employee.do?searchStatus="+searchStatus+"&startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val();
	
	
	
	/* $('input[name="searchStatus"]').each(function(index,element) {
		$(this).removeClass("btn-primary");
		$(this).addClass("btn-normal");
	 });
	$(obj).removeClass("btn-normal");
	$(obj).addClass("btn-primary"); */

//	
	
	/* $.ajax({ 
		type: 'post' ,
		url : "/humanResource/getEmpData.do" ,
		dataType : 'json' ,
		data : {
			searchStatus : searchStatus,
			startDt : $("#startDt").val(),
			endDt : $("#endDt").val()
		},
		success : function(data, textStatus, jqXHR)
		{
			var resultCode = data.resultCode;
			var resultData = data.resultData;
			var resultDataSub = data.resultDataSub;
			$("#dataArea").html("");
			if(resultCode == "0000"){
				var result = "";
				 for(var i = 0; i < resultData.length; i++){
					 result += '<tr class="ui-state-default">';
					 result += '<td style="text-align:center;">'+resultData[i].emp_id+'</td>';
					 result += '<td style="text-align:center;">'+resultData[i].emp_name+'</td>';
					 result += '<td style="text-align:center;">';
					 if(resultData[i].phone_work != null && resultData[i].phone_work != ""){
						 result += '<div style="display:block;">업무용 : '+resultData[i].phone_work+'</div>';	 
					 }
					 if(resultData[i].phone_personal != null && resultData[i].phone_personal != ""){
						 result += '<div style="display:block; margin-top:5px;">개인 : '+resultData[i].phone_personal+'</div>';	 
					 }
					 result += '</td>';
					 result += '<td style="text-align:center;">'+resultData[i].join_dt+'</td>';
					 result += '<td style="text-align:center;">'+resultData[i].resign_dt+'</td>';
					 result += '<td style="text-align:center;">'+resultData[i].emp_position+'</td>';
					 result += '<td style="text-align:center;">'+resultData[i].email_work+' </td>';
					 result += '</tr>';
					  
				 }
				 $("#dataArea").html(result);
				 $("#startDt").val(resultDataSub.startDt);
				 $("#endDt").val(resultDataSub.endDt);
			}else if(resultCode == "0002"){
				alert("");
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	}); */	
	
	
	

	
}


function inputDate(){
	
	$.MessageBox({
			  input    : true,
			  buttonDone: "확인",
			  message  : "계산서 발행일자를 입력 해 주세요(ex:20190401)"
			}).done(function(data){
			  if ($.trim(data)) {
				 //전화번호만 입력
				phoneNumber = data;
			  } else {
				$.alert("인수증 전송이 취소 되었습니다. \r\n 메일 또는 전화번호 하나는 반드시 입력 되어야 합니다.",function(a){
             	});   
			  }
			});
	
	
}


function getNumberOnly (str) {			//저장할때는 콤마 제거 후 저장한다.
    var len = str.length;
    var sReturn = "";

    for (var i=0; i<len; i++){
        if ( (str.charAt(i) >= "0") && (str.charAt(i) <= "9") ){
            sReturn += str.charAt(i);
        }
    }
    return sReturn;
}


function billPublishRequest(){
	
	var total = $('input:checkbox[name="forDecideStatus"]:checked').length;
	var customerId = "";
	
		if(total == 0){
			alert("거래처가 선택 되지 않았습니다.");
			return false;
		}else{
			var billPublishInfoList = new Array();
			var requestDtChk = true;
			$('input:checkbox[name="forDecideStatus"]:checked').each(function(index,element) {
					if(this.checked){
						var billPublishInfo= new Object();
						billPublishInfo.index = index;	
						billPublishInfo.customerId=$(this).attr("customerId");
						billPublishInfo.customerName=$(this).parent().next().html();
						billPublishInfo.amount=Number(getNumberOnly($(this).parent().next().next().next().html()));
						billPublishInfo.requestDt=$(this).parent().next().next().next().next().next().find("input").val();
						
						if($(this).parent().next().next().next().next().next().find("input").val() == ""){
							requestDtChk = false;
						}
						billPublishInfoList.push(billPublishInfo);
					}
			 });

			if(!requestDtChk){
				alert("세금계산서 발행일이 입력 되지 않았습니다.");
				return false;
			}else{
				if(confirm("선택된 "+total+"건에 대하여 세금계산서 발행 요청을 하시겠습니까? 세금계산서 미발행건은 포함되지 않습니다.")){
					//alert(JSON.stringify({billPublishInfoList : billPublishInfoList}));
					$.ajax({ 
						type: 'post' ,
						url : "/account/billPublishRequest.do" ,
						dataType : 'json' ,
						data : {
							billPublishInfoList : JSON.stringify({billPublishInfoList : billPublishInfoList}),
							includeZero : "${paramMap.includeZero}",
							searchDateType : "${paramMap.searchDateType}",
							searchType : "${paramMap.searchType}",
							searchWord : "${paramMap.searchWord}",
							carrierType : "${paramMap.carrierType}",
							decideStatus : "${paramMap.decideStatus}",
							startDt : "${paramMap.startDt}",
							endDt : "${paramMap.endDt}"
							
						},
						success : function(data, textStatus, jqXHR)
						{
							var resultCode = data.resultCode;
							var resultData = data.resultData;
							
							if(resultCode == "0000"){
								alert("계산서 발행 요청에 성공 했습니다.");
								document.location.reload();
							}else if(resultCode == "0002"){
								alert(resultData+"거래처의 계산서 발행 요청에 실패 했습니다.");
								document.location.reload();
							}else if(resultCode == "E001"){
								alert("증빙구분이 입력되지 않았습니다.");
								document.location.reload();
							}
						} ,
						error : function(xhRequest, ErrorText, thrownError) {
						}
					});
						
				}
					
			}
			
		}
	
	
}

function checkAll(){

	if($('input:checkbox[id="checkAll"]').is(":checked")){
		$('input:checkbox[name="forDecideStatus"]').each(function(index,element) {
			$(this).prop("checked","true");
		 });	
	}else{
		$('input:checkbox[name="forDecideStatus"]').each(function(index,element) {
			$(this).prop("checked","");
		 });
	}
}


function excelDownload(){
	
	
	var total = $('input:checkbox[name="forDecideStatus"]:checked').length;
	var anotherId = "";
	var decideMonth = "";
	
		if(total == 0){
			alert("매입처가 선택 되지 않았습니다.");
			return false;
		}else if(total > 1){
			alert("한개의 매입처만 선택 할 수 있습니다.");
			return false;
		}else{
			$('input:checkbox[name="forDecideStatus"]:checked').each(function(index,element) {
			      if(this.checked){//checked 처리된 항목의 값
			    	  anotherId+=$(this).attr("anotherId");
			    	  decideMonth = $(this).attr("decideMonth");
			      }
			 });

			/* var includeZero = "";
			
			if($("#includeZero").is(":checked")){
				includeZero = "Y";
			}else{
				includeZero = "N";
			} */
			document.location.href = "/account/excel_download_another.do?&decideStatus=${paramMap.decideStatus}&paymentPartnerId="+anotherId+"&searchDateType="+$("#searchDateType").val()+"&searchType="+$("#searchType").val()+"&searchWord="+encodeURI($("#searchWord").val())+"&carrierType="+$("#carrierType").val()+"&startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val()+"&decideMonth="+decideMonth;

		}
	
	
}

function sendMail(){
	
	
	var total = $('input:checkbox[name="forDecideStatus"]:checked').length;
	var customerId = "";
	
		if(total == 0){
			alert("거래처가 선택 되지 않았습니다.");
			return false;
		}else{
			
			if(confirm("선택한 거래처로 메일을 보내시겠습니까?")){
				$('input:checkbox[name="forDecideStatus"]:checked').each(function(index,element) {
				      if(this.checked){//checked 처리된 항목의 값
				    	  customerId+=$(this).attr("customerId");
				      }
				      if(index<total-1){
				    	  customerId += ","; 
				      } 
				 });
				var includeZero = "";
				if($("#includeZero").is(":checked")){
					includeZero = "Y";
				}else{
					includeZero = "N";
				}
				$.ajax({ 
					type: 'post' ,
					url : "/account/sendMail.do" ,
					dataType : 'json' ,
					data : {
						decideStatus : "${paramMap.decideStatus}",
						customerId : customerId,
						includeZero : includeZero,
						searchDateType : $("#searchDateType").val(),
						searchType : $("#searchType").val(),
						searchWord : encodeURI($("#searchWord").val()),
						carrierType : $("#carrierType").val(),
						startDt : $("#startDt").val(),
						endDt : $("#endDt").val()
					},
					success : function(data, textStatus, jqXHR)
					{
						var resultCode = data.resultCode;
						var resultData = data.resultData;
						
						if(resultCode == "0000"){
							alert("메일을 전송 하였습니다.");
						}else if(resultCode == "0002"){
							/* if(confirm("거래처의 세금계산서 이메일 주소가 저장되어 있지 않습니다. 세금계산서 이메일을 입력 하시겠습니까?")){
								document.location.href = "/baseinfo/edit-customer.do?customerId="+resultData;
							}else{
								return false;	
							} */
							alert(resultData);
							
						}
					} ,
					error : function(xhRequest, ErrorText, thrownError) {
					}
				});
			}		
			
		
	
			
			
			
			
		}
	
	
}





function search(){//searchDateType
	
	
	var includeZero = "";
	
	if($("#includeZero").is(":checked")){
		includeZero = "Y";
	}else{
		includeZero = "N";
	}
	/* document.location.href = "/account/anotherCal.do?&decideStatus=${paramMap.decideStatus}&includeZero="+includeZero+"&searchDateType="+$("#searchDateType").val()+"&searchType="+$("#searchType").val()+"&searchWord="+encodeURI($("#searchWord").val())+"&carrierType="+$("#carrierType").val()+"&startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val(); */
	document.location.href = "/account/anotherCal.do?&decideStatus=${paramMap.decideStatus}&includeZero="+includeZero+"&searchDateType="+$("#searchDateType").val()+"&searchType=00&searchWord="+encodeURI($("#searchWord").val())+"&carrierType="+$("#carrierType").val()+"&startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val();
	
	
}


function viewTotalSales(paymentPartnerId,decideMonth){
	
	//alert(customerId);
	//document.location.href = "/account/anotherCalDetail.do?&decideStatus=${paramMap.decideStatus}&searchDateType="+$("#searchDateType").val()+"&paymentPartnerId="+paymentPartnerId+"&carrierType=${paramMap.carrierType}&startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val()+"&decideMonth="+decideMonth;
	document.location.href = "/account/anotherCalDetail.do?&decideStatus=${paramMap.decideStatus}&searchDateType="+$("#searchDateType").val()+"&paymentPartnerId="+paymentPartnerId+"&carrierType=${paramMap.carrierType}&startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val()+"&decideMonth="+decideMonth;
}


function decideStatus(status){
	
	/* document.location.href = "/account/anotherCal.do?&includeZero=${paramMap.includeZero}&searchDateType="+$("#searchDateType").val()+"&searchType="+$("#searchType").val()+"&searchWord="+encodeURI($("#searchWord").val())+"&carrierType="+$("#carrierType").val()+"&startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val()+"&decideStatus="+status; */
	document.location.href = "/account/anotherCal.do?&includeZero=${paramMap.includeZero}&searchDateType="+$("#searchDateType").val()+"&searchType=00&searchWord="+encodeURI($("#searchWord").val())+"&carrierType="+$("#carrierType").val()+"&startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val()+"&decideStatus="+status;
}


function decideFinal(status){
	
	/* document.location.href = "/account/finalCal.do?&includeZero=${paramMap.includeZero}&searchDateType="+$("#searchDateType").val()+"&searchType="+$("#searchType").val()+"&searchWord="+encodeURI($("#searchWord").val())+"&carrierType="+$("#carrierType").val()+"&startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val()+"&decideFinal="+status; */
	document.location.href = "/account/finalCal.do?&includeZero=${paramMap.includeZero}&searchDateType="+$("#searchDateType").val()+"&searchType=00&searchWord="+encodeURI($("#searchWord").val())+"&carrierType="+$("#carrierType").val()+"&startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val()+"&decideFinal="+status;
}


function goSearch(obj){
	
	var includeZero = "";
	
	if($("#includeZero").is(":checked")){
		includeZero = "Y";
	}else{
		includeZero = "N";
	}
	/* document.location.href = "/account/anotherCal.do?&decideStatus=${paramMap.decideStatus}&includeZero="+includeZero+"&searchDateType="+$("#searchDateType").val()+"&searchType="+$("#searchType").val()+"&searchWord="+encodeURI($("#searchWord").val())+"&carrierType="+$(obj).val()+"&startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val(); */
	document.location.href = "/account/anotherCal.do?&decideStatus=${paramMap.decideStatus}&includeZero="+includeZero+"&searchDateType="+$("#searchDateType").val()+"&searchType=00&searchWord="+encodeURI($("#searchWord").val())+"&carrierType="+$(obj).val()+"&startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val();
	
//	alert($(obj).val());
	
	
}


function decideCheck(status){
	
	var msg = "확정";
	if(status == "N"){
		msg = "수정";
	}

	
		var paymentPartnerId = "";
		var total = $('input:checkbox[name="forDecideStatus"]:checked').length;
		
		if(total == 0){
			alert("일괄 "+msg+"할 매입처가 선택 되지 않았습니다.");
			return false;
		}else{
	
			if(total > 1){
				if(status == "N"){
					alert("확정 취소는 1개의 매입처씩 진행 가능 합니다.");
					return false;
						
				}
			}

			if(confirm("선택하신 매입처의 매입액을 일괄 "+msg+" 하시겠습니까?")){
					

				if(status == "Y"){

					$('input:checkbox[name="forDecideStatus"]:checked').each(function(index,element) {
					      if(this.checked){//checked 처리된 항목의 값
					    	  paymentPartnerId+=encodeURI($(this).attr("anotherId"));
						    	  if(index<total-1){
						    		  paymentPartnerId += ","; 
						         } 
					      }
					 });
					
					var date = new Date();
					var firstDayOfMonth = new Date( date.getFullYear(), date.getMonth() , 1 );
					var lastMonth = new Date ( firstDayOfMonth.setDate( firstDayOfMonth.getDate() - 1 ) );
					//alert(lastMonth.getFullYear() + "-" + (lastMonth.getMonth()+1));
					
					var decideMonth = prompt("확정 월을 yyyy-mm의 형태로 입력 하세요. 미 입력시는 전월마감으로 확정 됩니다.");
					if(decideMonth != null) {
						
						if(decideMonth != ""){
							var decideMonthArr = decideMonth.split('-');
						      if(decideMonthArr.length < 2){
						    	  alert("yyyy-mm의 형태로 입력 되지 않았습니다.");
						    	  return false;
						      }else{
						    	  
					    		  var year = Number(decideMonthArr[0]);
						    	  var month = Number(decideMonthArr[1]);  
						    	  if(isNaN(year) || isNaN(month)){
						    		alert("숫자로 입력 해 주세요.");
						    		return false;
						    	  }else{
						    		  if(year > Number(date.getFullYear())){
						    			  alert("년도가 잘못 입력 되었습니다.");
								    		return false;
						    		  }else if(month <= 0 || month > 12){
						    			  alert("월이 잘못 입력 되었습니다.");
								    		return false;
						    		  }else{
						    			  if(month < 10){
						    				  month = "0"+month;
						    			  }
						    			  if(confirm(year+"-"+month+"로 확정 하시겠습니까?")){
						    				  updateDecideStatus('${paramMap.carrierType}','${paramMap.searchDateType}','${paramMap.startDt}','${paramMap.endDt}','${paramMap.searchType}','${paramMap.searchWord}',paymentPartnerId,status,year+"-"+month);
						    			  }else{
						    				  return false;
						    			  }
						    		  }
						    	  }
					    	  
						      }
						}else{
							//alert(1);
							//updateDecideStatus('${paramMap.carrierType}','${paramMap.searchDateType}','${paramMap.startDt}','${paramMap.endDt}','${paramMap.searchType}','${paramMap.searchWord}',paymentPartnerId,status,"");
						}
						
					}else{
						
					}


					
				}else{

					var decideMonth = "";

					$('input:checkbox[name="forDecideStatus"]:checked').each(function(index,element) {
					      if(this.checked){//checked 처리된 항목의 값
					    	  paymentPartnerId+=encodeURI($(this).attr("anotherId"));
						    	  if(index<total-1){
						    		  paymentPartnerId += ","; 
						         }
						    	 decideMonth = $(this).parent().parent().children().last().html();
					      }
					 });
					updateDecideStatus('${paramMap.carrierType}','${paramMap.searchDateType}','${paramMap.startDt}','${paramMap.endDt}','${paramMap.searchType}','${paramMap.searchWord}',paymentPartnerId,status,decideMonth);

				}




				
				
				
				
				
				//updateDecideStatus('${paramMap.carrierType}','${paramMap.searchDateType}','${paramMap.startDt}','${paramMap.endDt}','${paramMap.searchType}','${paramMap.searchWord}',paymentPartnerId,status);
			}
		}	
		
		
		//alert(paymentPartnerId);
		
		
			
	
	
	
	
}



function updateDecideStatus(carrierType,searchDateType,startDt,endDt,searchType,searchWord,paymentPartnerId,status,decideMonth){
	
		
	var msg = "확정";
	if(status == "N"){
		msg = "수정";
	}
	
	$.ajax({ 
		type: 'post' ,
		url : "/account/updateAnotherDecideStatus.do" ,
		dataType : 'json' ,
		data : {
			paymentPartnerId : paymentPartnerId,
			carrierType : carrierType,
			searchDateType : searchDateType,
			startDt : startDt,
			endDt : endDt,
			searchType : searchType,
			searchWord : encodeURI(searchWord),
			currentDecideStatus : "${paramMap.decideStatus}",
			setDecideStatus : status,
			decideMonth : decideMonth
		},
		success : function(data, textStatus, jqXHR)
		{
			var result = data.resultCode;
			var resultData = data.resultData;
			if(result == "0000"){
				alert(msg+"되었습니다.");
				var searchWord = encodeURI('${paramMap.searchWord}');
				document.location.href = "/account/anotherCal.do?&decideStatus=${paramMap.decideStatus}&includeZero=${paramMap.includeZero}&cPage=${paramMap.cPage}&searchDateType=${paramMap.searchDateType}&carrierType=${paramMap.carrierType}&startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&searchType=${paramMap.searchType}&searchWord="+searchWord;
			}else if(result == "0001"){
				alert(msg+"하는데 실패 하였습니다.");
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
	

	
}









function decideFinalCheck(status){
	
	var msg = "최종확정";
	if(status == "N"){
		msg = "수정";
	}
		
		var paymentPartnerId = "";
		var decideMonth = "";
		var total = $('input:checkbox[name="forDecideStatus"]:checked').length;
		
		if(total == 0){
			alert("일괄 "+msg+"할 매입처가 선택 되지 않았습니다.");
			return false;
		}else{
			if(confirm("선택하신 매입처의 매입액을 일괄 "+msg+" 하시겠습니까?")){
				$('input:checkbox[name="forDecideStatus"]:checked').each(function(index,element) {
				      if(this.checked){//checked 처리된 항목의 값
				    	  paymentPartnerId+=encodeURI($(this).attr("anotherId"));
				    	  decideMonth+=$(this).attr("decideMonth");
					    	  if(index<total-1){
					    		  paymentPartnerId += ",";
					    		  decideMonth += ",";
					         } 
				      }
				 });	
				updateDecideFinalStatus('${paramMap.carrierType}','${paramMap.searchDateType}','${paramMap.startDt}','${paramMap.endDt}','${paramMap.searchType}','${paramMap.searchWord}',paymentPartnerId,decideMonth,status);
				/* updateAnotherDecideFinalForMakeExcel(paymentPartnerId); */
			}else{
				//alert("취소 되었습니다.");
				return false;
			}
		}	
	
}



function updateDecideFinalStatus(carrierType,searchDateType,startDt,endDt,searchType,searchWord,paymentPartnerId,decideMonth,status){
	
		
	var msg = "최종확정";
	if(status == "N"){
		msg = "수정";
	}
	
	$.ajax({ 
		type: 'post' ,
		url : "/account/updateAnotherDecideFinal.do" ,
		dataType : 'json' ,
		data : {
			paymentPartnerId : paymentPartnerId,
			carrierType : carrierType,
			searchDateType : searchDateType,
			startDt : startDt,
			endDt : endDt,
			searchType : searchType,
			searchWord : encodeURI(searchWord),
			currentDecideStatus : "${paramMap.decideStatus}",
			decideMonth : decideMonth,
			decideFinal : status
		},
		success : function(data, textStatus, jqXHR)
		{
			var result = data.resultCode;
			var resultData = data.resultData;
			if(result == "0000"){
				alert(msg+"되었습니다.");
				var searchWord = encodeURI('${paramMap.searchWord}');
				document.location.href = "/account/anotherCal.do?&decideStatus=${paramMap.decideStatus}&includeZero=${paramMap.includeZero}&cPage=${paramMap.cPage}&searchDateType=${paramMap.searchDateType}&carrierType=${paramMap.carrierType}&startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&searchType=${paramMap.searchType}&searchWord="+searchWord;
			}else if(result == "0001"){
				alert(msg+"하는데 실패 하였습니다.");
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
}




function updateAnotherDecideFinalForMakeExcel(paymentPartnerId){
	
	
	if(confirm("ekrjwlekrjwlkerjw")){
	
	$.ajax({ 
		type: 'post' ,
		/* url : "/account/updateAnotherDecideFinal.do" , */
		url : "/account/updateAnotherDecideFinalForMakeExcel.do" ,
		dataType : 'json' ,
		data : {
			paymentPartnerId : paymentPartnerId
		},
		success : function(data, textStatus, jqXHR)
		{
			var result = data.resultCode;
			var resultData = data.resultData;
			if(result == "0000"){
				alert("되었습니다.");

				
			}else if(result == "0001"){
				alert("하는데 실패 하였습니다.");
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
	}
	
	
}








var order = "${paramMap.order}";
var forOrder = "${paramMap.forOrder}";
function sortBy(gubun){

	
	//alert(order);
	//alert(forOrder);

	//alert(gubun);
	
	if(gubun == "A" && (order == "asc" && forOrder == "paymentPartner")){
		//alert(1);
		gubun = "A";
		order = "asc";
	}else if(gubun == "A" && (order == "asc" && forOrder == "A")){
		//alert(2);
		gubun = "A";
		order = "desc";
	}else if(gubun == "A" && (order == "desc" && forOrder == "A")){
		//alert(3);
		gubun = "paymentPartner";
		order = "asc";
	}else if(gubun == "B" && (order == "asc" && forOrder == "paymentPartner")){
		//alert(4);
		gubun = "B";
		order = "asc";
	}else if(gubun == "B" && (order == "asc" && forOrder == "B")){
		//alert(5);
		gubun = "B";
		order = "desc";
	}else if(gubun == "B" && (order == "desc" && forOrder == "B")){
		//alert(6);
		gubun = "paymentPartner";
		order = "asc";
	}else if(gubun == "C" && (order == "asc" && forOrder == "paymentPartner")){
		gubun = "C";
		order = "asc";
	}else if(gubun == "C" && (order == "asc" && forOrder == "C")){
		gubun = "C";
		order = "desc";
	}else if(gubun == "C" && (order == "desc" && forOrder == "C")){
		gubun = "paymentPartner";
		order = "asc";
	}else if(gubun == "D" && (order == "asc" && forOrder == "paymentPartner")){
		gubun = "D";
		order = "asc";
	}else if(gubun == "D" && (order == "asc" && forOrder == "D")){
		gubun = "D";
		order = "desc";
	}else if(gubun == "D" && (order == "desc" && forOrder == "D")){
		gubun = "paymentPartner";
		order = "asc";
	}
	
	
	
/* 	if(order == "" || (forOrder == "A" && order == "desc")){
		gubun = "A"
		order = "asc";
	}else if(forOrder == "A" && order == "asc"){
		gubun = "A"
			order = "desc";
		} */
	
	
	
	
	
	
	/* if((forOrder == "A" || forOrder == "B") && order == "desc"){
		gubun = "regDt"
		order = "asc";
	}else{
		if(order == "" || order == "desc"){
			order = "asc";
		}else{
			order = "desc";
		}
	} */
			
	/* if(order == "" || order == "desc"){
		order = "asc";
	}else{
		order = "desc";
	} */
	
	var loc = document.location.href;
	var str = "";
	if(loc.indexOf("?") > -1){
		//forOrder 가 있는경우 ㅎㅎ
		if(loc.indexOf("forOrder") > -1){
			var queryString = loc.split("?");
			var query = queryString[1].split("&");
			
			for(var i = 0; i < query.length; i++){
				if(query[i].indexOf("forOrder") > -1){
					query[i] = "forOrder="+gubun+"%5E"+order;
				}
			}
			for(var i = 0; i < query.length; i++){
				if(query[i] != ""){
					str += "&"+query[i];	
				}
			}
			document.location.href = queryString[0]+"?"+str;
		}else{
			str="&forOrder="+gubun+"%5E"+order;
			document.location.href = loc+str;
		}
		
	}else{
		str="?&forOrder="+gubun+"%5E"+order;
		document.location.href = loc+str;
	}
	
}




</script>
		<!-- <section class="side-nav"> -->
            <!-- <ul>
                <li class="active"><a href="/baseinfo/employee.do">직원관리</a></li>
                <li class=""><a href="/baseinfo/driver.do">기사관리</a></li>
                <li class=""><a href="/baseinfo/customer.do">거래처관리</a></li>
                <li class=""><a href="/baseinfo/company.do">회사정보관리</a></li>
            </ul> -->
        <!-- </section> -->

        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">운송비 정산</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">매입처 정산</a></li>
                </ul>
            </div>
            <div class="up-dl clearfix header-search">
            
                 <table>
                    <tbody>
                        <tr>
                        	<!-- <td>조회기간 :</td> -->
                  <%--       	  <td>
                            
                        		 &nbsp;소속 선택 :&nbsp;
                            </td>
                       
                             <td style="width: 149px;">
                                <div class="select-con">
							        <select class="dropdown" name="companyType" id="companyType">
							        	<option value="" selected="selected">전체</option>
							            <option value="company1" <c:if test='${paramMap.companyType eq "company1"}'> selected="selected"</c:if>>한국캐리어</option>
							            <option value="company2" <c:if test='${paramMap.companyType eq "company2" }'> selected="selected"</c:if>>한국카캐리어(주)</option>
							        </select>
							    </div>
                            </td> --%>
				            <td class="widthAuto" style="width:500px;">
				            	<select name="searchDateType"  id="searchDateType">
									<option value="" <c:if test='${paramMap.searchDateType eq "" }'> selected="selected"</c:if> >선택</option>
									<option value="S" <c:if test='${paramMap.searchDateType eq "S" }'> selected="selected"</c:if> >의뢰일</option>
									<option value="D" <c:if test='${paramMap.searchDateType eq "D" }'> selected="selected"<</c:if> >출발일</option>
								</select>
				                <input style="width:40%; text-align:center;" class="datepick" type="text" placeholder="조회시작일" onclick="javascript:$(this).val('');"  readonly="readonly" name="startDt" id="startDt" value="${paramMap.startDt}">&nbsp;~&nbsp;
				                <input style="width:40%; text-align:center;" class="datepick" type="text" placeholder="조회종료일" onclick="javascript:$(this).val('');" readonly="readonly" name="endDt" id="endDt"  value="${paramMap.endDt}">
				            </td>
				            <%-- <td style="width: 145px;">
                                <div class="select-con">
							        <select class="dropdown" name="searchType" id="searchType">
							        	<option value="" <c:if test='${paramMap.searchType eq "" }'> selected="selected"</c:if>>조회구분 선택</option>
							            <option value="00" <c:if test='${paramMap.searchType eq "00" }'> selected="selected"</c:if>>기사명</option>
							        </select>
							        <span></span>
							    </div>
                            </td> --%>
                            <td>
                                <input  style="width: 150px;" type="text" id="searchWord" name="searchWord" value="${paramMap.searchWord}" placeholder="매입처명 입력"   onkeypress="if(event.keyCode=='13') search();">
                            </td>
				            <td>
				            	<div class="select-con">
							        <select style="width:100px;" class="dropdown" name="carrierType" id="carrierType" onchange="javascript:goSearch(this);">
							        	<option value="A" <c:if test='${paramMap.carrierType eq "A" }'> selected="selected"</c:if>>전체</option>
							            <option value="S" <c:if test='${paramMap.carrierType eq "S" }'> selected="selected"</c:if>>셀프</option>
							            <option value="C" <c:if test='${paramMap.carrierType eq "C" }'> selected="selected"</c:if>>캐리어</option>
							        </select>
							        <span></span>
							    </div>
				            </td>
				            <td>
                        		<%-- <input type="checkbox" style="width:15px;" id="includeZero" <c:if test='${paramMap.includeZero eq "Y" }'> checked</c:if> class="btn-primary" onclick="javascript:search();"> --%>
                            </td>
                            <td>
                        		<!-- <div>0건매출처포함</div> -->
                            </td>
                        	<td>
                        		<input type="button" id="searchStatusU" name="" value="매출처 정산" class="btn-normal" onclick="javascript:document.location.href='/account/customerCal.do';">
                                <!-- <input type="button" id="searchStatusU" name="" value="매출처 정산" class="btn-primary" onclick="javascript:searchStatusChange('U',this);"> -->
                            </td>
                        	<td>					
                        		<input type="button" id="searchStatusI" name="" value="매입처 정산" class="btn-primary" onclick="javascript:search();">
                                <!-- <input type="button" id="searchStatusI" name="" value="매입처 정산" class="btn-normal" onclick="javascript:searchStatusChange('I',this);"> -->
                            </td>
                            <td>
                            	<!-- <input type="button" id="searchStatusO" name="" value="기사 정산" class="btn-normal" onclick="javascript:searchStatusChange('O',this);"> -->
                                <!-- <input type="button" id="searchStatusO" name="" value="기사 정산" class="btn-normal" onclick="javascript:searchStatusChange('O',this);"> -->
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </section>

			<div class="confirmation" style="min-height:0px; text-align:left; margin-top:-10px; margin-left:290px; margin-bottom:10px;">
				<div class="<c:if test='${paramMap.decideStatus eq "A" }'> confirm</c:if><c:if test='${paramMap.decideStatus eq "Y" }'> cancel</c:if><c:if test='${paramMap.decideStatus eq "N" }'> cancel</c:if>">
	            	<a style="cursor:pointer;" onclick="javascript:decideStatus('A');">전체</a>
	            </div>
	            <div class="<c:if test='${paramMap.decideStatus eq "A" }'> cancel</c:if><c:if test='${paramMap.decideStatus eq "Y" }'> cancel</c:if><c:if test='${paramMap.decideStatus eq "N" }'> confirm</c:if>">
	            	<a style="cursor:pointer;" onclick="javascript:decideStatus('N');">미확정</a>
	            </div>
	            <div class="<c:if test='${paramMap.decideStatus eq "A" }'> cancel</c:if><c:if test='${paramMap.decideStatus eq "Y" }'> confirm</c:if><c:if test='${paramMap.decideStatus eq "N" }'> cancel</c:if>">
	            	<a style="cursor:pointer;" onclick="javascript:decideStatus('Y');">확정</a>
	            </div>
	            <div class="cancel">
	            	<!-- <a style="cursor:pointer;" onclick="">세금계산서 청구요청</a> -->
	            	<a style="cursor:pointer;" onclick="">청구요청</a>
	            </div>
	            <!-- <div class="cancel">
	            	<a style="cursor:pointer;" onclick="javascript:decideFinal('N');">최종확정대기중</a>
	            </div>
	            <div class="cancel">
	            	<a style="cursor:pointer;" onclick="javascript:decideFinal('Y');">최종확정완료</a>
	            </div> -->
            </div>

            <section class="bottom-table" style="width:1600px; margin-left:290px;">
            	                                        
                <table class="article-table" style="margin-top:25px; table-layout:fixed;">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                        	<td style="width:40px;"><input type="checkbox" id="checkAll" onclick="javascript:checkAll();" ></td>
                        	
                            <!-- <td style="width:250px; text-align:center; ">소속</td> -->
                            <td style="text-align:center; cursor:pointer;" onclick="javascript:sortBy('A');">매입처명<c:if test='${paramMap.forOrder eq "A" }'><c:if test='${paramMap.order eq "asc" }'><img style="margin-left:10px;" src="/img/arrow-up.png" alt=""></c:if><c:if test='${paramMap.order eq "desc" }'><img style="margin-left:10px;" src="/img/arrow-down.png" alt=""></c:if></c:if></td>
                            <td style="text-align:center;" onclick="">구분</td>
                            <td style="text-align:center; width:300px;" onclick="">세금계산서 발행 구분<c:if test='${paramMap.forOrder eq "B" }'><c:if test='${paramMap.order eq "asc" }'><img style="margin-left:10px;" src="/img/arrow-up.png" alt=""></c:if><c:if test='${paramMap.order eq "desc" }'><img style="margin-left:10px;" src="/img/arrow-down.png" alt=""></c:if></c:if></td>
                            <td style="text-align:center;" onclick="">매출액</td>
                            <td style="text-align:center; cursor:pointer;" onclick="javascript:sortBy('D');">매입건수 <c:if test='${paramMap.forOrder eq "D" }'><c:if test='${paramMap.order eq "asc" }'><img style="margin-left:10px;" src="/img/arrow-up.png" alt=""></c:if><c:if test='${paramMap.order eq "desc" }'><img style="margin-left:10px;" src="/img/arrow-down.png" alt=""></c:if></c:if></td>
                            <td style="text-align:center; cursor:pointer;" onclick="javascript:sortBy('C');">매입액 <c:if test='${paramMap.forOrder eq "C" }'><c:if test='${paramMap.order eq "asc" }'><img style="margin-left:10px;" src="/img/arrow-up.png" alt=""></c:if><c:if test='${paramMap.order eq "desc" }'><img style="margin-left:10px;" src="/img/arrow-down.png" alt=""></c:if></c:if></td>
                            <td style="text-align:center;" onclick="">현장수금액</td>
                            <td style="text-align:center;" onclick="">총매입액</td>
                            <c:if test='${paramMap.decideStatus eq "Y" }'>
                            	<td style="text-align:center;" onclick="">확정월</td>
                            </c:if>
                            <c:if test="${user.emp_role == 'A'}">
                            </c:if>
                            <%-- <c:if test='${paramMap.decideStatus eq "Y" }'>
                            	<td style="text-align:center;">세금계산서발행일</td>
                            </c:if> --%>
                        </tr>
                    </thead>
                    <tbody id="dataArea">
                    		<%-- <tr class="ui-state-default"> 
								<td>${totalCount}</td>
	                            <td style="text-align:center;"></td>
	                            <td style="text-align:center;"></td>
	                            <td style="text-align:center;"></td>
	                            <td style="text-align:center;"></td>
                        	</tr> --%>
                    	<c:forEach var="data" items="${listData}" varStatus="status">
							<tr class="ui-state-default"> 
								<td><input type="checkbox" name="forDecideStatus" anotherId="${data.payment_partner_id}" decideMonth="${data.decide_month}"></td>
								<%-- 
								    <td style="text-align:center;"><c:if test ='${data.companyType eq "company1"}'>한국캐리어</c:if>
	                            											<c:if test ='${data.companyType eq "company2"}'>한국카캐리어(주)</c:if></td> --%>
	                            <td style="text-align:center;  cursor:pointer;" onclick="javascript:viewTotalSales('${data.payment_partner_id}','${data.decide_month}');">${data.payment_partner}</td>
	                            <td style="text-align:center;">
	                            	<c:if test="${data.driver_kind == '00'}">
		                            	직영
		                            </c:if>
		                            <c:if test="${data.driver_kind == '01'}">
		                            	지입
		                            </c:if>
		                            <c:if test="${data.driver_kind == '02'}">
		                            	외부
		                            </c:if>
		                            <c:if test="${data.driver_kind == '99'}">
		                            	외부(용차)
		                            </c:if>
								</td>
	                            <td style="text-align:center;">&nbsp;발행&nbsp;:&nbsp;${data.billing_cnt}&nbsp;/&nbsp;미발행&nbsp;:&nbsp;${data.cash_cnt}&nbsp;/&nbsp;현장수금&nbsp;:&nbsp;${data.dd_cnt}</td>
	                            <td style="text-align:center;">${data.sales}</td>
	                            <td style="text-align:center;">${data.cnt}건</td>
	                            <td style="text-align:center;">${data.another_sales}</td>
	                            <td style="text-align:center;">${data.dd_sales}</td>
	                            <td style="text-align:center;">${data.total_sales}</td>
	                            <c:if test='${paramMap.decideStatus eq "Y" }'>
	                            	<td style="text-align:center;">${data.decide_month}</td>
	                            </c:if>
	                            <%-- <c:if test='${paramMap.decideStatus eq "Y" }'>
	                            	<td style="text-align:center;"><input style="text-align:center;" class="datepick" type="text" placeholder="발행일 입력" onclick="javascript:$(this).val('');" readonly="readonly" value=""></td>
	                            </c:if> --%>
                        	</tr>
						</c:forEach>
						<tr class="ui-state-default"> 
								<c:if test='${paramMap.decideStatus ne "A" }'>
									<td style="background:#e1e1e1;"></td>
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;">통&nbsp;&nbsp;&nbsp;계</td>
		                        
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;"></td>
		                         <!--    <td style="text-align:center; background:#e1e1e1; font-weight:bold;"></td> -->
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;"><c:if test='${paramMap.decideStatus eq "Y" }'> 확정&nbsp;</c:if><c:if test='${paramMap.decideStatus eq "N" }'> 미확정&nbsp;</c:if>매입처 수 : ${totalStatistics.another_cnt}</td>
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;"></td>
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;"><c:if test='${paramMap.decideStatus eq "Y" }'> 확정&nbsp;</c:if><c:if test='${paramMap.decideStatus eq "N" }'> 미확정&nbsp;</c:if>건 수 : ${totalStatistics.cnt}건</td>
		                            
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;"><c:if test='${paramMap.decideStatus eq "Y" }'> &nbsp;</c:if><c:if test='${paramMap.decideStatus eq "N" }'> &nbsp;</c:if>매입 : ${totalStatistics.another_sales}원</td>
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;"><c:if test='${paramMap.decideStatus eq "Y" }'> &nbsp;</c:if><c:if test='${paramMap.decideStatus eq "N" }'> &nbsp;</c:if>현장수금액 : ${totalStatistics.dd_sales}원</td>
		                            
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;"><c:if test='${paramMap.decideStatus eq "Y" }'> &nbsp;</c:if><c:if test='${paramMap.decideStatus eq "N" }'> &nbsp;</c:if>총매입 : ${totalStatistics.total_sales}원</td>
		                            <c:if test='${paramMap.decideStatus eq "Y" }'>
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;"></td>
		                            </c:if>
		                            <%-- <c:if test='${paramMap.decideStatus eq "Y" }'>
		                            	<td style="background:#e1e1e1;"></td>
		                            </c:if> --%>
	                            </c:if>
	                            <c:if test='${paramMap.decideStatus eq "A" }'>
									<td style="background:#e1e1e1;"></td>
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;">통&nbsp;&nbsp;&nbsp;계</td>
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;"></td>
		                            <!-- <td style="text-align:center; background:#e1e1e1; font-weight:bold;"></td> -->
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;"> 총 매입처 수 : ${totalStatistics.another_cnt}&nbsp;&nbsp;<br> 미확정 매입처 수 : ${totalStatisticsN.another_cnt}&nbsp;&nbsp;<br> 확정 매입처 수 : ${totalStatisticsY.another_cnt}</td>
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;"></td>
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;"> 총 건 수 : ${totalStatistics.cnt}건&nbsp;&nbsp;<br> 미확정 건 수 : ${totalStatisticsN.cnt}건&nbsp;&nbsp;<br> 확정 건 수 : ${totalStatisticsY.cnt}건</td>
		                            
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;"> 매입 : ${totalStatistics.another_sales}건&nbsp;&nbsp;<br> 미확정 매입 : ${totalStatisticsN.another_sales}건&nbsp;&nbsp; <br>확정 매입 : ${totalStatisticsY.another_sales}건</td>
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;"> 총 현장수금액 : ${totalStatistics.dd_sales}건&nbsp;&nbsp; <br>미확정 현장수금액 : ${totalStatisticsN.dd_sales}건&nbsp;&nbsp; <br>확정 현장수금액 : ${totalStatisticsY.dd_sales}건</td>
		                            
		                            <td style="text-align:center; background:#e1e1e1; font-weight:bold;"> 총 매입 : ${totalStatistics.total_sales}원&nbsp;&nbsp;<br> 미확정 : ${totalStatisticsN.total_sales}원&nbsp;&nbsp;<br> 확정 : ${totalStatisticsY.total_sales}원</td>
	                            </c:if>
                        	</tr>
                    </tbody>
                </table>
                <div class="table-pagination text-center">
                    <ul class="pagination">
                        <!-- <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="curr-page"><a href="#">1</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li> -->
                        <%-- <html:paging uri="/account/anotherCal.do" forGroup="&decideStatus=${paramMap.decideStatus}&forOrder=${paramMap.forOrder}%5E${paramMap.order}&includeZero=${paramMap.includeZero}&carrierType=${paramMap.carrierType}&startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&searchType=${paramMap.searchType}&searchWord=${paramMap.searchWord}&searchDateType=${paramMap.searchDateType}&customerId=${paramMap.customerId}" frontYn="N" /> --%>
                        <html:paging uri="/account/anotherCal.do" forGroup="&decideStatus=${paramMap.decideStatus}&forOrder=${paramMap.forOrder}%5E${paramMap.order}&includeZero=${paramMap.includeZero}&carrierType=${paramMap.carrierType}&startDt=${paramMap.startDt}&endDt=${paramMap.endDt}&searchType=00&searchWord=${paramMap.searchWord}&searchDateType=${paramMap.searchDateType}&customerId=${paramMap.customerId}" frontYn="N" />
                    </ul>
                </div>
                <div class="confirmation">
                	<c:if test='${paramMap.decideStatus eq "N" }'>
	                   <div class="confirm">
	                    	<a style="cursor:pointer;" onclick="javascript:decideCheck('Y')">일괄확정</a>
	                    </div>
	                    <!-- <div class="cancel">
	                    	<a style="cursor:pointer;" onclick="javascript:sendMail();">메일전송</a>
	                    </div> -->
	                    <div class="cancel">
	                    	<a style="cursor:pointer;" onclick="javascript:excelDownload();">엑셀다운로드</a>
	                    </div>
                    </c:if>
                    <c:if test='${paramMap.decideStatus eq "Y" }'>
	                   <div class="confirm">
	                    	<a style="cursor:pointer;" onclick="javascript:decideCheck('N')">수정/확정취소</a>
	                    </div>
	                    <div class="cancel">
	                    	<a style="cursor:pointer;" onclick="javascript:excelDownload();">엑셀다운로드</a>
	                    </div>
	                    <c:if test="${user.emp_role == 'A' || user.emp_id=='hk0009'}">
		                    <div class="cancel">
		                    	<a style="cursor:pointer;" onclick="javascript:decideFinalCheck('Y');">기사 지급금 최종확정</a>
		                    </div>
	                    </c:if>
                    </c:if>
                    <c:if test='${paramMap.decideStatus eq "A" }'>
	                    <div class="confirm">
	                    	<a style="cursor:pointer;" onclick="javascript:excelDownload();">엑셀다운로드</a>
	                    </div>
                    </c:if>
                </div>
                
                <div style="color:#8B0000;">※각 컬럼을 클릭 하여 오름차순 또는 내림차순으로 정렬 할 수 있으며 오름차순->내림차순->초기화 순으로 동작 합니다.</div>
                
                
                
                
            </section>
     
