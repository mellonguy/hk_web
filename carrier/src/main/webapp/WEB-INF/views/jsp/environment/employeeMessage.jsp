<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>
<%

%>
<style type="text/css">
.tg th {
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	padding: 10px 5px;
}

.tg .tg-uzvj {
	padding: 10px 5px;
	text-align: center;
	vertical-align: middle
}

</style>

<script type="text/javascript">
$(document).ready(function(){

	$("#evaluationMonth").MonthPicker({ 

		Button: false
		,MonthFormat: 'yy-mm'	
		,OnAfterChooseMonth: function() { 
	        //alert($(this).val());
	        document.location.href = "/environment/evaluation-empList.do?&evaluationMonth="+$(this).val();
	        
	     }
	});
	
});

function editEmp(empId){
	
	document.location.href = "/baseinfo/edit-employee.do?empId="+empId;
}

function addEmp(){
	document.location.href = "/baseinfo/add-employee.do";
}


function search(){
	
	document.location.href = "/baseinfo/employee.do?&searchWord="+encodeURI($("#searchWord").val());
	
}

function goEvaluation_register(emp_id,evaluationMonth){

	document.location.href = "/environment/evaluation-register.do?&empId="+emp_id+"&evaluationMonth="+"${paramMap.evaluationMonth}";

	
}

function showModal(obj){
	selectDriverObj =obj; 

}

function goShowMessageHistory(){

	$("#showMessgageHistory").show();

	
}



function goDeleteMessage(messageIdArr){


	$.ajax({
		type : 'post',
		url : "/environment/deleteEmployeeMessage.do",
		dataType : 'json',
		traditional : true,
		data : {
			messageIdArr : messageIdArr,
			sendMessage :"${paramMap.sendMessage}",
		},
		success : function(data, textStatus, jqXHR) {
			var result = data.resultCode; 
			var resultData = data.resultData;
			if (result == "0000") {
				//alert("삭제가 완료되었습니다.");
				window.location.reload();
			} else if (result == "0001") {
				

			}
		},
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
	
	
}


function goMessage(sendMessage){

	document.location.href ="/environment/employeeMessage.do?&sendMessage="+sendMessage;

}


function showMessageList(text){
	
	$("#showMessageList").show();	
	$("#messageDetatil").val(text);	
	
	
}

function goInsertMessageEmp(empId){

	$("#sendEmployeeMessageJsp").show();	
	$("#empIdHidden").val(empId);	
	

}


function goEmployeeSendMessageJsp(){


	if($("#textAreaJsp").val() == ""){
		alert("내용을 입력해주세요");
	
	}else{

		
	
	$.ajax({
		type : 'post',
		url : "/allocation/sendEmployeeMessage.do",
		dataType : 'json',
		data : {
			empId : $('#empIdHidden').val(),
			textarea : $("textarea#textAreaJsp").val(), 
		},
		success : function(data, textStatus, jqXHR) {
			var result = data.resultCode;
			var resultData = data.resultData;
			if (result == "0000") {
				alert("쪽지를 전송했습니다.");
				window.location.reload();
			}else if (result == "0001") {

			}
		},
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});

	}
}




function goAlldelete(obj){


	var messageIdArr = new Array();

	var checkboxTotal = $('input:checkbox[name="forEmpCheckbox"]:checked').length;
	
	if(checkboxTotal == 0){
		alert("목록이 선택 되지 않았습니다.");
		return false;

	}else if(checkboxTotal > 0){

		if(confirm(checkboxTotal+"건의 쪽지를 삭제하시겠습니까?")){
		

			 $('input:checkbox[name="forEmpCheckbox"]:checked').each(function(index,element) {

				 messageIdArr.push($(this).attr("messageId"));

				 goDeleteMessage(messageIdArr);

				});
		}

		
		}else{
		alert("취소됨");
		 return false;
			}
	}
 





</script>

<div class="modal-field" id="showMessageList">
		<div class="modal-box">
			<h3 class="text-center">쪽지 내용</h3>
			<div class="modal-table-container">
				<table class="article-table">
					<colgroup>

					</colgroup>

					<br>
					<!-- <span style="color: #00f;"> 쪽지</span>를 작성하고 전송버튼을 눌러주세요. -->
					<br>
					<input type="hidden" id="" value="">
					<br>
					<tr>
						<textarea id="messageDetatil" rows="15" cols="100"
							placeholder="" style="margin-left: 5%;" readOnly></textarea>
					</tr>
					<br>
					</thead>

				</table>
			</div>

			<div class="confirmation">
				<div class="cancel">
					<input type="button" value="취소" name="" >
				</div>
			<!-- 	<div class="confirm">
					<input type="button" value="전송" name="" onclick="javascript:goEmployeeSendMessage($('#empIdBox').val());">
				</div> -->
			</div>

		</div>
	</div>
	
	
		<div class="modal-field" id="sendEmployeeMessageJsp">
		<div class="modal-box">
			<h3 class="text-center">쪽지 보내기</h3>
			<div class="modal-table-container">
				<table class="article-table">
					<colgroup>

					</colgroup>

					<br>
					<span style="color: #00f;"> 쪽지</span>를 작성하고 전송버튼을 눌러주세요.
					<br>
					<input type="hidden" id="empIdHidden" value="">
					<br>
					<tr>
						<textarea id="textAreaJsp" rows="15" cols="100"
							placeholder="쪽지 내용을 입력해주세요." style="margin-left: 5%;"></textarea>
					</tr>
					<br>
					</thead>

				</table>
			</div>

			<div class="confirmation">
				<div class="cancel">
					<input type="button" value="취소" name="" >
				</div>
				<div class="confirm">
					<input type="button" value="전송" name="" onclick="javascript:goEmployeeSendMessageJsp();">
				</div>
			</div>

		</div>
	</div>
	
	

        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix" style="margin-left:-200px;">
                <ul>
                    <li><a href="">HOME</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">(주)한국카캐리어 쪽지함</a></li>
                </ul>
            </div>

		<div class="dispatch-btn-container">	
				<div style="width:350px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">(주)한국카캐리어 쪽지함</div>					
            </div>
       
    
      <br>



 📧<span style="color: #00f;">쪽지 내용</span>을 클릭하면 내용을 더 쉽게 보실 수 있습니다.
<br>
<br>


  

<!-- <span style="color: #00f;">최종평점</span>은 관리자 모두가 평가를 마쳤을 시 표시 됩니다. -->
 <!-- <span style="color: #00f;">최종평점리스트</span>를 누르시면 최종평점을 볼 수 있습니다. -->
	<div class="up-dl clearfix header-search">
		<table>
			<tbody>
				<tr>
					<td></td>

				</tr>
			</tbody>
		</table>
	</div>



</section>
   <section class="bottom-table" style="width:1600px; margin-left:290px; ">
  
  
             
          <%--       
                <table class="article-table" style="margin-top:15px">
                    <colgroup>
                        <div class="dispatch-btn-container"  >	
								<div style="width:350px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">쪽지 내역 📧</div>					
           			 	</div>
                    </colgroup>
                    <thead>
                        <tr>
                            <!-- <td style="text-align:center;">기사아이디</td> -->
                            <td style="text-align:center; width:50px;"><input type="checkbox" id="checkAll" onclick="javascript:checkAll();" ></td>
                            <td style="text-align:center; width:auto;">직원이름</td>
                            <td style="text-align:center; width:auto;">직급</td>
                            <td style="text-align:center; width:auto;">부서</td>
                            <td style="text-align:center; width:auto;">쪽지 내용</td>
                
                            <!-- <td style="text-align:center; width:auto;">최종 평점</td> -->
                        </tr>
                    </thead>
                    <tbody id="">
                    	<c:forEach var="data" items="${employeeNoteList}" varStatus="status">
							<tr class="ui-state-default" > 
	                            <td style="text-align:center;"><input type="checkbox" name="forDeductStatus" ></td>
	                            <td style="text-align:center;" onclick="">${data.emp_name}</td>
	                            <td style="text-align:center;">${data.emp_position}</td>
	                           
	                            <c:if test="${data.allocation eq 'C'}" ><td style="text-align:center;" >배차(캐리어)</td></c:if>
	                            <c:if test="${data.allocation eq 'S'}" ><td style="text-align:center;" >배차(셀프)</td></c:if>
	                           <c:if test="${data.allocation eq 'D'}" > <td style="text-align:center;" >개발</td></c:if>
	                            <c:if test="${data.allocation eq 'M'}" ><td style="text-align:center;" >회계</td></c:if>
	                            <c:if test="${data.allocation eq 'N'}" ><td style="text-align:center;" >경영관리</td></c:if>
	                         
	                            
	                      
	                            <td style="text-align:center;">${data.test}</td>
                        	   <td style="text-align:center;"> 
	                        	   	<input type="button"  class="btn-warning btn" style="" value="보기" onclick="javascript:goShowMessageHistory();">
	                        	</td>
                        	
                        	</tr>
                        	
						</c:forEach>
                   
                    </tbody>
                    

                </table>
                --%>
               
               
               
                 
                <table class="article-table" style="margin-top:30px">
                    <colgroup>
                    
                        <%-- <h3>${user.emp_name}님의 쪽지함</h3> --%>
				<div class="dispatch-btn-container"  style="margin-top:20px">	
					<div style="width:350px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">${user.emp_name} 님의 쪽지함 📬</div>					
            	</div>
            	
            	<div class=""  style="margin-top:20px; text-align:right; ">	
						<input type="button"  onclick ="javascript:goMessage('N');" value="📥 받은 쪽지함" style="width:150px; margin:auto; text-align:center; font-weight:bold; font-size:15px; color:blue;">					
						<input type="button"  onclick ="javascript:goMessage('Y');"  value="📤 보낸 쪽지함" style="width:150px; margin:auto; text-align:center; font-weight:bold; font-size:15px; color:green;">					
            	</div>
                    </colgroup>
                    <thead>
                        <tr>
                            <!-- <td style="text-align:center;">기사아이디</td> -->
                            <td style="text-align:center; width:50px;"><input type="checkbox" id="checkAll" onclick="javascript:checkAll();" ></td>
                            <td style="text-align:center; width:auto;">직원명</td>
                            <td style="text-align:center; width:auto;">쪽지 내용</td>
                            <td style="text-align:center; width:auto;">답장 하기</td>
                  
           
                        </tr>
                    </thead>
                    <tbody id="">
                    	<c:forEach var="data" items="${employeeMineList}" varStatus="status">
                    	
							<tr class="ui-state-default" > 
	                            <td style="text-align:center;"><input type="checkbox" messageId="${data.message_id}" name="forEmpCheckbox" ></td>
	                            <td style="text-align:center;" onclick="">${data.send_emp_name}  -->  ${data.receive_emp_name}</td>
	                            <td style="text-align:center;" onclick="javascript:showMessageList('${data.text}');">${data.text}</td>
		                            <c:if test ="${pararmMap.sendMessage eq 'N' || pararmMap.sendMessage eq null}">
			                            <td style="text-align:center;">
			                            	<input type="button"  class="btn-warning btn" style="" value="작성" onclick="javascript:goInsertMessageEmp('${data.send_emp_id}');">
			                            </td>
	                        		</c:if>
                        	</tr>	
                        	
                        	
                        	
						</c:forEach>
                   
                    </tbody>
                </table>
               
	                  	<div class="confirmation">
		                    <div class="confirm">
		       				<!--          <a href="/baseinfo/add-employee.do">직원등록</a> -->
		                   		<input type="button"  class="btn-warning btn" style="" value="선택한 목록 쪽지 삭제" onclick="javascript:goAlldelete();">
		                    </div>
		                 </div>
		                  
               
               
                
            </section>
     
