<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>
<%

%>
<script type="text/javascript">
$(document).ready(function(){

});


function getEmp(empId,status,obj){
	
		$.ajax({ 
			type: 'post' ,
			url : "/environment/getEmp.do" ,
			dataType : 'json' ,
			data : {
				empId : empId
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					
					if(status == "S" || status == "C" || status == "M" || status == "N" || status == "D"){
						if(status != resultData.allocation){
							editUser(empId,status,obj);	
						}
					}else{
						if(status != resultData.emp_role){
							editUser(empId,status,obj);	
						}	
					}
					
				}else if(result == "0002"){
					alert("변경 하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
 
}


function editUser(empId,status,obj){
	
	var callUrl = "";
	if(status == "S" || status == "C" || status == "M" || status == "N" || status == "D"){
		callUrl = "/environment/updateEmpAllocation.do";
	}else{
		callUrl = "/environment/updateEmpRole.do";
	}
	
   	if(confirm("변경 하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : callUrl ,
			dataType : 'json' ,
			data : {
				empId : empId,
				status : status
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
					if(result == "0000"){
						alert("변경 되었습니다.");
						document.location.href = "/environment/user-role.do?searchType="+$("#searchType").val()+"&searchWord="+encodeURI($("#searchWord").val());
					}else if(result == "0002"){
						alert("변경 하는데 실패 하였습니다.");
					}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	} 
	
}


function search(){
	
	document.location.href = "/environment/user-role.do?searchType="+$("#searchType").val()+"&searchWord="+encodeURI($("#searchWord").val());
	
}


</script>




		<!-- <section class="side-nav">
            <ul>
                <li class="active"><a href="/environment/admin.do">관리자 기본정보</a></li>
            </ul>
        </section>
 -->
        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">HOME</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">관리자</a></li>
                </ul>
            </div>

            <div class="up-dl clearfix header-search">
                 <table>
                    <tbody>
                        <tr>
                            <td style="width: 145px;">
                                <div class="select-con">
							        <select class="dropdown" name="searchType" id="searchType">
							        	<option value="ALL" <c:if test='${paramMap.searchType eq "ALL" }'> selected="selected"</c:if>>전체</option>
							            <option value="A" <c:if test='${paramMap.searchType eq "A" }'> selected="selected"</c:if>>관리자</option>
							            <option value="U" <c:if test='${paramMap.searchType eq "U" }'> selected="selected"</c:if>>사용자</option>
							            <option value="E" <c:if test='${paramMap.searchType eq "E" }'> selected="selected"</c:if>>기타</option>
							        </select>
							        <span></span>
							    </div>
                            </td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="검색어 입력" name="searchWord" id="searchWord" value="${paramMap.searchWord}">
                            </td>
                            <td>
                                <input type="button" id="btn-search" value="검색" class="btn-primary" onclick="javascript:search();">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
<!-- 
            <div class="dispatch-btn-container">
               
                <div class="dispatch-btn">
                    <i class="fa fa-angle-down active" aria-hidden="true"></i>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div>
            </div> -->
        </section>

        <div class="dispatch-wrapper" style="width:100%;">
            <!-- <section class="dispatch-bottom-content">
                
            </section> -->
            
            
            <section class="bottom-table" style="width:1600px; margin-left:290px;">
                <table class="article-table"  style="margin-top:15px">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <td style="text-align:center;">아이디</td>
                            <td style="text-align:center;">이름</td>
                            <td style="text-align:center;">재직상태</td>
                            <td style="text-align:center;">승인상태</td>
                            <td style="text-align:center;">직급</td>
                            <td style="text-align:center;">연락처</td>
                            <td style="text-align:center;">역할</td>
                            <td style="text-align:center;">권한관리</td>                            
                            <td style="text-align:center;">등록일시</td>
                        </tr>
                    </thead>
                    <tbody id="">
                    	<c:forEach var="data" items="${listData}" varStatus="status">
							<tr class="ui-state-default"> 
                            <td style="text-align:center;">${data.emp_id}</td>
                            <td style="text-align:center;">${data.emp_name}</td>
                            <c:if test='${data.emp_status eq "01" }'>
                            	<td style="text-align:center;">재직</td>
                            </c:if>
                            <c:if test='${data.emp_status eq "02" }'>
                            	<td style="text-align:center;">휴직</td>
                            </c:if>
                            <c:if test='${data.emp_status eq "03" }'>
                            	<td style="text-align:center;">퇴사</td>
                            </c:if>
                            <c:if test='${data.approve_yn eq "Y" }'>
                            	<td style="text-align:center;">승인</td>
                            </c:if>
                            <c:if test='${data.approve_yn eq "N" }'>
                            	<td style="text-align:center;">승인대기중</td>
                            </c:if>
                            <td style="text-align:center;">${data.emp_position}</td>
                            <td style="text-align:center;">${data.phone_personal}</td>
                            <td style="text-align:center;">
	                            <input type="radio" onclick="javascript:getEmp('${data.emp_id}','S',this);" id="allocationS${data.emp_id}" name="allocation${data.emp_id}" value="S" <c:if test="${data.allocation eq 'S'}">checked="checked"</c:if>>
								<label for="allocationS${data.emp_id}">셀프</label>
								<input style="margin-left:10px;" type="radio" onclick="javascript:getEmp('${data.emp_id}','C',this);" id="allocationC${data.emp_id}" name="allocation${data.emp_id}" value="C" <c:if test="${data.allocation eq 'C'}">checked="checked"</c:if>>
								<label for="allocationC${data.emp_id}">캐리어</label>
								<input style="margin-left:10px;" type="radio" onclick="javascript:getEmp('${data.emp_id}','D',this);" id="allocationD${data.emp_id}" name="allocation${data.emp_id}" value="D" <c:if test="${data.allocation eq 'D'}">checked="checked"</c:if>>
								<label for="allocationD${data.emp_id}">개발</label>
								<input style="margin-left:10px;" type="radio" onclick="javascript:getEmp('${data.emp_id}','M',this);" id="allocationM${data.emp_id}" name="allocation${data.emp_id}" value="M" <c:if test="${data.allocation eq 'M'}">checked="checked"</c:if>>
								<label for="allocationM${data.emp_id}">경영관리</label>
								<input style="margin-left:10px;" type="radio" onclick="javascript:getEmp('${data.emp_id}','N',this);" id="allocationN${data.emp_id}" name="allocation${data.emp_id}" value="N" <c:if test="${data.allocation eq 'N'}">checked="checked"</c:if>>
								<label for="allocationN${data.emp_id}">아님</label>
                            </td>
                            <td style="text-align:center;">
	                            <input type="radio" onclick="javascript:getEmp('${data.emp_id}','A',this);" id="empRoleA${data.emp_id}" name="empRole${data.emp_id}" value="A" <c:if test="${data.emp_role eq 'A'}">checked="checked"</c:if>>
								<label for="empRoleA${data.emp_id}">관리자</label>
								<input style="margin-left:10px;" type="radio" onclick="javascript:getEmp('${data.emp_id}','U',this);" id="empRoleU${data.emp_id}" name="empRole${data.emp_id}" value="U" <c:if test="${data.emp_role eq 'U'}">checked="checked"</c:if>>
								<label for="empRoleU${data.emp_id}">사용자</label>
								<input style="margin-left:10px;" type="radio" onclick="javascript:getEmp('${data.emp_id}','E',this);" id="empRoleE${data.emp_id}" name="empRole${data.emp_id}" value="E" <c:if test="${data.emp_role eq 'E'}">checked="checked"</c:if>>
								<label for="empRoleE${data.emp_id}">기타</label>
                            </td>
                            <td style="text-align:center;">${data.reg_dt_str}  ${data.reg_dt_si}</td>
                        </tr>
						</c:forEach>
                    </tbody>
                </table>
                <div class="table-pagination text-center">
                    <ul class="pagination">
                        <html:paging uri="/environment/user-role.do" frontYn="N" forGroup="&searchType=${paramMap.searchType}&searchWord=${paramMap.searchWord}"/>
                    </ul>
                </div>
            </section>
        </div>



     
        <script>

        </script>

