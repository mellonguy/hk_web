<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>
<%

%>
<style type="text/css">
.tg th {
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	padding: 10px 5px;
}

.tg .tg-uzvj {
	padding: 10px 5px;
	text-align: center;
	vertical-align: middle
}

</style>

<script type="text/javascript">
$(document).ready(function(){

/* 
$('form').submit(function(){

var check_submit =confirm('제출 하시겠습니까? ( ※  최종제출버튼을 누르시면 수정이 불가능합니다.)');


return check_submit;

	}); */






});

//t($("input[type='checkbox'][name='chk[]']:checked").length);


function insertEvaluation(decideFinal){







	 var radio = $("input[value =emp_question_id ]:checked").val();

	if(decideFinal == "N"){

		if(confirm("임시저장 하시겠습니까? ")){

			test(decideFinal);		
			
		}else{
			return false;
		}

	}else{

		if(confirm("최종제출 하시겠습니까? (※ 최종제출을 하시면 수정이 불가능합니다.) ")){



			var totalCnt = $("input[type='radio']").length;
			
			var checkedCnt = $("input[type='radio']:checked").length;
			if(checkedCnt == 20 ){
					test(decideFinal);
	            
		 	}else{
				    alert("항목을 모두 선택해주셔야 제출 가능합니다."); 
					return false;
						
		}

		}else{
			return false;
			
			}

}

}


function test(decideFinal){

	$("#decideFinal").val(decideFinal);

	$('form').submit();
	
	
}


function goEmpPage(){

	
	document.location.href = "/environment/evaluation-register.do&empname="+$("#searchName").val();

	
}





function editEmp(empId){
	
	document.location.href = "/baseinfo/edit-employee.do?empId="+empId;
}

function addEmp(){
	document.location.href = "/baseinfo/add-employee.do";
}


function search(){
	
	document.location.href = "/baseinfo/employee.do?&searchWord="+encodeURI($("#searchWord").val());
	
}

function goEvaluationPage(emp_name){

	
	document.location.href = "/environment/evaluation-check.do";


	
}

function showModal(obj){
	selectDriverObj =obj; 
	$('.modal-field').show();
}








</script>


        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">HOME</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">직원 인사 고과 평가</a></li>
                </ul>
            </div>

		<div class="dispatch-btn-container">	
				<div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;">직원 인사 고과 평가 </div>					
            </div>
    <!--         	<div style="text-align: right">
		<input type="button" value="사장님" onclick="javascript:showModal();">
		<input type="button" value="실장님" onclick="">
		<input type="button" value="부장님" onclick="">
	</div>
	
     -->
<%--    <div style="">
                 
         ※ 평가 할<span style="color: #F9BA0E;"> 직원 </span>을 선택해주세요.
         <select name="empName" id="empName">
		<option value=""  selected="selected">직원 이름</option>
		<c:forEach var="data" items="${empList}" varStatus="status">
			<option value="${data.emp_name}" id="searchName">${data.emp_name} /
				${data.emp_position}</option>
		</c:forEach>
	<input type="button" value="검색" onclick="javascript:goEmpPage($(this).prev().val())">
	</select>
	
	
	
	<br>
<!-- 	<div style="text-align: right">
		<input type="button" value="등록" onclick="">
			<input type="button" value="확인" onclick="">
	</div> -->
      </div> --%>
      <br>
	
	

	<div class="up-dl clearfix header-search">
		<table>
			<tbody>
				<tr>
					<td></td>

				</tr>
			</tbody>
		</table>
	</div>
</section>


<section class="bottom-table" style="width: calc(100% - 280px); margin-left: 290px; padding: 1% 98px 8px 98px;">

	<form name="evaluationForm" method="post" action="insert-evaluation.do">
 	<input type="hidden" name="empId" value="${paramMap.empId}"  />
		<input type="hidden" name="evaluationMonth" value="${paramMap.evaluationMonth}"  />
		<input type="hidden" id="decideFinal" name="decideFinal" value=""  />
		
	<div id="bottom-table">
	 평가 문항은 <span style="color:#A716F5;"> 총 20문항 </span>입니다. 체크박스에 선택해주세요.
	<table class="tg article-table" style="table-layout: fixed; margin-top: 15px">
	
		<thead>
			<tr>
				<td class="tg-uzvj" style="width: 100px;">역량요소</td>
				<td class="tg-uzvj" style="width: 150px;">역량지표</td>
				<td class="tg-uzvj">바람직한 행동기술</td>
				<td class="tg-uzvj" style="width: 50px;">배점</td>
				<td class="tg-uzvj" style="width: 200px;">
						1점 &nbsp;&nbsp;&nbsp; 2점&nbsp;&nbsp;&nbsp;  3점&nbsp;&nbsp;&nbsp; 4점&nbsp;&nbsp;&nbsp;  5점 
					<br>				
					<!-- <input type="radio" name="one" value="1" style="margin-left: 8px;" />
					<input type="radio" name="one" value="2" style="margin-left: 10px;" />
					<input type="radio" name="one" value="3" style="margin-left: 10px;"  checked/>
					<input type="radio" name="one" value="4" style="margin-left: 10px;" />
					<input type="radio" name="one" value="5" style="margin-left: 10px;" /> -->
				</td>
			</tr>
		</thead>
		
		<tbody>
				
				<c:set var ="big_category_id" value =""/>
				<c:set var ="mid_category_id" value =""/>
				
			    <c:forEach var="data" items="${evaluationList}" varStatus="status">
	    <tr>            

		<c:if test="${big_category_id ne data.evaluation_ability_id}">
			<c:set var ="big_category_id" value ="${data.evaluation_ability_id}"/>
        	<td class="tg-uzvj" rowspan="${data.topcategory}">${data.evaluation_ability_text}</td>     
		</c:if>
       
             
             
            <!-- 역량요소 -->
  
                                 
		<c:if test="${mid_category_id ne data.evaluation_indicators_id}">
			<c:set var ="mid_category_id" value ="${data.evaluation_indicators_id}"/>
        	<td class="tg-uzvj" rowspan="${data.midcategory}">${data.evaluation_indicators_text}</td>     
		</c:if>
                                              
                                                   
            <!-- 역량지표  -->
      
            <td class="tg-uzvj" style="text-align:left;">${data.evaluation_question_text}</td> <!-- 바람직한 행동기술 -->
            <td class="tg-uzvj">${data.points}</td> <!-- 배점 -->
            
            <td class="tg-uzvj"> <!-- 체크박스 -->
                <input type="radio" name="${data.evaluation_question_id}" id="eTest" value="1" <c:if test="${data.dpoint eq '1'}">checked</c:if>/>
                <input type="radio" name="${data.evaluation_question_id}" id="eTest" value="2" style="margin-left: 15px;" <c:if test="${data.dpoint eq '2'}">checked</c:if>/>
                <input type="radio" name="${data.evaluation_question_id}" id="eTest" value="3" style="margin-left: 15px;" <c:if test="${data.dpoint eq '3'}">checked</c:if>/>
                <input type="radio" name="${data.evaluation_question_id}" id="eTest" value="4" style="margin-left: 15px;" <c:if test="${data.dpoint eq '4'}">checked</c:if>/>
                <input type="radio" name="${data.evaluation_question_id}" id="eTest" value="5" style="margin-left: 15px;" <c:if test="${data.dpoint eq '5'}">checked</c:if>/>
                  
            </td>
        
       
        
        
        
        </tr>
    </c:forEach>
			<tr>
				<td class="tg-uzvj" colspan="5"> 역량 고과 총점 (100점) </td>
				
			</tr> 
		</tbody>
	</table>
	
	
		</div>
		<div style="float: right; margin-right: 10px">
		
			<input type="button" class="btn-primary btn"  value="임시저장"  onclick ="javascript:insertEvaluation('N')">
			<input type="button"  class="btn-primary btn" value="최종제출"  onclick ="javascript:insertEvaluation('Y')">
		</div>
	</form>



	<div class="confirmation">
		<div class="confirm"></div>
	</div>


</section>

