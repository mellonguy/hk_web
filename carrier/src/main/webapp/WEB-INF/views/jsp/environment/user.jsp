<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>
<%

%>
<script type="text/javascript">
$(document).ready(function(){

});


function getEmp(empId,status,obj){
	
		$.ajax({ 
			type: 'post' ,
			url : "/environment/getEmp.do" ,
			dataType : 'json' ,
			data : {
				empId : empId
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					
					if(status == "Y" || status == "N"){
						if(status != resultData.approve_yn){
							editUser(empId,status,obj);	
						}	
					}else{
						if(status != resultData.emp_status){
							editUser(empId,status,obj);	
						}
					}
					
					
					
				}else if(result == "0002"){
					alert("변경 하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
 
}


function editUser(empId,status,obj){
	
	var callUrl = "";
	if(status == "Y" || status == "N"){
		callUrl = "/environment/updateEmpApproveYn.do";
	}else{
		callUrl = "/environment/updateEmpStatus.do";
	}
	
   	if(confirm("변경 하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : callUrl ,
			dataType : 'json' ,
			data : {
				empId : empId,
				status : status
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
					if(result == "0000"){
						alert("변경 되었습니다.");
						document.location.href = "/environment/user.do?searchType="+$("#searchType").val()+"&searchWord="+encodeURI($("#searchWord").val());
					}else if(result == "0002"){
						alert("변경 하는데 실패 하였습니다.");
					}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	} 
	
}


function search(){
	
	document.location.href = "/environment/user.do?searchType="+$("#searchType").val()+"&searchWord="+encodeURI($("#searchWord").val())+"&searchGubun="+$("#searchGubun").val();
	
}


function changeCompany(empId,obj){


	if(confirm("소속회사를 변경 하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : "/environment/updateEmployeeCompanyId.do" ,
			dataType : 'json' ,
			data : {
				empId : empId,
				companyId : $(obj).val()
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					alert("변경 되었습니다.");
					document.location.reload();
				}else if(result == "0002"){
					alert("변경 하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
	}
	
}



</script>


        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">HOME</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">관리자</a></li>
                </ul>
            </div>

            <div class="up-dl clearfix header-search">
                 <table>
                    <tbody>
                        <tr>
                            <td style="width: 100px;">
                                <div class="select-con">
							        <select class="dropdown" name="searchType" id="searchType">
							        	<option value="ALL" <c:if test='${paramMap.searchType eq "ALL" }'> selected="selected"</c:if>>전체</option>
							            <option value="Y" <c:if test='${paramMap.searchType eq "Y" }'> selected="selected"</c:if>>승인</option>
							            <option value="N" <c:if test='${paramMap.searchType eq "N" }'> selected="selected"</c:if>>승인대기</option>
							        </select>
							        <span></span>
							    </div>
                            </td>
                            <td style="width: 145px;">
                                <div class="select-con">
							        <select class="dropdown" style="width:145px;" name="searchGubun" id="searchGubun">
							        	<option value="" <c:if test='${paramMap.searchGubun eq "" }'> selected="selected"</c:if>>재직상태 선택</option>
							            <option value="01" <c:if test='${paramMap.searchGubun eq "01" }'> selected="selected"</c:if>>재직</option>
							            <option value="02" <c:if test='${paramMap.searchGubun eq "02" }'> selected="selected"</c:if>>휴직</option>
							            <option value="03" <c:if test='${paramMap.searchGubun eq "03" }'> selected="selected"</c:if>>퇴사</option>
							        </select>
							        <span></span>
							    </div>
                            </td>
                            
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="검색어 입력" name="searchWord" id="searchWord" value="${paramMap.searchWord}">
                            </td>
                            <td>
                                <input type="button" id="btn-search" value="검색" class="btn-primary" onclick="javascript:search();">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </section>

        <div class="dispatch-wrapper" style="width:100%;">
            <!-- <section class="dispatch-bottom-content">
                
            </section> -->
            
            
            <section class="bottom-table" style="width:1600px; margin-left:290px;">
                <table class="article-table"  style="margin-top:15px">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <td style="text-align:center;">아이디</td>
                            <td style="text-align:center;">이름</td>
                            <td style="text-align:center;">소속</td>
                            <td style="text-align:center;">재직상태</td>
                            <td style="text-align:center;">승인상태</td>
                            <td style="text-align:center;">직급</td>
                            <td style="text-align:center;">연락처</td>
                            <td style="text-align:center;">승인관리</td>
                            <td style="text-align:center;">재직상태관리</td>                            
                            <td style="text-align:center;">등록일시</td>
                        </tr>
                    </thead>
                    <tbody id="">
                    	<c:forEach var="data" items="${listData}" varStatus="status">
							<tr class="ui-state-default"> 
                            <td style="text-align:center;">${data.emp_id}</td>
                            <td style="text-align:center;">${data.emp_name}</td>
                            
                            <td style="text-align:center;">
                            	<select class="dropdown" name="searchType" id="searchType" onchange="javascript:changeCompany('${data.emp_id}',this);">
	                            	<c:forEach var="companyData" items="${companyList}" varStatus="status">
	                            		<option value="${companyData.company_id}" <c:if test='${data.company_id eq companyData.company_id }'> selected="selected"</c:if>>${companyData.company_name}</option>
	                            	</c:forEach>
                            	</select>
                            </td>
                            <c:if test='${data.emp_status eq "01" }'>
                            	<td style="text-align:center;">재직</td>
                            </c:if>
                            <c:if test='${data.emp_status eq "02" }'>
                            	<td style="text-align:center;">휴직</td>
                            </c:if>
                            <c:if test='${data.emp_status eq "03" }'>
                            	<td style="text-align:center;">퇴사</td>
                            </c:if>
                            <c:if test='${data.approve_yn eq "Y" }'>
                            	<td style="text-align:center;">승인</td>
                            </c:if>
                            <c:if test='${data.approve_yn eq "N" }'>
                            	<td style="text-align:center;">승인대기중</td>
                            </c:if>
                            <td style="text-align:center;">${data.emp_position}</td>
                            <td style="text-align:center;">${data.phone_personal}</td>
                            <td style="text-align:center;">
	                            <%-- <a style="cursor:pointer;" onclick="javascript:editUser('${data.emp_id}')" class="btn-edit">승인</a> --%>
                                <input type="radio" onclick="javascript:getEmp('${data.emp_id}','Y',this);" id="approveY${data.emp_id}" name="approve${data.emp_id}" value="Y" <c:if test="${data.approve_yn eq 'Y'}">checked="checked"</c:if>>
							    <label for="approveY">승인</label>
							    <input style="margin-left:10px;" onclick="javascript:getEmp('${data.emp_id}','N',this);" type="radio" id="approveN${data.emp_id}" name="approve${data.emp_id}" value="N" <c:if test="${data.approve_yn eq 'N'}">checked="checked"</c:if>>
							    <label for="approveN">승인대기</label>
                            </td>
                            <td style="text-align:center;">
	                            <%-- <a style="cursor:pointer;" onclick="javascript:editUser('${data.emp_id}')" class="btn-edit">승인</a> --%>
                                <input type="radio" onclick="javascript:getEmp('${data.emp_id}','01',this);" id="status01${data.emp_id}" name="status${data.emp_id}" value="01" <c:if test="${data.emp_status eq '01'}">checked="checked"</c:if>>
							    <label for="status01${data.emp_id}">재직</label>
							    <input style="margin-left:10px;" onclick="javascript:getEmp('${data.emp_id}','02',this);" type="radio" id="status02${data.emp_id}" name="status${data.emp_id}" value="02" <c:if test="${data.emp_status eq '02'}">checked="checked"</c:if>>
							    <label for="status02${data.emp_id}">휴직</label>
							    <input style="margin-left:10px;" onclick="javascript:getEmp('${data.emp_id}','03',this);" type="radio" id="status03${data.emp_id}" name="status${data.emp_id}" value="03" <c:if test="${data.emp_status eq '03'}">checked="checked"</c:if>>
							    <label for="status03${data.emp_id}">퇴사</label>
                            </td>
                            <td style="text-align:center;">${data.reg_dt_str}  ${data.reg_dt_si}</td>
                        </tr>
						</c:forEach>
                    </tbody>
                </table>
                <div class="table-pagination text-center">
                    <ul class="pagination">
                        <html:paging uri="/environment/user.do" frontYn="N" forGroup="&searchType=${paramMap.searchType}&searchWord=${paramMap.searchWord}&searchGubun=${paramMap.searchGubun}" />
                    </ul>
                </div>
                <!-- <div class="confirmation">
                    <div class="confirm">
                        <a href="/environment/add-admin.do">관리자등록</a>
                    </div>
                </div> -->
            </section>
        </div>



     
        <script>

        </script>

