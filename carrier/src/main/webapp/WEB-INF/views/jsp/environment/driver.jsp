<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>
<%

%>

<script type="text/javascript">
$(document).ready(function(){

});


function getDriver(driverId,gubun,value,driver_deposit_yn,obj){
	
		$.ajax({ 
			type: 'post' ,
			url : "/environment/getDriver.do" ,
			dataType : 'json' ,
			data : {
				driverId : driverId
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					if(value == "Y" || value == "N"){
						if(value != resultData.approve_yn){
							editDriver(driverId,gubun,value,driver_deposit_yn,obj);	
						}	
					}else{
						if(value != resultData.driver_status){
							editDriver(driverId,gubun,value,driver_deposit_yn,obj);	
						}
					}
				}else if(result == "0002"){
					alert("변경 하는데 실패 하였습니다.");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
 
}


function editDriver(driverId,gubun,value,driver_deposit_yn,obj){
	
	/* gubun 
	        status = 재직상태
	        emg    = 비상연락망 오픈여부 (App) */
	var callUrl = "";
// 	if(value == "Y" || value == "N"){
// 		callUrl = "/environment/updateEmpApproveYn.do";
// 	}else{
		callUrl = "/environment/updateDriverStatus.do";
// 	}	
   	if(confirm("변경 하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : callUrl ,
			dataType : 'json' ,
			data : {
				driverId : driverId,
				gubun : gubun,
				value : value,
				driver_deposit_yn : driver_deposit_yn
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
					if(result == "0000"){
						alert("변경 되었습니다.");
						//document.location.href = "/environment/driver.do?searchType="+$("#searchType").val()+"&searchWord="+encodeURI($("#searchWord").val());
						if(gubun=='status' && value=='03'){
							$('input:radio[name='+"emr"+driverId+']:input[value="N"]').attr("checked", true);
						}
					}else if(result == "0002"){
						alert("변경 하는데 실패 하였습니다.");
					}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	} 
	
}


function search(){
	
	document.location.href = "/environment/driver.do?searchType="+$("#searchType").val()+"&searchWord="+encodeURI($("#searchWord").val());
	
}




</script>


        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix">
                <ul>
                    <li><a href="">HOME</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">관리자</a></li>
                </ul>
            </div>

            <div class="up-dl clearfix header-search">
                 <table>
                    <tbody>
                        <tr>
                            <td style="width: 145px;">
                                <div class="select-con">
							        <select class="dropdown" name="searchType" id="searchType">
							        	<option value="ALL" <c:if test='${paramMap.searchType eq "ALL" }'> selected="selected"</c:if>>전체</option>
							            <option value="00" <c:if test='${paramMap.searchType eq "00" }'> selected="selected"</c:if>>기사명</option>
							            <option value="02" <c:if test='${paramMap.searchType eq "02" }'> selected="selected"</c:if>>기사아이디</option>
							        </select>
							        <span></span>
							    </div>
                            </td>
                            <td class="widthAuto" style="">
                                <input type="text" placeholder="검색어 입력" name="searchWord" id="searchWord" value="${paramMap.searchWord}"   onkeypress="if(event.keyCode=='13') search();" >
                            </td>
                            <td>
                                <input type="button" id="btn-search" value="검색" class="btn-primary" onclick="javascript:search();">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </section>

        <div class="dispatch-wrapper" style="width:100%;">
            <!-- <section class="dispatch-bottom-content">
                
            </section> -->
            
            
            <section class="bottom-table" style="width:1600px; margin-left:290px;">
                <table class="article-table"  style="margin-top:15px">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <td style="text-align:center;">아이디</td>
                            <td style="text-align:center;">이름</td>
                            <td style="text-align:center;">재직상태</td>
                            <!-- <td style="text-align:center;">승인상태</td> -->
                            <!-- <td style="text-align:center;">직급</td> -->
                            <td style="text-align:center;">연락처</td>
                            <!-- <td style="text-align:center;">승인관리</td> -->
                            <td style="text-align:center;">재직상태관리</td>                            
                            <td style="text-align:center;">비상연락망 오픈여부(App)</td>
                            <td style="text-align:center;">등록일시</td>
                        </tr>
                    </thead>
                    <tbody id="">
                    	<c:forEach var="data" items="${listData}" varStatus="status">
							<tr class="ui-state-default"> 
                            <td style="text-align:center;">${data.driver_id}</td>
                            <td style="text-align:center;">${data.driver_name}</td>
                            <c:if test='${data.driver_status eq "01" }'>
                            	<td style="text-align:center;">재직</td>
                            </c:if>
                            <c:if test='${data.driver_status eq "02" }'>
                            	<td style="text-align:center;">휴직</td>
                            </c:if>
                            <c:if test='${data.driver_status eq "03" }'>
                            	<td style="text-align:center;">퇴사</td>
                            </c:if>
                            <%-- <c:if test='${data.approve_yn eq "Y" }'>
                            	<td style="text-align:center;">승인</td>
                            </c:if>
                            <c:if test='${data.approve_yn eq "N" }'>
                            	<td style="text-align:center;">승인대기중</td>
                            </c:if> --%>
                            <%-- <td style="text-align:center;">${data.emp_position}</td> --%>
                            <td style="text-align:center;">${data.phone_num}</td>
                            <%-- <td style="text-align:center;">
                                <input type="radio" onclick="javascript:getEmp('${data.emp_id}','Y',this);" id="approveY${data.emp_id}" name="approve${data.emp_id}" value="Y" <c:if test="${data.approve_yn eq 'Y'}">checked="checked"</c:if>>
							    <label for="approveY">승인</label>
							    <input style="margin-left:10px;" onclick="javascript:getEmp('${data.emp_id}','N',this);" type="radio" id="approveN${data.emp_id}" name="approve${data.emp_id}" value="N" <c:if test="${data.approve_yn eq 'N'}">checked="checked"</c:if>>
							    <label for="approveN">승인대기</label>
                            </td> --%>
                            <td style="text-align:center;">
                                <input type="radio" onclick="javascript:getDriver('${data.driver_id}','status','01','${data.driver_deposit_yn}',this);" id="status01${data.driver_id}" name="status${data.driver_id}" value="01" <c:if test="${data.driver_status eq '01'}">checked="checked"</c:if>>
							    <label for="status01${data.driver_id}">재직</label>
							    <input style="margin-left:10px;" onclick="javascript:getDriver('${data.driver_id}','status','02','${data.driver_deposit_yn}',this);" type="radio" id="status02${data.driver_id}" name="status${data.driver_id}" value="02" <c:if test="${data.driver_status eq '02'}">checked="checked"</c:if>>
							    <label for="status02${data.driver_id}">휴직</label>
							    <input style="margin-left:10px;" onclick="javascript:getDriver('${data.driver_id}','status','03','${data.driver_deposit_yn}',this);" type="radio" id="status03${data.driver_id}" name="status${data.driver_id}" value="03" <c:if test="${data.driver_status eq '03'}">checked="checked"</c:if>>
							    <label for="status03${data.driver_id}">퇴사</label>
                            </td>
                            <td style="text-align:center;">
                                <input type="radio" onclick="javascript:getDriver('${data.driver_id}','emg','Y','',this);" id="emr${data.driver_id}" name="emr${data.driver_id}" value="Y" <c:if test="${data.emg_num_view_yn eq 'Y'}">checked="checked"</c:if>>
							    <label for="${data.driver_id}_Y">Y</label>
							    <input type="radio" style="margin-left:10px;" onclick="javascript:getDriver('${data.driver_id}','emg','N','',this);" id="emr${data.driver_id}" name="emr${data.driver_id}"  value="N" <c:if test="${data.emg_num_view_yn eq 'N'}">checked="checked"</c:if>>
							    <label for="${data.driver_id}_N">N</label>							    
                            </td>
                            <td style="text-align:center;">${data.reg_dt_str}  ${data.reg_dt_si}</td>
                        </tr>
						</c:forEach>
                    </tbody>
                </table>
                <div class="table-pagination text-center">
                    <ul class="pagination">
                        <html:paging uri="/environment/driver.do" forGroup="&searchType=${paramMap.searchType}&searchWord=${paramMap.searchWord}" />
                    </ul>
                </div>
                <!-- <div class="confirmation">
                    <div class="confirm">
                        <a href="/environment/add-admin.do">관리자등록</a>
                    </div>
                </div> -->
            </section>
        </div>



     
        <script>

        </script>

