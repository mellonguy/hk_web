<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/html.tld" %>
<%

%>
<style type="text/css">
.tg th {
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	padding: 10px 5px;
}

.tg .tg-uzvj {
	padding: 10px 5px;
	text-align: center;
	vertical-align: middle
}

</style>

<script type="text/javascript">
$(document).ready(function(){

	$("#evaluationMonth").MonthPicker({ 

		Button: false
		,MonthFormat: 'yy-mm'	
		,OnAfterChooseMonth: function() { 
	        //alert($(this).val());
	        document.location.href = "/environment/evaluation-empList.do?&evaluationMonth="+$(this).val();
	        
	     }
	});
	
});

function editEmp(empId){
	
	document.location.href = "/baseinfo/edit-employee.do?empId="+empId;
}

function addEmp(){
	document.location.href = "/baseinfo/add-employee.do";
}


function search(){
	
	document.location.href = "/baseinfo/employee.do?&searchWord="+encodeURI($("#searchWord").val());
	
}

function goEvaluation_register(emp_id,evaluationMonth){

	document.location.href = "/environment/evaluation-register.do?&empId="+emp_id+"&evaluationMonth="+"${paramMap.evaluationMonth}";
	

	
}

function showModal(obj){
	selectDriverObj =obj; 
	$('.modal-field').show();
}



</script>

 <div class="modal-field">
            <div class="modal-box">
                <h3 class="text-center">평가 된 직원 최종평점</h3>
            <div class="modal-table-container">
                <table class="article-table">
                    <colgroup>
                        
                    </colgroup>
                <!--        ※ 이미 평가 된 직원 은  이름 뒤에  
                       <div style ="background-color: #00FF00;  border-radius:4px; width:8px; height:8px; display: inline-block; margin-right:5px;" ></div> 가 표시됩니다. 
                    <thead> -->
                    <br>
                      ※ <span style="color:#00f;"> 최종 평점</span>은 관리자 모두가 평가를 마쳤을 시 표시 됩니다.
                      <br>
         
              	     <br>    
                        <tr>
                            <td>직원 이름</td>
                            <td>직급</td>
                            <td>최종평점</td>
                            
                        </tr>
                       <br>
                    </thead>
                    <tbody id="driverSelectList">
                       <c:forEach var="data" items="${evalautionCompleteList}" varStatus="status" >
                          <tr class="ui-state-default" allocationId="${data.emp_name}" > 
                               <td >${data.emp_name}</td>
                               <td>${data.emp_position}</td>
                          
                               <td> 
                               <c:if test ="${data.manegerCnt == 3}"><span style="color:#0080FF;">${data.sumtest}점</span></c:if>
                               <c:if test ="${data.manegerCnt != 3}">평가중</c:if> </td>
                     
                           </tr>
                  </c:forEach>
                    </tbody>
                </table>
                </div>
                <div class="confirmation">
                    <div class="confirm">
                        <input type="button" value="취소" name="">
                    </div>
                </div>
            </div>
        </div> 

        <section class="dispatch-top-content">
            <div class="breadcrumbs clearfix" style="margin-left:-200px;">
                <ul>
                    <li><a href="">HOME</a></li>
                    <li><img src="/img/bc-arrow.png" alt=""></li>
                    <li><a href="">직원 월별 인사 고과</a></li>
                </ul>
            </div>

		<div class="dispatch-btn-container">	
				<div style="width:300px; margin:auto; text-align:center; font-weight:bold; font-size:30px;"> 직원 월별 인사 고과 </div>					
            </div>
       
    
      <br>
<%-- 	   <div class="up-dl clearfix header-search" >
                 <table>
                    <tbody>
                        <tr>
                        	<td>조회월 :</td>
				            <td class="widthAuto" style="width:300px;" >
				                <input style="width:100%; text-align:center;"  type="text" placeholder="조회월" onclick="javascript:$(this).val('');"  readonly="readonly" name="startDt" id="startDt" value="${paramMap.selectMonth}">
				                <input style="width:40%; text-align:center;" class="datepick" type="text" placeholder="조회종료일" onclick="javascript:$(this).val('');" readonly="readonly" name="endDt" id="endDt"  value="${paramMap.endDt}">
				            </td>
                        	<td>
                        		<input type="button" id="searchStatusU" name="searchStatus" value="조회" class="btn-primary" onclick="javascript:search('U',this);">
                                <input type="button" id="searchStatusU" name="searchStatus" value="매출처 정산" class="<c:if test="${paramMap.searchStatus == 'U'}">btn-primary</c:if><c:if test="${paramMap.searchStatus != 'U'}">btn-normal</c:if>" onclick="javascript:searchStatusChange('U',this);">
                            </td>
                        	<td>					
                                <input type="button" id="searchStatusI" name="searchStatus" value="매입처 정산" class="<c:if test="${paramMap.searchStatus == 'I'}">btn-primary</c:if><c:if test="${paramMap.searchStatus != 'I'}">btn-normal</c:if>" onclick="javascript:searchStatusChange('I',this);">
                            </td>
                            <td>
                                <input type="button" id="searchStatusO" name="searchStatus" value="기사 정산" class="<c:if test="${paramMap.searchStatus == 'O'}">btn-primary</c:if><c:if test="${paramMap.searchStatus != 'O'}">btn-normal</c:if>" onclick="javascript:searchStatusChange('O',this);">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div> --%>




 <span style="color: #00f;">등록</span>을 클릭하면 평가페이지로 넘어갑니다.
<br>
<br>


  

<!-- <span style="color: #00f;">최종평점</span>은 관리자 모두가 평가를 마쳤을 시 표시 됩니다. -->
 <span style="color: #00f;">최종평점리스트</span>를 누르시면 최종평점을 볼 수 있습니다.
	<div class="up-dl clearfix header-search">
		<table>
			<tbody>
				<tr>
					<td></td>

				</tr>
			</tbody>
		</table>
	</div>
<div class="" style="margin-left:1400px;">
	<input type="button" class="btn btn-primary"  onclick ="javascript:showModal()" value="최종평점리스트">
	
	</div>	 

    <table>
         <tbody>
            <tr>
               <td>평가 월 </td>
               <td class="widthAuto" style="width: 200px;">
                  <input style="width:100%; text-align:center;" id="evaluationMonth" name="evaluationMonth"  type="text" placeholder="평가월" readonly="readonly" value="<c:if test="${paramMap.evaluationMonth != null }">${paramMap.evaluationMonth}</c:if>">
               </td>
               </tr>
         </tbody>
      </table>


</section>

	

   <section class="bottom-table" style="width:1600px; margin-left:290px;">
  
             
                
                <table class="article-table" style="margin-top:15px">
                    <colgroup>
                        
                    </colgroup>
                    <thead>
                        <tr>
                            <!-- <td style="text-align:center;">기사아이디</td> -->
                            <td style="text-align:center; width:50px;"><input type="checkbox" id="checkAll" onclick="javascript:checkAll();" ></td>
                            <td style="text-align:center; width:auto;">직원이름</td>
                            <td style="text-align:center; width:auto;">직급</td>
                            <td style="text-align:center; width:auto;">부서</td>
                            <td style="text-align:center; width:auto;">평가유무</td>
                            <td style="text-align:center; width:auto;">평가월</td>
                            <td style="text-align:center; width:auto;">배점</td>
                            <td style="text-align:center;width:auto;">총점</td>
                            <td style="text-align:center; width:auto;">등록 및 수정</td>
                            <!-- <td style="text-align:center; width:auto;">최종 평점</td> -->
                        </tr>
                    </thead>
                    <tbody id="">
                    	<c:forEach var="data" items="${evlautionListMap}" varStatus="status">
							<tr class="ui-state-default" > 
	                            <td style="text-align:center;"><input type="checkbox" name="forDeductStatus" ></td>
	                            <td style="text-align:center;" >${data.emp_name}</td>
	                            <td style="text-align:center;">${data.emp_position}</td>
	                           
	                            <c:if test="${data.allocation eq 'C'}" ><td style="text-align:center;" >배차(캐리어)</td></c:if>
	                            <c:if test="${data.allocation eq 'S'}" ><td style="text-align:center;" >배차(셀프)</td></c:if>
	                           <c:if test="${data.allocation eq 'D'}" > <td style="text-align:center;" >개발</td></c:if>
	                            <c:if test="${data.allocation eq 'M'}" ><td style="text-align:center;" >회계</td></c:if>
	                            
	                            <td style="text-align:center;"><c:if test="${data.decide_final eq 'Y'}">최종제출됨</c:if><c:if test="${data.decide_final eq null}"><span style="color:#FF0000;">미평가</span></c:if><c:if test="${data.decide_final eq 'N'}">임시저장중</c:if></td>
	                            <td style="text-align:center;"><c:if test ="${paramMap.selectMonth ne null}">${paramMap.selectMonth}</c:if><c:if test ="${paramMap.selectMonth eq null}">${data.evaluation_month} </c:if></td>
	                            <td style="text-align:center;">100</td>
	                            <td style="text-align:center;">${data.sum_evaluation_points}</td>
	                            <td style="text-align:center;">
	                            
	                            <c:if test="${data.decide_final eq null}">
	                            <input type="button"  class="btn-primary btn" value="등록" onclick="javascript:goEvaluation_register('${data.emp_id}','${paramMap.evaluationMonth}');">
	                            </c:if>
	                            <c:if test="${data.decide_final eq 'N'}">
	                            <input type="button"  class="btn-warning btn" value="수정가능" onclick="javascript:goEvaluation_register('${data.emp_id}','${paramMap.evaluationMonth}');">
	                            </c:if>
	                            <c:if test="${data.decide_final eq 'Y'}">수정불가</c:if>
	                           
	                            </td>
	                       <%--      <td style="text-align:center;">${data.test}</td> --%>
                        	</tr>
						</c:forEach>
                        
                    </tbody>
                </table>
               
                <%-- div class="table-pagination text-center">
                    <ul class="pagination">
                        <!-- <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="curr-page"><a href="#">1</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li> -->
                        <html:paging uri="/carmanagement/lowViolation-reg.do" forGroup="&occurrenceDt=${paramMap.occurrenceDt}" />
                    </ul>
                </div> --%>
                <div class="confirmation">
                    <div class="confirm">
                        <!-- <a href="/baseinfo/add-employee.do">직원등록</a> -->
                    </div>
                </div>
                
            </section>
     
