package kr.co.carrier.scheduler;

import java.io.File;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import kr.co.carrier.service.DriverDecideStatusService;
import kr.co.carrier.service.DriverDeductFileService;
import kr.co.carrier.service.DriverDeductStatusService;
import kr.co.carrier.service.DriverService;
import kr.co.carrier.utils.WebUtils;
import kr.co.carrier.vo.DriverDecideStatusVO;
import kr.co.carrier.vo.DriverDeductStatusVO;

@Component
public class DeductFileMakeScheduler {

	
	private static final Logger logger = LoggerFactory.getLogger(DeductFileMakeScheduler.class);
	
	
	@Value("#{appProp['upload.base.path']}")
    private String rootDir;    
    private String subDir = "bbs";	
	
	@Autowired
	private DriverService driverService;
	
	@Autowired
	private DriverDeductFileService driverDeductFileService;
	
	@Autowired
	private DriverDeductStatusService driverDeductStatusService;
	
	@Autowired
	private DriverDecideStatusService driverDecideStatusService;
	
	
	 	@Scheduled(cron = "0 10 0 15 * *")			//매달 15일 0시 10분 실행(17일에서 변경)
		//@Scheduled(cron = "0 45 * * * *")			 
	    public void deleteDriverLocationInfo(){
	        try {
	                    	
	        	
				logger.info("DeductFileMakeScheduler_before");
		
				String decideMonth = WebUtils.getPrevMonth();	
				logger.info(""+decideMonth);
				
				List<Map<String, Object>> driverList = driverService.selectDriverList(new HashMap<String, Object>());
				
				for(int i = 0; i < driverList.size(); i++){
					Map<String, Object> driverMap = driverList.get(i);
					Map<String, Object> map = new HashMap<String, Object>();
					//map.put("driverId", URLDecoder.decode(paymentPartnerIdArr[i], "UTF-8"));
					map.put("driverId", URLDecoder.decode(driverMap.get("driver_id").toString(), "UTF-8"));
					map.put("decideMonth", decideMonth);
					//map.put("decideMonth", WebUtils.getPrevMonth());
					Map<String, Object> driver = driverService.selectDriver(map);
					if(driver != null && driver.get("driver_id") != null) {
						
						
						//(회계)공제내역이 확정 되었고
						Map<String, Object> deductStatusMap = driverDeductStatusService.selectDriverDeductStatus(map);
						//미확정인 경우 강제로 확정 한다.
						if(deductStatusMap == null || deductStatusMap.get("driver_id") == null) {
							DriverDeductStatusVO deductStatusVO = new DriverDeductStatusVO();
							deductStatusVO.setDeductMonth(decideMonth);
							deductStatusVO.setDeductStatus("Y");
							deductStatusVO.setDriverDeductStatusId("DDS"+UUID.randomUUID().toString().replaceAll("-",""));
							deductStatusVO.setDriverId(URLDecoder.decode(driverMap.get("driver_id").toString(), "UTF-8"));						
							driverDeductStatusService.insertDriverDeductStatus(deductStatusVO);	
							deductStatusMap = driverDeductStatusService.selectDriverDeductStatus(map);
						}
						
						//(기사)정산내역이 확정 되었다면
						Map<String, Object> decideStatusMap = driverDecideStatusService.selectDriverDecideStatus(map);
						//미확정인 경우 강제로 확정 한다.
						if(decideStatusMap == null || decideStatusMap.get("driver_id") == null) {
							DriverDecideStatusVO statusVO = new DriverDecideStatusVO();
							statusVO.setDecideMonth(decideMonth);
							statusVO.setDriverDecideStatusId("DDI"+UUID.randomUUID().toString().replaceAll("-", ""));
							statusVO.setDriverId(URLDecoder.decode(driverMap.get("driver_id").toString(), "UTF-8"));
							statusVO.setListStatus("Y");
							driverDecideStatusService.insertDriverDecideStatus(statusVO);
							decideStatusMap = driverDecideStatusService.selectDriverDecideStatus(map);
						}
						
						//운송비 공제 내역서를 생성한다(둘다 확정 된 경우에만 생성 한다)												
						if(deductStatusMap != null && decideStatusMap != null) {
							//이미 생성된 파일이 있는경우 기존에 있는 파일을 삭제 하고 새로 생성 한다.
							List<Map<String, Object>> driverDeductFileList = driverDeductFileService.selectDriverDeductFileList(map);
							
							//이미 보내기 버튼을 눌러 기사님이 확인 할 수 있는 경우에는 기존에 있는 파일을 삭제하고 새로 생성 하지 않는다....
							if(driverDeductFileList.size() > 0 && !driverDeductFileList.get(0).get("driver_view_flag").toString().equals("Y")) {
								
								if(driverDeductFileList.size() > 0) {
									for(int j = 0; j < driverDeductFileList.size(); j++) {
										Map<String, Object> driverDeductFile = driverDeductFileList.get(j);
										driverDeductFile.put("driverDeductFileId", driverDeductFile.get("driver_deduct_file_id").toString());
										File removeFile = new File(rootDir+"/deduct_list"+driverDeductFile.get("driver_deduct_file_path").toString());
										if(removeFile.exists()){
											removeFile.delete();
										}
										removeFile = new File(rootDir+"/deduct_list"+driverDeductFile.get("driver_deduct_file_path").toString()+".pdf");
										if(removeFile.exists()){
											removeFile.delete();
										}
										driverDeductFileService.deleteDriverDeductFile(driverDeductFile);
									}
								}
								
								Map<String, Object> fileMap = driverDeductFileService.makeDriverDeductFile(map);
							}else {
								
								
								
								
							}
							
												
						}
												
					}
					
				}
				
				logger.info("DeductFileMakeScheduler_after");
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
		
		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
