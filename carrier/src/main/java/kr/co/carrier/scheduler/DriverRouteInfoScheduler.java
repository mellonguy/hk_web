package kr.co.carrier.scheduler;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import kr.co.carrier.service.DriverLocationInfoService;
import kr.co.carrier.service.YearMonthService;
import kr.co.carrier.utils.WebUtils;
import kr.co.carrier.vo.YearMonthVO;

@Component
public class DriverRouteInfoScheduler {

	private static final Logger logger = LoggerFactory.getLogger(DriverRouteInfoScheduler.class);
	
    @Autowired
    private DriverLocationInfoService driverLocationInfoService;
     
    @Autowired
    private YearMonthService yearMonthService;
    
    /**
     *  매일 00:20분에 오늘 날짜가 아닌 기사 위치 정보를 삭제 한다.
     */
    @Scheduled(cron = "0 20 0 * * *")
    //@Scheduled(cron = "0 * * * * *")
    public void deleteDriverLocationInfo(){
        try {
                    	
        	Map<String, Object> map = new HashMap<String, Object>();
        	String today = WebUtils.getNow("yyyy-MM-dd");
			map.put("today", today);
			
			String yearMonth = WebUtils.getNow("yyyy-MM");
			map.put("yearMonthA", yearMonth);
			
			Map<String, Object> yearMonthMap = yearMonthService.selectYearMonthByYearMonth(map);
			
			//해당월에 배차 정보가 없거나 입력된 공제 내역이 없는경우에 월별로 가져오기 위해..........
			if(yearMonthMap == null || yearMonthMap.get("year_month_id") == null) {
				YearMonthVO yearMonthVO = new YearMonthVO();
				yearMonthVO.setYearMonthA(yearMonth);
				yearMonthVO.setYearMonthId("YMI"+UUID.randomUUID().toString().replaceAll("-",""));
				yearMonthService.insertYearMonth(yearMonthVO);
			}
			
			logger.info("before");
			driverLocationInfoService.deleteDriverLocationInfoList(map);
			logger.info("after");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
	
	
	
	
	
	
	
	
	
	
	
	
}
