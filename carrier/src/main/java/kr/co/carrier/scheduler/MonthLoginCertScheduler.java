package kr.co.carrier.scheduler;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import kr.co.carrier.service.DriverService;
import kr.co.carrier.service.EmpLoginInfoService;
import kr.co.carrier.utils.WebUtils;



@Component
public class MonthLoginCertScheduler {
	
	private static final Logger logger = LoggerFactory.getLogger(MonthLoginCertScheduler.class);
	
	@Autowired
	private EmpLoginInfoService empLoginInfoService;
	
	@Scheduled(cron = "0 30 4 * * *")			//  초 분 시 일  새벽4시 30분 미라클모닝 
	public void updateEmpLoginCertCheck() {
			
		
		try {
			
			logger.info("MonthLoginCertScheduler_before");
			
			Map<String, Object> map = new HashMap<String, Object>();
			String today = WebUtils.getNow("yyyy-MM-dd").toString();
			
			
			String selectDate  = WebUtils.getDate(-30); //지금으로부터 30일전 날짜 구해옴
			selectDate = selectDate.substring(0,4)+"-"+selectDate.substring(4,6)+"-"+selectDate.substring(6,8); 
			System.out.println(selectDate); 

			map.put("today", today);
			map.put("selectDate", selectDate);
			
			try {
				
					empLoginInfoService.updateEmpLoginCertY(map);
					logger.info("MonthLoginCertScheduler_after");
				
			}catch(Exception e) {
				e.printStackTrace();
			
			}
			
			
			
			
		}catch(Exception e) {
		e.printStackTrace();
			
			
			
		}
	}
	
	

}
