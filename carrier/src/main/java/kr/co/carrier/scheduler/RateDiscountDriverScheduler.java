package kr.co.carrier.scheduler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import kr.co.carrier.service.DriverService;
import kr.co.carrier.utils.WebUtils;

@Component
public class RateDiscountDriverScheduler {

	private static final Logger logger = LoggerFactory.getLogger(MonthLoginCertScheduler.class);	
	
	@Autowired
	private DriverService driverService;
	
	
	@Scheduled(cron = "0 0 3 * * *")			//  초 분 시 일
	
	
	//@Scheduled(cron = "0 58 15 * * *")	
	public void updateDisCountDriverRate() {
		
		try {
			
			logger.info("RateDiscountDriverScheduler_before");
			
			String selectDate  = WebUtils.getNow("yyyy-MM-dd").toString();
			String lastYear  = WebUtils.getDate(-365);
			lastYear = lastYear.substring(0,4)+"-"+lastYear.substring(4,6)+"-"+lastYear.substring(6,8); 
		
			Map<String, Object> map = new HashMap<String, Object>();
			List<Map<String,Object>> rateDistCountList = driverService.selectRateDiscountDriverList(map);
			
			
			for(int i=0; i<rateDistCountList.size(); i++) {
				
				String joinDt = rateDistCountList.get(i).get("join_dt") == null ? "" : rateDistCountList.get(i).get("join_dt").toString();
				String driverId = rateDistCountList.get(i).get("driver_id").toString();
				String beforeDeductionRate = rateDistCountList.get(i).get("deduction_rate").toString();
				String rateDiscountMinimum = rateDistCountList.get(i).get("rate_discount_minimum") == null ? "" : rateDistCountList.get(i).get("rate_discount_minimum").toString(); //최솟값 적용요율
				int deductionRate= Integer.parseInt(rateDistCountList.get(i).get("deduction_rate").toString())-1; // 1년에 -1씩 감소될 적용요율
				
				map.put("driverId",driverId);
				map.put("deductionRate",deductionRate);
				
				if(joinDt.equals(lastYear)) {
					
					if(deductionRate > Integer.parseInt(rateDiscountMinimum)){
						
						driverService.updatedeDuctionRateDriver(map);		
					}
				
				
				map.put("rateDiscountDriverId", "RDI"+UUID.randomUUID().toString().replaceAll("-", ""));
				map.put("driverId", driverId);
				map.put("previousRateDiscount",beforeDeductionRate);
				map.put("nowRateDiscount",deductionRate);
				map.put("rateDiscountMinimum",rateDistCountList.get(i).get("rate_discount_minimum").toString());
				
				driverService.insertRateDiscountDriver(map);
				
					logger.info("RateDiscountDriverSchedule_after");
					
				}else {
				
				}
				
				
			}
			
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
}
