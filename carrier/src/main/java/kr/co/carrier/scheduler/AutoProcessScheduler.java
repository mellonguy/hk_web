package kr.co.carrier.scheduler;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import kr.co.carrier.service.AdjustmentService;
import kr.co.carrier.service.AllocationService;
import kr.co.carrier.service.DebtorCreditorService;
import kr.co.carrier.service.PaymentInfoService;
import kr.co.carrier.utils.BaseAppConstants;
import kr.co.carrier.utils.WebUtils;
import kr.co.carrier.vo.DebtorCreditorVO;

/**
 * @author 20180006
 *
 */
@Component
public class AutoProcessScheduler {

	
	
	@Autowired
	private AllocationService allocationService;
	
	@Autowired
	private PaymentInfoService paymentInfoService;
	
	@Autowired
	private AdjustmentService adjustmentService;
	
	@Autowired
	private DebtorCreditorService debtorCreditorService;
	
	
	private static final Logger logger = LoggerFactory.getLogger(AutoProcessScheduler.class);
	
	
	//@Scheduled(cron = "0 57 * * * *")
	//매일 오전 1시에 출발일 기준으로 2일 전의 외주기사 배차건 중에서 현장수금,현금건에 대해 완료처리를 하고 현장수금건은 입금 처리까지 진행 한다.
	@Scheduled(cron = "0 0 1 * * *")
    public void autoProcess(){
        try {
                    	
        	Map<String, Object> map = new HashMap<String, Object>();
        	String today = WebUtils.getNow("yyyy-MM-dd");
			map.put("today", today);
			
			String processDate = WebUtils.getDate(-2);
			processDate = processDate.substring(0,4)+"-"+processDate.substring(4,6)+"-"+processDate.substring(6,8); 
	
			try {
				DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Date parseDate = sdf.parse(processDate);	
				logger.info("parseDate="+parseDate);
				
			}catch(ParseException e){
				e.printStackTrace();
			}	
				
			map.put("processDate", processDate);
			//출발일 기준으로 2일전의 외주기사 탁송 목록을 가져 온다.
			List<Map<String, Object>> allocationList = allocationService.selectAllocationListForAutoProcess(map);
				
				for(int i = 0; i < allocationList.size(); i++) {
					
					//외주기사 탁송건중 현장수금,현금건에 대해 완료처리를 하고 현장수금건은 입금 처리까지 진행 한다.
					Map<String, Object> allocationIdMap = new HashMap<String, Object>();
					allocationIdMap.put("allocationId", allocationList.get(i).get("allocation_id").toString());
					//allocationIdMap.put("driverId", allocationList.get(i).get("driver_id").toString());
					String allocationId = allocationList.get(i).get("allocation_id").toString();
					String driverId = allocationList.get(i).get("driver_id").toString();
					
					Map<String, Object> allocationMap = allocationService.selectAllocation(allocationIdMap);
					
					if(allocationMap != null) {
					
						// 56line 쿼리에서 미배차,탁송취소,취소대기 목록은 제외 하고 가져오고 있지만 혹시 모르니..... 
						if(allocationMap.get("allocation_status").toString().equals("C") || allocationMap.get("allocation_status").toString().equals("X")){
							//건너뛴다
							continue;			
						}
						//배차건이 탁송 완료 처리가 되어 있지 않은경우 탁송 완료로 변경 해 준다.
						if(!allocationMap.get("allocation_status").toString().equals("F")){
							Map<String, Object> updateMap = new HashMap<String, Object>();
							updateMap.put("allocationId", allocationMap.get("allocation_id").toString());
							updateMap.put("allocationStatus", BaseAppConstants.ALLOCATION_STATUS_FINISH);
							updateMap.put("modId", "AutoProcessScheduler");
							allocationService.updateAllocationStatus(updateMap);
							updateMap.put("logType", "U");
							allocationService.insertAllocationLogByMap(updateMap); //로그기록
							
						}
						
						//완료처리가 되는경우 또는 이미 되어 있는 경우 에도 해당 기사의 결제 정보가 확정 되어 있는지 확인 후 확정이 되어 있지 않은경우  출발월 기준으로 확정 진행 한다.
						
						Map<String, Object> updateMapSub = new HashMap<String, Object>();
						
						//20210209 박태환 대리님 요청 미확정 유지 
						//updateMapSub.put("decideStatus", "Y");
						//updateMapSub.put("decideMonth", processDate.substring(0,7));
						
						updateMapSub.put("decideStatus", "N");
						updateMapSub.put("decideMonth", "");
						updateMapSub.put("allocationId", allocationId);
						updateMapSub.put("driverId", driverId);
						updateMapSub.put("modId", "AutoProcessScheduler0");
						updateMapSub.put("logType", "U");
						List<Map<String, Object>> selectList = paymentInfoService.selectPaymentInfoDecideStatusByPaymentPartnerId(updateMapSub);
						if(selectList != null && selectList.size() > 0) {
							//일반적으로 하나만 있겠지만...... 
							Map<String, Object> decideStatusMap = selectList.get(0);
							//decide_status가 Y가 아니라면 decide_month 는 고려할 필요가 없을것 같다... 
							if(decideStatusMap.get("decide_status") != null && !decideStatusMap.get("decide_status").toString().equals("") && !decideStatusMap.get("decide_status").toString().equals("Y")) {
								paymentInfoService.updatePaymentInfoDecideStatusByPaymentPartnerId(updateMapSub);
								paymentInfoService.insertPaymentInfoLogByMap(updateMapSub); //로그기록
							}
						}
						
						//매출로 넘어가지 않았고 차변이 입력 되지 않은경우 
						if(allocationMap.get("billing_status").toString().equals("N") && (allocationMap.get("debtor_creditor_id") == null || allocationMap.get("debtor_creditor_id").toString().equals(""))) {
							Map<String, Object> selectMap = new HashMap<String, Object>();
							selectMap.put("allocationId", allocationMap.get("allocation_id").toString());
							
							List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(selectMap);
							
							if(paymentInfoList != null && paymentInfoList.size() > 0){
								for(int k = 0; k < paymentInfoList.size(); k++) {
									Map<String, Object> paymentInfo = paymentInfoList.get(k);
									if(paymentInfo.get("payment_division").toString().equals("01")) {		//매출처 
										if(paymentInfo.get("payment_kind") != null) {
											
											//현장수금인경우 현금건인경우는 해당 배차를 확정 하고 차변에 입력 한다.
											String debtorCreditorIdForD = "DCI"+UUID.randomUUID().toString().replaceAll("-","");
											if((paymentInfo.get("payment_kind").toString().equals("DD") || paymentInfo.get("payment_kind").toString().equals("CA")) ) {
											
												//현장 수금건 탁송 완료시 해당 건에 대해 확정 및 매출로 넘기고 ....
												String billPublishRequestId = "BPR"+UUID.randomUUID().toString().replaceAll("-","");
												Map<String, Object> carInfoUpdateMap = new HashMap<String, Object>();
												carInfoUpdateMap.put("allocationId", allocationMap.get("allocation_id").toString());
												carInfoUpdateMap.put("billPublishRequestId", billPublishRequestId);
												carInfoUpdateMap.put("decideStatus", "Y");
												carInfoUpdateMap.put("billingStatus", "D");
												carInfoUpdateMap.put("debtorCreditorId", debtorCreditorIdForD);
												adjustmentService.updateCarInfoForBillPublish(carInfoUpdateMap);
												//차변에 입력--------------------------------------------------------------------------
												//세금계산서 미 발행건에 대해서 각 건별로 차변에 등록한다.
												DebtorCreditorVO debtorCreditorVO = new DebtorCreditorVO();	
												debtorCreditorVO.setCustomerId(allocationMap.get("customer_id").toString());
												debtorCreditorVO.setDebtorCreditorId(debtorCreditorIdForD);
												debtorCreditorVO.setOccurrenceDt(allocationMap.get("departure_dt").toString());
												debtorCreditorVO.setSummary("운송료 ("+allocationMap.get("departure").toString()+"-"+allocationMap.get("arrival").toString()+")");
												debtorCreditorVO.setTotalVal(allocationMap.get("sales_total").toString());
												debtorCreditorVO.setPublishYn("N");			//미발행건
												debtorCreditorVO.setType(BaseAppConstants.DEBTOR_CREDITOR_TYPE_D);
												debtorCreditorVO.setRegisterId("AUTO");
												debtorCreditorVO.setEtc("");
												debtorCreditorVO.setCompanyId("AUTO");
												debtorCreditorService.insertDebtorCreditor(debtorCreditorVO);
												
											}
											
											
											
										}
									}
									
								}
								
							}
						
							
						}else {
						//매출로 넘어갔거나 차변이 입력 되어 있는경우는 아무것도 하지 않는다.
							
					
							
							
						}
						
						
						
						Map<String, Object> selectMap = new HashMap<String, Object>();
						selectMap.put("allocationId", allocationMap.get("allocation_id").toString());
						
						List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(selectMap);
						
						if(paymentInfoList != null && paymentInfoList.size() > 0){
							for(int k = 0; k < paymentInfoList.size(); k++) {
								Map<String, Object> paymentInfo = paymentInfoList.get(k);
								if(paymentInfo.get("payment_division").toString().equals("01")) {		//매출처 
									if(paymentInfo.get("payment_kind") != null) {
										
										
										if(paymentInfo.get("payment_kind").toString().equals("DD")) {		//결제 방법이 기사 수금이면
											
											Map<String, Object> paymentUpdateMap = new HashMap<String, Object>();
											
											paymentUpdateMap.put("payment", "Y");
											paymentUpdateMap.put("paymentDt", allocationMap.get("departure_dt").toString());
											paymentUpdateMap.put("allocationId", allocationMap.get("allocation_id").toString());
											paymentUpdateMap.put("idx", paymentInfo.get("idx").toString());
											paymentUpdateMap.put("modId","AutoProcessScheduler1"); //수정자
											paymentUpdateMap.put("logType","U");
											//결제 여부 업데이트
											//기존에 결제 정보가 이미 작성 되어 있는경우에는 하지 않는다.
											if((paymentInfo.get("payment") == null || paymentInfo.get("payment").toString().equals("N")) && (paymentInfo.get("payment_dt") == null || paymentInfo.get("payment_dt").toString().equals(""))) {
												paymentInfoService.updatePaymentInfoForDDFinish(paymentUpdateMap);
												paymentInfoService.insertPaymentInfoLogByMap(paymentUpdateMap); //로그기록
											}
											
											//allocationMap으로 비교하거나 조건으로 사용 하지 않으니... 다시 select해도.....
											allocationMap = allocationService.selectAllocation(allocationIdMap);
											
											//차변이 무조건 있을테니 차변 아이디를 select 해 와서...
											String debtorCreditorIdForD = allocationMap.get("debtor_creditor_id").toString();
											
											//대변 입력(대변에 입력 되어 있우에는 다시 입력 하지 않는다...)
											if(paymentInfo.get("debtor_creditor_id") == null || paymentInfo.get("debtor_creditor_id").toString().equals("")) {
										
												String debtorCreditorId = "DCI"+UUID.randomUUID().toString().replaceAll("-","");
												DebtorCreditorVO debtorCreditorVO = new DebtorCreditorVO();
												debtorCreditorVO.setCustomerId(allocationMap.get("customer_id").toString());
												debtorCreditorVO.setDebtorCreditorId(debtorCreditorId);
												debtorCreditorVO.setOccurrenceDt(allocationMap.get("departure_dt").toString());
												paymentUpdateMap.put("debtorCreditorId", debtorCreditorId);
																				
												String summaryDt = ""; 
												summaryDt ="운송료 ("+allocationMap.get("departure").toString()+" - "+allocationMap.get("arrival").toString()+" 입금)"; 
												debtorCreditorVO.setSummary(summaryDt);
												debtorCreditorVO.setTotalVal(allocationMap.get("sales_total").toString());
												debtorCreditorVO.setPublishYn("N");
												debtorCreditorVO.setType(BaseAppConstants.DEBTOR_CREDITOR_TYPE_C);
												debtorCreditorVO.setRegisterId("driver");
												debtorCreditorVO.setEtc("");
												debtorCreditorVO.setCompanyId("AUTO");
												debtorCreditorVO.setDebtorId(debtorCreditorIdForD);
												//대변에 입력 하고 
												debtorCreditorService.insertDebtorCreditor(debtorCreditorVO);	
												paymentInfoService.updatePaymentInfoDebtorCreditorId(paymentUpdateMap);
												paymentInfoService.insertPaymentInfoLogByMap(paymentUpdateMap); //로그기록
											}
											
										}
										
										
										
									}
								
								}else if(paymentInfo.get("payment_division").toString().equals("03")) {
									
									if(paymentInfo.get("payment_kind") != null) {
										if(paymentInfo.get("payment_kind").toString().equals("DD")) {
											//매입처 결제 여부 업데이트
											//기존에 결제 정보가 이미 작성 되어 있는경우에는 하지 않는다.
											if((paymentInfo.get("payment") == null || paymentInfo.get("payment").toString().equals("N")) && (paymentInfo.get("payment_dt") == null || paymentInfo.get("payment_dt").toString().equals(""))) {
												
												Map<String, Object> paymentUpdateMap = new HashMap<String, Object>();
												paymentUpdateMap.put("payment", "Y");
												paymentUpdateMap.put("paymentDt", allocationMap.get("departure_dt").toString());
												paymentUpdateMap.put("allocationId", allocationMap.get("allocation_id").toString());
												paymentUpdateMap.put("idx", paymentInfo.get("idx").toString());
												paymentUpdateMap.put("modId","AutoProcessScheduler2"); //수정자
												paymentUpdateMap.put("logType","U");
												paymentInfoService.updatePaymentInfoForDDFinish(paymentUpdateMap);
												paymentInfoService.insertPaymentInfoLogByMap(paymentUpdateMap); //로그기록
											}		
											
										}
										
									}
									
								}
								
							}
							
						
						}
						
					}
					
				}
				
			logger.info("processDate="+processDate);
			logger.info("**************AutoProcessScheduler*************************");
			
	
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
	
	
}

