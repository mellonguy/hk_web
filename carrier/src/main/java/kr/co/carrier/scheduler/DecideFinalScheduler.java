package kr.co.carrier.scheduler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import kr.co.carrier.service.AdjustmentService;
import kr.co.carrier.service.DecideFinalService;
import kr.co.carrier.service.DriverDecideStatusService;
import kr.co.carrier.service.DriverDeductFileService;
import kr.co.carrier.service.DriverDeductStatusService;
import kr.co.carrier.service.DriverService;
import kr.co.carrier.service.PaymentInfoService;
import kr.co.carrier.utils.WebUtils;
import kr.co.carrier.vo.DecideFinalVO;



@Component
public class DecideFinalScheduler {

	
	private static final Logger logger = LoggerFactory.getLogger(DecideFinalScheduler.class);
	
	@Value("#{appProp['upload.base.path']}")
    private String rootDir;    
    private String subDir = "bbs";	
	
	@Autowired
	private DriverService driverService;
	
	@Autowired
	private DriverDeductFileService driverDeductFileService;
	
	@Autowired
	private DriverDeductStatusService driverDeductStatusService;
	
	@Autowired
	private DriverDecideStatusService driverDecideStatusService;
	
	@Autowired
	private AdjustmentService adjustmentService;
	
	@Autowired
	private PaymentInfoService paymentInfoService;
	
	@Autowired
	private DecideFinalService decideFinalService;
	
 		@Scheduled(cron = "0 10 0 12 * *")			//매달 12일 0시 10분 실행
		//@Scheduled(cron = "0 53 * * * *")			//매분마다 실행 
	    public void deleteDriverLocationInfo(){
	        try {
	                    	
	        	
				logger.info("DecideFinalScheduler_before");
		
				String decideMonth = WebUtils.getPrevMonth();	
				logger.info(""+decideMonth);
				
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("decideMonth", decideMonth);
				
				List<Map<String, Object>> anotherList = adjustmentService.selectAnotherListForDecideFinal(map);
				int listSize = anotherList.size();
				
				for(Map<String, Object> another : anotherList) {
				
					String cnt = another.get("cnt").toString().replaceAll(",", "");
					String totalSales = another.get("total_sales").toString().replaceAll(",", "");
					String decideFinalId = "DFI"+UUID.randomUUID().toString().replaceAll("-","");
					
					Map<String, Object> updateMap = new HashMap<String, Object>();
					updateMap.put("decideFinal", "Y");
					updateMap.put("decideFinalId", decideFinalId);
					updateMap.put("paymentPartnerId", another.get("payment_partner_id").toString());
					updateMap.put("decideMonth", decideMonth);	
					updateMap.put("currentDecideStatus", "Y");
					updateMap.put("currentDecideFinal", "N");
					updateMap.put("modId", "DecideFinalScheduler"); //수정자
					updateMap.put("logType","U");
					//if(!another.get("payment_partner_id").toString().equals("sourcream")) {
					paymentInfoService.updatePaymentInfoListForDecideFinal(updateMap);
					//}
					paymentInfoService.insertPaymentInfoLogByMap(updateMap); //로그기록
					
					//같은 아이디를 가진 최종 확정된 리스트를 불러 와서 최종 확정 테이블에 최종 확정 정보를  insert 한다 
					Map<String, Object> paymentInfoMap = paymentInfoService.selectPaymentInfoByDecideFinalId(updateMap);
					
					if(paymentInfoMap != null && paymentInfoMap.get("payment_partner_id") != null && !paymentInfoMap.get("payment_partner_id").toString().equals("")) {
						DecideFinalVO decideFinalVO = new DecideFinalVO();
						decideFinalVO.setAmount(paymentInfoMap.get("amount").toString().replaceAll(",", ""));
						decideFinalVO.setComment("");
						decideFinalVO.setDecideFinalId(decideFinalId);
						decideFinalVO.setDecideFinalStatus("Y");
						decideFinalVO.setEmpId("AUTO");
						decideFinalVO.setEmpName("AUTO");
						decideFinalVO.setCompanyId("AUTO");
						decideFinalVO.setPaymentPartnerId(paymentInfoMap.get("payment_partner_id").toString());
						decideFinalVO.setPaymentPartnerName(paymentInfoMap.get("payment_partner").toString());
						decideFinalVO.setDecideMonth(paymentInfoMap.get("decide_month").toString());
						
						//if(!another.get("payment_partner_id").toString().equals("sourcream")) {
							decideFinalService.insertDecideFinal(decideFinalVO);
						//}	
					}
					
					
					
				}
				
				
				logger.info("DecideFinalScheduler_after");
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
		
		
	
	
	
	
	
	
	
	
	
	
	
}
