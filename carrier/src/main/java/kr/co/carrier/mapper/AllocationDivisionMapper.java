package kr.co.carrier.mapper;

import java.util.List;
import java.util.Map;

public interface AllocationDivisionMapper {

	
	public List<Map<String, Object>>selectAllocationDivisionInfoList() throws Exception;
	public Map<String, Object>selectAllocationDivisionInfo(Map<String, Object> map) throws Exception;
	
	
	
	
	
}
