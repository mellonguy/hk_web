package kr.co.carrier.mapper;

import java.util.List;
import java.util.Map;

public interface AdjustmentMapper {

	
	
	public List<Map<String, Object>> selectCustomerList(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectCustomerListRev(Map<String, Object> map) throws Exception;
	public int selectCustomerListCountRev(Map<String, Object> map) throws Exception;
	
	public int selectCustomerListCount(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectAllocationListByCustomerId(Map<String, Object> map) throws Exception;
	public int selectAllocationCountByCustomerId(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectAllocationByAllocationId(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectTotalStatistics(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectTotalStatisticsRev(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectAllocationListByCustomerIdForBillPublish(Map<String, Object> map) throws Exception;
	public void updateCarInfoListForBillPublish(Map<String, Object> map) throws Exception;
	public void updateCarInfoForBillPublish(Map<String, Object> map) throws Exception;
	public void updateCarInfoBillingStatus(Map<String, Object> map) throws Exception;
	public int selectAllocationCountBybillPublishRequestId(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectAllocationListBybillPublishRequestId(Map<String, Object> map) throws Exception;
	public int selectAnotherListCount(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectAnotherList(Map<String, Object> map) throws Exception;
	public int selectAllocationCountByPaymentPartnerId(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectAllocationListByPaymentPartnerId(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectAnotherTotalStatistics(Map<String, Object> map) throws Exception;
	public int selectFinalListCount(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectFinalList(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectFinalTotalStatistics(Map<String, Object> map) throws Exception;
	public int selectAllocationCountByPaymentPartnerIdAndDecideMonth(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectAllocationListByPaymentPartnerIdAndDecideMonth(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectAnotherListForDecideFinal(Map<String, Object> map) throws Exception;
	
	public void updateCarInfoDebtorCreditorId(Map<String, Object> map) throws Exception;
	public void updateCarInfoDebtorCreditorIdByBillPublishRequstId(Map<String, Object> map) throws Exception;
	public void initCarInfoForBillPublish(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectReceivableList(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectReceivableTaxList(Map<String, Object> map) throws Exception;
	
	public int selectFinishSalesListCount(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectFinishSalesList(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectFinishSalesListForDownload(Map<String, Object> map) throws Exception;
	public int selectLayoutDecideCountY(Map<String, Object> map) throws Exception;
	public int selectLayoutDecideCountN(Map<String, Object> map) throws Exception;
	
	
	
	
}
