package kr.co.carrier.mapper;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.BillPublishRequestVO;

public interface BillPublishRequestMapper {

	public Map<String, Object> selectBillPublishRequest(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectBillPublishRequestList(Map<String, Object> map) throws Exception;
	public int selectBillPublishRequestListCount(Map<String, Object> map) throws Exception;
	public int insertBillPublishRequest(BillPublishRequestVO billPublishRequestVO) throws Exception;
	public void deleteBillPublishRequest(Map<String, Object> map) throws Exception;
	public void updateBillPublishRequest(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectBillPublishRequestStatistics(Map<String, Object> map) throws Exception;
	public int selectCustomerListCount(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectCustomerList(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectBillPublishRequestListByCustomerId(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectBillPublishRequestIssueList(Map<String, Object> map) throws Exception;
	public int selectBillPublishRequestIssueListCount(Map<String, Object> map) throws Exception;
	
	
	
	
	
}
