package kr.co.carrier.mapper;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.DriverDeductStatusVO;

public interface DriverDeductStatusMapper {

	
	
	public Map<String, Object> selectDriverDeductStatus(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectDriverDeductStatusList(Map<String, Object> map) throws Exception;
	public int selectDriverDeductStatusListCount(Map<String, Object> map) throws Exception;
	public int insertDriverDeductStatus(DriverDeductStatusVO driverDeductStatusVO) throws Exception;
	public void deleteDriverDeductStatus(Map<String, Object> map) throws Exception;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
