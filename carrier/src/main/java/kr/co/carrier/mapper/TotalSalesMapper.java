package kr.co.carrier.mapper;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.CustomerVO;

public interface TotalSalesMapper {
	public Map<String, Object> selectCustomer(Map<String, Object> map) throws Exception; 
	public Map<String, Object> selectCustomerByCustomerName(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectCustomerList(Map<String, Object> map) throws Exception;
	public int selectCustomerListCount(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectAllocationListByCustomerId(Map<String, Object> map) throws Exception;
	public int selectAllocationCountByCustomerId(Map<String, Object> map) throws Exception;
	
	
	
	
	
	
	
	
}
