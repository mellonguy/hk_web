package kr.co.carrier.mapper;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.AddressInfoVO;

public interface AddressInfoMapper {

	public Map<String, Object> selectAddressInfo(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectAddressInfoList(Map<String, Object> map) throws Exception;
	public int insertAddressInfo(AddressInfoVO addressInfoVO) throws Exception;
	public void deleteAddressInfo(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectAddressInfoListByKeyWord(Map<String, Object> map) throws Exception;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
