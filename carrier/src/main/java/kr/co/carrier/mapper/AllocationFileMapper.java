package kr.co.carrier.mapper;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.AllocationFileVO;

public interface AllocationFileMapper {

	public int insertAllocationFile(AllocationFileVO fileVO) throws Exception;	
	public List<Map<String, Object>> selectAllocationFileList(Map<String, Object> map) throws Exception;
	public void deleteAllocationFile(Map<String, Object> map) throws Exception;
	public void insertCaSendAllocationFile(Map<String, Object> map) throws Exception;
	
	
	
	
	
	
	
	
	
}
