package kr.co.carrier.mapper;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.AlarmSocketVO;
import kr.co.carrier.vo.AllocationVO;

public interface AllocationMapper {

	
	
	public Map<String, Object> selectAllocation(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectAllocationList(Map<String, Object> map) throws Exception;
	public int selectAllocationListCount(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectAllocationByListOrder(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectAllocationAllList() throws Exception;
	public List<Map<String, Object>> selectAllocationListByBatchStatusId(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectAllocationDownloadList(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectAllocationListByPaymentDt(Map<String, Object> map) throws Exception;
	public int selectAllocationCountByPaymentDt(Map<String, Object> map) throws Exception;
	public List<Map<String,Object>> selectSearchModify(Map<String,Object>map)throws Exception;
	public List<Map<String,Object>> selectListManageMiddle(Map<String,Object>map)throws Exception;
	public List<Map<String,Object>> selectAllocationAppConfirmList(Map<String,Object>map)throws Exception;
	public Map<String, Object> selectCustomerAppCall(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectAllocationListSub(Map<String, Object> map) throws Exception;
	public int selectAllocationListCountSub(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectAllocationListForAutoProcess(Map<String, Object> map) throws Exception;
	
	public int insertAllocation(AllocationVO allocationVO) throws Exception;
	public int insertAllocationLogByVO(AllocationVO allocationVO) throws Exception;
	public int insertAllocationLogByMap(Map<String, Object> map) throws Exception;
	public void insertCustomerCall(Map<String, Object> map) throws Exception;
	public int insertSocketMessage(AlarmSocketVO alarmSocketVO) throws Exception;
	public int insertPageStatus(Map<String, String> map) throws Exception; // page 수정중 lock
	
	public void updateAllocationListOrder(Map<String, Object> map) throws Exception;
	public void updateAllocationSetConfirm(Map<String, Object> map) throws Exception;
	public void updateAllocation(AllocationVO allocationVO) throws Exception;
	public void updateAllocationByMap(Map<String, Object> map) throws Exception;
	public void updateAllocationStatus(Map<String, Object> map) throws Exception;
	public void updateAllocationSetPickup(Map<String, Object> map) throws Exception;
	
	public int deletePageStatus(Map<String, String> map) throws Exception; // page 수정중 lock 해제 
	
	public Map<String,String> allocationModCheck(Map<String,String>map)throws Exception; //배차정보 작업중인지 여부 확인 
	
	
	
	
	
}
