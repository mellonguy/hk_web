package kr.co.carrier.mapper;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.AllocationFinishInfoVO;

public interface AllocationFinishInfoMapper {

	
	public Map<String, Object> selectAllocationFinishInfo(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectAllocationFinishInfoList(Map<String, Object> map) throws Exception;
	public int selectAllocationFinishInfoListCount(Map<String, Object> map) throws Exception;
	public int insertAllocationFinishInfo(AllocationFinishInfoVO allocationFinishInfoVO) throws Exception;
	public void deleteAllocationFinishInfo(Map<String, Object> map) throws Exception;
	public List<Map<String,Object>>selectAllocationStatusList(Map<String, Object> map) throws Exception;
	public List<Map<String,Object>>selectSocketAlarmTalkList(Map<String, Object> map) throws Exception;
	
	
}
