package kr.co.carrier.mapper;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.EmpLoginInfoVO;

public interface EmpLoginInfoMapper {

	
	public Map<String, Object> selectEmpLoginInfo(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectEmpLoginInfoList(Map<String, Object> map) throws Exception;
	public int selectEmpLoginInfoListCount(Map<String, Object> map) throws Exception;
	public int insertEmpLoginInfo(EmpLoginInfoVO empLoginInfoVO) throws Exception;
	public void deleteEmpLoginInfo(Map<String, Object> map) throws Exception;
	public void updateEmpLoginInfoLogoutDt(Map<String, Object> map) throws Exception;
	public int insertDeadLineCheck(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectLoginCheck(Map<String, Object> map) throws Exception;	
	public void updateEmpLoginCertY(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectMonthCertCheck(Map<String, Object> map) throws Exception; 
	public int insertMonthLoginCert(Map<String, Object> map) throws Exception;
	
	
	
	
	
}
