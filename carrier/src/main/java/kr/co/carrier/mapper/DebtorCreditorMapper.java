package kr.co.carrier.mapper;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.DebtorCreditorVO;

public interface DebtorCreditorMapper {

	
	public Map<String, Object> selectDebtorCreditor(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectDebtorCreditorList(Map<String, Object> map) throws Exception;
	public int selectDebtorCreditorListCount(Map<String, Object> map) throws Exception;
	public int insertDebtorCreditor(DebtorCreditorVO debtorCreditorVO) throws Exception;
	public void deleteDebtorCreditor(Map<String, Object> map) throws Exception;
	
	
	public List<Map<String, Object>> selectAllocationListForInsert(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectAllocationListUpdated(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectBillPublishRequestListForInsert(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectCarInfoListByBillPublishRequestId(Map<String, Object> map) throws Exception;
	
	
	public int selectCustomerListCount(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectCustomerList(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectDebtorCreditorListByCustomerId(Map<String, Object> map) throws Exception;
	
	
	public int selectAllocationCountByDebtorCreditorId(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectAllocationListByDebtorCreditorId(Map<String, Object> map) throws Exception;
	
	public List<Map<String, Object>> selectPaymentListByPayment(Map<String, Object> map) throws Exception;
	
	
	public void updateDebtorCreditor(Map<String, Object> map) throws Exception;
	public void updateDebtorCreditorTotalVal(Map<String, Object> map) throws Exception;
	
	public int selectCarInfoCountByDebtorCreditorId(Map<String, Object> map) throws Exception;
	
	/*매출의 일괄 취소 - 계산서발행요청 테이블- 계산서 발행 요청 상태 update (R:발행요청,C:취소,Y:발행)*/
	public void updateBillPublishRequest(Map<String, Object> map)  throws Exception;
	
	/*매출의 일괄 취소 - 운행정보 테이블- 계산서 상태(N:미발행,R:발행요청,Y:발행,D:발행제외) update*/
	public void updateBillingStatus(Map<String, Object> map)  throws Exception;
	
	/*매출의 일괄 취소 - 운행정보 테이블- 계산서 발행 요청 아이디로 delete*/
	public void deleteDebtorCreditors(Map<String, Object> map) throws Exception;
	
	/*매출의 일괄 취소 - 패키지 운송일경우 allocation id를 묶음 찾기*/
	public String selectAllocationIdByBatchStatusId(Map<String, Object> map) throws Exception; 
	
}
