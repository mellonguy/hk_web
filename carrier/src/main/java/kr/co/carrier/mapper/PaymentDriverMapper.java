package kr.co.carrier.mapper;

import java.util.Map;

import kr.co.carrier.vo.PaymentDriverVO;

public interface PaymentDriverMapper {

	
	
	public Map<String, Object> selectPaymentDriver(Map<String, Object> map) throws Exception;
	public void updatePaymentDriver(PaymentDriverVO paymentDriverVO) throws Exception;
	
	
	
	
}
