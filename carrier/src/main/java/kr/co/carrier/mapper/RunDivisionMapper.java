package kr.co.carrier.mapper;

import java.util.List;
import java.util.Map;

public interface RunDivisionMapper {

	
	public List<Map<String, Object>>selectRunDivisionInfoList() throws Exception;
	public Map<String, Object>selectRunDivisionInfo(Map<String, Object> map) throws Exception;
	
	
	
	
	
}
