package kr.co.carrier.mapper;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.DepositDriverVO;
import kr.co.carrier.vo.DriverDepositVO;

public interface DriverDepositMapper {

	
	public Map<String, Object> selectDriverDeposit(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectDriverDepositList(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectDriverDepositListDetail(Map<String, Object> map) throws Exception;
	public int selectDriverDepositListCount(Map<String, Object> map) throws Exception;
	public int insertDriverDeposit(DriverDepositVO driverDepositVO) throws Exception;
	public void deleteDriverDeposit(Map<String, Object> map) throws Exception;
	public void updateDriverDeposit(DriverDepositVO driverDepositVO) throws Exception;
	
	public int insertDepositDriver(DepositDriverVO depositDriverVO) throws Exception;
	public Map<String, Object> selectDepositDriver(Map<String, Object> map) throws Exception;
	public int updateDriver(Map<String, String> map) throws Exception;
	public int updateDepositDriver(Map<String, String> map) throws Exception;
	
	
	
	
	
}
