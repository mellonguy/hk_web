package kr.co.carrier.mapper;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.DriverVO;

public interface DriverMapper {

	
	public Map<String, Object> selectDriver(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectDriverList(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectArticlesDriverList(Map<String, Object> map) throws Exception;
	public int selectDriverListCount(Map<String, Object> map) throws Exception;
	public int selectArticlesCount(Map<String, Object> map) throws Exception;
	public int insertDriver(DriverVO driverVO) throws Exception;
	public void updateDriver(DriverVO driverVO) throws Exception;
	public void deleteDriver(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectDriverListByCustomerId(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectDriverListForDriverLocation(Map<String, Object> map) throws Exception;
	public void updateDriverStatus(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectDriverByDriverName(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectWorkDriverList(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectDriverListByDriverName(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectDriverListByCarNum(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectDriverListByDriverNameForDriverPayment(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectSearchForDriverNameList(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectRateDiscountDriverList(Map<String, Object> map) throws Exception;
	public void updatedeDuctionRateDriver(Map<String, Object> map) throws Exception;
	public int insertRateDiscountDriver(Map<String, Object> map) throws Exception;
	
	
	
	
	
}
