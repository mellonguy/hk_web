package kr.co.carrier.mapper;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.CustomerAddressInfoVO;
public interface CustomerAddressInfoMapper {

	
public List<Map<String,Object>> selectKeywordAddressInfoList (Map<String, Object> map) throws Exception;
public int insertCustomerAddressInfoInsert(CustomerAddressInfoVO customerAddressInfoVO)throws Exception;
public void deleteCustomerAddressInfo(Map<String,Object> map) throws Exception;	

}
