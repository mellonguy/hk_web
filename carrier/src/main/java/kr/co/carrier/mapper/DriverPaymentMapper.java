package kr.co.carrier.mapper;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.DriverPaymentVO;
import kr.co.carrier.vo.PaymentDriverVO;

public interface DriverPaymentMapper {

	
	public Map<String, Object> selectDriverPayment(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectDriverPaymentList(Map<String, Object> map) throws Exception;
	public int selectDriverPaymentListCount(Map<String, Object> map) throws Exception;
	public int insertDriverPayment(DriverPaymentVO driverPaymentVO) throws Exception;
	public void deleteDriverPayment(Map<String, Object> map) throws Exception;
	public void updateDriverPayment(DriverPaymentVO driverPaymentVO) throws Exception;
	public void updatePaymentDriver(PaymentDriverVO paymentDriverVO) throws Exception;
	public List<Map<String, Object>>  selectDriverPaymentOwnerList(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>>  selectDriverPaymentDriverList(Map<String, Object> map) throws Exception;
	
	public int selectDriverPaymentOwnerListCount(Map<String, Object> map) throws Exception;
	public int selectDriverPaymentDriverListCount(Map<String, Object> map) throws Exception;
	
	public int insertPaymentDriver(PaymentDriverVO paymentDriverVO) throws Exception;
	public List<Map<String, Object>>  selectDriverPaymentDetailList(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>>  selectCarMiddlePaymentDriverList(Map<String, Object> map) throws Exception;
	public void updateExemptionsPaymentM(Map<String, Object> map) throws Exception;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
