package kr.co.carrier.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import kr.co.carrier.handler.EchoHandler;
import kr.co.carrier.mapper.FileMapper;
import kr.co.carrier.service.AddressInfoService;
import kr.co.carrier.service.AdjustmentService;
import kr.co.carrier.service.AlarmTalkService;
import kr.co.carrier.service.AllocationDivisionService;
import kr.co.carrier.service.AllocationFileService;
import kr.co.carrier.service.AllocationFinishInfoService;
import kr.co.carrier.service.AllocationService;
import kr.co.carrier.service.AllocationStatusService;
import kr.co.carrier.service.BillingDivisionService;
import kr.co.carrier.service.CarInfoService;
import kr.co.carrier.service.CheckDetailService;
import kr.co.carrier.service.CompanyService;
import kr.co.carrier.service.CustomerService;
import kr.co.carrier.service.DebtorCreditorService;
import kr.co.carrier.service.DriverDepositService;
import kr.co.carrier.service.DriverLocationInfoService;
import kr.co.carrier.service.DriverService;
import kr.co.carrier.service.EmployeeService;
import kr.co.carrier.service.FcmService;
import kr.co.carrier.service.FileService;
import kr.co.carrier.service.FileUploadService;
import kr.co.carrier.service.PayDivisionService;
import kr.co.carrier.service.PaymentDivisionService;
import kr.co.carrier.service.PaymentInfoService;
import kr.co.carrier.service.PersonInChargeService;
import kr.co.carrier.service.RunDivisionService;
import kr.co.carrier.service.SendSmsService;
import kr.co.carrier.utils.BaseAppConstants;
import kr.co.carrier.utils.PagingUtils;
import kr.co.carrier.utils.ParamUtils;
import kr.co.carrier.utils.ResultApi;
import kr.co.carrier.utils.WebUtils;
import kr.co.carrier.vo.AddressInfoVO;
import kr.co.carrier.vo.AlarmSocketVO;
import kr.co.carrier.vo.AllocationFinishInfoVO;
import kr.co.carrier.vo.AllocationVO;
import kr.co.carrier.vo.CarInfoVO;
import kr.co.carrier.vo.DebtorCreditorVO;
import kr.co.carrier.vo.DepositDriverVO;
import kr.co.carrier.vo.FileVO;
import kr.co.carrier.vo.PaymentInfoVO;

@Controller
@RequestMapping(value="/allocation")
public class AllocationController {

	
	private static final Logger logger = LoggerFactory.getLogger(AllocationController.class);
	
	@Value("#{appProp['upload.base.path']}")
    private String rootDir;    
    private String subDir = "bbs";	
	
    
    @Autowired
    private AllocationService allocationService;
	
    @Autowired
    private CompanyService companyService;
    
    @Autowired
    private DriverService driverService;
    
    @Autowired
    private CarInfoService carInfoService;
    
    @Autowired
    private BillingDivisionService billingDivisionService;
    
    @Autowired
    private AllocationDivisionService allocationDivisionService;
    
    @Autowired
    private RunDivisionService runDivisionService;

    @Autowired
    private PaymentDivisionService paymentDivisionService;
    
    @Autowired
    private PayDivisionService payDivisionService;
    
    @Autowired
    private PaymentInfoService paymentInfoService;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private PersonInChargeService personInChargeService;
    
    @Autowired
    private FcmService fcmService;
    
    @Autowired
    private AllocationFileService allocationFileService;
    
    @Autowired
    private CheckDetailService checkDetailService;
    
    @Autowired
    private AddressInfoService addressInfoService;
    
    @Autowired
    private DriverLocationInfoService driverLocationInfoService;
    
    @Autowired
    private AlarmTalkService alarmTalkService;
    
    @Autowired
    private AllocationFinishInfoService allocationFinishInfoService;
    
    @Autowired
    private DebtorCreditorService debtorCreditorService;
    
    @Autowired
    private AdjustmentService adjustmentService;
    
    @Autowired
    private SendSmsService sendSmsService;
    
    @Autowired
    private AllocationStatusService allocationStatusService;
    
    
    @Autowired
    private DriverDepositService driverDepositService;
    
    @Autowired
    private EmployeeService empService;
    
    
    @Autowired
	private FileUploadService fileUploadService;
    
    @Autowired
    private FileService fileService;
    
    @Resource(name="fileMapper")
	private FileMapper fileMapper;
    
    
	@RequestMapping(value = "/combination", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView archive(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			// String companyId = session.getAttribute("companyId").toString();
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			String dept = userMap != null && userMap.get("dept") != null && !userMap.get("dept").toString().equals("") ? userMap.get("dept").toString():"";
			if(dept.equals("sunbo")){		//선보 부서의 아이디 인 경우 선보의 목록만 보여준다.
				paramMap.put("dept", dept);
			}
			
			if(paramMap.get("searchType") == null || paramMap.get("searchType").equals("")) {
				paramMap.put("searchType", "total");
			}
			
			if(paramMap.get("searchType") != null && !paramMap.get("searchType").equals("")){
				if(paramMap.get("searchType").equals("carrier")){
					if(paramMap.get("searchWord").equals("셀프")){
						paramMap.put("searchWord", "S");
					}else{
						paramMap.put("searchWord", "C");
					}
				}else if(paramMap.get("searchType").equals("distance")){
					if(paramMap.get("searchWord").equals("시내")){
						paramMap.put("searchWord", "C");
					}else if(paramMap.get("searchWord").equals("상행")){
						paramMap.put("searchWord", "U");
					}else if(paramMap.get("searchWord").equals("하행")){
						paramMap.put("searchWord", "D");
					}else if(paramMap.get("searchWord").equals("픽업")){
						paramMap.put("searchWord", "P");
					}
				}
			}
			
			
			String forOrder = request.getParameter("forOrder")==null || request.getParameter("forOrder").toString().equals("") ?"":request.getParameter("forOrder").toString();
			if(!forOrder.equals("")){
				String[] sort = forOrder.split("\\^"); 
				if(sort.length > 1 && !sort[0].equals("") && !sort[1].equals("")){
					paramMap.put("forOrder", sort[0]);
					paramMap.put("order", sort[1]);
					mav.addObject("order",  sort[1]);
				}else {
					paramMap.put("forOrder", "");
					paramMap.put("order", "");
					mav.addObject("order",  "");
				}
			}
			
			
			
			String startDt = request.getParameter("startDt") == null ? "" : request.getParameter("startDt").toString();
			String endDt = request.getParameter("endDt") == null ? "" : request.getParameter("endDt").toString();
			String searchDateType = request.getParameter("searchDateType") == null || request.getParameter("searchDateType").toString().equals("") ? "D" : request.getParameter("searchDateType").toString();
			String allocationStatus =request.getParameter("allocationStatus") == null || request.getParameter("allocationStatus").toString().equals("") ? "" : request.getParameter("allocationStatus").toString();
			String regType =request.getParameter("regType") == null || request.getParameter("regType").toString().equals("") ? "" : request.getParameter("regType").toString();
			String allocationStatistics = request.getParameter("allocationStatistics") == null || request.getParameter("allocationStatistics").toString().equals("") ? "" : request.getParameter("allocationStatistics").toString();
			

			
			/*if(startDt.equals("")) {
				startDt = WebUtils.getNow("yyyy-MM-dd");
			}
			if(endDt.equals("")) {
				endDt = WebUtils.getNow("yyyy-MM-dd");
			}*/
			String location = "combination";
			paramMap.put("location", location);
			paramMap.put("startDt", startDt);
			paramMap.put("endDt", endDt);
			paramMap.put("searchDateType", searchDateType);
			paramMap.put("allocationStatus", allocationStatus);
			paramMap.put("regType", regType);
			paramMap.put("allocationStatistics", allocationStatistics);
			
			if(!allocationStatistics.equals("")) {
				
				paramMap.put("today",WebUtils.getNow("yyyy-MM-dd").toString());
				
			}
			
			
			// paramMap.put("companyId", companyId);
			
			int totalCount =0;
			
			if(!allocationStatistics.equals("")) {
				
				totalCount	=	allocationService.selectAllocationListCountSub(paramMap);
		
			}else {
				
				 totalCount =	allocationService.selectAllocationListCount(paramMap);
			}
			
			PagingUtils.setPageing(request, totalCount, paramMap);
			paramMap.put("carrierType", "");
			
			//List<Map<String, Object>> allocationList=	allocationService.selectAllocationList(paramMap);
			
			List<Map<String, Object>> allocationList = null;
					
					if(!allocationStatistics.equals("")) {
						
						allocationList = allocationService.selectAllocationListSub(paramMap);
					}else {
						allocationList = allocationService.selectAllocationList(paramMap);
					}
				
					
		
			/*
			for(int i = 0; i < allocationList.size(); i++) {
				Map<String, Object> map = allocationList.get(i);
				//batch_status 가 Y 이면 일괄배차 매출액 총액을 가저온다.
				if(map.get("batch_status").toString().equals("Y")) {
					Map<String, Object> salesTotalMap = carInfoService.selectSumSalesTotalByBatchStatusId(map);
					map.put("sales_total", salesTotalMap.get("sales_total").toString());
					map.put("driver_amount", salesTotalMap.get("driver_amount").toString());
					allocationList.set(i, map);
				}
			}
			*/
			
			if(paramMap.get("searchType") != null && !paramMap.get("searchType").equals("")){
				if(paramMap.get("searchType").equals("carrier")){
					if(paramMap.get("searchWord").equals("S")){
						paramMap.put("searchWord", "셀프");
					}else{
						paramMap.put("searchWord", "캐리어");
					}
				}else if(paramMap.get("searchType").equals("distance")){
					if(paramMap.get("searchWord").equals("C")){
						paramMap.put("searchWord", "시내");
					}else if(paramMap.get("searchWord").equals("U")){
						paramMap.put("searchWord", "상행");
					}else if(paramMap.get("searchWord").equals("D")){
						paramMap.put("searchWord", "하행");
					}else if(paramMap.get("searchWord").equals("P")){
						paramMap.put("searchWord", "픽업");
					}
				}
			}
			
			Map<String, Object> map = new HashMap<String, Object>();
			List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
			//List<Map<String, Object>> driverList = driverService.selectDriverList(map);
			//List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
			//mav.addObject("billingDivisionList",  billingDivisionList);
			mav.addObject("userMap",  userMap);
			//mav.addObject("driverList",  driverList);RALL
			mav.addObject("companyList",  companyList);
			mav.addObject("listData",  allocationList);
			
			String forOpen = request.getParameter("forOpen")==null?"":request.getParameter("forOpen").toString();
			mav.addObject("forOpen", forOpen);
			mav.addObject("paramMap", paramMap);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
		
			List<Map<String, Object>> allocationStatusList = allocationStatusService.selectAllocationStatusList(map);
			mav.addObject("allocationStatusList", allocationStatusList);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	@RequestMapping(value = "/self", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView self(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			// String companyId = session.getAttribute("companyId").toString();
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			if(paramMap.get("searchType") == null || paramMap.get("searchType").equals("")) {
				paramMap.put("searchType", "total");
			}
			
			String dept = userMap != null && userMap.get("dept") != null && !userMap.get("dept").toString().equals("") ? userMap.get("dept").toString():"";
			if(dept.equals("sunbo")){		//선보 부서의 아이디 인 경우 선보의 목록만 보여준다.
				paramMap.put("dept", dept);
			}
			
			paramMap.put("carrierType", "S");
			String allocationStatus = request.getParameter("allocationStatus")==null?"N":request.getParameter("allocationStatus").toString();
			paramMap.put("allocationStatus", allocationStatus);
			String startDt = request.getParameter("startDt") == null ? "" : request.getParameter("startDt").toString();
			String endDt = request.getParameter("endDt") == null ? "" : request.getParameter("endDt").toString();
			String searchDateType = request.getParameter("searchDateType") == null || request.getParameter("searchDateType").toString().equals("") ? "D" : request.getParameter("searchDateType").toString();
			/*if(startDt.equals("")) {
				startDt = WebUtils.getNow("yyyy-MM-dd");
			}
			if(endDt.equals("")) {
				endDt = WebUtils.getNow("yyyy-MM-dd");
			}*/
			String location = "self";
			paramMap.put("location", location);
			paramMap.put("startDt", startDt);
			paramMap.put("endDt", endDt);
			paramMap.put("searchDateType", searchDateType);
			// paramMap.put("companyId", companyId);
			
			int totalCount = allocationService.selectAllocationListCount(paramMap);
			PagingUtils.setPageing(request, totalCount, paramMap);
		
			
			String forOrder = request.getParameter("forOrder")==null?"":request.getParameter("forOrder").toString();
			if(!forOrder.equals("")){
				String[] sort = forOrder.split("\\^"); 
				if(sort.length > 1 && !sort[0].equals("") && !sort[1].equals("")){
					paramMap.put("forOrder", sort[0]);
					paramMap.put("order", sort[1]);
					mav.addObject("order",  sort[1]);
				}else {
					paramMap.put("forOrder", "");
					paramMap.put("order", "");
					mav.addObject("order",  "");
				}
			}
			
			List<Map<String, Object>> allocationList = allocationService.selectAllocationList(paramMap);
			for(int i = 0; i < allocationList.size(); i++) {
				Map<String, Object> map = allocationList.get(i);
				//batch_status 가 Y 이면 일괄배차 매출액 총액을 가저온다.
				if(map.get("batch_status").toString().equals("Y")) {
					Map<String, Object> salesTotalMap = carInfoService.selectSumSalesTotalByBatchStatusId(map);
					map.put("sales_total", salesTotalMap.get("sales_total").toString());
					map.put("driver_amount", salesTotalMap.get("driver_amount").toString());
					allocationList.set(i, map);
				}
			}
			Map<String, Object> map = new HashMap<String, Object>();
			List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
			
			if(allocationStatus.equals("N")) {
				List<Map<String, Object>> driverList = driverService.selectWorkDriverList(map);
				mav.addObject("driverList",  driverList);
			}
			//List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
			int total = customerService.selectCustomerListCount(map);
			map.put("startRownum", 0);
			map.put("numOfRows", total);
			List<Map<String, Object>> customerList = customerService.selectCustomerListIncludeDriver(map);
			//mav.addObject("billingDivisionList",  billingDivisionList);
			mav.addObject("companyList",  companyList);
			mav.addObject("customerList",  customerList);
			String listOrder = "";
			String allocationId = "";
			for(int i = 0; i < allocationList.size(); i++){
				Map<String, Object> allocation = allocationList.get(i);
				listOrder += allocation.get("list_order").toString();
				allocationId += allocation.get("allocation_id").toString();
				if(i < allocationList.size()-1){
					listOrder += ",";	
					allocationId += ",";
				}
			}
			
			mav.addObject("userMap",  userMap);
			mav.addObject("allocationId",  allocationId);
			mav.addObject("listOrder",  listOrder);
			mav.addObject("listData",  allocationList);
			mav.addObject("paramMap", paramMap);
			mav.addObject("totalCount", totalCount);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
			
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	@RequestMapping(value = "/carrier", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView carrier(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			// String companyId = session.getAttribute("companyId").toString();
			
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			paramMap.put("carrierType", "C");
			
			if(paramMap.get("searchType") == null || paramMap.get("searchType").equals("")) {
				paramMap.put("searchType", "total");
			}
			
			String dept = userMap != null && userMap.get("dept") != null && !userMap.get("dept").toString().equals("") ? userMap.get("dept").toString():"";
			if(dept.equals("sunbo")){		//선보 부서의 아이디 인 경우 선보의 목록만 보여준다.
				paramMap.put("dept", dept);
			}
			
			String allocationStatus = request.getParameter("allocationStatus")==null?"N":request.getParameter("allocationStatus").toString();
			paramMap.put("allocationStatus", allocationStatus);
			String startDt = request.getParameter("startDt") == null ? "" : request.getParameter("startDt").toString();
			String endDt = request.getParameter("endDt") == null ? "" : request.getParameter("endDt").toString();
			String searchDateType = request.getParameter("searchDateType") == null || request.getParameter("searchDateType").toString().equals("") ? "D" : request.getParameter("searchDateType").toString();
			/*if(startDt.equals("")) {
				startDt = WebUtils.getNow("yyyy-MM-dd");
			}
			if(endDt.equals("")) {
				endDt = WebUtils.getNow("yyyy-MM-dd");
			}*/
			String location = "carrier";
			paramMap.put("location", location);
			paramMap.put("startDt", startDt);
			paramMap.put("endDt", endDt);
			paramMap.put("searchDateType", searchDateType);
			// paramMap.put("companyId", companyId);
			
			int totalCount = allocationService.selectAllocationListCount(paramMap);
			PagingUtils.setPageing(request, totalCount, paramMap);
			
			String forOrder = request.getParameter("forOrder")==null?"":request.getParameter("forOrder").toString();
			if(!forOrder.equals("")){
				String[] sort = forOrder.split("\\^");
				if(sort.length > 1 && !sort[0].equals("") && !sort[1].equals("")){
					paramMap.put("forOrder", sort[0]);
					paramMap.put("order", sort[1]);
					mav.addObject("order",  sort[1]);	
				}else {
					paramMap.put("forOrder", "");
					paramMap.put("order", "");
					mav.addObject("order",  "");
				}
			}
			List<Map<String, Object>> allocationList = allocationService.selectAllocationList(paramMap);
			for(int i = 0; i < allocationList.size(); i++) {
				Map<String, Object> map = allocationList.get(i);
				//batch_status 가 Y 이면 일괄배차 매출액 총액을 가저온다.
				if(map.get("batch_status").toString().equals("Y")) {
					Map<String, Object> salesTotalMap = carInfoService.selectSumSalesTotalByBatchStatusId(map);
					map.put("sales_total", salesTotalMap.get("sales_total").toString());
					map.put("driver_amount", salesTotalMap.get("driver_amount").toString());
					allocationList.set(i, map);
				}
			}
			
			Map<String, Object> map = new HashMap<String, Object>();
			List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
			if(allocationStatus.equals("N")) {
				List<Map<String, Object>> driverList = driverService.selectWorkDriverList(map);
				mav.addObject("driverList",  driverList);
			}
			//List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
			int total = customerService.selectCustomerListCount(map);
			map.put("startRownum", 0);
			map.put("numOfRows", total);
			List<Map<String, Object>> customerList = customerService.selectCustomerListIncludeDriver(map);
			//mav.addObject("billingDivisionList",  billingDivisionList);
			mav.addObject("companyList",  companyList);
			mav.addObject("customerList",  customerList);
			String listOrder = "";
			String allocationId = "";
			for(int i = 0; i < allocationList.size(); i++){
				Map<String, Object> allocation = allocationList.get(i);
				listOrder += allocation.get("list_order").toString();
				allocationId += allocation.get("allocation_id").toString();
				if(i < allocationList.size()-1){
					listOrder += ",";	
					allocationId += ",";
				}
			}
			
			mav.addObject("userMap",  userMap);
			mav.addObject("allocationId",  allocationId);
			mav.addObject("listOrder",  listOrder);
			mav.addObject("listData",  allocationList);
			mav.addObject("paramMap", paramMap);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
						
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	
	
	
	
	@RequestMapping(value = "/viewCancelRequest", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView viewCancelRequest(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
		
			
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			String companyId = session.getAttribute("companyId").toString();
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			String dept = userMap != null && userMap.get("dept") != null && !userMap.get("dept").toString().equals("") ? userMap.get("dept").toString():"";
			if(dept.equals("sunbo")){		//선보 부서의 아이디 인 경우 선보의 목록만 보여준다.
				paramMap.put("dept", dept);
			}
			
			
			String allocationStatus = request.getParameter("allocationStatus")==null?"X":request.getParameter("allocationStatus").toString();
			paramMap.put("allocationStatus", allocationStatus);
			String startDt = request.getParameter("startDt") == null ? "" : request.getParameter("startDt").toString();
			String endDt = request.getParameter("endDt") == null ? "" : request.getParameter("endDt").toString();
			String searchDateType = request.getParameter("searchDateType") == null ? "" : request.getParameter("searchDateType").toString();
			/*if(startDt.equals("")) {
				startDt = WebUtils.getNow("yyyy-MM-dd");
			}
			if(endDt.equals("")) {
				endDt = WebUtils.getNow("yyyy-MM-dd");
			}*/
			
			paramMap.put("startDt", startDt);
			paramMap.put("endDt", endDt);
			paramMap.put("searchDateType", searchDateType);
			//paramMap.put("companyId", companyId);
			
			int totalCount = allocationService.selectAllocationListCount(paramMap);
			PagingUtils.setPageing(request, totalCount, paramMap);
		
			
			String forOrder = request.getParameter("forOrder")==null?"":request.getParameter("forOrder").toString();
			if(!forOrder.equals("")){
				String[] sort = forOrder.split("\\^"); 
				if(sort.length > 1 && !sort[0].equals("") && !sort[1].equals("")){
					paramMap.put("forOrder", sort[0]);
					paramMap.put("order", sort[1]);
					mav.addObject("order",  sort[1]);
				}else {
					paramMap.put("forOrder", "");
					paramMap.put("order", "");
					mav.addObject("order",  "");
				}
			}
			
			List<Map<String, Object>> allocationList = allocationService.selectAllocationList(paramMap);
			for(int i = 0; i < allocationList.size(); i++) {
				Map<String, Object> map = allocationList.get(i);
				//batch_status 가 Y 이면 일괄배차 매출액 총액을 가저온다.
				if(map.get("batch_status").toString().equals("Y")) {
					Map<String, Object> salesTotalMap = carInfoService.selectSumSalesTotalByBatchStatusId(map);
					map.put("sales_total", salesTotalMap.get("sales_total").toString());
					map.put("driver_amount", salesTotalMap.get("driver_amount").toString());
					allocationList.set(i, map);
				}
			}
			Map<String, Object> map = new HashMap<String, Object>();
			//List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
			
			if(allocationStatus.equals("N")) {
				List<Map<String, Object>> driverList = driverService.selectWorkDriverList(map);
				mav.addObject("driverList",  driverList);
			}
			//List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
			int total = customerService.selectCustomerListCount(map);
			map.put("startRownum", 0);
			map.put("numOfRows", total);
			List<Map<String, Object>> customerList = customerService.selectCustomerListIncludeDriver(map);
			//mav.addObject("billingDivisionList",  billingDivisionList);
			//mav.addObject("companyList",  companyList);
			mav.addObject("customerList",  customerList);
			String listOrder = "";
			String allocationId = "";
			for(int i = 0; i < allocationList.size(); i++){
				Map<String, Object> allocation = allocationList.get(i);
				listOrder += allocation.get("list_order").toString();
				allocationId += allocation.get("allocation_id").toString();
				if(i < allocationList.size()-1){
					listOrder += ",";	
					allocationId += ",";
				}
			}
			
			mav.addObject("userMap",  userMap);
			mav.addObject("allocationId",  allocationId);
			mav.addObject("listOrder",  listOrder);
			mav.addObject("listData",  allocationList);
			mav.addObject("paramMap", paramMap);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
			
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	
	@RequestMapping(value = "/insert-allocation", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView insertAllocation(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute AllocationVO allocationVO) throws Exception {
		
		try{
			
			
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			Map<String, Object> map = new HashMap<String, Object>();
			
			map.put("duplicateChk", "true");
			
			String carInfoVal = request.getParameter("carInfoVal").toString();
			String paymentInfoVal = request.getParameter("paymentInfoVal").toString();
			
			if(!carInfoVal.equals("")){
				JSONParser jsonParser = new JSONParser();
				JSONObject jsonObject = (JSONObject) jsonParser.parse(carInfoVal);
				JSONArray jsonArray = (JSONArray) jsonObject.get("carInfoList");	
				String batchStatusId = "BAT"+UUID.randomUUID().toString().replaceAll("-","");
				for(int i = 0; i < jsonArray.size(); i++){
					
					String allocationId = "ALO"+UUID.randomUUID().toString().replaceAll("-","");
					allocationVO.setAllocationId(allocationId);
					
					//담당자 명이 현대캐피탈인 경우 
					if(allocationVO.getChargeName().equals("현대캐피탈")) {
						allocationVO.setHcYn("Y");
					}
					
					JSONObject carInfo = (JSONObject)jsonArray.get(i);
					CarInfoVO carInfoVO = new CarInfoVO();
					carInfoVO.setAllocationId(allocationId);
					carInfoVO.setCarrierType(carInfo.get("carrier_type").toString());
					carInfoVO.setDistanceType(carInfo.get("distance_type").toString());
					carInfoVO.setDepartureDt(carInfo.get("departure_dt").toString());
					carInfoVO.setDepartureTime(carInfo.get("departure_time").toString());
					carInfoVO.setSalesTotal(carInfo.get("sales_total").toString());
					carInfoVO.setPrice(carInfo.get("sales_total").toString());
					carInfoVO.setDriverId(carInfo.get("driver_id").toString());
					carInfoVO.setReceiptSendYn(carInfo.get("receipt_send_yn").toString());
					String receiptName = "";
					String receiptEmail = "";
					String receiptPhone = "";
					receiptName = carInfo.get("receipt_name") != null && !carInfo.get("receipt_name").toString().equals("") ? carInfo.get("receipt_name").toString():"";
					receiptEmail = carInfo.get("receipt_email") != null && !carInfo.get("receipt_email").toString().equals("") ? carInfo.get("receipt_email").toString():"";
					receiptPhone = carInfo.get("receipt_phone") != null && !carInfo.get("receipt_phone").toString().equals("") ? carInfo.get("receipt_phone").toString():"";
					carInfoVO.setReceiptName(receiptName);
					carInfoVO.setReceiptEmail(receiptEmail);
					carInfoVO.setReceiptPhone(receiptPhone);
					
					//거래처정보
					Map<String, Object> customerFindMap = new HashMap<String, Object>();
					customerFindMap.put("customerId", allocationVO.getCustomerId());
					Map<String , Object> customer = customerService.selectCustomer(customerFindMap);
					String sendAccountInfoYn = customer.get("send_account_info_yn").toString();
					
					map.put("driverId", carInfo.get("driver_id").toString());
					Map<String, Object> driver = driverService.selectDriver(map);
					if(driver != null){
						carInfoVO.setDriverName(driver.get("driver_name").toString());	
					}
					if(driver != null || !carInfo.get("driver_name").toString().equals("")) {		//신규 배차 등록시 기사가 배정 되어 있으면 미배차가 아니고 배차된 상태이다.
						//해당 기사가 거래처 소속인 기사인 경우는 배차 된 상태이나 거래처 소속이 아닌경우 배차 승인대기중이다. 
						if(driver.get("customer_id") != null && !driver.get("customer_id").toString().equals("")) {		//거래처 소속
							
							allocationVO.setAllocationStatus("Y");	//배차완료처리
							
							//거래처 소속이고 문자발송이 Y 일때는 배차승인 대기중으로 표시 
							if(customer.get("sms_yn").equals("Y")) {
								allocationVO.setAllocationStatus("A");//일반
							}
						
						
						}else {
							allocationVO.setAllocationStatus("A");				//일반
						}
					}else {
						allocationVO.setAllocationStatus("N"); //미배차
					}
					
					String paymentInputStatus = request.getParameter("paymentInputStatus") == null ? "" : request.getParameter("paymentInputStatus").toString();
					if(paymentInputStatus.equals("N")) {				//배차 등록시 결제 정보가 입력 되지 않은경우 기사가 배정 되어 있어도 미배차 상태로 표시 한다.
						allocationVO.setAllocationStatus("N");
					}
					//해당 배차에 거래처 소속이 아닌 기사가 배정 되었고 결제 정보가 모두 입력 되어 있는 경우에는 배차승인 대기상태이고 해당기사에게 푸시 알림을 전송 한다.
					if(allocationVO.getAllocationStatus().equals("A")) {			 
						if(driver.get("fcm_token") != null && !driver.get("fcm_token").toString().equals("")) {
							Map<String, Object> sendMessageMap = new HashMap<String, Object>();
							sendMessageMap.put("title", "배차 정보가 도착 했습니다.");
							sendMessageMap.put("body", 
									"상차지:"+carInfo.get("departure").toString()+
									",하차지:"+carInfo.get("arrival").toString()+
									",차종:"+carInfo.get("car_kind").toString()+
									",차대번호:"+carInfo.get("car_id_num").toString());
							sendMessageMap.put("fcm_token", driver.get("fcm_token").toString());
							sendMessageMap.put("device_os", driver.get("device_os").toString());
							sendMessageMap.put("allocationId", allocationId);
							fcmService.fcmSendMessage(sendMessageMap);	
						
						}
						
						Map<String, Object> driverSetEmpMap = new HashMap<String, Object>();
						driverSetEmpMap.put("empId", userMap.get("emp_id").toString());
						driverSetEmpMap.put("empName", userMap.get("emp_name").toString());
						driverSetEmpMap.put("allocationId",allocationId);
						driverSetEmpMap.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
						driverSetEmpMap.put("logType","U");
						carInfoService.updateCarInfoDriverSetEmp(driverSetEmpMap);
						carInfoService.insertCarInfoLogByMap(driverSetEmpMap); //로그기록
					}
					
					if(allocationVO.getBatchStatus().equals("N")) {//개별								//일괄 등록과 개별 등록 구분
						allocationVO.setBatchStatusId("BAT"+UUID.randomUUID().toString().replaceAll("-",""));
						allocationVO.setBatchIndex(String.valueOf(0));
					}else if(allocationVO.getBatchStatus().equals("Y")) {//일괄
						allocationVO.setBatchStatusId(batchStatusId);
						allocationVO.setBatchIndex(String.valueOf(i));
					}else if(allocationVO.getBatchStatus().equals("P")) {//픽업
						allocationVO.setBatchStatusId(batchStatusId);
						allocationVO.setBatchIndex("0");
					}
					
					allocationService.insertAllocation(allocationVO);
					
					allocationVO.setLogType("I");
					allocationService.insertAllocationLogByVO(allocationVO); //로그기록
					
					carInfoVO.setCarKind(carInfo.get("car_kind").toString());
					carInfoVO.setCarIdNum(carInfo.get("car_id_num").toString());
					carInfoVO.setCarNum(carInfo.get("car_num").toString());
					carInfoVO.setContractNum(carInfo.get("contract_num").toString());
					carInfoVO.setTowDistance(carInfo.get("tow_distance").toString());
					carInfoVO.setRequirePicCnt(carInfo.get("require_pic_cnt").toString());
					carInfoVO.setRequireDnPicCnt(carInfo.get("require_dn_pic_cnt").toString());
					carInfoVO.setAccidentYn(carInfo.get("accident_yn").toString());
					carInfoVO.setEtc(carInfo.get("etc").toString());
					carInfoVO.setDeparture(carInfo.get("departure").toString());
					carInfoVO.setDepartureAddr(carInfo.get("departure_addr").toString());
					carInfoVO.setDeparturePersonInCharge(carInfo.get("departure_person_in_charge").toString());
					carInfoVO.setDeparturePhone(carInfo.get("departure_phone").toString());
					carInfoVO.setArrival(carInfo.get("arrival").toString());
					carInfoVO.setArrivalAddr(carInfo.get("arrival_addr").toString());
					carInfoVO.setArrivalPersonInCharge(carInfo.get("arrival_person_in_charge").toString());
					carInfoVO.setArrivalPhone(carInfo.get("arrival_phone").toString());
					carInfoVO.setDriverCnt(carInfo.get("driver_cnt") == null || carInfo.get("driver_cnt").toString().equals("")?"1":carInfo.get("driver_cnt").toString());
					carInfoVO.setModId(userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자추가
					carInfoVO.setLogType("I");
					
					carInfoService.insertCarInfo(carInfoVO);					
		    		carInfoService.insertCarInfoLogByVO(carInfoVO); //로그기록
		    		
					String carInfoIndex = carInfo.get("index").toString();
					
					if(!paymentInfoVal.equals("")){
						JSONParser jsonPaymentParser = new JSONParser();
						JSONObject jsonPaymentObject = (JSONObject) jsonPaymentParser.parse(paymentInfoVal);
						JSONArray jsonPaymentArray = (JSONArray) jsonPaymentObject.get("paymentInfoList");	
						for(int j = 0; j < jsonPaymentArray.size(); j++){
							JSONObject paymentInfo = (JSONObject)jsonPaymentArray.get(j);
							if(paymentInfo.get("index").toString().equals(carInfoIndex)) {
								PaymentInfoVO paymentInfoVO = new PaymentInfoVO();
								paymentInfoVO.setAllocationId(allocationId);
								paymentInfoVO.setAmount(paymentInfo.get("amount").toString());
								paymentInfoVO.setBillForPayment(paymentInfo.get("bill_for_payment").toString());
								paymentInfoVO.setBillingDivision(paymentInfo.get("billing_division").toString());
								paymentInfoVO.setBillingDt(paymentInfo.get("billing_dt").toString());
								paymentInfoVO.setDeductionRate(paymentInfo.get("deduction_rate").toString());
								paymentInfoVO.setEtc(paymentInfo.get("etc").toString());
								//결제 여부(payment)가 부분결제(P) 또는 결제(Y)인것에 대해 대변에 입력 한다.
								paymentInfoVO.setPayment(paymentInfo.get("payment") != null && !paymentInfo.get("payment").toString().equals("") ? paymentInfo.get("payment").toString() : "N");
								paymentInfoVO.setPaymentDivision(paymentInfo.get("payment_division").toString());
								
								//매출처 결제 정보인 경우이고 
								if(paymentInfoVO.getPaymentDivision().equals("01")) {
									
									paymentInfoVO.setVatIncludeYn("N");
									paymentInfoVO.setVatExcludeYn("N");
									//paymentInfoVO.setAddColYn(paymentInfo.get("add_col_yn").toString()); //추가 컬럼 여부
									//결제 여부가 부분결제 또는 결제인 경우 대변에 입력 한다...
									if(paymentInfoVO.getPayment().equals("P") || paymentInfoVO.getPayment().equals("Y")) {
										
										//결제가 완료된 배차건의 경우 계좌 정보를 발송 하지 않는다..
										if(paymentInfoVO.getPayment().equals("Y")) {
											sendAccountInfoYn = "N";
										}
										
										String debtorCreditorId = "DCI"+UUID.randomUUID().toString().replaceAll("-","");
										DebtorCreditorVO debtorCreditorVO = new DebtorCreditorVO();
										debtorCreditorVO.setCustomerId(allocationVO.getCustomerId());
										debtorCreditorVO.setDebtorCreditorId(debtorCreditorId);
										debtorCreditorVO.setOccurrenceDt(paymentInfo.get("payment_dt").toString());
										  
										String summaryDt = ""; //한건인 경우 각 건의 상차지 하차지로 하여 적요에 작성 하나 한건이 아닌경우 월 단위의 적요를 작성 한다. 
										summaryDt ="운송료 ("+carInfoVO.getDeparture()+" - "+carInfoVO.getArrival()+")"; 
										debtorCreditorVO.setSummary(summaryDt);
										debtorCreditorVO.setTotalVal(paymentInfo.get("amount").toString().replaceAll(",", ""));
										debtorCreditorVO.setPublishYn("N");
										debtorCreditorVO.setType(BaseAppConstants.DEBTOR_CREDITOR_TYPE_C);
										//대변에 입력 하고 
										debtorCreditorService.insertDebtorCreditor(debtorCreditorVO);	
										paymentInfoVO.setDebtorCreditorId(debtorCreditorId);
									}
									
								}
								
								//기사 결제정보이고 
								if(paymentInfoVO.getPaymentDivision().equals("02")) {
									
									if(driver != null) {
										//현대캐피탈건 입력시 셀프기사님인 경우 공제율을 변경 한다.									
										if(allocationVO.getHcYn().equals("Y") && driver.get("car_assign_company") != null && driver.get("car_assign_company").toString().equals("S")){
											
											int deductionRate = 0;
											deductionRate = Integer.parseInt(driver.get("deduction_rate").toString());
											if(deductionRate > 10) {
		    			        				if(deductionRate == 15) {
		    			        					//지입기사이면 14% 아니면 15%
		    			        					if(driver.get("driver_kind") != null) {
		    			        						if(driver.get("driver_kind").equals("00") || driver.get("driver_kind").equals("03")) {
			    			        						deductionRate = 0;
			    			        					}else if(driver.get("driver_kind").equals("01")) {
			    			        						deductionRate = 4;
			    			        					}else if(driver.get("driver_kind").equals("02")) {
			    			        						deductionRate = 5;
			    			        					}else {
			    			        						deductionRate = 0;
			    			        					}
		    			        					}else {
		    			        						deductionRate = 0;
		    			        					}
		    			        					
		    			        				}else {
		    			        					deductionRate = deductionRate-10;
		    			        				}
		    			        			}else if(deductionRate == 10) {
		    			        				deductionRate = 0;
		    			        			} 
											
											int amount = Integer.parseInt(paymentInfo.get("amount").toString());
											int billForPayment = amount - (amount*deductionRate/100);
											paymentInfoVO.setBillForPayment(String.valueOf(billForPayment));
											paymentInfoVO.setDeductionRate(String.valueOf(deductionRate));
											
										}
										
									}
									
									if(paymentInfo.get("payment_kind").toString().equals("AT") || paymentInfo.get("payment_kind").toString().equals("CD")) {
										paymentInfoVO.setVatIncludeYn("N");
										paymentInfoVO.setVatExcludeYn("Y");
									}
									//paymentInfoVO.setAddColYn(paymentInfo.get("add_col_yn").toString()); //추가 컬럼 여부
								}
								
								if(paymentInfoVO.getPaymentDivision().equals("03")) {
									if(paymentInfo.get("payment_kind").toString().equals("AT") || paymentInfo.get("payment_kind").toString().equals("CD")) {
										paymentInfoVO.setVatIncludeYn("N");
										paymentInfoVO.setVatExcludeYn("Y");
									}
									//paymentInfoVO.setAddColYn(paymentInfo.get("add_col_yn").toString()); //추가 컬럼 여부
								}
								
								paymentInfoVO.setPaymentDt(paymentInfo.get("payment_dt").toString());
								paymentInfoVO.setPaymentKind(paymentInfo.get("payment_kind").toString());
								paymentInfoVO.setPaymentPartner(paymentInfo.get("payment_partner").toString());
								paymentInfoVO.setPaymentPartnerId(paymentInfo.get("payment_partner_id").toString());
								paymentInfoVO.setModId(userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자추가 
								paymentInfoVO.setLogType("I");
								paymentInfoService.insertPaymentInfo(paymentInfoVO);
								paymentInfoService.insertPaymentInfoLogByVO(paymentInfoVO);//로그기록
							}
						}
					}
					
					if(allocationVO.getAllocationStatus().equals("N") || allocationVO.getAllocationStatus().equals("A")) {
							Map<String, Object> payment = new HashMap<String, Object>();
							payment.put("allocationId", allocationVO.getAllocationId());
							List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(payment);
							String paymentKind = "";
							String billingDivision = "";
							if(paymentInfoList != null && paymentInfoList.size() > 0){
								for(int k = 0; k < paymentInfoList.size(); k++) {
									Map<String, Object> paymentInfo = paymentInfoList.get(k);
									if(paymentInfo.get("payment_division").toString().equals("01")) {		//매출처 
										if(paymentInfo.get("payment_kind") != null) {
											if(paymentInfo.get("payment_kind").toString().equals("DD")) {		//결제 방법이 기사 수금이면
												paymentKind = "DD";
											}else {
												paymentKind = paymentInfo.get("payment_kind").toString();
											}
										}
										if(paymentInfo.get("billing_division") != null) {
											billingDivision = paymentInfo.get("billing_division").toString();
										}
									}
								}
							}
							Map<String, Object> alarmTalkMap = new HashMap<String, Object>();
							//알림톡 발송 구분		0 : 예약 완료,1:기사배정,2:상차완료,3:하차완료,4:인계완료,5:탁송완료,6탁송취소,7:배차정보(기사님에게),8:배차승인(배차담당직원)
							if(allocationVO.getAllocationStatus().equals("N")) {
								alarmTalkMap.put("gubun", "0");
							}else if(allocationVO.getAllocationStatus().equals("A")) {
								alarmTalkMap.put("gubun", "1");	
							}
							alarmTalkMap.put("departure", carInfo.get("departure").toString());
							alarmTalkMap.put("arrival", carInfo.get("arrival").toString());
							if(driver != null) {
								alarmTalkMap.put("driver_name", driver.get("driver_name"));
								alarmTalkMap.put("driver_id", driver.get("driver_id"));
								alarmTalkMap.put("phone_num", driver.get("phone_num").toString());
							}
							alarmTalkMap.put("car_kind", carInfo.get("car_kind").toString());
							alarmTalkMap.put("car_id_num", carInfo.get("car_id_num").toString());
							alarmTalkMap.put("departure_dt", carInfo.get("departure_dt").toString());
							alarmTalkMap.put("customer_id", customer.get("customer_id").toString());
							alarmTalkMap.put("customer_name", customer.get("customer_name").toString());
							alarmTalkMap.put("allocation_id", allocationId);
							alarmTalkMap.put("departure_phone", carInfo.get("departure_phone").toString());
							alarmTalkMap.put("arrival_phone", carInfo.get("arrival_phone").toString());
							
							//티케이물류 문자 발송시 문자 내용에 고객명 추가.
							String personInChargeStr = carInfo.get("arrival_person_in_charge") != null && !carInfo.get("arrival_person_in_charge").toString().equals("") ? carInfo.get("arrival_person_in_charge").toString() : "";
							personInChargeStr = personInChargeStr.replaceAll(" ", "");
							personInChargeStr = personInChargeStr.trim();
							
							String[] personInCharge = personInChargeStr.split(";");  
							alarmTalkMap.put("person_in_charge",personInCharge[0]);
							
							if(carInfo.get("departure_phone") != null && !carInfo.get("departure_phone").toString().equals("")) {
								if(WebUtils.isNumber(carInfo.get("departure_phone").toString().replaceAll("-", "").replaceAll(" ", ""))) {
									alarmTalkMap.put("customer_phone", allocationVO.getChargePhone()+";"+carInfo.get("departure_phone").toString().replaceAll("-", "").replaceAll(" ", ""));
								}else {
									alarmTalkMap.put("customer_phone", allocationVO.getChargePhone());	
								}
							}else {
								alarmTalkMap.put("customer_phone", allocationVO.getChargePhone());	
							}
							
							alarmTalkMap.put("alarm_talk_yn", customer.get("alarm_talk_yn").toString());
							alarmTalkMap.put("send_account_info_yn", sendAccountInfoYn);
							alarmTalkMap.put("payment_kind",paymentKind);
							alarmTalkMap.put("customer_kind",customer.get("customer_kind").toString());
							alarmTalkMap.put("billing_division",billingDivision);
							alarmTalkMap.put("alarmOrSmsYn", customer.get("alarm_or_sms_yn").toString());
							if(customer.get("alarm_talk_yn").toString().equals("Y") && allocationVO.getChargePhone() != null && !allocationVO.getChargePhone().equals("")) {
								alarmTalkService.alarmTalkSends(alarmTalkMap);	
							}
							
							if(customer.get("sms_yn").toString().equals("Y")) {
								sendSmsService.smsSend(alarmTalkMap);
							}
							
							
					}
					
				}
			}
			
			WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/allocation/combination.do?forOpen=N");

		}catch(Exception e){
			
			WebUtils.messageAndRedirectUrl(mav, "등록하는데 실패 했습니다. 다시 등록 해 주세요.", "/allocation/combination.do?forOpen=N");
			e.printStackTrace();
			
		}
		

		return mav;
	}
	
	
	@RequestMapping(value = "/update-order", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updateListOrder(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			String oldListOrder = request.getParameter("oldListOrder").toString();
			String newlistOrder = request.getParameter("newlistOrder").toString();
			String allocationId = request.getParameter("allocationId").toString();
			Map<String,Object> map = new HashMap<String, Object>();
			String[] oldListOrderArr = oldListOrder.split(",");
			String[] newlistOrderArr = newlistOrder.split(",");
			String[] allocationIdArr = allocationId.split(",");
			int arrLen = oldListOrderArr.length;
			List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
			if(oldListOrderArr.length != newlistOrderArr.length){
				result.setResultCode("0002");
			}else{
				boolean changed = false;
				for(int i = 0; i < arrLen; i++){
					String listOrder = newlistOrderArr[i];
					map.put("listOrder", newlistOrderArr[i]);
					Map<String, Object> allocationMap = allocationService.selectAllocationByListOrder(map);
					for(int j = 0; j < arrLen; j++){
						if(listOrder.equals(oldListOrderArr[j].toString())){
							if(!allocationIdArr[j].toString().equals(allocationMap.get("allocation_id"))){
								changed = true;
								break;
							}
						}
					}
					list.add(allocationMap);
				}
				
				if(!changed){
					if(list.size() != arrLen){
						result.setResultCode("0003");
					}
					map.clear();
					for(int i = 0; i < list.size(); i++){
						Map<String, Object> allocationMap = list.get(i);
						map.put("oldlistOrder", oldListOrderArr[i]);
						map.put("allocationId",allocationMap.get("allocation_id").toString());
						map.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
						map.put("logType","U");
						allocationService.updateAllocationListOrder(map);
						allocationService.insertAllocationLogByMap(map);
					}
	
				}else{
					result.setResultCode("0004");
				}
			}
		
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	
	@RequestMapping(value = "/update-allocation-sub", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updateAllocationSub(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			Map<String, Object> map = new HashMap<String, Object>();		
				map.put("inputDt", request.getParameter("inputDt").toString());
				map.put("carrierType", request.getParameter("carrierType").toString());
				map.put("distanceType", request.getParameter("distanceType").toString());
				map.put("departureDt", request.getParameter("departureDt").toString());
				map.put("departureTime", request.getParameter("departureTime").toString());
				map.put("customerName", request.getParameter("customerName").toString());
				map.put("carKind", request.getParameter("carKind").toString());
				map.put("carIdNum", request.getParameter("carIdNum").toString());
				map.put("carNum", request.getParameter("carNum").toString());
				map.put("departure", request.getParameter("departure").toString());
				map.put("arrival", request.getParameter("arrival").toString());
				map.put("driverName", request.getParameter("driverName").toString());
				map.put("carCnt", request.getParameter("carCnt").toString());
				map.put("amount", request.getParameter("amount").toString());
				map.put("paymentKind", request.getParameter("paymentKind").toString());
				map.put("allocationId", request.getParameter("allocationId").toString());
				map.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():"");
				allocationService.updateAllocationByMap(map);
				map.put("logType", "U");
				allocationService.insertAllocationLogByMap(map); //로그기록
	
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	@RequestMapping(value = "/selectAllocationInfo", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi selectAllocationInfo(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			Map<String, Object> map = new HashMap<String, Object>();		
				map.put("allocationId", request.getParameter("allocationId").toString());
				Map<String, Object> allocationMap = allocationService.selectAllocation(map);
				if(allocationMap != null && allocationMap.get("charge_id") != null && !allocationMap.get("charge_id").toString().equals("")) {
					Map<String, Object> personInChargeMap = new HashMap<String, Object>();
					personInChargeMap.put("personInChargeId", allocationMap.get("charge_id").toString());
					String department = personInChargeService.selectPersonInChargeByPersonInChargeId(personInChargeMap).get("department").toString();
					allocationMap.put("department", department);
				}
				
				Map<String, Object> carInfo = carInfoService.selectCarInfo(map);
				List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(map);
				
				result.setResultData(allocationMap);
				result.setResultDataSub(carInfo);
				result.setResultDataThird(paymentInfoList);
				
				
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	@RequestMapping(value = "/updateAllocationStatus", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updateAllocationStatus(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			String allocationStatus = request.getParameter("allocationStatus").toString();
			String allocationId = request.getParameter("allocationId").toString();
			String[] allocationIdArr = allocationId.split(",");
			String cancelReason = request.getParameter("cancelReason") != null && !request.getParameter("cancelReason").toString().equals("") ? request.getParameter("cancelReason").toString():"";
			
			for(int i = 0; i < allocationIdArr.length; i++){
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("allocationId", allocationIdArr[i]);
				Map<String, Object> allocationMap = allocationService.selectAllocation(map);
				
				if(!allocationMap.get("batch_status").toString().equals("P")) {			//픽업건이 아닌경우는 기존과 동일하게 동작 하면 된다.
					map.put("batchStatusId", allocationMap.get("batch_status_id"));
					List<Map<String, Object>> allocationList = allocationService.selectAllocationList(map);
					
					for(int j = 0; j < allocationList.size(); j++) {
						Map<String, Object> allocation = allocationList.get(j);
						
						Map<String, Object> updateMap = new HashMap<String, Object>();
						updateMap.put("allocationStatus", allocationStatus);
						updateMap.put("allocationId", allocationList.get(j).get("allocation_id").toString());
						updateMap.put("cancelReason", cancelReason);
						updateMap.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
						
						//탁송 상태를 업데이트 하기 전에 billing_status를 확인 하여 처리 한다.
						if(allocationStatus.equals("C") || allocationStatus.equals("X")) {
							//세금계산서 발행 또는 발행 요청인 경우에만...
							if(allocationList.get(j).get("billing_status") != null && !allocationList.get(j).get("billing_status").toString().equals("N") && !allocationList.get(j).get("billing_status").toString().equals("D")) {
								throw new Exception();
							}
						}
						
						allocationService.updateAllocationStatus(updateMap);
						updateMap.put("logType", "U");
						allocationService.insertAllocationLogByMap(updateMap); //로그기록
						
						if(allocationStatus.equals("F")) {
							
							Map<String,Object> customerMap = new HashMap<String, Object>();
							customerMap.put("customerId",allocationMap.get("customer_id").toString());
							Map<String, Object> customer = customerService.selectCustomer(customerMap);
					
							Map<String, Object> alarmTalkMap = new HashMap<String, Object>();
							//외부기사일때 
							if(customerMap != null && customerMap.get("customerId") != null && !customerMap.get("customerId").equals("")) {	
								
								
							
							if(customer.get("sms_yn").equals("Y")) {
								alarmTalkMap.put("gubun", "5");
								String personInChargeStr = allocationMap.get("arrival_person_in_charge") != null && !allocationMap.get("arrival_person_in_charge").toString().equals("") ? allocationMap.get("arrival_person_in_charge").toString() : "";
								personInChargeStr = personInChargeStr.replaceAll(" ", "");
								personInChargeStr = personInChargeStr.trim();
								
								String[] personInCharge = personInChargeStr.split(";");  
								alarmTalkMap.put("person_in_charge",personInCharge[0]);
							
								alarmTalkMap.put("car_kind", allocation.get("car_kind").toString());
								alarmTalkMap.put("customer_id", allocation.get("customer_id").toString());
								alarmTalkMap.put("arrival", allocationMap.get("arrival").toString());
								alarmTalkMap.put("departure", allocationMap.get("departure").toString());
								alarmTalkMap.put("arrival_phone", allocationMap.get("arrival_phone").toString());
								alarmTalkMap.put("allocation_id", allocationMap.get("allocation_id").toString());
								alarmTalkMap.put("alarmOrSmsYn", customer.get("alarm_or_sms_yn").toString());
								
								sendSmsService.smsSend(alarmTalkMap);
								
								}
							}
						
						}
						
						if(allocationStatus.equals("C") || allocationStatus.equals("X")) {
							
							//세금계산서 발행 또는 발행 요청인 경우에만...
							//if(allocationList.get(j).get("billing_status") != null && !allocationList.get(j).get("billing_status").toString().equals("N") && !allocationList.get(j).get("billing_status").toString().equals("D")) {
							//	throw new Exception();
							//}
							
							//취소건에 대해 차변 또는 대변에 입력된 건이 있으면 해당하는 차변과 대변을 삭제 한다.
							if(allocationList.get(j).get("debtor_creditor_id") != null && !allocationList.get(j).get("debtor_creditor_id").toString().equals("")) {
								Map<String, Object> deleteMap = new HashMap<String, Object>();
								deleteMap.put("debtorCreditorId", allocationList.get(j).get("debtor_creditor_id").toString());
								List<Map<String, Object>> selectList = paymentInfoService.selectPaymentInfoListByDebtorCreditorId(deleteMap);
								if(selectList.size() <= 1 ) {
									debtorCreditorService.deleteDebtorCreditor(deleteMap);
								}
							}
							
							Map<String, Object> selectMap = new HashMap<String, Object>();
							selectMap.put("allocationId", allocationList.get(j).get("allocation_id").toString());
							List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(selectMap);
							
							for(int m = 0; m < paymentInfoList.size(); m++) {
								Map<String, Object> paymentInfo = paymentInfoList.get(m);
								if(paymentInfo.get("debtor_creditor_id") != null && !paymentInfo.get("debtor_creditor_id").toString().equals("")) {
									Map<String, Object> deleteMap = new HashMap<String, Object>();
									deleteMap.put("debtorCreditorId", paymentInfo.get("debtor_creditor_id").toString());
									List<Map<String, Object>> selectList = paymentInfoService.selectPaymentInfoListByDebtorCreditorId(deleteMap);
									if(selectList.size() <= 1 ) {
										debtorCreditorService.deleteDebtorCreditor(deleteMap);
									}
								}	
							}
								
							//취소된건에 대해 billing_status와 debtor_creditor_id, bill_publish_request_id를 변경 해 준다.
							Map<String, Object> carInfoUpdateMap = new HashMap<String,Object>();
							carInfoUpdateMap.put("allocationId", allocationList.get(j).get("allocation_id").toString());
							carInfoUpdateMap.put("billing_status", "N");
							carInfoUpdateMap.put("debtor_creditor_id", "");
							carInfoUpdateMap.put("bill_publish_request_id", "");
							carInfoUpdateMap.put("decide_status", "N");
							carInfoUpdateMap.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
							carInfoUpdateMap.put("logType", "U");
							carInfoService.updateCarInfoForCancel(carInfoUpdateMap);
							carInfoService.insertCarInfoLogByMap(carInfoUpdateMap); //로그기록
							
							
						}
						
					}	
				}else {				// 픽업건인 경우는 취소는 둘다 취소 되도록 하되 완료인 경우는 각각의 건
					Map<String, Object> updateMap = new HashMap<String, Object>();
					updateMap.put("allocationStatus", allocationStatus);
					updateMap.put("allocationId", allocationMap.get("allocation_id").toString());
					updateMap.put("cancelReason", cancelReason);
					updateMap.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
					allocationService.updateAllocationStatus(updateMap);
					updateMap.put("logType", "U");
					allocationService.insertAllocationLogByMap(updateMap); //로그기록
					
					if(allocationStatus.equals("F")) {
						//List<Map<String, Object>> allocationList = allocationService.selectAllocationList(map);
						/* Map<String, Object> allocation = allocationList.get(i); */
						Map<String,Object> customerMap = new HashMap<String, Object>();
						customerMap.put("customerId",allocationMap.get("customer_id").toString());
						Map<String, Object> customer = customerService.selectCustomer(customerMap);
				
						Map<String, Object> alarmTalkMap = new HashMap<String, Object>();
						//외부기사일때 
						if(customerMap != null && customerMap.get("customerId") != null && !customerMap.get("customerId").equals("")) {	
						
						
						if(customer.get("sms_yn").equals("Y")) {
							alarmTalkMap.put("gubun", "5");
							String personInChargeStr = allocationMap.get("arrival_person_in_charge") != null && !allocationMap.get("arrival_person_in_charge").toString().equals("") ? allocationMap.get("arrival_person_in_charge").toString() : "";
							personInChargeStr = personInChargeStr.replaceAll(" ", "");
							personInChargeStr = personInChargeStr.trim();
							
							String[] personInCharge = personInChargeStr.split(";");  
							alarmTalkMap.put("person_in_charge",personInCharge[0]);
							
							
							alarmTalkMap.put("car_kind", allocationMap.get("car_kind").toString());
							alarmTalkMap.put("customer_id", allocationMap.get("customer_id").toString());
							alarmTalkMap.put("arrival", allocationMap.get("arrival").toString());
							alarmTalkMap.put("arrival_phone", allocationMap.get("arrival_phone").toString());
							alarmTalkMap.put("allocation_id", allocationMap.get("allocation_id").toString());
							alarmTalkMap.put("alarmOrSmsYn", customer.get("alarm_or_sms_yn").toString());
							
							
							
							sendSmsService.smsSend(alarmTalkMap);
							
							}
						}
						
					}
					
					
					
					
					
					if(allocationStatus.equals("C") || allocationStatus.equals("X")) {
							
						if(allocationMap.get("billing_status") != null && !allocationMap.get("billing_status").toString().equals("N")) {
							throw new Exception();
						}
						
						//취소건에 대해 차변 또는 대변에 입력된 건이 있으면 해당하는 차변과 대변을 삭제 한다.
						if(allocationMap.get("debtor_creditor_id") != null && !allocationMap.get("debtor_creditor_id").toString().equals("")) {
							Map<String, Object> deleteMap = new HashMap<String, Object>();
							deleteMap.put("debtorCreditorId", allocationMap.get("debtor_creditor_id").toString());
							List<Map<String, Object>> selectList = paymentInfoService.selectPaymentInfoListByDebtorCreditorId(deleteMap);
							if(selectList.size() <= 1 ) {
								debtorCreditorService.deleteDebtorCreditor(deleteMap);
							}
						}
						
						Map<String, Object> selectMap = new HashMap<String, Object>();
						selectMap.put("allocationId", allocationMap.get("allocation_id").toString());
						List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(selectMap);
						
						for(int m = 0; m < paymentInfoList.size(); m++) {
							Map<String, Object> paymentInfo = paymentInfoList.get(m);
							if(paymentInfo.get("debtor_creditor_id") != null && !paymentInfo.get("debtor_creditor_id").toString().equals("")) {
								Map<String, Object> deleteMap = new HashMap<String, Object>();
								deleteMap.put("debtorCreditorId", paymentInfo.get("debtor_creditor_id").toString());
								List<Map<String, Object>> selectList = paymentInfoService.selectPaymentInfoListByDebtorCreditorId(deleteMap);
								if(selectList.size() <= 1 ) {
									debtorCreditorService.deleteDebtorCreditor(deleteMap);
								}
							}	
						}
						
						//취소된건에 대해 billing_status와 debtor_creditor_id, bill_publish_request_id를 변경 해 준다.
						Map<String, Object> carInfoUpdateMap = new HashMap<String,Object>();
						carInfoUpdateMap.put("allocationId", allocationMap.get("allocation_id").toString());
						carInfoUpdateMap.put("billing_status", "N");
						carInfoUpdateMap.put("debtor_creditor_id", "");
						carInfoUpdateMap.put("bill_publish_request_id", "");
						carInfoUpdateMap.put("decide_status", "N");
						carInfoService.updateCarInfoForCancel(carInfoUpdateMap);
							
					}
					
				}
				
				
				//어플리케이션에서 등록된 배차건의 경우 취소요청 되는 경우 고객에게 알림톡을 발송 한다.
				if(allocationMap.get("reg_type")!= null && allocationMap.get("reg_type").equals("A") && allocationStatus.equals("X")) {
					Map<String,Object> customerMap = new HashMap<String, Object>();
					customerMap.put("customerId",allocationMap.get("customer_id").toString());
					Map<String, Object> customer = customerService.selectCustomer(customerMap);
					
					map.put("gubun", "13");
					map.put("customerName", allocationMap.get("customer_name").toString());
					map.put("customer_phone", allocationMap.get("charge_phone").toString());
					map.put("departureAddr",allocationMap.get("departure_addr").toString());
					map.put("arrivalAddr",allocationMap.get("arrival_addr").toString());
					map.put("carKind",allocationMap.get("car_kind").toString());
					map.put("carIdNum",allocationMap.get("car_id_num").toString());
					map.put("carNum",allocationMap.get("car_num").toString());
					map.put("alarmOrSmsYn", customer.get("alarm_or_sms_yn").toString());
				
					alarmTalkService.alarmTalkSends(map);
						
				}
				
				
			}
			
		}catch(Exception e){
 			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	@RequestMapping(value = "/update-allocationAllData", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView updateAllocationAllData(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute AllocationVO allocationVO) throws Exception {
		
		try{
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("duplicateChk", "true");
			
			String returnPage = "";
			if(request.getParameter("returnPage") != null && !request.getParameter("returnPage").toString().equals("")){
				returnPage = request.getParameter("returnPage").toString();
			}else{
				returnPage = "combination.do";
			}
			
			map.put("allocationId", allocationVO.getAllocationId());
			map.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
			map.put("logType", "D");
			
			carInfoService.deleteCarInfo(map);
			carInfoService.insertCarInfoLogByMap(map); // 삭제 로그 기록
			
			paymentInfoService.deletePaymentInfo(map);
			paymentInfoService.insertPaymentInfoLogByMap(map); // 삭제 로그 기록
			
			String carInfoVal = request.getParameter("carInfoVal").toString();
			String paymentInfoVal = request.getParameter("paymentInfoVal").toString();
			
			if(!carInfoVal.equals("")){
				JSONParser jsonParser = new JSONParser();
				JSONObject jsonObject = (JSONObject) jsonParser.parse(carInfoVal);
				JSONArray jsonArray = (JSONArray) jsonObject.get("carInfoList");	
				JSONObject carInfoF = (JSONObject)jsonArray.get(0);
				//allocationVO.setCarrierType(carInfoF.get("carrier_type").toString());
				
				for(int i = 0; i < jsonArray.size(); i++){
					JSONObject carInfo = (JSONObject)jsonArray.get(i);
					CarInfoVO carInfoVO = new CarInfoVO();
					carInfoVO.setAllocationId(allocationVO.getAllocationId());
					carInfoVO.setCarrierType(carInfo.get("carrier_type").toString());
					carInfoVO.setDistanceType(carInfo.get("distance_type").toString());
					carInfoVO.setDepartureDt(carInfo.get("departure_dt").toString());
					carInfoVO.setDepartureTime(carInfo.get("departure_time").toString());
					carInfoVO.setDriverId(carInfo.get("driver_id").toString());
					map.put("driverId", carInfo.get("driver_id").toString());
					Map<String, Object> driver = driverService.selectDriver(map);
					if(driver != null){
						carInfoVO.setDriverName(driver.get("driver_name").toString());	
					}
					carInfoVO.setCarKind(carInfo.get("car_kind").toString());
					carInfoVO.setCarIdNum(carInfo.get("car_id_num").toString());
					carInfoVO.setCarNum(carInfo.get("car_num").toString());
					carInfoVO.setContractNum(carInfo.get("contract_num").toString());
					carInfoVO.setTowDistance(carInfo.get("tow_distance").toString());
					carInfoVO.setAccidentYn(carInfo.get("accident_yn").toString());
					carInfoVO.setEtc(carInfo.get("etc").toString());
					carInfoVO.setDeparture(carInfo.get("departure").toString());
					carInfoVO.setDepartureAddr(carInfo.get("departure_addr").toString());
					carInfoVO.setDeparturePersonInCharge(carInfo.get("departure_person_in_charge").toString());
					carInfoVO.setDeparturePhone(carInfo.get("departure_phone").toString());
					carInfoVO.setArrival(carInfo.get("arrival").toString());
					carInfoVO.setArrivalAddr(carInfo.get("arrival_addr").toString());
					carInfoVO.setArrivalPersonInCharge(carInfo.get("arrival_person_in_charge").toString());
					carInfoVO.setArrivalPhone(carInfo.get("arrival_phone").toString());
					carInfoVO.setModId(userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자추가
					carInfoVO.setLogType("I");
					carInfoService.insertCarInfo(carInfoVO);
					carInfoService.insertCarInfoLogByVO(carInfoVO); //로그기록
				}
			}
			
			if(!paymentInfoVal.equals("")){
				JSONParser jsonParser = new JSONParser();
				JSONObject jsonObject = (JSONObject) jsonParser.parse(paymentInfoVal);
				JSONArray jsonArray = (JSONArray) jsonObject.get("paymentInfoList");	
				
				for(int i = 0; i < jsonArray.size(); i++){
					JSONObject paymentInfo = (JSONObject)jsonArray.get(i);
					PaymentInfoVO paymentInfoVO = new PaymentInfoVO();
					
					paymentInfoVO.setAllocationId(allocationVO.getAllocationId());
					paymentInfoVO.setAmount(paymentInfo.get("amount").toString());
					paymentInfoVO.setBillForPayment(paymentInfo.get("bill_for_payment").toString());
					paymentInfoVO.setBillingDivision(paymentInfo.get("billing_division").toString());
					paymentInfoVO.setBillingDt(paymentInfo.get("billing_dt").toString());
					paymentInfoVO.setDeductionRate(paymentInfo.get("deduction_rate").toString());
					paymentInfoVO.setEtc(paymentInfo.get("etc").toString());
					paymentInfoVO.setPayment(paymentInfo.get("payment") != null && !paymentInfo.get("payment").toString().equals("") ? paymentInfo.get("payment").toString() : "N");
					paymentInfoVO.setPaymentDivision(paymentInfo.get("payment_division").toString());
					paymentInfoVO.setPaymentDt(paymentInfo.get("payment_dt").toString());
					paymentInfoVO.setPaymentKind(paymentInfo.get("payment_kind").toString());
					paymentInfoVO.setPaymentPartner(paymentInfo.get("payment_partner").toString());
					paymentInfoVO.setPaymentPartnerId(paymentInfo.get("payment_partner_id").toString());
					paymentInfoVO.setModId(userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자추가
					paymentInfoVO.setLogType("I");
					paymentInfoService.insertPaymentInfo(paymentInfoVO);
					paymentInfoService.insertPaymentInfoLogByVO(paymentInfoVO); //로그기록
					
				}
			}
			
			allocationService.updateAllocation(allocationVO);
			allocationVO.setLogType("U");
			allocationService.insertAllocationLogByVO(allocationVO); //로그기록
			WebUtils.messageAndRedirectUrl(mav, "수정되었습니다.", "/allocation/"+returnPage);

		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	

	@RequestMapping(value = "/allocation-register", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView allocationRegister(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			String empCompanyId = userMap.get("company_id").toString();
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			String today = "";			
			today = WebUtils.getNow("yyyy-MM-dd (E)");
			paramMap.put("today", today);
			
			Map<String, Object> map = new HashMap<String, Object>();
			List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
			List<Map<String, Object>> driverList = driverService.selectWorkDriverList(map);
			List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
			List<Map<String, Object>> allocationDivisionList = allocationDivisionService.selectAllocationDivisionInfoList();
			List<Map<String, Object>> runDivisionList = runDivisionService.selectRunDivisionInfoList();
			List<Map<String, Object>> paymentDivisionList = paymentDivisionService.selectPaymentDivisionInfoList();
			List<Map<String, Object>> payDivisionList = payDivisionService.selectPayDivisionInfoList();
			
			
			if(!userMap.get("emp_id").equals("hk0001")) {
			
				if(!empCompanyId.equals(session.getAttribute("companyId").toString())) {
				
				StringBuffer sb1 = new StringBuffer();
				 sb1.append("<script type='text/javascript'>");
				 sb1.append("alert('소속이 다르므로 등록이 불가능합니다.');");
				 sb1.append("window.history.go(-1);");
				 sb1.append("self.close();");
				 sb1.append("</script>");

				 response.setContentType("text/html; charset=UTF-8");
				 PrintWriter out1 = response.getWriter();
				 out1.println(sb1);
				 out1.flush();
				
				}
			}
			
			int totalCount = customerService.selectCustomerListCount(paramMap);
			map.put("startRownum", 0);
			map.put("numOfRows", totalCount);
			List<Map<String, Object>> customerList = customerService.selectCustomerListIncludeDriver(map);
			
			mav.addObject("billingDivisionList",  billingDivisionList);
			mav.addObject("allocationDivisionList",  allocationDivisionList);
			mav.addObject("runDivisionList",  runDivisionList);
			mav.addObject("paymentDivisionList",  paymentDivisionList);
			mav.addObject("payDivisionList",  payDivisionList);
			mav.addObject("userMap",  userMap);
			mav.addObject("driverList",  driverList);
			mav.addObject("companyList",  companyList);
			mav.addObject("customerList",  customerList);
			
			mav.addObject("paramMap", paramMap);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	
	@RequestMapping(value = "/allocation-view", method = RequestMethod.GET)
	public ModelAndView view(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute AllocationVO allocationVO) throws Exception{
		
		
		try{
			
			HttpSession session = request.getSession(); 
			//Map userMap = (Map)session.getAttribute("user");
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			Map<String, Object> map = new HashMap<String, Object>();
			List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
			List<Map<String, Object>> driverList = driverService.selectDriverList(map);
			List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
			List<Map<String, Object>> allocationDivisionList = allocationDivisionService.selectAllocationDivisionInfoList();
			List<Map<String, Object>> runDivisionList = runDivisionService.selectRunDivisionInfoList();
			List<Map<String, Object>> paymentDivisionList = paymentDivisionService.selectPaymentDivisionInfoList();
			List<Map<String, Object>> payDivisionList = payDivisionService.selectPayDivisionInfoList();
			Map<String,Object> customerAppCallMap = allocationService.selectCustomerAppCall(paramMap);
			
			int totalCount = customerService.selectCustomerListCount(paramMap);
			map.put("startRownum", 0);
			map.put("numOfRows", totalCount);
			List<Map<String, Object>> customerList = customerService.selectCustomerList(map);
		

			//String referer = request.getHeader("referer");
			//logger.info("referer="+referer);			
			map.put("allocationId", request.getParameter("allocationId").toString());
			
			Map<String, Object> allocationMap = allocationService.selectAllocation(map);
			map.put("batchStatusId", allocationMap.get("batch_status_id"));
			List<Map<String, Object>> allocationList = allocationService.selectAllocationList(map);
			
			
			  Map<String,Object> map1= new HashMap<String, Object>();
			  
			  map1.put("customerId", allocationList.get(0).get("customer_id").toString());
			  
			  List<Map<String, Object>> chargeList = customerService.selectChargeList(map1);
			  
			  
			int income = 0;
			for(int i = 0; i < allocationList.size(); i++) {
				Map<String, Object> allocation = allocationList.get(i); 
				map.put("allocationId", allocation.get("allocation_id").toString());
				Map<String, Object> carInfo = carInfoService.selectCarInfo(map);
				allocation.put("carInfo", carInfo);
				Map<String, Object> checkDetailInfo = checkDetailService.selectCheckDetail(map);
				allocation.put("checkDetailInfo", checkDetailInfo);
				List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(map);
				for(int j = 0; j < paymentInfoList.size(); j++) {
					Map<String, Object> paymentInfo = paymentInfoList.get(j);
					if(paymentInfo.get("payment_division").toString().equals("01")) {
						String calc = "";
						calc = paymentInfo.get("amount") != null && !paymentInfo.get("amount").toString().equals("") ? paymentInfo.get("amount").toString().replaceAll(",", "") : "0"; 
						income += Integer.parseInt(calc);
					}else if(paymentInfo.get("payment_division").toString().equals("02")) {
						String calc = "";
						calc = paymentInfo.get("amount") != null && !paymentInfo.get("amount").toString().equals("") ? paymentInfo.get("amount").toString().replaceAll(",", "") : "0";
						income -= Integer.parseInt(calc);
					}else if(paymentInfo.get("payment_division").toString().equals("03")) {
						String calc = "";
						calc = paymentInfo.get("amount") != null && !paymentInfo.get("amount").toString().equals("") ? paymentInfo.get("amount").toString().replaceAll(",", "") : "0";
						income -= Integer.parseInt(calc);
					}
				}
				allocation.put("paymentInfoList", paymentInfoList);
				//map.put("allocationStatus", allocation.get("allocation_status").toString());			//현재 상태의 사진을 가져 오는 경우가 무의미 하므로 사진을 전부 가져온다.
				map.put("allocationStatus", "F");
				List<Map<String, Object>> allocationFileList = allocationFileService.selectAllocationFileList(map);
				map.put("allocationStatus", allocation.get("allocation_status").toString());
				allocation.put("allocationFileList", allocationFileList);
				allocationList.set(i,allocation);
			}
			
				DecimalFormat formatter = new DecimalFormat("###,###");
				allocationMap.put("income", formatter.format(income));
				mav.addObject("allocationMap",  allocationMap);
				mav.addObject("allocationList",  allocationList);
				mav.addObject("billingDivisionList",  billingDivisionList);
				mav.addObject("allocationDivisionList",  allocationDivisionList);
				mav.addObject("runDivisionList",  runDivisionList);
				mav.addObject("paymentDivisionList",  paymentDivisionList);
				mav.addObject("payDivisionList",  payDivisionList);
				mav.addObject("driverList",  driverList);
				mav.addObject("companyList",  companyList);
				mav.addObject("paramMap", paramMap);
				mav.addObject("customerList", customerList);
				mav.addObject("allocationStatus", allocationMap.get("allocation_status").toString());
				mav.addObject("chargeList",chargeList);
				mav.addObject("customerAppCallMap", customerAppCallMap);
				
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return mav;
	}
	
	
	
	@RequestMapping(value = "/allocation-mod", method = RequestMethod.GET)
	public ModelAndView mod(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute AllocationVO allocationVO) throws Exception{
		
		
		try{
			
			HttpSession session = request.getSession(); 
			//String companyId = session.getAttribute("companyId").toString();
			Map userMap = (Map)session.getAttribute("user");
			//String empCompanyId = userMap.get("company_id").toString();
			String empCompanyId = userMap.get("company_id") == null ? "" : userMap.get("company_id").toString(); 
			
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			Map<String, Object> map = new HashMap<String, Object>();
			List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
			List<Map<String, Object>> driverList = driverService.selectWorkDriverList(map);
			List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
			List<Map<String, Object>> allocationDivisionList = allocationDivisionService.selectAllocationDivisionInfoList();
			List<Map<String, Object>> runDivisionList = runDivisionService.selectRunDivisionInfoList();
			List<Map<String, Object>> paymentDivisionList = paymentDivisionService.selectPaymentDivisionInfoList();
			List<Map<String, Object>> payDivisionList = payDivisionService.selectPayDivisionInfoList();
			Map<String,Object> customerAppCallMap = allocationService.selectCustomerAppCall(paramMap);
			
			
			int totalCount = customerService.selectCustomerListCount(paramMap);
			map.put("startRownum", 0);
			map.put("numOfRows", totalCount);
			List<Map<String, Object>> customerList = customerService.selectCustomerListIncludeDriver(map);
			
			//String referer = request.getHeader("referer");
			//logger.info("referer="+referer);			
			map.put("allocationId", request.getParameter("allocationId").toString());
			
			Map<String, Object> allocationMap = allocationService.selectAllocation(map);
			
			String billingStatus = allocationMap.get("billing_status").toString();
			
			
			//세금계산서 발행 요청 아이디가 같은 배차건의 갯수
			int eqBillingCnt = 0;
			int compareCnt = 0;
			//세금계산서 발행 요청 아이디
			String billPublishRequestId = "";
			
			if(billingStatus.equals("Y")) {
				
				billPublishRequestId =  allocationMap.get("bill_publish_request_id").toString();
				Map<String, Object> searchMap = new HashMap<String, Object>();
				searchMap.put("billPublishRequestId", billPublishRequestId);
				eqBillingCnt = carInfoService.selectEqBillingCnt(searchMap);
				
			}
			
			//System.out.println("eqBillingCnt="+eqBillingCnt);
			
			
			
	/*		if(!empCompanyId.equals(allocationMap.get("company_id").toString())) {
				
				StringBuffer sb1 = new StringBuffer();
				 sb1.append("<script type='text/javascript'>");
				 sb1.append("alert('등록된 배차건의 소속이 다르므로 수정이 불가능합니다.');");
				 sb1.append("window.history.go(-1);");
				 sb1.append("self.close();");
				 sb1.append("</script>");

				 response.setContentType("text/html; charset=UTF-8");
				 PrintWriter out1 = response.getWriter();
				 out1.println(sb1);
				 out1.flush();
				
			}
			*/
			
			
			//List<Map<String, Object>> paymentInfoListForMod = paymentInfoService.selectPaymentInfoForMod(map);
			
			//2020-03-13 결제가 완료된 배차건에 대해서 수정 페이지로 이동 할 수 없도록 한다.
	//		if(paymentInfoListForMod.size() > 0) {
				
			/*
				StringBuffer sb1 = new StringBuffer();
				 sb1.append("<script type='text/javascript'>");
				 sb1.append("alert('결제가 완료된 배차건은 수정 할 수 없습니다.');");
				 sb1.append("window.history.go(-1);");
				 sb1.append("self.close();");
				 sb1.append("</script>");

				 response.setContentType("text/html; charset=UTF-8");
				 PrintWriter out1 = response.getWriter();
				 out1.println(sb1);
				 out1.flush();
				 
				*/
				
		//	}else {
			
				map.put("batchStatusId", allocationMap.get("batch_status_id"));
				List<Map<String, Object>> allocationList = allocationService.selectAllocationList(map);
				int income = 0;
				for(int i = 0; i < allocationList.size(); i++) {
					Map<String, Object> allocation = allocationList.get(i); 
					map.put("allocationId", allocation.get("allocation_id").toString());
					Map<String, Object> carInfo = carInfoService.selectCarInfo(map);
					allocation.put("carInfo", carInfo);
					List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(map);
					for(int j = 0; j < paymentInfoList.size(); j++) {
						Map<String, Object> paymentInfo = paymentInfoList.get(j);
						if(paymentInfo.get("payment_division").toString().equals("01")) {
							String calc = "";
							calc = paymentInfo.get("amount") != null && !paymentInfo.get("amount").toString().equals("") ? paymentInfo.get("amount").toString().replaceAll(",", "") : "0"; 
							income += Integer.parseInt(calc);
						}else if(paymentInfo.get("payment_division").toString().equals("02")) {
							String calc = "";
							calc = paymentInfo.get("amount") != null && !paymentInfo.get("amount").toString().equals("") ? paymentInfo.get("amount").toString().replaceAll(",", "") : "0";
							income -= Integer.parseInt(calc);
						}else if(paymentInfo.get("payment_division").toString().equals("03")) {
							String calc = "";
							calc = paymentInfo.get("amount") != null && !paymentInfo.get("amount").toString().equals("") ? paymentInfo.get("amount").toString().replaceAll(",", "") : "0";
							income -= Integer.parseInt(calc);
						}
					}
					allocation.put("paymentInfoList", paymentInfoList);
					allocationList.set(i,allocation);
					
					//미발행건은 상관없음.
					if(billingStatus.equals("Y")) {
						if(billPublishRequestId.equals(allocation.get("bill_publish_request_id").toString())) {
							compareCnt++;
						}
					}
					
					
				}
				

				//세금계산서 발행 아이디가 같은 배차건의 갯수와 비교되는 배차건의 개수가 같은경우 수정 할 수 있도록 한다.(단건 또는  픽업건만... , 일괄건은 제외)
				//if(eqBillingCnt == compareCnt && !allocationMap.get("batch_status").toString().equals("Y")) {
				if(eqBillingCnt == compareCnt) {
					mav.addObject("possibleMod",  "Y");
				}else {
					mav.addObject("possibleMod",  "N");
				}
				
					DecimalFormat formatter = new DecimalFormat("###,###");
					allocationMap.put("income", formatter.format(income));
					mav.addObject("allocationMap",  allocationMap);
					mav.addObject("allocationList",  allocationList);
					mav.addObject("billingDivisionList",  billingDivisionList);
					mav.addObject("allocationDivisionList",  allocationDivisionList);
					mav.addObject("runDivisionList",  runDivisionList);
					mav.addObject("paymentDivisionList",  paymentDivisionList);
					mav.addObject("payDivisionList",  payDivisionList);
					mav.addObject("driverList",  driverList);
					mav.addObject("companyList",  companyList);
					mav.addObject("paramMap", paramMap);
					mav.addObject("customerList", customerList);
					mav.addObject("billingStatus", billingStatus);
					mav.addObject("customerAppCallMap", customerAppCallMap);
					mav.addObject("userMap", userMap);
					
			//}
				//페이지 수정중 표시

					Map<String,String> insertPageStatusParam = new HashMap<String, String>();
					insertPageStatusParam.put("url",request.getServletPath());
					insertPageStatusParam.put("empId", userMap.get("emp_id").toString());
					insertPageStatusParam.put("empName", userMap.get("emp_name").toString());
					insertPageStatusParam.put("allocationId", request.getParameter("allocationId").toString());
					allocationService.insertPageStatus(insertPageStatusParam);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return mav;
	}
	
	
	
	
	@RequestMapping(value = "/update-allocation", method = {RequestMethod.GET,RequestMethod.POST})
	public ModelAndView updateAllocation(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute AllocationVO allocationVO) throws Exception {
		
		try{
			
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			
			Map<String, Object> map = new HashMap<String, Object>();
			
			
			//String referer = request.getHeader("referer");
			//logger.info("referer="+referer);
			String carInfoVal = request.getParameter("carInfoVal").toString();
			String paymentInfoVal = request.getParameter("paymentInfoVal").toString();
			String location = request.getParameter("location") == null ? "" : request.getParameter("location").toString(); 
			
			String possibleMod = request.getParameter("possibleMod").toString();
			
			
			
			if(!carInfoVal.equals("")){
				JSONParser jsonParser = new JSONParser();
				JSONObject jsonObject = (JSONObject) jsonParser.parse(carInfoVal);
				JSONArray jsonArray = (JSONArray) jsonObject.get("carInfoList");	
				String batchStatusId = "BAT"+UUID.randomUUID().toString().replaceAll("-","");
				for(int i = 0; i < jsonArray.size(); i++){
					
					JSONObject carInfo = (JSONObject)jsonArray.get(i);
					String allocationId =	""; 
					boolean isExist = true;			//추가된 운행 정보는 insert를 해야 한다.
					if(carInfo.get("allocation_id") != null && !carInfo.get("allocation_id").toString().equals("")) { //운행정보는 이미 id가 있으므로 바로 업데이트 한다.
						allocationId = carInfo.get("allocation_id").toString();			
					}else {				//운행 정보에 아이디가 없는 경우는 업데이트시 수정된 경우 이므로 아이디를 새로 부여 한다.
						allocationId = "ALO"+UUID.randomUUID().toString().replaceAll("-","");
						allocationVO.setRegisterId(userMap.get("emp_id").toString());	
						allocationVO.setRegisterName(userMap.get("emp_name").toString());
						allocationVO.setAllocationStatus("N");	
						isExist = false;
					}
							
					//담당자 명이 현대캐피탈인 경우 
					if(allocationVO.getChargeName().equals("현대캐피탈")) {
						allocationVO.setHcYn("Y");
					}
					
					allocationVO.setAllocationId(allocationId);
					CarInfoVO carInfoVO = new CarInfoVO();
					carInfoVO.setAllocationId(allocationId);
					carInfoVO.setCarrierType(carInfo.get("carrier_type").toString());
					carInfoVO.setDistanceType(carInfo.get("distance_type").toString());
					carInfoVO.setDepartureDt(carInfo.get("departure_dt").toString());
					carInfoVO.setDepartureTime(carInfo.get("departure_time").toString());
					carInfoVO.setReceiptSendYn(carInfo.get("receipt_send_yn").toString());
					carInfoVO.setBillPublishId(carInfo.get("bill_publish_id") == null ? "":carInfo.get("bill_publish_id").toString());
					carInfoVO.setBillPublishRequestId(carInfo.get("bill_publish_request_id") == null ? "":carInfo.get("bill_publish_request_id").toString());
					carInfoVO.setBillingStatus(carInfo.get("billing_status") == null ? "N":carInfo.get("billing_status").toString());
					carInfoVO.setDecideStatus(carInfo.get("decide_status") == null ? "N":carInfo.get("decide_status").toString());
					carInfoVO.setVatIncludeYn(carInfo.get("vat_include_yn") == null ? "N":carInfo.get("vat_include_yn").toString());
					carInfoVO.setVatExcludeYn(carInfo.get("vat_exclude_yn") == null ? "N":carInfo.get("vat_exclude_yn").toString());
					carInfoVO.setDebtorCreditorId(carInfo.get("debtor_creditor_id") == null ? "":carInfo.get("debtor_creditor_id").toString());
					carInfoVO.setModId(userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자추가
					String customerAmount = carInfo.get("sales_total").toString() == null ? "0":carInfo.get("sales_total").toString();
					
					
					Map <String, Object> allocationSelectMap = new HashMap<String, Object>();
					allocationSelectMap.put("allocationId", allocationId);
					Map <String, Object> allocationMap = allocationService.selectAllocation(allocationSelectMap);
					Map <String, Object> carInfoMap = carInfoService.selectCarInfo(allocationSelectMap);
					
					String vatIncludeStatus = "";
					String vatExcludeStatus = "";
					
					if(carInfoMap != null) {
						vatIncludeStatus = carInfoMap.get("vat_include_yn")==null? "N":carInfoMap.get("vat_include_yn").toString();
						vatExcludeStatus = carInfoMap.get("vat_exclude_yn")==null? "N":carInfoMap.get("vat_exclude_yn").toString();;
					}else {
						vatIncludeStatus = "N";
						vatExcludeStatus = "N";
					}
					
					boolean modStatus = false;
					
					
					
					if(!customerAmount.equals("0")) {
						if(carInfoVO.getVatIncludeYn().equals("Y") && carInfoVO.getVatExcludeYn().equals("N")) {
							//이전 상태에서 변경 되는 경우 
							if(vatIncludeStatus.equals("N") && carInfoVO.getVatIncludeYn().equals("Y")) {
								
								int total = Integer.parseInt(customerAmount);
								
								float supply = (float) ((float)Float.parseFloat(customerAmount)/1.1);
								int vat = (int) (total - Math.round(supply));
								customerAmount = String.valueOf(Integer.parseInt(customerAmount)-vat);
								carInfoVO.setSalesTotal(customerAmount);
								carInfoVO.setVat(String.valueOf(vat));
								carInfoVO.setPrice(String.valueOf(total));
								modStatus = true;
							//변화 없음
							}else if(vatIncludeStatus.equals("Y") && carInfoVO.getVatIncludeYn().equals("Y")) {

								//금액이 같은경우
								if(carInfoMap.get("sales_total").toString().replaceAll(",", "").equals(customerAmount)) {
								
									carInfoVO.setSalesTotal(carInfoMap.get("sales_total").toString().replaceAll(",", ""));
									carInfoVO.setVat(carInfoMap.get("vat").toString().replaceAll(",", ""));
									carInfoVO.setPrice(carInfoMap.get("price").toString().replaceAll(",", ""));
									
								}else {
									
									int total = Integer.parseInt(customerAmount);
									float supply = (float) ((float)Float.parseFloat(customerAmount)/1.1);
									int vat = (int) (total - Math.round(supply));
									customerAmount = String.valueOf(Integer.parseInt(customerAmount)-vat);
									carInfoVO.setSalesTotal(customerAmount);
									carInfoVO.setVat(String.valueOf(vat));
									carInfoVO.setPrice(String.valueOf(total));
									modStatus = true;
								}
									
							}else{
								
								//금액이 같은경우
								if(carInfoMap.get("sales_total").toString().replaceAll(",", "").equals(customerAmount)) {
								
									carInfoVO.setSalesTotal(carInfoMap.get("sales_total").toString().replaceAll(",", ""));
									carInfoVO.setVat(carInfoMap.get("vat").toString().replaceAll(",", ""));
									carInfoVO.setPrice(carInfoMap.get("price").toString().replaceAll(",", ""));
									
								}else {
									
									int total = Integer.parseInt(customerAmount);
									float supply = (float) ((float)Float.parseFloat(customerAmount)/1.1);
									int vat = (int) (total - Math.round(supply));
									customerAmount = String.valueOf(Integer.parseInt(customerAmount)-vat);
									carInfoVO.setSalesTotal(customerAmount);
									carInfoVO.setVat(String.valueOf(vat));
									carInfoVO.setPrice(String.valueOf(total));
									modStatus = true;
								}
								
							}
							
						}else if(carInfoVO.getVatIncludeYn().equals("N") && carInfoVO.getVatExcludeYn().equals("Y")) {
							
							//이전 상태에서 변경 되는 경우
							
							//포함에서 별도로 변경 되는경우
							if(vatIncludeStatus.equals("Y") && carInfoVO.getVatExcludeYn().equals("Y")) {
								
								//금액이 같은경우
								if(carInfoMap.get("sales_total").toString().replaceAll(",", "").equals(customerAmount)) {
									
									//포함에서 별도가 되는경우 총액이 공급가액
									carInfoVO.setSalesTotal(carInfoMap.get("price").toString().replaceAll(",", ""));
									carInfoVO.setVat(String.valueOf(Math.round(Integer.parseInt(carInfoVO.getSalesTotal())/10)));
									carInfoVO.setPrice(String.valueOf(Integer.parseInt(carInfoVO.getSalesTotal())+Integer.parseInt(carInfoVO.getVat())));
									
								//금액이 변경 된 경우
								}else {
									
									int vat = Integer.parseInt(customerAmount)/10;
									int total = Integer.parseInt(customerAmount)+vat;
									carInfoVO.setSalesTotal(customerAmount);
									carInfoVO.setVat(String.valueOf(vat));
									carInfoVO.setPrice(String.valueOf(total));
									
								}
								modStatus = true;
								
							}else if(vatExcludeStatus.equals("N") && carInfoVO.getVatExcludeYn().equals("Y")) {
								
								int vat = Integer.parseInt(customerAmount)/10;
								int total = Integer.parseInt(customerAmount)+vat;
								carInfoVO.setSalesTotal(customerAmount);
								carInfoVO.setVat(String.valueOf(vat));
								carInfoVO.setPrice(String.valueOf(total));
								modStatus = true;
							}else if(vatExcludeStatus.equals("Y") && carInfoVO.getVatExcludeYn().equals("Y")) {
								
								if(carInfoMap.get("sales_total").toString().replaceAll(",", "").equals(customerAmount)) {
									
									carInfoVO.setSalesTotal(carInfoMap.get("sales_total").toString().replaceAll(",", ""));
									carInfoVO.setVat(carInfoMap.get("vat").toString().replaceAll(",", ""));
									carInfoVO.setPrice(carInfoMap.get("price").toString().replaceAll(",", ""));
									
								}else {
									
									int vat = Integer.parseInt(customerAmount)/10;
									int total = Integer.parseInt(customerAmount)+vat;
									carInfoVO.setSalesTotal(customerAmount);
									carInfoVO.setVat(String.valueOf(vat));
									carInfoVO.setPrice(String.valueOf(total));
									modStatus = true;
								}
								
							}else {
								
								if(carInfoMap.get("sales_total").toString().replaceAll(",", "").equals(customerAmount)) {
									
									carInfoVO.setSalesTotal(carInfoMap.get("sales_total").toString().replaceAll(",", ""));
									carInfoVO.setVat(carInfoMap.get("vat").toString().replaceAll(",", ""));
									carInfoVO.setPrice(carInfoMap.get("price").toString().replaceAll(",", ""));
									
								}else {
									
									int vat = Integer.parseInt(customerAmount)/10;
									int total = Integer.parseInt(customerAmount)+vat;
									carInfoVO.setSalesTotal(customerAmount);
									carInfoVO.setVat(String.valueOf(vat));
									carInfoVO.setPrice(String.valueOf(total));
									modStatus = true;
								}
								
							}
							
						}else {
							
							//포함에서 미포함
							if(vatIncludeStatus.equals("Y") && carInfoVO.getVatIncludeYn().equals("N")) {
							
								carInfoVO.setSalesTotal(carInfoMap.get("price").toString().replaceAll(",", ""));
								carInfoVO.setVat("0");
								carInfoVO.setPrice(carInfoMap.get("price").toString().replaceAll(",", ""));
								modStatus = true;
							//별도에서 미별도
							}else if(vatExcludeStatus.equals("Y") && carInfoVO.getVatExcludeYn().equals("N")) {
								
								carInfoVO.setSalesTotal(carInfoMap.get("sales_total").toString().replaceAll(",", ""));
								carInfoVO.setVat("0");
								carInfoVO.setPrice(carInfoMap.get("sales_total").toString().replaceAll(",", ""));
								modStatus = true;
							}else {
								
								//포함도 아니고 별도도 아님
								carInfoVO.setSalesTotal(carInfo.get("sales_total").toString());
								carInfoVO.setPrice(carInfo.get("sales_total").toString());
								carInfoVO.setVat("0");	
								
							}
								
						}	
					}else {
						/*
						carInfoVO.setSalesTotal(carInfo.get("sales_total").toString());
						carInfoVO.setPrice(carInfo.get("sales_total").toString());
						carInfoVO.setVat(carInfo.get("vat") == null ? "0":carInfo.get("vat").toString());
						*/
						carInfoVO.setSalesTotal("0");
						carInfoVO.setPrice("0");
						carInfoVO.setVat("0");
					}
					
					
					String receiptName = "";
					String receiptEmail = "";
					String receiptPhone = "";
					receiptName = carInfo.get("receipt_name") != null && !carInfo.get("receipt_name").toString().equals("") ? carInfo.get("receipt_name").toString():"";
					receiptEmail = carInfo.get("receipt_email") != null && !carInfo.get("receipt_email").toString().equals("") ? carInfo.get("receipt_email").toString():"";
					receiptPhone = carInfo.get("receipt_phone") != null && !carInfo.get("receipt_phone").toString().equals("") ? carInfo.get("receipt_phone").toString():"";
					carInfoVO.setReceiptName(receiptName);
					carInfoVO.setReceiptEmail(receiptEmail);
					carInfoVO.setReceiptPhone(receiptPhone);
					
					String driverCancel = carInfo.get("driver_cancel_yn").toString();				//기사 배정이 취소 된 경우 view단에서 데이터를 넘기지 않는다.
					
					carInfoVO.setDriverId(carInfo.get("driver_id").toString());
					map.put("driverId", carInfo.get("driver_id").toString());
					Map<String, Object> driver = driverService.selectDriver(map);
					if(driver != null){
						carInfoVO.setDriverName(driver.get("driver_name").toString());	
					}
					
					/**********************************************
					String status = "";
					
					if(!paymentInfoVal.equals("")){
						JSONParser jsonPaymentParser = new JSONParser();
						JSONObject jsonPaymentObject = (JSONObject) jsonPaymentParser.parse(paymentInfoVal);
						JSONArray jsonPaymentArray = (JSONArray) jsonPaymentObject.get("paymentInfoList");	
						map.put("allocationId", allocationId);
						for(int m = 0; m < jsonPaymentArray.size(); m++){
							JSONObject paymentInfo = (JSONObject)jsonPaymentArray.get(m);
							if(paymentInfo.get("index").toString().equals("0")) {
								if(paymentInfo.get("payment_kind").toString().equals("CA") || paymentInfo.get("payment_kind").toString().equals("DD") || paymentInfo.get("payment_kind").toString().equals("AT")){
									status = paymentInfo.get("payment_kind").toString();
								}
							}
						}
					}
					*/
					
					
					//기사정보가 있는경우는 기사 정보가 달라지지 않는 이상 상태를 변경 할 필요가 없다. 기사정보가 변경 된 경우(없었다가 생겼거나,있었는데 변경 되었거나, 있었는데 없었거나) 에만 상태를 변경 한다.
					
					//거래처정보
					Map<String, Object> customerFindMap = new HashMap<String, Object>();
					customerFindMap.put("customerId", allocationVO.getCustomerId());
					Map<String , Object> customer = customerService.selectCustomer(customerFindMap);
					String changeDriverSendAlarmTalkYn = "";
					
					//1. 없었다가 생긴 경우...
					
					if(carInfoMap != null && (carInfoMap.get("driver_id") == null || carInfoMap.get("driver_id").toString().equals("")) ) {
						
							if(driver != null || !carInfo.get("driver_name").toString().equals("")) {		//신규 배차 등록시 기사가 배정 되어 있으면 미배차가 아니고 배차된 상태이다.
								//해당 기사가 거래처 소속인 기사인 경우는 배차 된 상태이나 거래처 소속이 아닌경우 배차 승인대기중이다. 
								if(driver.get("customer_id") != null && !driver.get("customer_id").toString().equals("")) {		//거래처 소속
									
									allocationVO.setAllocationStatus("Y");	
									
									if(customer.get("sms_yn").equals("Y")) { //자동 문자 발송 'Y'
									allocationVO.setAllocationStatus("A");
									}
									
								}else {
									
									/*if(driver.get("driver_id") != null && driver.get("driver_id").toString().equals(carInfoMap.get("driver_id").toString())) { //기사가 배정 되어 있고 업데이트 이전과 기사 아이디가 같다면 이 전 상태를 유지 한다.
										allocationVO.setAllocationStatus(allocationMap.get("allocation_status").toString());
									}else {*/
										allocationVO.setAllocationStatus("A");				//일반	
									/*}*/
									
								}
							}else {
								allocationVO.setAllocationStatus("N");
							}
							
							String paymentInputStatus = request.getParameter("paymentInputStatus") == null ? "" : request.getParameter("paymentInputStatus").toString();
							if(paymentInputStatus.equals("N")) {				//배차 등록시 결제 정보가 입력 되지 않은경우 기사가 배정 되어 있어도 미배차 상태로 표시 한다.
								allocationVO.setAllocationStatus("N");
							}
							
							if(allocationVO.getAllocationStatus().equals("A")) {			//해당 배차에 거래처 소속이 아닌 기사가 배정 되었고 결제 정보가 모두 입력 되어 있는 경우에는 배차승인 대기상태이고 해당기사에게 푸시 알림을 전송 한다. 
								if(driver.get("fcm_token") != null && !driver.get("fcm_token").toString().equals("")) {
									Map<String, Object> sendMessageMap = new HashMap<String, Object>();
									sendMessageMap.put("title", "배차 정보가 도착 했습니다.");
									sendMessageMap.put("body", 
											"상차지:"+carInfo.get("departure").toString()+
											",하차지:"+carInfo.get("arrival").toString()+
											",차종:"+carInfo.get("car_kind").toString()+
											",차대번호:"+carInfo.get("car_id_num").toString());
									sendMessageMap.put("fcm_token", driver.get("fcm_token").toString());
									sendMessageMap.put("device_os", driver.get("device_os").toString());
									sendMessageMap.put("allocationId", allocationId);
									fcmService.fcmSendMessage(sendMessageMap);	
									
									/*Map<String, Object> alarmTalkMap = new HashMap<String, Object>();
									alarmTalkMap.put("gubun", "1");			//알림톡 발송 구분		1:기사배정,2:상차완료,3:하차완료,4:인계완료,5:탁송완료,6탁송취소,7:배차정보(기사님에게),8:배차승인(배차담당직원)
									alarmTalkMap.put("departure", carInfo.get("departure").toString());
									alarmTalkMap.put("arrival", carInfo.get("arrival").toString());
									alarmTalkMap.put("driver_name", driver.get("driver_name").toString());
									alarmTalkMap.put("car_kind", carInfo.get("car_kind").toString());
									alarmTalkMap.put("car_id_num", carInfo.get("car_id_num").toString());
									alarmTalkMap.put("phone_num", driver.get("phone_num").toString());
									//alarmTalkMap.put("customer_phone", customer.get("phone").toString());
									alarmTalkMap.put("customer_phone", allocationVO.getChargePhone());
									alarmTalkService.alarmTalkSend(alarmTalkMap);*/
									
									
								}
								Map<String, Object> driverSetEmpMap = new HashMap<String, Object>();
								driverSetEmpMap.put("empId", userMap.get("emp_id").toString());
								driverSetEmpMap.put("empName", userMap.get("emp_name").toString());
								driverSetEmpMap.put("allocationId",allocationId);
								driverSetEmpMap.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
								driverSetEmpMap.put("logType","U");
								carInfoService.insertCarInfoLogByMap(driverSetEmpMap); //로그기록
								
							}
					
					//2. 다른 기사로 변경 된 경우.
					}else if(driverCancel.equals("N") && driver != null && carInfoMap != null && (carInfoMap.get("driver_id") != null && !carInfoMap.get("driver_id").toString().equals(driver.get("driver_id").toString()))) {
						changeDriverSendAlarmTalkYn ="Y";
						
						if(driver.get("customer_id") != null && !driver.get("customer_id").toString().equals("")) {		//거래처 소속
							if(allocationMap.get("allocation_status").toString().equals("N") || allocationMap.get("allocation_status").toString().equals("A") || allocationMap.get("allocation_status").toString().equals("Y")) {
								allocationVO.setAllocationStatus("Y");	
								
								if(customer.get("sms_yn").equals("Y")) { //자동 문자 발송 'Y'
									allocationVO.setAllocationStatus("A");
									}
								
							}else {
								allocationVO.setAllocationStatus(allocationMap.get("allocation_status").toString());
							}
						}else {
							//미배차,배차승인대기중,배차완료상태(상차검사이전단계)에서 기사가 변경 된경우에만 상태를 변경 하고 그 이외의 상태에서는 이전 상태를 유지한다.
							if(allocationMap.get("allocation_status").toString().equals("N") || allocationMap.get("allocation_status").toString().equals("A") || allocationMap.get("allocation_status").toString().equals("Y")) {
								if(driver.get("driver_id").toString().equals(carInfoMap.get("driver_id").toString())) { //기사가 배정 되어 있고 업데이트 이전과 기사 아이디가 같다면 이 전 상태를 유지 한다.
									allocationVO.setAllocationStatus(allocationMap.get("allocation_status").toString());
								}else {
									allocationVO.setAllocationStatus("A");				//일반	
								}	
							}else {
								allocationVO.setAllocationStatus(allocationMap.get("allocation_status").toString());
							}
						}
						
						
						
						String paymentInputStatus = request.getParameter("paymentInputStatus") == null ? "" : request.getParameter("paymentInputStatus").toString();
						if(paymentInputStatus.equals("N")) {				//배차 등록시 결제 정보가 입력 되지 않은경우 기사가 배정 되어 있어도 미배차 상태로 표시 한다.
							allocationVO.setAllocationStatus("N");
						}
						
						if(allocationVO.getAllocationStatus().equals("A")) {			//해당 배차에 거래처 소속이 아닌 기사가 배정 되었고 결제 정보가 모두 입력 되어 있는 경우에는 배차승인 대기상태이고 해당기사에게 푸시 알림을 전송 한다. 
							if(driver.get("fcm_token") != null && !driver.get("fcm_token").toString().equals("")) {
								Map<String, Object> sendMessageMap = new HashMap<String, Object>();
								sendMessageMap.put("title", "배차 정보가 도착 했습니다.");
								sendMessageMap.put("body", 
										"상차지:"+carInfo.get("departure").toString()+
										",하차지:"+carInfo.get("arrival").toString()+
										",차종:"+carInfo.get("car_kind").toString()+
										",차대번호:"+carInfo.get("car_id_num").toString());
								sendMessageMap.put("fcm_token", driver.get("fcm_token").toString());
								sendMessageMap.put("device_os", driver.get("device_os").toString());
								sendMessageMap.put("allocationId", allocationId);
								fcmService.fcmSendMessage(sendMessageMap);	
								
								/*Map<String, Object> alarmTalkMap = new HashMap<String, Object>();
								alarmTalkMap.put("gubun", "1");			//알림톡 발송 구분		1:기사배정,2:상차완료,3:하차완료,4:인계완료,5:탁송완료,6탁송취소,7:배차정보(기사님에게),8:배차승인(배차담당직원)
								alarmTalkMap.put("departure", carInfo.get("departure").toString());
								alarmTalkMap.put("arrival", carInfo.get("arrival").toString());
								alarmTalkMap.put("driver_name", driver.get("driver_name").toString());
								alarmTalkMap.put("car_kind", carInfo.get("car_kind").toString());
								alarmTalkMap.put("car_id_num", carInfo.get("car_id_num").toString());
								alarmTalkMap.put("phone_num", driver.get("phone_num").toString());
								//alarmTalkMap.put("customer_phone", customer.get("phone").toString());
								alarmTalkMap.put("customer_phone", allocationVO.getChargePhone());
								alarmTalkService.alarmTalkSend(alarmTalkMap);*/
			
							}
							Map<String, Object> driverSetEmpMap = new HashMap<String, Object>();
							driverSetEmpMap.put("empId", userMap.get("emp_id").toString());
							driverSetEmpMap.put("empName", userMap.get("emp_name").toString());
							driverSetEmpMap.put("allocationId",allocationId);
							driverSetEmpMap.put("logType","U");
							carInfoService.updateCarInfoDriverSetEmp(driverSetEmpMap);
							carInfoService.insertCarInfoLogByMap(driverSetEmpMap); //로그기록
						}
						
						
					//3. 새로 배차 정보가 등록 되는 경우
					}else if(carInfoMap == null) {
						//allocationVO.setAllocationStatus("N");
						
						if(driver != null || !carInfo.get("driver_name").toString().equals("")) {		//신규 배차 등록시 기사가 배정 되어 있으면 미배차가 아니고 배차된 상태이다.
							//해당 기사가 거래처 소속인 기사인 경우는 배차 된 상태이나 거래처 소속이 아닌경우 배차 승인대기중이다. 
							if(driver.get("customer_id") != null && !driver.get("customer_id").toString().equals("")) {		//거래처 소속
								allocationVO.setAllocationStatus("Y");	
								
								if(customer.get("sms_yn").equals("Y")) { //자동 문자 발송 'Y'
									allocationVO.setAllocationStatus("A");
									}
								
								
							}else {
								
								/*if(driver.get("driver_id") != null && driver.get("driver_id").toString().equals(carInfoMap.get("driver_id").toString())) { //기사가 배정 되어 있고 업데이트 이전과 기사 아이디가 같다면 이 전 상태를 유지 한다.
									allocationVO.setAllocationStatus(allocationMap.get("allocation_status").toString());
								}else {*/
									allocationVO.setAllocationStatus("A");				//일반	
								/*}*/
								
							}
						}else {
							allocationVO.setAllocationStatus("N");
						}
						
						String paymentInputStatus = request.getParameter("paymentInputStatus") == null ? "" : request.getParameter("paymentInputStatus").toString();
						if(paymentInputStatus.equals("N")) {				//배차 등록시 결제 정보가 입력 되지 않은경우 기사가 배정 되어 있어도 미배차 상태로 표시 한다.
							allocationVO.setAllocationStatus("N");
						}
						
						if(allocationVO.getAllocationStatus().equals("A")) {			//해당 배차에 거래처 소속이 아닌 기사가 배정 되었고 결제 정보가 모두 입력 되어 있는 경우에는 배차승인 대기상태이고 해당기사에게 푸시 알림을 전송 한다. 
							if(driver.get("fcm_token") != null && !driver.get("fcm_token").toString().equals("")) {
								Map<String, Object> sendMessageMap = new HashMap<String, Object>();
								sendMessageMap.put("title", "배차 정보가 도착 했습니다.");
								sendMessageMap.put("body", 
										"상차지:"+carInfo.get("departure").toString()+
										",하차지:"+carInfo.get("arrival").toString()+
										",차종:"+carInfo.get("car_kind").toString()+
										",차대번호:"+carInfo.get("car_id_num").toString());
								sendMessageMap.put("fcm_token", driver.get("fcm_token").toString());
								sendMessageMap.put("device_os", driver.get("device_os").toString());
								sendMessageMap.put("allocationId", allocationId);
								fcmService.fcmSendMessage(sendMessageMap);	
								
								/*Map<String, Object> alarmTalkMap = new HashMap<String, Object>();
								alarmTalkMap.put("gubun", "1");			//알림톡 발송 구분		1:기사배정,2:상차완료,3:하차완료,4:인계완료,5:탁송완료,6탁송취소,7:배차정보(기사님에게),8:배차승인(배차담당직원)
								alarmTalkMap.put("departure", carInfo.get("departure").toString());
								alarmTalkMap.put("arrival", carInfo.get("arrival").toString());
								alarmTalkMap.put("driver_name", driver.get("driver_name").toString());
								alarmTalkMap.put("car_kind", carInfo.get("car_kind").toString());
								alarmTalkMap.put("car_id_num", carInfo.get("car_id_num").toString());
								alarmTalkMap.put("phone_num", driver.get("phone_num").toString());
								//alarmTalkMap.put("customer_phone", customer.get("phone").toString());
								alarmTalkMap.put("customer_phone", allocationVO.getChargePhone());
								alarmTalkService.alarmTalkSend(alarmTalkMap);*/
							}
							Map<String, Object> driverSetEmpMap = new HashMap<String, Object>();
							driverSetEmpMap.put("empId", userMap.get("emp_id").toString());
							driverSetEmpMap.put("empName", userMap.get("emp_name").toString());
							driverSetEmpMap.put("allocationId",allocationId);
							driverSetEmpMap.put("logType","U");
							carInfoService.updateCarInfoDriverSetEmp(driverSetEmpMap);
							carInfoService.insertCarInfoLogByMap(driverSetEmpMap); //로그기록
							
						}
						
						
					//4. 기사 배정이 취소 된경우	
					}else if(driverCancel.equals("Y")) {
						allocationVO.setAllocationStatus("N");
					}else {
						allocationVO.setAllocationStatus(allocationMap != null && allocationMap.get("allocation_status")!= null && !allocationMap.get("allocation_status").toString().equals("") ? allocationMap.get("allocation_status").toString() : "N");
					}
					
					//allocation_status가 X 또는 C 인 경우에는 allocation_status를 변경 하지 않도록 수정.
					if(allocationMap != null && allocationMap.get("allocation_status") != null && (allocationMap.get("allocation_status").toString().equals("X") || allocationMap.get("allocation_status").toString().equals("C"))) {
						allocationVO.setAllocationStatus(allocationMap.get("allocation_status").toString());
					}
					
					if(allocationVO.getBatchStatus().equals("N")) {//개별								//일괄 등록과 개별 등록 구분
						allocationVO.setBatchStatusId("BAT"+UUID.randomUUID().toString().replaceAll("-",""));
						allocationVO.setBatchIndex("0");
					}else if(allocationVO.getBatchStatus().equals("Y")) {//일괄
						allocationVO.setBatchStatusId(batchStatusId);
						allocationVO.setBatchIndex(String.valueOf(i));
					}else if(allocationVO.getBatchStatus().equals("P")) {//픽업
						allocationVO.setBatchStatusId(batchStatusId);
						allocationVO.setBatchIndex("0");
					}					
					
					if(isExist) {
						allocationService.updateAllocation(allocationVO);
						allocationVO.setLogType("U");
					}else {
						allocationService.insertAllocation(allocationVO);
						allocationVO.setLogType("I");
					}
					allocationVO.setModId(userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():"");
					allocationService.insertAllocationLogByVO(allocationVO);
					
					carInfoVO.setCarKind(carInfo.get("car_kind").toString());
					carInfoVO.setCarIdNum(carInfo.get("car_id_num").toString());
					carInfoVO.setCarNum(carInfo.get("car_num").toString());
					carInfoVO.setContractNum(carInfo.get("contract_num").toString());
					carInfoVO.setTowDistance(carInfo.get("tow_distance").toString());
					carInfoVO.setRequirePicCnt(carInfo.get("require_pic_cnt").toString());
					carInfoVO.setRequireDnPicCnt(carInfo.get("require_dn_pic_cnt").toString());
					carInfoVO.setAccidentYn(carInfo.get("accident_yn").toString());
					carInfoVO.setEtc(carInfo.get("etc").toString());
					carInfoVO.setDeparture(carInfo.get("departure").toString());
					carInfoVO.setDepartureAddr(carInfo.get("departure_addr").toString());
					carInfoVO.setDeparturePersonInCharge(carInfo.get("departure_person_in_charge").toString());
					carInfoVO.setDeparturePhone(carInfo.get("departure_phone").toString());
					carInfoVO.setArrival(carInfo.get("arrival").toString());
					carInfoVO.setArrivalAddr(carInfo.get("arrival_addr").toString());
					carInfoVO.setArrivalPersonInCharge(carInfo.get("arrival_person_in_charge").toString());
					carInfoVO.setArrivalPhone(carInfo.get("arrival_phone").toString());
					carInfoVO.setDriverCnt(carInfo.get("driver_cnt") == null || carInfo.get("driver_cnt").toString().equals("") ? "1" : carInfo.get("driver_cnt").toString());
					carInfoVO.setModId(userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자

					
					//
					
					if(isExist) {
						//배차 정보를 업데이트 할때 DB에 입력 되어 있는 차대번호와 상이한경우(차대번호가 수정되는경우)에는 수정한 배차 직원 정보를 저장한다..
						Map<String, Object> selectMap = new HashMap<String, Object>();
						selectMap.put("allocationId", allocationId);
						Map<String, Object> selectCarInfo = carInfoService.selectCarInfo(selectMap);
						if(selectCarInfo != null) {
							if(!selectCarInfo.get("car_id_num").toString().equals(carInfo.get("car_id_num").toString())) {
								//차대번호가 수정된경우
								carInfoVO.setDriverModYn("N");
								carInfoVO.setModEmp(userMap.get("emp_id").toString());
							}else {
								//수정 되지 않았으면 기존의 정보 그대로....
								carInfoVO.setDriverModYn(selectCarInfo.get("driver_mod_yn").toString());
							}
						}else {
							carInfoVO.setDriverModYn("N");
						}
						carInfoService.updateCarInfo(carInfoVO);
						carInfoVO.setLogType("U");
						carInfoService.insertCarInfoLogByVO(carInfoVO);//로그기록
						
						Map<String, Object> carInfoUpdateMap = new HashMap<String, Object>();
						carInfoUpdateMap.put("allocationId", allocationId);
						
						/***********************************************
						if(!status.equals("")) {
							adjustmentService.initCarInfoForBillPublish(carInfoUpdateMap);	
						}
						*/
						
					}else {
						//새로 입력되는건 그냥 하면 되고....
						carInfoService.insertCarInfo(carInfoVO);
						carInfoVO.setLogType("I");
						carInfoService.insertCarInfoLogByVO(carInfoVO); //로그기록
						Map<String, Object> carInfoUpdateMap = new HashMap<String, Object>();
						carInfoUpdateMap.put("allocationId", allocationId);
						//adjustmentService.initCarInfoForBillPublish(carInfoUpdateMap);
					}
					
					/**********************************************
					if(!status.equals("")) {
						if(carInfoVO.getDebtorCreditorId() != null || !carInfoVO.getDebtorCreditorId().equals("")) {
							Map<String, Object> deleteMap = new HashMap<String, Object>();
							deleteMap.put("debtorCreditorId", carInfoVO.getDebtorCreditorId());
							debtorCreditorService.deleteDebtorCreditor(deleteMap);
						}
					}
					*/
										
					//차변이 등록 되어 있는경우 한건인경우 
					
					/*
					if(carInfoVO.getDebtorCreditorId() != null && !carInfoVO.getDebtorCreditorId().equals("")) {
						Map<String, Object> deleteMap = new HashMap<String, Object>();
						deleteMap.put("debtorCreditorId", carInfoVO.getDebtorCreditorId());
						Map<String, Object> debtorCreditor = debtorCreditorService.selectDebtorCreditor(deleteMap);
						
						//차변에 해당하는건이 한건인경우 삭제 한다...
						if(debtorCreditorService.selectCarInfoCountByDebtorCreditorId(deleteMap) == 1) {
						
							carInfoVO.setDebtorCreditorId("");
							Map<String, Object> updateMap = new HashMap<String, Object>();
							updateMap.put("debtorCreditorId", "");
							updateMap.put("allocationId", allocationId);
					  		adjustmentService.updateCarInfoDebtorCreditorId(updateMap);
							debtorCreditorService.deleteDebtorCreditor(deleteMap);	
						
						}else {
							
							//여러건인경우 해당 배차건을 빼주고........
							Map<String, Object> updateMap = new HashMap<String, Object>();
							updateMap.put("debtorCreditorId", carInfoVO.getDebtorCreditorId());
							int updateAmount = Math.round(Integer.parseInt(debtorCreditor.get("total_val").toString().replaceAll(",", ""))-Integer.parseInt(carInfoVO.getPrice().toString()));
							updateMap.put("debtorCreditorId", String.valueOf(updateAmount));
							debtorCreditorService.updateDebtorCreditorTotalVal(updateMap);
							
							carInfoVO.setDebtorCreditorId("");
							updateMap = new HashMap<String, Object>();
							updateMap.put("debtorCreditorId", "");
							updateMap.put("allocationId", allocationId);
					  		adjustmentService.updateCarInfoDebtorCreditorId(updateMap);
							
						}
						
					}
					*/
					
					String carInfoIndex = carInfo.get("index").toString();
					
					String sendAccountInfoYn = customer.get("send_account_info_yn").toString();
					
					if(!paymentInfoVal.equals("")){
						JSONParser jsonPaymentParser = new JSONParser();
						JSONObject jsonPaymentObject = (JSONObject) jsonPaymentParser.parse(paymentInfoVal);
						JSONArray jsonPaymentArray = (JSONArray) jsonPaymentObject.get("paymentInfoList");	
						map.put("allocationId", allocationId);
						
						List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(map);
						
						
						//수정 할 수 있는 배차 건인경우 차대변을 삭제 후 다시 입력 한다. 그렇지 않은경우 차대변을 삭제하지 않는다.
						if(possibleMod.equals("Y")) {
							
							//기존에 등록된 대변 금액과 같으면 삭제 아니면 뺀 금액 만큼 대변에서 빼기....
							for(int m = 0; m < paymentInfoList.size(); m++) {
								Map<String, Object> paymentInfo = paymentInfoList.get(m);
								//대변이 입력 되어 있으면 같이 삭제 한다...							
								if(paymentInfo.get("debtor_creditor_id") != null && !paymentInfo.get("debtor_creditor_id").toString().equals("")) {
									Map<String, Object> deleteMap = new HashMap<String, Object>();
									deleteMap.put("debtorCreditorId", paymentInfo.get("debtor_creditor_id").toString());
									
									Map<String, Object> debtorCreditor = debtorCreditorService.selectDebtorCreditor(deleteMap);
									
									//부가세 고려한 총액으로 변환해서 계산.......
									int result = 0;
									if(vatIncludeStatus.equals("Y")){
										
										//포함건인경우 공급가가 변경 되므로 계산 해서 넣어 주는게 맞고
										//부가세 포함건이면
										int total =  (int) Math.round((Integer.parseInt(paymentInfo.get("amount").toString().replaceAll(",", ""))*11/10));
										result = total;
									
									}else if(vatExcludeStatus.equals("Y")) {
										
										
										//별도건인경우 공급가는 변함 없으므로.......(200601)
										result = Integer.parseInt(paymentInfo.get("amount").toString().replaceAll(",", ""));
										//부가세 별도건이면	
										//int total =  (int) Math.round((Integer.parseInt(paymentInfo.get("amount").toString().replaceAll(",", ""))/11)*10);
										//result = total;
										
									//포함도 아니고 별도도 아니면
									}else{
										result = Integer.parseInt(paymentInfo.get("amount").toString().replaceAll(",", ""));
									}
									
									//대변금액과 같으면 삭제
									
									if(debtorCreditor != null) {
										if(debtorCreditor.get("total_val").toString().replaceAll(",", "").equals(String.valueOf(result))){
											debtorCreditorService.deleteDebtorCreditor(deleteMap);	
											
										//대변금액과 다르면 금액만큼 대변 금액에서 빼기.....
										}else {
											Map<String, Object> updateMap = new HashMap<String, Object>();
											updateMap.put("debtorCreditorId", paymentInfo.get("debtor_creditor_id").toString());
											int updateAmount = Math.round(Integer.parseInt(debtorCreditor.get("total_val").toString().replaceAll(",", ""))-result);
											updateMap.put("debtorCreditorId", String.valueOf(updateAmount));
											debtorCreditorService.updateDebtorCreditorTotalVal(updateMap);
											
										}	
									}
									
								}
							}
							
							
							
						}
						map.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
						paymentInfoService.deletePaymentInfo(map);				//결제정보의 수 및 결제처의 정보가 변경 될 수 있으므로 기존의 결제정보를 삭제 하고 다시 등록 한다.
						map.put("logType","D");
						paymentInfoService.insertPaymentInfoLogByMap(map);
						
						for(int j = 0; j < jsonPaymentArray.size(); j++){
							JSONObject paymentInfo = (JSONObject)jsonPaymentArray.get(j);
							if(paymentInfo.get("index").toString().equals(carInfoIndex)) {
								PaymentInfoVO paymentInfoVO = new PaymentInfoVO();
								paymentInfoVO.setAllocationId(allocationId);
								
								if(paymentInfo.get("payment_division").toString().equals("01")) {
									
									paymentInfoVO.setVatIncludeYn(carInfoVO.getVatIncludeYn());
									paymentInfoVO.setVatExcludeYn(carInfoVO.getVatExcludeYn());
									
									if(modStatus) {
										
										int result = 0;
										if(carInfoVO.getVatIncludeYn().equals("Y") && carInfoVO.getVatExcludeYn().equals("N")){
											
											//부가세 포함건이면
											int total =  (int) Math.round((Float.parseFloat(paymentInfo.get("amount").toString().replaceAll(",", ""))*10/11));
											result = total;
										
										}else if(carInfoVO.getVatExcludeYn().equals("Y") && carInfoVO.getVatIncludeYn().equals("N")) {
											
											//부가세 별도건에서 건 인경우 공급가는
											
											//포함에서 별도가 된 경우는 
											if(vatIncludeStatus.equals("Y") && carInfoVO.getVatExcludeYn().equals("Y")) {
												result = Integer.parseInt(carInfoVO.getSalesTotal());
											}else {
												result = Integer.parseInt(paymentInfo.get("amount").toString().replaceAll(",", ""));	
											}
											
											//부가세 별도건이면	
											//int total =  (int) Math.round((Float.parseFloat(paymentInfo.get("amount").toString().replaceAll(",", ""))*11)/10);
											//result = total;
											
										//포함도 아니고 별도도 아니면
										}else{
											
											//기존에 포함 -> 미포함으로
											if(vatIncludeStatus.equals("Y") && carInfoVO.getVatIncludeYn().equals("N")) {
												result =  (int) Math.round((Float.parseFloat(paymentInfo.get("amount").toString().replaceAll(",", ""))*11)/10);
												
											//기존에 별도 -> 미별도로
											}else if(vatExcludeStatus.equals("Y") && carInfoVO.getVatExcludeYn().equals("N")) {
												//result =  (int) Math.round((Float.parseFloat(paymentInfo.get("amount").toString().replaceAll(",", ""))/1.1));
												result = Integer.parseInt(paymentInfo.get("amount").toString().replaceAll(",", ""));
											}else {
												result = Integer.parseInt(paymentInfo.get("amount").toString().replaceAll(",", ""));
											}
											//result = Integer.parseInt(paymentInfo.get("amount").toString().replaceAll(",", ""));
										}
										
										//paymentInfoVO.setAmount(String.valueOf(result));
										paymentInfoVO.setAmount(paymentInfo.get("amount").toString());
									}else {
										paymentInfoVO.setAmount(paymentInfo.get("amount").toString());
									}
								}else {
									paymentInfoVO.setAmount(paymentInfo.get("amount").toString());
								}
								
								paymentInfoVO.setBillingDivision(paymentInfo.get("billing_division") != null && !paymentInfo.get("billing_division").toString().equals("") ? paymentInfo.get("billing_division").toString() : "");
								paymentInfoVO.setBillingDt(paymentInfo.get("billing_dt").toString());
								paymentInfoVO.setDeductionRate(paymentInfo.get("deduction_rate").toString());
								paymentInfoVO.setEtc(paymentInfo.get("etc").toString());
								
								paymentInfoVO.setPaymentDivision(paymentInfo.get("payment_division").toString());
								
								//결제 여부(payment)가 부분결제(P) 또는 결제(Y)인것에 대해 대변에 입력 한다.
								paymentInfoVO.setPayment(paymentInfo.get("payment") != null && !paymentInfo.get("payment").toString().equals("") ? paymentInfo.get("payment").toString() : "N");
								
								//매출처 결제 정보인 경우이고 
								if(paymentInfoVO.getPaymentDivision().equals("01")) {
									
									//결제가 완료된 배차건의 경우 계좌 정보를 발송 하지 않는다..
									if(paymentInfoVO.getPayment().equals("Y")) {
										sendAccountInfoYn = "N";
									}
									
									
									//정상적인 결제정보 입력인 경우
							//		if(jsonPaymentArray.size() == 3) {
									//	paymentInfoVO.setAmount(carInfoVO.getPrice());	
							//		}else {
										
									//	paymentInfoVO.setAmount(paymentInfo.get("amount").toString());
										/*
										//부가세 포함건이면
										if(vatIncludeStatus.equals("Y")){
											
											int total = Integer.parseInt(paymentInfo.get("amount").toString());
											float supply = (float) ((float)Float.parseFloat(paymentInfo.get("amount").toString())/1.1);
											paymentInfoVO.setAmount(String.valueOf(supply));
										//부가세 별도건이면
										}else if(vatExcludeStatus.equals("Y")) {
											int vat = Integer.parseInt(paymentInfo.get("amount").toString())/10;
											int total = Integer.parseInt(paymentInfo.get("amount").toString())+vat;
											paymentInfoVO.setAmount(String.valueOf(total));
											
										//포함도 아니고 별도도 아니면
										}else{
											paymentInfoVO.setAmount(paymentInfo.get("amount").toString());
										}
										*/
							//		}
										
								}
								
								paymentInfoVO.setPaymentDt(paymentInfo.get("payment_dt").toString());
								paymentInfoVO.setPaymentKind(paymentInfo.get("payment_kind").toString());
								paymentInfoVO.setPaymentPartner(paymentInfo.get("payment_partner").toString());
								paymentInfoVO.setPaymentPartnerId(paymentInfo.get("payment_partner_id").toString());
								paymentInfoVO.setBillForPayment(paymentInfo.get("bill_for_payment").toString());
								paymentInfoVO.setBillingStatus(paymentInfo.get("billing_status") == null ? "N":paymentInfo.get("billing_status").toString());
								paymentInfoVO.setDepositId(paymentInfo.get("deposit_id") == null ? "":paymentInfo.get("deposit_id").toString());
								paymentInfoVO.setDecideStatus(paymentInfo.get("decide_status") == null ? "N":paymentInfo.get("decide_status").toString());
								paymentInfoVO.setSelectedStatus(paymentInfo.get("selected_status") == null ? "N":paymentInfo.get("selected_status").toString());
								paymentInfoVO.setDecideMonth(paymentInfo.get("decide_month") == null ? "":paymentInfo.get("decide_month").toString());
								paymentInfoVO.setDecideFinal(paymentInfo.get("decide_final") == null ? "N":paymentInfo.get("decide_final").toString());
								paymentInfoVO.setDecideFinalId(paymentInfo.get("decide_final_id") == null ? "":paymentInfo.get("decide_final_id").toString());
								
								//기사 결제정보이고 
								if(paymentInfoVO.getPaymentDivision().equals("02")) {
									
									if(driver != null) {
										//현대캐피탈건 입력시 셀프기사님인 경우 공제율을 변경 한다.									
										if(allocationVO.getHcYn().equals("Y") && driver.get("car_assign_company") != null && driver.get("car_assign_company").toString().equals("S")){
											
											int deductionRate = 0;
											deductionRate = Integer.parseInt(driver.get("deduction_rate").toString());
											if(deductionRate > 10) {
		    			        				if(deductionRate == 15) {
		    			        					//지입기사이면 14% 아니면 15%
		    			        					if(driver.get("driver_kind") != null) {
		    			        						if(driver.get("driver_kind").equals("00") || driver.get("driver_kind").equals("03")) {
			    			        						deductionRate = 0;
			    			        					}else if(driver.get("driver_kind").equals("01")) {
			    			        						deductionRate = 4;
			    			        					}else if(driver.get("driver_kind").equals("02")) {
			    			        						deductionRate = 5;
			    			        					}else {
			    			        						deductionRate = 0;
			    			        					}
		    			        					}else {
		    			        						deductionRate = 0;
		    			        					}
		    			        					
		    			        				}else {
		    			        					deductionRate = deductionRate-10;
		    			        				}
		    			        			}else if(deductionRate == 10) {
		    			        				deductionRate = 0;
		    			        			} 
											
											int amount = Integer.parseInt(paymentInfo.get("amount").toString());
											int billForPayment = amount - (amount*deductionRate/100);
											paymentInfoVO.setBillForPayment(String.valueOf(billForPayment));
											paymentInfoVO.setDeductionRate(String.valueOf(deductionRate));
											
										}
										
									}
									if(paymentInfo.get("payment_kind").toString().equals("AT") || paymentInfo.get("payment_kind").toString().equals("CD")) {
										paymentInfoVO.setVatIncludeYn("N");
    									paymentInfoVO.setVatExcludeYn("Y");
									}
									
								}
								
								
								if(paymentInfoVO.getPaymentDivision().equals("03")) {
									if(paymentInfo.get("payment_kind").toString().equals("AT") || paymentInfo.get("payment_kind").toString().equals("CD")) {
										paymentInfoVO.setVatIncludeYn("N");
										paymentInfoVO.setVatExcludeYn("Y");
									}	
								}
								
								
								if((paymentInfo.get("payment_division").toString().equals("02") || paymentInfo.get("payment_division").toString().equals("03")) && driverCancel.equals("Y")){		//기사 배정이 취소 된경우
									paymentInfoVO.setPaymentPartner("");
									paymentInfoVO.setPaymentPartnerId("");	
									paymentInfoVO.setDeductionRate("");
									//paymentInfoVO.setAmount("");
									paymentInfoVO.setPaymentDt("");
									paymentInfoVO.setBillingDt("");
									paymentInfoVO.setBillForPayment("");
									paymentInfoVO.setPayment("N");
								}
								paymentInfoVO.setModId(userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자추가 
								paymentInfoVO.setLogType("U");
								paymentInfoService.insertPaymentInfo(paymentInfoVO);
								paymentInfoService.insertPaymentInfoLogByVO(paymentInfoVO);
							}
						}
						
						//기존의 결제정보와 변경된 결제 정보를 비교하여 차,대변을 설정 한다........
						if(possibleMod.equals("Y")) {
							paymentInfoService.comparePaymentInfoForDebtorCreditor(userMap,carInfoVO,carInfoMap,paymentInfoList, allocationId);	
						}
						
					}
					
					
					//2020-02-13
					//if(allocationVO.getAllocationStatus().equals("N") || allocationVO.getAllocationStatus().equals("A")) {
					if(allocationVO.getAllocationStatus().equals("A")) {
						Map<String, Object> payment = new HashMap<String, Object>();
						payment.put("allocationId", allocationVO.getAllocationId());
						List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(payment);
						String paymentKind = "";
						String billingDivision = "";
						if(paymentInfoList != null && paymentInfoList.size() > 0){
							for(int k = 0; k < paymentInfoList.size(); k++) {
								Map<String, Object> paymentInfo = paymentInfoList.get(k);
								if(paymentInfo.get("payment_division").toString().equals("01")) {		//매출처 
									if(paymentInfo.get("payment_kind") != null) {
										if(paymentInfo.get("payment_kind").toString().equals("DD")) {		//결제 방법이 기사 수금이면
											paymentKind = "DD";
										}else {
											paymentKind = paymentInfo.get("payment_kind").toString();
										}
										if(paymentInfo.get("billing_division") != null) {
											billingDivision = paymentInfo.get("billing_division").toString();
										}
									}
								}
							}
						}
						Map<String, Object> alarmTalkMap = new HashMap<String, Object>();
						//알림톡 발송 구분		0 : 예약 완료,1:기사배정,2:상차완료,3:하차완료,4:인계완료,5:탁송완료,6탁송취소,7:배차정보(기사님에게),8:배차승인(배차담당직원)
						if(allocationVO.getAllocationStatus().equals("N")) {
							alarmTalkMap.put("gubun", "0");
						}else if(allocationVO.getAllocationStatus().equals("A")) {
							
							alarmTalkMap.put("gubun", "1");	
							alarmTalkMap.put("changeDriverSendAlarmTalkYn", changeDriverSendAlarmTalkYn);
						}
						
						
						alarmTalkMap.put("departure", carInfo.get("departure").toString());
						alarmTalkMap.put("arrival", carInfo.get("arrival").toString());
						if(driver != null) {
							alarmTalkMap.put("driver_name", driver.get("driver_name") != null ? driver.get("driver_name").toString() : "");
							alarmTalkMap.put("driver_id", driver.get("driver_id"));
							alarmTalkMap.put("phone_num", driver.get("phone_num").toString());
						}
						alarmTalkMap.put("car_kind", carInfo.get("car_kind").toString());
						alarmTalkMap.put("car_id_num", carInfo.get("car_id_num").toString());
						alarmTalkMap.put("departure_dt", carInfo.get("departure_dt").toString());
						alarmTalkMap.put("customer_id", customer.get("customer_id").toString());
						alarmTalkMap.put("customer_name", customer.get("customer_name").toString());
						alarmTalkMap.put("allocation_id", allocationId);
						alarmTalkMap.put("departure_phone", carInfo.get("departure_phone").toString());
						alarmTalkMap.put("arrival_phone", carInfo.get("arrival_phone").toString());
						
						//티케이물류 문자 발송시 문자 내용에 고객명 추가.
						String personInChargeStr = carInfo.get("arrival_person_in_charge") != null && !carInfo.get("arrival_person_in_charge").toString().equals("") ? carInfo.get("arrival_person_in_charge").toString() : "";
						personInChargeStr = personInChargeStr.replaceAll(" ", "");
						personInChargeStr = personInChargeStr.trim();
						
						String[] personInCharge = personInChargeStr.split(";");  
						//alarmTalkMap.put("person_in_charge",personInCharge[0]);
						if(personInCharge.length > 1) {
							alarmTalkMap.put("person_in_charge",personInCharge[0]);
						}else {
							alarmTalkMap.put("person_in_charge", personInChargeStr);
						}
				
						
						
						//사장님 요청 하차지 담당자에게도 알림톡을 발송 한다.
						String customerPhone = allocationVO.getChargePhone();
						if(carInfo.get("departure_phone") != null && !carInfo.get("departure_phone").toString().equals("")) {
							if(WebUtils.isNumber(carInfo.get("departure_phone").toString().replaceAll("-", "").replaceAll(" ", ""))) {
								customerPhone += ";"+carInfo.get("departure_phone").toString().replaceAll("-", "").replaceAll(" ", "");
							}
						}
						
						if(carInfo.get("arrival_phone") != null && !carInfo.get("arrival_phone").toString().equals("")) {
							if(WebUtils.isNumber(carInfo.get("arrival_phone").toString().replaceAll("-", "").replaceAll(" ", ""))) {
								customerPhone += ";"+carInfo.get("arrival_phone").toString().replaceAll("-", "").replaceAll(" ", "");
							}
						}
						
						alarmTalkMap.put("customer_phone", customerPhone);
						
						/*
						if(carInfo.get("departure_phone") != null && !carInfo.get("departure_phone").toString().equals("")) {
							if(WebUtils.isNumber(carInfo.get("departure_phone").toString().replaceAll("-", "").replaceAll(" ", ""))) {
								alarmTalkMap.put("customer_phone", allocationVO.getChargePhone()+";"+carInfo.get("departure_phone").toString().replaceAll("-", "").replaceAll(" ", ""));
							}else{
								alarmTalkMap.put("customer_phone", allocationVO.getChargePhone());	
							}
						}else {
							alarmTalkMap.put("customer_phone", allocationVO.getChargePhone());	
						}
						*/
						
						//alarmTalkMap.put("customer_phone", allocationVO.getChargePhone());
						alarmTalkMap.put("alarm_talk_yn", customer.get("alarm_talk_yn").toString());
						//alarmTalkMap.put("send_account_info_yn", customer.get("send_account_info_yn").toString());
						alarmTalkMap.put("send_account_info_yn", sendAccountInfoYn);
						alarmTalkMap.put("payment_kind",paymentKind);
						alarmTalkMap.put("customer_kind",customer.get("customer_kind").toString());
						alarmTalkMap.put("billing_division",billingDivision);
						alarmTalkMap.put("alarmOrSmsYn", customer.get("alarm_or_sms_yn").toString());
						if(customer.get("alarm_talk_yn").toString().equals("Y") && allocationVO.getChargePhone() != null && !allocationVO.getChargePhone().equals("")) {
							alarmTalkService.alarmTalkSends(alarmTalkMap);	
						}
						if(customer.get("sms_yn").toString().equals("Y")) {
							sendSmsService.smsSend(alarmTalkMap);
						}
						
						
					}
					
				}
			}
			
			
			//allocationStatusForResult
			//cPage
			String allocationStatusForResult = request.getParameter("allocationStatusForResult") == null ? "" : request.getParameter("allocationStatusForResult").toString();
			String cPage = request.getParameter("cPage") == null ? "" : request.getParameter("cPage").toString();
			String searchDateType = request.getParameter("searchDateType") == null ? "" : request.getParameter("searchDateType").toString();
			String startDt = request.getParameter("startDt") == null ? "" : request.getParameter("startDt").toString();
			String endDt = request.getParameter("endDt") == null ? "" : request.getParameter("endDt").toString();
			String searchType = request.getParameter("searchType") == null ? "" : request.getParameter("searchType").toString();
			String searchWord = request.getParameter("searchWord") == null ? "" : request.getParameter("searchWord").toString();
			searchWord = URLEncoder.encode(searchWord, "UTF-8");
			
			if(location.equals("")) {
				WebUtils.messageAndRedirectUrl(mav, "수정되었습니다.", "/allocation/combination.do?allocationStatus="+allocationStatusForResult+"&cPage="+cPage+"&searchDateType="+searchDateType+"&startDt="+startDt+"&endDt="+endDt+"&searchType="+searchType+"&searchWord="+searchWord);	
			}else {
				if(location.equals("combination")) {
					WebUtils.messageAndRedirectUrl(mav, "수정되었습니다.", "/allocation/combination.do?allocationStatus="+allocationStatusForResult+"&cPage="+cPage+"&searchDateType="+searchDateType+"&startDt="+startDt+"&endDt="+endDt+"&searchType="+searchType+"&searchWord="+searchWord);
				}else if(location.equals("self")) {
					WebUtils.messageAndRedirectUrl(mav, "수정되었습니다.", "/allocation/self.do?allocationStatus="+allocationStatusForResult+"&cPage="+cPage+"&searchDateType="+searchDateType+"&startDt="+startDt+"&endDt="+endDt+"&searchType="+searchType+"&searchWord="+searchWord);
				}else if(location.equals("carrier")) {
					WebUtils.messageAndRedirectUrl(mav, "수정되었습니다.", "/allocation/carrier.do?allocationStatus="+allocationStatusForResult+"&cPage="+cPage+"&searchDateType="+searchDateType+"&startDt="+startDt+"&endDt="+endDt+"&searchType="+searchType+"&searchWord="+searchWord);
				}
			}
			
			/**
			 * ca_page_status 삭제 처리하여 lock 해제 
			 */
			Map<String,String> deleteAllocationParam = new HashMap<String, String>();
			deleteAllocationParam.put("url","/allocation/allocation-mod.do");
			deleteAllocationParam.put("allocationId", request.getParameter("allocationId").toString());
			allocationService.deletePageStatus(deleteAllocationParam);
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	@RequestMapping(value = "/setDriver", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi setDriver(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			Map<String, Object> map = new HashMap<String, Object>();
			
			String allocationId = request.getParameter("allocationId").toString();
			String[] allocationIdArr = allocationId.split(",");
			
			for(int i = 0; i < allocationIdArr.length; i++){
				String driverId = request.getParameter("driverId").toString();
				String driverCnt = request.getParameter("driverCnt") == null || request.getParameter("driverCnt").equals("")  ? "1" : request.getParameter("driverCnt").toString();
				String deductionRate = "";
				map.put("driverId", driverId);
				Map<String, Object> driverMap = driverService.selectDriver(map);
				map.put("allocationId", allocationIdArr[i]);
				Map<String, Object> allocationMap = allocationService.selectAllocation(map);
				
				map.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
				
				//기사 지정시 거래처 정보가 있는 경우는 거래처에 소속된 기사인 경우 이므로 결제정보의 매입처 부분에 표시 되도록 한다.
				String customerInfo = request.getParameter("customerInfo").toString();
				JSONParser jsonParser = new JSONParser();
				JSONObject jsonObject = (JSONObject) jsonParser.parse(customerInfo);
				
				String customerId = "";
				String customerName =  "";
				if(jsonObject != null && jsonObject.get("customerId") != null && jsonObject.get("customerName") != null) {
					customerId = jsonObject.get("customerId").toString();
					customerName = jsonObject.get("customerName").toString();
				}
				
				if(!allocationMap.get("batch_status").toString().equals("P")) {			//픽업건이 아닌경우는 기존과 동일하게 동작 하면 된다.
					map.put("batchStatusId", allocationMap.get("batch_status_id"));
					List<Map<String, Object>> allocationList = allocationService.selectAllocationList(map);	
					if(allocationList.size() > 0) {
						
						if(driverMap != null) {
							deductionRate = driverMap.get("deduction_rate") != null && !driverMap.get("deduction_rate").toString().equals("") ? driverMap.get("deduction_rate").toString(): "0";			//해당 기사의 공제율
							for(int j = 0; j < allocationList.size(); j++) {
								//Map<String, Object> allocation = allocationList.get(j);
								map.put("allocationId", allocationList.get(j).get("allocation_id").toString());
								map.put("driverId", driverId);
								map.put("driverName", driverMap.get("driver_name").toString());
								map.put("driverCnt", driverCnt);
								carInfoService.updateCarInfoDriver(map);
								map.put("logType","U");
								carInfoService.insertCarInfoLogByMap(map); //로그기록
								//거래처정보
								Map<String, Object> customerFindMap = new HashMap<String, Object>();
								customerFindMap.put("customerId", allocationList.get(j).get("customer_id").toString());
								Map<String , Object> customer = customerService.selectCustomer(customerFindMap);
								
								String sendAccountInfoYn = customer.get("send_account_info_yn").toString();
								
								//결제 정보가 입력 되지 않은경우 상태를 변경 하지 않는다.
								List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(map);
								String paymentKind = "";
								String billingDivision = "";
								boolean paymentStatus = true;
								for(int k = 0; k < paymentInfoList.size(); k++) {//배차 등록시 결제 정보가 입력 되지 않은경우 기사가 배정 되어 있어도 미배차 상태로 표시 한다.
									Map<String, Object> paymentInfo = paymentInfoList.get(k);
									if(paymentInfo.get("payment_division").toString().equals("01")) {		//매출처
										
										//결제가 완료된 배차건의 경우 계좌 정보를 발송 하지 않는다..
										if(paymentInfo.get("payment") != null && paymentInfo.get("payment").toString().equals("Y")) {
											sendAccountInfoYn = "N";
										}
										
										if(paymentInfo.get("payment_kind") != null) {
											if(paymentInfo.get("payment_kind").toString().equals("DD")) {		//결제 방법이 기사 수금이면
												paymentKind = "DD";
											}else {
												paymentKind = paymentInfo.get("payment_kind").toString();
											}
											if(paymentInfo.get("billing_division") != null) {
												billingDivision = paymentInfo.get("billing_division").toString();
											}
										}
									}
									//if(paymentInfo.get("payment_division").toString().equals("01") || paymentInfo.get("payment_division").toString().equals("02")) {
										
									if(paymentInfo.get("payment_division").toString().equals("01") && paymentInfo.get("amount") != null && paymentInfo.get("amount").toString().equals("")){
										paymentStatus = false;
									}else if(customerId.equals("") && paymentInfo.get("payment_division").toString().equals("02")){
										if(paymentInfo.get("amount") != null && paymentInfo.get("amount").toString().equals("")){
											paymentStatus = false;
										}	
										//deduction_rate 공제율
										//bill_for_payment 최종지급액
										//amount	기사지급액
										Map<String, Object> paymentUpdateMap = new HashMap<String, Object>();
										paymentUpdateMap.put("allocationId", allocationList.get(j).get("allocation_id").toString());
										paymentUpdateMap.put("paymentDivision", "03");
										paymentInfoService.updatePaymentInfoInit(paymentUpdateMap);
										paymentUpdateMap.put("deductionRate", deductionRate);
										paymentUpdateMap.put("paymentDivision", paymentInfo.get("payment_division").toString());
										//결제 정보에 기사 정보를 업데이트 한다.
										paymentUpdateMap.put("paymentPartnerId", driverId);
										paymentUpdateMap.put("paymentPartner", driverMap.get("driver_name").toString());
										paymentUpdateMap.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자

										paymentInfoService.updatePaymentInfoDriverInfo(paymentUpdateMap);
										
										if(paymentInfo.get("amount") != null && !paymentInfo.get("amount").toString().equals("")) {
											if(!paymentInfo.get("amount").toString().equals("0") && !paymentInfo.get("amount").toString().equals("")) {
												String amount = paymentInfo.get("amount").toString();
												amount = amount.replaceAll(",", "");
												int billForPayment = Integer.parseInt(amount)-(Integer.parseInt(deductionRate)*Integer.parseInt(amount)/100);
												paymentUpdateMap.put("billForPayment", String.valueOf(billForPayment));
												paymentInfoService.updatePaymentInfoDriverAmount(paymentUpdateMap);
											}else {
												//0이면
												
												paymentInfoService.updatePaymentInfoDriverAmount(paymentUpdateMap);
											}
										}
										
										paymentUpdateMap.put("logType","U");
										paymentInfoService.insertPaymentInfoLogByMap(paymentUpdateMap); //로그기록
										
									}else if(!customerId.equals("") && paymentInfo.get("payment_division").toString().equals("03")){		//거래처 기사인 경우
										if(paymentInfo.get("amount") != null && paymentInfo.get("amount").toString().equals("")){
											paymentStatus = false;
										}
										Map<String, Object> paymentUpdateMap = new HashMap<String, Object>();
										paymentUpdateMap.put("allocationId", allocationList.get(j).get("allocation_id").toString());
										paymentUpdateMap.put("paymentDivision", "02");
										Map<String, Object> savePaymentInfo = paymentInfoService.selectPaymentInfoByPaymentDivision(paymentUpdateMap);
										
										paymentInfoService.updatePaymentInfoInit(paymentUpdateMap);
										paymentUpdateMap.put("paymentDivision", paymentInfo.get("payment_division").toString());
										paymentUpdateMap.put("paymentPartnerId", customerId);
										paymentUpdateMap.put("paymentPartner", customerName);
										
										paymentUpdateMap.put("paymentKind", savePaymentInfo.get("payment_kind").toString());
										paymentUpdateMap.put("billingDivision", savePaymentInfo.get("billing_division").toString());
										paymentUpdateMap.put("amount", savePaymentInfo.get("amount").toString());
										paymentUpdateMap.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자 
										paymentUpdateMap.put("logType","U");
										paymentInfoService.updatePaymentInfoDriverInfo(paymentUpdateMap);
										paymentInfoService.insertPaymentInfoLogByMap(paymentUpdateMap); //로그기록
									}
									
										/*if(paymentInfo.get("amount") != null && paymentInfo.get("amount").toString().equals("")){
											paymentStatus = false;
										}
										if(customerId.equals("") && paymentInfo.get("payment_division").toString().equals("02")) {		//해당 기사의 공제율을 적용 한다.
												//deduction_rate 공제율
												//bill_for_payment 최종지급액
												//amount	기사지급액
											Map<String, Object> paymentUpdateMap = new HashMap<String, Object>();
											paymentUpdateMap.put("allocationId", allocationList.get(j).get("allocation_id").toString());
											paymentUpdateMap.put("deductionRate", deductionRate);
											paymentUpdateMap.put("paymentDivision", paymentInfo.get("payment_division").toString());
											//결제 정보에 기사 정보를 업데이트 한다.
											paymentUpdateMap.put("paymentPartnerId", driverId);
											paymentUpdateMap.put("paymentPartner", driverMap.get("driver_name").toString());
											paymentInfoService.updatePaymentInfoDriverInfo(paymentUpdateMap);
											if(paymentInfo.get("amount") != null && !paymentInfo.get("amount").toString().equals("")) {
												if(!paymentInfo.get("amount").toString().equals("0") && !paymentInfo.get("amount").toString().equals("")) {
													String amount = paymentInfo.get("amount").toString();
													amount = amount.replaceAll(",", "");
													int billForPayment = Integer.parseInt(amount)-(Integer.parseInt(deductionRate)*Integer.parseInt(amount)/100);
													paymentUpdateMap.put("billForPayment", String.valueOf(billForPayment));
													paymentInfoService.updatePaymentInfoDriverAmount(paymentUpdateMap);
												}else {
													//0이면
													
													paymentInfoService.updatePaymentInfoDriverAmount(paymentUpdateMap);
												}
											}
												
										}*/
										
									//}
								}
								
								
								
								
								if(paymentStatus) {
									if(driverMap.get("customer_id") != null && !driverMap.get("customer_id").toString().equals("")) {		//거래처 소속
										map.put("allocationStatus", BaseAppConstants.ALLOCATION_STATUS_Y);	
									}else {
										map.put("allocationStatus", BaseAppConstants.ALLOCATION_STATUS_A);
										//기사가 배정 된 경우 해당 기사에게 푸시 알림을 보낸다.
										if(driverMap.get("fcm_token") != null && !driverMap.get("fcm_token").toString().equals("")) {
											Map<String, Object> carInfoMap = carInfoService.selectCarInfo(map);
											Map<String, Object> sendMessageMap = new HashMap<String, Object>();
											sendMessageMap.put("title", "배차 정보가 도착 했습니다.");
											String carKind = "";
											String carIdNum = "";
											
											carKind = carInfoMap.get("car_kind") != null && !carInfoMap.get("car_kind").toString().equals("")? carInfoMap.get("car_kind").toString():"정보없음";
											carIdNum = carInfoMap.get("car_id_num") != null && !carInfoMap.get("car_id_num").toString().equals("")? carInfoMap.get("car_id_num").toString():"정보 없음";
											
											String departure = carInfoMap.get("departure") != null && !carInfoMap.get("departure").toString().equals("") ? carInfoMap.get("departure").toString() : "";
											String arrival = carInfoMap.get("arrival") != null && !carInfoMap.get("arrival").toString().equals("") ? carInfoMap.get("arrival").toString() : "";
											sendMessageMap.put("body", "상차지:"+departure+",하차지:"+arrival+",차종:"+carKind+",차대번호:"+carIdNum);
											
											sendMessageMap.put("fcm_token", driverMap.get("fcm_token").toString());
											sendMessageMap.put("device_os", driverMap.get("device_os").toString());
											sendMessageMap.put("allocationId", allocationMap.get("allocation_id").toString());
											fcmService.fcmSendMessage(sendMessageMap);	
											Map<String, Object> alarmTalkMap = new HashMap<String, Object>();
											alarmTalkMap.put("gubun", "1");			//알림톡 발송 구분		1:기사배정,2:상차완료,3:하차완료,4:인계완료,5:탁송완료,6탁송취소,7:배차정보(기사님에게),8:배차승인(배차담당직원)
											alarmTalkMap.put("departure", carInfoMap.get("departure").toString());
											alarmTalkMap.put("arrival", carInfoMap.get("arrival").toString());
											alarmTalkMap.put("driver_name", driverMap.get("driver_name"));
											alarmTalkMap.put("driver_id", driverMap.get("driver_id"));
											alarmTalkMap.put("car_kind", carInfoMap.get("car_kind").toString());
											alarmTalkMap.put("car_id_num", carInfoMap.get("car_id_num").toString());
											alarmTalkMap.put("departure_dt", carInfoMap.get("departure_dt").toString());
											alarmTalkMap.put("phone_num", driverMap.get("phone_num").toString());
											alarmTalkMap.put("customer_id", customer.get("customer_id").toString());
											alarmTalkMap.put("allocation_id", allocationId);
											alarmTalkMap.put("departure_phone", carInfoMap.get("departure_phone").toString());
											alarmTalkMap.put("arrival_phone", carInfoMap.get("arrival_phone").toString());
											alarmTalkMap.put("alarmOrSmsYn", customer.get("alarm_or_sms_yn").toString());
											
											//alarmTalkMap.put("customer_id", allocationVO.getCustomerId());
											//티케이물류 문자 발송시 문자 내용에 고객명 추가.
											String personInChargeStr = carInfoMap.get("arrival_person_in_charge") != null && !carInfoMap.get("arrival_person_in_charge").toString().equals("") ? carInfoMap.get("arrival_person_in_charge").toString() : "";
											personInChargeStr = personInChargeStr.replaceAll(" ", "");
											personInChargeStr = personInChargeStr.trim();
											
											String[] personInCharge = personInChargeStr.split(";");  
											alarmTalkMap.put("person_in_charge",personInCharge[0]);
											
											alarmTalkMap.put("payment_kind",paymentKind);
											
											if(carInfoMap.get("departure_phone") != null && !carInfoMap.get("departure_phone").toString().equals("")) {
												if(WebUtils.isNumber(carInfoMap.get("departure_phone").toString().replaceAll("-", "").replaceAll(" ", ""))) {
													alarmTalkMap.put("customer_phone", allocationList.get(j).get("charge_phone").toString()+";"+carInfoMap.get("departure_phone").toString().replaceAll("-", "").replaceAll(" ", ""));
												}else {
													alarmTalkMap.put("customer_phone", allocationList.get(j).get("charge_phone").toString());	
												}
											}else {
												alarmTalkMap.put("customer_phone", allocationList.get(j).get("charge_phone").toString());	
											}
											
											//alarmTalkMap.put("customer_phone", allocationList.get(j).get("charge_phone").toString());
											alarmTalkMap.put("alarm_talk_yn", customer.get("alarm_talk_yn").toString());
											//alarmTalkMap.put("send_account_info_yn", customer.get("send_account_info_yn").toString());
											alarmTalkMap.put("send_account_info_yn", sendAccountInfoYn);
											alarmTalkMap.put("customer_kind",customer.get("customer_kind").toString());
											alarmTalkMap.put("billing_division",billingDivision);
											alarmTalkMap.put("alarmOrSmsYn", customer.get("alarm_or_sms_yn").toString());
											if(customer.get("alarm_talk_yn").toString().equals("Y") && allocationList.get(j).get("charge_phone") != null && !allocationList.get(j).get("charge_phone").toString().equals("")) {
												alarmTalkService.alarmTalkSends(alarmTalkMap);	
											}
											if(customer.get("sms_yn").toString().equals("Y")) {
												sendSmsService.smsSend(alarmTalkMap);
											}
											
											Map<String, Object> driverSetEmpMap = new HashMap<String, Object>();
											driverSetEmpMap.put("empId", userMap.get("emp_id").toString());
											driverSetEmpMap.put("empName", userMap.get("emp_name").toString());
											driverSetEmpMap.put("allocationId",allocationMap.get("allocation_id").toString());
											driverSetEmpMap.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
											driverSetEmpMap.put("logType","U");
											carInfoService.updateCarInfoDriverSetEmp(driverSetEmpMap);
											carInfoService.insertCarInfoLogByMap(driverSetEmpMap); //로그기록
										}
									}
									allocationService.updateAllocationStatus(map);			
									map.put("logType", "U");
									allocationService.insertAllocationLogByMap(map); //로그기록
								}
								map.put("allocationStatus", "");
							}	
						}else {
							result.setResultCode("0003");
							result.setResultMsg("기사 정보를 찾을 수 없습니다.");
						}	
						
					}else {
						result.setResultCode("0002");
						result.setResultMsg("운행 정보를 찾을 수 없습니다.");
					}
				}else {				//픽업건인 경우는 해당 배차의 기사정보만 업데이트 한다.
					
					deductionRate = driverMap.get("deduction_rate") != null && !driverMap.get("deduction_rate").toString().equals("") ? driverMap.get("deduction_rate").toString(): "";			//해당 기사의 공제율
					
						//Map<String, Object> allocation = allocationList.get(j);
						map.put("allocationId", allocationMap.get("allocation_id").toString());
						map.put("driverId", driverId);
						map.put("driverName", driverMap.get("driver_name"));
						map.put("driverCnt", driverCnt);
						carInfoService.updateCarInfoDriver(map);
						//거래처정보
						Map<String, Object> customerFindMap = new HashMap<String, Object>();
						customerFindMap.put("customerId", allocationMap.get("customer_id").toString());
						Map<String , Object> customer = customerService.selectCustomer(customerFindMap);
						
						String sendAccountInfoYn = customer.get("send_account_info_yn").toString();
						
						//결제 정보가 입력 되지 않은경우 상태를 변경 하지 않는다.
						List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(map);
						String paymentKind = "";
						String billingDivision = "";
						boolean paymentStatus = true;
						for(int k = 0; k < paymentInfoList.size(); k++) {//배차 등록시 결제 정보가 입력 되지 않은경우 기사가 배정 되어 있어도 미배차 상태로 표시 한다.
							Map<String, Object> paymentInfo = paymentInfoList.get(k);
							if(paymentInfo.get("payment_division").toString().equals("01")) {		//매출처
								
								//결제가 완료된 배차건의 경우 계좌 정보를 발송 하지 않는다..
								if(paymentInfo.get("payment").toString().equals("Y")) {
									sendAccountInfoYn = "N";
								}
								
								if(paymentInfo.get("payment_kind") != null) {
									if(paymentInfo.get("payment_kind").toString().equals("DD")) {		//결제 방법이 기사 수금이면
										paymentKind = "DD";
									}else {
										paymentKind = paymentInfo.get("payment_kind").toString();
									}
									if(paymentInfo.get("billing_division") != null) {
										billingDivision = paymentInfo.get("billing_division").toString();
									}
								}
							}
							
							if(paymentInfo.get("payment_division").toString().equals("01") && paymentInfo.get("amount") != null && paymentInfo.get("amount").toString().equals("")){
								paymentStatus = false;
							}else if(customerId.equals("") && paymentInfo.get("payment_division").toString().equals("02")){
								if(paymentInfo.get("amount") != null && paymentInfo.get("amount").toString().equals("")){
									paymentStatus = false;
								}	
								//deduction_rate 공제율
								//bill_for_payment 최종지급액
								//amount	기사지급액
								Map<String, Object> paymentUpdateMap = new HashMap<String, Object>();
								paymentUpdateMap.put("allocationId", allocationMap.get("allocation_id").toString());
								paymentUpdateMap.put("deductionRate", deductionRate);
								paymentUpdateMap.put("paymentDivision", paymentInfo.get("payment_division").toString());
								//결제 정보에 기사 정보를 업데이트 한다.
								paymentUpdateMap.put("paymentPartnerId", driverId);
								paymentUpdateMap.put("paymentPartner", driverMap.get("driver_name").toString());
								paymentUpdateMap.put("modId", userMap.get("emp_id").toString()); //수정자 
								paymentUpdateMap.put("logType","U");
								paymentInfoService.updatePaymentInfoDriverInfo(paymentUpdateMap);
								
								if(paymentInfo.get("amount") != null && !paymentInfo.get("amount").toString().equals("")) {
									if(!paymentInfo.get("amount").toString().equals("0") && !paymentInfo.get("amount").toString().equals("")) {
										String amount = paymentInfo.get("amount").toString();
										amount = amount.replaceAll(",", "");
										int billForPayment = Integer.parseInt(amount)-(Integer.parseInt(deductionRate)*Integer.parseInt(amount)/100);
										paymentUpdateMap.put("billForPayment", String.valueOf(billForPayment));
										paymentInfoService.updatePaymentInfoDriverAmount(paymentUpdateMap);
									}else {
										//0이면
										
										paymentInfoService.updatePaymentInfoDriverAmount(paymentUpdateMap);
									}
								}
								
								paymentInfoService.insertPaymentInfoLogByMap(paymentUpdateMap); //로그기록
							}else if(!customerId.equals("") && paymentInfo.get("payment_division").toString().equals("03")){		//거래처 기사인 경우
								if(paymentInfo.get("amount") != null && paymentInfo.get("amount").toString().equals("")){
									paymentStatus = false;
								}
								Map<String, Object> paymentUpdateMap = new HashMap<String, Object>();
								paymentUpdateMap.put("allocationId", allocationMap.get("allocation_id").toString());
								paymentUpdateMap.put("paymentDivision", "02");
								Map<String, Object> savePaymentInfo = paymentInfoService.selectPaymentInfoByPaymentDivision(paymentUpdateMap);
								
								paymentUpdateMap.put("paymentDivision", paymentInfo.get("payment_division").toString());
								paymentUpdateMap.put("paymentPartnerId", customerId);
								paymentUpdateMap.put("paymentPartner", customerName);
								paymentUpdateMap.put("paymentKind", savePaymentInfo.get("payment_kind").toString());
								paymentUpdateMap.put("billingDivision", savePaymentInfo.get("billing_division").toString());
								paymentUpdateMap.put("amount", savePaymentInfo.get("amount").toString());
								paymentUpdateMap.put("modId", userMap.get("emp_id").toString()); //수정자 
								paymentUpdateMap.put("logType","U");
								paymentInfoService.updatePaymentInfoDriverInfo(paymentUpdateMap);
								paymentInfoService.insertPaymentInfoLogByMap(paymentUpdateMap); //로그기록
								
							}
							
							/*if(paymentInfo.get("payment_division").toString().equals("01") || paymentInfo.get("payment_division").toString().equals("02")) {
								if(paymentInfo.get("amount") != null && paymentInfo.get("amount").toString().equals("")){
									paymentStatus = false;
								}
								if(paymentInfo.get("payment_division").toString().equals("02")) {		//해당 기사의 공제율을 적용 한다.
										//deduction_rate 공제율
										//bill_for_payment 최종지급액
										//amount	기사지급액
									Map<String, Object> paymentUpdateMap = new HashMap<String, Object>();
									paymentUpdateMap.put("allocationId", allocationMap.get("allocation_id").toString());
									paymentUpdateMap.put("deductionRate", deductionRate);
									paymentUpdateMap.put("paymentDivision", paymentInfo.get("payment_division").toString());
									//결제 정보에 기사 정보를 업데이트 한다.
									paymentUpdateMap.put("paymentPartnerId", driverId);
									paymentUpdateMap.put("paymentPartner", driverMap.get("driver_name"));
									paymentInfoService.updatePaymentInfoDriverInfo(paymentUpdateMap);
									if(paymentInfo.get("amount") != null && !paymentInfo.get("amount").toString().equals("")) {
										if(!paymentInfo.get("amount").toString().equals("0")) {
											String amount = paymentInfo.get("amount").toString();
											amount = amount.replaceAll(",", "");
											int billForPayment = Integer.parseInt(amount)-(Integer.parseInt(deductionRate)*Integer.parseInt(amount)/100);
											paymentUpdateMap.put("billForPayment", String.valueOf(billForPayment));
											paymentInfoService.updatePaymentInfoDriverAmount(paymentUpdateMap);
										}else {
											//0이면
											
											
											paymentInfoService.updatePaymentInfoDriverAmount(paymentUpdateMap);
										}
									}
										
								}
							}*/
							
							
						}
						if(paymentStatus) {
							if(driverMap.get("customer_id") != null && !driverMap.get("customer_id").toString().equals("")) {		//거래처 소속
								map.put("allocationStatus", BaseAppConstants.ALLOCATION_STATUS_Y);	
							}else {
								map.put("allocationStatus", BaseAppConstants.ALLOCATION_STATUS_A);
								//기사가 배정 된 경우 해당 기사에게 푸시 알림을 보낸다.
								if(driverMap.get("fcm_token") != null && !driverMap.get("fcm_token").toString().equals("")) {
									Map<String, Object> carInfoMap = carInfoService.selectCarInfo(map);
									Map<String, Object> sendMessageMap = new HashMap<String, Object>();
									sendMessageMap.put("title", "배차 정보가 도착 했습니다.");
									sendMessageMap.put("body", "상차지:"+carInfoMap.get("departure").toString()+",하차지:"+carInfoMap.get("arrival").toString()+",차종:"+carInfoMap.get("car_kind").toString()+",차대번호:"+carInfoMap.get("car_id_num").toString());
									sendMessageMap.put("fcm_token", driverMap.get("fcm_token").toString());
									sendMessageMap.put("device_os", driverMap.get("device_os").toString());
									sendMessageMap.put("allocationId", allocationMap.get("allocation_id").toString());
									fcmService.fcmSendMessage(sendMessageMap);	
									Map<String, Object> alarmTalkMap = new HashMap<String, Object>();
									alarmTalkMap.put("gubun", "1");			//알림톡 발송 구분		1:기사배정,2:상차완료,3:하차완료,4:인계완료,5:탁송완료,6탁송취소,7:배차정보(기사님에게),8:배차승인(배차담당직원)
									alarmTalkMap.put("departure", carInfoMap.get("departure").toString());
									alarmTalkMap.put("arrival", carInfoMap.get("arrival").toString());
									alarmTalkMap.put("driver_name", driverMap.get("driver_name"));
									alarmTalkMap.put("driver_id", driverMap.get("driver_id"));
									alarmTalkMap.put("car_kind", carInfoMap.get("car_kind").toString());
									alarmTalkMap.put("car_id_num", carInfoMap.get("car_id_num").toString());
									alarmTalkMap.put("departure_dt", carInfoMap.get("departure_dt").toString());
									alarmTalkMap.put("phone_num", driverMap.get("phone_num").toString());
									alarmTalkMap.put("customer_id", customer.get("customer_id").toString());
									alarmTalkMap.put("allocation_id", allocationId);
									alarmTalkMap.put("departure_phone", carInfoMap.get("departure_phone").toString());
									alarmTalkMap.put("arrival_phone", carInfoMap.get("arrival_phone").toString());
									//alarmTalkMap.put("customer_id", allocationVO.getCustomerId());
									
									//티케이물류 문자 발송시 문자 내용에 고객명 추가.
									String personInChargeStr = carInfoMap.get("arrival_person_in_charge") != null && !carInfoMap.get("arrival_person_in_charge").toString().equals("") ? carInfoMap.get("arrival_person_in_charge").toString() : "";
									personInChargeStr = personInChargeStr.replaceAll(" ", "");
									personInChargeStr = personInChargeStr.trim();
									
									String[] personInCharge = personInChargeStr.split(";");  
									alarmTalkMap.put("person_in_charge",personInCharge[0]);
									
									alarmTalkMap.put("payment_kind",paymentKind);
									alarmTalkMap.put("customer_phone", allocationMap.get("charge_phone").toString());
									alarmTalkMap.put("alarm_talk_yn", customer.get("alarm_talk_yn").toString());
									//alarmTalkMap.put("send_account_info_yn", customer.get("send_account_info_yn").toString());
									alarmTalkMap.put("send_account_info_yn", sendAccountInfoYn);
									alarmTalkMap.put("customer_kind",customer.get("customer_kind").toString());
									alarmTalkMap.put("billing_division",billingDivision);
									alarmTalkMap.put("alarmOrSmsYn", customer.get("alarm_or_sms_yn").toString());
									try {
										if(customer.get("alarm_talk_yn").toString().equals("Y") && allocationMap.get("charge_phone") != null && !allocationMap.get("charge_phone").toString().equals("")) {
											alarmTalkService.alarmTalkSends(alarmTalkMap);	
										}
										if(customer.get("sms_yn").toString().equals("Y")) {
											sendSmsService.smsSend(alarmTalkMap);
										}	
									}catch(Exception e) {
										e.printStackTrace();
									}
									
									Map<String, Object> driverSetEmpMap = new HashMap<String, Object>();
									driverSetEmpMap.put("empId", userMap.get("emp_id").toString());
									driverSetEmpMap.put("empName", userMap.get("emp_name").toString());
									driverSetEmpMap.put("allocationId",allocationMap.get("allocation_id").toString());
									driverSetEmpMap.put("modId",userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자추가
									driverSetEmpMap.put("logType","U");
									carInfoService.updateCarInfoDriverSetEmp(driverSetEmpMap);
									carInfoService.insertCarInfoLogByMap(driverSetEmpMap); //로그기록
								}
							}
							allocationService.updateAllocationStatus(map);
							map.put("logType","U");
							allocationService.insertAllocationLogByMap(map); //로그기록
						}
						map.put("allocationStatus", "");
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
			result.setResultMsg("오류가 발생 하였습니다.\r\n 관리자에게 문의 하세요.");
		}
		
		return result;
	}
	
	
	
	@RequestMapping(value = "/getDistance", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi getDistance(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
		
			String departure = WebUtils.getAddressInfo(request.getParameter("departureAddr").toString());
			String arrival = WebUtils.getAddressInfo(request.getParameter("arrivalAddr").toString());
			
			
			String resultStr = "&start="+WebUtils.getAddressToLatLng(departure)+"&destination="+WebUtils.getAddressToLatLng(arrival);
	        
			String[] departurePointArr = WebUtils.getAddressToLatLng(departure).split(",");
			String[] arrivalPointArr = WebUtils.getAddressToLatLng(arrival).split(",");
			
			BufferedReader    oBufReader = null;
	        HttpURLConnection httpConn   = null;
	        
	        JSONParser jsonParser = new JSONParser();
	        
	        JSONObject jsonDeparture = (JSONObject)jsonParser.parse(departure);
	        JSONObject jsonArrival = (JSONObject)jsonParser.parse(arrival);
	        
	        JSONObject depatureMeta =(JSONObject)jsonParser.parse(jsonDeparture.get("meta").toString());
	        JSONObject arrivalMeta =(JSONObject)jsonParser.parse(jsonArrival.get("meta").toString());
	        
	        
	        if(depatureMeta.get("total_count").toString().equals("0")) {
				result.setResultCode("0002");
				return result;
			}
	        
			if(arrivalMeta.get("total_count").toString().equals("0")) {
				result.setResultCode("0003");
				return result;
			}

	        

	        //tmap 프로젝트 정보
	        //프로젝트 코드 : 	project_191025141908PT7
	        //프로젝트 키 : l7xx6b701ff5595c4e3b859e42f57de0c975
	        
	       // https://apis.openapi.sk.com/tmap/routes
	        	        
	        
	        //appKey=l7xx6b701ff5595c4e3b859e42f57de0c975
	        //version=1
	        //&endX
	        //&endY
	        //&startX
	        //&startY
	        //&carType=5
	        
	        //경로 탐색 옵션 1입니다. - 0: 교통최적+추천(기본값) - 1: 교통최적+무료우선 - 2: 교통최적+최소시간 - 3: 교통최적+초보 - 4: 교통최적+고속도로우선 - 10: 최단
	        //searchOption=0
	        //응답결과를 선택합니다. 1: 자동차 경로안내 API의 전체 응답 데이터를 받을 경우 2: totalDistance(총 길이), totalTime(총 소요 시간), totalFare(총 요금 정보), taxiFare(택시 예상 요금 정보)의 정보만 받을 경우
	        //totalValue=2
	        
	        //String strEncodeUrl = "http://map.naver.com/spirra/findCarRoute.nhn?route=route3&output=json&result=web3&coord_type=naver&search=0&car=0&mileage=12.4&start="+departure+","+departureAdd+"&destination="+destination+","+destinationAdd;
	        
	        
	        String strEncodeUrl = "https://apis.openapi.sk.com/tmap/routes?&appKey=l7xx6b701ff5595c4e3b859e42f57de0c975&"
	        		+ "&version=1&endX="+arrivalPointArr[0]+"&endY="+arrivalPointArr[1]
	        		+ "&startX="+departurePointArr[0]+"&startY="+departurePointArr[1]+"&carType=5";
	        
	        URL oOpenURL = new URL(strEncodeUrl);
	      
	        httpConn =  (HttpURLConnection) oOpenURL.openConnection();          
	        httpConn.setRequestMethod("GET");          
	        httpConn.connect(); 
	        Thread.sleep(100);
	        oBufReader = new BufferedReader(new InputStreamReader(oOpenURL.openStream()));
	        
	      //  JSONParser jsonParser = new JSONParser();
	        JSONObject jsonObject = (JSONObject)jsonParser.parse(oBufReader);
	        
	        String jsonString = jsonObject.toJSONString();
	        
	        //System.out.println(jsonString);
	        
	        
	        Object obj = JSONValue.parse(jsonString);
	        JSONObject jsonObj = (JSONObject)obj;
	        
	        JSONArray list = (JSONArray)jsonObject.get("features");
	        JSONObject jsonObj2 = (JSONObject)list.get(0); 
	        
	        JSONObject summary = (JSONObject)jsonObj2.get("geometry"); 
	        JSONObject properties = (JSONObject)jsonObj2.get("properties");
	        
	        String pointType = properties.get("pointType").toString();
	        String totalDistance = properties.get("totalDistance").toString();
	        
	        String resultPayment = allocationService.paymentPrice(totalDistance);
	        
	 
	        
	        DecimalFormat form = new DecimalFormat("#.#");
	        float distanceF = Float.valueOf(totalDistance)/1000;
	        String distance =String.valueOf(form.format(distanceF));

	        Map<String, Object> map = new HashMap<String, Object>();
	        map.put("distance", distance);
	        map.put("departure", WebUtils.getAddressToLngLat(departure));
	        map.put("arrival", WebUtils.getAddressToLngLat(arrival));
	        map.put("resultPayment", resultPayment);
	        
			result.setResultData(map);
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	@RequestMapping(value = "/routeView", method = RequestMethod.GET)
	public ModelAndView routeView(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute AllocationVO allocationVO) throws Exception {
		
		try{
			
			String departure = request.getParameter("departure"); 
			String arrival = request.getParameter("arrival");
			
			String departureArr[] = departure.split(",");
			String arrivalArr[] = arrival.split(",");
			
			Map<String,Double> map = new HashMap<String,Double>();
			
			map.put("departure_x", Double.parseDouble(departureArr[0]));
			map.put("departure_y", Double.parseDouble(departureArr[1]));
			map.put("arrival_x", Double.parseDouble(arrivalArr[0]));
			map.put("arrival_y", Double.parseDouble(arrivalArr[1]));
			
			if(map.get("departure_x")  > map.get("arrival_x")){
    			map.put("center_x",map.get("departure_x")-((map.get("departure_x")-map.get("arrival_x"))/2));
    		}else{
    			map.put("center_x",map.get("arrival_x")-((map.get("arrival_x")-map.get("departure_x"))/2));
    		}
    		if(map.get("departure_y")  > map.get("arrival_x")){
    			map.put("center_y",map.get("departure_y")-((map.get("departure_y")-map.get("arrival_y"))/2));
    		}else{
    			map.put("center_y",map.get("arrival_y")-((map.get("arrival_y")-map.get("departure_y"))/2));
    		}
			
    		mav.addObject("map", map);
    		mav.addObject("departure", departure);
    		mav.addObject("arrival", arrival);
    		
    		
    		String jsonStrings = new String();
    		try {
    			String buf;
    	        URL url = new URL("https://naveropenapi.apigw.ntruss.com/map-direction/v1/driving?cartype=5&start="+departureArr[1]+","+departureArr[0]+"&goal="+arrivalArr[1]+","+arrivalArr[0]+"&option=trafast:traoptimal");
    	        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
    	        conn.setRequestMethod("GET");
    	        conn.setRequestProperty("X-Requested-With", "curl");
    	        conn.setRequestProperty("X-NCP-APIGW-API-KEY-ID", "t1kzgrc3sv");
    	        conn.setRequestProperty("X-NCP-APIGW-API-KEY", "CXGKmrSguaEOzEBc1bQy7Egqiq4gsdoat4vLeo2z");
    	        
    	        BufferedReader br = new BufferedReader(new InputStreamReader(
    	                conn.getInputStream(), "UTF-8"));
    	        while ((buf = br.readLine()) != null) {
    	            jsonStrings += buf;
    	        }	
    	        
    	        JSONParser jsonParser = new JSONParser();
    	        JSONObject jsonObject = (JSONObject)jsonParser.parse(jsonStrings);
    	        String jsonString = jsonObject.toJSONString();
    	        Object obj = JSONValue.parse(jsonString);
    	        //JSONObject jsonObj = (JSONObject)obj;
    	        
    	        	JSONObject route = (JSONObject)jsonObject.get("route");
        	        JSONArray traoptimal = (JSONArray)route.get("traoptimal");
        	        JSONObject jsonObj2 = (JSONObject)traoptimal.get(0);
        	        //JSONObject summary = (JSONObject)jsonObj2.get("summary");
        	        JSONArray pathList = (JSONArray)jsonObj2.get("path");
        	        
        	        List<Map<String, Double>> posList = new ArrayList<Map<String, Double>>();
        	        for(int i = 0; i < pathList.size(); i++) {
        	        	Map<String, Double> pos = new HashMap<String, Double>();
        	        	JSONArray path = (JSONArray)pathList.get(i);
        	        	pos.put("x",Double.parseDouble(path.get(1).toString()));
        	        	pos.put("y",Double.parseDouble(path.get(0).toString()));
        	        	posList.add(pos);
        	        //	System.out.println(pos);
        	        }
        	        mav.addObject("posList", posList);
    	        
    		}catch(Exception e) {
    			e.printStackTrace();
    		}
    		
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	@RequestMapping(value = "/viewDriverLocation", method = RequestMethod.GET)
	public ModelAndView viewDriverLocation(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute AllocationVO allocationVO) throws Exception {
		
		try{
			
			
			Map<String, Object> map = new HashMap<String, Object>();
			
			List<Map<String, Object>> driverList = driverService.selectDriverListForDriverLocation(map);			
			
			mav.addObject("driverList", driverList);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	
	
	
	
	//차대번호 중복체크 a.jax
	@RequestMapping(value = "/duplicationCaridnum-check", method = {RequestMethod.POST,RequestMethod.GET})
	@ResponseBody
	public ResultApi duplicationCaridnumCheck(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response ,AlarmSocketVO alarmSocketVO
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
					
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			List<Map<String,Object>> carIdNumDoubleCheckList = carInfoService.selectCarIdNumDubleCheckList(paramMap);
			
			if(carIdNumDoubleCheckList.size() > 0) {
				result.setResultCode("0000");
				result.setResultData(carIdNumDoubleCheckList);
		
			}else {
			result.setResultCode("0001");
				
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0002");
		}
		
		return result;
	}
	
	
	
	
	
	
	// 차대번호 팝업 컨트롤러
	@RequestMapping(value = "/doubleCheck-caridNum", method = RequestMethod.GET)
	public ModelAndView doubleCheckCaridNum(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute AllocationVO allocationVO) throws Exception {
		
		try{
			
			
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			// String companyId = session.getAttribute("companyId").toString();
			
			

			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			
			String[] allocationIdArr = request.getParameter("allocationIdArr").toString().split(",");
			
			List<String> allocationIdList = Arrays.asList(allocationIdArr);
			//String[]  allocationIdList = request.getParameter("allocationIdList").toString();
			//allocationIdList.split(",");
	
			String location = "duplication-list";
			paramMap.put("location", location);
			paramMap.put("allocationIdList", allocationIdList);
	
			
				
			int totalCount = allocationService.selectAllocationListCount(paramMap);
			PagingUtils.setPageing(request, totalCount, paramMap);
			paramMap.put("carrierType", "");
			List<Map<String, Object>> allocationList = allocationService.selectAllocationList(paramMap);
			
			Map<String, Object> map = new HashMap<String, Object>();
			List<Map<String, Object>> companyList = companyService.selectCompanyList(map);

			mav.addObject("userMap",  userMap);
			mav.addObject("companyList",  companyList);
			mav.addObject("listData",  allocationList);
			
			mav.addObject("paramMap", paramMap);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
			
			List<Map<String, Object>> allocationStatusList = allocationStatusService.selectAllocationStatusList(map);
			mav.addObject("allocationStatusList", allocationStatusList);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	@RequestMapping(value = "/viewDriverRoute", method = RequestMethod.GET)
	public ModelAndView viewDriverRoute(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute AllocationVO allocationVO) throws Exception {
		
		try{
			
			
			Map<String, Object> map = new HashMap<String, Object>();
			String driverId = request.getParameter("driverId").toString();
			String today = WebUtils.getNow("yyyy-MM-dd");
			map.put("driverId", driverId);
			map.put("today", today);
			
			List<Map<String, Object>> driverRoute = driverLocationInfoService.selectDriverLocationInfoList(map);
			mav.addObject("driverRoute", driverRoute);
			
			if(driverRoute.size() > 2) {			//경로 정보가 없으면 어떻게 하지??
				
				Map<String, Object> departure = driverRoute.get(0);
				Map<String, Object> arrival = driverRoute.get(driverRoute.size()-1);
				
				Double departure_x = Double.parseDouble(departure.get("lat").toString());
				Double departure_y = Double.parseDouble(departure.get("lng").toString());
				Double arrival_x = Double.parseDouble(arrival.get("lat").toString());
				Double arrival_y = Double.parseDouble(arrival.get("lng").toString());
				map.put("departure_x", departure_x);
				map.put("departure_y", departure_y);
				map.put("arrival_x", arrival_x);
				map.put("arrival_y", arrival_y);
				
				if(departure_x > arrival_x){
	    			map.put("center_x",departure_x-((departure_x-arrival_x)/2));
	    		}else{
	    			map.put("center_x",arrival_x-((arrival_x-departure_x)/2));
	    		}
	    		if(departure_y  > arrival_x){
	    			map.put("center_y",departure_y-((departure_y-arrival_y)/2));
	    		}else{
	    			map.put("center_y",arrival_y-((arrival_y-departure_y)/2));
	    		}
				
	    		mav.addObject("map", map);	
			}else {
				StringBuffer sb1 = new StringBuffer();
				 sb1.append("<script type='text/javascript'>");
				 sb1.append("alert('선택한 기사의 이동 경로가 없습니다.');");
				 sb1.append("self.close();");
				 sb1.append("</script>");

				 response.setContentType("text/html; charset=UTF-8");
				 PrintWriter out1 = response.getWriter();
				 out1.println(sb1);
				 out1.flush();
				
				
				//WebUtils.messageAndRedirectUrl(mav,"선택한 기사의 이동 경로가 없습니다.", "/allocation/allocation-register.do");
			}
			
    		
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	
	
	
	
	
	
	@RequestMapping(value = "/allocation-excel-insert", method = RequestMethod.POST)
	public ModelAndView allocationExcelInsert(ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception{
		
		ResultApi resultApi = new ResultApi();
		int inputDataRow = 0;
		String cause = "";					//업로드 실패시 원인을 확인 할 수 있도록 한다.
		try{
			
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
	    	List<MultipartFile> fileList = multipartRequest.getFiles("bbsFile");
			
	    	Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
	    	paramMap.put("fileId", request.getAttribute("fileId").toString());
	    	Map<String, Object> fileMap = fileService.selectFile(paramMap);
	    	
	    	HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			String companyId = session.getAttribute("companyId").toString();
			
			
			List<AllocationVO> allocationList = new ArrayList<AllocationVO>();
			List<CarInfoVO> carInfoList = new ArrayList<CarInfoVO>();
			List<PaymentInfoVO> paymentInfoList = new ArrayList<PaymentInfoVO>();
			
			boolean duplication = false;
			List<String> allocationIdList = new ArrayList<String>();
			
	    	if(fileList != null && fileList.size() > 0){
	    		for(MultipartFile mFile : fileList){	
	    			if(mFile != null && mFile.getSize() > 0){
	    				/*
	    				Long fileSize = mFile.getSize();
	    				String fileName = mFile.getOriginalFilename();		
	    				String ext = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length());
	    				File file = new File(mFile.getOriginalFilename());
	    				mFile.transferTo(file);
	    				*/
	    				
	    				File file = new File(rootDir+"/"+"dupList"+fileMap.get("file_path").toString());
	    				
	    				XSSFWorkbook wb = null;
	    				XSSFRow row;
	    				XSSFCell cell;
	    				XSSFCell cellForSearch;
	    				try {//엑셀 파일 오픈
	    					wb = new XSSFWorkbook(new FileInputStream(file));
	    				} catch (FileNotFoundException e) {
	    					e.printStackTrace();
	    				} catch (IOException e) {
	    					e.printStackTrace();
	    				}
	    				
	    				XSSFSheet sheet = wb.getSheetAt(1);
	    		         //int rows = sheet.getPhysicalNumberOfRows();
	    				//int cells = sheet.getRow(0).getPhysicalNumberOfCells();
	    				int rows 
	    				= sheet.getLastRowNum();
	    				int cells = sheet.getRow(1).getLastCellNum();
	    				
	    				FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
	    				
	    				 for (int r = 1; r <= rows; r++) {
	    					 inputDataRow = r+1;
	    					 AllocationVO allocationVO = new AllocationVO();
	    					 CarInfoVO carInfoVO = new CarInfoVO();
	    					 Map<String, Object> driverMap = new HashMap<String, Object>();
	    					 Map<String, Object> customerMap = new HashMap<String, Object>();
	    					 String allocationId = "ALO"+UUID.randomUUID().toString().replaceAll("-","");
	    					 allocationVO.setAllocationId(allocationId);
	    					 //allocationVO.setAllocationStatus("N");
	    					 
	    					 
	    					 /*allocationVO.setBatchStatus("N");
	    					 allocationVO.setBatchStatusId("BAT"+UUID.randomUUID().toString().replaceAll("-",""));
	 						 allocationVO.setBatchIndex(String.valueOf(0));*/
	 						 allocationVO.setRegisterId(userMap.get("emp_id").toString());
	 						 allocationVO.setRegisterName(userMap.get("emp_name").toString());
	 						 
	 						 //allocationVO.setCompanyId("company2");				//2019.01.31 엑셀 등록시 사업자정보를 무조건 한국카캐리어(법인)으로 등록 되게 함.
	 						 allocationVO.setCompanyId(companyId);				//2019.12.27 엑셀 등록시 소속된 회사의 아이디를 넣도록 수정
	 						 
	 						Map<String, Object> searchMap = new HashMap<String, Object>();
	 						searchMap.put("companyId", companyId);
	 						Map<String, Object> company = companyService.selectCompany(searchMap);
	 						Map<String,Object> customer= new HashMap<String, Object>();
	 						
	 						
	 						allocationVO.setCompanyName(company.get("company_name").toString());
	    					carInfoVO.setAllocationId(allocationId);
	    					carInfoVO.setReceiptSendYn("N");
	    					carInfoVO.setModId(userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자추가
	    					allocationVO.setModId(userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자추가
	    					 String customerAmount = "";
	    					 String driverAmount = "";
	    					 String paymentKind = "";
	    					 String billingDivision = "";
	    					 FormulaEvaluator formulaEval = wb.getCreationHelper().createFormulaEvaluator();
	    					 
	    			        	row = sheet.getRow(r); // row 가져오기
	    			        	if (row != null) {
	    			        		for (int c = 0; c < cells; c++) {
	    			        			cell = row.getCell(c);
	    			        			if (cell != null) { 
	    			        				String value = "";

	    									switch (cell.getCellType()) {
	    									   	case XSSFCell.CELL_TYPE_FORMULA:
	    									   		
	    									   		CellValue evaluate = formulaEval.evaluate(cell);
	    									   	  if( evaluate != null ) {
	    									   		  
	    									   		  try {
	    									   			  
	    									   			  if(WebUtils.isNumber(evaluate.formatAsString())) {
	    									   				  
	    									   				value=  String.valueOf(new Double(evaluate.getNumberValue()).intValue());
	    									   				   
	    									   			  }else {
	    									   				  
	    									   				  value = evaluate.formatAsString();
	    									   			  }
	    									   			  
	    									   		  }catch(Exception e) {
	    									   			  e.printStackTrace();
	    									   			  
	    									   		  }
	    									   		  
	    									   		  
	 	    			        				   }else {
	 	    			        					   
	 	    			        					   value = "";
	 	    			        				   }
	    									   		
	    									   		/*
	    									   		if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_NUMERIC){
	    									   			value = "" + (int)cell.getNumericCellValue();
	    									   		}else if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_STRING){
	    									   			value = "" + cell.getStringCellValue(); 
	    									   		}
	    									   		value = cell.getCellFormula();
	    									   		*/
	    									   		
	    									   		break;
	    									   	case XSSFCell.CELL_TYPE_NUMERIC:
	    								            if (DateUtil.isCellDateFormatted(cell)){  
	    								                //날짜  
	    								            	Date date = cell.getDateCellValue();
	    								                value = String.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(date));
	    								            }else{  
	    								                //수치
	    								            	value = NumberToTextConverter.toText(cell.getNumericCellValue());
	    								            	
	    								                //value = String.valueOf(cell.getNumericCellValue());  
	    								            } 
	    									   		//value = "" + (int)cell.getNumericCellValue();
	    									   		break;
	    									   	case XSSFCell.CELL_TYPE_STRING:
	    									   		value = "" + cell.getStringCellValue();
	    									   		break;
	    									   	case XSSFCell.CELL_TYPE_BLANK:
	    									   		value = "";
	    									   		break;
	    									   	case XSSFCell.CELL_TYPE_ERROR:
	    									   		value = "" + cell.getErrorCellValue();
	    									   		break;
	    									   	default:
	    									}
	    									if(value != null && !value.equals("")) {
	    										value = value.trim();
	    									}
	    									int nIndex = cell.getColumnIndex();
	    									if(nIndex == 0){
	    										
	    										if(value != null && !value.equals("")) {
	    											Map<String, Object> map = new HashMap<String, Object>();
		    										map.put("allocationDivision",value);
		    										Map<String, Object> allocationDivision = allocationDivisionService.selectAllocationDivisionInfo(map);
		    										carInfoVO.setCarrierType(allocationDivision.get("allocation_division_cd").toString());	
	    										}else {		//배차 구분이 지정 되어 있지 않으면 익셉션
	    											cause = "배차 구분이 지정 되지 않았습니다.";
	    											throw new Exception();
	    										}
	    									}else if(nIndex == 1){
	    										Map<String, Object> map = new HashMap<String, Object>();
	    										if(!value.equals("")) {
	    											map.put("runDivision",value);
		    										Map<String, Object> runDivision = runDivisionService.selectRunDivisionInfo(map);	
		    										if(runDivision != null) {
		    											carInfoVO.setDistanceType(runDivision.get("run_division_cd").toString());			    											
		    										}else {
		    											carInfoVO.setDistanceType("C");	
		    										}
		    										
	    										}else {
	    											carInfoVO.setDistanceType("C");
	    										}
	    										
	    									}else if(nIndex == 2){
	    										
	    										if(!value.equals("")) {
	    											String dateDay = WebUtils.getDateDay(value);
		    										allocationVO.setInputDt(value+" ("+dateDay+")");	
	    										}else {
	    											String dateDay = WebUtils.getDateDay(WebUtils.getNow("yyyy-MM-dd"));
		    										allocationVO.setInputDt(WebUtils.getNow("yyyy-MM-dd")+" ("+dateDay+")");
	    										}
	    										
	    									}else if(nIndex == 3){
	    										carInfoVO.setDepartureDt(value);
	    									}else if(nIndex == 4){
	    										Map<String, Object> map = new HashMap<String, Object>();
	    										map.put("customerName",value);
	    										cellForSearch = row.getCell(6);
	    										String chargeName = "";
	    										
	    										if(cellForSearch != null) {
	    											chargeName = ""+cellForSearch.getStringCellValue();
	    										}
	    										if(!chargeName.equals("")) {
	    											map.put("chargeName",chargeName);	
	    											customer = customerService.selectCustomerByCustomerNameAndChargeName(map);
	    										}else {
	    											customer = customerService.selectCustomerByCustomerName(map);	
	    										}
	    										 
	    										if(customer != null) {
	    											
	    											if(!customer.get("black_list_yn").toString().equals("Y")) {
		    											allocationVO.setCustomerId(customer.get("customer_id").toString());
			    										allocationVO.setCustomerName(customer.get("customer_name").toString());
			    										allocationVO.setCustomerSignificantData(customer.get("significant_data") != null && !customer.get("significant_data").toString().equals("") ? customer.get("significant_data").toString():"");
	    											}else {
	    												cause = "해당 거래처가 블랙리스트로 등록 되어 있습니다.";
		    											throw new Exception();
	    											}
	    											
	    										}else {		//거래처가 없는경우 익셉션
	    											cause = "거래처가 등록 되어 있지 않습니다.";
	    											throw new Exception();
	    										}
	    									}else if(nIndex == 5){
	    										//업체(담당자 부서로 사용 하고 있으나 현재 배차 등록에는 필요 하지 않아 일단 사용하지 않음..
	    									}else if(nIndex == 6){
	    										//업체의 담당자가 한명만 있는경우 그 담당자가 해당 배차의 담당자가 되도록 진행. 
	    										Map<String, Object> map = new HashMap<String, Object>();
	    										map.put("customerId",allocationVO.getCustomerId());
	    										map.put("name",value);
	    										if(!value.equals("")) {
	    											Map<String, Object> personInCharge = personInChargeService.selectPersonInCharge(map);
	    											if(value.equals("현대캐피탈")) {
	    												allocationVO.setHcYn("Y");
	    											}
	    											
		    										if(personInCharge != null) {
		    											allocationVO.setChargeName(personInCharge.get("name").toString());
		    											allocationVO.setChargeId(personInCharge.get("person_in_charge_id").toString());
		    											allocationVO.setChargePhone(personInCharge.get("phone_num") != null && !personInCharge.get("phone_num").toString().equals("") ? personInCharge.get("phone_num").toString():"");
		    											allocationVO.setChargeAddr(personInCharge.get("address") != null && !personInCharge.get("address").toString().equals("") ? personInCharge.get("address").toString():"");
		    										}else {
		    											cause = "담당자 정보를 찾을 수 없습니다.";
		    											throw new Exception();	
		    										}
	    										}else {
	    											List<Map<String, Object>> personInChargeList = personInChargeService.selectPersonInChargeList(map);
	    											if(personInChargeList.size() == 1) {			//업체의 담당자가 한명인 경우 해당 배차의 담당자가 된다.
	    												Map<String, Object> personInCharge = personInChargeList.get(0);
	    												allocationVO.setChargeName(personInCharge.get("name").toString());
		    											allocationVO.setChargeId(personInCharge.get("person_in_charge_id").toString());
		    											allocationVO.setChargePhone(personInCharge.get("phone_num") != null && !personInCharge.get("phone_num").toString().equals("") ? personInCharge.get("phone_num").toString():"");
		    											allocationVO.setChargeAddr(personInCharge.get("address") != null && !personInCharge.get("address").toString().equals("") ? personInCharge.get("address").toString():"");
	    											}else {
		    											cause = "담당자가 지정되지 않았습니다.";
		    											throw new Exception();	
		    										}
	    										}
	    									}else if(nIndex == 7){
	    										//차종
	    										carInfoVO.setCarKind(value);
	    									}else if(nIndex == 8){
	    										//차대번호
	    										carInfoVO.setCarIdNum(value);
	    									}else if(nIndex == 9){
	    										//차량번호
	    										carInfoVO.setCarNum(value);
	    									}else if(nIndex == 10){
	    										//계약번호
	    										carInfoVO.setContractNum(value);
	    									}else if(nIndex == 11){
	    										//출발지
	    										if(value != null && !value.equals("")) {
	    											Map<String, Object> map = new HashMap<String, Object>();
		    										map.put("keyword", value);
		    										Map<String, Object> addressInfo = addressInfoService.selectAddressInfo(map);
		    										if(addressInfo != null) {
		    											carInfoVO.setDeparture(value);
		    											carInfoVO.setDepartureAddr(addressInfo.get("address") != null ? addressInfo.get("address").toString():"");
		    											carInfoVO.setDeparturePersonInCharge(addressInfo.get("name") != null ? addressInfo.get("name").toString():"");
		    											carInfoVO.setDeparturePhone(addressInfo.get("phone_num") != null ? addressInfo.get("phone_num").toString():"");
		    											
		    										}else {
		    											carInfoVO.setDeparture(value);
		    										}
	    										}else {
	    											carInfoVO.setDeparture(value);
	    										}	    										
	    									}else if(nIndex == 12){
	    										//출발지상세주소
	    										if(value != "") {
	    											carInfoVO.setDepartureAddr(value);
	    										}else {
	    											if(carInfoVO.getDepartureAddr() == null || carInfoVO.getDepartureAddr().equals("")) {
		    											carInfoVO.setDepartureAddr(value);	
		    										}	
	    										}
	    									}else if(nIndex == 13){
	    										//상차지담당자명
	    										if(value != "") {
	    											carInfoVO.setDeparturePersonInCharge(value);
	    										}else {
	    											if(carInfoVO.getDeparturePersonInCharge() == null || carInfoVO.getDeparturePersonInCharge().equals("")) {
		    											carInfoVO.setDeparturePersonInCharge(value);	
		    										}	
	    										}
	    									}else if(nIndex == 14){
	    										//상차지담당자연락처
	    										if(value != "") {
	    											carInfoVO.setDeparturePhone(value);
	    										}else {
	    											if(carInfoVO.getDeparturePhone() == null || carInfoVO.getDeparturePhone().equals("")) {
		    											carInfoVO.setDeparturePhone(value);	
		    										}	
	    										}
	    									}else if(nIndex == 15){
	    										//하차지
	    										if(value != null && !value.equals("")) {
	    											Map<String, Object> map = new HashMap<String, Object>();
		    										map.put("keyword", value);
		    										Map<String, Object> addressInfo = addressInfoService.selectAddressInfo(map);
		    										if(addressInfo != null) {
		    											carInfoVO.setArrival(value);
		    											carInfoVO.setArrivalAddr(addressInfo.get("address") != null ? addressInfo.get("address").toString():"");
		    											carInfoVO.setArrivalPersonInCharge(addressInfo.get("name") != null ? addressInfo.get("name").toString():"");
		    											carInfoVO.setArrivalPhone(addressInfo.get("phone_num") != null ? addressInfo.get("phone_num").toString():"");
		    										}else {
		    											carInfoVO.setArrival(value);
		    										}
	    										}else {
	    											carInfoVO.setArrival(value);
	    										}
	    									}else if(nIndex == 16){
	    										//하차지상세주소
	    										if(value != "") {
	    											carInfoVO.setArrivalAddr(value);
	    										}else {
	    											if(carInfoVO.getArrivalAddr() == null || carInfoVO.getArrivalAddr().equals("")) {
		    											carInfoVO.setArrivalAddr(value);	
		    										}	
	    										}
	    									}else if(nIndex == 17){
	    										//하차지담당자명
	    										if(value != "") {
	    											carInfoVO.setArrivalPersonInCharge(value);
	    										}else {
	    											if(carInfoVO.getArrivalPersonInCharge() == null || carInfoVO.getArrivalPersonInCharge().equals("")) {
		    											carInfoVO.setArrivalPersonInCharge(value);	
		    										}	
	    										}
	    									}else if(nIndex == 18){
	    										//하차지담당자연락처
	    										if(value != "") {
	    											carInfoVO.setArrivalPhone(value);
	    										}else {
	    											if(carInfoVO.getArrivalPhone() == null || carInfoVO.getArrivalPhone().equals("")) {
		    											carInfoVO.setArrivalPhone(value);	
		    										}	
	    										}
	    									}else if(nIndex == 19){
	    										//기사명					//엑셀 등록시 기사명이 등록 되어 있더라도 배차 정보에서 수정 할 수 있도록....	    										
	    										if(!value.equals("")) {
	    											String driverName = value;
		    										Map<String, Object> map = new HashMap<String, Object>();
		    										map.put("driverName", driverName);
		    										map.put("driverStatus", "01");
		    										
		    										driverMap = driverService.selectDriverByDriverName(map);
		    										
		    										if(driverMap != null) {
		    											carInfoVO.setDriverId(driverMap.get("driver_id").toString());
		    											carInfoVO.setDriverName(driverMap.get("driver_name").toString());	
		    										}
		    										//외부기사인 경우
		    										if(driverMap != null && driverMap.get("customer_id") != null && !driverMap.get("customer_id").toString().equals("")) {
		    											map.put("customerId", driverMap.get("customer_id").toString());
		    											customerMap = customerService.selectCustomer(map);
		    										}
	    										}
	    									}else if(nIndex == 20){
	    										//회전수
	    										if(!value.equals("")) {
	    											
	    											try {
	    												int driverCnt = Integer.parseInt(value);
	    											}catch(NumberFormatException e) {
	    												cause = "회전수가 잘못 입력 되었습니다.";
		    											throw new Exception();
	    											}
	    											
	    											//driverMap.put("driverCnt", value);
	    											if(driverMap != null && driverMap.get("driver_id") != null) {
	    												carInfoVO.setDriverCnt(value);	
	    											}else {
	    												cause = "회전수가 지정되었지만 기사가 지정되지 않았습니다.";
		    											throw new Exception();
	    											}
	    										}
	    									}else if(nIndex == 21){
	    										//특이사항//특이사항(비고)
	    										carInfoVO.setEtc(value);
	    									}else if(nIndex == 22){
	    										//car_cnt(대수)
	    										allocationVO.setCarCnt(value);
	    									}else if(nIndex == 23){
	    										//업체청구액
	    										if(value.equals("")) {
	    											value = "0";
	    										}
	    										customerAmount = value.replaceAll(",", "");
	    										carInfoVO.setSalesTotal(customerAmount);
	    										
	    									}else if(nIndex == 24){
	    										//부가세 구분
	    										
	    										if(!value.equals("")) {
	    											//포함인경우
	    											if(value.equals("포함")){
	    												carInfoVO.setVatIncludeYn("Y");
	    	    										carInfoVO.setVatExcludeYn("N");
	    	    										int total = Integer.parseInt(customerAmount);
	    	    										int vat = Integer.parseInt(customerAmount)/11;
	    	    										customerAmount = String.valueOf(Integer.parseInt(customerAmount)-vat);
	    	    										carInfoVO.setSalesTotal(customerAmount);
			    										carInfoVO.setVat(String.valueOf(vat));
			    										carInfoVO.setPrice(String.valueOf(total));
	    											//별도인경우
		    										}else if(value.equals("별도")){
		    											carInfoVO.setVatIncludeYn("N");
			    										carInfoVO.setVatExcludeYn("Y");
			    										int vat = Integer.parseInt(customerAmount)/10;
			    										int total = Integer.parseInt(customerAmount)+vat;
			    										carInfoVO.setVat(String.valueOf(vat));
			    										carInfoVO.setPrice(String.valueOf(total));
		    										}else {
		    											carInfoVO.setVat("0");
		    											carInfoVO.setVatIncludeYn("N");
			    										carInfoVO.setVatExcludeYn("N");
		    											carInfoVO.setPrice(customerAmount);
		    										}
	    										}else {
	    											//포함도 아니고 별도도 아닌경우
	    											carInfoVO.setVat("0");
	    											carInfoVO.setVatIncludeYn("N");
		    										carInfoVO.setVatExcludeYn("N");
	    											carInfoVO.setPrice(customerAmount);
	    										}
	    										
	    									}else if(nIndex == 25){
	    										//기사지급액
	    										if(value.equals("")) {
	    											value = "0";
	    										}
	    										driverAmount = value.replaceAll(",", "");
	    									}else if(nIndex == 28){
	    										
	    										//엑셀 업로드시 기사가 지정 된 경우 배차 승인 또는 배차 완료이다
	    										if(carInfoVO.getDriverId() != null && carInfoVO.getDriverCnt() != null && !carInfoVO.getDriverId().equals("") && !carInfoVO.getDriverCnt().equals("")) {
	    											//거래처 소속인 경우 탁송 완료
	    											if(customerMap != null && customerMap.get("customer_id") != null && !customerMap.get("customer_id").equals("")) {
	    												allocationVO.setAllocationStatus("F");	
	    												
	    											// 문자발송 여부가 Y일때 배차 승인대기 상태
	    												if(customer.get("sms_yn").equals("Y")) {
	    													allocationVO.setAllocationStatus("A");
	    												}
	    												
	    												
	    											}else {
	    												//거래처 소속이 아닌 경우는 배차 승인 대기 상태
	    												allocationVO.setAllocationStatus("A");	
	    											}
	    										}else {
	    											
	    											//기사 또는 회차 하나만 지정 되어 있는경우
	    											if(!carInfoVO.getDriverId().equals("") || !carInfoVO.getDriverCnt().equals("")) {
	    												if(!carInfoVO.getDriverId().equals("")) {
	    													cause = "기사가 지정 되었으나 회차가 지정 되지 않았습니다.";
			    											throw new Exception();
	    												}else {
	    													cause = "회차가 지정 되었으나 기사가 지정되지 않았습니다.";
			    											throw new Exception();	
	    												}
	    											}else {
	    												//미확정,미배차
		    											if(value.equals("미확정")){
			    											allocationVO.setAllocationStatus("U");	
			    										}else if(value.equals("미배차")){
			    											allocationVO.setAllocationStatus("N");
			    										}else {
			    											//미확정도 아니고 미배차도 아닌경우 미배차 상태로...
			    											allocationVO.setAllocationStatus("N");
			    										}	
	    											}
	    										
	    										}
	    										
	    									}else if(nIndex == 29) {		//픽업관리
	    										if(!value.equals("")) {		//픽업건인경우
	    											allocationVO.setPickupNum(value);
	    											allocationVO.setBatchStatus("P");
	    										}else {							//픽업 아님
	    											allocationVO.setPickupNum("");
	    											allocationVO.setBatchStatus("N");
	    										}
	    										
	    									}else if(nIndex == 30) {		//사진제한
	    										if(!value.equals("")) {
	    											try {
	    												 Double.parseDouble(value);
	    												 carInfoVO.setRequirePicCnt(value);
	    											}catch(NumberFormatException e) {
	    												cause = "사진제한은 숫자로만 입력 가능 합니다.";
		    											throw new Exception();
	    											}
	    										}else {							
	    											carInfoVO.setRequirePicCnt("3");
	    										}
	    									}else if(nIndex == 31) {		//사진제한
	    										if(!value.equals("")) {
	    											try {
	    												 Double.parseDouble(value);
	    												 carInfoVO.setRequireDnPicCnt(value);
	    											}catch(NumberFormatException e) {
	    												cause = "사진제한은 숫자로만 입력 가능 합니다.";
		    											throw new Exception();
	    											}
	    										}else {							
	    											carInfoVO.setRequireDnPicCnt("3");
	    										}
	    									}else if(nIndex == 32) {		//결제방법
	    										if(!value.equals("")) {	    											
	    											paymentKind = value;
	    										}else {							
	    											cause = "결제방법이 입력 되지 않았습니다.";
	    											throw new Exception();
	    										}
	    									}else if(nIndex == 33) {		//증빙구분
	    										if(!value.equals("")) {
	    											billingDivision = value;
	    										}else {							
	    											cause = "증빙구분이 입력 되지 않았습니다.";
	    											throw new Exception();
	    										}
	    									}
	    									allocationVO.setBatchIndex(String.valueOf(0));		//픽업건여부에 관계 없이 index는 0
	    			        			} else {
	    			        				System.out.print("[null]\t");
	    			        			}
	    			        		} // for(c) 문
	    			        			
	    			        		if(carInfoVO.getCarrierType() != null && !carInfoVO.getCarrierType().equals("")) {		//배차 구분이 작성 되지 않은경우 등록 하지 않는다.
	    			        			
	    			        			
	    			        			Map<String, Object> findMap = new HashMap<String, Object>();
	    			        			findMap.put("carIdNum", carInfoVO.getCarIdNum()!=null ? carInfoVO.getCarIdNum() : "");
	    			        			findMap.put("carNum", carInfoVO.getCarNum()!=null ? carInfoVO.getCarNum() : "");
	    			        			findMap.put("contractNum", carInfoVO.getContractNum()!=null ? carInfoVO.getContractNum() : "");
	    			        			findMap.put("departure", carInfoVO.getDeparture()!=null ? carInfoVO.getDeparture() : "");
	    			        			findMap.put("arrival", carInfoVO.getArrival()!=null ? carInfoVO.getArrival() : "");
	    			        			
	    			        			if(!findMap.get("carIdNum").toString().equals("") || !findMap.get("carNum").toString().equals("") || !findMap.get("contractNum").toString().equals("")) {
	    			        				Map<String, Object> findResult = carInfoService.selectCarInfoForDuplicate(findMap);
		    			        			if(findResult != null) {
		    			        				cause = "중복된 배차 정보가 있습니다.";
												//throw new Exception();
		    			        				duplication = true;
		    			        				allocationIdList.add(carInfoVO.getAllocationId());
		    			        			}	
	    			        			}
	    			        			
	    			        			if(carInfoVO.getRequirePicCnt() == null || carInfoVO.getRequirePicCnt().equals("")) {
	    			        				cause = "상차사진 제한이 입력 되지 않았습니다.";
											throw new Exception();
	    			        			}
	    			        			if(carInfoVO.getRequireDnPicCnt() == null || carInfoVO.getRequireDnPicCnt().equals("")) {
	    			        				cause = "하차사진 제한이 입력 되지 않았습니다.";
											throw new Exception();
	    			        			}
	    			        			
	    			        			carInfoList.add(carInfoVO);
	    			        			//allocationService.insertAllocation(allocationVO);
		    			        		//carInfoService.insertCarInfo(carInfoVO);
	    			        			int income = 0;
	    			        			
	    			        			if(paymentKind.equals("")) {
	    			        				cause = "결제방법이 입력 되지 않았습니다.";
											throw new Exception();
	    			        			}
	    			        			if(billingDivision.equals("")) {
	    			        				cause = "증빙구분이 입력 되지 않았습니다.";
											throw new Exception();
	    			        			}
	    			        			
		    			        		for(int j = 0; j < 3; j++){
		    								PaymentInfoVO paymentInfoVO = new PaymentInfoVO();
		    								paymentInfoVO.setAllocationId(allocationId);
		    								paymentInfoVO.setPaymentDivision("0"+(j+1));
		    								paymentInfoVO.setPayment("N");
		    								paymentInfoVO.setModId(userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자추가
		    								if(paymentInfoVO.getPaymentDivision().equals("01")) {
		    									paymentInfoVO.setAmount(customerAmount);
		    									paymentInfoVO.setPaymentPartner(allocationVO.getCustomerName());
		    									paymentInfoVO.setPaymentPartnerId(allocationVO.getCustomerId());
		    									paymentInfoVO.setPaymentKind(paymentKind);
		    									paymentInfoVO.setBillingDivision(billingDivision);
		    									paymentInfoVO.setVatIncludeYn(carInfoVO.getVatIncludeYn());
		    									paymentInfoVO.setVatExcludeYn(carInfoVO.getVatExcludeYn());
		    									
		    									income += Integer.parseInt(customerAmount);
		    								}else if(paymentInfoVO.getPaymentDivision().equals("02")) {
		    									
		    									if(customerMap == null || customerMap.get("customer_id") == null) {
		    										if(driverMap != null && driverMap.get("driver_name") != null) {
		    											paymentInfoVO.setPaymentPartner(driverMap.get("driver_name").toString());
		    											paymentInfoVO.setPaymentPartnerId(driverMap.get("driver_id").toString());
		    											paymentInfoVO.setDeductionRate(driverMap.get("deduction_rate").toString());
			    										int billForPayment = Integer.parseInt(driverAmount)-(Integer.parseInt(driverMap.get("deduction_rate").toString())*Integer.parseInt(driverAmount)/100);
			    										paymentInfoVO.setBillForPayment(String.valueOf(billForPayment));
			    										
			    										if(allocationVO.getHcYn().equals("Y") && driverMap.get("car_assign_company").toString().equals("S")){
			    											
			    											int deductionRate = 0;
			    											deductionRate = Integer.parseInt(driverMap.get("deduction_rate").toString());
			    											if(deductionRate > 10) {
			    		    			        				if(deductionRate == 15) {
			    		    			        					//지입기사이면 14% 아니면 15%
			    		    			        					if(driverMap.get("driver_kind") != null) {
			    		    			        						if(driverMap.get("driver_kind").equals("00") || driverMap.get("driver_kind").equals("03")) {
			    			    			        						deductionRate = 0;
			    			    			        					}else if(driverMap.get("driver_kind").equals("01")) {
			    			    			        						deductionRate = 4;
			    			    			        					}else if(driverMap.get("driver_kind").equals("02")) {
			    			    			        						deductionRate = 5;
			    			    			        					}else {
			    			    			        						deductionRate = 0;
			    			    			        					}
			    		    			        					}else {
			    		    			        						deductionRate = 0;
			    		    			        					}
			    		    			        					
			    		    			        				}else {
			    		    			        					deductionRate = deductionRate-10;
			    		    			        				}
			    		    			        			}else if(deductionRate == 10) {
			    		    			        				deductionRate = 0;
			    		    			        			} 
			    											
			    											int amount = Integer.parseInt(driverAmount);
			    											billForPayment = amount - (amount*deductionRate/100);
			    											paymentInfoVO.setBillForPayment(String.valueOf(billForPayment));
			    											paymentInfoVO.setDeductionRate(String.valueOf(deductionRate));
			    											
			    										}
			    										
		    										}
		    										if(paymentKind.equals("DD")) {
		    											paymentInfoVO.setPaymentKind(paymentKind);
				    									paymentInfoVO.setBillingDivision(billingDivision);
		    										}else {
		    											paymentInfoVO.setPaymentKind("AT");
				    									paymentInfoVO.setBillingDivision("02");	
		    										}
		    										
		    										if(paymentKind.equals("AT") || paymentKind.equals("CD")) {
		    											paymentInfoVO.setVatIncludeYn("N");
				    									paymentInfoVO.setVatExcludeYn("Y");
		    										}
		    										
		    										paymentInfoVO.setAmount(driverAmount);
		    										income -= Integer.parseInt(driverAmount);	
		    									}else {
		    										paymentInfoVO.setAmount("0");
		    									}
		    									
		    								}else if(paymentInfoVO.getPaymentDivision().equals("03")) {
		    									
		    									if(customerMap != null && customerMap.get("customer_id") != null && !customerMap.get("customer_id").equals("")) {
		    										paymentInfoVO.setPaymentPartner(customerMap.get("customer_name").toString());
			    									paymentInfoVO.setPaymentPartnerId(customerMap.get("customer_id").toString());
			    									if(paymentKind.equals("DD")) {
		    											paymentInfoVO.setPaymentKind(paymentKind);
				    									paymentInfoVO.setBillingDivision(billingDivision);
		    										}else {
		    											paymentInfoVO.setPaymentKind("AT");
				    									paymentInfoVO.setBillingDivision("02");	
		    										}
			    									
			    									if(paymentKind.equals("AT") || paymentKind.equals("CD")) {
		    											paymentInfoVO.setVatIncludeYn("N");
				    									paymentInfoVO.setVatExcludeYn("Y");
		    										}
			    									
		    										paymentInfoVO.setAmount(driverAmount);
		    										income -= Integer.parseInt(driverAmount);
		    									}else {
		    										paymentInfoVO.setAmount("0");
		    									}
		    									
		    								} 
		    								paymentInfoList.add(paymentInfoVO);
		    								//paymentInfoService.insertPaymentInfo(paymentInfoVO);
		    							}	
		    			        		allocationVO.setProfit(String.valueOf(income));
		    			        		allocationList.add(allocationVO);
	    			        		}
	    			        	}
	    			        	
	    			        } // for(r) 문
	    				
	    			}
	    		}
	    		
	    		
	    		if(duplication) {
	    			
	    			//allocationIdList.add(carInfoVO.getAllocationId());
	    			WebUtils.messageAndRedirectUrl(mav,"중복된 배차 정보가 있습니다.", "/allocation/duplication-list.do?allocationIdList="+allocationIdList);
	    			
	    		}
	    		
	    		
	    		
	    		for(int i = 0; i < allocationList.size(); i++) {		//null인 경우 처리
	    			if(allocationList.get(i).getAllocationStatus() == null) {
	    				allocationList.get(i).setAllocationStatus("N");
	    			}
	    			if(allocationList.get(i).getBatchStatus() == null) {
	    				allocationList.get(i).setBatchStatus("N");
	    				allocationList.get(i).setPickupNum("");
	    				allocationList.get(i).setBatchIndex(String.valueOf(0));		//픽업건여부에 관계 없이 index는 0
	    			}
	    		}
	    		
	    		for(int i = 0; i < allocationList.size(); i++) {
	    			
	    			//픽업 관리 번호 "" 가 아니고 관리번호가 같은 배차 정보는 픽업건으로 구분한다.	
	    			//픽업건은 batch_status가 P, batch_status_id가 같으나 batch_index는 0인 배차이다.
	    			String batchStatusId = "BAT"+UUID.randomUUID().toString().replaceAll("-","");
	    			if(allocationList.get(i).getBatchStatusId() == null || allocationList.get(i).getBatchStatusId().equals("")){	//
		    			allocationList.get(i).setBatchStatusId(batchStatusId);	
		    			AllocationVO allocation = allocationList.get(i);
		    			for(int j = i+1; j < allocationList.size(); j++) {
		    				if(allocation.getBatchStatus().equals("P") && allocation.getBatchStatus().equals(allocationList.get(j).getBatchStatus())){//픽업 건이고		
		    					if(allocation.getPickupNum().equals(allocationList.get(j).getPickupNum())){		//픽업 관리 번호가 같으면 같은 픽업건이다.
		    						allocationList.get(j).setBatchStatusId(batchStatusId);
		    					}
		    				}
		    			}
	    			}else {
	    					//여기서는 할게 없네...
	    			}
	    			/*AllocationVO allocation = allocationList.get(i);
	    			for(int j = i+1; j < allocationList.size(); j++) {
	    				if(allocation.getBatchStatus().equals("P") && allocation.getBatchStatus().equals(allocationList.get(j).getBatchStatus())){//픽업 건이고		
	    					if(allocation.getPickupNum().equals(allocationList.get(j).getPickupNum())){		//픽업 관리 번호가 같으면 같은 픽업건이다.
	    						allocationList.get(j).setBatchStatusId(batchStatusId);
	    					}
	    				}
	    			}*/
	    			
	    			allocationService.insertAllocation(allocationList.get(i));
	    			
	    			allocationList.get(i).setLogType("I"); //등록
	    			allocationService.insertAllocationLogByVO(allocationList.get(i)); // 로그기록
	    		}
	    		
	    		
	    		for(int i = 0; i < carInfoList.size(); i++) {
	    			
	    			//외부기사님이 아닌경우
	    			if(allocationList.get(i).getAllocationStatus().equals("A")){
	    				CarInfoVO carInfo = carInfoList.get(i);

	    				Map<String, Object> map = new HashMap<String, Object>();
						map.put("driverId", carInfo.getDriverId());
						Map<String, Object> driverMap = driverService.selectDriver(map);
						map.put("customerId", allocationList.get(i).getCustomerId());
						Map<String, Object> customer= customerService.selectCustomer(map);
						
						
						
						if(driverMap.get("fcm_token") != null && !driverMap.get("fcm_token").toString().equals("") || customer.get("sms_yn").equals("Y")) {
							Map<String, Object> sendMessageMap = new HashMap<String, Object>();
							sendMessageMap.put("title", "배차 정보가 도착 했습니다.");
							String carKind = "";
							String carIdNum = "";
							
							carKind = carInfo.getCarKind() != null && !carInfo.getCarKind().toString().equals("")? carInfo.getCarKind().toString():"정보없음";
							carIdNum = carInfo.getCarIdNum() != null && !carInfo.getCarIdNum().toString().equals("")? carInfo.getCarIdNum().toString():"정보 없음";
							String departure = carInfo.getDeparture() != null && !carInfo.getDeparture().toString().equals("") ? carInfo.getDeparture().toString() : "";
							String arrival = carInfo.getArrival() != null && !carInfo.getArrival().toString().equals("") ? carInfo.getArrival().toString() : "";
							
							
							
							if(allocationList.get(i).getAllocationStatus().equals("A") && driverMap.get("fcm_token")!= null) {
								sendMessageMap.put("body", "상차지:"+departure+",하차지:"+arrival+",차종:"+carKind+",차대번호:"+carIdNum);
								sendMessageMap.put("fcm_token", driverMap.get("fcm_token").toString());
								sendMessageMap.put("device_os", driverMap.get("device_os").toString());
								sendMessageMap.put("allocationId", carInfo.getAllocationId());
								fcmService.fcmSendMessage(sendMessageMap);	
							}
							
							Map<String, Object> alarmTalkMap = new HashMap<String, Object>();
							//알림톡 발송 구분		0 : 예약 완료,1:기사배정,2:상차완료,3:하차완료,4:인계완료,5:탁송완료,6탁송취소,7:배차정보(기사님에게),8:배차승인(배차담당직원)
							if(allocationList.get(i).getAllocationStatus().equals("N")) {
								alarmTalkMap.put("gubun", "0");
							}else if(allocationList.get(i).getAllocationStatus().equals("A")) {
								alarmTalkMap.put("gubun", "1");	
							}
							alarmTalkMap.put("departure", carInfo.getDeparture());
							alarmTalkMap.put("arrival", carInfo.getArrival());
							if(driverMap != null) {
								alarmTalkMap.put("driver_name", driverMap.get("driver_name"));
								alarmTalkMap.put("driver_id", driverMap.get("driver_id"));
								alarmTalkMap.put("phone_num", driverMap.get("phone_num").toString());
							}
							alarmTalkMap.put("car_kind", carInfo.getCarKind());
							alarmTalkMap.put("car_id_num", carInfo.getCarIdNum());
							alarmTalkMap.put("departure_dt", carInfo.getDepartureDt());
							alarmTalkMap.put("customer_id", customer.get("customer_id").toString());
							alarmTalkMap.put("customer_name", customer.get("customer_name").toString());
							alarmTalkMap.put("allocation_id", allocationList.get(i).getAllocationId());
							alarmTalkMap.put("departure_phone", carInfo.getDeparturePhone());
							alarmTalkMap.put("arrival_phone", carInfo.getArrivalPhone());
							
							//티케이물류 문자 발송시 문자 내용에 고객명 추가.
							String personInChargeStr = carInfo.getArrivalPersonInCharge() != null && !carInfo.getArrivalPersonInCharge().equals("") ? carInfo.getArrivalPersonInCharge() : "";
									
							personInChargeStr = personInChargeStr.replaceAll(" ", "");
							personInChargeStr = personInChargeStr.trim();
							
							String[] personInCharge = personInChargeStr.split(";");  
							if(personInCharge.length>0) {
								alarmTalkMap.put("person_in_charge",personInCharge[0]);	
							}else {
								alarmTalkMap.put("person_in_charge",personInChargeStr);	
							}
							
							String paymentKind = "";
							String billingDivision = "";
							for(int k = 0; k < paymentInfoList.size(); k++) {
								//allocationId가 같고 paymentDivision이 01인데 payment_kind가 dd인 경우....
								if(carInfo.getAllocationId().equals(paymentInfoList.get(k).getAllocationId()) && paymentInfoList.get(k).getPaymentDivision().equals("01")) {
									if(paymentInfoList.get(k).getPaymentKind() != null) {
										if(paymentInfoList.get(k).getPaymentKind().equals("DD")) {		//결제 방법이 기사 수금이면
											paymentKind = "DD";
										}else {
											paymentKind = paymentInfoList.get(k).getPaymentKind();
										}
										if(paymentInfoList.get(k).getBillingDivision() != null) {
											billingDivision = paymentInfoList.get(k).getBillingDivision();
										}
									}
									
								}
							}
							alarmTalkMap.put("payment_kind",paymentKind);
							alarmTalkMap.put("customer_phone", allocationList.get(i).getChargePhone());
							alarmTalkMap.put("alarm_talk_yn", customer.get("alarm_talk_yn").toString());
							alarmTalkMap.put("send_account_info_yn", customer.get("send_account_info_yn").toString());
							alarmTalkMap.put("customer_kind",customer.get("customer_kind").toString());
							alarmTalkMap.put("billing_division",billingDivision);
							alarmTalkMap.put("alarmOrSmsYn", customer.get("alarm_or_sms_yn").toString());
//							if(customer.get("alarm_talk_yn").toString().equals("Y") && allocationList.get(i).getChargePhone() != null && !allocationList.get(i).getChargePhone().equals("")) {
//								alarmTalkService.alarmTalkSends(alarmTalkMap);	
//							}
//							if(customer.get("sms_yn").toString().equals("Y")) {
//								sendSmsService.smsSend(alarmTalkMap);
//							}
							
							Map<String, Object> driverSetEmpMap = new HashMap<String, Object>();
							driverSetEmpMap.put("empId", userMap.get("emp_id").toString());
							driverSetEmpMap.put("empName", userMap.get("emp_name").toString());
							driverSetEmpMap.put("allocationId",carInfo.getAllocationId());
							driverSetEmpMap.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
							driverSetEmpMap.put("logType", "U");
							carInfoService.updateCarInfoDriverSetEmp(driverSetEmpMap);
							carInfoService.insertCarInfoLogByMap(driverSetEmpMap); //로그기록
						}
	    				
	    			}
	    			
	    			
	    			//미배차인경우
	    			if(allocationList.get(i).getAllocationStatus().equals("N")){
	    				CarInfoVO carInfo = carInfoList.get(i);

	    				Map<String, Object> map = new HashMap<String, Object>();
						map.put("customerId", allocationList.get(i).getCustomerId());
						Map<String, Object> customer= customerService.selectCustomer(map);
											
						Map<String, Object> alarmTalkMap = new HashMap<String, Object>();
						//알림톡 발송 구분		0 : 예약 완료,1:기사배정,2:상차완료,3:하차완료,4:인계완료,5:탁송완료,6탁송취소,7:배차정보(기사님에게),8:배차승인(배차담당직원)
						alarmTalkMap.put("gubun", "0");
						alarmTalkMap.put("departure", carInfo.getDeparture());
						alarmTalkMap.put("arrival", carInfo.getArrival());
						alarmTalkMap.put("car_kind", carInfo.getCarKind());
						alarmTalkMap.put("car_id_num", carInfo.getCarIdNum());
						alarmTalkMap.put("departure_dt", carInfo.getDepartureDt());
						alarmTalkMap.put("customer_id", customer.get("customer_id").toString());
						alarmTalkMap.put("customer_name", customer.get("customer_name").toString());
						alarmTalkMap.put("allocation_id", allocationList.get(i).getAllocationId());
						alarmTalkMap.put("departure_phone", carInfo.getDeparturePhone());
						alarmTalkMap.put("arrival_phone", carInfo.getArrivalPhone());
						
						//티케이물류 문자 발송시 문자 내용에 고객명 추가.
						String personInChargeStr = carInfo.getArrivalPersonInCharge() != null && !carInfo.getArrivalPersonInCharge().equals("") ? carInfo.getArrivalPersonInCharge() : "";
								
						personInChargeStr = personInChargeStr.replaceAll(" ", "");
						personInChargeStr = personInChargeStr.trim();
						
						String[] personInCharge = personInChargeStr.split(";"); 
						try {
							alarmTalkMap.put("person_in_charge",personInCharge[0]);	
						}catch(Exception e) {
							e.printStackTrace();
							personInCharge = new String[2];
							personInCharge[0] = "";
							personInCharge[1] = "";
							alarmTalkMap.put("person_in_charge",personInCharge[0]);
						}
						
						
						
						String paymentKind = "";
						String billingDivision = "";
						for(int k = 0; k < paymentInfoList.size(); k++) {
							//allocationId가 같고 paymentDivision이 01인데 payment_kind가 dd인 경우....
							if(carInfo.getAllocationId().equals(paymentInfoList.get(k).getAllocationId()) && paymentInfoList.get(k).getPaymentDivision().equals("01")) {
								if(paymentInfoList.get(k).getPaymentKind() != null) {
									if(paymentInfoList.get(k).getPaymentKind().equals("DD")) {		//결제 방법이 기사 수금이면
										paymentKind = "DD";
									}else {
										paymentKind = paymentInfoList.get(k).getPaymentKind();
									}
									if(paymentInfoList.get(k).getBillingDivision() != null) {
										billingDivision = paymentInfoList.get(k).getBillingDivision();
									}
								}
								
							}
						}
						alarmTalkMap.put("payment_kind",paymentKind);
						alarmTalkMap.put("customer_phone", allocationList.get(i).getChargePhone());
						alarmTalkMap.put("alarm_talk_yn", customer.get("alarm_talk_yn").toString());
						alarmTalkMap.put("send_account_info_yn", customer.get("send_account_info_yn").toString());
						alarmTalkMap.put("customer_kind",customer.get("customer_kind").toString());
						alarmTalkMap.put("billing_division",billingDivision);
						alarmTalkMap.put("alarmOrSmsYn", customer.get("alarm_or_sms_yn").toString());
						if(customer.get("alarm_talk_yn").toString().equals("Y") && allocationList.get(i).getChargePhone() != null && !allocationList.get(i).getChargePhone().equals("")) {
							alarmTalkService.alarmTalkSends(alarmTalkMap);	
						}
						if(customer.get("sms_yn").toString().equals("Y") ) {
							sendSmsService.smsSend(alarmTalkMap);
						}
						
						Map<String, Object> driverSetEmpMap = new HashMap<String, Object>();
						driverSetEmpMap.put("empId", userMap.get("emp_id").toString());
						driverSetEmpMap.put("empName", userMap.get("emp_name").toString());
						driverSetEmpMap.put("allocationId",carInfo.getAllocationId());
						driverSetEmpMap.put("logType","U");
						carInfoService.updateCarInfoDriverSetEmp(driverSetEmpMap);
						carInfoService.insertCarInfoLogByMap(driverSetEmpMap); //로그기록
    				
	    			}
	    			
		    		carInfoService.insertCarInfo(carInfoList.get(i));	   
		    		carInfoList.get(i).setLogType("I");
		    		carInfoService.insertCarInfoLogByVO(carInfoList.get(i)); //로그기록
	    		}
	    		for(int i = 0; i < paymentInfoList.size(); i++) {
	    			paymentInfoService.insertPaymentInfo(paymentInfoList.get(i));
	    			paymentInfoList.get(i).setLogType("I");
	    			paymentInfoService.insertPaymentInfoLogByVO(paymentInfoList.get(i)); //로그기록
	    		}
	    		
	    		
	    	
	    		
	    		
	    		
	    		
	    	}else{
	    		resultApi.setResultCode("1111"); 	//file 없음
	    	}
			
			WebUtils.messageAndRedirectUrl(mav,allocationList.size()+"건의 배차 정보가 등록되었습니다.", "/allocation/allocation-register.do");
		}catch(Exception e){
			WebUtils.messageAndRedirectUrl(mav, inputDataRow+"번째 행을 확인 해 주세요.("+cause+")", "/allocation/allocation-register.do");
			e.printStackTrace();
		}
		
		return mav;
	}
	
	
	
	//배차 엑셀 중복 체크
	@RequestMapping(value = "/duplication-check", method = {RequestMethod.POST,RequestMethod.GET})
	
	public ModelAndView duplication_check(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		

		int inputDataRow = 0;
		String cause = "";					//업로드 실패시 원인을 확인 할 수 있도록 한다.
		try{
			
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
	    	List<MultipartFile> fileList = multipartRequest.getFiles("bbsFile");
			
	    	HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			String companyId = session.getAttribute("companyId").toString();
			
			
			List<AllocationVO> allocationList = new ArrayList<AllocationVO>();
			List<CarInfoVO> carInfoList = new ArrayList<CarInfoVO>();
			List<PaymentInfoVO> paymentInfoList = new ArrayList<PaymentInfoVO>();
			
			boolean duplication = false;
			List<String> allocationIdList = new ArrayList<String>();
			String dupAllocationId = "";
			
			
			
	    	if(fileList != null && fileList.size() > 0){
	    		for(MultipartFile mFile : fileList){	
	    			if(mFile != null && mFile.getSize() > 0){
	    				/*
	    				Long fileSize = mFile.getSize();
	    				String fileName = mFile.getOriginalFilename();		
	    				String ext = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length());
	    				File file = new File(mFile.getOriginalFilename());
	    				mFile.transferTo(file);
	    				*/
	    				FileVO fileVO = new FileVO();
	    				Map<String, Object> fileMap = fileUploadService.upload(rootDir, "dupList", mFile);
	    				
	    				fileVO.setContentId("test");
	    				fileVO.setCategoryType("dupList");
	    				fileVO.setFileId("FLE"+UUID.randomUUID().toString().replaceAll("-", ""));
	    				fileVO.setFileNm(fileMap.get("ORG_NAME").toString());
	    				fileVO.setFilePath(fileMap.get("SAVE_NAME").toString());
	    				fileMapper.insertFile(fileVO);	
	    				
	    				File file = new File(rootDir+"/dupList"+fileMap.get("SAVE_NAME").toString());
	    				String fileId = fileVO.getFileId();
	    				
	    				XSSFWorkbook wb = null;
	    				XSSFRow row;
	    				XSSFCell cell;
	    				XSSFCell cellForSearch;
	    				
	    				try {//엑셀 파일 오픈
	    					wb = new XSSFWorkbook(new FileInputStream(file));
	    				} catch (FileNotFoundException e) {
	    					e.printStackTrace();
	    				} catch (IOException e) {
	    					e.printStackTrace();
	    				}
	    				
	    				//fileUploadService.upload(rootDir,"dupList","dupList_excel.xlsx", wb);
	    				//fileService.insertFile(rootDir, "dupList", wb);
	    				
	    				//fileService.insertFile(rootDir, "dupList", "test", request);
	    				//fileService.insertFile(rootDir, "dupList", "", request);
	    				
	    				XSSFSheet sheet = wb.getSheetAt(1);
	    		         //int rows = sheet.getPhysicalNumberOfRows();
	    				//int cells = sheet.getRow(0).getPhysicalNumberOfCells();
	    				int rows = sheet.getLastRowNum();
	    				int cells = sheet.getRow(1).getLastCellNum();
	    				
	    				FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
	    				

	    				for (int r = 1; r <= rows; r++) {
	    					 inputDataRow = r+1;
	    					 AllocationVO allocationVO = new AllocationVO();
	    					 CarInfoVO carInfoVO = new CarInfoVO();
	    					 Map<String, Object> driverMap = new HashMap<String, Object>();
	    					 Map<String, Object> customerMap = new HashMap<String, Object>();
	    					 String allocationId = "ALO"+UUID.randomUUID().toString().replaceAll("-","");
	    					 allocationVO.setAllocationId(allocationId);
	    					 //allocationVO.setAllocationStatus("N");
	    					 
	    					 
	    					 /*allocationVO.setBatchStatus("N");
	    					 allocationVO.setBatchStatusId("BAT"+UUID.randomUUID().toString().replaceAll("-",""));
	 						 allocationVO.setBatchIndex(String.valueOf(0));*/
	 						 allocationVO.setRegisterId(userMap.get("emp_id").toString());
	 						 allocationVO.setRegisterName(userMap.get("emp_name").toString());
	 						 
	 						 //allocationVO.setCompanyId("company2");				//2019.01.31 엑셀 등록시 사업자정보를 무조건 한국카캐리어(법인)으로 등록 되게 함.
	 						 allocationVO.setCompanyId(companyId);				//2019.12.27 엑셀 등록시 소속된 회사의 아이디를 넣도록 수정
	 						 
	 						Map<String, Object> searchMap = new HashMap<String, Object>();
	 						searchMap.put("companyId", companyId);
	 						Map<String, Object> company = companyService.selectCompany(searchMap);
	 						Map<String,Object> customer= new HashMap<String, Object>();
	 						
	 						
	 						allocationVO.setCompanyName(company.get("company_name").toString());
	    					carInfoVO.setAllocationId(allocationId);
	    					carInfoVO.setReceiptSendYn("N");
	    					 String customerAmount = "";
	    					 String driverAmount = "";
	    					 String paymentKind = "";
	    					 String billingDivision = "";
	    					 
	    					 FormulaEvaluator formulaEval = wb.getCreationHelper().createFormulaEvaluator();
	    					 
	    			        	row = sheet.getRow(r); // row 가져오기
	    			        	if (row != null) {
	    			        		for (int c = 0; c < cells; c++) {
	    			        			cell = row.getCell(c);
	    			        			if (cell != null) { 
	    			        				String value = "";

	    									switch (cell.getCellType()) {
	    									   	case XSSFCell.CELL_TYPE_FORMULA:
	    									   		
	    									   		CellValue evaluate = formulaEval.evaluate(cell);
	    									   	  if( evaluate != null ) {
	    									   		  try {
	    									   			  
	    									   			  if(WebUtils.isNumber(evaluate.formatAsString())) {
	    									   				value=  String.valueOf(new Double(evaluate.getNumberValue()).intValue());
	    									   				   
	    									   			  }else {
	    									   				  
	    									   				  value = evaluate.formatAsString();
	    									   			  }
	    									   			  
	    									   		  }catch(Exception e) {
	    									   			  e.printStackTrace();
	    									   			  
	    									   		  }
	    									   		  
	    									   		  
	 	    			        				   }else {
	 	    			        					   
	 	    			        					   value = "";
	 	    			        				   }
	    									   		
	    									   		/*
	    									   		if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_NUMERIC){
	    									   			value = "" + (int)cell.getNumericCellValue();
	    									   		}else if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_STRING){
	    									   			value = "" + cell.getStringCellValue(); 
	    									   		}
	    									   		value = cell.getCellFormula();
	    									   		*/
	    									   		
	    									   		break;
	    									   	case XSSFCell.CELL_TYPE_NUMERIC:
	    								            if (DateUtil.isCellDateFormatted(cell)){  
	    								                //날짜  
	    								            	Date date = cell.getDateCellValue();
	    								                value = String.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(date));
	    								            }else{  
	    								                //수치
	    								            	value = NumberToTextConverter.toText(cell.getNumericCellValue());
	    								            	
	    								                //value = String.valueOf(cell.getNumericCellValue());  
	    								            } 
	    									   		//value = "" + (int)cell.getNumericCellValue();
	    									   		break;
	    									   		
	    									   	case XSSFCell.CELL_TYPE_STRING:
	    									   		value = "" + cell.getStringCellValue();
	    									   		break;
	    									   	case XSSFCell.CELL_TYPE_BLANK:
	    									   		value = "";
	    									   		break;
	    									   	case XSSFCell.CELL_TYPE_ERROR:
	    									   		value = "" + cell.getErrorCellValue();
	    									   		break;
	    									   	default:
	    									   		
	    									   		
	    									   		
	    									   		
	    									}
	    									if(value != null && !value.equals("")) {
	    										value = value.trim();
	    									}
	    									int nIndex = cell.getColumnIndex();
	    									if(nIndex == 0){
	    										
	    										if(value != null && !value.equals("")) {
	    											Map<String, Object> map = new HashMap<String, Object>();
		    										map.put("allocationDivision",value);
		    										Map<String, Object> allocationDivision = allocationDivisionService.selectAllocationDivisionInfo(map);
		    										carInfoVO.setCarrierType(allocationDivision.get("allocation_division_cd").toString());	
	    										}else {		//배차 구분이 지정 되어 있지 않으면 익셉션
	    											cause = "배차 구분이 지정 되지 않았습니다.";
	    											throw new Exception();
	    										}
	    									}else if(nIndex == 1){
	    										Map<String, Object> map = new HashMap<String, Object>();
	    										if(!value.equals("")) {
	    											map.put("runDivision",value);
		    										Map<String, Object> runDivision = runDivisionService.selectRunDivisionInfo(map);	
		    										if(runDivision != null) {
		    											carInfoVO.setDistanceType(runDivision.get("run_division_cd").toString());			    											
		    										}else {
		    											carInfoVO.setDistanceType("C");	
		    										}
		    										
	    										}else {
	    											carInfoVO.setDistanceType("C");
	    										}
	    										
	    									}else if(nIndex == 2){
	    										
	    										if(!value.equals("")) {
	    											String dateDay = WebUtils.getDateDay(value);
		    										allocationVO.setInputDt(value+" ("+dateDay+")");	
	    										}else {
	    											String dateDay = WebUtils.getDateDay(WebUtils.getNow("yyyy-MM-dd"));
		    										allocationVO.setInputDt(WebUtils.getNow("yyyy-MM-dd")+" ("+dateDay+")");
	    										}
	    										
	    									}else if(nIndex == 3){
	    										carInfoVO.setDepartureDt(value);
	    										
	    										try {
	    											
	    											if(!value.equals("")) {
		    											SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		    											Date departureDt_toDate = format.parse(value);
		    												
		    										}
	    											
	    										}catch(Exception e) {
	    											e.printStackTrace();
	    											cause = "날짜 형식에 맞지않은 유형입니다.";
	    											throw new Exception();
	    										} 
	    										
	    										
	    										
	    									}else if(nIndex == 4){
	    										Map<String, Object> map = new HashMap<String, Object>();

	    										map.put("customerName",value);
	    										cellForSearch = row.getCell(6);
	    										String chargeName = "";
	    										
	    										if(cellForSearch != null) {
	    											chargeName = ""+cellForSearch.getStringCellValue();
	    										}
	    										if(!chargeName.equals("")) {
	    											map.put("chargeName",chargeName);	
	    											customer = customerService.selectCustomerByCustomerNameAndChargeName(map);
	    										}else {
	    											customer = customerService.selectCustomerByCustomerName(map);	
	    										}
	    										
	    										
	    										if(customer != null) {
	    											
	    											if(!customer.get("black_list_yn").toString().equals("Y")) {
		    											allocationVO.setCustomerId(customer.get("customer_id").toString());
			    										allocationVO.setCustomerName(customer.get("customer_name").toString());
			    										allocationVO.setCustomerSignificantData(customer.get("significant_data") != null && !customer.get("significant_data").toString().equals("") ? customer.get("significant_data").toString():"");
	    											}else {
	    												cause = "해당 거래처가 블랙리스트로 등록 되어 있습니다.";
		    											throw new Exception();
	    											}
	    											
	    										}else {		//거래처가 없는경우 익셉션
	    											cause = "거래처가 등록 되어 있지 않습니다.";
	    											throw new Exception();
	    										}
	    									}else if(nIndex == 5){
	    										//업체(담당자 부서로 사용 하고 있으나 현재 배차 등록에는 필요 하지 않아 일단 사용하지 않음..
	    									}else if(nIndex == 6){
	    										//업체의 담당자가 한명만 있는경우 그 담당자가 해당 배차의 담당자가 되도록 진행. 
	    										Map<String, Object> map = new HashMap<String, Object>();
	    										map.put("customerId",allocationVO.getCustomerId());
	    										map.put("name",value);
	    										if(!value.equals("")) {
	    											Map<String, Object> personInCharge = personInChargeService.selectPersonInCharge(map);
	    											if(value.equals("현대캐피탈")) {
	    												allocationVO.setHcYn("Y");
	    											}
	    											
		    										if(personInCharge != null) {
		    											allocationVO.setChargeName(personInCharge.get("name").toString());
		    											allocationVO.setChargeId(personInCharge.get("person_in_charge_id").toString());
		    											allocationVO.setChargePhone(personInCharge.get("phone_num") != null && !personInCharge.get("phone_num").toString().equals("") ? personInCharge.get("phone_num").toString():"");
		    											allocationVO.setChargeAddr(personInCharge.get("address") != null && !personInCharge.get("address").toString().equals("") ? personInCharge.get("address").toString():"");
		    										}else {
		    											cause = "담당자 정보를 찾을 수 없습니다.";
		    											throw new Exception();	
		    										}
	    										}else {
	    											List<Map<String, Object>> personInChargeList = personInChargeService.selectPersonInChargeList(map);
	    											if(personInChargeList.size() == 1) {			//업체의 담당자가 한명인 경우 해당 배차의 담당자가 된다.
	    												Map<String, Object> personInCharge = personInChargeList.get(0);
	    												allocationVO.setChargeName(personInCharge.get("name").toString());
		    											allocationVO.setChargeId(personInCharge.get("person_in_charge_id").toString());
		    											allocationVO.setChargePhone(personInCharge.get("phone_num") != null && !personInCharge.get("phone_num").toString().equals("") ? personInCharge.get("phone_num").toString():"");
		    											allocationVO.setChargeAddr(personInCharge.get("address") != null && !personInCharge.get("address").toString().equals("") ? personInCharge.get("address").toString():"");
	    											}else {
		    											cause = "담당자가 지정되지 않았습니다.";
		    											throw new Exception();	
		    										}
	    										}
	    									}else if(nIndex == 7){
	    										//차종
	    										carInfoVO.setCarKind(value);
	    									}else if(nIndex == 8){
	    										//차대번호
	    										carInfoVO.setCarIdNum(value);
	    									}else if(nIndex == 9){
	    										//차량번호
	    										carInfoVO.setCarNum(value);
	    									}else if(nIndex == 10){
	    										//계약번호
	    										carInfoVO.setContractNum(value);
	    									}else if(nIndex == 11){
	    										//출발지
	    										if(value != null && !value.equals("")) {
	    											Map<String, Object> map = new HashMap<String, Object>();
		    										map.put("keyword", value);
		    										Map<String, Object> addressInfo = addressInfoService.selectAddressInfo(map);
		    										if(addressInfo != null) {
		    											carInfoVO.setDeparture(value);
		    											carInfoVO.setDepartureAddr(addressInfo.get("address") != null ? addressInfo.get("address").toString():"");
		    											carInfoVO.setDeparturePersonInCharge(addressInfo.get("name") != null ? addressInfo.get("name").toString():"");
		    											carInfoVO.setDeparturePhone(addressInfo.get("phone_num") != null ? addressInfo.get("phone_num").toString():"");
		    											
		    										}else {
		    											carInfoVO.setDeparture(value);
		    										}
	    										}else {
	    											carInfoVO.setDeparture(value);
	    										}	    										
	    									}else if(nIndex == 12){
	    										//출발지상세주소
	    										if(value != "") {
	    											carInfoVO.setDepartureAddr(value);
	    										}else {
	    											if(carInfoVO.getDepartureAddr() == null || carInfoVO.getDepartureAddr().equals("")) {
		    											carInfoVO.setDepartureAddr(value);	
		    										}	
	    										}
	    									}else if(nIndex == 13){
	    										//상차지담당자명
	    										if(value != "") {
	    											carInfoVO.setDeparturePersonInCharge(value);
	    										}else {
	    											if(carInfoVO.getDeparturePersonInCharge() == null || carInfoVO.getDeparturePersonInCharge().equals("")) {
		    											carInfoVO.setDeparturePersonInCharge(value);	
		    										}	
	    										}
	    									}else if(nIndex == 14){
	    										//상차지담당자연락처
	    										if(value != "") {
	    											carInfoVO.setDeparturePhone(value);
	    										}else {
	    											if(carInfoVO.getDeparturePhone() == null || carInfoVO.getDeparturePhone().equals("")) {
		    											carInfoVO.setDeparturePhone(value);	
		    										}	
	    										}
	    									}else if(nIndex == 15){
	    										//하차지
	    										if(value != null && !value.equals("")) {
	    											Map<String, Object> map = new HashMap<String, Object>();
		    										map.put("keyword", value);
		    										Map<String, Object> addressInfo = addressInfoService.selectAddressInfo(map);
		    										if(addressInfo != null) {
		    											carInfoVO.setArrival(value);
		    											carInfoVO.setArrivalAddr(addressInfo.get("address") != null ? addressInfo.get("address").toString():"");
		    											carInfoVO.setArrivalPersonInCharge(addressInfo.get("name") != null ? addressInfo.get("name").toString():"");
		    											carInfoVO.setArrivalPhone(addressInfo.get("phone_num") != null ? addressInfo.get("phone_num").toString():"");
		    										}else {
		    											carInfoVO.setArrival(value);
		    										}
	    										}else {
	    											carInfoVO.setArrival(value);
	    										}
	    									}else if(nIndex == 16){
	    										//하차지상세주소
	    										if(value != "") {
	    											carInfoVO.setArrivalAddr(value);
	    										}else {
	    											if(carInfoVO.getArrivalAddr() == null || carInfoVO.getArrivalAddr().equals("")) {
		    											carInfoVO.setArrivalAddr(value);	
		    										}	
	    										}
	    									}else if(nIndex == 17){
	    										//하차지담당자명
	    										if(value != "") {
	    											carInfoVO.setArrivalPersonInCharge(value);
	    										}else {
	    											if(carInfoVO.getArrivalPersonInCharge() == null || carInfoVO.getArrivalPersonInCharge().equals("")) {
		    											carInfoVO.setArrivalPersonInCharge(value);	
		    										}	
	    										}
	    									}else if(nIndex == 18){
	    										//하차지담당자연락처
	    										if(value != "") {
	    											carInfoVO.setArrivalPhone(value);
	    										}else {
	    											if(carInfoVO.getArrivalPhone() == null || carInfoVO.getArrivalPhone().equals("")) {
		    											carInfoVO.setArrivalPhone(value);	
		    										}	
	    										}
	    										
	    										if(allocationVO.getCustomerId().equals("CUSa8b74209a826425698ef189f61f08ff6")) {
	    											
	    											//하차지가 티케이 물류가 아닌건에 대해 작성된 전화번호가 하나인 경우 다시 등록 할 수 있도록 한다.
	    											if(carInfoVO.getArrival() != "" && !carInfoVO.getArrival().equals("티케이물류") && !carInfoVO.getArrival().equals("티케이화성")) {
	    											
	    												if(value.equals("")) {
	    													cause = "티케이물류(화성) 2차 탁송건에 대해 연락처가 입력 되지 않았습니다.";
			    											throw new Exception();
	    												}else {
	    													String[] arrivalPhone = value.split(";");
	    													if(arrivalPhone.length == 1) {
	    														cause = "티케이물류(화성) 2차 탁송건에 대해 연락처 하나만 입력 되었습니다.";
				    											throw new Exception();	
	    													}else if(arrivalPhone.length > 1) {
	    														if(arrivalPhone[1].equals("")) {
	    															cause = "티케이물류(화성) 2차 탁송건에 대해 연락처 하나만 입력 되었습니다.";
					    											throw new Exception();	
	    														}
	    													}
	    												}
	    											}
	    										}
	    										
	    									}else if(nIndex == 19){
	    										//기사명					//엑셀 등록시 기사명이 등록 되어 있더라도 배차 정보에서 수정 할 수 있도록....	    										
	    										if(!value.equals("")) {
	    											String driverName = value;
		    										Map<String, Object> map = new HashMap<String, Object>();
		    										map.put("driverName", driverName);
		    										map.put("driverStatus", "01");
		    										
		    										driverMap = driverService.selectDriverByDriverName(map);
		    										
		    										if(driverMap != null) {
		    											carInfoVO.setDriverId(driverMap.get("driver_id").toString());
		    											carInfoVO.setDriverName(driverMap.get("driver_name").toString());	
		    										}
		    										//외부기사인 경우
		    										if(driverMap != null && driverMap.get("customer_id") != null && !driverMap.get("customer_id").toString().equals("")) {
		    											map.put("customerId", driverMap.get("customer_id").toString());
		    											customerMap = customerService.selectCustomer(map);
		    										}
	    										}
	    									}else if(nIndex == 20){
	    										//회전수
	    										if(!value.equals("")) {
	    											
	    											try {
	    												int driverCnt = Integer.parseInt(value);
	    											}catch(NumberFormatException e) {
	    												cause = "회전수가 잘못 입력 되었습니다.";
		    											throw new Exception();
	    											}
	    											
	    											//driverMap.put("driverCnt", value);
	    											if(driverMap != null && driverMap.get("driver_id") != null) {
	    												carInfoVO.setDriverCnt(value);	
	    											}else {
	    												cause = "회전수가 지정되었지만 기사가 지정되지 않았습니다.";
		    											throw new Exception();
	    											}
	    										}
	    									}else if(nIndex == 21){
	    										//특이사항//특이사항(비고)
	    										carInfoVO.setEtc(value);
	    									}else if(nIndex == 22){
	    										//car_cnt(대수)
	    										allocationVO.setCarCnt(value);
	    									}else if(nIndex == 23){
	    										//업체청구액
	    										if(value.equals("")) {
	    											value = "0";
	    										}
	    										customerAmount = value.replaceAll(",", "");
	    										carInfoVO.setSalesTotal(customerAmount);
	    										
	    									}else if(nIndex == 24){
	    										//부가세 구분
	    										
	    										if(!value.equals("")) {
	    											//포함인경우
	    											if(value.equals("포함")){
	    												carInfoVO.setVatIncludeYn("Y");
	    	    										carInfoVO.setVatExcludeYn("N");
	    	    										int total = Integer.parseInt(customerAmount);
	    	    										int vat = Integer.parseInt(customerAmount)/11;
	    	    										customerAmount = String.valueOf(Integer.parseInt(customerAmount)-vat);
	    	    										carInfoVO.setSalesTotal(customerAmount);
			    										carInfoVO.setVat(String.valueOf(vat));
			    										carInfoVO.setPrice(String.valueOf(total));
	    											//별도인경우
		    										}else if(value.equals("별도")){
		    											carInfoVO.setVatIncludeYn("N");
			    										carInfoVO.setVatExcludeYn("Y");
			    										int vat = Integer.parseInt(customerAmount)/10;
			    										int total = Integer.parseInt(customerAmount)+vat;
			    										carInfoVO.setVat(String.valueOf(vat));
			    										carInfoVO.setPrice(String.valueOf(total));
		    										}else {
		    											carInfoVO.setVat("0");
		    											carInfoVO.setVatIncludeYn("N");
			    										carInfoVO.setVatExcludeYn("N");
		    											carInfoVO.setPrice(customerAmount);
		    										}
	    										}else {
	    											//포함도 아니고 별도도 아닌경우
	    											carInfoVO.setVat("0");
	    											carInfoVO.setVatIncludeYn("N");
		    										carInfoVO.setVatExcludeYn("N");
	    											carInfoVO.setPrice(customerAmount);
	    										}
	    										
	    									}else if(nIndex == 25){
	    										//기사지급액
	    										if(value.equals("")) {
	    											value = "0";
	    										}
	    										driverAmount = value.replaceAll(",", "");
	    									}else if(nIndex == 28){
	    										
	    										//엑셀 업로드시 기사가 지정 된 경우 배차 승인 또는 배차 완료이다
	    										if(carInfoVO.getDriverId() != null && carInfoVO.getDriverCnt() != null && !carInfoVO.getDriverId().equals("") && !carInfoVO.getDriverCnt().equals("")) {
	    											//거래처 소속인 경우 탁송 완료
	    											if(customerMap != null && customerMap.get("customer_id") != null && !customerMap.get("customer_id").equals("")) {
	    												allocationVO.setAllocationStatus("F");	
	    												
	    											// 문자발송 여부가 Y일때 배차 승인대기 상태
	    												if(customer.get("sms_yn").equals("Y")) {
	    													allocationVO.setAllocationStatus("A");
	    												}
	    												
	    												
	    											}else {
	    												//거래처 소속이 아닌 경우는 배차 승인 대기 상태
	    												allocationVO.setAllocationStatus("A");	
	    											}
	    										}else {
	    											
	    											//기사 또는 회차 하나만 지정 되어 있는경우
	    											if(!carInfoVO.getDriverId().equals("") || !carInfoVO.getDriverCnt().equals("")) {
	    												if(!carInfoVO.getDriverId().equals("")) {
	    													cause = "기사가 지정 되었으나 회차가 지정 되지 않았습니다.";
			    											throw new Exception();
	    												}else {
	    													cause = "회차가 지정 되었으나 기사가 지정되지 않았습니다.";
			    											throw new Exception();	
	    												}
	    											}else {
	    												//미확정,미배차
		    											if(value.equals("미확정")){
			    											allocationVO.setAllocationStatus("U");	
			    										}else if(value.equals("미배차")){
			    											allocationVO.setAllocationStatus("N");
			    										}else {
			    											//미확정도 아니고 미배차도 아닌경우 미배차 상태로...
			    											allocationVO.setAllocationStatus("N");
			    										}	
	    											}
	    										
	    										}
	    										
	    									}else if(nIndex == 29) {		//픽업관리
	    										if(!value.equals("")) {		//픽업건인경우
	    											allocationVO.setPickupNum(value);
	    											allocationVO.setBatchStatus("P");
	    										}else {							//픽업 아님
	    											allocationVO.setPickupNum("");
	    											allocationVO.setBatchStatus("N");
	    										}
	    										
	    									}else if(nIndex == 30) {		//사진제한
	    										if(!value.equals("")) {
	    											try {
	    												 Double.parseDouble(value);
	    												 carInfoVO.setRequirePicCnt(value);
	    											}catch(NumberFormatException e) {
	    												cause = "사진제한은 숫자로만 입력 가능 합니다.";
		    											throw new Exception();
	    											}
	    										}else {							
	    											carInfoVO.setRequirePicCnt("3");
	    										}
	    									}else if(nIndex == 31) {		//사진제한
	    										if(!value.equals("")) {
	    											try {
	    												 Double.parseDouble(value);
	    												 carInfoVO.setRequireDnPicCnt(value);
	    											}catch(NumberFormatException e) {
	    												cause = "사진제한은 숫자로만 입력 가능 합니다.";
		    											throw new Exception();
	    											}
	    										}else {							
	    											carInfoVO.setRequireDnPicCnt("3");
	    										}
	    									}else if(nIndex == 32) {		//결제방법
	    										if(!value.equals("")) {	    											
	    											paymentKind = value;
	    										}else {							
	    											cause = "결제방법이 입력 되지 않았습니다.";
	    											throw new Exception();
	    										}
	    									}else if(nIndex == 33) {		//증빙구분
	    										if(!value.equals("")) {
	    											billingDivision = value;
	    										}else {							
	    											cause = "증빙구분이 입력 되지 않았습니다.";
	    											throw new Exception();
	    										}
	    									}
	    									allocationVO.setBatchIndex(String.valueOf(0));		//픽업건여부에 관계 없이 index는 0
	    			        			} else {
	    			        				System.out.print("[null]\t");
	    			        			}
	    			        			
	    			        		} // for(c) 문
	    			        
			    		
			    		
			    	   }
	    			        	
	    			        	
	    			        	Map<String, Object> findMap = new HashMap<String, Object>();
			        			findMap.put("carIdNum", carInfoVO.getCarIdNum()!=null ? carInfoVO.getCarIdNum() : "");
			        			findMap.put("carNum", carInfoVO.getCarNum()!=null ? carInfoVO.getCarNum() : "");
			        			findMap.put("contractNum", carInfoVO.getContractNum()!=null ? carInfoVO.getContractNum() : "");
			        			findMap.put("departure", carInfoVO.getDeparture()!=null ? carInfoVO.getDeparture() : "");
			        			findMap.put("arrival", carInfoVO.getArrival()!=null ? carInfoVO.getArrival() : "");
			        			
    			        		if(!findMap.get("carIdNum").toString().equals("") || !findMap.get("carNum").toString().equals("") || !findMap.get("contractNum").toString().equals("")) {
			        				Map<String, Object> findResult = carInfoService.selectCarInfoForDuplicate(findMap);
    			        			if(findResult != null) {
    			        				cause = "중복된 배차 정보가 있습니다.";
    			        				dupAllocationId += findResult.get("allocation_id").toString()+",";
    			        			}	
			        			}
	    			        	
	    			}		
	    				
	    				if(!dupAllocationId.equals("")) {
			    			
			    			 WebUtils.messageAndRedirectUrl(mav,"중복된 배차 정보가 있습니다.", "/allocation/duplication-list.do?allocationIdArr="+dupAllocationId+"&fileId="+fileId);
			    			
			    		 }else {
			    			
			    			 //	다시 엑셀 업로드 컨트롤러로 redirect
			    			 //response.sendRedirect(request.getContextPath()+"/allocation/allocation-excel-insert.do");

			    			 //mav.addObject("fileId",fileId);
			    			 request.setAttribute("fileId", fileId);
			    			 this.allocationExcelInsert(mav,request,response);
			    			 
			    			//allocationService.insertAllocation(request);
			    		 }
	    				
	    				
	    		  }  			
	    	  }
	      }
	    	
	    	
			}catch(Exception e){
				e.printStackTrace();
				WebUtils.messageAndRedirectUrl(mav, inputDataRow+"번째 행을 확인 해 주세요.("+cause+")", "/allocation/allocation-register.do");
			
	    	}
			
			return mav;
		}
		
		
	
	
	
	@RequestMapping( value="/duplication-list", method = RequestMethod.GET )
	public ModelAndView duplication_list(ModelAndView mav,
			HttpServletRequest request ,HttpServletResponse response ) throws Exception {
		
		
		
		try{
			
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			// String companyId = session.getAttribute("companyId").toString();
			
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			
			
			String[] allocationIdArr = request.getParameter("allocationIdArr").toString().split(",");
			
			List<String> allocationIdList = Arrays.asList(allocationIdArr);
			//String[]  allocationIdList = request.getParameter("allocationIdList").toString();
			//allocationIdList.split(",");
	
			String location = "duplication-list";
			paramMap.put("location", location);
			paramMap.put("allocationIdList", allocationIdList);
	
			
				
			int totalCount = allocationService.selectAllocationListCount(paramMap);
			PagingUtils.setPageing(request, totalCount, paramMap);
			paramMap.put("carrierType", "");
			List<Map<String, Object>> allocationList = allocationService.selectAllocationList(paramMap);
			
			Map<String, Object> map = new HashMap<String, Object>();
			List<Map<String, Object>> companyList = companyService.selectCompanyList(map);

			mav.addObject("userMap",  userMap);
			mav.addObject("companyList",  companyList);
			mav.addObject("listData",  allocationList);
			
			mav.addObject("paramMap", paramMap);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
			
			List<Map<String, Object>> allocationStatusList = allocationStatusService.selectAllocationStatusList(map);
			mav.addObject("allocationStatusList", allocationStatusList);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
		
	}
	
	
	//중복리스트 덮어쓰기 a.jax

	@RequestMapping(value = "/updateDupAllocation", method = {RequestMethod.POST,RequestMethod.GET})
	@ResponseBody
	public ResultApi updateDupAllocation(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			//String[] allocationIdArr =request.getParameterValues("allocationIdArr").toString().split(",");
			//Map<String,Object> map = new HashMap<String, Object>();
			//Map<String,Object> fileMap = fileService.selectFile(paramMap);
			
		
			
			result =	allocationService.insertAllocation(request);
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	
	
	@RequestMapping( value="/excel_download", method = RequestMethod.GET )
	public ModelAndView excel_download(ModelAndView mav,
			HttpServletRequest request ,HttpServletResponse response) throws Exception {
		
		ModelAndView view = new ModelAndView();
		
		try{
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			List<Map<String, Object>> list = null;

			String varNameList[] = null;
			String titleNameList[] = null;
			
			//EchoHandler.sendMessage("tewstestestserersere");
			
			int totalCount = 0;
			
			String carrierType = request.getParameter("carrierType") != null && !request.getParameter("carrierType").toString().equals("") ? request.getParameter("carrierType").toString():"";
			String allocationStatus = request.getParameter("allocationStatus")==null?"Z":request.getParameter("allocationStatus").toString();
			
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			String companyId = session.getAttribute("companyId").toString();
			
			
			if(userMap.get("emp_id").toString().equals("hk0001")) {
				paramMap.put("carrierType","");	
			}else {
				paramMap.put("carrierType",carrierType);
			}
			
			//paramMap.put("companyId",companyId);
			
			if(paramMap.get("searchType") != null && !paramMap.get("searchType").toString().equals("")) {
				
				if(paramMap.get("searchType") != null && !paramMap.get("searchType").equals("")){
					if(paramMap.get("searchType").equals("carrier")){
						if(paramMap.get("searchWord").equals("셀프")){
							paramMap.put("searchWord", "S");
						}else{
							paramMap.put("searchWord", "C");
						}
					}else if(paramMap.get("searchType").equals("distance")){
						if(paramMap.get("searchWord").equals("시내")){
							paramMap.put("searchWord", "C");
						}else if(paramMap.get("searchWord").equals("상행")){
							paramMap.put("searchWord", "U");
						}else if(paramMap.get("searchWord").equals("하행")){
							paramMap.put("searchWord", "D");
						}else if(paramMap.get("searchWord").equals("픽업")){
							paramMap.put("searchWord", "P");
						}
					}
				}
				paramMap.put("allocationStatus", "");
			}else {
				paramMap.put("allocationStatus", allocationStatus);	
			}
			
			String startDt = request.getParameter("startDt") == null ? "" : request.getParameter("startDt").toString();
			String endDt = request.getParameter("endDt") == null ? "" : request.getParameter("endDt").toString();
			String searchDateType = request.getParameter("searchDateType") == null ? "" : request.getParameter("searchDateType").toString();
			paramMap.put("startDt", startDt);
			paramMap.put("endDt", endDt);
			paramMap.put("searchDateType", searchDateType);
			paramMap.put("forExcel", "Y");
			
			totalCount = allocationService.selectAllocationListCount(paramMap);
			paramMap.put("startRownum", 0);
			paramMap.put("numOfRows", totalCount);
			
			String forOrder = request.getParameter("forOrder")==null?"":request.getParameter("forOrder").toString();
			if(!forOrder.equals("")){
				String[] sort = forOrder.split("\\^"); 
				if(sort.length > 1 && !sort[0].equals("") && !sort[1].equals("")){
					paramMap.put("forOrder", sort[0]);
					paramMap.put("order", sort[1]);
					mav.addObject("order",  sort[1]);
				}else {
					paramMap.put("forOrder", "E");
					paramMap.put("order", "asc");
					mav.addObject("order",  "");
				}
			}else {
				paramMap.put("forOrder", "E");
				paramMap.put("order", "asc");
			}
			
			paramMap.put("forExcelDownload", "Y");
			
			list = allocationService.selectAllocationDownloadList(paramMap);
			
			Map<String, Object> map = new HashMap<String, Object>();
			/*
			for(int i = 0; i < list.size(); i++) {
				Map<String, Object> allocation = list.get(i);
				map.put("allocationId", allocation.get("allocation_id").toString());
				
				int amount = 0;
				
				map.put("paymentDivision", "02");
				Map<String, Object> paymentInfo = paymentInfoService.selectPaymentInfoByPaymentDivision(map);
				amount += Integer.parseInt(paymentInfo.get("amount") != null && !paymentInfo.get("amount").toString().equals("") ? paymentInfo.get("amount").toString():"0");
				map.put("paymentDivision", "03");
				paymentInfo = paymentInfoService.selectPaymentInfoByPaymentDivision(map);
				amount += Integer.parseInt(paymentInfo.get("amount") != null && !paymentInfo.get("amount").toString().equals("") ? paymentInfo.get("amount").toString():"0");
				allocation.put("driver_amount", String.valueOf(amount));
				list.set(i, allocation);
			}
			*/
			titleNameList = new String[]{"배차구분","운행구분","의뢰일","출발일","업체","부서","담당자명","차종","차대번호","차량번호","계약번호","출발지","출발지주소","출발지담당자명","출발지담당자연락처","도착지","도착지주소","도착지담당자명","도착지담당자연락처","기사","회차","특이사항","대수","본사청구액","부가세","지급액","결제","비고(운행정보)","차량상태","픽업관리","상차사진제한","하차사진제한","결제방법","증빙구분","탁송상태"};
			
			
			varNameList = new String[]{"allocation_division","run_division","input_dt","departure_dt","customer_name","test","charge_name","car_kind","car_id_num","car_num","contract_num","departure","departure_addr","departure_person_in_charge","departure_phone","arrival","arrival_addr","arrival_person_in_charge","arrival_phone","driver_name","driver_cnt","etc","car_cnt","sales_total","test","driver_amount","test","test","test","test","test","test","payment_kind_nm","billing_division_nm","allocation_status_name"};
			
			view.setViewName("excelDownloadView");
			view.addObject("list", list);
			view.addObject("varNameList", varNameList);
			view.addObject("titleNameList", titleNameList);
			view.addObject("excelName", "allocation-list.xlsx");
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return view;
		
	}
	
	

	@RequestMapping( value="/customer_excel_download", method = RequestMethod.GET )
	public ModelAndView customer_excel_download(ModelAndView mav,
			HttpServletRequest request ,HttpServletResponse response) throws Exception {
		
		ModelAndView view = new ModelAndView();
		
		
		Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
		Map<String,Object> fileMap = new HashMap<String, Object>();
		
		
		try{
			
			String today = WebUtils.getNow("yyyy-MM-dd");
				
			fileMap.put("paramMap",paramMap);
			fileMap.put("fileUploadPath", rootDir+"/forms");
			fileMap.put("fileLogicName", "("+today+")"+".xlsx");
			fileMap.put("filePhysicName", "customer_excel.xlsx");
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return new ModelAndView("downloadView", "downloadFile", fileMap);
		
	}
	
	
	
	@RequestMapping(value = "/sendSocketMessage", method = {RequestMethod.POST,RequestMethod.GET})
	@ResponseBody
	public ResultApi sendSocketMessage(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			
			String allocationId = request.getParameter("allocationId").toString();
			String driverId = request.getParameter("driverId").toString();
			Map<String,Object> map = new HashMap<String, Object>();
			//allocationId
			map.put("allocationId", allocationId);
			Map<String, Object> allocationMap = allocationService.selectAllocation(map);
			map.put("driverId", driverId);
			Map<String, Object> driverMap = driverService.selectDriver(map);
			String resultMsg = "<a style='cursor:pointer;' href='/allocation/allocation-view?allocationId=\""+allocationId+"\">"+ driverMap.get("driver_name").toString()+"기사님이 탁송을 완료 하였습니다." + "</a>";
			
			EchoHandler.sendMessage(resultMsg,allocationId,driverId,driverMap.get("driver_name").toString());
			
			AllocationFinishInfoVO finishInfoVO = new AllocationFinishInfoVO();
			finishInfoVO.setFinishInfoId("AFI"+UUID.randomUUID().toString().replaceAll("-",""));
			finishInfoVO.setAllocationId(allocationId);
			finishInfoVO.setDriverId(driverId);
			finishInfoVO.setDriverName(driverMap.get("driver_name").toString());
			finishInfoVO.setFinishMessage(driverMap.get("driver_name").toString()+"기사님이 탁송을 완료 하였습니다.");
			allocationFinishInfoService.insertAllocationFinishInfo(finishInfoVO);
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	
	
	// 고객용 어플에서 탁송 예약했을 때 전산에서 보이는 웹소켓
	@RequestMapping(value = "/sendAllocationSocket", method = {RequestMethod.POST,RequestMethod.GET})
	@ResponseBody
	public ResultApi sendAllocationSocket(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response ,AlarmSocketVO alarmSocketVO
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			
			String allocationId = request.getParameter("allocationId").toString();
			
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("allocationId", allocationId);
			
			Map<String, Object> allocationMap = allocationService.selectAllocation(map);
			
			alarmSocketVO.setAllocationId(allocationId);
			alarmSocketVO.setSocketId("SOC"+UUID.randomUUID().toString().replaceAll("-",""));
			alarmSocketVO.setText("★어플 탁송 예약★ 확정버튼을 눌러주세요 상차지 :"+allocationMap.get("departure").toString()+" 하차지 :"+allocationMap.get("arrival").toString());
			
			
			
			allocationService.insertSocketMessage(alarmSocketVO);
			EchoHandler.sendMessage(allocationId);
			
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	//직원간 쪽지 전송했을 때 뜨는 웹소켓 알림 // 쪽지 insert
	@RequestMapping(value = "/sendEmployeeMessage", method = {RequestMethod.POST,RequestMethod.GET})
	@ResponseBody
	public ResultApi sendEmployeeMessage(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response ,AlarmSocketVO alarmSocketVO
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			Map<String,Object> empMap = empService.selectIdChectkForPassword(paramMap);
			
			
			
			Map<String,Object> insertMessageMap = new HashMap<String, Object>();
			insertMessageMap.put("messageId", "MSI"+UUID.randomUUID().toString().replaceAll("-", ""));
			insertMessageMap.put("sendEmpId", userMap.get("emp_id").toString());
			insertMessageMap.put("receiveEmpName", empMap.get("emp_name").toString());
			insertMessageMap.put("receiveEmpId",paramMap.get("empId").toString());
			insertMessageMap.put("text",paramMap.get("textarea").toString());
			insertMessageMap.put("sendEmpName",userMap.get("emp_name").toString());
			insertMessageMap.put("deleteDetail","N");

			 empService.insertEmployeeMessage(insertMessageMap);
						
			 
		
		    
			EchoHandler.sendNote(paramMap.get("empId").toString());
			
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	

	
	
	
	
	//입력 되어 있는 모든 배차 정보의 매출처, 매출액,기사정보를 업데이트 한다.
	@RequestMapping( value="/allocation-update", method = RequestMethod.GET )
	public ModelAndView allocationUpdate(ModelAndView mav,
			HttpServletRequest request ,HttpServletResponse response) throws Exception {
		
		ModelAndView view = new ModelAndView();
		
		try{
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			
	
			//현재 등록 되어 있는 모든 배차 정보를 가지고 온다.
			List<Map<String, Object>> allocationAllList = allocationService.selectAllocationAllList();
	
			for(int i = 0; i < allocationAllList.size(); i++) {
				
				Map<String, Object> map = new HashMap<String, Object>();
				Map<String, Object> allocation = allocationAllList.get(i);
				String allocationId = allocation.get("allocation_id").toString();
				String customerId = allocation.get("customer_id").toString();
				String customerName = allocation.get("customer_name").toString();
				
				map.put("allocationId", allocationId);
				map.put("paymentDivision", "01");		
				map.put("paymentPartner", customerName);
				map.put("paymentPartnerId", customerId);
				map.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
				
				Map<String, Object> paymentInfo = null; 
				paymentInfo = paymentInfoService.selectPaymentInfoByPaymentDivision(map);
				if(paymentInfo != null && (paymentInfo.get("payment_partner_id") == null || paymentInfo.get("payment_partner_id").toString().equals(""))) {			//결제 정보의 매출처 정보 업데이트
					paymentInfoService.updatePaymentInfoDriverInfo(map);	
				}
				
				Map<String, Object> carInfo = carInfoService.selectCarInfo(map);
				if(carInfo.get("driver_id") != null && !carInfo.get("driver_id").toString().equals("")) {		//기사가 배정 되어 있는 배차 정보이면
					
					String driverId = carInfo.get("driver_id").toString();
					String driverName = carInfo.get("driver_name").toString();
					
					map.put("driverId", driverId);
					Map<String, Object> driverMap = driverService.selectDriver(map);
					
					if(driverMap != null) {
						
						String deductionRate  = driverMap.get("deduction_rate") != null && !driverMap.get("deduction_rate").toString().equals("") ? driverMap.get("deduction_rate").toString(): "0";
						map.put("paymentPartner", driverName);
						map.put("paymentPartnerId", driverId);
						map.put("paymentDivision", "02");				//결제정보의 기사정보
						map.put("deductionRate", deductionRate);

						paymentInfoService.updatePaymentInfoDriverInfo(map);
						paymentInfo = paymentInfoService.selectPaymentInfoByPaymentDivision(map);
						if(paymentInfo != null) {
							if(paymentInfo.get("amount") != null && !paymentInfo.get("amount").toString().equals("")) {
								if(!paymentInfo.get("amount").toString().equals("0")) {
									String amount = paymentInfo.get("amount").toString();
									amount = amount.replaceAll(",", "");
									int billForPayment = Integer.parseInt(amount)-(Integer.parseInt(deductionRate)*Integer.parseInt(amount)/100);
									map.put("billForPayment", String.valueOf(billForPayment));
									paymentInfoService.updatePaymentInfoDriverAmount(map);
								}else {
									//0이면
									String amount = paymentInfo.get("amount").toString();
									amount = amount.replaceAll(",", "");
									int billForPayment = 0;
									map.put("billForPayment", String.valueOf(billForPayment));
									paymentInfoService.updatePaymentInfoDriverAmount(map);
								}
							}
						}
					}
					
				}
				map.put("logType", "U");
				paymentInfoService.insertPaymentInfoLogByMap(map); //log 기록
			}
	
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return view;
		
	}
	
	@RequestMapping(value = "/updateAmountForPickup", method = {RequestMethod.POST,RequestMethod.GET})
	@ResponseBody
	public ResultApi updateAmountForPickup(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			String allocationId = request.getParameter("allocationId").toString();
			String paymentDivision = request.getParameter("paymentDivision").toString();
			String vatIncludeYn = request.getParameter("vatIncludeYn").toString();
			String vatExcludeYn = request.getParameter("vatExcludeYn").toString();
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("allocationId", allocationId);
			Map<String, Object> allocationMap = allocationService.selectAllocation(map);
			map.put("batchStatusId", allocationMap.get("batch_status_id"));
			List<Map<String, Object>> allocationList = allocationService.selectAllocationList(map);
			for(int j = 0; j < allocationList.size(); j++) {
				Map<String, Object> updateMap = new HashMap<String, Object>();
				updateMap.put("allocationId", allocationList.get(j).get("allocation_id").toString());
				updateMap.put("paymentDivision", paymentDivision);
				updateMap.put("vatIncludeYn", vatIncludeYn);
				updateMap.put("vatExcludeYn", vatExcludeYn);
				int amount = Integer.parseInt(allocationList.get(j).get("sales_total").toString().replaceAll(",", ""));
				int vat = Integer.parseInt(allocationList.get(j).get("vat").toString().replaceAll(",", ""));
				int price = Integer.parseInt(allocationList.get(j).get("price").toString().replaceAll(",", ""));
				
				if(allocationList.get(j).get("vat_include_yn").toString().equals("N") && vatIncludeYn.equals("Y")) {  //미포함 -> 포함
					
					int amountb = (int) Math.round(amount/1.1);
					int vatb = (int)Math.round(amount/1.1/10); 
					int priceb = (int)Math.round(amountb+vatb);
					if(priceb%10 != 0){
						priceb = priceb-(priceb%10);
					}
					amount = amountb;
					vat = vatb;
					price = priceb;
					
				}else if(allocationList.get(j).get("vat_exclude_yn").toString().equals("N") && vatExcludeYn.equals("Y")) { //미별도 -> 별도
					vat = (int)Math.round(amount/10); 
					price = amount+vat;
				}else if(allocationList.get(j).get("vat_include_yn").toString().equals("Y") && vatIncludeYn.equals("N")) { //포함 -> 미포함
					amount = price;
					vat = 0;
				}else if(allocationList.get(j).get("vat_exclude_yn").toString().equals("Y") && vatExcludeYn.equals("N")) { //별도 -> 미별도
					vat = 0; 
					price = amount;
				}
				
				updateMap.put("amount", amount);
				updateMap.put("vat", vat);
				updateMap.put("price", price);
				updateMap.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
				updateMap.put("logType","U"); 
				carInfoService.updateCarInfoSalesTotal(updateMap);
				carInfoService.insertCarInfoLogByMap(updateMap); //log기록
				
				paymentInfoService.updatePaymentInfoAmount(updateMap);
				paymentInfoService.insertPaymentInfoLogByMap(updateMap); //log 기록
				
			}	
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	@RequestMapping(value = "/updateAmount", method = {RequestMethod.POST,RequestMethod.GET})
	@ResponseBody
	public ResultApi updateAmount(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			
			String allocationId = request.getParameter("allocationId").toString();
			String paymentDivision = request.getParameter("paymentDivision").toString();
			String amount = request.getParameter("amount").toString();
			String vat = request.getParameter("vat").toString();
			String price = request.getParameter("price").toString();
			String vatIncludeYn = request.getParameter("vatIncludeYn").toString();
			String vatExcludeYn = request.getParameter("vatExcludeYn").toString();
			
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("allocationId", allocationId);
			map.put("paymentDivision", paymentDivision);
			map.put("amount", amount);
			map.put("vat", vat);
			map.put("price", price);
			map.put("vatIncludeYn", vatIncludeYn);
			map.put("vatExcludeYn", vatExcludeYn);
			map.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
			map.put("logType", "U");
			carInfoService.updateCarInfoSalesTotal(map);
			carInfoService.insertCarInfoLogByMap(map);//log기록
			
			paymentInfoService.updatePaymentInfoAmount(map);
			paymentInfoService.insertPaymentInfoLogByMap(map); //log 기록
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	

	
	@RequestMapping(value = "/getAddressList", method = {RequestMethod.POST,RequestMethod.GET})
	@ResponseBody
	public ResultApi getAddressList(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			HttpSession session = request.getSession();
			String companyId = session.getAttribute("companyId").toString();
			
			String keyword = request.getParameter("keyword").toString();
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("keyword", keyword);
			map.put("companyId", companyId);
			List<Map<String, Object>> addressInfoList = addressInfoService.selectAddressInfoListByKeyWord(map);
			result.setResultData(addressInfoList);
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	
	

	@RequestMapping(value = "/excelUploadForDecideStatus", method = RequestMethod.POST)
	public ModelAndView excelUploadForDecideStatus(ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception{
		
		ResultApi resultApi = new ResultApi();
		int inputDataRow = 0;
		try{
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
	    	List<MultipartFile> fileList = multipartRequest.getFiles("bbsFile");
			
			List<AddressInfoVO> addressInfoList = new ArrayList<AddressInfoVO>();
			
	    	if(fileList != null && fileList.size() > 0){
	    		for(MultipartFile mFile : fileList){	
	    			if(mFile != null && mFile.getSize() > 0){
	    				
	    				Long fileSize = mFile.getSize();
	    				String fileName = mFile.getOriginalFilename();		
	    				String ext = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length());
	    				File file = new File(mFile.getOriginalFilename());
	    				mFile.transferTo(file);
	    				
	    				
	    				XSSFWorkbook wb = null;
	    				XSSFRow row;
	    				XSSFCell cell;
	    				try {//엑셀 파일 오픈
	    					wb = new XSSFWorkbook(new FileInputStream(file));
	    				} catch (FileNotFoundException e) {
	    					e.printStackTrace();
	    				} catch (IOException e) {
	    					e.printStackTrace();
	    				}
	    				XSSFSheet sheet = wb.getSheetAt(0);         
	    		        
	    				CellStyle style = wb.createCellStyle();
	    				style.setFillForegroundColor(IndexedColors.PALE_BLUE.getIndex());
	    		        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    		        style.setAlignment(CellStyle.ALIGN_CENTER);
	    		        style.setBorderBottom(CellStyle.BORDER_DOUBLE);
	    		        style.setBorderTop(CellStyle.BORDER_DOUBLE);
	    		        style.setBorderLeft(CellStyle.BORDER_DOUBLE);
	    		        style.setBorderRight(CellStyle.BORDER_DOUBLE);
	    		        
	    		        
	    				int rows = sheet.getLastRowNum();
	    				int cells = sheet.getRow(0).getLastCellNum();
	    				
	    				FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
	    				
	    				 for (int r = 2; r <= rows; r++) {
	    					 inputDataRow = r+1;
	    					 Map<String, Object> map = new HashMap<String, Object>();
	    					 FormulaEvaluator formulaEval = wb.getCreationHelper().createFormulaEvaluator();
	    					 
	    			        	row = sheet.getRow(r); // row 가져오기
	    			        	if (row != null) {
	    			        		for (int c = 0; c < cells; c++) {
	    			        			cell = row.getCell(c);
	    			        			if (cell != null) { 
	    			        				String value = "";

	    									switch (cell.getCellType()) {
	    									   	case XSSFCell.CELL_TYPE_FORMULA:
	    									   		
	    									   		CellValue evaluate = formulaEval.evaluate(cell);
	    									   	  if( evaluate != null ) {
	    									   		  
	    									   		  try {
	    									   			  
	    									   			  if(WebUtils.isNumber(evaluate.formatAsString())) {
	    									   				  
	    									   				value=  String.valueOf(new Double(evaluate.getNumberValue()).intValue());
	    									   				   
	    									   			  }else {
	    									   				  
	    									   				  value = evaluate.formatAsString();
	    									   			  }
	    									   			  
	    									   		  }catch(Exception e) {
	    									   			  e.printStackTrace();
	    									   			  
	    									   		  }
	    									   		  
	    									   		  
	 	    			        				   }else {
	 	    			        					   
	 	    			        					   value = "";
	 	    			        				   }
	    									   		
	    									   		/*
	    									   		if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_NUMERIC){
	    									   			value = "" + (int)cell.getNumericCellValue();
	    									   		}else if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_STRING){
	    									   			value = "" + cell.getStringCellValue(); 
	    									   		}
	    									   		value = cell.getCellFormula();
	    									   		*/
	    									   		
	    									   		break;
	    									   	case XSSFCell.CELL_TYPE_NUMERIC:
	    									   		value = "" + (int)cell.getNumericCellValue();
	    									   		break;
	    									   	case XSSFCell.CELL_TYPE_STRING:
	    									   		value = "" + cell.getStringCellValue();
	    									   		break;
	    									   	case XSSFCell.CELL_TYPE_BLANK:
	    									   		value = "";
	    									   		break;
	    									   	case XSSFCell.CELL_TYPE_ERROR:
	    									   		value = "" + cell.getErrorCellValue();
	    									   		break;
	    									   	default:
	    									}
	    									if(value != null && !value.equals("")) {
	    										value = value.trim();
	    									}
	    									int nIndex = cell.getColumnIndex();
	    									if(nIndex == 0){
	    										
	    									}else if(nIndex == 1){
	    										
	    										
	    									}else if(nIndex == 2){
	    										
	    										
	    									}else if(nIndex == 3){
	    										
	    										
	    									}else if(nIndex == 4){
	    										//차종
	    										map.put("carKind",value);
	    									}else if(nIndex == 5){
	    										//차대번호
	    										map.put("carIdNum",value);
	    									}else if(nIndex == 6){
	    										//차량번호
	    										map.put("carNum",value);
	    									}else if(nIndex == 7){
	    										//계약번호
	    										map.put("contractNum",value);
	    									}else if(nIndex == 8){
	    										//출발지
	    										map.put("departure",value);
	    									}else if(nIndex == 9){
	    										//도착지
	    										map.put("arrival",value);
	    									}else if(nIndex == 10){
	    										//청구액(,삭제 해야함)
	    										value = value.replaceAll(",", "");
	    										map.put("salesTotal",value);
	    									}
	    									
	    			        			} else {
	    			        				System.out.print("[null]\t");
	    			        			}
	    			        		} // for(c) 문
	    			        		
	    			        		
	    			        		List<Map<String, Object>> allocationList = null;
	    			        		//여기서 알고리즘 구현
	    			        		//먼저 차대번호로 해당 배차를 찾는다.
	    			        		if(!map.get("carIdNum").toString().equals("")) {
	    			        			allocationList = carInfoService.selectCarInfoListByCarIdNum(map);
	    			        		}else if(!map.get("carNum").toString().equals("")) {
	    			        			allocationList = carInfoService.selectCarInfoListByCarNum(map);
	    			        		}else if(!map.get("contractNum").toString().equals("")) {
	    			        			allocationList = carInfoService.selectCarInfoListByContractNum(map);
	    			        		}
	    			        		
	    			        		if(allocationList != null && allocationList.size() > 0) {
    			        				for(int i = 0; i < allocationList.size(); i++) {
    			        					Map<String, Object> allocation = allocationList.get(i);
    			        					if(allocation.get("batch_status").toString().equals("P")){			//픽업인경우
    			        						//해당하는 배차를 모두 확정으로 바꿔준다.
    			        						Map<String, Object> batchMap = new HashMap<String, Object>();
    			        						batchMap.put("batchStatusId", allocation.get("batch_status_id"));
    			        						batchMap.put("startRownum", 0);
    			        						batchMap.put("numOfRows", 20);
    			        						
    			        						List<Map<String, Object>> batchList = allocationService.selectAllocationList(batchMap);
    			        						for(int j = 0; j < batchList.size(); j++) {
    			        							Map<String, Object> batch = batchList.get(j);
    			        							Map<String, Object> updateMap = new HashMap<String, Object>();
    			        							updateMap.put("setDecideStatus", "Y");
    			        							updateMap.put("allocationId", batch.get("allocation_id").toString());
    			        							updateMap.put("currentDecideStatus", "N");
    			        							updateMap.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
    			        							updateMap.put("logType", "U");
    			        							carInfoService.updateCarInfoDecideStatus(updateMap);
    			        							carInfoService.insertCarInfoLogByMap(updateMap); //로그기록
    			        							
    			        							cell = row.getCell(5);
    		    			        				cell.setCellStyle(style);
    		    			        				cell = row.getCell(12);
    		    			        				cell.setCellValue(String.valueOf(batchList.size()));
    		    			        				cell = row.getCell(13);
    		    			        				cell.setCellValue("픽업");
    			        						}
    			        						
    			        					}else {//픽업건이 아닌경우
    			        						
    			        						String departure = map.get("departure").toString();
    			        						String arrival = map.get("arrival").toString();
    			        						String allocationId = allocation.get("allocation_id").toString();
    			        						Map<String, Object> selectMap = new HashMap<String, Object>();
    			        						selectMap.put("departure", departure);
    			        						selectMap.put("arrival", arrival);
    			        						selectMap.put("allocationId", allocationId);
    			        						
    			        						//출발지와 도착지가 같은경우에만 업데이트 한다.
    			        						Map<String, Object> test = carInfoService.selectCarInfo(selectMap);
    			        						if(test.get("departure") != null && test.get("arrival") != null && test.get("departure").toString().equals(map.get("departure").toString()) && test.get("arrival").toString().equals(map.get("arrival").toString())) {
    			        							Map<String, Object> updateMap = new HashMap<String, Object>();
    			        							updateMap.put("setDecideStatus", "Y");
    			        							updateMap.put("allocationId", allocation.get("allocation_id").toString());
    			        							updateMap.put("currentDecideStatus", "N");
    			        							updateMap.put("modId", userMap.get("emp_id").toString()); //수정자
    			        							updateMap.put("logType", "U");
    			        							carInfoService.updateCarInfoDecideStatus(updateMap);
    			        							carInfoService.insertCarInfoLogByMap(updateMap); //로그기록
    			        							cell = row.getCell(5);
    		    			        				cell.setCellStyle(style);	
    		    			        				cell = row.getCell(12);
    		    			        				cell.setCellValue("1");
    		    			        				cell = row.getCell(13);
    		    			        				cell.setCellValue("일반");
    			        						}
    			        						
    			        					}
    			        				}
    			        				//리스트가 있는경우 확정으로 변경 되었을테고 변경된 열의 차대번호 셀의 디자인을 바꿔준다..................
    			        				
    			        				
    			        			}

	    			        	}
	    			        	
	    			        } // for(r) 문
	    				
	    				//여기서 파일 다운로드
	    		    		
	    				 	try {
	    		    	        FileOutputStream outputStream = new FileOutputStream(file);
	    		    	        wb.write(outputStream);
	    		    	    } catch (Exception e) {
	    		    	        e.printStackTrace();

	    		    	    }

	    			}
	    		}
	    		
	    		/*for(int i = 0; i < addressInfoList.size(); i++) {
	    			addressInfoService.insertAddressInfo(addressInfoList.get(i));	    			
	    		}*/
	    		
	    		
	    		
	    	}else{
	    		resultApi.setResultCode("1111"); 	//file 없음
	    	}
			
			//resultApi = fileService.insertFile(rootDir, subDir,uuid,request);
			WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/baseinfo/customer.do");
		}catch(Exception e){
			WebUtils.messageAndRedirectUrl(mav, inputDataRow+"번째 행을 확인 해 주세요.", "/baseinfo/customer.do");
			//WebUtils.messageAndRedirectUrl(mav, "등록하는데 실패 하였습니다.관리자에게 문의 하세요.", "/baseinfo/customer.do");
			e.printStackTrace();
		}
		
		return mav;
	}
	
	
	@RequestMapping(value = "/allocation-excel-insert-finish", method = RequestMethod.POST)
	public ModelAndView allocationExcelInsertFinish(ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception{
		
		ResultApi resultApi = new ResultApi();
		int inputDataRow = 0;
		String cause = "";					//업로드 실패시 원인을 확인 할 수 있도록 한다.
		try{
			
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
	    	List<MultipartFile> fileList = multipartRequest.getFiles("bbsFile");
			
	    	HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			String companyId = session.getAttribute("companyId").toString();
			
	    	
			List<AllocationVO> allocationList = new ArrayList<AllocationVO>();
			List<CarInfoVO> carInfoList = new ArrayList<CarInfoVO>();
			List<PaymentInfoVO> paymentInfoList = new ArrayList<PaymentInfoVO>();
			
	    	
	    	if(fileList != null && fileList.size() > 0){
	    		for(MultipartFile mFile : fileList){	
	    			if(mFile != null && mFile.getSize() > 0){
	    				
	    				Long fileSize = mFile.getSize();
	    				String fileName = mFile.getOriginalFilename();		
	    				String ext = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length());
	    				File file = new File(mFile.getOriginalFilename());
	    				mFile.transferTo(file);
	    				
	    				
	    				XSSFWorkbook wb = null;
	    				XSSFRow row;
	    				XSSFCell cell;
	    				try {//엑셀 파일 오픈
	    					wb = new XSSFWorkbook(new FileInputStream(file));
	    				} catch (FileNotFoundException e) {
	    					e.printStackTrace();
	    				} catch (IOException e) {
	    					e.printStackTrace();
	    				}
	    				
	    				XSSFSheet sheet = wb.getSheetAt(1);
	    		         //int rows = sheet.getPhysicalNumberOfRows();
	    				//int cells = sheet.getRow(0).getPhysicalNumberOfCells();
	    				int rows = sheet.getLastRowNum();
	    				int cells = sheet.getRow(1).getLastCellNum();
	    				
	    				FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
	    				
	    				 for (int r = 1; r <= rows; r++) {
	    					 inputDataRow = r+1;
	    					 AllocationVO allocationVO = new AllocationVO();
	    					 CarInfoVO carInfoVO = new CarInfoVO();
	    					 Map<String, Object> driverMap = new HashMap<String, Object>();
	    					 Map<String, Object> customerMap = new HashMap<String, Object>();
	    					 String allocationId = "ALO"+UUID.randomUUID().toString().replaceAll("-","");
	    					 allocationVO.setAllocationId(allocationId);
	    					 //allocationVO.setAllocationStatus("N");
	    					 
	    					 
	    					 /*allocationVO.setBatchStatus("N");
	    					 allocationVO.setBatchStatusId("BAT"+UUID.randomUUID().toString().replaceAll("-",""));
	 						 allocationVO.setBatchIndex(String.valueOf(0));*/
	 						 allocationVO.setRegisterId(userMap.get("emp_id").toString());
	 						 allocationVO.setRegisterName(userMap.get("emp_name").toString());
	 						 
	 						 
	 						Map<String, Object> searchMap = new HashMap<String, Object>();
	 						searchMap.put("companyId", companyId);
	 						Map<String, Object> company = companyService.selectCompany(searchMap);
	 						
	 						allocationVO.setCompanyId(company.get("company_id").toString());				//2019.01.31 엑셀 등록시 사업자정보를 무조건 한국카캐리어(법인)으로 등록 되게 함.
	 						allocationVO.setCompanyName(company.get("company_name").toString());			//2019.12.30 엑셀 등록시  사업자 정보는 입력 하는 직원이 소속한 사업자로 등록 되게 하여야 한다..
	 						 
	    					 carInfoVO.setAllocationId(allocationId);
	    					 carInfoVO.setReceiptSendYn("N");
	    					 carInfoVO.setModId(userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자추가
	    					 allocationVO.setModId(userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자추가
	    					 
	    					 String customerAmount = "";
	    					 String driverAmount = "";
	    					 String paymentKind = "";
	    					 String billingDivision = "";
	    					 FormulaEvaluator formulaEval = wb.getCreationHelper().createFormulaEvaluator();
	    					 
	    			        	row = sheet.getRow(r); // row 가져오기
	    			        	if (row != null) {
	    			        		for (int c = 0; c < cells; c++) {
	    			        			cell = row.getCell(c);
	    			        			if (cell != null) { 
	    			        				String value = "";

	    									switch (cell.getCellType()) {
	    									   	case XSSFCell.CELL_TYPE_FORMULA:
	    									   		
	    									   		CellValue evaluate = formulaEval.evaluate(cell);
	    									   	  if( evaluate != null ) {
	    									   		  
	    									   		  try {
	    									   			  
	    									   			  if(WebUtils.isNumber(evaluate.formatAsString())) {
	    									   				  
	    									   				value=  String.valueOf(new Double(evaluate.getNumberValue()).intValue());
	    									   				   
	    									   			  }else {
	    									   				  
	    									   				  value = evaluate.formatAsString();
	    									   			  }
	    									   			  
	    									   		  }catch(Exception e) {
	    									   			  e.printStackTrace();
	    									   			  
	    									   		  }
	    									   		  
	    									   		  
	 	    			        				   }else {
	 	    			        					   
	 	    			        					   value = "";
	 	    			        				   }
	    									   		
	    									   		/*
	    									   		if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_NUMERIC){
	    									   			value = "" + (int)cell.getNumericCellValue();
	    									   		}else if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_STRING){
	    									   			value = "" + cell.getStringCellValue(); 
	    									   		}
	    									   		value = cell.getCellFormula();
	    									   		*/
	    									   		
	    									   		break;
	    									   	case XSSFCell.CELL_TYPE_NUMERIC:
	    								            if (DateUtil.isCellDateFormatted(cell)){  
	    								                //날짜  
	    								            	Date date = cell.getDateCellValue();
	    								                value = String.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(date));
	    								            }else{  
	    								                //수치
	    								            	value = NumberToTextConverter.toText(cell.getNumericCellValue());
	    								            	
	    								                //value = String.valueOf(cell.getNumericCellValue());  
	    								            } 
	    									   		//value = "" + (int)cell.getNumericCellValue();
	    									   		break;
	    									   	case XSSFCell.CELL_TYPE_STRING:
	    									   		value = "" + cell.getStringCellValue();
	    									   		break;
	    									   	case XSSFCell.CELL_TYPE_BLANK:
	    									   		value = "";
	    									   		break;
	    									   	case XSSFCell.CELL_TYPE_ERROR:
	    									   		value = "" + cell.getErrorCellValue();
	    									   		break;
	    									   	default:
	    									}
	    									if(value != null && !value.equals("")) {
	    										value = value.trim();
	    									}
	    									int nIndex = cell.getColumnIndex();
	    									if(nIndex == 0){
	    										
	    										if(value != null && !value.equals("")) {
	    											Map<String, Object> map = new HashMap<String, Object>();
		    										map.put("allocationDivision",value);
		    										Map<String, Object> allocationDivision = allocationDivisionService.selectAllocationDivisionInfo(map);
		    										carInfoVO.setCarrierType(allocationDivision.get("allocation_division_cd").toString());	
	    										}else {		//배차 구분이 지정 되어 있지 않으면 익셉션
	    											cause = "배차 구분이 지정 되지 않았습니다.";
	    											throw new Exception();
	    										}
	    									}else if(nIndex == 1){
	    										Map<String, Object> map = new HashMap<String, Object>();
	    										map.put("runDivision",value);
	    										Map<String, Object> runDivision = runDivisionService.selectRunDivisionInfo(map);
	    										carInfoVO.setDistanceType(runDivision.get("run_division_cd").toString());
	    									}else if(nIndex == 2){
	    										String dateDay = WebUtils.getDateDay(value);
	    										allocationVO.setInputDt(value+" ("+dateDay+")");
	    									}else if(nIndex == 3){
	    										carInfoVO.setDepartureDt(value);
	    									}else if(nIndex == 4){
	    										Map<String, Object> map = new HashMap<String, Object>();
	    										map.put("customerName",value);
	    										Map<String, Object> customer = customerService.selectCustomerByCustomerName(map);
	    										if(customer != null) {
	    											allocationVO.setCustomerId(customer.get("customer_id").toString());
		    										allocationVO.setCustomerName(customer.get("customer_name").toString());
		    										allocationVO.setCustomerSignificantData(customer.get("significant_data") != null && !customer.get("significant_data").toString().equals("") ? customer.get("significant_data").toString():"");
	    										}else {		//거래처가 없는경우 익셉션
	    											cause = "거래처가 등록 되어 있지 않습니다.";
	    											throw new Exception();
	    										}
	    									}else if(nIndex == 5){
	    										//업체(담당자 부서로 사용 하고 있으나 현재 배차 등록에는 필요 하지 않아 일단 사용하지 않음..
	    									}else if(nIndex == 6){
	    										//업체의 담당자가 한명만 있는경우 그 담당자가 해당 배차의 담당자가 되도록 진행. 
	    										Map<String, Object> map = new HashMap<String, Object>();
	    										map.put("customerId",allocationVO.getCustomerId());
	    										map.put("name",value);
	    										if(!value.equals("")) {
	    											Map<String, Object> personInCharge = personInChargeService.selectPersonInCharge(map);
	    											
	    											if(value.equals("현대캐피탈")) {
	    												allocationVO.setHcYn("Y");
	    											}
	    											
		    										if(personInCharge != null) {
		    											allocationVO.setChargeName(personInCharge.get("name").toString());
		    											allocationVO.setChargeId(personInCharge.get("person_in_charge_id").toString());
		    											allocationVO.setChargePhone(personInCharge.get("phone_num") != null && !personInCharge.get("phone_num").toString().equals("") ? personInCharge.get("phone_num").toString():"");
		    											allocationVO.setChargeAddr(personInCharge.get("address") != null && !personInCharge.get("address").toString().equals("") ? personInCharge.get("address").toString():"");
		    										}else {
		    											cause = "담당자 정보를 찾을 수 없습니다.";
		    											throw new Exception();	
		    										}
	    										}else {
	    											List<Map<String, Object>> personInChargeList = personInChargeService.selectPersonInChargeList(map);
	    											if(personInChargeList.size() == 1) {			//업체의 담당자가 한명인 경우 해당 배차의 담당자가 된다.
	    												Map<String, Object> personInCharge = personInChargeList.get(0);
	    												allocationVO.setChargeName(personInCharge.get("name").toString());
		    											allocationVO.setChargeId(personInCharge.get("person_in_charge_id").toString());
		    											allocationVO.setChargePhone(personInCharge.get("phone_num") != null && !personInCharge.get("phone_num").toString().equals("") ? personInCharge.get("phone_num").toString():"");
		    											allocationVO.setChargeAddr(personInCharge.get("address") != null && !personInCharge.get("address").toString().equals("") ? personInCharge.get("address").toString():"");
	    											}else {
		    											cause = "담당자가 지정되지 않았습니다.";
		    											throw new Exception();	
		    										}
	    										}
	    									}else if(nIndex == 7){
	    										//차종
	    										carInfoVO.setCarKind(value);
	    									}else if(nIndex == 8){
	    										//차대번호
	    										carInfoVO.setCarIdNum(value);
	    									}else if(nIndex == 9){
	    										//차량번호
	    										carInfoVO.setCarNum(value);
	    									}else if(nIndex == 10){
	    										//계약번호
	    										carInfoVO.setContractNum(value);
	    									}else if(nIndex == 11){
	    										//출발지
	    										if(value != null && !value.equals("")) {
	    											Map<String, Object> map = new HashMap<String, Object>();
		    										map.put("keyword", value);
		    										Map<String, Object> addressInfo = addressInfoService.selectAddressInfo(map);
		    										if(addressInfo != null) {
		    											carInfoVO.setDeparture(value);
		    											carInfoVO.setDepartureAddr(addressInfo.get("address").toString());
		    											carInfoVO.setDeparturePersonInCharge(addressInfo.get("name").toString());
		    											carInfoVO.setDeparturePhone(addressInfo.get("phone_num").toString());
		    										}else {
		    											carInfoVO.setDeparture(value);
		    										}
	    										}else {
	    											carInfoVO.setDeparture(value);
	    										}	    										
	    									}else if(nIndex == 12){
	    										//출발지상세주소
	    										if(value != "") {
	    											carInfoVO.setDepartureAddr(value);
	    										}else {
	    											if(carInfoVO.getDepartureAddr() == null || carInfoVO.getDepartureAddr().equals("")) {
		    											carInfoVO.setDepartureAddr(value);	
		    										}	
	    										}
	    									}else if(nIndex == 13){
	    										//상차지담당자명
	    										if(value != "") {
	    											carInfoVO.setDeparturePersonInCharge(value);
	    										}else {
	    											if(carInfoVO.getDeparturePersonInCharge() == null || carInfoVO.getDeparturePersonInCharge().equals("")) {
		    											carInfoVO.setDeparturePersonInCharge(value);	
		    										}	
	    										}
	    									}else if(nIndex == 14){
	    										//상차지담당자연락처
	    										if(value != "") {
	    											carInfoVO.setDeparturePhone(value);
	    										}else {
	    											if(carInfoVO.getDeparturePhone() == null || carInfoVO.getDeparturePhone().equals("")) {
		    											carInfoVO.setDeparturePhone(value);	
		    										}	
	    										}
	    									}else if(nIndex == 15){
	    										//하차지
	    										if(value != null && !value.equals("")) {
	    											Map<String, Object> map = new HashMap<String, Object>();
		    										map.put("keyword", value);
		    										Map<String, Object> addressInfo = addressInfoService.selectAddressInfo(map);
		    										if(addressInfo != null) {
		    											carInfoVO.setArrival(value);
		    											carInfoVO.setArrivalAddr(addressInfo.get("address").toString());
		    											carInfoVO.setArrivalPersonInCharge(addressInfo.get("name").toString());
		    											carInfoVO.setArrivalPhone(addressInfo.get("phone_num").toString());
		    										}else {
		    											carInfoVO.setArrival(value);
		    										}
	    										}else {
	    											carInfoVO.setArrival(value);
	    										}
	    									}else if(nIndex == 16){
	    										//하차지상세주소
	    										if(value != "") {
	    											carInfoVO.setArrivalAddr(value);
	    										}else {
	    											if(carInfoVO.getArrivalAddr() == null || carInfoVO.getArrivalAddr().equals("")) {
		    											carInfoVO.setArrivalAddr(value);	
		    										}	
	    										}
	    									}else if(nIndex == 17){
	    										//하차지담당자명
	    										if(value != "") {
	    											carInfoVO.setArrivalPersonInCharge(value);
	    										}else {
	    											if(carInfoVO.getArrivalPersonInCharge() == null || carInfoVO.getArrivalPersonInCharge().equals("")) {
		    											carInfoVO.setArrivalPersonInCharge(value);	
		    										}	
	    										}
	    									}else if(nIndex == 18){
	    										//하차지담당자연락처
	    										if(value != "") {
	    											carInfoVO.setArrivalPhone(value);
	    										}else {
	    											if(carInfoVO.getArrivalPhone() == null || carInfoVO.getArrivalPhone().equals("")) {
		    											carInfoVO.setArrivalPhone(value);	
		    										}	
	    										}
	    									}else if(nIndex == 19){
	    										//기사명					//엑셀 등록시 기사명이 등록 되어 있더라도 배차 정보에서 수정 할 수 있도록....	    										
	    										if(!value.equals("")) {
	    											String driverName = value;
		    										Map<String, Object> map = new HashMap<String, Object>();
		    										map.put("driverName", driverName);
		    										map.put("driverStatus", "01");
		    										
		    										driverMap = driverService.selectDriverByDriverName(map);
		    										
		    										if(driverMap != null) {
		    											carInfoVO.setDriverId(driverMap.get("driver_id").toString());
		    											carInfoVO.setDriverName(driverMap.get("driver_name").toString());	
		    										}
		    										//외부기사인 경우
		    										if(driverMap != null && driverMap.get("customer_id") != null && !driverMap.get("customer_id").toString().equals("")) {
		    											map.put("customerId", driverMap.get("customer_id").toString());
		    											customerMap = customerService.selectCustomer(map);
		    										}
	    										}
	    										
	    										/*
	    										//기사명					//엑셀 등록시 기사명이 등록 되어 있더라도 배차 정보에서 수정 할 수 있도록....
	    										if(value != ""){
	    											String driverName = value;
	    											Map<String, Object> map = new HashMap<String, Object>();
	    											map.put("driverName", value);
	    											driverMap = driverService.selectDriverByDriverName(map);
	    											
	    											if(driverMap != null) {
		    											carInfoVO.setDriverId(driverMap.get("driver_id").toString());
		    											carInfoVO.setDriverName(driverMap.get("driver_name").toString());	
		    										}else {
	    												cause = "기사를 찾지 못했습니다.";
		    											throw new Exception();
	    											}
		    										//외부기사인 경우
		    										if(driverMap != null && driverMap.get("customer_id") != null && !driverMap.get("customer_id").toString().equals("")) {
		    											map.put("customerId", driverMap.get("customer_id").toString());
		    											customerMap = customerService.selectCustomer(map);
		    										}
	    											
	    										}else {
	    											cause = "기사를 찾지 못했습니다.";
	    											throw new Exception();
	    										}
	    										*/
	    										
	    										
	    									}else if(nIndex == 20){
	    										//회전수
	    										if(!value.equals("")) {
	    											
	    											try {
	    												int driverCnt = Integer.parseInt(value);
	    											}catch(NumberFormatException e) {
	    												cause = "회전수가 잘못 입력 되었습니다.";
		    											throw new Exception();
	    											}
	    											
	    											//driverMap.put("driverCnt", value);
	    											if(driverMap != null && driverMap.get("driver_id") != null) {
	    												carInfoVO.setDriverCnt(value);	
	    											}else {
	    												cause = "회전수가 지정되었지만 기사가 지정되지 않았습니다.";
		    											throw new Exception();
	    											}
	    										}
	    									}else if(nIndex == 21){
	    										//특이사항//특이사항(비고)
	    										carInfoVO.setEtc(value);
	    									}else if(nIndex == 22){
	    										//car_cnt(대수)
	    										allocationVO.setCarCnt(value);
	    									}else if(nIndex == 23){
	    										//업체청구액
	    										if(value.equals("")) {
	    											value = "0";
	    										}
	    										customerAmount = value.replaceAll(",", "");
	    										carInfoVO.setSalesTotal(customerAmount);
	    										carInfoVO.setDecideStatus("Y");
	    									}else if(nIndex == 24){
	    										//부가세 구분
	    										if(!value.equals("")) {
	    											//포함인경우
	    											if(value.equals("포함")){
	    												carInfoVO.setVatIncludeYn("Y");
	    	    										carInfoVO.setVatExcludeYn("N");
	    	    										int total = Integer.parseInt(customerAmount);
	    	    										int vat = Integer.parseInt(customerAmount)/11;
	    	    										customerAmount = String.valueOf(Integer.parseInt(customerAmount)-vat);
	    	    										carInfoVO.setSalesTotal(customerAmount);
			    										carInfoVO.setVat(String.valueOf(vat));
			    										carInfoVO.setPrice(String.valueOf(total));
	    											//별도인경우
		    										}else if(value.equals("별도")){
		    											carInfoVO.setVatIncludeYn("N");
			    										carInfoVO.setVatExcludeYn("Y");
			    										int vat = Integer.parseInt(customerAmount)/10;
			    										int total = Integer.parseInt(customerAmount)+vat;
			    										carInfoVO.setVat(String.valueOf(vat));
			    										carInfoVO.setPrice(String.valueOf(total));
		    										}else {
		    											carInfoVO.setVat("0");
		    											carInfoVO.setVatIncludeYn("N");
			    										carInfoVO.setVatExcludeYn("N");
		    											carInfoVO.setPrice(customerAmount);
		    										}
	    										}else {
	    											//포함도 아니고 별도도 아닌경우
	    											carInfoVO.setVat("0");
	    											carInfoVO.setVatIncludeYn("N");
		    										carInfoVO.setVatExcludeYn("N");
	    											carInfoVO.setPrice(customerAmount);
	    										}
	    										
	    									}else if(nIndex == 25){
	    										//기사지급액
	    										if(value.equals("")) {
	    											value = "0";
	    										}
	    										driverAmount = value.replaceAll(",", "");
	    									}else if(nIndex == 28){
	    										//미확정,미배차
	    										if(value.equals("미확정")){
	    											allocationVO.setAllocationStatus("U");	
	    										}else if(value.equals("미배차")){
	    											allocationVO.setAllocationStatus("N");
	    										}else if(value.equals("완료")){
	    											allocationVO.setAllocationStatus("F");
	    										}else {
	    											//미확정도 아니고 미배차도 아닌경우 미배차 상태로...
	    											allocationVO.setAllocationStatus("N");
	    										}
	    									}else if(nIndex == 29) {		//픽업관리
	    										if(!value.equals("")) {		//픽업건인경우
	    											allocationVO.setPickupNum(value);
	    											allocationVO.setBatchStatus("P");
	    										}else {							//픽업 아님
	    											allocationVO.setPickupNum("");
	    											allocationVO.setBatchStatus("N");
	    										}
	    										
	    									}else if(nIndex == 30) {		//사진제한
	    										if(!value.equals("")) {
	    											try {
	    												 Double.parseDouble(value);
	    												 carInfoVO.setRequirePicCnt(value);
	    											}catch(NumberFormatException e) {
	    												cause = "사진제한은 숫자로만 입력 가능 합니다.";
		    											throw new Exception();
	    											}
	    										}else {							
	    											carInfoVO.setRequirePicCnt("3");
	    										}
	    									}else if(nIndex == 31) {		//사진제한
	    										if(!value.equals("")) {
	    											try {
	    												 Double.parseDouble(value);
	    												 carInfoVO.setRequireDnPicCnt(value);
	    											}catch(NumberFormatException e) {
	    												cause = "사진제한은 숫자로만 입력 가능 합니다.";
		    											throw new Exception();
	    											}
	    										}else {							
	    											carInfoVO.setRequireDnPicCnt("3");
	    										}
	    									}else if(nIndex == 32) {		//결제방법
	    										if(!value.equals("")) {	    											
	    											paymentKind = value;
	    										}else {							
	    											cause = "결제방법이 입력 되지 않았습니다.";
	    											throw new Exception();
	    										}
	    									}else if(nIndex == 33) {		//증빙구분
	    										if(!value.equals("")) {
	    											billingDivision = value;
	    										}else {							
	    											cause = "증빙구분이 입력 되지 않았습니다.";
	    											throw new Exception();
	    										}
	    									}
	    									allocationVO.setBatchIndex(String.valueOf(0));		//픽업건여부에 관계 없이 index는 0
	    			        			} else {
	    			        				System.out.print("[null]\t");
	    			        			}
	    			        		} // for(c) 문
	    			        			
	    			        		if(carInfoVO.getCarrierType() != null && !carInfoVO.getCarrierType().equals("")) {		//배차 구분이 작성 되지 않은경우 등록 하지 않는다.
	    			        			
	    			        			carInfoList.add(carInfoVO);
	    			        			//allocationService.insertAllocation(allocationVO);
		    			        		//carInfoService.insertCarInfo(carInfoVO);
	    			        			int income = 0;
		    			        		for(int j = 0; j < 3; j++){
		    								PaymentInfoVO paymentInfoVO = new PaymentInfoVO();
		    								paymentInfoVO.setAllocationId(allocationId);
		    								paymentInfoVO.setPaymentDivision("0"+(j+1));
		    								paymentInfoVO.setPayment("N");
		    								paymentInfoVO.setModId(userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자추가 		    								
		    								if(paymentInfoVO.getPaymentDivision().equals("01")) {
		    									paymentInfoVO.setAmount(customerAmount);
		    									paymentInfoVO.setPaymentPartner(allocationVO.getCustomerName());
		    									paymentInfoVO.setPaymentPartnerId(allocationVO.getCustomerId());
		    									paymentInfoVO.setPaymentKind(paymentKind);
		    									paymentInfoVO.setBillingDivision(billingDivision);
		    									income += Integer.parseInt(customerAmount);
		    								}else if(paymentInfoVO.getPaymentDivision().equals("02")) {
		    									
		    									if(customerMap == null || customerMap.get("customer_id") == null) {
		    										if(driverMap != null && driverMap.get("driver_name") != null) {
		    											paymentInfoVO.setPaymentPartner(driverMap.get("driver_name").toString());
		    											paymentInfoVO.setPaymentPartnerId(driverMap.get("driver_id").toString());
		    											paymentInfoVO.setDecideStatus("N");
				    									paymentInfoVO.setDecideMonth("2019-12");
				    									paymentInfoVO.setDecideFinal("N");
				    									if(paymentKind.equals("DD")) {
			    											paymentInfoVO.setPaymentKind(paymentKind);
					    									paymentInfoVO.setBillingDivision(billingDivision);
			    										}else {
			    											paymentInfoVO.setPaymentKind("AT");
					    									paymentInfoVO.setBillingDivision("02");	
			    										}
			    										paymentInfoVO.setAmount(driverAmount);
			    										
			    										if(allocationVO.getHcYn().equals("Y") && driverMap.get("car_assign_company").toString().equals("S")){
			    											
			    											int deductionRate = 0;
			    											deductionRate = Integer.parseInt(driverMap.get("deduction_rate").toString());
			    											if(deductionRate > 10) {
			    		    			        				if(deductionRate == 15) {
			    		    			        					//지입기사이면 14% 아니면 15%
			    		    			        					if(driverMap.get("driver_kind") != null) {
			    		    			        						if(driverMap.get("driver_kind").equals("00") || driverMap.get("driver_kind").equals("03")) {
			    			    			        						deductionRate = 0;
			    			    			        					}else if(driverMap.get("driver_kind").equals("01")) {
			    			    			        						deductionRate = 4;
			    			    			        					}else if(driverMap.get("driver_kind").equals("02")) {
			    			    			        						deductionRate = 5;
			    			    			        					}else {
			    			    			        						deductionRate = 0;
			    			    			        					}
			    		    			        					}else {
			    		    			        						deductionRate = 0;
			    		    			        					}
			    		    			        					
			    		    			        				}else {
			    		    			        					deductionRate = deductionRate-10;
			    		    			        				}
			    		    			        			}else if(deductionRate == 10) {
			    		    			        				deductionRate = 0;
			    		    			        			} 
			    											
			    											int amount = Integer.parseInt(driverAmount);
			    											int billForPayment = amount - (amount*deductionRate/100);
			    											paymentInfoVO.setBillForPayment(String.valueOf(billForPayment));
			    											paymentInfoVO.setDeductionRate(String.valueOf(deductionRate));
			    											
			    										}
			    										
		    										}
		    										income -= Integer.parseInt(driverAmount);	
		    									}else {
		    										paymentInfoVO.setAmount("0");
		    									}
		    									
		    								}else if(paymentInfoVO.getPaymentDivision().equals("03")) {
		    									
		    									if(customerMap != null && customerMap.get("customer_id") != null && !customerMap.get("customer_id").equals("")) {
		    										paymentInfoVO.setPaymentPartner(customerMap.get("customer_name").toString());
			    									paymentInfoVO.setPaymentPartnerId(customerMap.get("customer_id").toString());
			    									if(paymentKind.equals("DD")) {
		    											paymentInfoVO.setPaymentKind(paymentKind);
				    									paymentInfoVO.setBillingDivision(billingDivision);
		    										}else {
		    											paymentInfoVO.setPaymentKind("AT");
				    									paymentInfoVO.setBillingDivision("02");	
		    										}
		    										paymentInfoVO.setAmount(driverAmount);
		    										paymentInfoVO.setDecideStatus("N");
			    									paymentInfoVO.setDecideMonth("2019-12");
			    									paymentInfoVO.setDecideFinal("N");
		    										income -= Integer.parseInt(driverAmount);
		    									}else {
		    										paymentInfoVO.setAmount("0");
		    									}
		    									
		    								}
		    								
		    								paymentInfoList.add(paymentInfoVO);
		    								//paymentInfoService.insertPaymentInfo(paymentInfoVO);
		    							}	
		    			        		allocationVO.setProfit(String.valueOf(income));
		    			        		allocationList.add(allocationVO);
	    			        		}
	    			        	}
	    			        	
	    			        } // for(r) 문
	    				
	    			}
	    		}
	    		
	    		for(int i = 0; i < allocationList.size(); i++) {		//null인 경우 처리
	    			if(allocationList.get(i).getAllocationStatus() == null) {
	    				allocationList.get(i).setAllocationStatus("N");
	    			}
	    			if(allocationList.get(i).getBatchStatus() == null) {
	    				allocationList.get(i).setBatchStatus("N");
	    				allocationList.get(i).setPickupNum("");
	    				allocationList.get(i).setBatchIndex(String.valueOf(0));		//픽업건여부에 관계 없이 index는 0
	    			}
	    		}
	    		
	    		for(int i = 0; i < allocationList.size(); i++) {
	    			
	    			//픽업 관리 번호 "" 가 아니고 관리번호가 같은 배차 정보는 픽업건으로 구분한다.	
	    			//픽업건은 batch_status가 P, batch_status_id가 같으나 batch_index는 0인 배차이다.
	    			String batchStatusId = "BAT"+UUID.randomUUID().toString().replaceAll("-","");
	    			if(allocationList.get(i).getBatchStatusId() == null || allocationList.get(i).getBatchStatusId().equals("")){	//
		    			allocationList.get(i).setBatchStatusId(batchStatusId);	
		    			AllocationVO allocation = allocationList.get(i);
		    			for(int j = i+1; j < allocationList.size(); j++) {
		    				if(allocation.getBatchStatus().equals("P") && allocation.getBatchStatus().equals(allocationList.get(j).getBatchStatus())){//픽업 건이고		
		    					if(allocation.getPickupNum().equals(allocationList.get(j).getPickupNum())){		//픽업 관리 번호가 같으면 같은 픽업건이다.
		    						allocationList.get(j).setBatchStatusId(batchStatusId);
		    					}
		    				}
		    			}
	    			}else {
	    					//여기서는 할게 없네...
	    			}
	    			/*AllocationVO allocation = allocationList.get(i);
	    			for(int j = i+1; j < allocationList.size(); j++) {
	    				if(allocation.getBatchStatus().equals("P") && allocation.getBatchStatus().equals(allocationList.get(j).getBatchStatus())){//픽업 건이고		
	    					if(allocation.getPickupNum().equals(allocationList.get(j).getPickupNum())){		//픽업 관리 번호가 같으면 같은 픽업건이다.
	    						allocationList.get(j).setBatchStatusId(batchStatusId);
	    					}
	    				}
	    			}*/
	    			allocationService.insertAllocation(allocationList.get(i));
	    			
	    			allocationList.get(i).setLogType("I");
	    			allocationService.insertAllocationLogByVO(allocationList.get(i));
	    		}
	    		for(int i = 0; i < carInfoList.size(); i++) {
	    			carInfoService.insertCarInfo(carInfoList.get(i));
	    			carInfoList.get(i).setLogType("I");
		    		carInfoService.insertCarInfoLogByVO(carInfoList.get(i)); //로그기록
	    		}
	    		for(int i = 0; i < paymentInfoList.size(); i++) {
	    			paymentInfoService.insertPaymentInfo(paymentInfoList.get(i));
	    			paymentInfoList.get(i).setLogType("I");
	    			paymentInfoService.insertPaymentInfoLogByVO(paymentInfoList.get(i)); //로그기록
	    		}
	    		
	    		
	    	}else{
	    		resultApi.setResultCode("1111"); 	//file 없음
	    	}
			
			WebUtils.messageAndRedirectUrl(mav,allocationList.size()+"건의 배차 정보가 등록되었습니다.", "/allocation/allocation-register.do");
		}catch(Exception e){
			WebUtils.messageAndRedirectUrl(mav, inputDataRow+"번째 행을 확인 해 주세요.("+cause+")", "/allocation/allocation-register.do");
			e.printStackTrace();
		}
		
		return mav;
	}
	
	
	@RequestMapping(value = "/selectPaymentInfoListForChange", method = RequestMethod.GET)
	
	public ModelAndView selectPaymentInfoListForChange(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		
		try{
			
			HttpSession session = request.getSession(); 
			Map<String, Object> map = new HashMap<String, Object>();
			Map userMap = (Map)session.getAttribute("user");
			List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoListForChange(map);
			
			
			for(int i = 0; i < paymentInfoList.size(); i++) {
				
				Map<String, Object> paymentInfo = paymentInfoList.get(i);
				
				String driverId = paymentInfo.get("payment_partner_id").toString();
				Map<String, Object> findMap = new HashMap<String, Object>();
				findMap.put("driverId", driverId);
				Map<String, Object> driver = driverService.selectDriver(findMap);
				String customerId = driver.get("customer_id").toString();
				findMap.put("customerId", customerId);
				Map<String, Object> customer = customerService.selectCustomer(findMap);
				
				Map<String, Object> updateMap = new HashMap<String, Object>();
				
				updateMap.put("allocationId", paymentInfo.get("allocation_id").toString());
				updateMap.put("paymentDivision", "03");
				updateMap.put("paymentPartner", customer.get("customer_name").toString());
				updateMap.put("paymentPartnerId", customer.get("customer_id").toString());
				updateMap.put("paymentKind", paymentInfo.get("payment_kind") == null ? "": paymentInfo.get("payment_kind").toString());
				updateMap.put("billingDivision", paymentInfo.get("billing_division") == null ? "": paymentInfo.get("billing_division").toString());
				updateMap.put("payment", paymentInfo.get("payment") == null ? "": paymentInfo.get("payment").toString());
				updateMap.put("paymentDt", paymentInfo.get("payment_dt") == null ? "": paymentInfo.get("payment_dt").toString());
				updateMap.put("billingDt", paymentInfo.get("billing_dt") == null ? "": paymentInfo.get("billing_dt").toString());
				updateMap.put("amount", paymentInfo.get("amount") == null ? "": paymentInfo.get("amount").toString());
				updateMap.put("etc", paymentInfo.get("etc") == null ? "": paymentInfo.get("etc").toString());
				updateMap.put("billingStatus", paymentInfo.get("billing_status") == null ? "": paymentInfo.get("billing_status").toString());
				updateMap.put("depositId", paymentInfo.get("deposit_id") == null ? "": paymentInfo.get("deposit_id").toString());
				updateMap.put("decideStatus", paymentInfo.get("decide_status") == null ? "": paymentInfo.get("decide_status").toString());
				updateMap.put("selectedStatus", paymentInfo.get("selected_status") == null ? "": paymentInfo.get("selected_status").toString());
				updateMap.put("decideMonth", paymentInfo.get("decide_month") == null ? "": paymentInfo.get("decide_month").toString());
				updateMap.put("decideFinal", paymentInfo.get("decide_final") == null ? "": paymentInfo.get("decide_final").toString());
				updateMap.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
				//paymentInfoService.updatePaymentInfoChange(updateMap);
				updateMap.put("allocationId", paymentInfo.get("allocation_id").toString());
				updateMap.put("paymentDivision", "02");
				updateMap.put("paymentPartner", "");
				updateMap.put("paymentPartnerId", "");
				updateMap.put("paymentKind", "");
				updateMap.put("billingDivision", "");
				updateMap.put("payment", "");
				updateMap.put("paymentDt", "");
				updateMap.put("billingDt", "");
				updateMap.put("amount", "");
				updateMap.put("etc", "");
				updateMap.put("billingStatus", "N");
				updateMap.put("depositId", "");
				updateMap.put("decideStatus", "N");
				updateMap.put("selectedStatus", "N");
				updateMap.put("decideMonth", "");
				updateMap.put("decideFinal", "N");
				updateMap.put("modId", userMap.get("emp_id").toString()); //수정자
				updateMap.put("logType", "U");  
				paymentInfoService.updatePaymentInfoChange(updateMap);
				paymentInfoService.updatePaymentInfoInit(updateMap);
				paymentInfoService.insertPaymentInfoLogByMap(updateMap); //log 기록
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
		
		}
		
		return mav;
	}
	
	
	
	
	
	//현대캐피탈 기사 지급금 수정 전용
	@RequestMapping(value = "/excelUploadForHcDriverAmount", method = RequestMethod.POST)
	public ModelAndView excelUploadForHcDriverAmount(ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception{
		
		ResultApi resultApi = new ResultApi();
		int inputDataRow = 0;
		String cause = "";					//업로드 실패시 원인을 확인 할 수 있도록 한다.
		try{
			
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
	    	List<MultipartFile> fileList = multipartRequest.getFiles("bbsFile");
			
	    	HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
	    	
	    	if(fileList != null && fileList.size() > 0){
	    		for(MultipartFile mFile : fileList){	
	    			if(mFile != null && mFile.getSize() > 0){
	    				
	    				Long fileSize = mFile.getSize();
	    				String fileName = mFile.getOriginalFilename();		
	    				String ext = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length());
	    				File file = new File(mFile.getOriginalFilename());
	    				mFile.transferTo(file);
	    				
	    				
	    				XSSFWorkbook wb = null;
	    				XSSFRow row;
	    				XSSFCell cell;
	    				try {//엑셀 파일 오픈
	    					wb = new XSSFWorkbook(new FileInputStream(file));
	    				} catch (FileNotFoundException e) {
	    					e.printStackTrace();
	    				} catch (IOException e) {
	    					e.printStackTrace();
	    				}
	    				
	    				XSSFSheet sheet = wb.getSheetAt(0);
	    		         //int rows = sheet.getPhysicalNumberOfRows();
	    				//int cells = sheet.getRow(0).getPhysicalNumberOfCells();
	    				int rows = sheet.getLastRowNum();
	    				int cells = sheet.getRow(0).getLastCellNum();
	    				
	    				FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
	    				
	    				 for (int r = 0; r <= rows; r++) {
	    					 inputDataRow = r+1;
	    					 
	    					 Map<String, Object> map = new HashMap<String, Object>();
	    					 
	    					 FormulaEvaluator formulaEval = wb.getCreationHelper().createFormulaEvaluator();
	    					 
	    			        	row = sheet.getRow(r); // row 가져오기
	    			        	if (row != null) {
	    			        		for (int c = 0; c < cells; c++) {
	    			        			cell = row.getCell(c);
	    			        			if (cell != null) { 
	    			        				String value = "";

	    									switch (cell.getCellType()) {
	    									   	case XSSFCell.CELL_TYPE_FORMULA:
	    									   		
	    									   		CellValue evaluate = formulaEval.evaluate(cell);
	    									   	  if( evaluate != null ) {
	    									   		  
	    									   		  try {
	    									   			  
	    									   			  if(WebUtils.isNumber(evaluate.formatAsString())) {
	    									   				  
	    									   				value=  String.valueOf(new Double(evaluate.getNumberValue()).intValue());
	    									   				   
	    									   			  }else {
	    									   				  
	    									   				  value = evaluate.formatAsString();
	    									   			  }
	    									   			  
	    									   		  }catch(Exception e) {
	    									   			  e.printStackTrace();
	    									   			  
	    									   		  }
	    									   		  
	    									   		  
	 	    			        				   }else {
	 	    			        					   
	 	    			        					   value = "";
	 	    			        				   }
	    									   		
	    									   		/*
	    									   		if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_NUMERIC){
	    									   			value = "" + (int)cell.getNumericCellValue();
	    									   		}else if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_STRING){
	    									   			value = "" + cell.getStringCellValue(); 
	    									   		}
	    									   		value = cell.getCellFormula();
	    									   		*/
	    									   		
	    									   		break;
	    									   	case XSSFCell.CELL_TYPE_NUMERIC:
	    								            if (DateUtil.isCellDateFormatted(cell)){  
	    								                //날짜  
	    								            	Date date = cell.getDateCellValue();
	    								                value = String.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(date));
	    								            }else{  
	    								                //수치
	    								            	value = NumberToTextConverter.toText(cell.getNumericCellValue());
	    								            	
	    								                //value = String.valueOf(cell.getNumericCellValue());  
	    								            } 
	    									   		//value = "" + (int)cell.getNumericCellValue();
	    									   		break;
	    									   	case XSSFCell.CELL_TYPE_STRING:
	    									   		value = "" + cell.getStringCellValue();
	    									   		break;
	    									   	case XSSFCell.CELL_TYPE_BLANK:
	    									   		value = "";
	    									   		break;
	    									   	case XSSFCell.CELL_TYPE_ERROR:
	    									   		value = "" + cell.getErrorCellValue();
	    									   		break;
	    									   	default:
	    									}
	    									if(value != null && !value.equals("")) {
	    										value = value.trim();
	    									}
	    									int nIndex = cell.getColumnIndex();
	    									
	    									map.put("hcYn", "Y");	    									
	    									if(nIndex == 0){
	    										//allocaiton_id
	    										map.put("allocationId", value);
	    									}else if(nIndex == 1){
	    										//출발일
	    										map.put("departureDt", value);
	    									}else if(nIndex == 2){
	    										//기사 아이디
	    										map.put("paymentPartnerId", value);
	    										map.put("driverId", value);
	    									}else if(nIndex == 3){
	    										//기사 지급액
	    										map.put("amount", value);
	    									}else if(nIndex == 4){
	    										map.put("decideMonth", value);
	    									}
	    									
	    			        			} else {
	    			        				System.out.print("[null]\t");
	    			        			}
	    			        		} // for(c) 문
	    			        			
	    			        	
	    			        	}
	    			        	 
	    			        	//Map<String, Object> allocationInfo = allocationService.selectAllocation(map);
	    			        	
	    			        	//해당 배차 건의 정보가 있는지 확인 한다.
	    			        	Map<String, Object> paymentInfo = paymentInfoService.selectPaymentInfoByPaymentParnerId(map);
	    			        	
	    			        	Map<String, Object> driver = driverService.selectDriver(map);
	    			        	int deductionRate = 0;
	    			        	
	    			        	map.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
	    			        	
	    			        	if(driver != null && paymentInfo != null) {
	    			        		
	    			        		if(driver.get("deduction_rate") != null && !driver.get("deduction_rate").toString().equals("")) {
	    			        			
	    			        			//캐리어 기사님은 입력 되어 있는 공제율을 적용 하면 되고
	    			        			deductionRate = Integer.parseInt(driver.get("deduction_rate").toString());
	    			        		
	    			        			//셀프 기사님은 현대캐피탈 건인경우 공제율이 변경 된다....    			        			
	    			        			if(driver.get("car_assign_company") != null && driver.get("car_assign_company").toString().equals("S")) {
	    			        				if(deductionRate > 10) {
		    			        				if(deductionRate == 15) {
		    			        					//지입기사이면 14% 아니면 15%
		    			        					if(driver.get("driver_kind") != null) {
		    			        						if(driver.get("driver_kind").equals("00") || driver.get("driver_kind").equals("03")) {
			    			        						deductionRate = 0;
			    			        					}else if(driver.get("driver_kind").equals("01")) {
			    			        						deductionRate = 4;
			    			        					}else if(driver.get("driver_kind").equals("02")) {
			    			        						deductionRate = 5;
			    			        					}else {
			    			        						deductionRate = 0;
			    			        					}
		    			        					}else {
		    			        						deductionRate = 0;
		    			        					}
		    			        					
		    			        				}else {
		    			        					deductionRate = deductionRate-10;
		    			        				}
		    			        			}else if(deductionRate == 10) {
		    			        				deductionRate = 0;
		    			        			}    			        				
	    			        			}
	    			        			
	    			        		}
	    			        		
	    			        		int amount = Integer.parseInt(map.get("amount").toString()); 
	    			        		int billForPayment = amount - (amount*deductionRate/100);
	    			        		map.put("deductionRate", deductionRate);
	    			        		map.put("billForPayment", billForPayment);
	    			        		map.put("decideStatus", "Y");
	    			        		map.put("decideFinal", "N");
	    			        		map.put("decideFinalId", "");
	    			        		
		    			        	paymentInfoService.updatePaymentInfoByPaymentParnerIdForDriverAmount(map);
		    			        	paymentInfoService.updateAllocationHcYn(map);
	    			        		
	    			        	//기사가 거래처에 속한 기사이면 마감월은 수정 하지 않고 금액만 수정 한다. 
	    			        	}else if(driver == null && paymentInfo != null) {
	    			        		
									//String partnerId = driver.get("customer_id") != null && !driver.get("customer_id").toString().equals("") ? driver.get("customer_id").toString() : "";
	    			        		
	    			        		String partnerId = paymentInfo.get("payment_partner_id").toString();
	    			        		
									
									if(!partnerId.equals("")) {
										
										map.put("paymentPartnerId", partnerId);
										paymentInfo = paymentInfoService.selectPaymentInfoByPaymentParnerId(map);
										
										if(paymentInfo != null) {
											
											map.put("billForPayment", map.get("amount").toString());
											map.put("decideStatus", "N");
											map.put("decideFinal", "N");
											map.put("decideFinalId", "");
									    	paymentInfoService.updatePaymentInfoByPaymentParnerIdForDriverAmount(map);
											    			        	
									    //
										}else if(paymentInfo == null) {
											
											//매입처 결제 정보를 가져온다.
											map.put("paymentDivision", "03");
											Map<String, Object> paymentInfoBybpaymentDivision = paymentInfoService.selectPaymentInfoByPaymentDivision(map);
											
											map.put("paymentPartnerId", paymentInfoBybpaymentDivision.get("payment_partner_id").toString());
											paymentInfo = paymentInfoService.selectPaymentInfoByPaymentParnerId(map);
											
											if(paymentInfo != null) {
									    		map.put("billForPayment", map.get("amount").toString());
									    		map.put("decideStatus", "N");
									    		map.put("decideFinal", "N");
									    		map.put("decideFinalId", "");
									        	paymentInfoService.updatePaymentInfoByPaymentParnerIdForDriverAmount(map);
											}
											    			        				
										}
										
										
											    			        			
									}
									
									}
	    			        	map.put("logType","U");
								paymentInfoService.insertPaymentInfoLogByMap(map); //log 기록
	    			        } // for(r) 문
	    				
	    			}
	    		}
	    		
	    		
	    		
	    		
	    		
	    	}else{
	    		resultApi.setResultCode("1111"); 	//file 없음
	    	}
			
			WebUtils.messageAndRedirectUrl(mav,"건의 배차 정보가 등록되었습니다.", "/allocation/allocation-register.do");
		}catch(Exception e){
			WebUtils.messageAndRedirectUrl(mav, inputDataRow+"번째 행을 확인 해 주세요.("+cause+")", "/allocation/allocation-register.do");
			e.printStackTrace();
		}
		
		return mav;
	}
	
	
	
	
	
	
	@RequestMapping(value = "/excelUploadForALLDriverAmount", method = RequestMethod.POST)
	public ModelAndView excelUploadForALLDriverAmount(ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception{
		
		ResultApi resultApi = new ResultApi();
		int inputDataRow = 0;
		String cause = "";					//업로드 실패시 원인을 확인 할 수 있도록 한다.
		try{
			
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
	    	List<MultipartFile> fileList = multipartRequest.getFiles("bbsFile");
			
	    	HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
	    	
	    	if(fileList != null && fileList.size() > 0){
	    		for(MultipartFile mFile : fileList){	
	    			if(mFile != null && mFile.getSize() > 0){
	    				
	    				Long fileSize = mFile.getSize();
	    				String fileName = mFile.getOriginalFilename();		
	    				String ext = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length());
	    				File file = new File(mFile.getOriginalFilename());
	    				mFile.transferTo(file);
	    				
	    				
	    				XSSFWorkbook wb = null;
	    				XSSFRow row;
	    				XSSFCell cell;
	    				try {//엑셀 파일 오픈
	    					wb = new XSSFWorkbook(new FileInputStream(file));
	    				} catch (FileNotFoundException e) {
	    					e.printStackTrace();
	    				} catch (IOException e) {
	    					e.printStackTrace();
	    				}
	    				
	    				XSSFSheet sheet = wb.getSheetAt(0);
	    		         //int rows = sheet.getPhysicalNumberOfRows();
	    				//int cells = sheet.getRow(0).getPhysicalNumberOfCells();
	    				int rows = sheet.getLastRowNum();
	    				int cells = sheet.getRow(0).getLastCellNum();
	    				
	    				FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
	    				
	    				 for (int r = 1; r <= rows; r++) {
	    					 inputDataRow = r+1;
	    					 
	    					 Map<String, Object> map = new HashMap<String, Object>();
	    					 
	    					 FormulaEvaluator formulaEval = wb.getCreationHelper().createFormulaEvaluator();
	    					 
	    			        	row = sheet.getRow(r); // row 가져오기
	    			        	if (row != null) {
	    			        		for (int c = 0; c < cells; c++) {
	    			        			cell = row.getCell(c);
	    			        			if (cell != null) { 
	    			        				String value = "";

	    									switch (cell.getCellType()) {
	    									   	case XSSFCell.CELL_TYPE_FORMULA:
	    									   		
	    									   		CellValue evaluate = formulaEval.evaluate(cell);
	    									   	  if( evaluate != null ) {
	    									   		  
	    									   		  try {
	    									   			  
	    									   			  if(WebUtils.isNumber(evaluate.formatAsString())) {
	    									   				  
	    									   				value=  String.valueOf(new Double(evaluate.getNumberValue()).intValue());
	    									   				   
	    									   			  }else {
	    									   				  
	    									   				  value = evaluate.formatAsString();
	    									   			  }
	    									   			  
	    									   		  }catch(Exception e) {
	    									   			  e.printStackTrace();
	    									   			  
	    									   		  }
	    									   		  
	    									   		  
	 	    			        				   }else {
	 	    			        					   
	 	    			        					   value = "";
	 	    			        				   }
	    									   		
	    									   		/*
	    									   		if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_NUMERIC){
	    									   			value = "" + (int)cell.getNumericCellValue();
	    									   		}else if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_STRING){
	    									   			value = "" + cell.getStringCellValue(); 
	    									   		}
	    									   		value = cell.getCellFormula();
	    									   		*/
	    									   		
	    									   		break;
	    									   	case XSSFCell.CELL_TYPE_NUMERIC:
	    								            if (DateUtil.isCellDateFormatted(cell)){  
	    								                //날짜  
	    								            	Date date = cell.getDateCellValue();
	    								                value = String.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(date));
	    								            }else{  
	    								                //수치
	    								            	value = NumberToTextConverter.toText(cell.getNumericCellValue());
	    								            	
	    								                //value = String.valueOf(cell.getNumericCellValue());  
	    								            } 
	    									   		//value = "" + (int)cell.getNumericCellValue();
	    									   		break;
	    									   	case XSSFCell.CELL_TYPE_STRING:
	    									   		value = "" + cell.getStringCellValue();
	    									   		break;
	    									   	case XSSFCell.CELL_TYPE_BLANK:
	    									   		value = "";
	    									   		break;
	    									   	case XSSFCell.CELL_TYPE_ERROR:
	    									   		value = "" + cell.getErrorCellValue();
	    									   		break;
	    									   	default:
	    									}
	    									if(value != null && !value.equals("")) {
	    										value = value.trim();
	    									}
	    									int nIndex = cell.getColumnIndex();
	    										    									
	    									if(nIndex == 0){
	    										//allocaiton_id
	    										map.put("allocationId", value);
	    									}else if(nIndex == 1){
	    										//출발일
	    										map.put("departureDt", value);
	    									}else if(nIndex == 2){
	    										//기사 아이디
	    										map.put("paymentPartnerId", value);
	    										map.put("driverId", value);
	    									}else if(nIndex == 3){
	    										//기사 지급액
	    										map.put("amount", value);
	    									}
	    									
	    									else if(nIndex == 4){
	    										//상차지
	    										map.put("departure", value);
	    									}
	    									
	    									else if(nIndex == 5){
	    										//하차지
	    										map.put("arrival", value);
	    									}
	    									
	    										    									
	    			        			} else {
	    			        				System.out.print("[null]\t");
	    			        			}
	    			        		} // for(c) 문
	    			        			
	    			        	
	    			        	}
	    			        	
	    			        	map.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
	    			        	
	    			        	Map<String, Object> allocationInfo = allocationService.selectAllocation(map);
	    			        	
	    			        	//해당 배차 건의 정보가 있는지 확인 한다.
	    			        	Map<String, Object> paymentInfo = paymentInfoService.selectPaymentInfoByPaymentParnerId(map);
	    			        	
	    			        	Map<String, Object> driver = driverService.selectDriver(map);
	    			        	int deductionRate = 0;
	    			        	
	    			        	if(map.get("arrival")!= null && !map.get("arrival").toString().equals("")) {
	    			        		map.put("logType", "U");
    			        			carInfoService.updateCarInfoDepartureAndArrival(map);
    			        			carInfoService.insertCarInfoLogByMap(map); //로그기록
    			        			
    			        		}
	    			        	
	    			        	
	    			        	//if(driver != null && paymentInfo != null) {
	    			        	
	    			        	if(driver != null && paymentInfo != null) {
	    			        		
	    			        		if(driver.get("deduction_rate") != null && !driver.get("deduction_rate").toString().equals("")) {
	    			        			
	    			        			deductionRate = Integer.parseInt(driver.get("deduction_rate").toString());
	    			        			
	    			        			//셀프 기사님은 현대캐피탈 건인경우 공제율이 변경 된다....    			        			
	    			        			if(driver.get("car_assign_company") != null && driver.get("car_assign_company").toString().equals("S") && allocationInfo.get("hc_yn").toString().equals("Y")) {
	    			        				
	    			        				if(deductionRate > 10) {
		    			        				if(deductionRate == 15) {
		    			        					//지입기사이면 14% 아니면 15%
		    			        					if(driver.get("driver_kind") != null) {
		    			        						if(driver.get("driver_kind").equals("00") || driver.get("driver_kind").equals("03")) {
			    			        						deductionRate = 0;
			    			        					}else if(driver.get("driver_kind").equals("01")) {
			    			        						deductionRate = 4;
			    			        					}else if(driver.get("driver_kind").equals("02")) {
			    			        						deductionRate = 5;
			    			        					}else {
			    			        						deductionRate = 0;
			    			        					}
		    			        					}else {
		    			        						deductionRate = 0;
		    			        					}
		    			        					
		    			        				}else {
		    			        					deductionRate = deductionRate-10;
		    			        				}
		    			        			}else if(deductionRate == 10) {
		    			        				deductionRate = 0;
		    			        			}
	    			        			}
	    			        			
	    			        			
	    			        		}
	    			        		
	    			        		int amount = Integer.parseInt(map.get("amount").toString()); 
	    			        		int billForPayment = amount - (amount*deductionRate/100);
	    			        		map.put("deductionRate", deductionRate);
	    			        		map.put("billForPayment", billForPayment);
		    			        	paymentInfoService.updatePaymentInfoByPaymentParnerIdForSelfDriverAmount(map);
		    			        	
	    			        	}else if(driver == null && paymentInfo != null) {
	    			        		
									//String partnerId = driver.get("customer_id") != null && !driver.get("customer_id").toString().equals("") ? driver.get("customer_id").toString() : "";
	    			        		
	    			        		String partnerId = paymentInfo.get("payment_partner_id").toString();
	    			        		
									
									if(!partnerId.equals("")) {
										
										map.put("paymentPartnerId", partnerId);
										paymentInfo = paymentInfoService.selectPaymentInfoByPaymentParnerId(map);
										
										if(paymentInfo != null) {
											
											map.put("billForPayment", map.get("amount").toString());
											map.put("decideStatus", "N");
											map.put("decideFinal", "N");
											map.put("decideFinalId", "");
									    	paymentInfoService.updatePaymentInfoByPaymentParnerIdForDriverAmount(map);
									    	
											    			        	
									    //
										}else if(paymentInfo == null) {
											
											//매입처 결제 정보를 가져온다.
											map.put("paymentDivision", "03");
											Map<String, Object> paymentInfoBybpaymentDivision = paymentInfoService.selectPaymentInfoByPaymentDivision(map);
											
											map.put("paymentPartnerId", paymentInfoBybpaymentDivision.get("payment_partner_id").toString());
											paymentInfo = paymentInfoService.selectPaymentInfoByPaymentParnerId(map);
											
											if(paymentInfo != null) {
									    		map.put("billForPayment", map.get("amount").toString());
									    		map.put("decideStatus", "N");
									    		map.put("decideFinal", "N");
									    		map.put("decideFinalId", "");
									        	paymentInfoService.updatePaymentInfoByPaymentParnerIdForDriverAmount(map);
											}
											    			        				
										}
										
										
											    			        			
									}
									
									}
	    			        	map.put("logType", "U");
	    			        	paymentInfoService.insertPaymentInfoLogByMap(map); //log 기록
	    			        } // for(r) 문
	    				
	    			}
	    		}
	    		
	    		
	    		
	    		
	    		
	    	}else{
	    		resultApi.setResultCode("1111"); 	//file 없음
	    	}
			
			WebUtils.messageAndRedirectUrl(mav,"건의 배차 정보가 등록되었습니다.", "/allocation/allocation-register.do");
		}catch(Exception e){
			WebUtils.messageAndRedirectUrl(mav, inputDataRow+"번째 행을 확인 해 주세요.("+cause+")", "/allocation/allocation-register.do");
			e.printStackTrace();
		}
		
		return mav;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	//업체 청구액 수정******
		@RequestMapping(value = "/excelUploadForAmountMod", method = RequestMethod.POST)
		public ModelAndView excelUploadForAmountMod(ModelAndView mav, HttpServletRequest request, 
				HttpServletResponse response) throws Exception{
			
			ResultApi resultApi = new ResultApi();
			int inputDataRow = 0;
			String cause = "";					//업로드 실패시 원인을 확인 할 수 있도록 한다.
			try{
				
				MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
		    	List<MultipartFile> fileList = multipartRequest.getFiles("bbsFile");
				
		    	HttpSession session = request.getSession(); 
				Map userMap = (Map)session.getAttribute("user");
		    	
		    	if(fileList != null && fileList.size() > 0){
		    		for(MultipartFile mFile : fileList){	
		    			if(mFile != null && mFile.getSize() > 0){
		    				
		    				Long fileSize = mFile.getSize();
		    				String fileName = mFile.getOriginalFilename();		
		    				String ext = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length());
		    				File file = new File(mFile.getOriginalFilename());
		    				mFile.transferTo(file);
		    				
		    				
		    				XSSFWorkbook wb = null;
		    				XSSFRow row;
		    				XSSFCell cell;
		    				try {//엑셀 파일 오픈
		    					wb = new XSSFWorkbook(new FileInputStream(file));
		    				} catch (FileNotFoundException e) {
		    					e.printStackTrace();
		    				} catch (IOException e) {
		    					e.printStackTrace();
		    				}
		    				
		    				XSSFSheet sheet = wb.getSheetAt(0);
		    		         //int rows = sheet.getPhysicalNumberOfRows();
		    				//int cells = sheet.getRow(0).getPhysicalNumberOfCells();
		    				int rows = sheet.getLastRowNum();
		    				int cells = sheet.getRow(0).getLastCellNum();
		    				
		    				FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
		    				
		    				 for (int r = 1; r <= rows; r++) {
		    					 inputDataRow = r+1;
		    					 
		    					 Map<String, Object> map = new HashMap<String, Object>();
		    					 
		    					 FormulaEvaluator formulaEval = wb.getCreationHelper().createFormulaEvaluator();
		    					 
		    			        	row = sheet.getRow(r); // row 가져오기
		    			        	if (row != null) {
		    			        		for (int c = 0; c < cells; c++) {
		    			        			cell = row.getCell(c);
		    			        			if (cell != null) { 
		    			        				String value = "";

		    									switch (cell.getCellType()) {
		    									   	case XSSFCell.CELL_TYPE_FORMULA:
		    									   		
		    									   		CellValue evaluate = formulaEval.evaluate(cell);
		    									   	  if( evaluate != null ) {
		    									   		  
		    									   		  try {
		    									   			  
		    									   			  if(WebUtils.isNumber(evaluate.formatAsString())) {
		    									   				value=  String.valueOf(new Double(evaluate.getNumberValue()).intValue());
		    									   				   
		    									   			  }else {
		    									   				  
		    									   				  value = evaluate.formatAsString();
		    									   			  }
		    									   		  }catch(Exception e) {
		    									   			  e.printStackTrace();
		    									   		  }
		    									   		  
		 	    			        				   }else {
		 	    			        					   
		 	    			        					   value = "";
		 	    			        				   }
		    									   		
		    									   		/*
		    									   		if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_NUMERIC){
		    									   			value = "" + (int)cell.getNumericCellValue();
		    									   		}else if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_STRING){
		    									   			value = "" + cell.getStringCellValue(); 
		    									   		}
		    									   		value = cell.getCellFormula();
		    									   		*/
		    									   		
		    									   		break;
		    									   	case XSSFCell.CELL_TYPE_NUMERIC:
		    								            if (DateUtil.isCellDateFormatted(cell)){  
		    								                //날짜  
		    								            	Date date = cell.getDateCellValue();
		    								                value = String.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(date));
		    								            }else{  
		    								                //수치
		    								            	value = NumberToTextConverter.toText(cell.getNumericCellValue());
		    								            	
		    								                //value = String.valueOf(cell.getNumericCellValue());  
		    								            } 
		    									   		//value = "" + (int)cell.getNumericCellValue();
		    									   		break;
		    									   	case XSSFCell.CELL_TYPE_STRING:
		    									   		value = "" + cell.getStringCellValue();
		    									   		break;
		    									   	case XSSFCell.CELL_TYPE_BLANK:
		    									   		value = "";
		    									   		break;
		    									   	case XSSFCell.CELL_TYPE_ERROR:
		    									   		value = "" + cell.getErrorCellValue();
		    									   		break;
		    									   	default:
		    									}
		    									if(value != null && !value.equals("")) {
		    										value = value.trim();
		    									}
		    									int nIndex = cell.getColumnIndex();
		    										    									
		    									if(nIndex == 0){
		    										//allocaiton_id
		    										map.put("allocationId", value);
		    									}else if(nIndex == 8){
		    										
		    										map.put("amount", value);
		    									}else if(nIndex == 9){
		    										
		    										map.put("vat", value);
		    									}else if(nIndex == 10){
		    										
		    										map.put("price", value);
		    									}
		    									
		    			        			} else {
		    			        				System.out.print("[null]\t");
		    			        			}
		    			        		} // for(c) 문
		    			        			
		    			        	
		    			        	}
		    			        	 
		    			        	Map<String, Object> carInfo = carInfoService.selectCarInfo(map);
		    			        	Map<String, Object> allocation = allocationService.selectAllocation(map);
		    			        	
		    			        	map.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
		    			        	
		    			        	//챠량정보 업데이트
		    			        	if(carInfo != null) {
		    			        		map.put("vatIncludeYn", carInfo.get("vat_include_yn").toString());
		    			        		map.put("vatExcludeYn", carInfo.get("vat_exclude_yn").toString());
		    			        		
		    			        		//금액 입력 하고 확정까지 하는경우 주석 풀면 됨.
		    			        		//map.put("decideStatus", "Y");
		    			        		
		    			        		if(carInfo.get("vat_exclude_yn").toString().equals("N")) {
		    			        			System.out.println("\r\n");
		    			        		}
		    			        		
		    			        		carInfoService.updateCarInfoSalesTotal(map);
		    			        		if(map.get("arrival")!= null && !map.get("arrival").toString().equals("")) {		    			        			
		    			        			carInfoService.updateCarInfoDepartureAndArrival(map);
		    			        			map.put("logType", "U");
			    			        		carInfoService.insertCarInfoLogByMap(map); //로그기록
		    			        		}
		    			        		
		    			        		//결제정보 업데이트
		    			        		map.put("paymentDivision", "01"); // 임시주석
		    			        		paymentInfoService.updatePaymentInfoAmount(map);
		    			        		
		    			        		map.put("logType", "U");
		    			        		paymentInfoService.insertPaymentInfoLogByMap(map); //로그기록
		    			        	}
		    			        	
		    			        	
		    			        	
		    			        	
		    			        	//한진 청구액 수정시 광주출고 배차건에 대해서 광주출고(손병용) 매입처가 입력 되지 않은경우 매입처를 추가 하도록 기능 수정. 
		    			        	if(allocation.get("customer_id").toString().equals("CUSf36a8de650dd4e24ae52a250ac6ac110") && carInfo.get("departure") != null && carInfo.get("departure").toString().equals("광주출고")) {
		    			        	
		    			        		Map<String, Object> searchMap = new HashMap<String, Object>();
		    			        		searchMap.put("allocationId", map.get("allocationId").toString());
		    			        		searchMap.put("paymentPartnerId", "CUS92847998eacb43e8ab71b0dc92a3aef7");
		    			        		
		    			        		//손병용 등록 확인
		    			        		Map<String, Object> selectMap = paymentInfoService.selectPaymentInfoByPaymentParnerId(searchMap);
		    			        		
		    			        		
		    			        		//손병용등록 되어 있지 않으면 등록 한다.
		    			        		if(selectMap == null) {
		    			        			
		    			        			searchMap.put("paymentDivision", "03");		
			    			        		List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoListByPaymentDivision(searchMap);
			    			        	
			    			        		//결제정보에 매입처가 이미 있는경우 insert를 하고 
			    			        		if(paymentInfoList.size() == 1 && paymentInfoList.get(0).get("payment_partner_id") != null && !paymentInfoList.get(0).get("payment_partner_id").toString().equals("") && paymentInfoList.get(0).get("payment_partner") != null && !paymentInfoList.get(0).get("payment_partner").toString().equals("")) {
			    			        			
			    			        			PaymentInfoVO paymentInfoVO = new PaymentInfoVO();
				    			        		paymentInfoVO.setAllocationId(map.get("allocationId").toString());
				    			        		paymentInfoVO.setPaymentPartner("손병용(광주 출고대리인)");
												paymentInfoVO.setPaymentPartnerId("CUS92847998eacb43e8ab71b0dc92a3aef7");
												paymentInfoVO.setPaymentKind("CA");
												paymentInfoVO.setPaymentDivision("03");
												paymentInfoVO.setBillingDivision("00");
												paymentInfoVO.setPayment("N");
												paymentInfoVO.setPaymentDt("");
												paymentInfoVO.setBillingDt("");
												paymentInfoVO.setAmount("8000");
												paymentInfoVO.setEtc("");
												paymentInfoVO.setDeductionRate("");
												paymentInfoVO.setBillForPayment("");
												paymentInfoVO.setBillingStatus("N");
												paymentInfoVO.setDecideStatus("N");
												paymentInfoVO.setSelectedStatus("N");
												paymentInfoVO.setDecideFinal("N");
												paymentInfoVO.setVatIncludeYn("N");
												paymentInfoVO.setVatExcludeYn("N");
												paymentInfoVO.setModId(userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자추가 
												paymentInfoService.insertPaymentInfo(paymentInfoVO);
												paymentInfoVO.setLogType("I");
												paymentInfoService.insertPaymentInfoLogByVO(paymentInfoVO); //log 기록
			    			        			
			    			        		}else if(paymentInfoList.size() == 1 && (paymentInfoList.get(0).get("payment_partner_id") == null || paymentInfoList.get(0).get("payment_partner_id").toString().equals("")) && (paymentInfoList.get(0).get("payment_partner") == null || paymentInfoList.get(0).get("payment_partner").toString().equals(""))) {
			    			        			//그렇지 않은경우 update를 한다.
			    			        			
			    			        			Map<String, Object> updateMap = new HashMap<String, Object>();
			    			        			
			    			        			updateMap.put("allocationId", map.get("allocationId").toString());
			    			        			updateMap.put("idx", paymentInfoList.get(0).get("idx").toString());
			    			        			updateMap.put("amount", "8000");
			    			        			updateMap.put("paymentPartner", "손병용(광주 출고대리인)");
			    			        			updateMap.put("paymentPartnerId", "CUS92847998eacb43e8ab71b0dc92a3aef7");
			    			        			updateMap.put("paymentKind", "CA");
			    			        			updateMap.put("billingDivision", "00");
			    			        			updateMap.put("payment", "N");
			    			        			updateMap.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
			    			        			updateMap.put("logType", "U");
			    			        			paymentInfoService.updatePaymentInfo(updateMap);
												paymentInfoService.insertPaymentInfoLogByMap(updateMap); //log 기록
												
			    			        			
			    			        		}else {
			    			        			
			    			        			//리스트 사이즈가 1보다 클때는 일단 등록 되어 있는게 없으니 무조건 insert를 하면 될것 같다..... 
			    			        			PaymentInfoVO paymentInfoVO = new PaymentInfoVO();
				    			        		paymentInfoVO.setAllocationId(map.get("allocationId").toString());
				    			        		paymentInfoVO.setPaymentPartner("손병용(광주 출고대리인)");
												paymentInfoVO.setPaymentPartnerId("CUS92847998eacb43e8ab71b0dc92a3aef7");
												paymentInfoVO.setPaymentKind("CA");
												paymentInfoVO.setPaymentDivision("03");
												paymentInfoVO.setBillingDivision("00");
												paymentInfoVO.setPayment("N");
												paymentInfoVO.setPaymentDt("");
												paymentInfoVO.setBillingDt("");
												paymentInfoVO.setAmount("8000");
												paymentInfoVO.setEtc("");
												paymentInfoVO.setDeductionRate("");
												paymentInfoVO.setBillForPayment("");
												paymentInfoVO.setBillingStatus("N");
												paymentInfoVO.setDecideStatus("N");
												paymentInfoVO.setSelectedStatus("N");
												paymentInfoVO.setDecideFinal("N");
												paymentInfoVO.setVatIncludeYn("N");
												paymentInfoVO.setVatExcludeYn("N");
												
												//일단 주석처리.....
												//paymentInfoService.insertPaymentInfo(paymentInfoVO);
			    			        			
			    			        		}
			    			        		
		    			        			
		    			        		}else {
		    			        			//이미 등록 되어 있는경우 아무것도 하지 않으면 될것 같은데...
		    			        			
		    			        			
		    			        		}
		    			        		
		    			        		
		    			        		
		    			        	}
		    			        	
		    			        } // for(r) 문
		    				
		    			}
		    		}
		    		
		    		
		    	}else{
		    		resultApi.setResultCode("1111"); 	//file 없음
		    	}
				
				WebUtils.messageAndRedirectUrl(mav,"건의 배차 정보가 등록되었습니다.", "/allocation/allocation-register.do");
			}catch(Exception e){
				WebUtils.messageAndRedirectUrl(mav, inputDataRow+"번째 행을 확인 해 주세요.("+cause+")", "/allocation/allocation-register.do");
				e.printStackTrace();
			}
			
			return mav;
		}
		
		
		//픽업설정
		@RequestMapping(value = "/setPickup", method = RequestMethod.POST)
		@ResponseBody
		public ResultApi setPickup(ModelAndView mav,
				Model model,HttpServletRequest request ,HttpServletResponse response
				) throws Exception{
			
			ResultApi result = new ResultApi();
			try{
				
				Map<String, Object> map = new HashMap<String, Object>();
				HttpSession session = request.getSession(); 
				Map userMap = (Map)session.getAttribute("user");
				
				map.put("batchStatus", request.getParameter("batchStatus").toString());
				
				String[] allocationIdArr = request.getParameterValues("allocationIdArr");
				
				
				List<String> allocationIdList = Arrays.asList(allocationIdArr);
				
				map.put("allocationId_list", allocationIdList);
				map.put("batchStatusId", "BAT"+UUID.randomUUID().toString().replaceAll("-",""));
				

				
				//allocationId가 없는 경우는 실행 하지도 않고 실행 해도 익셉션.... 
				if(allocationIdList.size() > 1) {
					allocationService.updateAllocationSetPickup(map);
					map.put("logType","U");
					map.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
					allocationService.insertAllocationLogByMap(map); //로그기록
				}
				
			}catch(Exception e){
				e.printStackTrace();
				result.setResultCode("0001");
			}
			
			return result;
		}
		
		
		//탁송예약확정처리
		@RequestMapping(value = "/confirmAllocation", method = RequestMethod.POST)
		@ResponseBody
		public ResultApi confirmAllocation(ModelAndView mav,
				Model model,HttpServletRequest request ,HttpServletResponse response
				) throws Exception{
			
			ResultApi result = new ResultApi();
			try{
				
				Map<String, Object> map = new HashMap<String, Object>();
				HttpSession session = request.getSession(); 
				Map userMap = (Map)session.getAttribute("user");
				
				map.put("allocationStatus", request.getParameter("allocationStatus").toString());
				String[] allocationIdArr = request.getParameterValues("allocationIdArr");
				
				
				List<String> allocationIdList = Arrays.asList(allocationIdArr);
				
				
				map.put("allocationId_list", allocationIdList);
				List<Map<String,Object>> allocationAppList = allocationService.selectAllocationAppConfirmList(map);
				
				
				
				//allocationId가 없는 경우는 실행 하지도 않고 실행 해도 익셉션.... 
				if(allocationIdList.size() >= 1 ) {
					for(int i=0;   i<allocationIdList.size();  i++) {

						map.put("gubun", "12");
						map.put("customerName", allocationAppList.get(i).get("customer_name").toString());
						map.put("customer_phone", allocationAppList.get(i).get("charge_phone").toString());
						map.put("departureAddr",allocationAppList.get(i).get("departure_addr").toString());
						map.put("arrivalAddr",allocationAppList.get(i).get("arrival_addr").toString());
						map.put("carKind",allocationAppList.get(i).get("car_kind").toString());
						map.put("carIdNum",allocationAppList.get(i).get("car_id_num").toString());
						map.put("carNum",allocationAppList.get(i).get("car_num").toString());
						map.put("customerId", allocationAppList.get(i).get("customer_id").toString());
						map.put("alarmOrSmsYn", "A");
						map.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
						//map.put("")
						
						allocationService.updateAllocationSetConfirm(map);
						map.put("logType", "U");
						allocationService.insertAllocationLogByMap(map); //로그기록
						alarmTalkService.alarmTalkSends(map);
					}
					
					
				}
				
			}catch(Exception e){
				e.printStackTrace();
				result.setResultCode("0001");
			}
			
			return result;
		}
		
		
		
		//어플로 들어온 배차건 고객과 단가 상의 후 통화 완료 처리 controller
		@RequestMapping(value = "/customercallfinish", method = RequestMethod.POST)
		@ResponseBody
		public ResultApi customercall_finish(ModelAndView mav,
				Model model,HttpServletRequest request ,HttpServletResponse response
				) throws Exception{
			
			ResultApi result = new ResultApi();
			try{
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				HttpSession session = request.getSession(); 
				Map userMap = (Map)session.getAttribute("user");
				
				Map<String, Object> allocationMap = allocationService.selectAllocation(paramMap);
				
				Map<String,Object> insertCustomerCallMap = new HashMap<String, Object>();
				
				insertCustomerCallMap.put("customercallId", "CCI"+UUID.randomUUID().toString().replaceAll("-",""));
				insertCustomerCallMap.put("allocationId", paramMap.get("allocationId").toString());
				insertCustomerCallMap.put("customerId", allocationMap.get("customer_id").toString());
				insertCustomerCallMap.put("customerName", allocationMap.get("customer_name").toString());
				insertCustomerCallMap.put("userId", userMap.get("emp_id").toString());
				insertCustomerCallMap.put("userName", userMap.get("emp_name").toString());
				
				allocationService.insertCustomerCall(insertCustomerCallMap);
				result.setResultData(insertCustomerCallMap);
				
			}catch(Exception e){
				e.printStackTrace();
				result.setResultCode("0001");
			}
			

			return result;
		}
		
		
		
		//예치금 입력할 기사 리스트 db에 insert
		@RequestMapping(value = "/excelUploadForDepositDriver", method = RequestMethod.POST)
		public ModelAndView excelUploadForDepositDriver(ModelAndView mav, HttpServletRequest request, 
				HttpServletResponse response) throws Exception{
			
			ResultApi resultApi = new ResultApi();
			int inputDataRow = 0;
			String cause = "";					//업로드 실패시 원인을 확인 할 수 있도록 한다.
			try{
				
				MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
		    	List<MultipartFile> fileList = multipartRequest.getFiles("bbsFile");
				
		    	HttpSession session = request.getSession(); 
				Map userMap = (Map)session.getAttribute("user");
		    	
		    	if(fileList != null && fileList.size() > 0){
		    		for(MultipartFile mFile : fileList){	
		    			if(mFile != null && mFile.getSize() > 0){
		    				
		    				Long fileSize = mFile.getSize();
		    				String fileName = mFile.getOriginalFilename();		
		    				String ext = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length());
		    				File file = new File(mFile.getOriginalFilename());
		    				mFile.transferTo(file);
		    				
		    				
		    				XSSFWorkbook wb = null;
		    				XSSFRow row;
		    				XSSFCell cell;
		    				try {//엑셀 파일 오픈
		    					wb = new XSSFWorkbook(new FileInputStream(file));
		    				} catch (FileNotFoundException e) {
		    					e.printStackTrace();
		    				} catch (IOException e) {
		    					e.printStackTrace();
		    				}
		    				
		    				XSSFSheet sheet = wb.getSheetAt(0);
		    		         //int rows = sheet.getPhysicalNumberOfRows();
		    				//int cells = sheet.getRow(0).getPhysicalNumberOfCells();
		    				int rows = sheet.getLastRowNum();
		    				int cells = sheet.getRow(0).getLastCellNum();
		    				
		    				FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
		    				
		    				 for (int r = 0; r <= rows; r++) {
		    					 inputDataRow = r+1;
		    					 
		    					 DepositDriverVO depositDriverVO = new DepositDriverVO();
		    					 
		    					 FormulaEvaluator formulaEval = wb.getCreationHelper().createFormulaEvaluator();
		    					 
		    			        	row = sheet.getRow(r); // row 가져오기
		    			        	if (row != null) {
		    			        		for (int c = 0; c < cells; c++) {
		    			        			cell = row.getCell(c);
		    			        			if (cell != null) { 
		    			        				String value = "";

		    									switch (cell.getCellType()) {
		    									   	case XSSFCell.CELL_TYPE_FORMULA:
		    									   		
		    									   		CellValue evaluate = formulaEval.evaluate(cell);
		    									   	  if( evaluate != null ) {
		    									   		  
		    									   		  try {
		    									   			  
		    									   			  if(WebUtils.isNumber(evaluate.formatAsString())) {
		    									   				  
		    									   				value=  String.valueOf(new Double(evaluate.getNumberValue()).intValue());
		    									   				   
		    									   			  }else {
		    									   				  
		    									   				  value = evaluate.formatAsString();
		    									   			  }
		    									   			  
		    									   		  }catch(Exception e) {
		    									   			  e.printStackTrace();
		    									   			  
		    									   		  }
		    									   		  
		    									   		  
		 	    			        				   }else {
		 	    			        					   
		 	    			        					   value = "";
		 	    			        				   }
		    									   		
		    									   		/*
		    									   		if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_NUMERIC){
		    									   			value = "" + (int)cell.getNumericCellValue();
		    									   		}else if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_STRING){
		    									   			value = "" + cell.getStringCellValue(); 
		    									   		}
		    									   		value = cell.getCellFormula();
		    									   		*/
		    									   		
		    									   		break;
		    									   	case XSSFCell.CELL_TYPE_NUMERIC:
		    								            if (DateUtil.isCellDateFormatted(cell)){  
		    								                //날짜  
		    								            	Date date = cell.getDateCellValue();
		    								                value = String.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(date));
		    								            }else{  
		    								                //수치
		    								            	value = NumberToTextConverter.toText(cell.getNumericCellValue());
		    								            	
		    								                //value = String.valueOf(cell.getNumericCellValue());  
		    								            } 
		    									   		//value = "" + (int)cell.getNumericCellValue();
		    									   		break;
		    									   	case XSSFCell.CELL_TYPE_STRING:
		    									   		value = "" + cell.getStringCellValue();
		    									   		break;
		    									   	case XSSFCell.CELL_TYPE_BLANK:
		    									   		value = "";
		    									   		break;
		    									   	case XSSFCell.CELL_TYPE_ERROR:
		    									   		value = "" + cell.getErrorCellValue();
		    									   		break;
		    									   	default:
		    									}
		    									if(value != null && !value.equals("")) {
		    										value = value.trim();
		    									}
		    									int nIndex = cell.getColumnIndex();
		    																	
		    									if(nIndex == 0){
		    										depositDriverVO.setCarNum(value);		
		    										depositDriverVO.setDriverId("DRI"+UUID.randomUUID().toString().replaceAll("-",""));
		    									}else if(nIndex == 1){
		    										depositDriverVO.setAssignCompany(value);
		    									}else if(nIndex == 2){
		    										depositDriverVO.setDriverOwner(value);
		    									}else if(nIndex == 3){
		    										depositDriverVO.setDriverName(value);
		    									}else if(nIndex == 4){
		    										depositDriverVO.setCarAssignCompany(value);
		    									}else if(nIndex == 5){
		    										depositDriverVO.setWorkKind(value);
		    									}else if(nIndex == 6){
		    										depositDriverVO.setJoinDt(value);
		    										depositDriverVO.setResignDt("");
		    									}else if(nIndex == 7){
		    										depositDriverVO.setPhoneNum(value);
		    									}else if(nIndex == 8){
		    										depositDriverVO.setCarIdNum("");
		    									}else if(nIndex == 9){
		    										depositDriverVO.setBusinessLicenseNumber(value);
		    									}else if(nIndex == 10){
		    										depositDriverVO.setDriverDeposit(value);
		    									}
		    									
		    			        			} else {
		    			        				System.out.print("[null]\t");
		    			        			}
		    			        		} // for(c) 문
		    			        			
		    			        	
		    			        	}
		    			        	
		    			        	// 여기서 insert 
		    			        	driverDepositService.insertDepositDriver(depositDriverVO);
		    			        } // for(r) 문
		    				
		    			}
		    		}
		    		
		    		
		    	}else{
		    		resultApi.setResultCode("1111"); 	//file 없음
		    	}
				
				WebUtils.messageAndRedirectUrl(mav,"건의 배차 정보가 등록되었습니다.", "/allocation/allocation-register.do");
			}catch(Exception e){
				WebUtils.messageAndRedirectUrl(mav, inputDataRow+"번째 행을 확인 해 주세요.("+cause+")", "/allocation/allocation-register.do");
				e.printStackTrace();
			}
			
			return mav;
		}
		
		
		
		@RequestMapping(value="/sendfile-allocation",method= RequestMethod.POST)
		   @ResponseBody
		   public ResultApi sendFileAllocation(Locale locale, HttpServletRequest request, HttpServletResponse response) {
		      
		      ResultApi result = new ResultApi();

		      try {
		         
		         HttpSession session = request.getSession(); 
		         Map userSessionMap = (Map)session.getAttribute("user");
		         
		         Map <String,Object> map = new HashMap<String,Object>();
		   
		         String senderId = userSessionMap.get("emp_id").toString(); 
		         String sendAllocationFileId= "SFI"+UUID.randomUUID().toString().replaceAll("-","");
		         
		         Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
		         
		         
		         
		         Map<String, Object> selectMap = new HashMap<String, Object>();
		         selectMap.put("allocationId", paramMap.get("allocationId"));
		      
		         
		         Map<String, Object> allocationMap = allocationService.selectAllocation(selectMap); 
		         allocationMap.put("customerId", allocationMap.get("customer_id").toString());
		         Map<String, Object> customer= customerService.selectCustomer(allocationMap);
		         
		         String phoneNum = paramMap.get("phoneNum").toString().replaceAll("-","");
		         phoneNum = phoneNum.replaceAll(" ", "");
		         phoneNum= phoneNum.trim();
		         
		         map.put("senderId", senderId); 
		         map.put("sendAllocationFileId", sendAllocationFileId);
		         map.put("car_kind", allocationMap.get("car_kind").toString());
		         map.put("car_id_num", allocationMap.get("car_id_num").toString());
		         map.put("car_num", allocationMap.get("car_num").toString());
		         map.put("arrival", allocationMap.get("arrival").toString());
		         map.put("resultCode", "0000");
		         map.put("gubun","10");
		         map.put("customer_phone",phoneNum);
		         map.put("file_path",paramMap.get("filePath").toString());
		         map.put("allocationId",paramMap.get("allocationId").toString());
		         map.put("allocationFileId",paramMap.get("fileId").toString());
		         map.put("chargeId",paramMap.get("chargeId").toString());
		         map.put("alarmOrSmsYn", customer.get("alarm_or_sms_yn").toString());
		         
		         String resultStr = alarmTalkService.alarmTalkSends(map);
		         
		         /*result.setResultData();*/
		         
		         
		         if(resultStr.equals("success")) {
		               result.setResultCode("0000");
		               result.setResultMsg("성공");
		               allocationFileService.insertCaSendAllocationFile(map);          
		            }else {
		            
		               if(resultStr.equals("0001")) {
		                  result.setResultCode("0001");
		                   result.setResultMsg("실패");
		                }else if(resultStr.equals("0005")) {
		                      result.setResultCode("0005");
		                      result.setResultMsg("실패");
		                 
		                }else if(resultStr.contains("K101")){
		                      result.setResultCode("K101");
		                      result.setResultMsg("실패");
		                                
		                }else if(resultStr.contains("K102")){
		                      result.setResultCode("K102");
		                      result.setResultMsg("유효 하지 않은 전화번호");
		                }else if(resultStr.contains("K103")){
		                      result.setResultCode("K103");
		                      result.setResultMsg("메세지 길이 제한 오류 입니다.");
		                }else {
		                   result.setResultCode("fail");
		                      result.setResultMsg("네트워크 오류입니다.");
		                }
		               
		            }
		              
		      }catch (Exception e) {
		         e.printStackTrace();
		         result.setResultCode("0006");
		         result.setResultMsg("서버문제로 전송에 실패하였습니다.\r\n 관리자에게 문의 하세요.");
		         
		      }
		      
		      return result;
		      
		   }
		
		
		
		
		
		@RequestMapping(value="/searchForModify",method= RequestMethod.POST)
        @ResponseBody
        public ResultApi searchForModify(Locale locale, HttpServletRequest request, HttpServletResponse response) {
           
           ResultApi result = new ResultApi();

           try {
              
              HttpSession session = request.getSession(); 
              Map userSessionMap = (Map)session.getAttribute("user");
              
              Map <String,Object> map = new HashMap<String,Object>();
              String searchWord = request.getParameter("searchWord").toString().replaceAll(" ","");
              
              
              map.put("searchWord", searchWord);
              List <Map<String,Object>>searchMap = allocationService.selectSearchModify(map);
              
              if(searchWord != null) {
                 result.setResultData(searchMap);;
                
              }

           
              }catch(Exception e) {
              e.printStackTrace();
              result.setResultCode("0001");
              
              
              
              }
              
              return result;
           
              
     

     }
  

		
		//박주임 요청 티케이 마감 관련....
		@RequestMapping(value = "/modForTk", method = RequestMethod.POST)
		public ModelAndView modForTk(ModelAndView mav, HttpServletRequest request, 
				HttpServletResponse response) throws Exception{
			
			ResultApi resultApi = new ResultApi();
			int inputDataRow = 0;
			String cause = "";					//업로드 실패시 원인을 확인 할 수 있도록 한다.
			try{
				
				MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
		    	List<MultipartFile> fileList = multipartRequest.getFiles("bbsFile");
				
		    	HttpSession session = request.getSession(); 
				Map userMap = (Map)session.getAttribute("user");
		    	
		    	if(fileList != null && fileList.size() > 0){
		    		for(MultipartFile mFile : fileList){	
		    			if(mFile != null && mFile.getSize() > 0){
		    				
		    				Long fileSize = mFile.getSize();
		    				String fileName = mFile.getOriginalFilename();		
		    				String ext = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length());
		    				File file = new File(mFile.getOriginalFilename());
		    				mFile.transferTo(file);
		    				
		    				
		    				XSSFWorkbook wb = null;
		    				XSSFRow row;
		    				XSSFCell cell;
		    				try {//엑셀 파일 오픈
		    					wb = new XSSFWorkbook(new FileInputStream(file));
		    				} catch (FileNotFoundException e) {
		    					e.printStackTrace();
		    				} catch (IOException e) {
		    					e.printStackTrace();
		    				}
		    				
		    				XSSFSheet sheet = wb.getSheetAt(0);
		    		         //int rows = sheet.getPhysicalNumberOfRows();
		    				//int cells = sheet.getRow(0).getPhysicalNumberOfCells();
		    				int rows = sheet.getLastRowNum();
		    				int cells = sheet.getRow(0).getLastCellNum();
		    				
		    				FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
		    				
		    				 for (int r = 1; r <= rows; r++) {
		    					 inputDataRow = r+1;
		    					 
		    					 Map<String, Object> map = new HashMap<String, Object>();
		    					 
		    					 FormulaEvaluator formulaEval = wb.getCreationHelper().createFormulaEvaluator();
		    					 
		    			        	row = sheet.getRow(r); // row 가져오기
		    			        	if (row != null) {
		    			        		for (int c = 0; c < cells; c++) {
		    			        			cell = row.getCell(c);
		    			        			if (cell != null) { 
		    			        				String value = "";

		    									switch (cell.getCellType()) {
		    									   	case XSSFCell.CELL_TYPE_FORMULA:
		    									   		
		    									   		CellValue evaluate = formulaEval.evaluate(cell);
		    									   	  if( evaluate != null ) {
		    									   		  
		    									   		  try {
		    									   			  
		    									   			  if(WebUtils.isNumber(evaluate.formatAsString())) {
		    									   				  
		    									   				value=  String.valueOf(new Double(evaluate.getNumberValue()).intValue());
		    									   				   
		    									   			  }else {
		    									   				  
		    									   				  value = evaluate.formatAsString();
		    									   			  }
		    									   			  
		    									   		  }catch(Exception e) {
		    									   			  e.printStackTrace();
		    									   			  
		    									   		  }
		    									   		  
		    									   		  
		 	    			        				   }else {
		 	    			        					   
		 	    			        					   value = "";
		 	    			        				   }
		    									   		
		    									   		/*
		    									   		if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_NUMERIC){
		    									   			value = "" + (int)cell.getNumericCellValue();
		    									   		}else if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_STRING){
		    									   			value = "" + cell.getStringCellValue(); 
		    									   		}
		    									   		value = cell.getCellFormula();
		    									   		*/
		    									   		
		    									   		break;
		    									   	case XSSFCell.CELL_TYPE_NUMERIC:
		    								            if (DateUtil.isCellDateFormatted(cell)){  
		    								                //날짜  
		    								            	Date date = cell.getDateCellValue();
		    								                value = String.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(date));
		    								            }else{  
		    								                //수치
		    								            	value = NumberToTextConverter.toText(cell.getNumericCellValue());
		    								            	
		    								                //value = String.valueOf(cell.getNumericCellValue());  
		    								            } 
		    									   		//value = "" + (int)cell.getNumericCellValue();
		    									   		break;
		    									   	case XSSFCell.CELL_TYPE_STRING:
		    									   		value = "" + cell.getStringCellValue();
		    									   		break;
		    									   	case XSSFCell.CELL_TYPE_BLANK:
		    									   		value = "";
		    									   		break;
		    									   	case XSSFCell.CELL_TYPE_ERROR:
		    									   		value = "" + cell.getErrorCellValue();
		    									   		break;
		    									   	default:
		    									}
		    									if(value != null && !value.equals("")) {
		    										value = value.trim();
		    									}
		    									int nIndex = cell.getColumnIndex();
		    								
		    									
		    									if(nIndex == 0){
		    										//차대번호 6자리
		    										map.put("carIdNum", value);
		    									}else if(nIndex == 1){
		    										//전산에 있어야 하는 탁송건수
		    										map.put("requireCnt", value);
		    									}else if(nIndex == 2){
		    										//부가세 포함 총액
		    										map.put("price", value);
		    										
		    									}
		    									
		    			        			} else {
		    			        				System.out.print("[null]\t");
		    			        			}
		    			        		} // for(c) 문
		    			        			
		    			        	
		    			        	}
		    			        	
		    			        	//티케이 물류 수정건 이므로 고정..
		    			        	map.put("customerId", "CUSa8b74209a826425698ef189f61f08ff6");
		    			        	List<Map<String, Object>> allocationList = carInfoService.selectCarInfoListByCarIdNum(map);
		    			        	
		    			        	
		    			        	//조회 한 건수와 검증 건수가 같은 경우 확정 및 금액 입력을 진행 한다.
		    			        	if(Integer.parseInt(map.get("requireCnt").toString()) == allocationList.size()) {
		    			        	
		    			        		String[] allocationIdArr = new String[allocationList.size()];
		    			        		for(int k = 0; k < allocationList.size(); k++) {
		    			        			Map<String, Object> allocation = allocationList.get(k);
		    			        			allocationIdArr[k] = allocation.get("allocation_id").toString();
		    			        		}

		    			        		List<String> allocationIdList = Arrays.asList(allocationIdArr);
		    			        		map.put("allocationIdList", allocationIdList);
		    			        		List<Map<String, Object>> carInfoList = carInfoService.selectCarInfoListByAllocationIdList(map);
		    			        		
		    			        		for(int m = 0; m < carInfoList.size(); m++) {
		    			        			Map<String, Object> carInfo = carInfoList.get(m);
		    			        			Map<String, Object> modMap = new HashMap<String, Object>();

		    			        			modMap.put("allocationId", carInfo.get("allocation_id").toString());
		    			        			modMap.put("vatIncludeYn", "Y");
			    			        		modMap.put("vatExcludeYn", "N");
				    			        	//확정으로 바꿔준다.
			    			        		modMap.put("decideStatus", "Y");
			    			        		modMap.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
		    			        			//같은 차대번호를 가진 리스트 중 제일 마지막 리스트를 제외 하고 0원으로 초기화 하고 마지막 리스트만 금액을 넣어준다.
		    			        			if(m != carInfoList.size()-1) {
		    			        				
				    			        		modMap.put("amount", "0");
				    			        		modMap.put("vat", "0");
				    			        		modMap.put("price", "0");
				    			        		
		    			        			}else {
		    			        				
		    			        				//금액을 넣을 배차건.	
		    			        				long amount,vat,price;
		    			        				price = Integer.parseInt(map.get("price").toString());
		    			        				amount = Math.round(price/1.1);
		    			        				vat = price-amount;
				    			        		modMap.put("amount", amount);
				    			        		modMap.put("vat", vat);
				    			        		modMap.put("price", price);
				    			        		
		    			        			}
		    			        			modMap.put("logType","U");
		    			        			
		    			        			//차량정보(금액)업데이트
		    			        			carInfoService.updateCarInfoSalesTotal(modMap);
		    			        			carInfoService.insertCarInfoLogByMap(modMap);//log 기록
		    			        			
			    			        		//결제정보 업데이트
		    			        			modMap.put("paymentDivision", "01");
			    			        		paymentInfoService.updatePaymentInfoAmount(modMap);
			    			        		paymentInfoService.insertPaymentInfoLogByMap(modMap); //log 기록
			    			        		
		    			        		}
		    			        		
		    			        		
		    			        		cell = row.getCell(3);
				        				cell.setCellValue("O");
		    			        		
		    			        		
		    			        	}else {
		    			        		//건수가 다른경우
		    			        		cell = row.getCell(3);
				        				cell.setCellValue("X");
		    			        		
		    			        	}
		    			        } // for(r) 문

		    				 XSSFFormulaEvaluator.evaluateAllFormulaCells(wb);
		    				 fileUploadService.upload(rootDir, "tk_list","for_tk_mod.xlsx", wb);
		    				 
		    			}
		    		}
		    		
		    		
		    	}else{
		    		resultApi.setResultCode("1111"); 	//file 없음
		    	}
				
		    	
		    	
		    	
		    	
		    	
		    	
		    	
				WebUtils.messageAndRedirectUrl(mav,"건의 배차 정보가 등록되었습니다.", "/allocation/allocation-register.do");
			}catch(Exception e){
				WebUtils.messageAndRedirectUrl(mav, inputDataRow+"번째 행을 확인 해 주세요.("+cause+")", "/allocation/allocation-register.do");
				e.printStackTrace();
			}
			
			return mav;
		}
		
		
		
		
		
		
		@RequestMapping(value = "/excelUploadForUpdateCarIdNum", method = RequestMethod.POST)
		public ModelAndView excelUploadForUpdateCarIdNum(ModelAndView mav, HttpServletRequest request, 
				HttpServletResponse response) throws Exception{
			
			ResultApi resultApi = new ResultApi();
			int inputDataRow = 0;
			String cause = "";					//업로드 실패시 원인을 확인 할 수 있도록 한다.
			try{
				
				MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
		    	List<MultipartFile> fileList = multipartRequest.getFiles("bbsFile");
				
		    	HttpSession session = request.getSession(); 
				Map userMap = (Map)session.getAttribute("user");
		    	
		    	if(fileList != null && fileList.size() > 0){
		    		for(MultipartFile mFile : fileList){	
		    			if(mFile != null && mFile.getSize() > 0){
		    				
		    				Long fileSize = mFile.getSize();
		    				String fileName = mFile.getOriginalFilename();		
		    				String ext = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length());
		    				File file = new File(mFile.getOriginalFilename());
		    				mFile.transferTo(file);
		    				
		    				
		    				XSSFWorkbook wb = null;
		    				XSSFRow row;
		    				XSSFCell cell;
		    				try {//엑셀 파일 오픈
		    					wb = new XSSFWorkbook(new FileInputStream(file));
		    				} catch (FileNotFoundException e) {
		    					e.printStackTrace();
		    				} catch (IOException e) {
		    					e.printStackTrace();
		    				}
		    				
		    				XSSFSheet sheet = wb.getSheetAt(0);
		    		         //int rows = sheet.getPhysicalNumberOfRows();
		    				//int cells = sheet.getRow(0).getPhysicalNumberOfCells();
		    				int rows = sheet.getLastRowNum();
		    				int cells = sheet.getRow(0).getLastCellNum();
		    				
		    				FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
		    				
		    				 for (int r = 0; r <= rows; r++) {
		    					 inputDataRow = r+1;
		    					 
		    					 Map<String, Object> map = new HashMap<String, Object>();
		    					 
		    					 FormulaEvaluator formulaEval = wb.getCreationHelper().createFormulaEvaluator();
		    					 
		    			        	row = sheet.getRow(r); // row 가져오기
		    			        	if (row != null) {
		    			        		for (int c = 0; c < cells; c++) {
		    			        			cell = row.getCell(c);
		    			        			if (cell != null) { 
		    			        				String value = "";

		    									switch (cell.getCellType()) {
		    									   	case XSSFCell.CELL_TYPE_FORMULA:
		    									   		
		    									   		CellValue evaluate = formulaEval.evaluate(cell);
		    									   	  if( evaluate != null ) {
		    									   		  
		    									   		  try {
		    									   			  
		    									   			  if(WebUtils.isNumber(evaluate.formatAsString())) {
		    									   				  
		    									   				value=  String.valueOf(new Double(evaluate.getNumberValue()).intValue());
		    									   				   
		    									   			  }else {
		    									   				  
		    									   				  value = evaluate.formatAsString();
		    									   			  }
		    									   			  
		    									   		  }catch(Exception e) {
		    									   			  e.printStackTrace();
		    									   			  
		    									   		  }
		    									   		  
		    									   		  
		 	    			        				   }else {
		 	    			        					   
		 	    			        					   value = "";
		 	    			        				   }
		    									   		
		    									   		/*
		    									   		if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_NUMERIC){
		    									   			value = "" + (int)cell.getNumericCellValue();
		    									   		}else if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_STRING){
		    									   			value = "" + cell.getStringCellValue(); 
		    									   		}
		    									   		value = cell.getCellFormula();
		    									   		*/
		    									   		
		    									   		break;
		    									   	case XSSFCell.CELL_TYPE_NUMERIC:
		    								            if (DateUtil.isCellDateFormatted(cell)){  
		    								                //날짜  
		    								            	Date date = cell.getDateCellValue();
		    								                value = String.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(date));
		    								            }else{  
		    								                //수치
		    								            	value = NumberToTextConverter.toText(cell.getNumericCellValue());
		    								            	
		    								                //value = String.valueOf(cell.getNumericCellValue());  
		    								            } 
		    									   		//value = "" + (int)cell.getNumericCellValue();
		    									   		break;
		    									   	case XSSFCell.CELL_TYPE_STRING:
		    									   		value = "" + cell.getStringCellValue();
		    									   		break;
		    									   	case XSSFCell.CELL_TYPE_BLANK:
		    									   		value = "";
		    									   		break;
		    									   	case XSSFCell.CELL_TYPE_ERROR:
		    									   		value = "" + cell.getErrorCellValue();
		    									   		break;
		    									   	default:
		    									}
		    									if(value != null && !value.equals("")) {
		    										value = value.trim();
		    									}
		    									int nIndex = cell.getColumnIndex();
		    										    									
		    									if(nIndex == 0){
		    										//allocaiton_id
		    										map.put("allocationId", value);
		    									}else if(nIndex == 1){
		    										//출발일
		    										map.put("departureDt", value);
		    									}else if(nIndex == 2){
		    										//차대번호
		    										map.put("carIdNum", value);
		    										
		    									}
		    										    									
		    			        			} else {
		    			        				System.out.print("[null]\t");
		    			        			}
		    			        		} // for(c) 문
		    			        			
		    			        	
		    			        	}
		    			        	
		    			        	
		    			        	Map<String, Object> allocationInfo = allocationService.selectAllocation(map);
		    			        	
		    			        	
		    			        	if(allocationInfo != null) {
		    			        		map.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
		    			        		map.put("logType", "U");
	    			        			carInfoService.updateCarInfoCarIdNum(map);
	    			        			carInfoService.insertCarInfoLogByMap(map); //로그기록
	    			        			
	    			        		}
		    			        	
		    			        } // for(r) 문
		    				
		    			}
		    		}
		    		
		    		
		    		
		    		
		    		
		    	}else{
		    		resultApi.setResultCode("1111"); 	//file 없음
		    	}
				
				WebUtils.messageAndRedirectUrl(mav,"건의 배차 정보가 등록되었습니다.", "/allocation/allocation-register.do");
			}catch(Exception e){
				WebUtils.messageAndRedirectUrl(mav, inputDataRow+"번째 행을 확인 해 주세요.("+cause+")", "/allocation/allocation-register.do");
				e.printStackTrace();
			}
			
			return mav;
		}
		
		
	
	
		
		@RequestMapping(value = "/forbackup", method = RequestMethod.POST)
		public ModelAndView forbackupUpdateCarIdNum(ModelAndView mav, HttpServletRequest request, 
				HttpServletResponse response) throws Exception{
			
			ResultApi resultApi = new ResultApi();
			int inputDataRow = 0;
			String cause = "";					//업로드 실패시 원인을 확인 할 수 있도록 한다.
			try{
				
				MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
		    	List<MultipartFile> fileList = multipartRequest.getFiles("bbsFile");
				
		    	HttpSession session = request.getSession(); 
				Map userMap = (Map)session.getAttribute("user");
		    	
		    	if(fileList != null && fileList.size() > 0){
		    		for(MultipartFile mFile : fileList){	
		    			if(mFile != null && mFile.getSize() > 0){
		    				
		    				Long fileSize = mFile.getSize();
		    				String fileName = mFile.getOriginalFilename();		
		    				String ext = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length());
		    				File file = new File(mFile.getOriginalFilename());
		    				mFile.transferTo(file);
		    				
		    				
		    				XSSFWorkbook wb = null;
		    				XSSFRow row;
		    				XSSFCell cell;
		    				try {//엑셀 파일 오픈
		    					wb = new XSSFWorkbook(new FileInputStream(file));
		    				} catch (FileNotFoundException e) {
		    					e.printStackTrace();
		    				} catch (IOException e) {
		    					e.printStackTrace();
		    				}
		    				
		    				XSSFSheet sheet = wb.getSheetAt(0);
		    		         //int rows = sheet.getPhysicalNumberOfRows();
		    				//int cells = sheet.getRow(0).getPhysicalNumberOfCells();
		    				int rows = sheet.getLastRowNum();
		    				int cells = sheet.getRow(0).getLastCellNum();
		    				
		    				FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
		    				
		    				 for (int r = 1; r <= rows; r++) {
		    					 inputDataRow = r+1;
		    					 
		    					 Map<String, Object> map = new HashMap<String, Object>();
		    					 
		    					 FormulaEvaluator formulaEval = wb.getCreationHelper().createFormulaEvaluator();
		    					 
		    			        	row = sheet.getRow(r); // row 가져오기
		    			        	if (row != null) {
		    			        		for (int c = 0; c < cells; c++) {
		    			        			cell = row.getCell(c);
		    			        			if (cell != null) { 
		    			        				String value = "";

		    									switch (cell.getCellType()) {
		    									   	case XSSFCell.CELL_TYPE_FORMULA:
		    									   		
		    									   		CellValue evaluate = formulaEval.evaluate(cell);
		    									   	  if( evaluate != null ) {
		    									   		  
		    									   		  try {
		    									   			  
		    									   			  if(WebUtils.isNumber(evaluate.formatAsString())) {
		    									   				  
		    									   				value=  String.valueOf(new Double(evaluate.getNumberValue()).intValue());
		    									   				   
		    									   			  }else {
		    									   				  
		    									   				  value = evaluate.formatAsString();
		    									   			  }
		    									   			  
		    									   		  }catch(Exception e) {
		    									   			  e.printStackTrace();
		    									   			  
		    									   		  }
		    									   		  
		    									   		  
		 	    			        				   }else {
		 	    			        					   
		 	    			        					   value = "";
		 	    			        				   }
		    									   		
		    									   		/*
		    									   		if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_NUMERIC){
		    									   			value = "" + (int)cell.getNumericCellValue();
		    									   		}else if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_STRING){
		    									   			value = "" + cell.getStringCellValue(); 
		    									   		}
		    									   		value = cell.getCellFormula();
		    									   		*/
		    									   		
		    									   		break;
		    									   	case XSSFCell.CELL_TYPE_NUMERIC:
		    								            if (DateUtil.isCellDateFormatted(cell)){  
		    								                //날짜  
		    								            	Date date = cell.getDateCellValue();
		    								                value = String.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(date));
		    								            }else{  
		    								                //수치
		    								            	value = NumberToTextConverter.toText(cell.getNumericCellValue());
		    								            	
		    								                //value = String.valueOf(cell.getNumericCellValue());  
		    								            } 
		    									   		//value = "" + (int)cell.getNumericCellValue();
		    									   		break;
		    									   	case XSSFCell.CELL_TYPE_STRING:
		    									   		value = "" + cell.getStringCellValue();
		    									   		break;
		    									   	case XSSFCell.CELL_TYPE_BLANK:
		    									   		value = "";
		    									   		break;
		    									   	case XSSFCell.CELL_TYPE_ERROR:
		    									   		value = "" + cell.getErrorCellValue();
		    									   		break;
		    									   	default:
		    									}
		    									if(value != null && !value.equals("")) {
		    										value = value.trim();
		    									}
		    									int nIndex = cell.getColumnIndex();
		    										    									
		    									if(nIndex == 0){
		    										
		    										map.put("idx", value);
		    									}else if(nIndex == 1){
		    										
		    										map.put("allocationId", value);
		    									}else if(nIndex == 2){
		    										
		    										map.put("paymentPartner", value);
		    										
		    									}else if(nIndex == 3){
		    										
		    										map.put("paymentPartnerId", value);
		    										
		    									}else if(nIndex == 7){
		    										
		    										map.put("payment", value);
		    										
		    									}else if(nIndex == 8){
		    										
		    										map.put("paymentDt", value);
		    										
		    									}
		    										    									
		    			        			} else {
		    			        				System.out.print("[null]\t");
		    			        			}
		    			        		} // for(c) 문
		    			        	
		    			        	}
		    			        	
		    			        	Map<String, Object> allocationInfo = allocationService.selectAllocation(map);
		    			        	
		    			        	
		    			        	if(allocationInfo != null) {
		    			        		map.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
		    			        		//paymentInfo
		    			        		map.put("logType", "U");
		    			        		paymentInfoService.updatePaymentInfoForDDFinishBackup(map);
		    			        		paymentInfoService.insertPaymentInfoLogByMap(map); //로그기록
	    			        			
	    			        		}
		    			        	
		    			        } // for(r) 문
		    				
		    			}
		    		}
		    		
		    		
		    		
		    		
		    		
		    	}else{
		    		resultApi.setResultCode("1111"); 	//file 없음
		    	}
				
				WebUtils.messageAndRedirectUrl(mav,"건의 배차 정보가 등록되었습니다.", "/allocation/allocation-register.do");
			}catch(Exception e){
				WebUtils.messageAndRedirectUrl(mav, inputDataRow+"번째 행을 확인 해 주세요.("+cause+")", "/allocation/allocation-register.do");
				e.printStackTrace();
			}
			
			return mav;
		}
		
//	셀프배차판 
		@RequestMapping(value = "/selfTable", method = {RequestMethod.POST,RequestMethod.GET})
		public ModelAndView selfTable(Locale locale,ModelAndView mav, HttpServletRequest request, 
				HttpServletResponse response) throws Exception {
			return mav;
		}		
		

		
		// 통합배차현황 ajax로 새로고침을 위한 redirect url 필요하여 생성 combination2.jsp 
		@RequestMapping(value = "/combination2", method = {RequestMethod.POST,RequestMethod.GET})
		public String archive2(Locale locale, Model model, HttpServletRequest request, 
				HttpServletResponse response) throws Exception {
			
			try{
				
				HttpSession session = request.getSession(); 
				Map userMap = (Map)session.getAttribute("user");
				// String companyId = session.getAttribute("companyId").toString();
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				String dept = userMap != null && userMap.get("dept") != null && !userMap.get("dept").toString().equals("") ? userMap.get("dept").toString():"";
				if(dept.equals("sunbo")){		//선보 부서의 아이디 인 경우 선보의 목록만 보여준다.
					paramMap.put("dept", dept);
				}
				
				if(paramMap.get("searchType") == null || paramMap.get("searchType").equals("")) {
					paramMap.put("searchType", "total");
				}
				
				if(paramMap.get("searchType") != null && !paramMap.get("searchType").equals("")){
					if(paramMap.get("searchType").equals("carrier")){
						if(paramMap.get("searchWord").equals("셀프")){
							paramMap.put("searchWord", "S");
						}else{
							paramMap.put("searchWord", "C");
						}
					}else if(paramMap.get("searchType").equals("distance")){
						if(paramMap.get("searchWord").equals("시내")){
							paramMap.put("searchWord", "C");
						}else if(paramMap.get("searchWord").equals("상행")){
							paramMap.put("searchWord", "U");
						}else if(paramMap.get("searchWord").equals("하행")){
							paramMap.put("searchWord", "D");
						}else if(paramMap.get("searchWord").equals("픽업")){
							paramMap.put("searchWord", "P");
						}
					}
				}
				
				
				String forOrder = request.getParameter("forOrder")==null || request.getParameter("forOrder").toString().equals("") ?"":request.getParameter("forOrder").toString();
				if(!forOrder.equals("")){
					String[] sort = forOrder.split("\\^"); 
					if(sort.length > 1 && !sort[0].equals("") && !sort[1].equals("")){
						paramMap.put("forOrder", sort[0]);
						paramMap.put("order", sort[1]);
						model.addAttribute("order",  sort[1]);
					}else {
						paramMap.put("forOrder", "");
						paramMap.put("order", "");
						model.addAttribute("order",  "");
					}
				}
				
				
				
				String startDt = request.getParameter("startDt") == null ? "" : request.getParameter("startDt").toString();
				String endDt = request.getParameter("endDt") == null ? "" : request.getParameter("endDt").toString();
				String searchDateType = request.getParameter("searchDateType") == null || request.getParameter("searchDateType").toString().equals("") ? "D" : request.getParameter("searchDateType").toString();
				String allocationStatus =request.getParameter("allocationStatus") == null || request.getParameter("allocationStatus").toString().equals("") ? "" : request.getParameter("allocationStatus").toString();
				String regType =request.getParameter("regType") == null || request.getParameter("regType").toString().equals("") ? "" : request.getParameter("regType").toString();
				String allocationStatistics = request.getParameter("allocationStatistics") == null || request.getParameter("allocationStatistics").toString().equals("") ? "" : request.getParameter("allocationStatistics").toString();
				

				
				/*if(startDt.equals("")) {
					startDt = WebUtils.getNow("yyyy-MM-dd");
				}
				if(endDt.equals("")) {
					endDt = WebUtils.getNow("yyyy-MM-dd");
				}*/
				String location = "combination";
				paramMap.put("location", location);
				paramMap.put("startDt", startDt);
				paramMap.put("endDt", endDt);
				paramMap.put("searchDateType", searchDateType);
				paramMap.put("allocationStatus", allocationStatus);
				paramMap.put("regType", regType);
				paramMap.put("allocationStatistics", allocationStatistics);
				
				if(!allocationStatistics.equals("")) {
					
					paramMap.put("today",WebUtils.getNow("yyyy-MM-dd").toString());
					
				}
				
				
				// paramMap.put("companyId", companyId);
				
				int totalCount =0;
				
				if(!allocationStatistics.equals("")) {
					
					totalCount	=	allocationService.selectAllocationListCountSub(paramMap);
			
				}else {
					
					 totalCount =	allocationService.selectAllocationListCount(paramMap);
				}
				
				PagingUtils.setPageing(request, totalCount, paramMap);
				paramMap.put("carrierType", "");
				
				//List<Map<String, Object>> allocationList=	allocationService.selectAllocationList(paramMap);
				
				List<Map<String, Object>> allocationList = null;
						
						if(!allocationStatistics.equals("")) {
							
							allocationList = allocationService.selectAllocationListSub(paramMap);
						}else {
							allocationList = allocationService.selectAllocationList(paramMap);
						}
					
						
			
				/*
				for(int i = 0; i < allocationList.size(); i++) {
					Map<String, Object> map = allocationList.get(i);
					//batch_status 가 Y 이면 일괄배차 매출액 총액을 가저온다.
					if(map.get("batch_status").toString().equals("Y")) {
						Map<String, Object> salesTotalMap = carInfoService.selectSumSalesTotalByBatchStatusId(map);
						map.put("sales_total", salesTotalMap.get("sales_total").toString());
						map.put("driver_amount", salesTotalMap.get("driver_amount").toString());
						allocationList.set(i, map);
					}
				}
				*/
				
				if(paramMap.get("searchType") != null && !paramMap.get("searchType").equals("")){
					if(paramMap.get("searchType").equals("carrier")){
						if(paramMap.get("searchWord").equals("S")){
							paramMap.put("searchWord", "셀프");
						}else{
							paramMap.put("searchWord", "캐리어");
						}
					}else if(paramMap.get("searchType").equals("distance")){
						if(paramMap.get("searchWord").equals("C")){
							paramMap.put("searchWord", "시내");
						}else if(paramMap.get("searchWord").equals("U")){
							paramMap.put("searchWord", "상행");
						}else if(paramMap.get("searchWord").equals("D")){
							paramMap.put("searchWord", "하행");
						}else if(paramMap.get("searchWord").equals("P")){
							paramMap.put("searchWord", "픽업");
						}
					}
				}
				
				Map<String, Object> map = new HashMap<String, Object>();
				List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
				//List<Map<String, Object>> driverList = driverService.selectDriverList(map);
				//List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
				//mav.addObject("billingDivisionList",  billingDivisionList);
				model.addAttribute("userMap",  userMap);
				//mav.addObject("driverList",  driverList);RALL
				model.addAttribute("companyList",  companyList);
				model.addAttribute("listData",  allocationList);
				
				String forOpen = request.getParameter("forOpen")==null?"":request.getParameter("forOpen").toString();
				model.addAttribute("forOpen", forOpen);
				model.addAttribute("paramMap", paramMap);
				model.addAttribute("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
			
				List<Map<String, Object>> allocationStatusList = allocationStatusService.selectAllocationStatusList(map);
				model.addAttribute("allocationStatusList", allocationStatusList);
				
			}catch(Exception e){
				e.printStackTrace();
			}
			

			return "/allocation/combination2";
		}
			
			// 배차정보 수정중인지 여부
			@RequestMapping(value = "/allocationModCheck", method = RequestMethod.POST)
			@ResponseBody
			public ResultApi allocationModCheck(ModelAndView mav,
					Model model,HttpServletRequest request ,HttpServletResponse response
					) throws Exception{
				ResultApi result = new ResultApi();		
				
				  try {
		              HttpSession session = request.getSession(); 
		              Map userSessionMap = (Map)session.getAttribute("user");
		              String empId = userSessionMap.get("emp_id").toString();
		               
		              Map <String,String> map = new HashMap<String,String>();
		              String allocationId = request.getParameter("allocationId").toString();
		              String url = request.getParameter("url").toString();
		              
		              map.put("allocationId", allocationId);
		              map.put("empId", empId);
		              map.put("url",url);
		              Map<String,String> rMap = allocationService.allocationModCheck(map);
		              
		              if(rMap != null) {
		            	  result.setResultCode("0000");
		            	  result.setResultData(rMap);
		              	}else {
		              		result.setResultCode("0001");
		              	}
		              }catch(Exception e) {
			              e.printStackTrace();
			              result.setResultCode("0001");
		              }
		              return result;
				}	
		
		
		
		

}
	
	
	

