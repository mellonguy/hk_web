package kr.co.carrier.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.co.carrier.service.PersonInChargeService;
import kr.co.carrier.utils.ResultApi;
import kr.co.carrier.vo.CustomerPersonInChargeVO;

@Controller
@RequestMapping(value="/personInCharge")
public class PersonInChargeController {

	
	@Autowired
    private PersonInChargeService personInChargeService;
	
	
	@RequestMapping(value = "/getPersonInChargeList", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi getCustomerList(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		ResultApi result = new ResultApi();
		try{
			
			Map<String, Object> map = new HashMap<String, Object>();
			
			
			map.put("customerId", request.getParameter("customerId").toString());
			map.put("searchWord", request.getParameter("searchWord").toString()); 

			
			List<Map<String, Object>> personInChargeMap = personInChargeService.selectPersonInChargeList(map);
			
			
			
			
				
			

			
			result.setResultData(personInChargeMap);
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}

		return result;
	}
	
	
	
	@RequestMapping(value = "/insertPersonInCharge", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi insertPersonInCharge(ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception{
		
		ResultApi result = new ResultApi(); 
		 
		 try{
			 
			JSONParser jsonParser = new JSONParser();
			String customerId = (String)request.getParameter("customerId");
			String chargeList = request.getParameter("chargeList");
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("customerId", customerId);
			personInChargeService.deletePersonInCharge(map);
			
			JSONObject jsonObject = (JSONObject) jsonParser.parse(chargeList);
			JSONArray jsonArray = (JSONArray) jsonObject.get("chargeList");
			
			for(int i = 0; i < jsonArray.size(); i++){
				CustomerPersonInChargeVO pVO = new CustomerPersonInChargeVO();
				JSONObject obj = (JSONObject)jsonArray.get(i);
				String personInChargeId = "";
				personInChargeId = "PIC"+UUID.randomUUID().toString().replaceAll("-", "");
				pVO.setPersonInChargeId(personInChargeId);
				pVO.setCustomerId(customerId);
				pVO.setDepartment(obj.get("department").toString());
				pVO.setName(obj.get("name").toString());
				pVO.setPhoneNum(obj.get("phoneNum").toString());
				pVO.setAddress(obj.get("address").toString());
				pVO.setEmail(obj.get("email").toString());
				pVO.setEtc(obj.get("etc").toString());
				personInChargeService.insertPersonInCharge(pVO);
			}
				
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		
		return result;
	}
	
	
	
	
	
	
}
