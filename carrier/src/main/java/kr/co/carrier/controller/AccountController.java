 package kr.co.carrier.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.co.carrier.service.AccountService;
import kr.co.carrier.service.AdjustmentService;
import kr.co.carrier.service.AllocationService;
import kr.co.carrier.service.BillPublishRequestService;
import kr.co.carrier.service.CarInfoService;
import kr.co.carrier.service.CustomerService;
import kr.co.carrier.service.DebtorCreditorService;
import kr.co.carrier.service.DecideFinalService;
import kr.co.carrier.service.DriverBillingFileService;
import kr.co.carrier.service.DriverBillingStatusService;
import kr.co.carrier.service.DriverDeductFileService;
import kr.co.carrier.service.DriverDeductInfoService;
import kr.co.carrier.service.DriverDeductStatusService;
import kr.co.carrier.service.DriverService;
import kr.co.carrier.service.PaymentInfoService;
import kr.co.carrier.service.TotalSalesService;
import kr.co.carrier.utils.BaseAppConstants;
import kr.co.carrier.utils.MailUtils;
import kr.co.carrier.utils.PagingUtils;
import kr.co.carrier.utils.ParamUtils;
import kr.co.carrier.utils.ResultApi;
import kr.co.carrier.utils.WebUtils;
import kr.co.carrier.vo.BillPublishRequestVO;
import kr.co.carrier.vo.DebtorCreditorVO;
import kr.co.carrier.vo.DecideFinalVO;
import kr.co.carrier.vo.DriverDeductInfoVO;
import kr.co.carrier.vo.DriverDeductStatusVO;
import kr.co.carrier.vo.PaymentInfoVO;
import kr.co.carrier.vo.WithdrawVO;
import kr.co.carrier.utils.StringUtilsEx;

@Controller
@RequestMapping(value="/account")
public class AccountController {

	
	@Value("#{appProp['upload.base.path']}")
    private String rootDir;    
    private String subDir = "bbs";	
	
	
    @Autowired
    private DriverDeductInfoService driverDeductInfoService;
    
    @Autowired
    private DriverService driverService;
    
    @Autowired
    private TotalSalesService totalSalesService;
    
    @Autowired
    private AllocationService allocationService;
    
    @Autowired
    private AdjustmentService adjustmentService;
    
    @Autowired
    private CarInfoService carInfoService;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private BillPublishRequestService billPublishRequestService;
    
    @Autowired
    private PaymentInfoService paymentInfoService;
    
    @Autowired
    private DecideFinalService decideFinalService;
    
    @Autowired
    private DriverDeductFileService driverDeductFileService;
    
    @Autowired
    private DriverDeductStatusService driverDeductStatusService;
    
    @Autowired
    private DriverBillingFileService driverBillingFileService;
    
    
    @Autowired
    private DebtorCreditorService debtorCreditorService;
    
    @Autowired
    private AccountService accountService;
    
    @Autowired
    private DriverBillingStatusService driverBillingStatusService;
    
    
    @RequestMapping(value = "/driverList", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView employee(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			String companyId = session.getAttribute("companyId").toString();
			
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			String searchType = request.getParameter("searchType") == null  ? "" : request.getParameter("searchType").toString();
			String searchWord = request.getParameter("searchWord") == null  ? "" : request.getParameter("searchWord").toString();
			String companyType = request.getParameter("companyType") == null ? "" : request.getParameter("companyType").toString();
 			
			
			//String selectMonth = request.getParameter("selectMonth") == null || request.getParameter("selectMonth").toString().equals("")  ? WebUtils.getPrevMonth() : request.getParameter("selectMonth").toString();
			String selectMonth = request.getParameter("selectMonth") == null || request.getParameter("selectMonth").toString().equals("")  ? WebUtils.getNow("yyyy-MM") : request.getParameter("selectMonth").toString();
						
			//String startDt = "";
			//String endDt = "";
			
			//startDt = selectMonth;
			//startDt = startDt.split("-")[0]+"-"+startDt.split("-")[1]+"-01";
			//endDt = selectMonth;
			//endDt = startDt.split("-")[0]+"-"+startDt.split("-")[1]+"-"+WebUtils.getLastDate("yyyy-mm-dd",Integer.parseInt(startDt.split("-")[0]),Integer.parseInt(startDt.split("-")[1]));
			//paramMap.put("startDt", startDt);
			//paramMap.put("endDt", endDt);
			paramMap.put("selectMonth", selectMonth);
			
			paramMap.put("searchType", "00");
			paramMap.put("searchWord", searchWord);
			/* paramMap.put("companyId", companyId); */
			paramMap.put("companyType", companyType);
			
			
			int totalCount = driverDeductInfoService.selectDriverListCount(paramMap);
			PagingUtils.setPageing(request, totalCount, paramMap);
			
			paramMap.put("numOfRows", totalCount);
			
			String forOrder = request.getParameter("forOrder")==null?"":request.getParameter("forOrder").toString();
			if(!forOrder.equals("")){
				String[] sort = forOrder.split("\\^"); 
				if(sort.length > 1 && !sort[0].equals("") && !sort[1].equals("")){
					paramMap.put("forOrder", sort[0]);
					paramMap.put("order", sort[1]);
					mav.addObject("order",  sort[1]);
				}else {
					paramMap.put("forOrder", "");
					paramMap.put("order", "");
					mav.addObject("order",  "");
				}
			}
			
			List<Map<String, Object>> driverList = driverDeductInfoService.selectDriverList(paramMap);
			Map<String, Object> driverTotalStatistics = driverDeductInfoService.selectDriverListTotalStatistics(paramMap);
			
			mav.addObject("driverTotalStatistics",  driverTotalStatistics);
			mav.addObject("userSessionMap",  userSessionMap);
			mav.addObject("listData",  driverList);
			mav.addObject("paramMap", paramMap);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
						
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
    
    
  //운송비 정산 -> 매입처 정산 ->  매입리스트 -> 매입처별  목록 리스트
	@RequestMapping(value = "/driverListDetail", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView driverListDetail(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			String companyId = session.getAttribute("companyId").toString();
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			/*String paymentPartnerId = request.getParameter("paymentPartnerId") == null || request.getParameter("paymentPartnerId").toString().equals("") ? "A" : request.getParameter("paymentPartnerId").toString();
			String decideMonth = request.getParameter("decideMonth") != null && !request.getParameter("decideMonth").toString().equals("") ? request.getParameter("decideMonth").toString() :"";
			paramMap.put("decideMonth", decideMonth);
			paramMap.put("paymentPartnerId", paymentPartnerId);*/
			
			paramMap.put("decideStatus", "Y");
			//paramMap.put("decideFinal", "Y");
			
			//paramMap.put("companyId", companyId);
						
			int totalCount = adjustmentService.selectAllocationCountByPaymentPartnerIdAndDecideMonth(paramMap);
			PagingUtils.setPageing(request, totalCount, paramMap);
			
			String forOrder = request.getParameter("forOrder")==null?"":request.getParameter("forOrder").toString();
			if(!forOrder.equals("")){
				String[] sort = forOrder.split("\\^"); 
				if(sort.length > 1 && !sort[0].equals("") && !sort[1].equals("")){
					paramMap.put("forOrder", sort[0]);
					paramMap.put("order", sort[1]);
					mav.addObject("order",  sort[1]);
				}else {
					paramMap.put("forOrder", "departureDt");
					paramMap.put("order", "asc");
					mav.addObject("order",  "");
				}
			}else {
				paramMap.put("forOrder", "departureDt");
				paramMap.put("order", "asc");
				mav.addObject("order",  "");
			}
			
			List<Map<String, Object>> allocationList = adjustmentService.selectAllocationListByPaymentPartnerIdAndDecideMonth(paramMap);
			Map<String, Object> map = new HashMap<String, Object>();
			
			mav.addObject("userMap",  userMap);
			mav.addObject("listData",  allocationList);
			
			String forOpen = request.getParameter("forOpen")==null?"":request.getParameter("forOpen").toString();
			mav.addObject("forOpen", forOpen);
			mav.addObject("paramMap", paramMap);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
						
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
    
    
    @RequestMapping( value="/driverList_download", method = RequestMethod.GET )
	public ModelAndView driverList_download(ModelAndView mav,
			HttpServletRequest request ,HttpServletResponse response) throws Exception {
		
		ModelAndView view = new ModelAndView();
		
		try{
			
			String varNameList[] = null;
			String titleNameList[] = null;
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			String companyId = session.getAttribute("companyId").toString();
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			String searchType = request.getParameter("searchType") == null  ? "" : request.getParameter("searchType").toString();
			String searchWord = request.getParameter("searchWord") == null  ? "" : request.getParameter("searchWord").toString();
			
			String selectMonth = request.getParameter("selectMonth") == null || request.getParameter("selectMonth").toString().equals("")  ? WebUtils.getPrevMonth() : request.getParameter("selectMonth").toString();
			/*String startDt = "";
			String endDt = "";
			
			startDt = selectMonth;
			startDt = startDt.split("-")[0]+"-"+startDt.split("-")[1]+"-01";
			endDt = selectMonth;
			endDt = startDt.split("-")[0]+"-"+startDt.split("-")[1]+"-"+WebUtils.getLastDate("yyyy-mm-dd",Integer.parseInt(startDt.split("-")[0]),Integer.parseInt(startDt.split("-")[1]));
			paramMap.put("startDt", startDt);
			paramMap.put("endDt", endDt);*/
			paramMap.put("selectMonth", selectMonth);
			
			paramMap.put("searchType", "00");
			paramMap.put("searchWord", searchWord);
			
			//paramMap.put("companyId", companyId);
			
			int totalCount = driverDeductInfoService.selectDriverListCount(paramMap);
			
			paramMap.put("startRownum", 0);
			paramMap.put("numOfRows", totalCount);
			//PagingUtils.setPageing(request, totalCount, paramMap);
			
			List<Map<String, Object>> driverList = driverDeductInfoService.selectDriverList(paramMap);
			
			String today = WebUtils.getNow("yyyy-MM-dd");
			
			titleNameList = new String[]{"기사명","총매입건수","총매입금액","현장수금건수","현장수금액","공제건수","공제금액"};
			varNameList = new String[]{"driver_name","another_cnt","another_sum","dd_cnt","dd_sales","deduct_cnt","deduct_amount"};
			
			view.setViewName("excelDownloadView");
			view.addObject("list", driverList);
			view.addObject("varNameList", varNameList);
			view.addObject("titleNameList", titleNameList);
			view.addObject("excelName", "("+selectMonth+")"+"driverList_"+today+".xlsx");
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return view;
		
	}
    
    
    //기사 월별 공제내역 
    @RequestMapping(value = "/viewMonthDriverDeduct", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView viewMonthDriverDeduct(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			String companyId = session.getAttribute("companyId").toString();
			
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			//paramMap.put("companyId", companyId);
			
			int totalCount = driverDeductInfoService.selectMonthDriverDeductListCount(paramMap);
			PagingUtils.setPageing(request, totalCount, paramMap);
			
			List<Map<String, Object>> driverList = driverDeductInfoService.selectMonthDriverDeductList(paramMap);
			
			mav.addObject("userSessionMap",  userSessionMap);
			mav.addObject("listData",  driverList);
			mav.addObject("paramMap", paramMap);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
						
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
    
    
    
    
    
    @RequestMapping( value="/downloadDriverDeductInfo", method = RequestMethod.GET )
   	public ModelAndView downloadDriverDeductInfo(ModelAndView mav,
   			HttpServletRequest request ,HttpServletResponse response) throws Exception {
   		
   		
   		Map<String, Object> paramMap = new HashMap<String, Object>();
   		Map<String, Object> fileMap = new HashMap<String, Object>();
   		
   		try{
   			
   			HttpSession session = request.getSession();
   			Map userSessionMap = (Map)session.getAttribute("user");
   			String companyId = session.getAttribute("companyId").toString();
   			
   			
   			paramMap = ParamUtils.getParamToMap(request);
   			//paramMap.put("companyId", companyId);
   			
   			
   			String selectMonth = request.getParameter("selectMonth");
   			String driverId = request.getParameter("driverId");
   			String today = WebUtils.getNow("yyyy-MM-dd");
   			
   			Map<String, Object> map = new HashMap<String, Object>();
			map.put("driverId",driverId);
			Map<String, Object> driver = driverService.selectDriver(map);
   			
			/*if(driver != null && driver.get("driver_id") != null){
				
				map.put("decideMonth", selectMonth);
				List<Map<String, Object>> driverDeductFileList = driverDeductFileService.selectDriverDeductFileList(map);
				if(driverDeductFileList.size() > 0) {
					for(int j = 0; j < driverDeductFileList.size(); j++) {
						Map<String, Object> driverDeductFile = driverDeductFileList.get(j);
						File removeFile = new File(rootDir+"/deduct_list"+driverDeductFile.get("driver_deduct_file_path").toString());
						if(removeFile.exists()){
							removeFile.delete();
						}
						driverDeductFileService.deleteDriverDeductFile(driverDeductFile);
					}
				}
				fileMap = driverDeductFileService.makeDriverDeductFile(map);
				fileMap.put("fileUploadPath", rootDir+"/deduct_list");
				fileMap.put("fileLogicName", fileMap.get("ORG_NAME").toString());
				fileMap.put("filePhysicName", fileMap.get("SAVE_NAME").toString());
				mav.addObject("fileMap", fileMap);*/
				/*mav.addObject("encodeUrl", URLEncoder.encode("http://52.78.153.148:8081/files/deduct_list"+fileMap.get("SAVE_NAME").toString(), "UTF-8"));*/
			/*}*/
			
   			//파일 상세
			fileMap.put("fileUploadPath", rootDir+"/forms");
			fileMap.put("fileLogicName", "("+selectMonth+")"+driver.get("driver_name").toString()+"_"+today+".xlsx");
			fileMap.put("filePhysicName", "deductlist.xlsx");
			fileMap.put("driverId", driverId);
			fileMap.put("selectMonth", selectMonth);
			
			
   		}catch(Exception e){
   			e.printStackTrace();
   		}
   		
   		//return mav;
   		return new ModelAndView("downloadView", "downloadFile", fileMap);
   		
   	}
    
    
    
    
    @RequestMapping( value="/viewDriverDeductInfo", method = RequestMethod.GET )
   	public ModelAndView viewDriverDeductInfo(ModelAndView mav,
   			HttpServletRequest request ,HttpServletResponse response) throws Exception {
   		
   		
   		Map<String, Object> paramMap = new HashMap<String, Object>();
   		Map<String, Object> fileMap = new HashMap<String, Object>();
   		
   		try{
   			
   			HttpSession session = request.getSession();
   			Map userSessionMap = (Map)session.getAttribute("user");
   			String companyId = session.getAttribute("companyId").toString();
   			
   			paramMap = ParamUtils.getParamToMap(request);
   			//paramMap.put("companyId", companyId);
   			
   			String selectMonth = request.getParameter("selectMonth");
   			String driverId = request.getParameter("driverId");
   			String today = WebUtils.getNow("yyyy-MM-dd");
   			String thisMonth = WebUtils.getNow("yyyy-MM");
   			
   			Map<String, Object> map = new HashMap<String, Object>();
			map.put("driverId",driverId);
			Map<String, Object> driver = driverService.selectDriver(map);
   			
   			//같은 월의 운송내역서를 생성 할 수 없도록 한다.
   			if(!thisMonth.equals(selectMonth)) {
   				
   				if(driver != null && driver.get("driver_id") != null){
   					map.put("driver",driver);
   					map.put("decideMonth", selectMonth);
   					List<Map<String, Object>> driverDeductFileList = driverDeductFileService.selectDriverDeductFileList(map);
   					
   					
   					//기존에 파일이 있는경우 지우고 새로 생성하지 않는다.
   					/*if(driverDeductFileList.size() > 0) {
   						for(int j = 0; j < driverDeductFileList.size(); j++) {
   							Map<String, Object> driverDeductFile = driverDeductFileList.get(j);
   							File removeFile = new File(rootDir+"/deduct_list"+driverDeductFile.get("driver_deduct_file_path").toString());
   							if(removeFile.exists()){
   								removeFile.delete();
   							}
   							driverDeductFileService.deleteDriverDeductFile(driverDeductFile);
   						}
   					}*/
   					
   				
   					//파일이 없는경우 생성하는 방법으로 변경
   					//파일은 하나만 있을테니.....
   					//fileMap.put("fileUploadPath", rootDir+"/deduct_list");
   					//fileMap.put("fileLogicName", driverDeductFileList.get(0).get("driver_deduct_file_nm").toString());
   					//fileMap.put("filePhysicName", driverDeductFileList.get(0).get("driver_deduct_file_path").toString());
   					
   					
   					//파일이 없는경우 생성함.
   					if(driverDeductFileList.size() > 0) {
   						//기사님에게 내역서가 공개 된 경우는 새로 생성 하지 않으나 그렇지 않은 경우에는 확인 할 때 마다 새로 생성 한다.
   						if(driverDeductFileList.get(0).get("driver_view_flag").toString().equals("Y")) {
   							map.put("driverDeductFile", driverDeductFileList.get(0));
   							//파일은 하나만 있을테니.....
   							fileMap.put("fileUploadPath", rootDir+"/deduct_list");
   							fileMap.put("fileLogicName", driverDeductFileList.get(0).get("driver_deduct_file_nm").toString());
   							fileMap.put("filePhysicName", driverDeductFileList.get(0).get("driver_deduct_file_path").toString());	
   						}else {
   							for(int j = 0; j < driverDeductFileList.size(); j++) {
   								Map<String, Object> driverDeductFile = driverDeductFileList.get(j);
   								driverDeductFile.put("driverDeductFileId", driverDeductFile.get("driver_deduct_file_id").toString());
   								File removeFile = new File(rootDir+"/deduct_list"+driverDeductFile.get("driver_deduct_file_path").toString());
   								if(removeFile.exists()){
   									removeFile.delete();
   								}
   								removeFile = new File(rootDir+"/deduct_list"+driverDeductFile.get("driver_deduct_file_path").toString()+".pdf");
   								if(removeFile.exists()){
   									removeFile.delete();
   								}
   								driverDeductFileService.deleteDriverDeductFile(driverDeductFile);
   							}
   							fileMap = driverDeductFileService.makeDriverDeductFile(map);
   							driverDeductFileList = driverDeductFileService.selectDriverDeductFileList(map);
   							map.put("driverDeductFile", driverDeductFileList.get(0));
   							fileMap.put("fileUploadPath", rootDir+"/deduct_list");
   							fileMap.put("fileLogicName", fileMap.get("ORG_NAME").toString());
   							fileMap.put("filePhysicName", fileMap.get("SAVE_NAME").toString());
   						}
   					}else {
   						//파일이 없는경우가 있을 수 없겠지만 없는경우 파일을 생성한다.
   						fileMap = driverDeductFileService.makeDriverDeductFile(map);
   						driverDeductFileList = driverDeductFileService.selectDriverDeductFileList(map);
   						map.put("driverDeductFile", driverDeductFileList.get(0));
   						fileMap.put("fileUploadPath", rootDir+"/deduct_list");
   						fileMap.put("fileLogicName", fileMap.get("ORG_NAME").toString());
   						fileMap.put("filePhysicName", fileMap.get("SAVE_NAME").toString());
   					}
   					
   					List<Map<String,Object>> fileList = driverDeductFileService.selectDriverDeductFileList(map);
   					
   					mav.addObject("fileList", fileList);
   					mav.addObject("map", map);
   					//response.setHeader("Content-type", "application/vnd.ms-excel;charset=UTF-8"); 
   					/*mav.addObject("encodeUrl", URLEncoder.encode("http://52.78.153.148:8081/files/deduct_list"+fileMap.get("SAVE_NAME").toString(), "UTF-8"));*/
   				}
   				
   				
   			}else {
   				
   				WebUtils.messageAndRedirectUrl(mav, "해당월의 운송내역서를 생성 할 수 없습니다.", "/account/viewMonthDriverDeduct.do?driverId="+driverId);
   				
   			}
   				
   		}catch(Exception e){
   			e.printStackTrace();
   		}
   		
  		return mav;
   		//return new ModelAndView("downloadView", "downloadFile", fileMap);
   		
   	}
    
    @RequestMapping( value="/excelTest", method = RequestMethod.GET )
   	public ModelAndView excelTest(ModelAndView mav,
   			HttpServletRequest request ,HttpServletResponse response) throws Exception {
   		
   		
   		try{
   			
   			
   			int getHour = Integer.parseInt(WebUtils.getNow("HH"));
   			
   			
   			//오전 8시에서 오후 10시 사이에만 플러스 친구 톡을 발송 한다.
   			if(getHour > 8 && getHour < 22) {
   				
   			}
   			
   			//response.setContentType("application/octet-stream");
   			//response.setHeader("Content-disposition", "inline;filename=0358b4a7-f52d-4566-baa2-49a60e2be7e4.xlsx;");
				
   		}catch(Exception e){
   			e.printStackTrace();
   		}
   		
   		
   		
  		return mav;
   		
   	}
    
    
    
    @RequestMapping( value="/viewDriverBillingFile", method = RequestMethod.GET )
   	public ModelAndView viewDriverBillingFile(ModelAndView mav,
   			HttpServletRequest request ,HttpServletResponse response) throws Exception {
   		
   		
   		Map<String, Object> paramMap = new HashMap<String, Object>();
   		Map<String, Object> fileMap = new HashMap<String, Object>();
   		
   		try{
   			
   			HttpSession session = request.getSession();
   			Map userSessionMap = (Map)session.getAttribute("user");
   			String companyId = session.getAttribute("companyId").toString();
   			
   			
   			paramMap = ParamUtils.getParamToMap(request);
   			
   			String selectMonth = request.getParameter("selectMonth");
   			String driverId = request.getParameter("driverId");
   			String today = WebUtils.getNow("yyyy-MM-dd");
   			
   			Map<String, Object> map = new HashMap<String, Object>();
			map.put("driverId",driverId);
			Map<String, Object> driver = driverService.selectDriver(map);
   			
			if(driver != null && driver.get("driver_id") != null){
				map.put("driver",driver);
				map.put("decideMonth", selectMonth);
				map.put("billingDivision","O");
				Map<String, Object> driverBillingFile = driverBillingFileService.selectDriverBillingFile(map);
				
				mav.addObject("fileMap", driverBillingFile);
				mav.addObject("map", map);
				mav.addObject("paramMap", paramMap);
				
				
			}
				
   		}catch(Exception e){
   			e.printStackTrace();
   		}
   		
   		
   		
  		return mav;
   		
   	}
    
    @RequestMapping(value = "/updateDriverViewFlag", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updateDriverViewFlag(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
		
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			
			String companyId = session.getAttribute("companyId").toString();

			String driverId = request.getParameter("driverId").toString();
			String decideMonth = request.getParameter("decideMonth").toString();
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("driverId",driverId);
			map.put("decideMonth",decideMonth);
			map.put("driverViewFlag","Y");
			
			driverDeductFileService.updateDriverViewFlag(map);
				
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	} 
    
    
    
    
    
    @RequestMapping(value = "/depositProcess", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi depositProcess(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
		
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			String companyId = session.getAttribute("companyId").toString();
			
			String summary =  request.getParameter("summary").toString();
			String occurrenceDt = request.getParameter("occurrenceDt").toString();
			String amount = request.getParameter("amount").toString();
			String etc = request.getParameter("etc").toString();
			String debtorCreditorId = request.getParameter("debtorCreditorId").toString();
		
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("debtorCreditorId",debtorCreditorId);
			map.put("amount",amount);
			
			
			Map<String, Object> debtorCreditor = debtorCreditorService.selectDebtorCreditor(map);
			
			//결제정보에 대변아이디를 입력 
			String debtorCreditorIdForC = "DCI"+UUID.randomUUID().toString().replaceAll("-","");
			
			if(debtorCreditor != null) {
				
				int amountIntVal = Integer.parseInt(amount);
				int totalIntVal = Integer.parseInt(debtorCreditor.get("total_val").toString().replaceAll(",", ""));
				
				Map<String, Object> updateMap = new HashMap<String, Object>();
				updateMap.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
				updateMap.put("logType","U");
				
				//입금된 금액과 차변의 금액이 같은 경우 (뷰단에서 차변의 금액보다 큰 금액을 입력 할 수 없게 처리 하고 있지만 혹시 모르니.....)
				if(amountIntVal == totalIntVal) {
					
					updateMap.put("occurrenceDt", occurrenceDt);
					updateMap.put("payment", "Y");
					updateMap.put("debtorCreditorId", debtorCreditorId);
					updateMap.put("debtorCreditorIdForC", debtorCreditorIdForC);
					updateMap.put("etc", etc);
					paymentInfoService.updatePaymentInfoForAllDeposit(updateMap);
					paymentInfoService.insertPaymentInfoLogByMap(updateMap); //로그기록
					
				}else {
					//입금된 금액과 차변의 금액이 다른경우 
					Map<String, Object> searchMap = new HashMap<String, Object>();
					searchMap.put("debtorCreditorId", debtorCreditorId);
					searchMap.put("amount", amountIntVal);
					
					//입금된 금액으로 입금완료 처리 할 수 있는 금액을 확인 한다.
					Map<String, Object> runTotal = paymentInfoService.selectPaymentInfoRunTotal(searchMap);
					
					//입금 완료 처리 할 수 있는 탁송료
					String rt = runTotal != null && runTotal.get("rt") != null && !runTotal.get("rt").toString().equals("") ?runTotal.get("rt").toString() : "0";
							
					if(runTotal != null) {
						rt = runTotal.get("rt").toString();
					}
							
					//남는 탁송료
					int remain = amountIntVal-Integer.parseInt(rt);
					
					updateMap.put("payment", "Y");
					updateMap.put("occurrenceDt", occurrenceDt);
					updateMap.put("debtorCreditorIdForC", debtorCreditorIdForC);
					updateMap.put("debtorCreditorId", debtorCreditorId);
					updateMap.put("amount", amountIntVal);
					updateMap.put("etc", etc);
					
					//입금 완료 처리 한다.
					paymentInfoService.updatePaymentInfoForPartDeposit(updateMap);
					paymentInfoService.insertPaymentInfoLogByMap(updateMap); //로그기록
					//남는 탁송료는 부분입금 처리 한다.
					//부가세 계산할때 1원 차이 나는 부분을 어떻게 해야 할까.....
					if(remain != 0 && remain >= 10) {
						
						Map<String, Object> info = paymentInfoService.selectPaymentInfoForNextPayment(searchMap);
							
						Map<String, Object> findMap = new HashMap<String, Object>();
						findMap.put("allocationId", info.get("allocation_id").toString());
						Map<String, Object> allocation = allocationService.selectAllocation(findMap);
						List<Map<String, Object>> paymentList = paymentInfoService.selectPaymentInfoListForPayment(findMap);
						
						Map<String, Object> subUpdateMap = new HashMap<String, Object>();
						subUpdateMap.put("payment", "P");		//부분결제
						subUpdateMap.put("occurrenceDt", occurrenceDt);		//입금일
						subUpdateMap.put("debtorCreditorIdForC", debtorCreditorIdForC);		//대변아이디
						subUpdateMap.put("allocationId", allocation.get("allocation_id").toString());		//배차아이디
						subUpdateMap.put("idx",paymentList.get(0).get("idx"));		//결제정보 인덱스
						subUpdateMap.put("etc",etc);
						
						//부가세 별도이면
						if(allocation.get("vat_exclude_yn").toString().equals("Y") && allocation.get("vat_include_yn").toString().equals("N")) {
							
							remain = (int)Math.round(remain/1.1);
							subUpdateMap.put("remain", String.valueOf(remain));		
						//부가세 포함이면	
						}else if(allocation.get("vat_exclude_yn").toString().equals("N") && allocation.get("vat_include_yn").toString().equals("Y")) {
							remain = (int)Math.round(remain/1.1);
							subUpdateMap.put("remain", String.valueOf(remain));		
						//별도도 아니고 포함도 아니면
						}else {
							subUpdateMap.put("remain", String.valueOf(remain));		
						}
						
						subUpdateMap.put("remain", String.valueOf(remain));
						subUpdateMap.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
						subUpdateMap.put("logType","U");
						paymentInfoService.updatePaymentInfoForPayment(subUpdateMap);
						paymentInfoService.insertPaymentInfoLogByMap(subUpdateMap); //로그기록
						
						//부분결제 후 남은 금액
						int finalRemain = 0; 
								
						//부가세 별도이면
						if(allocation.get("vat_exclude_yn").toString().equals("Y") && allocation.get("vat_include_yn").toString().equals("N")) {
							finalRemain = Integer.parseInt(allocation.get("sales_total").toString())-remain;
						//부가세 포함이면	
						}else if(allocation.get("vat_exclude_yn").toString().equals("N") && allocation.get("vat_include_yn").toString().equals("Y")) {
							finalRemain = Integer.parseInt(allocation.get("sales_total").toString())-remain;
						//별도도 아니고 포함도 아니면
						}else {
							finalRemain = Integer.parseInt(allocation.get("price").toString())-remain;		
						}
						
						//그럴리 없겠지만......
						if(finalRemain < 0) {
							finalRemain = finalRemain*-1;
						}
						
						PaymentInfoVO paymentInfoVO = new PaymentInfoVO();
						paymentInfoVO.setAllocationId(allocation.get("allocation_id").toString());
						paymentInfoVO.setAmount(String.valueOf(finalRemain));
						paymentInfoVO.setBillingDivision(paymentList.get(0).get("billing_division").toString());
						paymentInfoVO.setBillingDt(paymentList.get(0).get("billing_dt").toString());
						paymentInfoVO.setPayment("N");
						paymentInfoVO.setPaymentDivision("01");
						paymentInfoVO.setPaymentDt("");
						paymentInfoVO.setPaymentKind(paymentList.get(0).get("payment_kind").toString());
						paymentInfoVO.setPaymentPartner(paymentList.get(0).get("payment_partner").toString());
						paymentInfoVO.setPaymentPartnerId(paymentList.get(0).get("payment_partner_id").toString());
						paymentInfoVO.setVatExcludeYn(allocation.get("vat_exclude_yn").toString());
						paymentInfoVO.setVatIncludeYn(allocation.get("vat_include_yn").toString());
						paymentInfoVO.setModId(userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
						paymentInfoVO.setLogType("I");
						paymentInfoService.insertPaymentInfo(paymentInfoVO);
						paymentInfoService.insertPaymentInfoLogByVO(paymentInfoVO); //로그기록
						
					}
				}
				
				//입금된 내역을 대변에 등록 한다.
				
				DebtorCreditorVO debtorCreditorVO = new DebtorCreditorVO();
				debtorCreditorVO.setCustomerId(debtorCreditor.get("customer_id").toString());
				debtorCreditorVO.setDebtorCreditorId(debtorCreditorIdForC);
				debtorCreditorVO.setOccurrenceDt(occurrenceDt);
				  
				//String summaryDt = ""; //한건인 경우 각 건의 상차지 하차지로 하여 적요에 작성 하나 한건이 아닌경우 월 단위의 적요를 작성 한다. 
				//summaryDt = debtorCreditor.get("summary").toString()+" 입금"; 
				
				if(summary.equals("")) {
					debtorCreditorVO.setSummary(debtorCreditor.get("summary").toString()+" 입금");	
				}else {
					debtorCreditorVO.setSummary(summary);
				}
				
				debtorCreditorVO.setTotalVal(amount);
				debtorCreditorVO.setPublishYn(debtorCreditor.get("publish_yn").toString());
				debtorCreditorVO.setType(BaseAppConstants.DEBTOR_CREDITOR_TYPE_C);
				debtorCreditorVO.setDebtorId(debtorCreditorId);
				debtorCreditorVO.setRegisterId(userMap.get("emp_id").toString());
				debtorCreditorVO.setCompanyId(companyId);
				//대변에 입력 하고 
				debtorCreditorService.insertDebtorCreditor(debtorCreditorVO);
				
			}else {
				//해당 내역을 찾을 수 없습니다.
				result.setResultCode("E001");
			}
				
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	} 
    
    
    
    
    @RequestMapping(value = "/depositAllProcess", method = RequestMethod.POST)
 	@ResponseBody
 	public ResultApi depositAllProcess(ModelAndView mav,
 			Model model,HttpServletRequest request ,HttpServletResponse response
 			) throws Exception{
 		
 		ResultApi result = new ResultApi();
 		try{
 		
 			HttpSession session = request.getSession(); 
 			Map userMap = (Map)session.getAttribute("user");
 			String companyId = session.getAttribute("companyId").toString();
 			
 			String customerId = request.getParameter("customerId").toString();
 			String debtorCreditorId = "DCI"+UUID.randomUUID().toString().replaceAll("-","");
 			String amount = request.getParameter("amount").toString();
 			String occurrenceDt = request.getParameter("occurrenceDt").toString();
 			String summary =  request.getParameter("summary").toString();
 			String etc = request.getParameter("etc").toString();
 			
 			//결제정보에 대변아이디를 입력 
 			String debtorCreditorIdForC = "DCI"+UUID.randomUUID().toString().replaceAll("-","");
 		
			int amountIntVal = Integer.parseInt(amount);
						
			Map<String, Object> updateMap = new HashMap<String, Object>();
					
			Map<String, Object> searchMap = new HashMap<String, Object>();
			searchMap.put("paymentPartnerId", customerId);
			searchMap.put("amount", amountIntVal);
			
			//입금된 금액으로 입금완료 처리 할 수 있는 금액을 확인 한다.
			Map<String, Object> runTotal = paymentInfoService.selectPaymentInfoAllRunTotal(searchMap);
			
			//입금 완료 처리 할 수 있는 탁송료
			String rt = runTotal != null && runTotal.get("rt") != null && !runTotal.get("rt").toString().equals("") ?runTotal.get("rt").toString() : "0";
		
			//남는 탁송료
			int remain = amountIntVal-Integer.parseInt(rt);
			
			updateMap.put("payment", "Y");
			updateMap.put("occurrenceDt", occurrenceDt);
			updateMap.put("debtorCreditorIdForC", debtorCreditorIdForC);
			updateMap.put("amount", amountIntVal);
			updateMap.put("paymentPartnerId", customerId);
			updateMap.put("etc", etc);
			updateMap.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
			updateMap.put("logType", "U");
			//입금 완료 처리 한다.
			paymentInfoService.updatePaymentInfoForAllPartDeposit(updateMap);
			paymentInfoService.insertPaymentInfoLogByMap(updateMap); //로그기록
			
			//남는 탁송료는 부분입금 처리 한다.
			if(remain != 0) {
				
				Map<String, Object> info = paymentInfoService.selectPaymentInfoForAllNextPayment(searchMap);
					
				Map<String, Object> findMap = new HashMap<String, Object>();
				findMap.put("allocationId", info.get("allocation_id").toString());
				Map<String, Object> allocation = allocationService.selectAllocation(findMap);
				List<Map<String, Object>> paymentList = paymentInfoService.selectPaymentInfoListForPayment(findMap);
				
				Map<String, Object> subUpdateMap = new HashMap<String, Object>();
				subUpdateMap.put("payment", "P");		//부분결제
				subUpdateMap.put("occurrenceDt", occurrenceDt);		//입금일
				subUpdateMap.put("debtorCreditorIdForC", debtorCreditorIdForC);		//대변아이디
				subUpdateMap.put("allocationId", allocation.get("allocation_id").toString());		//배차아이디
				subUpdateMap.put("idx",paymentList.get(0).get("idx"));		//결제정보 인덱스
				subUpdateMap.put("etc",etc);
				
				//부가세 별도이면
				if(allocation.get("vat_exclude_yn").toString().equals("Y") && allocation.get("vat_include_yn").toString().equals("N")) {
					
					remain = (int)Math.round(remain/1.1);
					subUpdateMap.put("remain", String.valueOf(remain));		
				//부가세 포함이면	
				}else if(allocation.get("vat_exclude_yn").toString().equals("N") && allocation.get("vat_include_yn").toString().equals("Y")) {
					remain = (int)Math.round(remain/1.1);
					subUpdateMap.put("remain", String.valueOf(remain));		
				//별도도 아니고 포함도 아니면
				}else {
					subUpdateMap.put("remain", String.valueOf(remain));		
				}
				
				subUpdateMap.put("remain", String.valueOf(remain));
				subUpdateMap.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
				subUpdateMap.put("logType","U");
				paymentInfoService.updatePaymentInfoForPayment(subUpdateMap);
				paymentInfoService.insertPaymentInfoLogByMap(subUpdateMap); //로그기록
			
				//부분결제 후 남은 금액은 미결제로
				int finalRemain = 0; 
						
				//부가세 별도이면
				if(allocation.get("vat_exclude_yn").toString().equals("Y") && allocation.get("vat_include_yn").toString().equals("N")) {
					finalRemain = Integer.parseInt(allocation.get("sales_total").toString())-remain;
				//부가세 포함이면	
				}else if(allocation.get("vat_exclude_yn").toString().equals("N") && allocation.get("vat_include_yn").toString().equals("Y")) {
					finalRemain = Integer.parseInt(allocation.get("sales_total").toString())-remain;
				//별도도 아니고 포함도 아니면
				}else {
					finalRemain = Integer.parseInt(allocation.get("price").toString())-remain;		
				}
				
				//그럴리 없겠지만......
				if(finalRemain < 0) {
					finalRemain = finalRemain*-1;
				}
				
				PaymentInfoVO paymentInfoVO = new PaymentInfoVO();
				paymentInfoVO.setAllocationId(allocation.get("allocation_id").toString());
				paymentInfoVO.setAmount(String.valueOf(finalRemain));
				paymentInfoVO.setBillingDivision(paymentList.get(0).get("billing_division").toString());
				paymentInfoVO.setBillingDt(paymentList.get(0).get("billing_dt") != null ? paymentList.get(0).get("billing_dt").toString() : "");
				paymentInfoVO.setPayment("N");
				paymentInfoVO.setPaymentDivision("01");
				paymentInfoVO.setPaymentDt("");
				paymentInfoVO.setPaymentKind(paymentList.get(0).get("payment_kind").toString());
				paymentInfoVO.setPaymentPartner(paymentList.get(0).get("payment_partner").toString());
				paymentInfoVO.setPaymentPartnerId(paymentList.get(0).get("payment_partner_id").toString());
				paymentInfoVO.setVatExcludeYn(allocation.get("vat_exclude_yn").toString());
				paymentInfoVO.setVatIncludeYn(allocation.get("vat_include_yn").toString());
				paymentInfoVO.setModId(userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
				paymentInfoVO.setLogType("I");
    			paymentInfoService.insertPaymentInfo(paymentInfoVO);
    			paymentInfoService.insertPaymentInfoLogByVO(paymentInfoVO); //로그기록
				
			}
			
			//입금된 내역을 대변에 등록 한다.
			
			DebtorCreditorVO debtorCreditorVO = new DebtorCreditorVO();
			debtorCreditorVO.setCustomerId(customerId);
			debtorCreditorVO.setDebtorCreditorId(debtorCreditorIdForC);
			debtorCreditorVO.setOccurrenceDt(occurrenceDt);
			  
			//String summaryDt = ""; //한건인 경우 각 건의 상차지 하차지로 하여 적요에 작성 하나 한건이 아닌경우 월 단위의 적요를 작성 한다. 
			//summaryDt = debtorCreditor.get("summary").toString()+" 입금"; 
			
			debtorCreditorVO.setSummary(summary);
			debtorCreditorVO.setTotalVal(amount);
			debtorCreditorVO.setPublishYn("A");				//입금된 건이 발행,미발행이 섞여 있을 수 있으므로 특정이 안되네......
			debtorCreditorVO.setType(BaseAppConstants.DEBTOR_CREDITOR_TYPE_C);
			debtorCreditorVO.setDebtorId(debtorCreditorId);
			debtorCreditorVO.setRegisterId(userMap.get("emp_id").toString());
			debtorCreditorVO.setCompanyId(companyId);
			//대변에 입력 하고 
			debtorCreditorService.insertDebtorCreditor(debtorCreditorVO);
			
		
 				
 		}catch(Exception e){
 			e.printStackTrace();
 			result.setResultCode("0001");
 		}
 		
 		return result;
 	} 
    
    
    @RequestMapping(value = "/withdrawAllProcess", method = RequestMethod.POST)
 	@ResponseBody
 	public ResultApi withdrawAllProcess(ModelAndView mav,
 			Model model,HttpServletRequest request ,HttpServletResponse response
 			) throws Exception{
 		
 		ResultApi result = new ResultApi();
 		try{
 		
 			HttpSession session = request.getSession(); 
 			Map userMap = (Map)session.getAttribute("user");
 			String companyId = session.getAttribute("companyId").toString();
 			
 			String paymentPartnerId = request.getParameter("paymentPartnerId").toString();
 			
 			String withdrawId = "WDI"+UUID.randomUUID().toString().replaceAll("-","");
 			
 			String amount = request.getParameter("amount").toString();
 			String occurrenceDt = request.getParameter("occurrenceDt").toString();
 			String summary =  request.getParameter("summary").toString();
 			String etc = request.getParameter("etc").toString();
 			
			int amountIntVal = Integer.parseInt(amount);
						
			Map<String, Object> updateMap = new HashMap<String, Object>();
					
			Map<String, Object> searchMap = new HashMap<String, Object>();
			searchMap.put("paymentPartnerId", paymentPartnerId);
			searchMap.put("amount", amountIntVal);
			
			
			//입금된 금액으로 입금완료 처리 할 수 있는 금액을 확인 한다.
			Map<String, Object> runTotal = paymentInfoService.selectWithdrawAllRunTotal(searchMap);
			
			//입금 완료 처리 할 수 있는 탁송료
			String rt = runTotal != null && runTotal.get("rt") != null && !runTotal.get("rt").toString().equals("") ?runTotal.get("rt").toString() : "0"; 
		
			//남는 탁송료
			int remain = amountIntVal-Integer.parseInt(rt);
			
			updateMap.put("payment", "Y");
			updateMap.put("occurrenceDt", occurrenceDt);
			updateMap.put("withdrawId", withdrawId);
			updateMap.put("amount", amountIntVal);
			updateMap.put("paymentPartnerId", paymentPartnerId);
			updateMap.put("etc", etc);
			updateMap.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
			updateMap.put("logType", "U");
			//입금 완료 처리 한다.
			paymentInfoService.updatePaymentInfoForAllPartWithdraw(updateMap);
			paymentInfoService.insertPaymentInfoLogByMap(updateMap); //로그기록
			
			//남는 탁송료는 부분입금 처리 한다.
			if(remain != 0) {
				
				Map<String, Object> info = paymentInfoService.selectPaymentInfoForAllNextWithdraw(searchMap);
					
				Map<String, Object> findMap = new HashMap<String, Object>();
				findMap.put("allocationId", info.get("allocation_id").toString());
				findMap.put("paymentPartnerId", paymentPartnerId);
				
				
				Map<String, Object> allocation = allocationService.selectAllocation(findMap);
				
				List<Map<String, Object>> paymentList = paymentInfoService.selectPaymentInfoListForWithdraw(findMap);
				
				Map<String, Object> subUpdateMap = new HashMap<String, Object>();
				subUpdateMap.put("payment", "P");		//부분결제
				subUpdateMap.put("occurrenceDt", occurrenceDt);		//입금일
				subUpdateMap.put("paymentPartnerId", paymentPartnerId);
				subUpdateMap.put("allocationId", allocation.get("allocation_id").toString());		//배차아이디
				subUpdateMap.put("idx",paymentList.get(0).get("idx"));		//결제정보 인덱스
				subUpdateMap.put("etc",etc);
				subUpdateMap.put("withdrawId", withdrawId);
				
				subUpdateMap.put("remain", String.valueOf(remain));		
				subUpdateMap.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
				subUpdateMap.put("logType","U");
				paymentInfoService.updatePaymentInfoForWithdraw(subUpdateMap);
				paymentInfoService.insertPaymentInfoLogByMap(subUpdateMap); //로그기록
			
				//부분결제 후 남은 금액은 미결제로
				int finalRemain = 0; 
				
				finalRemain = Integer.parseInt(paymentList.get(0).get("amount").toString().replaceAll(",", ""))-remain;		
				
				//그럴리 없겠지만......
				if(finalRemain < 0) {
					finalRemain = finalRemain*-1;
				}
				
				PaymentInfoVO paymentInfoVO = new PaymentInfoVO();
				paymentInfoVO.setAllocationId(allocation.get("allocation_id").toString());
				paymentInfoVO.setAmount(String.valueOf(finalRemain));
				paymentInfoVO.setBillingDivision(paymentList.get(0).get("billing_division").toString());
				paymentInfoVO.setBillingDt(paymentList.get(0).get("billing_dt") != null ? paymentList.get(0).get("billing_dt").toString() : "");
				paymentInfoVO.setPayment("N");
				paymentInfoVO.setPaymentDivision(info.get("payment_division").toString());
				paymentInfoVO.setPaymentDt("");
				paymentInfoVO.setPaymentKind(paymentList.get(0).get("payment_kind").toString());
				paymentInfoVO.setPaymentPartner(paymentList.get(0).get("payment_partner").toString());
				paymentInfoVO.setPaymentPartnerId(paymentList.get(0).get("payment_partner_id").toString());
				
				if(paymentList.get(0).get("deduction_rate") != null && !paymentList.get(0).get("deduction_rate").toString().equals("")) {
					int deductionRate = Integer.parseInt(paymentList.get(0).get("deduction_rate").toString());
					paymentInfoVO.setDeductionRate(paymentList.get(0).get("deduction_rate").toString());
					String billForPayment = String.valueOf(finalRemain-(deductionRate*finalRemain/100));
					paymentInfoVO.setBillForPayment(billForPayment);
				}else {
					paymentInfoVO.setDeductionRate("0");
					paymentInfoVO.setBillForPayment(String.valueOf(finalRemain));	
				}
				paymentInfoVO.setBillingStatus(paymentList.get(0).get("billing_status")!= null && !paymentList.get(0).get("billing_status").toString().equals("")?paymentList.get(0).get("billing_status").toString():"");
				paymentInfoVO.setDecideFinal(paymentList.get(0).get("decide_final")!= null && !paymentList.get(0).get("decide_final").toString().equals("")?paymentList.get(0).get("decide_final").toString():"");
				paymentInfoVO.setDecideFinalId(paymentList.get(0).get("decide_final_id")!= null && !paymentList.get(0).get("decide_final_id").toString().equals("")?paymentList.get(0).get("decide_final_id").toString():"");
				paymentInfoVO.setDecideMonth(paymentList.get(0).get("decide_month")!= null && !paymentList.get(0).get("decide_month").toString().equals("")?paymentList.get(0).get("decide_month").toString():"");
				paymentInfoVO.setDecideStatus(paymentList.get(0).get("decide_status")!= null && !paymentList.get(0).get("decide_status").toString().equals("")?paymentList.get(0).get("decide_status").toString():"");
				paymentInfoVO.setModId(userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자추가
				paymentInfoVO.setLogType("I");
				paymentInfoService.insertPaymentInfo(paymentInfoVO);
				paymentInfoService.insertPaymentInfoLogByVO(paymentInfoVO); //로그기록
				
			}
			
			//출금된 내역을 출금내역 테이블에 등록한다.
			WithdrawVO withdrawVO = new WithdrawVO();
			
			withdrawVO.setEmpId(userMap.get("emp_id").toString());
			withdrawVO.setEtc(etc);
			withdrawVO.setOccurrenceDt(occurrenceDt);
			withdrawVO.setPaymentPartnerId(paymentPartnerId);
			withdrawVO.setSummary(summary);
			withdrawVO.setTotalVal(amount);
			withdrawVO.setWithdrawId(withdrawId);
			withdrawVO.setCompanyId(companyId);
			
			paymentInfoService.insertWithdraw(withdrawVO);
			
 		}catch(Exception e){
 			e.printStackTrace();
 			result.setResultCode("0001");
 		}
 		
 		return result;
 	} 
    
    
    
    @RequestMapping(value = "/withdrawProcess", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi withdrawProcess(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
		
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			String companyId = session.getAttribute("companyId").toString();
			
			String paymentPartnerId =  request.getParameter("paymentPartnerId").toString();
			String decideMonth =  request.getParameter("decideMonth").toString();
			String summary =  request.getParameter("summary").toString();
			String occurrenceDt = request.getParameter("occurrenceDt").toString();
			String amount = request.getParameter("amount").toString();
			String etc = request.getParameter("etc").toString();
			
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("paymentPartnerId",paymentPartnerId);
			map.put("decideMonth",decideMonth);
			
			
			Map<String, Object> paymentInfo = paymentInfoService.selectPaymentInfoForWithdraw(map);
			
			//결제정보에 대변아이디를 입력 
			String withdrawId = "WDI"+UUID.randomUUID().toString().replaceAll("-","");
			
			if(paymentInfo != null) {
				
				int amountIntVal = Integer.parseInt(amount);
				int totalIntVal = Integer.parseInt(paymentInfo.get("amount_sum").toString().replaceAll(",", ""));
				
				Map<String, Object> updateMap = new HashMap<String, Object>();
				
				//입금된 금액과 차변의 금액이 같은 경우 (뷰단에서 차변의 금액보다 큰 금액을 입력 할 수 없게 처리 하고 있지만 혹시 모르니.....)
				if(amountIntVal == totalIntVal) {
					
					updateMap.put("occurrenceDt", occurrenceDt);
					updateMap.put("payment", "Y");
					updateMap.put("paymentPartnerId",paymentPartnerId);
					updateMap.put("decideMonth",decideMonth);
					updateMap.put("etc", etc);
					updateMap.put("debtorCreditorId",withdrawId);
					updateMap.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
					updateMap.put("logType","U");
					paymentInfoService.updatePaymentInfoForAllWithdraw(updateMap);
					paymentInfoService.insertPaymentInfoLogByMap(updateMap); //로그기록
					
				}else {
					//입금된 금액과 차변의 금액이 다른경우 
					Map<String, Object> searchMap = new HashMap<String, Object>();
					searchMap.put("paymentPartnerId",paymentPartnerId);
					searchMap.put("decideMonth",decideMonth);
					searchMap.put("amount", amountIntVal);
					
					//입금된 금액으로 입금완료 처리 할 수 있는 금액을 확인 한다.
					Map<String, Object> runTotal = paymentInfoService.selectPaymentInfoWithdrawRunTotal(searchMap);
					
					//입금 완료 처리 할 수 있는 탁송료
					String rt = runTotal != null && runTotal.get("rt") != null && !runTotal.get("rt").toString().equals("") ?runTotal.get("rt").toString() : "0";
							
					if(runTotal != null) {
						rt = runTotal.get("rt").toString();
					}
							
					//남는 탁송료
					int remain = amountIntVal-Integer.parseInt(rt);
					
					updateMap.put("occurrenceDt", occurrenceDt);
					updateMap.put("payment", "Y");
					updateMap.put("paymentPartnerId",paymentPartnerId);
					updateMap.put("decideMonth",decideMonth);
					updateMap.put("etc", etc);
					updateMap.put("debtorCreditorId",withdrawId);
					updateMap.put("amount", amountIntVal);
					updateMap.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
					updateMap.put("logType", "U");
					//입금 완료 처리 한다.
					paymentInfoService.updatePaymentInfoForPartWithdraw(updateMap);
					paymentInfoService.insertPaymentInfoLogByMap(updateMap); //로그기록
											    
					//남는 탁송료는 부분입금 처리 한다.
					if(remain != 0) {
						
						Map<String, Object> info = paymentInfoService.selectPaymentInfoForPartNextWithdraw(searchMap);
							
						Map<String, Object> findMap = new HashMap<String, Object>();
						findMap.put("allocationId", info.get("allocation_id").toString());
						Map<String, Object> allocation = allocationService.selectAllocation(findMap);
						List<Map<String, Object>> paymentList = paymentInfoService.selectPaymentInfoListForWithdraw(findMap);
						
						Map<String, Object> subUpdateMap = new HashMap<String, Object>();
						subUpdateMap.put("payment", "P");		//부분결제
						subUpdateMap.put("occurrenceDt", occurrenceDt);		//입금일
						subUpdateMap.put("debtorCreditorId", withdrawId);		//대변아이디
						subUpdateMap.put("paymentPartnerId", paymentPartnerId);
						subUpdateMap.put("allocationId", allocation.get("allocation_id").toString());		//배차아이디
						subUpdateMap.put("idx",paymentList.get(0).get("idx"));		//결제정보 인덱스
						subUpdateMap.put("etc",etc);
						subUpdateMap.put("withdrawId", withdrawId);
						subUpdateMap.put("remain", String.valueOf(remain));		
						subUpdateMap.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
						subUpdateMap.put("logType","U");
						paymentInfoService.updatePaymentInfoForWithdraw(subUpdateMap);
						paymentInfoService.insertPaymentInfoLogByMap(subUpdateMap); //로그기록
					
						//부분결제 후 남은 금액
						int finalRemain = 0; 
						
						finalRemain = Integer.parseInt(paymentList.get(0).get("amount").toString().replaceAll(",", ""))-remain;		
						
						
						//그럴리 없겠지만......
						if(finalRemain < 0) {
							finalRemain = finalRemain*-1;
						}
						
						PaymentInfoVO paymentInfoVO = new PaymentInfoVO();
						paymentInfoVO.setAllocationId(allocation.get("allocation_id").toString());
						paymentInfoVO.setAmount(String.valueOf(finalRemain));
						paymentInfoVO.setBillingDivision(paymentList.get(0).get("billing_division").toString());
						paymentInfoVO.setBillingDt(paymentList.get(0).get("billing_dt").toString());
						paymentInfoVO.setPayment("N");
						paymentInfoVO.setPaymentDivision(info.get("payment_division").toString());
						paymentInfoVO.setPaymentDt("");
						paymentInfoVO.setPaymentKind(paymentList.get(0).get("payment_kind").toString());
						paymentInfoVO.setPaymentPartner(paymentList.get(0).get("payment_partner").toString());
						paymentInfoVO.setPaymentPartnerId(paymentList.get(0).get("payment_partner_id").toString());
						
						if(paymentList.get(0).get("deduction_rate") != null && !paymentList.get(0).get("deduction_rate").toString().equals("")) {
							int deductionRate = Integer.parseInt(paymentList.get(0).get("deduction_rate").toString());
							paymentInfoVO.setDeductionRate(paymentList.get(0).get("deduction_rate").toString());
							String billForPayment = String.valueOf(finalRemain-(deductionRate*finalRemain/100));
							paymentInfoVO.setBillForPayment(billForPayment);
						}else {
							paymentInfoVO.setDeductionRate("0");
							paymentInfoVO.setBillForPayment(String.valueOf(finalRemain));	
						}
						paymentInfoVO.setBillingStatus(paymentList.get(0).get("billing_status")!= null && !paymentList.get(0).get("billing_status").toString().equals("")?paymentList.get(0).get("billing_status").toString():"");
						paymentInfoVO.setDecideFinal(paymentList.get(0).get("decide_final")!= null && !paymentList.get(0).get("decide_final").toString().equals("")?paymentList.get(0).get("decide_final").toString():"");
						paymentInfoVO.setDecideFinalId(paymentList.get(0).get("decide_final_id")!= null && !paymentList.get(0).get("decide_final_id").toString().equals("")?paymentList.get(0).get("decide_final_id").toString():"");
						paymentInfoVO.setDecideMonth(paymentList.get(0).get("decide_month")!= null && !paymentList.get(0).get("decide_month").toString().equals("")?paymentList.get(0).get("decide_month").toString():"");
						paymentInfoVO.setDecideStatus(paymentList.get(0).get("decide_status")!= null && !paymentList.get(0).get("decide_status").toString().equals("")?paymentList.get(0).get("decide_status").toString():"");
						paymentInfoVO.setModId(userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
						paymentInfoVO.setLogType("I");
						paymentInfoService.insertPaymentInfo(paymentInfoVO);
						paymentInfoService.insertPaymentInfoLogByVO(paymentInfoVO); //로그기록
					}
					
				}
				
				//출금된 내역을 출금내역 테이블에 등록한다.
				WithdrawVO withdrawVO = new WithdrawVO();
				
				withdrawVO.setEmpId(userMap.get("emp_id").toString());
				withdrawVO.setEtc(etc);
				withdrawVO.setOccurrenceDt(occurrenceDt);
				withdrawVO.setPaymentPartnerId(paymentPartnerId);
				withdrawVO.setSummary(summary);
				withdrawVO.setTotalVal(amount);
				withdrawVO.setWithdrawId(withdrawId);
				withdrawVO.setCompanyId(companyId);
				
				paymentInfoService.insertWithdraw(withdrawVO);
				
			}else {
				//해당 내역을 찾을 수 없습니다.
				result.setResultCode("E001");
			}
				
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	} 
    
    
    
    
    @RequestMapping(value = "/withdrawListDetail", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView withdrawListDetail(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{	
			
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			String companyId = session.getAttribute("companyId").toString();
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			/*String paymentPartnerId = request.getParameter("paymentPartnerId") == null || request.getParameter("paymentPartnerId").toString().equals("") ? "A" : request.getParameter("paymentPartnerId").toString();
			String decideMonth = request.getParameter("decideMonth") != null && !request.getParameter("decideMonth").toString().equals("") ? request.getParameter("decideMonth").toString() :"";
			paramMap.put("decideMonth", decideMonth);
			paramMap.put("paymentPartnerId", paymentPartnerId);*/
			String paymentKind = request.getParameter("paymentKind") == null ? "" : request.getParameter("paymentKind").toString(); 
			
			paramMap.put("decideStatus", "Y");
			paramMap.put("paymentKind", paymentKind);
			
			//paramMap.put("decideFinal", "Y");
			
			//paramMap.put("companyId", companyId);
						
			int totalCount = adjustmentService.selectAllocationCountByPaymentPartnerIdAndDecideMonth(paramMap);
			PagingUtils.setPageing(request, totalCount, paramMap);
			
			String forOrder = request.getParameter("forOrder")==null?"":request.getParameter("forOrder").toString();
			if(!forOrder.equals("")){
				String[] sort = forOrder.split("\\^"); 
				if(sort.length > 1 && !sort[0].equals("") && !sort[1].equals("")){
					paramMap.put("forOrder", sort[0]);
					paramMap.put("order", sort[1]);
					mav.addObject("order",  sort[1]);
				}else {
					paramMap.put("forOrder", "departureDt");
					paramMap.put("order", "asc");
					mav.addObject("order",  "");
				}
			}else {
				paramMap.put("forOrder", "departureDt");
				paramMap.put("order", "asc");
				mav.addObject("order",  "");
			}
			
			List<Map<String, Object>> allocationList = adjustmentService.selectAllocationListByPaymentPartnerIdAndDecideMonth(paramMap);
			Map<String, Object> map = new HashMap<String, Object>();
			
			mav.addObject("userMap",  userMap);
			mav.addObject("listData",  allocationList);
			
			String forOpen = request.getParameter("forOpen")==null?"":request.getParameter("forOpen").toString();
			mav.addObject("forOpen", forOpen);
			mav.addObject("paramMap", paramMap);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
						
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
    
    //현장수금건인데 입금완료가 되지 않았을 경우 Ajax
    @RequestMapping(value = "/update-payment-DD", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updatePaymentDD(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
		
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			
			
			Map<String, Object> map = new HashMap<String, Object>();   
			String allocationIdList = request.getParameter("allocationIdArray").toString();
			String[] allocationIdArr = allocationIdList.split(","); 
			map.put("allocationId_list",allocationIdArr);
			
			
			List<Map<String, Object>> allocationList = allocationService.selectAllocationAppConfirmList(map);
			
			
			for(int i = 0; i < allocationIdArr.length; i++) {
				
				//외주기사 탁송건중 현장수금,현금건에 대해 완료처리를 하고 현장수금건은 입금 처리까지 진행 한다.
				Map<String, Object> allocationIdMap = new HashMap<String, Object>();
				allocationIdMap.put("allocationId", allocationList.get(i).get("allocation_id").toString());
				//allocationIdMap.put("driverId", allocationList.get(i).get("driver_id").toString());
				String allocationId = allocationList.get(i).get("allocation_id").toString();
				Map<String, Object> carInfo = carInfoService.selectCarInfo(allocationIdMap);
				String driverId = carInfo.get("driver_id").toString();
				
				Map<String, Object> allocationMap = allocationService.selectAllocation(allocationIdMap);
				
				if(allocationMap != null) {
				
					// 56line 쿼리에서 미배차,탁송취소,취소대기 목록은 제외 하고 가져오고 있지만 혹시 모르니..... 
					if(allocationMap.get("allocation_status").toString().equals("C") || allocationMap.get("allocation_status").toString().equals("X")){
						//건너뛴다
						continue;			
					}
					//배차건이 탁송 완료 처리가 되어 있지 않은경우 탁송 완료로 변경 해 준다.
					if(!allocationMap.get("allocation_status").toString().equals("F")){
						Map<String, Object> updateMap = new HashMap<String, Object>();
						updateMap.put("allocationId", allocationMap.get("allocation_id").toString());
						updateMap.put("allocationStatus", BaseAppConstants.ALLOCATION_STATUS_FINISH);
						updateMap.put("modId", userMap.get("emp_id").toString()); //수정자
						allocationService.updateAllocationStatus(updateMap);
						
						updateMap.put("logType", "U");
						allocationService.insertAllocationLogByMap(updateMap); //로그기록
						
					}
					
					//완료처리가 되는경우 또는 이미 되어 있는 경우 에도 해당 기사의 결제 정보가 확정 되어 있는지 확인 후 확정이 되어 있지 않은경우  출발월 기준으로 확정 진행 한다.
					
					Map<String, Object> updateMapSub = new HashMap<String, Object>();
					
					//20210209 박태환 대리님 요청 미확정 유지 
					//updateMapSub.put("decideStatus", "Y");
					//updateMapSub.put("decideMonth", processDate.substring(0,7));
					
					updateMapSub.put("decideStatus", "N");
					updateMapSub.put("decideMonth", "");
					updateMapSub.put("allocationId", allocationId);
					updateMapSub.put("driverId", driverId);
					
					List<Map<String, Object>> selectList = paymentInfoService.selectPaymentInfoDecideStatusByPaymentPartnerId(updateMapSub);
					if(selectList != null && selectList.size() > 0) {
						//일반적으로 하나만 있겠지만...... 
						Map<String, Object> decideStatusMap = selectList.get(0);
						//decide_status가 Y가 아니라면 decide_month 는 고려할 필요가 없을것 같다... 
						if(decideStatusMap.get("decide_status") != null && !decideStatusMap.get("decide_status").toString().equals("") && !decideStatusMap.get("decide_status").toString().equals("Y")) {
							updateMapSub.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
							updateMapSub.put("logType","U");
							paymentInfoService.updatePaymentInfoDecideStatusByPaymentPartnerId(updateMapSub);
							paymentInfoService.insertPaymentInfoLogByMap(updateMapSub); //로그기록
						}
					}
					
					
					//매출로 넘어가지 않았고 차변이 입력 되지 않은경우 
					if(allocationMap.get("billing_status").toString().equals("N") && (allocationMap.get("debtor_creditor_id") == null || allocationMap.get("debtor_creditor_id").toString().equals(""))) {
						Map<String, Object> selectMap = new HashMap<String, Object>();
						selectMap.put("allocationId", allocationMap.get("allocation_id").toString());
						
						List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(selectMap);
						
						if(paymentInfoList != null && paymentInfoList.size() > 0){
							for(int k = 0; k < paymentInfoList.size(); k++) {
								Map<String, Object> paymentInfo = paymentInfoList.get(k);
								if(paymentInfo.get("payment_division").toString().equals("01")) {		//매출처 
									if(paymentInfo.get("payment_kind") != null) {
										
										//현장수금인경우 현금건인경우는 해당 배차를 확정 하고 차변에 입력 한다.
										String debtorCreditorIdForD = "DCI"+UUID.randomUUID().toString().replaceAll("-","");
										if((paymentInfo.get("payment_kind").toString().equals("DD") || paymentInfo.get("payment_kind").toString().equals("CA")) ) {
										
											//현장 수금건 탁송 완료시 해당 건에 대해 확정 및 매출로 넘기고 ....
											String billPublishRequestId = "BPR"+UUID.randomUUID().toString().replaceAll("-","");
											Map<String, Object> carInfoUpdateMap = new HashMap<String, Object>();
											carInfoUpdateMap.put("allocationId", allocationMap.get("allocation_id").toString());
											carInfoUpdateMap.put("billPublishRequestId", billPublishRequestId);
											carInfoUpdateMap.put("decideStatus", "Y");
											carInfoUpdateMap.put("billingStatus", "D");
											carInfoUpdateMap.put("debtorCreditorId", debtorCreditorIdForD);
											adjustmentService.updateCarInfoForBillPublish(carInfoUpdateMap);
											//차변에 입력--------------------------------------------------------------------------
											//세금계산서 미 발행건에 대해서 각 건별로 차변에 등록한다.
											DebtorCreditorVO debtorCreditorVO = new DebtorCreditorVO();	
											debtorCreditorVO.setCustomerId(allocationMap.get("customer_id").toString());
											debtorCreditorVO.setDebtorCreditorId(debtorCreditorIdForD);
											debtorCreditorVO.setOccurrenceDt(allocationMap.get("departure_dt").toString());
											debtorCreditorVO.setSummary("운송료 ("+allocationMap.get("departure").toString()+"-"+allocationMap.get("arrival").toString()+")");
											debtorCreditorVO.setTotalVal(allocationMap.get("sales_total").toString());
											
											debtorCreditorVO.setPublishYn("N");			//미발행건
											debtorCreditorVO.setType(BaseAppConstants.DEBTOR_CREDITOR_TYPE_D);
											debtorCreditorVO.setRegisterId("AUTO");
											debtorCreditorVO.setEtc("");
											debtorCreditorVO.setCompanyId("AUTO");
											debtorCreditorService.insertDebtorCreditor(debtorCreditorVO);
											
										}
										
										
										
									}
								}
								
							}
							
						}
					
						
					}else {
					//매출로 넘어갔거나 차변이 입력 되어 있는경우는 아무것도 하지 않는다.
						
				
						
						
					}
					
					
					
					Map<String, Object> selectMap = new HashMap<String, Object>();
					selectMap.put("allocationId", allocationMap.get("allocation_id").toString());
					
					List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(selectMap);
					
					if(paymentInfoList != null && paymentInfoList.size() > 0){
						for(int k = 0; k < paymentInfoList.size(); k++) {
							Map<String, Object> paymentInfo = paymentInfoList.get(k);
							if(paymentInfo.get("payment_division").toString().equals("01")) {		//매출처 
								if(paymentInfo.get("payment_kind") != null) {
									
									
									if(paymentInfo.get("payment_kind").toString().equals("DD")) {		//결제 방법이 기사 수금이면
										
										Map<String, Object> paymentUpdateMap = new HashMap<String, Object>();
										
										paymentUpdateMap.put("payment", "Y");
										paymentUpdateMap.put("paymentDt", allocationMap.get("departure_dt").toString());
										paymentUpdateMap.put("allocationId", allocationMap.get("allocation_id").toString());
										paymentUpdateMap.put("idx", paymentInfo.get("idx").toString());
										paymentUpdateMap.put("modId","AutoProcessScheduler"); //수정자
										//결제 여부 업데이트
										//기존에 결제 정보가 이미 작성 되어 있는경우에는 하지 않는다.
										if((paymentInfo.get("payment") == null || paymentInfo.get("payment").toString().equals("N")) && (paymentInfo.get("payment_dt") == null || paymentInfo.get("payment_dt").toString().equals(""))) {
											paymentInfoService.updatePaymentInfoForDDFinish(paymentUpdateMap);
											paymentUpdateMap.put("logType", "U");
											paymentInfoService.insertPaymentInfoLogByMap(paymentUpdateMap); //로그기록
										}
										
										//allocationMap으로 비교하거나 조건으로 사용 하지 않으니... 다시 select해도.....
										allocationMap = allocationService.selectAllocation(allocationIdMap);
										
										//차변이 무조건 있을테니 차변 아이디를 select 해 와서...
										String debtorCreditorIdForD = allocationMap.get("debtor_creditor_id").toString();
										
										//대변 입력(대변에 입력 되어 있우에는 다시 입력 하지 않는다...)
										if(paymentInfo.get("debtor_creditor_id") == null || paymentInfo.get("debtor_creditor_id").toString().equals("")) {
									
											String debtorCreditorId = "DCI"+UUID.randomUUID().toString().replaceAll("-","");
											DebtorCreditorVO debtorCreditorVO = new DebtorCreditorVO();
											debtorCreditorVO.setCustomerId(allocationMap.get("customer_id").toString());
											debtorCreditorVO.setDebtorCreditorId(debtorCreditorId);
											debtorCreditorVO.setOccurrenceDt(allocationMap.get("departure_dt").toString());
											paymentUpdateMap.put("debtorCreditorId", debtorCreditorId);
																			
											String summaryDt = ""; 
											summaryDt ="운송료 ("+allocationMap.get("departure").toString()+" - "+allocationMap.get("arrival").toString()+" 입금)"; 
											debtorCreditorVO.setSummary(summaryDt);
											debtorCreditorVO.setTotalVal(allocationMap.get("sales_total").toString());
											debtorCreditorVO.setPublishYn("N");
											debtorCreditorVO.setType(BaseAppConstants.DEBTOR_CREDITOR_TYPE_C);
											debtorCreditorVO.setRegisterId("driver");
											debtorCreditorVO.setEtc("");
											debtorCreditorVO.setCompanyId("AUTO");
											debtorCreditorVO.setDebtorId(debtorCreditorIdForD);
											//대변에 입력 하고 
											debtorCreditorService.insertDebtorCreditor(debtorCreditorVO);	
											paymentInfoService.updatePaymentInfoDebtorCreditorId(paymentUpdateMap);
											paymentUpdateMap.put("logType", "U");
											paymentInfoService.insertPaymentInfoLogByMap(paymentUpdateMap); //로그기록
										}
										
									}
									
									
									
								}
							
							}else if(paymentInfo.get("payment_division").toString().equals("03")) {
								
								if(paymentInfo.get("payment_kind") != null) {
									if(paymentInfo.get("payment_kind").toString().equals("DD")) {
										//매입처 결제 여부 업데이트
										//기존에 결제 정보가 이미 작성 되어 있는경우에는 하지 않는다.
										if((paymentInfo.get("payment") == null || paymentInfo.get("payment").toString().equals("N")) && (paymentInfo.get("payment_dt") == null || paymentInfo.get("payment_dt").toString().equals(""))) {
											
											Map<String, Object> paymentUpdateMap = new HashMap<String, Object>();
											paymentUpdateMap.put("payment", "Y");
											paymentUpdateMap.put("paymentDt", allocationMap.get("departure_dt").toString());
											paymentUpdateMap.put("allocationId", allocationMap.get("allocation_id").toString());
											paymentUpdateMap.put("idx", paymentInfo.get("idx").toString());
											paymentInfoService.updatePaymentInfoForDDFinish(paymentUpdateMap);
											paymentUpdateMap.put("logType", "U");
											paymentInfoService.insertPaymentInfoLogByMap(paymentUpdateMap); //로그기록
										}		
										
									}
									
								}
								
							}
							
						}
						
					
					}
					
				}
				
			}
			
			
			
			
				
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
    
    
    
    
    
    
    //미수 미지급 관리
    @RequestMapping(value = "/receivable", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView receivable(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
    
    	 
    	  try {
	            
	            HttpSession  session  = request.getSession();
	            Map userMap = (Map)session.getAttribute("user");
	            
	            String searchType = request.getParameter("searchType") == null || request.getParameter("searchType").toString().equals("") ? "date" : request.getParameter("searchType").toString();
	            String searchWord = request.getParameter("searchWord") == null || request.getParameter("searchWord").toString().equals("") ? WebUtils.getNow("yyyy-MM-dd")  : request.getParameter("searchWord").toString();
	            String yearType = request.getParameter("yearType") == null || request.getParameter("yearType").toString().equals("")?"": request.getParameter("yearType").toString();
	            String quarterType= request.getParameter("quarterType") == null || request.getParameter("quarterType").toString().equals("") ? "0" : request.getParameter("quarterType").toString();
	            String companyType =request.getParameter("companyType") == null ? "" :request.getParameter("companyType").toString();
	            String unpaid =request.getParameter("unpaid") == null || request.getParameter("unpaid").toString().equals("") ? "" : request.getParameter("unpaid").toString();
	       
	            Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
					
				if(paramMap.get("searchType") == null){
			         paramMap.put("searchType", searchType);
			         paramMap.put("searchWord", searchWord);
			         paramMap.put("companyType",companyType);
				}
				
				 Map<String,Object> map = new HashMap<String, Object>();
				 List<Map<String,Object>> yearMap = accountService.selectForYear(map);
				 
	
				 List<String> quarterTypeList = new ArrayList<String>();
	            
	    		if(quarterType !=null && !quarterType.toString().equals("0") ) {
					
					searchWord ="";
	            	searchType ="";
					
				} if(quarterType.equals("5")) {
	            	quarterTypeList.add("1");
	            	quarterTypeList.add("2");
	            	
	            }else if(quarterType.equals("6")) {
	            	quarterTypeList.add("3");
	            	quarterTypeList.add("4");
	            }else {
	            	quarterTypeList.add(quarterType);
	            }
	            
	            paramMap.put("searchWord", searchWord);
	            paramMap.put("searchType", searchType);
	            paramMap.put("quarterType",quarterType);	
	            paramMap.put("quarterTypeList",quarterTypeList);
	            paramMap.put("yearType",yearType);
	            paramMap.put("companyType",companyType);
	            paramMap.put("unpaid",unpaid);
	         
	            	
	            int totalCount=accountService.selectRecivableCount(paramMap);
				PagingUtils.setPageing(request, totalCount, paramMap);
				
	
	            List<Map<String,Object>>summaryList =accountService.selectRecivableList(paramMap);
	       
	            mav.addObject("paramMap",paramMap);
	            mav.addObject("yearType",yearType);
	            mav.addObject("list",summaryList);
	            mav.addObject("yearList",yearMap);
	            
	            
	         }catch (Exception e) {
	            e.printStackTrace();
	         }
	         
	         return mav;
	      }

    
    //미수 미지급관리 상세 보기페이지 
    @RequestMapping(value = "/receivable_list", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView receivableList(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
    
    	 
    	  try {
	            
	            HttpSession  session  = request.getSession();
	            Map userMap = (Map)session.getAttribute("user");
	            
	            String searchType = request.getParameter("searchType") == null || request.getParameter("searchType").toString().equals("") ? "date" : request.getParameter("searchType").toString();
	            String searchWord = request.getParameter("searchWord") == null || request.getParameter("searchWord").toString().equals("") ? WebUtils.getNow("yyyy-MM-dd")  : request.getParameter("searchWord").toString();
	            String yearType = request.getParameter("yearType") == null || request.getParameter("yearType").toString().equals("")?"": request.getParameter("yearType").toString();
	            String quarterType= request.getParameter("quarterType") == null || request.getParameter("quarterType").toString().equals("") ? "0" : request.getParameter("quarterType").toString();
	            String companyType =request.getParameter("companyType") == null ? "" :request.getParameter("companyType").toString();
	            String unpaid =request.getParameter("unpaid") == null || request.getParameter("unpaid").toString().equals("") ? "" : request.getParameter("unpaid").toString();
	            String customerId =request.getParameter("customerId") == null || request.getParameter("customerId").toString().equals("") ? "" : request.getParameter("customerId").toString();
	      
	            Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
					
				if(paramMap.get("searchType") == null){
			         paramMap.put("searchType", searchType);
			         paramMap.put("searchWord", searchWord);
			         paramMap.put("companyType",companyType);
				}
				
				 Map<String,Object> map = new HashMap<String, Object>();
				 List<Map<String,Object>> yearMap = accountService.selectForYear(map);
				 
	
				 List<String> quarterTypeList = new ArrayList<String>();
	            
	    		if(quarterType !=null && !quarterType.toString().equals("0") ) {
					
					searchWord ="";
	            	searchType ="";
					
				} if(quarterType.equals("5")) {
	            	quarterTypeList.add("1");
	            	quarterTypeList.add("2");
	            	
	            }else if(quarterType.equals("6")) {
	            	quarterTypeList.add("3");
	            	quarterTypeList.add("4");
	            }else {
	            	quarterTypeList.add(quarterType);
	            }
	            
	            paramMap.put("searchWord", searchWord);
	            paramMap.put("searchType", searchType);
	            paramMap.put("quarterType",quarterType);	
	            paramMap.put("quarterTypeList",quarterTypeList);
	            paramMap.put("yearType",yearType);
	            paramMap.put("companyType",companyType);
	            paramMap.put("unpaid",unpaid);
	            paramMap.put("customerId",customerId);
	            
	            	
	            int totalCount=accountService.selectRecivableDetailCount(paramMap);
	            //int totalCount=accountService.selectRecivableCount(paramMap);
	            PagingUtils.setPageing(request, totalCount, paramMap);
				
				 List<Map<String,Object>>summaryList =accountService.selectRecivableDetail(paramMap);
				//List<Map<String,Object>>summaryList =accountService.selectRecivableList(paramMap);
				
				//List<Map<String,Object>> employeelist = accountService.selectEmployeeBatchDetail(paramMap);
				
	            mav.addObject("paramMap",paramMap);
	            mav.addObject("yearType",yearType);
	            mav.addObject("list",summaryList);
	            mav.addObject("yearList",yearMap);
	            
	            
	         }catch (Exception e) {
	            e.printStackTrace();
	         }
	         
	         return mav;
	      }
    
  //기사별 공제내역 목록
    @RequestMapping(value = "/driverCal", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView driverCal(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			String companyId = session.getAttribute("companyId").toString();


			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			String selectMonth = request.getParameter("selectMonth") == null || request.getParameter("selectMonth").toString().equals("")  ? WebUtils.getPrevMonth() : request.getParameter("selectMonth").toString();
			String startDt = "";
			String endDt = "";
			
			startDt = selectMonth;
			startDt = startDt.split("-")[0]+"-"+startDt.split("-")[1]+"-01";
			endDt = selectMonth;
			endDt = startDt.split("-")[0]+"-"+startDt.split("-")[1]+"-"+WebUtils.getLastDate("yyyy-mm-dd",Integer.parseInt(startDt.split("-")[0]),Integer.parseInt(startDt.split("-")[1]));
			paramMap.put("startDt", startDt);
			paramMap.put("endDt", endDt);
			paramMap.put("selectMonth", selectMonth);
			//paramMap.put("companyId", companyId);
			
			int totalCount = driverDeductInfoService.selectDriverDeductInfoListCount(paramMap);
			PagingUtils.setPageing(request, totalCount, paramMap);
			
			List<Map<String, Object>> driverList = driverDeductInfoService.selectDriverDeductInfoList(paramMap);
			Map<String, Object> driver = driverService.selectDriver(paramMap);
			
			mav.addObject("userSessionMap",  userSessionMap);
			mav.addObject("listData",  driverList);
			mav.addObject("paramMap", paramMap);
			mav.addObject("driver", driver);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
						
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
    
    
    
    
    @RequestMapping(value = "/insert-driver-deduct", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi insertDriverDeduct(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
		
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			String companyId = session.getAttribute("companyId").toString();
			
			
			String deductInfo = request.getParameter("deductInfo").toString();
			String driverId = request.getParameter("driverId").toString();
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("driverId",driverId);
			Map<String, Object> driver = driverService.selectDriver(map);
			
			JSONParser jsonParser = new JSONParser();
			JSONObject jsonObject = (JSONObject) jsonParser.parse(deductInfo);
			JSONObject driverDeductInfo = (JSONObject) jsonObject.get("deductInfo");	
			
			String occurrenceDt = driverDeductInfo.get("occurrence_dt").toString();
			String[] occurrenceDtArr = occurrenceDt.split("-");
			String year = occurrenceDtArr[0];
			String month = occurrenceDtArr[1];
			String date = occurrenceDtArr[2];
			String lastDate = WebUtils.getLastDate("yyyy-mm-dd",Integer.parseInt(year),Integer.parseInt(month));
			
			String now = WebUtils.getNow("yyyy-mm-dd");
			String nYear = now.split("-")[0];
			
			//if(Integer.parseInt(year) > Integer.parseInt(nYear) || Integer.parseInt(month) <= 0 || Integer.parseInt(month) > 12 || Integer.parseInt(date) > Integer.parseInt(lastDate)) {
			if(Integer.parseInt(month) <= 0 || Integer.parseInt(month) > 12 || Integer.parseInt(date) > Integer.parseInt(lastDate)) {
				result.setResultCode("E000");
			}else {
				DriverDeductInfoVO driverDeductInfoVO = new DriverDeductInfoVO();
				driverDeductInfoVO.setAmount(driverDeductInfo.get("amount").toString());
				driverDeductInfoVO.setDeductInfoId("DDI"+UUID.randomUUID().toString().replaceAll("-",""));
				driverDeductInfoVO.setDivision(driverDeductInfo.get("division").toString());
				driverDeductInfoVO.setDriverId(driver.get("driver_id").toString());
				driverDeductInfoVO.setDriverName(driver.get("driver_name").toString());
				driverDeductInfoVO.setEtc(URLDecoder.decode(driverDeductInfo.get("etc").toString(), "UTF-8"));
				driverDeductInfoVO.setItem(driverDeductInfo.get("item").toString());
				driverDeductInfoVO.setOccurrenceDt(driverDeductInfo.get("occurrence_dt").toString());
				driverDeductInfoVO.setRegisterId(userMap.get("emp_id").toString());
				driverDeductInfoVO.setRegisterName(userMap.get("emp_name").toString());
				driverDeductInfoService.insertDriverDeductInfo(driverDeductInfoVO);	
			}
				
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
    
    
    
    @RequestMapping(value = "/update-driver-deduct", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updateDriverDeduct(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
			
		ResultApi result = new ResultApi();
		try{
		
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			String companyId = session.getAttribute("companyId").toString();
			
			
			String deductInfo = request.getParameter("deductInfo").toString();
			String driverId = request.getParameter("driverId").toString();
			String deductInfoId = request.getParameter("deductInfoId").toString();
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("driverId",driverId);
			Map<String, Object> driver = driverService.selectDriver(map);
			
			JSONParser jsonParser = new JSONParser();
			JSONObject jsonObject = (JSONObject) jsonParser.parse(deductInfo);
			JSONObject driverDeductInfo = (JSONObject) jsonObject.get("deductInfo");	

			DriverDeductInfoVO driverDeductInfoVO = new DriverDeductInfoVO();
			driverDeductInfoVO.setAmount(driverDeductInfo.get("amount").toString());
			driverDeductInfoVO.setDeductInfoId(deductInfoId);
			driverDeductInfoVO.setDivision(driverDeductInfo.get("division").toString());
			driverDeductInfoVO.setEtc(URLDecoder.decode(driverDeductInfo.get("etc").toString(), "UTF-8"));
			driverDeductInfoVO.setItem(driverDeductInfo.get("item").toString());
			driverDeductInfoVO.setOccurrenceDt(driverDeductInfo.get("occurrence_dt").toString());
			driverDeductInfoService.updateDriverDeductInfo(driverDeductInfoVO);
				
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
    
    @RequestMapping(value = "/delete-driver-deduct", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi deleteDriverDeduct(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
		
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			String companyId = session.getAttribute("companyId").toString();
			
			
			String driverId = request.getParameter("driverId").toString();
			String deductInfoId = request.getParameter("deductInfoId").toString();
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("driverId",driverId);
			map.put("deductInfoId",deductInfoId);
			driverDeductInfoService.deleteDriverDeductInfo(map);
				
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
    
    
    
    
    
    
    
    
    //매출관리 거래처 리스트
	@RequestMapping(value = "/customerList", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView customer(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			HttpSession session = request.getSession();
			String companyId = session.getAttribute("companyId").toString();
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			String searchType = request.getParameter("searchType") == null  ? "" : request.getParameter("searchType").toString();
			String searchWord = request.getParameter("searchWord") == null  ? "" : request.getParameter("searchWord").toString();
			
			paramMap.put("searchType", searchType);
			paramMap.put("searchWord", searchWord);
			//paramMap.put("companyId", companyId);
			
			int totalCount = totalSalesService.selectCustomerListCount(paramMap);
			PagingUtils.setPageing(request, totalCount, paramMap);
			
			List<Map<String, Object>> customerList = totalSalesService.selectCustomerList(paramMap);
			mav.addObject("listData",  customerList);
			mav.addObject("paramMap", paramMap);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
						
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
    
	
	//회계관리 세금계산서발행요청
	@RequestMapping(value = "/billRequest", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView billRequest(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			HttpSession session = request.getSession();
			String companyId = session.getAttribute("companyId").toString();
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			String searchType = request.getParameter("searchType") == null  ? "" : request.getParameter("searchType").toString();
			String searchWord = request.getParameter("searchWord") == null  ? "" : request.getParameter("searchWord").toString();
			String companyType = request.getParameter("companyType") == null  ? "" : request.getParameter("companyType").toString();
			
			paramMap.put("searchType", searchType);
			paramMap.put("searchWord", searchWord);
			paramMap.put("publishRequestStatus", "R");
			//paramMap.put("companyId", companyId);
			/* paramMap.put("companyType", companyType); */
			
			int totalCount = billPublishRequestService.selectBillPublishRequestListCount(paramMap);
			PagingUtils.setPageing(request, totalCount, paramMap);
			
			List<Map<String, Object>> billPublishRequestList = billPublishRequestService.selectBillPublishRequestList(paramMap);
			mav.addObject("listData",  billPublishRequestList);
			mav.addObject("paramMap", paramMap);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
			
			Map<String, Object> selectBillPublishRequestStatistics = billPublishRequestService.selectBillPublishRequestStatistics(paramMap);
			mav.addObject("selectBillPublishRequestStatistics",  selectBillPublishRequestStatistics);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	//회계관리 세금계산서 발행 리스트
	@RequestMapping(value = "/billRequestList", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView billRequestList(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			HttpSession session = request.getSession();
			String companyId = session.getAttribute("companyId").toString();
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			String searchType = request.getParameter("searchType") == null  ? "" : request.getParameter("searchType").toString();
			String searchWord = request.getParameter("searchWord") == null  ? "" : request.getParameter("searchWord").toString();
			String companyType = request.getParameter("companyType") == null  ? "" : request.getParameter("companyType").toString();
			String publisher_dt = request.getParameter("publisher_dt") == null ? "" : request.getParameter("publisher_dt").toString();
			
			paramMap.put("searchType", searchType);
			paramMap.put("searchWord", searchWord);
			paramMap.put("publishRequestStatus", "Y");
			paramMap.put("publisher_dt", "Y");
			
			//paramMap.put("companyId", companyId);
			/* paramMap.put("companyType", companyType); */
			
			int totalCount = billPublishRequestService.selectBillPublishRequestIssueListCount(paramMap);
			PagingUtils.setPageing(request, totalCount, paramMap);
			
			List<Map<String, Object>> billPublishRequestList = billPublishRequestService.selectBillPublishRequestIssueList(paramMap);
			mav.addObject("listData",  billPublishRequestList);
			mav.addObject("paramMap", paramMap);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
			
			Map<String, Object> selectBillPublishRequestStatistics = billPublishRequestService.selectBillPublishRequestStatistics(paramMap);
			mav.addObject("selectBillPublishRequestStatistics",  selectBillPublishRequestStatistics);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	//회계관리 세금계산서발행요청 임시로 모든 발행 요청건을 볼 수 있도록 함
	@RequestMapping(value = "/sales", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView sales(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			HttpSession session = request.getSession();
			String companyId = session.getAttribute("companyId").toString();
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			String searchType = request.getParameter("searchType") == null  ? "00" : request.getParameter("searchType").toString();
			String searchWord = request.getParameter("searchWord") == null  ? "" : request.getParameter("searchWord").toString();
			String companyType = request.getParameter("companyType") == null ? "" :request.getParameter("companyType").toString();
			
			//String startDt = request.getParameter("startDt") == null  ? WebUtils.getNow("yyyy")+"-01-01" : request.getParameter("startDt").toString();
			//WebUtils.getPrevYear()+
			String startDt = request.getParameter("startDt") == null  ? "2019-01-01" : request.getParameter("startDt").toString();
			String endDt = request.getParameter("endDt") == null  ? WebUtils.getNow("yyyy")+"-12-31" : request.getParameter("endDt").toString();
			
			String startYear= startDt.split("-")[0];
		
			String endYear = endDt.split("-")[0];
			String startMonth = startDt.split("-")[1];
			String endMonth = endDt.split("-")[1];
			String startDate = startDt.split("-")[2];
			String endDate = endDt.split("-")[2];
			
			paramMap.put("startYear", startYear);
			paramMap.put("endYear", endYear);
			paramMap.put("startMonth", startMonth);
			paramMap.put("endMonth", endMonth);
			paramMap.put("startDate", startDate);
			paramMap.put("endDate", endDate);
			paramMap.put("startDt", startDt);
			paramMap.put("endDt", endDt);
			paramMap.put("searchType", searchType);
			paramMap.put("searchWord", searchWord);
			/* paramMap.put("companyId", companyId); */
			paramMap.put("companyType", companyType);
			
			
			
			paramMap.put("publishRequestStatus", "Y");
			
			//int totalCount = billPublishRequestService.selectCustomerListCount(paramMap);
			int totalCount = debtorCreditorService.selectCustomerListCount(paramMap);
			
			PagingUtils.setPageing(request, totalCount, paramMap);
			
			//List<Map<String, Object>> billPublishRequestList = billPublishRequestService.selectCustomerList(paramMap);
			List<Map<String, Object>> billPublishRequestList = debtorCreditorService.selectCustomerList(paramMap);
			
			mav.addObject("listData",  billPublishRequestList);
			mav.addObject("paramMap", paramMap);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
					
			Map<String, Object> selectBillPublishRequestStatistics = billPublishRequestService.selectBillPublishRequestStatistics(paramMap);
			mav.addObject("selectBillPublishRequestStatistics",  selectBillPublishRequestStatistics);
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	
		@RequestMapping(value = "/finish_sales", method = {RequestMethod.POST,RequestMethod.GET})
		public ModelAndView finishSales(Locale locale,ModelAndView mav, HttpServletRequest request, 
				HttpServletResponse response) throws Exception {
			
			try{
				
				HttpSession session = request.getSession(); 
				Map userMap = (Map)session.getAttribute("user");
				
				String companyId = session.getAttribute("companyId").toString();
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				String searchType = request.getParameter("searchType") == null  ? "" : request.getParameter("searchType").toString();
				String searchWord = request.getParameter("searchWord") == null  ? "" : request.getParameter("searchWord").toString();
				
				//선택된 달이 있으면 선택한 달을, 그렇지 않으면 이전월의 마감분을 가져온다....
				String selectMonth = request.getParameter("selectMonth") != null && !request.getParameter("selectMonth").toString().equals("") ? request.getParameter("selectMonth").toString() :WebUtils.getPrevMonth();
		
				paramMap.put("searchType", searchType);
				paramMap.put("searchWord", searchWord);
				paramMap.put("selectMonth", selectMonth);
				//paramMap.put("companyId", companyId);
				
				int totalCount = adjustmentService.selectFinishSalesListCount(paramMap);
				
				PagingUtils.setPageing(request, totalCount, paramMap);
				
				List<Map<String, Object>> anotherList = adjustmentService.selectFinishSalesList(paramMap);
				mav.addObject("listData",  anotherList);
				mav.addObject("paramMap", paramMap);
				mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
				mav.addObject("totalCount", totalCount);
				
				
				
			}catch(Exception e){
				e.printStackTrace();
			}

			return mav;

}
	
		
		@RequestMapping( value="/finish_sales_download", method = RequestMethod.GET )
		public ModelAndView finishSalesDownload(ModelAndView mav,
				HttpServletRequest request ,HttpServletResponse response) throws Exception {
			
			ModelAndView view = new ModelAndView();
			
			try{
				
				String varNameList[] = null;
				String titleNameList[] = null;
				
				HttpSession session = request.getSession(); 
				Map userMap = (Map)session.getAttribute("user");
				String companyId = session.getAttribute("companyId").toString();
				
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				String searchType = request.getParameter("searchType") == null  ? "" : request.getParameter("searchType").toString();
				String searchWord = request.getParameter("searchWord") == null  ? "" : request.getParameter("searchWord").toString();
				
				//선택된 달이 있으면 선택한 달을, 그렇지 않으면 이전월의 마감분을 가져온다....
				String selectMonth = request.getParameter("selectMonth") != null && !request.getParameter("selectMonth").toString().equals("") ? request.getParameter("selectMonth").toString() :WebUtils.getPrevMonth();
		
				paramMap.put("searchType", searchType);
				paramMap.put("searchWord", searchWord);
				paramMap.put("selectMonth", selectMonth);
				//paramMap.put("companyId", companyId);
				
				int totalCount = adjustmentService.selectFinishSalesListCount(paramMap);
				
				paramMap.put("startRownum", 0);
				paramMap.put("numOfRows", totalCount);
				
				List<Map<String, Object>> anotherList = adjustmentService.selectFinishSalesList(paramMap);
				
				String today = WebUtils.getNow("yyyy-MM-dd");
				
				
				titleNameList = new String[]{"장비구분","운행구분","출발일","출발시간","업체","담당자명","차종","차대번호","차량번호","계약번호","출발지","도착지","기사유형","업무구분","매입처명","적용요율","발행여부","청구액","지급액","탁송상태"};
				varNameList = new String[]{"allocation_division","run_division","departure_dt","departure_time","customer_name","charge_name","car_kind","car_id_num","car_num","contract_num","departure","arrival","driver_kind","work_kind","payment_partner","deduction_rate","publish","price","driver_amount","allocation_status_name"};
				
				view.setViewName("excelDownloadView");
				view.addObject("list", anotherList);
				view.addObject("varNameList", varNameList);
				view.addObject("titleNameList", titleNameList);
				view.addObject("excelName", selectMonth+"월 마감"+today+".xlsx");
			}catch(Exception e){
				e.printStackTrace();
			}
			
			return view;
			
		}
		
		
		
		
		
		
	//회계관리 세금계산서발행요청 임시로 모든 발행 요청건을 볼 수 있도록 함
		@RequestMapping(value = "/customerSales", method = {RequestMethod.POST,RequestMethod.GET})
		public ModelAndView customerSales(Locale locale,ModelAndView mav, HttpServletRequest request, 
				HttpServletResponse response) throws Exception {
			
			try{
				
				HttpSession session = request.getSession(); 
				Map userMap = (Map)session.getAttribute("user");
				String companyId = session.getAttribute("companyId").toString();
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				String searchType = request.getParameter("searchType") == null  ? "00" : request.getParameter("searchType").toString();
				String searchWord = request.getParameter("searchWord") == null  ? "" : request.getParameter("searchWord").toString();
								
				String companyType = request.getParameter("companyType") == null ? "": request.getParameter("companyType").toString();
				String startDt = request.getParameter("startDt") == null  ? WebUtils.getNow("yyyy")+"-01-01" : request.getParameter("startDt").toString();
				String endDt = request.getParameter("endDt") == null  ? WebUtils.getNow("yyyy")+"-12-31" : request.getParameter("endDt").toString();
				
				String startYear = startDt.split("-")[0];
				String endYear = endDt.split("-")[0];
				String startMonth = startDt.split("-")[1];
				String endMonth = endDt.split("-")[1];
				String startDate = startDt.split("-")[2];
				String endDate = endDt.split("-")[2];
				
				paramMap.put("startYear", startYear);
				paramMap.put("endYear", endYear);
				paramMap.put("startMonth", startMonth);
				paramMap.put("endMonth", endMonth);
				paramMap.put("startDate", startDate);
				paramMap.put("endDate", endDate);
				paramMap.put("startDt", startDt);
				paramMap.put("endDt", endDt);
				paramMap.put("searchType", searchType);
				paramMap.put("searchWord", searchWord);
				paramMap.put("publishRequestStatus", "Y");
				//paramMap.put("companyId", companyId);
				paramMap.put("companyType", companyType);
				
				
				Map<String, Object> map = new HashMap<String, Object>();
				Map<String, Object> customer = new HashMap<String, Object>();
				if(paramMap.get("customerId") != null){
					map.put("customerId", paramMap.get("customerId").toString());
					customer = customerService.selectCustomer(map);
					paramMap.put("searchWord", customer.get("customer_name").toString());
				}else {
					map.put("customerName", paramMap.get("searchWord").toString().trim());
					customer = customerService.selectCustomerByCustomerName(map);
					if(customer != null) {
						paramMap.put("searchWord", customer.get("customer_name").toString());	
					}else {
						paramMap.put("searchWord", "");
					}
				}
				
				if(customer != null) {
					paramMap.put("customerId", customer.get("customer_id").toString());
				}else {
					
				}
				//List<Map<String, Object>> billPublishRequestList = billPublishRequestService.selectBillPublishRequestListByCustomerId(paramMap);
				List<Map<String, Object>> billPublishRequestList = debtorCreditorService.selectDebtorCreditorListByCustomerId(paramMap);
				
				
				mav.addObject("listData",  billPublishRequestList);
				mav.addObject("paramMap", paramMap);
				mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
				
				
				//Map<String, Object> selectBillPublishRequestStatistics = billPublishRequestService.selectBillPublishRequestStatistics(paramMap);
				//mav.addObject("selectBillPublishRequestStatistics",  selectBillPublishRequestStatistics);
			}catch(Exception e){
				e.printStackTrace();
			}
			

			return mav;
		}
	
	
	
	
	@RequestMapping(value = "/purchase", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView purchase(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			String companyId = session.getAttribute("companyId").toString();
			
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			String startDt = request.getParameter("startDt") == null ? "" : request.getParameter("startDt").toString();
			String endDt = request.getParameter("endDt") == null ? "" : request.getParameter("endDt").toString();
			String searchDateType = request.getParameter("searchDateType") == null ? "D" : request.getParameter("searchDateType").toString();
			
			String carrierType = request.getParameter("carrierType") == null ? "A" : request.getParameter("carrierType").toString();
			String searchType = request.getParameter("searchType") == null  ? "" : request.getParameter("searchType").toString();
			String searchWord = request.getParameter("searchWord") == null  ? "" : request.getParameter("searchWord").toString();
			String includeZero = request.getParameter("includeZero") != null && !request.getParameter("includeZero").toString().equals("") ? request.getParameter("includeZero").toString() :"N";
			
			String decideStatus = request.getParameter("decideStatus") != null && !request.getParameter("decideStatus").toString().equals("") ? request.getParameter("decideStatus").toString() :"Y";
			String decideMonth = request.getParameter("decideMonth") != null && !request.getParameter("decideMonth").toString().equals("") ? request.getParameter("decideMonth").toString() :"";
			
			
			String forOrder = request.getParameter("forOrder")==null?"":request.getParameter("forOrder").toString();
			if(!forOrder.equals("")){
				String[] sort = forOrder.split("\\^"); 
				if(sort.length > 1 && !sort[0].equals("") && !sort[1].equals("")){
					paramMap.put("forOrder", sort[0]);
					paramMap.put("order", sort[1]);
					mav.addObject("order",  sort[1]);
				}else {
					paramMap.put("forOrder", "paymentPartner");
					paramMap.put("order", "asc");
					mav.addObject("order",  "");
				}
			}else {
				paramMap.put("forOrder", "paymentPartner");
				paramMap.put("order", "asc");
				mav.addObject("order",  "");
			}
			
			paramMap.put("carrierType", carrierType);
			paramMap.put("searchType", searchType);
			paramMap.put("searchWord", searchWord);
			paramMap.put("startDt", startDt);
			paramMap.put("endDt", endDt);
			paramMap.put("searchDateType", searchDateType);
			paramMap.put("includeZero", includeZero);
			paramMap.put("decideStatus", decideStatus);
			paramMap.put("decideFinal", "Y");
			paramMap.put("decideMonth", decideMonth);
			//paramMap.put("companyId", companyId);
			
			
			int totalCount = adjustmentService.selectFinalListCount(paramMap);
			
			PagingUtils.setPageing(request, totalCount, paramMap);
			
			List<Map<String, Object>> anotherList = adjustmentService.selectFinalList(paramMap);
			mav.addObject("listData",  anotherList);
			mav.addObject("paramMap", paramMap);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
			mav.addObject("totalCount", totalCount);
			
			Map<String, Object> totalStatistics = adjustmentService.selectAnotherTotalStatistics(paramMap);
			mav.addObject("totalStatistics", totalStatistics);
			
			if(decideStatus.equals("A")) {
				paramMap.put("decideStatus", "N");
				Map<String, Object> totalStatisticsN = adjustmentService.selectAnotherTotalStatistics(paramMap);
				mav.addObject("totalStatisticsN", totalStatisticsN);
				paramMap.put("decideStatus", "Y");
				Map<String, Object> totalStatisticsY = adjustmentService.selectAnotherTotalStatistics(paramMap);
				mav.addObject("totalStatisticsY", totalStatisticsY);
				paramMap.put("decideStatus", "A");
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}

		return mav;
	}
	
	
	@RequestMapping(value = "/purchase_new", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView purchaseNew(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			// String companyId = session.getAttribute("companyId").toString();
			 
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			//String startDt = request.getParameter("startDt") == null ? "" : request.getParameter("startDt").toString();
			//String endDt = request.getParameter("endDt") == null ? "" : request.getParameter("endDt").toString();
			
					
			String searchType = request.getParameter("searchType") == null  ? "00" : request.getParameter("searchType").toString();
			String searchWord = request.getParameter("searchWord") == null  ? "" : request.getParameter("searchWord").toString();
			String companyType = request.getParameter("companyType") == null ? "" :request.getParameter("companyType").toString();
			
			//String decideStatus = request.getParameter("decideStatus") != null && !request.getParameter("decideStatus").toString().equals("") ? request.getParameter("decideStatus").toString() :"Y";
			//String decideMonth = request.getParameter("decideMonth") != null && !request.getParameter("decideMonth").toString().equals("") ? request.getParameter("decideMonth").toString() :"";
			
			
			String forOrder = request.getParameter("forOrder")==null?"":request.getParameter("forOrder").toString();
			if(!forOrder.equals("")){
				String[] sort = forOrder.split("\\^"); 
				if(sort.length > 1 && !sort[0].equals("") && !sort[1].equals("")){
					paramMap.put("forOrder", sort[0]);
					paramMap.put("order", sort[1]);
					mav.addObject("order",  sort[1]);
				}else {
					paramMap.put("forOrder", "paymentPartner");
					paramMap.put("order", "asc");
					mav.addObject("order",  "");
				}
			}else {
				paramMap.put("forOrder", "paymentPartner");
				paramMap.put("order", "asc");
				mav.addObject("order",  "");
			}
			
			//paramMap.put("carrierType", carrierType);
			paramMap.put("searchType", searchType);
			paramMap.put("searchWord", searchWord);
			//paramMap.put("startDt", startDt);
			//paramMap.put("endDt", endDt);
			//paramMap.put("includeZero", includeZero);
			//paramMap.put("decideStatus", decideStatus);
			//paramMap.put("decideFinal", "Y");
			//paramMap.put("decideMonth", decideMonth);
			//paramMap.put("companyId", companyId);
			paramMap.put("companyType", companyType);
			
			
			int totalCount = paymentInfoService.selectPurchaseListCount(paramMap);
			
			PagingUtils.setPageing(request, totalCount, paramMap);
			
			List<Map<String, Object>> anotherList = paymentInfoService.selectPurchaseList(paramMap);
			mav.addObject("listData",  anotherList);
			mav.addObject("paramMap", paramMap);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
			mav.addObject("totalCount", totalCount);
			
			
			
		}catch(Exception e){
			e.printStackTrace();
		}

		return mav;
	}
	
	
	
	
	
	@RequestMapping(value = "/purchaseByPaymentPartnerId", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView purchaseByPaymentPartnerId(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			String companyId = session.getAttribute("companyId").toString();
			
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			String startDt = request.getParameter("startDt") == null ? "" : request.getParameter("startDt").toString();
			String endDt = request.getParameter("endDt") == null ? "" : request.getParameter("endDt").toString();
			String searchDateType = request.getParameter("searchDateType") == null ? "D" : request.getParameter("searchDateType").toString();
			
			String carrierType = request.getParameter("carrierType") == null ? "A" : request.getParameter("carrierType").toString();
			String searchType = request.getParameter("searchType") == null  ? "" : request.getParameter("searchType").toString();
			String searchWord = request.getParameter("searchWord") == null  ? "" : request.getParameter("searchWord").toString();
			String includeZero = request.getParameter("includeZero") != null && !request.getParameter("includeZero").toString().equals("") ? request.getParameter("includeZero").toString() :"N";
			
			String decideStatus = request.getParameter("decideStatus") != null && !request.getParameter("decideStatus").toString().equals("") ? request.getParameter("decideStatus").toString() :"Y";
			String decideMonth = request.getParameter("decideMonth") != null && !request.getParameter("decideMonth").toString().equals("") ? request.getParameter("decideMonth").toString() :"";
			
			
			String forOrder = request.getParameter("forOrder")==null?"":request.getParameter("forOrder").toString();
			if(!forOrder.equals("")){
				String[] sort = forOrder.split("\\^"); 
				if(sort.length > 1 && !sort[0].equals("") && !sort[1].equals("")){
					paramMap.put("forOrder", sort[0]);
					paramMap.put("order", sort[1]);
					mav.addObject("order",  sort[1]);
				}else {
					paramMap.put("forOrder", "paymentPartner");
					paramMap.put("order", "asc");
					mav.addObject("order",  "");
				}
			}else {
				paramMap.put("forOrder", "paymentPartner");
				paramMap.put("order", "asc");
				mav.addObject("order",  "");
			}
			
			paramMap.put("carrierType", carrierType);
			paramMap.put("searchType", searchType);
			paramMap.put("searchWord", searchWord);
			paramMap.put("startDt", startDt);
			paramMap.put("endDt", endDt);
			paramMap.put("searchDateType", searchDateType);
			paramMap.put("includeZero", includeZero);
			paramMap.put("decideStatus", decideStatus);
			paramMap.put("decideFinal", "Y");
			paramMap.put("decideMonth", decideMonth);
			//paramMap.put("companyId", companyId);
			
			
			List<Map<String, Object>> anotherList = paymentInfoService.selectPurchaseListByPaymentPartnerId(paramMap);
			mav.addObject("listData",  anotherList);
			mav.addObject("paramMap", paramMap);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
			
		}catch(Exception e){
			e.printStackTrace();
		}

		return mav;
	}
	
	
	
	
	
	
	//운송비 정산 -> 매입처 정산 ->  매입리스트 -> 매입처별  목록 리스트
			@RequestMapping(value = "/purchaseDetail", method = {RequestMethod.POST,RequestMethod.GET})
			public ModelAndView purchaseDetail(Locale locale,ModelAndView mav, HttpServletRequest request, 
					HttpServletResponse response) throws Exception {
				
				try{
					
					HttpSession session = request.getSession(); 
					Map userMap = (Map)session.getAttribute("user");
					String companyId = session.getAttribute("companyId").toString();
					
					
					Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
					
					/*String paymentPartnerId = request.getParameter("paymentPartnerId") == null || request.getParameter("paymentPartnerId").toString().equals("") ? "A" : request.getParameter("paymentPartnerId").toString();
					String decideMonth = request.getParameter("decideMonth") != null && !request.getParameter("decideMonth").toString().equals("") ? request.getParameter("decideMonth").toString() :"";
					paramMap.put("decideMonth", decideMonth);
					paramMap.put("paymentPartnerId", paymentPartnerId);*/
					
					paramMap.put("decideStatus", "Y");
					paramMap.put("decideFinal", "Y");
					//paramMap.put("companyId", companyId);
					
					int totalCount = adjustmentService.selectAllocationCountByPaymentPartnerIdAndDecideMonth(paramMap);
					PagingUtils.setPageing(request, totalCount, paramMap);
					
					String forOrder = request.getParameter("forOrder")==null?"":request.getParameter("forOrder").toString();
					if(!forOrder.equals("")){
						String[] sort = forOrder.split("\\^"); 
						if(sort.length > 1 && !sort[0].equals("") && !sort[1].equals("")){
							paramMap.put("forOrder", sort[0]);
							paramMap.put("order", sort[1]);
							mav.addObject("order",  sort[1]);
						}else {
							paramMap.put("forOrder", "departureDt");
							paramMap.put("order", "asc");
							mav.addObject("order",  "");
						}
					}else {
						paramMap.put("forOrder", "departureDt");
						paramMap.put("order", "asc");
						mav.addObject("order",  "");
					}
					
					List<Map<String, Object>> allocationList = adjustmentService.selectAllocationListByPaymentPartnerIdAndDecideMonth(paramMap);
					Map<String, Object> map = new HashMap<String, Object>();
					
					mav.addObject("userMap",  userMap);
					mav.addObject("listData",  allocationList);
					
					String forOpen = request.getParameter("forOpen")==null?"":request.getParameter("forOpen").toString();
					mav.addObject("forOpen", forOpen);
					mav.addObject("paramMap", paramMap);
					mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
								
				}catch(Exception e){
					e.printStackTrace();
				}
				

				return mav;
			}
			
			
	
			@RequestMapping(value = "/purchaseDetail_download", method = {RequestMethod.POST,RequestMethod.GET})
			public ModelAndView purchaseDetailDownload(Locale locale,ModelAndView mav, HttpServletRequest request, 
					HttpServletResponse response) throws Exception {
				
				ModelAndView view = new ModelAndView();
				
				try{
					
					String varNameList[] = null;
					String titleNameList[] = null;

					HttpSession session = request.getSession(); 
					Map userMap = (Map)session.getAttribute("user");
					String companyId = session.getAttribute("companyId").toString();
					
					Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
					
					/*String paymentPartnerId = request.getParameter("paymentPartnerId") == null || request.getParameter("paymentPartnerId").toString().equals("") ? "A" : request.getParameter("paymentPartnerId").toString();
					String decideMonth = request.getParameter("decideMonth") != null && !request.getParameter("decideMonth").toString().equals("") ? request.getParameter("decideMonth").toString() :"";
					paramMap.put("decideMonth", decideMonth);
					paramMap.put("paymentPartnerId", paymentPartnerId);*/
					
					paramMap.put("decideStatus", "Y");
					//paramMap.put("decideFinal", "Y");
					//paramMap.put("companyId", companyId);
					
					int totalCount = adjustmentService.selectAllocationCountByPaymentPartnerIdAndDecideMonth(paramMap);
					paramMap.put("startRownum", 0);
					paramMap.put("numOfRows", totalCount);
					
					String forOrder = request.getParameter("forOrder")==null?"":request.getParameter("forOrder").toString();
					if(!forOrder.equals("")){
						String[] sort = forOrder.split("\\^"); 
						if(sort.length > 1 && !sort[0].equals("") && !sort[1].equals("")){
							paramMap.put("forOrder", sort[0]);
							paramMap.put("order", sort[1]);
						}else {
							paramMap.put("forOrder", "departureDt");
							paramMap.put("order", "asc");
						}
					}else {
						paramMap.put("forOrder", "departureDt");
						paramMap.put("order", "asc");
					}
					
					List<Map<String, Object>> allocationList = adjustmentService.selectAllocationListByPaymentPartnerIdAndDecideMonth(paramMap);
					
					titleNameList = new String[]{"출발일","고객명","배차구분","매입처명","결제방법","증빙구분","출발지","하차지","차종","차대번호","차량번호","업체청구액","기사지급액","매출확정여부","공제율","최종지급액"};
					varNameList = new String[]{"departure_dt","customer_name","carrier_type_name","payment_partner","payment_division","billing_division","departure","arrival","car_kind","car_id_num","car_num","sales_total","amount","car_info_decide_status_name","deduction_rate","bill_for_payment"};
					
					Map<String, Object> map = new HashMap<String, Object>();
					String today = WebUtils.getNow("yyyy-MM-dd");
					map.put("driverId", paramMap.get("paymentPartnerId").toString());
					Map<String, Object> another = driverService.selectDriver(map);
					String fileName = "";
					if(another == null) {
						paramMap.put("customerId", paramMap.get("paymentPartnerId").toString());
						another = customerService.selectCustomer(paramMap);
						fileName =  another.get("customer_name").toString()+"_"+today+".xlsx";
					}else{
						fileName =  another.get("driver_name").toString()+"_"+today+".xlsx";
					}
					
					
					view.setViewName("excelDownloadView");
					view.addObject("list", allocationList);
					view.addObject("varNameList", varNameList);
					view.addObject("titleNameList", titleNameList);
					view.addObject("excelName",fileName);
								
				}catch(Exception e){
					e.printStackTrace();
				}
				

				return view;
			}	
	
	
	
    //매출관리 거래처별 매출건
	@RequestMapping(value = "/totalSales", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView totalSales(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			
			String startDt = request.getParameter("startDt") == null ? "" : request.getParameter("startDt").toString();
			String endDt = request.getParameter("endDt") == null ? "" : request.getParameter("endDt").toString();
			String searchDateType = request.getParameter("searchDateType") == null ? "" : request.getParameter("searchDateType").toString();
			
			/*if(startDt.equals("")) {
				startDt = WebUtils.getNow("yyyy-MM-dd");
			}
			if(endDt.equals("")) {
				endDt = WebUtils.getNow("yyyy-MM-dd");
			}*/
			
			paramMap.put("startDt", startDt);
			paramMap.put("endDt", endDt);
			paramMap.put("searchDateType", searchDateType);
			
			int totalCount = totalSalesService.selectAllocationCountByCustomerId(paramMap);
			PagingUtils.setPageing(request, totalCount, paramMap);
			paramMap.put("carrierType", "");
			List<Map<String, Object>> allocationList = totalSalesService.selectAllocationListByCustomerId(paramMap);
			Map<String, Object> map = new HashMap<String, Object>();
			
			mav.addObject("userMap",  userMap);
			mav.addObject("listData",  allocationList);
			
			String forOpen = request.getParameter("forOpen")==null?"":request.getParameter("forOpen").toString();
			mav.addObject("forOpen", forOpen);
			mav.addObject("paramMap", paramMap);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
						
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
    
    
    //운송비 정산 -> 매출처 정산 -> 거래처 리스트
	@RequestMapping(value = "/customerCal", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView paymentCal(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			/* String companyId = session.getAttribute("companyId").toString(); */
			
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			String startDt = request.getParameter("startDt") == null ? "" : request.getParameter("startDt").toString();
			String endDt = request.getParameter("endDt") == null ? "" : request.getParameter("endDt").toString();
			String searchDateType = request.getParameter("searchDateType") == null || request.getParameter("searchDateType").equals("") ? "D" : request.getParameter("searchDateType").toString();
			
			String carrierType = request.getParameter("carrierType") == null ? "A" : request.getParameter("carrierType").toString();
			String searchType = request.getParameter("searchType") == null || request.getParameter("searchType").toString().equals("")  ? "00" : request.getParameter("searchType").toString();
			String searchWord = request.getParameter("searchWord") == null  ? "" : request.getParameter("searchWord").toString();
			String includeZero = request.getParameter("includeZero") != null && !request.getParameter("includeZero").toString().equals("") ? request.getParameter("includeZero").toString() :"N";
			
			
			String decideStatus = request.getParameter("decideStatus") != null && !request.getParameter("decideStatus").toString().equals("") ? request.getParameter("decideStatus").toString() :"N";
			String companyType =request.getParameter("companyType") == null ? "" : request.getParameter("companyType").toString();
					
					
			
			
			String forOrder = request.getParameter("forOrder")==null?"":request.getParameter("forOrder").toString();
			if(!forOrder.equals("")){
				String[] sort = forOrder.split("\\^"); 
				if(sort.length > 1 && !sort[0].equals("") && !sort[1].equals("")){
					paramMap.put("forOrder", sort[0]);
					paramMap.put("order", sort[1]);
					mav.addObject("order",  sort[1]);
				}else {
					paramMap.put("forOrder", "regDt");
					paramMap.put("order", "asc");
					mav.addObject("order",  "");
				}
			}else {
				paramMap.put("forOrder", "regDt");
				paramMap.put("order", "asc");
				mav.addObject("order",  "");
			}
			
			paramMap.put("carrierType", carrierType);
			paramMap.put("searchType", searchType);
			paramMap.put("searchWord", searchWord);
			paramMap.put("startDt", startDt);
			paramMap.put("endDt", endDt);
			paramMap.put("searchDateType", searchDateType);
			paramMap.put("includeZero", includeZero);
			paramMap.put("decideStatus", decideStatus);
			/* paramMap.put("companyId", companyId); */
			//paramMap.put("companyType", companyType);
			 
			
			//int totalCount = adjustmentService.selectCustomerListCountRev(paramMap);
			int totalCount = adjustmentService.selectCustomerListCount(paramMap);
			
			PagingUtils.setPageing(request, totalCount, paramMap);
			
			//List<Map<String, Object>> customerList = adjustmentService.selectCustomerListRev(paramMap);
			List<Map<String, Object>> customerList = adjustmentService.selectCustomerList(paramMap);
			
			mav.addObject("listData",  customerList);
			mav.addObject("paramMap", paramMap);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
			mav.addObject("totalCount", totalCount);
			
			//Map<String, Object> totalStatistics = adjustmentService.selectTotalStatisticsRev(paramMap);
			Map<String, Object> totalStatistics = adjustmentService.selectTotalStatistics(paramMap);
			mav.addObject("totalStatistics", totalStatistics);
			
			if(decideStatus.equals("A")) {
				paramMap.put("decideStatus", "N");
				Map<String, Object> totalStatisticsN = adjustmentService.selectTotalStatistics(paramMap);
				mav.addObject("totalStatisticsN", totalStatisticsN);
				paramMap.put("decideStatus", "Y");
				Map<String, Object> totalStatisticsY = adjustmentService.selectTotalStatistics(paramMap);
				mav.addObject("totalStatisticsY", totalStatisticsY);
				paramMap.put("decideStatus", "A");
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		

 		return mav;
	}
    
    
	//운송비 정산 -> 매출처 정산 -> 거래처 리스트 -> 거래처별 매출 목록 리스트
	@RequestMapping(value = "/customerCalDetail", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView customerCalDetail(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			String companyId = session.getAttribute("companyId").toString();
			
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			String carrierType = request.getParameter("carrierType") == null || request.getParameter("carrierType").toString().equals("") ? "A" : request.getParameter("carrierType").toString();
			String startDt = request.getParameter("startDt") == null ? "" : request.getParameter("startDt").toString();
			String endDt = request.getParameter("endDt") == null ? "" : request.getParameter("endDt").toString();
			String searchDateType = request.getParameter("searchDateType") == null || request.getParameter("searchDateType").equals("")  ? "D" : request.getParameter("searchDateType").toString();
			String decideStatus = request.getParameter("decideStatus") != null && !request.getParameter("decideStatus").toString().equals("") ? request.getParameter("decideStatus").toString() :"N";
	        String companyType =request.getParameter("companyType") == null ? "" : request.getParameter("companyType").toString();
	        		
			/*if(startDt.equals("")) {
				startDt = WebUtils.getNow("yyyy-MM-dd");
			}
			if(endDt.equals("")) {
				endDt = WebUtils.getNow("yyyy-MM-dd");
			}*/
			
			paramMap.put("startDt", startDt);
			paramMap.put("endDt", endDt);
			paramMap.put("searchDateType", searchDateType);
			paramMap.put("carrierType", carrierType);
			paramMap.put("decideStatus", decideStatus);
			/* paramMap.put("companyId", companyId); */
			// paramMap.put("companyType", companyType);
			
			int totalCount = adjustmentService.selectAllocationCountByCustomerId(paramMap);
			PagingUtils.setPageing(request, totalCount, paramMap);
			
			String forOrder = request.getParameter("forOrder")==null?"":request.getParameter("forOrder").toString();
			if(!forOrder.equals("")){
				String[] sort = forOrder.split("\\^"); 
				if(sort.length > 1 && !sort[0].equals("") && !sort[1].equals("")){
					paramMap.put("forOrder", sort[0]);
					paramMap.put("order", sort[1]);
					mav.addObject("order",  sort[1]);
				}else {
					paramMap.put("forOrder", "departureDt");
					paramMap.put("order", "asc");
					mav.addObject("order",  "");
				}
			}else {
				paramMap.put("forOrder", "departureDt");
				paramMap.put("order", "asc");
				mav.addObject("order",  "");
			}
			
			List<Map<String, Object>> allocationList = adjustmentService.selectAllocationListByCustomerId(paramMap);
			Map<String, Object> map = new HashMap<String, Object>();
			
			mav.addObject("userMap",  userMap);
			mav.addObject("listData",  allocationList);
			
			String forOpen = request.getParameter("forOpen")==null?"":request.getParameter("forOpen").toString();
			mav.addObject("forOpen", forOpen);
			mav.addObject("paramMap", paramMap);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
						
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
    
	 //운송비 정산 -> 매입처 정산 -> 매입처 리스트
		@RequestMapping(value = "/anotherCal", method = {RequestMethod.POST,RequestMethod.GET})
		public ModelAndView anotherCal(Locale locale,ModelAndView mav, HttpServletRequest request, 
				HttpServletResponse response) throws Exception {
			
			try{
				
				HttpSession session = request.getSession(); 
				Map userMap = (Map)session.getAttribute("user");
				String companyId = session.getAttribute("companyId").toString();
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				String startDt = request.getParameter("startDt") == null ? "" : request.getParameter("startDt").toString();
				String endDt = request.getParameter("endDt") == null ? "" : request.getParameter("endDt").toString();
				String searchDateType = request.getParameter("searchDateType") == null ? "D" : request.getParameter("searchDateType").toString();
				
				String carrierType = request.getParameter("carrierType") == null ? "A" : request.getParameter("carrierType").toString();
				String searchType = request.getParameter("searchType") == null  ? "" : request.getParameter("searchType").toString();
				String searchWord = request.getParameter("searchWord") == null  ? "" : request.getParameter("searchWord").toString();
				String includeZero = request.getParameter("includeZero") != null && !request.getParameter("includeZero").toString().equals("") ? request.getParameter("includeZero").toString() :"N";
				String companyType =request.getParameter("companyType") == null ? "" : request.getParameter("companyType").toString();
				String decideStatus = request.getParameter("decideStatus") != null && !request.getParameter("decideStatus").toString().equals("") ? request.getParameter("decideStatus").toString() :"N";
				
				String forOrder = request.getParameter("forOrder")==null?"":request.getParameter("forOrder").toString();
				if(!forOrder.equals("")){
					String[] sort = forOrder.split("\\^"); 
					if(sort.length > 1 && !sort[0].equals("") && !sort[1].equals("")){
						paramMap.put("forOrder", sort[0]);
						paramMap.put("order", sort[1]);
						mav.addObject("order",  sort[1]);
					}else {
						paramMap.put("forOrder", "paymentPartner");
						paramMap.put("order", "asc");
						mav.addObject("order",  "");
					}
				}else {
					paramMap.put("forOrder", "paymentPartner");
					paramMap.put("order", "asc");
					mav.addObject("order",  "");
				}
				
				paramMap.put("carrierType", carrierType);
				paramMap.put("searchType", searchType);
				paramMap.put("searchWord", searchWord);
				paramMap.put("startDt", startDt);
				paramMap.put("endDt", endDt);
				paramMap.put("searchDateType", searchDateType);
				paramMap.put("includeZero", includeZero);
				paramMap.put("decideStatus", decideStatus);
				// paramMap.put("companyType", companyType);
				
			/* paramMap.put("companyId", companyId); */
				
				
				int totalCount = adjustmentService.selectAnotherListCount(paramMap);
				
				PagingUtils.setPageing(request, totalCount, paramMap);
				
				List<Map<String, Object>> anotherList = adjustmentService.selectAnotherList(paramMap);
				mav.addObject("listData",  anotherList);
				mav.addObject("paramMap", paramMap);
				mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
				mav.addObject("totalCount", totalCount);
				
				Map<String, Object> totalStatistics = adjustmentService.selectAnotherTotalStatistics(paramMap);
				mav.addObject("totalStatistics", totalStatistics);
				
				if(decideStatus.equals("A")) {
					paramMap.put("decideStatus", "N");
					Map<String, Object> totalStatisticsN = adjustmentService.selectAnotherTotalStatistics(paramMap);
					mav.addObject("totalStatisticsN", totalStatisticsN);
					paramMap.put("decideStatus", "Y");
					Map<String, Object> totalStatisticsY = adjustmentService.selectAnotherTotalStatistics(paramMap);
					mav.addObject("totalStatisticsY", totalStatisticsY);
					paramMap.put("decideStatus", "A");
				}
				
			}catch(Exception e){
				e.printStackTrace();
			}
			

			return mav;
		}
	
	
		//운송비 정산 -> 매입처 정산 ->  매입리스트 -> 매입처별  목록 리스트
		@RequestMapping(value = "/anotherCalDetail", method = {RequestMethod.POST,RequestMethod.GET})
		public ModelAndView anotherCalDetail(Locale locale,ModelAndView mav, HttpServletRequest request, 
				HttpServletResponse response) throws Exception {
			
			try{
				
				HttpSession session = request.getSession(); 
				Map userMap = (Map)session.getAttribute("user");
				String companyId = session.getAttribute("companyId").toString();
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				//String paymentPartnerId = request.getParameter("paymentPartnerId") == null || request.getParameter("paymentPartnerId").toString().equals("") ? "A" : request.getParameter("paymentPartnerId").toString();
				String carrierType = request.getParameter("carrierType") == null || request.getParameter("carrierType").toString().equals("") ? "A" : request.getParameter("carrierType").toString();
				String startDt = request.getParameter("startDt") == null ? "" : request.getParameter("startDt").toString();
				String endDt = request.getParameter("endDt") == null ? "" : request.getParameter("endDt").toString();
				String decideMonth = request.getParameter("decideMonth") == null ? "" : request.getParameter("decideMonth").toString();
				String searchDateType = request.getParameter("searchDateType") == null ? "D" : request.getParameter("searchDateType").toString();
				String decideStatus = request.getParameter("decideStatus") != null && !request.getParameter("decideStatus").toString().equals("") ? request.getParameter("decideStatus").toString() :"N";
				String decideFinal = request.getParameter("decideFinal") != null && !request.getParameter("decideFinal").toString().equals("") ? request.getParameter("decideFinal").toString() :"";
				String companyType = request.getParameter("companyType") == null ? "" : request.getParameter("companyType").toString();
				
				/*if(startDt.equals("")) {
					startDt = WebUtils.getNow("yyyy-MM-dd");
				}
				if(endDt.equals("")) {
					endDt = WebUtils.getNow("yyyy-MM-dd");
				}*/
				
				paramMap.put("startDt", startDt);
				paramMap.put("endDt", endDt);
				paramMap.put("searchDateType", searchDateType);
				paramMap.put("carrierType", carrierType);
				paramMap.put("decideStatus", decideStatus);
				paramMap.put("decideMonth", decideMonth);
			/* paramMap.put("companyId", companyId); */
				// paramMap.put("companyType", companyType);
				
				
				if(!decideStatus.equals("A")){
					if(decideFinal.equals("")) {
						paramMap.put("decideFinal", "N");	
					}else {
						paramMap.put("decideFinal", decideFinal);
					}
					
				}
				
				int totalCount = adjustmentService.selectAllocationCountByPaymentPartnerId(paramMap);
				PagingUtils.setPageing(request, totalCount, paramMap);
				
				String forOrder = request.getParameter("forOrder")==null?"":request.getParameter("forOrder").toString();
				if(!forOrder.equals("")){
					String[] sort = forOrder.split("\\^"); 
					if(sort.length > 1 && !sort[0].equals("") && !sort[1].equals("")){
						paramMap.put("forOrder", sort[0]);
						paramMap.put("order", sort[1]);
						mav.addObject("order",  sort[1]);
					}else {
						paramMap.put("forOrder", "departureDt");
						paramMap.put("order", "asc");
						mav.addObject("order",  "");
					}
				}else {
					paramMap.put("forOrder", "departureDt");
					paramMap.put("order", "asc");
					mav.addObject("order",  "");
				}
				
				List<Map<String, Object>> allocationList = adjustmentService.selectAllocationListByPaymentPartnerId(paramMap);
				Map<String, Object> map = new HashMap<String, Object>();
				
				mav.addObject("userMap",  userMap);
				mav.addObject("listData",  allocationList);
				
				String forOpen = request.getParameter("forOpen")==null?"":request.getParameter("forOpen").toString();
				mav.addObject("forOpen", forOpen);
				mav.addObject("paramMap", paramMap);
				mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
							
			}catch(Exception e){
				e.printStackTrace();
			}
			

			return mav;
		}
		
		@RequestMapping(value = "/finalCal", method = {RequestMethod.POST,RequestMethod.GET})
		public ModelAndView finalCal(Locale locale,ModelAndView mav, HttpServletRequest request, 
				HttpServletResponse response) throws Exception {
			
			try{
				
				HttpSession session = request.getSession(); 
				Map userMap = (Map)session.getAttribute("user");
				String companyId = session.getAttribute("companyId").toString();
				
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				String startDt = request.getParameter("startDt") == null ? "" : request.getParameter("startDt").toString();
				String endDt = request.getParameter("endDt") == null ? "" : request.getParameter("endDt").toString();
				String searchDateType = request.getParameter("searchDateType") == null ? "D" : request.getParameter("searchDateType").toString();
				
				String carrierType = request.getParameter("carrierType") == null ? "A" : request.getParameter("carrierType").toString();
				String searchType = request.getParameter("searchType") == null  ? "" : request.getParameter("searchType").toString();
				String searchWord = request.getParameter("searchWord") == null  ? "" : request.getParameter("searchWord").toString();
				String includeZero = request.getParameter("includeZero") != null && !request.getParameter("includeZero").toString().equals("") ? request.getParameter("includeZero").toString() :"N";
				
				String decideStatus = "Y";
				String decideFinal = request.getParameter("decideFinal") != null && !request.getParameter("decideFinal").toString().equals("") ? request.getParameter("decideFinal").toString() :"N";
				
				
				String forOrder = request.getParameter("forOrder")==null?"":request.getParameter("forOrder").toString();
				if(!forOrder.equals("")){
					String[] sort = forOrder.split("\\^"); 
					if(sort.length > 1 && !sort[0].equals("") && !sort[1].equals("")){
						paramMap.put("forOrder", sort[0]);
						paramMap.put("order", sort[1]);
						mav.addObject("order",  sort[1]);
					}else {
						paramMap.put("forOrder", "paymentPartner");
						paramMap.put("order", "asc");
						mav.addObject("order",  "");
					}
				}else {
					paramMap.put("forOrder", "paymentPartner");
					paramMap.put("order", "asc");
					mav.addObject("order",  "");
				}
				
				paramMap.put("carrierType", carrierType);
				paramMap.put("searchType", searchType);
				paramMap.put("searchWord", searchWord);
				paramMap.put("startDt", startDt);
				paramMap.put("endDt", endDt);
				paramMap.put("searchDateType", searchDateType);
				paramMap.put("includeZero", includeZero);
				paramMap.put("decideStatus", decideStatus);
				paramMap.put("decideFinal", decideFinal);
				//paramMap.put("currentDecideFinal", currentDecideFinal);
				//paramMap.put("companyId", companyId);
				
				
				int totalCount = adjustmentService.selectFinalListCount(paramMap);
				
				PagingUtils.setPageing(request, totalCount, paramMap);
				
				List<Map<String, Object>> finalList = adjustmentService.selectFinalList(paramMap);
				mav.addObject("listData",  finalList);
				mav.addObject("paramMap", paramMap);
				mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
				mav.addObject("totalCount", totalCount);
				
				Map<String, Object> totalStatistics = adjustmentService.selectFinalTotalStatistics(paramMap);
				mav.addObject("totalStatistics", totalStatistics);
				
				/*if(decideStatus.equals("A")) {
					paramMap.put("decideStatus", "N");
					Map<String, Object> totalStatisticsN = adjustmentService.selectAnotherTotalStatistics(paramMap);
					mav.addObject("totalStatisticsN", totalStatisticsN);
					paramMap.put("decideStatus", "Y");
					Map<String, Object> totalStatisticsY = adjustmentService.selectAnotherTotalStatistics(paramMap);
					mav.addObject("totalStatisticsY", totalStatisticsY);
					paramMap.put("decideStatus", "A");
				}*/
				
			}catch(Exception e){
				e.printStackTrace();
			}
			

			return mav;
		}
		
		
		@RequestMapping(value = "/finalCalDetail", method = {RequestMethod.POST,RequestMethod.GET})
		public ModelAndView finalCalDetail(Locale locale,ModelAndView mav, HttpServletRequest request, 
				HttpServletResponse response) throws Exception {
			
			try{
				
				HttpSession session = request.getSession(); 
				Map userMap = (Map)session.getAttribute("user");
				String companyId = session.getAttribute("companyId").toString();
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				//String paymentPartnerId = request.getParameter("paymentPartnerId") == null || request.getParameter("paymentPartnerId").toString().equals("") ? "A" : request.getParameter("paymentPartnerId").toString();
				String carrierType = request.getParameter("carrierType") == null || request.getParameter("carrierType").toString().equals("") ? "A" : request.getParameter("carrierType").toString();
				String startDt = request.getParameter("startDt") == null ? "" : request.getParameter("startDt").toString();
				String endDt = request.getParameter("endDt") == null ? "" : request.getParameter("endDt").toString();
				String searchDateType = request.getParameter("searchDateType") == null ? "D" : request.getParameter("searchDateType").toString();
				String decideStatus = "Y";
				String decideFinal = request.getParameter("decideFinal") != null && !request.getParameter("decideFinal").toString().equals("") ? request.getParameter("decideFinal").toString() :"N";
				
				/*if(startDt.equals("")) {
					startDt = WebUtils.getNow("yyyy-MM-dd");
				}
				if(endDt.equals("")) {
					endDt = WebUtils.getNow("yyyy-MM-dd");
				}*/
				
				paramMap.put("startDt", startDt);
				paramMap.put("endDt", endDt);
				paramMap.put("searchDateType", searchDateType);
				paramMap.put("carrierType", carrierType);
				paramMap.put("decideStatus", decideStatus);
				paramMap.put("decideFinal", decideFinal);
				//paramMap.put("companyId", companyId);
				
				int totalCount = adjustmentService.selectAllocationCountByPaymentPartnerId(paramMap);
				PagingUtils.setPageing(request, totalCount, paramMap);
				
				String forOrder = request.getParameter("forOrder")==null?"":request.getParameter("forOrder").toString();
				if(!forOrder.equals("")){
					String[] sort = forOrder.split("\\^"); 
					if(sort.length > 1 && !sort[0].equals("") && !sort[1].equals("")){
						paramMap.put("forOrder", sort[0]);
						paramMap.put("order", sort[1]);
						mav.addObject("order",  sort[1]);
					}else {
						paramMap.put("forOrder", "departureDt");
						paramMap.put("order", "asc");
						mav.addObject("order",  "");
					}
				}else {
					paramMap.put("forOrder", "departureDt");
					paramMap.put("order", "asc");
					mav.addObject("order",  "");
				}
				
				List<Map<String, Object>> allocationList = adjustmentService.selectAllocationListByPaymentPartnerId(paramMap);
				Map<String, Object> map = new HashMap<String, Object>();
				
				mav.addObject("userMap",  userMap);
				mav.addObject("listData",  allocationList);
				
				String forOpen = request.getParameter("forOpen")==null?"":request.getParameter("forOpen").toString();
				mav.addObject("forOpen", forOpen);
				mav.addObject("paramMap", paramMap);
				mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
							
			}catch(Exception e){
				e.printStackTrace();
			}
			

			return mav;
		}
		
		
		
	
		@RequestMapping(value = "/updateAnotherDecideStatus", method = RequestMethod.POST)
		@ResponseBody
		public ResultApi updateAnotherDecideStatus(ModelAndView mav,
				Model model,HttpServletRequest request ,HttpServletResponse response
				) throws Exception{
			
			ResultApi result = new ResultApi();
			try{
				HttpSession session = request.getSession(); 
				String companyId = session.getAttribute("companyId").toString();
				Map userMap = (Map)session.getAttribute("user");
				
				String carrierType = request.getParameter("carrierType").toString();
				if(carrierType.equals("")){
					carrierType = "A";
				}
				String paymentPartnerId = request.getParameter("paymentPartnerId").toString();
				String[] paymentPartnerIdArr = paymentPartnerId.split(",");
				
				String searchDateType = request.getParameter("searchDateType").toString(); 
				String startDt = request.getParameter("startDt") != null && !request.getParameter("startDt").toString().equals("") ? request.getParameter("startDt").toString():"";
				String endDt = request.getParameter("endDt") != null && !request.getParameter("endDt").toString().equals("") ? request.getParameter("endDt").toString():"";
				String searchType = request.getParameter("searchType") != null && !request.getParameter("searchType").toString().equals("") ? request.getParameter("searchType").toString():"";
				String searchWord = request.getParameter("searchWord") != null && !request.getParameter("searchWord").toString().equals("") ? request.getParameter("searchWord").toString():"";
				String currentDecideStatus = request.getParameter("currentDecideStatus").toString();
				String setDecideStatus = request.getParameter("setDecideStatus").toString();
				String setSelectedStatus = "N";
				String decideMonth = "";
				if(setDecideStatus.equals("Y")) {
					if(request.getParameter("decideMonth") != null && !request.getParameter("decideMonth").toString().equals("")) {
						decideMonth = request.getParameter("decideMonth").toString();
					}else {
						decideMonth = WebUtils.getPrevMonth();	
					}
				}else {
					decideMonth = request.getParameter("decideMonth").toString();
				}
				
				for(int i = 0; i < paymentPartnerIdArr.length; i++){
					Map<String, Object> map = new HashMap<String, Object>();
					
					map.put("paymentPartnerId", URLDecoder.decode(paymentPartnerIdArr[i], "UTF-8"));
					map.put("searchDateType", searchDateType);
					map.put("startDt", startDt);
					map.put("endDt", endDt);
					map.put("searchType", searchType);
					map.put("searchWord", URLDecoder.decode(searchWord, "UTF-8"));
					map.put("currentDecideStatus", currentDecideStatus);
					map.put("setDecideStatus", setDecideStatus);
					map.put("carrierType", carrierType);
					map.put("setSelectedStatus", setSelectedStatus);
					map.put("decideMonth", decideMonth);
					map.put("companyId", companyId);
					map.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
					map.put("logtype","U");
					paymentInfoService.updatePaymentInfoListBySearchData(map);
					paymentInfoService.insertPaymentInfoLogByMap(map); //로그기록
				}
				
			}catch(Exception e){
				e.printStackTrace();
				result.setResultCode("0001");
			}
			
			return result;
		}
		
		
		
		
		
		@RequestMapping(value = "/updateAnotherDecideFinal", method = RequestMethod.POST)
		@ResponseBody
		public ResultApi updateAnotherDecideFinal(ModelAndView mav,
				Model model,HttpServletRequest request ,HttpServletResponse response
				) throws Exception{
			
			ResultApi result = new ResultApi();
			try{
		
				HttpSession session = request.getSession(); 
				Map userMap = (Map)session.getAttribute("user");
				String companyId = session.getAttribute("companyId").toString();
				
				String carrierType = request.getParameter("carrierType").toString();
				if(carrierType.equals("")){
					carrierType = "A";
				}
				String paymentPartnerId = request.getParameter("paymentPartnerId").toString();
				String[] paymentPartnerIdArr = paymentPartnerId.split(",");
				
				String decideMonth = request.getParameter("decideMonth").toString();
				String[] decideMonthArr = decideMonth.split(",");
				
				String searchDateType = request.getParameter("searchDateType").toString(); 
				String startDt = request.getParameter("startDt") != null && !request.getParameter("startDt").toString().equals("") ? request.getParameter("startDt").toString():"";
				String endDt = request.getParameter("endDt") != null && !request.getParameter("endDt").toString().equals("") ? request.getParameter("endDt").toString():"";
				String searchType = request.getParameter("searchType") != null && !request.getParameter("searchType").toString().equals("") ? request.getParameter("searchType").toString():"";
				String searchWord = request.getParameter("searchWord") != null && !request.getParameter("searchWord").toString().equals("") ? request.getParameter("searchWord").toString():"";
				String currentDecideStatus = request.getParameter("currentDecideStatus").toString();
				String decideFinal = request.getParameter("decideFinal").toString();
				String currentDecideFinal = "";
				if(decideFinal.equals("Y")) {
					currentDecideFinal = "N";
				}else {
					currentDecideFinal = "Y";
				}
				
				for(int i = 0; i < paymentPartnerIdArr.length; i++){
					Map<String, Object> map = new HashMap<String, Object>();

					String decideFinalId = "DFI"+UUID.randomUUID().toString().replaceAll("-","");
					map.put("paymentPartnerId", URLDecoder.decode(paymentPartnerIdArr[i], "UTF-8"));
					map.put("searchDateType", searchDateType);
					map.put("startDt", startDt);
					map.put("endDt", endDt);
					map.put("searchType", searchType);
					map.put("searchWord", URLDecoder.decode(searchWord, "UTF-8"));
					map.put("currentDecideStatus", currentDecideStatus);
					map.put("decideFinal", decideFinal);
					map.put("carrierType", carrierType);
					map.put("currentDecideFinal", currentDecideFinal);
					map.put("decideFinalId", decideFinalId);
					map.put("decideMonth", decideMonthArr[i]);
					map.put("logType", "U");
					map.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
					paymentInfoService.updatePaymentInfoListDecideFinalBySearchData(map);
					paymentInfoService.insertPaymentInfoLogByMap(map); //로그타입
					
					//같은 아이디를 가진 최종 확정된 리스트를 불러 와서 최종 확정 테이블에 최종 확정 정보를  insert 한다 
					Map<String, Object> paymentInfoMap = paymentInfoService.selectPaymentInfoByDecideFinalId(map);
					DecideFinalVO decideFinalVO = new DecideFinalVO();
					decideFinalVO.setAmount(paymentInfoMap.get("amount").toString().replaceAll(",", ""));
					decideFinalVO.setComment("");
					decideFinalVO.setDecideFinalId(decideFinalId);
					decideFinalVO.setDecideFinalStatus("Y");
					decideFinalVO.setEmpId(userMap.get("emp_id").toString());
					decideFinalVO.setEmpName(userMap.get("emp_name").toString());
					decideFinalVO.setPaymentPartnerId(paymentInfoMap.get("payment_partner_id").toString());
					decideFinalVO.setPaymentPartnerName(paymentInfoMap.get("payment_partner").toString());
					decideFinalVO.setDecideMonth(paymentInfoMap.get("decide_month").toString());
					decideFinalVO.setCompanyId(companyId);
					decideFinalService.insertDecideFinal(decideFinalVO);
				
					//기사 아이디를 기준으로 최종 확정을 하고 있다. 최종 확정시 공제 내역서 엑셀 파일도 생성 하여 각 기사의 어플리케이션에서 확인 할 수 있도록 한다.    <-- 최종 확정 후 공제 내역서 보내기 버튼을 클릭 하면 기사 어플리케이션에서 확인 할 수 있게 변경됨.
					
					
					//최종 확정이 된 후 입력되어 있는 공제 내역을 토대로 각 탁송 내역에 지급 처리를 한다.
					accountService.afterProcessDecideFinal(map);
					
					
					
					
					
					
					
					
				}
				
			}catch(Exception e){
				e.printStackTrace();
				result.setResultCode("0001");
			}
			
			return result;
		}
	
	
	
		@RequestMapping(value = "/updateAnotherDecideFinalForMakeExcel", method = RequestMethod.POST)
		@ResponseBody
		public ResultApi updateAnotherDecideFinalForMakeExcel(ModelAndView mav,
				Model model,HttpServletRequest request ,HttpServletResponse response
				) throws Exception{
			
			ResultApi result = new ResultApi();
			try{
		
				HttpSession session = request.getSession(); 
				Map userMap = (Map)session.getAttribute("user");
				String companyId = session.getAttribute("companyId").toString();
				
				//String paymentPartnerId = request.getParameter("paymentPartnerId").toString();
				//String[] paymentPartnerIdArr = paymentPartnerId.split(",");
				
				String decideMonth = request.getParameter("decideMonth").toString();
				
				List<Map<String, Object>> driverList = driverService.selectDriverList(new HashMap<String, Object>());
				
				//for(int i = 0; i < paymentPartnerIdArr.length; i++){
				for(int i = 0; i < driverList.size(); i++){
					Map<String, Object> driverMap = driverList.get(i);
					Map<String, Object> map = new HashMap<String, Object>();
					//map.put("driverId", URLDecoder.decode(paymentPartnerIdArr[i], "UTF-8"));
					map.put("driverId", URLDecoder.decode(driverMap.get("driver_id").toString(), "UTF-8"));
					Map<String, Object> driver = driverService.selectDriver(map);
					if(driver != null && driver.get("driver_id") != null) {
						//map.put("decideMonth", WebUtils.getPrevMonth());
						map.put("decideMonth", decideMonth);
						List<Map<String, Object>> driverDeductFileList = driverDeductFileService.selectDriverDeductFileList(map);
						if(driverDeductFileList.size() > 0) {
							for(int j = 0; j < driverDeductFileList.size(); j++) {
								Map<String, Object> driverDeductFile = driverDeductFileList.get(j);
								File removeFile = new File(rootDir+"/deduct_list"+driverDeductFile.get("driver_deduct_file_path").toString());
								if(removeFile.exists()){
									removeFile.delete();
								}
								driverDeductFileService.deleteDriverDeductFile(driverDeductFile);
							}
						}
						Map<String, Object> fileMap = driverDeductFileService.makeDriverDeductFile(map);	
					}
					
				}
				
			}catch(Exception e){
				e.printStackTrace();
				result.setResultCode("0001");
			}
			
			return result;
		}
	
	
	
	
	
	
	
		@RequestMapping(value = "/updateAnotherPaymentInfo", method = RequestMethod.POST)
		@ResponseBody
		public ResultApi updateAnotherPaymentInfo(ModelAndView mav,
				Model model,HttpServletRequest request ,HttpServletResponse response
				) throws Exception{
			
			ResultApi result = new ResultApi();
			try{
				HttpSession session = request.getSession();
				String companyId = session.getAttribute("companyId").toString();
				Map userMap = (Map)session.getAttribute("user");
				String amount = request.getParameter("amount") != null && !request.getParameter("amount").toString().equals("") ? request.getParameter("amount").toString():"";
				String billForPayment = request.getParameter("billForPayment") != null && !request.getParameter("billForPayment").toString().equals("") ? request.getParameter("billForPayment").toString():"";
				String idx = request.getParameter("idx") != null && !request.getParameter("idx").toString().equals("") ? request.getParameter("idx").toString():"";
				String paymentPartnerId = request.getParameter("paymentPartnerId") != null && !request.getParameter("paymentPartnerId").toString().equals("") ? request.getParameter("paymentPartnerId").toString():"";
				
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("amount", amount);
				map.put("billForPayment", billForPayment);
				map.put("idx", idx);
				map.put("paymentPartnerId", paymentPartnerId);
				map.put("companyId", companyId);
				map.put("modId", userMap.get("emp_id").toString()); //수정자
				map.put("logType", "U");				
				paymentInfoService.updatePaymentInfoForAnother(map);
				paymentInfoService.insertPaymentInfoLogByMap(map); //로그기록
				
			}catch(Exception e){
				e.printStackTrace();
				result.setResultCode("0001");
			}
			
			return result;
		}
	
	
	
	@RequestMapping( value="/excel_download", method = RequestMethod.GET )
	public ModelAndView excel_download(ModelAndView mav,
			HttpServletRequest request ,HttpServletResponse response) throws Exception {
		
		ModelAndView view = new ModelAndView();
		
		try{
			
			String varNameList[] = null;
			String titleNameList[] = null;
			
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			String companyId = session.getAttribute("companyId").toString();
			
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			String carrierType = request.getParameter("carrierType") == null || request.getParameter("carrierType").toString().equals("") ? "A" : request.getParameter("carrierType").toString();
			String startDt = request.getParameter("startDt") == null ? "" : request.getParameter("startDt").toString();
			String endDt = request.getParameter("endDt") == null ? "" : request.getParameter("endDt").toString();
			String searchDateType = request.getParameter("searchDateType") == null ? "D" : request.getParameter("searchDateType").toString();
			String decideStatus = request.getParameter("decideStatus") != null && !request.getParameter("decideStatus").toString().equals("") ? request.getParameter("decideStatus").toString() :"N";
			
			paramMap.put("startDt", startDt);
			paramMap.put("endDt", endDt);
			paramMap.put("searchDateType", searchDateType);
			paramMap.put("carrierType", carrierType);
			paramMap.put("decideStatus", decideStatus);
			//paramMap.put("companyId", companyId);
			paramMap.put("forOrder", "departureDt");
			paramMap.put("order", "asc");
			int totalCount = adjustmentService.selectAllocationCountByCustomerId(paramMap);
			
			paramMap.put("startRownum", 0);
			paramMap.put("numOfRows", totalCount);
			
			List<Map<String, Object>> allocationList = adjustmentService.selectAllocationListByCustomerId(paramMap);
			
			String today = WebUtils.getNow("yyyy-MM-dd");
			Map<String, Object> customer = customerService.selectCustomer(paramMap);
			
			titleNameList = new String[]{"출발일","고객명","담당자명","결제방법","증빙구분","출발지","도착지","차종","차대번호","차량번호","계약번호","기사명","기사지급액","공급가액","부가세액","총액"};
			varNameList = new String[]{"departure_dt","customer_name","charge_name","payment_division","billing_division","departure","arrival","car_kind","car_id_num","car_num","contract_num","driver_name","amount","sales_total","vat","price"};
			
			//titleNameList = new String[]{"출발일","고객명","담당자명","결제방법","증빙구분","출발지","도착지","차종","차대번호","차량번호","계약번호","공급가액","부가세액","총액"};
			//varNameList = new String[]{"departure_dt","customer_name","charge_name","payment_division","billing_division","departure","arrival","car_kind","car_id_num","car_num","contract_num","sales_total","vat","price"};
			
			view.setViewName("excelDownloadView");
			view.addObject("list", allocationList);
			view.addObject("varNameList", varNameList);
			view.addObject("titleNameList", titleNameList);
			view.addObject("excelName", customer.get("customer_name").toString()+"_"+today+".xlsx");
			view.addObject("forSum", "Y");
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return view;
		
	}
	
	
	@RequestMapping( value="/excel_download_by_id", method = RequestMethod.GET )
	public ModelAndView excel_download_by_id(ModelAndView mav,
			HttpServletRequest request ,HttpServletResponse response) throws Exception {
		
		ModelAndView view = new ModelAndView();
		
		try{
			
			
			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			String varNameList[] = null;
			String titleNameList[] = null;
			
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			String companyId = session.getAttribute("companyId").toString();
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			String today = WebUtils.getNow("yyyy-MM-dd");
			Map<String, Object> customer = customerService.selectCustomer(paramMap);
			
			titleNameList = new String[]{"출발일","고객명","담당자명","출발지","도착지","차종","차대번호","차량번호","기사명","기사지급액","공급가액","부가세액","총액"};
			varNameList = new String[]{"departure_dt","customer_name","charge_name","departure","arrival","car_kind","car_id_num","car_num","payment_partner","amount","sales_total","vat","price"};
			
			String allocationId = request.getParameter("allocationId").toString();
			String[] allocationIdArr = allocationId.split(",");
			
			for(int i = 0; i < allocationIdArr.length; i++){
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("allocationId", allocationIdArr[i]);
				Map<String, Object> allocation = adjustmentService.selectAllocationByAllocationId(map);
				list.add(allocation);
			}
			
			view.setViewName("excelDownloadView");
			view.addObject("list", list);
			view.addObject("varNameList", varNameList);
			view.addObject("titleNameList", titleNameList);
			view.addObject("excelName", customer.get("customer_name").toString()+"_"+today+".xlsx");
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return view;
		
	}
	
	
    
    
	@RequestMapping(value = "/updateDecideStatus", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updateDecideStatus(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			HttpSession session = request.getSession();
			Map userMap = (Map)session.getAttribute("user");
			String companyId = session.getAttribute("companyId").toString();
			String carrierType = request.getParameter("carrierType").toString();
			if(carrierType.equals("")){
				carrierType = "A";
			}
			String customerId = request.getParameter("customerId").toString();
			String[] customerIdArr = customerId.split(",");
			
			String searchDateType = request.getParameter("searchDateType").toString(); 
			String startDt = request.getParameter("startDt") != null && !request.getParameter("startDt").toString().equals("") ? request.getParameter("startDt").toString():"";
			String endDt = request.getParameter("endDt") != null && !request.getParameter("endDt").toString().equals("") ? request.getParameter("endDt").toString():"";
			String searchType = request.getParameter("searchType") != null && !request.getParameter("searchType").toString().equals("") ? request.getParameter("searchType").toString():"";
			String searchWord = request.getParameter("searchWord") != null && !request.getParameter("searchWord").toString().equals("") ? request.getParameter("searchWord").toString():"";
			String currentDecideStatus = request.getParameter("currentDecideStatus").toString();
			String setDecideStatus = request.getParameter("setDecideStatus").toString();
			
			for(int i = 0; i < customerIdArr.length; i++){
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("customerId", customerIdArr[i]);
				map.put("searchDateType", searchDateType);
				map.put("startDt", startDt);
				map.put("endDt", endDt);
				map.put("searchType", searchType);
				map.put("searchWord", searchWord);
				map.put("currentDecideStatus", currentDecideStatus);
				map.put("setDecideStatus", setDecideStatus);
				map.put("carrierType", carrierType);
				map.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
				map.put("logType", "U");
				carInfoService.updateDecideStatusBySearchData(map);
				carInfoService.insertCarInfoLogByMap(map); //로그기록
			}
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
    
    
    
	@RequestMapping(value = "/updateCarInfoDecideStatus", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updateCarInfoDecide(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			HttpSession session = request.getSession();
			String allocationId = request.getParameter("allocationId").toString();
			String[] allocationIdArr = allocationId.split(",");
			String currentDecideStatus = request.getParameter("currentDecideStatus").toString();
			String setDecideStatus = request.getParameter("setDecideStatus").toString();
			Map userMap = (Map)session.getAttribute("user");
			for(int i = 0; i < allocationIdArr.length; i++){
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("allocationId", allocationIdArr[i]);
				//픽업건의 경우 하나가 확정 되면 나머지도 확정된다.
				Map<String, Object> allocation = allocationService.selectAllocation(map);
				if(allocation != null) {
					if(allocation.get("batch_status").toString().equals("P")) {		//픽업건인경우
						map.put("batchStatusId", allocation.get("batch_status_id"));
						List<Map<String, Object>> allocationList = allocationService.selectAllocationList(map);
						for(int j = 0; j < allocationList.size(); j++) {
							Map<String, Object> updateMap = new HashMap<String, Object>();
							updateMap.put("allocationId", allocationList.get(j).get("allocation_id").toString());
							updateMap.put("currentDecideStatus", currentDecideStatus);
							updateMap.put("setDecideStatus", setDecideStatus);
							updateMap.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
							updateMap.put("logType", "U");
							carInfoService.updateCarInfoDecideStatus(updateMap);
							carInfoService.insertCarInfoLogByMap(updateMap); //로그기록
						}
					}else {
						map.put("currentDecideStatus", currentDecideStatus);
						map.put("setDecideStatus", setDecideStatus);
						map.put("logType", "U");
						carInfoService.updateCarInfoDecideStatus(map);
						carInfoService.insertCarInfoLogByMap(map); //로그기록
					}
					
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}

	
	
	
	@RequestMapping(value = "/updatePaymentInfoDecideStatus", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updatePaymentInfoDecide(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
	
			HttpSession session = request.getSession();
			//String idx = request.getParameter("idx").toString();
			//String[] idxArr = idx.split(",");
			String currentDecideStatus = request.getParameter("currentDecideStatus").toString();
			String currentSelectedStatus = "Y";
			String setDecideStatus = request.getParameter("setDecideStatus").toString();
			String setSelectedStatus = "N";
			String paymentPartnerId = request.getParameter("paymentPartnerId").toString();
			Map userMap = (Map)session.getAttribute("user");
			
			String decideMonth = "";
			if(setDecideStatus.equals("Y")) {
				if(request.getParameter("decideMonth") != null && !request.getParameter("decideMonth").toString().equals("")) {
					decideMonth = request.getParameter("decideMonth").toString();
				}else {
					decideMonth = WebUtils.getPrevMonth();	
				}
			}else {
				decideMonth = "";
			}
			//for(int i = 0; i < idxArr.length; i++){
				Map<String, Object> map = new HashMap<String, Object>();
				//map.put("idx", idxArr[i]);
				map.put("currentDecideStatus", currentDecideStatus);
				map.put("currentSelectedStatus", currentSelectedStatus);
				map.put("setDecideStatus", setDecideStatus);
				map.put("setSelectedStatus", setSelectedStatus);
				map.put("decideMonth", decideMonth);
				map.put("paymentPartnerId", paymentPartnerId);
				map.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
				map.put("logType","U");
				//paymentInfoService.updatePaymentInfoByIdx(map);
				
				//2019-06-24
				paymentInfoService.updatePaymentInfoBySelectedStatus(map);
				paymentInfoService.insertPaymentInfoLogByMap(map); //로그기록
			//}
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	
	
	@RequestMapping(value = "/sendMail", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi sendMail(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
	
			String varNameList[] = null;
			String titleNameList[] = null;
			
			String rtnResult = "";
			
			
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			String carrierType = request.getParameter("carrierType") == null || request.getParameter("carrierType").toString().equals("") ? "A" : request.getParameter("carrierType").toString();
			String startDt = request.getParameter("startDt") == null ? "" : request.getParameter("startDt").toString();
			String endDt = request.getParameter("endDt") == null ? "" : request.getParameter("endDt").toString();
			String searchDateType = request.getParameter("searchDateType") == null ? "D" : request.getParameter("searchDateType").toString();
			String decideStatus = request.getParameter("decideStatus") != null && !request.getParameter("decideStatus").toString().equals("") ? request.getParameter("decideStatus").toString() :"N";
			
			String customerId = request.getParameter("customerId").toString();
			String[] customerIdArr = customerId.split(",");
			
			for(int p = 0; p < customerIdArr.length; p++){
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("customerId", customerIdArr[p]);
				int totalCount = adjustmentService.selectAllocationCountByCustomerId(map);
				map.put("startDt", startDt);
				map.put("endDt", endDt);
				map.put("searchDateType", searchDateType);
				map.put("carrierType", carrierType);
				map.put("decideStatus", decideStatus);
				map.put("startRownum", 0);
				map.put("numOfRows", totalCount);
				map.put("forOrder", "departureDt");
				map.put("order", "asc");
				
				List<Map<String, Object>> allocationList = adjustmentService.selectAllocationListByCustomerId(map);
				
				String today = WebUtils.getNow("yyyy-MM-dd");
				Map<String, Object> customer = customerService.selectCustomer(map);
				
				if(customer.get("billing_email") != null && !customer.get("billing_email").toString().equals("")) {
					
					titleNameList = new String[]{"출발일","고객명","출발지","도착지","차종","차대번호","차량번호","공급가액","부가세액","총액"};
					varNameList = new String[]{"departure_dt","customer_name","departure","arrival","car_kind","car_id_num","car_num","sales_total","vat","price"};				
					SXSSFWorkbook workbook = new SXSSFWorkbook();
					Sheet sheet = workbook.createSheet();
					String excelName = customer.get("customer_name").toString()+"_"+today+".xlsx";
					Row row = null;
			        Cell cell = null;
			        int r = 0;
			        int c = 0;
			        
			        long supply = 0L;
			        long vat = 0L;
			        long total = 0L;
			        
			        
			        CellStyle style = workbook.createCellStyle();
			        style.setFillForegroundColor(IndexedColors.SKY_BLUE.index);
			        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
			        style.setAlignment(CellStyle.ALIGN_CENTER);

			        CellStyle styleNum = workbook.createCellStyle();
			        XSSFDataFormat df = (XSSFDataFormat) workbook.createDataFormat();
			        styleNum.setDataFormat(df.getFormat("#,###"));
			        
			        
			        //Create header cells
			        row = sheet.createRow(r++);
			      
			        //title
					for(int i=0; i<titleNameList.length; i++){
						cell = row.createCell(c++);
				        cell.setCellStyle(style);
				        cell.setCellValue(titleNameList[i]);
					}
			        for ( Map<String, Object> dataMap:allocationList ) {
			        	row = sheet.createRow(r++);
			            c = 0;
			            for(int i=0; i<varNameList.length; i++){
			            	
			            	cell = row.createCell(c++);
			            	
			            	if(i >= 7 && i < 10) {
			            		cell.setCellStyle(styleNum);
			            		String value = dataMap.get(varNameList[i]).toString().replaceAll(",", "");
			            		cell.setCellValue(Integer.parseInt(value));
			            		if(i==7) {
			            			supply += Integer.parseInt(value);
			            		}else if(i==8) {
			            			vat += Integer.parseInt(value);
			            		}else if(i==9) {
			            			total += Integer.parseInt(value);
			            		}
			            	}else {
			            		cell.setCellValue(dataMap.get(varNameList[i])+"");
			            	}
			    		}
			        }
			        
			        for(int i = 0 ; i < titleNameList.length; i++){
			            sheet.autoSizeColumn(i);
			            sheet.setColumnWidth(i, (sheet.getColumnWidth(i))+512 );
			        }
			        
			        //한줄 추가
			        row = sheet.createRow(r++);
			        
			        //합계쪽 변경할 스타일 적용
			        CellStyle styleBot = workbook.createCellStyle();
			        styleBot.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
			        styleBot.setFillPattern(CellStyle.SOLID_FOREGROUND);
			        styleBot.setAlignment(CellStyle.ALIGN_CENTER);
			        
			        CellStyle styleBotNum = workbook.createCellStyle();
			        styleBotNum.setDataFormat(df.getFormat("#,###"));
			        styleBotNum.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
			        styleBotNum.setFillPattern(CellStyle.SOLID_FOREGROUND);
			        styleBotNum.setAlignment(CellStyle.ALIGN_CENTER);
			        
			        
			        //셀에 스타일 적용 하고
			        for(int i=0; i<titleNameList.length; i++){
						cell = row.createCell(i);
				        cell.setCellStyle(styleBot);
					}
			        
			        //셀을 병합 한다.
			        sheet.addMergedRegion(new CellRangeAddress(r-1,r-1,0,6));
			        
			        row.getCell(0).setCellValue("합        계");
			        /*
			        row.getCell(7).setCellFormula("sum(H1:H"+(r-1)+")");
			        row.getCell(8).setCellFormula("sum(I1:I"+(r-1)+")");
			        row.getCell(9).setCellFormula("sum(J1:J"+(r-1)+")");
			        
			        
			        row.getCell(7).setCellValue(supply);
			        row.getCell(8).setCellValue(vat);
			        row.getCell(9).setCellValue(total);
			        
			        */
			        
			        cell = row.getCell(7);
			        cell.setCellStyle(styleBotNum);
			        cell.setCellValue(supply);
			        
			        cell = row.getCell(8);
			        cell.setCellStyle(styleBotNum);
			        cell.setCellValue(vat);
			        
			        cell = row.getCell(9);
			        cell.setCellStyle(styleBotNum);
			        cell.setCellValue(total);
			        
			        try {
			            File xlsFile = new File(rootDir+"/excel");
			            
			            if(xlsFile.exists() == false){
			            	xlsFile.mkdirs();
						}
			            
			            File newFile = new File(rootDir+"/excel/"+excelName);
			            
			            FileOutputStream fileOut = new FileOutputStream(newFile);
			            workbook.write(fileOut);
			            
			            String mailSendresult = "";
			            
			            if(customer.get("billing_email") != null && !customer.get("billing_email").toString().equals("")) {
			            	mailSendresult = MailUtils.mailSend(customer.get("billing_email").toString(),"탁송내역","","/excel/"+excelName,excelName);
			            	//mailSendresult = MailUtils.mailSend("gunasss@naver.com","탁송내역","","/excel/"+excelName,excelName);
			            }
			            
			         if(!mailSendresult.equals("ok")) {
			        	 rtnResult += customer.get("customer_name").toString()+",";
			         }
			            
			        } catch (FileNotFoundException e) {
			            e.printStackTrace();
			        } catch (IOException e) {
			            e.printStackTrace();
			        }
					
				}else {
					rtnResult += customer.get("customer_name").toString()+",";
				}
				
			}
			
			if(!rtnResult.equals("")) {
				result.setResultCode("0002");
				result.setResultData(rtnResult+"거래처에 메일을 발송 하는데 실패 했습니다.");	
			}
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	
	
	
	
	
	
	@RequestMapping(value = "/billPublishRequest", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi billPublishRequest(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
	
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			
			String companyId = session.getAttribute("companyId").toString();
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			String startDt = request.getParameter("startDt") == null ? "" : request.getParameter("startDt").toString();
			String endDt = request.getParameter("endDt") == null ? "" : request.getParameter("endDt").toString();
			String searchDateType = request.getParameter("searchDateType") == null ? "D" : request.getParameter("searchDateType").toString();
			
			String carrierType = request.getParameter("carrierType") == null ? "A" : request.getParameter("carrierType").toString();
			String searchType = request.getParameter("searchType") == null  ? "" : request.getParameter("searchType").toString();
			String searchWord = request.getParameter("searchWord") == null  ? "" : request.getParameter("searchWord").toString();
			String includeZero = request.getParameter("includeZero") != null && !request.getParameter("includeZero").toString().equals("") ? request.getParameter("includeZero").toString() :"N";
			
			String decideStatus = request.getParameter("decideStatus") != null && !request.getParameter("decideStatus").toString().equals("") ? request.getParameter("decideStatus").toString() :"N";
			
			paramMap.put("carrierType", carrierType);
			paramMap.put("searchType", searchType);
			paramMap.put("searchWord", searchWord);
			paramMap.put("startDt", startDt);
			paramMap.put("endDt", endDt);
			paramMap.put("searchDateType", searchDateType);
			paramMap.put("includeZero", includeZero);
			paramMap.put("decideStatus", decideStatus);
			//paramMap.put("companyId", companyId);
			
			String billPublishInfoVal = request.getParameter("billPublishInfoList").toString();
			
			JSONParser jsonParser = new JSONParser();
			JSONObject jsonObject = (JSONObject) jsonParser.parse(billPublishInfoVal);
			JSONArray jsonArray = (JSONArray) jsonObject.get("billPublishInfoList");
			
			String failCustomer = "";
			
			for(int i = 0; i < jsonArray.size(); i++){
			JSONObject billPublishInfo = (JSONObject)jsonArray.get(i);
			
			paramMap.put("customerId", billPublishInfo.get("customerId").toString());
			
			List<Map<String, Object>> allocationList = adjustmentService.selectAllocationListByCustomerIdForBillPublish(paramMap);
			int salesTotal = 0;
			int vat = 0;
			int total = 0;
			
			String billPublishRequestId = "BPR"+UUID.randomUUID().toString().replaceAll("-","");
			
			boolean publishInsertStatus = false;
			
			for(int j = 0; j < allocationList.size(); j++) {
				Map<String, Object> allocation = allocationList.get(j);
				
				if(allocation.get("billing_division") == null || allocation.get("billing_division").toString().equals("")) {
					result.setResultCode("E001");
					return result;
				//}else if(allocation.get("billing_division").toString().equals("00") || allocation.get("billing_division").toString().equals("05")) {		//세금계산서 미발행건은 바로 매출로 넘기고 발행건에 대헤서만 합계를 계산한다.
					//(회계팀 요청) 카드건에 대해 세금계산서 발행일을 다시 입력 하도록 수정 
				}else if(allocation.get("billing_division").toString().equals("00")) {		//세금계산서 미발행건은 바로 매출로 넘기고 발행건에 대헤서만 합계를 계산한다.
					
					Map<String, Object> updateMap = new HashMap<String, Object>();
			
					Map<String, Object> allocationNewFindMap = new HashMap<String, Object>();
					allocationNewFindMap.put("allocationId", allocation.get("allocation_id").toString());
					allocation = debtorCreditorService.selectAllocationListUpdated(allocationNewFindMap);
					
					String debtorCreditorIdForD = "DCI"+UUID.randomUUID().toString().replaceAll("-","");
					int subTotal = 0;
					//차대변 아이디가 입력 되어 있는 경우는 픽업건으로 이미 차변에 등록 되어 있고 매출로 이미 넘어간 경우 이므로 입력이 되지 않은 경우만.... 
					if(allocation.get("debtor_creditor_id") == null || allocation.get("debtor_creditor_id").toString().equals("")) {
						
						//픽업 건의 경우 한건으로 차변에 등록 하기 위해...
						Map<String, Object> map = new HashMap<String,Object>();
						if(allocation.get("batch_status").toString().equals("P")) {
							map.put("batchStatusId", allocation.get("batch_status_id").toString());
							List<Map<String, Object>> pickupList = allocationService.selectAllocationList(map);
							for(int k = 0; k < pickupList.size(); k++) {
								Map<String, Object> pickup = pickupList.get(k);
								updateMap.put("allocationId", pickup.get("allocation_id").toString());
								updateMap.put("billPublishRequestId", billPublishRequestId);
								updateMap.put("billingStatus", "D");
								updateMap.put("debtorCreditorId", debtorCreditorIdForD);
								adjustmentService.updateCarInfoForBillPublish(updateMap);
								subTotal += Integer.parseInt(pickup.get("price").toString().replaceAll(",", ""));
							}
							
						}else {
							updateMap.put("allocationId", allocation.get("allocation_id").toString());
							updateMap.put("billPublishRequestId", billPublishRequestId);
							updateMap.put("billingStatus", "D");
							updateMap.put("debtorCreditorId", debtorCreditorIdForD);
							adjustmentService.updateCarInfoForBillPublish(updateMap);
							subTotal += Integer.parseInt(allocation.get("price").toString().replaceAll(",", ""));
						}
						
						//세금계산서 미 발행건에 대해서 각 건별로 차변에 등록한다.
						DebtorCreditorVO debtorCreditorVO = new DebtorCreditorVO();	
						debtorCreditorVO.setCustomerId(billPublishInfo.get("customerId").toString());
						debtorCreditorVO.setDebtorCreditorId(debtorCreditorIdForD);
						debtorCreditorVO.setOccurrenceDt(allocation.get("departure_dt").toString());
						debtorCreditorVO.setSummary("운송료 ("+allocation.get("departure").toString()+"-"+allocation.get("arrival").toString()+")");
						debtorCreditorVO.setTotalVal(String.valueOf(subTotal));
						debtorCreditorVO.setPublishYn("N");			//미발행건
						debtorCreditorVO.setType(BaseAppConstants.DEBTOR_CREDITOR_TYPE_D);
						debtorCreditorVO.setCompanyId(companyId);
						debtorCreditorService.insertDebtorCreditor(debtorCreditorVO);
						
					}
					
					
					
					
					
				}else {
					//CUSd5443753dd1245dd88737ae5a016072e
					//주성네트웍스는 ........ 나중에 고쳐야겠어 ㅜㅜ
					salesTotal += Integer.parseInt(allocation.get("sales_total").toString().replaceAll(",", ""));
					//if(allocation.get("customer_id").toString().equals("CUSd5443753dd1245dd88737ae5a016072e") || allocation.get("customer_id").toString().equals("CUSa8b74209a826425698ef189f61f08ff6")) {
					if(allocation.get("customer_id").toString().equals("CUSd5443753dd1245dd88737ae5a016072e")) {
						vat = salesTotal/10;
						total = salesTotal+vat;
						if(total%10 != 0){
							total = total-(total%10);
						}
					}else {
						vat += Integer.parseInt(allocation.get("vat").toString().replaceAll(",", ""));
						total += Integer.parseInt(allocation.get("price").toString().replaceAll(",", ""));
					}
					publishInsertStatus = true;
				}
			}
			
			if(salesTotal > 0) {
				paramMap.put("billPublishRequestId", billPublishRequestId);
				paramMap.put("billingStatus", "R");
				adjustmentService.updateCarInfoListForBillPublish(paramMap);
				BillPublishRequestVO billPublishRequestVO = new BillPublishRequestVO();				
				billPublishRequestVO.setAmount(String.valueOf(salesTotal));
				billPublishRequestVO.setVat(String.valueOf(vat));
				billPublishRequestVO.setTotal(String.valueOf(total));
				billPublishRequestVO.setBillPublishRequestId(billPublishRequestId);
				billPublishRequestVO.setCustomerId(billPublishInfo.get("customerId").toString());
				billPublishRequestVO.setCustomerName(billPublishInfo.get("customerName").toString());
				billPublishRequestVO.setComment(billPublishInfo.get("comment").toString());
				billPublishRequestVO.setPublishRequestStatus("R");
				billPublishRequestVO.setRequestDt(billPublishInfo.get("requestDt").toString());
				billPublishRequestVO.setRequesterId(userMap.get("emp_id").toString());
				billPublishRequestVO.setRequesterName(userMap.get("emp_name").toString());
				billPublishRequestVO.setSummary(billPublishInfo.get("summary").toString());
				billPublishRequestVO.setCompanyId(companyId);
				billPublishRequestService.insertBillPublishRequest(billPublishRequestVO);
			}else if(salesTotal == 0) {
				
				paramMap.put("billPublishRequestId", billPublishRequestId);
				paramMap.put("billingStatus", "Y");
				adjustmentService.updateCarInfoListForBillPublish(paramMap);
				BillPublishRequestVO billPublishRequestVO = new BillPublishRequestVO();				
				billPublishRequestVO.setAmount(String.valueOf(salesTotal));
				billPublishRequestVO.setVat(String.valueOf(vat));
				billPublishRequestVO.setTotal(String.valueOf(total));
				billPublishRequestVO.setBillPublishRequestId(billPublishRequestId);
				billPublishRequestVO.setCustomerId(billPublishInfo.get("customerId").toString());
				billPublishRequestVO.setCustomerName(billPublishInfo.get("customerName").toString());
				billPublishRequestVO.setComment(billPublishInfo.get("comment").toString());
				billPublishRequestVO.setPublishRequestStatus("Y");
				billPublishRequestVO.setRequestDt(billPublishInfo.get("requestDt").toString());
				billPublishRequestVO.setRequesterId(userMap.get("emp_id").toString());
				billPublishRequestVO.setRequesterName(userMap.get("emp_name").toString());
				billPublishRequestVO.setSummary(billPublishInfo.get("summary").toString());
				billPublishRequestVO.setCompanyId(companyId);
				
				//0원인 경우에도 계산서 발행일자가 입력 되도록 수정.
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("billPublishRequestId", billPublishRequestId);
				map.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
				map.put("logType", "U");
				paymentInfoService.updatePaymentInfoBillingDt(map);
				paymentInfoService.insertPaymentInfoLogByMap(map); //로그기록
				
				// 금액이 0원 인 경우 미발행건이 아닌건이 포함된 건에 대해서만
				if(publishInsertStatus) {
					billPublishRequestService.insertBillPublishRequest(billPublishRequestVO);	
				}
			} 
			
			}
			result.setResultData(failCustomer);
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	
	
	@RequestMapping(value = "/billPublishRequestByAllocationId", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi billPublishRequestByAllocationId(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
	
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			
			String companyId = session.getAttribute("companyId").toString();
			
			String billPublishInfoVal = request.getParameter("billPublishInfoList").toString();
			String customerId = request.getParameter("customerId").toString();
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("customerId", customerId);
			Map<String, Object> customer = customerService.selectCustomer(paramMap);
			JSONParser jsonParser = new JSONParser();
			JSONObject jsonObject = (JSONObject) jsonParser.parse(billPublishInfoVal);
			JSONArray jsonArray = (JSONArray) jsonObject.get("billPublishInfoList");
			
			int amount = 0;
			int vat = 0;
			int total = 0;
			String requestDt = "";
			String billPublishRequestId = "BPR"+UUID.randomUUID().toString().replaceAll("-","");
			
			BillPublishRequestVO billPublishRequestVO = new BillPublishRequestVO();
			billPublishRequestVO.setBillPublishRequestId(billPublishRequestId);
			
			billPublishRequestVO.setCustomerId(customerId);
			billPublishRequestVO.setCustomerName(customer.get("customer_name").toString());
			billPublishRequestVO.setCompanyId(companyId);
			
			billPublishRequestVO.setRequesterId(userMap.get("emp_id").toString());
			billPublishRequestVO.setRequesterName(userMap.get("emp_name").toString());
			
			boolean publishInsertStatus = false;
			
			for(int i = 0; i < jsonArray.size(); i++){
				JSONObject billPublishInfo = (JSONObject)jsonArray.get(i);
				Map<String, Object> map = new HashMap<String,Object>();
				map.put("allocationId", billPublishInfo.get("allocationId").toString());
				requestDt = billPublishInfo.get("requestDt").toString();
				billPublishRequestVO.setRequestDt(requestDt);	
				billPublishRequestVO.setComment(billPublishInfo.get("comment").toString());
				billPublishRequestVO.setSummary(billPublishInfo.get("summary").toString());
				Map<String, Object> allocation = allocationService.selectAllocation(map);
				
				//픽업건은 둘다 완료 하면 되는데 일괄건은 아니니깐.... 픽업건은 그대로 하되
				if(allocation.get("batch_status").toString().equals("P")) {
					map.put("batchStatusId", allocation.get("batch_status_id").toString());
					List<Map<String, Object>> allocationList = allocationService.selectAllocationList(map);
					
					String debtorCreditorIdForD = "DCI"+UUID.randomUUID().toString().replaceAll("-","");
					int subTotal = 0;
					for(int j = 0; j < allocationList.size(); j++) {
						
						Map<String, Object> updateMap = new HashMap<String, Object>();
												
						if(allocationList.get(j).get("billing_division") == null || allocationList.get(j).get("billing_division").toString().equals("")) {
							result.setResultCode("E001");
							return result;
						}else if(allocationList.get(j).get("billing_division").toString().equals("00") || allocationList.get(j).get("billing_division").toString().equals("05")) {		//세금계산서 미발행건은 바로 매출로 넘기고 발행건에 대헤서만 합계를 계산한다.
							
							//차대변 아이디가 입력 되어 있는 경우는 픽업건으로 이미 차변에 등록 되어 있고 매출로 이미 넘어간 경우 이므로 입력이 되지 않은 경우만.... 
							if(allocationList.get(j).get("debtor_creditor_id") == null || allocationList.get(j).get("debtor_creditor_id").toString().equals("")) {

								updateMap.put("allocationId", allocationList.get(j).get("allocation_id").toString());
								updateMap.put("billPublishRequestId", billPublishRequestId);
								updateMap.put("billingStatus", "D");
								updateMap.put("debtorCreditorId", debtorCreditorIdForD);
								adjustmentService.updateCarInfoForBillPublish(updateMap);
								subTotal += Integer.parseInt(allocationList.get(j).get("price").toString().replaceAll(",", ""));
								
								//세금계산서 미 발행건에 대해서 각 건별로 차변에 등록한다.
								DebtorCreditorVO debtorCreditorVO = new DebtorCreditorVO();	
								debtorCreditorVO.setCustomerId(billPublishInfo.get("customerId").toString());
								debtorCreditorVO.setDebtorCreditorId(debtorCreditorIdForD);
								debtorCreditorVO.setOccurrenceDt(allocation.get("departure_dt").toString());
								//debtorCreditorVO.setOccurrenceDt(billPublishInfo.get("requestDt").toString());
								debtorCreditorVO.setCompanyId(companyId);
								debtorCreditorVO.setSummary("운송료 ("+allocation.get("departure").toString()+"-"+allocation.get("arrival").toString()+")");							
								//debtorCreditorVO.setSummary(billPublishInfo.get("summary").toString());
								debtorCreditorVO.setTotalVal(String.valueOf(subTotal));
								debtorCreditorVO.setPublishYn("N");			//미발행건
								debtorCreditorVO.setType(BaseAppConstants.DEBTOR_CREDITOR_TYPE_D);
								debtorCreditorService.insertDebtorCreditor(debtorCreditorVO);
								
							}
							
						}else {
						
							amount += Integer.parseInt(allocationList.get(j).get("sales_total").toString().replaceAll(",", ""));
							vat += Integer.parseInt(allocationList.get(j).get("vat").toString().replaceAll(",", ""));
							total += Integer.parseInt(allocationList.get(j).get("price").toString().replaceAll(",", ""));
							updateMap.put("allocationId", allocationList.get(j).get("allocation_id").toString());
							updateMap.put("billPublishRequestId", billPublishRequestId);
							if(Integer.parseInt(allocationList.get(j).get("sales_total").toString().replaceAll(",", "")) == 0) {
								updateMap.put("billingStatus", "Y");	
							}else {
								updateMap.put("billingStatus", "R");
							}
							adjustmentService.updateCarInfoForBillPublish(updateMap);	
							publishInsertStatus = true;
						}
						
					}
					
					
					
					
				}else {
					if(allocation.get("billing_division") == null || allocation.get("billing_division").toString().equals("")) {
						result.setResultCode("E001");
						return result;
					//}else if(allocation.get("billing_division").toString().equals("00") || allocation.get("billing_division").toString().equals("05")) {		//세금계산서 미발행건은 바로 매출로 넘기고 발행건에 대헤서만 합계를 계산한다.
						//(회계팀 요청) 카드건에 대해 세금계산서 발행일을 다시 입력 하도록 수정 
					}else if(allocation.get("billing_division").toString().equals("00")) {		//세금계산서 미발행건은 바로 매출로 넘기고 발행건에 대헤서만 합계를 계산한다.
						Map<String, Object> updateMap = new HashMap<String, Object>();
						String debtorCreditorIdForD = "DCI"+UUID.randomUUID().toString().replaceAll("-","");
						updateMap.put("allocationId", allocation.get("allocation_id").toString());
						updateMap.put("billPublishRequestId", billPublishRequestId);
						updateMap.put("billingStatus", "D");
						updateMap.put("debtorCreditorId", debtorCreditorIdForD);
						adjustmentService.updateCarInfoForBillPublish(updateMap);
						
						//세금계산서 미 발행건에 대해서 각 건별로 차변에 등록한다.
						DebtorCreditorVO debtorCreditorVO = new DebtorCreditorVO();	
						debtorCreditorVO.setCustomerId(billPublishInfo.get("customerId").toString());
						debtorCreditorVO.setDebtorCreditorId(debtorCreditorIdForD);
						debtorCreditorVO.setOccurrenceDt(allocation.get("departure_dt").toString());
						debtorCreditorVO.setSummary("운송료 ("+allocation.get("departure").toString()+"-"+allocation.get("arrival").toString()+")");
						debtorCreditorVO.setTotalVal(allocation.get("price").toString().replaceAll(",", ""));
						debtorCreditorVO.setPublishYn("N");			//미발행건
						debtorCreditorVO.setCompanyId(companyId);
						debtorCreditorVO.setType(BaseAppConstants.DEBTOR_CREDITOR_TYPE_D);
						debtorCreditorService.insertDebtorCreditor(debtorCreditorVO);
						
					}else {
						Map<String, Object> updateMap = new HashMap<String, Object>();
						amount += Integer.parseInt(allocation.get("sales_total").toString().replaceAll(",", ""));
						vat += Integer.parseInt(allocation.get("vat").toString().replaceAll(",", ""));
						total += Integer.parseInt(allocation.get("price").toString().replaceAll(",", ""));
						updateMap.put("allocationId", allocation.get("allocation_id").toString());
						updateMap.put("billPublishRequestId", billPublishRequestId);
						if(Integer.parseInt(allocation.get("sales_total").toString().replaceAll(",", "")) == 0) {
							updateMap.put("billingStatus", "Y");	
						}else {
							updateMap.put("billingStatus", "R");
						}
						adjustmentService.updateCarInfoForBillPublish(updateMap);	
						publishInsertStatus = true;
					}
				}
				
			}
			
			billPublishRequestVO.setTotal(String.valueOf(total));
			billPublishRequestVO.setAmount(String.valueOf(amount));
			billPublishRequestVO.setVat(String.valueOf(vat));
			if(amount > 0) {
				billPublishRequestVO.setPublishRequestStatus("R");
				billPublishRequestService.insertBillPublishRequest(billPublishRequestVO);	
			}else if(amount == 0) {
				//0원인 경우에는 insert가 되어야 하는(발행건이 섞여 있는 경우) 매출건인 경우에만 insert를 한다.
				if(publishInsertStatus) {
					billPublishRequestVO.setPublishRequestStatus("Y");
					billPublishRequestService.insertBillPublishRequest(billPublishRequestVO);	
				}
				
			} 
			
			//result.setResultData(failCustomer);
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	@RequestMapping(value = "/updateBillPublishRequest", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updateBillPublishRequest(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
	
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			String companyId = session.getAttribute("companyId").toString();
			String billPublishRequestId = request.getParameter("billPublishRequestId").toString();
			String status = request.getParameter("status").toString();
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("billPublishRequestId", billPublishRequestId);
			Map<String, Object> billPublishRequest = billPublishRequestService.selectBillPublishRequest(map);
			
			String debtorCreditorId = "DCI"+UUID.randomUUID().toString().replaceAll("-","");
			
			if(status.equals("N")) {
				map.put("publishRequestStatus", "C");	
			}else {
				map.put("publishRequestStatus", status);
				map.put("debtorCreditorId", debtorCreditorId);
			}
			map.put("billingStatus", status);
			map.put("publisherId",userMap.get("emp_id").toString());
			map.put("publisherName",userMap.get("emp_name").toString());
			billPublishRequestService.updateBillPublishRequest(map);

			// debtorCreditorId도 업데이트 해 준다........
			adjustmentService.updateCarInfoBillingStatus(map);
			//계산서 발행 일자는 계산서를 발행 하는 경우에만 입력
			if(!status.equals("N")) {
				map.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
				map.put("logType", "U");
				paymentInfoService.updatePaymentInfoBillingDt(map);
				paymentInfoService.insertPaymentInfoLogByMap(map); //로그기록
			}
			
			//발행요청한 내역이 있고 발행을 진행 하는 경우에 차변에 내용을 insert 한다.
			if(billPublishRequest != null && billPublishRequest.get("bill_publish_request_id") != null && status.equals("Y")) {
				DebtorCreditorVO debtorCreditorVO = new DebtorCreditorVO();	
				debtorCreditorVO.setCustomerId(billPublishRequest.get("customer_id").toString());
				debtorCreditorVO.setDebtorCreditorId(debtorCreditorId);
				debtorCreditorVO.setOccurrenceDt(billPublishRequest.get("request_dt").toString());
				String summaryDt = billPublishRequest.get("request_dt").toString().split("-")[0]+"-"+billPublishRequest.get("request_dt").toString().split("-")[1]+" 운송비";
				debtorCreditorVO.setSummary(billPublishRequest.get("summary") == null ? summaryDt : billPublishRequest.get("summary").toString());
				debtorCreditorVO.setTotalVal(billPublishRequest.get("total").toString().replaceAll(",", ""));
				debtorCreditorVO.setPublishYn("Y");
				debtorCreditorVO.setType(BaseAppConstants.DEBTOR_CREDITOR_TYPE_D);
				debtorCreditorVO.setCompanyId(companyId);
				debtorCreditorService.insertDebtorCreditor(debtorCreditorVO);
			}
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	//계산서 미 발행건에 대한 차변을 입력 한다.
	//@RequestMapping(value = "/insertDebtorCreditorForPublishN", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView insertDebtorCreditor(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			HttpSession session = request.getSession();
			List<Map<String, Object>> selectAllocationListFor = debtorCreditorService.selectAllocationListForInsert(new HashMap<String, Object>());
			
			for(int i = 0; i < selectAllocationListFor.size(); i++){
				
				Map<String, Object> allocation = selectAllocationListFor.get(i);
				
				Map<String, Object> allocationNewFindMap = new HashMap<String, Object>();
				allocationNewFindMap.put("allocationId", allocation.get("allocation_id").toString());
				allocation = debtorCreditorService.selectAllocationListUpdated(allocationNewFindMap);
				
				Map<String, Object> updateMap = new HashMap<String, Object>();
				String debtorCreditorIdForD = "DCI"+UUID.randomUUID().toString().replaceAll("-","");
				int subTotal = 0;
				//차대변 아이디가 입력 되어 있는 경우는 픽업건으로 이미 차변에 등록 되어 있고 매출로 이미 넘어간 경우 이므로 입력이 되지 않은 경우만.... 
	//			if(allocation.get("debtor_creditor_id") == null || allocation.get("debtor_creditor_id").toString().equals("")) {
					
					//픽업 건의 경우 한건으로 차변에 등록 하기 위해...
				/*
					Map<String, Object> map = new HashMap<String,Object>();
					if(allocation.get("batch_status").toString().equals("P")) {
						map.put("batchStatusId", allocation.get("batch_status_id").toString());
						List<Map<String, Object>> pickupList = allocationService.selectAllocationList(map);
						for(int k = 0; k < pickupList.size(); k++) {
							Map<String, Object> pickup = pickupList.get(k);
							updateMap.put("allocationId", pickup.get("allocation_id").toString());
							updateMap.put("debtorCreditorId", debtorCreditorIdForD);
							adjustmentService.updateCarInfoDebtorCreditorId(updateMap);
							subTotal += Integer.parseInt(pickup.get("price").toString().replaceAll(",", ""));
						}
						
					}else {*/
						
						updateMap.put("allocationId", allocation.get("allocation_id").toString());
						updateMap.put("debtorCreditorId", debtorCreditorIdForD);
						adjustmentService.updateCarInfoDebtorCreditorId(updateMap);
						subTotal += Integer.parseInt(allocation.get("price").toString().replaceAll(",", ""));
					//}
					
					//세금계산서 미 발행건에 대해서 각 건별로 차변에 등록한다.
					DebtorCreditorVO debtorCreditorVO = new DebtorCreditorVO();	
					debtorCreditorVO.setCustomerId(allocation.get("customer_id").toString());
					debtorCreditorVO.setDebtorCreditorId(debtorCreditorIdForD);
					debtorCreditorVO.setOccurrenceDt(allocation.get("departure_dt").toString());
					debtorCreditorVO.setSummary("운송료 ("+allocation.get("departure").toString()+"-"+allocation.get("arrival").toString()+")");
					debtorCreditorVO.setTotalVal(String.valueOf(subTotal));
					debtorCreditorVO.setPublishYn("N");			//미발행건
					debtorCreditorVO.setType(BaseAppConstants.DEBTOR_CREDITOR_TYPE_D);
					debtorCreditorService.insertDebtorCreditor(debtorCreditorVO);
					
		//		}
				
				
			}
			
			
					
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	
	
	
	//계산서 발행건에 대한 차변을 입력 한다.
		//@RequestMapping(value = "/insertDebtorCreditorForPublishY", method = {RequestMethod.POST,RequestMethod.GET})
		public ModelAndView insertDebtorCreditorY(Locale locale,ModelAndView mav, HttpServletRequest request, 
				HttpServletResponse response) throws Exception {
			
			try{
				
				HttpSession session = request.getSession();
				List<Map<String, Object>> billPublishRequestList = debtorCreditorService.selectBillPublishRequestListForInsert(new HashMap<String, Object>());
				
				for(int i = 0; i < billPublishRequestList.size(); i++) {
					
					Map<String, Object> billPublishRequest = billPublishRequestList.get(i);
					
											
						Map<String , Object> map = new HashMap<String, Object>();
						String debtorCreditorId = "DCI"+UUID.randomUUID().toString().replaceAll("-","");
						String billPublishRequestId = billPublishRequest.get("bill_publish_request_id").toString();
						
						map.put("debtorCreditorId", debtorCreditorId);
						map.put("billPublishRequestId", billPublishRequestId);
						
						List<Map<String, Object>> carInfoList = debtorCreditorService.selectCarInfoListByBillPublishRequestId(map);
						
						DebtorCreditorVO debtorCreditorVO = new DebtorCreditorVO();	
						debtorCreditorVO.setCustomerId(billPublishRequest.get("customer_id").toString());
						debtorCreditorVO.setDebtorCreditorId(debtorCreditorId);
						debtorCreditorVO.setOccurrenceDt(billPublishRequest.get("request_dt").toString());
						
						String summaryDt = "";
						//한건인 경우 각 건의 상차지 하차지로 하여 적요에 작성 하나 한건이 아닌경우 월 단위의 적요를 작성 한다.
						if(carInfoList.size() == 1) {
							summaryDt = "운송료 ("+carInfoList.get(0).get("departure").toString()+" - "+carInfoList.get(0).get("arrival").toString()+")";
						}else{
							summaryDt = billPublishRequest.get("request_dt").toString().split("-")[0]+"-"+billPublishRequest.get("request_dt").toString().split("-")[1]+" 운송료";
						}
						
						debtorCreditorVO.setSummary(billPublishRequest.get("summary") == null ? summaryDt : billPublishRequest.get("summary").toString());
						debtorCreditorVO.setTotalVal(billPublishRequest.get("total").toString().replaceAll(",", ""));
						debtorCreditorVO.setPublishYn("Y");
						debtorCreditorVO.setType(BaseAppConstants.DEBTOR_CREDITOR_TYPE_D);
						
						debtorCreditorService.insertDebtorCreditor(debtorCreditorVO);
						
						//발행건에 대해 차변 아이디를 입력 하는것이므로 billing_status가 Y인것만 업데이트 하면 된다.
						adjustmentService.updateCarInfoDebtorCreditorIdByBillPublishRequstId(map);
					
				}
				
				
				
				
				
			
						
			}catch(Exception e){
				e.printStackTrace();
			}
			

			return mav;
		}

	
	
		//결제 완료 된 목록을 대변에 입력 한다.
			//@RequestMapping(value = "/insertDebtorCreditor", method = {RequestMethod.POST,RequestMethod.GET})
			public ModelAndView insertDebtorCreditorDDD(Locale locale,ModelAndView mav, HttpServletRequest request, 
					HttpServletResponse response) throws Exception {
				
				try{
					
					HttpSession session = request.getSession(); 
					Map userMap = (Map)session.getAttribute("user");
					// 취소 되지 않은 목록 중에서 결제완료 또는 부분 결제 된 목록을 가져온다.
					List<Map<String, Object>> paymentList = debtorCreditorService.selectPaymentListByPayment(new HashMap<String, Object>());
					
					
					for(int i = 0; i < paymentList.size(); i++) {
						
						Map<String, Object> payment = paymentList.get(i);
						Map<String , Object> map = new HashMap<String, Object>();
						Map<String, Object> updateMap = new HashMap<String, Object>();
						
						String debtorCreditorId = "DCI"+UUID.randomUUID().toString().replaceAll("-","");
						
						updateMap.put("debtorCreditorId", debtorCreditorId);
						updateMap.put("idx", payment.get("idx").toString());
						updateMap.put("allocationId", payment.get("allocation_id").toString());
						
						map.put("allocationId", payment.get("allocation_id").toString());
						Map<String, Object> allocation = allocationService.selectAllocation(map);
				
						//결제 완료 또는 부분결제 된 탁송건이 현재 매출로 넘어가지 않은경우 매출로 넘기고 차변에 등록 한 후 대변에 입력 한다.
						
						String debtorCreditorIdForD = "DCI"+UUID.randomUUID().toString().replaceAll("-","");
						
						if(allocation.get("billing_status").toString().equals("N")) {
							
							String billPublishRequestId = "BPR"+UUID.randomUUID().toString().replaceAll("-","");
							Map<String, Object> carInfoUpdateMap = new HashMap<String, Object>();
							carInfoUpdateMap.put("allocationId", payment.get("allocation_id").toString());
							carInfoUpdateMap.put("billPublishRequestId", billPublishRequestId);
							carInfoUpdateMap.put("decideStatus", "Y");
							carInfoUpdateMap.put("billingStatus", "D");
							carInfoUpdateMap.put("debtorCreditorId", debtorCreditorIdForD);
							adjustmentService.updateCarInfoForBillPublish(carInfoUpdateMap);
							
							DebtorCreditorVO debtorCreditorVO = new DebtorCreditorVO();
							debtorCreditorVO.setCustomerId(allocation.get("customer_id").toString());
							debtorCreditorVO.setDebtorCreditorId(debtorCreditorIdForD);
							debtorCreditorVO.setOccurrenceDt(allocation.get("departure_dt").toString());
							String summaryDt = ""; //한건인 경우 각 건의 상차지 하차지로 하여 적요에 작성 하나 한건이 아닌경우 월 단위의 적요를 작성 한다. 
							summaryDt ="운송료 ("+allocation.get("departure").toString()+" - "+allocation.get("arrival").toString()+")"; 
							debtorCreditorVO.setSummary(summaryDt);
							//debtorCreditorVO.setTotalVal(paymentInfo.get("amount").toString().replaceAll(",", ""));
							debtorCreditorVO.setTotalVal(allocation.get("price").toString().replaceAll(",", ""));
							debtorCreditorVO.setPublishYn("N");
							debtorCreditorVO.setType(BaseAppConstants.DEBTOR_CREDITOR_TYPE_D);
							debtorCreditorVO.setRegisterId(userMap.get("emp_id").toString());
							
							//차변에 입력 하고 
							debtorCreditorService.insertDebtorCreditor(debtorCreditorVO);
							
						}
						
						DebtorCreditorVO debtorCreditorVO = new DebtorCreditorVO();
						debtorCreditorVO.setCustomerId(allocation.get("customer_id").toString());
						debtorCreditorVO.setDebtorCreditorId(debtorCreditorId);
						
						if(payment.get("payment_dt") != null && !payment.get("payment_dt").toString().equals("")) {
							debtorCreditorVO.setOccurrenceDt(payment.get("payment_dt").toString());	
						}else {
							debtorCreditorVO.setOccurrenceDt(allocation.get("departure_dt").toString());
						}
						  
						String summaryDt = ""; //한건인 경우 각 건의 상차지 하차지로 하여 적요에 작성 하나 한건이 아닌경우 월 단위의 적요를 작성 한다. 
						summaryDt ="운송료 ("+allocation.get("departure").toString()+" - "+allocation.get("arrival").toString()+") 입금"; 
						  
						debtorCreditorVO.setSummary(summaryDt);
						//debtorCreditorVO.setTotalVal(payment.get("amount").toString().replaceAll(",", ""));
						
						//대변에 공급가액과 부가세를 더한 총액을 입력.......
						debtorCreditorVO.setTotalVal(allocation.get("price").toString().replaceAll(",", ""));
						debtorCreditorVO.setPublishYn("N");
						debtorCreditorVO.setType(BaseAppConstants.DEBTOR_CREDITOR_TYPE_C);
						if(allocation.get("billing_status").toString().equals("N")) {
							debtorCreditorVO.setDebtorId(debtorCreditorIdForD);
						}else {
							debtorCreditorVO.setDebtorId(allocation.get("debtor_creditor_id").toString().replaceAll(",", ""));
						}
						debtorCreditorVO.setRegisterId(userMap.get("emp_id").toString());
						
						//대변에 입력 하고 
						debtorCreditorService.insertDebtorCreditor(debtorCreditorVO);
						
						//대변에 입력한 아이디로 결제 정보를 업데이트 해 준다.
						paymentInfoService.updateDebtorCreditorIdByIdx(updateMap);
						
						
					}
						
				}catch(Exception e){
					e.printStackTrace();
				}
				

				return mav;
			}
		
		
		
		
		
		
		
		
		
		
		
	
	
	
	
		@RequestMapping(value = "/billRequestDetail", method = {RequestMethod.POST,RequestMethod.GET})
		public ModelAndView billRequestDetail(Locale locale,ModelAndView mav, HttpServletRequest request, 
				HttpServletResponse response) throws Exception {
			
			try{
				
				HttpSession session = request.getSession(); 
				Map userMap = (Map)session.getAttribute("user");
				String companyId = session.getAttribute("companyId").toString();
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				/*String carrierType = request.getParameter("carrierType") == null || request.getParameter("carrierType").toString().equals("") ? "A" : request.getParameter("carrierType").toString();*/
				
				String billPublishRequestId = request.getParameter("billPublishRequestId").toString();
				String billingStatus = request.getParameter("billingStatus").toString();
				
				paramMap.put("billPublishRequestId", billPublishRequestId);
				paramMap.put("billingStatus", billingStatus);
			/* paramMap.put("companyId", companyId); */
				
				int totalCount = adjustmentService.selectAllocationCountBybillPublishRequestId(paramMap);
				PagingUtils.setPageing(request, totalCount, paramMap);
				
				/*String forOrder = request.getParameter("forOrder")==null?"":request.getParameter("forOrder").toString();
				if(!forOrder.equals("")){
					String[] sort = forOrder.split("\\^"); 
					if(sort.length > 1 && !sort[0].equals("") && !sort[1].equals("")){
						paramMap.put("forOrder", sort[0]);
						paramMap.put("order", sort[1]);
						mav.addObject("order",  sort[1]);
					}else {
						paramMap.put("forOrder", "departureDt");
						paramMap.put("order", "asc");
						mav.addObject("order",  "");
					}
				}else {
					paramMap.put("forOrder", "departureDt");
					paramMap.put("order", "asc");
					mav.addObject("order",  "");
				}*/
				
				List<Map<String, Object>> allocationList = adjustmentService.selectAllocationListBybillPublishRequestId(paramMap);
				Map<String, Object> map = new HashMap<String, Object>();
				
				mav.addObject("userMap",  userMap);
				mav.addObject("listData",  allocationList);
				
				String forOpen = request.getParameter("forOpen")==null?"":request.getParameter("forOpen").toString();
				mav.addObject("forOpen", forOpen);
				mav.addObject("paramMap", paramMap);
				mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
							
			}catch(Exception e){
				e.printStackTrace();
			}
			

			return mav;
		}
	
	
		
		@RequestMapping(value = "/debtorCreditorDetail", method = {RequestMethod.POST,RequestMethod.GET})
		public ModelAndView debtorCreditorIdDetail(Locale locale,ModelAndView mav, HttpServletRequest request, 
				HttpServletResponse response) throws Exception {
			
			try{
				
				HttpSession session = request.getSession(); 
				Map userMap = (Map)session.getAttribute("user");
				String companyId = session.getAttribute("companyId").toString();
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				String debtorCreditorId = request.getParameter("debtorCreditorId").toString();
				String deleteYN = "N"; //회계팀이 직접 삭제할 수 있는 기능 flag
				paramMap.put("debtorCreditorId", debtorCreditorId);
				//paramMap.put("companyId", companyId);
				
				int totalCount = debtorCreditorService.selectAllocationCountByDebtorCreditorId(paramMap);
				PagingUtils.setPageing(request, totalCount, paramMap);
				
				if(totalCount < 1){
					deleteYN ="Y";
				}
				
				List<Map<String, Object>> allocationList = debtorCreditorService.selectAllocationListByDebtorCreditorId(paramMap);
				Map<String, Object> map = new HashMap<String, Object>();
				
				mav.addObject("userMap",  userMap);
				mav.addObject("listData",  allocationList);
				
				String forOpen = request.getParameter("forOpen")==null?"":request.getParameter("forOpen").toString();
				mav.addObject("forOpen", forOpen);
				mav.addObject("paramMap", paramMap);
				mav.addObject("deleteYN",deleteYN);
				mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
							
			}catch(Exception e){
				e.printStackTrace();
			}
			

			return mav;
		}
		
		
		//debtorCreditorId 삭제 ajax
		@RequestMapping(value = "/deleteDebtorCreditorId", method = RequestMethod.POST)
		@ResponseBody
		public ResultApi deleteDebtorCreditorId(ModelAndView mav,
				Model model,HttpServletRequest request ,HttpServletResponse response
				) throws Exception{
			
			ResultApi result = new ResultApi();
			try{
		
				
				HttpSession session = request.getSession();
				String debtorCreditorId = request.getParameter("debtorCreditorId").toString();
				
					Map<String, Object> map = new HashMap<String, Object>();
			
					map.put("debtorCreditorId", debtorCreditorId);
					debtorCreditorService.deleteDebtorCreditor(map);
					
				
			}catch(Exception e){
				e.printStackTrace();
				result.setResultCode("0001");
			}
			
			return result;
		}
	
		
		
		
		
		
		@RequestMapping(value = "/updateSelectedStatus", method = RequestMethod.POST)
		@ResponseBody
		public ResultApi updateSelectedStatus(ModelAndView mav,
				Model model,HttpServletRequest request ,HttpServletResponse response
				) throws Exception{
			
			ResultApi result = new ResultApi();
			try{
		
				
				HttpSession session = request.getSession();
				String idx = request.getParameter("idx").toString();
				String selectedStatus = request.getParameter("selectedStatus").toString();
				
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("idx", idx);
					map.put("selectedStatus", selectedStatus);
					paymentInfoService.updateSelectedStatusByIdx(map);
					//이런건 로그 기록할 필요 없음
			}catch(Exception e){
				e.printStackTrace();
				result.setResultCode("0001");
			}
			
			return result;
		}
	
		
		
		@RequestMapping(value = "/insertDriverDeductStatus", method = RequestMethod.POST)
		@ResponseBody
		public ResultApi insertDriverDeductStatus(ModelAndView mav,
				Model model,HttpServletRequest request ,HttpServletResponse response
				) throws Exception{
			
			ResultApi result = new ResultApi();
			try{
		
				HttpSession session = request.getSession();
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				if(paramMap.get("driverName") != null && paramMap.get("driverId") != null && paramMap.get("decideMonth") != null){
					if(!paramMap.get("driverName").equals("") && !paramMap.get("driverId").equals("") && !paramMap.get("decideMonth").equals("")){
						
						DriverDeductStatusVO deductStatusVO = new DriverDeductStatusVO();
						deductStatusVO.setDeductMonth(paramMap.get("decideMonth").toString());
						deductStatusVO.setDeductStatus("Y");
						deductStatusVO.setDriverDeductStatusId("DDS"+UUID.randomUUID().toString().replaceAll("-",""));
						deductStatusVO.setDriverId(paramMap.get("driverId").toString());						
						driverDeductStatusService.insertDriverDeductStatus(deductStatusVO);

					}else {
						result.setResultCode("E001");
					}
						
				}else {
					result.setResultCode("E001");
				}
				
			}catch(Exception e){
				e.printStackTrace();
				result.setResultCode("0001");
			}
			
			return result;
		}
		
		
		@RequestMapping(value = "/insertAllDriverDeductStatus", method = RequestMethod.POST)
		@ResponseBody
		public ResultApi insertAllDriverDeductStatus(ModelAndView mav,
				Model model,HttpServletRequest request ,HttpServletResponse response
				) throws Exception{
			
			ResultApi result = new ResultApi();
			try{
		
				/*
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				paramMap.put("forInsert", "Y");
				
				int totalCount = driverDeductInfoService.selectDriverListCount(paramMap);
				paramMap.put("startRownum", 0);
				paramMap.put("numOfRows", totalCount);
								
				List<Map<String, Object>> driverList = driverDeductInfoService.selectDriverList(paramMap);
				DriverDeductStatusVO deductStatusVO = new DriverDeductStatusVO();
				for(Map<String, Object> map : driverList) {
					deductStatusVO.setDeductMonth(paramMap.get("selectMonth").toString());
					deductStatusVO.setDeductStatus("Y");
					deductStatusVO.setDriverDeductStatusId("DDS"+UUID.randomUUID().toString().replaceAll("-",""));
					deductStatusVO.setDriverId(map.get("driver_id").toString());						
					driverDeductStatusService.insertDriverDeductStatus(deductStatusVO);
				}
				*/
				HttpSession session = request.getSession();
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				String[] driverIdArr = request.getParameter("driverId").toString().split("\\,");
				DriverDeductStatusVO deductStatusVO = new DriverDeductStatusVO();
				
				for(int i = 0; i < driverIdArr.length; i++) {
					deductStatusVO.setDeductMonth(paramMap.get("selectMonth").toString());
					deductStatusVO.setDeductStatus("Y");
					deductStatusVO.setDriverDeductStatusId("DDS"+UUID.randomUUID().toString().replaceAll("-",""));
					deductStatusVO.setDriverId(driverIdArr[i]);						
					driverDeductStatusService.insertDriverDeductStatus(deductStatusVO);
					
				}
				
			}catch(Exception e){
				e.printStackTrace();
				result.setResultCode("0001");
			}
			
			return result;
		}
		
		
		
		@RequestMapping( value="/excel_download_another", method = RequestMethod.GET )
		public ModelAndView excel_download_another(ModelAndView mav,
				HttpServletRequest request ,HttpServletResponse response) throws Exception {
			
			ModelAndView view = new ModelAndView();
			
			try{
				
				String varNameList[] = null;
				String titleNameList[] = null;
				
				HttpSession session = request.getSession(); 
				Map userMap = (Map)session.getAttribute("user");
				String companyId = session.getAttribute("companyId").toString();
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				//String paymentPartnerId = request.getParameter("paymentPartnerId") == null || request.getParameter("paymentPartnerId").toString().equals("") ? "A" : request.getParameter("paymentPartnerId").toString();
				String carrierType = request.getParameter("carrierType") == null || request.getParameter("carrierType").toString().equals("") ? "A" : request.getParameter("carrierType").toString();
				String startDt = request.getParameter("startDt") == null ? "" : request.getParameter("startDt").toString();
				String endDt = request.getParameter("endDt") == null ? "" : request.getParameter("endDt").toString();
				String decideMonth = request.getParameter("decideMonth") == null ? "" : request.getParameter("decideMonth").toString();
				
				String searchDateType = request.getParameter("searchDateType") == null ? "D" : request.getParameter("searchDateType").toString();
				String decideStatus = request.getParameter("decideStatus") != null && !request.getParameter("decideStatus").toString().equals("") ? request.getParameter("decideStatus").toString() :"N";
				/*if(startDt.equals("")) {
					startDt = WebUtils.getNow("yyyy-MM-dd");
				}
				if(endDt.equals("")) {
					endDt = WebUtils.getNow("yyyy-MM-dd");
				}*/
				
				paramMap.put("startDt", startDt);
				paramMap.put("endDt", endDt);
				paramMap.put("searchDateType", searchDateType);
				paramMap.put("carrierType", carrierType);
				paramMap.put("decideMonth", decideMonth);
				paramMap.put("decideStatus", decideStatus);
				//paramMap.put("companyId", companyId);
				int totalCount = adjustmentService.selectAllocationCountByPaymentPartnerId(paramMap);
				
				paramMap.put("startRownum", 0);
				paramMap.put("numOfRows", totalCount);
				
				String forOrder = request.getParameter("forOrder")==null?"":request.getParameter("forOrder").toString();
				if(!forOrder.equals("")){
					String[] sort = forOrder.split("\\^"); 
					if(sort.length > 1 && !sort[0].equals("") && !sort[1].equals("")){
						paramMap.put("forOrder", sort[0]);
						paramMap.put("order", sort[1]);
						mav.addObject("order",  sort[1]);
					}else {
						paramMap.put("forOrder", "departureDt");
						paramMap.put("order", "asc");
						mav.addObject("order",  "");
					}
				}else {
					paramMap.put("forOrder", "departureDt");
					paramMap.put("order", "asc");
					mav.addObject("order",  "");
				}
				
				List<Map<String, Object>> allocationList = adjustmentService.selectAllocationListByPaymentPartnerId(paramMap);
				
				String today = WebUtils.getNow("yyyy-MM-dd");
				paramMap.put("driverId", paramMap.get("paymentPartnerId").toString());
				Map<String, Object> another = driverService.selectDriver(paramMap);
				String fileName = "";
				if(another == null) {
					paramMap.put("customerId", paramMap.get("paymentPartnerId").toString());
					another = customerService.selectCustomer(paramMap);
					fileName =  another.get("customer_name").toString()+"_"+today+".xlsx";
				}else{
					fileName =  another.get("driver_name").toString()+"_"+today+".xlsx";
				}
				
				titleNameList = new String[]{"출발일","고객명","담당자명","배차구분","매입처명","결제방법","증빙구분","출발지","하차지","차종","차대번호","차량번호","업체청구액","기사지급액","매출확정여부","공제율","최종지급액"};
				varNameList = new String[]{"departure_dt","customer_name","charge_name","carrier_type_name","payment_partner","payment_division","billing_division","departure","arrival","car_kind","car_id_num","car_num","sales_total","amount","car_info_decide_status_name","deduction_rate","bill_for_payment"};
				view.setViewName("excelDownloadView");
				view.addObject("list", allocationList);
				view.addObject("varNameList", varNameList);
				view.addObject("titleNameList", titleNameList);
				view.addObject("excelName",fileName);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			return view;
			
		}
	
	
	
		@RequestMapping( value="/receivableDownload", method = RequestMethod.GET )
		public ModelAndView receivableDownload(ModelAndView mav,
				HttpServletRequest request ,HttpServletResponse response) throws Exception {
			
			ModelAndView view = new ModelAndView();
			
			try{
				
				String varNameList[] = null;
				String titleNameList[] = null;
				
				HttpSession session = request.getSession(); 
				Map userMap = (Map)session.getAttribute("user");
				String companyId = session.getAttribute("companyId").toString();
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				String today = WebUtils.getNow("yyyy-MM-dd");
				//paramMap.put("companyId", companyId);
				
				
				List<Map<String, Object>> allocationList = adjustmentService.selectReceivableList(paramMap);
				
								
				titleNameList = new String[]{"회사명","업체명","연락처","출발일","기사명","차종","차대번호","차량번호","상차지","하차지","금액","입금액","미수액","메모"};
				varNameList = new String[]{"company_name","customer_name","charge_phone","departure_dt","driver_name","car_kind","car_id_num","car_num","departure","arrival","sales_total","receive_payment","remain_payment","memo"};
				
				view.setViewName("excelDownloadView");
				view.addObject("list", allocationList);
				view.addObject("varNameList", varNameList);
				view.addObject("titleNameList", titleNameList);
				view.addObject("excelName", "현금,계좌이체미발행날짜없음_"+today+".xlsx");
			}catch(Exception e){
				e.printStackTrace();
			}
			
			return view;
			
		}
	
		
		
		@RequestMapping( value="/receivableTaxDownload", method = RequestMethod.GET )
		public ModelAndView receivableTaxDownload(ModelAndView mav,
				HttpServletRequest request ,HttpServletResponse response) throws Exception {
			
			ModelAndView view = new ModelAndView();
			
			try{
				
				String varNameList[] = null;
				String titleNameList[] = null;
				
				HttpSession session = request.getSession(); 
				Map userMap = (Map)session.getAttribute("user");
				String companyId = session.getAttribute("companyId").toString();
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				String today = WebUtils.getNow("yyyy-MM-dd");
				//paramMap.put("companyId", companyId);
				
				
				List<Map<String, Object>> allocationList = adjustmentService.selectReceivableTaxList(paramMap);
				
								
				titleNameList = new String[]{"업체명","연락처","출발일","기사명","차종","차대번호","차량번호","상차지","하차지","금액","입금액","미수액"};
				varNameList = new String[]{"customer_name","charge_phone","departure_dt","driver_name","car_kind","car_id_num","car_num","departure","arrival","sales_total","receive_payment","remain_payment"};
				
				view.setViewName("excelDownloadView");
				view.addObject("list", allocationList);
				view.addObject("varNameList", varNameList);
				view.addObject("titleNameList", titleNameList);
				view.addObject("excelName", "현금,계좌이체미발행날짜없음_"+today+".xlsx");
			}catch(Exception e){
				e.printStackTrace();
			}
			
			return view;
			
		}
	
	
		
		@RequestMapping(value = "/collectManage", method = {RequestMethod.POST,RequestMethod.GET})
		public ModelAndView collectManage(Locale locale,ModelAndView mav, HttpServletRequest request, 
				HttpServletResponse response) throws Exception {
			
			try{
				
				HttpSession session = request.getSession();
				Map userSessionMap = (Map)session.getAttribute("user");
				String companyId = session.getAttribute("companyId").toString();
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				String startDt = paramMap.get("startDt") != null && !paramMap.get("startDt").toString().equals("") ? paramMap.get("startDt").toString() : WebUtils.getNow("yyyy-MM-dd"); 
				String endDt = paramMap.get("endDt") != null && !paramMap.get("endDt").toString().equals("") ? paramMap.get("endDt").toString() : WebUtils.getNow("yyyy-MM-dd");
				
	            
				String searchWord = request.getParameter("searchWord") == null || request.getParameter("searchWord").toString().equals("") ? "" : request.getParameter("searchWord").toString();
	            String yearType = request.getParameter("yearType") == null || request.getParameter("yearType").toString().equals("")?"": request.getParameter("yearType").toString();
	            String quarterType= request.getParameter("quarterType") == null || request.getParameter("quarterType").toString().equals("") ? "0" : request.getParameter("quarterType").toString();
	            String companyType =request.getParameter("companyType") == null ? "" :request.getParameter("companyType").toString();
	           
	            Map<String,Object> map = new HashMap<String, Object>();
				List<Map<String,Object>> yearMap = accountService.selectForYear(map);
				 
	
				List<String> quarterTypeList = new ArrayList<String>();
	            
	            
	            if(quarterType !=null && !quarterType.toString().equals("0") ) {
					
					searchWord ="";
					
				} if(quarterType.equals("5")) {
	            	quarterTypeList.add("1");
	            	quarterTypeList.add("2");
	            	
	            }else if(quarterType.equals("6")) {
	            	quarterTypeList.add("3");
	            	quarterTypeList.add("4");
	            }else {
	            	quarterTypeList.add(quarterType);
	            }
	            
				
				
				paramMap.put("startDt", startDt);
				paramMap.put("endDt", endDt);
				paramMap.put("searchWord", searchWord);
				paramMap.put("yearType", yearType);
				paramMap.put("quarterType", quarterType);
				paramMap.put("quarterTypeList",quarterTypeList);
				
				
				 List<Map<String, Object>> listData = paymentInfoService.selectPaymentInfoForCollectManage(paramMap);
				
				 mav.addObject("listData", listData);
				mav.addObject("paramMap", paramMap);
				mav.addObject("yearList", yearMap);
				mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
							
			}catch(Exception e){
				e.printStackTrace();
			}
			

			return mav;
		}
	
		
		
		
	    @RequestMapping(value = "/collectManageMiddle", method = {RequestMethod.POST,RequestMethod.GET})
        public ModelAndView collectManagemld(Locale locale,ModelAndView mav, HttpServletRequest request, 
                    HttpServletResponse response) throws Exception {
        
           
           try {
           
           		HttpSession session = request.getSession();
           		Map userSessionMap = (Map)session.getAttribute("user");
           		// String companyId = session.getAttribute("companyId").toString();
           		String companyType =request.getParameter("companyType") == null ? "" : request.getParameter("companyType").toString();
           				
           		
           		Map<String,Object> paramMap = ParamUtils.getParamToMap(request);

           		int totalCount = allocationService.selectAllocationCountByPaymentDt(paramMap);
				
				PagingUtils.setPageing(request, totalCount, paramMap);

				List<Map<String,Object>> listData = allocationService.selectListManageMiddle(paramMap);
           	
				mav.addObject("listData", listData);
				mav.addObject("paramMap", paramMap);
				mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
           	
				
               }catch(Exception e) {
            	  e.printStackTrace();
               	
               	
               }
               
            
               return mav;
            
        }
        
		
		
		
		@RequestMapping(value = "/collectManageDetail", method = {RequestMethod.POST,RequestMethod.GET})
		public ModelAndView collectManageDetail(Locale locale,ModelAndView mav, HttpServletRequest request, 
				HttpServletResponse response) throws Exception {
			
			try{
				
				HttpSession session = request.getSession();
				Map userSessionMap = (Map)session.getAttribute("user");
				String companyId = session.getAttribute("companyId").toString();
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				
				int totalCount = allocationService.selectAllocationCountByPaymentDt(paramMap);
				
				PagingUtils.setPageing(request, totalCount, paramMap);
				
				 List<Map<String, Object>> listData = allocationService.selectAllocationListByPaymentDt(paramMap);
				
				mav.addObject("listData", listData);
				mav.addObject("paramMap", paramMap);
				mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
							
			}catch(Exception e){
				e.printStackTrace();
			}
			

			return mav;
		}
	
		
		
		@RequestMapping( value="/collect_excel_download", method = RequestMethod.GET )
		public ModelAndView collect_excel_download(ModelAndView mav,
				HttpServletRequest request ,HttpServletResponse response) throws Exception {
			
			ModelAndView view = new ModelAndView();
			
			try{
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				List<Map<String, Object>> list = null;

				String varNameList[] = null;
				String titleNameList[] = null;
				
				int totalCount = 0;
				
				HttpSession session = request.getSession(); 
				Map userMap = (Map)session.getAttribute("user");
				String companyId = session.getAttribute("companyId").toString();
				
				
				paramMap.put("forExcel", "Y"); 
				totalCount = allocationService.selectAllocationCountByPaymentDt(paramMap);
				paramMap.put("startRownum", 0);
				paramMap.put("numOfRows", totalCount);
				
				
				list = allocationService.selectAllocationListByPaymentDt(paramMap);
				
				Map<String, Object> map = new HashMap<String, Object>();
	
				titleNameList = new String[]{"입금일","출발일","고객명","배차구분","출발지","하차지","결제방법","증빙구분","차종","차대번호","차량번호","기사명","공급가액","부가세액","총액"};
				varNameList = new String[]{"payment_dt","departure_dt","customer_name","carrier_type","departure","arrival","payment_kind","billing_division","car_kind","car_id_num","car_num","driver_name","sales_total","vat","price"};
				
				view.setViewName("excelDownloadView");
				view.addObject("list", list);
				view.addObject("varNameList", varNameList);
				view.addObject("titleNameList", titleNameList);
				view.addObject("excelName", "allocation-list.xlsx");
			}catch(Exception e){
				e.printStackTrace();
			}
			
			return view;
			
		}
		
		
		
		
		
		
		//애증의 요약관리 controller
		@RequestMapping(value = "/summary", method = {RequestMethod.POST,RequestMethod.GET})
	      public ModelAndView summary(Locale locale,ModelAndView mav, HttpServletRequest request, 
	            HttpServletResponse response) throws Exception {
	         
	         try {
	            
	            
	            HttpSession  session  = request.getSession();
	            Map userMap = (Map)session.getAttribute("user");
	            
	            String searchType = request.getParameter("searchType") == null || request.getParameter("searchType").toString().equals("") ? "date" : request.getParameter("searchType").toString();
	            String searchWord = request.getParameter("searchWord") == null || request.getParameter("searchWord").toString().equals("") ? WebUtils.getNow("yyyy-MM-dd")  : request.getParameter("searchWord").toString();
	            String yearType = request.getParameter("yearType") == null || request.getParameter("yearType").toString().equals("")?"": request.getParameter("yearType").toString();
	            String quarterType= request.getParameter("quarterType") == null || request.getParameter("quarterType").toString().equals("") ? "0" : request.getParameter("quarterType").toString();
	            String companyType =request.getParameter("companyType") == null ? "" :request.getParameter("companyType").toString();
	            
	       
	            
	            Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
					
				if(paramMap.get("searchType") == null){
			         paramMap.put("searchType", searchType);
			         paramMap.put("searchWord", searchWord);
			         paramMap.put("companyType",companyType);
				}
				
				 Map<String,Object> map = new HashMap<String, Object>();
				 List<Map<String,Object>> yearMap = accountService.selectForYear(map);
				 
	
				 List<String> quarterTypeList = new ArrayList<String>();
	            
	    		if(quarterType !=null && !quarterType.toString().equals("0") ) {
					
					searchWord ="";
	            	searchType ="";
					
				} if(quarterType.equals("5")) {
	            	quarterTypeList.add("1");
	            	quarterTypeList.add("2");
	            	
	            }else if(quarterType.equals("6")) {
	            	quarterTypeList.add("3");
	            	quarterTypeList.add("4");
	            }else {
	            	quarterTypeList.add(quarterType);
	            }
	            
	            paramMap.put("searchWord", searchWord);
	            paramMap.put("searchType", searchType);
	            paramMap.put("quarterType",quarterType);	
	            paramMap.put("quarterTypeList",quarterTypeList);
	            paramMap.put("yearType",yearType);
	            paramMap.put("companyType",companyType);
	            
	
	            	
	            int totalCount=accountService.selectMonthTotalCount(paramMap);
				PagingUtils.setPageing(request, totalCount, paramMap);
				
			/* 거래처별요약관리 */
	            List<Map<String,Object>>summaryList =accountService.selectSummary(paramMap);
	       
	            mav.addObject("paramMap",paramMap);
	            mav.addObject("yearType",yearType);
	            mav.addObject("list",summaryList);
	            
	            mav.addObject("yearList",yearMap);
	            
	            
	         // 직원별배차및 요약관리 
	            List <Map<String,Object>> employeeList = accountService.selectEmployeeSummary(paramMap);
	            
	            mav.addObject("map1",paramMap);
	            mav.addObject("list1", employeeList);
	            
	         // 캐리어타입 별  배차 구분 
	            List<Map<String,Object>> teamEmpList =accountService.selectTeamSummary(paramMap);
	            
	            mav.addObject("tmap",paramMap);
	            mav.addObject("tlist",teamEmpList);
	            
	         // 서비스별 집계 구분 
	           List<Map<String,Object>> teamPaymentList = accountService.selectTeamPaymentList(paramMap);
	           
	           mav.addObject("paramMap",paramMap);
	           mav.addObject("teampaymentList",teamPaymentList);
	           
	           //서비스별 수금 현황
	            
	            List<Map<String,Object>>selectCarrierPaymentList =accountService.selectCarrierPaymentList(paramMap);
	            
	            mav.addObject("paramMap",paramMap);
		        mav.addObject("selectCarrierPaymentList",selectCarrierPaymentList);
	           
	         // 현장집계 
	            List<Map <String,Object>>paymentList = accountService.selectPaymentList(paramMap);
	            mav.addObject("list2",paymentList); 
	            
	         //직원별 현장집계
	            
	           List<Map<String,Object>>employeePaymentList = accountService.selectEmployeePaymentList(paramMap);
	           mav.addObject("paramMap",paramMap);
	           mav.addObject("employeePaymentList",employeePaymentList);
	            
	         // 직원별 배차건수 
	            
	            List<Map<String,Object>>allocationstatusList = accountService.selectAllocationstatus(paramMap);

	            mav.addObject("map3",paramMap);
	            mav.addObject("list3",allocationstatusList);
	            
	            
	         }catch (Exception e) {
	            e.printStackTrace();
	         }
	         
	         return mav;
	      }

		
	      
	    //상세보기 controller
	      @RequestMapping(value = "/viewBycustomerId", method = {RequestMethod.POST,RequestMethod.GET})
	      public ModelAndView viewBycustomerId(Locale locale,ModelAndView mav, HttpServletRequest request, 
	            HttpServletResponse response) throws Exception {
	         
	         try {
	            
	               HttpSession session = request.getSession();
	               Map userMap = (Map) session.getAttribute("user");
	                
	                  
	               Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
	               String companyId = session.getAttribute("companyId").toString();
	                //String allocation_id =request.getParameter("allocation_id");
	               
	               String searchType = request.getParameter("searchType") != null && !request.getParameter("searchType").toString().equals("") ? request.getParameter("searchType").toString() : ""; 
	               String selectMonth = request.getParameter("selectMonth") == null || request.getParameter("selectMonth").toString().equals("")  ? WebUtils.getNow("yyyy-MM") : request.getParameter("selectMonth").toString();
	             
	               int totalCount = accountService.selectCustomerTotalCount(paramMap);
	            
	               PagingUtils.setPageing(request, totalCount, paramMap);
	 
	               paramMap.put("selectMonth", selectMonth);
	               paramMap.put("searchType", searchType);
	            
	            List<Map<String,Object>> list = accountService.selectCustomerId(paramMap);
	            
	            mav.addObject("list",list);
	            mav.addObject("paramMap",paramMap);
	            
	            
	            
	         }catch(Exception e) {
	            e.printStackTrace();
	            
	            
	         }
	            
	      return mav;
	      
	      }
	      

	      @RequestMapping(value = "/viewBatchTeam", method = {RequestMethod.POST,RequestMethod.GET})
	      public ModelAndView viewBatchTeam(Locale locale,ModelAndView mav, HttpServletRequest request, 
	            HttpServletResponse response) throws Exception {
	      

	         try {
	               HttpSession session = request.getSession();
	                  Map userSessionMap = (Map) session.getAttribute("user");
	                  //String dateSearch =request.getParameter("dateSearch") !=null && ! request.getParameter("dateSearch").toString().equals("") ? request.getParameter("dateSearch").toString() :WebUtils.getNow("yyyy-MM-dd");
	                  
	                  
	                  Map <String,Object> map = new HashMap<String, Object>();

	                  String dateSearch =WebUtils.getNow("yyyy-MM-dd");
	                  String emp_id =userSessionMap.get("emp_id").toString(); 
	     
	                     /* String emp_id ="hk0006";*/                  
	                
	          
	                   map.put("emp_id", emp_id);
	                    map.put("dateSearch",dateSearch);
	                
	               List<Map<String,Object>> list = accountService.selectBatchTeam(map);
	               
	               mav.addObject("list",list);
	               mav.addObject("map",map);
	               
	         
	               

	                 
	         }catch(Exception e) {
	            e.printStackTrace();
	            
	         }
	         return mav;
	                  
	         
	      }
	         @RequestMapping(value = "/batchTeamFinal", method = {RequestMethod.POST,RequestMethod.GET})
	         @ResponseBody
	         public Map<String, Object> batchTeamFinal(Locale locale,ModelAndView mav, HttpServletRequest request, 
	                     HttpServletResponse response) throws Exception {
	         
	            Map <String,Object> map = new HashMap<String,Object>();
	            
	            try {
	               
	                 HttpSession session = request.getSession();
	                 Map userSessionMap = (Map) session.getAttribute("user");
	                  
	                 String emp_id =userSessionMap.get("emp_id").toString();
	                 String now_dt =WebUtils.getNow("yyyy-MM-dd");

                     map.put("emp_work_finish_id", "EWF" +UUID.randomUUID().toString().replaceAll("-", ""));
                     map.put("now_dt",now_dt);
                     map.put("emp_id",emp_id);
                     map.put("successCode", "0000");
                     
                     accountService.insertBatchTeam(map);
	                     
	                  
	               }catch (Exception e) {
	                  e.printStackTrace();
	                   map.put("successCode", "0001");
	               }
	               
	               
	               return map;
	               
	               }
	        
	         
	         @RequestMapping(value = "/employeeBatchDetail", method = {RequestMethod.POST,RequestMethod.GET})
	         public ModelAndView employeeBacthDetail(Locale locale,ModelAndView mav, HttpServletRequest request, 
	                     HttpServletResponse response) throws Exception {
	            
	            try {
	               
	                  HttpSession session = request.getSession();
	                  Map userSessionMap = (Map) session.getAttribute("user");
	                     
	                     
	                   Map <String,Object> paramMap = ParamUtils.getParamToMap(request);
	                    
	                   String register_id =request.getParameter("register_id")== null || request.getParameter("register_id").toString().equals("") ? "" : request.getParameter("register_id").toString();	
	                   String searchType = request.getParameter("searchType") == null || request.getParameter("searchType").toString().equals("") ? "date": request.getParameter("searchType").toString();
	                   String searchWord=request.getParameter("searchWord") ==null || request.getParameter("searchWord").toString().equals("") ? "" : request.getParameter("searchWord").toString();	
	                   String carrierType = request.getParameter("carrierType") == null || request.getParameter("carrierType").toString().equals("") ? "": request.getParameter("carrierType").toString();
	                   String paymentKind =request.getParameter("paymentKind") == null || request.getParameter("paymentKind").toString().equals("") ? "": request.getParameter("paymentKind").toString();
	                   String customerId =request.getParameter("customerId") == null || request.getParameter("customerId").toString().equals("") ? "": request.getParameter("customerId").toString();
	                   String yearType = request.getParameter("yearType") == null || request.getParameter("yearType").toString().equals("")?"": request.getParameter("yearType").toString();
	   	               String quarterType= request.getParameter("quarterType") == null || request.getParameter("quarterType").toString().equals("") ? "0" : request.getParameter("quarterType").toString();
	   	               String allocationStatus =request.getParameter("allocationStatus") == null || request.getParameter("allocationStatus").toString().equals("") ? "" :request.getParameter("allocationStatus").toString();
	   	               String companyType =request.getParameter("companyType") == null ? "" : request.getParameter("companyType").toString();
	   	               String payment =request.getParameter("payment") == null ? "" : request.getParameter("payment").toString();
	   	               
	               
	                   if(searchType.equals("month") && searchWord.equals("")){
	                	   
	                	  searchWord = WebUtils.getNow("yyyy-MM") ;
	                	   
	                   }else if(searchType.equals("date") && searchWord.equals("")) {
	                	   
	                	   searchWord =  WebUtils.getNow("yyyy-MM-dd");
	                   }

	                   
	               	if(paramMap.get("searchType") == null){
				         paramMap.put("searchType", searchType);
				         paramMap.put("searchWord", searchWord);
				         
					}

	  				 List<String> quarterTypeList = new ArrayList<String>();
	  	            
	  	    		if(quarterType !=null && !quarterType.toString().equals("0") ) {
	  					
	  					searchWord ="";
	  	            	searchType ="";
	  					
	  				} if(quarterType.equals("5")) {
	  	            	quarterTypeList.add("1");
	  	            	quarterTypeList.add("2");
	  	            	
	  	            }else if(quarterType.equals("6")) {
	  	            	quarterTypeList.add("3");
	  	            	quarterTypeList.add("4");
	  	            }else {
	  	            	quarterTypeList.add(quarterType);
	  	            }
	                   
 	                  paramMap.put("register_id", register_id);
	                  paramMap.put("searchType", searchType);
	                  paramMap.put("carrierType", carrierType);
	             	  paramMap.put("paymentKind",paymentKind);
	             	  paramMap.put("searchWord", searchWord);
	             	  paramMap.put("customerId", customerId);
	             	  paramMap.put("quarterType", quarterType);
	             	  paramMap.put("yearType",yearType);
	             	  paramMap.put("quarterTypeList",quarterTypeList);
	             	  paramMap.put("allocationStatus", allocationStatus);
	             	  paramMap.put("companyType", companyType);
	             	  paramMap.put("payment", payment);
	             	  
	                   
	             	  int totalCount =accountService.selectBatchTeamTotalCount(paramMap);
	             	  PagingUtils.setPageing(request, totalCount, paramMap);
	             	  
	                  List<Map<String,Object>> employeelist = accountService.selectEmployeeBatchDetail(paramMap);
	                  
	                  mav.addObject("employeelist",employeelist);
	                  mav.addObject("paramMap",paramMap);

	                  
	            }catch (Exception e) {
	                  e.printStackTrace();
	                  
	               }
	               
	               
	               return mav;
	               }
	
	         
	         
	         
	         
	
	         @RequestMapping( value="/summary_excel", method = RequestMethod.POST )
	     	
	         public ModelAndView summary_excel(ModelAndView mav,
	     			HttpServletRequest request ,HttpServletResponse response) throws Exception {
	     		
	     		ModelAndView view = new ModelAndView();
	     		
	     		try{
	     			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
	     	
	     			Map<String, Object> map = new HashMap<String, Object>();
	     	
	     			String receipt = StringUtils.split(request.getParameter("receipt"), ",")[1];
	    	        String uploadDir = rootDir + "/" + subDir;
	    			String saveName = "/" + WebUtils.getNow("yyyy") + "/" + WebUtils.getNow("MM") + "/" + WebUtils.getNow("dd");
	    			String newFilename = UUID.randomUUID().toString()+".png";
	    			File saveDir = new File(uploadDir + saveName);
	    			
	    			if(saveDir.exists() == false){
	    				saveDir.mkdirs();
	    			}
	    			saveName = saveName + "/" + newFilename;
	    			
	    			File test = new File(uploadDir + saveName);
	    			
	    			FileUtils.writeByteArrayToFile(test, Base64.decodeBase64(receipt));
	     			
	     			
	     			view.setViewName("excelDownloadView");
	     			view.addObject("excelName", "summary-list.xlsx");
	     			view.addObject("file", test);
	     			view.addObject("forSummary", "Y");
	     			
	     			
	     		}catch(Exception e){
	     			e.printStackTrace();
	     		}
	     		
	     		return view;
	     		
	     	}
	
	     
	         
	         
	         
	         

	 		//현장 수금건에 대해서 결제 완료로 바꿔주고 이미 차변이 있으므로 대변만 입력 한다.
	 			//@RequestMapping(value = "/insertDebtorCreditorDaeByeon", method = {RequestMethod.POST,RequestMethod.GET})
	 			public ModelAndView insertDebtorCreditorDaeByeon(Locale locale,ModelAndView mav, HttpServletRequest request, 
	 					HttpServletResponse response) throws Exception {
	 				
	 				try{
	 					
	 					HttpSession session = request.getSession(); 
	 					Map userMap = (Map)session.getAttribute("user");
	 					
	 					
	 					// 아까 보낸 쿼리로 리스트를 가져와서 
	 					Map<String,Object> map = new HashMap<String, Object>();
	 					List<Map<String, Object>> paymentList = paymentInfoService.selectDaeByeonForInsertList(map);
	 					
	 					
	 					
	 					for(int i = 0; i < paymentList.size(); i++) {
	 						
	 						Map<String, Object> payment = paymentList.get(i);
	 						map = new HashMap<String, Object>();
	 						Map<String, Object> updateMap = new HashMap<String, Object>();
	 						
	 						String debtorCreditorId = "DCI"+UUID.randomUUID().toString().replaceAll("-","");
	 						
	 						updateMap.put("debtorCreditorId", debtorCreditorId);
	 						updateMap.put("idx", payment.get("idx").toString());
	 						updateMap.put("allocationId", payment.get("allocation_id").toString());
	 						updateMap.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자

	 						map.put("allocationId", payment.get("allocation_id").toString());
	 						Map<String, Object> allocation = allocationService.selectAllocation(map);
	 				
	 						//String debtorCreditorIdForD = allocation.get("debtor_creditor_id").toString();
	 						
	 						DebtorCreditorVO debtorCreditorVO = new DebtorCreditorVO();
	 						debtorCreditorVO.setCustomerId(allocation.get("customer_id").toString());
	 						debtorCreditorVO.setDebtorCreditorId(debtorCreditorId);
	 						
	 						if(payment.get("payment_dt") != null && !payment.get("payment_dt").toString().equals("")) {
	 							debtorCreditorVO.setOccurrenceDt(payment.get("payment_dt").toString());	
	 						}else {
	 							debtorCreditorVO.setOccurrenceDt(allocation.get("departure_dt").toString());
	 						}
	 						  
	 						String summaryDt = ""; //한건인 경우 각 건의 상차지 하차지로 하여 적요에 작성 하나 한건이 아닌경우 월 단위의 적요를 작성 한다. 
	 						summaryDt ="운송료 ("+allocation.get("departure").toString()+" - "+allocation.get("arrival").toString()+") 입금"; 
	 						  
	 						debtorCreditorVO.setSummary(summaryDt);
	 						//debtorCreditorVO.setTotalVal(payment.get("amount").toString().replaceAll(",", ""));
	 						
	 						//대변에 공급가액과 부가세를 더한 총액을 입력.......
	 						debtorCreditorVO.setTotalVal(allocation.get("price").toString().replaceAll(",", ""));
	 						debtorCreditorVO.setPublishYn("N");
	 						debtorCreditorVO.setType(BaseAppConstants.DEBTOR_CREDITOR_TYPE_C);
	 						debtorCreditorVO.setDebtorId(allocation.get("debtor_creditor_id").toString().replaceAll(",", ""));
	 						debtorCreditorVO.setRegisterId("AUTO");
	 						
	 						//대변에 입력 하고 
	 						debtorCreditorService.insertDebtorCreditor(debtorCreditorVO);
	 						
	 						//대변에 입력한 아이디로 결제 정보를 업데이트 해 준다.
	 						paymentInfoService.updateDebtorCreditorIdByIdxForDaeByeon(updateMap);
	 						
	 						
	 					}
	 						
	 				}catch(Exception e){
	 					e.printStackTrace();
	 				}
	 				

	 				return mav;
	 			}
	 		
	 		
	 		
	@RequestMapping(value = "/updateDriverBillingCancel", method = {RequestMethod.POST,RequestMethod.GET})
	@ResponseBody
 	public ResultApi updateDriverBillingCancel(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) throws Exception {
 
		ResultApi result = new ResultApi();
    
    try {
       
         HttpSession session = request.getSession();
         Map userSessionMap = (Map) session.getAttribute("user");
         
         Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
         
         
         //기존에 발행한 세금계산서를 삭제 하고
         driverBillingFileService.deleteDriverBillingFileByDriverIdAndDecideMonth(paramMap);
         
         //세금계산서 발행 상태를 삭제 하고
         driverBillingStatusService.deleteDriverBillingStatusByDriverIdAndDecideMonth(paramMap);
         
         //정산내역서 driver_view_flag를 N으로 업데이트 한다.
         paramMap.put("driverViewFlag","N");
         driverDeductFileService.updateDriverViewFlag(paramMap);
         
          
       }catch (Exception e) {
          e.printStackTrace();
          result.setResultCode("E001");
       }
       
       
       return result;
       
       }	
	         
	

	//매입관리에서 취소 ajax

		@RequestMapping(value = "/canslePurchase", method = { RequestMethod.POST, RequestMethod.GET })
		@ResponseBody
		public ResultApi cansleForPurchase(Locale locale, ModelAndView mav, HttpServletRequest request,
				HttpServletResponse response) throws Exception {

			ResultApi result = new ResultApi();

			try {

				Map<String, Object> map = new HashMap<String, Object>();
				HttpSession session = request.getSession();
				Map userMap = (Map)session.getAttribute("user");

				String paymentPartnerId = request.getParameter("purchaseByPaymentPartnerId") == null ? "": request.getParameter("purchaseByPaymentPartnerId").toString();
				String decideMonth = request.getParameter("decideMonth") == null ? "": request.getParameter("decideMonth").toString();

				map.put("decideMonth", decideMonth);
				map.put("paymentPartnerId", paymentPartnerId);
				map.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자

				paymentInfoService.updatePurchaseCansle(map);

			} catch (Exception e) {
				e.printStackTrace();
				result.setResultCode("0001");
			}

			return result;

		}
	
		
		@RequestMapping(value = "/updateStatus", method = RequestMethod.POST)
		@ResponseBody
		public ResultApi updateStatus(ModelAndView mav, Model model,HttpServletRequest request ,HttpServletResponse response) throws Exception{
			ResultApi result = new ResultApi();
			try{
			
				Map<String, Object> param = new HashMap<String, Object>();
				
				// type = A 매출 취소 날짜별 일괄
				// type = B 매출 취소 선택 날짜안에 차대별 일괄 처리
				String type = request.getParameter("type").toString() == null ? "" : request.getParameter("type").toString(); 
				String[] debtorCreditorIds = StringUtilsEx.toStringArray(request.getParameter("debtorCreditorId").toString(),",");
				List<String> debtorCreditorIdList = new ArrayList<String>(Arrays.asList(debtorCreditorIds));
				String[] allocationIds = null;
				List<String> allocationIdList = null;
				// type = B 인 경우만  allocationid가 있다
				if("B".equals(type)) {
					String[] ids = StringUtilsEx.toStringArray(request.getParameter("allocationId").toString(),",");
					List<String> idsList = new ArrayList<String>(Arrays.asList(ids));
					Map<String, Object> param2 = new HashMap<String, Object>();
					param2.put("idsList", idsList);
					String idAll = debtorCreditorService.selectAllocationIdByBatchStatusId(param2);
					allocationIds = StringUtilsEx.toStringArray(idAll,",");
					allocationIdList = new ArrayList<String>(Arrays.asList(allocationIds));
				}
				
				// type = B일경우만 allcheck 값이 있고, 
				// allCheck = Y일경우 ==> type = A 처럼 처리
				// allCheck = N일경우 ==> allocation_id로 일부만 처리
				String allCheck = request.getParameter("allCheck") == null ? "N" : request.getParameter("allCheck").toString(); 
				String startDt = request.getParameter("startDt") == null ? "" : request.getParameter("startDt").toString();
				String endDt = request.getParameter("endDt") == null ? "" : request.getParameter("endDt").toString();
				
				HttpSession session = request.getSession(); 
				String companyId = session.getAttribute("companyId").toString();
				param.put("companyId", companyId);
				param.put("status", request.getParameter("setDecideStatus").toString());
				param.put("debtorCreditorIdList", debtorCreditorIdList);
				param.put("allocationIdList", allocationIdList);
				param.put("type", type);
				param.put("allCheck", allCheck);
			
				if("A".equals(type)) {
					/*매출의 취소 - 계산서발행요청 테이블- 계산서 발행 요청 상태 update (R:발행요청,C:취소,Y:발행)*/
				    debtorCreditorService.updateBillPublishRequest(param);
				    /*매출의 일괄 취소 - 운행정보 테이블- 계산서 상태(N:미발행,R:발행요청,Y:발행,D:발행제외) update*/
				    debtorCreditorService.updateBillingStatus(param);
				    /*매출의 일괄 취소 - 운행정보 테이블- 계산서 발행 요청 아이디로 delete*/
				    debtorCreditorService.deleteDebtorCreditors(param);
				}else {
					if("Y".equals(allCheck)) {
						/*매출의 취소 - 계산서발행요청 테이블- 계산서 발행 요청 상태 update (R:발행요청,C:취소,Y:발행)*/
					    debtorCreditorService.updateBillPublishRequest(param);
					    /*매출의 일괄 취소 - 운행정보 테이블- 계산서 상태(N:미발행,R:발행요청,Y:발행,D:발행제외) update*/
					    debtorCreditorService.updateBillingStatus(param);
					    /*매출의 일괄 취소 - 운행정보 테이블- 계산서 발행 요청 아이디로 delete*/
					    debtorCreditorService.deleteDebtorCreditors(param);
					}else {
						/*매출의 취소 - 계산서발행요청 테이블- 계산서 발행 요청 상태 update (R:발행요청,C:취소,Y:발행)*/
					    debtorCreditorService.updateBillPublishRequest(param);
					    /*매출의 일괄 취소 - 운행정보 테이블- 계산서 상태(N:미발행,R:발행요청,Y:발행,D:발행제외) update*/
					    debtorCreditorService.updateBillingStatus(param);
					}
				}

					
			}catch(Exception e){
				e.printStackTrace();
				result.setResultCode("0001");
			}
			
			return result;
		}
	         
	         
	
}
