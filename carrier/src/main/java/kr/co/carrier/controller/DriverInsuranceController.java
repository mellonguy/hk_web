package kr.co.carrier.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.co.carrier.service.DriverInsuranceService;
import kr.co.carrier.utils.ResultApi;
import kr.co.carrier.utils.WebUtils;
import kr.co.carrier.vo.DriverInsuranceVO;

@Controller
@RequestMapping(value="/driverInsurance")
public class DriverInsuranceController {

	
	@Autowired
	private DriverInsuranceService driverInsuranceService;
	
	
	
	
	
	@RequestMapping(value = "/insert-driverInsurance", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView getCustomerList(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response, DriverInsuranceVO driverInsuranceVO) throws Exception {
		
		ResultApi result = new ResultApi();
		try{
			
			Map<String, Object> map = new HashMap<String, Object>();
			String mode = "";
			if(driverInsuranceVO.getInsuranceId().equals("")){
				String uuid = UUID.randomUUID().toString().replaceAll("-","");
				String insuranceId = "INS" + uuid;
				driverInsuranceVO.setInsuranceId(insuranceId);	
			}
			
			if(request.getParameter("mode").equals("0")){
				driverInsuranceService.insertDriverInsurance(driverInsuranceVO);
				mode = "등록";
			}else{
				driverInsuranceService.updateDriverInsurance(driverInsuranceVO);
				mode = "수정";
			}
			
			
			WebUtils.messageAndRedirectUrl(mav, mode+"되었습니다.", "/baseinfo/driver.do");
			
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}

		return mav;
	}
	
	
	@RequestMapping(value = "/getDriverInsurance", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi getDriverInsurance(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		ResultApi result = new ResultApi();
		try{
			
			Map<String, Object> map = new HashMap<String, Object>();
			
			map.put("driverId", request.getParameter("driverId").toString());
			
			Map<String, Object> insuranceMap = driverInsuranceService.selectDriverInsurance(map);
			result.setResultData(insuranceMap);
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}

		return result;
	}
	
	
	
	
}
