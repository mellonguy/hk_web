package kr.co.carrier.controller;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.co.carrier.service.AdminService;
import kr.co.carrier.service.CompanyService;
import kr.co.carrier.service.DriverService;
import kr.co.carrier.service.EmployeeService;
import kr.co.carrier.utils.PagingUtils;
import kr.co.carrier.utils.ParamUtils;
import kr.co.carrier.utils.ResultApi;
import kr.co.carrier.utils.WebUtils;
import kr.co.carrier.vo.AdminVO;
import kr.co.carrier.vo.AlarmSocketVO;
import kr.co.carrier.vo.EvaluationCompleteVO;
import kr.co.carrier.vo.EvaluationVO;

@Controller
@RequestMapping(value="/environment")
public class EnvironmentSettingController {

	@Value("#{appProp['upload.base.path']}")
    private String rootDir;    
    private String subDir = "bbs";	
	
    @Autowired
    private AdminService adminService;
    
    @Autowired
    private EmployeeService empService;

    
    @Autowired
    private DriverService driverService;
    
    @Autowired
    private CompanyService companyService;
    
    
    
	@RequestMapping(value = "/admin", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView admin(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			int totalCount = adminService.selectAdminListCount(paramMap);
			PagingUtils.setPageing(request, totalCount, paramMap);
			
			List<Map<String, Object>> adminList = adminService.selectAdminList(paramMap);
			mav.addObject("listData",  adminList);
			mav.addObject("paramMap", paramMap);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
						
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	@RequestMapping(value = "/user", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView user(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			paramMap.put("searchStatus", "");
			int totalCount = empService.selectEmployeeListCount(paramMap);
			PagingUtils.setPageing(request, totalCount, paramMap);
			
			/*String searchType = request.getParameter("searchType") == null ? "" : request.getParameter("searchType").toString();
			String searchWord = request.getParameter("searchWord") == null ? "" : request.getParameter("searchWord").toString();
			
			if(searchType.equals("")) {
				searchType = "ALL";
			}*/
			
			List<Map<String, Object>> empList = empService.selectEmployeeList(paramMap);
			List<Map<String, Object>> companyList = companyService.selectCompanyList(paramMap);
			
			mav.addObject("listData",  empList);
			mav.addObject("paramMap", paramMap);
			mav.addObject("companyList",  companyList);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
						
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	
	@RequestMapping(value = "/driver", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView driver(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			paramMap.put("searchStatus", "");
			int totalCount = driverService.selectDriverListCount(paramMap);
			PagingUtils.setPageing(request, totalCount, paramMap);
			
			String searchType = request.getParameter("searchType") == null ? "" : request.getParameter("searchType").toString();
			String searchWord = request.getParameter("searchWord") == null ? "" : request.getParameter("searchWord").toString();
			
			if(searchType.equals("")) {
				searchType = "ALL";
			}
			paramMap.put("searchType", searchType);
			paramMap.put("searchWord", searchWord);
			
			
			List<Map<String, Object>> driverList = driverService.selectDriverList(paramMap);
			mav.addObject("listData",  driverList);
			mav.addObject("paramMap", paramMap);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
						
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	@RequestMapping(value = "/getDriver", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi getDriver(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		ResultApi result = new ResultApi();
		try{
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("driverId", request.getParameter("driverId").toString());
			map.put("duplicateChk", "Y");
			
			Map<String, Object> driverMap = driverService.selectDriver(map);
			result.setResultData(driverMap);
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}

		return result;
	}
	
	
	
	@RequestMapping(value = "/updateDriverStatus", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updateDriverStatus(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		ResultApi result = new ResultApi();
		try{
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("driverId", request.getParameter("driverId").toString());
			map.put("driver_deposit_yn", request.getParameter("driver_deposit_yn").toString()); //기사 예치금 사용여부
			
			String gubun = request.getParameter("gubun").toString();
			
			//변경 대상이 기사 재직여부일 경우
			if("status".equals(gubun)) {
				map.put("driverStatus", request.getParameter("value").toString());
			}

			//변경 대상이 기사(App) 비상연락망 오픈여부일 경우
			if("emg".equals(gubun)) {
				map.put("emgNumViewYn", request.getParameter("value").toString());
			}
			
			driverService.updateDriverStatus(map);
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}

		return result;
	}

	@RequestMapping(value = "/updateEmpApproveYn", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updateEmpApproveYn(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		ResultApi result = new ResultApi();
		try{
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("empId", request.getParameter("empId").toString());
			map.put("approveYn", request.getParameter("status").toString());
			empService.updateEmployeeApproveYn(map);
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}

		return result;
	}

	@RequestMapping(value = "/getEmp", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi getEmpApproveYn(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		ResultApi result = new ResultApi();
		try{
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("empId", request.getParameter("empId").toString());
			map.put("duplicateChk", "Y");
			
			Map<String, Object> empMap = empService.selectEmployee(map);
			result.setResultData(empMap);
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}

		return result;
	}
	
	@RequestMapping(value = "/user-role", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView userRole(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			paramMap.put("searchStatus", "");
			int totalCount = empService.selectEmployeeListCount(paramMap);
			PagingUtils.setPageing(request, totalCount, paramMap);
			
			List<Map<String, Object>> empList = empService.selectEmployeeList(paramMap);
			mav.addObject("listData",  empList);
			mav.addObject("paramMap", paramMap);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
						
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	@RequestMapping(value = "/updateEmpRole", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updateEmpRole(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		ResultApi result = new ResultApi();
		try{
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("empId", request.getParameter("empId").toString());
			map.put("empRole", request.getParameter("status").toString());
			empService.updateEmployeeRole(map);
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}

		return result;
	}
	
	@RequestMapping(value = "/updateEmpAllocation", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updateEmpAllocation(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		ResultApi result = new ResultApi();
		try{
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("empId", request.getParameter("empId").toString());
			map.put("allocation", request.getParameter("status").toString());
			empService.updateEmployeeAllocation(map);
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}

		return result;
	}
	
	@RequestMapping(value = "/updateEmpStatus", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updateEmpStatus(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		ResultApi result = new ResultApi();
		try{
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("empId", request.getParameter("empId").toString());
			map.put("empStatus", request.getParameter("status").toString());
			empService.updateEmployeeStatus(map);
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}

		return result;
	}
	
	
	@RequestMapping(value = "/updateEmployeeListRowCnt", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updateEmployeeListRowCnt(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		ResultApi result = new ResultApi();
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("empId", userSessionMap.get("emp_id").toString());
				map.put("listRowCnt", request.getParameter("listRowCnt").toString());
				empService.updateEmployeeListRowCnt(map);	
			
			}else {
				result.setResultCode("E001");
			}
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}

		return result;
	}
	
	
	
		
	@RequestMapping(value = "/add-admin", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView addadmin(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			Map<String, Object> weekMap = new HashMap<String, Object>();
			Map<String, Object> monthMap = new HashMap<String, Object>();
			
			Date date = new Date();
			DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
			
			String formattedDate = dateFormat.format(date);
			
			mav.addObject("serverTime", formattedDate );
			
			mav.addObject("test", "test");
						
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	@RequestMapping(value = "/insert-admin", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView insertAdmin(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute AdminVO adminVO) throws Exception {
		
		try{
			
			Map<String, Object> map = new HashMap<String, Object>();
			
			map.put("duplicateChk", "true");
			map.put("adminId", adminVO.getAdminId());
			
			Map<String, Object> adminMap = adminService.selectAdmin(map);
			
			if(adminMap != null){
				WebUtils.messageAndRedirectUrl(mav, "이미 사용중인 아이디 입니다.", "/environment/add-admin.do");
			}else{
				adminService.insertAdmin(adminVO);		
				WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/environment/admin.do");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	@RequestMapping(value = "/edit-admin", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView editAdmin(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute AdminVO adminVO) throws Exception {
		
		try{
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("duplicateChk", "true");
			map.put("adminId", adminVO.getAdminId());
			Map<String, Object> adminMap = adminService.selectAdmin(map);
			mav.addObject("adminMap",adminMap);
		}catch(Exception e){
			e.printStackTrace();
		}

		return mav;
	}

	@RequestMapping(value = "/update-admin", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView updateAdmin(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute AdminVO adminVO) throws Exception {
		
		try{
			adminService.updateAdmin(adminVO);
			WebUtils.messageAndRedirectUrl(mav, "수정되었습니다.", "/environment/admin.do");
		}catch(Exception e){
			e.printStackTrace();
		}

		return mav;
	}	
	

	
	@RequestMapping(value = "/updateEmployeeCompanyId", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updateEmployeeCompanyId(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		ResultApi result = new ResultApi();
		try{
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("empId", request.getParameter("empId").toString());
			map.put("companyId", request.getParameter("companyId").toString());
			empService.updateEmployeeCompanyId(map);
			
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}

		return result;
	}
	
	
	
	

	   @RequestMapping(value = "/evaluation-register", method = {RequestMethod.POST,RequestMethod.GET})
	   public ModelAndView evaluationRegister(Locale locale,ModelAndView mav, HttpServletRequest request, 
	         HttpServletResponse response ,EvaluationVO evaluationVO) throws Exception {
	      
	      try{
	   
	         Map<String, Object> map = new HashMap<String, Object>();
	         
	   
	         Map<String,Object> paramMap =  ParamUtils.getParamToMap(request);
	         String evaluationMonth = request.getParameter("evaluationMonth") == null || request.getParameter("evaluationMonth").toString().equals("") ? WebUtils.getNow("yyyy-MM")  : request.getParameter("evaluationMonth").toString();
	         String empId = request.getParameter("empId").toString();
	       
	         
	         
	         map.put("evaluationMonth",evaluationMonth);
	         map.put("empId", empId);
	         
	         
	         List<Map<String, Object>> evaluationList = empService.selectEvaluationQuestion(map);
	         mav.addObject("evaluationList",evaluationList);
	         mav.addObject("paramMap",paramMap);
	         
	        
	         
	         
	         
	      }catch(Exception e){
	         e.printStackTrace();
	      }

	      return mav;
	   }   
	   
	   
	   
	   

	   @RequestMapping(value = "/insert-evaluation", method = {RequestMethod.POST,RequestMethod.GET})
	   public ModelAndView insertEvaluation(Locale locale,ModelAndView mav,HttpServletRequest request, 
	         HttpServletResponse response ,EvaluationVO evaluationVO ,EvaluationCompleteVO evaluationCompleteVO) throws Exception {
	      try{
	   
	         Map<String, Object> map = new HashMap<String, Object>();
	         
	         HttpSession session = request.getSession();
	         Map userMap = (Map)session.getAttribute("user");
	   
	         String evaluationMonth = request.getParameter("evaluationMonth") == null || request.getParameter("evaluationMonth").toString().equals("") ? WebUtils.getNow("yyyy-MM")  : request.getParameter("evaluationMonth").toString();
	         String empId = request.getParameter("empId").toString();
	         String empManagerId= userMap.get("emp_id").toString();
	         String empEvaluationCompleteId = "EEC"+UUID.randomUUID().toString().replaceAll("-","");
	         int evaluationCompletePoints = 0;
	         
	         map.put("empId",empId);
	         map.put("evaluationMonth",evaluationMonth);
	         map.put("empManagerId", empManagerId);
	         
	         //for문을 돌면서 
	         
	         List<Map<String, Object>> questionList = new ArrayList<Map<String,Object>>();         
	         
	         List<String> str = empService.selectEvaluationQuestionIdList();
	         
	        empService.deleteEvaluationData(map); 
	         
	         for(int i = 0; i < str.size(); i++) {
	            
	            String empEvaluationId = "EEI"+UUID.randomUUID().toString().replaceAll("-","");
	            
	            String empquestionId = str.get(i);
	            
	            
	            String evaluationPoints= request.getParameter(empquestionId) == null || request.getParameter(empquestionId).toString().equals("")? "0":request.getParameter(empquestionId).toString();
	            
	                 evaluationVO.setEmpId(empId);
		   	         evaluationVO.setEmpEvaluationId(empEvaluationId);
		   	         evaluationVO.setEmpManagerId(empManagerId);
		   	         evaluationVO.setEmpQuestionId(empquestionId);
		   	         evaluationVO.setEvaluationMonth(evaluationMonth);
		   	         evaluationVO.setEvaluationPoints(evaluationPoints);
		   	         
		   	         evaluationCompletePoints += Integer.parseInt(evaluationPoints);
	                 
		   	         empService.insertEvaluation(evaluationVO);
	            
	         }
	         
	         if(evaluationVO.getDecideFinal().equals("Y")) {
	        	 evaluationCompleteVO.setEmpEvaluationCompleteId(empEvaluationCompleteId);
	        	 evaluationCompleteVO.setEmpId(empId);
	        	 evaluationCompleteVO.setEmpManagerId(empManagerId);
	        	 evaluationCompleteVO.setEvaluationCompletePoints(String.valueOf(evaluationCompletePoints));
	        	 evaluationCompleteVO.setEvaluationMonth(evaluationMonth);
	        	 evaluationCompleteVO.setIncludeRatioPoint(String.valueOf(Math.floor(evaluationCompletePoints*Float.parseFloat(userMap.get("ratio").toString()))));
	        	 empService.insertEvaluationComplete(evaluationCompleteVO);
	         }
	         
	         
	        
	      }catch(Exception e){
	         e.printStackTrace();
	      }
	      //ModelAndView mav = new ModelAndView();
	      //mav.setViewName("environment/evaluation-check");
	      String evaluationMonth = request.getParameter("evaluationMonth") == null || request.getParameter("evaluationMonth").toString().equals("") ? WebUtils.getNow("yyyy-MM")  : request.getParameter("evaluationMonth").toString();
	      WebUtils.messageAndRedirectUrl(mav, "등록 되었습니다.", "/environment/evaluation-empList.do?&evaluationMonth="+evaluationMonth);
	      
	      
	      return mav;
	   }   
	   
	   
	
	   
	   
	   
	   
	   @RequestMapping(value = "/evaluation-empList", method = {RequestMethod.POST,RequestMethod.GET})
	   public ModelAndView evaluationEmpList(Locale locale,ModelAndView mav, HttpServletRequest request, 
	         HttpServletResponse response,@ModelAttribute AdminVO adminVO) throws Exception {
	      
	      try{
	   
	    	  
	         HttpSession session = request.getSession();
	         Map userMap = (Map)session.getAttribute("user");
	         
	         
	         String empRole =userMap.get("emp_role").toString();
	         
	         if(!empRole.equals("A")) {
	        	 
	        	 mav.setViewName("redirect:/index.do");
	         }
	          
	         String evaluationMonth = request.getParameter("evaluationMonth") == null || request.getParameter("evaluationMonth").toString().equals("") ? WebUtils.getNow("yyyy-MM")  : request.getParameter("evaluationMonth").toString();
	         String empManagerId= userMap.get("emp_id").toString();
	         
	         
	         Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
	         paramMap.put("evaluationMonth", evaluationMonth);
	         paramMap.put("empManagerId", empManagerId);
	         
	         List<Map <String,Object>> evlautionListMap = empService.selectEvaluationList(paramMap); 
	         
	         mav.addObject("evlautionListMap",evlautionListMap);
	         mav.addObject("paramMap",paramMap);


	         List<Map<String,Object>> evalautionCompleteList = empService.selectEvaluationCompleteList(paramMap);
	         mav.addObject("evalautionCompleteList",evalautionCompleteList);
	        
	   
	         
	         
	      }catch(Exception e){
	         e.printStackTrace();
	      }

	      
	      return mav;
	   }   
	   
	   
	   //직원 쪽지함
	   @RequestMapping(value = "/employeeMessage", method = {RequestMethod.POST,RequestMethod.GET})
	   public ModelAndView employeeMessage(Locale locale,ModelAndView mav, HttpServletRequest request, 
	         HttpServletResponse response) throws Exception {
	      
	      try{
	   
	    	  
	         HttpSession session = request.getSession();
	         Map userMap = (Map)session.getAttribute("user");
	         
	         Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
	         paramMap.put("empId", userMap.get("emp_id").toString());
	        
	         String sendMessage = request.getParameter("sendMessage") == null ? "N" : request.getParameter("sendMessage").toString();
	         paramMap.put("sendMessage", sendMessage);
	         List<Map<String, Object>> employeeMineList =empService.selectEmployeeNoteList(paramMap);
	         //if(userMap.get("emp_id").toString().equals(employeeMineList.get(0).get("emp_id").toString())) {
	             mav.addObject("employeeMineList",employeeMineList);
	        // }
	         
	         mav.addObject("user",userMap);
	         mav.addObject("paramMap",paramMap);
	         
	   
	        // mav.addObject("employeeNoteList",employeeNoteList);
	
	         
	      }catch(Exception e){
	         e.printStackTrace();
	      }

	      
	      return mav;
	   }   
	   
	   
	   //알림창 공지사항 입력
	   @RequestMapping(value = "/insertAlarmBoard", method = {RequestMethod.POST,RequestMethod.GET})
	   @ResponseBody
	   public ResultApi insertAlarmBoard(Locale locale,ModelAndView mav, HttpServletRequest request, 
	         HttpServletResponse response,@ModelAttribute AlarmSocketVO alarmSocketVO) throws Exception {
			
			ResultApi result = new ResultApi();
	      try{
	   
	    	  
	         HttpSession session = request.getSession();
	         Map userMap = (Map)session.getAttribute("user");
	         
	         String textarea = request.getParameter("textarea").toString();
	         String allocationId ="textArea";
	         
	         
	         alarmSocketVO.setAllocationId(allocationId);
	         alarmSocketVO.setText(textarea);
	         alarmSocketVO.setUserId(userMap.get("emp_id").toString());
	         alarmSocketVO.setSocketId("SOC"+UUID.randomUUID().toString().replaceAll("-",""));
	         
	         empService.insertAlarmBoard(alarmSocketVO);
	         
		        
	     
	      }catch(Exception e){
	         e.printStackTrace();
	       result.setResultCode("0001");
	      }

	      
	      return result;
	   }   
	   
	   //직원 쪽지함 삭제 
		@RequestMapping(value = "/deleteEmployeeMessage", method = RequestMethod.POST)
		@ResponseBody
		public ResultApi deleteEmployeeMessage(Locale locale,ModelAndView mav, HttpServletRequest request, 
				HttpServletResponse response) throws Exception {
			
			ResultApi result = new ResultApi();
			try{
				
				//Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				String sendMessage = request.getParameter("sendMessage") == null ? "N" : request.getParameter("sendMessage").toString();
				String[] messageIdArr = request.getParameterValues("messageIdArr");
				
				Map<String, Object> map = new HashMap<String, Object>();
				
				List<String> messageIdList = Arrays.asList(messageIdArr);
				map.put("message_Id_list", messageIdList);
				map.put("sendMessage", sendMessage);
				
				
				if(messageIdList.size() > 0) {
					
					empService.updateMessage(map);
						
					//empService.deleteEmployeeMessage(map);
			
				} 
				
				
				
			}catch(Exception e){
				e.printStackTrace();
				result.setResultCode("0001");
			}

			return result;
		}
		

	   
	}