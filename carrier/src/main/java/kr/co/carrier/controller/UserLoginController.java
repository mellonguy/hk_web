package kr.co.carrier.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.co.carrier.service.AdminService;
import kr.co.carrier.service.CompanyService;
import kr.co.carrier.service.EmpLoginInfoService;
import kr.co.carrier.service.EmployeeService;
import kr.co.carrier.service.UserService;
import kr.co.carrier.utils.BaseAppConstants;
import kr.co.carrier.utils.ResultApi;
import kr.co.carrier.utils.WebUtils;
import kr.co.carrier.vo.EmpLoginInfoVO;
	

@Controller
public class UserLoginController {

	private static final Logger logger = LoggerFactory.getLogger(UserLoginController.class);
	
	@Autowired
	AdminService adminService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	CompanyService companyService;
	
	@Autowired
	EmpLoginInfoService empLoginInfoService;
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */

	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView index(ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception{
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null && userSessionMap.get("emp_id") != null) {
				response.sendRedirect(request.getContextPath()+"/allocation/combination.do");
			}else {
				Map<String,Object> map = new HashMap<String,Object>();
				List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
				mav.addObject("companyList", companyList);	
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return mav;
	}
	
	
	
	
	@RequestMapping(value = "/error", method = { RequestMethod.POST, RequestMethod.GET })
	public ModelAndView error(ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception{
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			WebUtils.messageAndRedirectUrl(mav,"처리 할 수 없는 요청 입니다.", "/allocation/combination.do");
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return mav;
	}
	
	
	
	@RequestMapping(value = "/tmapOpen", method = RequestMethod.GET)
	public ModelAndView tmapOpen(ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception{
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			String tmapAppKey = BaseAppConstants.TMAP_API_APPKEY;
			mav.addObject("tmapAppKey", tmapAppKey);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return mav;
	}
	
	
	
	
	
	
	//   
	@RequestMapping(value = "/updateSessionCompanyId", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updateSessionCompanyId(ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception{
		ResultApi result = new ResultApi();
		try{
			
			HttpSession session = request.getSession();
			String companyId = request.getParameter("companyId").toString();
			
			session.setAttribute("companyId", companyId);
    		
    		Map<String, Object> searchMap = new HashMap<String, Object>();
    		searchMap.put("companyId", companyId);
    		Map<String, Object> company = companyService.selectCompany(searchMap);
    		session.setAttribute("companyName", company.get("company_name").toString());
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return result;
	}
	
	
	
	@RequestMapping(value = "/nopermission", method = RequestMethod.GET)
	public ModelAndView nopermission(ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception{
		
		try{
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return mav;
	}
	
	@RequestMapping(value = "/loginPersist", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi loginPersist(
			HttpServletRequest request, 
			HttpServletResponse response,
			HttpSession session, 
			@RequestParam String empId,
			@RequestParam String empPwd,
			@RequestParam String companyId
			) throws Exception{
			ResultApi result = new ResultApi();
			
			try{

				Map<String, Object> map = new HashMap<String, Object>();
		    	map.put("empId", empId);
		    	map.put("empPwd", empPwd);
		    //	Map<String, Object> userMap = adminService.selectAdmin(map);
		    	Map<String, Object> userMap = employeeService.selectEmployee(map);
		    	
		    	String remoteAddr = request.getHeader("X-FORWARDED-FOR");
    	        if(remoteAddr == null || remoteAddr.length() == 0 || "unknown".equalsIgnoreCase(remoteAddr)) {
    	            remoteAddr = request.getHeader("Proxy-Client-remoteAddr");
    	        }
    	        if(remoteAddr == null || remoteAddr.length() == 0 || "unknown".equalsIgnoreCase(remoteAddr)) {
    	            remoteAddr = request.getHeader("WL-Proxy-Client-remoteAddr");
    	        }
    	        if(remoteAddr == null || remoteAddr.length() == 0 || "unknown".equalsIgnoreCase(remoteAddr)) {
    	            remoteAddr = request.getHeader("HTTP_CLIENT_remoteAddr");
    	        }
    	        if(remoteAddr == null || remoteAddr.length() == 0 || "unknown".equalsIgnoreCase(remoteAddr)) {
    	            remoteAddr = request.getHeader("HTTP_X_FORWARDED_FOR");
    	        }
    	        if(remoteAddr == null || remoteAddr.length() == 0 || "unknown".equalsIgnoreCase(remoteAddr)) {
    	            remoteAddr = request.getRemoteAddr();
    	        }
	    		
    	        
    	        if(remoteAddr.equals("0:0:0:0:0:0:0:1")) { //희윤이 로컬로 로그인했을때
    				result.setResultCode("E002");
    				int hours = 60*60;
			    	session.setAttribute(empId, userMap);	//로그인 성공
		    		session.setAttribute("user", userMap);	
		    		session.setAttribute("listRowCnt", userMap.get("list_row_cnt").toString());
		    		//session.setAttribute("companyId", userMap.get("company_id").toString());
		    		session.setAttribute("companyId", companyId);
		    		
		    		Map<String, Object> searchMap = new HashMap<String, Object>();
		    		searchMap.put("companyId", companyId);
		    		Map<String, Object> company = companyService.selectCompany(searchMap);
		    		session.setAttribute("companyName", company.get("company_name").toString());
		    		session.setMaxInactiveInterval(hours*4);	
		    		
		    		EmpLoginInfoVO empLoginVO = new EmpLoginInfoVO();
		    		//로그인 성공시 로그인 관리 테이블에 insert 한다..
		    		empLoginVO.setEmpJsessionId(session.getId());
		    		
	    	        
	    	        empLoginVO.setEmpId(userMap.get("emp_id").toString());
	    	        empLoginVO.setEmpLoginInfoId("ELI"+UUID.randomUUID().toString().replaceAll("-",""));
	    	        empLoginVO.setEmpLoginIpV4(remoteAddr);
	    	        empLoginVO.setEmpLoginIpV6("");
	    	        empLoginVO.setEmpName(userMap.get("emp_name").toString());
	    	        empLoginInfoService.insertEmpLoginInfo(empLoginVO);
    	        	
    	        }else {
    	        	
    	          
    	        	
    	        	
    	        	
    	        
    	        String ipAddress = remoteAddr.substring(0,remoteAddr.lastIndexOf("."));  
    	        
    	        // System.out.println(shortIp);
    	      // map.put("ipAddress",remoteAddr);

		    	map.put("ipAddress", ipAddress);
		    	Map<String,Object> certMap = empLoginInfoService.selectMonthCertCheck(map);
		    	
		    	//guid
		    	if(userMap == null){
		    		result.setResultCode("E001");
		    	}else{
		    		
		    		
		    		if(certMap == null) {
		    			
		    			result.setResultCode("E006"); //한달 로그인 인증이 되지않을때		
		    			
		    			if(empId.equals("hk0001") || empId.equals("hk0000")) {		    				
		    				result.setResultCode("E002");
		    				
		    				session.setAttribute(empId, userMap);	//로그인 성공
				    		session.setAttribute("user", userMap);	
				    		session.setAttribute("listRowCnt", userMap.get("list_row_cnt").toString());
				    		//session.setAttribute("companyId", userMap.get("company_id").toString());
				    		session.setAttribute("companyId", companyId);
				    		
				    		Map<String, Object> searchMap = new HashMap<String, Object>();
				    		searchMap.put("companyId", companyId);
				    		Map<String, Object> company = companyService.selectCompany(searchMap);
				    		session.setAttribute("companyName", company.get("company_name").toString());
				    		
				    		//60*60=1시간
				    		int hours = 60*60;
				    		//사장님 자동 로그아웃 시간 12시간
				    		if(empId.equals("hk0001") || empId.equals("hk0000")) {
				    			//session.setMaxInactiveInterval(10*60);
				    			session.setMaxInactiveInterval(hours*48);
				    		// 그 이외는 4시간.
				    		}else {
				    			//기존의 4시간.
				    			session.setMaxInactiveInterval(hours*4);	
				    		}
				    		
				    		EmpLoginInfoVO empLoginVO = new EmpLoginInfoVO();
				    		//로그인 성공시 로그인 관리 테이블에 insert 한다..
				    		empLoginVO.setEmpJsessionId(session.getId());
				    		
			    	        
			    	        empLoginVO.setEmpId(userMap.get("emp_id").toString());
			    	        empLoginVO.setEmpLoginInfoId("ELI"+UUID.randomUUID().toString().replaceAll("-",""));
			    	        empLoginVO.setEmpLoginIpV4(remoteAddr);
			    	        empLoginVO.setEmpLoginIpV6("");
			    	        empLoginVO.setEmpName(userMap.get("emp_name").toString());
			    	        empLoginInfoService.insertEmpLoginInfo(empLoginVO);
		    				
		    			}
		    			
		    			
		    			
		    		}else {
		    			
		    			
			    		if(userMap.get("approve_yn").toString().equals("N")) {
			    			result.setResultCode("E003");			//승인 대기중
			    		}else if(userMap.get("emp_status").toString().equals("02")) {
			    			result.setResultCode("E004");			//휴직
			    		}else if(userMap.get("emp_status").toString().equals("03")) {
			    			result.setResultCode("E005");			//퇴직
			    		}else {
			    			
			    	     //  if(remoteAddr.equals("14.63.39.17") || remoteAddr.equals("127.0.0.1") || remoteAddr.equals("0:0:0:0:0:0:0:1") || remoteAddr.equals("192.168.0.1")) {

				    			result.setResultCode("E002");
						    	session.setAttribute(empId, userMap);	//로그인 성공
					    		session.setAttribute("user", userMap);	
					    		session.setAttribute("listRowCnt", userMap.get("list_row_cnt").toString());
					    		//session.setAttribute("companyId", userMap.get("company_id").toString());
					    		session.setAttribute("companyId", companyId);
					    		
					    		Map<String, Object> searchMap = new HashMap<String, Object>();
					    		searchMap.put("companyId", companyId);
					    		Map<String, Object> company = companyService.selectCompany(searchMap);
					    		session.setAttribute("companyName", company.get("company_name").toString());
					    		
					    		//60*60=1시간
					    		int hours = 60*60;
					    		//사장님 자동 로그아웃 시간 12시간
					    		if(empId.equals("hk0001") || empId.equals("hk0000")) {
					    			//session.setMaxInactiveInterval(10*60);
					    			session.setMaxInactiveInterval(hours*48);
					    		// 그 이외는 4시간.
					    		}else {
					    			//기존의 4시간.
					    			session.setMaxInactiveInterval(hours*4);	
					    		}
					    		
					    		EmpLoginInfoVO empLoginVO = new EmpLoginInfoVO();
					    		//로그인 성공시 로그인 관리 테이블에 insert 한다..
					    		empLoginVO.setEmpJsessionId(session.getId());
					    		
				    	        
				    	        empLoginVO.setEmpId(userMap.get("emp_id").toString());
				    	        empLoginVO.setEmpLoginInfoId("ELI"+UUID.randomUUID().toString().replaceAll("-",""));
				    	        empLoginVO.setEmpLoginIpV4(remoteAddr);
				    	        empLoginVO.setEmpLoginIpV6("");
				    	        empLoginVO.setEmpName(userMap.get("emp_name").toString());
				    	        empLoginInfoService.insertEmpLoginInfo(empLoginVO);
				           	
			    	   //     }else {
			    	        	
			    	        //	result.setResultCode("E006");
			    	        	
			       	        	
			    	//        }
			    			
			    			}
			    		
			    		}
		    	
		    		}
    	        }
		    	
			}catch( Exception e){
				result.setResultCode("E999");
				e.printStackTrace();
			}
			
			return result;
	}
	
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpSession session) {
	//	UserSessionVO  userSessionVO  = (UserSessionVO)session.getAttribute(BaseAppConstants.USER_SESSION_KEY);
		//loginManagerService.removeSession(userSessionVO.getUserId());
		session.invalidate();
		return "redirect:/index.do";
	}
	
	
	@RequestMapping(value = "/deadLine", method = { RequestMethod.POST, RequestMethod.GET })
	   @ResponseBody
	       public Map<String,Object> deadLine(ModelAndView mav, HttpServletRequest request, 
	             HttpServletResponse response, String str) throws Exception{
	    
	       Map <String,Object> map = new HashMap<String,Object>();

	       try {
	          
	          HttpSession session = request.getSession();
	          Map userSessionMap = (Map)session.getAttribute("user");
	          
	          String emp_id = userSessionMap.get("emp_id").toString();
	          String now_dt = WebUtils.getNow("yyyy-MM-dd");
	          
	          map.put("emp_work_finish_id", "EWF" +UUID.randomUUID().toString().replaceAll("-", ""));
	          map.put("now_dt", now_dt);
	          map.put("emp_id", emp_id);
	          map.put("successCode", "0000");
	          
	          empLoginInfoService.insertDeadLineCheck(map);
	           
	       }catch(Exception e) {
	          e.printStackTrace();
	          map.put("successCode", "0001");
	       }

	             return map;

	    }

	
	
	
	
	
	
	
	
	
	
	
}
