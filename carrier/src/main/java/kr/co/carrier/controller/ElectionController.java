package kr.co.carrier.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.co.carrier.service.ElectionService;
import kr.co.carrier.service.VoteService;
import kr.co.carrier.utils.PagingUtils;
import kr.co.carrier.utils.ParamUtils;
import kr.co.carrier.utils.ResultApi;
import kr.co.carrier.utils.WebUtils;
import kr.co.carrier.vo.ElectionVO;
import kr.co.carrier.vo.VoteVO;

@Controller
@RequestMapping(value="/election")
public class ElectionController {

	
	@Value("#{appProp['upload.base.path']}")
    private String rootDir;    
    private String subDir = "bbs";	
	
	
	@Autowired
	private ElectionService electionService;
	
	@Autowired
	private VoteService voteService;
	
	 @RequestMapping(value = "/getElectionList", method = {RequestMethod.POST,RequestMethod.GET})
		public ModelAndView getElectionList(Locale locale,ModelAndView mav, HttpServletRequest request, 
				HttpServletResponse response) throws Exception {
			
			try{
				
				HttpSession session = request.getSession();
				Map userSessionMap = (Map)session.getAttribute("user");
				String companyId = session.getAttribute("companyId").toString();
								
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				String searchType = request.getParameter("searchType") == null  ? "" : request.getParameter("searchType").toString();
				String searchWord = request.getParameter("searchWord") == null  ? "" : request.getParameter("searchWord").toString();
				
				
				//String startDt = "";
				//String endDt = "";
				
				//startDt = selectMonth;
				//startDt = startDt.split("-")[0]+"-"+startDt.split("-")[1]+"-01";
				//endDt = selectMonth;
				//endDt = startDt.split("-")[0]+"-"+startDt.split("-")[1]+"-"+WebUtils.getLastDate("yyyy-mm-dd",Integer.parseInt(startDt.split("-")[0]),Integer.parseInt(startDt.split("-")[1]));
				//paramMap.put("startDt", startDt);
				//paramMap.put("endDt", endDt);
				
				
				paramMap.put("searchType", "00");
				paramMap.put("searchWord", searchWord);
				paramMap.put("companyId", companyId);
				
				
				int totalCount = electionService.selectElectionListCount(paramMap);
				PagingUtils.setPageing(request, totalCount, paramMap);
				
				paramMap.put("numOfRows", totalCount);
				
				List<Map<String, Object>> electionList = electionService.selectElectionList(paramMap);
				
				mav.addObject("userSessionMap",  userSessionMap);
				mav.addObject("listData",  electionList);
				mav.addObject("paramMap", paramMap);
				mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
							
			}catch(Exception e){
				e.printStackTrace();
			}
			

			return mav;
		}
	 
	 
	 
	 
	 
	 @RequestMapping(value = "/add-election", method = {RequestMethod.POST,RequestMethod.GET})
		public ModelAndView addElection(Locale locale,ModelAndView mav, HttpServletRequest request, 
				HttpServletResponse response) throws Exception {
			
			try{
				
				HttpSession session = request.getSession();
				Map userSessionMap = (Map)session.getAttribute("user");
				String companyId = session.getAttribute("companyId").toString();
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				String searchType = request.getParameter("searchType") == null  ? "" : request.getParameter("searchType").toString();
				String searchWord = request.getParameter("searchWord") == null  ? "" : request.getParameter("searchWord").toString();
				
				paramMap.put("searchType", "00");
				paramMap.put("searchWord", searchWord);
				paramMap.put("companyId", companyId);
				
				int totalCount = electionService.selectElectionListCount(paramMap);
				PagingUtils.setPageing(request, totalCount, paramMap);
				
				paramMap.put("numOfRows", totalCount);
				
				List<Map<String, Object>> electionList = electionService.selectElectionList(paramMap);
				
				mav.addObject("userSessionMap",  userSessionMap);
				mav.addObject("listData",  electionList);
				mav.addObject("paramMap", paramMap);
				mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
							
			}catch(Exception e){
				e.printStackTrace();
			}
			

			return mav;
		}
	
	
	 @RequestMapping(value = "/insert-election", method = {RequestMethod.POST,RequestMethod.GET})
		public ModelAndView insertElection(Locale locale,ModelAndView mav, HttpServletRequest request, 
				HttpServletResponse response,@ModelAttribute ElectionVO electionVO) throws Exception {
			
			try{
				
				
				HttpSession session = request.getSession();
				Map userSessionMap = (Map)session.getAttribute("user");
				String companyId = session.getAttribute("companyId").toString();
				
				String electionId = "ELE"+UUID.randomUUID().toString().replaceAll("-", "");
				
				if(userSessionMap != null) {
					electionVO.setElectionId(electionId);
					electionVO.setStartTime("00:00:00");
					electionVO.setEndTime("23:59:59");
					electionVO.setRegisterId(userSessionMap.get("emp_id").toString());
					electionVO.setRegisterName(userSessionMap.get("emp_name").toString());
					electionVO.setCompanyId(companyId);
					electionService.insertElection(electionVO);		
					WebUtils.messageAndRedirectUrl(mav, "선거가 등록되었습니다.", "/election/getElectionList.do");
				}
				
			}catch(Exception e){
				e.printStackTrace();
			}
			

			return mav;
		}
	
	
	
	 @RequestMapping(value = "/viewElection", method = {RequestMethod.POST,RequestMethod.GET})
		public ModelAndView viewElection(Locale locale,ModelAndView mav, HttpServletRequest request, 
				HttpServletResponse response) throws Exception {
			
			try{
				
				HttpSession session = request.getSession();
				Map userSessionMap = (Map)session.getAttribute("user");
				
				String electionId = request.getParameter("electionId") == null  ? "" : request.getParameter("electionId").toString();
				
				int voteCount = -1;
				if(userSessionMap != null) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("electionId", electionId);
					map.put("empId", userSessionMap.get("emp_id").toString());
					voteCount = voteService.selectVoteCount(map);
				}
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				paramMap.put("electionId", electionId);
				
				Map<String, Object> election= electionService.selectElection(paramMap);
				mav.addObject("userSessionMap",  userSessionMap);
				mav.addObject("listData",  election);
				mav.addObject("paramMap", paramMap);
				mav.addObject("voteCount", voteCount);
				mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
							
			}catch(Exception e){
				e.printStackTrace();
			}
			

			return mav;
		}
	 
	
	 
	 @RequestMapping(value = "/goVote", method = {RequestMethod.POST,RequestMethod.GET})
		public ModelAndView goVote(Locale locale,ModelAndView mav, HttpServletRequest request, 
				HttpServletResponse response) throws Exception {
			
			try{
				
				HttpSession session = request.getSession();
				Map userSessionMap = (Map)session.getAttribute("user");
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				String electionId = request.getParameter("electionId") == null  ? "" : request.getParameter("electionId").toString();
				
				paramMap.put("electionId", electionId);
				
				
				Map<String, Object> election= electionService.selectElection(paramMap);
				
				List<Map<String, Object>> candidateList = null;
				
				
				if(election.get("candidate_division").toString().equals("A")) {
					
					candidateList = voteService.selectCandidateListA(paramMap);
				}else if(election.get("candidate_division").toString().equals("O")) {
					candidateList = voteService.selectCandidateListO(paramMap);
					
				}else if(election.get("candidate_division").toString().equals("D")) {
					candidateList = voteService.selectCandidateListD(paramMap);
					
				}else if(election.get("candidate_division").toString().equals("S")) {
					candidateList = voteService.selectCandidateListS(paramMap);
					
				}else if(election.get("candidate_division").toString().equals("C")) {
					candidateList = voteService.selectCandidateListC(paramMap);
					
				}
				
				mav.addObject("userSessionMap",  userSessionMap);
				mav.addObject("listData",  candidateList);
				mav.addObject("paramMap", paramMap);
				mav.addObject("election", election);
				
				mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
							
			}catch(Exception e){
				e.printStackTrace();
			}
			

			return mav;
		}
	
	
	 
	 @RequestMapping(value = "/insertVote", method = {RequestMethod.POST})
	 @ResponseBody
		public ResultApi insertVote(Locale locale,ModelAndView mav, HttpServletRequest request, 
				HttpServletResponse response,@ModelAttribute ElectionVO electionVO) throws Exception {
			
		 
		 	ResultApi result = new ResultApi();
		 
			try{
				
				
				HttpSession session = request.getSession();
				Map userSessionMap = (Map)session.getAttribute("user");
				
				String electionId = request.getParameter("electionId").toString();
				String voteInfoList = request.getParameter("voteInfoList").toString();
				
				
				if(userSessionMap != null) {
					
					
					JSONParser jsonParser = new JSONParser();
					JSONObject jsonObject = (JSONObject) jsonParser.parse(voteInfoList);
					JSONArray jsonArray = (JSONArray) jsonObject.get("voteInfoList");
					
					for(int i = 0; i < jsonArray.size(); i++){
						JSONObject voteInfo = (JSONObject)jsonArray.get(i);
						VoteVO voteVO = new VoteVO();	
						voteVO.setElectionId(electionId);
						voteVO.setPickCandidateAssign(voteInfo.get("assign").toString());
						voteVO.setPickCandidateId(voteInfo.get("candidateId").toString());
						voteVO.setPickCandidateName(voteInfo.get("name").toString());
						voteVO.setVoteId("VOT"+UUID.randomUUID().toString().replaceAll("-", ""));
						voteVO.setVoterId(userSessionMap.get("emp_id").toString());
						voteVO.setVoterName(userSessionMap.get("emp_name").toString());
						voteService.insertVote(voteVO);
					}
					
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("electionId", electionId);
					electionService.updateElectionJoinCount(map);
					//WebUtils.messageAndRedirectUrl(mav, "투표가 완료되었습니다.", "/election/getElectionList.do");
					
				}else {
					result.setResultCode("E000");
					//WebUtils.messageAndRedirectUrl(mav, "투표에 실패 하였습니다. 관리자에게 문의 하세요.", "/election/goVote.do?electionId="+electionId);
				}
				
				
			}catch(Exception e){
				e.printStackTrace();
			}
			

			return result;
		}
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	
}
