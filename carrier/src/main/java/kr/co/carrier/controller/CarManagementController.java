package kr.co.carrier.controller;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import kr.co.carrier.mapper.FileMapper;
import kr.co.carrier.service.AlarmTalkService;
import kr.co.carrier.service.CarInfoService;
import kr.co.carrier.service.DriverDepositService;
import kr.co.carrier.service.DriverPaymentMainService;
import kr.co.carrier.service.DriverPaymentService;
import kr.co.carrier.service.DriverService;
import kr.co.carrier.service.FcmService;
import kr.co.carrier.service.FileService;
import kr.co.carrier.service.FileUploadService;
import kr.co.carrier.service.LowViolationService;
import kr.co.carrier.service.PaymentDriverService;
import kr.co.carrier.utils.PagingUtils;
import kr.co.carrier.utils.ParamUtils;
import kr.co.carrier.utils.ResultApi;
import kr.co.carrier.utils.WebUtils;
import kr.co.carrier.vo.CarDetailVO;
import kr.co.carrier.vo.CarInsuranceSubVO;
import kr.co.carrier.vo.CarInsuranceVO;
import kr.co.carrier.vo.CarMaintanceVO;
import kr.co.carrier.vo.DepositDriverVO;
import kr.co.carrier.vo.DriverDepositVO;
import kr.co.carrier.vo.DriverPaymentMainVO;
import kr.co.carrier.vo.DriverPaymentVO;
import kr.co.carrier.vo.EmployeeVO;
import kr.co.carrier.vo.FileVO;
import kr.co.carrier.vo.LowViolationVO;
import kr.co.carrier.vo.PaymentDriverVO;

@Controller
@RequestMapping(value="/carmanagement")
public class CarManagementController {

    @Value("#{appProp['upload.base.path']}")
    private String rootDir;    
    private String subDir = "bbs";  
    
    
    @Autowired
    private LowViolationService lowViolationService;
    
    @Autowired
    private DriverService driverService;
    
    @Autowired
    private FileService fileService;
    
    @Autowired
    private FcmService fcmService;
    
    @Autowired
    private AlarmTalkService alarmTalkService;
    
    @Autowired
    private DriverDepositService driverDepositService;
    
    @Autowired
    private DriverPaymentService driverPaymentService;
    
    @Autowired
    private FileMapper fileMapper;
    
    @Autowired
    private FileUploadService fileUploadService;
    
    @Autowired
    private DriverPaymentMainService driverPaymentMainService;
    
    @Autowired
    private CarInfoService carInfoService;
    
    @Autowired
    private PaymentDriverService paymentDriverService;
    
    @Autowired
    private WebUtils webutils;
    
    
    @RequestMapping(value = "/driverLowViolation", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView employee(Locale locale,ModelAndView mav, HttpServletRequest request, 
            HttpServletResponse response) throws Exception {
        
        try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
            
            Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
            
            String searchType = request.getParameter("searchType") == null  ? "" : request.getParameter("searchType").toString();
            String searchWord = request.getParameter("searchWord") == null  ? "" : request.getParameter("searchWord").toString();
            
            //String selectMonth = request.getParameter("selectMonth") == null || request.getParameter("selectMonth").toString().equals("")  ? WebUtils.getPrevMonth() : request.getParameter("selectMonth").toString();
            String selectMonth = request.getParameter("selectMonth") == null || request.getParameter("selectMonth").toString().equals("")  ? WebUtils.getNow("yyyy-MM") : request.getParameter("selectMonth").toString();
            
            paramMap.put("occurrenceDt", selectMonth);
            
            paramMap.put("searchType", "00");
            paramMap.put("searchWord", searchWord);
            
            int totalCount = lowViolationService.selectDriverListCount(paramMap);
            PagingUtils.setPageing(request, totalCount, paramMap);
            
            paramMap.put("numOfRows", totalCount);
            
            List<Map<String, Object>> driverList = lowViolationService.selectDriverList(paramMap);
            
            mav.addObject("userSessionMap",  userSessionMap);
            mav.addObject("listData",  driverList);
            mav.addObject("paramMap", paramMap);
            mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
                        
        }catch(Exception e){
            e.printStackTrace();
        }
        

        return mav;
    }
    
    @RequestMapping(value = "/lowViolation-reg", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView lowViolationReg(Locale locale,ModelAndView mav, HttpServletRequest request, 
            HttpServletResponse response) throws Exception {
        
        try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
            
            Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
            
            String selectMonth = request.getParameter("selectMonth") == null || request.getParameter("selectMonth").toString().equals("")  ? WebUtils.getNow("yyyy-MM").toString() : request.getParameter("selectMonth").toString();
            String searchWord = request.getParameter("searchWord") == null || request.getParameter("searchWord").toString().equals("")  ? "" : request.getParameter("searchWord").toString();
            String startDt = request.getParameter("startDt") == null || request.getParameter("startDt").toString().equals("")  ? "" : request.getParameter("startDt").toString();;
            String endDt = request.getParameter("endDt") == null || request.getParameter("endDt").toString().equals("")  ? "" : request.getParameter("endDt").toString();;
            
            //startDt = selectMonth;
            //endDt = selectMonth;
            
            
            if(startDt.equals("")) {
                startDt =selectMonth+"-01";
                endDt = startDt.split("-")[0]+"-"+startDt.split("-")[1]+"-"+WebUtils.getLastDate("yyyy-mm-dd",Integer.parseInt(startDt.split("-")[0]),Integer.parseInt(startDt.split("-")[1]));

            }else {
                startDt = startDt.split("-")[0]+"-"+startDt.split("-")[1]+"-01";
                endDt = endDt.split("-")[0]+"-"+endDt.split("-")[1]+"-"+WebUtils.getLastDate("yyyy-mm-dd",Integer.parseInt(endDt.split("-")[0]),Integer.parseInt(endDt.split("-")[1]));
            }
            
            
        //  endDt = startDt.split("-")[0]+"-"+startDt.split("-")[1]+"-"+WebUtils.getLastDate("yyyy-mm-dd",Integer.parseInt(startDt.split("-")[0]),Integer.parseInt(startDt.split("-")[1]));
            paramMap.put("startDt", startDt);
            paramMap.put("endDt", endDt);
            paramMap.put("selectMonth", selectMonth);
            paramMap.put("searchWord", searchWord);
            
            int totalCount = lowViolationService.selectLowViolationListCount(paramMap);
            PagingUtils.setPageing(request, totalCount, paramMap);
            
            List<Map<String, Object>> driverList = lowViolationService.selectLowViolationList(paramMap);
            Map<String, Object> driver = driverService.selectDriver(paramMap);
            
            mav.addObject("userSessionMap",  userSessionMap);
            mav.addObject("listData",  driverList);
            mav.addObject("paramMap", paramMap);
            mav.addObject("driver", driver);
            mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
                        
        }catch(Exception e){
            e.printStackTrace();
        }
        

        return mav;
    }
    
    
    
    
    
    
    @RequestMapping(value = "/viewLowViolation", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView driverCal(Locale locale,ModelAndView mav, HttpServletRequest request, 
            HttpServletResponse response) throws Exception {
        
        try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
            
            Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
            
            String selectMonth = request.getParameter("selectMonth") == null || request.getParameter("selectMonth").toString().equals("")  ? WebUtils.getNow("yyyy-MM") : request.getParameter("selectMonth").toString();
            String startDt = request.getParameter("startDt") == null || request.getParameter("startDt").toString().equals("")  ? "" : request.getParameter("startDt").toString();;
            String endDt = request.getParameter("endDt") == null || request.getParameter("endDt").toString().equals("")  ? "" : request.getParameter("endDt").toString();;
        //  String startDt = "";
        //  String endDt = "";
            String driverId =request.getParameter("driverId").toString();
            
            startDt = startDt.split("-")[0]+"-"+startDt.split("-")[1]+"-01";
            endDt = endDt.split("-")[0]+"-"+endDt.split("-")[1]+"-"+WebUtils.getLastDate("yyyy-mm-dd",Integer.parseInt(endDt.split("-")[0]),Integer.parseInt(endDt.split("-")[1]));
        //  startDt = selectMonth;
        //  startDt = startDt.split("-")[0]+"-"+startDt.split("-")[1]+"-01";
        // endDt = selectMonth;
        // = startDt.split("-")[0]+"-"+startDt.split("-")[1]+"-"+WebUtils.getLastDate("yyyy-mm-dd",Integer.parseInt(startDt.split("-")[0]),Integer.parseInt(startDt.split("-")[1]));
            paramMap.put("startDt", startDt);
            paramMap.put("endDt", endDt);
            paramMap.put("selectMonth", selectMonth);
            paramMap.put("driverId", driverId);
            
            int totalCount = lowViolationService.selectLowViolationListCountByDriverId(paramMap);
            PagingUtils.setPageing(request, totalCount, paramMap);
            
            List<Map<String, Object>> driverList = lowViolationService.selectLowViolationListByDriverId(paramMap);
            Map<String, Object> driver = driverService.selectDriver(paramMap);
            
            mav.addObject("userSessionMap",  userSessionMap);
            mav.addObject("listData",  driverList);
            mav.addObject("paramMap", paramMap);
            mav.addObject("driver", driver);
            mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
                        
        }catch(Exception e){
            e.printStackTrace();
        }
        

        return mav;
    }
    
    
    
    @RequestMapping(value = "/insertLowViolation", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView insertLowViolation(Locale locale,ModelAndView mav, HttpServletRequest request, 
            HttpServletResponse response,@ModelAttribute LowViolationVO lowViolationVO) throws Exception {
        
        try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
            
            String selectMonth = request.getParameter("selectMonth").toString();  
            
            
            if(userSessionMap != null){             //사용자 로그인이 되어 있으면
                
                if(lowViolationVO.getDriverId().equals("")) {
                    lowViolationVO.setDriverId("DRI"+UUID.randomUUID().toString().replaceAll("-", ""));
                }
                
                lowViolationVO.setLowViolationId("LVI"+UUID.randomUUID().toString().replaceAll("-", ""));
                lowViolationVO.setAmount(lowViolationVO.getAmount().replaceAll(",", ""));
                lowViolationVO.setRegisterId(userSessionMap.get("emp_id").toString());
                
                lowViolationService.insertLowViolation(lowViolationVO);
                 
                subDir = "lowViolation";
                fileService.insertFile(rootDir, subDir, lowViolationVO.getLowViolationId(), request);
                
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("driverId", lowViolationVO.getDriverId());
                Map<String, Object> driverMap = driverService.selectDriver(map);
                
                if(driverMap != null && driverMap.get("fcm_token") != null && !driverMap.get("fcm_token").toString().equals("")) {
                    
                    /*Map<String, Object> sendMessageMap = new HashMap<String, Object>();
                    sendMessageMap.put("title", "과태료 납부 정보가 도착 했습니다.");
                    sendMessageMap.put("body", "위반일자:"+lowViolationVO.getOccurrenceDt()+",시간:"+lowViolationVO.getOccurrenceTime()+",차량번호:"+lowViolationVO.getCarNum()+",위반장소:"+lowViolationVO.getPlaceAddress()+"납부계좌정보 :"+lowViolationVO.getAccountInfo());
                    sendMessageMap.put("fcm_token", driverMap.get("fcm_token").toString());
                    
                    fcmService.fcmSendMessage(sendMessageMap);      */
                    
                }else {
                    
                    //map.get("customer_phone").toString()
                    
                    Map<String, Object> alarmTalkMap = new HashMap<String, Object>();
                    alarmTalkMap.put("gubun", "9");         //알림톡 발송 구분     1:기사배정,2:상차완료,3:하차완료,4:인계완료,5:탁송완료,6탁송취소,7:배차정보(기사님에게),8:배차승인(배차담당직원)
                    alarmTalkMap.put("driverName",lowViolationVO.getDriverName());
                    alarmTalkMap.put("carNum", lowViolationVO.getCarNum());
                    alarmTalkMap.put("occurrenceDt", lowViolationVO.getOccurrenceDt());
                    alarmTalkMap.put("occurrenceTime", lowViolationVO.getOccurrenceTime());
                    alarmTalkMap.put("placeddress", lowViolationVO.getPlaceAddress());
                    alarmTalkMap.put("summary", lowViolationVO.getSummary());
                    alarmTalkMap.put("amount", lowViolationVO.getAmount());
                    alarmTalkMap.put("accountInfo", lowViolationVO.getAccountInfo());
                    alarmTalkMap.put("endDt", lowViolationVO.getEndDt());
                    alarmTalkMap.put("customer_phone", lowViolationVO.getPhoneNum().replaceAll("-", ""));
                    alarmTalkMap.put("alarmOrSmsYn", "A");
                    
                    alarmTalkService.alarmTalkSends(alarmTalkMap);
                    
                }
                
                //WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/carmanagement/lowViolation-reg.do?driverId="+lowViolationVO.getDriverId()+"&occurrenceDt="+selectMonth);
                WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/carmanagement/lowViolation-reg.do?&selectMonth="+lowViolationVO.getOccurrenceDt().substring(0,7));
                
            }else {
                
                WebUtils.messageAndRedirectUrl(mav, "로그인이 되지 않았습니다. 로그인 화면으로 이동 합니다.", "/index.do");    
                
            }
            
            
        }catch(Exception e){
            e.printStackTrace();
        }
        

        return mav;
    }
    
    
    @RequestMapping(value = "/delete-lowViolation", method = RequestMethod.POST)
    @ResponseBody
    public ResultApi deleteLowViolation(ModelAndView mav,
            Model model,HttpServletRequest request ,HttpServletResponse response
            ) throws Exception{
        
        ResultApi result = new ResultApi();
        try{
        
            HttpSession session = request.getSession(); 
            Map userMap = (Map)session.getAttribute("user");

            String driverId = request.getParameter("driverId").toString();
            String lowViolationId = request.getParameter("lowViolationId").toString();
            
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("driverId",driverId);
            map.put("lowViolationId",lowViolationId);
            lowViolationService.deleteLowViolation(map);
                
        }catch(Exception e){
            e.printStackTrace();
            result.setResultCode("0001");
        }
        
        return result;
    }
    
    
    
    
    
    @RequestMapping(value = "/update-low-violation", method = RequestMethod.POST)
    @ResponseBody
    public ResultApi updateLowViolation(ModelAndView mav,
            Model model,HttpServletRequest request ,HttpServletResponse response
            ) throws Exception{
        
        ResultApi result = new ResultApi();
        try{
        
            HttpSession session = request.getSession(); 
            Map userMap = (Map)session.getAttribute("user");
            
            String lowViolationInfo = request.getParameter("lowViolationInfo").toString();
            String driverId = request.getParameter("driverId").toString();
            String lowViolationId = request.getParameter("lowViolationId").toString();
            
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("driverId",driverId);
            Map<String, Object> driver = driverService.selectDriver(map);
            
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(lowViolationInfo);
            JSONObject lowViolation = (JSONObject) jsonObject.get("lowViolationInfo");  

            LowViolationVO lowViolationVO = new LowViolationVO();
            lowViolationVO.setAmount(lowViolation.get("amount").toString());
            lowViolationVO.setCarNum(lowViolation.get("car_num").toString());
            lowViolationVO.setEndDt(lowViolation.get("end_dt").toString());
            lowViolationVO.setLowViolationId(lowViolationId);
            lowViolationVO.setDriverId(driverId);
            lowViolationVO.setOccurrenceDt(lowViolation.get("occurrence_dt").toString());
            lowViolationVO.setOccurrenceTime(lowViolation.get("occurrence_time").toString());
            lowViolationVO.setPlaceAddress(lowViolation.get("place_address").toString());
            lowViolationVO.setSummary(lowViolation.get("summary").toString());
            
            if(lowViolation.get("phone_num") != null && !lowViolation.get("phone_num").toString().equals("")) {
                lowViolationVO.setPhoneNum(lowViolation.get("phone_num").toString());   
            }else {
                lowViolationVO.setPhoneNum(driver.get("phone_num").toString());
            }
            
            lowViolationService.updateLowViolation(lowViolationVO);
                
        }catch(Exception e){
            e.printStackTrace();
            result.setResultCode("0001");
        }
        
        return result;
    }
    
    
    
    
    
    @RequestMapping(value = "/driverDeposit", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView driverDeposit(Locale locale,ModelAndView mav, HttpServletRequest request, 
            HttpServletResponse response) throws Exception {
        
        try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
            
            Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
            
            String searchType = request.getParameter("searchType") == null  ? "" : request.getParameter("searchType").toString();
            String searchWord = request.getParameter("searchWord") == null  ? "" : request.getParameter("searchWord").toString();
            String completeType = request.getParameter("completeType") == null  ? "" : request.getParameter("completeType").toString();
//            String notcomplete = request.getParameter("notcomplete") == null  ? "" : request.getParameter("notcomplete").toString();
            //String selectMonth = request.getParameter("selectMonth") == null || request.getParameter("selectMonth").toString().equals("")  ? WebUtils.getPrevMonth() : request.getParameter("selectMonth").toString();
            //String selectMonth = request.getParameter("selectMonth") == null || request.getParameter("selectMonth").toString().equals("")  ? WebUtils.getNow("yyyy-MM") : request.getParameter("selectMonth").toString();
            String sameParam = "";
            //paramMap.put("occurrenceDt", selectMonth);

            paramMap.put("searchType", "00");
            paramMap.put("searchWord", searchWord);
            paramMap.put("completeType", completeType);
            
            
            
            int totalCount = driverDepositService.selectDriverDepositListCount(paramMap);
            PagingUtils.setPageing(request, totalCount, paramMap);
            
            //paramMap.put("numOfRows", totalCount);
            
            List<Map<String, Object>> driverList = driverDepositService.selectDriverDepositList(paramMap);
            
            
            mav.addObject("userSessionMap",  userSessionMap);
            mav.addObject("listData",  driverList);
            mav.addObject("paramMap", paramMap);
            mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
                        
        }catch(Exception e){
            e.printStackTrace();
        }
        

        return mav;
    }
    
    
    @RequestMapping(value = "/driverDepositDetail", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView driverDepositDetail(Locale locale,ModelAndView mav, HttpServletRequest request, 
            HttpServletResponse response) throws Exception {
        
try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
            String companyId = session.getAttribute("companyId").toString();
            
            Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
            
            String selectMonth = request.getParameter("selectMonth") == null || request.getParameter("selectMonth").toString().equals("")  ? WebUtils.getNow("yyyy-MM") : request.getParameter("selectMonth").toString();
            String startDt = "";
            String endDt = "";
            
            startDt = selectMonth;
            startDt = startDt.split("-")[0]+"-"+startDt.split("-")[1]+"-01";
            endDt = selectMonth;
            endDt = startDt.split("-")[0]+"-"+startDt.split("-")[1]+"-"+WebUtils.getLastDate("yyyy-mm-dd",Integer.parseInt(startDt.split("-")[0]),Integer.parseInt(startDt.split("-")[1]));
            paramMap.put("startDt", startDt);
            paramMap.put("endDt", endDt);
            paramMap.put("selectMonth", selectMonth);
            paramMap.put("companyId", companyId);
                        
            List<Map<String, Object>> driverDepositList = driverDepositService.selectDriverDepositListDetail(paramMap);
            Map<String, Object> driver = driverService.selectDriver(paramMap);
            
            
            //ca_driver 테이블에 없는경우는 ca_deposit_driver테이블에 있는 기사 정보로 가져온다.....
            if(driver == null) {
                driver = driverDepositService.selectDepositDriver(paramMap);
            }
            
            mav.addObject("userSessionMap",  userSessionMap);
            mav.addObject("listData",  driverDepositList);
            mav.addObject("paramMap", paramMap);
            mav.addObject("driver", driver);
            mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
                        
        }catch(Exception e){
            e.printStackTrace();
        }
        

        return mav;
    }
    
    @RequestMapping(value = "/insertDepositDriver", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView insert_DepositDriver(Locale locale,ModelAndView mav, HttpServletRequest request, 
            HttpServletResponse response ,DepositDriverVO depositDriverVO) throws Exception {

        try{
            
            Map<String, Object> map = new HashMap<String, Object>();
            
            HttpSession session = request.getSession();
            Map userMap = (Map)session.getAttribute("user");

            String driverId = "DRI"+UUID.randomUUID().toString().replaceAll("-","");
          
            
            depositDriverVO.setDriverId(driverId);
            
            driverDepositService.insertDepositDriver(depositDriverVO);
            
            
            WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/carmanagement/driverDeposit.do");

            
            
        }catch(Exception e){
            e.printStackTrace();
    
        }

        return mav;
    }
    
    @RequestMapping(value = "/insertDriverDeposit", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView insertDriverDeposit(Locale locale,ModelAndView mav, HttpServletRequest request, 
            HttpServletResponse response,@ModelAttribute DriverDepositVO driverDepositVO) throws Exception {
        
        try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
            
            
            if(userSessionMap != null){             //사용자 로그인이 되어 있으면
                
                driverDepositVO.setDriverDepositId("DDP"+UUID.randomUUID().toString().replaceAll("-", ""));
                driverDepositVO.setDriverDeposit(driverDepositVO.getDriverDeposit().replaceAll(",", ""));
                driverDepositVO.setRegisterId(userSessionMap.get("emp_id").toString());
                driverDepositService.insertDriverDeposit(driverDepositVO);
                            
                subDir = "driverDeposit";
                fileService.insertFile(rootDir, subDir, driverDepositVO.getDriverDepositId(), request);
                
                WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/carmanagement/driverDepositDetail.do?&driverId="+driverDepositVO.getDriverId());
                
            }
            
            
        }catch(Exception e){
            e.printStackTrace();
        }
        

        return mav;
    }
    
    
    @RequestMapping(value = "/viewDepositFile", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView viewDepositFile(Locale locale,ModelAndView mav, HttpServletRequest request, 
            HttpServletResponse response,@ModelAttribute DriverDepositVO driverDepositVO) throws Exception {
        
        try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
            
            String driverDepositId = request.getParameter("driverDepositId") != null ? request.getParameter("driverDepositId").toString():"";
            String categoryType = request.getParameter("categoryType").toString();
            
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("contentId", driverDepositId);
            map.put("categoryType", categoryType);
            
            List<Map<String, Object>> fileList = fileService.selectFileList(map);
            mav.addObject("fileList", fileList);
            
        }catch(Exception e){
            e.printStackTrace();
        }
        

        return mav;
    }
    
    
    
    
    /*
    @RequestMapping(value = "/updateDriverDeposit", method = RequestMethod.POST)
    @ResponseBody
    public ResultApi updateDriverDeposit(ModelAndView mav,
            Model model,HttpServletRequest request ,HttpServletResponse response
            ) throws Exception{
        
        ResultApi result = new ResultApi();
        try{
        
            HttpSession session = request.getSession(); 
            Map userMap = (Map)session.getAttribute("user");
            
            String driverDepositInfo = request.getParameter("driverDepositInfo").toString();
            String driverId = request.getParameter("driverId").toString();
            String driverDepositId = request.getParameter("driverDepositId").toString();
            
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("driverId",driverId);
            Map<String, Object> driver = driverService.selectDriver(map);
            
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(driverDepositInfo);
            JSONObject driverDeposit = (JSONObject) jsonObject.get("driverDepositInfo");    

            DriverDepositVO driverDepositVO = new DriverDepositVO();
            
            driverDepositVO.setDriverDeposit(driverDeposit.get("driver_deposit").toString());
            driverDepositVO.setDriverDepositId(driverDepositId);
            driverDepositVO.setDriverId(driver.get("driver_id").toString());
            driverDepositVO.setDriverName(driver.get("driver_name").toString());
            driverDepositVO.setGuideDt(driverDeposit.get("guide_dt").toString());
            driverDepositVO.setIoAccount(driverDeposit.get("io_account").toString());
            driverDepositVO.setIoDt(driverDeposit.get("io_dt").toString());
            driverDepositVO.setIoStatus(driverDeposit.get("io_status").toString());
            driverDepositVO.setReturnTargetYn("N");
            driverDepositVO.setSummary(driverDeposit.get("summary").toString());
            driverDepositService.updateDriverDeposit(driverDepositVO);
                
        }catch(Exception e){
            e.printStackTrace();
            result.setResultCode("0001");
        }
        
        return result;
    }
    */
    
    
    @RequestMapping(value = "/updateDriverDeposit", method = RequestMethod.POST)
    public ModelAndView updateDriverDeposit(ModelAndView mav,
            Model model,HttpServletRequest request ,HttpServletResponse response,@ModelAttribute DriverDepositVO driverDepositVO
            ) throws Exception{
        
        ResultApi result = new ResultApi();
        try{
        
            HttpSession session = request.getSession(); 
            Map userMap = (Map)session.getAttribute("user");

            driverDepositService.updateDriverDeposit(driverDepositVO);
                
            subDir = "driverDeposit";
            fileService.insertFile(rootDir, subDir, driverDepositVO.getDriverDepositId(), request);
            
            WebUtils.messageAndRedirectUrl(mav, "수정되었습니다.", "/carmanagement/driverDepositDetail.do?&driverId="+driverDepositVO.getDriverId());
            
            
        }catch(Exception e){
            e.printStackTrace();
            result.setResultCode("0001");
        }
        
        return mav;
    }
    
    
    
    
    @RequestMapping(value = "/deleteDriverDeposit", method = RequestMethod.POST)
    @ResponseBody
    public ResultApi deleteDriverDeposit(ModelAndView mav,
            Model model,HttpServletRequest request ,HttpServletResponse response
            ) throws Exception{
        
        ResultApi result = new ResultApi();
        try{
        
            HttpSession session = request.getSession(); 
            Map userMap = (Map)session.getAttribute("user");

            String driverId = request.getParameter("driverId").toString();
            String driverDepositId = request.getParameter("driverDepositId").toString();
            
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("driverId",driverId);
            map.put("driverDepositId",driverDepositId);
            driverDepositService.deleteDriverDeposit(map);
                
        }catch(Exception e){
            e.printStackTrace();
            result.setResultCode("0001");
        }
        
        return result;
    }
    
    
    @RequestMapping(value = "/insertPaymentDriver", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView insert_PaymentDriver(Locale locale,ModelAndView mav, HttpServletRequest request, 
            HttpServletResponse response ,PaymentDriverVO paymentDriverVO ,DriverPaymentVO driverPaymentVO) throws Exception {

        try{

            
            Map<String, Object> map = new HashMap<String, Object>();
            
            HttpSession session = request.getSession();
            Map userMap = (Map)session.getAttribute("user");

            String paymentDriverId = "PDI"+UUID.randomUUID().toString().replaceAll("-", "");
            String driverId = "DRI"+UUID.randomUUID().toString().replaceAll("-","");
            String registerId = userMap.get("emp_id").toString(); 
            String registerName= userMap.get("emp_name").toString();
    
            driverPaymentService.insertPaymentDriver(paymentDriverVO);
            

//          int  result = WebUtils.getMonthsDifference(paymentDriverVO.getJoinDt().substring(0,paymentDriverVO.getJoinDt().lastIndexOf("-")) , WebUtils.getNow("yyyy-MM") );
//          
//          String paymentMonth= paymentDriverVO.getJoinDt().substring(0,paymentDriverVO.getJoinDt().lastIndexOf("-"));
//          
//          for(int i=0; i<result; i++) {
//              
//              String driverPaymentId ="DPI" + UUID.randomUUID().toString().replaceAll("-","");
//              String nextMonth =WebUtils.addMonth(Integer.parseInt(paymentMonth.split("-")[0]), Integer.parseInt(paymentMonth.split("-")[1]), i);
//              
//              driverPaymentVO.setDriverPaymentId(driverPaymentId);
//              driverPaymentVO .setRegisterId(registerId);
//              driverPaymentVO.setDriverId(driverId);
//              driverPaymentVO.setRegisterName(registerName);
//              driverPaymentVO.setPaymentMonth(nextMonth);
            
                            
                
               driverPaymentService.insertDriverPayment(driverPaymentVO);
                
                
//          }
                            
            WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/carmanagement/carinfo-list.do");

            
            
        }catch(Exception e){
            e.printStackTrace();
    
        }

        return mav;
    }
    
    
    
    
    @RequestMapping(value = "/carinfo-list", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView carInfoList(Locale locale,ModelAndView mav, HttpServletRequest request, 
            HttpServletResponse response) throws Exception {
        
        try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
            
            Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
            
            
            String startDt = request.getParameter("startDt") == null ? "" : request.getParameter("startDt").toString();
            String endDt = request.getParameter("endDt") == null ? "" : request.getParameter("endDt").toString();
            String driverStatus = request.getParameter("driverStatus") == null  ? "" : request.getParameter("driverStatus").toString();

            paramMap.put("driverStatus", driverStatus);
            paramMap.put("startDt", startDt);
            paramMap.put("endDt", endDt);
            
            
            int totalCount = driverPaymentService.selectDriverPaymentOwnerListCount(paramMap);
            PagingUtils.setPageing(request, totalCount, paramMap);
            
            List<Map<String, Object>> carDriverPaymentList = driverPaymentService.selectDriverPaymentOwnerList(paramMap);
            
            mav.addObject("userSessionMap",  userSessionMap);
            mav.addObject("listData",carDriverPaymentList);
            mav.addObject("paramMap", paramMap);
            mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
                        
        }catch(Exception e){
            e.printStackTrace();
        }
        

        return mav;
    }
    
    
    
    @RequestMapping(value = "/carinfo-middle", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView carInfoMiddle(Locale locale,ModelAndView mav, HttpServletRequest request, 
            HttpServletResponse response) throws Exception {
        
     // Map<String,Object> rtMap = new HashMap<String, Object>();
        try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
            String companyId = session.getAttribute("companyId").toString();
        
            
            Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
            
            String startDt = request.getParameter("startDt") == null ? "" : request.getParameter("startDt").toString();
            String endDt = request.getParameter("endDt") == null ? "" : request.getParameter("endDt").toString();
        
            //String selectMonth = request.getParameter("selectMonth") == null || request.getParameter("selectMonth").toString().equals("")  ? WebUtils.getPrevMonth() : request.getParameter("selectMonth").toString();
            //String selectMonth = request.getParameter("selectMonth") == null || request.getParameter("selectMonth").toString().equals("")  ? WebUtils.getNow("yyyy-MM") : request.getParameter("selectMonth").toString();
            String location = request.getParameter("location") == null  ? "" : request.getParameter("location").toString();
            
            paramMap.put("startDt",startDt);
            paramMap.put("endDt",endDt);
            paramMap.put("location",location);

            int totalCount = driverPaymentService.selectDriverPaymentDriverListCount(paramMap);
            PagingUtils.setPageing(request, totalCount, paramMap);
            
            /*List<Map<String,Object>> carPaymnetDriverList = driverPaymentService.selectCarMiddlePaymentDriverList(paramMap);*/
            List<Map<String,Object>> carPaymentDriverList = driverPaymentService.selectDriverPaymentDriverList(paramMap);
            
            Map<String, Object> driverMap = new HashMap<String, Object>();
            
            if(location.equals("sub")) {
                driverMap = paymentDriverService.selectPaymentDriver(paramMap);
            }else {
                driverMap = driverService.selectDriver(paramMap);
            }

            mav.addObject("listData",carPaymentDriverList);
            mav.addObject("paramMap", paramMap);
            mav.addObject("driver", driverMap);
            mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
            
          
         // rtMap.put("data", webutils.convertListToJson(carPaymentDriverList));
        }catch(Exception e){
            e.printStackTrace();
        }
        

        return mav;
    }
    
    //
    
    @RequestMapping(value = "/driverPaymentList", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView driverPaymentList(Locale locale,ModelAndView mav, HttpServletRequest request, 
            HttpServletResponse response) throws Exception {
        
        try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
            String companyId = session.getAttribute("companyId").toString();
        
            
            Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
            
            String searchType = request.getParameter("searchType") == null  ? "" : request.getParameter("searchType").toString();
            String startDt = request.getParameter("startDt") == null ? "" : request.getParameter("startDt").toString();
            String endDt = request.getParameter("endDt") == null ? "" : request.getParameter("endDt").toString();
        
            //String selectMonth = request.getParameter("selectMonth") == null || request.getParameter("selectMonth").toString().equals("")  ? WebUtils.getPrevMonth() : request.getParameter("selectMonth").toString();
            //String selectMonth = request.getParameter("selectMonth") == null || request.getParameter("selectMonth").toString().equals("")  ? WebUtils.getNow("yyyy-MM") : request.getParameter("selectMonth").toString();
            String searchWord = request.getParameter("searchWord") == null  ? "" : request.getParameter("searchWord").toString();
            String location = request.getParameter("location") == null  ? "" : request.getParameter("location").toString();
            
            paramMap.put("searchType",searchType);
            paramMap.put("startDt",startDt);
            paramMap.put("endDt",endDt);
            paramMap.put("searchType",searchType);
            paramMap.put("location",location);

            int totalCount = driverPaymentService.selectDriverPaymentListCount(paramMap);
            PagingUtils.setPageing(request, totalCount, paramMap);
            
            List<Map<String,Object>> carPaymentDriverList = driverPaymentService.selectDriverPaymentList(paramMap);
            
            Map<String, Object> driverMap = new HashMap<String, Object>();
            
            if(location.equals("sub")) {
                driverMap = paymentDriverService.selectPaymentDriver(paramMap);
            }else {
                driverMap = driverService.selectDriver(paramMap);
            }

            mav.addObject("listData",carPaymentDriverList);
            mav.addObject("paramMap", paramMap);
            mav.addObject("driver", driverMap);
            mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
            
        }catch(Exception e){
            e.printStackTrace();
        }
        

        return mav;
    }
    
    
    //해당월 면제 a.jax
    @RequestMapping(value = "/exemptions-month-payment", method = RequestMethod.POST)
    @ResponseBody
    public ResultApi exemptionsMonthPayment(ModelAndView mav,
            Model model,HttpServletRequest request ,HttpServletResponse response
            ) throws Exception{
        
        ResultApi result = new ResultApi();
        try{
        
            HttpSession session = request.getSession(); 
            Map userMap = (Map)session.getAttribute("user");

            String driverPaymentId = request.getParameter("driverPaymentId").toString();
            String paymentYn ="M";
            String driverPayment ="0";
            
            Map<String, Object> map = new HashMap<String, Object>();
            
            map.put("driverPaymentId",driverPaymentId);
            map.put("paymentYn",paymentYn);
            map.put("driverPayment",driverPayment);
            
            driverPaymentService.updateExemptionsPaymentM(map);
                
            
        }catch(Exception e){
            e.printStackTrace();
            result.setResultCode("0001");
        }
        
        return result;
    }
    
    
    
    
    
    
    @RequestMapping(value = "/carinfo-reg", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView carInfoReg(Locale locale,ModelAndView mav, HttpServletRequest request, 
            HttpServletResponse response) throws Exception {
        
        try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
            String companyId = session.getAttribute("companyId").toString();
        
            Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
            
            String searchType = request.getParameter("searchType") == null  ? "" : request.getParameter("searchType").toString();
            String searchWord = request.getParameter("searchWord") == null  ? "" : request.getParameter("searchWord").toString();
           
            //String selectMonth = request.getParameter("selectMonth") == null || request.getParameter("selectMonth").toString().equals("")  ? WebUtils.getPrevMonth() : request.getParameter("selectMonth").toString();
            String selectMonth = request.getParameter("selectMonth") == null || request.getParameter("selectMonth").toString().equals("")  ? WebUtils.getNow("yyyy-MM") : request.getParameter("selectMonth").toString();
            
            paramMap.put("occurrenceDt", selectMonth);
            paramMap.put("searchType", "00");
            paramMap.put("searchWord", searchWord);
            paramMap.put("companyId", companyId);
            
            
            //int totalCount = driverPaymentMainService.selectDriverPaymentMainListCount(paramMap);
            //PagingUtils.setPageing(request, totalCount, paramMap);
            
            //paramMap.put("numOfRows", totalCount);
            
            List<Map<String, Object>> driverPaymentList = driverPaymentService.selectDriverPaymentDetailList(paramMap);
            Map<String, Object> driver = driverService.selectDriver(paramMap);
            
            mav.addObject("userSessionMap",  userSessionMap);
            mav.addObject("listData",  driverPaymentList);
            mav.addObject("paramMap", paramMap);
            mav.addObject("driver", driver);
            mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
                        
        }catch(Exception e){
            e.printStackTrace();
        }
        

        return mav;
    }
    
    
    
    
    @RequestMapping(value = "/insertDriverPaymentReg", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView insertDriverPaymentReg(Locale locale,ModelAndView mav, HttpServletRequest request, 
            HttpServletResponse response,@ModelAttribute DriverPaymentVO driverPaymentVO ) throws Exception {
        
        try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
            
            Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
            
            String companyId = session.getAttribute("companyId").toString();
            String paymentMonth = WebUtils.getNow("yyyy-MM").toString();
            String driverId = request.getParameter("driverId").toString();
            
            if(userSessionMap != null){             //사용자 로그인이 되어 있으면
                
                Map<String, Object> driver = driverService.selectDriver(paramMap);
                driverPaymentVO.setDriverPaymentId("DPI"+UUID.randomUUID().toString().replaceAll("-", ""));
                driverPaymentVO.setDriverId(driverId);
                driverPaymentVO.setDriverName(driver.get("driver_name").toString());
                driverPaymentVO.setRegisterId(userSessionMap.get("emp_id").toString());
                driverPaymentVO.setRegisterName(userSessionMap.get("emp_name").toString());
                
                driverPaymentVO.setPaymentCarId("PCI"+UUID.randomUUID().toString().replaceAll("-", ""));
                driverPaymentVO.setPaymentMonth(paymentMonth);
                
                driverPaymentService.insertDriverPayment(driverPaymentVO);
                
                //driverPaymentMainService.insertDriverPaymentMain(paymentDriverVO);
                
                WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/carmanagement/carinfo-reg.do?&driverId="+paramMap.get("driverId").toString());
                
            }else {
                
                WebUtils.messageAndRedirectUrl(mav, "로그인이 되지 않았습니다. 로그인 화면으로 이동 합니다.", "/index.do");    
                
            }
            
            
        }catch(Exception e){
            e.printStackTrace();
        }
        

        return mav;
    }
    
    
    
    @RequestMapping(value = "/insertDriverPaymentMain", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView insertDriverPaymentMain(Locale locale,ModelAndView mav, HttpServletRequest request, 
            HttpServletResponse response,@ModelAttribute DriverPaymentMainVO driverPaymentMainVO) throws Exception {
        
        try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
              
            String companyId = session.getAttribute("companyId").toString();
            
            
            if(userSessionMap != null){             //사용자 로그인이 되어 있으면
                
                
                driverPaymentMainVO.setDriverPaymentMainId("DPM"+UUID.randomUUID().toString().replaceAll("-", ""));
                driverPaymentMainVO.setRegisterId(userSessionMap.get("emp_id").toString());
                driverPaymentMainVO.setCompanyId(companyId);
                
                
                driverPaymentMainService.insertDriverPaymentMain(driverPaymentMainVO);
                
                //WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/carmanagement/lowViolation-reg.do?driverId="+lowViolationVO.getDriverId()+"&occurrenceDt="+selectMonth);
                WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/carmanagement/carinfo-reg.do");
                
            }else {
                
                WebUtils.messageAndRedirectUrl(mav, "로그인이 되지 않았습니다. 로그인 화면으로 이동 합니다.", "/index.do");    
                
            }
            
            
        }catch(Exception e){
            e.printStackTrace();
        }
        

        return mav;
    }
    
    @RequestMapping(value = "/updateDriverPaymentMain", method = RequestMethod.POST)
    @ResponseBody
    public ResultApi updateDriverPaymentMain(ModelAndView mav,
            Model model,HttpServletRequest request ,HttpServletResponse response
            ) throws Exception{
        
        ResultApi result = new ResultApi();
        try{
        
            HttpSession session = request.getSession(); 
            Map userMap = (Map)session.getAttribute("user");
            
            String driverPaymentMainInfo = request.getParameter("driverPaymentMainInfo").toString();
            String driverPaymentId = request.getParameter("driverPaymentId").toString();
            String paymentDriverId = request.getParameter("paymentDriverId").toString();
            
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(driverPaymentMainInfo);
            JSONObject driverPaymentMain = (JSONObject) jsonObject.get("driverPaymentMainInfo");    

            
            
            DriverPaymentVO driverPaymentVO =new DriverPaymentVO();
            PaymentDriverVO paymentDriverVO = new PaymentDriverVO();
            
            
//              paymentDriverVO.setCarNum(driverPaymentMain.get("car_num").toString());
//              paymentDriverVO.setJoinDt(driverPaymentMain.get("join_dt").toString());
//              paymentDriverVO.setResignDt(driverPaymentMain.get("resign_dt").toString());
//              driverPaymentVO.setPaymentMonth(driverPaymentMain.get("payment_month").toString());
            driverPaymentVO.setDriverPayment(driverPaymentMain.get("driver_payment").toString());
            driverPaymentVO.setDriverPayment(driverPaymentMain.get("payment_yn").toString());
            driverPaymentVO.setDriverPayment(driverPaymentMain.get("etc").toString());
            
            
            driverPaymentService.updateDriverPayment(driverPaymentVO);
            driverPaymentService.updatePaymentDriver(paymentDriverVO);
            
            
        
            
                
        }catch(Exception e){
            e.printStackTrace();
            result.setResultCode("0001");
        }
        
        return result;
    }
    
    
    @RequestMapping(value = "/deleteDriverPaymentMain", method = RequestMethod.POST)
    @ResponseBody
    public ResultApi deleteDriverPaymentMain(ModelAndView mav,
            Model model,HttpServletRequest request ,HttpServletResponse response
            ) throws Exception{
        
        ResultApi result = new ResultApi();
        try{
        
            HttpSession session = request.getSession(); 
            Map userMap = (Map)session.getAttribute("user");

            String driverPaymentMainId = request.getParameter("driverPaymentId").toString();
            
            Map<String, Object> map = new HashMap<String, Object>();
            
            map.put("driverPaymentId",driverPaymentMainId);
            driverPaymentMainService.deleteDriverPaymentMain(map);
                
        }catch(Exception e){
            e.printStackTrace();
            result.setResultCode("0001");
        }
        
        return result;
    }
    
    
    
    
    
    
    @RequestMapping(value = "/driverPayment", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView driverAmount(Locale locale,ModelAndView mav, HttpServletRequest request, 
            HttpServletResponse response) throws Exception {
        
        try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
            
            
            if(userSessionMap != null) {
                        
                Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
                
                String driverPaymentMainId = request.getParameter("driverPaymentMainId") == null  ? "" : request.getParameter("driverPaymentMainId").toString();
                
                paramMap.put("driverPaymentMainId", driverPaymentMainId);
                
                Map<String, Object> driverPaymentMain = driverPaymentMainService.selectDriverPaymentMain(paramMap);
                
                int totalCount = driverPaymentService.selectDriverPaymentListCount(paramMap);
                PagingUtils.setPageing(request, totalCount, paramMap);
                
                paramMap.put("numOfRows", totalCount);
                
                List<Map<String, Object>> driverPaymentList = driverPaymentService.selectDriverPaymentList(paramMap);
                
                mav.addObject("userSessionMap",  userSessionMap);
                mav.addObject("listData",  driverPaymentList);
                mav.addObject("driverPaymentMain",  driverPaymentMain);
                mav.addObject("paramMap", paramMap);
                mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
                
            }
            
                        
        }catch(Exception e){
            e.printStackTrace();
        }
        

        return mav;
    }
    
    
    @RequestMapping(value = "/insertDriverPayment", method = {RequestMethod.POST,RequestMethod.GET})
    @ResponseBody
    public ResultApi insertDriverPayment(Locale locale,ModelAndView mav, HttpServletRequest request, 
            HttpServletResponse response,@ModelAttribute DriverPaymentVO driverPaymentVO) throws Exception {
        
        ResultApi result = new ResultApi();
        
        try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
              
            String companyId = session.getAttribute("companyId").toString();
            
            if(userSessionMap != null){             //사용자 로그인이 되어 있으면
            
                
                
                
                Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
                
                String location = request.getParameter("location") == null  ? "" : request.getParameter("location").toString();

                Map<String, Object> driverMap = new HashMap<String, Object>();
                if(location.equals("sub")) {
                    driverMap = paymentDriverService.selectPaymentDriver(paramMap);
                }else {
                    driverMap = driverService.selectDriver(paramMap);
                }
                
                String paymentInfo = request.getParameter("paymentInfo").toString();
                
                JSONParser jsonParser = new JSONParser();
                JSONObject jsonObject = (JSONObject) jsonParser.parse(paymentInfo);
                JSONObject driverPaymentInfo = (JSONObject) jsonObject.get("paymentInfo");
                
                driverPaymentVO.setDriverPaymentId("DPS"+UUID.randomUUID().toString().replaceAll("-", ""));
                driverPaymentVO.setDriverPayment(driverMap.get("driver_balance").toString().replaceAll(",", ""));
                driverPaymentVO.setDriverId(driverPaymentInfo.get("driverId").toString());
                driverPaymentVO.setDriverName(driverMap.get("driver_name").toString());
                driverPaymentVO.setPaymentYn("Y");
                driverPaymentVO.setEtc("");
                driverPaymentVO.setRegisterId(userSessionMap.get("emp_id").toString());
                driverPaymentVO.setRegisterName(userSessionMap.get("emp_name").toString());
                driverPaymentVO.setPaymentMonth("");
                driverPaymentVO.setPaymentCarId(driverMap.get("car_id_num").toString());
                driverPaymentVO.setDriverPaymentI(driverPaymentInfo.get("driverPaymentI").toString());
                int resultAmount = Integer.parseInt(driverPaymentInfo.get("paymentBalance").toString())-Integer.parseInt(driverPaymentInfo.get("driverPaymentI").toString());
                driverPaymentVO.setPaymentBalance(String.valueOf(resultAmount));
                driverPaymentVO.setPaymentDt(driverPaymentInfo.get("paymentDt").toString());
                
                driverPaymentService.insertDriverPayment(driverPaymentVO);
                
                //WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/carmanagement/lowViolation-reg.do?driverId="+lowViolationVO.getDriverId()+"&occurrenceDt="+selectMonth);
                //WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/carmanagement/carinfo-reg.do?driverId="+driverPaymentVO.getDriverId());
                
                WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/carmanagement/carinfo-middle.do?&businessLicenseNumber="+paramMap.get("businessLicenseNumber").toString()+"&location="+paramMap.get("location").toString());
                
            }else {
                
                WebUtils.messageAndRedirectUrl(mav, "로그인이 되지 않았습니다. 로그인 화면으로 이동 합니다.", "/index.do");    
                
            }
            
            
        }catch(Exception e){
            e.printStackTrace();
        }
        

        return result;
    }
    
    
    @RequestMapping(value = "/updateDriverPayment", method = RequestMethod.POST)
    @ResponseBody
    public ResultApi updateDriverPayment(ModelAndView mav,
            Model model,HttpServletRequest request ,HttpServletResponse response
            ) throws Exception{
        
        ResultApi result = new ResultApi();
        try{
        
            HttpSession session = request.getSession(); 
            Map userMap = (Map)session.getAttribute("user");
            
            String driverPaymentInfo = request.getParameter("driverPaymentInfo").toString();
            String driverPaymentId = request.getParameter("driverPaymentId").toString();
            
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(driverPaymentInfo);
            JSONObject driverPayment = (JSONObject) jsonObject.get("driverPaymentInfo");    

            DriverPaymentVO driverPaymentVO = new DriverPaymentVO();
            
            //driverPaymentVO.setAssignDivision(driverPayment.get("assign_division").toString());
            driverPaymentVO.setDriverPayment(driverPayment.get("driver_payment").toString().replaceAll(",", ""));
            driverPaymentVO.setDriverPaymentId(driverPaymentId);
            driverPaymentVO.setEtc(driverPayment.get("etc").toString());
            //driverPaymentVO.setPaymentEndDt(driverPayment.get("payment_end_dt").toString());
            //driverPaymentVO.setPaymentStartDt(driverPayment.get("payment_start_dt").toString());
            driverPaymentVO.setPaymentYn(driverPayment.get("payment_yn").toString());
            //driverPaymentVO.setWorkKind(driverPayment.get("work_kind").toString());s
            
            driverPaymentService.updateDriverPayment(driverPaymentVO);
                
        }catch(Exception e){
            e.printStackTrace();
            result.setResultCode("0001");
        }
        
        return result;
    }
    
    
    
    
    @RequestMapping(value = "/deleteDriverPayment", method = RequestMethod.POST)
    @ResponseBody
    public ResultApi deleteDriverPayment(ModelAndView mav,
            Model model,HttpServletRequest request ,HttpServletResponse response
            ) throws Exception{
        
        ResultApi result = new ResultApi();
        try{
        
            HttpSession session = request.getSession(); 
            Map userMap = (Map)session.getAttribute("user");

            String driverPaymentId = request.getParameter("driverPaymentId").toString();
            
            Map<String, Object> map = new HashMap<String, Object>();
            
            map.put("driverPaymentId",driverPaymentId);
            driverPaymentService.deleteDriverPayment(map);
                
        }catch(Exception e){
            e.printStackTrace();
            result.setResultCode("0001");
        }
        
        return result;
    }
    
    
    
    
    
    
    
    
    
    @RequestMapping(value = "/updateLowViolation", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView updateLowViolation(Locale locale,ModelAndView mav, HttpServletRequest request, 
            HttpServletResponse response,@ModelAttribute LowViolationVO lowViolationVO) throws Exception {
        
        try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
            
            String selectMonth = request.getParameter("selectMonth").toString();  
            
            
            if(userSessionMap != null){             //사용자 로그인이 되어 있으면
                
                
                lowViolationService.updateLowViolation(lowViolationVO);
                
                MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
                List<MultipartFile> fileList = multipartRequest.getFiles("bbsFile");
                
                if(fileList != null && fileList.size() > 0){        //업로드 되는 파일이 존재 하면........
                    //기존의 파일을 삭제 한다.
                    Map<String, Object> map = new HashMap<String, Object>();
                    map.put("contentId", lowViolationVO.getLowViolationId());
                    map.put("categoryType", "lowViolation");
                    
                    fileService.deleteFile(map);
                }
                
                subDir = "lowViolation";
                fileService.insertFile(rootDir, subDir, lowViolationVO.getLowViolationId(), request);
                
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("driverId", lowViolationVO.getDriverId());
                Map<String, Object> driverMap = driverService.selectDriver(map);
                
                if(driverMap != null) {
                    
                    Map<String, Object> sendMessageMap = new HashMap<String, Object>();
                    sendMessageMap.put("title", "과태료 납부 정보가 수정 되었습니다.");
                    sendMessageMap.put("body", "위반일자:"+lowViolationVO.getOccurrenceDt()+",시간:"+lowViolationVO.getOccurrenceTime()+",차량번호:"+lowViolationVO.getCarNum()+",위반장소:"+lowViolationVO.getPlaceAddress());
                    sendMessageMap.put("fcm_token", driverMap.get("fcm_token").toString());
                    sendMessageMap.put("device_os", driverMap.get("device_os").toString());
                    fcmService.fcmSendMessage(sendMessageMap);      
                    
                    
                }else {
                    
                    //map.get("customer_phone").toString()
                    
                    Map<String, Object> alarmTalkMap = new HashMap<String, Object>();
                    alarmTalkMap.put("gubun", "9");         //알림톡 발송 구분     1:기사배정,2:상차완료,3:하차완료,4:인계완료,5:탁송완료,6탁송취소,7:배차정보(기사님에게),8:배차승인(배차담당직원)
                    alarmTalkMap.put("driverName",lowViolationVO.getDriverName());
                    alarmTalkMap.put("carNum", lowViolationVO.getCarNum());
                    alarmTalkMap.put("occurrenceDt", lowViolationVO.getOccurrenceDt());
                    alarmTalkMap.put("occurrenceTime", lowViolationVO.getOccurrenceTime());
                    alarmTalkMap.put("placeddress", lowViolationVO.getPlaceAddress());
                    alarmTalkMap.put("summary", lowViolationVO.getSummary());
                    alarmTalkMap.put("amount", lowViolationVO.getAmount());
                    alarmTalkMap.put("endDt", lowViolationVO.getEndDt());
                    alarmTalkMap.put("customer_phone", lowViolationVO.getPhoneNum().replaceAll("-", ""));
                    alarmTalkMap.put("alarmOrSmsYn", "A");
                    alarmTalkService.alarmTalkSends(alarmTalkMap);
                    
                }
                
                //WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/carmanagement/lowViolation-reg.do?driverId="+lowViolationVO.getDriverId()+"&occurrenceDt="+selectMonth);
                WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/carmanagement/viewLowViolation.do?&driverId="+lowViolationVO.getDriverId()+"&selectMonth="+selectMonth);
                
            }else {
                
                WebUtils.messageAndRedirectUrl(mav, "로그인이 되지 않았습니다. 로그인 화면으로 이동 합니다.", "/index.do");    
                
            }
            
        }catch(Exception e){
            e.printStackTrace();
        }
        

        return mav;
    }
    
    
    
    @RequestMapping(value = "/carInsurance", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView carInsurance(Locale locale,ModelAndView mav, HttpServletRequest request, 
            HttpServletResponse response) throws Exception {
        
        
        
        try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
            
            Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
            
        
            String searchWord = request.getParameter("searchWord") == null  ? "" : request.getParameter("searchWord").toString();
            String selectMonth = request.getParameter("selectMonth") == null || request.getParameter("selectMonth").toString().equals("")  ? WebUtils.getNow("yyyy-MM") : request.getParameter("selectMonth").toString();
            String nowDate = WebUtils.getNow("yyyy-MM-dd").toString();
        
            
            paramMap.put("occurrenceDt", selectMonth);
            paramMap.put("nowDate", nowDate);
            paramMap.put("searchWord", searchWord);
            paramMap.put("insuranceFlag", "Y");
            
            
            //List<Map<String, Object>> carInsuranceList = carInfoService.selectCarInsuranceList(paramMap);
            
            List<Map<String,Object>> carInsuranceMainList = carInfoService.selectCarInsuranceMainList(paramMap);
            List<Map<String, Object>> carInsuranceSubList = carInfoService.selectCarInsuranceSubList(paramMap);
            
            mav.addObject("userSessionMap",  userSessionMap);
            mav.addObject("listData",  carInsuranceMainList);
            mav.addObject("carInsuranceSubList",  carInsuranceSubList);
            mav.addObject("paramMap", paramMap);
            mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
                        
        }catch(Exception e){
            e.printStackTrace();
        }
        

        return mav;
    }
    

    //차량보험 등록페이지
    @RequestMapping(value = "/add-insurance", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView addInsurance(Locale locale,ModelAndView mav, HttpServletRequest request, 
            HttpServletResponse response) throws Exception {
        
        try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
            
            Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
            
            String searchType = request.getParameter("searchType") == null  ? "" : request.getParameter("searchType").toString();
            String searchWord = request.getParameter("searchWord") == null  ? "" : request.getParameter("searchWord").toString();
            
            //String selectMonth = request.getParameter("selectMonth") == null || request.getParameter("selectMonth").toString().equals("")  ? WebUtils.getPrevMonth() : request.getParameter("selectMonth").toString();
            String selectMonth = request.getParameter("selectMonth") == null || request.getParameter("selectMonth").toString().equals("")  ? WebUtils.getNow("yyyy-MM") : request.getParameter("selectMonth").toString();
            
            paramMap.put("occurrenceDt", selectMonth);
            
            paramMap.put("searchType", "00");
            paramMap.put("searchWord", searchWord);
            
            
            mav.addObject("userSessionMap",  userSessionMap);

            mav.addObject("paramMap", paramMap);
            mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
                        
        }catch(Exception e){
            e.printStackTrace();
        }
        

        return mav;
    }
    
    
    
    
    //차량보험 수정페이지
    @RequestMapping(value = "/edit-insurance", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView editInsurance(Locale locale,ModelAndView mav, HttpServletRequest request, 
            HttpServletResponse response) throws Exception {
        
        try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
            
            Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
            
            String carInsuranceId =request.getParameter("carInsuranceId") == null || request.getParameter("carInsuranceId").toString().equals("")  ? "" : request.getParameter("carInsuranceId").toString();
            String editInsurance = request.getParameter("editInsurance") == null || request.getParameter("editInsurance").toString().equals("") ? "" : request.getParameter("editInsurance").toString();        
            
            
            paramMap.put("editInsurance", editInsurance);           //공차                
            paramMap.put("carInsuranceId", carInsuranceId);
            
        
            Map<String,Object> carInsuranceMap = carInfoService.selectCarInsurance(paramMap);
            List<Map<String, Object>> carInsuranceSubList = carInfoService.selectCarInsuranceSubList(paramMap);
            
            mav.addObject("userSessionMap",  userSessionMap);
            mav.addObject("carInsuranceMap",  carInsuranceMap);
            mav.addObject("listSubData",  carInsuranceSubList);
            mav.addObject("paramMap", paramMap);
            mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
                        
        }catch(Exception e){
            e.printStackTrace();
        }
        

        return mav;
    }
    
    //insert-add-Insurace 차량보험 등록페이지
    
    @RequestMapping(value = "/insert-add-insurance", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView insertAddInsurace(Locale locale,ModelAndView mav, HttpServletRequest request, @ModelAttribute CarInsuranceVO carInsuranceVO,
            @ModelAttribute CarInsuranceSubVO carInsuranceSubVO,
            HttpServletResponse response) throws Exception {
        
        try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
            
            Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
            Map<String,Object> updateMap = new HashMap<String, Object>();
                
            String carInsuranceId ="CII"+UUID.randomUUID().toString().replaceAll("-", ""); //차량보험 부모아이디
            String driverId ="DRI" +UUID.randomUUID().toString().replaceAll("-", ""); //기사 아이디
            String loadInsurance = request.getParameter("loadInsurance").toString(); //적재물보험 가입일 입력 했을 시 Y 입력안했을시 ""
            int count = 2;  
                    
            
            if(loadInsurance.equals("N")) {  //적재물가입일 입력 안했을 시  
                
                carInsuranceVO.setCarInsuranceId(carInsuranceId);
                carInsuranceVO.setDriverId(driverId);
                carInfoService.insertCarIncurance(carInsuranceVO); //차량보험   
                
                String carInsuranceSubId = "CSI"+UUID.randomUUID().toString().replaceAll("-", ""); //차량보험 서브 아이디
                carInsuranceSubVO.setInsuranceKind("T"); 
                carInsuranceSubVO.setCarInsuranceId(carInsuranceId);
                carInsuranceSubVO.setCarInsuranceSubId(carInsuranceSubId);  
                carInsuranceSubVO.setCarInsuranceId(carInsuranceId);
                carInsuranceSubVO.setInsuranceFlag("Y");
                carInsuranceSubVO.setRoundOfEvents("1");
                carInsuranceSubVO.setInsuranceAymentInInstallment(paramMap.get("integratedInsuranceAymentInInstallment").toString());
                carInsuranceSubVO.setInsuranceApplicationContributionFirst(paramMap.get("integratedInsuranceApplicationContributionFirst").toString());
                carInsuranceSubVO.setInsuranceApplicationContributionNext(paramMap.get("integratedInsuranceApplicationContributionNext").toString());
                
                updateMap.put("carInsuranceSubId", carInsuranceSubId);
                carInfoService.insertCarInsuranceSub(carInsuranceSubVO);
                List<Map<String,Object>> paymentClosingList = carInfoService.selectCarInsuranceList(updateMap);
                updateMap.put("paymentClosingDt",paymentClosingList.get(0).get("paymentDate").toString());
                carInfoService.updatePaymentClosingDt(updateMap);
                
                
            }else { //적재물 입력했을시 
                
                carInsuranceVO.setCarInsuranceId(carInsuranceId);
                carInsuranceVO.setDriverId(driverId);
                carInfoService.insertCarIncurance(carInsuranceVO); //차량보험
                
                for(int i=0; i<count; i++) {
                    
                    String carInsuranceSubId = "CSI"+UUID.randomUUID().toString().replaceAll("-", ""); //차량보험 서브 아이디
            
                    carInsuranceSubVO.setCarInsuranceId(carInsuranceId);
                    carInsuranceSubVO.setCarInsuranceSubId(carInsuranceSubId);                      
                
                    
                    if(i>0) {
                        carInsuranceSubVO.setInsuranceKind("J"); //적재물
                        
                     String insuranceRegisterDt = request.getParameter("insuranceRegisterDt2") == null ? "" : request.getParameter("insuranceRegisterDt2").toString(); 
                     String insuranceApplicationContributionFirst = request.getParameter("insuranceApplicationContributionFirst2") == null ? "" : request.getParameter("insuranceApplicationContributionFirst2").toString();
                     String insuranceApplicationContributionNext = request.getParameter("insuranceApplicationContributionNext2") == null ? "" : request.getParameter("insuranceApplicationContributionNext2").toString();
                     String insuranceAymentInInstallment2 = request.getParameter("insuranceAymentInInstallment2") == null ? "" : request.getParameter("insuranceAymentInInstallment2").toString();
                
                     carInsuranceSubVO.setInsuranceFlag("Y");
                     carInsuranceSubVO.setInsuranceRegisterDt(insuranceRegisterDt);
                     carInsuranceSubVO.setInsuranceApplicationContributionFirst(insuranceApplicationContributionFirst);
                     carInsuranceSubVO.setInsuranceApplicationContributionNext(insuranceApplicationContributionNext);
                     carInsuranceSubVO.setInsuranceAymentInInstallment(insuranceAymentInInstallment2);
                     carInsuranceSubVO.setRoundOfEvents("0");
                        
                
                    }else {
                        carInsuranceSubVO.setInsuranceFlag("Y");
                        carInsuranceSubVO.setInsuranceKind("T");    //통합
                        carInsuranceSubVO.setInsuranceRegisterDt(paramMap.get("insuranceRegisterDt").toString());
                        carInsuranceSubVO.setInsuranceApplicationContributionFirst(paramMap.get("integratedInsuranceApplicationContributionFirst").toString());
                        carInsuranceSubVO.setInsuranceApplicationContributionNext(paramMap.get("integratedInsuranceApplicationContributionNext").toString());
                        carInsuranceSubVO.setInsuranceAymentInInstallment(paramMap.get("integratedInsuranceAymentInInstallment").toString());
                        carInsuranceSubVO.setRoundOfEvents("0");
                        
                    }
                    carInfoService.insertCarInsuranceSub(carInsuranceSubVO);
                            
                }
                
                    
            }   
                //납부일 업데이트 
                
            updateMap.put("carInsuranceId", carInsuranceId);
            List<Map<String,Object>> paymentClosingList = carInfoService.selectCarInsuranceList(updateMap);
            for(int x=0; x< paymentClosingList.size(); x++) {
                
                updateMap.put("paymentClosingDt",paymentClosingList.get(x).get("paymentDate").toString());
                updateMap.put("carInsuranceSubId", paymentClosingList.get(x).get("car_insurance_sub_id").toString());
                carInfoService.updatePaymentClosingDt(updateMap);
                
            }
            
            
            WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/carmanagement/carInsurance.do");
                        
        }catch(Exception e){
            e.printStackTrace();
        }
        

        return mav;
    }
    
    // insurance-flas n>y
    @RequestMapping(value = "/insert-Tolerance", method = {RequestMethod.POST,RequestMethod.GET})
    @ResponseBody
    public ResultApi insertTolerance(Locale locale,ModelAndView mav, HttpServletRequest request, 
            HttpServletResponse response) throws Exception {
        ResultApi result = new ResultApi();
        
        try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
            
            Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
            
            
            String gongCarNum = request.getParameter("gongCarNum").toString();
            String insuranceFlag = "N";
            
            
            paramMap.put("gongCarNum", gongCarNum);
            //paramMap.put("insuranceFlag", insuranceFlag);
            
            List<Map<String, Object>> carInsuranceList = carInfoService.selectCarInsuranceList(paramMap);
            Map<String,Object> updateMap = new HashMap<String, Object>();
            
            
            if(carInsuranceList.size() > 0) {
                
                for(int i=0; i<carInsuranceList.size(); i++) {
                    
                    updateMap.put("carInsuranceId", carInsuranceList.get(i).get("car_insurance_id").toString());
                    updateMap.put("insuranceFlag", insuranceFlag);
                    carInfoService.updateCarInsuranceSubFlag(updateMap);
                    
                }
                
                result.setResultCode("0000");
                result.setResultData(carInsuranceList);
                
                }else {
                    
                    result.setResultCode("E001");
                
                }
            
            
        }catch(Exception e){
            e.printStackTrace();
            result.setResultCode("E002");
        }
        
        return result;
    }
    
    
    //공차 등록 계약 해지
    @RequestMapping(value = "/insert-termination", method = {RequestMethod.POST,RequestMethod.GET})
    @ResponseBody
    public ResultApi insertTermination(Locale locale,ModelAndView mav, HttpServletRequest request, 
            HttpServletResponse response) throws Exception {
        ResultApi result = new ResultApi();
        
        try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
            
            Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
            
            
            String gongCarNum = request.getParameter("gongCarNum").toString();
            String insuranceFlag = "N";
            
            
            paramMap.put("gongCarNum", gongCarNum);
            //paramMap.put("insuranceFlag", insuranceFlag);
            
            List<Map<String, Object>> carInsuranceList = carInfoService.selectCarInsuranceList(paramMap);
            
            if(carInsuranceList.size() > 0) {
                
                for(int i=0; i<carInsuranceList.size(); i++) {
                    
                //  carInfoService.updateCarInsuranceSubFlag(insuranceFlag);
                    
                    
                }
                
                result.setResultCode("0000");
                result.setResultData(carInsuranceList);
                
                }else {
                    
                    result.setResultCode("E001");
                
                }
            
            
        }catch(Exception e){
            e.printStackTrace();
            result.setResultCode("E002");
        }
        
        return result;
    }
    
    
    
    
    //업데이트 차량보험 수정
    @RequestMapping(value = "/update-insurance", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView updateInsurance(Locale locale,ModelAndView mav, HttpServletRequest request, 
            HttpServletResponse response,
            @ModelAttribute CarInsuranceVO carInsuranceVO,
            @ModelAttribute CarInsuranceSubVO carInsuranceSubVO) throws Exception {

        
        try{
            
            HttpSession session = request.getSession(); 
            Map userSessionMap = (Map)session.getAttribute("user");
            
            Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
            Map<String,Object> updateMap = new HashMap<String, Object>();
            String carInsuranceId =request.getParameter("carInsuranceId") == null || request.getParameter("carInsuranceId").toString().equals("")  ? "" : request.getParameter("carInsuranceId").toString();
            String editInsurance = request.getParameter("editInsurance") == null || request.getParameter("editInsurance").toString().equals("") ? "" : request.getParameter("editInsurance").toString();        
            String carInsuranceSubId ="";
            String compensationLimit = paramMap.get("compensationLimit") == null ? "" :paramMap.get("compensationLimit") .toString().replaceAll(",", "");
            String barrierRewardDistribution = paramMap.get("barrierRewardDistribution") == null ? "" : paramMap.get("barrierRewardDistribution").toString().replaceAll(",", "");
            
        
            paramMap.put("carInsuranceId", carInsuranceId);
            
            
            Map<String,Object> carInsuranceMap = carInfoService.selectCarInsurance(paramMap);
            List<Map<String, Object>> carInsuranceSubList = carInfoService.selectCarInsuranceSubList(paramMap);
            
            String roundOfEvents = carInsuranceSubList.get(0).get("round_of_events").toString();
            String driverId = carInsuranceMap.get("driver_id").toString();
                
                if(carInsuranceSubList.size()>1) {   //T랑 J가 있을때
                    for(int i=0; i<carInsuranceSubList.size(); i++) {
                        carInsuranceSubVO.setCarInsuranceId(carInsuranceId);
                        String insuranceFlag = carInsuranceSubList.get(i).get("insurance_flag").toString();
                        carInsuranceSubId = carInsuranceSubList.get(i).get("car_insurance_sub_id").toString();
                        
                        
                         if(carInsuranceSubList.get(i).get("insurance_kind").equals("T")) {
                             
                             carInsuranceSubVO.setCarInsuranceSubId(carInsuranceSubId);
                             carInsuranceSubVO.setInsuranceKind("T");
                             carInsuranceSubVO.setInsuranceRegisterDt(paramMap.get("insuranceRegisterDt").toString());
                             carInsuranceSubVO.setInsuranceApplicationContributionFirst(paramMap.get("insuranceApplicationContributionFirst").toString().replaceAll(",", ""));
                             carInsuranceSubVO.setInsuranceApplicationContributionNext(paramMap.get("insuranceApplicationContributionNext").toString().replaceAll(",", ""));
                             carInsuranceSubVO.setInsuranceAymentInInstallment(paramMap.get("integratedInsuranceAymentInInstallment").toString());
                             carInsuranceSubVO.setCompensationLimit(compensationLimit);
                             carInsuranceSubVO.setRoundOfEvents(roundOfEvents);
                             carInsuranceSubVO.setInsuranceFlag(insuranceFlag);
                             carInsuranceSubVO.setTolerance(editInsurance);
                             carInsuranceSubVO.setBarrierRewardDistribution(barrierRewardDistribution);
                             
                             
                             
                             carInsuranceVO.setCarInsuranceId(carInsuranceId);
                             carInsuranceVO.setDriverId(driverId);
                             
                             carInfoService.updateCarInsurance(carInsuranceVO);
                             carInfoService.updateCarInsuranceSub(carInsuranceSubVO);
                             
                             
                             updateMap.put("carInsuranceId", carInsuranceId);
                             List<Map<String,Object>> paymentClosingList = carInfoService.selectCarInsuranceList(updateMap);
                             updateMap.put("paymentClosingDt",paymentClosingList.get(0).get("paymentDate").toString());
                             updateMap.put("carInsuranceSubId", paymentClosingList.get(0).get("car_insurance_sub_id").toString());
                             carInfoService.updatePaymentClosingDt(updateMap);
                                 
                             
                             
                         }else { // J 적재물있을때
                             
                             carInsuranceSubVO.setCarInsuranceSubId(carInsuranceSubId);
                             carInsuranceSubVO.setInsuranceKind("J");
                             carInsuranceSubVO.setInsuranceRegisterDt(paramMap.get("loadInsuranceRegisterDt").toString());
                             carInsuranceSubVO.setInsuranceApplicationContributionFirst(paramMap.get("loadInsuranceApplicationContributionFirst").toString().replaceAll(",", ""));
                             carInsuranceSubVO.setInsuranceApplicationContributionNext(paramMap.get("loadInsuranceApplicationContributionNext").toString().replaceAll(",", ""));
                             carInsuranceSubVO.setInsuranceAymentInInstallment(paramMap.get("insuranceAymentInInstallment").toString());
                             carInsuranceSubVO.setCompensationLimit(compensationLimit);
                             carInsuranceSubVO.setRoundOfEvents(roundOfEvents);
                             carInsuranceSubVO.setInsuranceFlag(insuranceFlag);
                             carInsuranceSubVO.setTolerance(editInsurance);
                             
                             carInsuranceVO.setCarInsuranceId(carInsuranceId);
                             carInsuranceVO.setDriverId(driverId);
                             
                             carInfoService.updateCarInsurance(carInsuranceVO);
                             carInfoService.updateCarInsuranceSub(carInsuranceSubVO);
                             
                             
                             updateMap.put("carInsuranceId", carInsuranceId);
                             List<Map<String,Object>> paymentClosingList = carInfoService.selectCarInsuranceList(updateMap);
                             updateMap.put("paymentClosingDt",paymentClosingList.get(0).get("paymentDate").toString());
                             updateMap.put("carInsuranceSubId", paymentClosingList.get(0).get("car_insurance_sub_id").toString());
                             carInfoService.updatePaymentClosingDt(updateMap);
                             
                         }
                        
                    }
                    
        
                }else { //적재물 없을때는 insert !
                    
                     carInsuranceSubVO.setCarInsuranceSubId("CSI"+UUID.randomUUID().toString().replaceAll("-", ""));
                     carInsuranceSubVO.setCarInsuranceId(carInsuranceId);
                     carInsuranceSubVO.setInsuranceKind("J");
                     carInsuranceSubVO.setInsuranceRegisterDt(paramMap.get("loadInsuranceRegisterDt").toString());
                     carInsuranceSubVO.setInsuranceApplicationContributionFirst(paramMap.get("loadInsuranceApplicationContributionFirst").toString().replaceAll(",", ""));
                     carInsuranceSubVO.setInsuranceApplicationContributionNext(paramMap.get("loadInsuranceApplicationContributionNext").toString().replaceAll(",", ""));
                     carInsuranceSubVO.setInsuranceFlag(carInsuranceSubList.get(0).get("insurance_flag").toString());
                     carInsuranceSubVO.setTolerance(editInsurance);
                     carInsuranceSubVO.setInsuranceAymentInInstallment(paramMap.get("insuranceAymentInInstallment").toString());
                     carInsuranceSubVO.setCompensationLimit(compensationLimit);
                     
                     carInsuranceVO.setCarInsuranceId(carInsuranceId);
                     carInsuranceVO.setDriverId(driverId);
                     
                     carInfoService.updateCarInsurance(carInsuranceVO);
                     carInfoService.insertCarInsuranceSub(carInsuranceSubVO);
                     
                     updateMap.put("carInsuranceId", carInsuranceId);
                     List<Map<String,Object>> paymentClosingList = carInfoService.selectCarInsuranceList(updateMap);
                     updateMap.put("paymentClosingDt",paymentClosingList.get(0).get("paymentDate").toString());
                     updateMap.put("carInsuranceSubId", paymentClosingList.get(0).get("car_insurance_sub_id").toString());
                     carInfoService.updatePaymentClosingDt(updateMap);
                    
                }
                    
            
            WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/carmanagement/carInsuranceDetail.do?&gongCarNum="+carInsuranceSubList.get(0).get("car_num").toString());          
            
            
        }catch(Exception e){
            e.printStackTrace();
    
        }
        
        return mav;
    }
    
    
    
    
    
    // 차량보험 디테일 페이지
    @RequestMapping(value = "/carInsuranceDetail", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView carInsuranceDetail(Locale locale,ModelAndView mav, HttpServletRequest request, 
            HttpServletResponse response) throws Exception {
        
        try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
            
            Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
            
            String searchType = request.getParameter("searchType") == null  ? "" : request.getParameter("searchType").toString();
            String searchWord = request.getParameter("searchWord") == null  ? "" : request.getParameter("searchWord").toString();
            String gongCarNum = request.getParameter("gongCarNum") == null  ? "" : request.getParameter("gongCarNum").toString();
            String carInsuranceId = request.getParameter("carInsuranceId") == null  ? "" : request.getParameter("carInsuranceId").toString();
            
            paramMap.put("gongCarNum", gongCarNum);
            paramMap.put("carInsuranceId", carInsuranceId);
            paramMap.put("insuranceFlag", "");
            
        
            List<Map<String, Object>> carInsuranceList = carInfoService.selectCarInsuranceList(paramMap);
            List<Map<String, Object>> carInsuranceSubList = carInfoService.selectCarInsuranceSubList(paramMap);
            
            mav.addObject("userSessionMap",  userSessionMap);
            mav.addObject("listData",  carInsuranceList);
            mav.addObject("carInsuranceSubList",  carInsuranceSubList);
            mav.addObject("paramMap", paramMap);
            mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
                        
        }catch(Exception e){
            e.printStackTrace();
        }
        

        return mav;
    }
    
    
    
    //납부완료 a.jax
    @RequestMapping(value = "/updateInsuracneRountOfEvents", method = RequestMethod.POST)
    @ResponseBody
    public ResultApi updateInsuracneRountOfEvents(ModelAndView mav,
            Model model,HttpServletRequest request ,HttpServletResponse response
            ) throws Exception{
        
        ResultApi result = new ResultApi();
        try{
        
            HttpSession session = request.getSession(); 
            Map userMap = (Map)session.getAttribute("user");

            String roundOfEvents = request.getParameter("roundOfEvents").toString(); //회차
            String insuranceSubId = request.getParameter("insuranceSubId").toString(); // 보험아이디
            
            Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
            paramMap.put("insuranceSubId",insuranceSubId);
            
            List<Map<String, Object>> carInsuranceList = carInfoService.selectCarInsuranceList(paramMap);
            
        
            for(int i =0; i<carInsuranceList.size(); i++) {
                
                String paymentDate = carInsuranceList.get(i).get("paymentDate").toString().replaceAll("-", "");
                String paymentClosingDt =carInsuranceList.get(i).get("paymentClosingDt").toString();
                
                //분할 납입 특약 1:일시불 2:6개월 납부 6:2개월납부
                String insuranceAymentInInstallment =carInsuranceList.get(i).get("insurance_ayment_in_installment").toString();
                if(insuranceAymentInInstallment.equals("6")) { //6개월납부
                    
                        int total =Integer.parseInt(roundOfEvents) +1;
                        paramMap.put("paymentClosingDt", paymentClosingDt);
                        paramMap.put("roundOfEvents", String.valueOf(total));
                        carInfoService.updatePaymentCloseDt(paramMap);
                        
                }else if(insuranceAymentInInstallment.equals("2")){ //2개월납부
                    int total =Integer.parseInt(roundOfEvents) +1;
                    paramMap.put("paymentClosingDt", paymentClosingDt);
                    paramMap.put("roundOfEvents", String.valueOf(total));
                    carInfoService.updatePaymentCloseDt(paramMap);
                        
                    
                }else { //일시납
                    
                    result.setResultCode("A001");
                    return result;
                    
                }
            
            }
            
            
        }catch(Exception e){
            e.printStackTrace();
            result.setResultCode("0001");
        }
        
        return result;
    }
    
    
    //차량보험 삭제 
    @RequestMapping(value = "/deleteInsurance", method = RequestMethod.POST)
    @ResponseBody
    public ResultApi deleteInsurance(ModelAndView mav,
            Model model,HttpServletRequest request ,HttpServletResponse response
            ) throws Exception{
        
        ResultApi result = new ResultApi();
        try{
        
            HttpSession session = request.getSession(); 
            Map userMap = (Map)session.getAttribute("user");

            String[] carInsuranceIdArr = request.getParameterValues("carInsuranceIdArr");
            
            List<String> carInsuranceIdList =Arrays.asList(carInsuranceIdArr);
                
            Map<String, Object> map = new HashMap<String, Object>();
            
            map.put("carInsuranceSubId_list",carInsuranceIdList);
            map.put("carInsuranceId_list",carInsuranceIdList);
            
            if(carInsuranceIdList.size() > 1 || carInsuranceIdList.size() ==1) {
                carInfoService.deleteCarInsurance(map);
                carInfoService.deleteCarInsuranceSub(map);
            }
        
            
            
        }catch(Exception e){
            e.printStackTrace();
            result.setResultCode("0001");
        }
        
        return result;
    }
    
    
    
    
    
    @RequestMapping(value = "/accident", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView accident(Locale locale,ModelAndView mav, HttpServletRequest request, 
            HttpServletResponse response) throws Exception {
        
        try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
            
            Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
            
            String searchType = request.getParameter("searchType") == null  ? "" : request.getParameter("searchType").toString();
            String searchWord = request.getParameter("searchWord") == null  ? "" : request.getParameter("searchWord").toString();
            
            //String selectMonth = request.getParameter("selectMonth") == null || request.getParameter("selectMonth").toString().equals("")  ? WebUtils.getPrevMonth() : request.getParameter("selectMonth").toString();
            String selectMonth = request.getParameter("selectMonth") == null || request.getParameter("selectMonth").toString().equals("")  ? WebUtils.getNow("yyyy-MM") : request.getParameter("selectMonth").toString();
            
            paramMap.put("occurrenceDt", selectMonth);
            
            paramMap.put("searchType", "00");
            paramMap.put("searchWord", searchWord);
            
            int totalCount = lowViolationService.selectDriverListCount(paramMap);
            PagingUtils.setPageing(request, totalCount, paramMap);
            
            paramMap.put("numOfRows", totalCount);
            
            List<Map<String, Object>> driverList = lowViolationService.selectDriverList(paramMap);
            
            mav.addObject("userSessionMap",  userSessionMap);
            mav.addObject("listData",  driverList);
            mav.addObject("paramMap", paramMap);
            mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
                        
        }catch(Exception e){
            e.printStackTrace();
        }
        

        return mav;
    }
    
    //차계부 차수정 페이지
    @RequestMapping(value = "/edit-carMaintance", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView editCarMaintance(Locale locale,ModelAndView mav, HttpServletRequest request, 
            HttpServletResponse response) throws Exception {
        
        try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
            
            Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
                
            String code = request.getParameter("code") == null ? "" : request.getParameter("code").toString();

            String carIdNum = request.getParameter("carIdNum") == null ? "" : request.getParameter("carIdNum").toString();
            String carDetailId  =request.getParameter("carDetailId") == null ? "" : request.getParameter("carDetailId").toString();
            
            paramMap.put("code", code);

            paramMap.put("carIdNum", carIdNum);
            paramMap.put("carDetailId", carDetailId);
            
            if(!code.equals("A")) {
                
            //  Map<String,Object> selectCarDetailEMap = carInfoService.selectCarDetailE(paramMap);

                Map<String,Object> selectCarDetailEMap =carInfoService.selectCarDetailMap(paramMap);
                
                String contentId =  selectCarDetailEMap.get("car_detail_id") == null ? "" : selectCarDetailEMap.get("car_detail_id").toString();

                paramMap.put("contentId", contentId);
                paramMap.put("categoryType","car");
                
                List<Map<String, Object>> fileList = fileService.selectFileList(paramMap);
                if(fileList != null && fileList.size() > 0) {
                    
                    selectCarDetailEMap.put("fileList", fileList);  
                }
                
                mav.addObject("selectCarDetailEMap",selectCarDetailEMap);
                
            }
            
            
            mav.addObject("paramMap",paramMap);
        
            
            
            
            
        }catch(Exception e) {
        e.printStackTrace();
            
            
            
            
        }
    
    return mav;
    
    
    
    }
    
    //차계부 차등록 controller
    @RequestMapping(value = "/insert-carDetail", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView insertCarDetail(Locale locale,ModelAndView mav, HttpServletRequest request, @ModelAttribute CarDetailVO carDetailVO, 
            @ModelAttribute CarMaintanceVO  carMaintanceVO,
            HttpServletResponse response) throws Exception {
        
        try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
            
            Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
                
            String code = request.getParameter("code") == null ? "" : request.getParameter("code").toString();

            String carIdNum = request.getParameter("carIdNum") == null ? "" : request.getParameter("carIdNum").toString();
            String carText = request.getParameter("carText") == null ? "" : request.getParameter("carText").toString();
            String driverId = request.getParameter("driverId") == null ? "" : request.getParameter("driverId").toString();
            String driverName = request.getParameter("driverName") == null ? "" : request.getParameter("driverName").toString();
            
            List<MultipartFile>fileList =  multipartRequest.getFiles("bbsFile");
        
            
            
            paramMap.put("code", code);
            paramMap.put("carIdNum", carIdNum);
            paramMap.put("driverId", driverId);
            paramMap.put("driverName", driverName);
            
            String CarDetailId ="CDI"+UUID.randomUUID().toString().replaceAll("-", "");
            carDetailVO.setCarDetailId(CarDetailId);
            carDetailVO.setCarIdNum(paramMap.get("carIdNum").toString());
            
            if(driverId.equals("")) {
                
                carDetailVO.setDriverId("DRI"+UUID.randomUUID().toString().replaceAll("-",""));
                
            }else {
                
                carDetailVO.setDriverId(driverId);
            }
            
            
            carDetailVO.setDriverName(driverName);
            carDetailVO.setCarNum(paramMap.get("carNum").toString());
            carDetailVO.setCarKind(paramMap.get("carKind").toString());
            carDetailVO.setCarOwnerName(paramMap.get("carOwnerName").toString());
            carDetailVO.setCarModelYear(paramMap.get("carModelYear").toString());
            carDetailVO.setCarWeight(paramMap.get("carWeight").toString());
            carDetailVO.setCarText(carText);
            carDetailVO.setRegisterId(userSessionMap.get("emp_id").toString());
            
            
            if(fileList != null && fileList.size() > 0){
                for(MultipartFile mFile : fileList){    
                    if(mFile != null && mFile.getSize() > 0){
                
                        FileVO fileVO = new FileVO();
                        Map<String, Object> fileMap = fileUploadService.upload(rootDir, "dupList", mFile);
                        
                        fileVO.setContentId(CarDetailId);
                        fileVO.setCategoryType("car");
                        fileVO.setFileId("FLE"+UUID.randomUUID().toString().replaceAll("-", ""));
                        fileVO.setFileNm(fileMap.get("ORG_NAME").toString());
                        fileVO.setFilePath(fileMap.get("SAVE_NAME").toString());
                        fileMapper.insertFile(fileVO);  
                        
                        File file = new File(rootDir+"/dupList"+fileMap.get("SAVE_NAME").toString());
                
                
                    }
                }
            }
            
            
            
            carInfoService.insertCarDetail(carDetailVO);
            
            
            WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/carmanagement/carMaintanance-list.do?&searchWord=01");
        
            
            
        }catch(Exception e) {
        e.printStackTrace();
            
            
            
            
        }
    
    return mav;
    
    
    
    }
    
    
    
    
    //차계부 차량 정보 수정 기능
    @RequestMapping(value = "/update-carDetail", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView updateCarDetail(Locale locale,ModelAndView mav, HttpServletRequest request, @ModelAttribute CarDetailVO carDetailVO,
            HttpServletResponse response) throws Exception {
        
        try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;

            Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
            String carIdNum = request.getParameter("carIdNum") == null ? "" : request.getParameter("carIdNum").toString();
            String carText = request.getParameter("carText") == null ? "" : request.getParameter("carText").toString();
            String CarDetailId = request.getParameter("carDetailId") == null ? "" : request.getParameter("carDetailId").toString();
            String driverId = request.getParameter("driverId") == null ? "" : request.getParameter("driverId").toString();
            String driverName = request.getParameter("driverName") == null ? "" : request.getParameter("driverName").toString();
            
            //Map<String,Object> selectCarDetailEMap = carInfoService.selectCarDetailE(paramMap);
            List<MultipartFile>fileList =  multipartRequest.getFiles("bbsFile");
            
            
            paramMap.put("carIdNum", carIdNum);
            paramMap.put("carDetailId", CarDetailId);
            paramMap.put("driverId", driverId);
            paramMap.put("driverName", driverName);
            
            if(driverId.equals("")) {
                carDetailVO.setDriverId("DRI"+UUID.randomUUID().toString().replaceAll("-",""));
                
            }else {
                
                carDetailVO.setDriverId(driverId);
            }
            
            carDetailVO.setCarIdNum(paramMap.get("carIdNum").toString());
            carDetailVO.setCarNum(paramMap.get("carNum").toString());
            carDetailVO.setCarKind(paramMap.get("carKind").toString());
            carDetailVO.setDriverName(driverName);
            carDetailVO.setCarOwnerName(paramMap.get("carOwnerName").toString());
            carDetailVO.setCarModelYear(paramMap.get("carModelYear").toString());
            carDetailVO.setCarWeight(paramMap.get("carWeight").toString());
            carDetailVO.setCarText(carText);
            carDetailVO.setRegisterId(userSessionMap.get("emp_id").toString());
            
            if(fileList != null && fileList.size() > 0){
                for(MultipartFile mFile : fileList){    
                    if(mFile != null && mFile.getSize() > 0){
                        
                        //있으면 delete 하고 다시 insert 하기
                        
                        try{
                        
                            paramMap.put("categoryType","car");
                            paramMap.put("contentId",CarDetailId);
                            fileMapper.deleteFile(paramMap);
                        
                        
                        }catch(Exception e) {
                            e.printStackTrace();
                            
                        }
                        
                        
                        FileVO fileVO = new FileVO();
                        Map<String, Object> fileMap = fileUploadService.upload(rootDir, "dupList", mFile);
                        
                        fileVO.setContentId(CarDetailId);
                        fileVO.setCategoryType("car");
                        fileVO.setFileId("FLE"+UUID.randomUUID().toString().replaceAll("-", ""));
                        fileVO.setFileNm(fileMap.get("ORG_NAME").toString());
                        fileVO.setFilePath(fileMap.get("SAVE_NAME").toString());
                        fileMapper.insertFile(fileVO);  
                        
                        File file = new File(rootDir+"/dupList"+fileMap.get("SAVE_NAME").toString());
                
                        
                
                    }
                }
            }
            
        
            
            //mav.addObject("paramMap",paramMap);
            //mav.addObject("selectCarDetailEMap",selectCarDetailEMap);
            carInfoService.updateCarDetail(carDetailVO);
            
            WebUtils.messageAndRedirectUrl(mav, "수정되었습니다.", "/carmanagement/edit-carMaintance.do?&carDetailId="+CarDetailId);
            
        }catch(Exception e) {
        e.printStackTrace();
            
        
            
            
            
        }
    

        return mav;
    
    
    
    }
    
    //차량등록 기사이름으로 조회
    @RequestMapping(value = "searchForDriverNameList", method = RequestMethod.POST)
    @ResponseBody
    public ResultApi searchForDriverNameList(ModelAndView mav,
            Model model,HttpServletRequest request ,HttpServletResponse response
            ) throws Exception{
        
        ResultApi result = new ResultApi();
        try{
        
            HttpSession session = request.getSession(); 
            Map userMap = (Map)session.getAttribute("user");

            String driverName = request.getParameter("driverName").toString();
        
            
            Map<String, Object> map = new HashMap<String, Object>();
         
            map.put("driverName",driverName);

            List<Map<String,Object>> driverNameList = driverService.selectSearchForDriverNameList(map);
            
            result.setResultData(driverNameList);
            
                
        }catch(Exception e){
            e.printStackTrace();
            result.setResultCode("0001");
        }
        
        return result;
    }
    
    
    
    
    
    //차계부 관리 
    @RequestMapping(value = "/carMaintanance-list", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView carMaintanance(Locale locale,ModelAndView mav, HttpServletRequest request, 
            HttpServletResponse response) throws Exception {
        
        try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
            
            Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
        
            
            String searchWord = request.getParameter("searchWord") == null ? "" : request.getParameter("searchWord").toString();
            String searchType = request.getParameter("searchType") == null ? "" : request.getParameter("searchType").toString();
            String startDt = request.getParameter("startDt") == null ? "" : request.getParameter("startDt").toString();
            String endDt = request.getParameter("endDt") == null ? "" : request.getParameter("endDt").toString();
            String driverName = request.getParameter("driverName") == null ? "" : request.getParameter("driverName").toString();
            
                    
            paramMap.put("searchWord", searchWord);
            paramMap.put("searchType", searchType);
            paramMap.put("startDt", startDt);
            paramMap.put("endDt", endDt);
            paramMap.put("driverName", driverName);
            
            List<Map<String,Object>> carMaintanceList = carInfoService.selectCarMaintananceList(paramMap);
            //List<Map<String,Object>> carDetailList = carInfoService.selectCarDetailList(paramMap); 
            
            
            mav.addObject("listData",carMaintanceList);
            //mav.addObject("detailListData",carDetailList);
            
            mav.addObject("paramMap",paramMap);
            
            
        }catch(Exception e) {
        e.printStackTrace();
            
            
            
            
        }
    
    return mav;
    
    
    
    }
    
    
    //차계부 등록
    @RequestMapping(value = "/carMaintanance-reg", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView carMaintananceReg(Locale locale,ModelAndView mav, HttpServletRequest request, 
            HttpServletResponse response) throws Exception {
        
        try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
            
            Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
            
            
            String searchWord = request.getParameter("searchWord") == null ? "" : request.getParameter("searchWord").toString();
            String searchType = request.getParameter("searchType") == null ? "" : request.getParameter("searchType").toString();
            String startDt = request.getParameter("startDt") == null ? "" : request.getParameter("startDt").toString();
            String endDt = request.getParameter("endDt") == null ? "" : request.getParameter("endDt").toString();
            String carDetailCode=  request.getParameter("carDetailCode") == null ? "" : request.getParameter("carDetailCode").toString();
            String driverId = request.getParameter("driverId") == null ? "" : request.getParameter("driverId").toString();
            
            
            if(carDetailCode.equals("Y")) {
                Map<String,Object> selectCarDetailMap = carInfoService.selectCarDetailMap(paramMap);    
                paramMap.put("driverName",selectCarDetailMap.get("driver_name").toString());
            }else {
                
                Map<String,Object>driverMap = driverService.selectDriver(paramMap); 
                //paramMap.put("driverName",driverMap.get("driver_name").toString());                           
            }
                        
            
            
            
            
            paramMap.put("searchWord", searchWord);
            paramMap.put("searchType", searchType);
            paramMap.put("startDt", startDt);
            paramMap.put("endDt", endDt);
//              paramMap.put("driverName",driverMap.get("driver_name").toString());             
            paramMap.put("carDetailCode",carDetailCode);            
            paramMap.put("driverId",driverId);              
            
            
            
            
            
            List<Map<String,Object>> carMaintanceRegList = carInfoService.selectCarMaintananceRegList(paramMap);
            
                
            
            mav.addObject("paramMap",paramMap);
            mav.addObject("listData",carMaintanceRegList);
            
            
            
            
        }catch(Exception e) {
        e.printStackTrace();
            
            
            
            
        }
    
    return mav;
    
    
    
    }
    
    //차계부 등록
    @RequestMapping(value = "/insertCarMaintanance", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView insertCarMaintanance(Locale locale,ModelAndView mav, HttpServletRequest request, @ModelAttribute CarMaintanceVO carmaintanceVO, 
            HttpServletResponse response) throws Exception {
        
        try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
            
            
            if(userSessionMap != null){ 
                Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
                
                
                
                Map<String,Object> selectCarDetailMap = carInfoService.selectCarDetailMap(paramMap);
                
                if(selectCarDetailMap != null) {
                    carmaintanceVO.setDriverName(selectCarDetailMap.get("driver_name").toString());
                    carmaintanceVO.setCarNum(selectCarDetailMap.get("car_num").toString());
                    carmaintanceVO.setCarAssignCompany(selectCarDetailMap.get("car_kind").toString());
                }else {
                    
                Map<String,Object>driverMap = driverService.selectDriver(paramMap);     
                    
                    //carmaintanceVO.setDriverName(driverMap.get("driver_name").toString());
                    //carmaintanceVO.setCarNum(driverMap.get("car_num").toString());
                    //carmaintanceVO.setCarAssignCompany(driverMap.get("car_assign_company").toString());
                    
                }
     
            
                String carDistance = request.getParameter("carDistance") == null ? "" : request.getParameter("carDistance").toString().replaceAll(",","");
                String carGas = request.getParameter("carGas") == null ? "" : request.getParameter("carGas").toString();
                String carMileage = request.getParameter("carMileage") == null ? "" : request.getParameter("carMileage").toString().replaceAll(",","");
                String carText = request.getParameter("carText") == null ? "" : request.getParameter("carText").toString();
                String searchType = request.getParameter("searchType") == null ? "" : request.getParameter("searchType").toString();
                String carDetailCode =request.getParameter("carDetailCode") == null ? "" : request.getParameter("carDetailCode").toString();
                String driverName =request.getParameter("driverName") == null ? "" : request.getParameter("driverName").toString();
                
                
                carmaintanceVO.setCarMaintananceId("CMI"+UUID.randomUUID().toString().replaceAll("-",""));
                carmaintanceVO.setDriverId(paramMap.get("driverId").toString());
                carmaintanceVO.setCarMaintananceRegdt(paramMap.get("carMaintananceRegdt").toString());
                carmaintanceVO.setDriverName(driverName);
                carmaintanceVO.setCarDivision(paramMap.get("carDivision").toString());
                carmaintanceVO.setCarCategory(paramMap.get("carCategory").toString());
                carmaintanceVO.setCarPayment(paramMap.get("carPayment").toString().replaceAll(",",""));
                carmaintanceVO.setCarSubsidy(paramMap.get("carSubsidy").toString().replaceAll(",",""));
                carmaintanceVO.setCarRepair(paramMap.get("carRepair").toString());
                carmaintanceVO.setCarDistance(carDistance);
                carmaintanceVO.setCarGas(carGas);
                carmaintanceVO.setCarMileage(carMileage);
                carmaintanceVO.setCarText(carText);
        
                
                
                

                    
                  carInfoService.insertCarMaintance(carmaintanceVO);
                  
                  
                  
                  
                 if(!searchType.equals("")) {
                     WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/carmanagement/carMaintanance-reg.do?&driverId="+carmaintanceVO.getDriverId()+"&carNum="+carmaintanceVO.getCarNum()+"&searchType="+searchType+"&carDetailCode="+carDetailCode);
                 }else {
                     
                    WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/carmanagement/carMaintanance-reg.do?&driverId="+carmaintanceVO.getDriverId()+"&carNum="+carmaintanceVO.getCarNum()+"&carDetailCode="+carDetailCode);   
                 }
            
            }else {
                
                WebUtils.messageAndRedirectUrl(mav, "로그인이 되지 않았습니다. 로그인 화면으로 이동 합니다.", "/index.do");    
                
            }
            
            
        }catch(Exception e) {
            WebUtils.messageAndRedirectUrl(mav, "등록하는데 실패 하였습니다.관리자에게 문의 하세요.", "/carmanagement/carMaintanance-list.do");
        e.printStackTrace();
            
            
            
            
        }
    
    return mav;
    
    
    
    }
    
    //차계부 내역 삭제
    @RequestMapping(value = "/delete-carMaintanance", method = RequestMethod.POST)
    @ResponseBody
    public ResultApi deleteCarMaintanance(ModelAndView mav,
            Model model,HttpServletRequest request ,HttpServletResponse response
            ) throws Exception{
        
        ResultApi result = new ResultApi();
        try{
        
            HttpSession session = request.getSession(); 
            Map userMap = (Map)session.getAttribute("user");

            String driverId = request.getParameter("driverId").toString();
            String carMaintananceId = request.getParameter("carMaintananceId").toString();
            
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("driverId",driverId);
            map.put("carMaintananceId", carMaintananceId);
            carInfoService.deleteCarMaintanance(map);
                
        }catch(Exception e){
            e.printStackTrace();
            result.setResultCode("0001");
        }
        
        return result;
    }
    
    
    
    
    //차량 삭제
    @RequestMapping(value = "/delete-carDeatail", method = RequestMethod.POST)
    @ResponseBody
    public ResultApi deleteCarDeatail(ModelAndView mav,
            Model model,HttpServletRequest request ,HttpServletResponse response
            ) throws Exception{
        
        ResultApi result = new ResultApi();
        try{
        
            HttpSession session = request.getSession(); 
            Map userMap = (Map)session.getAttribute("user");

            String carDetailId = request.getParameter("carDetailId").toString();
            Map<String, Object> map = new HashMap<String, Object>();
        
            map.put("carDetailId", carDetailId);
            carInfoService.deleteCarDetail(map);
                
        }catch(Exception e){
            e.printStackTrace();
            result.setResultCode("0001");
        }
        
        return result;
    }
    
    //차계부 수정 
    @RequestMapping(value = "/updateCarMaintanance", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView updateCarMaintanance(Locale locale,ModelAndView mav, HttpServletRequest request, 
            HttpServletResponse response,@ModelAttribute CarMaintanceVO carmaintanceVO) throws Exception {
        
        try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
            
            
            if(userSessionMap != null){             //사용자 로그인이 되어 있으면
                
                Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
                Map<String,Object> driverMap = driverService.selectDriver(paramMap);   
                
                
                String carDistance = request.getParameter("carDistance") == null ? "" : request.getParameter("carDistance").toString().replaceAll(",","");
                String carGas = request.getParameter("carGas") == null ? "" : request.getParameter("carGas").toString();
                String carMileage = request.getParameter("carMileage") == null ? "" : request.getParameter("carMileage").toString().replaceAll(",","");
                String carText = request.getParameter("carText") == null ? "" : request.getParameter("carText").toString();
                String carMaintananceId = request.getParameter("carMaintananceId").toString();
                String searchType = request.getParameter("searchType") == null ? "" : request.getParameter("searchType").toString();
                String carDivision = request.getParameter("carDivision") == null ? "" : request.getParameter("carDivision").toString();
                String carPayment = request.getParameter("carPayment") == null ? "" : request.getParameter("carPayment").toString().replaceAll(",","");
                String carNum = request.getParameter("carNum") == null ? "" : request.getParameter("carNum").toString();
                String driverName = request.getParameter("driverName") == null ? "" : request.getParameter("driverName").toString();
                String carAssignCompany = request.getParameter("carAssignCompany") == null ? "" : request.getParameter("carAssignCompany").toString();
                
                
                //carmaintanceVO.setCarMaintananceId("CMI"+UUID.randomUUID().toString().replaceAll("-",""));
                carmaintanceVO.setDriverId(paramMap.get("driverId").toString());
                carmaintanceVO.setDriverName(driverName);
                carmaintanceVO.setCarNum(carNum);
                carmaintanceVO.setCarMaintananceRegdt(paramMap.get("carMaintananceRegdt").toString());
                carmaintanceVO.setCarDivision(carDivision);
                carmaintanceVO.setCarCategory(paramMap.get("carCategory").toString());
                carmaintanceVO.setCarPayment(carPayment);
                carmaintanceVO.setCarSubsidy(paramMap.get("carSubsidy").toString().replaceAll(",",""));
                carmaintanceVO.setCarRepair(paramMap.get("carRepair").toString());
                carmaintanceVO.setCarDistance(carDistance);
                carmaintanceVO.setCarGas(carGas);
                carmaintanceVO.setCarMileage(carMileage);
                carmaintanceVO.setCarText(carText);
                carmaintanceVO.setCarAssignCompany(carAssignCompany);
                
             carInfoService.updateCarMaintanance(carmaintanceVO);
                
                
             if(!searchType.equals("")) {
                 
                 WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/carmanagement/carMaintanance-reg.do?&driverId="+carmaintanceVO.getDriverId()+"&carNum="+carmaintanceVO.getCarNum()+"&searchType="+searchType);
             }else {
                WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/carmanagement/carMaintanance-reg.do?&driverId="+carmaintanceVO.getDriverId()+"&carNum="+carmaintanceVO.getCarNum());
             }
                
            
            
            }else {
                
                WebUtils.messageAndRedirectUrl(mav, "로그인이 되지 않았습니다. 로그인 화면으로 이동 합니다.", "/index.do");    
                
            }
            
            
        }catch(Exception e){
            e.printStackTrace();
        }
        

        return mav;
    }
    
    // 기사 예치금 관리, 기사 퇴사일 /지급일 등록 action
    @RequestMapping(value = "/updateDepositDriver", method = RequestMethod.POST)
    @ResponseBody
    public ResultApi updateDepositDriver(ModelAndView mav,
            Model model,HttpServletRequest request ,HttpServletResponse response
            ) throws Exception{
        
        ResultApi result = new ResultApi();
        try{
    
            HttpSession session = request.getSession();
            Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
            
            if(paramMap.get("driverName") != null && paramMap.get("driverId") != null ){
                if(!"".equals(paramMap.get("driverName")) && !"".equals(paramMap.get("driverId"))){
                    
                  Map<String,String> updateMap = new HashMap<String, String>();
                  updateMap.put("type", paramMap.get("type").toString());
                  updateMap.put("driverName", paramMap.get("driverName").toString());
                  updateMap.put("driverId", paramMap.get("driverId").toString());
                  if(paramMap.get("resignDate") != null && !"".equals(paramMap.get("resignDate"))) {
                	  	updateMap.put("resignDate", paramMap.get("resignDate").toString());	
                  }
                  if(paramMap.get("paymentDate")  != null && !"".equals(paramMap.get("paymentDate"))) {
                	  updateMap.put("paymentDate", paramMap.get("paymentDate").toString());	
                }                  		
                  
                  
                  if(paramMap.get("driverType") != null && paramMap.get("driverType") != null ){
                	  if("noDepositDriver".equals(paramMap.get("driverType"))) { //ca_driver 업데이트
                		  driverDepositService.updateDriver(updateMap);
                	  }else {
                		  driverDepositService.updateDepositDriver(updateMap); //ca_deposti_driver 업데이트
                	  }
                	  
                  }
                

                }else {
                    result.setResultCode("E001");
                }
                    
            }else {
                result.setResultCode("E001");
            }
            
        }catch(Exception e){
            e.printStackTrace();
            result.setResultCode("0001");
        }
        
        return result;
    }
    
/**
 * 
 * @param locale
 * @param mav
 * @param request
 * @param response
 * @return
 * @throws Exception
 * 차량검사관리List
 */
   	
    @RequestMapping(value = "/carInspectionManage", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView carInspectionManage(Locale locale,ModelAndView mav, HttpServletRequest request, 
            HttpServletResponse response) throws Exception {
        
        
        
        try{
            
            HttpSession session = request.getSession();
            Map userSessionMap = (Map)session.getAttribute("user");
            
            Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
            
        
            String carNum = request.getParameter("searchWord") == null  ? "" : request.getParameter("searchWord").toString();
            String startDt = request.getParameter("startDt") == null || request.getParameter("startDt").toString().equals("")  ? WebUtils.getPrevMonth("yyyy-MM-dd") : request.getParameter("startDt").toString();
            String endDt = request.getParameter("endDt") == null || request.getParameter("endDt").toString().equals("")  ? WebUtils.getNextMonth("yyyy-MM-dd") : request.getParameter("endDt").toString();
            
            paramMap.put("startDt", startDt);
            paramMap.put("endDt", endDt);
            paramMap.put("carNum", carNum);
            
            
            //List<Map<String, Object>> carInsuranceList = carInfoService.selectCarInsuranceList(paramMap);
            
            List<Map<String,Object>> carInsuranceMainList = carInfoService.selectCarInspectionManageList(paramMap);
            
            
            mav.addObject("userSessionMap",  userSessionMap);
            mav.addObject("listData",  carInsuranceMainList);
            mav.addObject("paramMap", paramMap);
            mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
                        
        }catch(Exception e){
            e.printStackTrace();
        }
        

        return mav;
    }
    
    /**
     * 
     * @param locale
     * @param mav
     * @param request
     * @param response
     * @return
     * @throws Exception
     * 차량검사등록
     */
        @RequestMapping(value = "/insertInspection", method = {RequestMethod.POST,RequestMethod.GET})
        public ModelAndView insertInspection(Locale locale,ModelAndView mav, HttpServletRequest request, 
                HttpServletResponse response) throws Exception {
            
            try{
                
                HttpSession session = request.getSession();
                Map userSessionMap = (Map)session.getAttribute("user");
                
                Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
                
                String carNum = request.getParameter("carNum") == null  ? "" : request.getParameter("carNum").toString();
                String startDate = request.getParameter("startDate").toString();
                String endDate = request.getParameter("endDate").toString();
                String driverName = request.getParameter("driverName").toString();
                String bizNum = request.getParameter("bizNum").toString();
                String inspectionDate = request.getParameter("inspectionDate").toString();
                String inspectionPossibleYn = request.getParameter("inspectionPossibleYn").toString();
                
                
                paramMap.put("car_num", carNum);
                paramMap.put("start_date", startDate);
                paramMap.put("end_date", endDate);
                paramMap.put("driver_name", driverName);
                paramMap.put("business_license_number", bizNum);
                paramMap.put("inspection_date", inspectionDate);
                paramMap.put("inspection_possible_yn", inspectionPossibleYn);
                paramMap.put("reg_emp", userSessionMap.get("emp_id").toString());
                
                int rt = carInfoService.insertInspection(paramMap);
                
                mav.addObject("paramMap", paramMap);
                mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
                
                if(rt > 0) {
                    WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/carmanagement/carInspectionManage.do");                    
                }else {
                	WebUtils.messageAndRedirectUrl(mav, "등록하는데 실패 하였습니다.관리자에게 문의 하세요", "/carmanagement/carInspectionManage.do");
                }           
            }catch(Exception e){
            	WebUtils.messageAndRedirectUrl(mav, "등록하는데 실패 하였습니다.관리자에게 문의 하세요.", "/carmanagement/carInspectionManage.do");
                e.printStackTrace();
            }
                return mav;
        }

        /**
         * 
         * @param locale
         * @param mav
         * @param request
         * @param response
         * @return
         * @throws Exception
         * 차량검사등록
         */
            @RequestMapping(value = "/updateInspection", method = {RequestMethod.POST,RequestMethod.GET})
            public ModelAndView updateInspection(Locale locale,ModelAndView mav, HttpServletRequest request, 
                    HttpServletResponse response) throws Exception {
                
                try{
                    
                    HttpSession session = request.getSession();
                    Map userSessionMap = (Map)session.getAttribute("user");
                    
                    Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
                    
                    String carNum = request.getParameter("carNum_mod") == null  ? "" : request.getParameter("carNum_mod").toString();
                    String startDate = request.getParameter("startDate_mod").toString();
                    String endDate = request.getParameter("endDate_mod").toString();
                    String driverName = request.getParameter("driverName_mod").toString();
                    String bizNum = request.getParameter("bizNum_mod").toString();
                    String inspectionDate = request.getParameter("inspectionDate_mod").toString();
                    String inspectionPossibleYn = request.getParameter("inspectionPossibleYn_mod").toString();
                    
                    
                    paramMap.put("car_num", carNum);
                    paramMap.put("start_date", startDate);
                    paramMap.put("end_date", endDate);
                    paramMap.put("driver_name", driverName);
                    paramMap.put("business_license_number", bizNum);
                    paramMap.put("inspection_date", inspectionDate);
                    paramMap.put("inspection_possible_yn", inspectionPossibleYn);
                    paramMap.put("mod_emp", userSessionMap.get("emp_id").toString());
                    
                    carInfoService.updateInspection(paramMap);
                    
                    mav.addObject("paramMap", paramMap);
                    mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
                    
                    
                    WebUtils.messageAndRedirectUrl(mav, "수정되었습니다.", "/carmanagement/carInspectionManage.do");
                         
                }catch(Exception e){
                	WebUtils.messageAndRedirectUrl(mav, "수정하는데 실패 하였습니다.관리자에게 문의 하세요.", "/carmanagement/carInspectionManage.do");
                    e.printStackTrace();
                }
                    return mav;
            }
        
            /* 번호판관리 */ 
            @RequestMapping(value = "/carNumberManage", method = {RequestMethod.POST,RequestMethod.GET})
            public ModelAndView carNumberManage(Locale locale,ModelAndView mav, HttpServletRequest request, 
                    HttpServletResponse response) throws Exception {
                return mav;
            }   
    
    
}
