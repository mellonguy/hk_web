package kr.co.carrier.controller;

import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.co.carrier.service.MessageService;
import kr.co.carrier.utils.ParamUtils;
import kr.co.carrier.utils.ResultApi;
import kr.co.carrier.utils.WebUtils;

@Controller
@RequestMapping(value="/message")
public class MessageController {
	private static final Logger logger = LoggerFactory.getLogger(AllocationController.class);
    
    @Autowired
    private MessageService messageService;
    
    @RequestMapping(value = "/message", method = {RequestMethod.POST,RequestMethod.GET})
  	public ModelAndView message(Locale locale,ModelAndView mav, HttpServletRequest request, 
  			HttpServletResponse response) throws Exception {
  		
  		try{
  			
  			HttpSession session = request.getSession();
  			Map userSessionMap = (Map)session.getAttribute("user");
  			
  			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);  			
  	
  		}catch(Exception e){
  			e.printStackTrace();
  		}
  		return mav;
  	}
    
	 @RequestMapping(value = "/sendMessage", method = {RequestMethod.POST,RequestMethod.GET})
	 @ResponseBody
		public ResultApi sendMessage(Locale locale,ModelAndView mav, HttpServletRequest request, 
				HttpServletResponse response) throws Exception {
		String resultStr = " ";
		ResultApi result = new ResultApi();
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");				
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			if("Y".equals(paramMap.get("alarmtalk"))) {
				messageService.alarmTalkSends(paramMap,userSessionMap);
			}
			if("Y".equals(paramMap.get("sms"))) {
				messageService.lmsSendMessage(paramMap,userSessionMap);
			}
			
			result.setResultCode("0000");
			result.setResultMsg("발송성공");
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
			result.setResultMsg("발송실패");
		}
		

		return result;
	}
	 
	 @RequestMapping(value = "/sendMessageNew", method = {RequestMethod.POST,RequestMethod.GET})
	 @ResponseBody
		public ResultApi sendMessageNew(Locale locale,ModelAndView mav, HttpServletRequest request, 
				HttpServletResponse response) throws Exception {
		String resultStr = " ";
		ResultApi result = new ResultApi();
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");				
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			if("Y".equals(paramMap.get("alarmtalk"))) {
				messageService.alarmTalkSendsNew(paramMap,userSessionMap);
			}
			if("Y".equals(paramMap.get("sms"))) {
				messageService.lmsSendMessageNew(paramMap,userSessionMap);
			}
			
			result.setResultCode("0000");
			result.setResultMsg("발송성공");
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
			result.setResultMsg("발송실패");
		}
		

		return result;
	}	 

}
