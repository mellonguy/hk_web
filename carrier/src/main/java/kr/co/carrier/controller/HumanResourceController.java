package kr.co.carrier.controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.co.carrier.service.CompanyService;
import kr.co.carrier.service.DriverService;
import kr.co.carrier.service.EmployeeService;
import kr.co.carrier.service.FileService;
import kr.co.carrier.utils.PagingUtils;
import kr.co.carrier.utils.ParamUtils;
import kr.co.carrier.utils.ResultApi;
import kr.co.carrier.utils.WebUtils;
import kr.co.carrier.vo.DriverVO;
import kr.co.carrier.vo.EmployeeVO;

@Controller
@RequestMapping(value="/humanResource")
public class HumanResourceController {

	
    @Autowired
    private EmployeeService empService;
	
    @Autowired
    private CompanyService companyService;
    
    @Autowired
    private DriverService driverService;
    
    
    @Autowired
	private FileService fileService;
	
	@Value("#{appProp['upload.base.path']}")
    private String rootDir;    
    private String subDir;
	
    
    @RequestMapping(value = "/employee", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView employee(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			String searchStatus = request.getParameter("searchStatus") == null ? "" : request.getParameter("searchStatus").toString();
			
			if(searchStatus.equals("")) {		//조회 상태가 없으면  재직자 현황으로 조회를 한다.
				searchStatus = "U";		 
			}
			
			/*String startDt = request.getParameter("startDt") == null ? WebUtils.getNow("yyyy-MM-dd") : request.getParameter("startDt").toString();
			String endDt = request.getParameter("endDt") == null ? WebUtils.getNow("yyyy-MM-dd") : request.getParameter("endDt").toString();*/
			
			String startDt = request.getParameter("startDt") == null ? "" : request.getParameter("startDt").toString();
			String endDt = request.getParameter("endDt") == null ? "" : request.getParameter("endDt").toString();
			
			
			paramMap.put("searchStatus", searchStatus);
			paramMap.put("startDt", startDt);
			paramMap.put("endDt", endDt);
			
			int totalCount = empService.selectEmployeeListCount(paramMap);
			PagingUtils.setPageing(request, totalCount, paramMap);
			
			List<Map<String, Object>> empList = empService.selectEmployeeList(paramMap);
			
			for(int i = 0; i < empList.size(); i++) {
				Map<String, Object> emp = empList.get(i);
				
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("contentId", emp.get("emp_id").toString());
	      		map.put("categoryType", "employee");
	      		List<Map<String, Object>> fileList = fileService.selectFileList(map);
	      		//emp.put("fileList", fileList)
	      		empList.get(i).put("fileList", fileList);
			}
			
			mav.addObject("userSessionMap",  userSessionMap);
			mav.addObject("listData",  empList);
			mav.addObject("paramMap", paramMap);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
						
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
    
	
    @RequestMapping(value = "/add-employee", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView addemployee(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			Map<String, Object> weekMap = new HashMap<String, Object>();
			Map<String, Object> monthMap = new HashMap<String, Object>();
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			mav.addObject("userSessionMap",  userSessionMap);
			
			Date date = new Date();
			DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
			String formattedDate = dateFormat.format(date);
			mav.addObject("serverTime", formattedDate );
			
			Map<String,Object> map = new HashMap<String,Object>();
			List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
			mav.addObject("companyList", companyList);
						
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	@RequestMapping(value = "/insert-employee", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView insertEmployee(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute EmployeeVO employeeVO) throws Exception {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(employeeVO.getEmpRole().equals("")) {
				employeeVO.setEmpRole("U");
			}
			
			if(userSessionMap != null){				//사용자 로그인이 되어 있으면
				Map<String, Object> map = new HashMap<String, Object>();
				
				map.put("duplicateChk", "true");
				map.put("empId", employeeVO.getEmpId());
				
				Map<String, Object> employeeMap = empService.selectEmployee(map);
				
				if(employeeMap != null){
					WebUtils.messageAndRedirectUrl(mav, "이미 사용중인 아이디 입니다.", "/baseinfo/add-employee.do");
				}else{
					if(employeeVO.getDeductionRate().equals("")) {
						employeeVO.setDeductionRate("0");
					}
					
					employeeVO.setRegId(userSessionMap.get("emp_id").toString());
					employeeVO.setRegName(userSessionMap.get("emp_name").toString());
					empService.insertEmployee(employeeVO);		
					subDir = "employee";
					fileService.insertFile(rootDir, subDir, employeeVO.getEmpId(), request);
					WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/baseinfo/employee.do");
				}
			}else {						//로그인이 되어 있지 않은경우 로그인 화면으로 이동 한다.
				
				String status = request.getParameter("status") == null ? "": request.getParameter("status").toString();
				
				if(status.equals("noLogin")) {
					Map<String, Object> map = new HashMap<String, Object>();
					
					map.put("duplicateChk", "true");
					map.put("empId", employeeVO.getEmpId());
					
					Map<String, Object> employeeMap = empService.selectEmployee(map);
					
					if(employeeMap != null){
						WebUtils.messageAndRedirectUrl(mav, "이미 사용중인 아이디 입니다.", "/baseinfo/add-employee.do");
					}else{
						if(employeeVO.getDeductionRate().equals("")) {
							employeeVO.setDeductionRate("0");
						}
						employeeVO.setRegId("self");
						employeeVO.setRegName("self");
						empService.insertEmployee(employeeVO);		
						subDir = "employee";
						fileService.insertFile(rootDir, subDir, employeeVO.getEmpId(), request);
						WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/index.do");
					}
				}else {
					WebUtils.messageAndRedirectUrl(mav, "로그인이 되지 않았습니다. 로그인 화면으로 이동 합니다.", "/index.do");	
				}
					
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	
	@RequestMapping(value = "/getEmpData", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi getEmpApproveYn(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		ResultApi result = new ResultApi();
		try{
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			String searchStatus = request.getParameter("searchStatus") == null ? "" : request.getParameter("searchStatus").toString();
			
			if(searchStatus.equals("")) {		//조회 상태가 없으면  재직자 현황으로 조회를 한다.
				searchStatus = "U";
			}
			
			String startDt = request.getParameter("startDt") == null ? WebUtils.getNow("yyyy-MM-dd") : request.getParameter("startDt").toString();
			String endDt = request.getParameter("endDt") == null ? WebUtils.getNow("yyyy-MM-dd") : request.getParameter("endDt").toString();
			
			paramMap.put("searchStatus", searchStatus);
			paramMap.put("startDt", startDt);
			paramMap.put("endDt", endDt);
			
			
			int totalCount = empService.selectEmployeeListCount(paramMap);
			PagingUtils.setPageing(request, totalCount, paramMap);
			
			List<Map<String, Object>> empList = empService.selectEmployeeList(paramMap);
			result.setResultData(empList);
			result.setResultDataSub(paramMap);
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}

		return result;
	}
	
	
	
	@RequestMapping(value = "/add-driver", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView adddriver(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			Map<String,Object> map = new HashMap<String,Object>();
			List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
			mav.addObject("companyList", companyList);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	@RequestMapping(value = "/add-payment-driver", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView addPaymentDriver(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			Map<String,Object> map = new HashMap<String,Object>();
			List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
			mav.addObject("companyList", companyList);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	
	
	@RequestMapping(value = "/insert-driver", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView insertDriver(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute DriverVO driverVO) throws Exception {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			String driverId = "";
			driverId = "DRI"+UUID.randomUUID().toString().replaceAll("-", "");
			driverVO.setDriverId(driverId);
			driverVO.setDriverDeposit(driverVO.getDriverDeposit().replaceAll(",", ""));
			driverVO.setDriverBalance(driverVO.getDriverBalance().replaceAll(",", ""));
			
			if(userSessionMap != null){				//사용자 로그인이 되어 있으면
				
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("duplicateChk", "true");
				map.put("driverId", driverVO.getDriverId());
				
				Map<String, Object> driverMap = driverService.selectDriver(map);
				
				if(driverMap != null){
					WebUtils.messageAndRedirectUrl(mav, "이미 사용중인 아이디 입니다.", "/humanResource/add-driver.do");
				}else{
					driverService.insertDriver(driverVO);
					subDir = "driver";
					fileService.insertFile(rootDir, subDir, driverId, request);
					WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/humanResource/add-driver.do");
				}
			
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	
	
	
	
	
	
	
}
