package kr.co.carrier.utils;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class MailUtils {

	public static String mailSend(String userEmail,String title,String content,String filePath,String fileName){
		
		
		/*
		// 환경정보 설정
		// 메일서버 주소를 IP 또는 도메인 명으로 지정
		Properties props = System.getProperties();
		props.setProperty("mail.smtp.host", "127.0.0.1");
		props.setProperty("mail.smtp.starttls.enable", "true");

		JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
		javaMailSender.setJavaMailProperties(props);
		javaMailSender.setPort(25);
		

		try {
		  final MimeMessage mimeMessage = javaMailSender.createMimeMessage();
		  MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, Charsets.UTF_8.displayName());
		  helper.setFrom(new InternetAddress("gunasss@naver.com", "관리자"));
		  helper.setTo(userEmail);
		  helper.setSubject(title);

		  helper.setText("Mail Content", content);

		  javaMailSender.send(mimeMessage);
		} catch (Exception e) {
		  e.printStackTrace();
		}*/
		
		String result = "ok";
		
		try {
			String host = "smtp.naver.com";
			//String host = "ezsmtp.bizmeka.com";

			final String username = "hkcc11"; 
			final String password = "hk15775268$$$";
			
			//final String username = "hkcc@hkccarrier.com"; 
			//final String password = "hk15775268$$$";
			
			int port=465; //포트번호
			//int port=587; //포트번호

			// 메일 내용 
			String recipient = userEmail; 
			//받는 사람의 메일주소를 입력해주세요.
			 String subject = title; //메일 제목 입력해주세요. 
			 String body = "안녕하세요 고객님. 저희 한국카캐리어를 이용해 주셔서 감사합니다."; //메일 내용 입력해주세요.
			Properties props = System.getProperties(); // 정보를 담기 위한 객체 생성
			// SMTP 서버 정보 설정 
			props.put("mail.smtp.host", host); 
			props.put("mail.smtp.port", port); 
			props.put("mail.smtp.auth", "true"); 
			
			//비즈메카 smtp로 변경 하면서 Could not connect to SMTP host: ezsmtp.bizmeka.com, port: 587; 익셉션 나는 부분 때문에 수정......
			props.put("mail.smtp.ssl.enable", "true"); 
			props.put("mail.smtp.ssl.trust", host);
			Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() { 
			String un=username; 
			String pw=password; 
			protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
			 return new javax.mail.PasswordAuthentication(un, pw); 
			 } 
			});

			session.setDebug(false); //for debug
			Message mimeMessage = new MimeMessage(session);
			
			MimeMultipart multipart = new MimeMultipart("related");
			//mimeMessage.setFrom(new InternetAddress("hkcc@hkccarrier.com"));
			mimeMessage.setFrom(new InternetAddress("hkcc11@naver.com"));
			mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
			mimeMessage.setSubject(subject); //제목셋팅 
			//mimeMessage.setText(body); //내용셋팅
			
			BodyPart msg_BodyPart = new MimeBodyPart();
			msg_BodyPart.setContent(body,"text/html; charset=UTF-8");
			multipart.addBodyPart(msg_BodyPart);
			
			msg_BodyPart = new MimeBodyPart();
			String attach_file = filePath;
			DataSource fds = new FileDataSource("/home/tomcat/carrier/files"+attach_file);
			//DataSource fds = new FileDataSource("D:/project/carrier/carrier/src/main/webapp/files"+attach_file);
						
			Thread.sleep(50);
						
			msg_BodyPart.setDataHandler(new DataHandler(fds));
			msg_BodyPart.setHeader("Content-ID", "<img_logo>");
			msg_BodyPart.setFileName(fileName);
			multipart.addBodyPart(msg_BodyPart);
			mimeMessage.setContent(multipart);
			
			
			Transport.send(mimeMessage);
			
			
		}catch(Exception e) {
			e.printStackTrace();
			result = "fail";
		}
		
		return result;
		
	}
	
	
}
