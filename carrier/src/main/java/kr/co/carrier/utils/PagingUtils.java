package kr.co.carrier.utils;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class PagingUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(PagingUtils.class);
	
	
	/**
	 * 페이지 정보를 셋팅한다.
	 * @param request
	 * @return Map
	 */
	public static Map<String,Object> setPageing(HttpServletRequest request, int total, Map<String,Object>  paramMap){

		int cPage = StringUtilsEx.parseInt(request.getParameter("cPage"), 1);
		int pageNum = BaseAppConstants.NUM_OF_PAGES;
		
		//String deviceType = (String)request.getSession().getAttribute("deviceType");
		//if("mobile".equals(deviceType)){
		//	pageNum = 5;
		//}
		if(request.getRequestURI().indexOf("mobile") > -1){
			pageNum = 5;
        }else{
            if(request.getRequestURI().indexOf("admin") > -1){
	            if(WebUtils.isPhone(request.getHeader("User-Agent"))){
	            	pageNum = 5;
	            }
            }
        }
		
		try{
		
			
			//화면에 표시하는 리스트의 개수를 사용자별로 10,20,30,40,50,100개 지정해서 사용 할 수 있도록 함.  
			//처음 회원 가입시 20개로 설정.
			HttpSession session = request.getSession();
			int numOfPages = session.getAttribute("listRowCnt") == null ? 20 : Integer.parseInt(session.getAttribute("listRowCnt").toString());
			
		/*
		  if(request.getRequestURL().indexOf("/allocation/combination.do") != -1){
			  paramMap = setPageInfo(cPage, total, BaseAppConstants.NUM_OF_PAGES, pageNum, paramMap);	  
		  }else if(request.getRequestURL().indexOf("/allocation/self.do") != -1){
			  paramMap = setPageInfo(cPage, total,20, pageNum, paramMap);					//셀프 리스트와 캐리어 리스트는 화면에 20개씩 표시 하기로 함.(19.02.01)
          }else if(request.getRequestURL().indexOf("/allocation/carrier.do") != -1){
			  paramMap = setPageInfo(cPage, total,20, pageNum, paramMap);
          }else if(request.getRequestURL().indexOf("/account/customerCalDetail.do") != -1){
			  paramMap = setPageInfo(cPage, total,20, pageNum, paramMap);
          }else{
        	  paramMap = setPageInfo(cPage, total, BaseAppConstants.NUM_OF_ROWS, pageNum, paramMap);
		  }
			*/

			paramMap = setPageInfo(cPage, total, numOfPages, pageNum, paramMap);
		
		}catch(Exception e ){
			e.printStackTrace();
		}
		return paramMap;
	}
	
	
	public static Map<String,Object> setPageing(HttpServletRequest request, int total, int numOfRows, int numOfPages, Map<String,Object>  paramMap){

		int cPage = StringUtilsEx.parseInt(request.getParameter("cPage"), 1);
		int pageNum = numOfPages;
		
		//String deviceType = (String)request.getSession().getAttribute("deviceType");
		//if("mobile".equals(deviceType)){
		//	pageNum = 5;
		//}
		if(request.getRequestURI().indexOf("mobile") > -1){
			pageNum = 5;
        }
		
		try{
			paramMap = setPageInfo(cPage, total, numOfRows, pageNum, paramMap);
		}catch(Exception e ){
			e.printStackTrace();
		}
		return paramMap;
	} 
	
	
	
    public static Map<String, Object>  setPageInfo(int cPage, int total, int numOfRows, int numOfPages, Map<String,Object>  paramMap) {

	    int startPage = 1;
	    int endPage = 1;
	    int totalPage = 1;
        int startRownum =  ((cPage-1) * numOfRows);
        //int startRownum =  ((cPage-1) * numOfRows) + 1;
        int endRownum = startRownum + numOfRows - 1;

        if ( total > 0) {   
            totalPage = total / numOfRows;
            if( (total % numOfRows) > 0 ) {
                totalPage += 1;
            }
            startPage = ((cPage - 1) / numOfPages) * numOfPages + 1;
            endPage = (((startPage - 1) + numOfPages) / numOfPages) * numOfPages;

            // 총페이지수가 계산된 마지막 페이지 번호보다 작을 경우 총 페이지수가 마지막 페이지 번호가 됨
            if(totalPage <= endPage) {
                endPage = totalPage;
            }
        } else {
            totalPage = 1;
            startPage = 1;
            endPage = 1;
        } 
        
        paramMap.put("cPage", cPage);
        paramMap.put("numOfRows", numOfRows);
        paramMap.put("numOfPages", numOfPages);
        paramMap.put("startPage", startPage);
        paramMap.put("endPage", endPage);
        paramMap.put("totalPage", totalPage);
        paramMap.put("total", total);
        paramMap.put("startRownum", startRownum);
        paramMap.put("endRownum", endRownum);

        return  paramMap; 
    }
	
	
}
