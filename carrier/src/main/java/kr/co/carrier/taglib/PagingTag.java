package kr.co.carrier.taglib;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import kr.co.carrier.utils.BaseAppConstants;
import kr.co.carrier.utils.StringUtilsEx;
import kr.co.carrier.utils.WebUtils;


public class PagingTag extends TagSupport  {

    
    private static final long serialVersionUID = -6420080584917643106L;

    private static final Logger logger = LoggerFactory.getLogger(PagingTag.class);
    
    private String uri;
    private String parameters;
    private String numOfRows;
    private String numOfPages;
    private String startPage;
    private String endPage;
    private String totalPage;
    private String cPage;
    private String total;
    private String scriptYn;
    private String frontYn;
    private String forGroup;
    private String searchType;
    private String bbsType;
    private String startDt;
    private String endDt;
    private String campaignId;
    private String adgroupId;
    
    
    private int setPageValue(HttpServletRequest request, String paramName, int defaultValue) {
    	Map<String, Object> paramMap = (Map<String, Object>) request.getAttribute("paramMap");
    	String paramValue = String.valueOf(paramMap.get(paramName));
    	return StringUtilsEx.parseInt(paramValue, defaultValue);
    }
    
    public int doStartTag() throws JspException {
        return (SKIP_BODY);
    }


    public int doEndTag() throws JspException {
        try {
            HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
            parameters = (String)request.getAttribute("parameters");
            if(parameters != null){
            	parameters = StringUtilsEx.replace(parameters, "&", "&amp;");
            }else{
            	parameters = "";
            }
            
            String mobileYn = "N";
           
            
            if(this.bbsType == null){
            	this.bbsType = "";
            }else{
            	this.bbsType = StringUtilsEx.replace(this.bbsType, "&", "&amp;");
            }
            
            if(this.forGroup == null){
            	this.forGroup = "";
            }else{
            	this.forGroup = StringUtilsEx.replace(this.forGroup, "&", "&amp;");
            }
            
            if(this.searchType == null){
            	this.searchType = "";
            }else{
            	this.searchType = StringUtilsEx.replace(this.searchType, "&", "&amp;");
            }
            
            if(this.startDt == null){
            	this.startDt = "";
            }else{
            	this.startDt = StringUtilsEx.replace(this.startDt, "&", "&amp;");
            }
            if(this.endDt == null){
            	this.endDt = "";
            }else{
            	this.endDt = StringUtilsEx.replace(this.endDt, "&", "&amp;");
            }
            
            
            if(this.adgroupId == null){
            	this.adgroupId = "";
            }else{
            	this.adgroupId = StringUtilsEx.replace(this.adgroupId, "&", "&amp;");
            }
            
            if(this.campaignId == null){
            	this.campaignId = "";
            }else{
            	this.campaignId = StringUtilsEx.replace(this.campaignId, "&", "&amp;");
            }
            
    		//String deviceType = (String)request.getSession().getAttribute("deviceType");
    		//if("mobile".equals(deviceType)){
    		//	mobileYn = "Y";
    		//}
            
            if(request.getRequestURI().indexOf("mobile") > -1){
            	mobileYn = "Y";
            }else{
                if(request.getRequestURI().indexOf("admin") > -1){
    	            if(WebUtils.isPhone(request.getHeader("User-Agent"))){
    	            	mobileYn = "Y";
    	            }
                }
            }
            
            int numOfRows = 0;
            
            if(request.getRequestURL().indexOf("/play/game-list-end.do") != -1){
            	numOfRows = setPageValue(request, "numOfRows", BaseAppConstants.NUM_OF_ROWS_FOR_END_LIST);
            }else if(request.getRequestURL().indexOf("/play/getPlayerList-bs.do") != -1){
            	numOfRows = setPageValue(request, "numOfRows", BaseAppConstants.NUM_OF_ROWS_FOR_PLAYER_LIST);
            }else{
            	numOfRows = setPageValue(request, "numOfRows", BaseAppConstants.NUM_OF_ROWS);
            }
            
            int numOfPages = setPageValue(request, "numOfPages", BaseAppConstants.NUM_OF_PAGES);
            int startPage = setPageValue(request, "startPage", 1);
            int endPage = setPageValue(request, "endPage", 1);
            int totalPage = setPageValue(request, "totalPage", -1);
            int cPage = setPageValue(request, "cPage", 1);
            int total = setPageValue(request,  "total", 0);
            if (totalPage < 0) {
                totalPage = total / numOfRows;
            }
            
            int prePageSet = (cPage - 1) / numOfPages * numOfPages;
            int nextPageSet = (cPage + numOfPages - 1) / numOfPages * numOfPages + 1;
            
           // boolean pageCounter = TypeConvertUtil.convertBoolean(this.pageCounter, false);           
            
            StringBuffer html = new StringBuffer();

            
            if (uri == null) {
            	if(request.getAttribute("requestURI") != null) //.do
                	uri = (String)request.getAttribute("requestURI");
                else{
                    uri = request.getRequestURI();
                }
            }
            
            //사용자
            if("Y".equals(frontYn))
            {    
            	///////////////////////////////////////////////////
            	//이전블럭
            	//html.append("<span class=\"prev\">");        	
            	
            	//이전 10
            	if(cPage > numOfPages)
            	{
            		if("Y".equals(scriptYn))
            			html.append("<a class=\"skip prev10\" href=\"javascript:").append(uri).append("(").append(cPage - numOfPages).append(")").append("\" '>");
            		else
            			html.append("<a class=\"skip prev10\" href=\"").append(uri).append("?cPage=").append(cPage - numOfPages).append(parameters).append(startDt).append(endDt).append(searchType).append(campaignId).append(adgroupId).append("\" >");
            	}
            	else
            	{
            		
            		html.append("<a class=\"skip prev10\" href=\"javascript:\" >");
            	}   
            	
            	if("Y".equals(mobileYn))
            		html.append("<span>&lt;&lt;</span> ").append("</a>");
            	else
            		html.append("이전 10페이지").append(numOfPages).append("</a>");
            	
            	//이전
            	if(cPage > 1)
            	{
            		if("Y".equals(scriptYn))
            			html.append("<a class=\"skip prev\" href=\"javascript:").append(uri).append("(").append(cPage - 1).append(")").append("\" '>");
            		else
            			html.append("<a class=\"skip prev\" href=\"").append(uri).append("?cPage=").append(cPage - 1).append(parameters).append(startDt).append(endDt).append(searchType).append(campaignId).append(adgroupId).append("\" >");            		
            	}
            	else
            	{
            		html.append("<a class=\"skip prev\" href=\"javascript:\" >");
            	}
            	
            	if("Y".equals(mobileYn))
            		html.append("<span>&lt;</span>").append("</a>");
            	else
            		html.append("이전 페이지").append("</a>");
            	
            	//html.append("</span>");
            	///////////////////////////////////////////////////
            	
            	///////////////////////////////////////////////////            	
            	//페이지번호
            	//html.append("<span class=\"page\">");
            	for (int i = startPage; i <= endPage; i++) {

	                if (i == cPage) { //현재페이지
	                	html.append("<strong>").append(i).append("</strong>");
	                } else {
	                	
	                	if("Y".equals(scriptYn))	                	
	                		html.append("<a href=\"javascript:").append(uri).append("(").append(i).append(")").append("\" >");
	                	else
	                		html.append("<a href=\"").append(uri).append("?cPage=").append(i).append(parameters).append(startDt).append(endDt).append(searchType).append(campaignId).append(adgroupId).append("\" >");
	                	
	                    html.append(i);
                		html.append("</a>");
	                }
	            }
            	//html.append("</span>");
            	///////////////////////////////////////////////////            	
            	
            	///////////////////////////////////////////////////            	
            	//다음블럭
            	//html.append("<span class=\"next\">");
            	
            	//다음
            	if(totalPage > 0 && cPage < totalPage)
            	{
            		if("Y".equals(scriptYn))
            			html.append("<a class=\"skip next\"  href=\"javascript:").append(uri).append("(").append(cPage + 1).append(")").append("\" '>");
            		else
            			html.append("<a class=\"skip next\"  href=\"").append(uri).append("?cPage=").append(cPage + 1).append(parameters).append(startDt).append(endDt).append(searchType).append(campaignId).append(adgroupId).append("\" >");            		
            	}
            	else
            	{
            		html.append("<a class=\"skip next\"  href=\"javascript:\" >");
            	}
            	
            	if("Y".equals(mobileYn))            	
            		html.append("<span>&gt;</span>").append("</a>");
            	else
            		html.append("다음 페이지").append("</a>");
            	
            	//다음 10
            	if(totalPage > 0 && cPage+numOfPages < totalPage)
            	{
            		if("Y".equals(scriptYn))
            			html.append("<a class=\"skip next10\"  href=\"javascript:").append(uri).append("(").append(cPage + numOfPages).append(")").append("\" '>");
            		else
            			html.append("<a class=\"skip next10\"   href=\"").append(uri).append("?cPage=").append(cPage + numOfPages).append(parameters).append(startDt).append(endDt).append(searchType).append(campaignId).append(adgroupId).append("\" >");
            	}
            	else
            	{
            		html.append("<a class=\"skip next10\" href=\"javascript:\" >");
            	}
            	
            	if("Y".equals(mobileYn))
            		html.append(" <span>&gt;&gt;</span></a>");
            	else
            		html.append("다음 10페이지").append(numOfPages).append("</a>");
            	
            	//html.append("</span>");
            	///////////////////////////////////////////////////            
            }
            //관리자
            else
            {
            	//처음
	            if (cPage <= 1) {
                	html
                	.append("<li>")
                	.append("<a href=\"javascript:").append("\" class='mws-paging-button disabled").append("'>");
                	if("Y".equals(mobileYn))
                		html.append("&lt;&lt;");
                	else
                		html.append("<i class=\"fa fa-caret-left\"></i><i class=\"fa fa-caret-left\"></i>");
                	html.append("</a>").append("</li>\n");
	            }else{
	            	
	            	if("Y".equals(scriptYn))
	            	{
	            		html
	                	.append("<li>")
	                	.append("<a href=\"javascript:").append(uri).append("(").append("1").append(")").append("\" class='mws-paging-button").append("'>");
	                	if("Y".equals(mobileYn))
	                		html.append("&lt;&lt;");
	                	else
	                		html.append("First");
	            		html.append("</a>").append("</li>\n");
	            	}
	            	else
	            	{
	                	html
	                	.append("<li>")
	                	.append("<a href=\"").append(uri).append("?cPage=").append("1").append(parameters).append(startDt).append(endDt).append(forGroup).append(searchType).append(campaignId).append(adgroupId).append("\" class='mws-paging-button").append("'>");
	                	if("Y".equals(mobileYn))
	                		html.append("&lt;&lt;");
	                	else
	                		/*html.append("First");*/
	                		html.append("<i class=\"fa fa-caret-left\"></i><i class=\"fa fa-caret-left\"></i>");
	                	html.append("</a>").append("</li>\n");
	            	}
	            }
	            
                if (cPage <= 1) {
    	            //이전
                	html
                	.append("<li>")
                	.append("<a href=\"javascript:").append("\" class='mws-paging-button disabled").append("'>");
                	if("Y".equals(mobileYn))
                		html.append("&lt;");
                	else
                		html.append("<i class=\"fa fa-caret-left\"></i>");
                	html.append("</a>").append("</li>\n");
                }else{
    	            //이전
                	if("Y".equals(scriptYn))
	            	{
	                	html
	                	.append("<li>")
	                	.append("<a href=\"javascript:").append(uri).append("(").append(prePageSet).append(")").append("\" class='mws-paging-button").append("'>");
	                	if("Y".equals(mobileYn))
	                		html.append("&lt;");
	                	else
	                		/*html.append("Prev");*/
	                		html.append("<i class=\"fa fa-caret-left\"></i>");
	                	html.append("</a>").append("</li>\n");
	            	}
                	else
                	{
                		html
	                	.append("<li>")
	                	.append("<a href=\"").append(uri).append("?cPage=").append(cPage - 1).append(parameters).append(startDt).append(endDt).append(forGroup).append(searchType).append(campaignId).append(adgroupId).append("\" class='mws-paging-button").append("'>");
	                	if("Y".equals(mobileYn))
	                		html.append("&lt;");
	                	else
	                		/*html.append("Prev");*/
	                		html.append("<i class=\"fa fa-caret-left\"></i>");
                		html.append("</a>").append("</li>\n");
                	}
                }

	            //페이징
            	for (int i = startPage; i <= endPage; i++) {

	                if (i == cPage) { //현재페이지
	                	html.append("<li class=\"active  curr-page\"><a href='javascript:' class='mws-paging-button current'>");
	                    html.append(i);
                		html.append("</a></li>\n");
	                } else {
	                	
	                	if("Y".equals(scriptYn))	                	
	                		html.append("<li><a href=\"javascript:").append(uri).append("(").append(i).append(")").append("\" class=\"mws-paging-button\">");
	                	else
	                		html.append("<li><a href=\"").append(uri).append("?cPage=").append(i).append(parameters).append(this.bbsType).append(this.startDt).append(this.endDt).append(this.forGroup).append(this.searchType).append(this.campaignId).append(this.adgroupId).append("\" class=\"mws-paging-button\">");
	                	
	                    html.append(i);
                		html.append("</a></li>\n");
	                }
	            }
	            
            	//다음
	            if (cPage >= totalPage) {
	            	html
	            	.append("<li>")
                	.append("<a href=\"javascript:").append("\" class='mws-paging-button disabled").append("'>");
                	if("Y".equals(mobileYn))
                		html.append("&gt;");
                	else
                		html.append("<i class=\"fa fa-caret-right\"></i>");
	            	html.append("</a>").append("</li>\n");
	            }else{
	            	if("Y".equals(scriptYn))
	            	{
	            		html
		            	.append("<li>")
	                	.append("<a href=\"javascript:").append(uri).append("(").append(nextPageSet).append(")").append("\" class='mws-paging-button").append("'>");
	                	if("Y".equals(mobileYn))
	                		html.append("&gt;");
	                	else
	                		html.append("Next");
	            		html.append("</a>").append("</li>\n");
	            	}
	            	else
	            	{
	            		html
		            	.append("<li>")
	                	.append("<a href=\"").append(uri).append("?cPage=").append(cPage+1).append(parameters).append(startDt).append(endDt).append(forGroup).append(searchType).append(campaignId).append(adgroupId).append("\" class='mws-paging-button").append("'>");
	                	if("Y".equals(mobileYn))
	                		html.append("&gt;");
	                	else
	                		/*html.append("Next");*/
	                		html.append("<i class=\"fa fa-caret-right\"></i>");
	                	html.append("</a>").append("</li>\n");
	            	}
	            }	          
	            
	            //마지막
	            if (cPage >= totalPage) {
	            	html
	            	.append("<li>")
                	.append("<a href=\"javascript:").append("\" class='mws-paging-button disabled").append("'>");
                	if("Y".equals(mobileYn))
                		html.append("&gt;&gt;");
                	else
                		html.append("<i class=\"fa fa-caret-right\"></i><i class=\"fa fa-caret-right\"></i>");
                	html.append("</a>").append("</li>\n");
	            }else{
	            	if("Y".equals(scriptYn))
	            	{
	            		html
		            	.append("<li>")
	                	.append("<a href=\"javascript:").append(uri).append("(").append(totalPage).append(")").append("\" class='mws-paging-button").append("'>");
	                	if("Y".equals(mobileYn))
	                		html.append("&gt;&gt;");
	                	else
	                		html.append("Last");
	            		html.append("</a>").append("</li>\n");
	            	}
	            	else
	            	{
	            		html
		            	.append("<li>")
	                	.append("<a href=\"").append(uri).append("?cPage=").append(totalPage).append(parameters).append(startDt).append(endDt).append(forGroup).append(searchType).append(campaignId).append(adgroupId).append("\" class='mws-paging-button").append("'>");
	                	if("Y".equals(mobileYn))
	                		html.append("&gt;&gt;");
	                	else
	                		/*html.append("Last");*/
	                		html.append("<i class=\"fa fa-caret-right\"></i><i class=\"fa fa-caret-right\"></i>");
	            		html.append("</a>").append("</li>\n");
	            	}
	            }

	            
	            /*
	            if(pageCounter) {
	            	html.append("<span class=\"kiss-paging-pageCounter\">").append(cPage).append(" / ").append(totalPage).append("</span>");
	            }
	            */
            }            
            
            JspWriter out = super.pageContext.getOut();
            out.print(html.toString());
        } catch (Exception e) {
        	e.printStackTrace();
            logger.error(e.getMessage());
        }
        return (SKIP_BODY);
    }
    
    
    
    /**
     * @param parameters The parameters to set.
     */
    public void setParameters(String parameters) {
        this.parameters = parameters;
    }  


    /**
     * @param totalPage The totalPage to set.
     */
    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }


    /**
     * @param page The cPage to set.
     */
    public void setCPage(String page) {
        cPage = page;
    }
    /**
     * @param endPage The endPage to set.
     */
    public void setEndPage(String endPage) {
        this.endPage = endPage;
    }
    /**
     * @param nextPageSet The nextPageSet to set.
     */
    public void setNumOfPages(String nextPageSet) {
        this.numOfPages = nextPageSet;
    }
    /**
     * @param prePageSet The prePageSet to set.
     */
    public void setNumOfRows(String prePageSet) {
        this.numOfRows = prePageSet;
    }
    /**
     * @param startPage The startPage to set.
     */
    public void setStartPage(String startPage) {
        this.startPage = startPage;
    }
    /**
     * @param total The total to set.
     */
    public void setTotal(String total) {
        this.total = total;
    }

    /**
     * @param uri The uri to set.
     */
    public void setUri(String uri) {
        this.uri = uri;
    }

    /**
     * @param scriptYn The scriptYn to set.
     */
    public void setScriptYn(String scriptYn) {
        this.scriptYn = scriptYn;
    }
    
    /**
     * @param frontYn The frontYn to set.
     */
    public void setFrontYn(String frontYn) {
        this.frontYn = frontYn;
    }

	public String getForGroup() {
		return forGroup;
	}

	public void setForGroup(String forGroup) {
		this.forGroup = forGroup;
	}

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public String getBbsType() {
		return bbsType;
	}

	public void setBbsType(String bbsType) {
		this.bbsType = bbsType;
	}

	public String getStartDt() {
		return startDt;
	}

	public void setStartDt(String startDt) {
		this.startDt = startDt;
	}

	public String getEndDt() {
		return endDt;
	}

	public void setEndDt(String endDt) {
		this.endDt = endDt;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public String getAdgroupId() {
		return adgroupId;
	}

	public void setAdgroupId(String adgroupId) {
		this.adgroupId = adgroupId;
	}


    
	
	
}