package kr.co.carrier.listener;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.FrameworkServlet;

import kr.co.carrier.service.EmpLoginInfoService;
import kr.co.carrier.service.EmployeeService;

@WebListener
public class SessionListener  implements HttpSessionListener {

	
	private static final Logger logger = LoggerFactory.getLogger(SessionListener.class);
	
	
	EmpLoginInfoService empLoginInfoService;
	
	@Override
	public void sessionCreated(HttpSessionEvent event) {

		try{
			HttpSession session = event.getSession();
			WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext( session.getServletContext(), FrameworkServlet.SERVLET_CONTEXT_PREFIX + "appServlet" );		
			empLoginInfoService = (EmpLoginInfoService) context.getBean("empLoginInfoService");			
			String sessionId = session.getId();
//			Map userMap = (Map)session.getAttribute("user");
//			String userId = userMap.get("user_id").toString();
//			System.out.println("init start +sessionId=["+sessionId+"]userId=["+userId+"]");
			
			System.out.println("init start sessionId = "+sessionId);						//세션이 생성 되고 로그인을 할때 db에 로그인 상태를 업데이트 하므로 ....... 여기서는 할 일이 없네 ㅎㅎ 
			
		}catch(Exception e){
			e.printStackTrace();
		}

	}

	
	@Override
	public void sessionDestroyed(HttpSessionEvent event) {
	
		try{
			HttpSession session = event.getSession();
			WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext( session.getServletContext(), FrameworkServlet.SERVLET_CONTEXT_PREFIX + "appServlet" );		
			empLoginInfoService = (EmpLoginInfoService) context.getBean("empLoginInfoService");
			Map userMap = (Map)session.getAttribute("user");
			
			if(userMap != null){									//처음 페이지에 접속하여 생성된 세션의 경우 로그인 하기 전이므로 userMap을 가지고 있지 않다. 로그인한 이후에만 필요하므로.......
				Map<String, Object> map = new HashMap<String, Object>();
		    	map.put("empId", userMap.get("emp_id").toString());
		    	map.put("empJsessionId", session.getId());
		    	empLoginInfoService.updateEmpLoginInfoLogoutDt(map);
				String empId = userMap.get("emp_id").toString();
				System.out.println("init stop +sessionId=["+session.getId()+"]empId=["+empId+"]");	
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
