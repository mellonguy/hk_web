package kr.co.carrier.vo;

public class CarDetailVO {

	
	
private String carDetailId;
private String driverId;
private String driverName;
private String carKind;
private String carIdNum;
private String carNum;
private String carModelYear;
private String carWeight;
private String carOwnerName;
private String carText;
private String registerId;
private String regDt;



public String getDriverId() {
	return driverId;
}
public void setDriverId(String driverId) {
	this.driverId = driverId;
}
public String getDriverName() {
	return driverName;
}
public void setDriverName(String driverName) {
	this.driverName = driverName;
}
public String getCarDetailId() {
	return carDetailId;
}
public void setCarDetailId(String carDetailId) {
	this.carDetailId = carDetailId;
}
public String getCarKind() {
	return carKind;
}
public void setCarKind(String carKind) {
	this.carKind = carKind;
}
public String getCarIdNum() {
	return carIdNum;
}
public void setCarIdNum(String carIdNum) {
	this.carIdNum = carIdNum;
}
public String getCarNum() {
	return carNum;
}
public void setCarNum(String carNum) {
	this.carNum = carNum;
}
public String getCarModelYear() {
	return carModelYear;
}
public void setCarModelYear(String carModelYear) {
	this.carModelYear = carModelYear;
}
public String getCarWeight() {
	return carWeight;
}
public void setCarWeight(String carWeight) {
	this.carWeight = carWeight;
}
public String getCarOwnerName() {
	return carOwnerName;
}
public void setCarOwnerName(String carOwnerName) {
	this.carOwnerName = carOwnerName;
}
public String getCarText() {
	return carText;
}
public void setCarText(String carText) {
	this.carText = carText;
}
public String getRegisterId() {
	return registerId;
}
public void setRegisterId(String registerId) {
	this.registerId = registerId;
}
public String getRegDt() {
	return regDt;
}
public void setRegDt(String regDt) {
	this.regDt = regDt;
}

}
