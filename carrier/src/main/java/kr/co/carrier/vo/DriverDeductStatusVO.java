package kr.co.carrier.vo;

public class DriverDeductStatusVO {

	private String driverDeductStatusId;
	private String driverId;
	private String deductStatus="N";
	private String deductMonth;
	private String regDt;
	
	
	public String getDriverDeductStatusId() {
		return driverDeductStatusId;
	}
	public void setDriverDeductStatusId(String driverDeductStatusId) {
		this.driverDeductStatusId = driverDeductStatusId;
	}
	public String getDriverId() {
		return driverId;
	}
	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
	public String getDeductStatus() {
		return deductStatus;
	}
	public void setDeductStatus(String deductStatus) {
		this.deductStatus = deductStatus;
	}
	public String getDeductMonth() {
		return deductMonth;
	}
	public void setDeductMonth(String deductMonth) {
		this.deductMonth = deductMonth;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}

	
}
