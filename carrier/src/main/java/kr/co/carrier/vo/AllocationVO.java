package kr.co.carrier.vo;

public class AllocationVO {

	private String listOrder;
	private String allocationId;
	private String companyId;
	private String companyName;
	private String inputDt;
	private String comment;
	private String customerId;
	private String customerName;
	private String chargeName;
	private String chargeId;
	private String chargePhone;
	private String chargeAddr;
	private String customerSignificantData;
	private String carCnt;
	private String memo;
	private String regDt;
	private String allocationStatus;
	private String registerId;
	private String registerName;
	private String profit;
	private String batchStatus;
	private String batchStatusId;
	private String batchIndex;
	private String pickupNum;
	private String hcYn="N";				//현대캐피탈 여부(기사 지급금 정산에서 사용됨)
	private String modId = "";
	private String logType = ""; //I:등록,U:수정,D:삭제
	private String mailAddress  = "";
	private String phoneNumber  = "";
	private String receiptSkip_yn  = "";
	private String receiptSkipReason = "";
	private String driverEtc = "";
	private String picturePointer_r = "";
	private String picturePointer_d = "";
	private String picturePointer_i = "";
	private String cancelReason = "";
	private String missingSend = "";
	private String regType = "";
	
	public String getListOrder() {
		return listOrder;
	}
	public void setListOrder(String listOrder) {
		this.listOrder = listOrder;
	}
	public String getAllocationId() {
		return allocationId;
	}
	public void setAllocationId(String allocationId) {
		this.allocationId = allocationId;
	}
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getInputDt() {
		return inputDt;
	}
	public void setInputDt(String inputDt) {
		this.inputDt = inputDt;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getChargeName() {
		return chargeName;
	}
	public void setChargeName(String chargeName) {
		this.chargeName = chargeName;
	}
	public String getChargeId() {
		return chargeId;
	}
	public void setChargeId(String chargeId) {
		this.chargeId = chargeId;
	}
	public String getChargePhone() {
		return chargePhone;
	}
	public void setChargePhone(String chargePhone) {
		this.chargePhone = chargePhone;
	}
	public String getChargeAddr() {
		return chargeAddr;
	}
	public void setChargeAddr(String chargeAddr) {
		this.chargeAddr = chargeAddr;
	}
	public String getCustomerSignificantData() {
		return customerSignificantData;
	}
	public void setCustomerSignificantData(String customerSignificantData) {
		this.customerSignificantData = customerSignificantData;
	}
	public String getCarCnt() {
		return carCnt;
	}
	public void setCarCnt(String carCnt) {
		this.carCnt = carCnt;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getRegisterId() {
		return registerId;
	}
	public void setRegisterId(String registerId) {
		this.registerId = registerId;
	}
	public String getRegisterName() {
		return registerName;
	}
	public void setRegisterName(String registerName) {
		this.registerName = registerName;
	}
	public String getProfit() {
		return profit;
	}
	public void setProfit(String profit) {
		this.profit = profit;
	}
	public String getAllocationStatus() {
		return allocationStatus;
	}
	public void setAllocationStatus(String allocationStatus) {
		this.allocationStatus = allocationStatus;
	}
	public String getBatchStatus() {
		return batchStatus;
	}
	public void setBatchStatus(String batchStatus) {
		this.batchStatus = batchStatus;
	}
	public String getBatchStatusId() {
		return batchStatusId;
	}
	public void setBatchStatusId(String batchStatusId) {
		this.batchStatusId = batchStatusId;
	}
	public String getBatchIndex() {
		return batchIndex;
	}
	public void setBatchIndex(String batchIndex) {
		this.batchIndex = batchIndex;
	}
	public String getPickupNum() {
		return pickupNum;
	}
	public void setPickupNum(String pickupNum) {
		this.pickupNum = pickupNum;
	}
	public String getHcYn() {
		return hcYn;
	}
	public void setHcYn(String hcYn) {
		this.hcYn = hcYn;
	}
	public String getModId() {
		return modId;
	}
	public void setModId(String modId) {
		this.modId = modId;
	}
	public String getLogType() {
		return logType;
	}
	public void setLogType(String logType) {
		this.logType = logType;
	}
	public String getMailAddress() {
		return mailAddress;
	}
	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getReceiptSkip_yn() {
		return receiptSkip_yn;
	}
	public void setReceiptSkip_yn(String receiptSkip_yn) {
		this.receiptSkip_yn = receiptSkip_yn;
	}
	public String getReceiptSkipReason() {
		return receiptSkipReason;
	}
	public void setReceiptSkipReason(String receiptSkipReason) {
		this.receiptSkipReason = receiptSkipReason;
	}
	public String getDriverEtc() {
		return driverEtc;
	}
	public void setDriverEtc(String driverEtc) {
		this.driverEtc = driverEtc;
	}
	public String getPicturePointer_r() {
		return picturePointer_r;
	}
	public void setPicturePointer_r(String picturePointer_r) {
		this.picturePointer_r = picturePointer_r;
	}
	public String getPicturePointer_d() {
		return picturePointer_d;
	}
	public void setPicturePointer_d(String picturePointer_d) {
		this.picturePointer_d = picturePointer_d;
	}
	public String getPicturePointer_i() {
		return picturePointer_i;
	}
	public void setPicturePointer_i(String picturePointer_i) {
		this.picturePointer_i = picturePointer_i;
	}
	public String getCancelReason() {
		return cancelReason;
	}
	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}
	public String getMissingSend() {
		return missingSend;
	}
	public void setMissingSend(String missingSend) {
		this.missingSend = missingSend;
	}
	public String getRegType() {
		return regType;
	}
	public void setRegType(String regType) {
		this.regType = regType;
	}
	
}
