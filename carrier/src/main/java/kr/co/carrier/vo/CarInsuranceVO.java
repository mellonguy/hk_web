package kr.co.carrier.vo;

public class CarInsuranceVO {

	private String carInsuranceId;
	private String driverId;
	private String driverName;   //기사명
	private String carNum;   //차량번호
	private String carIdNum;  //차대번호
	private String carMaximumLoadCapacity;  //최대벅재랑
	private String integratedInsuranceRegisterDt;  //통합보험 가입일
	
	
	public String getCarInsuranceId() {
		return carInsuranceId;
	}
	public void setCarInsuranceId(String carInsuranceId) {
		this.carInsuranceId = carInsuranceId;
	}
	public String getDriverId() {
		return driverId;
	}
	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getCarNum() {
		return carNum;
	}
	public void setCarNum(String carNum) {
		this.carNum = carNum;
	}
	public String getCarIdNum() {
		return carIdNum;
	}
	public void setCarIdNum(String carIdNum) {
		this.carIdNum = carIdNum;
	}
	public String getCarMaximumLoadCapacity() {
		return carMaximumLoadCapacity;
	}
	public void setCarMaximumLoadCapacity(String carMaximumLoadCapacity) {
		this.carMaximumLoadCapacity = carMaximumLoadCapacity;
	}
	public String getIntegratedInsuranceRegisterDt() {
		return integratedInsuranceRegisterDt;
	}
	public void setIntegratedInsuranceRegisterDt(String integratedInsuranceRegisterDt) {
		this.integratedInsuranceRegisterDt = integratedInsuranceRegisterDt;
	}
	
	
	
	
}
