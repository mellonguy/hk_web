package kr.co.carrier.vo;

public class WithdrawVO {

	
	
	private String withdrawId;
	private String paymentPartnerId;
	private String occurrenceDt;
	private String empId;
	private String totalVal;
	private String summary;
	private String etc;
	private String regDt;
	private String companyId;
	
	
	public String getWithdrawId() {
		return withdrawId;
	}
	public void setWithdrawId(String withdrawId) {
		this.withdrawId = withdrawId;
	}
	public String getPaymentPartnerId() {
		return paymentPartnerId;
	}
	public void setPaymentPartnerId(String paymentPartnerId) {
		this.paymentPartnerId = paymentPartnerId;
	}
	public String getOccurrenceDt() {
		return occurrenceDt;
	}
	public void setOccurrenceDt(String occurrenceDt) {
		this.occurrenceDt = occurrenceDt;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getTotalVal() {
		return totalVal;
	}
	public void setTotalVal(String totalVal) {
		this.totalVal = totalVal;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getEtc() {
		return etc;
	}
	public void setEtc(String etc) {
		this.etc = etc;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	
	
	
	
	
}
