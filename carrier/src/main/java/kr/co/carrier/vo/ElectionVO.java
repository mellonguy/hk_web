package kr.co.carrier.vo;

public class ElectionVO {

	
	private String electionId;
	private String electionTitle;
	private String electionContent;
	private String distributeDivision;
	private String candidateDivision;
	private String registerId;
	private String registerName;
	private String regDt;
	private String startDt;
	private String startTime;
	private String endDt;
	private String endTime;
	private String companyId;	
	
	public String getElectionId() {
		return electionId;
	}
	public void setElectionId(String electionId) {
		this.electionId = electionId;
	}
	public String getElectionTitle() {
		return electionTitle;
	}
	public void setElectionTitle(String electionTitle) {
		this.electionTitle = electionTitle;
	}
	public String getElectionContent() {
		return electionContent;
	}
	public void setElectionContent(String electionContent) {
		this.electionContent = electionContent;
	}
	public String getDistributeDivision() {
		return distributeDivision;
	}
	public void setDistributeDivision(String distributeDivision) {
		this.distributeDivision = distributeDivision;
	}
	public String getCandidateDivision() {
		return candidateDivision;
	}
	public void setCandidateDivision(String candidateDivision) {
		this.candidateDivision = candidateDivision;
	}
	public String getRegisterId() {
		return registerId;
	}
	public void setRegisterId(String registerId) {
		this.registerId = registerId;
	}
	public String getRegisterName() {
		return registerName;
	}
	public void setRegisterName(String registerName) {
		this.registerName = registerName;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getStartDt() {
		return startDt;
	}
	public void setStartDt(String startDt) {
		this.startDt = startDt;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndDt() {
		return endDt;
	}
	public void setEndDt(String endDt) {
		this.endDt = endDt;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	
	
}
