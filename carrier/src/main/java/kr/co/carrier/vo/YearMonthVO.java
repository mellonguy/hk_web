package kr.co.carrier.vo;

public class YearMonthVO {

	private String yearMonthId;
	private String yearMonthA;
	private String regDt;
	public String getYearMonthId() {
		return yearMonthId;
	}
	public void setYearMonthId(String yearMonthId) {
		this.yearMonthId = yearMonthId;
	}
	
	public String getYearMonthA() {
		return yearMonthA;
	}
	public void setYearMonthA(String yearMonthA) {
		this.yearMonthA = yearMonthA;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	
	
}
