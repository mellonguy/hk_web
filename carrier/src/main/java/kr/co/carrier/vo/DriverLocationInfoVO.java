package kr.co.carrier.vo;

public class DriverLocationInfoVO {

	
	private String locationInfoId;
	private String driverId;
	private String driverName;
	private String lat;
	private String lng;
	public String getLocationInfoId() {
		return locationInfoId;
	}
	public void setLocationInfoId(String locationInfoId) {
		this.locationInfoId = locationInfoId;
	}
	public String getDriverId() {
		return driverId;
	}
	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLng() {
		return lng;
	}
	public void setLng(String lng) {
		this.lng = lng;
	}
	
	
	
}
