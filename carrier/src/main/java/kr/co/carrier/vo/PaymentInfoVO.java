package kr.co.carrier.vo;

public class PaymentInfoVO {

	
	private String allocationId;
	private String amount;
	private String billForPayment;
	private String billingDivision;
	private String billingDt;
	private String deductionRate;
	private String etc;
	private String payment;
	private String paymentDivision;
	private String paymentDt;
	private String paymentKind;
	private String paymentPartner;
	private String paymentPartnerId;
	private String regDt;
	private String billingStatus="N";
	private String depositId;
	private String decideStatus="N";
	private String selectedStatus="N";
	private String decideMonth="";
	private String decideFinal="N";
	private String decideFinalId;
	private String debtorCreditorId;
	private String vatIncludeYn="N";
	private String vatExcludeYn="N";
	private String modId =""; //수정자
	private String logType = ""; //I:등록,U:수정,D:삭제 
	
	public String getAllocationId() {
		return allocationId;
	}
	public void setAllocationId(String allocationId) {
		this.allocationId = allocationId;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getBillForPayment() {
		return billForPayment;
	}
	public void setBillForPayment(String billForPayment) {
		this.billForPayment = billForPayment;
	}
	public String getBillingDivision() {
		return billingDivision;
	}
	public void setBillingDivision(String billingDivision) {
		this.billingDivision = billingDivision;
	}
	public String getBillingDt() {
		return billingDt;
	}
	public void setBillingDt(String billingDt) {
		this.billingDt = billingDt;
	}
	public String getDeductionRate() {
		return deductionRate;
	}
	public void setDeductionRate(String deductionRate) {
		this.deductionRate = deductionRate;
	}
	public String getEtc() {
		return etc;
	}
	public void setEtc(String etc) {
		this.etc = etc;
	}
	public String getPayment() {
		return payment;
	}
	public void setPayment(String payment) {
		this.payment = payment;
	}
	public String getPaymentDivision() {
		return paymentDivision;
	}
	public void setPaymentDivision(String paymentDivision) {
		this.paymentDivision = paymentDivision;
	}
	public String getPaymentDt() {
		return paymentDt;
	}
	public void setPaymentDt(String paymentDt) {
		this.paymentDt = paymentDt;
	}
	public String getPaymentKind() {
		return paymentKind;
	}
	public void setPaymentKind(String paymentKind) {
		this.paymentKind = paymentKind;
	}
	public String getPaymentPartner() {
		return paymentPartner;
	}
	public void setPaymentPartner(String paymentPartner) {
		this.paymentPartner = paymentPartner;
	}
	public String getPaymentPartnerId() {
		return paymentPartnerId;
	}
	public void setPaymentPartnerId(String paymentPartnerId) {
		this.paymentPartnerId = paymentPartnerId;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getBillingStatus() {
		return billingStatus;
	}
	public void setBillingStatus(String billingStatus) {
		this.billingStatus = billingStatus;
	}
	public String getDepositId() {
		return depositId;
	}
	public void setDepositId(String depositId) {
		this.depositId = depositId;
	}
	public String getDecideStatus() {
		return decideStatus;
	}
	public void setDecideStatus(String decideStatus) {
		this.decideStatus = decideStatus;
	}
	public String getSelectedStatus() {
		return selectedStatus;
	}
	public void setSelectedStatus(String selectedStatus) {
		this.selectedStatus = selectedStatus;
	}
	public String getDecideMonth() {
		return decideMonth;
	}
	public void setDecideMonth(String decideMonth) {
		this.decideMonth = decideMonth;
	}
	public String getDecideFinal() {
		return decideFinal;
	}
	public void setDecideFinal(String decideFinal) {
		this.decideFinal = decideFinal;
	}
	public String getDecideFinalId() {
		return decideFinalId;
	}
	public void setDecideFinalId(String decideFinalId) {
		this.decideFinalId = decideFinalId;
	}
	public String getDebtorCreditorId() {
		return debtorCreditorId;
	}
	public void setDebtorCreditorId(String debtorCreditorId) {
		this.debtorCreditorId = debtorCreditorId;
	}
	public String getVatIncludeYn() {
		return vatIncludeYn;
	}
	public void setVatIncludeYn(String vatIncludeYn) {
		this.vatIncludeYn = vatIncludeYn;
	}
	public String getVatExcludeYn() {
		return vatExcludeYn;
	}
	public void setVatExcludeYn(String vatExcludeYn) {
		this.vatExcludeYn = vatExcludeYn;
	}
	public String getModId() {
		return modId;
	}
	public void setModId(String modId) {
		this.modId = modId;
	}
	public String getLogType() {
		return logType;
	}
	public void setLogType(String logType) {
		this.logType = logType;
	}
	
	
}
