package kr.co.carrier.vo;

public class AlarmSocketVO {
	
	
    private String socketId;  //소켓 아이디
    private String userId;    //등록자 아이디
    private String allocationId; //배차 아이디
    private String text;   //텍스트
    private String regDt;  //등록일
    
    
	public String getSocketId() {
		return socketId;
	}
	public void setSocketId(String socketId) {
		this.socketId = socketId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getAllocationId() {
		return allocationId;
	}
	public void setAllocationId(String allocationId) {
		this.allocationId = allocationId;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}

    
}
