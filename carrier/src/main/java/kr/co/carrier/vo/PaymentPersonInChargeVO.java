package kr.co.carrier.vo;

public class PaymentPersonInChargeVO {


	private String address;
	private String customerId;
	private String paymentDepartment;
	private String email;
	private String etc;
	private String paymentName;
	private String paymentPersonInChargeId;
	private String paymentPhoneNum;
	private String regDt;
	
	
	public void setAddress(String address) {
		this.address = address;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public void setPaymentDepartment(String paymentDepartment) {
		this.paymentDepartment = paymentDepartment;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setEtc(String etc) {
		this.etc = etc;
	}
	public void setPaymentName(String paymentName) {
		this.paymentName = paymentName;
	}
	public void setPaymentPersonInChargeId(String paymentPersonInChargeId) {
		this.paymentPersonInChargeId = paymentPersonInChargeId;
	}
	public void setPaymentPhoneNum(String paymentPhoneNum) {
		this.paymentPhoneNum = paymentPhoneNum;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}   


}
