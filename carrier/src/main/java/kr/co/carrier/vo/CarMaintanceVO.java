package kr.co.carrier.vo;

public class CarMaintanceVO {
	
	

	private String carMaintananceId;
	private String driverId;
	private String driverName;
	private String carNum;
	private String carIdNum;
	private String carMaintananceRegdt;
	private String carDivision;
	private String carCategory;
	private String carPayment;
	private String carSubsidy;
	private String carRepair;
	private String carDistance;
	private String carGas;
	private String carMileage;
	private String carText;
	private String registerId;
	private String regDt;
	private String carAssignCompany;
	
	
	
	
	public String getCarIdNum() {
		return carIdNum;
	}
	public void setCarIdNum(String carIdNum) {
		this.carIdNum = carIdNum;
	}
	public String getCarMaintananceId() {
		return carMaintananceId;
	}
	public void setCarMaintananceId(String carMaintananceId) {
		this.carMaintananceId = carMaintananceId;
	}
	public String getDriverId() {
		return driverId;
	}
	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getCarNum() {
		return carNum;
	}
	public void setCarNum(String carNum) {
		this.carNum = carNum;
	}
	public String getCarMaintananceRegdt() {
		return carMaintananceRegdt;
	}
	public void setCarMaintananceRegdt(String carMaintananceRegdt) {
		this.carMaintananceRegdt = carMaintananceRegdt;
	}
	public String getCarDivision() {
		return carDivision;
	}
	public void setCarDivision(String carDivision) {
		this.carDivision = carDivision;
	}
	public String getCarCategory() {
		return carCategory;
	}
	public void setCarCategory(String carCategory) {
		this.carCategory = carCategory;
	}
	public String getCarPayment() {
		return carPayment;
	}
	public void setCarPayment(String carPayment) {
		this.carPayment = carPayment;
	}
	public String getCarSubsidy() {
		return carSubsidy;
	}
	public void setCarSubsidy(String carSubsidy) {
		this.carSubsidy = carSubsidy;
	}
	public String getCarRepair() {
		return carRepair;
	}
	public void setCarRepair(String carRepair) {
		this.carRepair = carRepair;
	}
	public String getCarDistance() {
		return carDistance;
	}
	public void setCarDistance(String carDistance) {
		this.carDistance = carDistance;
	}
	public String getCarGas() {
		return carGas;
	}
	public void setCarGas(String carGas) {
		this.carGas = carGas;
	}
	public String getCarMileage() {
		return carMileage;
	}
	public void setCarMileage(String carMileage) {
		this.carMileage = carMileage;
	}
	public String getCarText() {
		return carText;
	}
	public void setCarText(String carText) {
		this.carText = carText;
	}
	public String getRegisterId() {
		return registerId;
	}
	public void setRegisterId(String registerId) {
		this.registerId = registerId;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getCarAssignCompany() {
		return carAssignCompany;
	}
	public void setCarAssignCompany(String carAssignCompany) {
		this.carAssignCompany = carAssignCompany;
	}
	
	
	
	
}
