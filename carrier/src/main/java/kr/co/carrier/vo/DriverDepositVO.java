package kr.co.carrier.vo;

public class DriverDepositVO {

	
	
	private String driverDepositId;
	private String driverId;
	private String driverName;
	private String driverDeposit;
	private String guideDt;
	private String ioStatus;
	private String ioDt;
	private String ioAccount;
	private String registerId;
	private String regDt;
	private String returnTargetYn="N";
	private String summary;	
	
	public String getDriverDepositId() {
		return driverDepositId;
	}
	public void setDriverDepositId(String driverDepositId) {
		this.driverDepositId = driverDepositId;
	}
	public String getDriverId() {
		return driverId;
	}
	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getDriverDeposit() {
		return driverDeposit;
	}
	public void setDriverDeposit(String driverDeposit) {
		this.driverDeposit = driverDeposit;
	}
	public String getGuideDt() {
		return guideDt;
	}
	public void setGuideDt(String guideDt) {
		this.guideDt = guideDt;
	}
	public String getIoStatus() {
		return ioStatus;
	}
	public void setIoStatus(String ioStatus) {
		this.ioStatus = ioStatus;
	}
	public String getIoDt() {
		return ioDt;
	}
	public void setIoDt(String ioDt) {
		this.ioDt = ioDt;
	}
	public String getIoAccount() {
		return ioAccount;
	}
	public void setIoAccount(String ioAccount) {
		this.ioAccount = ioAccount;
	}
	public String getRegisterId() {
		return registerId;
	}
	public void setRegisterId(String registerId) {
		this.registerId = registerId;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getReturnTargetYn() {
		return returnTargetYn;
	}
	public void setReturnTargetYn(String returnTargetYn) {
		this.returnTargetYn = returnTargetYn;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	
	
}
