package kr.co.carrier.vo;

public class AllocationFinishInfoVO {

	private String finishInfoId;
	private String allocationId;
	private String finishMessage;
	private String driverName;
	private String driverId;
	private String regDt;
	public String getFinishInfoId() {
		return finishInfoId;
	}
	public void setFinishInfoId(String finishInfoId) {
		this.finishInfoId = finishInfoId;
	}
	public String getAllocationId() {
		return allocationId;
	}
	public void setAllocationId(String allocationId) {
		this.allocationId = allocationId;
	}
	public String getFinishMessage() {
		return finishMessage;
	}
	public void setFinishMessage(String finishMessage) {
		this.finishMessage = finishMessage;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getDriverId() {
		return driverId;
	}
	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	
	
}
