package kr.co.carrier.vo;

public class LowViolationVO {

	
	
										
	private String lowViolationId;		//법규위반 아이디
	private String driverId;            //기사 아이디
	private String driverName;          //기사명
	private String occurrenceDt;        //위반발생일
	private String occurrenceTime;      //위반시간
	private String carNum;              //차량번호
	private String placeAddress;        //위반장소
	private String summary;             //위반내용
	private String amount;              //금액
	private String endDt;               //납부마감기간
	private String accountInfo;  		//납부계좌정보
	private String registerId;          //등록자 아이디
	private String regDt;               //등록일
	private String phoneNum;			//연락처
	
	
	
	public String getLowViolationId() {
		return lowViolationId;
	}
	public void setLowViolationId(String lowViolationId) {
		this.lowViolationId = lowViolationId;
	}
	public String getDriverId() {
		return driverId;
	}
	public String getAccountInfo() {
		return accountInfo;
	}
	public void setAccountInfo(String accountInfo) {
		this.accountInfo = accountInfo;
	}
	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getOccurrenceDt() {
		return occurrenceDt;
	}
	public void setOccurrenceDt(String occurrenceDt) {
		this.occurrenceDt = occurrenceDt;
	}
	public String getOccurrenceTime() {
		return occurrenceTime;
	}
	public void setOccurrenceTime(String occurrenceTime) {
		this.occurrenceTime = occurrenceTime;
	}
	public String getCarNum() {
		return carNum;
	}
	public void setCarNum(String carNum) {
		this.carNum = carNum;
	}
	public String getPlaceAddress() {
		return placeAddress;
	}
	public void setPlaceAddress(String placeAddress) {
		this.placeAddress = placeAddress;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getEndDt() {
		return endDt;
	}
	public void setEndDt(String endDt) {
		this.endDt = endDt;
	}
	public String getRegisterId() {
		return registerId;
	}
	public void setRegisterId(String registerId) {
		this.registerId = registerId;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	
	
	
}
