package kr.co.carrier.vo;

public class EmpLoginInfoVO {

	
	private String empLoginInfoId;
	private String empJsessionId;
	private String empId;
	private String empName;
	private String empLoginIpV4;
	private String empLoginIpV6;
	private String loginDt;
	private String logoutDt;
	private String regDt;
	public String getEmpLoginInfoId() {
		return empLoginInfoId;
	}
	public void setEmpLoginInfoId(String empLoginInfoId) {
		this.empLoginInfoId = empLoginInfoId;
	}
	public String getEmpJsessionId() {
		return empJsessionId;
	}
	public void setEmpJsessionId(String empJsessionId) {
		this.empJsessionId = empJsessionId;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getEmpLoginIpV4() {
		return empLoginIpV4;
	}
	public void setEmpLoginIpV4(String empLoginIpV4) {
		this.empLoginIpV4 = empLoginIpV4;
	}
	public String getEmpLoginIpV6() {
		return empLoginIpV6;
	}
	public void setEmpLoginIpV6(String empLoginIpV6) {
		this.empLoginIpV6 = empLoginIpV6;
	}
	public String getLoginDt() {
		return loginDt;
	}
	public void setLoginDt(String loginDt) {
		this.loginDt = loginDt;
	}
	public String getLogoutDt() {
		return logoutDt;
	}
	public void setLogoutDt(String logoutDt) {
		this.logoutDt = logoutDt;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	

	
	
}
