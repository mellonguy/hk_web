package kr.co.carrier.vo;

public class CarInfoVO {

	
	private String accidentYn = "";
	private String allocationId = "";
	private String arrival = "";
	private String arrivalAddr = "";
	private String arrivalPersonInCharge = "";
	private String arrivalPhone  = "";
	private String carIdNum  = "";
	private String carKind = "";
	private String carNum = "";
	private String carrierType = "";
	private String contractNum = "";
	private String departure = "";
	private String departureAddr = "";
	private String departureDt = "";
	private String departurePersonInCharge = "";
	private String departurePhone = "";
	private String departureTime = "";
	private String distanceType = "";
	private String driverId = "";
	private String driverName = "";
	private String etc = "";
	private String regDt = "";
	private String towDistance = "";
	private String driverCnt = "";
	private String salesTotal = "";		//해당 배차의 매출액(업체 청구액)
	private String requirePicCnt = "";		//해당 배차의 최소 요구 사진 수
	private String requireDnPicCnt = "";
	private String driverSetEmpId = "";
	private String driverSetEmpName = "";
	private String receiptSendYn = "";  // 인수증발송	
	private String receiptName = "";
	private String receiptEmail = "";
	private String receiptPhone = "";
	private String vat = "0";
	private String price = "0";
	private String vatIncludeYn = "N";
	private String billingStatus = "N";
	private String decideStatus = "N";
	private String vatExcludeYn = "N";
	private String billPublishId = "";
	private String billPublishRequestId = "";
	private String driverModYn = "";
	private String modEmp = "";
	private String debtorCreditorId = "";
	private String modId = ""; //수정자 추가 
	private String logType = ""; //I:등록,U:수정,D:삭제
	private String customerEtc = "";
	private String customerYn = "";
	private String driverEtc = "";
	private String modDriver	 = "";

	public String getAccidentYn() {
		return accidentYn;
	}
	
	
	
	public String getDriverId() {
		return driverId;
	}



	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}



	public void setAccidentYn(String accidentYn) {
		this.accidentYn = accidentYn;
	}
	public String getAllocationId() {
		return allocationId;
	}
	public void setAllocationId(String allocationId) {
		this.allocationId = allocationId;
	}
	public String getArrival() {
		return arrival;
	}
	public void setArrival(String arrival) {
		this.arrival = arrival;
	}
	public String getArrivalAddr() {
		return arrivalAddr;
	}
	public void setArrivalAddr(String arrivalAddr) {
		this.arrivalAddr = arrivalAddr;
	}
	public String getArrivalPersonInCharge() {
		return arrivalPersonInCharge;
	}
	public void setArrivalPersonInCharge(String arrivalPersonInCharge) {
		this.arrivalPersonInCharge = arrivalPersonInCharge;
	}
	public String getArrivalPhone() {
		return arrivalPhone;
	}
	public void setArrivalPhone(String arrivalPhone) {
		this.arrivalPhone = arrivalPhone;
	}
	public String getCarIdNum() {
		return carIdNum;
	}
	public void setCarIdNum(String carIdNum) {
		this.carIdNum = carIdNum;
	}
	public String getCarKind() {
		return carKind;
	}
	public void setCarKind(String carKind) {
		this.carKind = carKind;
	}
	public String getCarNum() {
		return carNum;
	}
	public void setCarNum(String carNum) {
		this.carNum = carNum;
	}
	public String getCarrierType() {
		return carrierType;
	}
	public void setCarrierType(String carrierType) {
		this.carrierType = carrierType;
	}
	public String getContractNum() {
		return contractNum;
	}
	public void setContractNum(String contractNum) {
		this.contractNum = contractNum;
	}
	public String getDeparture() {
		return departure;
	}
	public void setDeparture(String departure) {
		this.departure = departure;
	}
	public String getDepartureAddr() {
		return departureAddr;
	}
	public void setDepartureAddr(String departureAddr) {
		this.departureAddr = departureAddr;
	}
	public String getDepartureDt() {
		return departureDt;
	}
	public void setDepartureDt(String departureDt) {
		this.departureDt = departureDt;
	}
	public String getDeparturePersonInCharge() {
		return departurePersonInCharge;
	}
	public void setDeparturePersonInCharge(String departurePersonInCharge) {
		this.departurePersonInCharge = departurePersonInCharge;
	}
	public String getDeparturePhone() {
		return departurePhone;
	}
	public void setDeparturePhone(String departurePhone) {
		this.departurePhone = departurePhone;
	}
	public String getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}
	public String getDistanceType() {
		return distanceType;
	}
	public void setDistanceType(String distanceType) {
		this.distanceType = distanceType;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getEtc() {
		return etc;
	}
	public void setEtc(String etc) {
		this.etc = etc;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getTowDistance() {
		return towDistance;
	}
	public void setTowDistance(String towDistance) {
		this.towDistance = towDistance;
	}
	public String getDriverCnt() {
		return driverCnt;
	}
	public void setDriverCnt(String driverCnt) {
		this.driverCnt = driverCnt;
	}
	public String getSalesTotal() {
		return salesTotal;
	}
	public void setSalesTotal(String salesTotal) {
		this.salesTotal = salesTotal;
	}

	public String getRequirePicCnt() {
		return requirePicCnt;
	}

	public void setRequirePicCnt(String requirePicCnt) {
		this.requirePicCnt = requirePicCnt;
	}

	public String getDriverSetEmpId() {
		return driverSetEmpId;
	}

	public void setDriverSetEmpId(String driverSetEmpId) {
		this.driverSetEmpId = driverSetEmpId;
	}

	public String getDriverSetEmpName() {
		return driverSetEmpName;
	}

	public void setDriverSetEmpName(String driverSetEmpName) {
		this.driverSetEmpName = driverSetEmpName;
	}

	public String getRequireDnPicCnt() {
		return requireDnPicCnt;
	}

	public void setRequireDnPicCnt(String requireDnPicCnt) {
		this.requireDnPicCnt = requireDnPicCnt;
	}

	public String getReceiptSendYn() {
		return receiptSendYn;
	}

	public void setReceiptSendYn(String receiptSendYn) {
		this.receiptSendYn = receiptSendYn;
	}

	public String getReceiptName() {
		return receiptName;
	}

	public void setReceiptName(String receiptName) {
		this.receiptName = receiptName;
	}

	public String getReceiptEmail() {
		return receiptEmail;
	}

	public void setReceiptEmail(String receiptEmail) {
		this.receiptEmail = receiptEmail;
	}

	public String getReceiptPhone() {
		return receiptPhone;
	}

	public void setReceiptPhone(String receiptPhone) {
		this.receiptPhone = receiptPhone;
	}



	public String getVat() {
		return vat;
	}



	public void setVat(String vat) {
		this.vat = vat;
	}



	public String getPrice() {
		return price;
	}



	public void setPrice(String price) {
		this.price = price;
	}



	public String getVatIncludeYn() {
		return vatIncludeYn;
	}



	public void setVatIncludeYn(String vatIncludeYn) {
		this.vatIncludeYn = vatIncludeYn;
	}



	public String getBillingStatus() {
		return billingStatus;
	}



	public void setBillingStatus(String billingStatus) {
		this.billingStatus = billingStatus;
	}



	public String getDecideStatus() {
		return decideStatus;
	}



	public void setDecideStatus(String decideStatus) {
		this.decideStatus = decideStatus;
	}



	public String getVatExcludeYn() {
		return vatExcludeYn;
	}



	public void setVatExcludeYn(String vatExcludeYn) {
		this.vatExcludeYn = vatExcludeYn;
	}



	public String getBillPublishId() {
		return billPublishId;
	}



	public void setBillPublishId(String billPublishId) {
		this.billPublishId = billPublishId;
	}



	public String getBillPublishRequestId() {
		return billPublishRequestId;
	}



	public void setBillPublishRequestId(String billPublishRequestId) {
		this.billPublishRequestId = billPublishRequestId;
	}



	public String getDriverModYn() {
		return driverModYn;
	}



	public void setDriverModYn(String driverModYn) {
		this.driverModYn = driverModYn;
	}


	public String getModEmp() {
		return modEmp;
	}



	public void setModEmp(String modEmp) {
		this.modEmp = modEmp;
	}

	public String getDebtorCreditorId() {
		return debtorCreditorId;
	}

	public void setDebtorCreditorId(String debtorCreditorId) {
		this.debtorCreditorId = debtorCreditorId;
	}



	public String getModId() {
		return modId;
	}



	public void setModId(String modId) {
		this.modId = modId;
	}



	public String getLogType() {
		return logType;
	}



	public void setLogType(String logType) {
		this.logType = logType;
	}



	public String getCustomerEtc() {
		return customerEtc;
	}



	public void setCustomerEtc(String customerEtc) {
		this.customerEtc = customerEtc;
	}



	public String getCustomerYn() {
		return customerYn;
	}



	public void setCustomerYn(String customerYn) {
		this.customerYn = customerYn;
	}



	public String getDriverEtc() {
		return driverEtc;
	}



	public void setDriverEtc(String driverEtc) {
		this.driverEtc = driverEtc;
	}



	public String getModDriver() {
		return modDriver;
	}



	public void setModDriver(String modDriver) {
		this.modDriver = modDriver;
	}

	
	
	
	
}
