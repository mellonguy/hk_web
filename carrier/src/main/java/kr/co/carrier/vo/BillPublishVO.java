package kr.co.carrier.vo;

public class BillPublishVO {

	private String billPublishId;
	private String billPublishRequestId;
	private String customerId;
	private String customerName;
	private String amount;
	private String publisherId;
	private String publisherName;
	private String publishStatus;
	private String regDt;
	private String companyId;
	
	public String getBillPublishId() {
		return billPublishId;
	}
	public void setBillPublishId(String billPublishId) {
		this.billPublishId = billPublishId;
	}
	public String getBillPublishRequestId() {
		return billPublishRequestId;
	}
	public void setBillPublishRequestId(String billPublishRequestId) {
		this.billPublishRequestId = billPublishRequestId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getPublisherId() {
		return publisherId;
	}
	public void setPublisherId(String publisherId) {
		this.publisherId = publisherId;
	}
	public String getPublisherName() {
		return publisherName;
	}
	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}
	public String getPublishStatus() {
		return publishStatus;
	}
	public void setPublishStatus(String publishStatus) {
		this.publishStatus = publishStatus;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	
		
}
