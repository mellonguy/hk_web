package kr.co.carrier.vo;

public class DriverPaymentVO {

	
	//private String driverPaymentMainId;		//차량정보 아이디
	private String driverPaymentId;				//지입료 아이디
	private String driverPayment;          		//지입료
	private String driverId;               			//기사 아이디
	private String driverName;             		//기사명
	//private String paymentStartDt;        	//시작일
	//private String paymentEndDt;          	//종료일
	//private String assignDivision;         		//구분  (1 : 직영  2 : 지입)
	//private String workKind;               		//업무구분(I:내부업무,O:외부업무)
	private String paymentYn;               		//납부여부(N:미납,Y:납부)
	private String etc;                     			//비고
	private String registerId;              		//등록자 아이디
	private String regDt;                   			//등록일
	private String carNum;
	private String registerName;                 //등록자 이름
	private String paymentMonth;                 //해당월 
	private String driverPaymentI;  				//입금액
	private String paymentCarId;  				//지입료 차량아이디
	private String paymentBalance;    	  	   //지입료 잔액
	private String paymentDt;
	
//	public String getDriverPaymentMainId() {
//		return driverPaymentMainId;
//	}
//	public void setDriverPaymentMainId(String driverPaymentMainId) {
//		this.driverPaymentMainId = driverPaymentMainId;
//	}
	
	
	
	public String getPaymentDt() {
		return paymentDt;
	}
	public String getCarNum() {
		return carNum;
	}
	public void setCarNum(String carNum) {
		this.carNum = carNum;
	}
	public void setPaymentDt(String paymentDt) {
		this.paymentDt = paymentDt;
	}
	public String getPaymentBalance() {
		return paymentBalance;
	}
	public void setPaymentBalance(String paymentBalance) {
		this.paymentBalance = paymentBalance;
	}
	public String getPaymentCarId() {
		return paymentCarId;
	}
	public void setPaymentCarId(String paymentCarId) {
		this.paymentCarId = paymentCarId;
	}
	public String getDriverPaymentI() {
		return driverPaymentI;
	}
	public void setDriverPaymentI(String driverPaymentI) {
		this.driverPaymentI = driverPaymentI;
	}
	public String getDriverPaymentId() {
		return driverPaymentId;
	}
	public String getPaymentMonth() {
		return paymentMonth;
	}
	public void setPaymentMonth(String paymentMonth) {
		this.paymentMonth = paymentMonth;
	}
	public String getRegisterName() {
		return registerName;
	}
	public void setRegisterName(String registerName) {
		this.registerName = registerName;
	}
	public void setDriverPaymentId(String driverPaymentId) {
		this.driverPaymentId = driverPaymentId;
	}
	
	public String getDriverPayment() {
		return driverPayment;
	}
	public void setDriverPayment(String driverPayment) {
		this.driverPayment = driverPayment;
	}
	public String getDriverId() {
		return driverId;
	}
	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
//	public String getPaymentStartDt() {
//		return paymentStartDt;
//	}
//	public void setPaymentStartDt(String paymentStartDt) {
//		this.paymentStartDt = paymentStartDt;
//	}
//	public String getPaymentEndDt() {
//		return paymentEndDt;
//	}
//	public void setPaymentEndDt(String paymentEndDt) {
//		this.paymentEndDt = paymentEndDt;
//	}
//	public String getAssignDivision() {
//		return assignDivision;
//	}
//	public void setAssignDivision(String assignDivision) {
//		this.assignDivision = assignDivision;
//	}
//	public String getWorkKind() {
//		return workKind;
//	}
//	public void setWorkKind(String workKind) {
//		this.workKind = workKind;
//	}
	public String getPaymentYn() {
		return paymentYn;
	}
	public void setPaymentYn(String paymentYn) {
		this.paymentYn = paymentYn;
	}
	public String getEtc() {
		return etc;
	}
	public void setEtc(String etc) {
		this.etc = etc;
	}
	public String getRegisterId() {
		return registerId;
	}
	public void setRegisterId(String registerId) {
		this.registerId = registerId;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
