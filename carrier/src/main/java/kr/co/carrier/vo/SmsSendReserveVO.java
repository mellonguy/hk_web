package kr.co.carrier.vo;

public class SmsSendReserveVO {

	private String smsSendReserveId;
	private String driverId;
	private String resignDt;
	private String sendReserveDt;
	private String sendYn;
	private String content;
	private String regDt;
	public String getSmsSendReserveId() {
		return smsSendReserveId;
	}
	public void setSmsSendReserveId(String smsSendReserveId) {
		this.smsSendReserveId = smsSendReserveId;
	}
	public String getDriverId() {
		return driverId;
	}
	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
	public String getResignDt() {
		return resignDt;
	}
	public void setResignDt(String resignDt) {
		this.resignDt = resignDt;
	}
	public String getSendReserveDt() {
		return sendReserveDt;
	}
	public void setSendReserveDt(String sendReserveDt) {
		this.sendReserveDt = sendReserveDt;
	}
	public String getSendYn() {
		return sendYn;
	}
	public void setSendYn(String sendYn) {
		this.sendYn = sendYn;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	
	
}
