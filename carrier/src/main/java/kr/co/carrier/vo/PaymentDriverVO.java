package kr.co.carrier.vo;

public class PaymentDriverVO {

	
	private String driverOwner="";
	private String paymentDriverId="";
	private String assignCompany="";
	private String phoneNum="";
	private String residentRegistrationNumber="";
	private String driverLicense="";
	private String joinDt="";
	private String resignDt="";
	private String businessCondition="";
	private String businessKind="";
	private String bondedCar="";
	private String deductionRate="";
	private String driverPayment="";
	private String driverDeposit="";
	private String driverName="";
	private String bankName="";
	private String depositor="";
	private String driverBalance="";
	private String accountNumber="";
	private String carKind="";
	private String carNickname="";
	private String carAssignCompany="";
	private String carNum="";
	private String carIdNum="";
	private String registerId="";
	private String regDt="";
	private String driverId="";
	private String companyKind="";
	private String businessLicenseNumber="";
	private String address="";
	private String addressDetail="";
	private String driverKind="";
	private String driverStatus="";
	private String driverPaymentYn="";
	private String driverDepositYn="";
	private String etc="";
	private String registerStatus="";
	private String ownerName="";
	private String paymentDriver="";
	
	
	

	public String getDriverBalance() {
		return driverBalance;
	}
	public void setDriverBalance(String driverBalance) {
		this.driverBalance = driverBalance;
	}
	public String getDriverOwner() {
		return driverOwner;
	}
	public void setDriverOwner(String driverOwner) {
		this.driverOwner = driverOwner;
	}
	public String getPaymentDriverId() {
		return paymentDriverId;
	}
	public void setPaymentDriverId(String paymentDriverId) {
		this.paymentDriverId = paymentDriverId;
	}
	public String getAssignCompany() {
		return assignCompany;
	}
	public void setAssignCompany(String assignCompany) {
		this.assignCompany = assignCompany;
	}
	public String getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	public String getResidentRegistrationNumber() {
		return residentRegistrationNumber;
	}
	public void setResidentRegistrationNumber(String residentRegistrationNumber) {
		this.residentRegistrationNumber = residentRegistrationNumber;
	}
	public String getDriverLicense() {
		return driverLicense;
	}
	public void setDriverLicense(String driverLicense) {
		this.driverLicense = driverLicense;
	}
	public String getJoinDt() {
		return joinDt;
	}
	public void setJoinDt(String joinDt) {
		this.joinDt = joinDt;
	}
	public String getResignDt() {
		return resignDt;
	}
	public void setResignDt(String resignDt) {
		this.resignDt = resignDt;
	}
	public String getBusinessCondition() {
		return businessCondition;
	}
	public void setBusinessCondition(String businessCondition) {
		this.businessCondition = businessCondition;
	}
	public String getBusinessKind() {
		return businessKind;
	}
	public void setBusinessKind(String businessKind) {
		this.businessKind = businessKind;
	}
	public String getBondedCar() {
		return bondedCar;
	}
	public void setBondedCar(String bondedCar) {
		this.bondedCar = bondedCar;
	}
	public String getDeductionRate() {
		return deductionRate;
	}
	public void setDeductionRate(String deductionRate) {
		this.deductionRate = deductionRate;
	}
	public String getDriverPayment() {
		return driverPayment;
	}
	public void setDriverPayment(String driverPayment) {
		this.driverPayment = driverPayment;
	}
	public String getDriverDeposit() {
		return driverDeposit;
	}
	public void setDriverDeposit(String driverDeposit) {
		this.driverDeposit = driverDeposit;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getDepositor() {
		return depositor;
	}
	public void setDepositor(String depositor) {
		this.depositor = depositor;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getCarKind() {
		return carKind;
	}
	public void setCarKind(String carKind) {
		this.carKind = carKind;
	}
	public String getCarNickname() {
		return carNickname;
	}
	public void setCarNickname(String carNickname) {
		this.carNickname = carNickname;
	}
	public String getCarAssignCompany() {
		return carAssignCompany;
	}
	public void setCarAssignCompany(String carAssignCompany) {
		this.carAssignCompany = carAssignCompany;
	}
	public String getCarNum() {
		return carNum;
	}
	public void setCarNum(String carNum) {
		this.carNum = carNum;
	}
	public String getCarIdNum() {
		return carIdNum;
	}
	public void setCarIdNum(String carIdNum) {
		this.carIdNum = carIdNum;
	}
	public String getRegisterId() {
		return registerId;
	}
	public void setRegisterId(String registerId) {
		this.registerId = registerId;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getDriverId() {
		return driverId;
	}
	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
	public String getCompanyKind() {
		return companyKind;
	}
	public void setCompanyKind(String companyKind) {
		this.companyKind = companyKind;
	}
	public String getBusinessLicenseNumber() {
		return businessLicenseNumber;
	}
	public void setBusinessLicenseNumber(String businessLicenseNumber) {
		this.businessLicenseNumber = businessLicenseNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAddressDetail() {
		return addressDetail;
	}
	public void setAddressDetail(String addressDetail) {
		this.addressDetail = addressDetail;
	}
	public String getDriverKind() {
		return driverKind;
	}
	public void setDriverKind(String driverKind) {
		this.driverKind = driverKind;
	}
	public String getDriverStatus() {
		return driverStatus;
	}
	public void setDriverStatus(String driverStatus) {
		this.driverStatus = driverStatus;
	}
	public String getDriverPaymentYn() {
		return driverPaymentYn;
	}
	public void setDriverPaymentYn(String driverPaymentYn) {
		this.driverPaymentYn = driverPaymentYn;
	}
	public String getDriverDepositYn() {
		return driverDepositYn;
	}
	public void setDriverDepositYn(String driverDepositYn) {
		this.driverDepositYn = driverDepositYn;
	}
	public String getEtc() {
		return etc;
	}
	public void setEtc(String etc) {
		this.etc = etc;
	}
	public String getRegisterStatus() {
		return registerStatus;
	}
	public void setRegisterStatus(String registerStatus) {
		this.registerStatus = registerStatus;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public String getPaymentDriver() {
		return paymentDriver;
	}
	public void setPaymentDriver(String paymentDriver) {
		this.paymentDriver = paymentDriver;
	}
	
	
	

	
	
	
}

