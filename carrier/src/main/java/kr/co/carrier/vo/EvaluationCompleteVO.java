package kr.co.carrier.vo;

public class EvaluationCompleteVO {

private String	empEvaluationCompleteId;
private String	empManagerId;
private String	empId;
private String	evaluationMonth;
private String	evaluationCompletePoints;
private String includeRatioPoint;



public String getIncludeRatioPoint() {
	return includeRatioPoint;
}
public void setIncludeRatioPoint(String includeRatioPoint) {
	this.includeRatioPoint = includeRatioPoint;
}
public String getEmpEvaluationCompleteId() {
	return empEvaluationCompleteId;
}
public void setEmpEvaluationCompleteId(String empEvaluationCompleteId) {
	this.empEvaluationCompleteId = empEvaluationCompleteId;
}
public String getEmpManagerId() {
	return empManagerId;
}
public void setEmpManagerId(String empManagerId) {
	this.empManagerId = empManagerId;
}
public String getEmpId() {
	return empId;
}
public void setEmpId(String empId) {
	this.empId = empId;
}
public String getEvaluationMonth() {
	return evaluationMonth;
}
public void setEvaluationMonth(String evaluationMonth) {
	this.evaluationMonth = evaluationMonth;
}
public String getEvaluationCompletePoints() {
	return evaluationCompletePoints;
}
public void setEvaluationCompletePoints(String evaluationCompletePoints) {
	this.evaluationCompletePoints = evaluationCompletePoints;
}
	




}
