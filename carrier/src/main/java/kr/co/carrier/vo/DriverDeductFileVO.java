package kr.co.carrier.vo;

public class DriverDeductFileVO {

	
	private String driverDeductFileId;
	private String driverId;
	private String driverDeductFilePath;
	private String driverDeductFileNm;
	private String categoryType;
	private String decideMonth;
	private String regDt;
	private String supplyVal;
	private String vatVal;
	private String totalVal;
	
	
	
	public String getDriverDeductFileId() {
		return driverDeductFileId;
	}
	public void setDriverDeductFileId(String driverDeductFileId) {
		this.driverDeductFileId = driverDeductFileId;
	}
	public String getDriverId() {
		return driverId;
	}
	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
	public String getDriverDeductFilePath() {
		return driverDeductFilePath;
	}
	public void setDriverDeductFilePath(String driverDeductFilePath) {
		this.driverDeductFilePath = driverDeductFilePath;
	}
	public String getDriverDeductFileNm() {
		return driverDeductFileNm;
	}
	public void setDriverDeductFileNm(String driverDeductFileNm) {
		this.driverDeductFileNm = driverDeductFileNm;
	}
	public String getCategoryType() {
		return categoryType;
	}
	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}
	public String getDecideMonth() {
		return decideMonth;
	}
	public void setDecideMonth(String decideMonth) {
		this.decideMonth = decideMonth;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getSupplyVal() {
		return supplyVal;
	}
	public void setSupplyVal(String supplyVal) {
		this.supplyVal = supplyVal;
	}
	public String getVatVal() {
		return vatVal;
	}
	public void setVatVal(String vatVal) {
		this.vatVal = vatVal;
	}
	public String getTotalVal() {
		return totalVal;
	}
	public void setTotalVal(String totalVal) {
		this.totalVal = totalVal;
	}
	
	
	
	
}
