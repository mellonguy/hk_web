package kr.co.carrier.vo;

public class DepositDriverVO {

	
	private String driverId;
	private String carNum;
	private String assignCompany;
	private String driverOwner;
	private String driverName;
	private String carAssignCompany;
	private String workKind;
	private String joinDt;
	private String resignDt;
	private String phoneNum;
	private String carIdNum;
	private String businessLicenseNumber;
	private String driverDeposit;
	private String regDt;
	private String paymentExpDt; //지급예정일 , 퇴사일이 결정되면 자동으로 +6개월 일자가 setting 
	private String paymentDt;    // 지급일 , 실제 지급한 날짜
	
	public String getDriverId() {
		return driverId;
	}
	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
	public String getCarNum() {
		return carNum;
	}
	public void setCarNum(String carNum) {
		this.carNum = carNum;
	}
	public String getAssignCompany() {
		return assignCompany;
	}
	public void setAssignCompany(String assignCompany) {
		this.assignCompany = assignCompany;
	}
	public String getDriverOwner() {
		return driverOwner;
	}
	public void setDriverOwner(String driverOwner) {
		this.driverOwner = driverOwner;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getCarAssignCompany() {
		return carAssignCompany;
	}
	public void setCarAssignCompany(String carAssignCompany) {
		this.carAssignCompany = carAssignCompany;
	}
	public String getWorkKind() {
		return workKind;
	}
	public void setWorkKind(String workKind) {
		this.workKind = workKind;
	}
	public String getJoinDt() {
		return joinDt;
	}
	public void setJoinDt(String joinDt) {
		this.joinDt = joinDt;
	}
	public String getResignDt() {
		return resignDt;
	}
	public void setResignDt(String resignDt) {
		this.resignDt = resignDt;
	}
	public String getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	public String getCarIdNum() {
		return carIdNum;
	}
	public void setCarIdNum(String carIdNum) {
		this.carIdNum = carIdNum;
	}
	public String getBusinessLicenseNumber() {
		return businessLicenseNumber;
	}
	public void setBusinessLicenseNumber(String businessLicenseNumber) {
		this.businessLicenseNumber = businessLicenseNumber;
	}
	public String getDriverDeposit() {
		return driverDeposit;
	}
	public void setDriverDeposit(String driverDeposit) {
		this.driverDeposit = driverDeposit;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
  public String getPaymentExpDt() {
    return paymentExpDt;
  }
  public void setPaymentExpDt(String paymentExpDt) {
    this.paymentExpDt = paymentExpDt;
  }
  public String getPaymentDt() {
    return paymentDt;
  }
  public void setPaymentDt(String paymentDt) {
    this.paymentDt = paymentDt;
  }
	
	
	
}
