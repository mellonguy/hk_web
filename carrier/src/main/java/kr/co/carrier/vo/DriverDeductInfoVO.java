package kr.co.carrier.vo;

public class DriverDeductInfoVO {

	private String deductInfoId;
	private String driverId;
	private String driverName;
	private String occurrenceDt;
	private String item;
	private String division;
	private String amount;
	private String etc;
	private String regDt;
	private String registerId;
	private String registerName;
	
	public String getDeductInfoId() {
		return deductInfoId;
	}
	public void setDeductInfoId(String deductInfoId) {
		this.deductInfoId = deductInfoId;
	}
	public String getDriverId() {
		return driverId;
	}
	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getOccurrenceDt() {
		return occurrenceDt;
	}
	public void setOccurrenceDt(String occurrenceDt) {
		this.occurrenceDt = occurrenceDt;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getEtc() {
		return etc;
	}
	public void setEtc(String etc) {
		this.etc = etc;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getRegisterId() {
		return registerId;
	}
	public void setRegisterId(String registerId) {
		this.registerId = registerId;
	}
	public String getRegisterName() {
		return registerName;
	}
	public void setRegisterName(String registerName) {
		this.registerName = registerName;
	}
	
}
