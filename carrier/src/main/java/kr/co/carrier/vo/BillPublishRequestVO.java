package kr.co.carrier.vo;

public class BillPublishRequestVO {

	private String billPublishRequestId;
	private String customerId;
	private String customerName;
	private String requestDt;
	private String amount;
	private String requesterId;
	private String requesterName;
	private String publishRequestStatus;
	private String regDt;
	private String vat;
	private String total;
	private String comment;
	private String summary;
	private String companyId;
	
	public String getBillPublishRequestId() {
		return billPublishRequestId;
	}
	public void setBillPublishRequestId(String billPublishRequestId) {
		this.billPublishRequestId = billPublishRequestId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getRequestDt() {
		return requestDt;
	}
	public void setRequestDt(String requestDt) {
		this.requestDt = requestDt;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getRequesterId() {
		return requesterId;
	}
	public void setRequesterId(String requesterId) {
		this.requesterId = requesterId;
	}
	public String getRequesterName() {
		return requesterName;
	}
	public void setRequesterName(String requesterName) {
		this.requesterName = requesterName;
	}
	public String getPublishRequestStatus() {
		return publishRequestStatus;
	}
	public void setPublishRequestStatus(String publishRequestStatus) {
		this.publishRequestStatus = publishRequestStatus;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getVat() {
		return vat;
	}
	public void setVat(String vat) {
		this.vat = vat;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	
		
}
