package kr.co.carrier.vo;

public class DriverPaymentMainVO {

	
	private String driverPaymentMainId;		//지입차량정보 아이디
	private String carKind;                    //차종
	private String carNum;                     //차량번호
	private String carIdNum;                  //차대번호
	private String etc;                         //비고
	private String registerId;                 //등록자 아이디
	private String regDt;                      //등록일
	private String companyId;
	
	
	
	public String getDriverPaymentMainId() {
		return driverPaymentMainId;
	}
	public void setDriverPaymentMainId(String driverPaymentMainId) {
		this.driverPaymentMainId = driverPaymentMainId;
	}
	public String getCarKind() {
		return carKind;
	}
	public void setCarKind(String carKind) {
		this.carKind = carKind;
	}
	public String getCarNum() {
		return carNum;
	}
	public void setCarNum(String carNum) {
		this.carNum = carNum;
	}
	public String getCarIdNum() {
		return carIdNum;
	}
	public void setCarIdNum(String carIdNum) {
		this.carIdNum = carIdNum;
	}
	public String getEtc() {
		return etc;
	}
	public void setEtc(String etc) {
		this.etc = etc;
	}
	public String getRegisterId() {
		return registerId;
	}
	public void setRegisterId(String registerId) {
		this.registerId = registerId;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	
	
}
