package kr.co.carrier.vo;

public class MessageVO {

	
	private String id;
	private String phone;
	private String merge_vals;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getMerge_vals() {
		return merge_vals;
	}
	public void setMerge_vals(String merge_vals) {
		this.merge_vals = merge_vals;
	}
	
	
}
