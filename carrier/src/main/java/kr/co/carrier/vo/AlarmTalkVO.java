package kr.co.carrier.vo;

public class AlarmTalkVO {

	
	private String idx;
	private String alarmTalkId;
	private String customerId;
	private String type;
	private String msg;
	private String template;
	private String rcvphns;
	private String regDt;
	private String allocationId;
	private String returnMsg;
	
	
	
	public String getIdx() {
		return idx;
	}
	public void setIdx(String idx) {
		this.idx = idx;
	}
	public String getAlarmTalkId() {
		return alarmTalkId;
	}
	public void setAlarmTalkId(String alarmTalkId) {
		this.alarmTalkId = alarmTalkId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getTemplate() {
		return template;
	}
	public void setTemplate(String template) {
		this.template = template;
	}
	public String getRcvphns() {
		return rcvphns;
	}
	public void setRcvphns(String rcvphns) {
		this.rcvphns = rcvphns;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getAllocationId() {
		return allocationId;
	}
	public void setAllocationId(String allocationId) {
		this.allocationId = allocationId;
	}
	public String getReturnMsg() {
		return returnMsg;
	}
	public void setReturnMsg(String returnMsg) {
		this.returnMsg = returnMsg;
	}
	
}
