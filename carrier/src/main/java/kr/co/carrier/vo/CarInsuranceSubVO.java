package kr.co.carrier.vo;

public class CarInsuranceSubVO {

	
	
	private String carInsuranceId; //차량보험 서브 부모 아이디'
	private String carInsuranceSubId;   // 차량 보험 서브 아이디'
	private String insuranceFlag ="N"; //현재 보험플래그 N:비활성화, Y:활성화'
	private String insuranceKind;  //보험 종류 T:통합,J:적재물
	private String rate;  //요율
	private String insuranceRegisterDt;  //보험가입일
	private String insuranceApplicationContributionFirst;  // 적용분단금 1회
	private String insuranceApplicationContributionNext;  // 적용분담금 2회~6회
	private String barrierRewardDistribution;  //대물배상한도
	private String selfAccident;  //자기신체사고 
	private String selfVehicleDamaget;  //자기차량손해
	private String legalExpensesSupportSpecial;  //법률비용지원특약
	private String urgentDispatch;  //긴급출동
	private String insuranceAymentInInstallment;  // 분할납입특약
	private String compensationLimit;  //보상한도
	private String selfPayments;  //자기부담금
	private String specialOffers;  //특약사항
	private String Tolerance = "N"; //공차여부
	private String toleranceStartDt; //공차 Y 시작일
	private String toleranceEndDt; //공차 Y 종료일
	private String actualPaymentDt; //실제 납부일
	private String paymentClosingDt; //납부 마감일
	private String terminationDt; //해지일
	private String	roundOfEvents; //회차

	
	
	public String getActualPaymentDt() {
		return actualPaymentDt;
	}
	public void setActualPaymentDt(String actualPaymentDt) {
		this.actualPaymentDt = actualPaymentDt;
	}
	public String getPaymentClosingDt() {
		return paymentClosingDt;
	}
	public void setPaymentClosingDt(String paymentClosingDt) {
		this.paymentClosingDt = paymentClosingDt;
	}
	public String getTerminationDt() {
		return terminationDt;
	}
	public void setTerminationDt(String terminationDt) {
		this.terminationDt = terminationDt;
	}
	public String getRoundOfEvents() {
		return roundOfEvents;
	}
	public void setRoundOfEvents(String roundOfEvents) {
		this.roundOfEvents = roundOfEvents;
	}
	public String getInsuranceRegisterDt() {
		return insuranceRegisterDt;
	}
	public void setInsuranceRegisterDt(String insuranceRegisterDt) {
		this.insuranceRegisterDt = insuranceRegisterDt;
	}
	public String getInsuranceApplicationContributionFirst() {
		return insuranceApplicationContributionFirst;
	}
	public void setInsuranceApplicationContributionFirst(String insuranceApplicationContributionFirst) {
		this.insuranceApplicationContributionFirst = insuranceApplicationContributionFirst;
	}
	public String getInsuranceApplicationContributionNext() {
		return insuranceApplicationContributionNext;
	}
	public void setInsuranceApplicationContributionNext(String insuranceApplicationContributionNext) {
		this.insuranceApplicationContributionNext = insuranceApplicationContributionNext;
	}
	public String getInsuranceAymentInInstallment() {
		return insuranceAymentInInstallment;
	}
	public void setInsuranceAymentInInstallment(String insuranceAymentInInstallment) {
		this.insuranceAymentInInstallment = insuranceAymentInInstallment;
	}
	
	public String getCarInsuranceId() {
		return carInsuranceId;
	}
	public void setCarInsuranceId(String carInsuranceId) {
		this.carInsuranceId = carInsuranceId;
	}
	public String getCarInsuranceSubId() {
		return carInsuranceSubId;
	}
	public void setCarInsuranceSubId(String carInsuranceSubId) {
		this.carInsuranceSubId = carInsuranceSubId;
	}
	public String getInsuranceFlag() {
		return insuranceFlag;
	}
	public void setInsuranceFlag(String insuranceFlag) {
		this.insuranceFlag = insuranceFlag;
	}
	public String getInsuranceKind() {
		return insuranceKind;
	}
	public void setInsuranceKind(String insuranceKind) {
		this.insuranceKind = insuranceKind;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getBarrierRewardDistribution() {
		return barrierRewardDistribution;
	}
	public void setBarrierRewardDistribution(String barrierRewardDistribution) {
		this.barrierRewardDistribution = barrierRewardDistribution;
	}
	public String getSelfAccident() {
		return selfAccident;
	}
	public void setSelfAccident(String selfAccident) {
		this.selfAccident = selfAccident;
	}
	public String getSelfVehicleDamaget() {
		return selfVehicleDamaget;
	}
	public void setSelfVehicleDamaget(String selfVehicleDamaget) {
		this.selfVehicleDamaget = selfVehicleDamaget;
	}
	public String getLegalExpensesSupportSpecial() {
		return legalExpensesSupportSpecial;
	}
	public void setLegalExpensesSupportSpecial(String legalExpensesSupportSpecial) {
		this.legalExpensesSupportSpecial = legalExpensesSupportSpecial;
	}
	public String getUrgentDispatch() {
		return urgentDispatch;
	}
	public void setUrgentDispatch(String urgentDispatch) {
		this.urgentDispatch = urgentDispatch;
	}


	public String getCompensationLimit() {
		return compensationLimit;
	}
	public void setCompensationLimit(String compensationLimit) {
		this.compensationLimit = compensationLimit;
	}

	public String getSelfPayments() {
		return selfPayments;
	}
	public void setSelfPayments(String selfPayments) {
		this.selfPayments = selfPayments;
	}
	public String getSpecialOffers() {
		return specialOffers;
	}
	public void setSpecialOffers(String specialOffers) {
		this.specialOffers = specialOffers;
	}

	public String getTolerance() {
		return Tolerance;
	}
	public void setTolerance(String tolerance) {
		Tolerance = tolerance;
	}
	public String getToleranceStartDt() {
		return toleranceStartDt;
	}
	public void setToleranceStartDt(String toleranceStartDt) {
		this.toleranceStartDt = toleranceStartDt;
	}
	public String getToleranceEndDt() {
		return toleranceEndDt;
	}
	public void setToleranceEndDt(String toleranceEndDt) {
		this.toleranceEndDt = toleranceEndDt;
	}
	
	
	
}
