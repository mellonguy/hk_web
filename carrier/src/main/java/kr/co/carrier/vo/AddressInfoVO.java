package kr.co.carrier.vo;

public class AddressInfoVO {

	
	
	private String addressInfoId;
	private String keyword;
	private String address;
	private String name;
	private String phoneNum;
	private String companyId;
	
	
	public String getAddressInfoId() {
		return addressInfoId;
	}
	public void setAddressInfoId(String addressInfoId) {
		this.addressInfoId = addressInfoId;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	
	
}
