package kr.co.carrier.vo;

public class EvaluationVO{

private String empId;
private String empEvaluationId;
private String empManagerId;
private String empQuestionId;
private String evaluationMonth;
private String decideFinal;
private String evaluationPoints;


public String getEmpId() {
	return empId;
}
public void setEmpId(String empId) {
	this.empId = empId;
}
public String getEmpEvaluationId() {
	return empEvaluationId;
}
public void setEmpEvaluationId(String empEvaluationId) {
	this.empEvaluationId = empEvaluationId;
}
public String getEmpManagerId() {
	return empManagerId;
}
public void setEmpManagerId(String empManagerId) {
	this.empManagerId = empManagerId;
}
public String getEmpQuestionId() {
	return empQuestionId;
}
public void setEmpQuestionId(String empQuestionId) {
	this.empQuestionId = empQuestionId;
}
public String getEvaluationMonth() {
	return evaluationMonth;
}
public void setEvaluationMonth(String evaluationMonth) {
	this.evaluationMonth = evaluationMonth;
}
public String getDecideFinal() {
	return decideFinal;
}
public void setDecideFinal(String decideFinal) {
	this.decideFinal = decideFinal;
}
public String getEvaluationPoints() {
	return evaluationPoints;
}
public void setEvaluationPoints(String evaluationPoints) {
	this.evaluationPoints = evaluationPoints;
}
	

}