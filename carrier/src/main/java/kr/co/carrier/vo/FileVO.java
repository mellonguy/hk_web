package kr.co.carrier.vo;

public class FileVO {

	
	private String filePath;
	private String fileNm;
	private String fileId;
	private String categoryType;			//기초정보관리 입력시 직원,기사,거래처,회사정보등을 구분
	private String contentId;				//직원 등록시 직원id,기사 등록시 기사id 각 id를 키로 하여 select 하기 위해 사용
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getFileNm() {
		return fileNm;
	}
	public void setFileNm(String fileNm) {
		this.fileNm = fileNm;
	}
	public String getFileId() {
		return fileId;
	}
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	
	public String getCategoryType() {
		return categoryType;
	}
	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}
	public String getContentId() {
		return contentId;
	}
	public void setContentId(String contentId) {
		this.contentId = contentId;
	}
	
	
	
	
	
	
	
	
	
	
}
