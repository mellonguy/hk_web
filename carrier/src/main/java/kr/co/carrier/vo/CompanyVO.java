package kr.co.carrier.vo;

public class CompanyVO {

	
	private String accountNumber;
	private String address;
	private String addressDetail;
	private String bankName;
	private String businessCondition;
	private String businessKind;
	private String companyId;
	private String companyKind;
	private String companyName;
	private String companyOwnerName;
	private String businessLicenseNumber;
	private String depositor;
	private String etc;
	private String idx;
	private String phoneNum;
	private String phoneNumSub;
	private String regDt;
	private String corporationRegistrationNumber;
	private String establishDate;
	private String setUpDate;
	
	
	
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAddressDetail() {
		return addressDetail;
	}
	public void setAddressDetail(String addressDetail) {
		this.addressDetail = addressDetail;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBusinessCondition() {
		return businessCondition;
	}
	public void setBusinessCondition(String businessCondition) {
		this.businessCondition = businessCondition;
	}
	public String getBusinessKind() {
		return businessKind;
	}
	public void setBusinessKind(String businessKind) {
		this.businessKind = businessKind;
	}
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	public String getCompanyKind() {
		return companyKind;
	}
	public void setCompanyKind(String companyKind) {
		this.companyKind = companyKind;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCompanyOwnerName() {
		return companyOwnerName;
	}
	public void setCompanyOwnerName(String companyOwnerName) {
		this.companyOwnerName = companyOwnerName;
	}
	
	public String getBusinessLicenseNumber() {
		return businessLicenseNumber;
	}
	public void setBusinessLicenseNumber(String businessLicenseNumber) {
		this.businessLicenseNumber = businessLicenseNumber;
	}
	public String getDepositor() {
		return depositor;
	}
	public void setDepositor(String depositor) {
		this.depositor = depositor;
	}
	public String getEtc() {
		return etc;
	}
	public void setEtc(String etc) {
		this.etc = etc;
	}
	public String getIdx() {
		return idx;
	}
	public void setIdx(String idx) {
		this.idx = idx;
	}
	public String getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	public String getPhoneNumSub() {
		return phoneNumSub;
	}
	public void setPhoneNumSub(String phoneNumSub) {
		this.phoneNumSub = phoneNumSub;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getCorporationRegistrationNumber() {
		return corporationRegistrationNumber;
	}
	public void setCorporationRegistrationNumber(String corporationRegistrationNumber) {
		this.corporationRegistrationNumber = corporationRegistrationNumber;
	}
	public String getEstablishDate() {
		return establishDate;
	}
	public void setEstablishDate(String establishDate) {
		this.establishDate = establishDate;
	}
	public String getSetUpDate() {
		return setUpDate;
	}
	public void setSetUpDate(String setUpDate) {
		this.setUpDate = setUpDate;
	}
	
	
}
