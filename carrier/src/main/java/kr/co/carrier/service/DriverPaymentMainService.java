package kr.co.carrier.service;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.DriverPaymentMainVO;

public interface DriverPaymentMainService {

	
	
	public Map<String, Object> selectDriverPaymentMain(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectDriverPaymentMainList(Map<String, Object> map) throws Exception;
	public int selectDriverPaymentMainListCount(Map<String, Object> map) throws Exception;
	public int insertDriverPaymentMain(DriverPaymentMainVO driverPaymentMainVO) throws Exception;
	public void deleteDriverPaymentMain(Map<String, Object> map) throws Exception;
	public void updateDriverPaymentMain(DriverPaymentMainVO driverPaymentMainVO) throws Exception;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
