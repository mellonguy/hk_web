package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.AllocationFinishInfoMapper;
import kr.co.carrier.service.AllocationFinishInfoService;
import kr.co.carrier.vo.AllocationFinishInfoVO;

@Service("allocationFinishInfoService")
public class AllocationFinishInfoServiceImpl implements AllocationFinishInfoService{

	
	@Resource(name="allocationFinishInfoMapper")
	private AllocationFinishInfoMapper allocationFinishInfoMapper;
	
	
	public Map<String, Object> selectAllocationFinishInfo(Map<String, Object> map) throws Exception{
	
		return allocationFinishInfoMapper.selectAllocationFinishInfo(map);
	}
	
	public List<Map<String, Object>> selectAllocationFinishInfoList(Map<String, Object> map) throws Exception{
		return allocationFinishInfoMapper.selectAllocationFinishInfoList(map);
	}
	
	public int selectAllocationFinishInfoListCount(Map<String, Object> map) throws Exception{
		return allocationFinishInfoMapper.selectAllocationFinishInfoListCount(map);
	}
	
	public int insertAllocationFinishInfo(AllocationFinishInfoVO allocationFinishInfoVO) throws Exception{
		return allocationFinishInfoMapper.insertAllocationFinishInfo(allocationFinishInfoVO);
	}
	
	public void deleteAllocationFinishInfo(Map<String, Object> map) throws Exception{
		allocationFinishInfoMapper.deleteAllocationFinishInfo(map);
	}
	
	public List<Map<String,Object>>selectAllocationStatusList(Map<String, Object> map) throws Exception{
		return allocationFinishInfoMapper.selectAllocationStatusList(map);
	}
	
	public List<Map<String,Object>>selectSocketAlarmTalkList(Map<String, Object> map) throws Exception{
		return allocationFinishInfoMapper.selectSocketAlarmTalkList(map);
	}
	
	
	
	
	
	
	
}
