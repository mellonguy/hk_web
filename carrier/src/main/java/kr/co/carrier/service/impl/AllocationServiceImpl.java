package kr.co.carrier.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.AllocationMapper;
import kr.co.carrier.mapper.FileMapper;
import kr.co.carrier.service.AddressInfoService;
import kr.co.carrier.service.AlarmTalkService;
import kr.co.carrier.service.AllocationDivisionService;
import kr.co.carrier.service.AllocationService;
import kr.co.carrier.service.CarInfoService;
import kr.co.carrier.service.CompanyService;
import kr.co.carrier.service.CustomerService;
import kr.co.carrier.service.DriverService;
import kr.co.carrier.service.FcmService;
import kr.co.carrier.service.FileService;
import kr.co.carrier.service.PaymentInfoService;
import kr.co.carrier.service.PersonInChargeService;
import kr.co.carrier.service.RunDivisionService;
import kr.co.carrier.service.SendSmsService;
import kr.co.carrier.utils.ParamUtils;
import kr.co.carrier.utils.ResultApi;
import kr.co.carrier.utils.WebUtils;
import kr.co.carrier.vo.AlarmSocketVO;
import kr.co.carrier.vo.AllocationVO;
import kr.co.carrier.vo.CarInfoVO;
import kr.co.carrier.vo.PaymentInfoVO;





@Service("allocationService")
public class AllocationServiceImpl implements AllocationService{

	@Value("#{appProp['upload.base.path']}")
    private String rootDir;    
    private String subDir = "dupList";
	
	
    @Autowired
    private CompanyService companyService;
    
    @Autowired
    private DriverService driverService;
    
    @Autowired
    private CarInfoService carInfoService;
        
    @Autowired
    private AllocationDivisionService allocationDivisionService;
    
    @Autowired
    private RunDivisionService runDivisionService;
        
    @Autowired
    private PaymentInfoService paymentInfoService;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private PersonInChargeService personInChargeService;
    
    @Autowired
    private FcmService fcmService;
        
    @Autowired
    private AddressInfoService addressInfoService;
        
    @Autowired
    private AlarmTalkService alarmTalkService;
        
    @Autowired
    private SendSmsService sendSmsService;
    
    @Autowired
    private FileService fileService;
    
    @Resource(name="fileMapper")
	private FileMapper fileMapper;
    
	
	
	
	
	
	
	@Resource(name="allocationMapper")
	private AllocationMapper allocationMapper;
	
	
	public Map<String, Object> selectAllocation(Map<String, Object> map) throws Exception{
		return allocationMapper.selectAllocation(map);
	}
	
	public List<Map<String, Object>> selectAllocationList(Map<String, Object> map) throws Exception{
		return allocationMapper.selectAllocationList(map);
	}
	
	public int selectAllocationListCount(Map<String, Object> map) throws Exception{
		return allocationMapper.selectAllocationListCount(map);
	}
	
	public int insertAllocation(AllocationVO allocationVO) throws Exception{
		return allocationMapper.insertAllocation(allocationVO);
	}
	
	public int insertAllocationLogByVO(AllocationVO allocationVO) throws Exception{
		return allocationMapper.insertAllocationLogByVO(allocationVO);
	}
	
	public int insertAllocationLogByMap(Map<String, Object> map) throws Exception{
		return allocationMapper.insertAllocationLogByMap(map);
	}
	
	public void updateAllocationListOrder(Map<String, Object> map) throws Exception{
		allocationMapper.updateAllocationListOrder(map);
	}
	
	public void updateAllocation(AllocationVO allocationVO) throws Exception{
		allocationMapper.updateAllocation(allocationVO);
	}

	public Map<String, Object> selectAllocationByListOrder(Map<String, Object> map) throws Exception{
		return allocationMapper.selectAllocationByListOrder(map);
	}
	
	public void updateAllocationByMap(Map<String, Object> map) throws Exception{
		allocationMapper.updateAllocationByMap(map);
	}
	
	public void updateAllocationStatus(Map<String, Object> map) throws Exception{
		allocationMapper.updateAllocationStatus(map);
	}

	public List<Map<String, Object>> selectAllocationAllList() throws Exception{
		return allocationMapper.selectAllocationAllList();
	}
	
	public List<Map<String, Object>> selectAllocationListByBatchStatusId(Map<String, Object> map) throws Exception{
		return allocationMapper.selectAllocationListByBatchStatusId(map);
	}
	
	public List<Map<String, Object>> selectAllocationDownloadList(Map<String, Object> map) throws Exception{
		return allocationMapper.selectAllocationDownloadList(map);
	}
	
	public void updateAllocationSetPickup(Map<String, Object> map) throws Exception{
		allocationMapper.updateAllocationSetPickup(map);
	}
	
	public List<Map<String, Object>> selectAllocationListByPaymentDt(Map<String, Object> map) throws Exception{
		return allocationMapper.selectAllocationListByPaymentDt(map);
	}
	
	public int selectAllocationCountByPaymentDt(Map<String, Object> map) throws Exception{
		return allocationMapper.selectAllocationCountByPaymentDt(map);
	}
 	
	public List<Map<String,Object>> selectSearchModify(Map<String,Object>map)throws Exception{
		return allocationMapper.selectSearchModify(map);
	}
	
	public List<Map<String,Object>> selectListManageMiddle(Map<String,Object>map)throws Exception{
		return allocationMapper.selectListManageMiddle(map);
		
	}
	
	public void updateAllocationSetConfirm(Map<String, Object> map) throws Exception{
		allocationMapper.updateAllocationSetConfirm(map);
	}

	public void insertCustomerCall(Map<String, Object> map) throws Exception{
		 allocationMapper.insertCustomerCall(map);
	}
	
	public Map<String, Object> selectCustomerAppCall(Map<String, Object> map) throws Exception{
		return allocationMapper.selectCustomerAppCall(map);
	}
	public List<Map<String, Object>> selectAllocationListSub(Map<String, Object> map) throws Exception{
		return allocationMapper.selectAllocationListSub(map);
	}
	public int selectAllocationListCountSub(Map<String, Object> map) throws Exception{
		return allocationMapper.selectAllocationListCountSub(map);
	}
	public int insertSocketMessage(AlarmSocketVO alarmSocketVO) throws Exception{
		return allocationMapper.insertSocketMessage(alarmSocketVO);
	}
	
	public List<Map<String,Object>> selectAllocationAppConfirmList(Map<String,Object>map)throws Exception{
		return allocationMapper.selectAllocationAppConfirmList(map);
	}
	public ResultApi insertAllocation(HttpServletRequest request) throws Exception{
		ResultApi result = new ResultApi();
		
		int inputDataRow = 0;
		String cause = "";					//업로드 실패시 원인을 확인 할 수 있도록 한다.
		try{
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			String[] allocationIdArr =request.getParameterValues("allocationIdArr");
			
			Map<String, Object> fileMap = fileService.selectFile(paramMap);
			
	    	HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			String companyId = session.getAttribute("companyId").toString();
			
			
			List<AllocationVO> allocationList = new ArrayList<AllocationVO>();
			List<CarInfoVO> carInfoList = new ArrayList<CarInfoVO>();
			List<PaymentInfoVO> paymentInfoList = new ArrayList<PaymentInfoVO>();
			
			boolean duplication = false;
			List<String> allocationIdList = new ArrayList<String>();
			
				File file = new File(rootDir+"/"+subDir+fileMap.get("file_path").toString());
	
				XSSFWorkbook wb = null;
				XSSFRow row;
				XSSFCell cell;
				XSSFCell cellForSearch;
				
				try {//엑셀 파일 오픈
					wb = new XSSFWorkbook(new FileInputStream(file));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				XSSFSheet sheet = wb.getSheetAt(1);
		         //int rows = sheet.getPhysicalNumberOfRows();
				//int cells = sheet.getRow(0).getPhysicalNumberOfCells();
				int rows = sheet.getLastRowNum();
				int cells = sheet.getRow(1).getLastCellNum();
				
				FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
				
				 for (int r = 1; r <= rows; r++) {
					 inputDataRow = r+1;
					 AllocationVO allocationVO = new AllocationVO();
					 CarInfoVO carInfoVO = new CarInfoVO();
					 Map<String, Object> driverMap = new HashMap<String, Object>();
					 Map<String, Object> customerMap = new HashMap<String, Object>();
					 String allocationId = "ALO"+UUID.randomUUID().toString().replaceAll("-","");
					 allocationVO.setAllocationId(allocationId);
					 //allocationVO.setAllocationStatus("N");
					 
					 
					 /*allocationVO.setBatchStatus("N");
					 allocationVO.setBatchStatusId("BAT"+UUID.randomUUID().toString().replaceAll("-",""));
					 allocationVO.setBatchIndex(String.valueOf(0));*/
					 allocationVO.setRegisterId(userMap.get("emp_id").toString());
					 allocationVO.setRegisterName(userMap.get("emp_name").toString());
					 
					 //allocationVO.setCompanyId("company2");				//2019.01.31 엑셀 등록시 사업자정보를 무조건 한국카캐리어(법인)으로 등록 되게 함.
					 allocationVO.setCompanyId(companyId);				//2019.12.27 엑셀 등록시 소속된 회사의 아이디를 넣도록 수정
					 
					Map<String, Object> searchMap = new HashMap<String, Object>();
					searchMap.put("companyId", companyId);
					Map<String, Object> company = companyService.selectCompany(searchMap);
					Map<String,Object> customer= new HashMap<String, Object>();
					
					
					allocationVO.setCompanyName(company.get("company_name").toString());
					carInfoVO.setAllocationId(allocationId);
					carInfoVO.setReceiptSendYn("N");
					carInfoVO.setModId(userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
					allocationVO.setModId(userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자

					 String customerAmount = "";
					 String driverAmount = "";
					 String paymentKind = "";
					 String billingDivision = "";
					 FormulaEvaluator formulaEval = wb.getCreationHelper().createFormulaEvaluator();
					 
			        	row = sheet.getRow(r); // row 가져오기
			        	if (row != null) {
			        		for (int c = 0; c < cells; c++) {
			        			cell = row.getCell(c);
			        			if (cell != null) { 
			        				String value = "";

									switch (cell.getCellType()) {
									   	case XSSFCell.CELL_TYPE_FORMULA:
									   		
									   		CellValue evaluate = formulaEval.evaluate(cell);
									   	  if( evaluate != null ) {
									   		  
									   		  try {
									   			  
									   			  if(WebUtils.isNumber(evaluate.formatAsString())) {
									   				  
									   				value=  String.valueOf(new Double(evaluate.getNumberValue()).intValue());
									   				   
									   			  }else {
									   				  
									   				  value = evaluate.formatAsString();
									   			  }
									   			  
									   		  }catch(Exception e) {
									   			  e.printStackTrace();
									   			  
									   		  }
									   		  
									   		  
  			        				   }else {
  			        					   
  			        					   value = "";
  			        				   }
									   		
									   		/*
									   		if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_NUMERIC){
									   			value = "" + (int)cell.getNumericCellValue();
									   		}else if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_STRING){
									   			value = "" + cell.getStringCellValue(); 
									   		}
									   		value = cell.getCellFormula();
									   		*/
									   		
									   		break;
									   	case XSSFCell.CELL_TYPE_NUMERIC:
								            if (DateUtil.isCellDateFormatted(cell)){  
								                //날짜  
								            	Date date = cell.getDateCellValue();
								                value = String.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(date));
								            }else{  
								                //수치
								            	value = NumberToTextConverter.toText(cell.getNumericCellValue());
								            	
								                //value = String.valueOf(cell.getNumericCellValue());  
								            } 
									   		//value = "" + (int)cell.getNumericCellValue();
									   		break;
									   	case XSSFCell.CELL_TYPE_STRING:
									   		value = "" + cell.getStringCellValue();
									   		break;
									   	case XSSFCell.CELL_TYPE_BLANK:
									   		value = "";
									   		break;
									   	case XSSFCell.CELL_TYPE_ERROR:
									   		value = "" + cell.getErrorCellValue();
									   		break;
									   	default:
									}
									if(value != null && !value.equals("")) {
										value = value.trim();
									}
									int nIndex = cell.getColumnIndex();
									if(nIndex == 0){
										
										if(value != null && !value.equals("")) {
											Map<String, Object> map = new HashMap<String, Object>();
    										map.put("allocationDivision",value);
    										Map<String, Object> allocationDivision = allocationDivisionService.selectAllocationDivisionInfo(map);
    										carInfoVO.setCarrierType(allocationDivision.get("allocation_division_cd").toString());	
										}else {		//배차 구분이 지정 되어 있지 않으면 익셉션
											cause = "배차 구분이 지정 되지 않았습니다.";
											throw new Exception();
										}
									}else if(nIndex == 1){
										Map<String, Object> map = new HashMap<String, Object>();
										if(!value.equals("")) {
											map.put("runDivision",value);
    										Map<String, Object> runDivision = runDivisionService.selectRunDivisionInfo(map);	
    										if(runDivision != null) {
    											carInfoVO.setDistanceType(runDivision.get("run_division_cd").toString());			    											
    										}else {
    											carInfoVO.setDistanceType("C");	
    										}
    										
										}else {
											carInfoVO.setDistanceType("C");
										}
										
									}else if(nIndex == 2){
										
										if(!value.equals("")) {
											String dateDay = WebUtils.getDateDay(value);
    										allocationVO.setInputDt(value+" ("+dateDay+")");	
										}else {
											String dateDay = WebUtils.getDateDay(WebUtils.getNow("yyyy-MM-dd"));
    										allocationVO.setInputDt(WebUtils.getNow("yyyy-MM-dd")+" ("+dateDay+")");
										}
										
									}else if(nIndex == 3){
										carInfoVO.setDepartureDt(value);
									}else if(nIndex == 4){
										Map<String, Object> map = new HashMap<String, Object>();
										
										map.put("customerName",value);
										cellForSearch = row.getCell(6);
										String chargeName = "";
										
										if(cellForSearch != null) {
											chargeName = ""+cellForSearch.getStringCellValue();
										}
										if(!chargeName.equals("")) {
											map.put("chargeName",chargeName);	
											customer = customerService.selectCustomerByCustomerNameAndChargeName(map);
										}else {
											customer = customerService.selectCustomerByCustomerName(map);	
										}
										
										if(customer != null) {
											
											if(!customer.get("black_list_yn").toString().equals("Y")) {
    											allocationVO.setCustomerId(customer.get("customer_id").toString());
	    										allocationVO.setCustomerName(customer.get("customer_name").toString());
	    										allocationVO.setCustomerSignificantData(customer.get("significant_data") != null && !customer.get("significant_data").toString().equals("") ? customer.get("significant_data").toString():"");
											}else {
												cause = "해당 거래처가 블랙리스트로 등록 되어 있습니다.";
    											throw new Exception();
											}
											
										}else {		//거래처가 없는경우 익셉션
											cause = "거래처가 등록 되어 있지 않습니다.";
											throw new Exception();
										}
									}else if(nIndex == 5){
										//업체(담당자 부서로 사용 하고 있으나 현재 배차 등록에는 필요 하지 않아 일단 사용하지 않음..
									}else if(nIndex == 6){
										//업체의 담당자가 한명만 있는경우 그 담당자가 해당 배차의 담당자가 되도록 진행. 
										Map<String, Object> map = new HashMap<String, Object>();
										map.put("customerId",allocationVO.getCustomerId());
										map.put("name",value);
										if(!value.equals("")) {
											Map<String, Object> personInCharge = personInChargeService.selectPersonInCharge(map);
											if(value.equals("현대캐피탈")) {
												allocationVO.setHcYn("Y");
											}
											
    										if(personInCharge != null) {
    											allocationVO.setChargeName(personInCharge.get("name").toString());
    											allocationVO.setChargeId(personInCharge.get("person_in_charge_id").toString());
    											allocationVO.setChargePhone(personInCharge.get("phone_num") != null && !personInCharge.get("phone_num").toString().equals("") ? personInCharge.get("phone_num").toString():"");
    											allocationVO.setChargeAddr(personInCharge.get("address") != null && !personInCharge.get("address").toString().equals("") ? personInCharge.get("address").toString():"");
    										}else {
    											cause = "담당자 정보를 찾을 수 없습니다.";
    											throw new Exception();	
    										}
										}else {
											List<Map<String, Object>> personInChargeList = personInChargeService.selectPersonInChargeList(map);
											if(personInChargeList.size() == 1) {			//업체의 담당자가 한명인 경우 해당 배차의 담당자가 된다.
												Map<String, Object> personInCharge = personInChargeList.get(0);
												allocationVO.setChargeName(personInCharge.get("name").toString());
    											allocationVO.setChargeId(personInCharge.get("person_in_charge_id").toString());
    											allocationVO.setChargePhone(personInCharge.get("phone_num") != null && !personInCharge.get("phone_num").toString().equals("") ? personInCharge.get("phone_num").toString():"");
    											allocationVO.setChargeAddr(personInCharge.get("address") != null && !personInCharge.get("address").toString().equals("") ? personInCharge.get("address").toString():"");
											}else {
    											cause = "담당자가 지정되지 않았습니다.";
    											throw new Exception();	
    										}
										}
									}else if(nIndex == 7){
										//차종
										carInfoVO.setCarKind(value);
									}else if(nIndex == 8){
										//차대번호
										carInfoVO.setCarIdNum(value);
									}else if(nIndex == 9){
										//차량번호
										carInfoVO.setCarNum(value);
									}else if(nIndex == 10){
										//계약번호
										carInfoVO.setContractNum(value);
									}else if(nIndex == 11){
										//출발지
										if(value != null && !value.equals("")) {
											Map<String, Object> map = new HashMap<String, Object>();
    										map.put("keyword", value);
    										Map<String, Object> addressInfo = addressInfoService.selectAddressInfo(map);
    										if(addressInfo != null) {
    											carInfoVO.setDeparture(value);
    											carInfoVO.setDepartureAddr(addressInfo.get("address") != null ? addressInfo.get("address").toString():"");
    											carInfoVO.setDeparturePersonInCharge(addressInfo.get("name") != null ? addressInfo.get("name").toString():"");
    											carInfoVO.setDeparturePhone(addressInfo.get("phone_num") != null ? addressInfo.get("phone_num").toString():"");
    											
    										}else {
    											carInfoVO.setDeparture(value);
    										}
										}else {
											carInfoVO.setDeparture(value);
										}	    										
									}else if(nIndex == 12){
										//출발지상세주소
										if(value != "") {
											carInfoVO.setDepartureAddr(value);
										}else {
											if(carInfoVO.getDepartureAddr() == null || carInfoVO.getDepartureAddr().equals("")) {
    											carInfoVO.setDepartureAddr(value);	
    										}	
										}
									}else if(nIndex == 13){
										//상차지담당자명
										if(value != "") {
											carInfoVO.setDeparturePersonInCharge(value);
										}else {
											if(carInfoVO.getDeparturePersonInCharge() == null || carInfoVO.getDeparturePersonInCharge().equals("")) {
    											carInfoVO.setDeparturePersonInCharge(value);	
    										}	
										}
									}else if(nIndex == 14){
										//상차지담당자연락처
										if(value != "") {
											carInfoVO.setDeparturePhone(value);
										}else {
											if(carInfoVO.getDeparturePhone() == null || carInfoVO.getDeparturePhone().equals("")) {
    											carInfoVO.setDeparturePhone(value);	
    										}	
										}
									}else if(nIndex == 15){
										//하차지
										if(value != null && !value.equals("")) {
											Map<String, Object> map = new HashMap<String, Object>();
    										map.put("keyword", value);
    										Map<String, Object> addressInfo = addressInfoService.selectAddressInfo(map);
    										if(addressInfo != null) {
    											carInfoVO.setArrival(value);
    											carInfoVO.setArrivalAddr(addressInfo.get("address") != null ? addressInfo.get("address").toString():"");
    											carInfoVO.setArrivalPersonInCharge(addressInfo.get("name") != null ? addressInfo.get("name").toString():"");
    											carInfoVO.setArrivalPhone(addressInfo.get("phone_num") != null ? addressInfo.get("phone_num").toString():"");
    										}else {
    											carInfoVO.setArrival(value);
    										}
										}else {
											carInfoVO.setArrival(value);
										}
									}else if(nIndex == 16){
										//하차지상세주소
										if(value != "") {
											carInfoVO.setArrivalAddr(value);
										}else {
											if(carInfoVO.getArrivalAddr() == null || carInfoVO.getArrivalAddr().equals("")) {
    											carInfoVO.setArrivalAddr(value);	
    										}	
										}
									}else if(nIndex == 17){
										//하차지담당자명
										if(value != "") {
											carInfoVO.setArrivalPersonInCharge(value);
										}else {
											if(carInfoVO.getArrivalPersonInCharge() == null || carInfoVO.getArrivalPersonInCharge().equals("")) {
    											carInfoVO.setArrivalPersonInCharge(value);	
    										}	
										}
									}else if(nIndex == 18){
										//하차지담당자연락처
										if(value != "") {
											carInfoVO.setArrivalPhone(value);
										}else {
											if(carInfoVO.getArrivalPhone() == null || carInfoVO.getArrivalPhone().equals("")) {
    											carInfoVO.setArrivalPhone(value);	
    										}	
										}
									}else if(nIndex == 19){
										//기사명					//엑셀 등록시 기사명이 등록 되어 있더라도 배차 정보에서 수정 할 수 있도록....	    										
										if(!value.equals("")) {
											String driverName = value;
    										Map<String, Object> map = new HashMap<String, Object>();
    										map.put("driverName", driverName);
    										map.put("driverStatus", "01");
    										
    										driverMap = driverService.selectDriverByDriverName(map);
    										
    										if(driverMap != null) {
    											carInfoVO.setDriverId(driverMap.get("driver_id").toString());
    											carInfoVO.setDriverName(driverMap.get("driver_name").toString());	
    										}
    										//외부기사인 경우
    										if(driverMap != null && driverMap.get("customer_id") != null && !driverMap.get("customer_id").toString().equals("")) {
    											map.put("customerId", driverMap.get("customer_id").toString());
    											customerMap = customerService.selectCustomer(map);
    										}
										}
									}else if(nIndex == 20){
										//회전수
										if(!value.equals("")) {
											
											try {
												int driverCnt = Integer.parseInt(value);
											}catch(NumberFormatException e) {
												cause = "회전수가 잘못 입력 되었습니다.";
    											throw new Exception();
											}
											
											//driverMap.put("driverCnt", value);
											if(driverMap != null && driverMap.get("driver_id") != null) {
												carInfoVO.setDriverCnt(value);	
											}else {
												cause = "회전수가 지정되었지만 기사가 지정되지 않았습니다.";
    											throw new Exception();
											}
										}
									}else if(nIndex == 21){
										//특이사항//특이사항(비고)
										carInfoVO.setEtc(value);
									}else if(nIndex == 22){
										//car_cnt(대수)
										allocationVO.setCarCnt(value);
									}else if(nIndex == 23){
										//업체청구액
										if(value.equals("")) {
											value = "0";
										}
										customerAmount = value.replaceAll(",", "");
										carInfoVO.setSalesTotal(customerAmount);
										
									}else if(nIndex == 24){
										//부가세 구분
										
										if(!value.equals("")) {
											//포함인경우
											if(value.equals("포함")){
												carInfoVO.setVatIncludeYn("Y");
	    										carInfoVO.setVatExcludeYn("N");
	    										int total = Integer.parseInt(customerAmount);
	    										int vat = Integer.parseInt(customerAmount)/11;
	    										customerAmount = String.valueOf(Integer.parseInt(customerAmount)-vat);
	    										carInfoVO.setSalesTotal(customerAmount);
	    										carInfoVO.setVat(String.valueOf(vat));
	    										carInfoVO.setPrice(String.valueOf(total));
											//별도인경우
    										}else if(value.equals("별도")){
    											carInfoVO.setVatIncludeYn("N");
	    										carInfoVO.setVatExcludeYn("Y");
	    										int vat = Integer.parseInt(customerAmount)/10;
	    										int total = Integer.parseInt(customerAmount)+vat;
	    										carInfoVO.setVat(String.valueOf(vat));
	    										carInfoVO.setPrice(String.valueOf(total));
    										}else {
    											carInfoVO.setVat("0");
    											carInfoVO.setVatIncludeYn("N");
	    										carInfoVO.setVatExcludeYn("N");
    											carInfoVO.setPrice(customerAmount);
    										}
										}else {
											//포함도 아니고 별도도 아닌경우
											carInfoVO.setVat("0");
											carInfoVO.setVatIncludeYn("N");
    										carInfoVO.setVatExcludeYn("N");
											carInfoVO.setPrice(customerAmount);
										}
										
									}else if(nIndex == 25){
										//기사지급액
										if(value.equals("")) {
											value = "0";
										}
										driverAmount = value.replaceAll(",", "");
									}else if(nIndex == 28){
										
										//엑셀 업로드시 기사가 지정 된 경우 배차 승인 또는 배차 완료이다
										if(carInfoVO.getDriverId() != null && carInfoVO.getDriverCnt() != null && !carInfoVO.getDriverId().equals("") && !carInfoVO.getDriverCnt().equals("")) {
											//거래처 소속인 경우 탁송 완료
											if(customerMap != null && customerMap.get("customer_id") != null && !customerMap.get("customer_id").equals("")) {
												allocationVO.setAllocationStatus("F");	
												
											// 문자발송 여부가 Y일때 배차 승인대기 상태
												if(customer.get("sms_yn").equals("Y")) {
													allocationVO.setAllocationStatus("A");
												}
												
												
											}else {
												//거래처 소속이 아닌 경우는 배차 승인 대기 상태
												allocationVO.setAllocationStatus("A");	
											}
										}else {
											
											//기사 또는 회차 하나만 지정 되어 있는경우
											if(!carInfoVO.getDriverId().equals("") || !carInfoVO.getDriverCnt().equals("")) {
												if(!carInfoVO.getDriverId().equals("")) {
													cause = "기사가 지정 되었으나 회차가 지정 되지 않았습니다.";
	    											throw new Exception();
												}else {
													cause = "회차가 지정 되었으나 기사가 지정되지 않았습니다.";
	    											throw new Exception();	
												}
											}else {
												//미확정,미배차
    											if(value.equals("미확정")){
	    											allocationVO.setAllocationStatus("U");	
	    										}else if(value.equals("미배차")){
	    											allocationVO.setAllocationStatus("N");
	    										}else {
	    											//미확정도 아니고 미배차도 아닌경우 미배차 상태로...
	    											allocationVO.setAllocationStatus("N");
	    										}	
											}
										
										}
										
									}else if(nIndex == 29) {		//픽업관리
										if(!value.equals("")) {		//픽업건인경우
											allocationVO.setPickupNum(value);
											allocationVO.setBatchStatus("P");
										}else {							//픽업 아님
											allocationVO.setPickupNum("");
											allocationVO.setBatchStatus("N");
										}
										
									}else if(nIndex == 30) {		//사진제한
										if(!value.equals("")) {
											try {
												 Double.parseDouble(value);
												 carInfoVO.setRequirePicCnt(value);
											}catch(NumberFormatException e) {
												cause = "사진제한은 숫자로만 입력 가능 합니다.";
    											throw new Exception();
											}
										}else {							
											carInfoVO.setRequirePicCnt("3");
										}
									}else if(nIndex == 31) {		//사진제한
										if(!value.equals("")) {
											try {
												 Double.parseDouble(value);
												 carInfoVO.setRequireDnPicCnt(value);
											}catch(NumberFormatException e) {
												cause = "사진제한은 숫자로만 입력 가능 합니다.";
    											throw new Exception();
											}
										}else {							
											carInfoVO.setRequireDnPicCnt("3");
										}
									}else if(nIndex == 32) {		//결제방법
										if(!value.equals("")) {	    											
											paymentKind = value;
										}else {							
											cause = "결제방법이 입력 되지 않았습니다.";
											throw new Exception();
										}
									}else if(nIndex == 33) {		//증빙구분
										if(!value.equals("")) {
											billingDivision = value;
										}else {							
											cause = "증빙구분이 입력 되지 않았습니다.";
											throw new Exception();
										}
									}
									allocationVO.setBatchIndex(String.valueOf(0));		//픽업건여부에 관계 없이 index는 0
			        			} else {
			        				System.out.print("[null]\t");
			        			}
			        		} // for(c) 문
			        			
			        		if(carInfoVO.getCarrierType() != null && !carInfoVO.getCarrierType().equals("")) {		//배차 구분이 작성 되지 않은경우 등록 하지 않는다.
			        			
			        			
			        			Map<String, Object> findMap = new HashMap<String, Object>();
			        			findMap.put("carIdNum", carInfoVO.getCarIdNum()!=null ? carInfoVO.getCarIdNum() : "");
			        			findMap.put("carNum", carInfoVO.getCarNum()!=null ? carInfoVO.getCarNum() : "");
			        			findMap.put("contractNum", carInfoVO.getContractNum()!=null ? carInfoVO.getContractNum() : "");
			        			findMap.put("departure", carInfoVO.getDeparture()!=null ? carInfoVO.getDeparture() : "");
			        			findMap.put("arrival", carInfoVO.getArrival()!=null ? carInfoVO.getArrival() : "");
			        			
			        			if(!findMap.get("carIdNum").toString().equals("") || !findMap.get("carNum").toString().equals("") || !findMap.get("contractNum").toString().equals("")) {
			        				Map<String, Object> findResult = carInfoService.selectCarInfoForDuplicate(findMap);
			        				
			        				
			        				//중복된 배차건이 있으면.....
    			        			if(findResult != null) {
    			        				
    			        				boolean dupChk = false;
    			        				
    			        				for(int k = 0; k < allocationIdArr.length; k++) {
    			        					
    			        					if(allocationIdArr[k].equals(findResult.get("allocation_id").toString())){
    			        						dupChk = true;
    			        						cause = "중복된 배차 정보가 있습니다.";
    	    			        				//duplication = true;
    	    			        				//String updateAllocationId = findResult.get("allocation_id").toString();
    	    			        				Map<String,Object> updateMap = new HashMap<String, Object>(); 
    	    			        				updateMap.put("allocationId", findResult.get("allocation_id").toString());
    	    			        				updateMap.put("allocationStatus", "C");
    	    			        				updateMap.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
    	    			        				this.updateAllocationStatus(updateMap);
    	    			        				updateMap.put("logType","U");
    	    			        				this.insertAllocationLogByMap(updateMap);
    	    			        				
    	    			        				//allocationVO.setAllocationId(updateAllocationId);
    	    			        				//this.updateAllocation(allocationVO);
    	    			        				//carInfoVO.setAllocationId(updateAllocationId);
    	    			        				//carInfoService.updateCarInfo(carInfoVO);
    	    			        				//carInfoService.updateCarInfoForDuplication(carInfoVO);
    	    			        				
    			        					}
    			        				}
    			        				//체크 되지 않은 배차건에 대해 아무것도 하지 않는다.
    			        				if(!dupChk) {
    			        					continue;
    			        				}
    			        			}	
			        			}
			        			
			        			if(carInfoVO.getRequirePicCnt() == null || carInfoVO.getRequirePicCnt().equals("")) {
			        				cause = "상차사진 제한이 입력 되지 않았습니다.";
									throw new Exception();
			        			}
			        			if(carInfoVO.getRequireDnPicCnt() == null || carInfoVO.getRequireDnPicCnt().equals("")) {
			        				cause = "하차사진 제한이 입력 되지 않았습니다.";
									throw new Exception();
			        			}
			        			
			        			carInfoList.add(carInfoVO);
			        			//allocationService.insertAllocation(allocationVO);
    			        		//carInfoService.insertCarInfo(carInfoVO);
			        			int income = 0;
			        			
			        			if(paymentKind.equals("")) {
			        				cause = "결제방법이 입력 되지 않았습니다.";
									throw new Exception();
			        			}
			        			if(billingDivision.equals("")) {
			        				cause = "증빙구분이 입력 되지 않았습니다.";
									throw new Exception();
			        			}
			        			
    			        		for(int j = 0; j < 3; j++){
    								PaymentInfoVO paymentInfoVO = new PaymentInfoVO();
    								paymentInfoVO.setAllocationId(allocationId);
    								paymentInfoVO.setPaymentDivision("0"+(j+1));
    								paymentInfoVO.setPayment("N");
    								paymentInfoVO.setModId(userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
    								if(paymentInfoVO.getPaymentDivision().equals("01")) {
    									paymentInfoVO.setAmount(customerAmount);
    									paymentInfoVO.setPaymentPartner(allocationVO.getCustomerName());
    									paymentInfoVO.setPaymentPartnerId(allocationVO.getCustomerId());
    									paymentInfoVO.setPaymentKind(paymentKind);
    									paymentInfoVO.setBillingDivision(billingDivision);
    									paymentInfoVO.setVatIncludeYn(carInfoVO.getVatIncludeYn());
    									paymentInfoVO.setVatExcludeYn(carInfoVO.getVatExcludeYn());
    									
    									income += Integer.parseInt(customerAmount);
    								}else if(paymentInfoVO.getPaymentDivision().equals("02")) {
    									
    									if(customerMap == null || customerMap.get("customer_id") == null) {
    										if(driverMap != null && driverMap.get("driver_name") != null) {
    											paymentInfoVO.setPaymentPartner(driverMap.get("driver_name").toString());
    											paymentInfoVO.setPaymentPartnerId(driverMap.get("driver_id").toString());
    											paymentInfoVO.setDeductionRate(driverMap.get("deduction_rate").toString());
	    										int billForPayment = Integer.parseInt(driverAmount)-(Integer.parseInt(driverMap.get("deduction_rate").toString())*Integer.parseInt(driverAmount)/100);
	    										paymentInfoVO.setBillForPayment(String.valueOf(billForPayment));
	    										
	    										if(allocationVO.getHcYn().equals("Y") && driverMap.get("car_assign_company").toString().equals("S")){
	    											
	    											int deductionRate = 0;
	    											deductionRate = Integer.parseInt(driverMap.get("deduction_rate").toString());
	    											if(deductionRate > 10) {
	    		    			        				if(deductionRate == 15) {
	    		    			        					//지입기사이면 14% 아니면 15%
	    		    			        					if(driverMap.get("driver_kind") != null) {
	    		    			        						if(driverMap.get("driver_kind").equals("00") || driverMap.get("driver_kind").equals("03")) {
	    			    			        						deductionRate = 0;
	    			    			        					}else if(driverMap.get("driver_kind").equals("01")) {
	    			    			        						deductionRate = 4;
	    			    			        					}else if(driverMap.get("driver_kind").equals("02")) {
	    			    			        						deductionRate = 5;
	    			    			        					}else {
	    			    			        						deductionRate = 0;
	    			    			        					}
	    		    			        					}else {
	    		    			        						deductionRate = 0;
	    		    			        					}
	    		    			        					
	    		    			        				}else {
	    		    			        					deductionRate = deductionRate-10;
	    		    			        				}
	    		    			        			}else if(deductionRate == 10) {
	    		    			        				deductionRate = 0;
	    		    			        			} 
	    											
	    											int amount = Integer.parseInt(driverAmount);
	    											billForPayment = amount - (amount*deductionRate/100);
	    											paymentInfoVO.setBillForPayment(String.valueOf(billForPayment));
	    											paymentInfoVO.setDeductionRate(String.valueOf(deductionRate));
	    											
	    										}
	    										
    										}
    										if(paymentKind.equals("DD")) {
    											paymentInfoVO.setPaymentKind(paymentKind);
		    									paymentInfoVO.setBillingDivision(billingDivision);
    										}else {
    											paymentInfoVO.setPaymentKind("AT");
		    									paymentInfoVO.setBillingDivision("02");	
    										}
    										
    										if(paymentKind.equals("AT") || paymentKind.equals("CD")) {
    											paymentInfoVO.setVatIncludeYn("N");
		    									paymentInfoVO.setVatExcludeYn("Y");
    										}
    										
    										paymentInfoVO.setAmount(driverAmount);
    										income -= Integer.parseInt(driverAmount);	
    									}else {
    										paymentInfoVO.setAmount("0");
    									}
    									
    								}else if(paymentInfoVO.getPaymentDivision().equals("03")) {
    									
    									if(customerMap != null && customerMap.get("customer_id") != null && !customerMap.get("customer_id").equals("")) {
    										paymentInfoVO.setPaymentPartner(customerMap.get("customer_name").toString());
	    									paymentInfoVO.setPaymentPartnerId(customerMap.get("customer_id").toString());
	    									if(paymentKind.equals("DD")) {
    											paymentInfoVO.setPaymentKind(paymentKind);
		    									paymentInfoVO.setBillingDivision(billingDivision);
    										}else {
    											paymentInfoVO.setPaymentKind("AT");
		    									paymentInfoVO.setBillingDivision("02");	
    										}
	    									
	    									if(paymentKind.equals("AT") || paymentKind.equals("CD")) {
    											paymentInfoVO.setVatIncludeYn("N");
		    									paymentInfoVO.setVatExcludeYn("Y");
    										}
	    									
    										paymentInfoVO.setAmount(driverAmount);
    										income -= Integer.parseInt(driverAmount);
    									}else {
    										paymentInfoVO.setAmount("0");
    									}
    									
    								} 
    								paymentInfoList.add(paymentInfoVO);
    								//paymentInfoService.insertPaymentInfo(paymentInfoVO);
    							}	
    			        		allocationVO.setProfit(String.valueOf(income));
    			        		allocationList.add(allocationVO);
			        		}
			        	}
			        	
			        } // for(r) 문
				
		
	    		if(duplication) {
	    			
	    			//allocationIdList.add(carInfoVO.getAllocationId());
	    			//WebUtils.messageAndRedirectUrl(mav,"중복된 배차 정보가 있습니다.", "/allocation/duplication-list.do?allocationIdList="+allocationIdList);
	    		}
	    		
	    		for(int i = 0; i < allocationList.size(); i++) {		//null인 경우 처리
	    			if(allocationList.get(i).getAllocationStatus() == null) {
	    				allocationList.get(i).setAllocationStatus("N");
	    			}
	    			if(allocationList.get(i).getBatchStatus() == null) {
	    				allocationList.get(i).setBatchStatus("N");
	    				allocationList.get(i).setPickupNum("");
	    				allocationList.get(i).setBatchIndex(String.valueOf(0));		//픽업건여부에 관계 없이 index는 0
	    			}
	    		}
	    		
	    		for(int i = 0; i < allocationList.size(); i++) {
	    			
	    			//픽업 관리 번호 "" 가 아니고 관리번호가 같은 배차 정보는 픽업건으로 구분한다.	
	    			//픽업건은 batch_status가 P, batch_status_id가 같으나 batch_index는 0인 배차이다.
	    			String batchStatusId = "BAT"+UUID.randomUUID().toString().replaceAll("-","");
	    			if(allocationList.get(i).getBatchStatusId() == null || allocationList.get(i).getBatchStatusId().equals("")){	//
		    			allocationList.get(i).setBatchStatusId(batchStatusId);	
		    			AllocationVO allocation = allocationList.get(i);
		    			for(int j = i+1; j < allocationList.size(); j++) {
		    				if(allocation.getBatchStatus().equals("P") && allocation.getBatchStatus().equals(allocationList.get(j).getBatchStatus())){//픽업 건이고		
		    					if(allocation.getPickupNum().equals(allocationList.get(j).getPickupNum())){		//픽업 관리 번호가 같으면 같은 픽업건이다.
		    						allocationList.get(j).setBatchStatusId(batchStatusId);
		    					}
		    				}
		    			}
	    			}else {
	    					//여기서는 할게 없네...
	    			}
	    			/*AllocationVO allocation = allocationList.get(i);
	    			for(int j = i+1; j < allocationList.size(); j++) {
	    				if(allocation.getBatchStatus().equals("P") && allocation.getBatchStatus().equals(allocationList.get(j).getBatchStatus())){//픽업 건이고		
	    					if(allocation.getPickupNum().equals(allocationList.get(j).getPickupNum())){		//픽업 관리 번호가 같으면 같은 픽업건이다.
	    						allocationList.get(j).setBatchStatusId(batchStatusId);
	    					}
	    				}
	    			}*/
	    			
	    			this.insertAllocation(allocationList.get(i));
	    			allocationList.get(i).setLogType("I");
	    			allocationMapper.insertAllocationLogByVO(allocationList.get(i));
	    		}
	    		
	    		
	    		for(int i = 0; i < carInfoList.size(); i++) {
	    			
	    			//외부기사님이 아닌경우
	    			if(allocationList.get(i).getAllocationStatus().equals("A")){
	    				CarInfoVO carInfo = carInfoList.get(i);

	    				Map<String, Object> map = new HashMap<String, Object>();
						map.put("driverId", carInfo.getDriverId());
						Map<String, Object> driverMap = driverService.selectDriver(map);
						map.put("customerId", allocationList.get(i).getCustomerId());
						Map<String, Object> customer= customerService.selectCustomer(map);
						
						
						
						if(driverMap.get("fcm_token") != null && !driverMap.get("fcm_token").toString().equals("") || customer.get("sms_yn").equals("Y")) {
							Map<String, Object> sendMessageMap = new HashMap<String, Object>();
							sendMessageMap.put("title", "배차 정보가 도착 했습니다.");
							String carKind = "";
							String carIdNum = "";
							
							carKind = carInfo.getCarKind() != null && !carInfo.getCarKind().toString().equals("")? carInfo.getCarKind().toString():"정보없음";
							carIdNum = carInfo.getCarIdNum() != null && !carInfo.getCarIdNum().toString().equals("")? carInfo.getCarIdNum().toString():"정보 없음";
							String departure = carInfo.getDeparture() != null && !carInfo.getDeparture().toString().equals("") ? carInfo.getDeparture().toString() : "";
							String arrival = carInfo.getArrival() != null && !carInfo.getArrival().toString().equals("") ? carInfo.getArrival().toString() : "";
							
							
							
							if(allocationList.get(i).getAllocationStatus().equals("A") && driverMap.get("fcm_token")!= null) {
								sendMessageMap.put("body", "상차지:"+departure+",하차지:"+arrival+",차종:"+carKind+",차대번호:"+carIdNum);
								sendMessageMap.put("fcm_token", driverMap.get("fcm_token").toString());
								sendMessageMap.put("device_os", driverMap.get("device_os").toString());
								sendMessageMap.put("allocationId", carInfo.getAllocationId());
								fcmService.fcmSendMessage(sendMessageMap);	
							}
							
							Map<String, Object> alarmTalkMap = new HashMap<String, Object>();
							//알림톡 발송 구분		0 : 예약 완료,1:기사배정,2:상차완료,3:하차완료,4:인계완료,5:탁송완료,6탁송취소,7:배차정보(기사님에게),8:배차승인(배차담당직원)
							if(allocationList.get(i).getAllocationStatus().equals("N")) {
								alarmTalkMap.put("gubun", "0");
							}else if(allocationList.get(i).getAllocationStatus().equals("A")) {
								alarmTalkMap.put("gubun", "1");	
							}
							alarmTalkMap.put("departure", carInfo.getDeparture());
							alarmTalkMap.put("arrival", carInfo.getArrival());
							if(driverMap != null) {
								alarmTalkMap.put("driver_name", driverMap.get("driver_name"));
								alarmTalkMap.put("driver_id", driverMap.get("driver_id"));
								alarmTalkMap.put("phone_num", driverMap.get("phone_num").toString());
							}
							alarmTalkMap.put("car_kind", carInfo.getCarKind());
							alarmTalkMap.put("car_id_num", carInfo.getCarIdNum());
							alarmTalkMap.put("departure_dt", carInfo.getDepartureDt());
							alarmTalkMap.put("customer_id", customer.get("customer_id").toString());
							alarmTalkMap.put("customer_name", customer.get("customer_name").toString());
							alarmTalkMap.put("allocation_id", allocationList.get(i).getAllocationId());
							alarmTalkMap.put("departure_phone", carInfo.getDeparturePhone());
							alarmTalkMap.put("arrival_phone", carInfo.getArrivalPhone());
							
							//티케이물류 문자 발송시 문자 내용에 고객명 추가.
							String personInChargeStr = carInfo.getArrivalPersonInCharge() != null && !carInfo.getArrivalPersonInCharge().equals("") ? carInfo.getArrivalPersonInCharge() : "";
									
							personInChargeStr = personInChargeStr.replaceAll(" ", "");
							personInChargeStr = personInChargeStr.trim();
							
							String[] personInCharge = personInChargeStr.split(";");  
							alarmTalkMap.put("person_in_charge",personInCharge[0]);
							
							String paymentKind = "";
							String billingDivision = "";
							for(int k = 0; k < paymentInfoList.size(); k++) {
								//allocationId가 같고 paymentDivision이 01인데 payment_kind가 dd인 경우....
								if(carInfo.getAllocationId().equals(paymentInfoList.get(k).getAllocationId()) && paymentInfoList.get(k).getPaymentDivision().equals("01")) {
									if(paymentInfoList.get(k).getPaymentKind() != null) {
										if(paymentInfoList.get(k).getPaymentKind().equals("DD")) {		//결제 방법이 기사 수금이면
											paymentKind = "DD";
										}else {
											paymentKind = paymentInfoList.get(k).getPaymentKind();
										}
										if(paymentInfoList.get(k).getBillingDivision() != null) {
											billingDivision = paymentInfoList.get(k).getBillingDivision();
										}
									}
									
								}
							}
							alarmTalkMap.put("payment_kind",paymentKind);
							alarmTalkMap.put("customer_phone", allocationList.get(i).getChargePhone());
							alarmTalkMap.put("alarm_talk_yn", customer.get("alarm_talk_yn").toString());
							alarmTalkMap.put("send_account_info_yn", customer.get("send_account_info_yn").toString());
							alarmTalkMap.put("customer_kind",customer.get("customer_kind").toString());
							alarmTalkMap.put("alarmOrSmsYn", customer.get("alarm_or_sms_yn").toString());
							alarmTalkMap.put("billing_division",billingDivision);
							if(customer.get("alarm_talk_yn").toString().equals("Y") && allocationList.get(i).getChargePhone() != null && !allocationList.get(i).getChargePhone().equals("")) {
								alarmTalkService.alarmTalkSends(alarmTalkMap);	
							}
							if(customer.get("sms_yn").toString().equals("Y")) {
								sendSmsService.smsSend(alarmTalkMap);
							}
							
							Map<String, Object> driverSetEmpMap = new HashMap<String, Object>();
							driverSetEmpMap.put("empId", userMap.get("emp_id").toString());
							driverSetEmpMap.put("empName", userMap.get("emp_name").toString());
							driverSetEmpMap.put("allocationId",carInfo.getAllocationId());
							driverSetEmpMap.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
							driverSetEmpMap.put("logType", "U");
							carInfoService.updateCarInfoDriverSetEmp(driverSetEmpMap);
							carInfoService.insertCarInfoLogByMap(driverSetEmpMap); //로그기록
						}
	    				
	    			}
	    			
	    			
	    			//미배차인경우
	    			if(allocationList.get(i).getAllocationStatus().equals("N")){
	    				CarInfoVO carInfo = carInfoList.get(i);

	    				Map<String, Object> map = new HashMap<String, Object>();
						map.put("customerId", allocationList.get(i).getCustomerId());
						Map<String, Object> customer= customerService.selectCustomer(map);
											
						Map<String, Object> alarmTalkMap = new HashMap<String, Object>();
						//알림톡 발송 구분		0 : 예약 완료,1:기사배정,2:상차완료,3:하차완료,4:인계완료,5:탁송완료,6탁송취소,7:배차정보(기사님에게),8:배차승인(배차담당직원)
						alarmTalkMap.put("gubun", "0");
						alarmTalkMap.put("departure", carInfo.getDeparture());
						alarmTalkMap.put("arrival", carInfo.getArrival());
						alarmTalkMap.put("car_kind", carInfo.getCarKind());
						alarmTalkMap.put("car_id_num", carInfo.getCarIdNum());
						alarmTalkMap.put("departure_dt", carInfo.getDepartureDt());
						alarmTalkMap.put("customer_id", customer.get("customer_id").toString());
						alarmTalkMap.put("customer_name", customer.get("customer_name").toString());
						alarmTalkMap.put("allocation_id", allocationList.get(i).getAllocationId());
						alarmTalkMap.put("departure_phone", carInfo.getDeparturePhone());
						alarmTalkMap.put("arrival_phone", carInfo.getArrivalPhone());
						
						//티케이물류 문자 발송시 문자 내용에 고객명 추가.
						String personInChargeStr = carInfo.getArrivalPersonInCharge() != null && !carInfo.getArrivalPersonInCharge().equals("") ? carInfo.getArrivalPersonInCharge() : "";
								
						personInChargeStr = personInChargeStr.replaceAll(" ", "");
						personInChargeStr = personInChargeStr.trim();
						
						String[] personInCharge = personInChargeStr.split(";");  
						alarmTalkMap.put("person_in_charge",personInCharge[0]);
						
						String paymentKind = "";
						String billingDivision = "";
						for(int k = 0; k < paymentInfoList.size(); k++) {
							//allocationId가 같고 paymentDivision이 01인데 payment_kind가 dd인 경우....
							if(carInfo.getAllocationId().equals(paymentInfoList.get(k).getAllocationId()) && paymentInfoList.get(k).getPaymentDivision().equals("01")) {
								if(paymentInfoList.get(k).getPaymentKind() != null) {
									if(paymentInfoList.get(k).getPaymentKind().equals("DD")) {		//결제 방법이 기사 수금이면
										paymentKind = "DD";
									}else {
										paymentKind = paymentInfoList.get(k).getPaymentKind();
									}
									if(paymentInfoList.get(k).getBillingDivision() != null) {
										billingDivision = paymentInfoList.get(k).getBillingDivision();
									}
								}
								
							}
						}
						alarmTalkMap.put("payment_kind",paymentKind);
						alarmTalkMap.put("customer_phone", allocationList.get(i).getChargePhone());
						alarmTalkMap.put("alarm_talk_yn", customer.get("alarm_talk_yn").toString());
						alarmTalkMap.put("send_account_info_yn", customer.get("send_account_info_yn").toString());
						alarmTalkMap.put("customer_kind",customer.get("customer_kind").toString());
						alarmTalkMap.put("billing_division",billingDivision);
						alarmTalkMap.put("alarmOrSmsYn", customer.get("alarm_or_sms_yn").toString());
						if(customer.get("alarm_talk_yn").toString().equals("Y") && allocationList.get(i).getChargePhone() != null && !allocationList.get(i).getChargePhone().equals("")) {
							alarmTalkService.alarmTalkSends(alarmTalkMap);	
						}
						if(customer.get("sms_yn").toString().equals("Y") ) {
							sendSmsService.smsSend(alarmTalkMap);
						}
						
						Map<String, Object> driverSetEmpMap = new HashMap<String, Object>();
						driverSetEmpMap.put("empId", userMap.get("emp_id").toString());
						driverSetEmpMap.put("empName", userMap.get("emp_name").toString());
						driverSetEmpMap.put("allocationId",carInfo.getAllocationId());
						carInfoService.updateCarInfoDriverSetEmp(driverSetEmpMap);
					
    				
	    			}
	    			carInfoService.insertCarInfo(carInfoList.get(i));
	    			carInfoList.get(i).setLogType("I");
		    		carInfoService.insertCarInfoLogByVO(carInfoList.get(i)); //로그기록
	    		}
	    		for(int i = 0; i < paymentInfoList.size(); i++) {
	    			paymentInfoService.insertPaymentInfo(paymentInfoList.get(i));
	    			paymentInfoList.get(i).setLogType("I");
	    			paymentInfoService.insertPaymentInfoLogByVO(paymentInfoList.get(i)); //로그기록
	    		}
	    		
	    		
	    	
	    		
	    		
	    		
	    		
	    
			
			//WebUtils.messageAndRedirectUrl(mav,allocationList.size()+"건의 배차 정보가 등록되었습니다.", "/allocation/allocation-register.do");
		}catch(Exception e){
			//WebUtils.messageAndRedirectUrl(mav, inputDataRow+"번째 행을 확인 해 주세요.("+cause+")", "/allocation/allocation-register.do");
			e.printStackTrace();
		}
		
		return result;
	}
	


	
	public List<Map<String, Object>> selectAllocationListForAutoProcess(Map<String, Object> map) throws Exception{
		return allocationMapper.selectAllocationListForAutoProcess(map);
	}
	
	
	 public String paymentPrice(String distance) throws Exception{
		 
			
			
			// int test =	 Integer.parseInt(String.valueOf(Math.round(finalDistance)));
			 
			 
			 
			 
			 
			 int test =  Integer.parseInt(distance);
			 /*
		
			if(0<test && test <9999) {
				
					test = 50000;
		
				}else if(10000<= test && test<19999){
					
					test = 60000;
				}else if(20000<= test && test<29999){
				
					test = 70000;
				}else if(30000<= test && test<39999){
					
					test = 80000;
				}else if(40000<= test && test<49999){
					
					test = 90000;
					
				}else if(50000<= test && test <99999) {
					
				    double finalDistance = Math.ceil(test/1000);
					int highPrice =Integer.parseInt(String.valueOf(Math.round(finalDistance)));
					test = highPrice * 2000;
					
				}else if(100000<= test && test <149999) {
			   
					double finalDistance = Math.ceil(test/1000);
					int highPrice =Integer.parseInt(String.valueOf(Math.round(finalDistance)));
					test = highPrice * 1600;
									
				}else if(150000<= test && test<199999) {
					
					double finalDistance = Math.ceil(test/1000);
					int highPrice =Integer.parseInt(String.valueOf(Math.round(finalDistance)));
					test = highPrice * 1500;
					
				}else if(200000<= test && test<249999){
					
					double finalDistance = Math.ceil(test/1000);
					int highPrice =Integer.parseInt(String.valueOf(Math.round(finalDistance)));
					test = highPrice * 1450;
					
				}else if(250000<= test && test<299999) {
					
					double finalDistance = Math.ceil(test/1000);
					int highPrice =Integer.parseInt(String.valueOf(Math.round(finalDistance)));
					test = highPrice * 1400;
					
				}else if(300000<= test && test<349999) {
					
					double finalDistance = Math.ceil(test/1000);
					int highPrice =Integer.parseInt(String.valueOf(Math.round(finalDistance)));
					test = highPrice * 1350;
					
				}else if(350000<= test && test<399999) {
					
					double finalDistance = Math.ceil(test/1000);
					int highPrice =Integer.parseInt(String.valueOf(Math.round(finalDistance)));
					test = highPrice * 1300;
					
				}else if(400000<= test && test<449999) {
					
					double finalDistance = Math.ceil(test/1000);
					int highPrice =Integer.parseInt(String.valueOf(Math.round(finalDistance)));
					test = highPrice * 1250;
					
				}else if(450000<= test && test<499999) {
					
					double finalDistance = Math.ceil(test/1000);
					int highPrice =Integer.parseInt(String.valueOf(Math.round(finalDistance)));
					test = highPrice * 1200;
					
				}else if(500000<= test && test<549999) {
					
					double finalDistance = Math.ceil(test/1000);
					int highPrice =Integer.parseInt(String.valueOf(Math.round(finalDistance)));
					test = highPrice * 1150;
					
				}else if(550000<= test && test<599999) {
					
					double finalDistance = Math.ceil(test/1000);
					int highPrice =Integer.parseInt(String.valueOf(Math.round(finalDistance)));
					test = highPrice * 1100;
					
				}
			
				if(100000 <= test && test <= 200000 ) {
		        	  test = test+20000;
		        }
				*/
			 
			 
			 
			 
			 
			 	if(0 < test && test <= 10000) {
					test = 50000;
				}else if(10000 < test && test <= 15000){
					test = 60000;
				}else if(15000 < test && test <= 20000){
					test = 70000;
				}else if(20000 < test && test <= 25000){
					test = 80000;
				}else if(25000 < test && test <= 30000){
					test = 90000;
				}else if(30000 < test && test <= 35000) {
					test = 100000;
				}else if(35000 < test && test <= 40000) {
					test = 110000;
				}else if(40000 < test && test <= 45000) {
					test = 120000;
				}else if(45000 < test && test <= 50000) {
					test = 130000;
				}else if(50000 < test && test <= 55000) {
					test = 140000;
				}else if(55000 < test && test <= 60000) {
					test = 150000;
				}else if(60000 < test && test <= 65000) {
					test = 160000;
				}else if(65000 < test && test <= 70000) {
					test = 170000;
				}else if(70000 < test && test <= 75000) {
					test = 180000;
				}else if(75000 < test && test <= 80000) {
					test = 190000;
				}else if(80000 < test && test <= 90000) {
					test = 200000;
				}else if(90000 < test && test <= 100000) {
					test = 210000;
				}else if(100000 < test && test <= 110000) {
					test = 220000;
				}else if(110000 < test && test <= 120000) {
					test = 230000;
				}else if(120000 < test && test <= 130000) {
					test = 240000;
				}else if(130000 < test && test <= 140000) {
					test = 250000;
				}else if(140000 < test && test <= 150000) {
					test = 260000;
				}else if(150000 < test && test <= 160000) {
					test = 270000;
				}else if(160000 < test && test <= 170000) {
					test = 280000;
				}else if(170000 < test && test <= 180000) {
					test = 290000;
				}else if(180000 < test && test <= 190000) {
					test = 300000;
				}else if(190000 < test && test <= 200000) {
					test = 310000;
				}else if(200000 < test && test <= 210000) {
					test = 320000;
				}else if(210000 < test && test <= 220000) {
					test = 330000;
				}else if(220000 < test && test <= 230000) {
					test = 340000;
				}else if(230000 < test && test <= 240000) {
					test = 350000;
				}else if(240000 < test && test <= 250000) {
					test = 360000;
				}else if(250000 < test && test <= 260000) {
					test = 370000;
				}else if(260000 < test && test <= 270000) {
					test = 380000;
				}else if(270000 < test && test <= 280000) {
					test = 390000;
				}else if(280000 < test && test <= 290000) {
					test = 400000;
				}else if(290000 < test && test <= 300000) {
					test = 410000;
				}else if(300000 < test && test <= 310000) {
					test = 420000;
				}else if(310000 < test && test <= 320000) {
					test = 430000;
				}else if(320000 < test && test <= 330000) {
					test = 440000;
				}else if(330000 < test && test <= 340000) {
					test = 450000;
				}else if(340000 < test && test <= 350000) {
					test = 460000;
				}else if(350000 < test && test <= 360000) {
					test = 470000;
				}else if(360000 < test && test <= 370000) {
					test = 480000;
				}else if(370000 < test && test <= 380000) {
					test = 490000;
				}else if(380000 < test && test <= 390000) {
					test = 500000;
				}else if(390000 < test && test <= 400000) {
					test = 510000;
				}else if(400000 < test && test <= 410000) {
					test = 520000;
				}else if(410000 < test && test <= 420000) {
					test = 530000;
				}else if(420000 < test && test <= 430000) {
					test = 540000;
				}else if(430000 < test && test <= 440000) {
					test = 550000;
				}else if(440000 < test && test <= 450000) {
					test = 560000;
				}else if(450000 < test && test <= 460000) {
					test = 570000;
				}else if(460000 < test && test <= 470000) {
					test = 580000;
				}else if(470000 < test && test <= 480000) {
					test = 590000;
				}else if(480000 < test && test <= 490000) {
					test = 600000;
				}else if(490000 < test && test <= 500000) {
					test = 610000;
				}else if(500000 < test && test <= 510000) {
					test = 620000;
				}else if(510000 < test && test <= 520000) {
					test = 630000;
				}else if(520000 < test && test <= 530000) {
					test = 640000;
				}else if(530000 < test && test <= 540000) {
					test = 650000;
				}else if(540000 < test && test <= 550000) {
					test = 660000;
				}else if(550000 < test && test <= 560000) {
					test = 670000;
				}else if(560000 < test && test <= 570000) {
					test = 680000;
				}else if(570000 < test && test <= 580000) {
					test = 690000;
				}else if(580000 < test && test <= 590000) {
					test = 700000;
				}else if(590000 < test && test <= 600000) {
					test = 710000;
				} 
			 	
			 	/*
				if(100000 <= test && test <= 200000 ) {
		        	  test = test+20000;
		        }
			 	*/
			 
			 String payment =String.valueOf(test);
			
			 return payment;
		 }
	 
	 public int insertPageStatus(Map<String, String> map) throws Exception{
		 
		 return allocationMapper.insertPageStatus(map);
		 
	 }
	 
	 public int deletePageStatus(Map<String, String> map) throws Exception{
		 
		 return allocationMapper.deletePageStatus(map);
		 
	 }
	 
	public Map<String,String> allocationModCheck(Map<String,String>map)throws Exception{
			return allocationMapper.allocationModCheck(map);
		}


}





