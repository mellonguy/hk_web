package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.DriverBillingStatusMapper;
import kr.co.carrier.service.DriverBillingStatusService;
import kr.co.carrier.vo.DriverBillingStatusVO;

@Service("driverBillingStatusService")
public class DriverBillingStatusServiceImpl implements DriverBillingStatusService{

	
	@Resource(name="driverBillingStatusMapper")
	private DriverBillingStatusMapper driverBillingStatusMapper;
	
	
	
	
	public Map<String, Object> selectDriverBillingStatus(Map<String, Object> map) throws Exception{
		return driverBillingStatusMapper.selectDriverBillingStatus(map);
	}
	
	public List<Map<String, Object>> selectDriverBillingStatusList(Map<String, Object> map) throws Exception{
		return driverBillingStatusMapper.selectDriverBillingStatusList(map);
	}
	
	public int selectDriverBillingStatusListCount(Map<String, Object> map) throws Exception{
		return driverBillingStatusMapper.selectDriverBillingStatusListCount(map);
	}
	
	public int insertDriverBillingStatus(DriverBillingStatusVO driverBillingStatusVO) throws Exception{
		return driverBillingStatusMapper.insertDriverBillingStatus(driverBillingStatusVO);
	}
	
	public void deleteDriverBillingStatus(Map<String, Object> map) throws Exception{
		driverBillingStatusMapper.deleteDriverBillingStatus(map);
	}
	
	public void deleteDriverBillingStatusByDriverIdAndDecideMonth(Map<String, Object> map) throws Exception{
		driverBillingStatusMapper.deleteDriverBillingStatusByDriverIdAndDecideMonth(map);
	}
	
	
	
	
}
