package kr.co.carrier.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import kr.co.carrier.mapper.AccountMapper;
import kr.co.carrier.service.AccountService;
import kr.co.carrier.service.DriverDeductInfoService;
import kr.co.carrier.vo.AccountVO;

@Service("accountService")
public class AccountServiceImpl implements AccountService{

	
	@Resource(name="accountMapper")
	private AccountMapper accountMapper;
	
	
	@Autowired
	private DriverDeductInfoService driverDeductInfoService;
	
	
	public Map<String, Object> selectAccount(Map<String, Object> map) throws Exception{
		return accountMapper.selectAccount(map);
	}
	
	public List<Map<String, Object>> selectAccountList(Map<String, Object> map) throws Exception{
		return accountMapper.selectAccountList(map);
	}
	
	public int selectAccountListCount(Map<String, Object> map) throws Exception{
		return accountMapper.selectAccountListCount(map);
	}
	
	public int insertAccount(AccountVO accountVO) throws Exception{
		return accountMapper.insertAccount(accountVO);
	}
		
	public void deleteAccount(Map<String, Object> map) throws Exception{
		accountMapper.deleteAccount(map);
	}
	
	public List<Map<String, Object>> selectSummary(Map<String, Object> map) throws Exception{
		return accountMapper.selectSummary(map);
	}
	
	public List<Map<String, Object>> selectCustomerId(Map<String, Object> map) throws Exception{
		return accountMapper.selectCustomerId(map);
	}
	
	public int selectCustomerTotalCount (Map<String, Object> map) throws Exception{
		return accountMapper.selectCustomerTotalCount(map);
	}
	
	 public List<Map<String, Object>> selectEmployeeSummary (Map<String, Object> map) throws Exception{
         return accountMapper.selectEmployeeSummary(map);
      }
      
      public int insertBatchTeam (Map<String,Object>map) throws Exception {
         return accountMapper.insertBatchTeam(map);
         
      }
      
      public int selectDeadLineCount (Map<String,Object>map) throws Exception {
         return accountMapper.selectDeadLineCount(map);
         
      }
      public List<Map<String, Object>> selectPaymentList(Map<String, Object> map) throws Exception{
         return accountMapper.selectPaymentList(map);
      }
      public List<Map<String, Object>> selectAllocationstatus(Map<String, Object> map) throws Exception{
         return accountMapper.selectAllocationstatus(map);
      }
      
      public List<Map<String,Object>>selectBatchTeam (Map <String,Object>map) throws Exception{
         return accountMapper.selectBatchTeam(map);
   }
      public List<Map <String,Object>>selectEmployeeBatchDetail(Map<String,Object>map)throws Exception{
         return accountMapper.selectEmployeeBatchDetail(map);
         
      }
      public int selectBatchTeamTotalCount(Map<String, Object> map) throws Exception{
         return accountMapper.selectBatchTeamTotalCount(map);
      }

      public List<Map<String,Object>>selectTeamSummary (Map<String,Object>map)throws Exception{
    	  return accountMapper.selectTeamSummary(map);
      }
      public int selectMonthTotalCount(Map<String,Object>map)throws Exception{
    	  return accountMapper.selectMonthTotalCount(map);
      }
      public List<Map<String,Object>> selectForYear (Map<String,Object>map)throws Exception{
    	  	return accountMapper.selectForYear(map);
      }
      public List<Map<String,Object>>selectQuarterAndSemiannually(Map<String,Object>map)throws Exception{
    	  return accountMapper.selectQuarterAndSemiannually(map);
      }
	
      public List<Map<String,Object>>selectTeamPaymentList (Map<String,Object>map)throws Exception{
    	  return accountMapper.selectTeamPaymentList(map);
      }
	
      public List<Map<String,Object>>selectEmployeePaymentList(Map<String,Object>map)throws Exception{
    	  return accountMapper.selectEmployeePaymentList(map);
      }
	
      public List<Map<String,Object>>selectCarrierPaymentList(Map<String,Object>map)throws Exception{
    	  return accountMapper.selectCarrierPaymentList(map);
      }
      public List<Map<String,Object>>selectRecivableList(Map<String,Object>map)throws Exception{
    	  return accountMapper.selectRecivableList(map);
      }
      public int selectRecivableCount(Map<String,Object>map)throws Exception{
    	  return accountMapper.selectRecivableCount(map);
      }
      
      public List<Map<String,Object>>selectRecivableDetail(Map<String, Object> map) throws Exception{
    	  return accountMapper.selectRecivableDetail(map);
      }
      public int selectRecivableDetailCount(Map<String,Object>map)throws Exception{
    	  return accountMapper.selectRecivableDetailCount(map);
      }
      @Transactional(propagation=Propagation.REQUIRED, rollbackFor={Exception.class})
      public void afterProcessDecideFinal(Map<String, Object> map) throws Exception{
    	  
    	  //최종 확정 후 공제 내역을 조회 하고 지급 처리를 진행 한다. 
    	  try {
    		  
    		  /**
				String decideFinalId = "DFI"+UUID.randomUUID().toString().replaceAll("-","");
				map.put("paymentPartnerId", URLDecoder.decode(paymentPartnerIdArr[i], "UTF-8"));
				map.put("searchDateType", searchDateType);
				map.put("startDt", startDt);
				map.put("endDt", endDt);
				map.put("searchType", searchType);
				map.put("searchWord", URLDecoder.decode(searchWord, "UTF-8"));
				map.put("currentDecideStatus", currentDecideStatus);
				map.put("decideFinal", decideFinal);
				map.put("carrierType", carrierType);
				map.put("currentDecideFinal", currentDecideFinal);
				map.put("decideFinalId", decideFinalId);
				map.put("decideMonth", decideMonthArr[i]);
    		   
    		   */
    		  
    		  
    		  // driverId=2005172020&selectMonth=2020-06
    		  
    		  
    		  
    		  //기사의 총 공제 금액을 가져온다.
    		  Map<String, Object> paramMap = new HashMap<String, Object>();
    		  paramMap.put("driverId", map.get("paymentPartnerId").toString());
    		  paramMap.put("selectMonth", map.get("decideMonth").toString());
    		  
    		  List<Map<String, Object>> driverList = driverDeductInfoService.selectDriverDeductInfoList(paramMap);
    		  
    		  
    		  
    		  
    		  
    		  
    		  
    		  String test = "";
    		  
    		  
    		  
    		  
    		  
    		  
    		  
    		  
    		  
    		  
    		  
    	  }catch(Exception e) {
    		  
    		  e.printStackTrace();
    		  
    	  }
    	  
    	  
    	  
      }
	
}
