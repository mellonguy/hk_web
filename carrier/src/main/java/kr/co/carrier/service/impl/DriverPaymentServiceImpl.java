package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.DriverPaymentMapper;
import kr.co.carrier.service.DriverPaymentService;
import kr.co.carrier.vo.DriverPaymentVO;
import kr.co.carrier.vo.PaymentDriverVO;

@Service("driverPaymentService")
public class DriverPaymentServiceImpl implements DriverPaymentService{

	
	@Resource(name="driverPaymentMapper")
	private DriverPaymentMapper driverPaymentMapper;
	
	public Map<String, Object> selectDriverPayment(Map<String, Object> map) throws Exception{
		return driverPaymentMapper.selectDriverPayment(map);
	}
	
	public List<Map<String, Object>> selectDriverPaymentList(Map<String, Object> map) throws Exception{
		return driverPaymentMapper.selectDriverPaymentList(map);
	}
	
	public int selectDriverPaymentListCount(Map<String, Object> map) throws Exception{
		return driverPaymentMapper.selectDriverPaymentListCount(map);
	}
	
	public int selectDriverPaymentOwnerListCount(Map<String, Object> map) throws Exception{
		return driverPaymentMapper.selectDriverPaymentOwnerListCount(map);
	}
	
	public int selectDriverPaymentDriverListCount(Map<String, Object> map) throws Exception{
		return driverPaymentMapper.selectDriverPaymentDriverListCount(map);
	}
	
	
	public int insertDriverPayment(DriverPaymentVO driverPaymentVO) throws Exception{
		return driverPaymentMapper.insertDriverPayment(driverPaymentVO);
	}
	
	public void deleteDriverPayment(Map<String, Object> map) throws Exception{
		driverPaymentMapper.deleteDriverPayment(map);
	}
	
	public void updateDriverPayment(DriverPaymentVO driverPaymentVO) throws Exception{
		driverPaymentMapper.updateDriverPayment(driverPaymentVO);
	}
	
	public List<Map<String, Object>>  selectDriverPaymentOwnerList(Map<String, Object> map) throws Exception{
		return driverPaymentMapper.selectDriverPaymentOwnerList(map);
	}
	
	public List<Map<String, Object>>  selectDriverPaymentDriverList(Map<String, Object> map) throws Exception{
		return driverPaymentMapper.selectDriverPaymentDriverList(map);
	}	
	
	public int insertPaymentDriver(PaymentDriverVO paymentDriverVO) throws Exception{
		return driverPaymentMapper.insertPaymentDriver(paymentDriverVO);
	}
	
	public List<Map<String, Object>>  selectDriverPaymentDetailList(Map<String, Object> map) throws Exception{
		return driverPaymentMapper.selectDriverPaymentDetailList(map);
	}
	public void updatePaymentDriver(PaymentDriverVO paymentDriverVO) throws Exception{
		driverPaymentMapper.updatePaymentDriver(paymentDriverVO);
	}
	
	public List<Map<String, Object>>  selectCarMiddlePaymentDriverList(Map<String, Object> map) throws Exception{
		return driverPaymentMapper.selectCarMiddlePaymentDriverList(map);
	}
	public void updateExemptionsPaymentM(Map<String, Object> map) throws Exception{
		driverPaymentMapper.updateExemptionsPaymentM(map);
	}
	
	
	
	
	
	
}
