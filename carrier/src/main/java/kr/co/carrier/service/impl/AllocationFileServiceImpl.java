package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import kr.co.carrier.mapper.AllocationFileMapper;
import kr.co.carrier.service.AllocationFileService;
import kr.co.carrier.service.FileUploadService;
import kr.co.carrier.utils.ResultApi;
import kr.co.carrier.vo.AllocationFileVO;

@Service("allocationFileService")
public class AllocationFileServiceImpl implements AllocationFileService{
	
	
	@Resource(name="allocationFileMapper")
	private AllocationFileMapper allocationFileMapper;
	
	@Autowired
	private FileUploadService fileUploadService;
	
public ResultApi insertAllocationFile(String rootDir, String subDir,String allocationId,String allocationStatus,HttpServletRequest request) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
	    	List<MultipartFile> fileList = multipartRequest.getFiles("bbsFile");
			
	    	if(fileList != null && fileList.size() > 0){
	    		for(MultipartFile mFile : fileList){	
	    			if(mFile != null && mFile.getSize() > 0){
	    				AllocationFileVO allocationFileVO = new AllocationFileVO();
	    				Map<String, Object> fileMap = fileUploadService.upload(rootDir, subDir, mFile);
	    				
	    				allocationFileVO.setAllocationId(allocationId);
	    				allocationFileVO.setAllocationStatus(allocationStatus);
	    				allocationFileVO.setCategoryType(subDir);
	    				allocationFileVO.setAllocationFileId("FLE"+UUID.randomUUID().toString().replaceAll("-", ""));
	    				allocationFileVO.setAllocationFileNm(fileMap.get("ORG_NAME").toString());
	    				allocationFileVO.setAllocationFilePath(fileMap.get("SAVE_NAME").toString());
	    				allocationFileMapper.insertAllocationFile(allocationFileVO);		
	    				
	    			}
	    		}
	    	}else{
	    		result.setResultCode("1111"); 	//file 없음
	    	}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return result; 
		
	}
	
	public void insertAllocationFile(AllocationFileVO allocationFileVO) throws Exception {
		allocationFileMapper.insertAllocationFile(allocationFileVO);
	}

	public List<Map<String, Object>> selectAllocationFileList(Map<String, Object> map) throws Exception{
		return allocationFileMapper.selectAllocationFileList(map);
	}
	
	public void deleteAllocationFile(Map<String, Object> map) throws Exception{
		allocationFileMapper.deleteAllocationFile(map);
	}
	public void insertCaSendAllocationFile(Map<String, Object> map) throws Exception{
	      allocationFileMapper.insertCaSendAllocationFile(map);
	      
	      
	   }
	

}
