package kr.co.carrier.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.SendSmsMapper;
import kr.co.carrier.service.AlarmTalkService;
import kr.co.carrier.service.CustomerService;
import kr.co.carrier.service.EmployeeService;
import kr.co.carrier.service.FileService;
import kr.co.carrier.service.SendSmsService;
import kr.co.carrier.utils.BaseAppConstants;
import kr.co.carrier.utils.WebUtils;
import kr.co.carrier.vo.SendSmsVO;

@Service("sendSmsService")
public class SendSmsServiceImpl implements SendSmsService {

	
	
	@Resource(name="sendSmsMapper")
	private SendSmsMapper sendSmsMapper;
	
	
	
	@Value("#{appProp['upload.base.path']}")
    private String rootDir;    
    private String subDir = "bbs";	
	
    private static String histUrl; // sms/alarmTalk url
    
    @Value("#{appProp['hist.url']}")
    public void setKey(String value) {
      histUrl = value;
    }
    
	@Autowired
	private FileService fileService;
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private AlarmTalkService alarmTalkService;
	
	@Autowired
	private EmployeeService employeeService;
	
	public Map<String, Object> selectSendSms(Map<String, Object> map) throws Exception{
		return sendSmsMapper.selectSendSms(map);
	}
	
	public List<Map<String, Object>> selectSendSmsList(Map<String, Object> map) throws Exception{
		return sendSmsMapper.selectSendSmsList(map);
	}
	
	public int selectSendSmsListCount(Map<String, Object> map) throws Exception{
		return sendSmsMapper.selectSendSmsListCount(map);
	}
	
	public int insertSendSms(SendSmsVO sendSmsVO) throws Exception{
		return sendSmsMapper.insertSendSms(sendSmsVO);
	}
	
	public void deleteSendSms(Map<String, Object> map) throws Exception{
		
	}
	
	
	
	public String smsSend(Map<String, Object> map)throws Exception{
		
		StringBuilder urlBuilderAT = new StringBuilder(histUrl+"/sms/histsms");

		String resultMsg = "";
		String result = "";
//		알림톡 발송 구분       0 : 예약 완료,1:기사배정,2:상차완료,3:하차완료,4:인계완료,5:탁송완료,6탁송취소,7:배차정보(기사님에게),8:배차승인(배차담당직원)
		
		urlBuilderAT.append("?");
		urlBuilderAT.append(URLEncoder.encode("uid","UTF-8") + "=" + URLEncoder.encode(BaseAppConstants.COMPANY_TYPE_NATIONAL, "UTF-8") + "&");
		
		String customerId = map.get("customer_id").toString();
		
		Map<String, Object> searchMap = new HashMap<String, Object>();
		searchMap.put("customerId", customerId);
		
		Map<String, Object> customerMap = customerService.selectCustomer(searchMap);
		
		if(customerMap != null) {
				
				
				if(map.get("gubun").toString().equals("1")) {			//기사배정
		
					if(customerMap.get("sms_yn").toString().equals("Y")) {
						
						//if(customerId.equals("CUS0f3a8db538d74ff580efea0530a08bf2")) {
						//고객이 티케이물류이고 상차지가 티케이물류인 건(2차탁송)  
						//if((customerId.equals("CUSa8b74209a826425698ef189f61f08ff6") && map.get("departure").toString().contains("티케이물류"))) {
						
						//고객이 티케이물류이고 하차지가 티케이물류가 아닌건(2차탁송)  
						//if((customerId.equals("CUSa8b74209a826425698ef189f61f08ff6") && !map.get("arrival").toString().contains("티케이물류"))) {
						if(customerId.equals("CUSa8b74209a826425698ef189f61f08ff6") && map.get("arrival") != null && !map.get("arrival").toString().contains("티케이물류")) {	
						
							resultMsg += "ㅁ NH농협캐피탈 탁송안내\r\n";
							if(map.get("person_in_charge") != null && !map.get("person_in_charge").toString().equals("")) {
								resultMsg += "- 고객명 : ";
								resultMsg += map.get("person_in_charge") != null && !map.get("person_in_charge").toString().equals("") ? map.get("person_in_charge").toString():"";
								resultMsg += "\r\n";
							}
							resultMsg += "- 차   종 : ";
							resultMsg += map.get("car_kind") != null && !map.get("car_kind").toString().equals("") ? map.get("car_kind").toString():"미정";
							resultMsg += "\r\n";
							resultMsg += "- 출발일 : "+ map.get("departure_dt").toString()+"\r\n";
							resultMsg += "- 출발지 : 경기 화성시\r\n";
							resultMsg += "- 하차지 : "+ map.get("arrival").toString()+"\r\n";
							resultMsg += "- 기사명 : "+ map.get("driver_name").toString()+"\r\n";
							resultMsg += "- 연락처 : "+ map.get("phone_num").toString()+"\r\n";
							resultMsg += "해당 차량이 기사님께 배정 되었습니다."+"\r\n";
							
						}
						
						urlBuilderAT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode(resultMsg, "UTF-8") + "&");
						urlBuilderAT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
						
					}
					
					
				}else if(map.get("gubun").toString().equals("2")) {	//상차완료
					resultMsg += "고객님의 차량이 안전하게 상차 되었습니다.";
					urlBuilderAT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode( resultMsg, "UTF-8") + "&");
					urlBuilderAT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
					urlBuilderAT.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_001", "UTF-8") + "&");
					
					//hkc_001
				}else if(map.get("gubun").toString().equals("3")) {	//하차완료
		
					
				}else if(map.get("gubun").toString().equals("4")) {	//인계완료
					
					
				}else if(map.get("gubun").toString().equals("5")) {	//탁송완료
					
					
					if(customerMap.get("sms_yn").toString().equals("Y")) {
						
						//if(customerId.equals("CUS0f3a8db538d74ff580efea0530a08bf2")) {
						//고객이 티케이물류이고 상차지가 티케이물류인 건(2차탁송)   
						//if(customerId.equals("CUSa8b74209a826425698ef189f61f08ff6") && map.get("departure") != null && map.get("departure").toString().contains("티케이물류")) {
						if(customerId.equals("CUSa8b74209a826425698ef189f61f08ff6") && map.get("arrival") != null && !map.get("arrival").toString().contains("티케이물류")) {
							
							resultMsg += "ㅁ NH농협캐피탈 탁송안내\r\n";
							if(map.get("person_in_charge") != null && !map.get("person_in_charge").toString().equals("")) {
								resultMsg += "- 고객명 : ";
								resultMsg += map.get("person_in_charge") != null && !map.get("person_in_charge").toString().equals("") ? map.get("person_in_charge").toString():"";
								resultMsg += "\r\n";
							}
							resultMsg += "- 차   종 : ";
							resultMsg += map.get("car_kind") != null && !map.get("car_kind").toString().equals("") ? map.get("car_kind").toString():"미정";
							resultMsg += "\r\n";
							resultMsg += "- 하차지 : "+ map.get("arrival").toString()+"\r\n";
							resultMsg += "해당 차량의 탁송이 완료 되었습니다."+"\r\n";
							
						}
						
						urlBuilderAT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode(resultMsg, "UTF-8") + "&");
						urlBuilderAT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
						
					}
					
					
				
				}else if(map.get("gubun").toString().equals("6")) {	//탁송취소
					
					urlBuilderAT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode( resultMsg+"문서 알림.\r\n 도착하였습니다.\r\n결재/합의 바랍니다.", "UTF-8") + "&");
					urlBuilderAT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
					urlBuilderAT.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_005", "UTF-8") + "&");
				
					
				}else if(map.get("gubun").toString().equals("7")) {	//배차정보
				
					urlBuilderAT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode( resultMsg+"문서 알림.\r\n 도착하였습니다.\r\n결재/합의 바랍니다.", "UTF-8") + "&");
					urlBuilderAT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
					urlBuilderAT.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_003", "UTF-8") + "&");
				
					
				}else if(map.get("gubun").toString().equals("8")) {	//배차승인
				
					
					urlBuilderAT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode( resultMsg+"문서 알림.\r\n 도착하였습니다.\r\n결재/합의 바랍니다.", "UTF-8") + "&");
					urlBuilderAT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
					urlBuilderAT.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_004", "UTF-8") + "&");
				
					
					
					
				}else if(map.get("gubun").toString().equals("9")) {
				
					
				}
				
				
				int getHour = Integer.parseInt(WebUtils.getNow("HH"));
		
				String arrivalPhone = map.get("arrival_phone") == null || map.get("arrival_phone").toString().equals("") ? "":map.get("arrival_phone").toString();
				
				//문자 서비스 사용시 등록된 전화번호가 없는경우 기본적으로 15775268(한국카캐리어 대표전화)로 발송 되도록 한다.
				String callback = customerMap.get("call_back_for_sms") == null || customerMap.get("call_back_for_sms").toString().equals("") ? "15775268" : customerMap.get("call_back_for_sms").toString();
				
				arrivalPhone = arrivalPhone.replaceAll("-", "");
				arrivalPhone = arrivalPhone.replaceAll(" ", "");
				arrivalPhone = arrivalPhone.replaceAll("/", ";");
				arrivalPhone = arrivalPhone.trim();
				
				// 하차지 담당자(고객)의 전화 번호가 없는경우 알림톡을 발송 하도록 설정...........
				if(arrivalPhone.equals("") || callback.equals("")) {
					//alarmTalkService.alarmTalkSends(map);
				    return "jsonView";
				}
				
				if(!resultMsg.equals("")) {
					
					urlBuilderAT.append(URLEncoder.encode("rcvphns","UTF-8") + "=" + URLEncoder.encode(arrivalPhone, "UTF-8"
							+ "") + "&");
					//urlBuilderAT.append(URLEncoder.encode("rcvphns","UTF-8") + "=" + URLEncoder.encode("01048644300", "UTF-8") + "&");
					urlBuilderAT.append(URLEncoder.encode("callback","UTF-8") + "=" + URLEncoder.encode(callback, "UTF-8") + "&");
					urlBuilderAT.append(URLEncoder.encode("subject","UTF-8") + "=" + URLEncoder.encode("기사배정안내", "UTF-8") + "&");
					
					//기사 지정시에만 발송 한다...
					if(map.get("gubun").toString().equals("1")) {
						urlBuilderAT.append(URLEncoder.encode("snddttm","UTF-8") + "=" + URLEncoder.encode(WebUtils.getOneHourLater(), "UTF-8") + "&");	
					}else {
						return "jsonView";
					}
					
					URL url = new URL(urlBuilderAT.toString());
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					conn.setDoOutput(true);
					conn.setRequestMethod("POST");
					conn.setRequestProperty("Content-Type", "application/json");        
					conn.setDoOutput(true);
						
					OutputStream os = conn.getOutputStream();
					os.flush();
					os.close();
					int responseCode = conn.getResponseCode();
					System.out.println("\nSending 'POST' request to URL : " + url);
					System.out.println("Response Code : " + responseCode);
						
					BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
					String inputLine = "";
					StringBuffer response = new StringBuffer();
					while ((inputLine = in.readLine()) != null) {
						response.append(inputLine);
					}
					in.close();
					System.out.println(response.toString());
					
					if(responseCode == 200 && response.toString().equals("0")) {
						
						String sendSmsId = "SMS"+UUID.randomUUID().toString().replaceAll("-","");
						SendSmsVO sendSmsVO = new SendSmsVO();
						sendSmsVO.setCallback(callback);
						sendSmsVO.setCustomerId(customerId);
						sendSmsVO.setMsg(resultMsg);
						sendSmsVO.setRcvphns(arrivalPhone);
						sendSmsVO.setSendSmsId(sendSmsId);
						sendSmsVO.setType(map.get("gubun").toString());
						sendSmsVO.setAllocationId(map.get("allocation_id").toString());
						sendSmsVO.setReturnMsg(response.toString());
						try {
							this.insertSendSms(sendSmsVO);	
						}catch(Exception e) {
							e.printStackTrace();
						}
						
					}
					
				}
				
		}
		

		return "jsonView";
		
		
	}
	
	
	public int insertSendCertNum(Map<String, Object> map) throws Exception{
		
		StringBuilder urlBuilder = new StringBuilder(histUrl+"/sms/histsms");
		String result="";
		
		urlBuilder.append("?");
		urlBuilder.append(URLEncoder.encode("uid","UTF-8") + "=" + URLEncoder.encode(BaseAppConstants.COMPANY_TYPE_NATIONAL, "UTF-8") + "&");
		
		
		try {
			
			String phoneNum = map.get("phoneNum").toString().replaceAll("-", "");
			phoneNum = phoneNum.replaceAll(" ", "");
			phoneNum = phoneNum.trim();
			
			
			String resultMsg ="";	
			
			resultMsg +="요청하신 인증번호 : ";
			resultMsg +="[";
			resultMsg += map.get("phoneCertNum").toString();
			resultMsg +="]"+"\r\n";
			resultMsg +=" 화면창에 번호를 입력하세요 ";
			
            urlBuilder.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode(resultMsg,"UTF-8") + "&");
	        urlBuilder.append(URLEncoder.encode("rcvphns","UTF-8") + "=" + URLEncoder.encode(phoneNum, "UTF-8") + "&");
	        urlBuilder.append(URLEncoder.encode("callback","UTF-8") + "=" + URLEncoder.encode("15775268", "UTF-8") + "&");
	        urlBuilder.append(URLEncoder.encode("subject","UTF-8") + "=" + URLEncoder.encode("인증번호가 도착했습니다.", "UTF-8") + "&");

	    	
	        map.put("phoneNum", phoneNum);
	        
	        URL url = new URL(urlBuilder.toString());
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=euc-kr");        
			conn.setDoOutput(true);
				
			OutputStream os = conn.getOutputStream();
			os.flush();
			os.close();
			int responseCode = conn.getResponseCode();
			System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);
				
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String inputLine = "";
			StringBuffer response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			System.out.println(response.toString());
	        	       
	        
		}catch(Exception e) {
		e.printStackTrace();	
			
		}
				
		return sendSmsMapper.insertSendCertNum(map);
	}
		

			 
}


