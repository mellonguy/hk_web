package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.DriverLocationInfoMapper;
import kr.co.carrier.service.DriverLocationInfoService;
import kr.co.carrier.vo.DriverLocationInfoVO;

@Service("driverLocationInfoService")
public class DriverLocationInfoServiceImpl implements DriverLocationInfoService {

	
	@Resource(name="driverLocationInfoMapper")
	private DriverLocationInfoMapper driverLocationInfoMapper;
	
	
	public Map<String, Object> selectDriverLocationInfo(Map<String, Object> map) throws Exception{
		return driverLocationInfoMapper.selectDriverLocationInfo(map);
	}
	
	public List<Map<String, Object>> selectDriverLocationInfoList(Map<String, Object> map) throws Exception{
		return driverLocationInfoMapper.selectDriverLocationInfoList(map);
	}
	
	public int selectDriverLocationInfoListCount(Map<String, Object> map) throws Exception{
		return driverLocationInfoMapper.selectDriverLocationInfoListCount(map);
	}
	
	public int insertDriverLocationInfo(DriverLocationInfoVO driverLocationInfoVO) throws Exception{
		return driverLocationInfoMapper.insertDriverLocationInfo(driverLocationInfoVO);
	}
	
	public void deleteDriverLocationInfoList(Map<String, Object> map) throws Exception{
		driverLocationInfoMapper.deleteDriverLocationInfoList(map);
	}
	
	
	
	
	
	
	
	
	
	
	
}
