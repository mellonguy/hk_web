package kr.co.carrier.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;



public interface FileUploadService {
    
    /**
     *  파일을 업로드한다.
     *
     * @param MultipartFile
     * @return Map
     * @throws Exception
     */
    public Map<String, Object> upload(String rootDir, String subDir, MultipartFile mf) throws Exception;
    
    
    
    public Map<String, Object> upload(String rootDir, String subDir,String fileName,XSSFWorkbook wb) throws Exception;
    
    
    public Map<String, Object> upload(String rootDir, String subDir,String fileName,HSSFWorkbook wb) throws Exception;
    
    /**
     *  이미지 파일을 업로드한다.
     *
     * @param MultipartFile
     * @return Map
     * @throws Exception
     */
    public Map<String, Object> uploadImage(String rootDir, String subDir, MultipartFile mf) throws Exception;
    
    /**
     *  멀티 파일을 업로드한다.
     *
     * @param MultipartFile
     * @return Map
     * @throws Exception
     */
    public List<Map<String, Object>> uploadList(HttpServletRequest request, String rootDir, String subDir) throws Exception;
    
    /**
     *  멀티 이미지 파일을 업로드한다.
     *
     * @param MultipartFile
     * @return Map
     * @throws Exception
     */
    public List<Map<String, Object>> uploadImageList(HttpServletRequest request, String rootDir, String subDir) throws Exception;    
}
