package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.SmsSendReserveMapper;
import kr.co.carrier.service.SmsSendReserveService;
import kr.co.carrier.vo.SmsSendReserveVO;

@Service("smsSendReserveService")
public class SmsSendReserveServiceImpl implements SmsSendReserveService{

	
	
	@Resource(name="smsSendReserveMapper")
	private SmsSendReserveMapper smsSendReserveMapper;
	
	
	
	public Map<String, Object> selectSmsSendReserve(Map<String, Object> map) throws Exception{
		return smsSendReserveMapper.selectSmsSendReserve(map);
	}
	
	public List<Map<String, Object>> selectSmsSendReserveList(Map<String, Object> map) throws Exception{
		return smsSendReserveMapper.selectSmsSendReserveList(map);
	}
	
	public int selectSmsSendReserveListCount(Map<String, Object> map) throws Exception{
		return smsSendReserveMapper.selectSmsSendReserveListCount(map);
	}
	
	public int insertSmsSendReserve(SmsSendReserveVO smsSendReserveVO) throws Exception{
		return smsSendReserveMapper.insertSmsSendReserve(smsSendReserveVO);
	}
	
	public void deleteSmsSendReserve(Map<String, Object> map) throws Exception{
		smsSendReserveMapper.deleteSmsSendReserve(map);
	}
	
	
	
	
	
	
	
	
	
	
}
