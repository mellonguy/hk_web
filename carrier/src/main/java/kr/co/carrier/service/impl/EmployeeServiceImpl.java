package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.EmployeeMapper;
import kr.co.carrier.vo.EvaluationCompleteVO;
import kr.co.carrier.service.EmployeeService;
import kr.co.carrier.vo.AlarmSocketVO;
import kr.co.carrier.vo.EmployeeVO;
import kr.co.carrier.vo.EvaluationVO;

@Service("employeeService")
public class EmployeeServiceImpl implements EmployeeService{

	@Resource(name="employeeMapper")
	private EmployeeMapper employeeMapper;
	
	
	public Map<String, Object> selectEmployee(Map<String, Object> map) throws Exception{
		return employeeMapper.selectEmployee(map);
	}
	
	public List<Map<String, Object>> selectEmployeeList(Map<String, Object> map) throws Exception{
		return employeeMapper.selectEmployeeList(map);
	}
	
	public int selectEmployeeListCount(Map<String, Object> map) throws Exception{
		return employeeMapper.selectEmployeeListCount(map);
	}
	
	public int insertEmployee(EmployeeVO employeeVO) throws Exception{
		return employeeMapper.insertEmployee(employeeVO);
	}
	
	public void updateEmployee(EmployeeVO employeeVO) throws Exception{
		employeeMapper.updateEmployee(employeeVO);
	}
	
	public void updateEmployeeApproveYn(Map<String, Object> map) throws Exception{
		employeeMapper.updateEmployeeApproveYn(map);
	}
	
	public void updateEmployeeRole(Map<String, Object> map) throws Exception{
		employeeMapper.updateEmployeeRole(map);
	}
	
	public void updateEmployeeAllocation(Map<String, Object> map) throws Exception{
		employeeMapper.updateEmployeeAllocation(map);
	}
	
	public void updateEmployeeStatus(Map<String, Object> map) throws Exception{
		employeeMapper.updateEmployeeStatus(map);
	}
	
	public void updateEmployeeListRowCnt(Map<String, Object> map) throws Exception{
		employeeMapper.updateEmployeeListRowCnt(map);
	}
	
	public void updateEmployeeCompanyId(Map<String, Object> map) throws Exception{
		employeeMapper.updateEmployeeCompanyId(map);
	}
	
	public Map<String,Object> selectIdChectkForPassword(Map<String,Object>map) throws Exception{
		return employeeMapper.selectIdChectkForPassword(map);
		
	}
	public Map<String,Object>selectCheckCertNumForPassword(Map<String,Object>map) throws Exception{
		return employeeMapper.selectCheckCertNumForPassword(map);
		
	}
	public void updateCreatePassword(Map<String, Object> map) throws Exception{
		employeeMapper.updateCreatePassword(map);
	}
	public List<Map<String,Object>>selectEvlauationListEmpName(Map<String, Object> map) throws Exception{
		return employeeMapper.selectEvlauationListEmpName(map);
	}
	public List<Map<String,Object>>selectEvaluationQuestion(Map<String, Object> map) throws Exception{
		return employeeMapper.selectEvaluationQuestion(map);
	}
	
	public void insertEvaluation(EvaluationVO evaluationVO) throws Exception{
	     employeeMapper.insertEvaluation(evaluationVO);
	}
	public void insertEvaluationComplete(EvaluationCompleteVO evaluationCompleteVO) throws Exception{
		employeeMapper.insertEvaluationComplete(evaluationCompleteVO);
	}
	
	public List<Map<String,Object>>selectEvaluationList(Map<String, Object> map) throws Exception{
		return employeeMapper.selectEvaluationList(map);
	}
	public List<String>selectEvaluationQuestionIdList() throws Exception{
		return employeeMapper.selectEvaluationQuestionIdList();
	}
	public void deleteEvaluationData(Map<String,Object>map)throws Exception{
		employeeMapper.deleteEvaluationData(map);
		
	}
	public List<Map<String,Object>>selectForFinalPointList(Map<String, Object> map) throws Exception{
		return employeeMapper.selectForFinalPointList(map);
	}
	public List<Map<String,Object>>selectEvaluationCompleteList(Map<String, Object> map) throws Exception{
		return employeeMapper.selectEvaluationCompleteList(map);
	}
	public int insertAlarmBoard(AlarmSocketVO alarmSocketVO) throws Exception{
		return employeeMapper.insertAlarmBoard(alarmSocketVO);
	}
	public List<Map<String,Object>>selectEmpNote(Map<String, Object> map) throws Exception{
		return employeeMapper.selectEmpNote(map);
	}
	public int insertEmployeeMessage(Map<String, Object> map) throws Exception{
		return employeeMapper.insertEmployeeMessage(map);
	}
	public List<Map<String,Object>>selectEmployeeNoteList(Map<String, Object> map) throws Exception{
		return employeeMapper.selectEmployeeNoteList(map);
	}
	public void deleteEmployeeMessage(Map<String,Object>map)throws Exception{
		employeeMapper.deleteEmployeeMessage(map);
	}
	public void updateMessage(Map<String, Object> map) throws Exception{
		employeeMapper.updateMessage(map);
	}
	
	
}
