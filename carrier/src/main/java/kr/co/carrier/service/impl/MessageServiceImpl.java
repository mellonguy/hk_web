package kr.co.carrier.service.impl;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import kr.co.carrier.controller.AllocationController;
import kr.co.carrier.mapper.AlarmTalkMapper;
import kr.co.carrier.mapper.SendSmsMapper;
import kr.co.carrier.service.MessageService;
import kr.co.carrier.utils.BaseAppConstants;
import kr.co.carrier.utils.MultipartUtility;
import kr.co.carrier.vo.AlarmTalkVO;
import kr.co.carrier.vo.MessageVO;
import kr.co.carrier.vo.SendSmsVO;

@Service("messagekService")
public class MessageServiceImpl implements MessageService{

	private static final Logger logger = LoggerFactory.getLogger(AllocationController.class);
	
	@Value("#{appProp['upload.base.path']}")
    private String rootDir;    
    private String subDir = "bbs";	
	
    @Resource(name="sendSmsMapper")
	private SendSmsMapper sendSmsMapper;
    
    @Resource(name="alarmTalkMapper")
    private AlarmTalkMapper alarmTalkMapper;
    
    private static String histUrl; // sms/alarmTalk url
    
    @Value("#{appProp['hist.url']}")
    public void setKey(String value) {
      histUrl = value;
    }
    
    @Value("#{appProp['hist.api.id']}")
    private String username;    
    @Value("#{appProp['hist.api.pw']}")
    private String password;    
    @Value("#{appProp['hist.api.clientid']}")
    private String clientid;    
    @Value("#{appProp['hist.api.clientsecret']}")
    private String clientsecret;    
    
    private static String histApiUrl; // sms/alarmTalk Api url
    @Value("#{appProp['hist.api.url']}")
    public void setValue(String value) {
      histApiUrl = value;
    }
    
	//알람톡발송
	public String alarmTalkSends(Map<String, Object> map, Map userSessionMap)throws Exception{
        logger.debug("//***** MessageServiceImple.alarmTalkSends *****//");
        String rtMessage ="";
        String Msg = "";
        String customerPhone = "";
        try {
        	StringBuilder urlBuilderAT = new StringBuilder(histUrl+"/sms/histmsg");
    	    // 메시지 본문 내용
    	    Msg = map.get("message").toString();
    	    
    	    // 받을사람 전화번호
    		customerPhone = map.get("customer_phone").toString().replaceAll("-", "");
    		customerPhone = customerPhone.replaceAll(" ", "");
    		customerPhone = customerPhone.trim();
    		String[] customerPhoneArr = customerPhone.split(",");
    		
    		ArrayList<String> arrayList = new ArrayList<String>();
    		
    		for(String data : customerPhoneArr){
    		    if(!arrayList.contains(data)){
    		        arrayList.add(data);
    		   }
    		}
    				
    		customerPhoneArr = arrayList.toArray(new String[arrayList.size()]);
    		customerPhone = "";
    		
    		for(int k = 0; k < customerPhoneArr.length; k++) {
    			customerPhone += customerPhoneArr[k]+";";
    		}
    	    
    		urlBuilderAT.append("?");
    		urlBuilderAT.append(URLEncoder.encode("uid","UTF-8") + "=" + URLEncoder.encode(BaseAppConstants.COMPANY_TYPE_NATIONAL, "UTF-8") + "&");		
    		urlBuilderAT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode(Msg, "UTF-8") + "&");
    		urlBuilderAT.append(URLEncoder.encode("rcvphns","UTF-8") + "=" + URLEncoder.encode(customerPhone, "UTF-8") + "&");
    		urlBuilderAT.append(URLEncoder.encode("profileKey","UTF-8") + "=" + URLEncoder.encode("dd3f81c615756d611bd9fb625ca58f2717d4ca15", "UTF-8") + "&");
    		urlBuilderAT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
    		urlBuilderAT.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_000", "UTF-8") +"&");    		
    		urlBuilderAT.append(URLEncoder.encode("adMsgYn","UTF-8") + "=" + URLEncoder.encode("N", "UTF-8"));     		//광고 아님
    		
    		URL url = new URL(urlBuilderAT.toString());
    		
    		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    		conn.setDoOutput(true);
    		conn.setRequestMethod("POST");
    		conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");        
    		conn.setDoOutput(true);

    		OutputStream os = conn.getOutputStream();
    		os.flush();
    		os.close();

    		int responseCode = conn.getResponseCode();
    		logger.debug("\nSending 'POST' request to URL : " + url);
    		logger.debug("Response Code : " + responseCode);
    		        
    		BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
    		String inputLine;
    		StringBuffer response = new StringBuffer();

    		while ((inputLine = in.readLine()) != null) {
    			response.append(inputLine);
    		}
    		in.close();
    		logger.debug(response.toString());
            rtMessage = response.toString(); 
		} catch (Exception e) {
			e.printStackTrace();			
		}finally {
            AlarmTalkVO alarmTalkVO = new AlarmTalkVO();
	        String alarmTalkId = "ATI"+UUID.randomUUID().toString().replaceAll("-","");
	        alarmTalkVO.setAlarmTalkId(alarmTalkId);
	        alarmTalkVO.setAllocationId("");
	        alarmTalkVO.setCustomerId(userSessionMap.get("emp_id").toString());
	        alarmTalkVO.setMsg(Msg);
	        alarmTalkVO.setRcvphns(customerPhone);
	        alarmTalkVO.setTemplate("");
	        alarmTalkVO.setType("12"); //WEB 메시지 알림톡 수동발송 
	        alarmTalkVO.setReturnMsg(rtMessage); 
	        
	        alarmTalkMapper.insertAlarmTalk(alarmTalkVO);	
		}
		
       
        return rtMessage;
}

	//문자발송
	public String lmsSendMessage(Map<String, Object> map , Map userSessionMap)throws Exception{
        
        logger.debug("//***** MessageServiceImple.lmsSendMessage *****//");
		String rtMessage ="";
		String Msg = "";
		String customerPhone ="";
        try {
        	
    	    Msg = map.get("message").toString();
    		customerPhone = map.get("customer_phone").toString().replaceAll("-", "");
    		customerPhone = customerPhone.replaceAll(" ", "");
    		customerPhone = customerPhone.trim();
    		String[] customerPhoneArr = customerPhone.split(",");
    		
    		ArrayList<String> arrayList = new ArrayList<String>();
    		for(String data : customerPhoneArr){
    		    if(!arrayList.contains(data)){
    		        arrayList.add(data);
    		        
    		   }
    		}
    				
    		customerPhoneArr = arrayList.toArray(new String[arrayList.size()]);
    		customerPhone = "";
    		for(int k = 0; k < customerPhoneArr.length; k++) {
    			customerPhone += customerPhoneArr[k]+";";
    		}
    		StringBuilder urlBuilderAT = new StringBuilder(histUrl+"/sms/histsms");
    		urlBuilderAT.append("?");
    		urlBuilderAT.append(URLEncoder.encode("uid","UTF-8") + "=" + URLEncoder.encode(BaseAppConstants.COMPANY_TYPE_NATIONAL, "UTF-8") + "&");
    		urlBuilderAT.append(URLEncoder.encode("rcvphns","UTF-8") + "=" + URLEncoder.encode(customerPhone, "UTF-8") + "&");
    		urlBuilderAT.append(URLEncoder.encode("callback","UTF-8") + "=" + URLEncoder.encode("15775268", "UTF-8") + "&");
    		urlBuilderAT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode(Msg, "UTF-8") + "&");
    		urlBuilderAT.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_000", "UTF-8") + "&");
    		urlBuilderAT.append(URLEncoder.encode("profileKey","UTF-8") + "=" + URLEncoder.encode("dd3f81c615756d611bd9fb625ca58f2717d4ca15", "UTF-8") + "&");
    		
    		
    		URL url = new URL(urlBuilderAT.toString());
    		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    		conn.setDoOutput(true);
    		conn.setRequestMethod("POST");
    		conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");        
    		conn.setDoOutput(true);
    			
    		OutputStream os = conn.getOutputStream();
    		os.flush();
    		os.close();
    		int responseCode = conn.getResponseCode();
    		logger.debug("\nSending 'POST' request to URL : " + url);
    		logger.debug("Response Code : " + responseCode);
    			
    		BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
    		String inputLine = "";
    		StringBuffer response = new StringBuffer();
    		while ((inputLine = in.readLine()) != null) {
    			response.append(inputLine);
    		}
    		in.close();
    		logger.debug(response.toString());
    		
    		rtMessage = response.toString();
			
		} catch (Exception e) {
			e.printStackTrace();
			
		}finally {
			String sendSmsId = "SMS"+UUID.randomUUID().toString().replaceAll("-","");
			SendSmsVO sendSmsVO = new SendSmsVO();
			sendSmsVO.setCallback("15775268");
			sendSmsVO.setCustomerId(userSessionMap.get("emp_id").toString());
			sendSmsVO.setMsg(Msg);
			sendSmsVO.setRcvphns(customerPhone);
			sendSmsVO.setSendSmsId(sendSmsId);
			sendSmsVO.setType("11"); // WEB 메시지 SMS 수동 발송
			sendSmsVO.setAllocationId("수동발송");
			sendSmsVO.setReturnMsg(rtMessage);
			
			sendSmsMapper.insertSendSms(sendSmsVO);
		}
		

			return rtMessage;
		
	}
	

	
	public static byte[] base64Enc(byte[] buffer) {
	    return Base64.encodeBase64(buffer);
	}
		
	public String fileUpload(File file) throws Exception {
		
		String response = "";
		
		String imgOriginalPath = file.getAbsolutePath() ;         //원본 이미지 경로
        String imgTargetPath =rootDir+"/allocationSend/"+file.getName();            //새 이미지 파일명
        String ext = file.getName().substring(file.getName().lastIndexOf(".")+1,file.getName().length());
        String imgFormat       = ext;
        int newWidth        =720;
        int newHeight       =720;
        String mainPosition    =   "";
        
        BufferedImage image;
        int imageWidth;
        int imageHeight;
        double ratio;
        int w;
        int h;
		
		try {
			
			String urlApi = histUrl+"/sms/add_msg_image";
			
			Map<String, String> headers = new HashMap<String, String>();
	        headers.put("HIST-uid", BaseAppConstants.COMPANY_TYPE_NATIONAL);
	        headers.put("HIST-profileKey", "dd3f81c615756d611bd9fb625ca58f2717d4ca15");
	        
	        MultipartUtility multipartUtility = new MultipartUtility(urlApi, headers);

	        // File part
	        //File file = new File("/img/test.jpg");        // 업로드할 이미지
	        multipartUtility.addFilePart("image", file);

	        response = multipartUtility.finish();
	        logger.debug("response : " + response);
			
	        JSONParser jsonParser = new JSONParser();
	         JSONObject jsonObject = (JSONObject) jsonParser.parse(response);
	        
	        //String fileName = file.getName();
	        //String ext = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length());
	        if(!jsonObject.get("code").toString().equals("success")) {
                
                try {
                   
                   if(jsonObject.get("code").toString().equals("fail")) {      
                      
                      //사진 사이즈 조정 후 재 전송
                      image =ImageIO.read(new File(imgOriginalPath));
                      imageWidth = image.getWidth(null);
                      imageHeight= image.getHeight(null);
                      
                     
                      w= newWidth;
                      h= newHeight;
                         
                     
                      
                      Image resizeImage = image.getScaledInstance(w, h, image.SCALE_SMOOTH);
                      
                      //변경 이미지 저장 
                      BufferedImage newImage = new BufferedImage(w, h,BufferedImage.TYPE_INT_RGB);
                    Graphics g = newImage.getGraphics();
                      g.drawImage(resizeImage, 0,0,null);
                      g.dispose();
                      
                      File saveDir = new File(rootDir+"/allocationSend");
                      
                      if(saveDir.exists()==false) {
                         saveDir.mkdir();
                      }
                   
                      File newFile = new File(imgTargetPath);
                      
                      ImageIO.write(newImage, imgFormat,newFile);
                      //this.fileUpload(newFile);
                    
                      multipartUtility = new MultipartUtility(urlApi, headers);
                      multipartUtility.addFilePart("image", newFile);
                      response = multipartUtility.finish();
                      logger.debug("response : " + response);
                      
                   }
                }catch(Exception e) {
                   e.printStackTrace();
                }
                
             }else {
                return response;
             }
	        
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return response;
		
	}
	
	
	// --------------------------- 신규 API 적용 --------------------------------//

	public JsonNode getAuthToken()throws Exception{
        logger.debug("//***** MessageServiceImple.getAuthToken *****//");
        JsonNode jsonNode = null;
        try {
        	StringBuilder requestUrl = new StringBuilder(histApiUrl+"/oauth/token");
        	/** 요청 URL SAMPLE
        	/*
			 * -X POST \ -H
			 * "Authorization: Basic cccccccccccclientidcleintsecretttttttttttt" \ -d
			 * "username={username}&password={password}&grant_type=password" \
			 * http://hisms.co.kr:8686/api/oauth/token
			 * 
			 * curl -X POST
			 *  -H 'Content-Type: application/x-www-form-urlencoded'
			 *  -H 'Authorization: Basic V1BnWjlDTUdiMkhlNHZMU0c0MUI6aEpBcVNIN2NNclFnbDU3bXdHN05yak1BT0FCUVlCN2VpeUh0OEtyZEE4eEJzQ3Y5UHdOb1N4aEFmQmZO'
			 *  -d 'username=hkcarcarrier&password=hkcarcarrier1!&grant_type=password'
			 *  http://hisms.co.kr:8686/api/oauth/token
			 * 
			 * {"access_token":"BY1ASVZklHciII0p+XWT6YowjjzUJpA/diene31/CvU/+mKAI7lpvwXVvNsFPOasif/1jg0O78uz+xl60MJTk6ihy3cbdyawbHQirWxbEHyzWi5gryVjpmXVdvzUwRqo/LyUnIE2dXtiV3/lLjlm2p5zu5n3X0/Q11SVN9xczxmzddJqZbzyyfJ5DUkV16d8ADN75ruwgRos9EiNvGtg5EzY353DXU5aQk6stMEMptU4ZUbg9moiwLUZb7D29bIGGv+oPobkHw3SQ997SDg/nfDfYbK+rLSrvjFk7Mci/ra+Leb/0/P/4v6MnRJDmq8Xb5O14SWU3bNcrJ6ZGxhwmy3vXil+cBYdZnbx+OTbB8LxL08Ke9kG+Avst3w2B5Viq6zLZBxwaUAuP4rmdpgEgHMBRAliLo1kEIox1KaXAdt1zjA5zdDS4vtVHPfm2GZgaM00o213q+ATWey8k7GHy1GusGLb+v1xjtW7CXhkhYh061VqtFTRGddlomubD+iR"
			 * ,"token_type":"bearer"
			 * ,"refresh_token":"BY1ASVZklHciII0p+XWT6YowjjzUJpA/diene31/CvU/+mKAI7lpvwXVvNsFPOasif/1jg0O78uz+xl60MJTk6ihy3cbdyawbHQirWxbEHyzWi5gryVjpmXVdvzUwRqo/LyUnIE2dXtiV3/lLjlm2p5zu5n3X0/Q11SVN9xczxmzddJqZbzyyfJ5DUkV16d8ADN75ruwgRos9EiNvGtg5EzY353DXU5aQk6stMEMptXw+PrdI/9uZ0dYcSjV7EFFD9bfxFzrF3zzQVPxODeC841zd8VnMVGAdPbab0Pwg0etuew6Fr9fuOKhvbGQzob2RdSkTJrQeKfwpSlGYltBNEvgNrWRJxe2/eQ2ZR9RycJqXqbjRwwXFTenKxB0PeDulr9FTvI1BByabwOZS8A3YtQ10nvGx+bVht4OOXAjM/Zv48K9cCa7+dwjK3m6NO6Jijj2xSBjZz9gX+vHlz4nCF37hzovifuZnJrxcDxwquRDXmR3vJgGx+u+TDWWKW1/c9F12GTYkPuO/nX0jjw6vJLoOHobgyH9bk9DcLS1piq+tg/tihqTDi8xHZ+yEkoKoa2s749AyT4XxxZHnhHNhg=="
			 * ,"expires_in":3599
			 * ,"scope":"base"
			 * ,"ip":"222.107.236.16"
			 * ,"push_uri":"http://localhost:1212/test"
			 * ,"jti":"bf5b7c2e-ef3e-4e49-a843-30051c4a772b"}
			 * 
			 */   
        	
        	/** RETURN SAMPLE
			/*
			 * { "access_token":"tokeeeeeeeennnnnnnnnnnnnnnnn",
			 * "token_type":"bearer",
			 * "refresh_token":"tokkkkkkeeeeeeeeeeeeeeeeeeennn",
			 * "expires_in":3599,
			 * "scope":"base", 
			 * "ip":"111.222.333.444",
			 * "push_uri":"http://localhost:1212/test",
			 * "jti":"567bba62-a123-4fc3-7c89-512ab362df4b" }
			 */
    		requestUrl.append("?");
			requestUrl.append("username="+username+"&");		
    		requestUrl.append("password="+password+"&");
    		requestUrl.append("grant_type=password&");
    		
    		URL url = new URL(requestUrl.toString());
    		
    		String targetText = clientid+":"+clientsecret;
    		byte[] targetBytes = targetText.getBytes();
    		String authorization = new String(base64Enc(targetBytes));
    		
    		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    		conn.setDoOutput(true);
    		conn.setRequestMethod("POST");
    		conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");        
    		conn.setRequestProperty("Authorization", "Basic " +authorization);
    		conn.setDoOutput(true);

    		OutputStream os = conn.getOutputStream();
    		os.flush();
    		os.close();
        	
    		StringBuffer responseJson = new StringBuffer();
    		BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
    		String inputLine;
    		while ((inputLine = in.readLine()) != null) {
    			responseJson.append(inputLine);
	        }
	        conn.disconnect();
    		in.close();
		    
    		ObjectMapper objectMapper = new ObjectMapper();
    		jsonNode = objectMapper.readTree(in);
	        
    		return jsonNode;    		
    		
		} catch (Exception e) {
			logger.debug(e.toString());
			// TODO: handle exception
		}
        return jsonNode;
        
	}
	
	//알람톡발송
	public String alarmTalkSendsNew(Map<String, Object> map, Map userSessionMap)throws Exception{
        logger.debug("//***** MessageServiceImple.alarmTalkSendsNew *****//");
        String rtMessage ="";
        String Msg = ""; //메시지
        String customerPhone = ""; // 수신번호
        String subject = ""; //제목
        try {
        	
        	String messageGubun = "";
        	
        	// 메시지 본문 내용
    	    Msg = map.get("message").toString();
    	    
    	    byte[] byteLength = Msg.getBytes();
    	    if(byteLength.length <= 90) {
    	    	messageGubun =  "/v1/messages/sms";
    	    	
    	    }else if(90 < byteLength.length &&  byteLength.length <= 2000) {
    	    	messageGubun =  "/v1/messages/lms";
    	    	subject = "";
    	    }else {
    	    	messageGubun =  "/v1/messages/mms";
    	    	subject = "";
    	    }
    	    
    	    StringBuilder requestUrl = new StringBuilder(histApiUrl+messageGubun);
    	    
    	    // 받을사람 전화번호
    		customerPhone = map.get("customer_phone").toString().replaceAll("-", "");
    		customerPhone = customerPhone.replaceAll(" ", "");
    		customerPhone = customerPhone.trim();
    		String[] customerPhoneArr = customerPhone.split(",");
    		
    		ArrayList<String> arrayList = new ArrayList<String>();
    		ArrayList<MessageVO> messageArrayList = new ArrayList<MessageVO>();
    		
    		// phone numbers
    		for(String data : customerPhoneArr){
    		    if(!arrayList.contains(data)){
    		        arrayList.add(data);
    		   }
    		}
    				
    		customerPhoneArr = arrayList.toArray(new String[arrayList.size()]);
    		customerPhone = "";
    		for(int k = 0; k < customerPhoneArr.length; k++) {
//    			customerPhone += customerPhoneArr[k]+";";
    			MessageVO m = new MessageVO();
    			m.setPhone(customerPhoneArr[k]);
    			messageArrayList.add(m);
    		}
    		String message = new Gson().toJson(messageArrayList);
    		requestUrl.append("?");
    		requestUrl.append(URLEncoder.encode("call_back","UTF-8") + "=" + URLEncoder.encode("1577-6268", "UTF-8") + "&");		//발신번호    		
    		requestUrl.append(URLEncoder.encode("text","UTF-8") + "=" + URLEncoder.encode(Msg, "UTF-8") + "&"); //메시지 sms:90byte , lms:2000byte , mms:2k
    		requestUrl.append(URLEncoder.encode("messages","UTF-8") + "=" + URLEncoder.encode(message, "UTF-8") + "&"); // id , phsone , merge_vals
    		requestUrl.append(URLEncoder.encode("subject","UTF-8") + "=" + URLEncoder.encode(subject, "UTF-8") + "&");
    		
    		URL url = new URL(requestUrl.toString());
    		
    		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    		conn.setDoOutput(true);
    		conn.setRequestMethod("POST");
    		conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");        
    		conn.setDoOutput(true);

    		OutputStream os = conn.getOutputStream();
    		os.flush();
    		os.close();

    		int responseCode = conn.getResponseCode();
    		logger.debug("\nSending 'POST' request to URL : " + url);
    		logger.debug("Response Code : " + responseCode);
    		        
    		BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
    		String inputLine;
    		StringBuffer response = new StringBuffer();

    		while ((inputLine = in.readLine()) != null) {
    			response.append(inputLine);
    		}
    		in.close();
    		logger.debug(response.toString());
            rtMessage = response.toString(); 
		} catch (Exception e) {
			e.printStackTrace();			
		}finally {
            AlarmTalkVO alarmTalkVO = new AlarmTalkVO();
	        String alarmTalkId = "ATI"+UUID.randomUUID().toString().replaceAll("-","");
	        alarmTalkVO.setAlarmTalkId(alarmTalkId);
	        alarmTalkVO.setAllocationId(map.get("allocation_id").toString());
	        alarmTalkVO.setCustomerId(map.get("customer_id").toString());
	        alarmTalkVO.setMsg(Msg);
	        alarmTalkVO.setRcvphns(customerPhone);
	        alarmTalkVO.setTemplate("");
	        alarmTalkVO.setType("12"); //WEB 메시지 알림톡 수동발송 
	        alarmTalkVO.setReturnMsg(rtMessage); 
	        
	        alarmTalkMapper.insertAlarmTalk(alarmTalkVO);	
		}
		
       
        return rtMessage;
}

	
	
	//문자발송
		public String lmsSendMessageNew(Map<String, Object> map , Map userSessionMap)throws Exception{
	        
	        logger.debug("//***** MessageServiceImple.lmsSendMessageNew *****//");
	        
	        String rtMessage ="";
	        String Msg = ""; //메시지
	        String customerPhone = ""; // 수신번호
	        String subject = ""; //제목
	        String access_token = ""; // access token
	        try {
	        	JsonNode jsonNode = getAuthToken(); // 토큰 생성
	        	
	        	access_token = jsonNode.get("access_token").asText();
	        	String messageGubun = "";
	        	
	        	// 메시지 본문 내용
	    	    Msg = map.get("message").toString();
	    	    
	    	    byte[] byteLength = Msg.getBytes();
	    	    if(byteLength.length <= 90) {
	    	    	messageGubun =  "/v1/messages/sms";
	    	    	
	    	    }else if(90 < byteLength.length &&  byteLength.length <= 2000) {
	    	    	messageGubun =  "/v1/messages/lms";
	    	    	subject = "";
	    	    }else {
	    	    	messageGubun =  "/v1/messages/mms";
	    	    	subject = "";
	    	    }
	    	    
	    	    StringBuilder requestUrl = new StringBuilder(histApiUrl+messageGubun);
	    	    
	    	    // 받을사람 전화번호
	    		customerPhone = map.get("customer_phone").toString().replaceAll("-", "");
	    		customerPhone = customerPhone.replaceAll(" ", "");
	    		customerPhone = customerPhone.trim();
	    		String[] customerPhoneArr = customerPhone.split(",");
	    		
	    		ArrayList<String> arrayList = new ArrayList<String>();
	    		ArrayList<MessageVO> messageArrayList = new ArrayList<MessageVO>();
	    		
	    		// phone numbers
	    		for(String data : customerPhoneArr){
	    		    if(!arrayList.contains(data)){
	    		        arrayList.add(data);
	    		   }
	    		}
	    				
	    		customerPhoneArr = arrayList.toArray(new String[arrayList.size()]);
	    		customerPhone = "";
	    		for(int k = 0; k < customerPhoneArr.length; k++) {
//	    			customerPhone += customerPhoneArr[k]+";";
	    			MessageVO m = new MessageVO();
	    			m.setId("");
	    			m.setPhone(customerPhoneArr[k]);
	    			m.setMerge_vals("");
	    			messageArrayList.add(m);
	    		}
	    		String message = new Gson().toJson(messageArrayList); //list json string으로 변환
	    		
	    		requestUrl.append("?");
	    		requestUrl.append(URLEncoder.encode("call_back","UTF-8") + "=" + URLEncoder.encode("1577-6268", "UTF-8") + "&");		//발신번호    		
	    		requestUrl.append(URLEncoder.encode("text","UTF-8") + "=" + URLEncoder.encode(Msg, "UTF-8") + "&"); //메시지 sms:90byte , lms:2000byte , mms:2k
	    		requestUrl.append(URLEncoder.encode("messages","UTF-8") + "=" + URLEncoder.encode(message, "UTF-8") + "&"); // List <id , phsone , merge_vals>
	    		requestUrl.append(URLEncoder.encode("subject","UTF-8") + "=" + URLEncoder.encode(subject, "UTF-8") + "&");
	    		
	    		URL url = new URL(requestUrl.toString());
	    		
	    		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    		conn.setDoOutput(true);
	    		conn.setRequestMethod("POST");
	    		conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");        
	    		conn.setRequestProperty("Authorization", "Bearer "+access_token);
	    		conn.setDoOutput(true);

	    		OutputStream os = conn.getOutputStream();
	    		os.flush();
	    		os.close();

	    		int responseCode = conn.getResponseCode();
	    		logger.debug("\nSending 'POST' request to URL : " + url);
	    		logger.debug("Response Code : " + responseCode);
	    		        
	    		BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	    		String inputLine;

	    		conn.disconnect();
	    		in.close();
			    
	    		ObjectMapper objectMapper = new ObjectMapper();
	    		jsonNode = objectMapper.readTree(in);

	    		
	    		rtMessage = jsonNode.get("code").asText()+":"+jsonNode.get("message").asText();
	    		
	    		logger.debug(rtMessage);
				
			} catch (Exception e) {
				logger.debug(e.toString());
				
			}finally {
				String sendSmsId = "SMS"+UUID.randomUUID().toString().replaceAll("-","");
				SendSmsVO sendSmsVO = new SendSmsVO();
				sendSmsVO.setCallback("15775268");
				sendSmsVO.setCustomerId(userSessionMap.get("emp_id").toString());
				sendSmsVO.setMsg(Msg);
				sendSmsVO.setRcvphns(customerPhone);
				sendSmsVO.setSendSmsId(sendSmsId);
				sendSmsVO.setType("11"); // WEB 메시지 SMS 수동 발송
				sendSmsVO.setAllocationId("수동발송");
				sendSmsVO.setReturnMsg(rtMessage);
				
				sendSmsMapper.insertSendSms(sendSmsVO);
			}
			

				return rtMessage;
			
		}
	
	
	
}
