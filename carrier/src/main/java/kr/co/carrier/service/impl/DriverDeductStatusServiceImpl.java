package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.DriverDeductStatusMapper;
import kr.co.carrier.service.DriverDeductStatusService;
import kr.co.carrier.vo.DriverDeductStatusVO;

@Service("driverDeductStatusService")
public class DriverDeductStatusServiceImpl implements DriverDeductStatusService{

	
	
	
	@Resource(name="driverDeductStatusMapper")
	private DriverDeductStatusMapper driverDeductStatusMapper;
	
	
	
	public Map<String, Object> selectDriverDeductStatus(Map<String, Object> map) throws Exception{
		return driverDeductStatusMapper.selectDriverDeductStatus(map);
	}
	
	public List<Map<String, Object>> selectDriverDeductStatusList(Map<String, Object> map) throws Exception{
		return driverDeductStatusMapper.selectDriverDeductStatusList(map);
	}
	
	public int selectDriverDeductStatusListCount(Map<String, Object> map) throws Exception{
		return driverDeductStatusMapper.selectDriverDeductStatusListCount(map);
	}
	
	public int insertDriverDeductStatus(DriverDeductStatusVO driverDeductStatusVO) throws Exception{
		return driverDeductStatusMapper.insertDriverDeductStatus(driverDeductStatusVO);
	}
	
	public void deleteDriverDeductStatus(Map<String, Object> map) throws Exception{
		
	}
	
	
	
	
	
	
	
	
	
	
	
}
