package kr.co.carrier.service;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.DecideFinalVO;

public interface DecideFinalService {

	
	
	
	public Map<String, Object> selectDecideFinal(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectDecideFinalList(Map<String, Object> map) throws Exception;
	public int selectDecideFinalListCount(Map<String, Object> map) throws Exception;
	public int insertDecideFinal(DecideFinalVO decideFinalVO) throws Exception;
	public void deleteDecideFinal(Map<String, Object> map) throws Exception;
	public void updateDecideFinalStatus(Map<String, Object> map) throws Exception;
	
	
	
	
	
	
	
	
	
	
	
	
}
