package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.AllocationStatusMapper;
import kr.co.carrier.service.AllocationStatusService;

@Service("allocationStatusService")
public class AllocationStatusServiceImpl implements AllocationStatusService{

		
	@Resource(name="allocationStatusMapper")
	private AllocationStatusMapper allocationStatusMapper;
	
	
	public Map<String, Object> selectAllocationStatus(Map<String, Object> map) throws Exception{
		return allocationStatusMapper.selectAllocationStatus(map);
	}
	
	public List<Map<String, Object>> selectAllocationStatusList(Map<String, Object> map) throws Exception{
		return allocationStatusMapper.selectAllocationStatusList(map);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
