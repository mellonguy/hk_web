package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.CustomerAddressInfoMapper;
import kr.co.carrier.service.CustomerAddressInfoService;
import kr.co.carrier.vo.CustomerAddressInfoVO;

@Service("customerAddressInfoService")
public class CustomerAddressInfoServiceImpl implements CustomerAddressInfoService {

	@Resource(name="customerAddressInfoMapper")
	private CustomerAddressInfoMapper customerAddressInfoMapper; 
	
	
	public List<Map<String,Object>> selectKeywordAddressInfoList (Map<String, Object> map) throws Exception{
		return customerAddressInfoMapper.selectKeywordAddressInfoList(map);
	}

	public int insertCustomerAddressInfoInsert(CustomerAddressInfoVO customerAddressInfoVO)throws Exception{
		return customerAddressInfoMapper.insertCustomerAddressInfoInsert(customerAddressInfoVO);
	}
	public void deleteCustomerAddressInfo(Map<String,Object> map) throws Exception{
		customerAddressInfoMapper.deleteCustomerAddressInfo(map);
	}
}
