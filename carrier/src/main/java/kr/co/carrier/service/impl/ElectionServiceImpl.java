package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.ElectionMapper;
import kr.co.carrier.service.ElectionService;
import kr.co.carrier.vo.ElectionVO;

@Service("electionService")
public class ElectionServiceImpl implements ElectionService{

	
	@Resource(name="electionMapper")
	private ElectionMapper electionMapper;
	
	public Map<String, Object> selectElection(Map<String, Object> map) throws Exception{
		return electionMapper.selectElection(map);
	}
	
	public List<Map<String, Object>> selectElectionList(Map<String, Object> map) throws Exception{
		return electionMapper.selectElectionList(map);
	}
	
	public int selectElectionListCount(Map<String, Object> map) throws Exception{
		return electionMapper.selectElectionListCount(map);
	}
	
	public int insertElection(ElectionVO electionVO) throws Exception{
		return electionMapper.insertElection(electionVO);
	}
	
	public void deleteElection(Map<String, Object> map) throws Exception{
		electionMapper.deleteElection(map);
	}
	
	public void updateElectionJoinCount(Map<String, Object> map) throws Exception{
		electionMapper.updateElectionJoinCount(map);
	}
	
}
