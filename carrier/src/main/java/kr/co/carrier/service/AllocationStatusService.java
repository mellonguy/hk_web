package kr.co.carrier.service;

import java.util.List;
import java.util.Map;

public interface AllocationStatusService {

	
	
	public Map<String, Object> selectAllocationStatus(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectAllocationStatusList(Map<String, Object> map) throws Exception;
	
	
	
	
	
	
	
	
}
