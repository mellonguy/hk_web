package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.BillPublishRequestMapper;
import kr.co.carrier.service.BillPublishRequestService;
import kr.co.carrier.vo.BillPublishRequestVO;




@Service("billPublishRequestService")
public class BillPublishRequestServiceImpl implements BillPublishRequestService{

	
	
	@Resource(name="billPublishRequestMapper")
	private BillPublishRequestMapper billPublishRequestMapper;
	
	public Map<String, Object> selectBillPublishRequest(Map<String, Object> map) throws Exception{
		return billPublishRequestMapper.selectBillPublishRequest(map);
	}
	
	public List<Map<String, Object>> selectBillPublishRequestList(Map<String, Object> map) throws Exception{
		return billPublishRequestMapper.selectBillPublishRequestList(map);
	}
	
	public int selectBillPublishRequestListCount(Map<String, Object> map) throws Exception{
		return billPublishRequestMapper.selectBillPublishRequestListCount(map);
	}
	
	public int insertBillPublishRequest(BillPublishRequestVO billPublishRequestVO) throws Exception{
		return billPublishRequestMapper.insertBillPublishRequest(billPublishRequestVO);
	}
	
	public void deleteBillPublishRequest(Map<String, Object> map) throws Exception{
		billPublishRequestMapper.deleteBillPublishRequest(map);
	}
	
	public void updateBillPublishRequest(Map<String, Object> map) throws Exception{
		billPublishRequestMapper.updateBillPublishRequest(map);
	}
	
	public Map<String, Object> selectBillPublishRequestStatistics(Map<String, Object> map) throws Exception{
		return billPublishRequestMapper.selectBillPublishRequestStatistics(map);
	}

	public int selectCustomerListCount(Map<String, Object> map) throws Exception{
		return billPublishRequestMapper.selectCustomerListCount(map);
	}
	
	public List<Map<String, Object>> selectCustomerList(Map<String, Object> map) throws Exception{
		return billPublishRequestMapper.selectCustomerList(map);
	}
	
	public List<Map<String, Object>> selectBillPublishRequestListByCustomerId(Map<String, Object> map) throws Exception{
		return billPublishRequestMapper.selectBillPublishRequestListByCustomerId(map);
	}
	
	public List<Map<String, Object>> selectBillPublishRequestIssueList(Map<String, Object> map) throws Exception{
		return billPublishRequestMapper.selectBillPublishRequestIssueList(map);
	}
	public int selectBillPublishRequestIssueListCount(Map<String, Object> map) throws Exception{
		return billPublishRequestMapper.selectBillPublishRequestIssueListCount(map);
	}
	
	
	
	
}
