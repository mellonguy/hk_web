package kr.co.carrier.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.DriverBillingFileMapper;
import kr.co.carrier.service.DriverBillingFileService;
import kr.co.carrier.service.DriverService;
import kr.co.carrier.service.FileUploadService;
import kr.co.carrier.utils.WebUtils;
import kr.co.carrier.vo.DriverBillingFileVO;
import kr.co.carrier.vo.DriverDeductFileVO;

@Service("driverBillingFileService")
public class DriverBillingFileServiceImpl implements DriverBillingFileService{

	
	@Resource(name="driverBillingFileMapper")
	private DriverBillingFileMapper driverBillingFileMapper;
	
	
	@Autowired
    private DriverService driverService;
	
	@Autowired
	private FileUploadService fileUploadService;
	
	@Value("#{appProp['upload.base.path']}")
    private String rootDir;    
    private String subDir;
	
	
	public Map<String, Object> selectDriverBillingFile(Map<String, Object> map) throws Exception{
		return driverBillingFileMapper.selectDriverBillingFile(map);
	}
	
	public List<Map<String, Object>> selectDriverBillingFileList(Map<String, Object> map) throws Exception{
		return driverBillingFileMapper.selectDriverBillingFileList(map);
	}
	
	public int selectDriverBillingFileListCount(Map<String, Object> map) throws Exception{
		return driverBillingFileMapper.selectDriverBillingFileListCount(map);
	}
	
	public int insertDriverBillingFile(DriverBillingFileVO driverBillingFileVO) throws Exception{
		return driverBillingFileMapper.insertDriverBillingFile(driverBillingFileVO);
	}
	
	public void deleteDriverBillingFile(Map<String, Object> map) throws Exception{
		driverBillingFileMapper.deleteDriverBillingFile(map);
	}
	
	
	public Map<String, Object> makeDriverBillingFile(Map<String, Object> paramMap) throws Exception{
		
		
		Map<String, Object> fileMap = new HashMap<String, Object>();
		
		try {
		
			 	File file = new File(rootDir+"/forms", "billing_provider.xlsx");
		        XSSFWorkbook wb = null;
		        
		    	XSSFRow row;
				XSSFCell cell;
				try {//엑셀 파일 오픈
					wb = new XSSFWorkbook(new FileInputStream(file));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
		    	
				XSSFSheet sheet = wb.getSheetAt(0);         
		        
				CellStyle style = wb.createCellStyle();
				style.setFillForegroundColor(IndexedColors.PALE_BLUE.getIndex());
		        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		        style.setAlignment(CellStyle.ALIGN_CENTER);
		        style.setBorderBottom(CellStyle.BORDER_DOUBLE);
		        style.setBorderTop(CellStyle.BORDER_DOUBLE);
		        style.setBorderLeft(CellStyle.BORDER_DOUBLE);
		        style.setBorderRight(CellStyle.BORDER_DOUBLE);
				
		        int rows = sheet.getLastRowNum();
				int cells = sheet.getRow(0).getLastCellNum();
				
				
				FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
				
				Map<String, Object> map = new HashMap<String, Object>();
		    	map.put("driverId", paramMap.get("driverId").toString());
		    	map.put("forUpdatePassWord", "");
		    	Map<String, Object> driver = driverService.selectDriver(map);
				
				if(driver != null && driver.get("driver_id") != null) {
					
					 for (int r = 2; r <= rows; r++) {
						 
						 FormulaEvaluator formulaEval = wb.getCreationHelper().createFormulaEvaluator();
    					 
 			        	row = sheet.getRow(r); // row 가져오기
 			        	if (row != null) {
 			        		for (int c = 0; c < cells; c++) {
 			        			cell = row.getCell(c);
 			        			if (cell != null) { 
 			        				String value = "";

 									switch (cell.getCellType()) {
 									   	case XSSFCell.CELL_TYPE_FORMULA:
 									   		
 									   		CellValue evaluate = formulaEval.evaluate(cell);
 									   	  if( evaluate != null ) {
 									   		  
 									   		  try {
 									   			  
 									   			  if(WebUtils.isNumber(evaluate.formatAsString())) {
 									   				  
 									   				value=  String.valueOf(new Double(evaluate.getNumberValue()).intValue());
 									   				   
 									   			  }else {
 									   				  
 									   				  value = evaluate.formatAsString();
 									   			  }
 									   			  
 									   		  }catch(Exception e) {
 									   			  e.printStackTrace();
 									   			  
 									   		  }
 									   		  
 									   		  
	    			        				   }else {
	    			        					   
	    			        					   value = "";
	    			        				   }
 									   		
 									   		/*
 									   		if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_NUMERIC){
 									   			value = "" + (int)cell.getNumericCellValue();
 									   		}else if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_STRING){
 									   			value = "" + cell.getStringCellValue(); 
 									   		}
 									   		value = cell.getCellFormula();
 									   		*/
 									   		
 									   		break;
 									   	case XSSFCell.CELL_TYPE_NUMERIC:
										   		value = "" + (int)cell.getNumericCellValue();
										   		break;
										   	case XSSFCell.CELL_TYPE_STRING:
										   		value = "" + cell.getStringCellValue();
										   		break;
										   	case XSSFCell.CELL_TYPE_BLANK:
										   		value = "";
										   		break;
										   	case XSSFCell.CELL_TYPE_ERROR:
										   		value = "" + cell.getErrorCellValue();
										   		break;
										   	default:
										}
										
										if(value != null && !value.equals("")) {
											value = value.trim();
										}
										int nIndex = cell.getColumnIndex();
									
									if(r==2) {
										//등록번호
										if(nIndex == 3){
											cell = row.getCell(3);
					        				cell.setCellValue(driver.get("business_license_number") != null ? driver.get("business_license_number").toString():"");
										}
										
									}else if(r==3) {
										if(nIndex == 3){
											cell = row.getCell(3);
											cell.setCellValue(driver.get("business_name") != null ? driver.get("business_name").toString():"");
										}
										if(nIndex == 9){
											cell = row.getCell(9);
											cell.setCellValue(driver.get("driver_name") != null ? driver.get("driver_name").toString():"");
										}
									}else if(r==4) {
										if(nIndex == 3){
											cell = row.getCell(3);
					        				cell.setCellValue(driver.get("address") != null ? driver.get("address").toString():""+" "+driver.get("address_detail") != null ? driver.get("address_detail").toString():"");
										}
									}else if(r==5) {	
										if(nIndex == 3){
											cell = row.getCell(3);
					        				cell.setCellValue(driver.get("business_condition") != null ? driver.get("business_condition").toString():"");
										}
										if(nIndex == 9){
											cell = row.getCell(9);
					        				cell.setCellValue(driver.get("business_kind") != null ? driver.get("business_kind").toString():"");
										}
									}else if(r==8) {
										boolean startFlag = false;
										String decideMonth = paramMap.get("decideMonth").toString();
										
										if(nIndex == 0){
											cell = row.getCell(0);
					        				cell.setCellValue(decideMonth.split("-")[0].substring(2));
										}else if(nIndex == 1){
											cell = row.getCell(1);
					        				cell.setCellValue(decideMonth.split("-")[1]);
										}else if(nIndex == 2){
											cell = row.getCell(2);
					        				cell.setCellValue(WebUtils.getLastDate("yyyy-MM", Integer.parseInt(decideMonth.split("-")[0]), Integer.parseInt(decideMonth.split("-")[1])));
										}else if(nIndex == 3){
											char[] supplyVal = paramMap.get("supplyVal").toString().toCharArray(); 
											int empty = 10-supplyVal.length;
											cell = row.getCell(3);
					        				cell.setCellValue(String.valueOf(empty));
										}else if(nIndex == 4){
											char[] supplyVal = paramMap.get("supplyVal").toString().toCharArray(); 
											int len = supplyVal.length;
											len=len-1;
											for(int i = 13; i > 3; i--) {
												if(len >= 0) {
													if(len >= 0) {
														cell = row.getCell(i);
														cell.setCellValue(Character.toString(supplyVal[len]));
													}
													len=len-1;	
												}else {
													break;
												}
											}
											c = 13;	
										}else if(nIndex == 14){
											char[] vatVal = paramMap.get("vatVal").toString().toCharArray(); 
											int len = vatVal.length;
											len=len-1;
											for(int i = 22; i > 13; i--) {
												if(len >= 0) {
													if(len >= 0) {
														cell = row.getCell(i);
														cell.setCellValue(Character.toString(vatVal[len]));
													}
													len=len-1;
												}else {
													break;
												}
											}
											c = 23;	
										}
										
									}else if(r==10) {	
										if(nIndex == 2){
											cell = row.getCell(2);
					        				cell.setCellValue(paramMap.get("decideMonth").toString().split("-")[1]+"월 운송료");
										}else if(nIndex == 18){
											cell = row.getCell(18);
					        				cell.setCellValue(Integer.parseInt(paramMap.get("supplyVal").toString()));
										}else if(nIndex == 23){
											cell = row.getCell(23);
					        				cell.setCellValue(Integer.parseInt(paramMap.get("vatVal").toString()));
										}
									}
										
				        			} else {
				        				//System.out.print("[null]\t");
				        			}
				        		} // for(c) 문
				        		

				        	}
				        	
				        }
					
					
				}
		    	
				 
					XSSFFormulaEvaluator.evaluateAllFormulaCells(wb);
					DriverBillingFileVO driverBillingFileVO = new DriverBillingFileVO();
					fileMap = fileUploadService.upload(rootDir, "billing_list","("+paramMap.get("decideMonth").toString().split("-")[1]+"월 공급자보관용)"+driver.get("driver_name").toString()+".xlsx", wb);
					driverBillingFileVO.setCategoryType("billing_list");
					driverBillingFileVO.setDecideMonth(paramMap.get("decideMonth").toString());
					driverBillingFileVO.setDriverBillingFileId("FLE"+UUID.randomUUID().toString().replaceAll("-", ""));
					driverBillingFileVO.setDriverBillingFileNm(fileMap.get("ORG_NAME").toString());
					driverBillingFileVO.setDriverBillingFilePath(fileMap.get("SAVE_NAME").toString());
					driverBillingFileVO.setDriverId(paramMap.get("driverId").toString());
					driverBillingFileVO.setBillingDivision("P");
					this.insertDriverBillingFile(driverBillingFileVO);
			
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return fileMap;
		
	}
	
	
public Map<String, Object> makeDriverBillingFileOtherSide(Map<String, Object> paramMap) throws Exception{
		
		
		Map<String, Object> fileMap = new HashMap<String, Object>();
		
		try {
		
			 	File file = new File(rootDir+"/forms", "billing_otherside.xlsx");
		        XSSFWorkbook wb = null;
		        
		    	XSSFRow row;
				XSSFCell cell;
				try {//엑셀 파일 오픈
					wb = new XSSFWorkbook(new FileInputStream(file));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
		    	
				XSSFSheet sheet = wb.getSheetAt(0);         
		        
				CellStyle style = wb.createCellStyle();
				style.setFillForegroundColor(IndexedColors.PALE_BLUE.getIndex());
		        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		        style.setAlignment(CellStyle.ALIGN_CENTER);
		        style.setBorderBottom(CellStyle.BORDER_DOUBLE);
		        style.setBorderTop(CellStyle.BORDER_DOUBLE);
		        style.setBorderLeft(CellStyle.BORDER_DOUBLE);
		        style.setBorderRight(CellStyle.BORDER_DOUBLE);
				
		        int rows = sheet.getLastRowNum();
				int cells = sheet.getRow(0).getLastCellNum();
				
				
				FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
				
				Map<String, Object> map = new HashMap<String, Object>();
		    	map.put("driverId", paramMap.get("driverId").toString());
		    	map.put("forUpdatePassWord", "");
		    	Map<String, Object> driver = driverService.selectDriver(map);
				
				if(driver != null && driver.get("driver_id") != null) {
					
					 for (int r = 2; r <= rows; r++) {
						 
						 FormulaEvaluator formulaEval = wb.getCreationHelper().createFormulaEvaluator();
    					 
 			        	row = sheet.getRow(r); // row 가져오기
 			        	if (row != null) {
 			        		for (int c = 0; c < cells; c++) {
 			        			cell = row.getCell(c);
 			        			if (cell != null) { 
 			        				String value = "";

 									switch (cell.getCellType()) {
 									   	case XSSFCell.CELL_TYPE_FORMULA:
 									   		
 									   		CellValue evaluate = formulaEval.evaluate(cell);
 									   	  if( evaluate != null ) {
 									   		  
 									   		  try {
 									   			  
 									   			  if(WebUtils.isNumber(evaluate.formatAsString())) {
 									   				  
 									   				value=  String.valueOf(new Double(evaluate.getNumberValue()).intValue());
 									   				   
 									   			  }else {
 									   				  
 									   				  value = evaluate.formatAsString();
 									   			  }
 									   			  
 									   		  }catch(Exception e) {
 									   			  e.printStackTrace();
 									   			  
 									   		  }
 									   		  
 									   		  
	    			        				   }else {
	    			        					   
	    			        					   value = "";
	    			        				   }
 									   		
 									   		/*
 									   		if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_NUMERIC){
 									   			value = "" + (int)cell.getNumericCellValue();
 									   		}else if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_STRING){
 									   			value = "" + cell.getStringCellValue(); 
 									   		}
 									   		value = cell.getCellFormula();
 									   		*/
 									   		
 									   		break;
 									   	case XSSFCell.CELL_TYPE_NUMERIC:
										   		value = "" + (int)cell.getNumericCellValue();
										   		break;
										   	case XSSFCell.CELL_TYPE_STRING:
										   		value = "" + cell.getStringCellValue();
										   		break;
										   	case XSSFCell.CELL_TYPE_BLANK:
										   		value = "";
										   		break;
										   	case XSSFCell.CELL_TYPE_ERROR:
										   		value = "" + cell.getErrorCellValue();
										   		break;
										   	default:
										}
										
										if(value != null && !value.equals("")) {
											value = value.trim();
										}
										int nIndex = cell.getColumnIndex();
									
									if(r==2) {
										//등록번호
										if(nIndex == 3){
											cell = row.getCell(3);
					        				cell.setCellValue(driver.get("business_license_number") != null ? driver.get("business_license_number").toString():"");
										}
										
									}else if(r==3) {
										if(nIndex == 3){
											cell = row.getCell(3);
											cell.setCellValue(driver.get("business_name") != null ? driver.get("business_name").toString():"");
										}
										if(nIndex == 9){
											cell = row.getCell(9);
											cell.setCellValue(driver.get("driver_name") != null ? driver.get("driver_name").toString():"");
										}
									}else if(r==4) {
										if(nIndex == 3){
											cell = row.getCell(3);
					        				cell.setCellValue(driver.get("address") != null ? driver.get("address").toString():""+" "+driver.get("address_detail") != null ? driver.get("address_detail").toString():"");
										}
									}else if(r==5) {	
										if(nIndex == 3){
											cell = row.getCell(3);
					        				cell.setCellValue(driver.get("business_condition") != null ? driver.get("business_condition").toString():"");
										}
										if(nIndex == 9){
											cell = row.getCell(9);
					        				cell.setCellValue(driver.get("business_kind") != null ? driver.get("business_kind").toString():"");
										}
									}else if(r==8) {
										boolean startFlag = false;
										String decideMonth = paramMap.get("decideMonth").toString();
										
										if(nIndex == 0){
											cell = row.getCell(0);
					        				cell.setCellValue(decideMonth.split("-")[0].substring(2));
										}else if(nIndex == 1){
											cell = row.getCell(1);
					        				cell.setCellValue(decideMonth.split("-")[1]);
										}else if(nIndex == 2){
											cell = row.getCell(2);
					        				cell.setCellValue(WebUtils.getLastDate("yyyy-MM", Integer.parseInt(decideMonth.split("-")[0]), Integer.parseInt(decideMonth.split("-")[1])));
										}else if(nIndex == 3){
											char[] supplyVal = paramMap.get("supplyVal").toString().toCharArray(); 
											int empty = 10-supplyVal.length;
											cell = row.getCell(3);
					        				cell.setCellValue(String.valueOf(empty));
										}else if(nIndex == 4){
											char[] supplyVal = paramMap.get("supplyVal").toString().toCharArray(); 
											int len = supplyVal.length;
											len=len-1;
											for(int i = 13; i > 3; i--) {
												if(len >= 0) {
													if(len >= 0) {
														cell = row.getCell(i);
														cell.setCellValue(Character.toString(supplyVal[len]));
													}
													len=len-1;	
												}else {
													break;
												}
											}
											c = 13;	
										}else if(nIndex == 14){
											char[] vatVal = paramMap.get("vatVal").toString().toCharArray(); 
											int len = vatVal.length;
											len=len-1;
											for(int i = 22; i > 13; i--) {
												if(len >= 0) {
													if(len >= 0) {
														cell = row.getCell(i);
														cell.setCellValue(Character.toString(vatVal[len]));
													}
													len=len-1;
												}else {
													break;
												}
											}
											c = 23;	
										}
										
									}else if(r==10) {	
										if(nIndex == 2){
											cell = row.getCell(2);
					        				cell.setCellValue(paramMap.get("decideMonth").toString().split("-")[1]+"월 운송료");
										}else if(nIndex == 18){
											cell = row.getCell(18);
					        				cell.setCellValue(Integer.parseInt(paramMap.get("supplyVal").toString()));
										}else if(nIndex == 23){
											cell = row.getCell(23);
					        				cell.setCellValue(Integer.parseInt(paramMap.get("vatVal").toString()));
										}
									}
										
				        			} else {
				        				//System.out.print("[null]\t");
				        			}
				        		} // for(c) 문
				        		

				        	}
				        	
				        }
					
					
				}
		    	
				 
					XSSFFormulaEvaluator.evaluateAllFormulaCells(wb);
					DriverBillingFileVO driverBillingFileVO = new DriverBillingFileVO();
					fileMap = fileUploadService.upload(rootDir, "billing_list","("+paramMap.get("decideMonth").toString().split("-")[1]+"월 공급받는자보관용)"+driver.get("driver_name").toString()+".xlsx", wb);
					driverBillingFileVO.setCategoryType("billing_list");
					driverBillingFileVO.setDecideMonth(paramMap.get("decideMonth").toString());
					driverBillingFileVO.setDriverBillingFileId("FLE"+UUID.randomUUID().toString().replaceAll("-", ""));
					driverBillingFileVO.setDriverBillingFileNm(fileMap.get("ORG_NAME").toString());
					driverBillingFileVO.setDriverBillingFilePath(fileMap.get("SAVE_NAME").toString());
					driverBillingFileVO.setDriverId(paramMap.get("driverId").toString());
					driverBillingFileVO.setBillingDivision("O");
					
					this.insertDriverBillingFile(driverBillingFileVO);
			
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return fileMap;
		
	}
	
public void deleteDriverBillingFileByDriverIdAndDecideMonth(Map<String, Object> map) throws Exception{
	
	driverBillingFileMapper.deleteDriverBillingFileByDriverIdAndDecideMonth(map);
	
}
	
	
	
	
	
}
