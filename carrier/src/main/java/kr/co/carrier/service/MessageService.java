package kr.co.carrier.service;

import java.util.Map;

public interface MessageService {

	
	public String alarmTalkSends(Map<String, Object> map, Map userSessionMap)throws Exception;
	public String lmsSendMessage(Map<String, Object> map, Map userSessionMap)throws Exception;
	
	public String alarmTalkSendsNew(Map<String, Object> map, Map userSessionMap)throws Exception;
	public String lmsSendMessageNew(Map<String, Object> map, Map userSessionMap)throws Exception;
	
	
}
