package kr.co.carrier.service;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.CarInfoVO;
import kr.co.carrier.vo.PaymentInfoVO;
import kr.co.carrier.vo.WithdrawVO;

public interface PaymentInfoService {

	
	
	public List<Map<String, Object>> selectPaymentInfoList(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectPaymentInfoListForPayment(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectPaymentInfoByPaymentDivision(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectPaymentInfoListByPaymentDivision(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectPaymentInfoListForChange(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectDaeByeonForInsertList(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectAnotherCalInfo(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectPaymentInfoListByDebtorCreditorId(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectAnotherCalDDInfo(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectPaymentInfoByDecideFinalId(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectPaymentInfoRunTotal(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectPaymentInfoAllRunTotal(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectPaymentInfoByPaymentParnerId(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectPaymentInfoForMod(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectPaymentInfoForCollectManage(Map<String, Object> map) throws Exception;
	public int selectPurchaseListCount(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectPurchaseList(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectPurchaseListByPaymentPartnerId(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectWithdrawRunTotal(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectWithdrawAllRunTotal(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectPaymentInfoForAllNextWithdraw(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectPaymentInfoListForWithdraw(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectPaymentInfoForWithdraw(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectPaymentInfoWithdrawRunTotal(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectPaymentInfoDecideStatusByPaymentPartnerId(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectPaymentInfoForPartNextWithdraw(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectMrKIMDriverDeductList(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectPaymentInfoForNextPayment(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectPaymentInfoForAllNextPayment(Map<String, Object> map) throws Exception;
		
	public int insertPaymentInfo(PaymentInfoVO paymentInfoVO) throws Exception;
	public int insertPaymentInfoLogByVO(PaymentInfoVO paymentInfoVO) throws Exception;
	public int insertPaymentInfoLogByMap(Map<String, Object> map) throws Exception;
	public void insertWithdraw(WithdrawVO withdrawVO) throws Exception;	
	
	public void updatePaymentInfo(Map<String, Object> map) throws Exception;
	public void updatePaymentInfoDriverAmount(Map<String, Object> map) throws Exception;
	public void updatePaymentInfoDriverInfo(Map<String, Object> map) throws Exception;
	public void updatePaymentInfoAmount(Map<String, Object> map) throws Exception;
	public void updatePaymentInfoListBySearchData(Map<String, Object> map) throws Exception;
	public void updatePaymentInfoByIdx(Map<String, Object> map) throws Exception;
	public void updatePaymentInfoForAnother(Map<String, Object> map) throws Exception;
	public void updateSelectedStatusByIdx(Map<String, Object> map) throws Exception;
	public void updatePaymentInfoBySelectedStatus(Map<String, Object> map) throws Exception;
	public void updatePaymentInfoBillingDt(Map<String, Object> map) throws Exception;
	public void updatePaymentInfoListDecideFinalBySearchData(Map<String, Object> map) throws Exception;
	public void updatePaymentInfoInit(Map<String, Object> map) throws Exception;
	public void updatePaymentInfoChange(Map<String, Object> map) throws Exception;
	public void updateDebtorCreditorIdByIdx(Map<String, Object> map) throws Exception;
	public void updateDebtorCreditorIdByIdxForDaeByeon(Map<String, Object> map) throws Exception;
	public void updatePaymentInfoListForDecideFinal(Map<String, Object> map) throws Exception;
	public void updatePaymentInfoForAllDeposit(Map<String, Object> map) throws Exception;
	//public List<Map<String, Object>> selectPaymentInfoRunTotalList(Map<String, Object> map) throws Exception;
	public void updatePaymentInfoForPartDeposit(Map<String, Object> map) throws Exception;
	public void updatePaymentInfoForAllPartDeposit(Map<String, Object> map) throws Exception;
	public void updatePaymentInfoForPayment(Map<String, Object> map) throws Exception;
	public void updatePaymentInfoByPaymentParnerIdForDriverAmount(Map<String, Object> map) throws Exception;
	public void updateAllocationHcYn(Map<String, Object> map) throws Exception;
	public void updatePaymentInfoByPaymentParnerIdForSelfDriverAmount(Map<String, Object> map) throws Exception;
	public void updatePaymentInfoForAllPartWithdraw(Map<String, Object> map) throws Exception;
	public void updatePaymentInfoForWithdraw(Map<String, Object> map) throws Exception;
	public void updatePaymentInfoForAllWithdraw(Map<String, Object> map) throws Exception;
	public void updatePaymentInfoForPartWithdraw(Map<String, Object> map) throws Exception;
	public void updatePaymentInfoForDDFinish(Map<String, Object> map) throws Exception;
	public void updatePaymentInfoDebtorCreditorId(Map<String, Object> map) throws Exception;
	public void updatePaymentInfoDecideStatusByPaymentPartnerId(Map<String, Object> map) throws Exception;
	public void updatePaymentInfoForDDFinishBackup(Map<String, Object> map) throws Exception;
	public void updatePurchaseCansle(Map<String, Object> map) throws Exception;
	
	public void deletePaymentInfo(Map<String, Object> map) throws Exception;

	public void comparePaymentInfoForDebtorCreditor(Map userMap,CarInfoVO carInfoVO,Map<String, Object> carInfoMap,List<Map<String, Object>> oldPaymentInfoList,String allocationId) throws Exception;
}
