package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.DriverDepositMapper;
import kr.co.carrier.service.DriverDepositService;
import kr.co.carrier.vo.DepositDriverVO;
import kr.co.carrier.vo.DriverDepositVO;

@Service("driverDepositService")
public class DriverDepositServiceImpl implements DriverDepositService{
	
	
	@Resource(name="driverDepositMapper")
	private DriverDepositMapper driverDepositMapper;
	
	
	public Map<String, Object> selectDriverDeposit(Map<String, Object> map) throws Exception{
		return driverDepositMapper.selectDriverDeposit(map);
	}
	
	public List<Map<String, Object>> selectDriverDepositList(Map<String, Object> map) throws Exception{
		return driverDepositMapper.selectDriverDepositList(map);
	}
	
	public List<Map<String, Object>> selectDriverDepositListDetail(Map<String, Object> map) throws Exception{
		return driverDepositMapper.selectDriverDepositListDetail(map);
	}
	
	public int selectDriverDepositListCount(Map<String, Object> map) throws Exception{
		return driverDepositMapper.selectDriverDepositListCount(map);
	}
	
	public int insertDriverDeposit(DriverDepositVO driverDepositVO) throws Exception{
		return driverDepositMapper.insertDriverDeposit(driverDepositVO);
	}
	
	public void deleteDriverDeposit(Map<String, Object> map) throws Exception{
		driverDepositMapper.deleteDriverDeposit(map);
	}
	
	public void updateDriverDeposit(DriverDepositVO driverDepositVO) throws Exception{
		driverDepositMapper.updateDriverDeposit(driverDepositVO);
	}
	
	public int insertDepositDriver(DepositDriverVO depositDriverVO) throws Exception{
		return driverDepositMapper.insertDepositDriver(depositDriverVO);
	}
	
	public Map<String, Object> selectDepositDriver(Map<String, Object> map) throws Exception{
		return driverDepositMapper.selectDepositDriver(map);
	}

	public int updateDriver(Map<String, String> map) throws Exception{
	     return driverDepositMapper.updateDriver(map);
	}
	
	public int updateDepositDriver(Map<String, String> map) throws Exception{
	     return driverDepositMapper.updateDepositDriver(map);
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
