package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.TotalSalesMapper;
import kr.co.carrier.service.TotalSalesService;
import kr.co.carrier.vo.CustomerVO;

@Service("totalSalesService")
public class TotalSalesServiceImpl implements TotalSalesService{

	
	@Resource(name="totalSalesMapper")
	private TotalSalesMapper totalSalesMapper;
	
	
	
	public Map<String, Object> selectCustomer(Map<String, Object> map) throws Exception{
		return totalSalesMapper.selectCustomer(map);
	}
	
	public Map<String, Object> selectCustomerByCustomerName(Map<String, Object> map) throws Exception{
		return totalSalesMapper.selectCustomerByCustomerName(map);
	}
	
	public List<Map<String, Object>> selectCustomerList(Map<String, Object> map) throws Exception{
		return totalSalesMapper.selectCustomerList(map);
	}
	
	public int selectCustomerListCount(Map<String, Object> map) throws Exception{
		return totalSalesMapper.selectCustomerListCount(map);
	}
	
	public List<Map<String, Object>> selectAllocationListByCustomerId(Map<String, Object> map) throws Exception{
		return totalSalesMapper.selectAllocationListByCustomerId(map);
	}
	
	public int selectAllocationCountByCustomerId(Map<String, Object> map) throws Exception{
		return totalSalesMapper.selectAllocationCountByCustomerId(map);
	}
	
	
	
	
}
