package kr.co.carrier.service.impl;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.CheckDetailMapper;
import kr.co.carrier.service.CheckDetailService;
import kr.co.carrier.vo.CarCheckDetailVO;

@Service("checkDetailService")
public class CheckDetailServiceImpl implements CheckDetailService{
	
	
	@Resource(name="checkDetailMapper")
	private CheckDetailMapper checkDetailMapper;

	
	public Map<String, Object> selectCheckDetail(Map<String, Object> map) throws Exception{
		return checkDetailMapper.selectCheckDetail(map);
	}
	
	public void insertCheckDetail(CarCheckDetailVO carCheckDetailVO) throws Exception{
		checkDetailMapper.insertCheckDetail(carCheckDetailVO);
	}
	
	public void updateCheckDetail(CarCheckDetailVO carCheckDetailVO) throws Exception{
		checkDetailMapper.updateCheckDetail(carCheckDetailVO);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
