package kr.co.carrier.service;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.SmsSendReserveVO;

public interface SmsSendReserveService {

	
	
	
	public Map<String, Object> selectSmsSendReserve(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectSmsSendReserveList(Map<String, Object> map) throws Exception;
	public int selectSmsSendReserveListCount(Map<String, Object> map) throws Exception;
	public int insertSmsSendReserve(SmsSendReserveVO smsSendReserveVO) throws Exception;
	public void deleteSmsSendReserve(Map<String, Object> map) throws Exception;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
