package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.DebtorCreditorMapper;
import kr.co.carrier.service.DebtorCreditorService;
import kr.co.carrier.vo.DebtorCreditorVO;

@Service("debtorCreditorService")
public class DebtorCreditorServiceImpl implements DebtorCreditorService {

	
	@Resource(name="debtorCreditorMapper")
	private DebtorCreditorMapper debtorCreditorMapper;
	
	
	public Map<String, Object> selectDebtorCreditor(Map<String, Object> map) throws Exception{
		return debtorCreditorMapper.selectDebtorCreditor(map);
	}
	
	public List<Map<String, Object>> selectDebtorCreditorList(Map<String, Object> map) throws Exception{
		return debtorCreditorMapper.selectDebtorCreditorList(map);
	}
	
	public int selectDebtorCreditorListCount(Map<String, Object> map) throws Exception{
		return debtorCreditorMapper.selectDebtorCreditorListCount(map);
	}
	
	public int insertDebtorCreditor(DebtorCreditorVO debtorCreditorVO) throws Exception{
		return debtorCreditorMapper.insertDebtorCreditor(debtorCreditorVO);
	}
	
	public void deleteDebtorCreditor(Map<String, Object> map) throws Exception{
		debtorCreditorMapper.deleteDebtorCreditor(map);
	}
	
	
	
	public List<Map<String, Object>> selectAllocationListForInsert(Map<String, Object> map) throws Exception{
		return debtorCreditorMapper.selectAllocationListForInsert(map);
	}
	
	public Map<String, Object> selectAllocationListUpdated(Map<String, Object> map) throws Exception{
		return debtorCreditorMapper.selectAllocationListUpdated(map);
	}
	
	public List<Map<String, Object>> selectBillPublishRequestListForInsert(Map<String, Object> map) throws Exception{
		return debtorCreditorMapper.selectBillPublishRequestListForInsert(map);
	}
	
	public List<Map<String, Object>> selectCarInfoListByBillPublishRequestId(Map<String, Object> map) throws Exception{
		return debtorCreditorMapper.selectCarInfoListByBillPublishRequestId(map);
	}
	
	public int selectCustomerListCount(Map<String, Object> map) throws Exception{
		return debtorCreditorMapper.selectCustomerListCount(map);
	}
	
	public List<Map<String, Object>> selectCustomerList(Map<String, Object> map) throws Exception{
		return debtorCreditorMapper.selectCustomerList(map);
	}
	
	public List<Map<String, Object>> selectDebtorCreditorListByCustomerId(Map<String, Object> map) throws Exception{
		return debtorCreditorMapper.selectDebtorCreditorListByCustomerId(map);
	}
	
	public int selectAllocationCountByDebtorCreditorId(Map<String, Object> map) throws Exception{
		return debtorCreditorMapper.selectAllocationCountByDebtorCreditorId(map);
	}
	
	public List<Map<String, Object>> selectAllocationListByDebtorCreditorId(Map<String, Object> map) throws Exception{
		return debtorCreditorMapper.selectAllocationListByDebtorCreditorId(map);
	}
		
	public List<Map<String, Object>> selectPaymentListByPayment(Map<String, Object> map) throws Exception{
		return debtorCreditorMapper.selectPaymentListByPayment(map);
	}
	
	public void updateDebtorCreditor(Map<String, Object> map) throws Exception{
		debtorCreditorMapper.updateDebtorCreditor(map);
	}
	
	public void updateDebtorCreditorTotalVal(Map<String, Object> map) throws Exception{
		debtorCreditorMapper.updateDebtorCreditorTotalVal(map);
	}
	
	public int selectCarInfoCountByDebtorCreditorId(Map<String, Object> map) throws Exception{
		return debtorCreditorMapper.selectCarInfoCountByDebtorCreditorId(map);
	}
	
	/***
	 * 2021.05.10
	 * 매출 취소 
	 */
	public void deleteDebtorCreditors(Map<String, Object> map) throws Exception{
		debtorCreditorMapper.deleteDebtorCreditors(map);
	}
	public void updateBillPublishRequest(Map<String, Object> map) throws Exception{
		debtorCreditorMapper.updateBillPublishRequest(map);
	}
	public void updateBillingStatus(Map<String, Object> map) throws Exception{
		debtorCreditorMapper.updateBillingStatus(map);
	}
	
	public String selectAllocationIdByBatchStatusId(Map<String, Object> map) throws Exception{
		return debtorCreditorMapper.selectAllocationIdByBatchStatusId(map);
	}
	
	
	
}
