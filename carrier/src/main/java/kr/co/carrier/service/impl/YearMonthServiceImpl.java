package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.YearMonthMapper;
import kr.co.carrier.service.YearMonthService;
import kr.co.carrier.vo.YearMonthVO;

@Service("yearMonthService")
public class YearMonthServiceImpl implements YearMonthService{

	
	
	
	@Resource(name="yearMonthMapper")
	private YearMonthMapper yearMonthMapper;
	
	public Map<String, Object> selectYearMonth(Map<String, Object> map) throws Exception{
		return yearMonthMapper.selectYearMonth(map);
	}
	
	public Map<String, Object> selectYearMonthByYearMonth(Map<String, Object> map) throws Exception{
		return yearMonthMapper.selectYearMonthByYearMonth(map);
	}
	
	public List<Map<String, Object>> selectYearMonthList(Map<String, Object> map) throws Exception{
		return yearMonthMapper.selectYearMonthList(map);
	}
	
	public int selectYearMonthListCount(Map<String, Object> map) throws Exception{
		return yearMonthMapper.selectYearMonthListCount(map);
	}
	
	public int insertYearMonth(YearMonthVO yearMonthVO) throws Exception{
		return yearMonthMapper.insertYearMonth(yearMonthVO);
	}
	
	
	
}
