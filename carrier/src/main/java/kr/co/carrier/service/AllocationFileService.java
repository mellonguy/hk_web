package kr.co.carrier.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import kr.co.carrier.utils.ResultApi;
import kr.co.carrier.vo.AllocationFileVO;

public interface AllocationFileService {

	public ResultApi insertAllocationFile(String rootDir, String subDir,String allocationId,String allocationStatus,HttpServletRequest request) throws Exception;
	public List<Map<String, Object>> selectAllocationFileList(Map<String, Object> map) throws Exception;
	public void deleteAllocationFile(Map<String, Object> map) throws Exception;
	public void insertAllocationFile(AllocationFileVO allocationFileVO) throws Exception;
	public void insertCaSendAllocationFile(Map<String, Object> map) throws Exception;
	
	
	
	
	
	
	
	
}
