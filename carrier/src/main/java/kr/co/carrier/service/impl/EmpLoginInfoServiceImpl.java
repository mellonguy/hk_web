package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.EmpLoginInfoMapper;
import kr.co.carrier.service.EmpLoginInfoService;
import kr.co.carrier.vo.EmpLoginInfoVO;

@Service("empLoginInfoService")
public class EmpLoginInfoServiceImpl implements EmpLoginInfoService{

	
	
	@Resource(name="empLoginInfoMapper")
	private EmpLoginInfoMapper empLoginInfoMapper;
	
	
	
	public Map<String, Object> selectEmpLoginInfo(Map<String, Object> map) throws Exception{
		return empLoginInfoMapper.selectEmpLoginInfo(map);
	}
	
	public List<Map<String, Object>> selectEmpLoginInfoList(Map<String, Object> map) throws Exception{
		return empLoginInfoMapper.selectEmpLoginInfoList(map);
	}
	
	public int selectEmpLoginInfoListCount(Map<String, Object> map) throws Exception{
		return empLoginInfoMapper.selectEmpLoginInfoListCount(map);
	}
	
	public int insertEmpLoginInfo(EmpLoginInfoVO empLoginInfoVO) throws Exception{
		return empLoginInfoMapper.insertEmpLoginInfo(empLoginInfoVO);
	}
	
	public void deleteEmpLoginInfo(Map<String, Object> map) throws Exception{
		empLoginInfoMapper.deleteEmpLoginInfo(map);
	}
	
	public void updateEmpLoginInfoLogoutDt(Map<String, Object> map) throws Exception{
		empLoginInfoMapper.updateEmpLoginInfoLogoutDt(map);
	}
	
	public int insertDeadLineCheck(Map<String, Object> map) throws Exception{
		return empLoginInfoMapper.insertDeadLineCheck(map);
	}
	
	public Map<String, Object> selectLoginCheck(Map<String, Object> map) throws Exception{
		return empLoginInfoMapper.selectLoginCheck(map);
	}
	public void updateEmpLoginCertY(Map<String, Object> map) throws Exception{
		 empLoginInfoMapper.updateEmpLoginCertY(map);
	}
	public Map<String, Object> selectMonthCertCheck(Map<String, Object> map) throws Exception{
		return empLoginInfoMapper.selectMonthCertCheck(map);
	}
	public int insertMonthLoginCert(Map<String, Object> map) throws Exception{
		return empLoginInfoMapper.insertMonthLoginCert(map);
	}
	
	
	
	
	
	
}
