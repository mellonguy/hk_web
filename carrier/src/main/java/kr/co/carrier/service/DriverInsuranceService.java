package kr.co.carrier.service;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.DriverInsuranceVO;

public interface DriverInsuranceService {

	
	public Map<String, Object> selectDriverInsurance(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectDriverInsuranceList(Map<String, Object> map) throws Exception;
	public int selectDriverInsuranceCount(Map<String, Object> map) throws Exception;
	public int insertDriverInsurance(DriverInsuranceVO driverInsuranceVO) throws Exception;
	public void updateDriverInsurance(DriverInsuranceVO driverInsuranceVO) throws Exception;
	
	
	
	
}
