package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.DriverDecideStatusMapper;
import kr.co.carrier.service.DriverDecideStatusService;
import kr.co.carrier.vo.DriverDecideStatusVO;

@Service("driverDecideStatusService")
public class DriverDecideStatusServiceImpl implements DriverDecideStatusService{

	
	
	@Resource(name="driverDecideStatusMapper")
	private DriverDecideStatusMapper driverDecideStatusMapper;
	
	
	public Map<String, Object> selectDriverDecideStatus(Map<String, Object> map) throws Exception{
		return driverDecideStatusMapper.selectDriverDecideStatus(map);
	}
	
	public List<Map<String, Object>> selectDriverDecideStatusList(Map<String, Object> map) throws Exception{
		return driverDecideStatusMapper.selectDriverDecideStatusList(map);
	}
	
	public int selectDriverDecideStatusListCount(Map<String, Object> map) throws Exception{
		return driverDecideStatusMapper.selectDriverDecideStatusListCount(map);
	}
	
	public int insertDriverDecideStatus(DriverDecideStatusVO driverDecideStatusVO) throws Exception{
		return driverDecideStatusMapper.insertDriverDecideStatus(driverDecideStatusVO);
	}
	
	public void deleteDriverDecideStatus(Map<String, Object> map) throws Exception{
		driverDecideStatusMapper.deleteDriverDecideStatus(map);
	}
	
	
	
	
	
	
}
