package kr.co.carrier.service;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.CustomerPersonInChargeVO;
import kr.co.carrier.vo.PaymentPersonInChargeVO;

public interface PersonInChargeService {

	public Map<String, Object> selectPersonInCharge(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectPersonInChargeList(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectPaymentPersonInChargeList(Map<String, Object> map) throws Exception;
	public int selectPersonInChargeListCount(Map<String, Object> map) throws Exception;
	public int insertPersonInCharge(CustomerPersonInChargeVO customerPersonInChargeVO) throws Exception;
	public int insertPaymentPersonInCharge(PaymentPersonInChargeVO paymentPersonInChargeVO) throws Exception;
	public void updatePersonInCharge(CustomerPersonInChargeVO customerPersonInChargeVO) throws Exception;
	public void deletePersonInCharge(Map<String, Object> map) throws Exception;
	public void deletePaymentPersonInCharge(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectPersonInChargeByPersonInChargeId(Map<String, Object> map) throws Exception;
	
	
	
	
	
	
	
	
	
	
	
}
