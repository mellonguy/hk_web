package kr.co.carrier.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.xml.bind.ParseConversionEvent;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFPrintSetup;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.DriverDeductFileMapper;
import kr.co.carrier.service.DriverDeductFileService;
import kr.co.carrier.service.DriverDeductInfoService;
import kr.co.carrier.service.DriverService;
import kr.co.carrier.service.FileUploadService;
import kr.co.carrier.service.PaymentInfoService;
import kr.co.carrier.utils.WebUtils;
import kr.co.carrier.vo.DriverDeductFileVO;

@Service("driverDeductFileService")
public class DriverDeductFileServiceImpl implements DriverDeductFileService{

	
	@Resource(name="driverDeductFileMapper")
	private DriverDeductFileMapper driverDeductFileMapper;
	
	
	@Autowired
    private DriverDeductInfoService driverDeductInfoService;
    
	
	@Autowired
    private DriverService driverService;
	
	@Autowired
    private PaymentInfoService paymentInfoService;
	
	@Autowired
	private FileUploadService fileUploadService;
	
	
	@Value("#{appProp['upload.base.path']}")
    private String rootDir;    
    private String subDir;
	
	
	
	public int insertDriverDeductFile(DriverDeductFileVO driverDeductfileVO) throws Exception{
		return driverDeductFileMapper.insertDriverDeductFile(driverDeductfileVO);
	}
	
	public List<Map<String, Object>> selectDriverDeductFileList(Map<String, Object> map) throws Exception{
		return driverDeductFileMapper.selectDriverDeductFileList(map);
	}
	
	public void deleteDriverDeductFile(Map<String, Object> map) throws Exception{
		driverDeductFileMapper.deleteDriverDeductFile(map);
	}
	
	public void updateDriverViewFlag(Map<String, Object> map) throws Exception{
		driverDeductFileMapper.updateDriverViewFlag(map);
	}
	
	
	public Map<String, Object> makeDriverDeductFile(Map<String, Object> paramMap) throws Exception{
		
		
		Map<String, Object> fileMap = new HashMap<String, Object>();
		List<Map<String, Object>> fileList = new ArrayList<Map<String,Object>>();
		
		
		try {
		
		
				String driverId = (String) paramMap.get("driverId");      
		        String selectMonth = (String) paramMap.get("decideMonth");
		        String year = selectMonth.split("-")[0]; 
		        String month = selectMonth.split("-")[1];
				String carAssignCompany = "";
				
		        
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("driverId",driverId);
				Map<String, Object> driver = driverService.selectDriver(map);
				
				File file = null;
				
				if(driver.get("car_assign_company") != null && driver.get("car_assign_company").toString().equals("S")) {
					file = new File(rootDir+"/forms", "deductlist_for_s.xlsx");
					carAssignCompany = "S";
					
				}else if(driver.get("car_assign_company") != null && driver.get("car_assign_company").toString().equals("C")) {
					file = new File(rootDir+"/forms", "deductlist_for_c.xlsx");
					carAssignCompany = "C";
				}else {
					//그럴일이 없겠지만 셀프도 아니고 캐리어도 아니면 그냥 리턴....
					return fileMap;
				}
				
				
		        //File file = new File(rootDir+"/forms", "deductlist.xlsx");
		        XSSFWorkbook wb = null;
		        
		        
				String startDt = "";
				String endDt = "";
				
				startDt = selectMonth;
				startDt = startDt.split("-")[0]+"-"+startDt.split("-")[1]+"-01";
				endDt = selectMonth;
				endDt = startDt.split("-")[0]+"-"+startDt.split("-")[1]+"-"+WebUtils.getLastDate("yyyy-mm-dd",Integer.parseInt(startDt.split("-")[0]),Integer.parseInt(startDt.split("-")[1]));
				map.put("startDt", startDt);
				map.put("endDt", endDt);
				map.put("selectMonth", selectMonth);
				map.put("startRownum", 0);
				map.put("numOfRows", 1);
				
				List<Map<String, Object>> driverList = driverDeductInfoService.selectDriverList(map);
				
			
						
				if(driverList.size() > 0) {
					//int total = Integer.parseInt(driverList.get(0).get("another_sum").toString().replaceAll(",", ""));
					
					//현대캐피탈아닌것 
					int totalA = Integer.parseInt(driverList.get(0).get("hc_n").toString().replaceAll(",", ""));
					//현대캐피탈인것
					int totalB = Integer.parseInt(driverList.get(0).get("hc_y").toString().replaceAll(",", ""));
					
					//캐리어 기사님은 전체 합에서 현장수금을 뺀만큼만 계산한다.
					int totalC = totalA+totalB;
					
					int deduction = Integer.parseInt(driver.get("deduction_rate").toString())*totalA/100;
					
					//현대캐피탈인것
					int deductionRate = 0;
					if(driver.get("deduction_rate") != null && !driver.get("deduction_rate").toString().equals("")) {
		    			deductionRate = Integer.parseInt(driver.get("deduction_rate").toString());
		    			
		    			if(driver.get("car_assign_company") != null && driver.get("car_assign_company").toString().equals("S")) {
		    				
		    				if(deductionRate > 10) {
		        				if(deductionRate == 15) {
		        					//지입기사이면 14% 아니면 15%
		        					if(driver.get("driver_kind") != null) {
		        						if(driver.get("driver_kind").equals("00") || driver.get("driver_kind").equals("03")) {
		            						//deductionRate = 0;
		        							deductionRate = deductionRate-10;
		        							if(deductionRate < 0) {
		        								deductionRate = 0;
		        							}
		            					}else if(driver.get("driver_kind").equals("01")) {
		            						deductionRate = 4;
		            					}else if(driver.get("driver_kind").equals("02")) {
		            						deductionRate = 5;
		            					}else {
		            						deductionRate = 0;
		            					}
		        					}else {
		        						deductionRate = 0;
		        					}
		        					
		        				}else {
		        					deductionRate = deductionRate-10;
		        				}
		        			}else if(deductionRate == 10) {
		        				deductionRate = 0;
		        			}
		    			}
		    			
		    		}
					
					//int deductionRate = 0;
					map.put("item", "1");
					map.put("decideMonth", selectMonth);
					String item1 = driverDeductInfoService.selectDriverDeductInfoByItem(map).get("amount").toString();
					map.put("item1", item1);
					Map<String, Object> ddAmount = paymentInfoService.selectAnotherCalDDInfo(map); //현장 수금 조회 
					map.put("ddAmount", ddAmount);
					map.put("item", "2");
					String item2 = driverDeductInfoService.selectDriverDeductInfoByItem(map).get("amount").toString(); 
					map.put("item2", item2);
					
					List<Map<String, Object>> driverDeductInfoList = driverDeductInfoService.selectDriverDeductInfoListByItem(map);
					
					XSSFRow row;
					XSSFCell cell;
					try {//엑셀 파일 오픈
						wb = new XSSFWorkbook(new FileInputStream(file));
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
					
					XSSFSheet sheet = wb.getSheetAt(0);         
			        
					XSSFPrintSetup print = sheet.getPrintSetup();
					print.setNoOrientation(false);
					print.setLandscape(false);
					
					CellStyle style = wb.createCellStyle();
					style.setFillForegroundColor(IndexedColors.PALE_BLUE.getIndex());
			        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
			        style.setAlignment(CellStyle.ALIGN_CENTER);
			        style.setBorderBottom(CellStyle.BORDER_DOUBLE);
			        style.setBorderTop(CellStyle.BORDER_DOUBLE);
			        style.setBorderLeft(CellStyle.BORDER_DOUBLE);
			        style.setBorderRight(CellStyle.BORDER_DOUBLE);
					
			        int rows = sheet.getLastRowNum();
					int cells = sheet.getRow(0).getLastCellNum();
					
					FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
					
					 for (int r = 2; r <= rows; r++) {
						 
						 FormulaEvaluator formulaEval = wb.getCreationHelper().createFormulaEvaluator();
    					 
 			        	row = sheet.getRow(r); // row 가져오기
 			        	if (row != null) {
 			        		for (int c = 0; c < cells; c++) {
 			        			cell = row.getCell(c);
 			        			if (cell != null) { 
 			        				String value = "";

 									switch (cell.getCellType()) {
 									   	case XSSFCell.CELL_TYPE_FORMULA:
 									   		
 									   		CellValue evaluate = formulaEval.evaluate(cell);
 									   	  if( evaluate != null ) {
 									   		  
 									   		  try {
 									   			  
 									   			  if(WebUtils.isNumber(evaluate.formatAsString())) {
 									   				  
 									   				value=  String.valueOf(new Double(evaluate.getNumberValue()).intValue());
 									   				   
 									   			  }else {
 									   				  
 									   				  value = evaluate.formatAsString();
 									   			  }
 									   			  
 									   		  }catch(Exception e) {
 									   			  e.printStackTrace();
 									   			  
 									   		  }
 									   		  
 									   		  
	    			        				   }else {
	    			        					   
	    			        					   value = "";
	    			        				   }
 									   		
 									   		/*
 									   		if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_NUMERIC){
 									   			value = "" + (int)cell.getNumericCellValue();
 									   		}else if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_STRING){
 									   			value = "" + cell.getStringCellValue(); 
 									   		}
 									   		value = cell.getCellFormula();
 									   		*/
 									   		
 									   		break;
 									   	case XSSFCell.CELL_TYPE_NUMERIC:
										   		value = "" + (int)cell.getNumericCellValue();
										   		break;
										   	case XSSFCell.CELL_TYPE_STRING:
										   		value = "" + cell.getStringCellValue();
										   		break;
										   	case XSSFCell.CELL_TYPE_BLANK:
										   		value = "";
										   		break;
										   	case XSSFCell.CELL_TYPE_ERROR:
										   		value = "" + cell.getErrorCellValue();
										   		break;
										   	default:
										}
										
										if(value != null && !value.equals("")) {
											value = value.trim();
										}
										int nIndex = cell.getColumnIndex();
										
									if(r==3) {
										if(nIndex == 8){
											String test  = value;
											test = test.replace("0000", year);
											test = test.replace("00", month);
											cell = row.getCell(8);
					        				cell.setCellValue(test);
										}
									}else if(r==4) {
										if(nIndex == 8){
											
											if(driver.get("provide_kind") != null && !driver.get("provide_kind").toString().equals("")) {
												if(driver.get("provide_kind").toString().equals("A")) {
													String test = WebUtils.getNextMonth(Integer.parseInt(year),Integer.parseInt(month));
													value = value.replace("AA", test.split("-")[0].substring(2));
													value = value.replace("BB", test.split("-")[1]);
													value = value.replace("CC", WebUtils.getLastDate("dd",Integer.parseInt(test.split("-")[0]),Integer.parseInt(test.split("-")[1])));
												}else if(driver.get("provide_kind").toString().equals("B")) {
													String test = WebUtils.getNextNextMonth(Integer.parseInt(year),Integer.parseInt(month));
													value = value.replace("AA", test.split("-")[0].substring(2));
													value = value.replace("BB", test.split("-")[1]);
													value = value.replace("CC", "05");
												}else if(driver.get("provide_kind").toString().equals("C")) {
													String test = WebUtils.getNextNextMonth(Integer.parseInt(year),Integer.parseInt(month));
													value = value.replace("AA", test.split("-")[0].substring(2));
													value = value.replace("BB", test.split("-")[1]);
													value = value.replace("CC", "07");
												}else if(driver.get("provide_kind").toString().equals("D")) {
													String test = WebUtils.getNextNextMonth(Integer.parseInt(year),Integer.parseInt(month));
													value = value.replace("AA", test.split("-")[0].substring(2));
													value = value.replace("BB", test.split("-")[1]);
													value = value.replace("CC", "10");
												}else if(driver.get("provide_kind").toString().equals("E")) {
													String test = WebUtils.getNextNextMonth(Integer.parseInt(year),Integer.parseInt(month));
													value = value.replace("AA", test.split("-")[0].substring(2));
													value = value.replace("BB", test.split("-")[1]);
													value = value.replace("CC", "15");
												}else if(driver.get("provide_kind").toString().equals("F")) {
													String test = WebUtils.getNextNextMonth(Integer.parseInt(year),Integer.parseInt(month));
													value = value.replace("AA", test.split("-")[0].substring(2));
													value = value.replace("BB", test.split("-")[1]);
													value = value.replace("CC",WebUtils.getLastDate("dd",Integer.parseInt(test.split("-")[0]),Integer.parseInt(test.split("-")[1])));
												}else if(driver.get("provide_kind").toString().equals("G")) {
													String test = WebUtils.getNextMonth(Integer.parseInt(year),Integer.parseInt(month));
													value = value.replace("AA", test.split("-")[0].substring(2));
													value = value.replace("BB", test.split("-")[1]);
													value = value.replace("CC", "15");
												}else if(driver.get("provide_kind").toString().equals("H")) {
													String test = WebUtils.getNextMonth(Integer.parseInt(year),Integer.parseInt(month));
													value = value.replace("AA", test.split("-")[0].substring(2));
													value = value.replace("BB", test.split("-")[1]);
													value = value.replace("CC", "25");
												}
											}else {
												value = value.replace("AA", "--");
												value = value.replace("BB", "--");
												value = value.replace("CC","--");
											}
											cell = row.getCell(8);
					        				cell.setCellValue(value);
										}
									}else if(r==5) {	//차량번호
										if(nIndex == 3){
											cell = row.getCell(3);
					        				cell.setCellValue(driver.get("car_num").toString());
										}
									}else if(r==6) {	//성명
										if(nIndex == 3){
											cell = row.getCell(3);
					        				cell.setCellValue(driver.get("driver_name").toString());
										}	
									}else if(r==7) {	//연락처
										if(nIndex == 3){
											cell = row.getCell(3);
					        				cell.setCellValue(driver.get("phone_num").toString());
										}
									}else if(r==11) {	//항목,금액
										
										if(nIndex == 1){
											cell = row.getCell(1);
											if(driverId.equals("2005172020")){ 		//김민수 기사님일 경우
												//공제적용 : 현대캐피탈 'N' - (픽업건이면서 현대캐피탈 'N')
												cell.setCellValue(month+"월 운송비(공제 적용)");
											}else {
												cell.setCellValue(month+"월 운송비");	
											}
					        				
										}else if(nIndex == 3){
											
											if(driverId.equals("2005172020")){ 		//김민수 기사님일 경우		
												
												map.put("distanceType","P");
												map.put("driverId", driverId);
												map.put("decideMonth", selectMonth);
												
												List<Map<String,Object>> mrKIMdrviverDeductFile = paymentInfoService.selectMrKIMDriverDeductList(map);
												String amount = mrKIMdrviverDeductFile.get(0).get("amount").toString().replaceAll(",", "");
												
												cell = row.getCell(3);

												//공제적용 : 현대캐피탈 'N' - (픽업건이면서 현대캐피탈 'N')
							        			cell.setCellValue(Integer.parseInt(driverList.get(0).get("hc_n").toString().replaceAll(",", "")) - Integer.parseInt(driverList.get(0).get("hc_n_p").toString().replaceAll(",", "")));
												
													
											}else { //김민수기사님이 아닌거 
												
												cell = row.getCell(3);
						        				cell.setCellValue(Integer.parseInt(driverList.get(0).get("hc_n").toString().replaceAll(",", "")));
												
											}
											
												
											
											
										}else {
											
											if(!driver.get("driver_kind").equals("03")) {
											
												if(driverDeductInfoList.size() > 0) {
													
													for(int t = 0; t < driverDeductInfoList.size(); t++) {
														row = sheet.getRow(r+t);
														cell = row.getCell(7);
														// 5 : 사고대납금 6:기타
														if(driverDeductInfoList.get(t).get("division").toString().equals("1")) {
															cell.setCellValue(driverDeductInfoList.get(t).get("etc") != null && !driverDeductInfoList.get(t).get("etc").toString().equals("") ? "보험료("+driverDeductInfoList.get(t).get("etc").toString()+")":"보험료");
														}else if(driverDeductInfoList.get(t).get("division").toString().equals("2")) {
															cell.setCellValue(driverDeductInfoList.get(t).get("etc") != null && !driverDeductInfoList.get(t).get("etc").toString().equals("") ? "차량할부("+driverDeductInfoList.get(t).get("etc").toString()+")":"차량할부");
														}else if(driverDeductInfoList.get(t).get("division").toString().equals("3")) {
															cell.setCellValue(driverDeductInfoList.get(t).get("etc") != null && !driverDeductInfoList.get(t).get("etc").toString().equals("") ? "과태료("+driverDeductInfoList.get(t).get("etc").toString()+")":"과태료");
														}else if(driverDeductInfoList.get(t).get("division").toString().equals("4")) {
															cell.setCellValue(driverDeductInfoList.get(t).get("etc") != null && !driverDeductInfoList.get(t).get("etc").toString().equals("") ? "미납통행료("+driverDeductInfoList.get(t).get("etc").toString()+")":"미납통행료");
														}else if(driverDeductInfoList.get(t).get("division").toString().equals("5")) {
															cell.setCellValue(driverDeductInfoList.get(t).get("etc") != null && !driverDeductInfoList.get(t).get("etc").toString().equals("") ? "사고대납금("+driverDeductInfoList.get(t).get("etc").toString()+")":"사고대납금");
														}else if(driverDeductInfoList.get(t).get("division").toString().equals("6")) {
															cell.setCellValue(driverDeductInfoList.get(t).get("etc") != null && !driverDeductInfoList.get(t).get("etc").toString().equals("") ? driverDeductInfoList.get(t).get("etc").toString():"");
															//cell.setCellValue("기타");
														}else {
															cell.setCellValue("");
														}
														cell = row.getCell(9);
								        				cell.setCellValue(Integer.parseInt(driverDeductInfoList.get(t).get("amount").toString().replaceAll(",", "")));
														
													}
													row = sheet.getRow(r);
			
												}	
											
											}
										}
										
										/*if(nIndex == 7){
											cell = row.getCell(7);
					        				cell.setCellValue("대납내역공제");
										}else if(nIndex == 9){
											cell = row.getCell(9);
					        				cell.setCellValue(Integer.parseInt(item2.replaceAll(",", "")));
										}*/
								
										
									}else if(r==12){	//항목,금액
										
										if(driverId.equals("2005172020")){ 		//김민수 기사님일 경우		
											
											map.put("distanceType","P");
											map.put("driverId", driverId);
											map.put("decideMonth", selectMonth);
											
											List<Map<String,Object>> mrKIMdrviverDeductFile = paymentInfoService.selectMrKIMDriverDeductList(map);
											String amount = mrKIMdrviverDeductFile.get(0).get("amount").toString().replaceAll(",", "");;
											
											if(nIndex == 1){
												
												cell = row.getCell(1);
						        				cell.setCellValue(month+"월 운송비(공제 미적용)");
						        				
						        				
						        				
											}else if(nIndex == 3){
												cell = row.getCell(3);
						        				cell.setCellValue(Integer.parseInt(amount));
											}
											
													
										}else { //김민수기사님이 아닌거 
											
											
											if(nIndex == 1){
												cell = row.getCell(1);
						        				cell.setCellValue(month+"월(현대캐피탈)");
											}else if(nIndex == 3){
												cell = row.getCell(3);
						        				cell.setCellValue(Integer.parseInt(driverList.get(0).get("hc_y").toString().replaceAll(",", "")));
											}
											
											
										}
										
											
									}else if(r==13) {	//항목,금액
										
										if(driverId.equals("2005172020")){ //김민수기사님인거
										
											if(nIndex == 1){
												cell = row.getCell(1);
						        				cell.setCellValue(month+"월(현대캐피탈)");
											}else if(nIndex == 3){
												cell = row.getCell(3);
//						        				cell.setCellValue(Integer.parseInt(driverList.get(0).get("hc_y").toString().replaceAll(",", "")));
						        				//현대캐피탈 금액 : 현대캐피탈 'Y' - (픽업건이면서 현대캐피탈 'Y')
						        				cell.setCellValue(Integer.parseInt(driverList.get(0).get("hc_y").toString().replaceAll(",", "")) - Integer.parseInt(driverList.get(0).get("hc_y_p").toString().replaceAll(",", "")));
											}
											
											
										}else { //김민수기사님이 아닌거 
												
										}
											
									}else if(r==18) {	//항목,금액
										
										if(carAssignCompany.equals("S")) {
											if(nIndex == 1){
												cell = row.getCell(1);
												//double val = Double.parseDouble(driver.get("deduction_rate").toString())/100;
						        				cell.setCellValue(Double.parseDouble(driver.get("deduction_rate").toString())/100);
											}/*else if(nIndex == 3){
												cell = row.getCell(3);
						        				cell.setCellValue(Integer.parseInt(ddAmount.get("ddAmount").toString().replaceAll(",", "")));
											}*/
										
										}else if(carAssignCompany.equals("C")) {
										  
										    if(nIndex == 1){
                                              cell = row.getCell(1);
                                              cell.setCellValue("현장수금");
                                            }else if(nIndex == 3){
                                              cell = row.getCell(3);
                                              if(0 < Integer.parseInt(ddAmount.get("cnt").toString())) {
                                            	  cell.setCellValue(Integer.parseInt(ddAmount.get("ddAmount").toString().replaceAll(",", "")));
                                              }
                                            }
										}
										
									}else if(r==19) {	//항목,금액
										
										if(carAssignCompany.equals("S")) {
											
											if(nIndex == 1){
												cell = row.getCell(1);
												cell.setCellValue(Double.parseDouble(String.valueOf(deductionRate))/100);
											}
											/*
											else if(nIndex == 3){
												cell = row.getCell(3);
						        				cell.setCellValue(Integer.parseInt(ddAmount.get("ddAmount").toString().replaceAll(",", "")));
											}
											*/	
											
										}else if(carAssignCompany.equals("C")) {
											
											if(nIndex == 1){
												cell = row.getCell(1);
												//double val = Double.parseDouble(driver.get("deduction_rate").toString())/100;
						        				cell.setCellValue(Double.parseDouble(driver.get("deduction_rate").toString())/100);
											}/*else if(nIndex == 3){
												cell = row.getCell(3);
						        				cell.setCellValue(Integer.parseInt(ddAmount.get("ddAmount").toString().replaceAll(",", "")));
											}*/
											
										}
										
										
									}else if(r==20) {	//항목,금액
										
										if(carAssignCompany.equals("S")) {
										  
									    if(nIndex == 1){
                                          cell = row.getCell(1);
                                          cell.setCellValue("현장수금");
                                          }else if(nIndex == 3){
                                          cell = row.getCell(3);
                                          	if(0 < Integer.parseInt(ddAmount.get("cnt").toString())) {
                                          		cell.setCellValue(Integer.parseInt(ddAmount.get("ddAmount").toString().replaceAll(",", "")));
                                          	}   
										  }
											
										}else if(carAssignCompany.equals("C")) {
											
											if(nIndex == 1){
												cell = row.getCell(1);
												
												//double val = Double.parseDouble(driver.get("deduction_rate").toString())/100;
												
												if(driver.get("driver_id").toString().equals("woongkyum.kim")) {
													cell.setCellValue(Double.parseDouble("10")/100);
												}else {
													cell.setCellValue(Double.parseDouble("15")/100);	
												}
												
											}/*else if(nIndex == 3){
												cell = row.getCell(3);
						        				cell.setCellValue(Integer.parseInt(ddAmount.get("ddAmount").toString().replaceAll(",", "")));
											}*/
											
										}
										
									}else if(r==21) {	//항목,금액
										if(nIndex == 1){
											cell = row.getCell(1);
					        				cell.setCellValue("청구내역공제");
										}else if(nIndex == 3){
											cell = row.getCell(3);
					        				cell.setCellValue(Integer.parseInt(item1.replaceAll(",", "")));
										}
									}
									
										
				        			} else {
				        				//System.out.print("[null]\t");
				        			}
				        		} // for(c) 문
				        		
		
				        	}
				        	
				        }
					
					 
					XSSFFormulaEvaluator.evaluateAllFormulaCells(wb);
					
					//공급가액
					row = sheet.getRow(24); 
					cell = row.getCell(3);
					int supplyVal = (int)cell.getNumericCellValue();
					//부가세액
					row = sheet.getRow(25); 
					cell = row.getCell(3);
					int vatVal = (int)cell.getNumericCellValue();
					//총액
					row = sheet.getRow(26); 
					cell = row.getCell(3);
					int totalVal = (int)cell.getNumericCellValue();
					
					DriverDeductFileVO driverDeductFileVO = new DriverDeductFileVO();
					fileMap = fileUploadService.upload(rootDir, "deduct_list","("+selectMonth+")"+driver.get("driver_name").toString()+".xlsx", wb);
					fileList.add(fileMap);
					driverDeductFileVO.setCategoryType("deduct_list");
					driverDeductFileVO.setDecideMonth(selectMonth);
					driverDeductFileVO.setDriverDeductFileId("FLE"+UUID.randomUUID().toString().replaceAll("-", ""));
					driverDeductFileVO.setDriverDeductFileNm(fileMap.get("ORG_NAME").toString());
					driverDeductFileVO.setDriverDeductFilePath(fileMap.get("SAVE_NAME").toString());
					driverDeductFileVO.setDriverId(driverId);
					driverDeductFileVO.setSupplyVal(String.valueOf(supplyVal));
					driverDeductFileVO.setVatVal(String.valueOf(vatVal));
					driverDeductFileVO.setTotalVal(String.valueOf(totalVal));
					this.insertDriverDeductFile(driverDeductFileVO);
				}
				
		 
		List<Map<String, Object>> commissionList = driverDeductInfoService.selectDriverCommissionRate(map);
				
		if(driver.get("driver_kind").equals("03")) { //직영기사
			
			file = new File(rootDir+"/forms", "deductlist_for_d.xlsx");
			carAssignCompany = "";
			
			
			if(driverList.size() > 0) {
				//int total = Integer.parseInt(driverList.get(0).get("another_sum").toString().replaceAll(",", ""));
				
				//현대캐피탈아닌것
				//int totalA = Integer.parseInt(driverList.get(0).get("hc_n").toString().replaceAll(",", ""));
				//현대캐피탈인것
				//int totalB = Integer.parseInt(driverList.get(0).get("hc_y").toString().replaceAll(",", ""));
				
				//캐리어 기사님은 전체 합에서 현장수금을 뺀만큼만 계산한다.
				//int totalC = totalA+totalB;
				
				//int deduction = Integer.parseInt(driver.get("deduction_rate").toString())*totalA/100;
				
				//현대캐피탈인것
				//int deductionRate = 0;
//				if(driver.get("deduction_rate") != null && !driver.get("deduction_rate").toString().equals("")) {
//	    			deductionRate = Integer.parseInt(driver.get("deduction_rate").toString());
//	    			
//	    			if(driver.get("car_assign_company") != null && driver.get("car_assign_company").toString().equals("S")) {
//	    				
//	    				if(deductionRate > 10) {
//	        				if(deductionRate == 15) {
//	        					//지입기사이면 14% 아니면 15%
//	        					if(driver.get("driver_kind") != null) {
//	        						if(driver.get("driver_kind").equals("00") || driver.get("driver_kind").equals("03")) {
//	            						deductionRate = 0;
//	            					}else if(driver.get("driver_kind").equals("01")) {
//	            						deductionRate = 4;
//	            					}else if(driver.get("driver_kind").equals("02")) {
//	            						deductionRate = 5;
//	            					}else {
//	            						deductionRate = 0;
//	            					}
//	        					}else {
//	        						deductionRate = 0;
//	        					}
//	        					
//	        				}else {
//	        					deductionRate = deductionRate-10;
//	        				}
//	        			}else if(deductionRate == 10) {
//	        				deductionRate = 0;
//	        			}
//	    			}
//	    			
//	    		}
//				
				map.put("item", "1");
				map.put("decideMonth", selectMonth);
				String item1 = driverDeductInfoService.selectDriverDeductInfoByItem(map).get("amount").toString();
				map.put("item1", item1);
				Map<String, Object> ddAmount = paymentInfoService.selectAnotherCalDDInfo(map); //현장 수금 조회 
				map.put("ddAmount", ddAmount);
				map.put("item", "2");
				String item2 = driverDeductInfoService.selectDriverDeductInfoByItem(map).get("amount").toString();
				//String etc =driverDeductInfoService.selectDriverDeductInfoByItem(map).get("etc").toString();
				
				map.put("item2", item2);
				
				//map.put("etc", etc);
				
				List<Map<String, Object>> driverDeductInfoList = driverDeductInfoService.selectDriverDeductInfoListByItem(map);
				
				XSSFRow row;
				XSSFCell cell;
				try {//엑셀 파일 오픈
					wb = new XSSFWorkbook(new FileInputStream(file));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				XSSFSheet sheet = wb.getSheetAt(0);         
		        
				XSSFPrintSetup print = sheet.getPrintSetup();
				print.setNoOrientation(false);
				print.setLandscape(false);
				
				CellStyle style = wb.createCellStyle();
				style.setFillForegroundColor(IndexedColors.PALE_BLUE.getIndex());
		        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		        style.setAlignment(CellStyle.ALIGN_CENTER);
		        style.setBorderBottom(CellStyle.BORDER_DOUBLE);
		        style.setBorderTop(CellStyle.BORDER_DOUBLE);
		        style.setBorderLeft(CellStyle.BORDER_DOUBLE);
		        style.setBorderRight(CellStyle.BORDER_DOUBLE);
				
		        int rows = sheet.getLastRowNum();
				int cells = sheet.getRow(0).getLastCellNum();
				
				
				FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
				
				 for (int r = 2; r <= rows; r++) {
					 
					 FormulaEvaluator formulaEval = wb.getCreationHelper().createFormulaEvaluator();
					 
			        	row = sheet.getRow(r); // row 가져오기
			        	if (row != null) {
			        		for (int c = 0; c < cells; c++) {
			        			cell = row.getCell(c);
			        			if (cell != null) { 
			        				String value = "";

									switch (cell.getCellType()) {
									   	case XSSFCell.CELL_TYPE_FORMULA:
									   		
									   		CellValue evaluate = formulaEval.evaluate(cell);
									   	  if( evaluate != null ) {
									   		  
									   		  try {
									   			  
									   			  if(WebUtils.isNumber(evaluate.formatAsString())) {
									   				  
									   				value=  String.valueOf(new Double(evaluate.getNumberValue()).intValue());
									   				   
									   			  }else {
									   				  
									   				  value = evaluate.formatAsString();
									   			  }
									   			  
									   		  }catch(Exception e) {
									   			  e.printStackTrace();
									   			  
									   		  }
									   		  
									   		  
  			        				   }else {
  			        					   
  			        					   value = "";
  			        				   }
									   		
									   		/*
									   		if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_NUMERIC){
									   			value = "" + (int)cell.getNumericCellValue();
									   		}else if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_STRING){
									   			value = "" + cell.getStringCellValue(); 
									   		}
									   		value = cell.getCellFormula();
									   		*/
									   		
									   		break;
									   	case XSSFCell.CELL_TYPE_NUMERIC:
									   		value = "" + (int)cell.getNumericCellValue();
									   		break;
									   	case XSSFCell.CELL_TYPE_STRING:
									   		value = "" + cell.getStringCellValue();
									   		break;
									   	case XSSFCell.CELL_TYPE_BLANK:
									   		value = "";
									   		break;
									   	case XSSFCell.CELL_TYPE_ERROR:
									   		value = "" + cell.getErrorCellValue();
									   		break;
									   	default:
									}
									
									if(value != null && !value.equals("")) {
										value = value.trim();
									}
									int nIndex = cell.getColumnIndex();
									
								if(r==3) {
									if(nIndex == 8){
										String test  = value;
										test = test.replace("0000", year);
										test = test.replace("00", month);
										cell = row.getCell(8);
				        				cell.setCellValue(test);
									}
								}else if(r==4) {
									if(nIndex == 8){
										
										if(driver.get("provide_kind") != null && !driver.get("provide_kind").toString().equals("")) {
											if(driver.get("provide_kind").toString().equals("A")) {
												String test = WebUtils.getNextMonth(Integer.parseInt(year),Integer.parseInt(month));
												value = value.replace("AA", test.split("-")[0].substring(2));
												value = value.replace("BB", test.split("-")[1]);
												value = value.replace("CC", WebUtils.getLastDate("dd",Integer.parseInt(test.split("-")[0]),Integer.parseInt(test.split("-")[1])));
											}else if(driver.get("provide_kind").toString().equals("B")) {
												String test = WebUtils.getNextNextMonth(Integer.parseInt(year),Integer.parseInt(month));
												value = value.replace("AA", test.split("-")[0].substring(2));
												value = value.replace("BB", test.split("-")[1]);
												value = value.replace("CC", "05");
											}else if(driver.get("provide_kind").toString().equals("C")) {
												String test = WebUtils.getNextNextMonth(Integer.parseInt(year),Integer.parseInt(month));
												value = value.replace("AA", test.split("-")[0].substring(2));
												value = value.replace("BB", test.split("-")[1]);
												value = value.replace("CC", "07");
											}else if(driver.get("provide_kind").toString().equals("D")) {
												String test = WebUtils.getNextNextMonth(Integer.parseInt(year),Integer.parseInt(month));
												value = value.replace("AA", test.split("-")[0].substring(2));
												value = value.replace("BB", test.split("-")[1]);
												value = value.replace("CC", "10");
											}else if(driver.get("provide_kind").toString().equals("E")) {
												String test = WebUtils.getNextNextMonth(Integer.parseInt(year),Integer.parseInt(month));
												value = value.replace("AA", test.split("-")[0].substring(2));
												value = value.replace("BB", test.split("-")[1]);
												value = value.replace("CC", "15");
											}else if(driver.get("provide_kind").toString().equals("F")) {
												String test = WebUtils.getNextNextMonth(Integer.parseInt(year),Integer.parseInt(month));
												value = value.replace("AA", test.split("-")[0].substring(2));
												value = value.replace("BB", test.split("-")[1]);
												value = value.replace("CC",WebUtils.getLastDate("dd",Integer.parseInt(test.split("-")[0]),Integer.parseInt(test.split("-")[1])));
											}else if(driver.get("provide_kind").toString().equals("G")) {
												String test = WebUtils.getNextMonth(Integer.parseInt(year),Integer.parseInt(month));
												value = value.replace("AA", test.split("-")[0].substring(2));
												value = value.replace("BB", test.split("-")[1]);
												value = value.replace("CC", "15");
											}else if(driver.get("provide_kind").toString().equals("H")) {
												String test = WebUtils.getNextMonth(Integer.parseInt(year),Integer.parseInt(month));
												value = value.replace("AA", test.split("-")[0].substring(2));
												value = value.replace("BB", test.split("-")[1]);
												value = value.replace("CC", "25");
											}
										}else {
											value = value.replace("AA", "--");
											value = value.replace("BB", "--");
											value = value.replace("CC","--");
										}
										cell = row.getCell(8);
				        				cell.setCellValue(value);
									}
								}else if(r==5) {	//차량번호
									if(nIndex == 3){
										cell = row.getCell(3);
				        				cell.setCellValue(driver.get("car_num").toString());
									}
								}else if(r==6) {	//성명
									if(nIndex == 3){
										cell = row.getCell(3);
				        				cell.setCellValue(driver.get("driver_name").toString());
									}	
								}else if(r==7) {	//연락처
									if(nIndex == 3){
										cell = row.getCell(3);
				        				cell.setCellValue(driver.get("phone_num").toString());
									}
								}else if(r==10) {	//운송비 내역
									if(nIndex == 1){
										cell = row.getCell(1);
				        				cell.setCellValue(month+"월 운송비");
							
									}else if(nIndex == 7){
										cell = row.getCell(7);
				        				//cell.setCellValue(Integer.parseInt(driverList.get(0).get("deduct_amount").toString().replaceAll(",", "")));
										cell.setCellValue(Integer.parseInt(driverList.get(0).get("another_sum").toString().replaceAll(",", "")));
					
									}
								
									
								}else if(r==13) {	//급여 내역
									if(nIndex == 1){
										cell = row.getCell(1);
										
				        				cell.setCellValue(Double.parseDouble(driver.get("commission_rate").toString())/100);
										
									}else if(nIndex == 7){
										
										cell = row.getCell(7);
										cell.setCellValue(Integer.parseInt(item2.replaceAll(",", "")));
									}
									
								}else if(r==17) {	//항목,금액
								  
							    if(nIndex == 1){
                                  row = sheet.getRow(r);
                                  cell = row.getCell(1);
                                  cell.setCellValue("현장수금");
                                  }else if(nIndex == 7){
                                      row = sheet.getRow(r);
                                      cell = row.getCell(7);
                                      if(0 < Integer.parseInt(ddAmount.get("cnt").toString())) {
                                    	  cell.setCellValue(Integer.parseInt(ddAmount.get("ddAmount").toString().replaceAll(",", "")));
                                      }
                                  }
								  if(commissionList.size() > 0) {
								
										for(int i=0; i<commissionList.size(); i++) {
											
											if(nIndex == 1){
											
												row = sheet.getRow(r+i+1);
												cell = row.getCell(1);
												cell.setCellValue(commissionList.get(i).get("etc").toString());
												
					        				}else if(nIndex == 7){
					        					
					        					row = sheet.getRow(r+i+1);
					        					cell = row.getCell(7);
					        					cell.setCellValue(Integer.parseInt(commissionList.get(i).get("amount").toString().replaceAll(",","")));

					        				}
										}
									}
								
								 }
								
			        			} else {
			        				//System.out.print("[null]\t");
			        			}
			        		} // for(c) 문
			        		
	
			        	}
			        	
			        }

				// System.out.println(Integer.parseInt(driver.get("commission_rate").toString()));
		
				 
				XSSFFormulaEvaluator.evaluateAllFormulaCells(wb);
				
				//공급가액
				row = sheet.getRow(25); 
				cell = row.getCell(7);
				int supplyVal = (int)cell.getNumericCellValue();
				//부가세액
				row = sheet.getRow(24); 
				cell = row.getCell(3);
				int vatVal = 0;
				//총액
				row = sheet.getRow(25); 
				cell = row.getCell(7);
				int totalVal = supplyVal;
				
				DriverDeductFileVO driverDeductFileVO = new DriverDeductFileVO();
				fileMap = fileUploadService.upload(rootDir, "deduct_list","("+selectMonth+")"+driver.get("driver_name").toString()+"(직영-수수료기사님)"+".xlsx", wb);
				fileList.add(fileMap);
				driverDeductFileVO.setCategoryType("deduct_list");
				driverDeductFileVO.setDecideMonth(selectMonth);
				driverDeductFileVO.setDriverDeductFileId("FLE"+UUID.randomUUID().toString().replaceAll("-", ""));
				driverDeductFileVO.setDriverDeductFileNm(fileMap.get("ORG_NAME").toString());
				driverDeductFileVO.setDriverDeductFilePath(fileMap.get("SAVE_NAME").toString());
				driverDeductFileVO.setDriverId(driverId);
				driverDeductFileVO.setSupplyVal(String.valueOf(supplyVal));
				driverDeductFileVO.setVatVal(String.valueOf(vatVal));
				driverDeductFileVO.setTotalVal(String.valueOf(totalVal));
				this.insertDriverDeductFile(driverDeductFileVO);
			}
			
		
			
			
		}
			
			
			
				
				
				
		}catch(Exception e) {
			
			e.printStackTrace();
			
		}
		
		
		return fileMap;
		
	}
	
	
	
	public boolean nioFileCopy(String inFileName, String outFileName) {
		Path source = Paths.get(inFileName);
		Path target = Paths.get(outFileName);

		// 사전체크
		if (source == null) {
			throw new IllegalArgumentException("source must be specified");
		}
		if (target == null) {
			throw new IllegalArgumentException("target must be specified");
		}

		// 소스파일이 실제로 존재하는지 체크
		if (!Files.exists(source, new LinkOption[] {})) {
			throw new IllegalArgumentException("Source file doesn't exist: "
					+ source.toString());
		}

		
		try {
			Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING); // 파일복사

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

		if (Files.exists(target, new LinkOption[] {})) { // 파일이 정상적으로 생성이 되었다면
			// System.out.println("File Copied");
			return true; // true 리턴
		} else {
			System.out.println("File Copy Failed");
			return false; // 실패시 false
		}
	}
	
	
	
	
	
}
