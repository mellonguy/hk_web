package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.VoteMapper;
import kr.co.carrier.service.VoteService;
import kr.co.carrier.vo.VoteVO;

@Service("voteService")
public class VoteServiceImpl implements VoteService{

	
	@Resource(name="voteMapper")
	private VoteMapper voteMapper;
	
	
	public Map<String, Object> selectVote(Map<String, Object> map) throws Exception{
		return voteMapper.selectVote(map);
	}
	
	public List<Map<String, Object>> selectVoteList(Map<String, Object> map) throws Exception{
		return voteMapper.selectVoteList(map);
	}
	
	public int selectVoteListCount(Map<String, Object> map) throws Exception{
		return voteMapper.selectVoteListCount(map);
	}
	
	public int insertVote(VoteVO voteVO) throws Exception{
		return voteMapper.insertVote(voteVO);
	}
	
	public void deleteVote(Map<String, Object> map) throws Exception{
		voteMapper.deleteVote(map);
	}
	
	public int selectVoteCount(Map<String, Object> map) throws Exception{
		return voteMapper.selectVoteCount(map);
	}
	
	
	public List<Map<String, Object>> selectCandidateListA(Map<String, Object> map) throws Exception{
		return voteMapper.selectCandidateListA(map);
	}
	
	public List<Map<String, Object>> selectCandidateListO(Map<String, Object> map) throws Exception{
		return voteMapper.selectCandidateListO(map);
	}
	
	public List<Map<String, Object>> selectCandidateListD(Map<String, Object> map) throws Exception{
		return voteMapper.selectCandidateListD(map);
	}
	
	public List<Map<String, Object>> selectCandidateListS(Map<String, Object> map) throws Exception{
		return voteMapper.selectCandidateListS(map);
	}
	
	public List<Map<String, Object>> selectCandidateListC(Map<String, Object> map) throws Exception{
		return voteMapper.selectCandidateListC(map);
	}
	
	
	
	
	
	
	
	
	
	
	
}
