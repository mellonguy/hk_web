package kr.co.carrier.service.impl;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.DriverMapper;
import kr.co.carrier.service.DriverService;
import kr.co.carrier.vo.DriverVO;

@Service("driverService")
public class DriverServiceImpl implements DriverService{

	@Resource(name="driverMapper")
	private DriverMapper driverMapper;
	
	public Map<String, Object> selectDriver(Map<String, Object> map) throws Exception{
		return driverMapper.selectDriver(map);
	}
	
	public List<Map<String, Object>> selectDriverList(Map<String, Object> map) throws Exception{
		return driverMapper.selectDriverList(map);
	}
	
	public int selectDriverListCount(Map<String, Object> map) throws Exception{
		return driverMapper.selectDriverListCount(map);
	}
	
	public int insertDriver(DriverVO driverVO) throws Exception{
		return driverMapper.insertDriver(driverVO);
	}
	
	public void updateDriver(DriverVO driverVO) throws Exception{
		driverMapper.updateDriver(driverVO);
	}
	
	public void deleteDriver(Map<String, Object> map) throws Exception{
		driverMapper.deleteDriver(map);
	}
	
	public List<Map<String, Object>> selectDriverListByCustomerId(Map<String, Object> map) throws Exception{
		return driverMapper.selectDriverListByCustomerId(map);
	}
	
	public List<Map<String, Object>> selectDriverListForDriverLocation(Map<String, Object> map) throws Exception{
		return driverMapper.selectDriverListForDriverLocation(map);
	}
	
	public void updateDriverStatus(Map<String, Object> map) throws Exception{
		driverMapper.updateDriverStatus(map);
	}
	
	public Map<String, Object> selectDriverByDriverName(Map<String, Object> map) throws Exception{
		return driverMapper.selectDriverByDriverName(map);
	}
	
	public List<Map<String, Object>> selectWorkDriverList(Map<String, Object> map) throws Exception{
		return driverMapper.selectWorkDriverList(map);
	}
	
	public List<Map<String, Object>> selectDriverListByDriverName(Map<String, Object> map) throws Exception{
		return driverMapper.selectDriverListByDriverName(map);
	}
	
	public List<Map<String, Object>> selectDriverListByCarNum(Map<String, Object> map) throws Exception{
		return driverMapper.selectDriverListByCarNum(map);
	}
	
	public List<Map<String, Object>> selectDriverListByDriverNameForDriverPayment(Map<String, Object> map) throws Exception{
		return driverMapper.selectDriverListByDriverNameForDriverPayment(map);
	}
	public List<Map<String, Object>> selectArticlesDriverList(Map<String, Object> map) throws Exception{
		return driverMapper.selectArticlesDriverList(map);
	}
	public int selectArticlesCount(Map<String, Object> map) throws Exception{
		return driverMapper.selectArticlesCount(map);
	}
	public List<Map<String, Object>> selectSearchForDriverNameList(Map<String, Object> map) throws Exception{
		return driverMapper.selectSearchForDriverNameList(map);
	}
	public List<Map<String, Object>> selectRateDiscountDriverList(Map<String, Object> map) throws Exception{
		return driverMapper.selectRateDiscountDriverList(map);
	}
	public void updatedeDuctionRateDriver(Map<String, Object> map) throws Exception{
		driverMapper.updatedeDuctionRateDriver(map);
	}
	public int insertRateDiscountDriver(Map<String, Object> map) throws Exception{
		return driverMapper.insertRateDiscountDriver(map);
	}
	
}

