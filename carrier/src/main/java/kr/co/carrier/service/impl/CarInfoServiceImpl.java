package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.CarInfoMapper;
import kr.co.carrier.service.CarInfoService;
import kr.co.carrier.vo.CarDetailVO;
import kr.co.carrier.vo.CarInfoVO;
import kr.co.carrier.vo.CarInsuranceSubVO;
import kr.co.carrier.vo.CarInsuranceVO;
import kr.co.carrier.vo.CarMaintanceVO;

@Service("carInfoService")
public class CarInfoServiceImpl implements CarInfoService {

	
	@Resource(name="carInfoMapper")
	private CarInfoMapper carInfoMapper;
	
	public Map<String, Object> selectCarInfo(Map<String, Object> map) throws Exception{
		return carInfoMapper.selectCarInfo(map);
	}
	
	public List<Map<String, Object>> selectCarInfoList(Map<String, Object> map) throws Exception{
		return carInfoMapper.selectCarInfoList(map);
	}
	
	public int selectCarInfoListCount(Map<String, Object> map) throws Exception{
		return carInfoMapper.selectCarInfoListCount(map);
	}
	
	//로그기록
	public int insertCarInfoLogByMap(Map<String, Object> map) throws Exception{
		return carInfoMapper.insertCarInfoLogByMap(map);
	}
	
	//로그기록
	public int insertCarInfoLogByVO(CarInfoVO carInfoVO) throws Exception{
		return carInfoMapper.insertCarInfoLogByVO(carInfoVO);
	}
	
	public int insertCarInfo(CarInfoVO carInfoVO) throws Exception{
		return carInfoMapper.insertCarInfo(carInfoVO);
	}
	
	public void updateCarInfo(CarInfoVO carInfoVO) throws Exception{
		carInfoMapper.updateCarInfo(carInfoVO);
	}
	
	public void deleteCarInfo(Map<String, Object> map) throws Exception{
		carInfoMapper.deleteCarInfo(map);
	}

	public void updateCarInfoDriver(Map<String, Object> map) throws Exception{
		carInfoMapper.updateCarInfoDriver(map);
	}
	
	public void updateCarInfoSalesTotal(Map<String, Object> map) throws Exception{
		carInfoMapper.updateCarInfoSalesTotal(map);
	}
	
	public void updateCarInfoDriverSetEmp(Map<String, Object> map) throws Exception{
		carInfoMapper.updateCarInfoDriverSetEmp(map);
	}
	
	public void updateCarInfoDecideStatus(Map<String, Object> map) throws Exception{
		carInfoMapper.updateCarInfoDecideStatus(map);
	}
	
	public void updateDecideStatusBySearchData(Map<String, Object> map) throws Exception{
		carInfoMapper.updateDecideStatusBySearchData(map);
	}
	
	public List<Map<String, Object>> selectCarInfoListByCarIdNum(Map<String, Object> map) throws Exception{
		return carInfoMapper.selectCarInfoListByCarIdNum(map);
	}
	
	public List<Map<String, Object>> selectCarInfoListByCarNum(Map<String, Object> map) throws Exception{
		return carInfoMapper.selectCarInfoListByCarNum(map);
	}
	
	public List<Map<String, Object>> selectCarInfoListByContractNum(Map<String, Object> map) throws Exception{
		return carInfoMapper.selectCarInfoListByContractNum(map);
	}
	
	public List<Map<String, Object>> selectCarInfoListByDepartureArrival(Map<String, Object> map) throws Exception{
		return carInfoMapper.selectCarInfoListByDepartureArrival(map);
	}
	
	public Map<String, Object> selectSumSalesTotalByBatchStatusId(Map<String, Object> map) throws Exception{
		return carInfoMapper.selectSumSalesTotalByBatchStatusId(map);
	}
	
	public Map<String, Object> selectCarInfoForDuplicate(Map<String, Object> map) throws Exception{
		return carInfoMapper.selectCarInfoForDuplicate(map);
	}
	
	public void updateCarInfoDebtorCreditorIdByBillPublishRequstId(Map<String, Object> map) throws Exception{
		carInfoMapper.updateCarInfoDebtorCreditorIdByBillPublishRequstId(map);
	}
	
	public void updateCarInfoForCancel(Map<String, Object> map) throws Exception{
		carInfoMapper.updateCarInfoForCancel(map);
	}
	
	
	public int selectEqBillingCnt(Map<String, Object> map) throws Exception{
		return carInfoMapper.selectEqBillingCnt(map);
	}
	
	public void updateCarInfoDepartureAndArrival(Map<String, Object> map) throws Exception{
		carInfoMapper.updateCarInfoDepartureAndArrival(map);
	}
	
	public List<Map<String, Object>> selectCarInfoListByAllocationIdList(Map<String, Object> map) throws Exception{
		return carInfoMapper.selectCarInfoListByAllocationIdList(map);
	}
	public void updateCarInfoForDuplication(CarInfoVO carInfoVO) throws Exception{
		carInfoMapper.updateCarInfoForDuplication(carInfoVO);
	}
	
	public void updateCarInfoCarIdNum(Map<String, Object> map) throws Exception{
		carInfoMapper.updateCarInfoCarIdNum(map);
	}

	public List<Map<String, Object>> selectCarIdNumDubleCheckList(Map<String, Object> map) throws Exception{
		return carInfoMapper.selectCarIdNumDubleCheckList(map);
	}
	public List<Map<String, Object>> selectCarMaintananceList(Map<String, Object> map) throws Exception{
		return carInfoMapper.selectCarMaintananceList(map);
	}
	public int insertCarMaintance(CarMaintanceVO carMaintanceVO) throws Exception{
		return carInfoMapper.insertCarMaintance(carMaintanceVO);
	}
	public List<Map<String, Object>> selectCarMaintananceRegList(Map<String, Object> map) throws Exception{
		return carInfoMapper.selectCarMaintananceRegList(map);
	}
	public void deleteCarMaintanance(Map<String, Object> map) throws Exception{
		 carInfoMapper.deleteCarMaintanance(map);
	}
	public void updateCarMaintanance(CarMaintanceVO carMaintanceVO) throws Exception{
		carInfoMapper.updateCarMaintanance(carMaintanceVO);
	}
	public Map<String, Object> selectCarDetailE(Map<String, Object> map) throws Exception{
		return carInfoMapper.selectCarDetailE(map);
	}
	public int insertCarDetail(CarDetailVO caDetailVO) throws Exception{
		return carInfoMapper.insertCarDetail(caDetailVO);
	}
	public void updateCarDetail(CarDetailVO carDetailVO) throws Exception{
		carInfoMapper.updateCarDetail(carDetailVO);
	}
	public List<Map<String,Object>> selectCarDetailList(Map<String,Object>map) throws Exception{
		return carInfoMapper.selectCarDetailList(map);
	}
	public void deleteCarDetail(Map<String,Object>map) throws Exception{
		 carInfoMapper.deleteCarDetail(map);
	}
	
	public Map<String, Object> selectCarDetailMap(Map<String, Object> map) throws Exception{
		return carInfoMapper.selectCarDetailMap(map);
	}
	public int insertCarIncurance(CarInsuranceVO carInsuranceVO) throws Exception{
		return carInfoMapper.insertCarIncurance(carInsuranceVO);
	}
	public int insertCarInsuranceSub(CarInsuranceSubVO carInsuranceSubVO) throws Exception{
		return carInfoMapper.insertCarInsuranceSub(carInsuranceSubVO);
	}
	
	public List<Map<String,Object>> selectCarInsuranceList(Map<String,Object>map) throws Exception{
		return carInfoMapper.selectCarInsuranceList(map);
	}
	public int updateCarInsurance(CarInsuranceVO carInsuranceVO) throws Exception{
		return carInfoMapper.updateCarInsurance(carInsuranceVO);
	}
	public List<Map<String,Object>> selectCarInsuranceSubList(Map<String,Object>map) throws Exception{
		return carInfoMapper.selectCarInsuranceSubList(map);
	}
	
	public Map<String, Object> selectCarInsurance(Map<String, Object> map) throws Exception{
		return carInfoMapper.selectCarInsurance(map);
	}
	public void updateCarInsuranceSubFlag(Map<String, Object> map)throws Exception{
		carInfoMapper.updateCarInsuranceSubFlag(map);
	}
	public void updateCarInsuranceSub(CarInsuranceSubVO carInsuranceSubVO)throws Exception{
		carInfoMapper.updateCarInsuranceSub(carInsuranceSubVO);
	}
	public void updatePaymentCloseDt(Map<String, Object> map)throws Exception{
		carInfoMapper.updatePaymentCloseDt(map);
	}
	public List<Map<String,Object>> selectCarInsuranceMainList(Map<String,Object>map) throws Exception{
		return carInfoMapper.selectCarInsuranceMainList(map);
	}
	public void deleteCarInsurance(Map<String, Object> map) throws Exception{
		 carInfoMapper.deleteCarInsurance(map);
	}
	public void deleteCarInsuranceSub(Map<String, Object> map) throws Exception{
		carInfoMapper.deleteCarInsuranceSub(map);
	}
	public void updatePaymentClosingDt(Map<String, Object> map)throws Exception{
		carInfoMapper.updatePaymentClosingDt(map);
	}
	public List<Map<String,Object>> selectCarInspectionManageList(Map<String,Object>map) throws Exception{
		return carInfoMapper.selectCarInspectionManageList(map);
	}
	public int insertInspection(Map<String,Object>map) throws Exception{
		return carInfoMapper.insertInspection(map);
	}
	public void updateInspection(Map<String,Object>map) throws Exception{
		 carInfoMapper.updateInspection(map);
	}	
	
	
}

