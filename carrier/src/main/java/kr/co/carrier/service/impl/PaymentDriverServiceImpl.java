package kr.co.carrier.service.impl;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.PaymentDriverMapper;
import kr.co.carrier.service.PaymentDriverService;
import kr.co.carrier.vo.PaymentDriverVO;

@Service("paymentDriverService")
public class PaymentDriverServiceImpl implements PaymentDriverService{

	
	@Resource(name="paymentDriverMapper")
	private PaymentDriverMapper paymentDriverMapper;
	
	
	
	public Map<String, Object> selectPaymentDriver(Map<String, Object> map) throws Exception{
		
		return paymentDriverMapper.selectPaymentDriver(map);
	}
	
	public void updatePaymentDriver(PaymentDriverVO paymentDriverVO) throws Exception{
		
		paymentDriverMapper.updatePaymentDriver(paymentDriverVO);
		
	}
	
	
	
	
	
	
	
	
	
	
}
