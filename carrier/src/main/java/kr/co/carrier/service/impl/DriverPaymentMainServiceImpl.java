package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.DriverPaymentMainMapper;
import kr.co.carrier.service.DriverPaymentMainService;
import kr.co.carrier.vo.DriverPaymentMainVO;

@Service("driverPaymentMainService")
public class DriverPaymentMainServiceImpl implements DriverPaymentMainService{

	
	
	@Resource(name="driverPaymentMainMapper")
	private DriverPaymentMainMapper driverPaymentMainMapper;
	
	
	public Map<String, Object> selectDriverPaymentMain(Map<String, Object> map) throws Exception{
		return driverPaymentMainMapper.selectDriverPaymentMain(map);
	}
	
	public List<Map<String, Object>> selectDriverPaymentMainList(Map<String, Object> map) throws Exception{
		return driverPaymentMainMapper.selectDriverPaymentMainList(map);
	}
	
	public int selectDriverPaymentMainListCount(Map<String, Object> map) throws Exception{
		return driverPaymentMainMapper.selectDriverPaymentMainListCount(map);
	}
	
	public int insertDriverPaymentMain(DriverPaymentMainVO driverPaymentMainVO) throws Exception{
		return driverPaymentMainMapper.insertDriverPaymentMain(driverPaymentMainVO);
	}
	
	public void deleteDriverPaymentMain(Map<String, Object> map) throws Exception{
		driverPaymentMainMapper.deleteDriverPaymentMain(map);
	}
	
	public void updateDriverPaymentMain(DriverPaymentMainVO driverPaymentMainVO) throws Exception{
		driverPaymentMainMapper.updateDriverPaymentMain(driverPaymentMainVO);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
