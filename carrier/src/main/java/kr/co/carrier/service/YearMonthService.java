package kr.co.carrier.service;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.AccountVO;
import kr.co.carrier.vo.YearMonthVO;

public interface YearMonthService {

	
	public Map<String, Object> selectYearMonth(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectYearMonthByYearMonth(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectYearMonthList(Map<String, Object> map) throws Exception;
	public int selectYearMonthListCount(Map<String, Object> map) throws Exception;
	public int insertYearMonth(YearMonthVO yearMonthVO) throws Exception;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
