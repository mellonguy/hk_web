package kr.co.carrier.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.PaymentInfoMapper;
import kr.co.carrier.service.AdjustmentService;
import kr.co.carrier.service.AllocationService;
import kr.co.carrier.service.BillPublishRequestService;
import kr.co.carrier.service.CarInfoService;
import kr.co.carrier.service.DebtorCreditorService;
import kr.co.carrier.service.PaymentInfoService;
import kr.co.carrier.utils.BaseAppConstants;
import kr.co.carrier.vo.BillPublishRequestVO;
import kr.co.carrier.vo.CarInfoVO;
import kr.co.carrier.vo.DebtorCreditorVO;
import kr.co.carrier.vo.PaymentInfoVO;
import kr.co.carrier.vo.WithdrawVO;

@Service("paymentInfoService")
public class PaymentInfoServiceImpl implements PaymentInfoService {

	
	@Resource(name="paymentInfoMapper")
	private PaymentInfoMapper paymentInfoMapper;
	
	
	
	@Autowired
	private DebtorCreditorService debtorCreditorService;
	
	
	@Autowired
	private AdjustmentService adjustmentService;
	
	@Autowired
	private CarInfoService carInfoService;
	
	@Autowired
	private BillPublishRequestService billPublishRequestService;
	
	@Autowired
	private AllocationService allocationService;
	
	
	public List<Map<String, Object>> selectPaymentInfoList(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.selectPaymentInfoList(map);
	}
	
	public List<Map<String, Object>> selectPaymentInfoListForPayment(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.selectPaymentInfoListForPayment(map);
	}
	
	public int insertPaymentInfo(PaymentInfoVO paymentInfoVO) throws Exception{
		return paymentInfoMapper.insertPaymentInfo(paymentInfoVO);
	}
	
	public int insertPaymentInfoLogByVO(PaymentInfoVO paymentInfoVO) throws Exception{
		return paymentInfoMapper.insertPaymentInfoLogByVO(paymentInfoVO);
	}
	
	public int insertPaymentInfoLogByMap(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.insertPaymentInfoLogByMap(map);
	}
	
	public void deletePaymentInfo(Map<String, Object> map) throws Exception{
		paymentInfoMapper.deletePaymentInfo(map);
	}

	public void updatePaymentInfoDriverAmount(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updatePaymentInfoDriverAmount(map);
	}
	
	public void updatePaymentInfo(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updatePaymentInfo(map);
	}
	
	public void updatePaymentInfoDriverInfo(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updatePaymentInfoDriverInfo(map);
	}
	
	public Map<String, Object> selectPaymentInfoByPaymentDivision(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.selectPaymentInfoByPaymentDivision(map);
	}
	
	public List<Map<String, Object>> selectPaymentInfoListByPaymentDivision(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.selectPaymentInfoListByPaymentDivision(map);
	}
	
	public void updatePaymentInfoAmount(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updatePaymentInfoAmount(map);
	}
	
	public void updatePaymentInfoListBySearchData(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updatePaymentInfoListBySearchData(map);
	}
	
	public void updatePaymentInfoByIdx(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updatePaymentInfoByIdx(map);
	}
	
	public void updatePaymentInfoForAnother(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updatePaymentInfoForAnother(map);
	}
	
	public void updateSelectedStatusByIdx(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updateSelectedStatusByIdx(map);
	}
	
	public void updatePaymentInfoBySelectedStatus(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updatePaymentInfoBySelectedStatus(map);
	}
	
	public void updatePaymentInfoBillingDt(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updatePaymentInfoBillingDt(map);
	}
	
	public void updatePaymentInfoListDecideFinalBySearchData(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updatePaymentInfoListDecideFinalBySearchData(map);
	}
	
	public void updatePaymentInfoInit(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updatePaymentInfoInit(map);
	}
	
	public void updatePaymentInfoChange(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updatePaymentInfoChange(map);
	}
	
	public void updateDebtorCreditorIdByIdx(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updateDebtorCreditorIdByIdx(map);
	}
	public void updateDebtorCreditorIdByIdxForDaeByeon(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updateDebtorCreditorIdByIdxForDaeByeon(map);
	}
	
	public List<Map<String, Object>> selectPaymentInfoListForChange(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.selectPaymentInfoListForChange(map);
	}
	public List<Map<String, Object>> selectDaeByeonForInsertList(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.selectDaeByeonForInsertList(map);
	}
	
	public List<Map<String, Object>> selectAnotherCalInfo(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.selectAnotherCalInfo(map);
	}
	
	public Map<String, Object> selectAnotherCalDDInfo(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.selectAnotherCalDDInfo(map);
	}
	
	public Map<String, Object> selectPaymentInfoByDecideFinalId(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.selectPaymentInfoByDecideFinalId(map);
	}
	
	
	public void updatePaymentInfoListForDecideFinal(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updatePaymentInfoListForDecideFinal(map);
	}
	
	public void updatePaymentInfoForAllDeposit(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updatePaymentInfoForAllDeposit(map);
	}
	
	public Map<String, Object> selectPaymentInfoRunTotal(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.selectPaymentInfoRunTotal(map);
	}
	
	public Map<String, Object> selectPaymentInfoAllRunTotal(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.selectPaymentInfoAllRunTotal(map);
	}
	
	//public List<Map<String, Object>> selectPaymentInfoRunTotalList(Map<String, Object> map) throws Exception{
	//	return paymentInfoMapper.selectPaymentInfoRunTotalList(map);		
	//}
	
	public void updatePaymentInfoForPartDeposit(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updatePaymentInfoForPartDeposit(map);
	}
	
	public void updatePaymentInfoForAllPartDeposit(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updatePaymentInfoForAllPartDeposit(map);
	}
	
	public Map<String, Object> selectPaymentInfoForNextPayment(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.selectPaymentInfoForNextPayment(map);
	}
	
	public Map<String, Object> selectPaymentInfoForAllNextPayment(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.selectPaymentInfoForAllNextPayment(map);
	}
	
	public void updatePaymentInfoForPayment(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updatePaymentInfoForPayment(map);
	}
	
	public List<Map<String, Object>> selectPaymentInfoListByDebtorCreditorId(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.selectPaymentInfoListByDebtorCreditorId(map);
	}
	
	public Map<String, Object> selectPaymentInfoByPaymentParnerId(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.selectPaymentInfoByPaymentParnerId(map);
	}
	
	public void updatePaymentInfoByPaymentParnerIdForDriverAmount(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updatePaymentInfoByPaymentParnerIdForDriverAmount(map);
	}
	
	public void updateAllocationHcYn(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updateAllocationHcYn(map);
	}
	
	public void updatePaymentInfoByPaymentParnerIdForSelfDriverAmount(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updatePaymentInfoByPaymentParnerIdForSelfDriverAmount(map);
	}
	public List<Map<String, Object>> selectMrKIMDriverDeductList(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.selectMrKIMDriverDeductList(map);
	}
	public void updatePurchaseCansle(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updatePurchaseCansle(map);
	}
	
	public void comparePaymentInfoForDebtorCreditor(Map userMap,CarInfoVO carInfoVO,Map<String, Object> carInfoMap,List<Map<String, Object>> oldPaymentInfoList,String allocationId) throws Exception{
	
		try{
	
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("allocationId", allocationId);
			
			Map<String, Object> allocationInfo = allocationService.selectAllocation(map);
			Map<String, Object> carInfo = carInfoService.selectCarInfo(map);
			
			String allocationStatus = allocationInfo.get("allocation_status").toString();
			
			
			List<Map<String, Object>> newPaymentInfoList = this.selectPaymentInfoList(map);
			
			int count = 0;
			
			
			for(int i = 0; i < newPaymentInfoList.size(); i++) {
				
				Map<String, Object> paymentInfo = newPaymentInfoList.get(i);
				
				//매출처 결제 정보인경우 
				if(paymentInfo.get("payment_division").toString().equals("01")) {
					
					//미발행건은 건별이므로 지우고 다시 넣어도 됨....
					if(paymentInfo.get("billing_division").toString().equals("00")){
						
						//한번만 진행 하면 됨...
						if(count == 0) {
							//차변을 삭제 한다.
							if(carInfoVO.getDebtorCreditorId() != null && !carInfoVO.getDebtorCreditorId().equals("")) {
								Map<String, Object> deleteMap = new HashMap<String, Object>();
								deleteMap.put("debtorCreditorId", carInfoVO.getDebtorCreditorId());
								debtorCreditorService.deleteDebtorCreditor(deleteMap);
							}
							
							
							// 수정 되기 전에 입력 되어 있던 대변에 해당하는건이 현재 수정 하고 있는 한건이면 그 대변을 삭제한다...
							for(int m = 0; m < oldPaymentInfoList.size(); m++) {
								Map<String, Object> oldPaymentInfo = oldPaymentInfoList.get(m);
								if(oldPaymentInfo.get("debtor_creditor_id") != null && !oldPaymentInfo.get("debtor_creditor_id").toString().equals("")) {
									Map<String, Object> deleteMap = new HashMap<String, Object>();
									deleteMap.put("debtorCreditorId", oldPaymentInfo.get("debtor_creditor_id").toString());
									
									Map<String, Object> debtorCreditor = debtorCreditorService.selectDebtorCreditor(deleteMap);
									
									//대변 아이디에 해당하는 매출처 결제 정보의 수가 하나이면.....삭제... 아니면 그 금액 만큼
									List<Map<String, Object>> selectList = this.selectPaymentInfoListByDebtorCreditorId(deleteMap);
									if(selectList.size() <= 1) {
										debtorCreditorService.deleteDebtorCreditor(deleteMap);	
									}else {
										Map<String, Object> updateMap = new HashMap<String, Object>();
										updateMap.put("debtorCreditorId", oldPaymentInfo.get("debtor_creditor_id").toString());
										
										int result = 0;
										if(carInfoVO.getVatIncludeYn().equals("Y")){
											
											int total =  (int) Math.round((Integer.parseInt(paymentInfo.get("amount").toString().replaceAll(",", ""))*11/10));
											result = total;
										//부가세 별도건이면
										}else if(carInfoVO.getVatExcludeYn().equals("Y")) {
											//부가세 별도건이면	
											int total =  (int) Math.round((Integer.parseInt(paymentInfo.get("amount").toString().replaceAll(",", ""))/11)*10);
											result = total;
											
										//포함도 아니고 별도도 아니면
										}else{
											result = Integer.parseInt(paymentInfo.get("amount").toString().replaceAll(",", ""));
										}
										
										int updateAmount = Math.round(Integer.parseInt(debtorCreditor.get("total_val").toString().replaceAll(",", ""))-result);
										updateMap.put("debtorCreditorId", String.valueOf(updateAmount));
										debtorCreditorService.updateDebtorCreditorTotalVal(updateMap);
										
									}	
								}	
							}
							count++;
							
							carInfo = carInfoService.selectCarInfo(map);
							DebtorCreditorVO debtorCreditorVO = new DebtorCreditorVO();
							String summaryDt = ""; //한건인 경우 각 건의 상차지 하차지로 하여 적요에 작성 하나 한건이 아닌경우 월 단위의 적요를 작성 한다.
							//차대변 전부 입력	
							//if(carInfo.get("debtor_creditor_id") == null || carInfo.get("debtor_creditor_id").toString().equals("")) {
							//if(true) {
							if(!allocationStatus.equals("C") && !allocationStatus.equals("X") && !allocationStatus.equals("N")) {
								String debtorCreditorIdForD = "DCI"+UUID.randomUUID().toString().replaceAll("-","");
								String billPublishRequestId = "BPR"+UUID.randomUUID().toString().replaceAll("-","");
								Map<String, Object> carInfoUpdateMap = new HashMap<String, Object>();
								carInfoUpdateMap.put("allocationId", allocationId);
								carInfoUpdateMap.put("billPublishRequestId", billPublishRequestId);
								carInfoUpdateMap.put("decideStatus", "Y");
								carInfoUpdateMap.put("billingStatus", "D");
								carInfoUpdateMap.put("debtorCreditorId", debtorCreditorIdForD);
								adjustmentService.updateCarInfoForBillPublish(carInfoUpdateMap);
								
								Map<String, Object> updateMap = new HashMap<String, Object>();
								
								updateMap.put("allocationId", allocationId);
								updateMap.put("debtorCreditorId", debtorCreditorIdForD);
								carInfoVO.setDebtorCreditorId(debtorCreditorIdForD);
								
								adjustmentService.updateCarInfoDebtorCreditorId(updateMap);
								
								
								debtorCreditorVO.setCustomerId(paymentInfo.get("payment_partner_id").toString());
								debtorCreditorVO.setDebtorCreditorId(debtorCreditorIdForD);
								debtorCreditorVO.setOccurrenceDt(carInfoVO.getDepartureDt());
								 
								summaryDt ="운송료 ("+carInfoVO.getDeparture()+" - "+carInfoVO.getArrival()+")"; 
								debtorCreditorVO.setSummary(summaryDt);
								//debtorCreditorVO.setTotalVal(paymentInfo.get("amount").toString().replaceAll(",", ""));
								debtorCreditorVO.setTotalVal(carInfoVO.getPrice());
								debtorCreditorVO.setPublishYn("N");
								debtorCreditorVO.setType(BaseAppConstants.DEBTOR_CREDITOR_TYPE_D);
								debtorCreditorVO.setRegisterId(userMap.get("emp_id").toString());
								
								//차변에 입력 하고 
								debtorCreditorService.insertDebtorCreditor(debtorCreditorVO);
								carInfo = carInfoService.selectCarInfo(map);
							}
							
						}
						
						//결제가 된경우 
						if(paymentInfo.get("payment").toString().equals("Y") || paymentInfo.get("payment").toString().equals("P")) {
						
							DebtorCreditorVO debtorCreditorVO = new DebtorCreditorVO();
							String summaryDt = ""; //한건인 경우 각 건의 상차지 하차지로 하여 적요에 작성 하나 한건이 아닌경우 월 단위의 적요를 작성 한다.
							String debtorCreditorId = "DCI"+UUID.randomUUID().toString().replaceAll("-","");
							debtorCreditorVO = new DebtorCreditorVO();
							debtorCreditorVO.setCustomerId(paymentInfo.get("payment_partner_id").toString());
							debtorCreditorVO.setDebtorCreditorId(debtorCreditorId);
							debtorCreditorVO.setOccurrenceDt(paymentInfo.get("payment_dt").toString());
							  
							summaryDt = ""; //한건인 경우 각 건의 상차지 하차지로 하여 적요에 작성 하나 한건이 아닌경우 월 단위의 적요를 작성 한다. 
							summaryDt ="운송료 ("+carInfoVO.getDeparture()+" - "+carInfoVO.getArrival()+") 입금"; 
							debtorCreditorVO.setSummary(summaryDt);
							/*
							if(carInfoVO.getVatIncludeYn().equals("Y")) {
							}else if(carInfoVO.getVatExcludeYn().equals("Y")) {
							}else {
							}
							*/
							int result = 0;
							if(carInfoVO.getVatIncludeYn().equals("Y")){
								
								int total =  (int) Math.round((Integer.parseInt(paymentInfo.get("amount").toString().replaceAll(",", ""))*11/10));
								result = total;
							//부가세 별도건이면
							}else if(carInfoVO.getVatExcludeYn().equals("Y")) {
								//부가세 별도건이면	
								int total =  (int) Math.round((Integer.parseInt(paymentInfo.get("amount").toString().replaceAll(",", ""))/11)*10);
								result = total;
								
							//포함도 아니고 별도도 아니면
							}else{
								result = Integer.parseInt(paymentInfo.get("amount").toString().replaceAll(",", ""));
							}
							
							debtorCreditorVO.setTotalVal(String.valueOf(result));
							
							//debtorCreditorVO.setTotalVal(paymentInfo.get("amount").toString().replaceAll(",", ""));
							//debtorCreditorVO.setTotalVal(carInfoVO.getPrice());
							debtorCreditorVO.setPublishYn("N");
							debtorCreditorVO.setType(BaseAppConstants.DEBTOR_CREDITOR_TYPE_C);
							debtorCreditorVO.setDebtorId(carInfo.get("debtor_creditor_id").toString());
							debtorCreditorVO.setRegisterId(userMap.get("emp_id").toString());
							
							//대변에 입력 하고 
							debtorCreditorService.insertDebtorCreditor(debtorCreditorVO);	
							
							Map<String, Object> updateMap = new HashMap<String, Object>();
							updateMap.put("debtorCreditorId", debtorCreditorId);
							updateMap.put("allocationId", allocationId);
							updateMap.put("idx", paymentInfo.get("idx").toString());
							updateMap.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자

							this.updateDebtorCreditorIdByIdx(updateMap);
							
							
						}else {
							
							//결제가 되지 않은경우 
							//차변이 있는경우 
							//Map<String, Object> carInfoMap = carInfoService.selectCarInfo(map);
							//String debtorCreditorIdForC = carInfoMap.get("debtor_creditor_id").toString();
							carInfo = carInfoService.selectCarInfo(map);
							DebtorCreditorVO debtorCreditorVO = new DebtorCreditorVO();
							String summaryDt = ""; //한건인 경우 각 건의 상차지 하차지로 하여 적요에 작성 하나 한건이 아닌경우 월 단위의 적요를 작성 한다.
							//차변이 입력 되어 있지 않은경우 차변을 입력 한다......	
							
							Map<String,Object> debtorCreditor = null;
							if(carInfo.get("debtor_creditor_id") != null && !carInfo.get("debtor_creditor_id").toString().equals("")) {
								Map<String, Object> selectMap = new HashMap<String, Object>();
								selectMap.put("debtorCreditorId", carInfo.get("debtor_creditor_id").toString());
								debtorCreditor = debtorCreditorService.selectDebtorCreditor(selectMap);
							}
							
							if(!allocationStatus.equals("C") && !allocationStatus.equals("X") && !allocationStatus.equals("N")) {
								if(debtorCreditor == null || debtorCreditor.get("debtor_creditor_id").toString().equals("")) {
									String debtorCreditorIdForD = "DCI"+UUID.randomUUID().toString().replaceAll("-","");
									String billPublishRequestId = "BPR"+UUID.randomUUID().toString().replaceAll("-","");
									Map<String, Object> carInfoUpdateMap = new HashMap<String, Object>();
									carInfoUpdateMap.put("allocationId", allocationId);
									carInfoUpdateMap.put("billPublishRequestId", billPublishRequestId);
									carInfoUpdateMap.put("decideStatus", "Y");
									carInfoUpdateMap.put("billingStatus", "D");
									carInfoUpdateMap.put("debtorCreditorId", debtorCreditorIdForD);
									adjustmentService.updateCarInfoForBillPublish(carInfoUpdateMap);
									
									Map<String, Object> updateMap = new HashMap<String, Object>();
									
									updateMap.put("allocationId", allocationId);
									updateMap.put("debtorCreditorId", debtorCreditorIdForD);
									carInfoVO.setDebtorCreditorId(debtorCreditorIdForD);
									
									adjustmentService.updateCarInfoDebtorCreditorId(updateMap);
									
									
									debtorCreditorVO.setCustomerId(paymentInfo.get("payment_partner_id").toString());
									debtorCreditorVO.setDebtorCreditorId(debtorCreditorIdForD);
									debtorCreditorVO.setOccurrenceDt(carInfoVO.getDepartureDt());
									 
									summaryDt ="운송료 ("+carInfoVO.getDeparture()+" - "+carInfoVO.getArrival()+")"; 
									debtorCreditorVO.setSummary(summaryDt);
									//debtorCreditorVO.setTotalVal(paymentInfo.get("amount").toString().replaceAll(",", ""));
									debtorCreditorVO.setTotalVal(carInfoVO.getPrice());
									debtorCreditorVO.setPublishYn("N");
									debtorCreditorVO.setType(BaseAppConstants.DEBTOR_CREDITOR_TYPE_D);
									debtorCreditorVO.setRegisterId(userMap.get("emp_id").toString());
									
									//차변에 입력 하고 
									debtorCreditorService.insertDebtorCreditor(debtorCreditorVO);
									carInfo = carInfoService.selectCarInfo(map);
								}
							
						}
							
							
							
							
						}
						
					//발행건인경우 기존의 대변을 삭제 해도 되는지 확인 하고 진행 한다.
					}else if(!paymentInfo.get("billing_division").toString().equals("00")) {
						
						boolean deleted = false;
						
						
						//발행건인경우 
						
						
						//세금계산서가 발행 되어 있던 경우 
						if(carInfoVO.getBillPublishRequestId() != null && !carInfoVO.getBillPublishRequestId().equals("") && carInfoMap.get("billing_status").toString().equals("Y")) {
							
							carInfo = carInfoService.selectCarInfo(map);
							Map<String, Object> selectMap = new HashMap<String, Object>();
							selectMap.put("debtorCreditorId", carInfo.get("debtor_creditor_id").toString());
							Map<String, Object> debtorCreditor = debtorCreditorService.selectDebtorCreditor(selectMap);
							
							//차변이 있는경우
							if(debtorCreditor != null &&  carInfo.get("debtor_creditor_id") != null &&  !carInfo.get("debtor_creditor_id").toString().equals("")) {
								// 금액 확인 정도??
								
								
								
								
							}else {
								
								//세금계산서가 발행 되었는데 차변이 없는경우는 문제네...... 기존에 발행한 데이터를 기준으로 차변을 다시 생성한다..... 
								String debtorCreditorIdForD = "";
								
								if(carInfo.get("debtor_creditor_id") != null &&  !carInfo.get("debtor_creditor_id").toString().equals("")) {
									debtorCreditorIdForD = carInfo.get("debtor_creditor_id").toString();
								}else {
									debtorCreditorIdForD = "DCI"+UUID.randomUUID().toString().replaceAll("-","");
								}

								selectMap.put("billPublishRequestId", carInfoVO.getBillPublishRequestId());
								Map<String, Object> billPublishRequest = billPublishRequestService.selectBillPublishRequest(selectMap);
								//bill_publish_request_id 가 같은 배차건에 대해 차변 아이디를 업데이트 해 준다...
								selectMap.put("debtorCreditorId", debtorCreditorIdForD);
					            selectMap.put("billingStatus", "Y");
					            selectMap.put("modId", userMap.get("emp_id").toString()!=null && !userMap.get("emp_id").toString().equals("")?userMap.get("emp_id").toString():""); //수정자
					            selectMap.put("logType", "U");
					            carInfoService.insertCarInfoLogByMap(selectMap); //로그기록
								
								//차변이 없는경우는  변경된 경우이므로 차변을 다시 넣어 준다....	
								DebtorCreditorVO debtorCreditorVO = new DebtorCreditorVO();
								String summaryDt = ""; //한건인 경우 각 건의 상차지 하차지로 하여 적요에 작성 하나 한건이 아닌경우 월 단위의 적요를 작성 한다.
								//차변이 입력 되어 있지 않은경우 차변을 입력 한다......	
								//세금계산서 발행건의 차변은 세금계산서 발행 요청 후 발행된 건에 대하여
								
								if(carInfo.get("debtor_creditor_id") == null || carInfo.get("debtor_creditor_id").toString().equals("") || debtorCreditor == null) {
									
									Map<String, Object> carInfoUpdateMap = new HashMap<String, Object>();
									carInfoUpdateMap.put("allocationId", allocationId);
									carInfoUpdateMap.put("billPublishRequestId", carInfoVO.getBillPublishRequestId());
									carInfoUpdateMap.put("decideStatus", "Y");
									carInfoUpdateMap.put("billingStatus", "Y");
									carInfoUpdateMap.put("debtorCreditorId", debtorCreditorIdForD);
									adjustmentService.updateCarInfoForBillPublish(carInfoUpdateMap);
									
									Map<String, Object> updateMap = new HashMap<String, Object>();
									
									updateMap.put("allocationId", allocationId);
									updateMap.put("debtorCreditorId", debtorCreditorIdForD);
									carInfoVO.setDebtorCreditorId(debtorCreditorIdForD);
									
									adjustmentService.updateCarInfoDebtorCreditorId(updateMap);
									
									debtorCreditorVO.setCustomerId(paymentInfo.get("payment_partner_id").toString());
									debtorCreditorVO.setDebtorCreditorId(debtorCreditorIdForD);
									debtorCreditorVO.setOccurrenceDt(carInfoVO.getDepartureDt());
									 
									summaryDt ="운송료 ("+carInfoVO.getDeparture()+" - "+carInfoVO.getArrival()+")"; 
									debtorCreditorVO.setSummary(billPublishRequest.get("summary").toString());
									//debtorCreditorVO.setTotalVal(paymentInfo.get("amount").toString().replaceAll(",", ""));
									debtorCreditorVO.setTotalVal(billPublishRequest.get("total").toString().replaceAll(",", ""));
									debtorCreditorVO.setPublishYn("Y");
									debtorCreditorVO.setType(BaseAppConstants.DEBTOR_CREDITOR_TYPE_D);
									debtorCreditorVO.setRegisterId(userMap.get("emp_id").toString());
									
									//차변에 입력 하고 
									debtorCreditorService.insertDebtorCreditor(debtorCreditorVO);
									carInfo = carInfoService.selectCarInfo(map);
								}
								
								
							}
							
						//미발행건 이었던 경우 
						}else if(carInfoVO.getBillPublishRequestId() != null && !carInfoVO.getBillPublishRequestId().equals("") && carInfoMap.get("billing_status").toString().equals("D")) {
							
							//현금건 매출로 넘어가 있는건을 확정으로 돌려 놓는다. 
							Map<String, Object> carInfoUpdateMap = new HashMap<String, Object>();
							carInfoUpdateMap.put("allocationId", allocationId);
							carInfoUpdateMap.put("billPublishRequestId", "");
							carInfoUpdateMap.put("decideStatus", "Y");
							carInfoUpdateMap.put("billingStatus", "N");
							carInfoUpdateMap.put("debtorCreditorId", "");
							adjustmentService.updateCarInfoForBillPublish(carInfoUpdateMap);
						
							for(int m = 0; m < oldPaymentInfoList.size(); m++) {
								Map<String, Object> oldPaymentInfo = oldPaymentInfoList.get(m);
								//현장수금건에서 세금계산서 발행건으로 변경 된 경우 차변과 대변을 삭제 한다.
								if(oldPaymentInfo.get("payment_division").equals("01") && oldPaymentInfo.get("payment_kind").equals("DD")) {
									//대변삭제
									if(oldPaymentInfo.get("debtor_creditor_id") != null && !oldPaymentInfo.get("debtor_creditor_id").toString().equals("")) {
										Map<String, Object> deleteMap = new HashMap<String, Object>();
										deleteMap.put("debtorCreditorId", oldPaymentInfo.get("debtor_creditor_id").toString());
										//대변 아이디에 해당하는 매출처 결제 정보의 수가 하나이면.....삭제... 아니면 그 금액 만큼
										List<Map<String, Object>> selectList = this.selectPaymentInfoListByDebtorCreditorId(deleteMap);
										if(selectList.size() <= 1) {
											debtorCreditorService.deleteDebtorCreditor(deleteMap);	
										}	
									}
									//차변삭제
									if(carInfoMap.get("debtor_creditor_id") != null && !carInfoMap.get("debtor_creditor_id").toString().equals("")) {
										Map<String, Object> deleteMap = new HashMap<String, Object>();
										deleteMap.put("debtorCreditorId", carInfoMap.get("debtor_creditor_id").toString());
										//대변 아이디에 해당하는 매출처 결제 정보의 수가 하나이면.....삭제... 아니면 그 금액 만큼
										List<Map<String, Object>> selectList = this.selectPaymentInfoListByDebtorCreditorId(deleteMap);
										if(selectList.size() <= 1) {
											debtorCreditorService.deleteDebtorCreditor(deleteMap);	
										}	
									}

								}
									
							}
							
						}
						
						//결제가 된경우 
						if(paymentInfo.get("payment").toString().equals("Y") || paymentInfo.get("payment").toString().equals("P")) {
							
							//차변이 있는경우 
							//Map<String, Object> carInfoMap = carInfoService.selectCarInfo(map);
							//String debtorCreditorIdForC = carInfoMap.get("debtor_creditor_id").toString();
							carInfo = carInfoService.selectCarInfo(map);
							DebtorCreditorVO debtorCreditorVO = new DebtorCreditorVO();
							String summaryDt = ""; //한건인 경우 각 건의 상차지 하차지로 하여 적요에 작성 하나 한건이 아닌경우 월 단위의 적요를 작성 한다.
							//차변이 입력 되어 있지 않은경우 차변을 입력 한다......	
							//세금계산서 발행건의 차변은 세금계산서 발행 요청 후 발행된 건에 대하여
							
							//세금계산서건이 결제 또는 부분 결제가 되어있고 입금일이 작성 되어 있는데 발행 요청 또는 발행이 되지 않은경우 세금계산서 발행 요청을 진행 한다.200609 
							if(!paymentInfo.get("payment_dt").toString().equals("") && carInfo.get("billing_status").toString().equals("N")) {
								
								String debtorCreditorIdForD = "DCI"+UUID.randomUUID().toString().replaceAll("-","");
								String billPublishRequestId = "BPR"+UUID.randomUUID().toString().replaceAll("-","");
								Map<String, Object> carInfoUpdateMap = new HashMap<String, Object>();
								carInfoUpdateMap.put("allocationId", allocationId);
								carInfoUpdateMap.put("billPublishRequestId", billPublishRequestId);
								carInfoUpdateMap.put("decideStatus", "Y");
								carInfoUpdateMap.put("billingStatus", "R");
								carInfoUpdateMap.put("debtorCreditorId", debtorCreditorIdForD);
								adjustmentService.updateCarInfoForBillPublish(carInfoUpdateMap);
								
								Map<String, Object> updateMap = new HashMap<String, Object>();
								
								updateMap.put("allocationId", allocationId);
								updateMap.put("debtorCreditorId", debtorCreditorIdForD);
								carInfoVO.setDebtorCreditorId(debtorCreditorIdForD);
								
								adjustmentService.updateCarInfoDebtorCreditorId(updateMap);
								
								debtorCreditorVO.setCustomerId(paymentInfo.get("payment_partner_id").toString());
								debtorCreditorVO.setDebtorCreditorId(debtorCreditorIdForD);
								debtorCreditorVO.setOccurrenceDt(carInfoVO.getDepartureDt());
								 
								summaryDt ="운송료 ("+carInfoVO.getDeparture()+" - "+carInfoVO.getArrival()+")"; 
								debtorCreditorVO.setSummary(summaryDt);
								//debtorCreditorVO.setTotalVal(paymentInfo.get("amount").toString().replaceAll(",", ""));
								debtorCreditorVO.setTotalVal(carInfoVO.getPrice());
								debtorCreditorVO.setPublishYn("Y");
								debtorCreditorVO.setType(BaseAppConstants.DEBTOR_CREDITOR_TYPE_D);
								debtorCreditorVO.setRegisterId(userMap.get("emp_id").toString());
								
								//차변에 입력 하고 
								debtorCreditorService.insertDebtorCreditor(debtorCreditorVO);
								carInfo = carInfoService.selectCarInfo(map);
								
								//세금계산서 발행 요청.
								BillPublishRequestVO billPublishRequestVO = new BillPublishRequestVO();				
								billPublishRequestVO.setAmount(carInfoVO.getSalesTotal());
								billPublishRequestVO.setVat(carInfoVO.getVat());
								billPublishRequestVO.setTotal(carInfoVO.getPrice());
								billPublishRequestVO.setBillPublishRequestId(billPublishRequestId);
								billPublishRequestVO.setCustomerId(allocationInfo.get("customer_id").toString());
								billPublishRequestVO.setCustomerName(allocationInfo.get("customer_name").toString());
								billPublishRequestVO.setComment("");
								billPublishRequestVO.setPublishRequestStatus("R");
								billPublishRequestVO.setRequestDt(carInfoVO.getDepartureDt());
								billPublishRequestVO.setRequesterId(userMap.get("emp_id").toString());
								billPublishRequestVO.setRequesterName(userMap.get("emp_name").toString());
								billPublishRequestVO.setSummary(summaryDt);
								billPublishRequestVO.setCompanyId(userMap.get("company_id").toString());
								billPublishRequestService.insertBillPublishRequest(billPublishRequestVO);
								
								
							}
							
							/*
							if(carInfo.get("debtor_creditor_id") == null || carInfo.get("debtor_creditor_id").toString().equals("")) {
								String debtorCreditorIdForD = "DCI"+UUID.randomUUID().toString().replaceAll("-","");
								String billPublishRequestId = "BPR"+UUID.randomUUID().toString().replaceAll("-","");
								Map<String, Object> carInfoUpdateMap = new HashMap<String, Object>();
								carInfoUpdateMap.put("allocationId", allocationId);
								carInfoUpdateMap.put("billPublishRequestId", billPublishRequestId);
								carInfoUpdateMap.put("decideStatus", "Y");
								carInfoUpdateMap.put("billingStatus", "Y");
								carInfoUpdateMap.put("debtorCreditorId", debtorCreditorIdForD);
								adjustmentService.updateCarInfoForBillPublish(carInfoUpdateMap);
								
								Map<String, Object> updateMap = new HashMap<String, Object>();
								
								updateMap.put("allocationId", allocationId);
								updateMap.put("debtorCreditorId", debtorCreditorIdForD);
								carInfoVO.setDebtorCreditorId(debtorCreditorIdForD);
								
								adjustmentService.updateCarInfoDebtorCreditorId(updateMap);
								
								
								debtorCreditorVO.setCustomerId(paymentInfo.get("payment_partner_id").toString());
								debtorCreditorVO.setDebtorCreditorId(debtorCreditorIdForD);
								debtorCreditorVO.setOccurrenceDt(carInfoVO.getDepartureDt());
								 
								summaryDt ="운송료 ("+carInfoVO.getDeparture()+" - "+carInfoVO.getArrival()+")"; 
								debtorCreditorVO.setSummary(summaryDt);
								//debtorCreditorVO.setTotalVal(paymentInfo.get("amount").toString().replaceAll(",", ""));
								debtorCreditorVO.setTotalVal(carInfoVO.getPrice());
								debtorCreditorVO.setPublishYn("Y");
								debtorCreditorVO.setType(BaseAppConstants.DEBTOR_CREDITOR_TYPE_D);
								debtorCreditorVO.setRegisterId(userMap.get("emp_id").toString());
								
								//차변에 입력 하고 
								debtorCreditorService.insertDebtorCreditor(debtorCreditorVO);
								carInfo = carInfoService.selectCarInfo(map);
							}
							*/
							
							//입력된 대변이 있으면 대변을 삭제 후 다시 입력한다...
							if(paymentInfo.get("debtor_creditor_id") != null && !paymentInfo.get("debtor_creditor_id").toString().equals("")) {
								Map<String, Object> deleteMap = new HashMap<String, Object>();
								deleteMap.put("debtorCreditorId", paymentInfo.get("debtor_creditor_id").toString());
								
								List<Map<String, Object>> paymentInfoListByDebtorCreditorId = this.selectPaymentInfoListByDebtorCreditorId(deleteMap);
								//해당하는 대변 아이디에 포함된 결제정보가 한건인 경우에만 삭제 한다........ 
								if(paymentInfoListByDebtorCreditorId.size() == 1) {
									debtorCreditorService.deleteDebtorCreditor(deleteMap);	
									deleted = true;
									summaryDt = "";
									
									debtorCreditorVO = new DebtorCreditorVO();
								
									String debtorCreditorId = "DCI"+UUID.randomUUID().toString().replaceAll("-","");
									debtorCreditorVO = new DebtorCreditorVO();
									debtorCreditorVO.setCustomerId(paymentInfo.get("payment_partner_id").toString());
									debtorCreditorVO.setDebtorCreditorId(debtorCreditorId);
									debtorCreditorVO.setOccurrenceDt(paymentInfo.get("payment_dt").toString());
									  
									summaryDt = ""; //한건인 경우 각 건의 상차지 하차지로 하여 적요에 작성 하나 한건이 아닌경우 월 단위의 적요를 작성 한다. 
									summaryDt ="운송료 ("+carInfoVO.getDeparture()+" - "+carInfoVO.getArrival()+") 입금"; 
									debtorCreditorVO.setSummary(summaryDt);
									
									
									
									debtorCreditorVO.setTotalVal(paymentInfo.get("amount").toString().replaceAll(",", ""));
									
									
									debtorCreditorVO.setPublishYn("Y");
									debtorCreditorVO.setType(BaseAppConstants.DEBTOR_CREDITOR_TYPE_C);
									debtorCreditorVO.setDebtorId(carInfo.get("debtor_creditor_id").toString());
									debtorCreditorVO.setRegisterId(userMap.get("emp_id").toString());
									
									if(deleted) {
										//대변에 입력 하고 
										debtorCreditorService.insertDebtorCreditor(debtorCreditorVO);	
										
										Map<String, Object> updateMap = new HashMap<String, Object>();
										updateMap.put("debtorCreditorId", debtorCreditorId);
										updateMap.put("allocationId", allocationId);
										updateMap.put("idx", paymentInfo.get("idx").toString());
										
										this.updateDebtorCreditorIdByIdx(updateMap);	
										
									}
									
									
									
								}else {
									
									
									
									
								}
								
								//입력된 대변이 없으면 대변을 등록한다...
							}else {
								
								summaryDt = "";
								
								debtorCreditorVO = new DebtorCreditorVO();
							
								String debtorCreditorId = "DCI"+UUID.randomUUID().toString().replaceAll("-","");
								debtorCreditorVO = new DebtorCreditorVO();
								debtorCreditorVO.setCustomerId(paymentInfo.get("payment_partner_id").toString());
								debtorCreditorVO.setDebtorCreditorId(debtorCreditorId);
								debtorCreditorVO.setOccurrenceDt(paymentInfo.get("payment_dt").toString());
								  
								summaryDt = ""; //한건인 경우 각 건의 상차지 하차지로 하여 적요에 작성 하나 한건이 아닌경우 월 단위의 적요를 작성 한다. 
								summaryDt ="운송료 ("+carInfoVO.getDeparture()+" - "+carInfoVO.getArrival()+") 입금"; 
								debtorCreditorVO.setSummary(summaryDt);
								
								if(carInfoVO.getVatIncludeYn().equals("Y")){
									
									int total = Integer.parseInt(paymentInfo.get("amount").toString().replaceAll(",", ""));
									float supply = (float) ((float)Float.parseFloat(paymentInfo.get("amount").toString().replaceAll(",", ""))/1.1);
									debtorCreditorVO.setTotalVal(String.valueOf(supply));
								//부가세 별도건이면
								}else if(carInfoVO.getVatExcludeYn().equals("Y")) {
									int vat = Integer.parseInt(paymentInfo.get("amount").toString().replaceAll(",", ""))/10;
									int total = Integer.parseInt(paymentInfo.get("amount").toString().replaceAll(",", ""))+vat;
									debtorCreditorVO.setTotalVal(String.valueOf(total));
									
								//포함도 아니고 별도도 아니면
								}else{
									debtorCreditorVO.setTotalVal(paymentInfo.get("amount").toString().replaceAll(",", ""));
								}
								
								debtorCreditorVO.setPublishYn("Y");
								debtorCreditorVO.setType(BaseAppConstants.DEBTOR_CREDITOR_TYPE_C);
								debtorCreditorVO.setDebtorId(carInfo.get("debtor_creditor_id").toString());
								debtorCreditorVO.setRegisterId(userMap.get("emp_id").toString());
								
								//대변에 입력 하고 
								debtorCreditorService.insertDebtorCreditor(debtorCreditorVO);	
								
								Map<String, Object> updateMap = new HashMap<String, Object>();
								updateMap.put("debtorCreditorId", debtorCreditorId);
								updateMap.put("allocationId", allocationId);
								updateMap.put("idx", paymentInfo.get("idx").toString());
								
								this.updateDebtorCreditorIdByIdx(updateMap);	
								
							}
							
							
						}else {
							//결제가 되지 않은경우 
							//차변이 있는경우 
							//Map<String, Object> carInfoMap = carInfoService.selectCarInfo(map);
							//String debtorCreditorIdForC = carInfoMap.get("debtor_creditor_id").toString();
							carInfo = carInfoService.selectCarInfo(map);
							DebtorCreditorVO debtorCreditorVO = new DebtorCreditorVO();
							String summaryDt = ""; //한건인 경우 각 건의 상차지 하차지로 하여 적요에 작성 하나 한건이 아닌경우 월 단위의 적요를 작성 한다.
							//차변이 입력 되어 있지 않은경우 차변을 입력 한다......	
							
							/*
							if(carInfo.get("debtor_creditor_id") == null || carInfo.get("debtor_creditor_id").toString().equals("")) {
								String debtorCreditorIdForD = "DCI"+UUID.randomUUID().toString().replaceAll("-","");
								String billPublishRequestId = "BPR"+UUID.randomUUID().toString().replaceAll("-","");
								Map<String, Object> carInfoUpdateMap = new HashMap<String, Object>();
								carInfoUpdateMap.put("allocationId", allocationId);
								carInfoUpdateMap.put("billPublishRequestId", billPublishRequestId);
								carInfoUpdateMap.put("decideStatus", "Y");
								carInfoUpdateMap.put("billingStatus", "Y");
								carInfoUpdateMap.put("debtorCreditorId", debtorCreditorIdForD);
								adjustmentService.updateCarInfoForBillPublish(carInfoUpdateMap);
								
								Map<String, Object> updateMap = new HashMap<String, Object>();
								
								updateMap.put("allocationId", allocationId);
								updateMap.put("debtorCreditorId", debtorCreditorIdForD);
								carInfoVO.setDebtorCreditorId(debtorCreditorIdForD);
								
								adjustmentService.updateCarInfoDebtorCreditorId(updateMap);
								
								
								debtorCreditorVO.setCustomerId(paymentInfo.get("payment_partner_id").toString());
								debtorCreditorVO.setDebtorCreditorId(debtorCreditorIdForD);
								debtorCreditorVO.setOccurrenceDt(carInfoVO.getDepartureDt());
								 
								summaryDt ="운송료 ("+carInfoVO.getDeparture()+" - "+carInfoVO.getArrival()+")"; 
								debtorCreditorVO.setSummary(summaryDt);
								//debtorCreditorVO.setTotalVal(paymentInfo.get("amount").toString().replaceAll(",", ""));
								debtorCreditorVO.setTotalVal(carInfoVO.getPrice());
								debtorCreditorVO.setPublishYn("Y");
								debtorCreditorVO.setType(BaseAppConstants.DEBTOR_CREDITOR_TYPE_D);
								debtorCreditorVO.setRegisterId(userMap.get("emp_id").toString());
								
								//차변에 입력 하고 
								debtorCreditorService.insertDebtorCreditor(debtorCreditorVO);
								carInfo = carInfoService.selectCarInfo(map);
							}
							*/
							
							
							
							
							
						}
						
						
						
					}
					
					
					
					
				}
				
				
				
			}
			
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	
	public List<Map<String, Object>> selectPaymentInfoForMod(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.selectPaymentInfoForMod(map);
	}
	
	public List<Map<String, Object>> selectPaymentInfoForCollectManage(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.selectPaymentInfoForCollectManage(map);
	}
	
	public int selectPurchaseListCount(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.selectPurchaseListCount(map);
	}
	
	public List<Map<String, Object>> selectPurchaseList(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.selectPurchaseList(map);
	}
	
	public List<Map<String, Object>> selectPurchaseListByPaymentPartnerId(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.selectPurchaseListByPaymentPartnerId(map);
	}
	
	public Map<String, Object> selectWithdrawRunTotal(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.selectWithdrawRunTotal(map);
	}
	
	public Map<String, Object> selectWithdrawAllRunTotal(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.selectWithdrawAllRunTotal(map);
	}
	
	public void updatePaymentInfoForAllPartWithdraw(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updatePaymentInfoForAllPartWithdraw(map);
	}
	
	public Map<String, Object> selectPaymentInfoForAllNextWithdraw(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.selectPaymentInfoForAllNextWithdraw(map);
	}
	
	public List<Map<String, Object>> selectPaymentInfoListForWithdraw(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.selectPaymentInfoListForWithdraw(map);
	}
	
	public void updatePaymentInfoForWithdraw(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updatePaymentInfoForWithdraw(map);
	}
	
	public void insertWithdraw(WithdrawVO withdrawVO) throws Exception{
		paymentInfoMapper.insertWithdraw(withdrawVO);
	}
	
	public Map<String, Object> selectPaymentInfoForWithdraw(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.selectPaymentInfoForWithdraw(map);
	}
	
	public void updatePaymentInfoForAllWithdraw(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updatePaymentInfoForAllWithdraw(map);
	}
	
	public Map<String, Object> selectPaymentInfoWithdrawRunTotal(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.selectPaymentInfoWithdrawRunTotal(map);
	}
	
	public void updatePaymentInfoForPartWithdraw(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updatePaymentInfoForPartWithdraw(map);
	}
	
	public Map<String, Object> selectPaymentInfoForPartNextWithdraw(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.selectPaymentInfoForPartNextWithdraw(map);
	}
	
	public void updatePaymentInfoForDDFinish(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updatePaymentInfoForDDFinish(map);
	}
	
	public void updatePaymentInfoDebtorCreditorId(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updatePaymentInfoDebtorCreditorId(map);
	}
	
	public void updatePaymentInfoDecideStatusByPaymentPartnerId(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updatePaymentInfoDecideStatusByPaymentPartnerId(map);
	}
	
	public List<Map<String, Object>> selectPaymentInfoDecideStatusByPaymentPartnerId(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.selectPaymentInfoDecideStatusByPaymentPartnerId(map);
	}
	
	
	public void updatePaymentInfoForDDFinishBackup(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updatePaymentInfoForDDFinishBackup(map);
	}
}
