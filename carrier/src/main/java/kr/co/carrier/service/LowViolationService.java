package kr.co.carrier.service;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.LowViolationVO;

public interface LowViolationService {

	
	
	public Map<String, Object> selectLowViolation(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectDriverList(Map<String, Object> map) throws Exception;
	public int selectDriverListCount(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectLowViolationList(Map<String, Object> map) throws Exception;
	public int selectLowViolationListCount(Map<String, Object> map) throws Exception;
	public int insertLowViolation(LowViolationVO lowViolationVO) throws Exception;
	public void deleteLowViolation(Map<String, Object> map) throws Exception;
	public void updateLowViolation(LowViolationVO lowViolationVO) throws Exception;
	public int selectLowViolationListCountByDriverId(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectLowViolationListByDriverId(Map<String, Object> map) throws Exception;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
