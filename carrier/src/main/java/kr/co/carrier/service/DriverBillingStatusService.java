package kr.co.carrier.service;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.DriverBillingStatusVO;

public interface DriverBillingStatusService {

	
	public Map<String, Object> selectDriverBillingStatus(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectDriverBillingStatusList(Map<String, Object> map) throws Exception;
	public int selectDriverBillingStatusListCount(Map<String, Object> map) throws Exception;
	public int insertDriverBillingStatus(DriverBillingStatusVO driverBillingStatusVO) throws Exception;
	public void deleteDriverBillingStatus(Map<String, Object> map) throws Exception;
	public void deleteDriverBillingStatusByDriverIdAndDecideMonth(Map<String, Object> map) throws Exception;
	
	
	
	
	
	
	
	
	
	
	
	
	
}
