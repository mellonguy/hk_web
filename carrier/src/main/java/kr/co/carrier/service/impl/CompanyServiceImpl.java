package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.CompanyMapper;
import kr.co.carrier.service.CompanyService;
import kr.co.carrier.vo.CompanyVO;

@Service("companyService")
public class CompanyServiceImpl implements CompanyService{

	
	@Resource(name="companyMapper")
	private CompanyMapper companyMapper;
	
	public Map<String, Object> selectCompany(Map<String, Object> map) throws Exception{
		return companyMapper.selectCompany(map);
	}
	
	public List<Map<String, Object>> selectCompanyList(Map<String, Object> map) throws Exception{
		return companyMapper.selectCompanyList(map);
	}
	
	public int selectCompanyListCount(Map<String, Object> map) throws Exception{
		return companyMapper.selectCompanyListCount(map);
	}
	
	public int insertCompany(CompanyVO companyVO) throws Exception{
		return companyMapper.insertCompany(companyVO);
	}
	
	public void updateCompany(CompanyVO companyVO) throws Exception{
		companyMapper.updateCompany(companyVO);
	}
	
	public void deleteCompany(CompanyVO companyVO) throws Exception{
		companyMapper.deleteCompany(companyVO);
	}
	
	
}
