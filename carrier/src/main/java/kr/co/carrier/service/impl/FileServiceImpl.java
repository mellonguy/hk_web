package kr.co.carrier.service.impl;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import kr.co.carrier.mapper.FileMapper;
import kr.co.carrier.service.FileService;
import kr.co.carrier.service.FileUploadService;
import kr.co.carrier.utils.MultipartUtility;
import kr.co.carrier.utils.ResultApi;
import kr.co.carrier.vo.FileVO;

@Service("fileService")
public class FileServiceImpl implements FileService{


	@Resource(name="fileMapper")
	private FileMapper fileMapper;
	
	@Autowired
	private FileUploadService fileUploadService;
	
	
	public ResultApi insertFile(String rootDir, String subDir,HttpServletRequest request) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
	    	List<MultipartFile> fileList = multipartRequest.getFiles("bbsFile");
			
	    	if(fileList != null && fileList.size() > 0){
	    		for(MultipartFile mFile : fileList){	
	    			if(mFile != null && mFile.getSize() > 0){
	    				FileVO fileVO = new FileVO();
	    				Map<String, Object> fileMap = fileUploadService.upload(rootDir, subDir, mFile);
	    				
	    				fileVO.setContentId((String)request.getParameter("bbsId"));
	    				
	    				/*private String useYn;
	    				private String imgYn;
	    				private String filePath;
	    				private String fileNm;
	    				private String fileId;
	    				private String bbsType;
	    				private String bbsId;*/
	    				
	    				
	    				
	    				/*resultMap.put("FILE_SIZE", fileSize);
	    				resultMap.put("ORG_NAME", fileName);
	    				resultMap.put("SAVE_NAME", saveName);*/
	    				
	    				
	    			}
	    		}
	    	}
			
			//fileMapper.insertFile(fileVO);
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return result; 
		
	}
	public ResultApi insertFile(String rootDir, String subDir,String contentId,HttpServletRequest request) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
	    	List<MultipartFile> fileList = multipartRequest.getFiles("bbsFile");
	    	List<MultipartFile> driverFileList = multipartRequest.getFiles("driverFile");
	    	List<MultipartFile> driverSealFileList = multipartRequest.getFiles("driverSealFile");
	    	if(fileList != null && fileList.size() > 0){
	    		result.setResultCode("0000");
	    		for(MultipartFile mFile : fileList){	
	    			if(mFile != null && mFile.getSize() > 0){
	    				FileVO fileVO = new FileVO();
	    				Map<String, Object> fileMap = fileUploadService.upload(rootDir, subDir, mFile);
	    				
	    				fileVO.setContentId(contentId);
	    				fileVO.setCategoryType(subDir);
	    				fileVO.setFileId("FLE"+UUID.randomUUID().toString().replaceAll("-", ""));
	    				fileVO.setFileNm(fileMap.get("ORG_NAME").toString());
	    				fileVO.setFilePath(fileMap.get("SAVE_NAME").toString());
	    				fileMapper.insertFile(fileVO);		
	    				
	    			}
	    		}
	    	}else{
	    		result.setResultCode("1111"); 	//file 없음
	    	}
	    	
	    	if(driverFileList != null && driverFileList.size() > 0) {
	    		result.setResultCode("0000");
	    		for(MultipartFile mFile : driverFileList){	
	    			if(mFile != null && mFile.getSize() > 0){
	    				FileVO fileVO = new FileVO();
	    				Map<String, Object> fileMap = fileUploadService.upload(rootDir, subDir, mFile);
	    				fileVO.setContentId(contentId);
	    				fileVO.setCategoryType("driverPic");
	    				fileVO.setFileId("FLE"+UUID.randomUUID().toString().replaceAll("-", ""));
	    				fileVO.setFileNm(fileMap.get("ORG_NAME").toString());
	    				fileVO.setFilePath(fileMap.get("SAVE_NAME").toString());
	    				fileMapper.insertFile(fileVO);		
	    			}
	    		}
	    	}else{
	    		result.setResultCode("1111"); 	//file 없음
	    	}
	    	
	    	
	    	if(driverSealFileList != null && driverSealFileList.size() > 0) {
	    		result.setResultCode("0000");
	    		for(MultipartFile mFile : driverSealFileList){	
	    			if(mFile != null && mFile.getSize() > 0){
	    				FileVO fileVO = new FileVO();
	    				Map<String, Object> fileMap = fileUploadService.upload(rootDir, subDir, mFile);
	    				fileVO.setContentId(contentId);
	    				fileVO.setCategoryType("driverSeal");
	    				fileVO.setFileId("FLE"+UUID.randomUUID().toString().replaceAll("-", ""));
	    				fileVO.setFileNm(fileMap.get("ORG_NAME").toString());
	    				fileVO.setFilePath(fileMap.get("SAVE_NAME").toString());
	    				fileMapper.insertFile(fileVO);		
	    			}
	    		}
	    	}else{
	    		result.setResultCode("1111"); 	//file 없음
	    	}
	    	
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return result; 
		
		
		
	}
	
	public List<Map<String, Object>> selectFileList(Map<String, Object> map) throws Exception{
		return fileMapper.selectFileList(map);
		
	}
	
	public void deleteFile(Map<String, Object> map) throws Exception{
		fileMapper.deleteFile(map);
	}
	
	public ResultApi insertFile(String rootDir, String subDir,File file) throws Exception{
		
		ResultApi result = new ResultApi();
		try {
			
			
			
			
		}catch(Exception e) {
			
			e.printStackTrace();
			
		}
		
		return result;
		
		
	}
	
	public Map<String,Object>selectFile(Map<String,Object>map) throws Exception{
		return fileMapper.selectFile(map);
	}
	
	
}
