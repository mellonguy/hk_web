package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.CustomerMapper;
import kr.co.carrier.service.CustomerService;
import kr.co.carrier.vo.CustomerVO;

@Service("customerService")
public class CustomerServiceImpl implements CustomerService{

	
	@Resource(name="customerMapper")
	private CustomerMapper customerMapper;
	
	public Map<String, Object> selectCustomer(Map<String, Object> map) throws Exception{
		return customerMapper.selectCustomer(map);
	}
	
	public Map<String, Object> selectCustomerByCustomerName(Map<String, Object> map) throws Exception{
		return customerMapper.selectCustomerByCustomerName(map);
	}
	
	public List<Map<String, Object>> selectCustomerList(Map<String, Object> map) throws Exception{
		return customerMapper.selectCustomerList(map);
	}
	
	public int selectCustomerListCount(Map<String, Object> map) throws Exception{
		return customerMapper.selectCustomerListCount(map);
	}
	
	public int insertCustomer(CustomerVO customerVO) throws Exception{
		return customerMapper.insertCustomer(customerVO);
	}
	
	public void updateCustomer(CustomerVO customerVO) throws Exception{
		customerMapper.updateCustomer(customerVO);
	}
	
	public List<Map<String, Object>> selectCustomerListIncludeDriver(Map<String, Object> map) throws Exception{
		return customerMapper.selectCustomerListIncludeDriver(map);
	}
	
	public Map<String, Object> selectCustomerByCustomerNameForInsert(Map<String, Object> map) throws Exception{
		return customerMapper.selectCustomerByCustomerNameForInsert(map);
	}
	
	public void updateAllocationCustomerInfo(Map<String, Object> map) throws Exception{
		customerMapper.updateAllocationCustomerInfo(map);
	}
	
	public void updatePaymentCustomerInfo(Map<String, Object> map) throws Exception{
		customerMapper.updatePaymentCustomerInfo(map);
	}

	public List<Map<String, Object>> selectCustomerListIncludePersonInCharge(Map<String, Object> map) throws Exception{
		return customerMapper.selectCustomerListIncludePersonInCharge(map);
	}
	
	public List<Map<String, Object>> selectChargeList(Map<String, Object> map) throws Exception{
		return customerMapper.selectChargeList(map);
	}
	
	public Map<String, Object> selectCustomerByCustomerNameAndChargeName(Map<String, Object> map) throws Exception{
		return customerMapper.selectCustomerByCustomerNameAndChargeName(map);
	}
	public List<Map<String, Object>> selectBlackListCusomterList(Map<String, Object> map) throws Exception{
		return customerMapper.selectBlackListCusomterList(map);
	}
	public int selectBlackListCusomterListCount(Map<String, Object> map) throws Exception{
		return customerMapper.selectBlackListCusomterListCount(map);
	}
	
	
	
	
	
}
