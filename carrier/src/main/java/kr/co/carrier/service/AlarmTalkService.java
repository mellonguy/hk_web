package kr.co.carrier.service;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.AlarmTalkVO;

public interface AlarmTalkService {

	
	public String alarmTalkSends(Map<String, Object> map)throws Exception;
	
	public Map<String, Object> selectAlarmTalk(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectAlarmTalkList(Map<String, Object> map) throws Exception;
	public int insertAlarmTalk(AlarmTalkVO alarmTalkVO) throws Exception;
	
}
