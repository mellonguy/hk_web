package kr.co.carrier.service;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.DebtorCreditorVO;

public interface DebtorCreditorService {

	
	public Map<String, Object> selectDebtorCreditor(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectDebtorCreditorList(Map<String, Object> map) throws Exception;
	public int selectDebtorCreditorListCount(Map<String, Object> map) throws Exception;
	public int insertDebtorCreditor(DebtorCreditorVO debtorCreditorVO) throws Exception;
	public void deleteDebtorCreditor(Map<String, Object> map) throws Exception;
	
	
	public List<Map<String, Object>> selectAllocationListForInsert(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectAllocationListUpdated(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectBillPublishRequestListForInsert(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectCarInfoListByBillPublishRequestId(Map<String, Object> map) throws Exception;
	
	public int selectCustomerListCount(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectCustomerList(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectDebtorCreditorListByCustomerId(Map<String, Object> map) throws Exception;
	
	public int selectAllocationCountByDebtorCreditorId(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectAllocationListByDebtorCreditorId(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectPaymentListByPayment(Map<String, Object> map) throws Exception;
	public void updateDebtorCreditor(Map<String, Object> map) throws Exception;
	public void updateDebtorCreditorTotalVal(Map<String, Object> map) throws Exception;
	public int selectCarInfoCountByDebtorCreditorId(Map<String, Object> map) throws Exception;
	
	/***
	 * 2021.05.10
	 * 매출 취소 
	 */
	public void updateBillPublishRequest(Map<String, Object> map) throws Exception;
	public void updateBillingStatus(Map<String, Object> map) throws Exception;
	public void deleteDebtorCreditors(Map<String, Object> map) throws Exception;
	public String selectAllocationIdByBatchStatusId(Map<String, Object> map) throws Exception;
	
	
}
