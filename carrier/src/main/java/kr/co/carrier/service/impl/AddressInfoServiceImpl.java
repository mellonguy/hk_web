package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.AddressInfoMapper;
import kr.co.carrier.service.AddressInfoService;
import kr.co.carrier.vo.AddressInfoVO;




@Service("addressInfoService")
public class AddressInfoServiceImpl implements AddressInfoService{

	
	
	@Resource(name="addressInfoMapper")
	private AddressInfoMapper addressInfoMapper;
	
	
	public Map<String, Object> selectAddressInfo(Map<String, Object> map) throws Exception{
		return addressInfoMapper.selectAddressInfo(map);
	}
	
	public List<Map<String, Object>> selectAddressInfoList(Map<String, Object> map) throws Exception{
		return addressInfoMapper.selectAddressInfoList(map);
	}
	
	public int insertAddressInfo(AddressInfoVO addressInfoVO) throws Exception{
		return addressInfoMapper.insertAddressInfo(addressInfoVO);
	}
	
	public void deleteAddressInfo(Map<String, Object> map) throws Exception{
		addressInfoMapper.deleteAddressInfo(map);
	}
	
	public List<Map<String, Object>> selectAddressInfoListByKeyWord(Map<String, Object> map) throws Exception{
		return addressInfoMapper.selectAddressInfoListByKeyWord(map);
	}
	
	
	
	
	
}
