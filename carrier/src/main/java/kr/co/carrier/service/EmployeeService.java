package kr.co.carrier.service;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.EvaluationCompleteVO;
import kr.co.carrier.vo.AlarmSocketVO;
import kr.co.carrier.vo.EmployeeVO;
import kr.co.carrier.vo.EvaluationVO;

public interface EmployeeService {

	
	public Map<String, Object> selectEmployee(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectEmployeeList(Map<String, Object> map) throws Exception;
	public int selectEmployeeListCount(Map<String, Object> map) throws Exception;
	public int insertEmployee(EmployeeVO employeeVO) throws Exception;
	public void updateEmployee(EmployeeVO employeeVO) throws Exception;
	public void updateEmployeeApproveYn(Map<String, Object> map) throws Exception;
	public void updateEmployeeRole(Map<String, Object> map) throws Exception;
	public void updateEmployeeAllocation(Map<String, Object> map) throws Exception;
	public void updateEmployeeStatus(Map<String, Object> map) throws Exception;
	public void updateEmployeeListRowCnt(Map<String, Object> map) throws Exception;
	public void updateEmployeeCompanyId(Map<String, Object> map) throws Exception;
	public Map<String,Object> selectIdChectkForPassword(Map<String,Object>map) throws Exception;
	public Map<String,Object>selectCheckCertNumForPassword(Map<String,Object>map) throws Exception;
	public void updateCreatePassword(Map<String, Object> map) throws Exception;
	public List<Map<String,Object>>selectEvlauationListEmpName(Map<String, Object> map) throws Exception;
	public List<Map<String,Object>>selectEvaluationQuestion(Map<String, Object> map) throws Exception;
	public void insertEvaluation(EvaluationVO evaluationVO) throws Exception;
	public void insertEvaluationComplete(EvaluationCompleteVO evaluationCompleteVO) throws Exception;
	public List<Map<String,Object>>selectEvaluationList(Map<String, Object> map) throws Exception;
	public List<String>selectEvaluationQuestionIdList() throws Exception;
	public void deleteEvaluationData(Map<String,Object>map)throws Exception;
	public List<Map<String,Object>>selectForFinalPointList(Map<String, Object> map) throws Exception;
	public List<Map<String,Object>>selectEvaluationCompleteList(Map<String, Object> map) throws Exception;
	public int insertAlarmBoard(AlarmSocketVO alarmSocketVO) throws Exception;	
	public List<Map<String,Object>>selectEmpNote(Map<String, Object> map) throws Exception;
	public int insertEmployeeMessage(Map<String, Object> map) throws Exception;
	public List<Map<String,Object>>selectEmployeeNoteList(Map<String, Object> map) throws Exception;
	public void deleteEmployeeMessage(Map<String,Object>map)throws Exception;
	public void updateMessage(Map<String, Object> map) throws Exception;

}
