package kr.co.carrier.service;

import java.io.File;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import kr.co.carrier.utils.ResultApi;

public interface FileService {

	public ResultApi insertFile(String rootDir, String subDir,HttpServletRequest request) throws Exception;
	public ResultApi insertFile(String rootDir, String subDir,String bbsId,HttpServletRequest request) throws Exception;
	public ResultApi insertFile(String rootDir, String subDir,File file) throws Exception;
	public Map<String,Object>selectFile(Map<String,Object>map) throws Exception;
	public List<Map<String, Object>> selectFileList(Map<String, Object> map) throws Exception;
	public void deleteFile(Map<String, Object> map) throws Exception;
	
	
	
}
