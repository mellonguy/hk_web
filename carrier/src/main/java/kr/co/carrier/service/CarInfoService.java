package kr.co.carrier.service;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.AllocationVO;
import kr.co.carrier.vo.CarDetailVO;
import kr.co.carrier.vo.CarInfoVO;
import kr.co.carrier.vo.CarInsuranceSubVO;
import kr.co.carrier.vo.CarInsuranceVO;
import kr.co.carrier.vo.CarMaintanceVO;

public interface CarInfoService {
	public Map<String, Object> selectCarInfo(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectCarInfoList(Map<String, Object> map) throws Exception;
	public int selectCarInfoListCount(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectCarInfoListByCarIdNum(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectCarInfoListByCarNum(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectCarInfoListByContractNum(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectCarInfoListByDepartureArrival(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectSumSalesTotalByBatchStatusId(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectCarInfoForDuplicate(Map<String, Object> map) throws Exception;
	public int selectEqBillingCnt(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectCarInfoListByAllocationIdList(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectCarIdNumDubleCheckList(Map<String, Object> map) throws Exception;	
	public List<Map<String, Object>> selectCarMaintananceList(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectCarMaintananceRegList(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectCarDetailE(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectCarDetailMap(Map<String, Object> map) throws Exception;
	public List<Map<String,Object>> selectCarDetailList(Map<String,Object>map) throws Exception;
	public List<Map<String,Object>> selectCarInsuranceList(Map<String,Object>map) throws Exception;
	public List<Map<String,Object>> selectCarInsuranceSubList(Map<String,Object>map) throws Exception;
	public Map<String, Object> selectCarInsurance(Map<String, Object> map) throws Exception;
	public List<Map<String,Object>> selectCarInsuranceMainList(Map<String,Object>map) throws Exception;
	public List<Map<String,Object>> selectCarInspectionManageList(Map<String,Object>map) throws Exception;
	
	public int insertCarInfo(CarInfoVO carInfoVO) throws Exception;
	public int insertCarMaintance(CarMaintanceVO carMaintanceVO) throws Exception;
	public int insertCarDetail(CarDetailVO caDetailVO) throws Exception;
	public int insertCarIncurance(CarInsuranceVO carInsuranceVO) throws Exception;
	public int insertCarInsuranceSub(CarInsuranceSubVO carInsuranceSubVO) throws Exception;
	public int insertInspection(Map<String, Object> map) throws Exception;
	public int insertCarInfoLogByVO(CarInfoVO carInfoVO) throws Exception; //로그기록
	public int insertCarInfoLogByMap(Map<String, Object> map) throws Exception; //로그기록
		
	public void updateCarInfo(CarInfoVO carInfoVO) throws Exception;
	public void updateCarInfoDriver(Map<String, Object> map) throws Exception;
	public void updateCarInfoSalesTotal(Map<String, Object> map) throws Exception;
	public void updateCarInfoDriverSetEmp(Map<String, Object> map) throws Exception;
	public void updateCarInfoDecideStatus(Map<String, Object> map) throws Exception;
	public void updateDecideStatusBySearchData(Map<String, Object> map) throws Exception;
	public void updateCarInfoDebtorCreditorIdByBillPublishRequstId(Map<String, Object> map) throws Exception;
	public void updateCarInfoForCancel(Map<String, Object> map) throws Exception;
	public void updateCarInfoDepartureAndArrival(Map<String, Object> map) throws Exception;
	public void updateCarMaintanance(CarMaintanceVO carMaintanceVO) throws Exception;
	public void updateCarInfoForDuplication(CarInfoVO carInfoVO) throws Exception;	
	public void updateCarInfoCarIdNum(Map<String, Object> map) throws Exception;
	public void updateCarDetail(CarDetailVO carDetailVO) throws Exception;
	public int updateCarInsurance(CarInsuranceVO carInsuranceVO) throws Exception;
	public void updateCarInsuranceSubFlag(Map<String, Object> map)throws Exception;
	public void updateCarInsuranceSub(CarInsuranceSubVO carInsuranceSubVO)throws Exception;
	public void updatePaymentCloseDt(Map<String, Object> map)throws Exception;
	public void updatePaymentClosingDt(Map<String, Object> map)throws Exception;
	public void updateInspection(Map<String, Object> map) throws Exception;
	
	public void deleteCarInfo(Map<String, Object> map) throws Exception;
	public void deleteCarMaintanance(Map<String, Object> map) throws Exception;
	public void deleteCarDetail(Map<String,Object>map) throws Exception;
	public void deleteCarInsurance(Map<String, Object> map) throws Exception;
	public void deleteCarInsuranceSub(Map<String, Object> map) throws Exception;	
}
