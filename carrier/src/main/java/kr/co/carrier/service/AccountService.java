package kr.co.carrier.service;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.AccountVO;

public interface AccountService {

	
	public Map<String, Object> selectAccount(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectAccountList(Map<String, Object> map) throws Exception;
	public int selectAccountListCount(Map<String, Object> map) throws Exception;
	public int insertAccount(AccountVO accountVO) throws Exception;
	public void deleteAccount(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectSummary(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectCustomerId(Map<String, Object> map) throws Exception;
	public int selectCustomerTotalCount (Map<String, Object> map) throws Exception;
	public int selectMonthTotalCount(Map<String,Object>map)throws Exception;
	
	public List<Map<String,Object>> selectEmployeeSummary(Map<String,Object>map)throws Exception;
	public int insertBatchTeam (Map<String,Object>map) throws Exception;
	public int selectDeadLineCount (Map<String,Object>map) throws Exception;
	public List<Map<String,Object>>selectPaymentList(Map<String,Object> map) throws Exception; 
	public List<Map<String,Object>>selectAllocationstatus(Map<String,Object> map) throws Exception; 
	public List<Map<String,Object>>selectBatchTeam(Map<String,Object>map)throws Exception;
	public List<Map <String,Object>>selectEmployeeBatchDetail(Map<String,Object>map)throws Exception;
	public int selectBatchTeamTotalCount(Map<String, Object> map) throws Exception;
	public List<Map<String,Object>>selectTeamSummary (Map<String,Object>map)throws Exception;
	public List<Map<String,Object>>selectTeamPaymentList (Map<String,Object>map)throws Exception;
	public List<Map<String,Object>> selectForYear (Map<String,Object>map)throws Exception;
	public List<Map<String,Object>>selectQuarterAndSemiannually(Map<String,Object>map)throws Exception;
	public List<Map<String,Object>>selectEmployeePaymentList(Map<String,Object>map)throws Exception; 
	public List<Map<String,Object>>selectCarrierPaymentList(Map<String,Object>map)throws Exception; 
	public void afterProcessDecideFinal(Map<String, Object> map) throws Exception;
	public List<Map<String,Object>>selectRecivableList(Map<String,Object>map)throws Exception; 	
	public int selectRecivableCount(Map<String,Object>map)throws Exception;	
	 public List<Map<String,Object>>selectRecivableDetail(Map<String, Object> map) throws Exception;
	public int selectRecivableDetailCount(Map<String,Object>map)throws Exception;
}
