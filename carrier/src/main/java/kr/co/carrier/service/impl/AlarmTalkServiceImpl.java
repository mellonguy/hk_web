package kr.co.carrier.service.impl;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.AlarmTalkMapper;
import kr.co.carrier.service.AlarmTalkService;
import kr.co.carrier.service.CustomerService;
import kr.co.carrier.service.FileService;
import kr.co.carrier.utils.BaseAppConstants;
import kr.co.carrier.utils.MultipartUtility;
import kr.co.carrier.utils.WebUtils;
import kr.co.carrier.vo.AlarmTalkVO;

@Service("alarmTalkService")
public class AlarmTalkServiceImpl implements AlarmTalkService{

	
	@Value("#{appProp['upload.base.path']}")
    private String rootDir;    
    private String subDir = "bbs";	
	
    
    private static String histUrl; // sms/alarmTalk url
    
    @Value("#{appProp['hist.url']}")
    public void setKey(String value) {
      histUrl = value;
    }
    
	@Autowired
	private FileService fileService;
	
	@Autowired
	private CustomerService customerService;
	
    @Resource(name="alarmTalkMapper")
    private AlarmTalkMapper alarmTalkMapper;
    
	public Map<String, Object> selectAlarmTalk(Map<String, Object> map) throws Exception{
		return alarmTalkMapper.selectAlarmTalk(map);
	}
	
	
	public List<Map<String, Object>> selectAlarmTalkList(Map<String, Object> map) throws Exception{
		return alarmTalkMapper.selectAlarmTalkList(map);
	}
	
	public int insertAlarmTalk(AlarmTalkVO alarmTalkVO) throws Exception{
		return alarmTalkMapper.insertAlarmTalk(alarmTalkVO);
	}
	
	//@Transactional(propagation=Propagation.REQUIRED, rollbackFor={Exception.class})		
	public String alarmTalkSends(Map<String, Object> map)throws Exception{
        
        
		StringBuilder urlBuilderAT = new StringBuilder(histUrl+"/sms/histmsg");
		StringBuilder urlBuilderFT = new StringBuilder(histUrl+"/sms/histmsg");
		String resultMsg = "";
		String result = "";
		String status = "AT";
	    String alarmTalkSendResult ="";

	    
	    
		//알림톡 발송 구분		1:기사배정,2:상차완료,3:하차완료,4:인계완료,5:탁송완료,6탁송취소,7:배차정보(기사님에게),8:배차승인(배차담당직원)
		
		urlBuilderAT.append("?");
		urlBuilderFT.append("?");
		urlBuilderAT.append(URLEncoder.encode("uid","UTF-8") + "=" + URLEncoder.encode(BaseAppConstants.COMPANY_TYPE_NATIONAL, "UTF-8") + "&");
		urlBuilderFT.append(URLEncoder.encode("uid","UTF-8") + "=" + URLEncoder.encode(BaseAppConstants.COMPANY_TYPE_NATIONAL, "UTF-8") + "&");
		
		
		
		
		
	//if(customerMap.get("alarm_or_sms_yn").equals("A")) {
			
		
		
		
		if(map.get("gubun").toString().equals("0")) {			//탁송 예약

			
			/*
			resultMsg += "[한국카캐리어] 탁송 예약이 완료 되었습니다.\r\n";
			resultMsg += map.get("departure_dt") != null && !map.get("departure_dt").toString().equals("") ?  "출발일 : "+map.get("departure_dt").toString()+"\r\n":"출발일 : \r\n";
			resultMsg += map.get("departure_time") != null && !map.get("departure_time").toString().equals("") ?  "시간 : "+map.get("departure_time").toString()+"\r\n":"시간 : \r\n";
			resultMsg += map.get("departure") != null && !map.get("departure").toString().equals("") ?  "상차지 : "+map.get("departure").toString()+"\r\n":"상차지 : \r\n";
			resultMsg += map.get("departure_phone") != null && !map.get("departure_phone").toString().equals("") ?  "상차지 연락처 : "+map.get("departure_phone").toString()+"\r\n":"상차지 연락처 : \r\n";
			resultMsg += map.get("arrival") != null && !map.get("arrival").toString().equals("") ?  "하차지 : "+map.get("arrival").toString()+"\r\n":"하차지 : \r\n";
			resultMsg += map.get("arrival_phone") != null && !map.get("arrival_phone").toString().equals("") ?  "하차지 연락처 : "+map.get("arrival_phone").toString()+"\r\n":"하차지 연락처 : \r\n";
			resultMsg += map.get("car_kind") != null && !map.get("car_kind").toString().equals("") ? "차종 : "+map.get("car_kind").toString()+"\r\n":"차종 : \r\n";
			resultMsg += map.get("car_id_num") != null && !map.get("car_id_num").toString().equals("") ?  "차대번호 : "+map.get("car_id_num").toString()+"\r\n":"차대번호 : \r\n";
			resultMsg += "상세 정보 부탁 드립니다. \r\n";
			resultMsg += "대표전화 : 1577-5268 \r\n";
			*/
			
			resultMsg += "[한국카캐리어] 탁송 예약이 완료 되었습니다.\r\n";
			resultMsg += "안녕하세요.\r\n";
			resultMsg += "대한민국 NO.1 차량 탁송 전문업체 한국카캐리어(주) 입니다.\r\n";
			resultMsg += "\r\n";
			resultMsg += "어플리케이션에서 간편예약을 해 주시면 탁송이 진행 되는 과정에 대해 어플리케이션에서 확인 할 수 있습니다.\r\n";
			resultMsg += "어플리케이션에서 간편예약을 하여 탁송된 건에 대해서는 추후 마일리지가 적립 되어 사용 할 수 있습니다.\r\n";
			resultMsg += "\r\n";
			resultMsg += "한국카캐리어 어플리케이션을 설치 하면\r\n";
			resultMsg += "1. 탁송금액 조회 및  탁송 관련 카카오톡 문의\r\n";
			resultMsg += "\r\n";
			resultMsg += "앱을 통해 회원가입을 하시면\r\n";
			resultMsg += "1. 탁송 예약 서비스 제공\r\n";
			resultMsg += "2. 탁송 예약 차량 조회 서비스 제공\r\n";
			resultMsg += "3. 예약 조회 및 탁송인수증 조회 서비스 제공\r\n";
			resultMsg += "4. 카카오톡 1:1 문의 서비스 제공\r\n";
			resultMsg += "5. 추후 마일리지 적립 서비스 제공할 예정\r\n";
			resultMsg += "\r\n";
			resultMsg += "구글플레이 및 앱스토어에서 '한국카캐리어'를 검색 해 주세요.\r\n";
			urlBuilderAT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode(resultMsg, "UTF-8") + "&");
			urlBuilderFT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode(resultMsg, "UTF-8") + "&");
			urlBuilderAT.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_008", "UTF-8") + "&");
			urlBuilderFT.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_008", "UTF-8") + "&");
			
			urlBuilderAT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
			urlBuilderFT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
			
			//return "jsonView";
			//hkc_000
		}else if(map.get("gubun").toString().equals("1")) {			//기사배정

			resultMsg += "차종 : ";
			resultMsg += map.get("car_kind") != null && !map.get("car_kind").toString().equals("") ? map.get("car_kind").toString():"미정";
			resultMsg += "\r\n";
			resultMsg += "차대번호 : ";
			resultMsg += map.get("car_id_num") != null && !map.get("car_id_num").toString().equals("") ?  map.get("car_id_num").toString():"미정";
			resultMsg += "\r\n";
			resultMsg += "출발일 : "+ map.get("departure_dt").toString()+"\r\n";
			resultMsg += "하차지 : "+ map.get("arrival").toString()+"\r\n";
			resultMsg += "기사명 : "+ map.get("driver_name").toString();
			
			//기사가 변경 되었을때.......
			if(map.get("changeDriverSendAlarmTalkYn") != null && map.get("changeDriverSendAlarmTalkYn").toString().equals("Y")) {
				resultMsg += "(기사변경됨)";	
			}
			
 			resultMsg += "\r\n";
			resultMsg += "연락처 : "+ map.get("phone_num").toString()+"\r\n";
			resultMsg += "해당 차량이 한국카캐리어 "+ map.get("driver_name").toString()+"기사님께 배정 되었습니다."+"\r\n";
			resultMsg += "상세 정보 부탁 드립니다. \r\n";
			
			
//			if(map.get("send_account_info_yn").toString().equals("Y")){
//				if(map.get("payment_kind").toString().equals("DD")) {
//					resultMsg += "탁송료는 기사님께 지급 부탁 드립니다.";
//				}else {
//					if(map.get("payment_kind").toString().equals("CA") && map.get("billing_division").toString().equals("00")) {
//						if(map.get("customer_kind").toString().equals("00")) {
//							resultMsg += "입금계좌 : 1005-002-196600\r\n";
//							resultMsg += "우리은행 한국카캐리어(주)\r\n";	
//						/*}else if(map.get("customer_kind").toString().equals("01")) {
//							resultMsg += "입금계좌 : 356-1328-4613-53\r\n";
//							resultMsg += "농협 임경숙\r\n";	
//						}	*/
//						}else if(map.get("customer_kind").toString().equals("01")) {
//							resultMsg += "입금계좌 : 352-1650-4931-63\r\n";
//							resultMsg += "농협 최지용\r\n";	
//						}
//					}
//				}	
//			}
			urlBuilderAT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode(resultMsg, "UTF-8") + "&");
			urlBuilderFT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode(resultMsg, "UTF-8") + "&");
			urlBuilderAT.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_000", "UTF-8") + "&");
			urlBuilderFT.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_000", "UTF-8") + "&");
			
			map.put("contentId", map.get("driver_id") != null && !map.get("driver_id").toString().equals("") ? map.get("driver_id").toString():"");
			map.put("categoryType", "driverPic");
			
			List<Map<String, Object>> fileMap = fileService.selectFileList(map);
			
			if(fileMap != null && fileMap.size() == 1) {			//기사 이미지 파일은 하나...
				
				File file = new File(rootDir+"/driver"+fileMap.get(0).get("file_path").toString());

				try {
		        	 result = fileUpload(file);	 
		         }catch(IOException e) {
		        	 result = "";
		         }
				
				if(!result.equals("")) {
					JSONParser jsonParser = new JSONParser();
					JSONObject jsonObject = null; 
							
					try {
						jsonObject = (JSONObject) jsonParser.parse(result);
						if(jsonObject.get("code").toString().equals("success")) {		//이미지 업로드 결과가 성공이면....
							status = "FT";
							urlBuilderFT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("ft", "UTF-8") + "&");
							JSONObject obj = (JSONObject)jsonParser.parse(jsonObject.get("data").toString());
							urlBuilderFT.append(URLEncoder.encode("talkImgUrl","UTF-8") + "=" + URLEncoder.encode(obj.get("img_url").toString(), "UTF-8") + "&");
						}else {
							urlBuilderAT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
							urlBuilderFT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
						}	
						
					}catch(Exception e) {
						e.printStackTrace();
						urlBuilderAT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
						urlBuilderFT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
					}
					
				}else {
					urlBuilderAT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
					urlBuilderFT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
				}
				
				//response : {"code":"success","data":{"img_name":"c42e7573-3d74-48f6-bb4b-dc23d2157664.jpg","img_url":"http://mud-kage.kakao.com/dn/jsD3g/btqvnka62eC/hhQxZ2AnSilVShhDn28N8k/img_l.jpg"},"message":""}
				
			}else {
				urlBuilderAT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
				urlBuilderFT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
			}
			
			//hkc_000
		}else if(map.get("gubun").toString().equals("2")) {	//상차완료
			resultMsg += "고객님의 차량이 안전하게 상차 되었습니다.";
			urlBuilderAT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode( resultMsg, "UTF-8") + "&");
			urlBuilderFT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode( resultMsg, "UTF-8") + "&");
			urlBuilderAT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
			urlBuilderFT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
			urlBuilderAT.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_001", "UTF-8") + "&");
			urlBuilderFT.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_001", "UTF-8") + "&");
			//hkc_001
		}else if(map.get("gubun").toString().equals("3")) {	//하차완료
			
			/*urlBuilder.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode( resultMsg+"문서 알림.\r\n 도착하였습니다.\r\n결재/합의 바랍니다.", "UTF-8") + "&");
			urlBuilder.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
			urlBuilder.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hanway01_2", "UTF-8") + "&");*/
		}else if(map.get("gubun").toString().equals("4")) {	//인계완료
			
			/*urlBuilder.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode( resultMsg+"문서 알림.\r\n 도착하였습니다.\r\n결재/합의 바랍니다.", "UTF-8") + "&");
			urlBuilder.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
			urlBuilder.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hanway01_2", "UTF-8") + "&");*/
		}else if(map.get("gubun").toString().equals("5")) {	//탁송완료
			resultMsg += "차량이 고객님께 안전하게 전달 되었습니다."+"\r\n";
			resultMsg += "저희 한국카캐리어를 이용해 주셔서 감사합니다."+"\r\n";
			resultMsg += "많은 이용 부탁 드립니다."+"\r\n";
			urlBuilderAT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode( resultMsg, "UTF-8") + "&");
			urlBuilderFT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode( resultMsg, "UTF-8") + "&");
			urlBuilderAT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
			urlBuilderFT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
			urlBuilderAT.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_002", "UTF-8") + "&");
			urlBuilderFT.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_002", "UTF-8") + "&");
			//hkc_002
		}else if(map.get("gubun").toString().equals("6")) {	//탁송취소
			/*#{고객명}님이 요청하신
			차종 : #{차종}
			하차지 :#{시도 시군구} 
			탁송이 취소 되었습니다.*/
			urlBuilderAT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode( resultMsg+"문서 알림.\r\n 도착하였습니다.\r\n결재/합의 바랍니다.", "UTF-8") + "&");
			urlBuilderAT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
			urlBuilderAT.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_005", "UTF-8") + "&");
			urlBuilderFT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode( resultMsg+"문서 알림.\r\n 도착하였습니다.\r\n결재/합의 바랍니다.", "UTF-8") + "&");
			urlBuilderFT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
			urlBuilderFT.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_005", "UTF-8") + "&");
			//hkc_005
		}else if(map.get("gubun").toString().equals("7")) {	//배차정보
			/*#{기사명} 기사님께 배차 정보가 도착 했습니다.
			상차지 : #{시도 시군구}
			 상차지담당자명 : #{이름}
			 담당자연락처 : #{전화번호}
			하차지 : #{시도 시군구}
			하차지담당자명 : #{이름}
			 담당자연락처 : #{전화번호}
			차종 : #{차종}
			차량번호 : #{차량번호}
			차대번호 : #{차대번호}
			오늘도 안전운행 하세요.*/
			
			urlBuilderAT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode( resultMsg+"문서 알림.\r\n 도착하였습니다.\r\n결재/합의 바랍니다.", "UTF-8") + "&");
			urlBuilderAT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
			urlBuilderAT.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_003", "UTF-8") + "&");
			urlBuilderFT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode( resultMsg+"문서 알림.\r\n 도착하였습니다.\r\n결재/합의 바랍니다.", "UTF-8") + "&");
			urlBuilderFT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
			urlBuilderFT.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_003", "UTF-8") + "&");
			//hkc_003
		}else if(map.get("gubun").toString().equals("8")) {	//배차승인
			/*#{기사명}기사님이 
			상차지 : #{시도 시군구}
			하차지 : #{시도 시군구}
			차종 : #{차종}
			차량번호 : #{차량번호}
			차대번호 : #{차대번호}
			배차를 승인 했습니다.*/
			
			urlBuilderAT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode( resultMsg+"문서 알림.\r\n 도착하였습니다.\r\n결재/합의 바랍니다.", "UTF-8") + "&");
			urlBuilderAT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
			urlBuilderAT.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_004", "UTF-8") + "&");
			urlBuilderFT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode( resultMsg+"문서 알림.\r\n 도착하였습니다.\r\n결재/합의 바랍니다.", "UTF-8") + "&");
			urlBuilderFT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
			urlBuilderFT.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_004", "UTF-8") + "&");
			//hkc_004
			
			
			
			
		}else if(map.get("gubun").toString().equals("9")) {
		
		/*
			#{기사명} 기사님 과태료 납부 안내 입니다.
			차량번호 : #{차량번호}
			위반일 : #{위반일}
			시간 : #{시간}
			위반장소 : #{위반장소}
			위반내용 : #{위반내용}
			금액 : #{금액}
			납부 계좌정보 : #{납부계좌정보}
			납부 마감기간 : #{납부마감기간}
			해당 내용을 확인 하시고 납부 마감기간 내에 납부 부탁 드립니다.
		*/
		
			resultMsg += map.get("driverName").toString()+" 기사님 과태료 납부 안내 입니다.\r\n";
			resultMsg += "차량번호 : "+ map.get("carNum").toString()+"\r\n";
			resultMsg += "위반일 : "+ map.get("occurrenceDt").toString()+"\r\n";
			resultMsg += "시간 : "+ map.get("occurrenceTime").toString()+"\r\n";
			resultMsg += "위반장소 : "+ map.get("placeddress").toString()+"\r\n";
			resultMsg += "위반내용 : "+ map.get("summary").toString()+"\r\n";
			resultMsg += "금액 : "+ map.get("amount").toString()+"\r\n";
			resultMsg += "납부계좌정보 : "+ map.get("accountInfo").toString()+"\r\n";
			resultMsg += "납부 마감기간 : "+ map.get("endDt").toString()+"\r\n";
			resultMsg += "해당 내용을 확인 하시고 납부 마감기간 내에 납부 부탁 드립니다.";
			
			urlBuilderAT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode(resultMsg, "UTF-8") + "&");
			urlBuilderAT.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_007", "UTF-8") + "&");
			
			
		}else if(map.get("gubun").toString().equals("10")) {
	         
	         /*
	            "차종 : #{차종}
	            #{차대번호}
	            #{차량번호}
	            하차지 :#{시도 시군구} 
	            위 차량의 인수증이 도착 했습니다."

	             
	          */
	         
	         
	          resultMsg += map.get("car_kind") != null &&!map.get("car_kind").toString().equals("")?"차종 : "+map.get("car_kind").toString()+"\r\n":"";
	          resultMsg += map.get("car_id_num") != null &&!map.get("car_id_num").toString().equals("")?"차대번호 : "+map.get("car_id_num").toString()+"\r\n":""; 
	          resultMsg += map.get("car_num") != null &&!map.get("car_num").toString().equals("")?"차량번호 : "+map.get("car_num").toString()+"\r\n":""; 
	          resultMsg += map.get("arrival") != null &&!map.get("arrival").toString().equals("")?"하차지 : "+map.get("arrival").toString()+"\r\n":""; 
	                    
	          
	          resultMsg +="위 차량의 인수증이 도착 했습니다.";
	          
	         urlBuilderFT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("ft", "UTF-8") + "&");
	         urlBuilderFT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode(resultMsg, "UTF-8") + "&");
	         urlBuilderFT.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_010", "UTF-8") + "&");
	                     
	            
	         File file = new File(rootDir+"/allocation"+map.get("file_path").toString());
	         
	         try {
	        	 result = fileUpload(file);	 
	         }catch(IOException e) {
	        	 result = "";
	         }
	         
	         System.out.println("result="+result);
	         
	         
	         
	         if(!result.equals("")) {
	                  JSONParser jsonParser = new JSONParser();
	                  JSONObject jsonObject = null; 
	                  
	                  try {
	                     jsonObject = (JSONObject) jsonParser.parse(result);
	                     if(jsonObject.get("code").toString().equals("success")) {      //이미지 업로드 결과가 성공이면....
	                        status = "FT";
	                        urlBuilderFT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("ft", "UTF-8") + "&");
	                        JSONObject obj = (JSONObject)jsonParser.parse(jsonObject.get("data").toString());
	                        urlBuilderFT.append(URLEncoder.encode("talkImgUrl","UTF-8") + "=" + URLEncoder.encode(obj.get("img_url").toString(), "UTF-8") + "&");
	                     }else {
	                        alarmTalkSendResult = "0001";
	                        return alarmTalkSendResult;
	                     }   
	                     
	                  }catch(Exception e) { //json 형식이 아니기 때문에..
	                     e.printStackTrace();
	                     //네트워크 오류 등
	                     alarmTalkSendResult = "0005";         
	                     System.out.println();
	                     return alarmTalkSendResult;
	                     
	                  }
	                  
	               }

		}else if(map.get("gubun").toString().equals("11")) { //고객용어플 탁송예약 확정완료 알림톡 
			
			
	      /*
	       
			"#{고객명}님이 예약하신 탁송건의
				상차지 : #{상차지}
				하차지 : #{하차지}
				차종 : #{차종}
				차량정보 : #{차량정보}
				탁송예약이 확정 되었습니다."

			
          */
			resultMsg += map.get("customerName").toString()+" 님이 예약하신 탁송건의\r\n";
			resultMsg += "상차지 : "+ map.get("departureAddr").toString()+"\r\n";
			resultMsg += "하차지 : "+ map.get("arrivalAddr").toString()+"\r\n";
			resultMsg += "차종 : "+ map.get("carKind").toString()+"\r\n";
			resultMsg += "차량정보 : \r\n";
				if(map.get("carIdNum") != null && !map.get("carIdNum").toString().equals("")) {
					resultMsg += "\t차대번호 : "+ map.get("carIdNum").toString()+"\r\n";
				}
				if(map.get("carNum") != null && !map.get("carNum").toString().equals("")) {
					resultMsg += "\t차량번호 : "+ map.get("carNum").toString()+"\r\n";
					
				}
				//if(map.get)
			resultMsg += "탁송예약이 확정 되었습니다.";
			
			
			urlBuilderAT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode( resultMsg, "UTF-8") + "&");
			urlBuilderAT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
			urlBuilderAT.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_012", "UTF-8") + "&");
			
			
		}else if(map.get("gubun").toString().equals("12")) { //고객용어플 탁송예약 확정완료 알림톡 
			
			
		      /*
		       
				"#{고객명}님이 예약하신 탁송건의
					상차지 : #{상차지}
					하차지 : #{하차지}
					차종 : #{차종}
					차량정보 : #{차량정보}
					탁송예약이 확정 되었습니다."

				
	          */
			
			Map<String,Object> customerMap = customerService.selectCustomer(map);
				
				resultMsg += map.get("customerName").toString()+" 님이 예약하신 탁송건의\r\n";
				resultMsg += "상차지 : "+ map.get("departureAddr").toString()+"\r\n";
				resultMsg += "하차지 : "+ map.get("arrivalAddr").toString()+"\r\n";
				resultMsg += "차종 : "+ map.get("carKind").toString()+"\r\n";
				resultMsg += "차량정보 : \r\n";
					if(map.get("carIdNum") != null && !map.get("carIdNum").toString().equals("")) {
						resultMsg += "\t차대번호 : "+ map.get("carIdNum").toString()+"\r\n";
					}
					if(map.get("carNum") != null && !map.get("carNum").toString().equals("")) {
						resultMsg += "\t차량번호 : "+ map.get("carNum").toString()+"\r\n";
					}
		 
					 if(customerMap.get("customer_kind").equals("01")) { //거래처가 개인일때만 
						 resultMsg += "입금계좌 : 1005-804-185508\r\n";		
						 resultMsg += "우리은행 (주)엔씨로직스\r\n";
					 }
					 
				resultMsg += "탁송예약이 접수 되었습니다.";
				
				
				urlBuilderAT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode( resultMsg, "UTF-8") + "&");
				urlBuilderAT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
				urlBuilderAT.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_013", "UTF-8") + "&");
				
				
			}else if(map.get("gubun").toString().equals("13")) { //고객용어플 탁송예약 취소 알림톡 
				
			      /*
			       
					#{고객명}님이 예약하신 탁송건의
					상차지 : #{상차지}
					하차지 : #{하차지}
					차종 : #{차종}
					차량정보 : #{차량정보}
					탁송예약이 취소 되었습니다.
					탁송 취소에 관한 문의는 카카오톡 채널을 이용 해 주세요.

					
		          */
					resultMsg += map.get("customerName").toString()+" 님이 예약하신 탁송건의\r\n";
					resultMsg += "상차지 : "+ map.get("departureAddr").toString()+"\r\n";
					resultMsg += "하차지 : "+ map.get("arrivalAddr").toString()+"\r\n";
					resultMsg += "차종 : "+ map.get("carKind").toString()+"\r\n";
					resultMsg += "차량정보 : \r\n";
						if(map.get("carIdNum") != null && !map.get("carIdNum").toString().equals("")) {
							resultMsg += "\t차대번호 : "+ map.get("carIdNum").toString()+"\r\n";
						}
						if(map.get("carNum") != null && !map.get("carNum").toString().equals("")) {
							resultMsg += "\t차량번호 : "+ map.get("carNum").toString()+"\r\n";
						}
			
					resultMsg += "탁송예약이 취소 되었습니다.\r\n";
					resultMsg += "탁송 취소에 관한 문의는 카카오톡 채널을 이용 해 주세요.\r\n";
					
					urlBuilderAT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode( resultMsg, "UTF-8") + "&");
					urlBuilderAT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
					urlBuilderAT.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_014", "UTF-8") + "&");
			
		
			}
		
		
		
		
		
		
		
		int getHour = Integer.parseInt(WebUtils.getNow("HH"));
		
		//출발일이 현재일과 2일 차이 인경우에만 
		
		String departureDt = "";
		
		//departureDt = map.get("departure_dt").toString() ;
		departureDt = map.get("departure_dt") == null ? "":map.get("departure_dt").toString() ;
		long calDateDays = 0L;
		String nowDt = 		WebUtils.getNow("yyyy-MM-dd");
		
		if(!departureDt.equals("")) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date departureDt_toDate = format.parse(departureDt);
			Date nowDt_toDate = format.parse(nowDt);
			
			long calDate = nowDt_toDate.getTime() - departureDt_toDate.getTime(); 
			calDateDays = calDate / ( 24*60*60*1000);
			
			//탁송 예약을 하는 경우 2일 이후가 될 수 있으므로 절댓값 부분 수정. 
			//calDateDays = Math.abs(calDateDays);	
		}
		
		
		
		if(map.get("alarmOrSmsYn").equals("A")) {
		
		
		
		
		//오전 8시에서 오후 10시 사이에만 플러스 친구 톡을 발송 한다.
		//if((getHour >= 8 && getHour <= 22)  && calDateDays <= 2) {
		//if((getHour >= 8 && getHour <= 22) || map.get("gubun").toString().equals("9")  || map.get("gubun").toString().equals("10")  && calDateDays <= 2) {
		if(  ((getHour >= 8 && getHour <= 22) && calDateDays <= 2) || map.get("gubun").toString().equals("9")  || map.get("gubun").toString().equals("10")) {
			
			JSONObject jsonObjectForSms = new JSONObject();
			
					
			
			
			if(!result.equals("")) {
				String customerPhone = map.get("customer_phone").toString().replaceAll("-", "");
				customerPhone = customerPhone.replaceAll(" ", "");
				customerPhone = customerPhone.trim();
				String[] customerPhoneArr = customerPhone.split(";");
				
				ArrayList<String> arrayList = new ArrayList<String>();
				
						for(String data : customerPhoneArr){
						    if(!arrayList.contains(data)){
						        arrayList.add(data);
						        
						   }
						}
						
						
					customerPhoneArr = arrayList.toArray(new String[arrayList.size()]);
					customerPhone = "";
					for(int k = 0; k < customerPhoneArr.length; k++) {
						customerPhone += customerPhoneArr[k]+";";
					}
					
				
				//urlBuilderAT.append(URLEncoder.encode("rcvphns","UTF-8") + "=" + URLEncoder.encode("01089265400", "UTF-8") + "&");
				//urlBuilderFT.append(URLEncoder.encode("rcvphns","UTF-8") + "=" + URLEncoder.encode("01089265400", "UTF-8") + "&");
				
				//urlBuilderAT.append(URLEncoder.encode("rcvphns","UTF-8") + "=" + URLEncoder.encode("01041615268", "UTF-8") + "&");
				//urlBuilderFT.append(URLEncoder.encode("rcvphns","UTF-8") + "=" + URLEncoder.encode("01041615268", "UTF-8") + "&");
				
				urlBuilderAT.append(URLEncoder.encode("rcvphns","UTF-8") + "=" + URLEncoder.encode(customerPhone, "UTF-8") + "&");
				urlBuilderFT.append(URLEncoder.encode("rcvphns","UTF-8") + "=" + URLEncoder.encode(customerPhone, "UTF-8") + "&");
				urlBuilderAT.append(URLEncoder.encode("profileKey","UTF-8") + "=" + URLEncoder.encode("dd3f81c615756d611bd9fb625ca58f2717d4ca15", "UTF-8") + "&");
				urlBuilderFT.append(URLEncoder.encode("profileKey","UTF-8") + "=" + URLEncoder.encode("dd3f81c615756d611bd9fb625ca58f2717d4ca15", "UTF-8") + "&");
				
				//광고 아님
				urlBuilderFT.append(URLEncoder.encode("adMsgYn","UTF-8") + "=" + URLEncoder.encode("N", "UTF-8") + "&");
				
				
				//친구톡을 먼저 발송 한 후 실패시 알림톡을 발송한다.
				URL url = new URL(urlBuilderFT.toString());
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setDoOutput(true);
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Type", "application/json");        
				conn.setDoOutput(true);

				OutputStream os = conn.getOutputStream();
				os.flush();
				os.close();

				int responseCode = conn.getResponseCode();
				System.out.println("\nSending 'POST' request to URL : " + url);
				System.out.println("Response Code : " + responseCode);
				        
				BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				System.out.println(response.toString());
				JSONParser jsonParser = new JSONParser();
	            JSONArray list = null;
	            
	            
	            try {
	            	
	            	list = (JSONArray)jsonParser.parse(response.toString());
	            	
	            	   JSONObject jsonObject = (JSONObject) list.get(0);
	   	            jsonObjectForSms = jsonObject;
	   	            
	   	            if(map.get("gubun").toString().equals("10")) {
	   	                alarmTalkSendResult = jsonObject.get("code").toString();
	   	                
	   	                if(jsonObject.get("code").toString().equals("fail")) {
	   	                   alarmTalkSendResult = jsonObject.get("message").toString();
	   	                }else {
	   	                   alarmTalkSendResult = jsonObject.get("code").toString();
	   	                }
	   	                
	   	                return alarmTalkSendResult;
	   	                    
	   	            }
	   				 
	   				//친구톡 이미지 발송이 실패 한 경우 알림톡으로 다시 발송 한다.
	   				if(status.equals("FT") && jsonObject.get("code").toString().equals("fail") && !map.get("gubun").toString().equals("10")) {
	   					//[{"code":"fail","data":{"phn":"01089265400","msgid":"WEB20190517164322400750","type":"FT"},"message":"K101:NotAvailableSendMessage"}]
	   					JSONObject obj = (JSONObject) jsonParser.parse(jsonObject.get("data").toString());
	   					url = new URL(urlBuilderAT.toString());
	   					conn = (HttpURLConnection) url.openConnection();
	   					conn.setDoOutput(true);
	   					conn.setRequestMethod("POST");
	   					conn.setRequestProperty("Content-Type", "application/json");        
	   					conn.setDoOutput(true);
	   					
	   					os = conn.getOutputStream();
	   					os.flush();
	   					os.close();
	   					responseCode = conn.getResponseCode();
	   					System.out.println("\nSending 'POST' request to URL : " + url);
	   					System.out.println("Response Code : " + responseCode);
	   					
	   					in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	   					inputLine = "";
	   					response = new StringBuffer();
	   					while ((inputLine = in.readLine()) != null) {
	   						response.append(inputLine);
	   					}
	   					in.close();
	   					System.out.println(response.toString());
	   					
	   				}	
	            	
	            	
	            }catch(Exception e) {
	            	e.printStackTrace();
	            }
	            
	            
	            
	            
	         
			}else {
				
				String customerPhone = map.get("customer_phone").toString().replaceAll("-", "");
				customerPhone = customerPhone.replaceAll(" ", "");
				customerPhone = customerPhone.trim();
				String[] customerPhoneArr = customerPhone.split(";");
				
				ArrayList<String> arrayList = new ArrayList<String>();
				
			
						for(String data : customerPhoneArr){
						    if(!arrayList.contains(data)){
						        arrayList.add(data);
						        
						   }
						}
						
						
					customerPhoneArr = arrayList.toArray(new String[arrayList.size()]);
					customerPhone = "";
					for(int k = 0; k < customerPhoneArr.length; k++) {
						customerPhone += customerPhoneArr[k]+";";
					}
					
				
				urlBuilderAT.append(URLEncoder.encode("rcvphns","UTF-8") + "=" + URLEncoder.encode(customerPhone, "UTF-8") + "&");
				urlBuilderFT.append(URLEncoder.encode("rcvphns","UTF-8") + "=" + URLEncoder.encode(customerPhone, "UTF-8") + "&");
				urlBuilderAT.append(URLEncoder.encode("profileKey","UTF-8") + "=" + URLEncoder.encode("dd3f81c615756d611bd9fb625ca58f2717d4ca15", "UTF-8") + "&");
				urlBuilderFT.append(URLEncoder.encode("profileKey","UTF-8") + "=" + URLEncoder.encode("dd3f81c615756d611bd9fb625ca58f2717d4ca15", "UTF-8") + "&");
		
				URL url = new URL(urlBuilderAT.toString());
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setDoOutput(true);
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Type", "application/json");        
				conn.setDoOutput(true);
					
				OutputStream os = conn.getOutputStream();
				os.flush();
				os.close();
				int responseCode = conn.getResponseCode();
				System.out.println("\nSending 'POST' request to URL : " + url);
				System.out.println("Response Code : " + responseCode);
					
				BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String inputLine = "";
				StringBuffer response = new StringBuffer();
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				System.out.println(response.toString());
				Object json =response.toString();				
				JSONParser jsonParser = new JSONParser();
				JSONObject jsonObject = null;
				if(json instanceof JSONObject){					
					JSONArray list = (JSONArray)jsonParser.parse(response.toString());
					jsonObject = (JSONObject)list.get(0);	
				}else {
					String str = "{\"code\":\""+response.toString()+"\"}";				 
					jsonObject =  (JSONObject)jsonParser.parse(str);					
				}
								
				jsonObjectForSms = jsonObject;
				
			}
			
			//친구톡과 알림톡이 전부 실패 한경우 
			//if(jsonObjectForSms.get("code").toString().equals("fail")  || !map.get("gubun").toString().equals("9")) {
			if(jsonObjectForSms.get("code").toString().equals("fail")) {
				lmsSendMessage(map);
			}
			
			
		}
		
		}else{
			
			
			
			lmsSendMessage(map);
			
			
		}

		
		
        return "jsonview";
}


	
	
	public static String fileToBinary(File file) {
	    String out = new String();
	    FileInputStream fis = null;
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	 
	    try {
	        fis = new FileInputStream(file);
	    } catch (FileNotFoundException e) {
	    	e.printStackTrace();
	        System.out.println("Exception position : FileUtil - fileToString(File file)");
	    }
	 
	    int len = 0;
	    byte[] buf = new byte[1024];
	    try {
	        while ((len = fis.read(buf)) != -1) {
	            baos.write(buf, 0, len);
	        }
	 
	        byte[] fileArray = baos.toByteArray();
	        out = new String(base64Enc(fileArray));
	        fis.close();
	        baos.close();
	        
	        
	        StringBuilder urlBuilder = new StringBuilder(histUrl+"/sms/add_msg_image");
	        urlBuilder.append("?");
	        urlBuilder.append(URLEncoder.encode("image","UTF-8") + "=" + out);
	        
	        URL url = new URL(urlBuilder.toString());
	        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	        conn.setDoOutput(true);
	        conn.setRequestMethod("POST");
	        conn.setRequestProperty("Content-Type", "multipart/form-data");
	        conn.setRequestProperty("HIST-uid", BaseAppConstants.COMPANY_TYPE_NATIONAL);
	        conn.setRequestProperty("HIST-profileKey", "dd3f81c615756d611bd9fb625ca58f2717d4ca15");
	        conn.setDoOutput(true);

	        OutputStream os = conn.getOutputStream();
	        os.flush();
	        os.close();
	        
	        int responseCode = conn.getResponseCode();
	        System.out.println("\nSending 'POST' request to URL : " + url);
	        System.out.println("Response Code : " + responseCode);
	        
	        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	        String inputLine;
	        StringBuffer response = new StringBuffer();

	        while ((inputLine = in.readLine()) != null) {
	            response.append(inputLine);
	        }
	        in.close();
	        System.out.println(response.toString());
	        
	        
	    } catch (IOException e) {
	        System.out.println("Exception position : FileUtil - fileToString(File file)");
	    }
	 
	    return out;
	}


	public static byte[] base64Enc(byte[] buffer) {
	    return Base64.encodeBase64(buffer);
	}

	
	public String fileUpload(File file) throws Exception {
		
		String response = "";
		
		String imgOriginalPath = file.getAbsolutePath() ;         //원본 이미지 경로
        String imgTargetPath =rootDir+"/allocationSend/"+file.getName();            //새 이미지 파일명
        String ext = file.getName().substring(file.getName().lastIndexOf(".")+1,file.getName().length());
        String imgFormat       = ext;
        int newWidth        =720;
        int newHeight       =720;
        String mainPosition    =   "";
        
        BufferedImage image;
        int imageWidth;
        int imageHeight;
        double ratio;
        int w;
        int h;
		
		try {
			
			String urlApi = histUrl+"/sms/add_msg_image";
			
			Map<String, String> headers = new HashMap<String, String>();
	        headers.put("HIST-uid", BaseAppConstants.COMPANY_TYPE_NATIONAL);
	        headers.put("HIST-profileKey", "dd3f81c615756d611bd9fb625ca58f2717d4ca15");
	        
	        MultipartUtility multipartUtility = new MultipartUtility(urlApi, headers);

	        // File part
	        //File file = new File("/img/test.jpg");        // 업로드할 이미지
	        multipartUtility.addFilePart("image", file);

	        response = multipartUtility.finish();
	        System.out.println("response : " + response);
			
	        JSONParser jsonParser = new JSONParser();
	         JSONObject jsonObject = (JSONObject) jsonParser.parse(response);
	        
	        //String fileName = file.getName();
	        //String ext = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length());
	        if(!jsonObject.get("code").toString().equals("success")) {
                
                try {
                   
                   if(jsonObject.get("code").toString().equals("fail")) {      
                      
                      //사진 사이즈 조정 후 재 전송
                      image =ImageIO.read(new File(imgOriginalPath));
                      imageWidth = image.getWidth(null);
                      imageHeight= image.getHeight(null);
                      
                     
                      w= newWidth;
                      h= newHeight;
                         
                     
                      
                      Image resizeImage = image.getScaledInstance(w, h, image.SCALE_SMOOTH);
                      
                      //변경 이미지 저장 
                      BufferedImage newImage = new BufferedImage(w, h,BufferedImage.TYPE_INT_RGB);
                    Graphics g = newImage.getGraphics();
                      g.drawImage(resizeImage, 0,0,null);
                      g.dispose();
                      
                      File saveDir = new File(rootDir+"/allocationSend");
                      
                      if(saveDir.exists()==false) {
                         saveDir.mkdir();
                      }
                   
                      File newFile = new File(imgTargetPath);
                      
                      ImageIO.write(newImage, imgFormat,newFile);
                      //this.fileUpload(newFile);
                    
                      multipartUtility = new MultipartUtility(urlApi, headers);
                      multipartUtility.addFilePart("image", newFile);
                      response = multipartUtility.finish();
                      System.out.println("response : " + response);
                      
                   }else {
                      
                   }   
                   
                }catch(Exception e) {
                   e.printStackTrace();
                   //나중에 생각 하고......
                }
                
             }else {
                return response;
             }
         
	        
	        
	        
	        
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return response;
		
	} 
	
	


	//문자발송
	public String lmsSendMessage(Map<String, Object> map)throws Exception{
        
        
		StringBuilder urlBuilderAT = new StringBuilder(histUrl+"/sms/histsms");
		urlBuilderAT.append("?");
		urlBuilderAT.append(URLEncoder.encode("uid","UTF-8") + "=" + URLEncoder.encode(BaseAppConstants.COMPANY_TYPE_NATIONAL, "UTF-8") + "&");
		String resultMsg="";
		//친구톡과 알림톡이 전부 실패 한경우 
	//	if(jsonObjectForSms.get("code").toString().equals("fail")  || !map.get("gubun").toString().equals("9")) {
			
			//이룸 남궁삼 010-5208-0297 //옵티무빙월드와이드 정성주 010-5509-7678
			String customerPhone = map.get("customer_phone").toString().replaceAll("-", "");
			customerPhone = customerPhone.replaceAll(" ", "");
			customerPhone = customerPhone.trim();
			String[] customerPhoneArr = customerPhone.split(";");
			
			
			ArrayList<String> arrayList = new ArrayList<String>();
			
		
					for(String data : customerPhoneArr){
					    if(!arrayList.contains(data)){
					        arrayList.add(data);
					        
					   }
					}
					
					
				customerPhoneArr = arrayList.toArray(new String[arrayList.size()]);
				customerPhone = "";
				for(int k = 0; k < customerPhoneArr.length; k++) {
					customerPhone += customerPhoneArr[k]+";";
				}
				
			
			// 이룸 또는 정성주인경우 LMS로 발송 될 수 있도록 한다..
			//if(customerPhone.contains("01052080297") || customerPhone.contains("01055097678")) {
				
				String forSms = histUrl+"/sms/histmsg"; 
				urlBuilderAT.replace(0, forSms.length(), histUrl+"/sms/histsms");
				
				
				urlBuilderAT.append(URLEncoder.encode("rcvphns","UTF-8") + "=" + URLEncoder.encode(customerPhone, "UTF-8") + "&");
				urlBuilderAT.append(URLEncoder.encode("callback","UTF-8") + "=" + URLEncoder.encode("15775268", "UTF-8") + "&");
				if(map.get("gubun").toString().equals("0")) {
					
					//urlBuilderAT.append(URLEncoder.encode("subject","UTF-8") + "=" + URLEncoder.encode("탁송예약안내", "UTF-8") + "&");
					
					
					resultMsg += "[한국카캐리어] 탁송 예약이 완료 되었습니다.\r\n";
					resultMsg += "안녕하세요.\r\n";
					resultMsg += "대한민국 NO.1 차량 탁송 전문업체 한국카캐리어(주) 입니다.\r\n";
					resultMsg += "\r\n";
					resultMsg += "어플리케이션에서 간편예약을 해 주시면 탁송이 진행 되는 과정에 대해 어플리케이션에서 확인 할 수 있습니다.\r\n";
					resultMsg += "어플리케이션에서 간편예약을 하여 탁송된 건에 대해서는 추후 마일리지가 적립 되어 사용 할 수 있습니다.\r\n";
					resultMsg += "\r\n";
					resultMsg += "한국카캐리어 어플리케이션을 설치 하면\r\n";
					resultMsg += "1. 탁송금액 조회 및  탁송 관련 카카오톡 문의\r\n";
					resultMsg += "\r\n";
					resultMsg += "앱을 통해 회원가입을 하시면\r\n";
					resultMsg += "1. 탁송 예약 서비스 제공\r\n";
					resultMsg += "2. 탁송 예약 차량 조회 서비스 제공\r\n";
					resultMsg += "3. 예약 조회 및 탁송인수증 조회 서비스 제공\r\n";
					resultMsg += "4. 카카오톡 1:1 문의 서비스 제공\r\n";
					resultMsg += "5. 추후 마일리지 적립 서비스 제공할 예정\r\n";
					resultMsg += "\r\n";
					resultMsg += "구글플레이 및 앱스토어에서 '한국카캐리어'를 검색 해 주세요.\r\n";
					
					urlBuilderAT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode(resultMsg, "UTF-8") + "&");
					urlBuilderAT.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_008", "UTF-8") + "&");
					
					
					
				}else if(map.get("gubun").toString().equals("1")) {
					
					
					//urlBuilderAT.append(URLEncoder.encode("subject","UTF-8") + "=" + URLEncoder.encode("기사배정안내", "UTF-8") + "&");	
					
					
					resultMsg += "차종 : ";
					resultMsg += map.get("car_kind") != null && !map.get("car_kind").toString().equals("") ? map.get("car_kind").toString():"미정";
					resultMsg += "\r\n";
					resultMsg += "차대번호 : ";
					resultMsg += map.get("car_id_num") != null && !map.get("car_id_num").toString().equals("") ?  map.get("car_id_num").toString():"미정";
					resultMsg += "\r\n";
					resultMsg += "출발일 : "+ map.get("departure_dt").toString()+"\r\n";
					resultMsg += "하차지 : "+ map.get("arrival").toString()+"\r\n";
					resultMsg += "기사명 : "+ map.get("driver_name").toString()+"\r\n";
					resultMsg += "연락처 : "+ map.get("phone_num").toString()+"\r\n";
					resultMsg += "해당 차량이 한국카캐리어 "+ map.get("driver_name").toString()+"기사님께 배정 되었습니다."+"\r\n";
					resultMsg += "상세 정보 부탁 드립니다. \r\n";
				
					urlBuilderAT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode(resultMsg, "UTF-8") + "&");
					urlBuilderAT.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_000", "UTF-8") + "&");
					
				
				}
				
				URL url = new URL(urlBuilderAT.toString());
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setDoOutput(true);
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Type", "application/json");        
				conn.setDoOutput(true);
					
				OutputStream os = conn.getOutputStream();
				os.flush();
				os.close();
				int responseCode = conn.getResponseCode();
				System.out.println("\nSending 'POST' request to URL : " + url);
				System.out.println("Response Code : " + responseCode);
					
				BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String inputLine = "";
				StringBuffer response = new StringBuffer();
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				System.out.println(response.toString());
				
				
			//}
			
			
	//	}
		
		
		
		
		return "";
		
		
		
	}
	
	
	
	
	
}
