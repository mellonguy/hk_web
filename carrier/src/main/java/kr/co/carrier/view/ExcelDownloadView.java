package kr.co.carrier.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelDownloadView extends AbstractExcelView {

	@Override
	protected void buildExcelDocument(Map<String, Object> model,
			SXSSFWorkbook workbook, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Sheet sheet = workbook.createSheet();

		String excelName = (String)model.get("excelName");
		String forSummary = (String)model.get("forSummary") != null && !model.get("forSummary").toString().equals("") ? model.get("forSummary").toString() : "";
		
		if(forSummary.equals("")) {
		
		
		List<Map<String, Object>> dataList = (List<Map<String, Object>>)model.get("list");
		String varNameList[] = (String[])model.get("varNameList");
		String titleNameList[] = (String[])model.get("titleNameList");
		
		String forSum = (String)model.get("forSum") != null && !model.get("forSum").toString().equals("") ? model.get("forSum").toString() : ""; 
		
		Row row = null;
        Cell cell = null;
        int r = 0;
        int c = 0;
        
        long supply = 0L;
        long vat = 0L;
        long total = 0L;
        
        CellStyle style = workbook.createCellStyle();
        style.setFillForegroundColor(IndexedColors.SKY_BLUE.index);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setBorderRight(CellStyle.BORDER_THIN);
        style.setBorderLeft(CellStyle.BORDER_THIN);
        style.setBorderTop(CellStyle.BORDER_THIN);
        style.setBorderBottom(CellStyle.BORDER_THIN);
        
      //합계쪽 변경할 스타일 적용
        CellStyle styleBot = workbook.createCellStyle();
        styleBot.setFillForegroundColor(IndexedColors.WHITE.index);
        styleBot.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styleBot.setAlignment(CellStyle.ALIGN_CENTER);
        styleBot.setBorderRight(CellStyle.BORDER_THIN);
        styleBot.setBorderLeft(CellStyle.BORDER_THIN);
        styleBot.setBorderTop(CellStyle.BORDER_THIN);
        styleBot.setBorderBottom(CellStyle.BORDER_THIN);
        
        XSSFDataFormat df = (XSSFDataFormat) workbook.createDataFormat();
        
        
        CellStyle styleBotNum = workbook.createCellStyle();
        styleBotNum.setDataFormat(df.getFormat("#,###"));
        styleBotNum.setFillForegroundColor(IndexedColors.WHITE.index);
        styleBotNum.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styleBotNum.setAlignment(CellStyle.ALIGN_RIGHT);
        styleBotNum.setBorderRight(CellStyle.BORDER_THIN);
        styleBotNum.setBorderLeft(CellStyle.BORDER_THIN);
        styleBotNum.setBorderTop(CellStyle.BORDER_THIN);
        styleBotNum.setBorderBottom(CellStyle.BORDER_THIN);
        
        CellStyle styleNum = workbook.createCellStyle();
        styleNum.setDataFormat(df.getFormat("#,###"));
        styleNum.setAlignment(CellStyle.ALIGN_RIGHT);
        styleNum.setBorderRight(CellStyle.BORDER_THIN);
        styleNum.setBorderLeft(CellStyle.BORDER_THIN);
        styleNum.setBorderTop(CellStyle.BORDER_THIN);
        styleNum.setBorderBottom(CellStyle.BORDER_THIN);
        
        
        //Create header cells
        row = sheet.createRow(r++);
        
        /*
        Iterator<String> iteratorKey = excelMap.keySet( ).iterator( );
		while(iteratorKey.hasNext()){
			String key = iteratorKey.next();
			//System.out.println(key+","+excelMap.get(key));
	        cell = row.createCell(c++);
	        cell.setCellStyle(style);
	        cell.setCellValue(excelMap.get(key));
		}
		*/
		
        //title
		for(int i=0; i<titleNameList.length; i++)
		{
			cell = row.createCell(c++);
	        cell.setCellStyle(style);
	        cell.setCellValue(titleNameList[i]);
		}

        for ( Map<String, Object> dataMap:dataList ) {
        	row = sheet.createRow(r++);
            c = 0;
            
            for(int i=0; i<varNameList.length; i++)
    		{
            	//row.createCell(c++).setCellValue(dataMap.get(varNameList[i])+"");
            	
            	if(forSum.equals("Y")) {
            		cell = row.createCell(c++);
                	if(i >= 13 && i < 16) {
                		cell.setCellStyle(styleNum);
                		String value = dataMap.get(varNameList[i]).toString().replaceAll(",", "");
                		cell.setCellValue(Integer.parseInt(value));
                		if(i==13) {
                			supply += Integer.parseInt(value);
                		}else if(i==14) {
                			vat += Integer.parseInt(value);
                		}else if(i==15) {
                			total += Integer.parseInt(value);
                		}
                	}else {
                		cell.setCellStyle(styleBot);
                		cell.setCellValue(dataMap.get(varNameList[i])+"");
                	}	
            	
            	}else {
            		row.createCell(c++).setCellValue(dataMap.get(varNameList[i])+"");
            	}
            	
            	
    		}
            
            /*
            Iterator<String> iteratorKey2 = excelMap.keySet( ).iterator( );
			while(iteratorKey2.hasNext()){
				String key = iteratorKey2.next();
				row.createCell(c++).setCellValue(dataMap.get(key)+"");
			}
			*/
        }
        
        for(int i = 0 ; i < titleNameList.length; i++)
        {
//            sheet.autoSizeColumn(i, true);
            sheet.autoSizeColumn(i);
            sheet.setColumnWidth(i, (sheet.getColumnWidth(i))+512 );

        }
        
        
        if(forSum.equals("Y")) {
	      //한줄 추가
	        row = sheet.createRow(r++);
	        
	        //셀에 스타일 적용 하고
	        for(int i=0; i<titleNameList.length; i++){
				cell = row.createCell(i);
		        cell.setCellStyle(styleBot);
			}
	        
	        //셀을 병합 한다.
	        sheet.addMergedRegion(new CellRangeAddress(r-1,r-1,0,12));
	        
	        row.getCell(0).setCellValue("합        계");
	        
	        cell = row.getCell(13);
	        cell.setCellStyle(styleBotNum);
	        cell.setCellValue(supply);
	        
	        cell = row.getCell(14);
	        cell.setCellStyle(styleBotNum);
	        cell.setCellValue(vat);
	        
	        cell = row.getCell(15);
	        cell.setCellStyle(styleBotNum);
	        cell.setCellValue(total);
	        
        }
        
        
	}else {
		
		File test = (File)model.get("file");
		
			/*
			 * XSSFWorkbook wb = null; wb = new XSSFWorkbook(new FileInputStream(file));
			 */
		
		InputStream inputStream = new FileInputStream(test);
        byte[] bytes = IOUtils.toByteArray(inputStream);
        int pictureIdx = workbook.addPicture(bytes, XSSFWorkbook.PICTURE_TYPE_PNG);
        inputStream.close();
        CreationHelper helper = workbook.getCreationHelper();
        Drawing drawing = sheet.createDrawingPatriarch();
        ClientAnchor anchor = helper.createClientAnchor();
        	
        // 이미지를 출력할 CELL 위치 선정
        anchor.setCol1(0);
        anchor.setRow1(0);
        
        // 이미지 그리기
        Picture pict = drawing.createPicture(anchor, pictureIdx);
        
        // 이미지 사이즈 비율 설정
        pict.resize();
		
	}
        
        
        
        
        
        String header = getBrowser(request);

        if (header.contains("MSIE")) {
               String docName = URLEncoder.encode(excelName,"UTF-8").replaceAll("\\+", "%20");
               response.setHeader("Content-Disposition", "attachment;filename=" + docName + ";");
        } else if (header.contains("Firefox")) {
               String docName = new String(excelName.getBytes("UTF-8"), "ISO-8859-1");
               response.setHeader("Content-Disposition", "attachment; filename=\"" + docName + "\"");
        } else if (header.contains("Opera")) {
               String docName = new String(excelName.getBytes("UTF-8"), "ISO-8859-1");
               response.setHeader("Content-Disposition", "attachment; filename=\"" + docName + "\"");
        } else if (header.contains("Chrome")) {
               String docName = new String(excelName.getBytes("UTF-8"), "ISO-8859-1");
               response.setHeader("Content-Disposition", "attachment; filename=\"" + docName + "\"");
        }
        response.setHeader("Content-Type", "application/octet-stream");
        response.setHeader("Content-Transfer-Encoding", "binary;");
        response.setHeader("Pragma", "no-cache;");
        response.setHeader("Expires", "-1;");
        response.setContentType("Application/Msexcel");
       // response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName);
	}
	
	
	private String getBrowser(HttpServletRequest request) {

        String header =request.getHeader("User-Agent");
        if (header.contains("MSIE")) {
               return "MSIE";
        } else if(header.contains("Chrome")) {
               return "Chrome";
        } else if(header.contains("Opera")) {
               return "Opera";
        }
        return "Firefox";

  }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
