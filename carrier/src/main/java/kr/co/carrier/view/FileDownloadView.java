package kr.co.carrier.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.servlet.view.AbstractView;

import kr.co.carrier.service.CustomerService;
import kr.co.carrier.service.DriverDeductFileService;
import kr.co.carrier.service.DriverDeductInfoService;
import kr.co.carrier.service.DriverService;
import kr.co.carrier.service.PaymentInfoService;
import kr.co.carrier.service.PersonInChargeService;
import kr.co.carrier.utils.WebUtils;
 
public class FileDownloadView extends AbstractView {
 
	
	@Value("#{appProp['upload.base.path']}")
    private String rootDir;    
    private String subDir = "bbs";	
	
	@Autowired
    private DriverDeductInfoService driverDeductInfoService;
    
	@Autowired
	private DriverDeductFileService driverDeductFileService;
	
	@Autowired
    private DriverService driverService;
	
	@Autowired
    private PaymentInfoService paymentInfoService;
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private PersonInChargeService personInChargeService;
	
	
    public FileDownloadView() {
        // 객체가 생성될 때 Content Type을 다음과 같이 변경 
        setContentType("application/download; charset=utf-8");
    }
 
    @Override
    protected void renderMergedOutputModel(
            Map<String, Object> model, 
            HttpServletRequest request, 
            HttpServletResponse response
            ) throws Exception {
         
        Map<String, Object> fileInfo = (Map<String, Object>) model.get("downloadFile"); // 넘겨받은 모델(파일 정보)
         
        String fileUploadPath = (String) fileInfo.get("fileUploadPath");    // 파일 업로드 경로
        String fileLogicName = (String) fileInfo.get("fileLogicName");      // 파일 논리명(화면에 표시될 파일 이름)
        String filePhysicName = (String) fileInfo.get("filePhysicName");    // 파일 물리명(실제 저장된 파일 이름)
        
        File file = new File(fileUploadPath, filePhysicName);
        XSSFWorkbook wb = null;
        
        response.setContentType(getContentType());
        response.setContentLength((int) file.length());
        String userAgent = request.getHeader("User-Agent");
        boolean ie = userAgent.indexOf("MSIE") > -1;
        String fileName = null;
 
        if(ie) {
            fileName = URLEncoder.encode(fileLogicName, "utf-8");
        } else {
            fileName = new String(fileLogicName.getBytes("utf-8"), "iso-8859-1");
        }
 
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\";");
        response.setHeader("Content-Transfer-Encoding", "binary");
        OutputStream out = response.getOutputStream();
        FileOutputStream fileOut = null;
        FileInputStream fis = null;
        
        //파일을 가져온 후 (운송내역서를 다운로드 받는 경우 운송내역서를 수정 한 후 다운로드 할 수 있도록 한다.)
        if(filePhysicName.equals("deductlist.xlsx")) {
        	
        	String driverId = (String) fileInfo.get("driverId");      
            String selectMonth = (String) fileInfo.get("selectMonth");
            String year = selectMonth.split("-")[0]; 
            String month = selectMonth.split("-")[1];
            
            Map<String, Object> map = new HashMap<String, Object>();
			map.put("driverId",driverId);
			Map<String, Object> driver = driverService.selectDriver(map);
			String startDt = "";
			String endDt = "";
			
			startDt = selectMonth;
			startDt = startDt.split("-")[0]+"-"+startDt.split("-")[1]+"-01";
			endDt = selectMonth;
			endDt = startDt.split("-")[0]+"-"+startDt.split("-")[1]+"-"+WebUtils.getLastDate("yyyy-mm-dd",Integer.parseInt(startDt.split("-")[0]),Integer.parseInt(startDt.split("-")[1]));
			map.put("startDt", startDt);
			map.put("endDt", endDt);
			map.put("selectMonth", selectMonth);
			map.put("startRownum", 0);
			map.put("numOfRows", 1);
			
			List<Map<String, Object>> driverList = driverDeductInfoService.selectDriverList(map);
			
			
			int total = Integer.parseInt(driverList.get(0).get("another_sum").toString().replaceAll(",", ""));
			int deduction = Integer.parseInt(driver.get("deduction_rate").toString())*total/100;
			
			map.put("item", "1");
			map.put("decideMonth", selectMonth);
			String item1 = driverDeductInfoService.selectDriverDeductInfoByItem(map).get("amount").toString();
			map.put("item1", item1);
			Map<String, Object> ddAmount = paymentInfoService.selectAnotherCalDDInfo(map);
			map.put("ddAmount", ddAmount);
			map.put("item", "2");
			String item2 = driverDeductInfoService.selectDriverDeductInfoByItem(map).get("amount").toString();
			map.put("item2", item2);
			
			
			XSSFRow row;
			XSSFCell cell;
			try {//엑셀 파일 오픈
				wb = new XSSFWorkbook(new FileInputStream(file));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			XSSFSheet sheet = wb.getSheetAt(0);         
	        
			CellStyle style = wb.createCellStyle();
			style.setFillForegroundColor(IndexedColors.PALE_BLUE.getIndex());
	        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
	        style.setAlignment(CellStyle.ALIGN_CENTER);
	        style.setBorderBottom(CellStyle.BORDER_DOUBLE);
	        style.setBorderTop(CellStyle.BORDER_DOUBLE);
	        style.setBorderLeft(CellStyle.BORDER_DOUBLE);
	        style.setBorderRight(CellStyle.BORDER_DOUBLE);
			
	        int rows = sheet.getLastRowNum();
			int cells = sheet.getRow(0).getLastCellNum();
			
			FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
			
			 for (int r = 2; r <= rows; r++) {
				 
				 FormulaEvaluator formulaEval = wb.getCreationHelper().createFormulaEvaluator();
				 
		        	row = sheet.getRow(r); // row 가져오기
		        	if (row != null) {
		        		for (int c = 0; c < cells; c++) {
		        			cell = row.getCell(c);
		        			if (cell != null) { 
		        				String value = "";

								switch (cell.getCellType()) {
								   	case XSSFCell.CELL_TYPE_FORMULA:
								   		
								   		CellValue evaluate = formulaEval.evaluate(cell);
								   	  if( evaluate != null ) {
								   		  
								   		  try {
								   			  
								   			  if(WebUtils.isNumber(evaluate.formatAsString())) {
								   				  
								   				value=  String.valueOf(new Double(evaluate.getNumberValue()).intValue());
								   				   
								   			  }else {
								   				  
								   				  value = evaluate.formatAsString();
								   			  }
								   			  
								   		  }catch(Exception e) {
								   			  e.printStackTrace();
								   			  
								   		  }
								   		  
								   		  
			        				   }else {
			        					   
			        					   value = "";
			        				   }
								   		
								   		/*
								   		if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_NUMERIC){
								   			value = "" + (int)cell.getNumericCellValue();
								   		}else if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_STRING){
								   			value = "" + cell.getStringCellValue(); 
								   		}
								   		value = cell.getCellFormula();
								   		*/
								   		
								   		break;
								   	case XSSFCell.CELL_TYPE_NUMERIC:
								   		value = "" + (int)cell.getNumericCellValue();
								   		break;
								   	case XSSFCell.CELL_TYPE_STRING:
								   		value = "" + cell.getStringCellValue();
								   		break;
								   	case XSSFCell.CELL_TYPE_BLANK:
								   		value = "";
								   		break;
								   	case XSSFCell.CELL_TYPE_ERROR:
								   		value = "" + cell.getErrorCellValue();
								   		break;
								   	default:
								}
								
								if(value != null && !value.equals("")) {
									value = value.trim();
								}
								int nIndex = cell.getColumnIndex();
								
							if(r==3) {
								if(nIndex == 8){
									String test  = value;
									test = test.replace("0000", year);
									test = test.replace("00", month);
									cell = row.getCell(8);
			        				cell.setCellValue(test);
								}
							}else if(r==4) {
								if(nIndex == 8){
									if(driver.get("provide_kind") != null && !driver.get("provide_kind").toString().equals("")) {
										if(driver.get("provide_kind").toString().equals("A")) {
											String test = WebUtils.getNextMonth(Integer.parseInt(year),Integer.parseInt(month));
											value = value.replace("AA", test.split("-")[0].substring(2));
											value = value.replace("BB", test.split("-")[1]);
											value = value.replace("CC", WebUtils.getLastDate("dd",Integer.parseInt(test.split("-")[0]),Integer.parseInt(test.split("-")[1])));
										}else if(driver.get("provide_kind").toString().equals("B")) {
											String test = WebUtils.getNextNextMonth(Integer.parseInt(year),Integer.parseInt(month));
											value = value.replace("AA", test.split("-")[0].substring(2));
											value = value.replace("BB", test.split("-")[1]);
											value = value.replace("CC", "05");
										}else if(driver.get("provide_kind").toString().equals("C")) {
											String test = WebUtils.getNextNextMonth(Integer.parseInt(year),Integer.parseInt(month));
											value = value.replace("AA", test.split("-")[0].substring(2));
											value = value.replace("BB", test.split("-")[1]);
											value = value.replace("CC", "07");
										}else if(driver.get("provide_kind").toString().equals("D")) {
											String test = WebUtils.getNextNextMonth(Integer.parseInt(year),Integer.parseInt(month));
											value = value.replace("AA", test.split("-")[0].substring(2));
											value = value.replace("BB", test.split("-")[1]);
											value = value.replace("CC", "10");
										}else if(driver.get("provide_kind").toString().equals("E")) {
											String test = WebUtils.getNextNextMonth(Integer.parseInt(year),Integer.parseInt(month));
											value = value.replace("AA", test.split("-")[0].substring(2));
											value = value.replace("BB", test.split("-")[1]);
											value = value.replace("CC", "15");
										}else if(driver.get("provide_kind").toString().equals("F")) {
											String test = WebUtils.getNextNextMonth(Integer.parseInt(year),Integer.parseInt(month));
											value = value.replace("AA", test.split("-")[0].substring(2));
											value = value.replace("BB", test.split("-")[1]);
											value = value.replace("CC",WebUtils.getLastDate("dd",Integer.parseInt(test.split("-")[0]),Integer.parseInt(test.split("-")[1])));
										}else if(driver.get("provide_kind").toString().equals("G")) {
											String test = WebUtils.getNextMonth(Integer.parseInt(year),Integer.parseInt(month));
											value = value.replace("AA", test.split("-")[0].substring(2));
											value = value.replace("BB", test.split("-")[1]);
											value = value.replace("CC", "15");
										}
									}else {
										
									}
									cell = row.getCell(8);
			        				cell.setCellValue(value);
								}
							}else if(r==5) {	//차량번호
								if(nIndex == 3){
									cell = row.getCell(3);
			        				cell.setCellValue(driver.get("car_num").toString());
								}
							}else if(r==6) {	//성명
								if(nIndex == 3){
									cell = row.getCell(3);
			        				cell.setCellValue(driver.get("driver_name").toString());
								}	
							}else if(r==7) {	//연락처
								if(nIndex == 3){
									cell = row.getCell(3);
			        				cell.setCellValue(driver.get("phone_num").toString());
								}
							}else if(r==11) {	//항목,금액
								if(nIndex == 1){
									cell = row.getCell(1);
			        				cell.setCellValue(month+"월 운송비");
								}else if(nIndex == 3){
									cell = row.getCell(3);
			        				cell.setCellValue(Integer.parseInt(driverList.get(0).get("another_sum").toString().replaceAll(",", "")));
								}
								
								if(nIndex == 7){
									cell = row.getCell(7);
			        				cell.setCellValue("대납내역공제");
								}else if(nIndex == 9){
									cell = row.getCell(9);
			        				cell.setCellValue(Integer.parseInt(item2.replaceAll(",", "")));
								}
								
							}else if(r==17) {	//항목,금액
								if(nIndex == 1){
									cell = row.getCell(1);
									//double val = Double.parseDouble(driver.get("deduction_rate").toString())/100;
			        				cell.setCellValue(Double.parseDouble(driver.get("deduction_rate").toString())/100);
								}/*else if(nIndex == 3){
									cell = row.getCell(3);
			        				cell.setCellValue(Integer.parseInt(ddAmount.get("ddAmount").toString().replaceAll(",", "")));
								}*/
							}else if(r==18) {	//항목,금액
								if(nIndex == 1){
									cell = row.getCell(1);
			        				cell.setCellValue("현장수금");
								}else if(nIndex == 3){
									cell = row.getCell(3);
			        				cell.setCellValue(Integer.parseInt(ddAmount.get("ddAmount").toString().replaceAll(",", "")));
								}
							}else if(r==19) {	//항목,금액
								if(nIndex == 1){
									cell = row.getCell(1);
			        				cell.setCellValue("청구내역공제");
								}else if(nIndex == 3){
									cell = row.getCell(3);
			        				cell.setCellValue(Integer.parseInt(item1.replaceAll(",", "")));
								}
							}else if(r==20) {	//항목,금액
						//		if(nIndex == 1){
						//			cell = row.getCell(1);
			        	//			cell.setCellValue("지입료");
						//		}else if(nIndex == 3){
						//			cell = row.getCell(3);
			        	//			cell.setCellValue(Integer.parseInt(driver.get("driver_balance").toString().replaceAll(",", "")));
						//		}
							}
							
								
		        			} else {
		        				//System.out.print("[null]\t");
		        			}
		        		} // for(c) 문
		        		

		        	}
		        	
		        }
			
			 
			 XSSFFormulaEvaluator.evaluateAllFormulaCells(wb);
				wb.write(out);
				wb.close();
        	
				List<Map<String, Object>> driverDeductFileList = driverDeductFileService.selectDriverDeductFileList(map);	
				if(driverDeductFileList.size() > 0) {
					for(int j = 0; j < driverDeductFileList.size(); j++) {
						Map<String, Object> driverDeductFile = driverDeductFileList.get(j);
						File removeFile = new File(rootDir+"/deduct_list"+driverDeductFile.get("driver_deduct_file_path").toString());
						if(removeFile.exists()){
							removeFile.delete();
						}
						driverDeductFileService.deleteDriverDeductFile(driverDeductFile);
					}
				}
				driverDeductFileService.makeDriverDeductFile(map);
        
        
        }else if(filePhysicName.equals("customer_excel.xlsx")) { //파일 다운로드
        	
        	XSSFRow row;
        	XSSFCell cell;
			try {//엑셀 파일 오픈
				wb = new XSSFWorkbook(new FileInputStream(file));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			XSSFSheet sheet = wb.getSheetAt(1);         
	        
			
			row = sheet.getRow(1); // row 가져오기
			
			//파일에 작성 하고 그 파일을 다운로드 하게 되므로 일단 초기화(내용 삭제)를 진행 하고 값이 있는경우 파일에 표시 해 준다.
			cell = row.getCell(2);
			cell.setCellValue("");
			cell = row.getCell(4);
			cell.setCellValue("");
			cell = row.getCell(5);
			cell.setCellValue("");
			cell = row.getCell(6);
			cell.setCellValue("");
			cell = row.getCell(7);
			cell.setCellValue("");
			cell = row.getCell(8);
			cell.setCellValue("");
			cell = row.getCell(9);
			cell.setCellValue("");
			cell = row.getCell(10);
			cell.setCellValue("");
			cell = row.getCell(12);
			cell.setCellValue("");
			cell = row.getCell(13);
			cell.setCellValue("");
			cell = row.getCell(14);
			cell.setCellValue("");
        	
        	Map<String, Object> paramMap = (Map<String,Object>)fileInfo.get("paramMap");
        	
        	String customerId = paramMap.get("customerId") != null && !paramMap.get("customerId").toString().equals("") ?  paramMap.get("customerId").toString() :"";	
        	String personInChargeId = paramMap.get("personInChargeId") != null && !paramMap.get("personInChargeId").toString().equals("") ?  paramMap.get("personInChargeId").toString() :"";  
        	String inputDt =paramMap.get("inputDt").toString();
        	
        	Map<String, Object> customer = null;
        	Map<String, Object> charge = null;
        	
        	if(!customerId.equals("")) {
        		customer = customerService.selectCustomer(paramMap);
        	}
        	if(!personInChargeId.equals("")) {
        		charge = personInChargeService.selectPersonInChargeByPersonInChargeId(paramMap);
        	}
        	
        	if(customer != null) {
        		cell = row.getCell(4);
    			cell.setCellValue(customer.get("customer_name").toString());	
        	}
        	
        	if(charge != null) {
        		cell = row.getCell(5);
    			cell.setCellValue(charge.get("department").toString());
    			cell = row.getCell(6);
    			cell.setCellValue(charge.get("name").toString());
    			cell = row.getCell(12);
    			cell.setCellValue(charge.get("address").toString());
    			cell = row.getCell(13);
    			cell.setCellValue(charge.get("name").toString());
    			cell = row.getCell(14);
    			cell.setCellValue(charge.get("phone_num").toString());
        	}
			if(inputDt != null) {
				cell =row.getCell(2);
				cell.setCellValue(inputDt);
			
			}
        	
        	
			XSSFFormulaEvaluator.evaluateAllFormulaCells(wb);
			
			fileOut = new FileOutputStream(file);
            wb.write(fileOut);
            wb.close();
			
			file = new File(fileUploadPath, filePhysicName);
			
			response.setContentType("application/octet-stream; charset=utf-8");
			response.setContentLength((int) file.length());
			
			
			response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\";");
			response.setHeader("Content-Transfer-Encoding", "binary");
			out = response.getOutputStream();
			
			
			
        }
        
        
        try {
                
                fis = new FileInputStream(file);
               FileCopyUtils.copy(fis, out);
           } finally {
        	   
        	   try {
        		   if(fis != null) {
        			   fis.close();
        		   }
              } catch(IOException e) {
            	  e.printStackTrace();
              }
               out.flush();
               out.close();
           }
          
         
    }
}
