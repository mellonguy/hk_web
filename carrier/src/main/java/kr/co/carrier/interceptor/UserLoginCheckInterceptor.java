package kr.co.carrier.interceptor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import kr.co.carrier.service.AdjustmentService;
import kr.co.carrier.service.AllocationFinishInfoService;
import kr.co.carrier.service.BillPublishRequestService;
import kr.co.carrier.service.CompanyService;
import kr.co.carrier.service.EmpLoginInfoService;
import kr.co.carrier.service.EmployeeService;
import kr.co.carrier.utils.WebUtils;

public class UserLoginCheckInterceptor extends HandlerInterceptorAdapter {
	
	private static final Logger logger = LoggerFactory.getLogger(UserLoginCheckInterceptor.class);
	
	
	@Autowired
    private AllocationFinishInfoService allocationFinishInfoService;
	
	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	BillPublishRequestService billPublishRequestService;
	
	@Autowired
	private CompanyService companyService;
	
	@Autowired
	private EmpLoginInfoService empLoginInfoService;
	
	@Autowired
	 private AdjustmentService adjustmentService;
	
	@SuppressWarnings("unchecked")
	@Override
	 public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object hadler) throws Exception {

		boolean isMobile = WebUtils.isPhone(request.getHeader("User-Agent"));			
		
		Map<String, Object> userSessionMap = (Map<String, Object>)request.getSession().getAttribute("user");
		Map<String, Object> userMap = null;
		
		try{
			
			
			System.out.println("request.getRequestURL()="+request.getRequestURL());
			
			if(userSessionMap == null){
				
				HttpSession session = request.getSession();
								
				
			
				//if(session.isNew()) {
				//}
				
				String status = request.getParameter("status") == null ? "": request.getParameter("status").toString();
								
				//System.out.println("request.getRequestURL()="+request.getRequestURL());
				//System.out.println("status="+status);
				//System.out.println("session.getId()="+session.getId());
												
				if(status.equals("noLogin")) {
					return true;
				}else {
					response.sendRedirect(request.getContextPath()+"/nopermission.do");
					return false;
				}
				
				/*if("POST".equalsIgnoreCase(request.getMethod())){
			    	if(isAjaxRequest(request)){
			    		response.sendError(499);
			    	}else{
			    		response.sendRedirect(request.getContextPath() +"/index.do");
			    		response.sendRedirect(request.getContextPath()+"/nopermission.do");
			    	}
			    	 return false;

				}else{
					String url = SslUtils.toSslUrl(request, request.getRequestURI().toString());
					if ( request.getQueryString() != null ){
						url = url + "?" + request.getQueryString();
					}
					response.sendRedirect(request.getContextPath()+ "/index.do?returnUrl=" + java.net.URLEncoder.encode(url));
					//response.sendRedirect(request.getContextPath()+ "/index.do");
					
					
				return false;
				}*/
				
				
				
				
			}else{
				
				
			}
			
		}catch(Exception e){
			System.out.println("===================preHandle======================");
			e.printStackTrace();
			return false;
		}
				
//		String test = request.getRequestURL().toString();
	
		return true;
		
	 }
	
	
	@SuppressWarnings("unchecked")
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView mav)throws Exception{
		
		
		Map<String, Object> userSessionMap = (Map<String, Object>)request.getSession().getAttribute("user");
		Map<String, Object> userMap = null;
		
		try {
			
			if(userSessionMap != null){
				HttpSession session = request.getSession();
				
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("today", WebUtils.getNow("yyyy-MM-dd"));
				
				List<Map<String, Object>> allocationFinishInfoList = allocationFinishInfoService.selectAllocationFinishInfoList(map);
			//  List<Map<String, Object>> allocationStatusList = null;
				List<Map<String, Object>> socketAlarmTalkList = null;
			//	List<Map<String, Object>> employeeNoteList = null;
				
				String allocationId = request.getParameter("allocationId") == null ? "": request.getParameter("allocationId").toString(); 			
				map.put("allocationId", allocationId);
				String combination = "N";
				
				String hidePrice = request.getParameter("hidePrice")== null ? "": request.getParameter("hidePrice").toString(); 
				
				if(!hidePrice.equals("")) {
					userSessionMap.put("hidePrice", hidePrice);
					
				}
				
				
				
				//combination.do에만 보여지는 알림창 /배차리스트 /세금계산서 
				if(request.getRequestURL().indexOf("/allocation/combination.do") != -1){
				//	allocationStatusList = allocationFinishInfoService.selectAllocationStatusList(map);
					socketAlarmTalkList = allocationFinishInfoService.selectSocketAlarmTalkList(map);	
				//	employeeNoteList =employeeService.selectEmpNote(map);
					int totalCount = adjustmentService.selectLayoutDecideCountY(map);
					int totalCountN =adjustmentService.selectLayoutDecideCountN(map);
				
					mav.addObject("decideCountY", totalCount);	
					mav.addObject("decideCountN", totalCountN );	
					combination="Y";
					
				}
				
				
				
				
	
				
				String date =WebUtils.getNow("yyyy-MM-dd").toString();
				String yoeil = WebUtils.getDateDay(date);
				String result =date.concat(yoeil);
				
				if(mav != null) {
					
					mav.addObject("allocationFinishInfoList", allocationFinishInfoList);	
				//	mav.addObject("allocationStatusListToday", allocationStatusList);	
					mav.addObject("socketAlarmTalkList", socketAlarmTalkList);	
					mav.addObject("today",result);
					mav.addObject("combination",combination);
				//	mav.addObject("employeeNoteList",employeeNoteList);
					
					
				}
				
				
				List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
								
				//HttpSession session = request.getSession();
				String companyId = session.getAttribute("companyId").toString();
				
				  String emp_id = userSessionMap.get("emp_id").toString();
		            String now_dt = WebUtils.getNow("yyyy-MM-dd");
		            
		            map.put("emp_id", emp_id);
		            map.put("now_dt", now_dt);
		            
		            Map <String,Object> deadLineCheck =empLoginInfoService.selectLoginCheck(map);
		            
		            
		            boolean bool = true;
		            
		            
		            if(deadLineCheck == null) {
		               
		               bool = false; }
		            
		             /* if(deadLineCheck.isEmpty()) {
		              
		              bool = false;
		              */
		              
		             session.setAttribute("bool", bool);
				
				
				map.put("publishRequestStatus", "R");
				map.put("companyId", companyId);
				int billPublishRequestCount = billPublishRequestService.selectBillPublishRequestListCount(map);
				
				map.put("empId", userSessionMap.get("emp_id").toString());
				map.put("duplicateChk", "Y");
				userMap = employeeService.selectEmployee(map);
				
				session.setAttribute("listRowCnt", userMap.get("list_row_cnt").toString());
				request.setAttribute("billPublishRequestCount", billPublishRequestCount);
				request.setAttribute("companyList", companyList);
				
			}
			
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}



	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	private boolean isAjaxRequest(HttpServletRequest req) {
        return req.getHeader("AJAX") != null && req.getHeader("AJAX").equals(Boolean.TRUE.toString());
	}
	
	
	
	
	
	
	
}
