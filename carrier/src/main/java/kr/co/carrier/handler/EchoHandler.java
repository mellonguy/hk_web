package kr.co.carrier.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.RemoteEndpoint.Basic;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.handler.TextWebSocketHandler;




@ServerEndpoint(value = "/echo-ws.do")
public class EchoHandler extends TextWebSocketHandler{

	private static final List<Session> sessionList=new ArrayList<Session>();
	private static final List<String> empIdList=new ArrayList<String>();
	
	//private static final List<Map<String,Session>> sessionList=new ArrayList<Map<String,Session>>();
	
	
    private static final Logger logger = LoggerFactory.getLogger(EchoHandler.class);
    public EchoHandler() {
        // TODO Auto-generated constructor stub
        System.out.println("웹소켓(서버) 객체생성");
    }
    
    public EchoHandler(String test) {
        // TODO Auto-generated constructor stub
        System.out.println("웹소켓(서버) 객체생성"+test);
    }
    
    
    
    @OnOpen
    public void onOpen(Session session) {
        //logger.info("Open session id:"+session.getId());
        
   
        Map<String,Object> map = new HashMap<String, Object>();
        
        try {
            final Basic basic=session.getBasicRemote();
            //basic.sendText("서버에 연결 되었습니다.");
            String queryString = session.getQueryString();
	        System.out.println(queryString);
        
	        String[] test = queryString.split("="); 
	     
	        logger.info("Open session id:"+session.getId()+", empId="+test[1]);
	        
        }catch (Exception e) {
            // TODO: handle exception
            System.out.println(e.getMessage());
        }
        
        map.put("sessionList", sessionList);
        sessionList.add(session);
 
       
       
   //     System.out.println(empIdList);
 
    }
    
    /*
     * 모든 사용자에게 메시지를 전달한다.
     * @param self
     * @param message
     */
    //EchoHandler.sendMessage(resultMsg,allocationId,driverId,driverMap.get("driver_name").toString());
    public static void sendMessage(String message,String allocationid, String driverId, String driverName) {
    	
    	try {
    		
    		logger.info("sendMessage"+message);
    		logger.info("sessionList.size = "+EchoHandler.sessionList.size());
    		
    		String result = allocationid+","+driverId+","+driverName;
    		
    		for(Session session : EchoHandler.sessionList) {
    			/*session.getBasicRemote().sendText("sendMessage"+message);*/
    			session.getBasicRemote().sendText(result);
    			
            }
    		
    	}catch(Exception e) {
    		e.printStackTrace();
    	}
    	
    }
    
    //탁송 예약 웹소켓 
	 public static void sendMessage(String allocationid) {
	    	
	    	try {
	    		
	    		logger.info("sessionList.size = "+EchoHandler.sessionList.size());
	    		
	    		String result = "allocationId="+allocationid+"&gubun=C";
	    			System.out.println(result);
	    		for(Session session : EchoHandler.sessionList) {
	    			/*session.getBasicRemote().sendText("sendMessage"+message);*/
	    			session.getBasicRemote().sendText(result);
	    			
	            }
	    		
	    	}catch(Exception e) {
	    		e.printStackTrace();
	    	}
	    	
	    }
    
	 //직원간 쪽지보내기 웹소켓
	 public static void sendNote(String receiveEmpId) {
	    	
	    	try {
	    		
	    		logger.info("sessionList.size = "+EchoHandler.sessionList.size());
	    		
	    		System.out.println(sessionList.size());
	    			String result ="noteAlarm";
	    			//String getId = sessionList.get(0).getId();
	    			//empIdList.add(receiveEmpId);
	    		 	
	    			System.out.println(result);
	    			System.out.println(sessionList);
	    		for(Session session : EchoHandler.sessionList) {
	    		
	    			String queryString = session.getQueryString();
	    	        System.out.println(queryString);
	            
	    	        String[] test = queryString.split("=");
	    	        if(receiveEmpId.equals(test[1])) {
	    	        	session.getBasicRemote().sendText(result);	
	    	        }else {
	    	        	
	    	        }
	    			
	            }
	    		
	    		
	    	}catch(Exception e) {
	    		e.printStackTrace();
	    	}
	    	
	    }
	 
	 
    
    private void sendAllSessionToMessage(Session self,String message) {
        try {
        	
        	/*logger.info("sendAllSessionToMessage "+message.split(",")[1] + ": "+message.split(",")[0]);*/
        	logger.info("sendAllSessionToMessage "+message);
            for(Session session : EchoHandler.sessionList) {
                if(!self.getId().equals(session.getId())) {
                    /*session.getBasicRemote().sendText(message.split(",")[1]+" : "+message);*/
                	session.getBasicRemote().sendText(message);
                }
            }
          // sendMessage();
        }catch (Exception e) {
            // TODO: handle exception
            System.out.println(e.getMessage());
        }
    }
    @OnMessage
    public void onMessage(String message,Session session) {
        /*logger.info("Message From "+message.split(",")[1] + ": "+message.split(",")[0]);*/
    	
    	logger.info("Message From ");
    	
        try {
            final Basic basic=session.getBasicRemote();
            basic.sendText("to : "+message);
        }catch (Exception e) {
            // TODO: handle exception
            System.out.println(e.getMessage());
        }
        sendAllSessionToMessage(session, message);
    }
    @OnError
    public void onError(Throwable e,Session session) {
        
    }
    @OnClose
    public void onClose(Session session) {
        
    	logger.info("Session "+session.getId()+" has ended");
        try {
    //    	session.getBasicRemote().sendText("Connection Closed");	
        }catch(Exception e) {
        	e.printStackTrace();
        }
        
        sessionList.remove(session);
    }

	
	
}
