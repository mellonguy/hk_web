package kr.co.carrier.handler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicInteger;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


//@ServerEndpoint(value = "/echo-ws.do")
public class ChatHandler

{

	private static Map<String, Session> sessionMap = new HashMap<String,Session>();
	private static final Logger logger = LoggerFactory.getLogger(ChatHandler.class);
	private static final String GUEST_PREFIX = "Guest";
	private static final AtomicInteger connectionIds = new AtomicInteger(0);
	private static final Set<ChatHandler> connections = new CopyOnWriteArraySet<ChatHandler>();
	private static List<Session> sessionList = new ArrayList<Session>();
	
	private final String nickname;

	public ChatHandler(){
		logger.debug("create SocketHandler instance!");
		// 클라이언트가 접속할 때마다 서버측에서는 Thread 가 새로 생성되는 것을 확인할 수 있다
		String threadName = "Thread-Name:" + Thread.currentThread().getName();
		nickname = GUEST_PREFIX + connectionIds.getAndIncrement();
		System.out.println(threadName + ", " + nickname);		
	}
	
    @OnOpen 
    public void onOpen(Session session) throws Exception{ 
		// Session:접속자마다 한개의 세션이 생성되어 데이터 통신수단으로 사용됨, 즉 접속자 마다 구분됨
		// 한개의 브라우저에서 여러개의 탭을 사용해서 접속하면 Session은 서로 다르지만 HttpSession 은 동일함
		ChatHandler.sessionList.add(session);
    } 
     
    @OnClose 
    public void onClose(Session session){ 
		ChatHandler.sessionList.remove(session);
    }    
    @OnError
	public void onError(Throwable t) throws Throwable {
    	
	}

    
    @OnMessage 
    public void onMessage(String json,Session session) throws ParseException{ 
    
    		try{
    			if (json == null || json.trim().equals("")){
        			return;	
        		}
    			JSONParser jsonParser = new JSONParser();
                //JSON데이터를 넣어 JSON Object 로 만들어 준다.
                JSONObject jsonObject = (JSONObject) jsonParser.parse(json);	
                String nickName = (String)jsonObject.get("nickname");
                String text = (String)jsonObject.get("text");
                /*Date date = new Date((long)jsonObject.get("date"));
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String putDate = sdf.format(date);*/
                
                String gameId = (String)session.getUserProperties().get("gameId");
                
                if(gameId == null){
                	session.getUserProperties().put("gameId", (String)jsonObject.get("gameId"));	
                	try {
						session.getBasicRemote().sendText((String)jsonObject.get("gameId") + "방에 접속 되었습니다.");
					} catch (IOException e) {
						e.printStackTrace();
					}
                	return;
                }
                for(Session sessions : sessionList){ 
                		try {
                		//	session.getBasicRemote().sendText(nickName + " : " +text + "("+putDate+")");
                			if(sessions.getUserProperties().get("gameId").toString().equals(session.getUserProperties().get("gameId").toString())){						//게임 아이디가 같다면 같은 방에 있는것이므로 게임 아이디가 같은 session에게만 메세지를 보낸다.
                				sessions.getBasicRemote().sendText(nickName + " : " +text);
							}
						} catch (IOException e) {
							e.printStackTrace();
						}	
                } 
                
    		}catch(ParseException e){
    			e.printStackTrace();
    		}
    	
        }
    

    
    
    
}
