package kr.co.carple.vo;

public class FileVO {

	
	
	private String idx;
	private String crfId;
	private String crfCrdId;
	private String crfCorId;
	private String crfFilePath;
	private String crfFileNm;
	private String crfCategoryType;
	private String crfStatus;
	private String crfRegDt;
	public String getIdx() {
		return idx;
	}
	public void setIdx(String idx) {
		this.idx = idx;
	}
	public String getCrfId() {
		return crfId;
	}
	public void setCrfId(String crfId) {
		this.crfId = crfId;
	}
	
	public String getCrfCrdId() {
		return crfCrdId;
	}
	public void setCrfCrdId(String crfCrdId) {
		this.crfCrdId = crfCrdId;
	}
	
	public String getCrfCorId() {
		return crfCorId;
	}
	public void setCrfCorId(String crfCorId) {
		this.crfCorId = crfCorId;
	}
	public String getCrfFilePath() {
		return crfFilePath;
	}
	public void setCrfFilePath(String crfFilePath) {
		this.crfFilePath = crfFilePath;
	}
	public String getCrfFileNm() {
		return crfFileNm;
	}
	public void setCrfFileNm(String crfFileNm) {
		this.crfFileNm = crfFileNm;
	}
	public String getCrfCategoryType() {
		return crfCategoryType;
	}
	public void setCrfCategoryType(String crfCategoryType) {
		this.crfCategoryType = crfCategoryType;
	}
	public String getCrfStatus() {
		return crfStatus;
	}
	public void setCrfStatus(String crfStatus) {
		this.crfStatus = crfStatus;
	}
	public String getCrfRegDt() {
		return crfRegDt;
	}
	public void setCrfRegDt(String crfRegDt) {
		this.crfRegDt = crfRegDt;
	}
	
}
