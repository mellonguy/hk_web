package kr.co.carple.vo;

public class MemberVO {

	
	private String memId;			//아이디
	private String memUuid;		//회원 고유 아이디
	private String memUserId;       //회원 아이디
	private String memEmail;        //회원 이메일
	private String memJoinType;     //회원 가입 유형(self:직접가입,naver:네이버아이디,kakao:카카오아이디,google:구글)
	private String memPassword;     //패스워드
	private String memUserName;     //회원 실명
	private String memNickName;     //회원 닉네임
	private String memGrade;        //회원 등급 N:일반,S:스페셜(기간제),V:VIP(영구)(매일 0시에 스케줄러를 돌려서 S등급의 회원의 기간이 종료 되었는지 확인 하여 등급 조정을 한다.)
	private String memRole;         //회원 권한 N:일반,A:관리자
	private String memStatus;       //회원 상태 0:정상,1:정지,2:차단,3:탈퇴
	private String memLoginEnv;     //회원로그인 환경 web:웹, android:안드로이드, ios:아이폰외
	private String memPoint;        //포인트
	private String memPhone;        //연락처
	private String memPhoneCert;    //폰 인증 상태 N:미인증 Y:인증
	private String memRegDt;        //회원 등록일
	private String memLastloginDt;  //최종 로그인 시간
	private String memAdminMemo;    //특이사항
	private String memDeviceKey;    //디바이스 토큰 또는 쿠키값(푸시 알림 전송 시 사용)
	private String memIcon;         //회원 아이콘 경로
	private String memPhoto;        //회원 이미지 경로(로그인시 화면에서 보여줌)
	private String memBirthDay;
	private String memAccessToken;
	
	
	public String getMemId() {
		return memId;
	}
	public void setMemId(String memId) {
		this.memId = memId;
	}
	public String getMemUuid() {
		return memUuid;
	}
	public void setMemUuid(String memUuid) {
		this.memUuid = memUuid;
	}
	public String getMemUserId() {
		return memUserId;
	}
	public void setMemUserId(String memUserId) {
		this.memUserId = memUserId;
	}
	public String getMemEmail() {
		return memEmail;
	}
	public void setMemEmail(String memEmail) {
		this.memEmail = memEmail;
	}
	public String getMemJoinType() {
		return memJoinType;
	}
	public void setMemJoinType(String memJoinType) {
		this.memJoinType = memJoinType;
	}
	public String getMemPassword() {
		return memPassword;
	}
	public void setMemPassword(String memPassword) {
		this.memPassword = memPassword;
	}
	public String getMemUserName() {
		return memUserName;
	}
	public void setMemUserName(String memUserName) {
		this.memUserName = memUserName;
	}
	public String getMemNickName() {
		return memNickName;
	}
	public void setMemNickName(String memNickName) {
		this.memNickName = memNickName;
	}
	public String getMemGrade() {
		return memGrade;
	}
	public void setMemGrade(String memGrade) {
		this.memGrade = memGrade;
	}
	public String getMemRole() {
		return memRole;
	}
	public void setMemRole(String memRole) {
		this.memRole = memRole;
	}
	public String getMemStatus() {
		return memStatus;
	}
	public void setMemStatus(String memStatus) {
		this.memStatus = memStatus;
	}
	public String getMemLoginEnv() {
		return memLoginEnv;
	}
	public void setMemLoginEnv(String memLoginEnv) {
		this.memLoginEnv = memLoginEnv;
	}
	public String getMemPoint() {
		return memPoint;
	}
	public void setMemPoint(String memPoint) {
		this.memPoint = memPoint;
	}
	public String getMemPhone() {
		return memPhone;
	}
	public void setMemPhone(String memPhone) {
		this.memPhone = memPhone;
	}
	public String getMemPhoneCert() {
		return memPhoneCert;
	}
	public void setMemPhoneCert(String memPhoneCert) {
		this.memPhoneCert = memPhoneCert;
	}
	public String getMemRegDt() {
		return memRegDt;
	}
	public void setMemRegDt(String memRegDt) {
		this.memRegDt = memRegDt;
	}
	public String getMemLastloginDt() {
		return memLastloginDt;
	}
	public void setMemLastloginDt(String memLastloginDt) {
		this.memLastloginDt = memLastloginDt;
	}
	public String getMemAdminMemo() {
		return memAdminMemo;
	}
	public void setMemAdminMemo(String memAdminMemo) {
		this.memAdminMemo = memAdminMemo;
	}
	public String getMemDeviceKey() {
		return memDeviceKey;
	}
	public void setMemDeviceKey(String memDeviceKey) {
		this.memDeviceKey = memDeviceKey;
	}
	public String getMemIcon() {
		return memIcon;
	}
	public void setMemIcon(String memIcon) {
		this.memIcon = memIcon;
	}
	public String getMemPhoto() {
		return memPhoto;
	}
	public void setMemPhoto(String memPhoto) {
		this.memPhoto = memPhoto;
	}
	public String getMemBirthDay() {
		return memBirthDay;
	}
	public void setMemBirthDay(String memBirthDay) {
		this.memBirthDay = memBirthDay;
	}
	public String getMemAccessToken() {
		return memAccessToken;
	}
	public void setMemAccessToken(String memAccessToken) {
		this.memAccessToken = memAccessToken;
	}
	
	
	
}
