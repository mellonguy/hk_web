package kr.co.carple.vo;

public class DriverVO {

	
	
	private String dvrId;						//아이디
	private String dvrUserId;                   //기사 아이디
	private String dvrEmail;                    //기사 이메일
	private String dvrJoinType;                 //회원 가입 유형(N:일반)
	private String dvrPassword;                 //패스워드
	private String dvrUserName;                 //기사 실명
	private String dvrNickName;                 //기사 닉네임
	private String dvrGrade;                    //기사 등급 N:일반,S:스페셜(기간제),V:VIP(영구)(매일 0시에 스케줄러를 돌려서 S등급의 기사의 기간이 종료 되었는지 확인 하여 등급 조정을 한다.)
	private String dvrStatus;                   //기사 상태 0:정상,1:정지,2:차단,3:탈퇴
	private String dvrApproveStatus;            //기사 승인 상태 N:미승인,Y:승인완료
	private String dvrLoginEnv;                 //기사로그인 환경 web:웹, android:안드로이드, ios:아이폰외
	private String dvrPoint;                    //포인트
	private String dvrPhone;                    //연락처
	private String dvrPhoneCert;                //폰 인증 상태 N:미인증 Y:인증
	private String dvrRegDt;                    //기사 등록일시
	private String dvrApproveDt;                //기사 승인일시
	private String dvrLastloginDt;              //최종 로그인 시간
	private String dvrAdminMemo;                //특이사항
	private String dvrDeviceKey;                //디바이스 토큰 또는 쿠키값(푸시 알림 전송 시 사용)
	private String dvrIcon;                     //기사 아이콘 경로
	private String dvrPhoto;                    //기사 이미지 경로(로그인시 화면에서 보여줌)
	private String dvrLicense;                  //면허증번호
	private String dvrCarKind;                  //차종
	private String dvrCarrierType;              //탁송차량 구분 S:세이프티로더,C:5톤캐리어,T:추레라,F:풀카,B:박스카
	private String dvrLat;                      //위도
	private String dvrLng;                      //경도
	private String dvrCompanyName;              //상호명
	private String dvrCeoName;                  //대표자명
	private String dvrBusinessLicenseNumber;    //사업자등록번호
	private String dvrBusinessCondition;        //업태
	private String dvrBusinessKind;             //종목
	private String dvrAddress;                  //사업장주소
	private String dvrAddressDetail;            //사업장주소상세
	private String dvrInsuranceStatus;          //보험가입여부 N:미가입 Y:가입
	private String dvrTemporaryYn;				//임시저장 여부
	private String dvrResidentRegistrationNumberFront;	//주민등록번호 앞
	private String dvrResidentRegistrationNumberRear;	//주민등록번호 뒤
	private String dvrCarNum;						//기사차량번호
	private String dbEncKey;			
	private String dvrAddressPoint;				//사업장 좌표
	private String dvrBondYn;						//보세차량여부
	private String dvrInsuranceStart;			//보험시작일
	private String dvrInsuranceEnd;				//보험 만료일
	private String dvrTransportLicenseNumber;	//화물운송자격면허번호
	private String dvrSimpleIntroduce;
	
	public String getDvrId() {
		return dvrId;
	}
	public void setDvrId(String dvrId) {
		this.dvrId = dvrId;
	}
	public String getDvrUserId() {
		return dvrUserId;
	}
	public void setDvrUserId(String dvrUserId) {
		this.dvrUserId = dvrUserId;
	}
	public String getDvrEmail() {
		return dvrEmail;
	}
	public void setDvrEmail(String dvrEmail) {
		this.dvrEmail = dvrEmail;
	}
	public String getDvrJoinType() {
		return dvrJoinType;
	}
	public void setDvrJoinType(String dvrJoinType) {
		this.dvrJoinType = dvrJoinType;
	}
	public String getDvrPassword() {
		return dvrPassword;
	}
	public void setDvrPassword(String dvrPassword) {
		this.dvrPassword = dvrPassword;
	}
	public String getDvrUserName() {
		return dvrUserName;
	}
	public void setDvrUserName(String dvrUserName) {
		this.dvrUserName = dvrUserName;
	}
	public String getDvrNickName() {
		return dvrNickName;
	}
	public void setDvrNickName(String dvrNickName) {
		this.dvrNickName = dvrNickName;
	}
	public String getDvrGrade() {
		return dvrGrade;
	}
	public void setDvrGrade(String dvrGrade) {
		this.dvrGrade = dvrGrade;
	}
	public String getDvrStatus() {
		return dvrStatus;
	}
	public void setDvrStatus(String dvrStatus) {
		this.dvrStatus = dvrStatus;
	}
	public String getDvrApproveStatus() {
		return dvrApproveStatus;
	}
	public void setDvrApproveStatus(String dvrApproveStatus) {
		this.dvrApproveStatus = dvrApproveStatus;
	}
	public String getDvrLoginEnv() {
		return dvrLoginEnv;
	}
	public void setDvrLoginEnv(String dvrLoginEnv) {
		this.dvrLoginEnv = dvrLoginEnv;
	}
	public String getDvrPoint() {
		return dvrPoint;
	}
	public void setDvrPoint(String dvrPoint) {
		this.dvrPoint = dvrPoint;
	}
	public String getDvrPhone() {
		return dvrPhone;
	}
	public void setDvrPhone(String dvrPhone) {
		this.dvrPhone = dvrPhone;
	}
	public String getDvrPhoneCert() {
		return dvrPhoneCert;
	}
	public void setDvrPhoneCert(String dvrPhoneCert) {
		this.dvrPhoneCert = dvrPhoneCert;
	}
	public String getDvrRegDt() {
		return dvrRegDt;
	}
	public void setDvrRegDt(String dvrRegDt) {
		this.dvrRegDt = dvrRegDt;
	}
	public String getDvrApproveDt() {
		return dvrApproveDt;
	}
	public void setDvrApproveDt(String dvrApproveDt) {
		this.dvrApproveDt = dvrApproveDt;
	}
	public String getDvrLastloginDt() {
		return dvrLastloginDt;
	}
	public void setDvrLastloginDt(String dvrLastloginDt) {
		this.dvrLastloginDt = dvrLastloginDt;
	}
	public String getDvrAdminMemo() {
		return dvrAdminMemo;
	}
	public void setDvrAdminMemo(String dvrAdminMemo) {
		this.dvrAdminMemo = dvrAdminMemo;
	}
	public String getDvrDeviceKey() {
		return dvrDeviceKey;
	}
	public void setDvrDeviceKey(String dvrDeviceKey) {
		this.dvrDeviceKey = dvrDeviceKey;
	}
	public String getDvrIcon() {
		return dvrIcon;
	}
	public void setDvrIcon(String dvrIcon) {
		this.dvrIcon = dvrIcon;
	}
	public String getDvrPhoto() {
		return dvrPhoto;
	}
	public void setDvrPhoto(String dvrPhoto) {
		this.dvrPhoto = dvrPhoto;
	}
	public String getDvrLicense() {
		return dvrLicense;
	}
	public void setDvrLicense(String dvrLicense) {
		this.dvrLicense = dvrLicense;
	}
	public String getDvrCarKind() {
		return dvrCarKind;
	}
	public void setDvrCarKind(String dvrCarKind) {
		this.dvrCarKind = dvrCarKind;
	}
	public String getDvrCarrierType() {
		return dvrCarrierType;
	}
	public void setDvrCarrierType(String dvrCarrierType) {
		this.dvrCarrierType = dvrCarrierType;
	}
	public String getDvrLat() {
		return dvrLat;
	}
	public void setDvrLat(String dvrLat) {
		this.dvrLat = dvrLat;
	}
	public String getDvrLng() {
		return dvrLng;
	}
	public void setDvrLng(String dvrLng) {
		this.dvrLng = dvrLng;
	}
	public String getDvrCompanyName() {
		return dvrCompanyName;
	}
	public void setDvrCompanyName(String dvrCompanyName) {
		this.dvrCompanyName = dvrCompanyName;
	}
	public String getDvrCeoName() {
		return dvrCeoName;
	}
	public void setDvrCeoName(String dvrCeoName) {
		this.dvrCeoName = dvrCeoName;
	}
	public String getDvrBusinessLicenseNumber() {
		return dvrBusinessLicenseNumber;
	}
	public void setDvrBusinessLicenseNumber(String dvrBusinessLicenseNumber) {
		this.dvrBusinessLicenseNumber = dvrBusinessLicenseNumber;
	}
	public String getDvrBusinessCondition() {
		return dvrBusinessCondition;
	}
	public void setDvrBusinessCondition(String dvrBusinessCondition) {
		this.dvrBusinessCondition = dvrBusinessCondition;
	}
	public String getDvrBusinessKind() {
		return dvrBusinessKind;
	}
	public void setDvrBusinessKind(String dvrBusinessKind) {
		this.dvrBusinessKind = dvrBusinessKind;
	}
	public String getDvrAddress() {
		return dvrAddress;
	}
	public void setDvrAddress(String dvrAddress) {
		this.dvrAddress = dvrAddress;
	}
	public String getDvrAddressDetail() {
		return dvrAddressDetail;
	}
	public void setDvrAddressDetail(String dvrAddressDetail) {
		this.dvrAddressDetail = dvrAddressDetail;
	}
	public String getDvrInsuranceStatus() {
		return dvrInsuranceStatus;
	}
	public void setDvrInsuranceStatus(String dvrInsuranceStatus) {
		this.dvrInsuranceStatus = dvrInsuranceStatus;
	}
	public String getDvrTemporaryYn() {
		return dvrTemporaryYn;
	}
	public void setDvrTemporaryYn(String dvrTemporaryYn) {
		this.dvrTemporaryYn = dvrTemporaryYn;
	}
	public String getDvrResidentRegistrationNumberFront() {
		return dvrResidentRegistrationNumberFront;
	}
	public void setDvrResidentRegistrationNumberFront(String dvrResidentRegistrationNumberFront) {
		this.dvrResidentRegistrationNumberFront = dvrResidentRegistrationNumberFront;
	}
	public String getDvrResidentRegistrationNumberRear() {
		return dvrResidentRegistrationNumberRear;
	}
	public void setDvrResidentRegistrationNumberRear(String dvrResidentRegistrationNumberRear) {
		this.dvrResidentRegistrationNumberRear = dvrResidentRegistrationNumberRear;
	}
	public String getDvrCarNum() {
		return dvrCarNum;
	}
	public void setDvrCarNum(String dvrCarNum) {
		this.dvrCarNum = dvrCarNum;
	}
	public String getDbEncKey() {
		return dbEncKey;
	}
	public void setDbEncKey(String dbEncKey) {
		this.dbEncKey = dbEncKey;
	}
	public String getDvrAddressPoint() {
		return dvrAddressPoint;
	}
	public void setDvrAddressPoint(String dvrAddressPoint) {
		this.dvrAddressPoint = dvrAddressPoint;
	}
	public String getDvrBondYn() {
		return dvrBondYn;
	}
	public void setDvrBondYn(String dvrBondYn) {
		this.dvrBondYn = dvrBondYn;
	}
	public String getDvrInsuranceStart() {
		return dvrInsuranceStart;
	}
	public void setDvrInsuranceStart(String dvrInsuranceStart) {
		this.dvrInsuranceStart = dvrInsuranceStart;
	}
	public String getDvrInsuranceEnd() {
		return dvrInsuranceEnd;
	}
	public void setDvrInsuranceEnd(String dvrInsuranceEnd) {
		this.dvrInsuranceEnd = dvrInsuranceEnd;
	}
	public String getDvrTransportLicenseNumber() {
		return dvrTransportLicenseNumber;
	}
	public void setDvrTransportLicenseNumber(String dvrTransportLicenseNumber) {
		this.dvrTransportLicenseNumber = dvrTransportLicenseNumber;
	}
	public String getDvrSimpleIntroduce() {
		return dvrSimpleIntroduce;
	}
	public void setDvrSimpleIntroduce(String dvrSimpleIntroduce) {
		this.dvrSimpleIntroduce = dvrSimpleIntroduce;
	}
	
	
	
	
	
	
	
}
