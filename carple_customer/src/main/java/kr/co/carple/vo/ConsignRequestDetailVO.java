package kr.co.carple.vo;

public class ConsignRequestDetailVO {

	
	private String crdId;							//탁송요청상세 아이디
	private String crdCorId;                        //탁송요청 메인 아이디
	private String crdCarMaker;                     //제조사
	private String crdCarKind;                      //차종
	private String crdCarTrim;                      //세부차종(트림)
	private String crdCarIdNum;                     //차대번호
	private String crdCarNum;                       //차량번호
	private String crdCarType;                      //차량구분 NC:신차,OC:중고차
	private String crdDeparture;                    //상차지(티맵)
	private String crdDepartureLatlng;              //상차지 좌표(티맵)
	private String crdDeparturePersonInCharge;      //상차담당자
	private String crdDeparturePhone;               //상차지연락처
	private String crdArrival;                      //하차지(티맵)
	private String crdArrivalLatlng;                //하차지 좌표(티맵)
	private String crdArrivalPersonInCharge;        //하차지담당자
	private String crdArrivalPhone;                 //하차지연락처
	private String crdDistance;                     //거리(티맵으로 조회한 상차지와 하차지 사이의 거리)
	private String crdSignificantData;              //특이사항
	private String crdRegDt;                        //등록일
	private String crdStatus;						//탁송건의 진행 상태 (N:진행중, A: 제외, Y:완료)
	
	public ConsignRequestDetailVO() {
		this.crdCarMaker = "";
		this.crdCarKind = "";
		this.crdCarTrim = "";
		this.crdCarIdNum = "";
		this.crdCarNum = "";
		this.crdDeparture = "";
		this.crdDepartureLatlng = "";
		this.crdDeparturePersonInCharge = "";
		this.crdDeparturePhone = "";
		this.crdArrival = "";
		this.crdArrivalLatlng = "";
		this.crdArrivalPersonInCharge = "";
		this.crdArrivalPhone = "";
		this.crdDistance = "";
		this.crdSignificantData = "";
		this.crdStatus = "N";
	}
	
	
	public String getCrdId() {
		return crdId;
	}
	public void setCrdId(String crdId) {
		this.crdId = crdId;
	}
	public String getCrdCorId() {
		return crdCorId;
	}
	public void setCrdCorId(String crdCorId) {
		this.crdCorId = crdCorId;
	}
	public String getCrdCarMaker() {
		return crdCarMaker;
	}
	public void setCrdCarMaker(String crdCarMaker) {
		this.crdCarMaker = crdCarMaker;
	}
	public String getCrdCarKind() {
		return crdCarKind;
	}
	public void setCrdCarKind(String crdCarKind) {
		this.crdCarKind = crdCarKind;
	}
	public String getCrdCarTrim() {
		return crdCarTrim;
	}
	public void setCrdCarTrim(String crdCarTrim) {
		this.crdCarTrim = crdCarTrim;
	}
	public String getCrdCarIdNum() {
		return crdCarIdNum;
	}
	public void setCrdCarIdNum(String crdCarIdNum) {
		this.crdCarIdNum = crdCarIdNum;
	}
	public String getCrdCarNum() {
		return crdCarNum;
	}
	public void setCrdCarNum(String crdCarNum) {
		this.crdCarNum = crdCarNum;
	}
	public String getCrdCarType() {
		return crdCarType;
	}
	public void setCrdCarType(String crdCarType) {
		this.crdCarType = crdCarType;
	}
	public String getCrdDeparture() {
		return crdDeparture;
	}
	public void setCrdDeparture(String crdDeparture) {
		this.crdDeparture = crdDeparture;
	}
	public String getCrdDepartureLatlng() {
		return crdDepartureLatlng;
	}
	public void setCrdDepartureLatlng(String crdDepartureLatlng) {
		this.crdDepartureLatlng = crdDepartureLatlng;
	}
	public String getCrdDeparturePersonInCharge() {
		return crdDeparturePersonInCharge;
	}
	public void setCrdDeparturePersonInCharge(String crdDeparturePersonInCharge) {
		this.crdDeparturePersonInCharge = crdDeparturePersonInCharge;
	}
	public String getCrdDeparturePhone() {
		return crdDeparturePhone;
	}
	public void setCrdDeparturePhone(String crdDeparturePhone) {
		this.crdDeparturePhone = crdDeparturePhone;
	}
	public String getCrdArrival() {
		return crdArrival;
	}
	public void setCrdArrival(String crdArrival) {
		this.crdArrival = crdArrival;
	}
	public String getCrdArrivalLatlng() {
		return crdArrivalLatlng;
	}
	public void setCrdArrivalLatlng(String crdArrivalLatlng) {
		this.crdArrivalLatlng = crdArrivalLatlng;
	}
	public String getCrdArrivalPersonInCharge() {
		return crdArrivalPersonInCharge;
	}
	public void setCrdArrivalPersonInCharge(String crdArrivalPersonInCharge) {
		this.crdArrivalPersonInCharge = crdArrivalPersonInCharge;
	}
	public String getCrdArrivalPhone() {
		return crdArrivalPhone;
	}
	public void setCrdArrivalPhone(String crdArrivalPhone) {
		this.crdArrivalPhone = crdArrivalPhone;
	}
	public String getCrdDistance() {
		return crdDistance;
	}
	public void setCrdDistance(String crdDistance) {
		this.crdDistance = crdDistance;
	}
	public String getCrdSignificantData() {
		return crdSignificantData;
	}
	public void setCrdSignificantData(String crdSignificantData) {
		this.crdSignificantData = crdSignificantData;
	}
	public String getCrdRegDt() {
		return crdRegDt;
	}
	public void setCrdRegDt(String crdRegDt) {
		this.crdRegDt = crdRegDt;
	}
	public String getCrdStatus() {
		return crdStatus;
	}
	public void setCrdStatus(String crdStatus) {
		this.crdStatus = crdStatus;
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
