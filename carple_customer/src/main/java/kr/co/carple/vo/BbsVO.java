package kr.co.carple.vo;

public class BbsVO {

	private String idx;
	private String bbsId;
	private String bbsSubject;
	private String bbsContents;
	private String bbsVisit;
	private String bbsRegDt;
	private String bbsWriterUuid;
	private String bbsWriterNm;
	private String bbsWriterNickNm;
	private String bbsLikeCnt;
	private String bbsCategory;
	private String bbsEmail;
	private String bbsBlackFlag;
	private String bbsDeleteFlag;
	
	public String getIdx() {
		return idx;
	}
	public void setIdx(String idx) {
		this.idx = idx;
	}
	public String getBbsId() {
		return bbsId;
	}
	public void setBbsId(String bbsId) {
		this.bbsId = bbsId;
	}
	public String getBbsSubject() {
		return bbsSubject;
	}
	public void setBbsSubject(String bbsSubject) {
		this.bbsSubject = bbsSubject;
	}
	public String getBbsContents() {
		return bbsContents;
	}
	public void setBbsContents(String bbsContents) {
		this.bbsContents = bbsContents;
	}
	public String getBbsVisit() {
		return bbsVisit;
	}
	public void setBbsVisit(String bbsVisit) {
		this.bbsVisit = bbsVisit;
	}
	public String getBbsRegDt() {
		return bbsRegDt;
	}
	public void setBbsRegDt(String bbsRegDt) {
		this.bbsRegDt = bbsRegDt;
	}
	public String getBbsWriterUuid() {
		return bbsWriterUuid;
	}
	public void setBbsWriterUuid(String bbsWriterUuid) {
		this.bbsWriterUuid = bbsWriterUuid;
	}
	public String getBbsWriterNm() {
		return bbsWriterNm;
	}
	public void setBbsWriterNm(String bbsWriterNm) {
		this.bbsWriterNm = bbsWriterNm;
	}
	public String getBbsWriterNickNm() {
		return bbsWriterNickNm;
	}
	public void setBbsWriterNickNm(String bbsWriterNickNm) {
		this.bbsWriterNickNm = bbsWriterNickNm;
	}
	public String getBbsLikeCnt() {
		return bbsLikeCnt;
	}
	public void setBbsLikeCnt(String bbsLikeCnt) {
		this.bbsLikeCnt = bbsLikeCnt;
	}
	public String getBbsCategory() {
		return bbsCategory;
	}
	public void setBbsCategory(String bbsCategory) {
		this.bbsCategory = bbsCategory;
	}
	public String getBbsEmail() {
		return bbsEmail;
	}
	public void setBbsEmail(String bbsEmail) {
		this.bbsEmail = bbsEmail;
	}
	public String getBbsBlackFlag() {
		return bbsBlackFlag;
	}
	public void setBbsBlackFlag(String bbsBlackFlag) {
		this.bbsBlackFlag = bbsBlackFlag;
	}
	public String getBbsDeleteFlag() {
		return bbsDeleteFlag;
	}
	public void setBbsDeleteFlag(String bbsDeleteFlag) {
		this.bbsDeleteFlag = bbsDeleteFlag;
	}
	
	
}
