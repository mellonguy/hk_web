package kr.co.carple.controller;

import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value="/danal")
public class DanalController {
	  
    @Value("#{appProp['CPID']}")
	private String CPID;
    
    @Value("#{appProp['CPPWD']}")
	private String CPPWD;
    
    
    @RequestMapping(value = "/Ready", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView Ready(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			
			if(userSessionMap != null) {
				
				System.out.println(userSessionMap.get("mem_user_id").toString());
				System.out.println(userSessionMap.get("mem_join_type").toString());
				
				mav.addObject("CPID", CPID);
				mav.addObject("CPPWD", CPPWD);
				
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
    
    
    @RequestMapping(value = "/Error", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView Error(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			
			if(userSessionMap != null) {
				
				System.out.println(userSessionMap.get("mem_user_id").toString());
				System.out.println(userSessionMap.get("mem_join_type").toString());
				
				mav.addObject("CPID", CPID);
				mav.addObject("CPPWD", CPPWD);
				
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
    @RequestMapping(value = "/BackURL", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView BackURL(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			
			if(userSessionMap != null) {
				
				System.out.println(userSessionMap.get("mem_user_id").toString());
				System.out.println(userSessionMap.get("mem_join_type").toString());
				
				mav.addObject("CPID", CPID);
				mav.addObject("CPPWD", CPPWD);
				
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
    @RequestMapping(value = "/CPCGI", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView CPCGI(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
				
			mav.addObject("CPID", CPID);
			mav.addObject("CPPWD", CPPWD);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return mav;
	}
	
    @RequestMapping(value = "/Success", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView Success(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			String ORDERID = request.getParameter("ORDERID") != null ? request.getParameter("ORDERID").toString():"";
			String DI = request.getParameter("DI") != null ?  request.getParameter("DI").toString():"";
			String CI = request.getParameter("CI") != null ?  request.getParameter("CI").toString():"";
			String IDEN = request.getParameter("IDEN") != null ?  request.getParameter("IDEN").toString():"";
			String USERID = request.getParameter("USERID") != null ?  request.getParameter("USERID").toString():"";
			String RETURNCODE = request.getParameter("RETURNCODE") != null ?  request.getParameter("RETURNCODE").toString():"";
			String RETURNMSG = request.getParameter("RETURNMSG") != null ?  request.getParameter("RETURNMSG").toString():"";
			String TID = request.getParameter("TID") != null ?  request.getParameter("TID").toString():"";
			String NAME = request.getParameter("NAME") != null ?  request.getParameter("NAME").toString():"";
			String PHONE = request.getParameter("PHONE") != null ?  request.getParameter("PHONE").toString():"";
			
			if(userSessionMap != null) {
				
				System.out.println(userSessionMap.get("mem_user_id").toString());
				System.out.println(userSessionMap.get("mem_join_type").toString());
				
				mav.addObject("CPID", CPID);
				mav.addObject("CPPWD", CPPWD);
				
				mav.addObject("ORDERID", ORDERID);
				mav.addObject("DI", DI);
				mav.addObject("CI", CI);
				mav.addObject("IDEN", IDEN);
				mav.addObject("USERID", USERID);
				mav.addObject("RETURNCODE", RETURNCODE);
				mav.addObject("RETURNMSG", RETURNMSG);
				mav.addObject("TID", TID);
				mav.addObject("NAME", NAME);
				mav.addObject("PHONE", PHONE);
				
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
    
    
    
	
	

}
