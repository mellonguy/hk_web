package kr.co.carple.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;



@Controller
@RequestMapping(value = "/system")
public class SystemController {
private static final Logger logger = LoggerFactory.getLogger(SystemController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/alert_redirect", method = RequestMethod.GET)
	public String alertRedirect(HttpSession session) {
		return "system/alert_redirect";
	}

}
