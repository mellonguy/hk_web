package kr.co.carple.controller;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/welcome")
public class WelcomeController {

	
	private static final Logger logger = LoggerFactory.getLogger(WelcomeController.class);
	
	
	
	
	@RequestMapping(value = "/welcome", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView welcome(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response)  throws Exception{

		try{
			
			String test = "";
			
			mav.addObject("test", test);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return mav;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
