package kr.co.carple.controller;

import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.co.carple.service.MemberService;
import kr.co.carple.utils.ResultApi;
import kr.co.carple.vo.MemberVO;

@Controller
@RequestMapping(value="/member")
public class MemberController {

	
private static final Logger logger = LoggerFactory.getLogger(MemberController.class);
	
	@Value("#{appProp['RestApiKey']}")
    private String RestApiKey;
    
    @Value("#{appProp['Redirect_URI']}")
    private String Redirect_URI;
	
    @Value("#{appProp['Redirect_URI_app']}")
    private String Redirect_URI_app;
    
    @Value("#{appProp['dbEncKey']}")
	private String dbEncKey;
  
    @Value("#{appProp['CPID']}")
	private String CPID;
    
    @Value("#{appProp['CPPWD']}")
	private String CPPWD;
    
    
    @Autowired
    private MemberService memberService;
    
    
    @RequestMapping(value = "/member-register", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView memberRegister(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			
			if(userSessionMap != null) {
				
				System.out.println(userSessionMap.get("mem_user_id").toString());
				System.out.println(userSessionMap.get("mem_join_type").toString());
			
				String memPhone = request.getParameter("memPhone") != null ?  request.getParameter("memPhone").toString():"";
				String memUserName = request.getParameter("memUserName") != null ?  request.getParameter("memUserName").toString():"";

				mav.addObject("memPhone", memPhone);
				mav.addObject("memUserName", memUserName);				
				mav.addObject("CPID", CPID);
				mav.addObject("CPPWD", CPPWD);
				
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
    
    @RequestMapping(value = "/insertMember", method = {RequestMethod.POST,RequestMethod.GET})
	@ResponseBody
	public ResultApi insertMember(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response, MemberVO memberVO) {
		ResultApi resultApi = new ResultApi();
		try{
			
			
			HttpSession session = request.getSession();
			//Map userSessionMap = (Map)session.getAttribute("user");
			
			resultApi = memberService.insertMember(memberVO,session);
			
		}catch(Exception e){
			e.printStackTrace();
			resultApi.setResultCode("0001");
			
		}
		
		
		return resultApi;
	}
    
	
    @RequestMapping(value = "/updateMemDeviceKey", method = {RequestMethod.POST,RequestMethod.GET})
	@ResponseBody
	public ResultApi updateMemDeviceKey(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response, MemberVO memberVO) {
		ResultApi resultApi = new ResultApi();
		try{
			
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			Map<String, Object> map = new HashMap<String, Object>();
			
			map.put("memDeviceKey", request.getParameter("memDeviceKey").toString());
			map.put("memUserId", userSessionMap.get("mem_user_id").toString());
			map.put("memJoinType", userSessionMap.get("mem_join_type").toString());
			
			resultApi = memberService.updateMemDeviceKey(map);
			
		}catch(Exception e){
			e.printStackTrace();
			resultApi.setResultCode("0001");
			
		}
		
		
		return resultApi;
	}
    
	
	
	
	
	
	
	
	
	
	
}
