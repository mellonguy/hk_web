package kr.co.carple.controller;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.co.carple.service.DriverService;
import kr.co.carple.utils.ParamUtils;
import kr.co.carple.utils.ResultApi;
import kr.co.carple.vo.DriverVO;

@Controller
@RequestMapping(value="/login")
public class LoginController {

	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	@Value("#{appProp['RestApiKey']}")
    private String RestApiKey;
    
    @Value("#{appProp['Redirect_URI']}")
    private String Redirect_URI;
	
    @Value("#{appProp['Redirect_URI_app']}")
    private String Redirect_URI_app;
    
    @Value("#{appProp['dbEncKey']}")
	private String dbEncKey;
  
    @Autowired
    private DriverService driverService;
    
    
	
    @RequestMapping(value = "/login-page", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView login_page(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
    
    
    @RequestMapping(value = "/driver-register", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView driver_register(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}

    
    
    @RequestMapping(value = "/loginPersist", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi loginPersist(HttpServletRequest request,HttpServletResponse response,HttpSession session,@RequestParam String dvrUserId,@RequestParam String loginPwd) throws Exception{
    	
    	ResultApi result = new ResultApi();
			
			try{

				Map<String, Object> map = new HashMap<String, Object>();
		    	map.put("dvrUserId", dvrUserId);
		    	map.put("loginPwd", loginPwd);
			
		    	Map<String, Object> driverMap = driverService.selectDriver(map);
		    	
		    	//guid
		    	if(driverMap == null){
		    		result.setResultCode("E001");
		    	}else{
		    		
		    		if(driverMap.get("dvr_approve_status") != null && !driverMap.get("dvr_approve_status").toString().equals("")){
		    		
		    			if(driverMap.get("dvr_approve_status").toString().equals("Y")) {
		    				result.setResultCode("0000");
						    session.setAttribute(dvrUserId, driverMap);	//로그인 성공
					    	session.setAttribute("driver", driverMap);		
		    			}else {
		    				result.setResultCode("E002");
		    				result.setResultMsg("승인 대기중인 아이디 입니다. 승인이 완료될때 까지 기다려 주세요.");
		    			}
		    		}
		    			
		    	}
		    	
			}catch( Exception e){
				result.setResultCode("E999");
				e.printStackTrace();
			}
			
			return result;
	}
	
	@RequestMapping(value = "/nopermission", method = RequestMethod.GET)
	public ModelAndView nopermission(ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception{
		
		try{
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return mav;
	}
    
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpSession session) {
    	
    	try {    	
    		session.invalidate();	
    	}catch(Exception e) {
    		e.printStackTrace();    	
    	}
		return "redirect:/?&status=logout";
	}
    
    
    @RequestMapping(value = "/terms", method = RequestMethod.GET)
	public ModelAndView terms(ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception{
    	
    	try {    	
    			
    	}catch(Exception e) {
    		e.printStackTrace();    	
    	}
    	return mav;
	}
    
    
    @RequestMapping(value = "/terms-mobile", method = RequestMethod.GET)
	public ModelAndView termsMobile(ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception{
    	
    	try {    	
    			
    	}catch(Exception e) {
    		e.printStackTrace();    	
    	}
    	return mav;
	}
    
   
    
	
	
	
}
