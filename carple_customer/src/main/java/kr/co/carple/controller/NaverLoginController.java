package kr.co.carple.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import kr.co.carple.service.MemberService;
import kr.co.carple.utils.BaseAppConstants;
import kr.co.carple.utils.NaverUtils;
import kr.co.carple.utils.ParamUtils;

@Controller
@RequestMapping(value="/naver")
public class NaverLoginController {

	//네이버 API
	//https://developers.naver.com/docs/login/web/#1-4-1--%EC%9D%B8%EC%A6%9D-%EC%A0%95%EB%B3%B4-%EC%9D%91%EB%8B%B5%EB%AC%B8-%ED%98%95%EC%8B%9D
	
	//private String RestApiKey = "e3a057034847accf088ffd0b3ad83801";
	
	@Value("#{appProp['RestApiKey']}")
    private String RestApiKey;
    
    @Value("#{appProp['Redirect_URI']}")
    private String Redirect_URI;
    
    @Value("#{appProp['Redirect_URI_app']}")
    private String Redirect_URI_app;
    
    @Value("#{appProp['clientId']}")
    private String clientId;
    
    @Value("#{appProp['clientSecret']}")
    private String clientSecret;
    
    @Value("#{appProp['callbackUrl']}")
    private String callbackUrl;
 
    @Value("#{appProp['appKey']}")
    private String appKey;
    
    @Autowired
    private MemberService memberService;
	
	
    @RequestMapping(value = "/naverLogin",produces="application/json", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView naverLogin(@RequestParam String code,@RequestParam String state,ModelAndView mav,HttpSession session, HttpServletRequest request,HttpServletResponse response) {
		
		try{

			JsonNode jsonToken  = NaverUtils.getAccessToken(code, state, clientId, clientSecret);			
			JsonNode userInfo = NaverUtils.getNaverUserInfo(jsonToken.get("access_token").toString());

			System.out.println("userInfo="+userInfo);
			
			
			String id = null;
			String nickname = null;
			String thumbnailImage = null;
			String profileImage = null;
			//String age = null;
			//String gender = null;
			String email = null;
			//String mobile = null;
			//String mobile_e164 = null;
			//String name = null;
			String birthday = null;
			//String birthyear = null;
			
			JsonNode properties = userInfo.path("response");
			
			System.out.println("properties="+properties);
			
			if (!properties.isMissingNode()) {
				id = properties.path("id").asText();
				nickname = properties.path("nickname").asText();
				profileImage = properties.path("profile_image").asText();
				thumbnailImage = properties.path("profile_image").asText();
				email = properties.path("email").asText();      
				birthday = properties.path("birthday").asText();   
				//age = properties.path("age").asText();
				//gender = properties.path("gender").asText();     
				//mobile = properties.path("mobile").asText();     
				//mobile_e164 = properties.path("mobile_e164").asText();
				//name = properties.path("name").asText();       
				//birthyear = properties.path("birthyear").asText();  
			}
						
			Map<String, Object> userMap = new HashMap<String, Object>();
			
			userMap.put("mem_user_id", id);
			userMap.put("mem_nick_name",  nickname != null && !nickname.equals("") ? nickname.replaceAll("-", ""):"");
 			userMap.put("mem_icon",  profileImage != null && !profileImage.equals("") ? profileImage.replaceAll("-", ""):"");
			userMap.put("mem_photo",  thumbnailImage != null && !thumbnailImage.equals("") ? thumbnailImage.replaceAll("-", ""):"");
			userMap.put("mem_access_token", jsonToken.get("access_token").toString().replaceAll("\"", ""));
			userMap.put("mem_email",   email != null && !email.equals("") ? email.replaceAll("-", ""):"");
			userMap.put("mem_birth_day",birthday != null && !birthday.equals("") ? birthday.replaceAll("-", ""):"");
			userMap.put("mem_join_type", BaseAppConstants.MEM_JOIN_TYPE_NAVER);
			
			Map<String, Object> member = memberService.selectMember(userMap);
			
			if(member != null) {
				session.setAttribute("user", member);
				session.setAttribute("appKey", appKey);
			}else {
				session.setAttribute("user", userMap);
				session.setAttribute("appKey", appKey);
			}
			
			
		
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		mav.setViewName("redirect:/menu/menu.do");
		return mav;
	}
    
    
    
    @RequestMapping(value = "/naverLoginByNaverUserInfo",produces="application/json", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView getNaverUserInfo(ModelAndView mav,HttpSession session, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			/*
			System.out.println("id"+paramMap.get("id").toString());
			System.out.println("email"+paramMap.get("email").toString());
			System.out.println("birthday"+paramMap.get("birthday").toString());
			System.out.println("nickname"+paramMap.get("nickname").toString());
			System.out.println("profileImage"+paramMap.get("profileImage").toString());
			System.out.println("thumbnailImage"+paramMap.get("thumbnailImage").toString());
			*/
			String id = paramMap.get("id").toString();
			String nickname = paramMap.get("nickname") != null && !paramMap.get("nickname").toString().equals("") ? paramMap.get("nickname").toString() :"" ;
			String thumbnailImage = paramMap.get("thumbnailImage") != null && !paramMap.get("thumbnailImage").toString().equals("") ? paramMap.get("thumbnailImage").toString() :"" ;
			String profileImage = paramMap.get("profileImage") != null && !paramMap.get("profileImage").toString().equals("") ? paramMap.get("profileImage").toString() :"" ;
			String email = paramMap.get("email") != null && !paramMap.get("email").toString().equals("") ? paramMap.get("email").toString() :"" ;			
			String birthday = paramMap.get("birthday") != null && !paramMap.get("birthday").toString().equals("") ? paramMap.get("birthday").toString() :"" ;
			String accessToken = paramMap.get("accessToken") != null && !paramMap.get("accessToken").toString().equals("") ? paramMap.get("accessToken").toString() :"" ;
			
			Map<String, Object> userMap = new HashMap<String, Object>();
			
			userMap.put("mem_user_id", id);
			userMap.put("mem_nick_name", nickname);
 			userMap.put("mem_icon", profileImage);
			userMap.put("mem_photo", thumbnailImage);
			userMap.put("mem_access_token", accessToken.replaceAll("\"", ""));
			userMap.put("mem_email", email);
			userMap.put("mem_birth_day", birthday.replaceAll("-", ""));
			userMap.put("mem_join_type", BaseAppConstants.MEM_JOIN_TYPE_NAVER);
			
			Map<String, Object> member = memberService.selectMember(userMap);
			
			if(member != null) {
				session.setAttribute("user", member);
				session.setAttribute("appKey", appKey);
			}else {
				session.setAttribute("user", userMap);
				session.setAttribute("appKey", appKey);
			}
			
			
		
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		mav.setViewName("redirect:/menu/menu.do");
		return mav;
	}
    
    
    
    
    
    
	
    
    
	@RequestMapping(value = "/logoutByNaverUserInfo", method = RequestMethod.GET)
	public String logoutByKakaoUserInfo(HttpSession session) {
	
		try{
			session.invalidate();	
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return "redirect:/menu/menu.do?type=logout";
	}
	
    
    
    
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpSession session) {
	
		try{
			Map<String, Object> userSessionMap = (Map<String, Object>)session.getAttribute("user");
			JsonNode userInfo = NaverUtils.naverUserLogout(clientId,clientSecret,userSessionMap.get("mem_access_token").toString());
			session.invalidate();	
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return "redirect:/menu/menu.do?type=logout";
	}
	
	
	
	
	
	
	
	
}
