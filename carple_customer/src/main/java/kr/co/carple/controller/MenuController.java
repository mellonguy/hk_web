package kr.co.carple.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.co.carple.service.BbsService;
import kr.co.carple.service.MemberService;
import kr.co.carple.utils.BaseAppConstants;
import kr.co.carple.utils.ResultApi;
import kr.co.carple.utils.WebUtils;

@Controller
@RequestMapping(value="/menu")
public class MenuController {

	private static final Logger logger = LoggerFactory.getLogger(MenuController.class);
	
	@Value("#{appProp['RestApiKey']}")
    private String RestApiKey;
    
    @Value("#{appProp['Redirect_URI']}")
    private String Redirect_URI;

    @Value("#{appProp['Redirect_URI_app']}")
    private String Redirect_URI_app;
    
    @Value("#{appProp['clientId']}")
    private String clientId;
    
    @Value("#{appProp['clientSecret']}")
    private String clientSecret;
    
    @Value("#{appProp['callbackUrl']}")
    private String callbackUrl;
 
    
    @Autowired
    private MemberService memberService;
    
    @Autowired
    private BbsService bbsService;
	
	@RequestMapping(value = "/menu", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView menu(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) throws Exception {
		
		try{
			HttpSession session = request.getSession();
			
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap == null){
				mav.setViewName("/menu/menu-sub");	
				mav.addObject("Redirect_URI", Redirect_URI);
				mav.addObject("Redirect_URI_app", Redirect_URI_app);
				mav.addObject("RestApiKey", RestApiKey);
				
				SecureRandom random = new SecureRandom();
			    String state  = new BigInteger(130, random).toString(32);
				
			    mav.addObject("state", state);
			    mav.addObject("clientId", clientId);
			    mav.addObject("clientSecret", clientSecret);
			    mav.addObject("callbackUrl", callbackUrl);
			    
			  // WebUtils.messageAndRedirectUrl(mav, "로그인 페이지로 이동 합니다.", "/menu/menu-sub.do?&state="+state+"&clientId="+clientId+"&clientSecret="+clientSecret+"&callbackUrl="+callbackUrl);
			    
			}else{
				
				Map<String, Object> userMap = new HashMap<String, Object>();
				
				if(userSessionMap.get("mem_uuid") == null || userSessionMap.get("mem_uuid").toString().equals("")) {
					
					userMap.put("mem_user_id", userSessionMap.get("mem_user_id").toString());
					userMap.put("mem_nick_name", userSessionMap.get("mem_nick_name").toString());
					userMap.put("mem_icon", userSessionMap.get("mem_icon") != null && !userSessionMap.get("mem_icon").toString().equals("")?userSessionMap.get("mem_icon").toString():"");
					userMap.put("mem_photo", userSessionMap.get("mem_photo") != null && !userSessionMap.get("mem_photo").toString().equals("")?userSessionMap.get("mem_photo").toString():"");
					userMap.put("mem_access_token", userSessionMap.get("mem_access_token").toString());
					userMap.put("mem_join_type", userSessionMap.get("mem_join_type").toString());
					userMap.put("mem_device_key", "testKey");
					Map<String, Object> member = memberService.selectMember(userMap);	
					if(member == null) {
						WebUtils.messageAndRedirectUrl(mav, "안녕하세요! "+userSessionMap.get("mem_nick_name").toString()+"님 추가 정보 입력 화면으로 이동 합니다.", "/member/member-register.do");
					}	
				
				}else {
				
					//회원 정보가 있는 경우 새로운 정보로 업데이트를 해 준다.
					userMap = new HashMap<String, Object>();
					userMap.put("mem_user_id", userSessionMap.get("mem_user_id").toString());
					userMap.put("mem_nick_name", userSessionMap.get("mem_nick_name").toString());
					userMap.put("mem_icon", userSessionMap.get("mem_icon") != null && !userSessionMap.get("mem_icon").toString().equals("")?userSessionMap.get("mem_icon").toString():"");
					userMap.put("mem_photo", userSessionMap.get("mem_photo") != null && !userSessionMap.get("mem_photo").toString().equals("")?userSessionMap.get("mem_photo").toString():"");
					userMap.put("mem_access_token", userSessionMap.get("mem_access_token").toString());
					userMap.put("mem_join_type", userSessionMap.get("mem_join_type").toString());
					userMap.put("mem_login_env", BaseAppConstants.MEM_LOGIN_ENV_ANDROID);
					userMap.put("mem_device_key", "testKey");
					memberService.updateMemberInfo(userMap);
				
				}
				
				/*
				if(member == null) {
					
					WebUtils.messageAndRedirectUrl(mav, "추가 정보가 입력 되지 않았습니다. 추가 정보 입력 화면으로 이동 합니다.", "/member/member-register.do");
				}else {
					//회원 정보가 있는 경우 새로운 정보로 업데이트를 해 준다.
					userMap = new HashMap<String, Object>();
					userMap.put("mem_user_id", userSessionMap.get("mem_user_id").toString());
					userMap.put("mem_nick_name", userSessionMap.get("mem_nick_name").toString());
					userMap.put("mem_icon", userSessionMap.get("mem_icon").toString());
					userMap.put("mem_photo", userSessionMap.get("mem_photo").toString());
					userMap.put("mem_access_token", userSessionMap.get("mem_access_token").toString());
					userMap.put("mem_join_type", userSessionMap.get("mem_join_type").toString());
					userMap.put("mem_login_env", BaseAppConstants.MEM_LOGIN_ENV_ANDROID);
					userMap.put("mem_device_key", "testKey");
					memberService.updateMemberInfo(userMap);
					member = memberService.selectMember(userMap);
					session.setAttribute("user", member);
				}
				*/
				
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return mav;
	}
	
	
	
	@RequestMapping(value = "/menu-sub", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView menuSub(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response)  throws Exception{

		try{
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			if(userSessionMap != null){
				//포인트 사용/적립 내역 조회
				Map<String,Object> userMap = new HashMap<String, Object>();
				userMap.put("kakaoId", userSessionMap.get("kakao_id"));
				
	//			List<Map<String, Object>> pointHistoryList = pointHistoryService.selectPointHistoryList(userMap);
	//			mav.addObject("pointHistoryList", pointHistoryList);				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return mav;
	}
	
	
	@RequestMapping(value = "/menu-list", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView menuList(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response)  throws Exception{

		try{
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			if(userSessionMap != null){
				
				
				
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return mav;
	}
	
	
	@RequestMapping(value = "/appInfo", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView appInfo(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response)  throws Exception{

		try{
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			if(userSessionMap != null){
				
				
				
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return mav;
	}
	
	
	@RequestMapping(value = "/question", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView question(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response)  throws Exception{

		try{
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			if(userSessionMap != null){
				
				
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("bbsCategory", BaseAppConstants.BBS_CATEGORY_QUESTION);
				map.put("bbsBlackFlag", "N");
				map.put("bbsDeleteFlag", "N");
				
				List<Map<String, Object>> bbsList = bbsService.selectBbsList(map);
				mav.addObject("listData", bbsList);
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return mav;
	}
	
	
	@RequestMapping(value = "/osLicense", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView osLicense(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response)  throws Exception{

		try{
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			if(userSessionMap != null){
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return mav;
	}
	
	
	
	
	
	@RequestMapping(value = "/getapidata", method = {RequestMethod.POST,RequestMethod.GET})
	@ResponseBody
	public ResultApi getapidata(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response)  throws Exception{

		ResultApi result = new ResultApi();
		
		try{
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			if(userSessionMap != null){
				
				URL url = new URL("http://data.ex.co.kr/openapi/burstInfo/realTimeSms?key=9318324770&sortType=desc&pagingYn=Y&type=json&numOfRows=40&pageNo=1");
				HttpURLConnection con = (HttpURLConnection) url.openConnection(); 
				con.setConnectTimeout(5000);
				con.setReadTimeout(5000);
				con.setRequestMethod("GET");
	            con.setDoOutput(false); 
				
	            JSONParser jsonParser = null;
				JSONObject jsonObject = null;
	            
				if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
					
					ArrayList<JSONObject> finalList = new ArrayList<JSONObject>();
					
					String line = "";
					StringBuilder sb = new StringBuilder();
					BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
					
					while ((line = br.readLine()) != null) {
						sb.append(line);
					}
					br.close();
					
					jsonParser = new JSONParser();
					jsonObject = (JSONObject) jsonParser.parse(sb.toString());
				
					JSONArray list = (JSONArray)jsonParser.parse(jsonObject.get("realTimeSMSList").toString());
					list = (JSONArray)jsonParser.parse(jsonObject.get("realTimeSMSList").toString());
					
					for(int i = 0; i < list.size(); i++) {
						JSONObject jsonData = (JSONObject) list.get(i);
						System.out.println(jsonData.toString());
						finalList.add(jsonData);
					}
					
					result.setResultData(finalList);
					
				} else {
					System.out.println(con.getResponseMessage());
				}
				
				
				
				/*
				URL url = new URL("http://data.ex.co.kr/openapi/burstInfo/realTimeSms?key=9318324770&type=json&numOfRows=20&pageNo=1");
				HttpURLConnection con = (HttpURLConnection) url.openConnection(); 
				con.setConnectTimeout(5000);
				con.setReadTimeout(5000);
				con.setRequestMethod("GET");
	            con.setDoOutput(false); 

	            //Thread.sleep(500);
	            
				StringBuilder sb = new StringBuilder();
				if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
					 
					JSONParser jsonParser = null;
					JSONObject jsonObject = null;
					
					ArrayList<JSONObject> finalList = new ArrayList<JSONObject>();
					
					
					BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
					String line;
					while ((line = br.readLine()) != null) {
						sb.append(line);
					}
					br.close();
					
					jsonParser = new JSONParser();
					jsonObject = (JSONObject) jsonParser.parse(sb.toString());
					String count = jsonObject.get("count").toString();
					String pageSize = jsonObject.get("pageSize").toString();
					
					//마지막-1 전페이지
					String pageNo = String.valueOf(Integer.parseInt(pageSize)-1);
					
					//카운트와 페이지 사이즈로 최근 목록을 가져온다.
					url = new URL("http://data.ex.co.kr/openapi/burstInfo/realTimeSms?key=9318324770&type=json&numOfRows=20&pageNo="+pageNo);
					con = (HttpURLConnection) url.openConnection(); 
					con.setConnectTimeout(5000);
					con.setReadTimeout(5000);
					con.setRequestMethod("GET");
		            con.setDoOutput(false); 

					sb = new StringBuilder();
					if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
						 
						br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
						line="";
						while ((line = br.readLine()) != null) {
							sb.append(line);
						}
						br.close();
						
						jsonParser = new JSONParser();
						jsonObject = (JSONObject) jsonParser.parse(sb.toString());
					
						JSONArray list = (JSONArray)jsonParser.parse(jsonObject.get("realTimeSMSList").toString());
						
						for(int i = 0; i < list.size(); i++) {
							JSONObject jsonData = (JSONObject) list.get(i);
							finalList.add(jsonData);					
							
						}
					
						//마지막페이지
						url = new URL("http://data.ex.co.kr/openapi/burstInfo/realTimeSms?key=9318324770&type=json&numOfRows=20&pageNo="+pageSize);
						con = (HttpURLConnection) url.openConnection(); 
						con.setConnectTimeout(5000);
						con.setReadTimeout(5000);
						con.setRequestMethod("GET");
			            con.setDoOutput(false); 

						sb = new StringBuilder();
						if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
							 
							br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
							line="";
							while ((line = br.readLine()) != null) {
								sb.append(line);
							}
							br.close();
							
							jsonParser = new JSONParser();
							jsonObject = (JSONObject) jsonParser.parse(sb.toString());
						
							list = (JSONArray)jsonParser.parse(jsonObject.get("realTimeSMSList").toString());
							
							for(int i = 0; i < list.size(); i++) {
								JSONObject jsonData = (JSONObject) list.get(i);
								finalList.add(jsonData);
							}
							
							
						} else {
							System.out.println(con.getResponseMessage());
						}
						
						result.setResultData(finalList);
						
					} else {
						System.out.println(con.getResponseMessage());
					}
					
				} else {
					System.out.println(con.getResponseMessage());
				}

				*/

				
				
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return result;
	}
	
	
	
	
	@RequestMapping(value = "/notification", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView notification(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) throws Exception{
		
		
		try{
				HttpSession session = request.getSession();
				Map userSessionMap = (Map)session.getAttribute("user");
			
				if(userSessionMap != null){
					//사용자의 알람 설정정보 
					Map<String,Object> map = new HashMap<String, Object>();
					map.put("kakaoId", userSessionMap.get("kakao_id"));
//					Map<String, Object> notificationSettingMap = notificationSettingService.selectNotificationSetting(map);
//					mav.addObject("notificationSettingMap", notificationSettingMap );
				}
			
			}catch(Exception e){
				e.printStackTrace();
			}
		
		return mav;
	}
	
	
	
	@RequestMapping(value = "/notification-list", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView notificationList(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) throws Exception {

		try{
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			if(userSessionMap != null){
				//포인트 사용/적립 내역 조회
				Map<String,Object> userMap = new HashMap<String, Object>();
				userMap.put("kakaoId", userSessionMap.get("kakao_id"));
				
//				List<Map<String, Object>> pointHistoryList = pointHistoryService.selectPointHistoryList(userMap);
//				mav.addObject("pointHistoryList", pointHistoryList);				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return mav;
	}
	
	
	
	@RequestMapping(value = "/support", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView support(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) throws Exception {

		try{
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			if(userSessionMap != null){
				//포인트 사용/적립 내역 조회
				Map<String,Object> userMap = new HashMap<String, Object>();
				userMap.put("kakaoId", userSessionMap.get("kakao_id"));
				
//				List<Map<String, Object>> pointHistoryList = pointHistoryService.selectPointHistoryList(userMap);
//				mav.addObject("pointHistoryList", pointHistoryList);				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return mav;
	}
	
	
	@RequestMapping(value = "/selectSupportList", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi selectAdviserList(ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception{
		
		ResultApi result = new ResultApi();
		 
		 try{
			 
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			String categoryCd = request.getParameter("categoryCd").toString();
			
				
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("categoryCd", categoryCd==null?"":categoryCd);
				
			//List<Map<String, Object>> adviserList = adviserService.selectAdviserList(map);
//			result.setResultData(customerSupportService.selectCustomerSupportList(map));
			
		 }catch(Exception e){
			 result.setResultCode("E001");
			 e.printStackTrace();
		 }
		
		return result;
	}
	
	
	@RequestMapping(value = "/support-content", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView supportContent(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) throws Exception {

		try{
			 
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			String supportId = request.getParameter("supportId").toString();
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("supportId", supportId==null?"":supportId);
				
//			Map<String, Object> supportMap = customerSupportService.selectCustomerSupport(map);
//			mav.addObject("supportMap", supportMap);
			
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		
		return mav;
	}
	
	
	
	@RequestMapping(value = "/event", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView event(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) throws Exception {

		try{
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			if(userSessionMap != null){
				//포인트 사용/적립 내역 조회
				Map<String,Object> userMap = new HashMap<String, Object>();
				userMap.put("kakaoId", userSessionMap.get("kakao_id"));
				
//				List<Map<String, Object>> pointHistoryList = pointHistoryService.selectPointHistoryList(userMap);
//				mav.addObject("pointHistoryList", pointHistoryList);				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return mav;
	}
	
	
	@RequestMapping(value = "/event-content", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView eventContent(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) throws Exception {
			
		try{
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			if(userSessionMap != null){
				//포인트 사용/적립 내역 조회
				Map<String,Object> userMap = new HashMap<String, Object>();
				userMap.put("kakaoId", userSessionMap.get("kakao_id"));
				
	//			List<Map<String, Object>> pointHistoryList = pointHistoryService.selectPointHistoryList(userMap);
	//			mav.addObject("pointHistoryList", pointHistoryList);				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return mav;
	}
	
	@RequestMapping(value = "/primary", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView primary(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) throws Exception {
		
		
		
		try{
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			if(userSessionMap == null){
				
			}else{
				Map<String,Object> userMap = new HashMap<String, Object>();
				userMap.put("kakaoId", userSessionMap.get("kakao_id"));
//				int userPoint = pointHistoryService.selectUserPoint(userMap);
//				mav.addObject("userPoint", userPoint);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return mav;
	}
	
	
	
	
	@RequestMapping(value = "/purchase-history", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView purchaseHistory(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) throws Exception {
		
		try{
			HttpSession session = request.getSession();
			
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap == null){
				
			}else{
				
				//포인트 사용/적립 내역 조회
				Map<String,Object> userMap = new HashMap<String, Object>();
				userMap.put("kakaoId", userSessionMap.get("kakao_id"));
				
//				List<Map<String, Object>> pointHistoryList = pointHistoryService.selectPointHistoryList(userMap);
//				mav.addObject("pointHistoryList", pointHistoryList);
			}
			
			
			//WsClient.main(null);
			
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return mav;
	}
	
	
	@RequestMapping(value = "/roboadvisor", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView roboadvisor(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) throws Exception {
		
		try{
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			if(userSessionMap != null){
				//포인트 사용/적립 내역 조회
				Map<String,Object> userMap = new HashMap<String, Object>();
				userMap.put("kakaoId", userSessionMap.get("kakao_id"));
				
//				List<Map<String, Object>> purchaseAdviserList = adviserService.selectPurchaseAdviserList(userMap);
//				mav.addObject("purchaseAdviserList", purchaseAdviserList);				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return mav;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
