package kr.co.carple.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import kr.co.carple.service.MemberService;
import kr.co.carple.utils.BaseAppConstants;
import kr.co.carple.utils.KakaoUtils;
import kr.co.carple.utils.ParamUtils;

@Controller
@RequestMapping(value="/kakao")
public class KakaoLoginController {

	//private String RestApiKey = "e3a057034847accf088ffd0b3ad83801";
	
	@Value("#{appProp['RestApiKey']}")
    private String RestApiKey;
    
    @Value("#{appProp['Redirect_URI']}")
    private String Redirect_URI;
        
    @Value("#{appProp['appKey']}")
    private String appKey;
    
    @Value("#{appProp['Redirect_URI_app']}")
    private String Redirect_URI_app;
    
    @Autowired
    private MemberService memberService;
	
  
    
    
	@RequestMapping(value = "/kakaoLogin",produces="application/json", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView kakaoLogin(@RequestParam("code") String code,ModelAndView mav,HttpSession session, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			JsonNode jsonToken  = KakaoUtils.getAccessToken(code,RestApiKey,Redirect_URI);			
			JsonNode userInfo = KakaoUtils.getKakaoUserInfo(jsonToken.get("access_token").toString(),RestApiKey,Redirect_URI);
			
			System.out.println("userInfo="+userInfo);
			
			String id = userInfo.path("id").asText();
			String nickname = null;
			String thumbnailImage = null;
			String profileImage = null;
			String email = null;			
			String birthday = null;
			
			
			JsonNode properties = userInfo.path("properties");
			JsonNode kakaoAccount = userInfo.path("kakao_account");
			JsonNode profile = kakaoAccount.path("profile");
			
			
			if (!properties.isMissingNode()) {
				nickname = profile.path("nickname").asText();
				thumbnailImage = profile.path("thumbnail_image_url").asText();
				profileImage = profile.path("profile_image_url").asText();
				email = kakaoAccount.path("email").asText();
				birthday = kakaoAccount.path("birthday").asText();
			}
			
			Map<String, Object> userMap = new HashMap<String, Object>();
			
			userMap.put("mem_user_id", id);
			userMap.put("mem_nick_name", nickname);
 			userMap.put("mem_icon", profileImage);
			userMap.put("mem_photo", thumbnailImage);
			userMap.put("mem_access_token", jsonToken.get("access_token").toString().replaceAll("\"", ""));
			userMap.put("mem_email", email);
			userMap.put("mem_birth_day", birthday.replaceAll("-", ""));
			userMap.put("mem_join_type", BaseAppConstants.MEM_JOIN_TYPE_KAKAO);
			
			Map<String, Object> member = memberService.selectMember(userMap);
			
			if(member != null) {
				session.setAttribute("user", member);	
				session.setAttribute("appKey", appKey);
				
			}else {
				session.setAttribute("user", userMap);
				session.setAttribute("appKey", appKey);
			}			

		
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		mav.setViewName("redirect:/menu/menu.do");
		return mav;
	}
	
	
	@RequestMapping(value = "/kakaoLoginByKakaoUserInfo",produces="application/json", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView kakaoLoginByKakaoUserInfo(ModelAndView mav,HttpSession session, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			/*
			System.out.println("id"+paramMap.get("id").toString());
			System.out.println("email"+paramMap.get("email").toString());
			System.out.println("birthday"+paramMap.get("birthday").toString());
			System.out.println("nickname"+paramMap.get("nickname").toString());
			System.out.println("profileImage"+paramMap.get("profileImage").toString());
			System.out.println("thumbnailImage"+paramMap.get("thumbnailImage").toString());
			*/
			String id = paramMap.get("id").toString();
			String nickname = paramMap.get("nickname") != null && !paramMap.get("nickname").toString().equals("") ? paramMap.get("nickname").toString() :"" ;
			String thumbnailImage = paramMap.get("thumbnailImage") != null && !paramMap.get("thumbnailImage").toString().equals("") ? paramMap.get("thumbnailImage").toString() :"" ;
			String profileImage = paramMap.get("profileImage") != null && !paramMap.get("profileImage").toString().equals("") ? paramMap.get("profileImage").toString() :"" ;
			String email = paramMap.get("email") != null && !paramMap.get("email").toString().equals("") ? paramMap.get("email").toString() :"" ;			
			String birthday = paramMap.get("birthday") != null && !paramMap.get("birthday").toString().equals("") ? paramMap.get("birthday").toString() :"" ;
			
			Map<String, Object> userMap = new HashMap<String, Object>();
			
			userMap.put("mem_user_id", id);
			userMap.put("mem_nick_name", nickname);
 			userMap.put("mem_icon", profileImage);
			userMap.put("mem_photo", thumbnailImage);
			userMap.put("mem_access_token", "");
			userMap.put("mem_email", email);
			userMap.put("mem_birth_day", birthday.replaceAll("-", ""));
			userMap.put("mem_join_type", BaseAppConstants.MEM_JOIN_TYPE_KAKAO);
			
			Map<String, Object> member = memberService.selectMember(userMap);
			
			if(member != null) {
				session.setAttribute("user", member);	
				session.setAttribute("appKey", appKey);
				
			}else {
				session.setAttribute("user", userMap);
				session.setAttribute("appKey", appKey);
			}			

		
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		mav.setViewName("redirect:/menu/menu.do");
		return mav;
	}
	

	@RequestMapping(value = "/logoutByKakaoUserInfo", method = RequestMethod.GET)
	public String logoutByKakaoUserInfo(HttpSession session) {
	
		try{
			session.invalidate();	
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return "redirect:/menu/menu.do?type=logout";
	}
	
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpSession session) {
	
		try{
			Map<String, Object> userSessionMap = (Map<String, Object>)session.getAttribute("user");
			JsonNode userInfo = KakaoUtils.kakaoUserLogout(userSessionMap.get("mem_access_token").toString());
			session.invalidate();	
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return "redirect:/menu/menu.do?type=logout";
	}
	
	
	
}
