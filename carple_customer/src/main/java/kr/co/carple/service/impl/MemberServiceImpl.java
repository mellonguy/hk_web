package kr.co.carple.service.impl;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

import kr.co.carple.mapper.MemberMapper;
import kr.co.carple.service.MemberService;
import kr.co.carple.utils.BaseAppConstants;
import kr.co.carple.utils.ResultApi;
import kr.co.carple.vo.MemberVO;

@Service("memberService")
public class MemberServiceImpl implements MemberService{

	@Resource(name="memberMapper")
	private MemberMapper memberMapper;
	
	
	public List<Map<String, Object>> selectMemberList(Map<String, Object> map) throws Exception{
		return memberMapper.selectMemberList(map);
	}
	
	public Map<String, Object> selectMember(Map<String, Object> map) throws Exception{
		return memberMapper.selectMember(map);
	}
	
	@SuppressWarnings("unchecked")
	public ResultApi insertMember(MemberVO memberVO,HttpSession session) throws Exception{
		
		ResultApi result = new ResultApi();
		
		try {
			
			String memUuid = "MEM"+UUID.randomUUID().toString().replaceAll("-","");
			
			Map<String, Object> userSessionMap = (Map<String, Object>)session.getAttribute("user");
			memberVO.setMemUuid(memUuid);
			memberVO.setMemGrade(BaseAppConstants.MEM_GRADE_NORMAL);
			memberVO.setMemRole(BaseAppConstants.MEM_ROLE_NORMAL);
			memberVO.setMemStatus(BaseAppConstants.MEM_STATUS_NORMAL);
			memberVO.setMemPoint("0");
			memberVO.setMemAdminMemo("");
			memberVO.setMemIcon(userSessionMap.get("mem_icon") != null && !userSessionMap.get("mem_icon").toString().equals("")?userSessionMap.get("mem_icon").toString():"");
			memberVO.setMemPhoto(userSessionMap.get("mem_photo") != null && !userSessionMap.get("mem_photo").toString().equals("")?userSessionMap.get("mem_photo").toString():"");
			int test =  memberMapper.insertMember(memberVO);
			
		}catch(Exception e) {
			
			e.printStackTrace();
			result.setResultCode("E0004");
			
		}
		
		return result;
	}
	
	public void deleteMember(Map<String, Object> map) throws Exception{
		memberMapper.deleteMember(map);
	}
	
	public void updateMemberInfo(Map<String, Object> map) throws Exception{
		memberMapper.updateMemberInfo(map);
	}
	
	
	public ResultApi updateMemDeviceKey(Map<String, Object> map) throws Exception{
		
		ResultApi result = new ResultApi();
		
		try {
			memberMapper.updateMemDeviceKey(map);
		}catch(Exception e) {
			result.setResultCode("E004");
		}
		return result;
	}
	
	
	
	
	
	
	
}
