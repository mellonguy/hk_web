package kr.co.carple.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carple.mapper.ConsignRequestAcceptMapper;
import kr.co.carple.service.ConsignRequestAcceptService;
import kr.co.carple.vo.ConsignRequestAcceptVO;

@Service("consignRequestAcceptService")
public class ConsignRequestAcceptServiceImpl implements ConsignRequestAcceptService{

	
	@Resource(name="consignRequestAcceptMapper")
	private ConsignRequestAcceptMapper consignRequestAcceptMapper;
	
	
	public List<Map<String, Object>> selectConsignRequestAcceptList(Map<String, Object> map) throws Exception{
		return consignRequestAcceptMapper.selectConsignRequestAcceptList(map);
	}
	
	public Map<String, Object> selectConsignRequestAccept(Map<String, Object> map) throws Exception{
		return consignRequestAcceptMapper.selectConsignRequestAccept(map);
	}
	
	public int insertConsignRequestAccept(ConsignRequestAcceptVO consignRequestAcceptVO) throws Exception{
		return consignRequestAcceptMapper.insertConsignRequestAccept(consignRequestAcceptVO);
	}
	
	public void deleteConsignRequestAccept(Map<String, Object> map) throws Exception{
		consignRequestAcceptMapper.deleteConsignRequestAccept(map);
	}
	
	
	
	
	
}
