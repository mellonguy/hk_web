package kr.co.carple.service;

import java.util.List;
import java.util.Map;

import kr.co.carple.utils.ResultApi;
import kr.co.carple.vo.ConsignRequestBidVO;

public interface ConsignRequestBidService {

	public List<Map<String, Object>> selectConsignRequestBidList(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectConsignRequestBid(Map<String, Object> map) throws Exception;
	public ResultApi insertConsignRequestBid(Map<String,Object> map) throws Exception;
	public void deleteConsignRequestBid(Map<String, Object> map) throws Exception;
	public void updateConsignRequestBidCrbStatus(Map<String, Object> map) throws Exception;
	public ResultApi updateConsignRequestBid(Map<String, Object> map) throws Exception;
	public void updateConsignRequestBidCrbStatusForBidSuccess(Map<String, Object> map) throws Exception;	
	
	
}
