package kr.co.carple.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import kr.co.carple.utils.ResultApi;
import kr.co.carple.vo.MemberVO;

public interface MemberService {

	
	
	public List<Map<String, Object>> selectMemberList(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectMember(Map<String, Object> map) throws Exception;
	public ResultApi insertMember(MemberVO memberVO,HttpSession session) throws Exception;
	public void deleteMember(Map<String, Object> map) throws Exception;
	public void updateMemberInfo(Map<String, Object> map) throws Exception;
	public ResultApi updateMemDeviceKey(Map<String, Object> map) throws Exception;
	
	
	
	
}
