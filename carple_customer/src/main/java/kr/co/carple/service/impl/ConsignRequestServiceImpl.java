package kr.co.carple.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import kr.co.carple.mapper.ConsignRequestMapper;
import kr.co.carple.service.ConsignRequestBidService;
import kr.co.carple.service.ConsignRequestDetailService;
import kr.co.carple.service.ConsignRequestService;
import kr.co.carple.service.FileService;
import kr.co.carple.service.FileUploadService;
import kr.co.carple.timer.StatusUpdateTimerTask;
import kr.co.carple.utils.BaseAppConstants;
import kr.co.carple.utils.ParamUtils;
import kr.co.carple.utils.ResultApi;
import kr.co.carple.vo.ConsignRequestDetailVO;
import kr.co.carple.vo.ConsignRequestVO;
import kr.co.carple.vo.FileVO;

@Service("consignRequestService")
public class ConsignRequestServiceImpl implements ConsignRequestService{
	
	
	
	@Resource(name="consignRequestMapper")
	private ConsignRequestMapper consignRequestMapper;
	
	@Value("#{appProp['upload.base.path']}")
    private String rootDir;
	private String subDir;
	
	@Autowired
	private ConsignRequestDetailService consignRequestDetailService;
	
	@Autowired
	private FileUploadService fileUploadService;
	
	
	@Autowired
	private FileService fileService;
	
	
	@Autowired
	private ConsignRequestBidService consignRequestBidService;
	
	
	public List<Map<String, Object>> selectConsignRequestList(Map<String, Object> map) throws Exception{
		return consignRequestMapper.selectConsignRequestList(map);
	}
	
	public Map<String, Object> selectConsignRequest(Map<String, Object> map) throws Exception{
		return consignRequestMapper.selectConsignRequest(map);
	}
	
	
	
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor={Exception.class})
	public ResultApi insertConsignRequest(HttpServletRequest request, HttpSession session, String[] contentArr) throws Exception{
		
		ResultApi result = new ResultApi();
		
		try {
		
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null) {
						
				MultipartHttpServletRequest multipartRequest = null;
				List<MultipartFile> fileList = null;
				try {
					multipartRequest = (MultipartHttpServletRequest)request;	
					fileList = multipartRequest.getFiles("carPicture"); 
				}catch(Exception e) {
					//사진이 없는경우
				}
				
				//corId가 있는 경우는 
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				//기존에 작성 되어 있던 탁송건을 취소(안보이게)처리 하고 새로 등록 한다.
				if(paramMap.get("corId") != null && !paramMap.get("corId").toString().equals("")) {
				
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("corId", paramMap.get("corId").toString());
					map.put("corReadFlag", "N");
					this.updateCorReadFlag(map);
				}
				
				int carCount = Integer.valueOf(contentArr[4]);
				ConsignRequestVO consignRequestVO = new ConsignRequestVO();
				String corId ="COR"+UUID.randomUUID().toString().replaceAll("-","");
	
				consignRequestVO.setCorId(corId);
				consignRequestVO.setCorMemUuid(userSessionMap.get("mem_uuid").toString());
				consignRequestVO.setCorBatchYn("N");
				consignRequestVO.setCorStatus(BaseAppConstants.BID_STATUS_WAIT);
				consignRequestVO.setCorDepartureDt(contentArr[0]); //출발일
				consignRequestVO.setCorDepartureTime(contentArr[1]); //출발시간
				consignRequestVO.setCorTitle(contentArr[2]); //제목
				
				if(contentArr[3].equals(BaseAppConstants.CARRIER_TYPE_SL)) {
					consignRequestVO.setCorCarrierType(BaseAppConstants.CARRIER_CODE_SL);
				}else if(contentArr[3].equals(BaseAppConstants.CARRIER_TYPE_CA)) {
					consignRequestVO.setCorCarrierType(BaseAppConstants.CARRIER_CODE_CA);
				}else if(contentArr[3].equals(BaseAppConstants.CARRIER_TYPE_RD)) {
					consignRequestVO.setCorCarrierType(BaseAppConstants.CARRIER_CODE_RD);
				}else if(contentArr[3].equals(BaseAppConstants.CARRIER_TYPE_TR)) {
					consignRequestVO.setCorCarrierType(BaseAppConstants.CARRIER_CODE_TR);
				}else if(contentArr[3].equals(BaseAppConstants.CARRIER_TYPE_FC)) {
					consignRequestVO.setCorCarrierType(BaseAppConstants.CARRIER_CODE_FC);
				}else if(contentArr[3].equals(BaseAppConstants.CARRIER_TYPE_BC)) {
					consignRequestVO.setCorCarrierType(BaseAppConstants.CARRIER_CODE_BC);
				}else if(contentArr[3].equals(BaseAppConstants.CARRIER_TYPE_NE)) {
					consignRequestVO.setCorCarrierType(BaseAppConstants.CARRIER_CODE_NE);
				}
				consignRequestVO.setCorCarCnt(contentArr[4]);
				if(contentArr[5].equals(BaseAppConstants.AUCTION_TYPE_RC)) {
					consignRequestVO.setCorAuctionType(BaseAppConstants.AUCTION_CODE_RC);
				}else if(contentArr[5].equals(BaseAppConstants.AUCTION_TYPE_DR)) {
					consignRequestVO.setCorAuctionType(BaseAppConstants.AUCTION_CODE_DR);
				}
				consignRequestVO.setCorAuctionAmount(contentArr[6]);
				
				if(contentArr[7].equals(BaseAppConstants.BID_DECISION_TYPE_PR)) {
					consignRequestVO.setCorBidDecisionType(BaseAppConstants.BID_DECISION_CODE_PR);
				}else if(contentArr[7].equals(BaseAppConstants.BID_DECISION_TYPE_OA)) {
					consignRequestVO.setCorBidDecisionType(BaseAppConstants.BID_DECISION_CODE_OA);
				}
				if(contentArr[8].equals(BaseAppConstants.BID_DECISION_TIME_10)) {
					consignRequestVO.setCorBidDecisionTime(BaseAppConstants.BID_DECISION_CODE_10);
				}else if(contentArr[8].equals(BaseAppConstants.BID_DECISION_TIME_20)) {
					consignRequestVO.setCorBidDecisionTime(BaseAppConstants.BID_DECISION_CODE_20);
				}else if(contentArr[8].equals(BaseAppConstants.BID_DECISION_TIME_30)) {
					consignRequestVO.setCorBidDecisionTime(BaseAppConstants.BID_DECISION_CODE_30);
				}
				consignRequestMapper.insertConsignRequest(consignRequestVO);
				
				int cpr = 0;
				
				for(int v = 0; v < carCount; v++ ) {
					//기본정보입력갯수
					int gap = 9;
					//차량정보및주소갯수
					int offset = 11;
					ConsignRequestDetailVO consignRequestDetailVO = new ConsignRequestDetailVO();
					consignRequestDetailVO.setCrdId("CRD"+UUID.randomUUID().toString().replaceAll("-",""));
					consignRequestDetailVO.setCrdCorId(corId);
					consignRequestDetailVO.setCrdCarMaker(contentArr[(v*offset)+gap]);	//제조사
					consignRequestDetailVO.setCrdCarKind(contentArr[(v*offset)+1+gap]);	//차종
					consignRequestDetailVO.setCrdCarTrim(contentArr[(v*offset)+2+gap]);	//트림
					consignRequestDetailVO.setCrdCarNum(contentArr[(v*offset)+3+gap]);	//차량번호
					consignRequestDetailVO.setCrdCarIdNum(contentArr[(v*offset)+4+gap]);//차대번호
					consignRequestDetailVO.setCrdStatus(BaseAppConstants.CRD_STATUS_NORMAL);	//개별건의 상태
										
					int fileCnt = Integer.parseInt(contentArr[(v*offset)+5+gap]);
					
					//파일 업로드.........
					for(int i = cpr; i < fileCnt+cpr; i++) {
					
						if(multipartRequest != null && fileList != null && fileCnt > 0) {
							MultipartFile mFile = fileList.get(i);
							subDir = "carPic";
							
							Map<String, Object> fileMap = fileUploadService.upload(rootDir, subDir, mFile);
							FileVO fileVO = new FileVO();
		    				fileVO.setCrfCategoryType(subDir);
		    				fileVO.setCrfCrdId(consignRequestDetailVO.getCrdId());
		    				fileVO.setCrfId("CRF"+UUID.randomUUID().toString().replaceAll("-", ""));
		    				fileVO.setCrfCorId(corId);
		    				fileVO.setCrfStatus("");
		    				fileVO.setCrfFileNm(fileMap.get("ORG_NAME").toString());
		    				fileVO.setCrfFilePath(fileMap.get("SAVE_NAME").toString());
		    				fileService.insertFile(fileVO);	
						}						
					}
					cpr += fileCnt;
					
					if(contentArr[(v*offset)+6+gap].equals(BaseAppConstants.CAR_STATUS_TYPE_NC)) {
						consignRequestDetailVO.setCrdCarType(BaseAppConstants.CAR_STATUS_CODE_NC);
					}else if(contentArr[(v*offset)+6+gap].equals(BaseAppConstants.CAR_STATUS_TYPE_OC)) {
						consignRequestDetailVO.setCrdCarType(BaseAppConstants.CAR_STATUS_CODE_OC);
					}
					consignRequestDetailVO.setCrdDeparture(contentArr[(v*offset)+7+gap]);								//출발지
					consignRequestDetailVO.setCrdDepartureLatlng(contentArr[(v*offset)+8+gap]);						//출발지 좌표
					consignRequestDetailVO.setCrdDeparturePersonInCharge("");			//출발지 담당자
					consignRequestDetailVO.setCrdDeparturePhone("");						//출발지 연락처
					consignRequestDetailVO.setCrdArrival(contentArr[(v*offset)+9+gap]);										//도착지
					consignRequestDetailVO.setCrdArrivalLatlng(contentArr[(v*offset)+10+gap]);								//도착지 좌표
					consignRequestDetailVO.setCrdArrivalPersonInCharge("");					//도착지 담당자
					consignRequestDetailVO.setCrdArrivalPhone("");								//도착지 연락처
					consignRequestDetailVO.setCrdDistance("");									//출발지와 도착지의 거리
					consignRequestDetailVO.setCrdSignificantData("");							//중요사함.
					consignRequestDetailService.insertConsignRequestDetail(consignRequestDetailVO);
				}
	
				//경매건이 전부 입력 되지 않으면 타이머도 안돌리고 ....... 전부 입력 되고 5분이 지난 후에 경매 대기 상태에서 진행중으로 변경 한다.
				Timer test = new Timer();
				long minute = 60*1000;
				long second = 1*1000;
				StatusUpdateTimerTask task = new StatusUpdateTimerTask(session,corId,consignRequestVO.getCorStatus());	
				//test.schedule(task,minute*BaseAppConstants.STATUS_CHANGE_WW_TO_II);
				
				// 테스트를 위해 10초로 설정
				test.schedule(task,second*10);

			}else {
				
				result.setResultCode("E000");
				result.setResultMsg("로그인이 되어 있지 않습니다.");
			}
			
			
		}catch(Exception e) {
			e.printStackTrace();
			result.setResultCode("E001");
			throw new Exception();
		}
		
		
		
		return result;
	}
	
	public void deleteConsignRequest(Map<String, Object> map) throws Exception{
		consignRequestMapper.deleteConsignRequest(map);		
	}

	
	
	public ResultApi updateCorStatus(Map<String, Object> map) throws Exception{
		
		ResultApi result = new ResultApi();
		
		try {
		
			Map<String, Object> consignRequest = this.selectConsignRequest(map);
			
			System.out.println("updateCorStatus 실행됨");
			if(consignRequest != null) {
			
				//취소되는 경우가 아니면 그냥 업데이트 하고....
				if(!map.get("corStatus").toString().equals("CC")) {
					consignRequestMapper.updateCorStatus(map);
				
				}else if(map.get("corStatus").toString().equals("CC")){
				//취소되는 경우.....현재 상태가 대기중 또는 진행중인 경우에만 취소를 할 수 있도록 함
					
					if(consignRequest.get("cor_status").equals("WW") || consignRequest.get("cor_status").equals("II")) {
						
						//입찰된 건을 취소 처리 한다.
						map.put("crbStatus", BaseAppConstants.CRB_STATUS_FAIL);
						consignRequestBidService.updateConsignRequestBidCrbStatusForBidSuccess(map);
						
						//취소 한다.
						consignRequestMapper.updateCorStatus(map);
					}else {
						result.setResultCode("E002");
						result.setResultMsg("낙찰 이후에는 경매를 취소 할 수 없습니다.");
					}
					
				}
				
			}else {
				result.setResultCode("E003");
			}
			
			
		}catch(Exception e) {
			result.setResultCode("E004");
			e.printStackTrace();
		}
		
		return result;
		
	}
	
	public void updateCorReadFlag(Map<String, Object> map) throws Exception{
		consignRequestMapper.updateCorReadFlag(map);
	}
	
	
	

}








