package kr.co.carple.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carple.mapper.BbsMapper;
import kr.co.carple.service.BbsService;
import kr.co.carple.vo.BbsVO;

@Service("bbsService")
public class BbsServiceImpl implements BbsService{

	
	@Resource(name="bbsMapper")
	private BbsMapper bbsMapper;
	
	
	
	public int insertBbs(BbsVO bbsVO) throws Exception{
		return bbsMapper.insertBbs(bbsVO);
	}
	
	public List<Map<String, Object>> selectBbsList(Map<String, Object> map) throws Exception{
		return bbsMapper.selectBbsList(map);
	}
	
	public Map<String,Object>selectBbs(Map<String,Object>map) throws Exception{
		return bbsMapper.selectBbs(map);
	}
	
	public void deleteBbs(Map<String, Object> map) throws Exception{
		bbsMapper.deleteBbs(map);
	}
	
	
	
	
	
	
	
	
}
