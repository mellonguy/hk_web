package kr.co.carple.service.impl;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import kr.co.carple.mapper.FileMapper;
import kr.co.carple.service.FileService;
import kr.co.carple.service.FileUploadService;
import kr.co.carple.utils.ResultApi;
import kr.co.carple.vo.FileVO;

@Service("fileService")
public class FileServiceImpl implements FileService{


	@Resource(name="fileMapper")
	private FileMapper fileMapper;
	
	@Autowired
	private FileUploadService fileUploadService;
	
	
	public ResultApi insertFile(FileVO fileVO) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			fileMapper.insertFile(fileVO);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return result; 
		
	}
	public ResultApi insertFile(String rootDir, String subDir,String contentId,HttpServletRequest request) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
	    	List<MultipartFile> fileList = multipartRequest.getFiles("bbsFile");
	    	List<MultipartFile> driverFileList = multipartRequest.getFiles("driverFile");
	    	List<MultipartFile> driverSealFileList = multipartRequest.getFiles("driverSealFile");
	    	if(fileList != null && fileList.size() > 0){
	    		result.setResultCode("0000");
	    		for(MultipartFile mFile : fileList){	
	    			if(mFile != null && mFile.getSize() > 0){
	    				FileVO fileVO = new FileVO();
	    				Map<String, Object> fileMap = fileUploadService.upload(rootDir, subDir, mFile);
	    				
	    		/*		fileVO.setContentId(contentId);
	    				fileVO.setCategoryType(subDir);
	    				fileVO.setFileId("FLE"+UUID.randomUUID().toString().replaceAll("-", ""));
	    				fileVO.setFileNm(fileMap.get("ORG_NAME").toString());
	    				fileVO.setFilePath(fileMap.get("SAVE_NAME").toString());*/
	    				fileMapper.insertFile(fileVO);		
	    				
	    			}
	    		}
	    	}else{
	    		result.setResultCode("1111"); 	//file 없음
	    	}
	    	
	    	if(driverFileList != null && driverFileList.size() > 0) {
	    		result.setResultCode("0000");
	    		for(MultipartFile mFile : driverFileList){	
	    			if(mFile != null && mFile.getSize() > 0){
	    				FileVO fileVO = new FileVO();
	    				Map<String, Object> fileMap = fileUploadService.upload(rootDir, subDir, mFile);
	    		/*		fileVO.setContentId(contentId);
	    				fileVO.setCategoryType("driverPic");
	    				fileVO.setFileId("FLE"+UUID.randomUUID().toString().replaceAll("-", ""));
	    				fileVO.setFileNm(fileMap.get("ORG_NAME").toString());
	    				fileVO.setFilePath(fileMap.get("SAVE_NAME").toString());*/
	    				fileMapper.insertFile(fileVO);		
	    			}
	    		}
	    	}else{
	    		result.setResultCode("1111"); 	//file 없음
	    	}
	    	
	    	
	    	if(driverSealFileList != null && driverSealFileList.size() > 0) {
	    		result.setResultCode("0000");
	    		for(MultipartFile mFile : driverSealFileList){	
	    			if(mFile != null && mFile.getSize() > 0){
	    				FileVO fileVO = new FileVO();
	    				Map<String, Object> fileMap = fileUploadService.upload(rootDir, subDir, mFile);
	    				/*fileVO.setContentId(contentId);
	    				fileVO.setCategoryType("driverSeal");
	    				fileVO.setFileId("FLE"+UUID.randomUUID().toString().replaceAll("-", ""));
	    				fileVO.setFileNm(fileMap.get("ORG_NAME").toString());
	    				fileVO.setFilePath(fileMap.get("SAVE_NAME").toString());*/
	    				fileMapper.insertFile(fileVO);		
	    			}
	    		}
	    	}else{
	    		result.setResultCode("1111"); 	//file 없음
	    	}
	    	
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return result; 
		
		
		
	}
	
	public List<Map<String, Object>> selectFileList(Map<String, Object> map) throws Exception{
		return fileMapper.selectFileList(map);
		
	}
	
	public void deleteFile(Map<String, Object> map) throws Exception{
		fileMapper.deleteFile(map);
	}
	
	public ResultApi insertFile(String rootDir, String subDir,File file) throws Exception{
		
		ResultApi result = new ResultApi();
		try {
			
			
			
			
		}catch(Exception e) {
			
			e.printStackTrace();
			
		}
		
		return result;
		
		
	}
	
	public Map<String,Object>selectFile(Map<String,Object>map) throws Exception{
		return fileMapper.selectFile(map);
	}
	
	
}
