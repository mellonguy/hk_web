package kr.co.carple.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carple.mapper.ConsignRequestDetailMapper;
import kr.co.carple.service.ConsignRequestDetailService;
import kr.co.carple.vo.ConsignRequestDetailVO;

@Service("consignRequestDetail")
public class ConsignRequestDetailServiceImpl implements ConsignRequestDetailService{

	
	@Resource(name="consignRequestDetailMapper")
	private ConsignRequestDetailMapper consignRequestDetailMapper;
	
	public List<Map<String, Object>> selectConsignRequestDetailList(Map<String, Object> map) throws Exception{
		return consignRequestDetailMapper.selectConsignRequestDetailList(map);
	}
	
	public Map<String, Object> selectConsignRequestDetail(Map<String, Object> map) throws Exception{
		return consignRequestDetailMapper.selectConsignRequestDetail(map);
	}
	
	public int insertConsignRequestDetail(ConsignRequestDetailVO consignRequestDetailVO) throws Exception{
		return consignRequestDetailMapper.insertConsignRequestDetail(consignRequestDetailVO);
	}
	
	public void deleteConsignRequestDetail(Map<String, Object> map) throws Exception{
		consignRequestDetailMapper.deleteConsignRequestDetail(map);
	}
	
	
	
	
	
}
