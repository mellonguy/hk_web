package kr.co.carple.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carple.mapper.DriverMapper;
import kr.co.carple.service.DriverService;
import kr.co.carple.vo.DriverVO;

@Service("driverService")
public class DriverServiceImpl implements DriverService{

	
	
	@Resource(name="driverMapper")
	private DriverMapper driverMapper;
	
	
	public List<Map<String, Object>> selectDriverList(Map<String, Object> map) throws Exception{
		return driverMapper.selectDriverList(map);		
	}
	
	public Map<String, Object> selectDriver(Map<String, Object> map) throws Exception{
		return driverMapper.selectDriver(map);
	}
	
	public int insertDriver(DriverVO driverVO) throws Exception{
		return driverMapper.insertDriver(driverVO);
	}
	
	public void deleteDriver(Map<String, Object> map) throws Exception{
		driverMapper.deleteDriver(map);		
	}
	
	
}
