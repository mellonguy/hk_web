package kr.co.carple.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import kr.co.carple.utils.ResultApi;
import kr.co.carple.vo.ConsignRequestVO;

public interface ConsignRequestService {

	public List<Map<String, Object>> selectConsignRequestList(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectConsignRequest(Map<String, Object> map) throws Exception;
	public ResultApi insertConsignRequest(HttpServletRequest request, HttpSession session, String[] contentArr) throws Exception;
	public void deleteConsignRequest(Map<String, Object> map) throws Exception;
	public ResultApi updateCorStatus(Map<String, Object> map) throws Exception;
	public void updateCorReadFlag(Map<String, Object> map) throws Exception;
	
}
