package kr.co.carple.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import kr.co.carple.service.FcmService;


@Service("fcmService")
public class FcmServiceImpl implements FcmService{

	
	@Value("#{appProp['pushKeyForios']}")
    private String pushKeyForios;    
    
	@Value("#{appProp['pushKeyForaos']}")
    private String pushKeyForaos;
	
	
	public String fcmSendMessage(Map<String, Object> map)throws Exception{
        
            String apiKey = pushKeyForaos;
        
            //	hkcarcarrier@gmail.com 계정으로 ios 푸시 알림 firebase 콘솔에 등록 해 놓았음...
             if(map.get("device_os").toString().equals("ios")){
            	 apiKey = pushKeyForios;
             }
             
            URL url = new URL("https://fcm.googleapis.com/fcm/send");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Authorization", "key=" + apiKey);

            conn.setDoOutput(true);

            // 이렇게 보내면 주제를 ALL로 지정해놓은 모든 사람들한테 알림을 날려준다.
            String input = "{\"notification\" : {\"title\" : \""+map.get("title").toString()+" \", \"body\" : \""+map.get("body").toString()+"\"}, \"to\":\""+map.get("fcm_token").toString()+"\"}";
            
            // 이걸로 보내면 특정 토큰을 가지고있는 어플에만 알림을 날려준다  위에 둘중에 한개 골라서 날려주자
           // String input = "{\"notification\" : {\"title\" : \" 여기다 제목넣기 \", \"body\" : \"여기다 내용 넣기\"}, \"to\":\" 여기가 받을 사람 토큰  \"}";

            OutputStream os = conn.getOutputStream();
            
            // 서버에서 날려서 한글 깨지는 사람은 아래처럼  UTF-8로 인코딩해서 날려주자
            os.write(input.getBytes("UTF-8"));
            os.flush();
            os.close();

            int responseCode = conn.getResponseCode();
            System.out.println("\nSending 'POST' request to URL : " + url);
            System.out.println("Post parameters : " + input);
            System.out.println("Response Code : " + responseCode);
            
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            // print result
            System.out.println(response.toString());
            

    return "jsonView";
}

	
	
	
	
	
	
	
}
