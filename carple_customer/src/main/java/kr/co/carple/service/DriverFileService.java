package kr.co.carple.service;

import java.io.File;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import kr.co.carple.utils.ResultApi;
import kr.co.carple.vo.DriverFileVO;

public interface DriverFileService {

	public ResultApi insertDriverFile(DriverFileVO driverFileVO) throws Exception;
	public ResultApi insertDriverFile(String rootDir, String subDir,String bbsId,HttpServletRequest request) throws Exception;
	public ResultApi insertDriverFile(String rootDir, String subDir,File file) throws Exception;
	public Map<String,Object>selectDriverFile(Map<String,Object>map) throws Exception;
	public List<Map<String, Object>> selectDriverFileList(Map<String, Object> map) throws Exception;
	public void deleteDriverFile(Map<String, Object> map) throws Exception;
	
	
	
}
