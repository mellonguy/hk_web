package kr.co.carple.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import kr.co.carple.mapper.ConsignRequestBidMapper;
import kr.co.carple.service.ConsignRequestAcceptService;
import kr.co.carple.service.ConsignRequestBidService;
import kr.co.carple.service.ConsignRequestService;
import kr.co.carple.service.DriverService;
import kr.co.carple.service.FcmService;
import kr.co.carple.utils.BaseAppConstants;
import kr.co.carple.utils.ResultApi;
import kr.co.carple.vo.ConsignRequestAcceptVO;
import kr.co.carple.vo.ConsignRequestBidVO;

@Service("consignRequestBidService")
public class ConsignRequestBidServiceImpl implements ConsignRequestBidService{

	
	@Resource(name="consignRequestBidMapper")
	private ConsignRequestBidMapper consignRequestBidMapper;
	
	
	
	@Autowired
	private ConsignRequestService consignRequestService;
	
	@Autowired
	private ConsignRequestAcceptService consignRequestAcceptService;
	
	@Autowired
	private FcmService fcmService;
	
	@Autowired
	private DriverService driverService;
	
	public List<Map<String, Object>> selectConsignRequestBidList(Map<String, Object> map) throws Exception{
		return consignRequestBidMapper.selectConsignRequestBidList(map);
	}
	
	public Map<String, Object> selectConsignRequestBid(Map<String, Object> map) throws Exception{
		return consignRequestBidMapper.selectConsignRequestBid(map);
	}
	
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor={Exception.class})
	public ResultApi insertConsignRequestBid(Map<String,Object> map) throws Exception{
		
		ResultApi result = new ResultApi();
		try {
			
			ConsignRequestBidVO consignRequestBidVO = new ConsignRequestBidVO();
			String crbId ="CRB"+UUID.randomUUID().toString().replaceAll("-","");
			consignRequestBidVO.setCrbId(crbId);
			consignRequestBidVO.setCorId(map.get("corId").toString());
			consignRequestBidVO.setCrbDvrUserId(map.get("dvrUserId").toString());
			consignRequestBidVO.setCrbType("N");
			consignRequestBidVO.setCrbStatus("N");
			consignRequestBidVO.setCrbAmount(map.get("amount").toString());
			consignRequestBidVO.setCrbDvrLat("");
			consignRequestBidVO.setCrbDvrLng("");
			consignRequestBidVO.setCrbDvrAddress("");
			consignRequestBidVO.setCrbDvrDistance("");
			
			
			List<Map<String, Object>> allBidList = this.selectConsignRequestBidList(map);
			map.put("validation", "Y");
			List<Map<String, Object>> bidList = this.selectConsignRequestBidList(map);
			
			if(bidList.size() > 0) {
				
				//같은건에 5회까지만 참여 할 수 있도록 한다.
				if(allBidList.size() >= BaseAppConstants.BID_MAX_CNT) {
					result.setResultCode("E011");
					result.setResultMsg("동일한 건에 참여 할 수 있는 최대 입찰 건수는 "+String.valueOf(BaseAppConstants.BID_MAX_CNT)+" 건 입니다.");
				}else {
					
					//입찰가 오름차순이므로 가장 마지막 리스트를 가져온다.
					Map<String, Object> bidMap = bidList.get(bidList.size()-1);
					int amount = Integer.parseInt(bidMap.get("amount").toString().replaceAll(",", ""));
					
					//같은 입찰건에 참여 하는경우 기존의 입찰가보다 높거나 같은 금액은 작성 할 수 없도록 한다. 
					if(Integer.parseInt(map.get("amount").toString()) >= amount){
						result.setResultCode("E010");
						result.setResultMsg("기존에 참여한 입찰가보다 높거나 같은 금액으로 참여 할 수 없습니다.");
					}else {
						consignRequestBidMapper.insertConsignRequestBid(consignRequestBidVO);		
					}	
					
				}
				
			}else {
				consignRequestBidMapper.insertConsignRequestBid(consignRequestBidVO);
			}
			
		}catch(Exception e) {
			e.printStackTrace();	
			result.setResultCode("E000");
			throw new Exception();
		}
		return result;
	}
	
	public void deleteConsignRequestBid(Map<String, Object> map) throws Exception{
		consignRequestBidMapper.deleteConsignRequestBid(map);
	}
	
	public void updateConsignRequestBidCrbStatusForBidSuccess(Map<String, Object> map) throws Exception{
		consignRequestBidMapper.updateConsignRequestBidCrbStatusForBidSuccess(map);
	}
	
	
	public void updateConsignRequestBidCrbStatus(Map<String, Object> map) throws Exception{
		consignRequestBidMapper.updateConsignRequestBidCrbStatus(map);
	}
	
	
	
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor={Exception.class})
	public ResultApi updateConsignRequestBid(Map<String, Object> map) throws Exception{
		
		ResultApi result = new ResultApi();
		try {
			
			
			//낙찰 진행 하는 입찰건에 대해 취소된 건인지 확인 진행...
			Map<String, Object> bid = this.selectConsignRequestBid(map);
			if(bid != null) {
				if(bid.get("crb_status").toString().equals("C")) {
					result.setResultCode("E010");
					result.setResultMsg("해당 입찰건은 취소된 입찰건 입니다.");
				}else {
					
					if(map.get("crbStatus").toString().equals("Y")) {
						
						//낙찰 결정 되는 경우 취소된 입찰건을 제외한 모든 입찰건에 대해 유찰 처리를 진행 한다.
						map.put("crbStatus", BaseAppConstants.CRB_STATUS_FAIL);
						this.updateConsignRequestBidCrbStatusForBidSuccess(map);
						
						//해당 입찰건을 낙찰 처리 한다.
						map.put("crbStatus", BaseAppConstants.CRB_STATUS_SUCCESS);
						this.updateConsignRequestBidCrbStatus(map);
						
						//해당 경매 건에 대해 상태를 낙찰로 변경 해 준다.
						map.put("corStatus", BaseAppConstants.BID_STATUS_DONE);
						consignRequestService.updateCorStatus(map);	
						
						Map<String, Object> consignRequestBid = this.selectConsignRequestBid(map);
						
						//낙찰 테이블에 insert 한다.
						ConsignRequestAcceptVO consignRequestAcceptVO = new ConsignRequestAcceptVO();
						consignRequestAcceptVO.setCraId("CRA"+UUID.randomUUID().toString().replaceAll("-",""));
						consignRequestAcceptVO.setCorId(map.get("corId").toString());
						consignRequestAcceptVO.setCraCrbAmount(consignRequestBid.get("amount").toString().replaceAll(",", ""));
						consignRequestAcceptVO.setCrbId(map.get("crbId").toString());
						consignRequestAcceptService.insertConsignRequestAccept(consignRequestAcceptVO);
						
						//낙찰된 경우 해당 입찰을 진행 한 기사에게 푸시 알림을 보낸다.
						Map<String, Object> driverSelectMap = new HashMap<String, Object>();
						driverSelectMap.put("dvrUserId", consignRequestBid.get("crb_dvr_user_id").toString());
						Map<String, Object> driverMap = driverService.selectDriver(driverSelectMap);
						
						if(driverMap != null && driverMap.get("dvr_device_key") != null && !driverMap.get("dvr_device_key").toString().equals("")) {
							
							Map<String, Object> consignRequest = consignRequestService.selectConsignRequest(map);
							Map<String, Object> sendMessageMap = new HashMap<String, Object>();
							sendMessageMap.put("title", "입찰 진행한 건이 낙찰 되었습니다. 입찰가 : "+consignRequestBid.get("amount").toString()+"원");
							sendMessageMap.put("body", "견적 제목 : "+consignRequest.get("cor_title").toString()+"\r\n"+"차량 대수 : "+consignRequest.get("cor_car_cnt").toString()+"\r\n"+"출발일 : "+consignRequest.get("cor_departure_dt").toString());
							
							
							sendMessageMap.put("fcm_token", driverMap.get("dvr_device_key").toString());
							sendMessageMap.put("device_os", driverMap.get("dvr_login_env").toString());
							fcmService.fcmSendMessage(sendMessageMap);
							
						}
						
					}
					
				}
			
			}else {
				result.setResultCode("E011");
				result.setResultMsg("해당 입찰건을 찾을 수 없습니다.");
			}
			
			
		}catch(Exception e) {
			result.setResultCode("E001");
			e.printStackTrace();
			throw new Exception();
		}
		return result;
	}


	
	
	
	
	
	
	
	
}
