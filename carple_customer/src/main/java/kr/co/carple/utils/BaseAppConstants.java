package kr.co.carple.utils;

public class BaseAppConstants {

	public static final int NUM_OF_ROWS = 15;
	public static final int NUM_OF_ROWS_FOR_PLAYER_LIST = 15;
	public static final int NUM_OF_ROWS_FOR_END_LIST = 50;
	public static final int NUM_OF_PAGES =10;
	public static final String CHARACTER_SET = "UTF-8";
	public static final String ADMIN_SESSION_KEY = "admin";
	public static final String USER_SESSION_KEY = "user";
	public static final String USER_TEMP_SESSION_KEY = "temp_user";
	public static final String GAME_CODE = "01";
	public static final String GAME_PARTICIPATION_DATE = "gameParticipationDate";
	public static final String GAME_PARTICIPATION_DATE_TIME = "gameParticipationDateTime";
	public static final String TIME_REMAINING = "time_remaining";
	public static final String REAL_USER_MAP = "realUserMap";   
	public static final String SYSTEM_NM = "system";  

	public static final String CARRIER_TYPE_SL = "세이프티로더";
	public static final String CARRIER_TYPE_CA = "캐리어";
	public static final String CARRIER_TYPE_RD = "로드탁송";
	public static final String CARRIER_TYPE_TR = "추레라";
	public static final String CARRIER_TYPE_FC = "풀카";
	public static final String CARRIER_TYPE_BC = "박스카";
	public static final String CARRIER_TYPE_NE = "상관없음";
	public static final String CARRIER_CODE_SL = "SL";
	public static final String CARRIER_CODE_CA = "CA";
	public static final String CARRIER_CODE_RD = "RD";
	public static final String CARRIER_CODE_TR = "TR";
	public static final String CARRIER_CODE_FC = "FC";
	public static final String CARRIER_CODE_BC = "BC";
	public static final String CARRIER_CODE_NE = "NE";
	
	public static final String AUCTION_TYPE_RC = "역경매";
	public static final String AUCTION_TYPE_DR = "오픈형";
	public static final String AUCTION_CODE_RC = "RC";
	public static final String AUCTION_CODE_DR = "DR";
	
	public static final String BID_DECISION_TYPE_PR = "우선순위결정";
	public static final String BID_DECISION_TYPE_OA = "선착순결정";
	public static final String BID_DECISION_CODE_PR = "PR";
	public static final String BID_DECISION_CODE_OA = "OA";
	
	public static final String BID_DECISION_TIME_10 = "10분";
	public static final String BID_DECISION_TIME_20 = "20분";
	public static final String BID_DECISION_TIME_30 = "30분";
	public static final String BID_DECISION_CODE_10 = "10";
	public static final String BID_DECISION_CODE_20 = "20";
	public static final String BID_DECISION_CODE_30 = "30";
	
	public static final String CAR_STATUS_TYPE_NC = "신차";
	public static final String CAR_STATUS_TYPE_OC = "중고차";
	public static final String CAR_STATUS_CODE_NC = "NC";
	public static final String CAR_STATUS_CODE_OC = "OC";
	
	
	public static final String BID_STATUS_WAIT = "WW";		//입찰대기중
	public static final String BID_STATUS_ING = "II";		//입찰진행중
	public static final String BID_STATUS_DONE = "DD";		//낙찰
	public static final String BID_STATUS_FAIL = "FF";		//유찰
	public static final String BID_STATUS_CANCEL = "CC";		//입찰취소
	public static final String BID_STATUS_RE = "RR";		//재입찰
	public static final String BID_STATUS_RE_FAIL = "RF";		//재입찰 후 유찰
	public static final String BID_STATUS_RE_CANCEL = "RC";		//재입찰 후 입찰취소
	public static final String BID_STATUS_SUCCESS = "SS";		//낙찰 후 완료
	
	public static final String CRB_STATUS_ING = "N";
	public static final String CRB_STATUS_CANCEL = "C";
	public static final String CRB_STATUS_SUCCESS = "Y";
	public static final String CRB_STATUS_FAIL = "F";
	
	public static final String MEM_JOIN_TYPE_KAKAO = "kakao";
	public static final String MEM_JOIN_TYPE_NAVER = "naver";
	
	public static final String MEM_LOGIN_ENV_WEB = "web";
	public static final String MEM_LOGIN_ENV_ANDROID = "android";
	public static final String MEM_LOGIN_ENV_IOS = "ios";
	
	
	public static final String MEM_GRADE_NORMAL = "N";		//일반
	public static final String MEM_GRADE_SPECIAL = "S";		//스페셜
	public static final String MEM_GRADE_VIP = "V";		//VIP
	
	public static final String MEM_ROLE_NORMAL = "N";		//일반
	public static final String MEM_ROLE_ADMIN = "A";			//관리자
	
	public static final String BBS_CATEGORY_NOTICE = "notice";	//공지사항
	public static final String BBS_CATEGORY_FREEBOARD = "freeBoard";	//자유게시판
	public static final String BBS_CATEGORY_FAQ = "faq";	//질문과답변
	public static final String BBS_CATEGORY_QUESTION = "question";	//자주 묻는 질문
	
	
	
	public static final String MEM_STATUS_NORMAL = "0";		//정상
	public static final String MEM_STATUS_STOP = "1";		//정지
	public static final String MEM_STATUS_BLIND = "2";		//차단
	public static final String MEM_STATUS_WITHDRAW = "3";		//탈퇴
	
	public static final String CRD_STATUS_NORMAL = "N";			//정상(진행중)
	public static final String CRD_STATUS_ABNORMAL = "A";		//제외(이상)
	public static final String CRD_STATUS_SUCCESS = "S";		//완료
	
	
	public static final int STATUS_CHANGE_WW_TO_II = 5;
	public static final int BID_MAX_CNT = 5;
	
	
}
