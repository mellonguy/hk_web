package kr.co.carple.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

public class NaverUtils {
	
	public static JsonNode getAccessToken(String code, String state ,String clientId ,String clientSecret){
		
			
			final String RequestUrl = "https://nid.naver.com/oauth2.0/token";
			
		    final List<NameValuePair> postParams = new ArrayList<NameValuePair>();
		    postParams.add(new BasicNameValuePair("grant_type", "authorization_code"));
		    postParams.add(new BasicNameValuePair("client_id", clientId));
		    postParams.add(new BasicNameValuePair("client_secret", clientSecret));
		    postParams.add(new BasicNameValuePair("state", state));
		    postParams.add(new BasicNameValuePair("code", code));

		    final HttpClient client = HttpClientBuilder.create().build();
		    final HttpPost post = new HttpPost(RequestUrl);
		    JsonNode returnNode = null;
		
		    try {
		      post.setEntity(new UrlEncodedFormEntity(postParams));
		      final HttpResponse response = client.execute(post);
		      final int responseCode = response.getStatusLine().getStatusCode();

		      //JSON 형태 반환값 처리
		      ObjectMapper mapper = new ObjectMapper();
		      returnNode = mapper.readTree(response.getEntity().getContent());

		    } catch (UnsupportedEncodingException e) {
		      e.printStackTrace();
		    } catch (ClientProtocolException e) {
		      e.printStackTrace();
		    } catch (IOException e) {
		      e.printStackTrace();
		    } finally {
		        // clear resources
		    }
		    
		    return returnNode;
		
	}
	
	
	
	public static JsonNode getNaverUserInfo(String string) {
		
		
		 final String RequestUrl = "https://openapi.naver.com/v1/nid/me";
		
		
		 	String code = string; // 로그인 과정중 얻은 토큰 값

		    final HttpClient client = HttpClientBuilder.create().build();
		    final HttpPost post = new HttpPost(RequestUrl);
		    
		    // add header
		    post.addHeader("Authorization", "Bearer " + code);
		    
		    JsonNode returnNode = null;
		    
		    try {
		      final HttpResponse response = client.execute(post);
		      final int responseCode = response.getStatusLine().getStatusCode();

		      //JSON 형태 반환값 처리
		      ObjectMapper mapper = new ObjectMapper();
		      returnNode = mapper.readTree(response.getEntity().getContent());
		      
		    } catch (UnsupportedEncodingException e) {
		      e.printStackTrace();
		    } catch (ClientProtocolException e) {
		      e.printStackTrace();
		    } catch (IOException e) {
		      e.printStackTrace();
		    } finally {
		        // clear resources
		    }
		    return returnNode;
		}



	
	
	public static JsonNode naverUserLogout(String clientId, String clientSecret, String accessToken) {
		
		//https://nid.naver.com/oauth2.0/token?grant_type=delete&client_id={클라이언트 아이디}&client_secret={클라이언트 시크릿}&access_token={접근 토큰}&service_provider=NAVER
		  
		final String RequestUrl = "https://nid.naver.com/oauth2.0/token";
			
		final List<NameValuePair> postParams = new ArrayList<NameValuePair>();
		postParams.add(new BasicNameValuePair("grant_type", "delete"));
		postParams.add(new BasicNameValuePair("client_id", clientId));
		postParams.add(new BasicNameValuePair("client_secret", clientSecret));
		postParams.add(new BasicNameValuePair("access_token", accessToken));
		postParams.add(new BasicNameValuePair("service_provider", "NAVER"));
		
		
		
		final HttpClient client = HttpClientBuilder.create().build();
		final HttpPost post = new HttpPost(RequestUrl);
		   
		JsonNode returnNode = null;
		    
		try {
			post.setEntity(new UrlEncodedFormEntity(postParams));
			final HttpResponse response = client.execute(post);
		    final int responseCode = response.getStatusLine().getStatusCode();

		    //JSON 형태 반환값 처리
		    ObjectMapper mapper = new ObjectMapper();
		    returnNode = mapper.readTree(response.getEntity().getContent());
		      
		    } catch (UnsupportedEncodingException e) {
		      e.printStackTrace();
		    } catch (ClientProtocolException e) {
		      e.printStackTrace();
		    } catch (IOException e) {
		      e.printStackTrace();
		    } finally {
		        // clear resources
		    }
		    return returnNode;
		}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
