package kr.co.carple.timer;

import java.util.HashMap;
import java.util.Map;
import java.util.TimerTask;

import javax.servlet.http.HttpSession;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.FrameworkServlet;

import kr.co.carple.service.ConsignRequestService;
import kr.co.carple.utils.BaseAppConstants;
import kr.co.carple.utils.WebUtils;


public class StatusUpdateTimerTask extends TimerTask {

	private String identifier;
	private String status;
	private HttpSession session;
		
	//@Autowired
	private ConsignRequestService consignRequestService;
	
	
	
	public StatusUpdateTimerTask(HttpSession session, String identifier,String status) {
		this.identifier = identifier;
		this.status = status;
		this.session = session;
	}
	
	@Override
	public void run() {
		
		/*
		public static final String BID_STATUS_WAIT = "WW";		//입찰대기중
		public static final String BID_STATUS_ING = "II";		//입찰진행중
		public static final String BID_STATUS_DONE = "DD";		//낙찰
		public static final String BID_STATUS_FAIL = "FF";		//유찰
		public static final String BID_STATUS_CANCEL = "CC";		//입찰취소
		public static final String BID_STATUS_RE = "RR";		//재입찰
		public static final String BID_STATUS_RE_FAIL = "RF";		//재입찰 후 유찰
		public static final String BID_STATUS_RE_CANCEL = "RC";		//재입찰 후 입찰취소
		public static final String BID_STATUS_SUCCESS = "SS";		//낙찰 후 완료
		*/
		
		try {
			
			WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext( session.getServletContext(), FrameworkServlet.SERVLET_CONTEXT_PREFIX + "appServlet" );		
			consignRequestService = (ConsignRequestService) context.getBean("consignRequestService");
						
			if(this.status.equals(BaseAppConstants.BID_STATUS_WAIT)) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("corStatus", BaseAppConstants.BID_STATUS_ING);
				map.put("corId", this.identifier);
				consignRequestService.updateCorStatus(map);
			}
				
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		
	}

}
