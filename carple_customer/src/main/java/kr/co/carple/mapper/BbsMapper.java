package kr.co.carple.mapper;

import java.util.List;
import java.util.Map;

import kr.co.carple.vo.BbsVO;

public interface BbsMapper {

	
	public int insertBbs(BbsVO bbsVO) throws Exception;	
	public List<Map<String, Object>> selectBbsList(Map<String, Object> map) throws Exception;
	public Map<String,Object>selectBbs(Map<String,Object>map) throws Exception;
	public void deleteBbs(Map<String, Object> map) throws Exception;
	
	
	
	
	
}
