package kr.co.carple.mapper;

import java.util.List;
import java.util.Map;

import kr.co.carple.vo.ConsignRequestBidVO;

public interface ConsignRequestBidMapper {

	public List<Map<String, Object>> selectConsignRequestBidList(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectConsignRequestBid(Map<String, Object> map) throws Exception;
	public int insertConsignRequestBid(ConsignRequestBidVO consignRequestBidVO) throws Exception;
	public void deleteConsignRequestBid(Map<String, Object> map) throws Exception;
	public void updateConsignRequestBidCrbStatus(Map<String, Object> map) throws Exception;
	public void updateConsignRequestBidCrbStatusForBidSuccess(Map<String, Object> map) throws Exception;
	
}
