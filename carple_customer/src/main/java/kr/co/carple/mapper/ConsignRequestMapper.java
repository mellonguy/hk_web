package kr.co.carple.mapper;

import java.util.List;
import java.util.Map;

import kr.co.carple.vo.ConsignRequestVO;

public interface ConsignRequestMapper {

	
	public List<Map<String, Object>> selectConsignRequestList(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectConsignRequest(Map<String, Object> map) throws Exception;
	public int insertConsignRequest(ConsignRequestVO consignRequestVO) throws Exception;
	public void deleteConsignRequest(Map<String, Object> map) throws Exception;
	public void updateCorStatus(Map<String, Object> map) throws Exception;
	public void updateCorReadFlag(Map<String, Object> map) throws Exception;

	
	
}

