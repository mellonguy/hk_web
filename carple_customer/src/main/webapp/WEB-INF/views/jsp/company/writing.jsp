<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
<body class="black">

	<script type="text/javascript">
 
 
 	$(document).ready(function(){
		
		//getNewsData("${searchWord}");
		
	}); 
 
 	var rgx1 = /\D/g;  // /[^0-9]/g 와 같은 표현
 	var rgx2 = /(\d+)(\d{3})/; 

 	
 	function setComma(inNum){
 	     
 	     var outNum;
 	     outNum = inNum; 
 	     while (rgx2.test(outNum)) {
 	          outNum = outNum.replace(rgx2, '$1' + ',' + '$2');
 	      }
 	     return outNum;

 	}
 	
 	function comma(str) {
 	    str = String(str);
 	    return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
 	}
 	
 	function getNumber(obj){
 		
 	     var num01;
 	     var num02;
 	     num01 = obj.value;
 	     num02 = num01.replace(rgx1,"");
 	      num01 = setComma(num02); 
 	     //num01 = num02;
 	     obj.value =  num01;
 	     
 	}	
 
 	
 	
 function removeComma(str){
	 
		n = parseInt(str.replace(/,/g,""));
		return n;
		
	}

 	
 	
  function setAmount(val){
	  
	 // alert(removeComma($("#trdPrc").html()));
	 
	  var value = removeComma($("#trdPrc").html());
	  $("#priceOrder").val(comma(parseInt(Number(value * (1 + val / 100)))));
  }
 
 
 
 function priceOrderPlus(){
	 
	 if($("#priceOrder").val() != ""){
		 var val = removeComma($("#priceOrder").val());
		 val += 1;
	 	$("#priceOrder").val(comma(val));
	 }
	 
 }
 
 
 function priceOrderMinus(){
	 
	 if($("#priceOrder").val() != ""){
		 var val = removeComma($("#priceOrder").val());
		 val -= 1;
	 	$("#priceOrder").val(comma(val));	 
	 }
	 
 }
 
 
 function deleteTargetPriceAlarm(targetPriceAlarmId,obj){
	 
	 
	 //alert(targetPriceAlarmId);
	 
	  $.ajax({ 
	 		type: 'post' ,
	 		url : "/company/deleteTargetPriceAlarm.do" ,
	 		dataType : 'json' ,
	 		data : {
	 			targetPriceAlarmId : targetPriceAlarmId
	 		},
	 		success : function(data, textStatus, jqXHR)
	 		{
	 			if(data.resultCode == "0000"){
	 				$(obj).parent().remove();
	 			}else if(data.resultCode == "E002"){
	 				//	alert("로그인 되지 않음");
	 			}
	 		} ,
	 		error : function(xhRequest, ErrorText, thrownError) {
	 		}
	 	});  
	 
 }
 
 
 function selectInterestItem(itemCd,marketCd,obj){
 	
	 if(obj == null){
		 obj = $("#forHeart"); 
	 }
	 	 
 	$.ajax({ 
 		type: 'post' ,
 		url : "/interest/selectInterestItem.do" ,
 		dataType : 'json' ,
 		data : {
 			itemCd : itemCd,
 			marketCd : marketCd
 		},
 		success : function(data, textStatus, jqXHR)
 		{
 			if(data.resultCode == "0000"){
 				insertInterestItem(itemCd,marketCd,obj);
 			}else{
 				insertTargetPriceAlarm(itemCd,marketCd,obj);
 			}
 		} ,
 		error : function(xhRequest, ErrorText, thrownError) {
 		}
 	}); 
 	
 	
 }    
     
 function insertInterestItem(itemCd,marketCd,obj){
 	
 	$.ajax({ 
 		type: 'post' ,
 		url : "/interest/insertInterestItem.do" ,
 		dataType : 'json' ,
 		data : {
 			itemSrtCd : itemCd,
 			marketCd : marketCd
 		},
 		success : function(data, textStatus, jqXHR)
 		{
 			if(data.resultCode == "0000"){
 				$(obj).addClass('heart-full');
 				insertTargetPriceAlarm(itemCd,marketCd,obj);
 			}else if(data.resultCode == "E002"){
 					alert("로그인 되지 않음");
 			}
 		} ,
 		error : function(xhRequest, ErrorText, thrownError) {
 		}
 	}); 
 }    
     
 
 function insertTargetPriceAlarm(itemCd,marketCd,obj){
	 
	 	$.ajax({ 
	 		type: 'post' ,
	 		url : "/company/insertTargetPriceAlarm.do" ,
	 		dataType : 'json' ,
	 		data : {
	 			itemSrtCd : itemCd,
	 			marketCd : marketCd,
	 			targetPrice : removeComma($("#priceOrder").val())
	 		},
	 		success : function(data, textStatus, jqXHR)
	 		{
	 			if(data.resultCode == "0000"){
	 				alert("알림 목록에 추가 되었습니다.");
	 				var result = "";
	 				result += '<div class="ticker-item">';
	 				result += '<span>목표가</span>';
	 				result += '<span>'+$("#priceOrder").val()+'원 </span>';
	 				result += '<a onclick="javascript:deleteTargetPriceAlarm(\''+data.resultData.targetPriceAlarmId+'\',this);" class="delete"></a>';
	 				result += '</div>';
	 				$("#ticker-list").append(result);
	 				
	 			}else if(data.resultCode == "E002"){
	 				//	alert("로그인 되지 않음");
	 			}
	 		} ,
	 		error : function(xhRequest, ErrorText, thrownError) {
	 		}
	 	}); 
	 
 }
 
 
 
 function addAlarm(itemCd,marketCd){

	 if($("#priceOrder").val() == ""){
		 alert("지정가가 설정 되지 않았습니다.");
		 return false;
	 }else{
		 //관심종목이 아닌경우 관심종목으로 등록 한다.
		 selectInterestItem(itemCd,marketCd,null);		 
	 }
	 
 }
 
 function insertDiscussion(){
	 
	 
	 var title = $("#title").val();
	 var content = $("#content").val();
	 
	 if(title == ""){
		 alert("제목이 작성 되지 않았습니다.");
		 return false;
	 }
	 
	 if(content == ""){
		 alert("내용이 작성 되지 않았습니다.");
		 return false;
	 }
	 
	 if(confirm("작성 하시겠습니까?")){
		 $.ajax({ 
		 		type: 'post' ,
		 		url : "/company/insertDiscussion.do" ,
		 		dataType : 'json' ,
		 		data : {
		 			title : $("#title").val(),
		 			content : $("#content").val(),
		 			itemSrtCd : "${itemSrtCd}",
		 			marketCd : "${marketCd}"
		 		},
		 		success : function(data, textStatus, jqXHR)
		 		{
		 			if(data.resultCode == "0000"){
		 				alert("작성 되었습니다.");
		 				document.location.href="/company/discussion.do?itemSrtCd=${itemSrtCd}&marketCd=${marketCd}";	 				
		 			}else if(data.resultCode == "E002"){
		 				//	alert("로그인 되지 않음");
		 			}
		 		} ,
		 		error : function(xhRequest, ErrorText, thrownError) {
		 		}
		 	}); 
	 }
	 
	 
	 	 
	 
	 
	 
	 
	 
 }
 
   
 </script>

	<div class="content-container writing pb50">
		<header class="clearfix nb">
			<div class="search-icon">
				<a style="cursor: pointer;" onclick="javascript:history.go(-1);"><img
					src="/img/back-icon.png" alt=""></a>
			</div>
			<div class="page-title txt-medium white">글쓰기</div>
			<div class="menu-bar pull-right">
				<a href="/adviser/adviser.do"><img src="/img/home-pink-icon.png"
					alt=""></a>
			</div>
		</header>
		<div class="writingarea">
			<div class="title">
				<input id="title" name="title" type="text" placeholder="제목을 입력하세요">
			</div>
			<div class="content">
				<textarea name="content" id="content" cols="30" rows="10"
					placeholder="내용을 입력하세요(욕설, 비방 금지)"></textarea>
			</div>
		</div>
		<div class="fixed-bottomarea">
			<a onclick="javascript:insertDiscussion();">작성 완료</a>
		</div>
	</div>

	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
	<script src="/js/vendor/bootstrap.min.js"></script>
	<!-- veiwport for countnumber -->
	<script src="/js/jquery.viewportchecker.js"></script>
	<script src="/js/main.js"></script>
</body>
</html>
