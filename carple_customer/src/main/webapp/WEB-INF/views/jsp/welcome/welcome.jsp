<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko">
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>

<body
	style="width: 100%; height: 100%; background-color: #231f20; background-size: cover; background-image: url(/img/ntnlconquest.png); background-repeat: no-repeat;">

	<p
		style="position: absolute; top: 67%; left: 31%; color: #fff; font-size: 100px;">Coming
		Soon!!</p>

	<div id="footer" role="contentinfo"
		style="border: none; position: fixed; bottom: 0; width: 100%; background-color: #efefef;">
		<div class="corp_area" data-clk-prefix="plc" style="margin-left: 30%;">
			<address class="addr" style="font-weight: 200; font-size: 12px;">
				상호명:주식회사 내셔널컨퀘스트<br>서울시 강남구 헌릉로 569길 21-30, 4층 407호(세곡동, 드림하이
				오피스텔) <br>대표자:임경숙 Tel:1577-5268 Fax:02-426-3508
				사업자등록번호:551-81-01923 <a target="_blank" data-clk="nhn"
					style="color: #000;">ⓒ National Conquest.</a>
			</address>
			<ul class="list_corp">
				<li class="corp_item"><a href="/login/terms.do"
					data-clk="privacy"><strong style="color: #000;">이용약관 및
							개인정보처리방침</strong></a></li>
			</ul>
		</div>

	</div>

	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
	<script src="/js/vendor/bootstrap.min.js"></script>
	<!-- veiwport for countnumber -->
	<script src="/js/jquery.viewportchecker.js"></script>
	<script src="/js/alert.js"></script>
	<script src="/js/main.js"></script>
</body>
</html>
