<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko">
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>

<body class="black">

	<script type="text/javascript">  

$(document).ready(function(){
	
	//getNewsData("${searchWord}");
	
});


/* function getNewsData(keyword){
	
	$.ajax({ 
		type: 'post' ,
		url : "http://newssearch.naver.com/search.naver?where=rss&query="+keyword ,
		dataType : 'xml' ,
		data : {
			
		},
		success : function(data, textStatus, jqXHR)
		{
			alert(data);
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	}); 
	
} */


function goSearch(){

	if($("#searchWord").val() == ""){
		alert("키워드가 입력되지 않았습니다.");
		return false;
	}else{
		document.location.href = "/search/search-result.do?searchWord="+encodeURIComponent($("#searchWord").val());
	}
	
}
  

function replaceAll(str, searchStr, replaceStr) {
	  return str.split(searchStr).join(replaceStr);
	}

function goNewsPage(uri){
	
	//var replaceUrlStr = replaceAll(url, "?", "^");
	//replaceUrlStr = replaceAll(replaceUrlStr, "=", "+");
	
	var replaceUrlStr = encodeURIComponent(uri)
	document.location.href = "/news/newsPage.do?newsUri="+replaceUrlStr;
}    
    
    
    
function selectInterestItem(itemCd,marketCd,obj){
	
	$.ajax({ 
		type: 'post' ,
		url : "/interest/selectInterestItem.do" ,
		dataType : 'json' ,
		data : {
			itemCd : itemCd,
			marketCd : marketCd
		},
		success : function(data, textStatus, jqXHR)
		{
			if(data.resultCode == "0000"){
				insertInterestItem(itemCd,marketCd,obj);
			}else{
				deleteInterestItem(itemCd,marketCd,obj);
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	}); 
	
	
}    
    
function insertInterestItem(itemCd,marketCd,obj){
	
	$.ajax({ 
		type: 'post' ,
		url : "/interest/insertInterestItem.do" ,
		dataType : 'json' ,
		data : {
			itemSrtCd : itemCd,
			marketCd : marketCd
		},
		success : function(data, textStatus, jqXHR)
		{
			if(data.resultCode == "0000"){
				$(obj).addClass('heart-full');
			}else if(data.resultCode == "E002"){
					alert("로그인 되지 않음");
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	}); 
}    
    
  

function deleteInterestItem(itemCd,marketCd,obj){
	
	$.ajax({ 
		type: 'post' ,
		url : "/interest/deleteInterestItem.do" ,
		dataType : 'json' ,
		data : {
			itemSrtCd : itemCd,
			marketCd : marketCd
		},
		success : function(data, textStatus, jqXHR)
		{
			if(data.resultCode == "0000"){
				$(obj).removeClass('heart-full');
			}else if(data.resultCode == "E002"){
					alert("로그인 되지 않음");
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	}); 
}    


function goCompanyPage(itemSrtCd,marketCd){
	
	document.location.href = "/company/company.do?itemSrtCd="+itemSrtCd+"&marketCd="+marketCd;
	
}

function goCompanyNewsPage(itemSrtCd,marketCd){
	
	document.location.href = "/company/company-news.do?itemSrtCd="+itemSrtCd+"&marketCd="+marketCd;
	
}

function goSearch(){
	
	$("#searchWord").val();
	document.location.href = "/search/search.do?searchWord="+$("#searchWord").val();
	
}


function goAdviserPage(adviserCd){
	
	//document.location.href = "/adviser/adviserRecommendItem.do?adviserCd="+adviserCd;
	document.location.href = "/adviser/adviserDetail.do?adviserCd="+adviserCd;
	
}


function goThemePage(themeCd){
	
	document.location.href = "/research/themeDetail.do?themeCd="+themeCd;
	
}

    
</script>


	<div
		class="content-container searchresult black emptyresult searchresult2">
		<header
			style="background-color: #1e212a; position: fixed; top: 0px; z-index: 10000;">
			<div class="search-icon">
				<!-- <a href="search.html"><img src="/img/back-icon.png" alt=""></a> -->
				<a href="/search/search.do"><img src="/img/back-icon.png" alt=""></a>
			</div>
			<div class="search-container">
				<a onclick="javascript:goSearch();" class="search-input-icon"> <img
					src="/img/search-input-icon.png" alt="">
				</a> <input type="text" name="searchWord" id="searchWord"
					onclick="javascript:goSearch();" value="${searchWord}">
			</div>
			<div class="cancel">
				<a href="/adviser/adviser.do">취소</a>
			</div>
		</header>
		<div class="moveTop">
			<a href="#"> <img src="/img/arrow-top-icon.png" alt=""> TOP
			</a>
		</div>
		<div class="viewing-option" style="margin-top: 55px;">
			<ul>
				<li><a href="/search/search-result.do?searchWord=${searchWord}">통합</a></li>
				<li class="active"><a
					href="/search/search-result-robo.do?searchWord=${searchWord}">로보어드바이저</a></li>
				<li><a
					href="/search/search-result-item.do?searchWord=${searchWord}">종목</a></li>
				<li><a
					href="/search/search-result-theme.do?searchWord=${searchWord}">테마</a></li>
				<li><a
					href="/search/search-result-news.do?searchWord=${searchWord}">뉴스</a></li>
			</ul>
		</div>
		<div class="recent-searches suggestion-box">
			<div class="suggestion gray">
				<div class="head lbl-head blue">
					<span class="title">로보어드바이저</span> <span class="count">${adviserListCount}
					</span>
				</div>
				<div class="suggestions">
					<c:forEach var="data" items="${adviserList}" varStatus="status">
						<c:if test="${status.index < 5}">
							<a onclick="javascript:goAdviserPage('${data.adviser_cd}');"
								class="item"> <span class="title">${data.adviser_name}</span>
								<div class="sub">${data.adviser_comment}</div>
							</a>
						</c:if>
					</c:forEach>
					<!-- <a  href="#" class="item">
                            <span class="title">주락펴락 시그널-S 3호</span>
                            <div class="sub">5G 섹터 관련 6종목 발굴</div>
                        </a>
                        <a  href="#" class="item">
                            <span class="title">주락펴락 알파-B 1호</span>
                            <div class="sub">대형가치주 관련 3종목 발굴</div>
                        </a> -->
				</div>
			</div>









		</div>
	</div>

	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
	<script src="/js/vendor/bootstrap.min.js"></script>
	<!-- veiwport for countnumber -->
	<script src="/js/jquery.viewportchecker.js"></script>
	<script src="/js/main.js"></script>
</body>
</html>
