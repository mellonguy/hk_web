<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko">
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>

<body class="black">


	<script type="text/javascript">  

$(document).ready(function(){
	
	//getNewsData("${searchWord}");
	getInterestItem();
	
});


/* function getNewsData(keyword){
	
	$.ajax({ 
		type: 'post' ,
		url : "http://newssearch.naver.com/search.naver?where=rss&query="+keyword ,
		dataType : 'xml' ,
		data : {
			
		},
		success : function(data, textStatus, jqXHR)
		{
			alert(data);
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	}); 
	
} */


function goSearch(){

/* 	if($("#searchWord").val() == ""){
		alert("키워드가 입력되지 않았습니다.");
		return false;
	}else{
		document.location.href = "/search/search-result.do?searchWord="+encodeURIComponent($("#searchWord").val());
	} */
	
	document.location.href = "/search/search.do";
	
}
  

function replaceAll(str, searchStr, replaceStr) {
	  return str.split(searchStr).join(replaceStr);
	}

function goNewsPage(uri){
	
	//var replaceUrlStr = replaceAll(url, "?", "^");
	//replaceUrlStr = replaceAll(replaceUrlStr, "=", "+");
	
	var replaceUrlStr = encodeURIComponent(uri)
	document.location.href = "/news/newsPage.do?newsUri="+replaceUrlStr;
}    
    
    
    
function selectInterestItem(itemCd,marketCd,obj,event){
	
	
	
	$.ajax({ 
		type: 'post' ,
		url : "/interest/selectInterestItem.do" ,
		dataType : 'json' ,
		data : {
			itemCd : itemCd,
			marketCd : marketCd
		},
		success : function(data, textStatus, jqXHR)
		{
			if(data.resultCode == "0000"){
				insertInterestItem(itemCd,marketCd,obj);
			}else{
				deleteInterestItem(itemCd,marketCd,obj);
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	}); 
	
	event.stopPropagation();
	
}    
    
function insertInterestItem(itemCd,marketCd,obj){
	
	$.ajax({ 
		type: 'post' ,
		url : "/interest/insertInterestItem.do" ,
		dataType : 'json' ,
		data : {
			itemSrtCd : itemCd,
			marketCd : marketCd
		},
		success : function(data, textStatus, jqXHR)
		{
			if(data.resultCode == "0000"){
				$(obj).addClass('heart-full');
			}else if(data.resultCode == "E002"){
					alert("로그인 되지 않음");
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	}); 
}    
    
  

function deleteInterestItem(itemCd,marketCd,obj){
	
	$.ajax({ 
		type: 'post' ,
		url : "/interest/deleteInterestItem.do" ,
		dataType : 'json' ,
		data : {
			itemSrtCd : itemCd,
			marketCd : marketCd
		},
		success : function(data, textStatus, jqXHR)
		{
			if(data.resultCode == "0000"){
				$(obj).removeClass('heart-full');
			}else if(data.resultCode == "E002"){
					alert("로그인 되지 않음");
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	}); 
}    


function goCompanyPage(itemSrtCd,marketCd){
	document.location.href = "/company/company.do?itemSrtCd="+itemSrtCd+"&marketCd="+marketCd;
	
}

function goCompanyNewsPage(itemSrtCd,marketCd){
	
	document.location.href = "/company/company-news.do?itemSrtCd="+itemSrtCd+"&marketCd="+marketCd;
	
}


var interestItemList = new Array();
function getInterestItem(){
	
	interestItemList.length = 0;
	$.ajax({ 
		type: 'post' ,
		url : "/interest/getInterestItem.do" ,
		dataType : 'json' ,
		data : {
			
		},
		success : function(data, textStatus, jqXHR)
		{
			interestItemList = data.resultData;
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	}); 
	
}


function sortOrder(index){
	
	
	var result = "";
	var list =  new Array();
	
	if(index == "A"){
		list = interestItemList.sort(function(a,b){
			return a.user_order < b.user_order ? -1 : 1;
		});
	
	}else if(index == "B"){
		list = interestItemList.sort(function(a,b){
			return a.created_at > b.created_at ? -1 : 1;
		});
	
	}else if(index == "C"){
		list = interestItemList.sort(function(a,b){
			return a.cmpprevddPer > b.cmpprevddPer ? -1 : 1;
		});
	
	}else if(index == "D"){
		list = interestItemList.sort(function(a,b){
			return a.cmpprevddPer < b.cmpprevddPer ? -1 : 1;
		});
	
	}
	
	$("#interestItemList").html("");
	 for(var i = 0; i < list.length; i++){
		 
		 result += '<div class="interest-item"  onclick="javascript:goCompanyPage(\''+list[i].item_srt_cd+'\',\''+list[i].market_cd+'\');">';
		 result += '<div class="interest-info">';
		 result += '<span class="name">'+list[i].item_name+'</span>';
		 result += '</div>';
		 
		 if(list[i].cmpprevddPrc >= 0){
			 result += '<div class="value red">'+list[i].trdPrc+'</div>';
			 result += '<div class="percent-value red">';
			 result += '<span class="percent">'+list[i].cmpprevddPer+'%</span>';
			 result += '<span class="value up">'+list[i].cmpprevddPrc+'</span>';
			 result += '</div>';	 
		 }
		 
		 if(list[i].cmpprevddPrc < 0){
			 result += '<div class="value blue">'+list[i].trdPrc+'</div>';
			 result += '<div class="percent-value blue">';
			 result += '<span class="percent">'+list[i].cmpprevddPer+'%</span>';
			 result += '<span class="value down">'+list[i].cmpprevddPrc+'</span>';
			 result += '</div>';	 
		 }
		 
		 result += '<div class="bell-holder">';
		 if(list[i].alarm_cnt != '0'){
			 result += '<a href="#" class="bell bell-full"></a>';	 
		 }
		 if(list[i].alarm_cnt == '0'){
			 result += '<a href="#" class="bell"></a>';	 
		 }
		 result += '</div>';
		 result += '</div>';
		//alert(list[i].item_name + list[i].user_order);	
	}
	 
	 $("#interestItemList").html(result);
	 
	
	
	
	
	
	
	
	
	
	
	
	
	
}

    
</script>





	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

	<div class="content-container interest-page black productsinuse ">




		<header
			style="background-color: #1e212a; position: fixed; top: 0px; z-index: 100;">
			<!-- <div id="fixedMenu" style="background-color:#1e212a; position:fixed; top:0px; z-index:10000;"> -->
			<div class="menu-bar">
				<a href="/menu/menu.do"><img src="/img/bar-icon.png" alt=""></a>
			</div>
			<div class="notif-icon">
				<a href="#"><img src="/img/bell2-icon.png" alt=""></a>
			</div>
			<div class="search-container">
				<a href="#" class="search-input-icon"> <img
					src="/img/search-input-icon.png" alt="">
				</a> <input type="text" name="" id="" onclick="javascript:goSearch();"
					placeholder="종목 혹은 테마를 검색해주세요">
			</div>
			<!-- </div> -->
		</header>
		<div class="moveTop">
			<a href="#"> <img src="/img/arrow-top-icon.png" alt=""> TOP
			</a>
		</div>
		<div class="view-option" style="margin-top: 55px;">
			<ul>
				<li class="option interest active "><a
					href="/interest/interest.do">시세알림</a></li>
				<li class="option advisor"><a href="/adviser/adviser.do">로보어드바이저</a>
				</li>
				<li class="option  research "><a href="/research/theme.do">테마</a>
				</li>
			</ul>
		</div>
		<div class="sort-edit">
			<a href="#" class="btn-sort">정렬</a> <a>|</a> <a
				href="/interest/edit.do" class="btn-edit">편집</a>
		</div>
		<div class="content-box">
			<span class="title">어드바이저 추천 종목</span>
		</div>
		<div class="recommended-box">
			<c:forEach var="data" items="${purchaseRecommendItemList}"
				varStatus="status">
				<div class="box-item"
					onclick="javascript:goCompanyPage('${data.item_srt_cd}','${data.market_cd}');">
					<div class="item-top-info clearfix">
						<div class="details-area">
							<span class="name">${data.item_name}</span> <span class="subinfo">추천가
								${data.recommend_price}</span>
						</div>
						<c:if test="${data.cmpprevddPrc >= 0}">
							<div class="value-area red">${data.trdPrc}</div>
							<div class="percent-area red">
								<span class="percent">${data.cmpprevddPer}%</span> <span
									class="eval up">${data.cmpprevddPrc}</span>
							</div>
						</c:if>
						<c:if test="${data.cmpprevddPrc < 0}">
							<div class="value-area blue">${data.trdPrc}</div>
							<div class="percent-area blue">
								<span class="percent">${data.cmpprevddPer}%</span> <span
									class="eval down">${data.cmpprevddPrc}</span>
							</div>
						</c:if>
						<div class="heart-box">
							<c:if test="${data.interest_item_cd != null}">
								<a
									onclick="javascript:selectInterestItem('${data.item_srt_cd}','${data.market_cd}',this,event)"
									class="heart heart-full"></a>
							</c:if>
							<c:if test="${data.interest_item_cd == null}">
								<a
									onclick="javascript:selectInterestItem('${data.item_srt_cd}','${data.market_cd}',this,event)"
									class="heart"></a>
							</c:if>
						</div>
					</div>
					<div class="line-holder clearfix">
						<div class="blue color-box" data-value="">
							<span></span>
						</div>
						<div class="color-box red active" data-value="200">
							<span style="width: 200px;"></span>
						</div>
					</div>
					<div class="offense-goal-box clearfix">
						<div class="box pull-left">
							<span class="lbl">손절가</span> <span class="points">${data.sell_price}원</span>
						</div>
						<div class="box pull-right text-right">
							<span class="lbl">최초목표가 </span> <span class="points">${data.target_price}원</span>
						</div>
					</div>
				</div>
			</c:forEach>

			<!-- <div class="box-item">
                    <div class="item-top-info clearfix">
                        <div class="details-area">
                            <span class="name">티플랙스</span>
                            <span class="subinfo">추천가 1,930</span>
                        </div>
                        <div class="value-area red">
                            300,000
                        </div>
                        <div class="percent-area red">
                            <span class="percent">+300.85%</span>
                            <span class="eval up">200,750</span>
                        </div>
                        <div class="heart-box">
                            <a href="#" class="heart"></a>
                        </div>
                    </div>
                    <div class="line-holder clearfix">
                        <div class="blue color-box" data-value="">
                            <span></span>
                        </div>
                        <div class="color-box red active" data-value="520">
                            <span></span>
                        </div>
                    </div>
                    <div class="offense-goal-box clearfix">
                        <div class="box pull-left">
                            <span class="lbl">손절가</span>
                            <span class="points">1,840원</span>
                        </div>
                        <div class="box pull-right text-right">
                            <span class="lbl">최초목표가 </span>
                            <span class="points">2,130원</span>
                        </div>
                    </div>
                </div> -->
			<!-- <div class="box-item">
                    <div class="item-top-info clearfix">
                        <div class="details-area">
                            <span class="name">티플랙스</span>
                            <span class="subinfo">추천가 1,930</span>
                        </div>
                        <div class="value-area red">
                            300,000
                        </div>
                        <div class="percent-area red">
                            <span class="percent">+300.85%</span>
                            <span class="eval up">200,750</span>
                        </div>
                        <div class="heart-box">
                            <a href="#" class="heart"></a>
                        </div>
                    </div>
                    <div class="line-holder clearfix">
                        <div class="blue color-box" data-value="">
                            <span></span>
                        </div>
                        <div class="color-box red active" data-value="260">
                            <span></span>
                        </div>
                    </div>
                    <div class="offense-goal-box clearfix">
                        <div class="box pull-left">
                            <span class="lbl">손절가</span>
                            <span class="points">1,840원</span>
                        </div>
                        <div class="box pull-right text-right">
                            <span class="lbl">최초목표가 </span>
                            <span class="points">2,130원</span>
                        </div>
                    </div>
                </div> -->
		</div>
		<div class="content-box interes-list-container">
			<span class="title">나의 관심 종목</span>
			<div class="interest-list" id="interestItemList">

				<c:forEach var="data" items="${interestItemList}" varStatus="status">
					<div class="interest-item"
						onclick="javascript:goCompanyPage('${data.item_srt_cd}','${data.market_cd}');">
						<div class="interest-info">
							<span class="name">${data.item_name}</span>
						</div>
						<c:if test="${data.cmpprevddPrc >= 0}">
							<div class="value red">${data.trdPrc}</div>
							<div class="percent-value red">
								<span class="percent">${data.cmpprevddPer}%</span> <span
									class="value up">${data.cmpprevddPrc}</span>
							</div>
						</c:if>
						<c:if test="${data.cmpprevddPrc < 0}">
							<div class="value blue">${data.trdPrc}</div>
							<div class="percent-value blue">
								<span class="percent">${data.cmpprevddPer}%</span> <span
									class="value down">${data.cmpprevddPrc}</span>
							</div>
						</c:if>
						<div class="bell-holder">
							<c:if test="${data.alarm_cnt != '0'}">
								<a href="#" class="bell bell-full"></a>
							</c:if>
							<c:if test="${data.alarm_cnt == '0'}">
								<a href="#" class="bell"></a>
							</c:if>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
		<!-- <div class="forads">
                <img src="img/ads-img.png" alt="">
            </div> -->
		<div class="for-sort">
			<div class="sort-option">
				<ul>
					<!-- <li><a onclick="javascript:sortOrder('A');" class="checked">사용자지정</a></li> -->
					<li><a onclick="javascript:sortOrder('A');">사용자지정</a></li>
					<li><a onclick="javascript:sortOrder('B');">최근조회</a></li>
					<li><a onclick="javascript:sortOrder('C');">상승률</a></li>
					<li><a onclick="javascript:sortOrder('D');">하락률</a></li>
					<!-- <li><a href="#">수익률</a></li> -->
				</ul>
			</div>
		</div>
	</div>


	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
	<script src="/js/vendor/bootstrap.min.js"></script>
	<!-- veiwport for countnumber -->
	<script src="/js/main.js"></script>
</body>
</html>
