<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title></title>
<meta name="description" content="">
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
<!-- <meta name="viewport" content="width=720px, user-scalable=no"> -->
<link rel="stylesheet" href="/css/bootstrap.min.css">
<link rel="stylesheet" href="/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="/css/owl.carousel.min.css">
<link rel="stylesheet" href="/css/owl.theme.default.min.css">
<link rel="stylesheet" href="/css/notosanskr.css">
<link rel="stylesheet" href="/css/main.css">
<script src="/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/js/vendor/jquery.ui.touch.js"></script>

</head>


<body class="black">


	<script type="text/javascript">  

$(document).ready(function(){
	
	//getNewsData("${searchWord}");

	$( function() {
	    $( "#sortable" ).sortable({
	    
	    	item : '.withbar',
	    	stop : function(event,ui){
	    		sortInterestItem();
	    	}
	    	
	    });
	    $( "#sortable" ).disableSelection();
	  } );
	
	
	
});


var interestItemList = new Array();
function sortInterestItem(){
	
	interestItemList.length = 0;
	
	$("#sortable").find("div").each(function(index,element){
		var interestItemInfo= new Object();
		interestItemInfo.user_order = index+1; 
		interestItemInfo.unique_id = $(this).attr("id"); 
		interestItemList.push(interestItemInfo);
	});
	
	//JSON.stringify({interestItemList : interestItemList})
	
	$.ajax({ 
		type: 'post' ,
		url : "/interest/updateInterestItemUserOrder.do" ,
		dataType : 'json' ,
		data : {
			interestItemList : JSON.stringify({interestItemList : interestItemList})
		},
		success : function(data, textStatus, jqXHR)
		{
			
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	}); 
	
	
	
	
	
}








/* function getNewsData(keyword){
	
	$.ajax({ 
		type: 'post' ,
		url : "http://newssearch.naver.com/search.naver?where=rss&query="+keyword ,
		dataType : 'xml' ,
		data : {
			
		},
		success : function(data, textStatus, jqXHR)
		{
			alert(data);
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	}); 
	
} */


function goSearch(){

/* 	if($("#searchWord").val() == ""){
		alert("키워드가 입력되지 않았습니다.");
		return false;
	}else{
		document.location.href = "/search/search-result.do?searchWord="+encodeURIComponent($("#searchWord").val());
	} */
	
	document.location.href = "/search/search.do";
	
}
  

function replaceAll(str, searchStr, replaceStr) {
	  return str.split(searchStr).join(replaceStr);
	}

function goNewsPage(uri){
	
	//var replaceUrlStr = replaceAll(url, "?", "^");
	//replaceUrlStr = replaceAll(replaceUrlStr, "=", "+");
	
	var replaceUrlStr = encodeURIComponent(uri)
	document.location.href = "/news/newsPage.do?newsUri="+replaceUrlStr;
}    
    
    
    
function selectInterestItem(itemCd,marketCd,obj,event){
	
	
	
	$.ajax({ 
		type: 'post' ,
		url : "/interest/selectInterestItem.do" ,
		dataType : 'json' ,
		data : {
			itemCd : itemCd,
			marketCd : marketCd
		},
		success : function(data, textStatus, jqXHR)
		{
			if(data.resultCode == "0000"){
				insertInterestItem(itemCd,marketCd,obj);
			}else{
				deleteInterestItem(itemCd,marketCd,obj);
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	}); 
	
	event.stopPropagation();
	
}    
    
function insertInterestItem(itemCd,marketCd,obj){
	
	$.ajax({ 
		type: 'post' ,
		url : "/interest/insertInterestItem.do" ,
		dataType : 'json' ,
		data : {
			itemSrtCd : itemCd,
			marketCd : marketCd
		},
		success : function(data, textStatus, jqXHR)
		{
			if(data.resultCode == "0000"){
				$(obj).addClass('heart-full');
			}else if(data.resultCode == "E002"){
					alert("로그인 되지 않음");
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	}); 
}    
    
  

function deleteInterestItem(itemCd,marketCd,obj){
	
	$.ajax({ 
		type: 'post' ,
		url : "/interest/deleteInterestItem.do" ,
		dataType : 'json' ,
		data : {
			itemSrtCd : itemCd,
			marketCd : marketCd
		},
		success : function(data, textStatus, jqXHR)
		{
			if(data.resultCode == "0000"){
				$(obj).removeClass('heart-full');
			}else if(data.resultCode == "E002"){
					alert("로그인 되지 않음");
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	}); 
}    


function goCompanyPage(itemSrtCd,marketCd){
	document.location.href = "/company/company.do?itemSrtCd="+itemSrtCd+"&marketCd="+marketCd;
	
}

function goCompanyNewsPage(itemSrtCd,marketCd){
	
	document.location.href = "/company/company-news.do?itemSrtCd="+itemSrtCd+"&marketCd="+marketCd;
	
}


    
</script>





	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

	<div class="content-container black searchpage">
		<header>
			<div class="search-icon">
				<a href="search.html"><img src="/img/search-icon.png" alt=""></a>
			</div>
			<div class="search-container">
				<input type="text" name="" id="" placeholder="종목명/코드 또는 키워드 입력">
			</div>
			<div class="cancel">
				<a href="/interest/interest.do">취소</a>
			</div>
		</header>

		<div class="recent-searches">
			<div class="head foricon">
				<span>관심종목 편집</span>
			</div>

			<div class="searches" id="sortable">
				<c:forEach var="data" items="${interestItemList}" varStatus="status">
					<div class="search-box foricon" id="${data.interest_item_cd}">
						<a class="search withbar">${data.item_name}</a> <a
							onclick="javascript:selectInterestItem('${data.item_srt_cd}','${data.market_cd}',this,event)"
							class="icon-box heart heart-full"></a>
						<!-- <a href="#" class="icon-box heart  heart-full"></a> -->
					</div>
				</c:forEach>

				<!-- <div class="search-box foricon">
                        <a href="#" class="search withbar">삼성전자</span>
                        <a href="#" class="icon-box heart"></a>
                    </div>
                    <div class="search-box foricon">
                        <a href="#" class="search withbar">LG전자</span>
                        <a href="#" class="icon-box heart heart-full"></a>
                    </div> -->

			</div>
		</div>

	</div>

	<script src="/js/vendor/bootstrap.min.js"></script>
	<!-- veiwport for countnumber -->
	<script src="/js/main.js"></script>
</body>
</html>
