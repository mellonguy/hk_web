<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="java.util.*,java.io.*,java.text.*, java.net.*"%>
<%@ page import="kr.co.danal.jsinbi.HttpClient"%>
<%@ include file="inc/function.jsp"%>
<%
	response.setHeader( "Pragma","No-cache" );

	/********************************************************************************
	*
	* 다날 본인인증
	*
	* - 인증 완료 페이지
	*
	* 인증 시스템 연동에 대한 문의사항이 있으시면 서비스개발팀으로 연락 주십시오.
	* DANAL Commerce Division Technique supporting Team
	* EMail : tech@danal.co.kr
	*
	********************************************************************************/
	
	/********************************************************************************
	 *
	 * XSS 취약점 방지를 위해 
	 * 모든 페이지에서 파라미터 값에 대해 검증하는 로직을 추가할 것을 권고 드립니다.
	 * XSS 취약점이 존재할 경우 웹페이지를 열람하는 접속자의 권한으로 부적절한 스크립트가 수행될 수 있습니다.
	 * 보안 대책
	 *  - html tag를 허용하지 않아야 합니다.(html 태그 허용시 white list를 선정하여 해당 태그만 허용)
	 *  - <, >, &, " 등의 문자를 replace등의 문자 변환함수를 사용하여 치환해야 합니다.
	 * 
	 ********************************************************************************/
       
	String BgColor	= (String)request.getParameter("BgColor");

	/*
	 * Get BgColor
	 */
	BgColor = GetBgColor( BgColor );
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<meta http-equiv="Content-Language" content="ko" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, target-densitydpi=medium-dpi" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>

	<div class="wrap color<%=BgColor%>">
		<!--각COLOR class="color00" ~ class="color10"-->
		<ul class="header">
			<li class="title">휴대폰 본인확인서비스</li>
			<li class="btn_close" style="display: none">close</li>
		</ul>
		<ul class="box_style03">
			<li>본인인증이 정상 처리되었습니다.</li>
		</ul>
		<div class="function">
			<div class="table">
				<span class="row"> <span class="cell"> <!--WebView 닫으며 App으로 데이터 전송(자세한 사항은 매뉴얼 참조)-->
						<!--<a href="Javascript:window.Android.Result('xxxxx');">...</a>-->
						<a
						href="/member/member-register.do?&memPhone=${PHONE}&memUserName=${NAME}"><button
								class="color01">확인</button></a>
				</span>
				</span>
			</div>
		</div>
		<p class="customercenter">다날고객센터: 1566-3355</p>
	</div>

</body>
</html>
