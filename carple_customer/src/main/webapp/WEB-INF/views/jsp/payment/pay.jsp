<%--
	/*
	 * =============================================
	 * Filename	:	pay.jsp
	 * Function	:	결제요청 페이지
	 * Author	:	All contents Copyright DuzonPay all rights reserved
	 * =============================================
	 */
--%>
<%@ page contentType="text/html;charset=euc-kr" pageEncoding="euc-kr"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="euc-kr" />
<title>더존페이 전자결제 샘플 페이지(DuzonPay)</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=yes" />
<!-- 
	* 1. duzonpaypay.js를 include 합니다. 
	* 주의 : 브라우저의 호환성 및 script의 올바른 동작을 위해 <head> tag 상단에 추가하시기 바랍니다.
-->
<script type="text/javascript"
	src="https://pay.duzonpay.com:9143/Script/duzonpay.js"></script>
<script type="text/javascript">    	
	/*
	 * 3. 결제를 시작하기 위해서 호출하는 함수
	 * 더존페이 결제호출!
	 */
	function doduzonpayCheck(){
		
		// 결제호출전에 필요한 변수세팅 등을 진행해주세요.
						
		// 결제호출
		doduzonpay(document.getElementById('duzonpay'));
	}

	/*
	 * 5. 더존페이결제종료후 자동으로 호출되는 함수 getduzonpay_Result()
	 * 실제 DB처리는 Noti_URL 을 통한 서버통신으로 우선 전달됩니다.
	 * 결제 후 화면이동에 대해서 정의하시면 됩니다.
	 */
	function getduzonpay_Result() {
					
		var BKW_RESULTCD = document.getElementById('BKW_RESULTCD').value;
		var BKW_RESULTMSG = document.getElementById('BKW_RESULTMSG').value;
		
		// 함수안에 결제성공/실패 시 필요한 동작들에 대해서 선언해주시면 됩니다.
		// 함수안의 코드에 대해서는 샘플과 다르게 하셔도 무관합니다.
		alert(1);
		// 1) 거래성공의 경우		
		if (BKW_RESULTCD == '0000') {
			// 거래성공 경우, DB처리 및 결제성공결과 안내할 수 있는 페이지로 이동하시면 됩니다.
			// 타겟설정필수!
			document.duzonpay.target = "_self";
			document.duzonpay.action = '/payment/result.do';
		  document.duzonpay.submit();
		} else {
		  // 2) 거래실패 경우 상점에서 원하는 형태로 구현하시면 됩니다.
		  // 2-1) 실패에 대해서도 별도 처리가 필요한 경우
		  
		  // 2-2) 별도 처리없이 화면 refresh.
		  alert("결제가 실패하였습니다. 사유:"+BKW_RESULTMSG);
		  window.location.reload();			  
		  
		  // 결제실패및 취소때 결제창이 안닫히는 경우(부모창 reload와 충돌하는 경우), 아래코드 추가
		  if(bp_popwin) {
		  	bp_popwin.close();
		  }
	 }
}
</script>
</head>
<body>
	<table style="width: 470px; border: 1px solid #dfdfdf;">
		<tr>
			<td>
				<!--
				* 2. 결제를 진행하기 위한 변수 값을 duzonpay form 채워줍니다.
				* 아래는 결제를 위해서 필요한 input 태그입니다. 데이터를 적절히 가져와 내용을 채워주세요
				* 자세한 내용은 [더존페이 결제연동매뉴얼] 을 참고하세요.
			--> <!-- 결제정보 입력 form : duzonpay form -->
				<form name="duzonpay" id="duzonpay" accept-charset="euc-kr"
					method="post">
					<!-- accept-charset 은 더존페이서버와의 통신에서 사용됩니다. 변경하지 마세요. -->
					<!--
				  ===========================
			  	* 공통변수 : 필수정보
			  	===========================
				-->
					<input type="hidden" name="site_cd" value="${site_cd}" />
					<!-- 사이트코드(필수) : 더존페이로 부터 받은 사이트코드를 입력하세요 -->
					<input type="hidden" name="pg_type" value="${pg_type}" />
					<!-- 결제모듈 타입(필수) : 변경하지마세요 -->
					<input type="hidden" name="charset" value="${charset}" />
					<!-- 페이지charset(필수) euc-kr,utf-8 등 -->
					<input type="hidden" name="Result_URL" value="${Result_URL}" />
					<!-- 결제모듈(pop_Result)을 설치한 절대경로. URL을 제외하고 입력하세요. (필수)-->

					<!--
					* 2012년 8월 18일 정자상거래법 개정 관련 설정 부분	
					* 자세한 설정방법은 결제연동 매뉴얼 참고
				-->
					<input type="hidden" name="goodperiod" value="" />
					<!-- [소액결제에 한해서 필수항목] 상품제공기간 -->


					<table style="width: 100%;">
						<!--
			      	* 결제유형(Pay_Type) : 필수
							* 결제종류에 해당되는 값을 넘겨주세요
							* 신용카드: CARD, 계좌이체: ACNT, 가상계좌: VCNT, 휴대폰소액결제: MCASH,  폰빌전화결제: PBILL
							* 더존페이에 신청된 결제수단으로만 결제가 가능합니다.
						-->
						<tr>
							<td colspan="2" align="center">결제 필수정보</td>
						</tr>
						<tr>
							<td>결제유형(Pay_Type)</td>
							<td><select name="Pay_Type">
									<option value="CARD" selected="selected">신용카드</option>
									<option value="VCNT">가상계좌</option>
									<!-- <option value="ACNT">계좌이체</option>
										<option value="MCASH">휴대폰</option>
										<option value="PBILL">폰빌</option> -->
							</select></td>
						</tr>
						<!--상점주문번호(ordr_idxx) : 필수 -->
						<tr>
							<td><input type="hidden" name="ordr_idxx"
								value="${ordr_idxx}" /></td>
						</tr>
						<!-- 상품명(good_name) : 필수 -->
						<tr>
							<td>상품명(good_name)</td>
							<td><input type="text" readonly name="good_name"
								value="${good_name}" /></td>
						</tr>
						<!--상품가격(good_mny) : 필수(숫자만 넘겨주세요) -->
						<tr>
							<td>결제금액(good_mny)</td>
							<td><input type="text" readonly name="good_mny"
								value="${good_mny}" /></td>
						</tr>
						<!-- 구매자명(buyr_name) : 필수 -->
						<tr>
							<td>주문자명(buyr_name)</td>
							<td><input type="text" readonly name="buyr_name"
								value="${buyr_name}" /></td>
						</tr>
					</table>
					<br />

					<!--
			  	===========================
			  	* 공통변수 : 부가정보
			  	===========================
				-->
					<table style="width: 100%;">
						<tr>
							<td colspan="2" align="center">결제 부가정보</td>
						</tr>
						<!--주문자 이메일(buyr_mail) : 권장 -->
						<tr>
							<td>주문자 이메일(buyr_mail)</td>
							<td><input type="text" name="buyr_mail" value="" /></td>
						</tr>
						<!--주문자 전화번호(buyr_tel1) : 권장 -->
						<tr>
							<td>주문자 전화번호(buyr_tel1)</td>
							<td><input type="text" name="buyr_tel1" value="" /></td>
						</tr>
						<!--주문자 전화번호(buyr_tel2) : 권장 -->
						<tr>
							<td>주문자 휴대폰번호(buyr_tel2)</td>
							<td><input type="text" name="buyr_tel2" value="" /></td>
						</tr>
						<!--배송지 주소(rcvr_zipx, rcvr_add1, rcvr_add2) : 옵션 -->
						<tr>
							<td>배송지 우편번호(rcvr_zipx)</td>
							<td><input type="text" name="rcvr_zipx" value="" /></td>
						</tr>
						<tr>
							<td>배송지 주소(rcvr_add1)</td>
							<td><input type="text" name="rcvr_add1" value="" /></td>
						</tr>
						<tr>
							<td>배송지 상세주소(rcvr_add2)</td>
							<td><input type="text" name="rcvr_add2" value="" /></td>
						</tr>
					</table>

					<input type="hidden" name="Noti_URL" value="" />
					<!-- 백노티URL. 공백일 시, 상점관리자에 등록된 URL로 결과값 전송 -->
					<input type="hidden" name="rcvr_name" value="" />
					<!-- 수취인명-->
					<input type="hidden" name="rcvr_tel1" value="" />
					<!-- 수취인 전화번호-->
					<input type="hidden" name="rcvr_mail" value="" />
					<!-- 수취인 E-MAIL-->
					<input type="hidden" name="rcvr_date" value="" />
					<!-- 배송 희망일-->
					<input type="hidden" name="rqst_msgx" value="" />
					<!-- 배송 코멘트-->
					<input type="hidden" name="kindcss" value="" />
					<!-- 결제창스킨(blue, green, pink, violet, yellow) 추후 지원예정 -->
					<input type="hidden" name="pay_option1" value="${good_name}" />
					<!-- 여유필드(상점에서 사용가능한 여유필드) -->
					<input type="hidden" name="pay_option2" value="${good_mny}" />
					<!-- 여유필드(상점에서 사용가능한 여유필드) -->
					<input type="hidden" name="pay_option3" value="${ordr_idxx}" />
					<!-- 여유필드(상점에서 사용가능한 여유필드) -->
					<input type="hidden" name="pay_option4" value="${userId }" />
					<!-- 여유필드(상점에서 사용가능한 여유필드) -->
					<input type="hidden" name="pay_option5" value="${point}" />
					<!-- 여유필드(상점에서 사용가능한 여유필드) -->

					<!--
			  	=================================
			  	* 결제수단별 변수 : 부가정보
			  	=================================
				-->
					<input type="hidden" name="card_quota" value="" />
					<!--[신용카드] 카드할부기간 -->
					<input type="hidden" name="cardtype" value="" />
					<!--[신용카드] 결제카드종류 -->

					<!-- 
					* 5만원이상의 계좌이체/가상계좌 거래에 대해서는 에스크로 결제여부를 묻는 창이 자동으로 표시됩니다.(단, 에스크로서비스를 신청한 상점에 한함)
					* 무조건 에스크로 결제만을 원하시는 경우 escrow_type 변수에 value값으로 "EA01" 를 넘겨주시면 됩니다.
				-->
					<input type="hidden" name="escrow_yn" value="" />
					<!-- [계좌이체/가상계좌] 에스크로 사용유무(Y/N)-->
					<input type="hidden" name="escrow_type" value="" />
					<!-- [계좌이체/가상계좌] 에스크로결제여부(EA01:무조건 에스크로결제/EA02:고객선택 에스크로)-->

					<input type="hidden" name="bankcode" value="" />
					<!-- [가상계좌] 발급은행코드 -->

					<input type="hidden" name="comm_type" value="" />
					<!-- [휴대폰] 통신사코드 -->

					<!--
					* 더존페이에서 지정한 변수 외에 상점측에서 필요한 변수도 사용가능합니다.
					* 단, 상점측 변수는 더존페이에 저장되지는 않으니, 별도로 관리하셔야합니다.
					* 만약 거래결과누락으로 인한 거래결과 재전송의 경우 상점측 변수 값은 전달되지않으니 유의바랍니다.
				-->


					<!--
				  ===========================
			  	* 결제응답 정보 : 선언 필수!!
			  	* 더존페이에서 결제를 진행하면서 값을 채우는 항목입니다.
			  	* 값을 설정하지 마세요.
			  	===========================
				-->
					<!--공통 파라미터-->
					<input type="hidden" id="BKW_RESULTCD" name="BKW_RESULTCD" value="">
					<!-- 지불결과코드 0000 성공  0000이외 오류 -->
					<input type="hidden" id="BKW_RESULTMSG" name="BKW_RESULTMSG"
						value="">
					<!-- 지불결과메시지-->
					<input type="hidden" id="BKW_TRADENO" name="BKW_TRADENO" value="">
					<!-- 더존페이거래번호(자동채번되니 수정하지마세요)-->
					<input type="hidden" id="BKW_PAYTYPE" name="BKW_PAYTYPE" value="">
					<!-- 결제수단 신용카드: PA11 / 계좌이체: PA03 / 가상계좌: PA10 / 휴대폰 소액결제: PA08 / 폰빌 전화결제: PA01-->
					<input type="hidden" id="BKW_AUTHDATE" name="BKW_AUTHDATE" value="">
					<!-- 승인일자-->
					<input type="hidden" id="BKW_AMOUNT" name="BKW_AMOUNT" value="">
					<!-- 승인금액-->
					<input type="hidden" id="BKW_SHA256" name="BKW_SHA256" value="">
					<!-- 거래금액검증 hash 값 -->

					<!--신용카드-->
					<input type="hidden" id="BKW_AUTHNO" name="BKW_AUTHNO" value="">
					<!-- 카드승인번호-->
					<input type="hidden" id="BKW_CARDNAME" name="BKW_CARDNAME" value="">
					<!-- 카드명-->
					<input type="hidden" id="BKW_QUOTA" name="BKW_QUOTA" value="">
					<!-- 할부개월 00 일시불 02 2개월-->

					<!--계좌이체 가상계좌-->
					<input type="hidden" id="BKW_BANKNM" name="BKW_BANKNM" value="">
					<!-- 은행명-->
					<input type="hidden" id="BKW_BANKACCOUNT" name="BKW_BANKACCOUNT"
						value="">
					<!-- 가상계좌 발급계좌번호-->

					<!--휴대폰/전화결제-->
					<input type="hidden" id="BKW_PHONENO" name="BKW_PHONENO" value="">
					<!--  결제전화번호-->
					<input type="hidden" id="BKW_PHONECOMMTYPE"
						name="BKW_PHONECOMMTYPE" value="">
					<!--  통신사(SKT,KT,LGT)-->
				</form>
			</td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
		</tr>
		<tr>
			<td align="center"><input type="button" value="결제하기"
				onClick="doduzonpayCheck();"></td>
		</tr>
	</table>
</body>
</html>