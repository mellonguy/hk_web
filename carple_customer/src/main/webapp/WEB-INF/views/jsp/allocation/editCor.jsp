<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!doctype html>
<html lang="ko">
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>

<body class="" style="overscroll-behavior: contain;">

	<script type="text/javascript">  

var contents = "";
var carCount = 0;
var carInfo = "";
var departure = "";
var arrival = "";
var address = "";
var addressInfoArr;
var baseInfo = "";
var homeLoader;
var carStatus = true;
var locationStatus = true;

var currentStatus = "base";
$(document).ready(function(){

	$("#total").children().each(function(index,element){
		$(this).css("display","none");
	});
	$("#base").css("display","")
	contents = $("#car").html();

	carInfo = $("#carInfo").html();
	address = $("#address").html();
	departure = $("#departure").html();
	arrival = $("#arrival").html();
	baseInfo = $("#baseInfo").html();
	$("#innerLocationDetail").html("");
		
	//$(".modal-field").css("display","none");

	
});

$(window).on("scroll", function(){ 
	var scroll_top=$(this).scrollTop();
	$(".loading-indicator-wrapper").css("top",scroll_top);
	$(".loading-indicator-wrapper").css("height","100%");
});


/*
(function() {
	  var touchStartHandler,
	      touchMoveHandler,
	      touchPoint;
	  // Only needed for touch events on chrome.
	  if ((window.chrome || navigator.userAgent.match("CriOS"))
	      && "ontouchstart" in document.documentElement) {
	      touchStartHandler = function() {
	          // Only need to handle single-touch cases
	          touchPoint = event.touches.length === 1 ? event.touches[0].clientY : null;
	      };
	      touchMoveHandler = function(event) {
	          var newTouchPoint;
	          // Only need to handle single-touch cases
	          if (event.touches.length !== 1) {
	              touchPoint = null;
	              return;
	          }
	          // We only need to defaultPrevent when scrolling up
	          newTouchPoint = event.touches[0].clientY;
	          if (newTouchPoint > touchPoint) {
	              event.preventDefault();
	          }
	          touchPoint = newTouchPoint;
	      };
	      document.addEventListener("touchstart", touchStartHandler, {
	          passive: false
	      });
	      document.addEventListener("touchmove", touchMoveHandler, {
	          passive: false
	      });
	  }
	})();
*/

        
    
    function selectInterestItem(itemCd,marketCd,obj){
    	
    	$.ajax({ 
    		type: 'post' ,
    		url : "/interest/selectInterestItem.do" ,
    		dataType : 'json' ,
    		data : {
    			itemCd : itemCd,
    			marketCd : marketCd
    		},
    		success : function(data, textStatus, jqXHR)
    		{
    			if(data.resultCode == "0000"){
    				insertInterestItem(itemCd,marketCd,obj);
    			}else{
    				deleteInterestItem(itemCd,marketCd,obj);
    			}
    		} ,
    		error : function(xhRequest, ErrorText, thrownError) {
    		}
    	}); 
    	
    	
    }    
        
    function insertInterestItem(itemCd,marketCd,obj){
    	
    	$.ajax({ 
    		type: 'post' ,
    		url : "/interest/insertInterestItem.do" ,
    		dataType : 'json' ,
    		data : {
    			itemSrtCd : itemCd,
    			marketCd : marketCd
    		},
    		success : function(data, textStatus, jqXHR)
    		{
    			if(data.resultCode == "0000"){
    				$(obj).addClass('heart-full');
    			}else if(data.resultCode == "E002"){
    					alert("로그인 되지 않음");
    			}
    		} ,
    		error : function(xhRequest, ErrorText, thrownError) {
    		}
    	}); 
    }    
        
      

    function deleteInterestItem(itemCd,marketCd,obj){
    	
    	$.ajax({ 
    		type: 'post' ,
    		url : "/interest/deleteInterestItem.do" ,
    		dataType : 'json' ,
    		data : {
    			itemSrtCd : itemCd,
    			marketCd : marketCd
    		},
    		success : function(data, textStatus, jqXHR)
    		{
    			if(data.resultCode == "0000"){
    				$(obj).removeClass('heart-full');
    			}else if(data.resultCode == "E002"){
    					alert("로그인 되지 않음");
    			}
    		} ,
    		error : function(xhRequest, ErrorText, thrownError) {
    		}
    	}); 
    }    


    function prevCategory(obj,category){
        
    	var tabIdx = 0;
		$("#category").children().each(function(index,element){
			if($(this).attr("class") == "active"){
				//$(this).next().trigger("click");
				tabIdx = index-1;
			} 
		});

		var categoryId = "";

		if(currentStatus == "request"){
    		categoryId = "confirm";
			$("#first").css("display","none");
			$("#middle").css("display","");
			$("#last").css("display","none");
        }else if(currentStatus == "confirm"){
    		categoryId = "location";
    		$("#first").css("display","none");
			$("#middle").css("display","");
			$("#last").css("display","none");
        }else if(currentStatus == "location"){
    		categoryId = "car";
    		$("#first").css("display","none");
			$("#middle").css("display","");
			$("#last").css("display","none");
        }else if(currentStatus == "car"){
    		categoryId = "base";
    		$("#first").css("display","");
			$("#middle").css("display","none");
			$("#last").css("display","none");
        }

    	changeCategory($("#category").children().eq(tabIdx),categoryId);
		
    }


    
 	function nextCategory(obj,category){

		var tabIdx = 0;
		$("#category").children().each(function(index,element){
			if($(this).attr("class") == "active"){
				//$(this).next().trigger("click");
				tabIdx = index+1;
			} 
		});
	
		//$.alert(currentStatus);
		
    	var categoryId = "";
    	if(currentStatus == "base"){
    		categoryId = "car";
			$("#first").css("display","none");
			$("#middle").css("display","");
			$("#last").css("display","none");
        }else if(currentStatus == "car"){
    		categoryId = "location";
    		$("#first").css("display","none");
			$("#middle").css("display","");
			$("#last").css("display","none");
        }else if(currentStatus == "location"){
    		categoryId = "confirm";
    		$("#first").css("display","none");
			$("#middle").css("display","none");
			$("#last").css("display","");
        }else if(currentStatus == "confirm"){
    		categoryId = "request";
    		$("#first").css("display","none");
			$("#middle").css("display","none");
			$("#last").css("display","");
        }  
    	changeCategory($("#category").children().eq(tabIdx),categoryId);
 	 }


function validation(obj){





	
	var contentLen = $("#confirm").find("p").length;
	var contentArr = new Array(contentLen);
	
	$("#confirm").find("p").each(function(index,element){
		contentArr[index] = $.trim($(this).html())+"";					
	});

	if(contentArr[2] == ""){
		$.alert("제목이 작성되지 않았습니다.",function(a){
			var categoryId = "base";
    		$("#first").css("display","");
			$("#middle").css("display","none");
			$("#last").css("display","none");
			changeCategory($("#category").children().eq(0),categoryId);
		});
		return false;
	}
	
	var carCount = Number(contentArr[4]);

	for(var v = 0; v < carCount; v++){
		var gap = Number(9);
		var offset = Number(11);

		
		if(contentArr[(v*offset)+gap] == ""){
			$.alert((v+1)+"번째 차량의 제조사가 작성되지 않았습니다.",function(a){
				var categoryId = "car";
				$("#first").css("display","none");
				$("#middle").css("display","");
				$("#last").css("display","none");
				changeCategory($("#category").children().eq(1),categoryId);
			});
			return false;
		}
		if(contentArr[(v*offset)+1+gap] == ""){
			$.alert((v+1)+"번째 차량의 차종(모델명)이(가) 작성되지 않았습니다.",function(a){
				var categoryId = "car";
				$("#first").css("display","none");
				$("#middle").css("display","");
				$("#last").css("display","none");
				changeCategory($("#category").children().eq(1),categoryId);
			});
			return false;
			
		}

		if(contentArr[(v*offset)+2+gap] == ""){
			$.alert((v+1)+"번째 차량의 세부차종(트림)이(가) 작성되지 않았습니다.",function(a){
				var categoryId = "car";
				$("#first").css("display","none");
				$("#middle").css("display","");
				$("#last").css("display","none");
				changeCategory($("#category").children().eq(1),categoryId);
			});
			return false;
			
		}

		if(contentArr[(v*offset)+3+gap] == ""){
			$.alert((v+1)+"번째 차량의 차량번호이(가) 작성되지 않았습니다.",function(a){
				var categoryId = "car";
				$("#first").css("display","none");
				$("#middle").css("display","");
				$("#last").css("display","none");
				changeCategory($("#category").children().eq(1),categoryId);
			});
			return false;
			
		}

		 if(contentArr[(v*offset)+4+gap] == ""){
			//차대번호
			
		}

		if(contentArr[(v*offset)+5+gap] == ""){
			//파일갯수

		}

		if(contentArr[(v*offset)+6+gap] == ""){
			$.alert((v+1)+"번째 차량의 출발지이(가) 작성되지 않았습니다.",function(a){
				var categoryId = "location";
	    		$("#first").css("display","none");
				$("#middle").css("display","");
				$("#last").css("display","none");
				changeCategory($("#category").children().eq(2),categoryId);
			});
			return false;
			
		}

		if(contentArr[(v*offset)+7+gap] == ""){
			$.alert((v+1)+"번째 차량의 출발지이(가) 검색되지 않았습니다. 주소를 검색 해 주세요.",function(a){
				var categoryId = "location";
	    		$("#first").css("display","none");
				$("#middle").css("display","");
				$("#last").css("display","none");
				changeCategory($("#category").children().eq(2),categoryId);
			});
			return false;
			
		}

		if(contentArr[(v*offset)+8+gap] == ""){
			$.alert((v+1)+"번째 차량의 도착지이(가) 작성되지 않았습니다.",function(a){
				var categoryId = "location";
	    		$("#first").css("display","none");
				$("#middle").css("display","");
				$("#last").css("display","none");
				changeCategory($("#category").children().eq(2),categoryId);
			});
			return false;
			
		}

		if(contentArr[(v*offset)+9+gap] == ""){
			$.alert((v+1)+"번째 차량의 도착지이(가) 검색되지 않았습니다. 주소를 검색 해 주세요.",function(a){
				var categoryId = "location";
	    		$("#first").css("display","none");
				$("#middle").css("display","");
				$("#last").css("display","none");
				changeCategory($("#category").children().eq(2),categoryId);
			});
			return false;
			
		}

	}

	$.confirm("이 내용으로 수정 하시겠습니까?",function(a){
			if(a){
				homeLoader.show();
				updateRequest(obj,contentArr)
			}
	});
	

}



 	
function updateRequest(obj,contentArr){

	
	$('#insertForm').empty();
	$('#insertForm').append($("#confirm").clone());

	$('#insertForm').ajaxForm({
		url : "/allocation/update-allocation.do" ,
	    type: "POST",
		dataType: "json",		
		enctype: "multipart/form-data",
		traditional : true,
		data : {
			contentArr : contentArr,
			corId : "${paramMap.corId}"
	    },
		success: function(data, response, status) {
			var result = data.resultCode;
			var resultData = data.resultData;
			if(result == "0000"){
				homeLoader.hide();
				$.alert("수정 되었습니다.",function(a){
					document.location.href = "/allocation/main.do";
				});
			}else if(result == "E001"){
				homeLoader.hide();
				$.alert("알 수없는 오류입니다. 카카오톡 플러스채팅으로 문의 주세요",function(a){
					return;	
				});
			}
		},
		error: function() {
			homeLoader.hide();
			$.alert("오류가 발생하였습니다. 관리자에게 문의 하세요.");
		}                               
	});
	$("#insertForm").submit();











	
 	 			
}
 		


	    
    
var isChange = false;

   function changeCategory(obj,categoryId){

	   
    	var inputCount = $("#car").children().length;
		$("#category").children().each(function(index,element){
			$(this).removeClass("active");
		});
		$(obj).addClass("active");

			if(categoryId != ""){
				
				$("#total").children().each(function(index,element){
					$(this).css("display","none");
				})
				
				if(categoryId == "car"){

					var tmp = carCount;

			    	if($("#carCount").val() == 0 && carCount == 0){
			    		$.alert("차량대수가 입력 되지 않았습니다.",function(a){
						});
			    		$("#"+categoryId).css("display","");
			    		isChange = false;
						return;
						
			    	}else if($("#carCount").val() != 0 && carCount == 0){
			        	if($("#carCount").val() < 1){
			        		$("#carCount").val(1)
			            }else if($("#carCount").val() > 10){
			        		$("#carCount").val(10)
			            }
			    		carCount = $("#carCount").val();

			    		//이미 추가가 되어 있는 상태임.
			    		//$("#car").html("");
						//for(var i = 0; i < carCount; i++){
						//	$("#car").append(contents);
						//}
						
						$("#"+categoryId).css("display","");
						isChange = true;
			        }else if(carCount != 0 && carCount != $("#carCount").val()){
						//기존 정보가 변경 되는경우
			        	carCount = $("#carCount").val();
			        	$.confirm("차량대수가 변경 되었습니다. 적용 하시겠습니까?",function(a){
							if(a){
								isChange = true;
								var carLen = $("#car").children().length;
								if(carLen < carCount){
									//기존의 차량대수보다 많아지는경우 많아진만큼 추가 한다.
									for(var i = 0; i < carCount-carLen; i++){
										$("#car").append(contents);
									}
								}else if(carLen > carCount){
									//기존의 차량대수보다 적어지는 경우
									$("#car").children().each(function(index,element){
										if(index > carCount-1){
											$(this).remove();
										}
									});
								}else{
									//그럴리는 없겠지만...
									$("#car").html("");
									for(var i = 0; i < carCount; i++){
										$("#car").append(contents);
									}
								}
								
								$("#"+categoryId).css("display","");
							}else{
								isChange = false;
								$("#carCount").val(tmp);
								carCount = tmp;
								$("#"+categoryId).css("display","");
							}
						});

			        	
			        }else if(carCount != 0 && carCount == $("#carCount").val()){
			        	isChange = false;
			        	$("#"+categoryId).css("display","");
				    }

				}else if(categoryId != "car"){

					if(categoryId == "location"){

						//$.alert(carCount);
						if(Number(carCount) == 0){

							$.alert("차량정보가 입력 되지 않았습니다.",function(a){
							});
							$("#category").children().each(function(index,element){
								$(this).removeClass("active");
							});
							$("#category").children().first().addClass("active");
							$("#base").css("display","");
							return false;
						}else{

							//$("#innerLocationDetail").html("");
							
							if(Number($("#carCount").val()) <= 1){
								//$("#locationSetting").css("display","none");
							}else{
								//$("#locationSetting").css("display","");
							}

							$("#locationSetting").css("display","none");
							
							$("#"+categoryId).css("display","");

							var carInfoArr = new Array(inputCount); 

							$("#car").children().each(function(index,element){
								var setInfo = $(carInfo);
								$(this).find("input").each(function(innerindex,innerelement){
									if($(this).attr('type') != "file"){
										$(setInfo).find("P").eq(innerindex).html($(this).val());
									}else{
										var x = this;
										$(setInfo).find("P").eq(innerindex).html(""+x.files.length);									
										$(setInfo).append($(this).clone());
									}
								});
								$(setInfo).find("P").last().html($(this).find("select").eq(0).val()=="OC"?"중고차":"신차");
								carInfoArr[index] = $(setInfo);
								//$.alert($(carInfoArr[index]).html());
							});

							
							/*
							for(var i = 0; i < Number(inputCount); i++){
								$("#innerLocationDetail").append(carInfoArr[i]);
							}
							$("#innerLocationDetail").append(departure);
							$("#innerLocationDetail").append(arrival);
							*/
								
						}

						if(currentStatus == "car"){
							getCheckBoxVal("");
						}
						
					}else if(categoryId == "confirm"){

						$("#confirm").html("");
						var baseInfoArr = new Array(7);
						baseInfoArr[0] = $("#base").find("input").eq(0).val();
						baseInfoArr[1] = $("#base").find("input").eq(1).val();
						baseInfoArr[2] = $("#base").find("input").eq(2).val();
						baseInfoArr[3] = "";
						if($("#base").find("select").eq(0).val() == "SL"){
							baseInfoArr[3] = "세이프티로더";
						}else if($("#base").find("select").eq(0).val() == "CA"){
							baseInfoArr[3] = "캐리어";
						}else if($("#base").find("select").eq(0).val() == "RD"){
							baseInfoArr[3] = "로드탁송";
						}else if($("#base").find("select").eq(0).val() == "TR"){
							baseInfoArr[3] = "추레라";
						}else if($("#base").find("select").eq(0).val() == "FC"){
							baseInfoArr[3] = "풀카";
						}else if($("#base").find("select").eq(0).val() == "BC"){
							baseInfoArr[3] = "박스카";
						}else if($("#base").find("select").eq(0).val() == "NE"){
							baseInfoArr[3] = "상관없음";
						}
						
						baseInfoArr[4] = carCount;
						baseInfoArr[5] = "";
						if($("#base").find("select").eq(1).val() == "RC"){
							baseInfoArr[5] = "역경매";
						}else if($("#base").find("select").eq(1).val() == "DR"){
							baseInfoArr[5] = "오픈형";
						}
						baseInfoArr[6] = $("#base").find("input").eq(4).val();
						baseInfoArr[7] = "";
						if($("#base").find("select").eq(2).val() == "PR"){
							baseInfoArr[7] = "우선순위결정";
						}else if($("#base").find("select").eq(2).val() == "OA"){
							baseInfoArr[7] = "선착순결정";
						}
						baseInfoArr[8] = $("#base").find("select").eq(3).val()+"분";
						var setBaseInfo = $(baseInfo);
						
						$(setBaseInfo).find("p").each(function(index,element){
							$(this).html(baseInfoArr[index]);
						});
						var departureIsChecked = $("#sameDeparture").is(':checked');
						var arrivalIsChecked = $("#sameArrival").is(':checked');
						$("#confirm").append($(setBaseInfo));
						
						//차량 관련
						var inputCount = $("#car").children().length;
						var carInfoArr = new Array(inputCount); 
						$("#car").children().each(function(index,element){
							var setInfo = $(carInfo);

							$(this).find("input").each(function(innerindex,innerelement){
								//$.alert($(this).attr('type'));
								if($(this).attr('type') != "file"){
									$(setInfo).find("P").eq(innerindex).html($(this).val());
								}else{

									var x = this;
									//$.alert(x.files.length);
									$(setInfo).find("P").eq(innerindex).html(""+x.files.length);									
									/*
									var txt = "";
									if ('files' in x) {

										$.alert(x.files.length);
										
									    if (x.files.length == 0) {
									        txt = "Select one or more files.";
									    } else {
									        for (var i = 0; i < x.files.length; i++) {
									            txt += "<br><strong>" + (i+1) + ". file</strong><br>";
									            var file = x.files[i];
									            if ('name' in file) {
									                txt += "name: " + file.name + "<br>";
									            }
									            if ('size' in file) {
									                txt += "size: " + file.size + " bytes <br>";
									            }
									        }
									    }
									}*/	
																	
									$(setInfo).append($(this).clone());
								}
								
							});
							$(setInfo).find("P").last().html($(this).find("select").eq(0).val()=="OC"?"중고차":"신차");
							carInfoArr[index] = $(setInfo);
						});

						//주소 관련						
						var addressCount = $("#innerLocationDetail").find(".product-use-item").length;
						addressInfoArr = new Array(addressCount);
						var innerDetail = $("#innerLocationDetail").clone();
						if(Number(addressCount) > 0){
							$(innerDetail).find(".product-use-item").each(function(index,element){
								addressInfoArr[index] = new Array(2);
								$(this).find("input").each(function(innerindex,innerelement){
									addressInfoArr[index][innerindex] = $(this).val();
								});
							});
						}
						
						if(departureIsChecked && arrivalIsChecked){
							//출발지와 도착지가 같은경우
							for(var i = 0; i < Number(inputCount); i++){
								var div = $('<div style="padding:3%;"></div>');
								var innerDiv = $('<div style="border:1px solid #ff463c;"></div>');
								innerDiv.append(carInfoArr[i]);
								innerDiv.append($(address));
								div.append(innerDiv);
								$("#confirm").append(div);
								//$("#confirm").append(carInfoArr[i]);
								//$("#confirm").append($(address));
							}
							$("#confirm").find(".product-use-item").each(function(index,element){
								$(this).find("p").each(function(innerindex,innerelement){
									$(this).html(addressInfoArr[index%2][innerindex]);
								});
							});
														
						}else if(departureIsChecked && !arrivalIsChecked){
							//출발지가 같고 도착지가 다른경우
							for(var i = 0; i < Number(inputCount); i++){
								var div = $('<div style="padding:3%;"></div>');
								var innerDiv = $('<div style="border:1px solid #ff463c;"></div>');
								innerDiv.append(carInfoArr[i]);
								innerDiv.append($(address));
								div.append(innerDiv);
								$("#confirm").append(div);
								//$("#confirm").append(carInfoArr[i]);
								//$("#confirm").append($(address));
							}
							var count = 0;
							$("#confirm").find(".product-use-item").each(function(index,element){							
								$(this).find("p").each(function(innerindex,innerelement){
									if(index%2 == 0){
										$(this).html(addressInfoArr[0][innerindex]);
									}else{
										$(this).html(addressInfoArr[index-count][innerindex]);
									}
								});
								if(index%2 != 0){
									count++;
								}
							});
						
						}else if(!departureIsChecked && arrivalIsChecked){
							//출발지가 다르고 도착지가 같은경우
							for(var i = 0; i < Number(inputCount); i++){
								var div = $('<div style="padding:3%;"></div>');
								var innerDiv = $('<div style="border:1px solid #ff463c;"></div>');
								innerDiv.append(carInfoArr[i]);
								innerDiv.append($(address));
								div.append(innerDiv);
								$("#confirm").append(div);
								//$("#confirm").append(carInfoArr[i]);
								//$("#confirm").append($(address));
							}
							var count = 0;
							$("#confirm").find(".product-use-item").each(function(index,element){							
								$(this).find("p").each(function(innerindex,innerelement){
									if(index%2 == 0){
										$(this).html(addressInfoArr[index-count][innerindex]);
									}else{
										$(this).html(addressInfoArr[addressInfoArr.length-1][innerindex]);
									}
								});
								if(index%2 == 0){
									count++;
								}
							});
						
						}else if(!departureIsChecked && !arrivalIsChecked){
							//출발지와 도착지가 다른경우
							for(var i = 0; i < Number(inputCount); i++){
								var div = $('<div style="padding:3%;"></div>');
								var innerDiv = $('<div style="border:1px solid #ff463c;"></div>');
								innerDiv.append(carInfoArr[i]);
								innerDiv.append($(address));
								div.append(innerDiv);
								$("#confirm").append(div);
								//$("#confirm").append(carInfoArr[i]);
								//$("#confirm").append($(address));
							}
							$("#confirm").find(".product-use-item").each(function(index,element){
								$(this).find("p").each(function(innerindex,innerelement){
									$(this).html(addressInfoArr[index][innerindex]);
								});
							});
						} 
						$("#"+categoryId).css("display","");
						
					}else{
						$("#"+categoryId).css("display","");
					}
				}
							
				
			}else{

			}
			

	currentStatus = categoryId;

	if($("#base").find("select").eq(1).val() == "RC"){
		$(".auctionAmount").css("display","none");
	}else if($("#base").find("select").eq(1).val() == "DR"){
		$(".auctionAmount").css("display","");
	}


	if(categoryId == "location" && locationStatus){
		var addressCount = $("#innerLocationDetail").find(".product-use-item").length;
		<c:forEach var="data" items="${consignRequestDetailList}" varStatus="status">
			$("#innerLocationDetail").find(".product-use-item").eq(Number("${status.index}")*2).find("input").eq(0).val("${data.crd_departure}");			
			$("#innerLocationDetail").find(".product-use-item").eq(Number("${status.index}")*2).find("input").eq(1).val("${data.crd_departure_latlng}");
			$("#innerLocationDetail").find(".product-use-item").eq(Number("${status.index}")*2+1).find("input").eq(0).val("${data.crd_arrival}");			
			$("#innerLocationDetail").find(".product-use-item").eq(Number("${status.index}")*2+1).find("input").eq(1).val("${data.crd_arrival_latlng}");		
		</c:forEach>
		locationStatus = false;
	}


	
	
}
    
    

function showMessage(obj){


	//alert($(obj).next().val());

	
	if($(obj).next().val() == "PR" || $(obj).next().val() == "OA" || $(obj).next().val() == "RC" || $(obj).next().val() == "DR"){

		if($(obj).next().val() == "PR"){
			$.alert("우선순위 결정 : 입찰 후 낙찰 결정시 1순위~5순위까지 결정 하고 1순위부터 차례로 낙찰 확정기회를 부여 하여 해당 차례에 낙찰 확정 하는 입찰 건에 최종 낙찰되는 경매 방식 입니다.",function(a){
			});
		}else if($(obj).next().val() == "OA"){
			$.alert("선착순 결정 : 입찰 후 낙찰 결정시 1~5개의 입찰 건에 낙찰 확정 기회를 부여 하고 가장 먼저 낙찰 확정 하는 입찰 건에 최종 낙찰되는 경매 방식 입니다.",function(a){
			});
		}else if($(obj).next().val() == "RC"){
			$.alert("역경매 방식은 기사님이 입찰가를 적어 해당 경매에 입찰 하는 방식 입니다.",function(a){
			});
		}else if($(obj).next().val() == "DR"){
			$.alert("오픈형 방식은 경매 작성시에 금액을 작성 하여 해당 금액에 진행 할 수 있는 기사님들이 입찰 하는 방식 입니다.",function(a){
			});
		}

	}else if($(obj).next().find("input").attr("id") == "sameDeparture"){

		$.alert("탁송하는 모든 차량의 출발지가 동일한경우에 사용 합니다. 출발지가 다른경우 각각 입력 할 수 있습니다.",function(a){
		});

	}else if($(obj).next().find("input").attr("id") == "sameArrival"){

		$.alert("탁송하는 모든 차량의 도착지가 동일한경우에 사용 합니다. 도착지가 다른경우 각각 입력 할 수 있습니다.",function(a){
		});

	}else{

		$.alert("우선순위 결정의 경우 후 순위 입찰에 기회가 부여 되는 시간이고 선착순 결정의 경우 낙찰 확정기회가 부여 되는 시간을 의미합니다.",function(a){

		});
		
	}
	
}



function getCheckBoxVal(obj){


	var addressCount = $("#innerLocationDetail").find(".product-use-item").length;
	var innerDetail = $("#innerLocationDetail").clone();

	var isInput = false;

	
	if(Number(addressCount) > 0){
		addressInfoArr = new Array(addressCount);			
		$(innerDetail).find(".product-use-item").each(function(index,element){
			addressInfoArr[index] = new Array(2);
			$(this).find("input").each(function(innerindex,innerelement){
				addressInfoArr[index][innerindex] = $(this).val();
				if($(this).val() != ""){
					isInput = true;
				}
			});
		});
	}

	if(obj != ""){

		if(isInput){
			$.confirm("주소 정보가 초기화 됩니다. 적용 하시겠습니까?",function(a){
				if(a){

					$("#innerLocationDetail").html("");
					
					var inputCount = $("#car").children().length;
					var carInfoArr = new Array(inputCount); 

					$("#car").children().each(function(index,element){
						var setInfo = $(carInfo);
						$(this).find("input").each(function(innerindex,innerelement){
							$(setInfo).find("P").eq(innerindex).html($(this).val())
						});
						//$(this).find("select").each(function(innerindex,innerelement){
						//	$(setInfo).find("P").last().html($(this).val())
						//});
						$(setInfo).find("P").last().html($(this).find("select").eq(0).val()=="OC"?"중고차":"신차");
						carInfoArr[index] = $(setInfo);
						//$.alert($(carInfoArr[index]).html());
					});
					

						var departureIsChecked = $("#sameDeparture").is(':checked');
						var arrivalIsChecked = $("#sameArrival").is(':checked');

						if(departureIsChecked && arrivalIsChecked){
						//상차지와 하차지가 같은경우
							//$.alert(0);
							for(var i = 0; i < Number(inputCount); i++){
								$("#innerLocationDetail").append(carInfoArr[i]);
							}
							$("#innerLocationDetail").append(departure);
							$("#innerLocationDetail").append(arrival);
						}else if(departureIsChecked && !arrivalIsChecked){
						//상차지는 같고 하차지가 다른경우
							//$.alert(1);
							$("#innerLocationDetail").append(departure);
							for(var i = 0; i < Number(inputCount); i++){
								$("#innerLocationDetail").append(carInfoArr[i]);
								$("#innerLocationDetail").append(arrival);
							}
						}else if(!departureIsChecked && arrivalIsChecked){
						//상차지가 다르고 하차지가 같은경우
							//$.alert(2);
							for(var i = 0; i < Number(inputCount); i++){
								$("#innerLocationDetail").append(carInfoArr[i]);
								$("#innerLocationDetail").append(departure);
							}
							$("#innerLocationDetail").append(arrival);
						}else if(!departureIsChecked && !arrivalIsChecked){
						//상차지도 다르고 하차지도 다른경우
							//$.alert(3);
							for(var i = 0; i < Number(inputCount); i++){
								$("#innerLocationDetail").append(carInfoArr[i]);
								$("#innerLocationDetail").append(departure);
								$("#innerLocationDetail").append(arrival);
							}
						}


						if(obj == ""){
							//생성이 끝났으면 입력 되어 잇던 주소를 입력 한다.		
							if(Number(addressCount) > 0){
								$("#innerLocationDetail").find(".product-use-item").each(function(index,element){
									$(this).find("input").each(function(innerindex,innerelement){
										$(this).val(addressInfoArr[index][innerindex]);
									});
								});
							}
						}


							
				}else{

					var isChecked = $(obj).is(':checked');
					
					if(isChecked){
						$(obj).prop("checked",false);
					}else{
						$(obj).prop("checked",true);
					}
				}
			});

		}else{

			$("#innerLocationDetail").html("");
			
			var inputCount = $("#car").children().length;
			var carInfoArr = new Array(inputCount); 

			$("#car").children().each(function(index,element){
				var setInfo = $(carInfo);
				$(this).find("input").each(function(innerindex,innerelement){
					$(setInfo).find("P").eq(innerindex).html($(this).val())
				});
				$(setInfo).find("P").last().html($(this).find("select").eq(0).val()=="OC"?"중고차":"신차");
				carInfoArr[index] = $(setInfo);
				//$.alert($(carInfoArr[index]).html());
			});
			

				var departureIsChecked = $("#sameDeparture").is(':checked');
				var arrivalIsChecked = $("#sameArrival").is(':checked');

				if(departureIsChecked && arrivalIsChecked){
				//상차지와 하차지가 같은경우
					//$.alert(0);
					for(var i = 0; i < Number(inputCount); i++){
						$("#innerLocationDetail").append(carInfoArr[i]);
					}
					$("#innerLocationDetail").append(departure);
					$("#innerLocationDetail").append(arrival);
				}else if(departureIsChecked && !arrivalIsChecked){
				//상차지는 같고 하차지가 다른경우
					//$.alert(1);
					$("#innerLocationDetail").append(departure);
					for(var i = 0; i < Number(inputCount); i++){
						$("#innerLocationDetail").append(carInfoArr[i]);
						$("#innerLocationDetail").append(arrival);
					}
				}else if(!departureIsChecked && arrivalIsChecked){
				//상차지가 다르고 하차지가 같은경우
					//$.alert(2);
					for(var i = 0; i < Number(inputCount); i++){
						$("#innerLocationDetail").append(carInfoArr[i]);
						$("#innerLocationDetail").append(departure);
					}
					$("#innerLocationDetail").append(arrival);
				}else if(!departureIsChecked && !arrivalIsChecked){
				//상차지도 다르고 하차지도 다른경우
					//$.alert(3);
					for(var i = 0; i < Number(inputCount); i++){
						$("#innerLocationDetail").append(carInfoArr[i]);
						$("#innerLocationDetail").append(departure);
						$("#innerLocationDetail").append(arrival);
					}
				}


		}
				
	}else{

	
		$("#innerLocationDetail").html("");
		var inputCount = $("#car").children().length;
		var carInfoArr = new Array(inputCount); 

		$("#car").children().each(function(index,element){
			var setInfo = $(carInfo);
			$(this).find("input").each(function(innerindex,innerelement){
				$(setInfo).find("P").eq(innerindex).html($(this).val())
			});
			$(setInfo).find("P").last().html($(this).find("select").eq(0).val()=="OC"?"중고차":"신차");
			carInfoArr[index] = $(setInfo);
			//$.alert($(carInfoArr[index]).html());
		});
		

			var departureIsChecked = $("#sameDeparture").is(':checked');
			var arrivalIsChecked = $("#sameArrival").is(':checked');

			if(departureIsChecked && arrivalIsChecked){
			//상차지와 하차지가 같은경우
				//$.alert(0);
				for(var i = 0; i < Number(inputCount); i++){
					$("#innerLocationDetail").append(carInfoArr[i]);
				}
				$("#innerLocationDetail").append(departure);
				$("#innerLocationDetail").append(arrival);
			}else if(departureIsChecked && !arrivalIsChecked){
			//상차지는 같고 하차지가 다른경우
				//$.alert(1);
				$("#innerLocationDetail").append(departure);
				for(var i = 0; i < Number(inputCount); i++){
					$("#innerLocationDetail").append(carInfoArr[i]);
					$("#innerLocationDetail").append(arrival);
				}
			}else if(!departureIsChecked && arrivalIsChecked){
			//상차지가 다르고 하차지가 같은경우
				//$.alert(2);
				for(var i = 0; i < Number(inputCount); i++){
					$("#innerLocationDetail").append(carInfoArr[i]);
					$("#innerLocationDetail").append(departure);
				}
				$("#innerLocationDetail").append(arrival);
			}else if(!departureIsChecked && !arrivalIsChecked){
			//상차지도 다르고 하차지도 다른경우
				//$.alert(3);
				for(var i = 0; i < Number(inputCount); i++){
					$("#innerLocationDetail").append(carInfoArr[i]);
					$("#innerLocationDetail").append(departure);
					$("#innerLocationDetail").append(arrival);
				}
			}

			if(!isChange){
				if(Number(addressCount) > 0){
					$("#innerLocationDetail").find(".product-use-item").each(function(index,element){
						$(this).find("input").each(function(innerindex,innerelement){
							$(this).val(addressInfoArr[index][innerindex]);
						});
					});
				}
			}

			
	}

	//공통으로 설정 되는 부분은 경매진행 하는 차량이 2대 이상인 경우에만...
	if(carCount > 1){
		if(departureIsChecked && arrivalIsChecked){
			$(".product-use-item").find("span").each(function(index,element){
				if($(this).html() == "출발지" || $(this).html() == "도착지"){
					$(this).html("공통"+$(this).html()+"(동일한 "+$(this).html()+"로 설정 됩니다.)");
				} 
			});
		}else if(departureIsChecked && !arrivalIsChecked){
			$(".product-use-item").find("span").each(function(index,element){
				if($(this).html() == "출발지"){
					$(this).html("공통"+$(this).html()+"(동일한 "+$(this).html()+"로 설정 됩니다.)");
				} 
			});
		}else if(!departureIsChecked && arrivalIsChecked){
			$(".product-use-item").find("span").each(function(index,element){
				if($(this).html() == "도착지"){
					$(this).html("공통"+$(this).html()+"(동일한 "+$(this).html()+"로 설정 됩니다.)");
				} 
			});
		}
	}
	
}
    


var map, marker;
var markerArr = [];
var selectedObj;

function goShowModal(obj){


	//return false;

	
	if($(obj).prev().prev().val() == ""){

		$.alert("주소가 입력 되지 않았습니다.",function(a){
		
		});
		return false;
	}else{

		selectedObj = $(obj);
		
		$(selectedObj).parent().next().show();
		$(selectedObj).parent().next().find("ul").css("display","block");
		
		$.ajax({
			method:"GET",
			url:"https://apis.openapi.sk.com/tmap/pois?version=1&format=json&callback=result",
			async:false,
			data:{
				"appKey" : "${appKey}",
				"searchKeyword" : $(obj).prev().prev().val(),
				"resCoordType" : "EPSG3857",
				"reqCoordType" : "WGS84GEO",
				"count" : 10
			},
			success:function(response){

				
				var resultpoisData = response.searchPoiInfo.pois.poi;
				
				// 기존 마커, 팝업 제거
				if(markerArr.length > 0){
					for(var i in markerArr){
						markerArr[i].setMap(null);
					}
				}
				var innerHtml ="";	// Search Reulsts 결과값 노출 위한 변수
				var positionBounds = new Tmapv2.LatLngBounds();		//맵에 결과물 확인 하기 위한 LatLngBounds객체 생성
				
				for(var k in resultpoisData){
					
					var noorLat = Number(resultpoisData[k].noorLat);
					var noorLon = Number(resultpoisData[k].noorLon);
					var name = resultpoisData[k].name;

					var upperAddrName = resultpoisData[k].upperAddrName;
					var middleAddrName = resultpoisData[k].middleAddrName;
					var lowerAddrName = resultpoisData[k].lowerAddrName;
					var detailAddrName = resultpoisData[k].detailAddrName;
					var firstNo = resultpoisData[k].firstNo;
					var secondNo = resultpoisData[k].secondNo == "" ? "" : "-"+resultpoisData[k].secondNo;

					var pointCng = new Tmapv2.Point(noorLon, noorLat);
					var projectionCng = new Tmapv2.Projection.convertEPSG3857ToWGS84GEO(pointCng);
					
					var lat = projectionCng._lat;
					var lon = projectionCng._lng;
					
					var markerPosition = new Tmapv2.LatLng(lat, lon);
					
					marker = new Tmapv2.Marker({
				 		position : markerPosition,
				 		icon : "http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_" + k + ".png",
						iconSize : new Tmapv2.Size(24, 38),
						title : name,
				 	});
					
					innerHtml += '<li  onclick="javascript:inputValue(\''+name+" ("+upperAddrName+" "+middleAddrName+" "+lowerAddrName+" "+detailAddrName+" "+firstNo+secondNo+")"+'\',\''+lon+'\',\''+lat+'\')">';
					innerHtml += '<img src="http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_' + k + '.png" style="vertical-align:middle;"/>';
					innerHtml += '<span>'+name+'</span>';
					innerHtml += '<span style="display:block; color:#3f3f3f;" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;('+upperAddrName+" "+middleAddrName+" "+lowerAddrName+" "+detailAddrName+" "+firstNo+secondNo+')</span>';
					innerHtml += '</li>';

					markerArr.push(marker);
					positionBounds.extend(markerPosition);	// LatLngBounds의 객체 확장
				}
				
				$(selectedObj).parent().next().find("ul").html(innerHtml);
				
			},
			error:function(request,status,error){
				console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
			}
		});
		
	}
	
}

	function inputValue(address,px,py){
		
		$(selectedObj).prev().prev().val(address);
		$(selectedObj).parent().next().find("ul").css("display","none");
		$(selectedObj).prev().val(px+","+py);
		$(selectedObj).parent().next().css('display','none');
	    			
	}

	function prevPageConfirm(){
		$.confirm("이 페이지를 나가면 수정한 내용을 잃게 됩니다. 정말 이동 하시겠습니까?",function(a){
			if(a){
				homeLoader.show();
				history.go(-1);
			}else{
			}
		});
	}


	function removeCarInfo(obj){

		if($("#car").children().length > 1){

			$.confirm("이 차량의 정보를 삭제 하시겠습니까?",function(a){
				if(a){
					$(obj).parent().parent().parent().parent().parent().remove();
					$("#carCount").val($("#carCount").val()-1);
					setCarCategory($("#category").children().eq(1),"car");
					$.alert("삭제되었습니다.",function(a){
					});		
				}else{
					$.alert("삭제가 취소 되었습니다.",function(a){
					});
				}
			});
		}else{
			$.alert("차량정보를 삭제 할 수 없습니다.",function(a){
				
			});

		}

	}


	  function setCarCategory(obj,categoryId){

	    	var inputCount = $("#car").children().length;
			$("#category").children().each(function(index,element){
				$(this).removeClass("active");
			});
			$(obj).addClass("active");

        	carCount = $("#carCount").val();
       	
			isChange = true;
			var carLen = $("#car").children().length;
			if(carLen < carCount){
				//기존의 차량대수보다 많아지는경우 많아진만큼 추가 한다.
				for(var i = 0; i < carCount-carLen; i++){
					$("#car").append(contents);
				}
			}else if(carLen > carCount){
				//기존의 차량대수보다 적어지는 경우
				$("#car").children().each(function(index,element){
					if(index > carCount-1){
						$(this).remove();
					}
				});
			}else{
				
			}
			
			$("#"+categoryId).css("display","");
		
		currentStatus = categoryId;
				
	   }
	    
	 




	function fileRegister(obj){

		$(obj).parent().next().trigger("click");
		$(obj).parent().next().val(""); 
		
	}



	
	function setThumbnail(event,obj) { 

		$(obj).prev().prev().html("");
		
		for (var image of event.target.files){
			var reader = new FileReader();
			reader.onload = function(event){
				var img = document.createElement("img");
				img.setAttribute("src", event.target.result);
				img.setAttribute("style", "width:60px; height:60px;");
				$(obj).prev().prev().append(img);
				//document.querySelector("div#image_container").appendChild(img);
			};
			console.log(image); reader.readAsDataURL(image);
		}
	}


	function changeDisplay(obj){

		if($(obj).val() == "RC"){
			$(".auctionAmount").css("display","none");
		}else{
			$(".auctionAmount").css("display","");
		}

	}


	
    </script>

	<div
		class="content-container withads themedetails companydetails writing pb100">
		<header class="clearfix nb">
			<div class="search-icon">
				<a style="cursor: pointer;" onclick="javascript:prevPageConfirm();"><img
					src="/img/back-icon.png" alt=""></a>
			</div>
			<div class="page-title txt-medium">견적수정</div>
			<div class="menu-bar pull-right">
				<a onclick="javascript:prevPageConfirm();"><img
					src="/img/delete-x-icon.png" alt=""></a>
			</div>
		</header>

		<div class="company-details-container">
			<div class="allocation-views">
				<ul id="category">
					<li class="active"><a>기본정보입력</a></li>
					<li><a>차량정보입력</a></li>
					<li><a>주소정보입력</a></li>
					<li><a>내용확인</a></li>
					<li><a>경매요청</a></li>
				</ul>
			</div>


			<div class="divider black"></div>

			<div id="total">
				<div id="base" style="">
					<div style="width: 100%; overflow: hidden;">
						<div style="float: left; width: 50%;">
							<div class="certNum" style="margin-top: 10%; width: 100%;">
								<div style="text-align: center;">
									<div style="text-align: left; margin-left: 2.5%;">
										<span style="color: #F00;">*</span> <span class="d-tbc"
											style="font-weight: bold;">출발일입력</span>
									</div>
									<%-- <input type="date" style="width: 90%; padding: 3%; margin-top: 5%;" placeholder="출발일" class="d-tbc" id="departureDt" name="departureDt" value="${today}"  onchange="$.alert(this.value);"> --%>
									<input type="date"
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="출발일" class="d-tbc" id="departureDt"
										name="departureDt" value="${consignRequest.cor_departure_dt}">
								</div>
							</div>
						</div>
						<div style="float: left; width: 50%;">
							<div class="certNum" style="margin-top: 10%; width: 100%;">
								<div style="text-align: center;">
									<div style="text-align: left; margin-left: 2.5%;">
										<span style="color: #F00;">*</span> <span class="d-tbc"
											style="font-weight: bold;">출발시간</span>
									</div>
									<%-- <input type="time" style="width: 90%; padding: 3%; margin-top: 5%;" placeholder="출발시간" class="d-tbc" id="departureTime" name="departureTime" value="${now}" onchange="$.alert(this.value);"> --%>
									<input type="time"
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="출발시간" class="d-tbc" id="departureTime"
										name="departureTime"
										value="${consignRequest.cor_departure_time}">
								</div>
							</div>
						</div>
					</div>


					<div style="clear: both; margin-top: 5%;">
						<div id="btn_group" class="certNum"
							style="width: 100%; margin-top: 5%;">
							<div style="text-align: center;">
								<div style="text-align: left; margin-left: 2.5%;">
									<span style="color: #F00;">*</span> <span class="d-tbc"
										style="font-weight: bold;">제목입력</span>
								</div>
								<input type="text"
									style="width: 95%; padding: 2%; margin-top: 2%;"
									placeholder="제목입력" class="d-tbc" id="title" name="title"
									value="${consignRequest.cor_title}" onfocus="">

							</div>
						</div>
					</div>


					<div style="width: 100%; overflow: hidden;">
						<div style="float: left; width: 50%;">
							<div class="certNum" style="margin-top: 10%; width: 100%;">
								<div style="text-align: center;">
									<div style="text-align: left; margin-left: 2.5%;">
										<span style="color: #F00;">*</span> <span class="d-tbc"
											style="font-weight: bold;">탁송방식선택</span>
									</div>
									<select name="carrierType"
										style="width: 85%; padding: 4%; margin-top: 5%;">
										<option value="SL"
											<c:if test="${consignRequest.cor_carrier_type eq 'SL' }">selected</c:if>>세이프티로더</option>
										<option value="CA"
											<c:if test="${consignRequest.cor_carrier_type eq 'CA' }">selected</c:if>>5톤캐리어</option>
										<option value="RD"
											<c:if test="${consignRequest.cor_carrier_type eq 'RD' }">selected</c:if>>로드탁송</option>
										<option value="TR"
											<c:if test="${consignRequest.cor_carrier_type eq 'TR' }">selected</c:if>>추레라</option>
										<option value="FC"
											<c:if test="${consignRequest.cor_carrier_type eq 'FC' }">selected</c:if>>풀카</option>
										<option value="BC"
											<c:if test="${consignRequest.cor_carrier_type eq 'BC' }">selected</c:if>>박스카</option>
										<option value="NE"
											<c:if test="${consignRequest.cor_carrier_type eq 'NE' }">selected</c:if>>상관없음</option>
									</select>
								</div>
							</div>
						</div>
						<div style="float: left; width: 50%;">
							<div class="certNum" style="margin-top: 10%; width: 100%;">
								<div style="text-align: center;">
									<div style="text-align: left; margin-left: 2.5%;">
										<span style="color: #F00;">*</span> <span class="d-tbc"
											style="font-weight: bold;">차량대수(1~9대까지 입력)</span>
									</div>
									<input type="number" readonly onclick=""
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="차량대수" class="d-tbc" id="carCount" name="carCount"
										value="${consignRequest.cor_car_cnt}" min="1" max="10"
										onfocus="">
								</div>
							</div>
						</div>
					</div>


					<div style="width: 100%; overflow: hidden;">
						<div style="float: left; width: 50%;">
							<div class="certNum" style="margin-top: 10%; width: 100%;">
								<div style="text-align: center;">
									<div style="text-align: left; margin-left: 2.5%;"
										onclick="javascript:showMessage(this)">
										<span style="color: #F00;">*</span> <span class="d-tbc"
											style="font-weight: bold;">경매방법</span> <span
											style="margin-left: 47%;"><img
											style="width: 15px; height: 15px; cursor: pointer;"
											src="/img/question-icon.png" onclick="" alt="" title=""></span>
									</div>
									<select name="auctionType" id="auctionType"
										style="width: 85%; padding: 4%; margin-top: 5%;"
										onchange="javascript:changeDisplay(this);">
										<option value="RC"
											<c:if test="${consignRequest.cor_auction_type eq 'RC' }">selected</c:if>>역경매</option>
										<option value="DR"
											<c:if test="${consignRequest.cor_auction_type eq 'DR' }">selected</c:if>>오픈형</option>
									</select>
								</div>
							</div>
						</div>
						<div style="float: left; width: 50%;" class="auctionAmount">
							<div class="certNum" style="margin-top: 10%; width: 100%;">
								<div style="text-align: center;">
									<div style="text-align: left; margin-left: 2.5%;">
										<span style="color: #F00;">*</span> <span class="d-tbc"
											style="font-weight: bold;">금액(오픈형인경우작성)</span>
									</div>
									<input type="number"
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="금액입력" class="d-tbc" name="auctionAmount"
										value="${consignRequest.auction_amount}" min="20000"
										max="1000000" onfocus="">
								</div>
							</div>
						</div>
					</div>

					<div style="width: 100%; overflow: hidden;">
						<div style="float: left; width: 50%;">
							<div class="certNum" style="margin-top: 10%; width: 100%;">
								<div style="text-align: center;">
									<div style="text-align: left; margin-left: 2.5%;"
										onclick="javascript:showMessage(this)">
										<span style="color: #F00;">*</span> <span class="d-tbc"
											style="font-weight: bold;">낙찰결정방식</span> <span
											style="margin-left: 35%;"><img
											style="width: 15px; height: 15px; cursor: pointer;"
											src="/img/question-icon.png" onclick="" alt="" title=""></span>
									</div>
									<select name="bidDecisionType" id="bidDecisionType"
										style="width: 85%; padding: 4%; margin-top: 5%;">
										<option value="PR"
											<c:if test="${consignRequest.cor_bid_decision_type eq 'PR' }">selected</c:if>>우선순위결정</option>
										<option value="OA"
											<c:if test="${consignRequest.cor_bid_decision_type eq 'OA' }">selected</c:if>>선착순결정</option>
									</select>
								</div>
							</div>
						</div>
						<div style="float: left; width: 50%;">
							<div class="certNum" style="margin-top: 10%; width: 100%;">
								<div style="text-align: center;">
									<div style="text-align: left; margin-left: 2.5%;"
										onclick="javascript:showMessage(this)">
										<span style="color: #F00;">*</span> <span class="d-tbc"
											style="font-weight: bold;">낙찰결정시간</span> <span
											style="margin-left: 35%;"><img
											style="width: 15px; height: 15px; cursor: pointer;"
											src="/img/question-icon.png" onclick="" alt="" title=""></span>
									</div>
									<select name="decisionTime" id="decisionTime"
										style="width: 85%; padding: 4%; margin-top: 5%;">
										<option value="10"
											<c:if test="${consignRequest.cor_bid_decision_time eq '10' }">selected</c:if>>10분</option>
										<option value="20"
											<c:if test="${consignRequest.cor_bid_decision_time eq '20' }">selected</c:if>>20분</option>
										<option value="30"
											<c:if test="${consignRequest.cor_bid_decision_time eq '30' }">selected</c:if>>30분</option>
									</select>
								</div>
							</div>
						</div>
					</div>

				</div>


				<div id="car" style="display: none;">
					<c:forEach var="data" items="${consignRequestDetailList}"
						varStatus="status">
						<div style="margin: 3%; border: 1px solid #ff463c;">
							<div style="width: 100%; overflow: hidden;">
								<div style="float: left; width: 50%;">
									<div class="certNum" style="margin-top: 10%; width: 100%;">
										<div style="text-align: center;">
											<div style="text-align: left; margin-left: 2.5%;">
												<span style="color: #F00;">*</span> <span class="d-tbc"
													style="font-weight: bold;">제조사</span>
											</div>
											<input type="text"
												style="width: 90%; padding: 3%; margin-top: 5%;"
												placeholder="제조사" class="d-tbc" name="maker"
												value="${data.crd_car_maker}" onfocus="">
										</div>
									</div>
								</div>
								<div style="float: left; width: 50%;">
									<div class="certNum" style="margin-top: 10%; width: 100%;">
										<div style="text-align: center;">
											<div style="text-align: left; margin-left: 2.5%;">
												<span style="color: #F00;">*</span> <span class="d-tbc"
													style="font-weight: bold;">차종(모델명)</span>
											</div>
											<input type="text"
												style="width: 90%; padding: 3%; margin-top: 5%;"
												placeholder="차종(모델명)" class="d-tbc" name="carKind"
												value="${data.crd_car_maker}" onfocus="">
										</div>
									</div>
								</div>
							</div>

							<div style="width: 100%; overflow: hidden;">
								<div style="float: left; width: 50%;">
									<div class="certNum" style="margin-top: 10%; width: 100%;">
										<div style="text-align: center;">
											<div style="text-align: left; margin-left: 2.5%;">
												<span style="color: #F00;">*</span> <span class="d-tbc"
													style="font-weight: bold;">세부차종(트림)</span>
											</div>
											<input type="text"
												style="width: 90%; padding: 3%; margin-top: 5%;"
												placeholder="세부차종(트림)" class="d-tbc" name="trims"
												value="${data.crd_car_maker}" onfocus="">
										</div>
									</div>
								</div>
								<div style="float: left; width: 50%;">
									<div class="certNum" style="margin-top: 10%; width: 100%;">
										<div style="text-align: center;">
											<div style="text-align: left; margin-left: 2.5%;">
												<span style="color: #F00;">*</span> <span class="d-tbc"
													style="font-weight: bold;">차량번호</span>
											</div>
											<input type="text"
												style="width: 90%; padding: 3%; margin-top: 5%;"
												placeholder="차량번호" class="d-tbc" name="carNum"
												value="${data.crd_car_maker}" onfocus="">
										</div>
									</div>
								</div>
							</div>

							<div style="width: 100%; overflow: hidden;">
								<div style="float: left; width: 50%;">
									<div class="certNum" style="margin-top: 10%; width: 100%;">
										<div style="text-align: center;">
											<div style="text-align: left; margin-left: 2.5%;">
												<span class="d-tbc" style="font-weight: bold;">차대번호</span>
											</div>
											<input type="text"
												style="width: 90%; padding: 3%; margin-top: 5%;"
												placeholder="차대번호" class="d-tbc" name="carIdNum"
												value="${data.crd_car_maker}" onfocus="">
										</div>
									</div>
								</div>
								<div class="productsinuse" style="float: left; width: 50%;">
									<div class="certNum product-use-list"
										style="margin-top: 10%; width: 100%;">
										<div class="product-use-item"
											style="text-align: center; border: none;">
											<div style="text-align: left; margin-left: 2.5%;">
												<span class="d-tbc" style="font-weight: bold;">차량사진</span>
											</div>
											<div style="float: left; width: 50%;"></div>
											<div style="float: left; width: 50%;">
												<a onclick="javascript:fileRegister(this);"
													class="product-use-search inlined" style="margin-top: 5%;">사진등록</a>
											</div>
											<input type="file"
												style="width: 90%; padding: 3%; margin-top: 5%; display: none;"
												placeholder="차량번호" class="d-tbc" name="carPicture" value=""
												onfocus="" onchange="javascript:setThumbnail(event,this);"
												multiple>
										</div>
									</div>
								</div>
							</div>

							<div style="width: 100%; overflow: hidden; padding-bottom: 5%;">
								<div style="float: left; width: 50%;">
									<div class="certNum" style="margin-top: 10%; width: 100%;">
										<div style="text-align: center;">
											<div style="text-align: left; margin-left: 2.5%;">
												<span class="d-tbc" style="font-weight: bold;">차량구분</span>
											</div>
											<select name="newCar" id=""
												style="width: 90%; padding: 4%; margin-top: 5%;">
												<option value="NC"
													<c:if test="${data.crd_car_type eq 'NC' }">selected</c:if>>신차</option>
												<option value="OC"
													<c:if test="${data.crd_car_type eq 'OC' }">selected</c:if>>중고차</option>
											</select>
										</div>
									</div>
								</div>
								<div class="productsinuse" style="float: left; width: 50%;">
									<div class="certNum product-use-list"
										style="margin-top: 10%; width: 100%;">
										<div class="product-use-item"
											style="text-align: right; border: none; padding: 8px 0;">
											<div style="text-align: left; margin-left: 2.5%;">
												<span class="d-tbc" style="font-weight: bold;">&nbsp;</span>
											</div>
											<!-- <a onclick="javascript:removeCarInfo(this);"><img src="/img/delete-x-icon.png" style="width:30px; height:30px;" alt=""></a> -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</c:forEach>
				</div>

				<div id="location" class="notification-settings content-box"
					style="display: none;">


					<div id="locationSetting" style="width: 100%; overflow: hidden;">
						<div class="notif-box pl22" style="padding: 0 15px;">
							<div onclick="javascript:showMessage(this);">
								<span class="d-tbc"
									style="line-height: 70px; font-weight: bold;">출발지 정보 동일</span>
								<span style="margin-left: 5%;"><img
									style="width: 15px; height: 15px; cursor: pointer;"
									src="/img/question-icon.png" onclick="" alt="" title=""></span>
							</div>
							<div class="track-holder">
								<input type="checkbox" id="sameDeparture" hidden
									onchange="javascript:getCheckBoxVal(this);" />
								<!-- <input type="checkbox" id="sameDeparture" hidden checked onchange="javascript:getCheckBoxVal();" /> -->
								<label for="sameDeparture"></label>
							</div>
						</div>
						<div class="notif-box pl22" style="padding: 0 15px;">
							<div onclick="javascript:showMessage(this);">
								<span class="d-tbc"
									style="line-height: 70px; font-weight: bold;">도착지 정보 동일</span>
								<span style="margin-left: 5%;"><img
									style="width: 15px; height: 15px; cursor: pointer;"
									src="/img/question-icon.png" onclick="" alt="" title=""></span>
							</div>
							<div class="track-holder">
								<input type="checkbox" id="sameArrival" hidden
									onchange="javascript:getCheckBoxVal(this);" />
								<!-- <input type="checkbox" id="sameArrival" hidden checked onchange="javascript:getCheckBoxVal();" /> -->
								<label for="sameArrival"></label>
							</div>
						</div>
					</div>

					<div id="innerLocationDetail">

						<div id="carInfo">
							<div style="padding: 3%;">
								<div
									style="width: 100%; overflow: hidden; border: 1px solid #eaeaea; border-bottom: none;">
									<div style="float: left; width: 50%;">
										<div class="certNum" style="margin-top: 5%; width: 100%;">
											<div style="text-align: right;">
												<div style="text-align: left; margin-left: 2.5%;">
													<span class="d-tbc" style="font-weight: bold;">제조사</span>
												</div>
												<p style="width: 90%; padding: 0%; margin: 0%;" onfocus="">메르세데스
													벤츠</p>
											</div>
										</div>
									</div>
									<div style="float: left; width: 50%;">
										<div class="certNum" style="margin-top: 5%; width: 100%;">
											<div style="text-align: right;">
												<div style="text-align: left; margin-left: 2.5%;">
													<span class="d-tbc" style="font-weight: bold;">차종(모델명)</span>
												</div>
												<p style="width: 90%; padding: 0%; margin: 0%;" onfocus="">S650</p>
											</div>
										</div>
									</div>
								</div>
								<div
									style="width: 100%; overflow: hidden; border: 1px solid #eaeaea; border-top: none; border-bottom: none;">
									<div style="float: left; width: 50%;">
										<div class="certNum" style="margin-top: 5%; width: 100%;">
											<div style="text-align: right;">
												<div style="text-align: left; margin-left: 2.5%;">
													<span class="d-tbc" style="font-weight: bold;">세부차종(트림)</span>
												</div>
												<p style="width: 90%; padding: 0%; margin: 0%;" onfocus="">S650</p>
											</div>
										</div>
									</div>
									<div style="float: left; width: 50%;">
										<div class="certNum" style="margin-top: 5%; width: 100%;">
											<div style="text-align: right;">
												<div style="text-align: left; margin-left: 2.5%;">
													<span class="d-tbc" style="font-weight: bold;">차량번호</span>
												</div>
												<p style="width: 90%; padding: 0%; margin: 0%;" onfocus="">151부9201</p>
											</div>
										</div>
									</div>
								</div>
								<div
									style="width: 100%; overflow: hidden; padding-bottom: 3%; border: 1px solid #eaeaea; border-top: none;">
									<div style="float: left; width: 50%;">
										<div class="certNum" style="margin-top: 5%; width: 100%;">
											<div style="text-align: right;">
												<div style="text-align: left; margin-left: 2.5%;">
													<span style="color: #F00;"></span> <span class="d-tbc"
														style="font-weight: bold;">차대번호</span>
												</div>
												<p style="width: 90%; padding: 0%; margin: 0%;" onfocus="">123456</p>
											</div>
										</div>
									</div>

									<p style="display: none;" onfocus=""></p>

									<div style="float: left; width: 50%;">
										<div class="certNum" style="margin-top: 5%; width: 100%;">
											<div style="text-align: right;">
												<div style="text-align: left; margin-left: 2.5%;">
													<span style="color: #F00;"></span> <span class="d-tbc"
														style="font-weight: bold;">차량구분</span>
												</div>
												<p style="width: 90%; padding: 0%; margin: 0%;" onfocus=""></p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="address">
							<div class="productsinuse"
								style="clear: both; padding: 0 3% 3% 3%;">
								<div id="" class="certNum  product-use-list"
									style="width: 100%; padding: 0;">
									<div class="product-use-item"
										style="text-align: right; padding: 0;">
										<div style="text-align: left; margin-left: 2.5%;">
											<span class="d-tbc" style="font-weight: bold;">출발지</span>
										</div>
										<p
											style="width: 100%; padding-bottom: 3%; padding-right: 8%; margin: 0%;"
											onfocus="">경기도 군포시 산본로 299</p>
										<p style="display: none;"></p>
									</div>
								</div>
							</div>
							<div class="productsinuse"
								style="clear: both; padding: 0 3% 3% 3%;">
								<div id="" class="certNum  product-use-list"
									style="width: 100%; padding: 0;">
									<div class="product-use-item"
										style="text-align: right; padding: 0;">
										<div style="text-align: left; margin-left: 2.5%;">
											<span class="d-tbc" style="font-weight: bold;">도착지</span>
										</div>
										<p
											style="width: 100%; padding-bottom: 3%; padding-right: 8%; margin: 0%;"
											onfocus="">경기도 군포시 산본로 299</p>
										<p style="display: none;"></p>
									</div>
								</div>
							</div>
						</div>
						<div id="departure">
							<div class="productsinuse" style="clear: both;">
								<div id="" class="certNum  product-use-list"
									style="width: 100%; padding: 0;">
									<div class="product-use-item" style="text-align: center;">
										<div style="text-align: left; margin-left: 2.5%;">
											<span style="color: #F00;">*</span> <span class="d-tbc"
												style="font-weight: bold;">출발지</span><span></span>
										</div>
										<input type="text"
											style="width: 75%; padding: 2%; margin-top: 2%;"
											placeholder="출발지" class="d-tbc " id="" name="" value=""
											onfocus=""> <input id="departurePoint" type="hidden"
											value=""> <a onclick="javascript:goShowModal(this);"
											class="product-use-search inlined btn_select"
											style="position: static; margin-top: -5px; line-height: 39px;">검색</a>
									</div>

									<div class="modal-field" style="display: none;">
										<div class="modal-box">
											<div class="modal-table-container">
												<div class="title"
													style="text-align: center; margin-top: 5%;">
													<strong>검색</strong> 목록
												</div>
												<br>
												<div class="rst_wrap">
													<div class="rst mCustomScrollbar">
														<!-- <ul id="searchResult" name="searchResult"> -->
														<ul name="searchResult">
														</ul>
													</div>
												</div>

											</div>
											<div class="confirmation">
												<div class="confirm" style="text-align: center;">
													<input type="button" value="창닫기" name=""
														onclick="javascript:$(this).parent().parent().parent().parent().hide();">
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>
						<div id="arrival">
							<div class="productsinuse" style="clear: both;">
								<div id="" class="certNum  product-use-list"
									style="width: 100%; padding: 0;">
									<div class="product-use-item" style="text-align: center;">
										<div style="text-align: left; margin-left: 2.5%;">
											<span style="color: #F00;">*</span> <span class="d-tbc"
												style="font-weight: bold;">도착지</span>
										</div>
										<input type="text"
											style="width: 75%; padding: 2%; margin-top: 2%;"
											placeholder="도착지" class="d-tbc" id="" name="" value=""
											onfocus=""> <input id="arrivalPoint" type="hidden"
											value=""> <a onclick="javascript:goShowModal(this);"
											class="product-use-search inlined btn_select"
											style="position: static; margin-top: -5px; line-height: 39px;">검색</a>
									</div>
									<div class="modal-field" style="display: none;">
										<div class="modal-box">
											<div class="modal-table-container">
												<div class="title"
													style="text-align: center; margin-top: 5%;">
													<strong>검색</strong> 목록
												</div>
												<br>
												<div class="rst_wrap">
													<div class="rst mCustomScrollbar">
														<!-- <ul id="searchResult" name="searchResult"> -->
														<ul name="searchResult">
														</ul>
													</div>
												</div>

											</div>
											<div class="confirmation">
												<div class="confirm" style="text-align: center;">
													<input type="button" value="창닫기" name=""
														onclick="javascript:$(this).parent().parent().parent().parent().hide();">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>






				</div>





				<div id="confirm" style="display: none;">

					<div id="baseInfo">
						<div style="padding: 3%;">
							<div
								style="width: 100%; overflow: hidden; border: 1px solid #eaeaea; border-bottom: none;">
								<div style="float: left; width: 50%;">
									<div class="certNum" style="margin-top: 5%; width: 100%;">
										<div style="text-align: right;">
											<div style="text-align: left; margin-left: 2.5%;">
												<span class="d-tbc" style="font-weight: bold;">출발일</span>
											</div>
											<p style="width: 90%; padding: 0%; margin: 0%;" onfocus="">2020-12-31</p>
										</div>
									</div>
								</div>
								<div style="float: left; width: 50%;">
									<div class="certNum" style="margin-top: 5%; width: 100%;">
										<div style="text-align: right;">
											<div style="text-align: left; margin-left: 2.5%;">
												<span class="d-tbc" style="font-weight: bold;">출발시간</span>
											</div>
											<p style="width: 90%; padding: 0%; margin: 0%;" onfocus="">17:30</p>
										</div>
									</div>
								</div>
							</div>
							<div
								style="width: 100%; overflow: hidden; border: 1px solid #eaeaea; border-top: none; border-bottom: none;">
								<!-- <div style="float: left; width: 50%;"> -->
								<div class="certNum" style="margin-top: 5%; width: 100%;">
									<div style="text-align: right;">
										<div style="text-align: left; margin-left: 2.5%;">
											<span style="color: #F00;"></span> <span class="d-tbc"
												style="font-weight: bold;">제목</span>
										</div>
										<p style="width: 90%; padding: 0%; margin: 0%;" onfocus="">빠른
											견적 부탁 드립니다.</p>
									</div>
								</div>
								<!-- </div> -->
								<!-- <div style="float: left; width: 50%; display:none;" >
											<div class="certNum" style="margin-top: 5%; width: 100%;">
												<div style="text-align:right;">
													<div style="text-align: left; margin-left: 2.5%;">
													<span style="color: #F00;"></span> <span class="d-tbc" style="font-weight: bold;"></span>
												</div>
													<p style="width: 90%; padding: 0%; margin: 0%;" onfocus=""></p>
												</div>
											</div>
										</div> -->
							</div>
							<div
								style="width: 100%; overflow: hidden; border: 1px solid #eaeaea; border-top: none; border-bottom: none;">
								<div style="float: left; width: 50%;">
									<div class="certNum" style="margin-top: 5%; width: 100%;">
										<div style="text-align: right;">
											<div style="text-align: left; margin-left: 2.5%;">
												<span class="d-tbc" style="font-weight: bold;">탁송방식</span>
											</div>
											<p style="width: 90%; padding: 0%; margin: 0%;" onfocus="">세이프티로더</p>
										</div>
									</div>
								</div>
								<div style="float: left; width: 50%;">
									<div class="certNum" style="margin-top: 5%; width: 100%;">
										<div style="text-align: right;">
											<div style="text-align: left; margin-left: 2.5%;">
												<span class="d-tbc" style="font-weight: bold;">차량대수</span>
											</div>
											<p style="width: 90%; padding: 0%; margin: 0%;" onfocus="">3</p>
										</div>
									</div>
								</div>
							</div>
							<div
								style="width: 100%; overflow: hidden; padding-bottom: 3%; border: 1px solid #eaeaea; border-top: none; border-bottom: none;">
								<div style="float: left; width: 50%;">
									<div class="certNum" style="margin-top: 5%; width: 100%;">
										<div style="text-align: right;">
											<div style="text-align: left; margin-left: 2.5%;">
												<span style="color: #F00;"></span> <span class="d-tbc"
													style="font-weight: bold;">경매방식</span>
											</div>
											<p style="width: 90%; padding: 0%; margin: 0%;" onfocus="">역경매</p>
										</div>
									</div>
								</div>
								<div class="auctionAmount"
									style="float: left; width: 50%; display: none;">
									<div class="certNum" style="margin-top: 5%; width: 100%;">
										<div style="text-align: right;">
											<div style="text-align: left; margin-left: 2.5%;">
												<span style="color: #F00;"></span> <span class="d-tbc"
													style="font-weight: bold;">금액</span>
											</div>
											<p style="width: 90%; padding: 0%; margin: 0%;" onfocus=""></p>
										</div>
									</div>
								</div>
							</div>
							<div
								style="width: 100%; overflow: hidden; padding-bottom: 3%; border: 1px solid #eaeaea; border-top: none;">
								<div style="float: left; width: 50%;">
									<div class="certNum" style="margin-top: 5%; width: 100%;">
										<div style="text-align: right;">
											<div style="text-align: left; margin-left: 2.5%;">
												<span style="color: #F00;"></span> <span class="d-tbc"
													style="font-weight: bold;">낙찰결정방식</span>
											</div>
											<p style="width: 90%; padding: 0%; margin: 0%;" onfocus="">선착순결정방식</p>
										</div>
									</div>
								</div>
								<div style="float: left; width: 50%;">
									<div class="certNum" style="margin-top: 5%; width: 100%;">
										<div style="text-align: right;">
											<div style="text-align: left; margin-left: 2.5%;">
												<span style="color: #F00;"></span> <span class="d-tbc"
													style="font-weight: bold;">낙찰결정시간</span>
											</div>
											<p style="width: 90%; padding: 0%; margin: 0%;" onfocus="">10분</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>









				<div id="request" style="display: none;"></div>

				<form id="insertForm"></form>




			</div>




			<div class="fixed-bottomarea">
				<!-- <a href="javascript:nextCategory(this,'')">다음단계로</a> -->
				<div id="first">
					<div style="width: 100%; display: inline-block;">
						<a href="javascript:nextCategory(this,'')">다음단계로</a>
					</div>
				</div>
				<div id="middle" style="display: none;">
					<div style="width: 50%; display: inline-block;">
						<a href="javascript:prevCategory(this,'')">이전단계로</a>
					</div>
					<div style="width: 50%; display: inline-block;">
						<a href="javascript:nextCategory(this,'')">다음단계로</a>
					</div>
				</div>
				<div id="last" style="display: none;">
					<!-- <div style="width:50%; display:inline-block;"><a href="javascript:prevCategory(this,'')">이전단계로</a></div><div style="width:50%; display:inline-block;"><a href="javascript:insertRequest(this,'')">경매요청</a></div> -->
					<div style="width: 50%; display: inline-block;">
						<a href="javascript:prevCategory(this,'')">이전단계로</a>
					</div>
					<div style="width: 50%; display: inline-block;">
						<a href="javascript:validation(this,'')">수정완료</a>
					</div>
				</div>
			</div>

		</div>
	</div>




	<script src="/js/vendor/jquery-1.11.2.min.js"></script>
	<!-- <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script> -->
	<script src="/js/vendor/bootstrap.min.js"></script>
	<!-- veiwport for countnumber -->
	<script src="/js/jquery.viewportchecker.js"></script>
	<script src="/js/main.js"></script>
	<script src="/js/alert.js"></script>
	<script src="/js/vendor/jquery.form.min.js"></script>
	<script
		src="https://apis.openapi.sk.com/tmap/jsv2?version=1&appKey=${appKey}"></script>
	<script src="/js/vendor/jquery.loading-indicator.min.js"></script>
	<script>

    $(document).ready(function(){
    
    	homeLoader = $('body').loadingIndicator({
			useImage: false,
			showOnInit : false
		}).data("loadingIndicator");
	
    	//homeLoader.show();
    	//homeLoader.hide();
    	var scroll_top=$(document).scrollTop();
    	$(".loading-indicator-wrapper").css("top",scroll_top);
    	$(".loading-indicator-wrapper").css("height","100%");
    	
    });

    

    

    </script>
</body>
</html>
