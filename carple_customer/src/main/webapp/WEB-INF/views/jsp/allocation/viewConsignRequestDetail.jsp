<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!doctype html>
<html lang="ko">
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>

<body class="" style="overscroll-behavior: contain;">

	<script type="text/javascript">  

    var homeLoader;
$(document).ready(function(){


});

$(window).on("scroll", function(){ 
	var scroll_top=$(this).scrollTop();
	$(".loading-indicator-wrapper").css("top",scroll_top);
	$(".loading-indicator-wrapper").css("height","100%");
});
    
    function selectInterestItem(itemCd,marketCd,obj){
    	
    	$.ajax({ 
    		type: 'post' ,
    		url : "/interest/selectInterestItem.do" ,
    		dataType : 'json' ,
    		data : {
    			itemCd : itemCd,
    			marketCd : marketCd
    		},
    		success : function(data, textStatus, jqXHR)
    		{
    			if(data.resultCode == "0000"){
    				insertInterestItem(itemCd,marketCd,obj);
    			}else{
    				deleteInterestItem(itemCd,marketCd,obj);
    			}
    		} ,
    		error : function(xhRequest, ErrorText, thrownError) {
    		}
    	}); 
    	
    	
    }    
        
    function insertInterestItem(itemCd,marketCd,obj){
    	
    	$.ajax({ 
    		type: 'post' ,
    		url : "/interest/insertInterestItem.do" ,
    		dataType : 'json' ,
    		data : {
    			itemSrtCd : itemCd,
    			marketCd : marketCd
    		},
    		success : function(data, textStatus, jqXHR)
    		{
    			if(data.resultCode == "0000"){
    				$(obj).addClass('heart-full');
    			}else if(data.resultCode == "E002"){
    					alert("로그인 되지 않음");
    			}
    		} ,
    		error : function(xhRequest, ErrorText, thrownError) {
    		}
    	}); 
    }    
        
      

    function deleteInterestItem(itemCd,marketCd,obj){
    	
    	$.ajax({ 
    		type: 'post' ,
    		url : "/interest/deleteInterestItem.do" ,
    		dataType : 'json' ,
    		data : {
    			itemSrtCd : itemCd,
    			marketCd : marketCd
    		},
    		success : function(data, textStatus, jqXHR)
    		{
    			if(data.resultCode == "0000"){
    				$(obj).removeClass('heart-full');
    			}else if(data.resultCode == "E002"){
    					alert("로그인 되지 않음");
    			}
    		} ,
    		error : function(xhRequest, ErrorText, thrownError) {
    		}
    	}); 
    }    


    function prevCategory(obj,category){
        
    	var tabIdx = 0;
		$("#category").children().each(function(index,element){
			if($(this).attr("class") == "active"){
				//$(this).next().trigger("click");
				tabIdx = index-1;
			} 
		});

		var categoryId = "";

		if(currentStatus == "request"){
    		categoryId = "confirm";
			$("#first").css("display","none");
			$("#middle").css("display","");
			$("#last").css("display","none");
        }else if(currentStatus == "confirm"){
    		categoryId = "location";
    		$("#first").css("display","none");
			$("#middle").css("display","");
			$("#last").css("display","none");
        }else if(currentStatus == "location"){
    		categoryId = "car";
    		$("#first").css("display","none");
			$("#middle").css("display","");
			$("#last").css("display","none");
        }else if(currentStatus == "car"){
    		categoryId = "base";
    		$("#first").css("display","");
			$("#middle").css("display","none");
			$("#last").css("display","none");
        }

    	changeCategory($("#category").children().eq(tabIdx),categoryId);
		
    }


    
 	function nextCategory(obj,category){

		var tabIdx = 0;
		$("#category").children().each(function(index,element){
			if($(this).attr("class") == "active"){
				//$(this).next().trigger("click");
				tabIdx = index+1;
			} 
		});
	
		//$.alert(currentStatus);
		
    	var categoryId = "";
    	if(currentStatus == "base"){
    		categoryId = "car";
			$("#first").css("display","none");
			$("#middle").css("display","");
			$("#last").css("display","none");
        }else if(currentStatus == "car"){
    		categoryId = "location";
    		$("#first").css("display","none");
			$("#middle").css("display","");
			$("#last").css("display","none");
        }else if(currentStatus == "location"){
    		categoryId = "confirm";
    		$("#first").css("display","none");
			$("#middle").css("display","none");
			$("#last").css("display","");
        }else if(currentStatus == "confirm"){
    		categoryId = "request";
    		$("#first").css("display","none");
			$("#middle").css("display","none");
			$("#last").css("display","");
        }  
    	changeCategory($("#category").children().eq(tabIdx),categoryId);
 	 }


function validation(obj){

	var contentLen = $("#confirm").find("p").length;
	var contentArr = new Array(contentLen);
	
	$("#confirm").find("p").each(function(index,element){
		contentArr[index] = $.trim($(this).html())+"";					
	});

	if(contentArr[2] == ""){
		$.alert("제목이 작성되지 않았습니다.",function(a){
			var categoryId = "base";
    		$("#first").css("display","");
			$("#middle").css("display","none");
			$("#last").css("display","none");
			changeCategory($("#category").children().eq(0),categoryId);
		});
		return false;
	}
	
	var carCount = Number(contentArr[4]);

	for(var v = 0; v < carCount; v++){
		var gap = Number(7);

		if(contentArr[(v*10)+gap] == ""){
			$.alert((v+1)+"번째 차량의 제조사가 작성되지 않았습니다.",function(a){
				var categoryId = "car";
				$("#first").css("display","none");
				$("#middle").css("display","");
				$("#last").css("display","none");
				changeCategory($("#category").children().eq(1),categoryId);
			});
			return false;
		}else if(contentArr[(v*10)+1+gap] == ""){
			$.alert((v+1)+"번째 차량의 차종(모델명)이(가) 작성되지 않았습니다.",function(a){
				var categoryId = "car";
				$("#first").css("display","none");
				$("#middle").css("display","");
				$("#last").css("display","none");
				changeCategory($("#category").children().eq(1),categoryId);
			});
			return false;
			
		}else if(contentArr[(v*10)+2+gap] == ""){
			$.alert((v+1)+"번째 차량의 세부차종(트림)이(가) 작성되지 않았습니다.",function(a){
				var categoryId = "car";
				$("#first").css("display","none");
				$("#middle").css("display","");
				$("#last").css("display","none");
				changeCategory($("#category").children().eq(1),categoryId);
			});
			return false;
			
		}else if(contentArr[(v*10)+3+gap] == ""){
			$.alert((v+1)+"번째 차량의 차량번호이(가) 작성되지 않았습니다.",function(a){
				var categoryId = "car";
				$("#first").css("display","none");
				$("#middle").css("display","");
				$("#last").css("display","none");
				changeCategory($("#category").children().eq(1),categoryId);
			});
			return false;
			
		}else if(contentArr[(v*10)+4+gap] == ""){
			//차대번호
			
		}else if(contentArr[(v*10)+6+gap] == ""){
			$.alert((v+1)+"번째 차량의 출발지이(가) 작성되지 않았습니다.",function(a){
				var categoryId = "location";
	    		$("#first").css("display","none");
				$("#middle").css("display","");
				$("#last").css("display","none");
				changeCategory($("#category").children().eq(2),categoryId);
			});
			return false;
			
		}else if(contentArr[(v*10)+7+gap] == ""){
			$.alert((v+1)+"번째 차량의 출발지이(가) 검색되지 않았습니다. 주소를 검색 해 주세요.",function(a){
				var categoryId = "location";
	    		$("#first").css("display","none");
				$("#middle").css("display","");
				$("#last").css("display","none");
				changeCategory($("#category").children().eq(2),categoryId);
			});
			return false;
			
		}else if(contentArr[(v*10)+8+gap] == ""){
			$.alert((v+1)+"번째 차량의 도착지이(가) 작성되지 않았습니다.",function(a){
				var categoryId = "location";
	    		$("#first").css("display","none");
				$("#middle").css("display","");
				$("#last").css("display","none");
				changeCategory($("#category").children().eq(2),categoryId);
			});
			return false;
			
		}else if(contentArr[(v*10)+9+gap] == ""){
			$.alert((v+1)+"번째 차량의 도착지이(가) 검색되지 않았습니다. 주소를 검색 해 주세요.",function(a){
				var categoryId = "location";
	    		$("#first").css("display","none");
				$("#middle").css("display","");
				$("#last").css("display","none");
				changeCategory($("#category").children().eq(2),categoryId);
			});
			return false;
			
		}

	}

	$.confirm("이 내용으로 경매요청을 진행 하시겠습니까?",function(a){
			if(a){
				insertRequest(obj,contentArr)
			}
	});
}



 	
function insertRequest(obj,contentArr){


		$.ajax({ 
			type: 'post' ,
			url : "/allocation/insert-allocation.do" ,
			dataType : 'json' ,
			traditional : true,
			data : {
				contentArr : contentArr
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					$.alert("견적 요청이 정상적으로 등록 되었습니다.",function(a){
						document.location.href = "/allocation/main.do";
					});
				}else if(result == "E001"){
					$.alert("알 수없는 오류입니다. 카카오톡 플러스채팅으로 문의 주세요",function(a){
						return;	
					});
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});

 	 			
}
 		


	    
    
var isChange = false;

   function changeCategory(obj,categoryId){

    	var inputCount = $("#car").children().length;
		$("#category").children().each(function(index,element){
			$(this).removeClass("active");
		});
		$(obj).addClass("active");

			if(categoryId != ""){
				
				$("#total").children().each(function(index,element){
					$(this).css("display","none");
				})
				
				if(categoryId == "car"){

					var tmp = carCount;

			    	if($("#carCount").val() == 0 && carCount == 0){
			    		$.alert("차량대수가 입력 되지 않았습니다.",function(a){
						});
			    		$("#"+categoryId).css("display","");
			    		isChange = false;
						return;
						
			    	}else if($("#carCount").val() != 0 && carCount == 0){
			        	if($("#carCount").val() < 1){
			        		$("#carCount").val(1)
			            }else if($("#carCount").val() > 10){
			        		$("#carCount").val(10)
			            }
			    		carCount = $("#carCount").val();

			    		$("#car").html("");
						for(var i = 0; i < carCount; i++){
							$("#car").append(contents);
						}
						$("#"+categoryId).css("display","");
						isChange = true;
			        }else if(carCount != 0 && carCount != $("#carCount").val()){
						//기존 정보가 변경 되는경우
			        	carCount = $("#carCount").val();
			        	$.confirm("차량대수가 변경 되었습니다. 적용 하시겠습니까?",function(a){
							if(a){
								isChange = true;
								var carLen = $("#car").children().length;
								if(carLen < carCount){
									//기존의 차량대수보다 많아지는경우 많아진만큼 추가 한다.
									for(var i = 0; i < carCount-carLen; i++){
										$("#car").append(contents);
									}
								}else if(carLen > carCount){
									//기존의 차량대수보다 적어지는 경우
									$("#car").children().each(function(index,element){
										if(index > carCount-1){
											$(this).remove();
										}
									});
								}else{
									//그럴리는 없겠지만...
									$("#car").html("");
									for(var i = 0; i < carCount; i++){
										$("#car").append(contents);
									}
								}
								
								$("#"+categoryId).css("display","");
							}else{
								isChange = false;
								$("#carCount").val(tmp);
								carCount = tmp;
								$("#"+categoryId).css("display","");
							}
						});

			        	
			        }else if(carCount != 0 && carCount == $("#carCount").val()){
			        	isChange = false;
			        	$("#"+categoryId).css("display","");
				    }

				}else if(categoryId != "car"){

					if(categoryId == "location"){

						//$.alert(carCount);
						if(Number(carCount) == 0){

							$.alert("차량정보가 입력 되지 않았습니다.",function(a){
							});
							$("#category").children().each(function(index,element){
								$(this).removeClass("active");
							});
							$("#category").children().first().addClass("active");
							$("#base").css("display","");
							return false;
						}else{

							//$("#innerLocationDetail").html("");
							
							if(Number($("#carCount").val()) <= 1){
								$("#locationSetting").css("display","none");
							}else{
								$("#locationSetting").css("display","");
							}
							$("#"+categoryId).css("display","");

							var carInfoArr = new Array(inputCount); 

							$("#car").children().each(function(index,element){
								var setInfo = $(carInfo);
								$(this).find("input").each(function(innerindex,innerelement){
									$(setInfo).find("P").eq(innerindex).html($(this).val());
								});
								$(setInfo).find("P").last().html($(this).find("select").eq(0).val()=="OC"?"중고차":"신차");
								carInfoArr[index] = $(setInfo);
								//$.alert($(carInfoArr[index]).html());
							});

							/*
							for(var i = 0; i < Number(inputCount); i++){
								$("#innerLocationDetail").append(carInfoArr[i]);
							}
							$("#innerLocationDetail").append(departure);
							$("#innerLocationDetail").append(arrival);
							*/
								
						}

						if(currentStatus == "car"){
							getCheckBoxVal("");
						}
						
					}else if(categoryId == "confirm"){

						$("#confirm").html("");
						var baseInfoArr = new Array(7);
						baseInfoArr[0] = $("#base").find("input").eq(0).val();
						baseInfoArr[1] = $("#base").find("input").eq(1).val();
						baseInfoArr[2] = $("#base").find("input").eq(2).val();
						baseInfoArr[3] = "";
						if($("#base").find("select").eq(0).val() == "SL"){
							baseInfoArr[3] = "세이프티로더";
						}else if($("#base").find("select").eq(0).val() == "CA"){
							baseInfoArr[3] = "캐리어";
						}else if($("#base").find("select").eq(0).val() == "RD"){
							baseInfoArr[3] = "로드탁송";
						}else if($("#base").find("select").eq(0).val() == "TR"){
							baseInfoArr[3] = "추레라";
						}else if($("#base").find("select").eq(0).val() == "FC"){
							baseInfoArr[3] = "풀카";
						}else if($("#base").find("select").eq(0).val() == "BC"){
							baseInfoArr[3] = "박스카";
						}else if($("#base").find("select").eq(0).val() == "NE"){
							baseInfoArr[3] = "상관없음";
						}
						
						baseInfoArr[4] = carCount;
						baseInfoArr[5] = "";
						if($("#base").find("select").eq(1).val() == "PR"){
							baseInfoArr[5] = "우선순위결정";
						}else if($("#base").find("select").eq(1).val() == "OA"){
							baseInfoArr[5] = "선착순결정";
						}
						baseInfoArr[6] = $("#base").find("select").eq(2).val()+"분";

						var setBaseInfo = $(baseInfo);
						
						$(setBaseInfo).find("p").each(function(index,element){
							$(this).html(baseInfoArr[index]);
						});
						var departureIsChecked = $("#sameDeparture").is(':checked');
						var arrivalIsChecked = $("#sameArrival").is(':checked');
						$("#confirm").append($(setBaseInfo));
						
						//차량 관련
						var inputCount = $("#car").children().length;
						var carInfoArr = new Array(inputCount); 
						$("#car").children().each(function(index,element){
							var setInfo = $(carInfo);
							$(this).find("input").each(function(innerindex,innerelement){
								$(setInfo).find("P").eq(innerindex).html($(this).val())
							});
							$(setInfo).find("P").last().html($(this).find("select").eq(0).val()=="OC"?"중고차":"신차");
							carInfoArr[index] = $(setInfo);
						});

						//주소 관련						
						var addressCount = $("#innerLocationDetail").find(".product-use-item").length;
						addressInfoArr = new Array(addressCount);
						var innerDetail = $("#innerLocationDetail").clone();
						if(Number(addressCount) > 0){
							$(innerDetail).find(".product-use-item").each(function(index,element){
								addressInfoArr[index] = new Array(2);
								$(this).find("input").each(function(innerindex,innerelement){
									addressInfoArr[index][innerindex] = $(this).val();
								});
							});
						}
						
						if(departureIsChecked && arrivalIsChecked){
							//출발지와 도착지가 같은경우
							for(var i = 0; i < Number(inputCount); i++){
								$("#confirm").append(carInfoArr[i]);
								$("#confirm").append($(address));
							}
							$("#confirm").find(".product-use-item").each(function(index,element){
								$(this).find("p").each(function(innerindex,innerelement){
									$(this).html(addressInfoArr[index%2][innerindex]);
								});
							});
														
						}else if(departureIsChecked && !arrivalIsChecked){
							//출발지가 같고 도착지가 다른경우
							for(var i = 0; i < Number(inputCount); i++){
								$("#confirm").append(carInfoArr[i]);
								$("#confirm").append($(address));
							}
							var count = 0;
							$("#confirm").find(".product-use-item").each(function(index,element){							
								$(this).find("p").each(function(innerindex,innerelement){
									if(index%2 == 0){
										$(this).html(addressInfoArr[0][innerindex]);
									}else{
										$(this).html(addressInfoArr[index-count][innerindex]);
									}
								});
								if(index%2 != 0){
									count++;
								}
							});
						
						}else if(!departureIsChecked && arrivalIsChecked){
							//출발지가 다르고 도착지가 같은경우
							for(var i = 0; i < Number(inputCount); i++){
								$("#confirm").append(carInfoArr[i]);
								$("#confirm").append($(address));
							}
							var count = 0;
							$("#confirm").find(".product-use-item").each(function(index,element){							
								$(this).find("p").each(function(innerindex,innerelement){
									if(index%2 == 0){
										$(this).html(addressInfoArr[index-count][innerindex]);
									}else{
										$(this).html(addressInfoArr[addressInfoArr.length-1][innerindex]);
									}
								});
								if(index%2 == 0){
									count++;
								}
							});
						
						}else if(!departureIsChecked && !arrivalIsChecked){
							//출발지와 도착지가 다른경우
							for(var i = 0; i < Number(inputCount); i++){
								$("#confirm").append(carInfoArr[i]);
								$("#confirm").append($(address));
							}
							$("#confirm").find(".product-use-item").each(function(index,element){
								$(this).find("p").each(function(innerindex,innerelement){
									$(this).html(addressInfoArr[index][innerindex]);
								});
							});
						} 
						$("#"+categoryId).css("display","");
					}else{
						$("#"+categoryId).css("display","");
					}
				}
							
				
			}else{

			}
			

	currentStatus = categoryId;
			
   }
    
    

function showMessage(obj){


	//alert($(obj).next().val());

	
	if($(obj).next().val() == "PR" || $(obj).next().val() == "OA"){

		if($(obj).next().val() == "PR"){
			$.alert("우선순위 결정 : 입찰 후 낙찰 결정시 1순위~5순위까지 결정 하고 1순위부터 차례로 낙찰 확정기회를 부여 하여 해당 차례에 낙찰 확정 하는 입찰 건에 최종 낙찰되는 경매 방식 입니다.",function(a){
			});
		}else if($(obj).next().val() == "OA"){
			$.alert("선착순 결정 : 입찰 후 낙찰 결정시 1~5개의 입찰 건에 낙찰 확정 기회를 부여 하고 가장 먼저 낙찰 확정 하는 입찰 건에 최종 낙찰되는 경매 방식 입니다.",function(a){
			});
		}

	}else if($(obj).next().find("input").attr("id") == "sameDeparture"){

		$.alert("탁송하는 모든 차량의 출발지가 동일한경우에 사용 합니다. 출발지가 다른경우 각각 입력 할 수 있습니다.",function(a){
		});

	}else if($(obj).next().find("input").attr("id") == "sameArrival"){

		$.alert("탁송하는 모든 차량의 도착지가 동일한경우에 사용 합니다. 도착지가 다른경우 각각 입력 할 수 있습니다.",function(a){
		});

	}else{

		$.alert("우선순위 결정의 경우 후 순위 입찰에 기회가 부여 되는 시간이고 선착순 결정의 경우 낙찰 확정기회가 부여 되는 시간을 의미합니다.",function(a){

		});
		
	}
	
}



function getCheckBoxVal(obj){


	var addressCount = $("#innerLocationDetail").find(".product-use-item").length;
	var innerDetail = $("#innerLocationDetail").clone();

	var isInput = false;

	
	if(Number(addressCount) > 0){
		addressInfoArr = new Array(addressCount);			
		$(innerDetail).find(".product-use-item").each(function(index,element){
			addressInfoArr[index] = new Array(2);
			$(this).find("input").each(function(innerindex,innerelement){
				addressInfoArr[index][innerindex] = $(this).val();
				if($(this).val() != ""){
					isInput = true;
				}
			});
		});
	}

	if(obj != ""){

		if(isInput){
			$.confirm("주소 정보가 초기화 됩니다. 적용 하시겠습니까?",function(a){
				if(a){

					$("#innerLocationDetail").html("");
					
					var inputCount = $("#car").children().length;
					var carInfoArr = new Array(inputCount); 

					$("#car").children().each(function(index,element){
						var setInfo = $(carInfo);
						$(this).find("input").each(function(innerindex,innerelement){
							$(setInfo).find("P").eq(innerindex).html($(this).val())
						});
						//$(this).find("select").each(function(innerindex,innerelement){
						//	$(setInfo).find("P").last().html($(this).val())
						//});
						$(setInfo).find("P").last().html($(this).find("select").eq(0).val()=="OC"?"중고차":"신차");
						carInfoArr[index] = $(setInfo);
						//$.alert($(carInfoArr[index]).html());
					});
					

						var departureIsChecked = $("#sameDeparture").is(':checked');
						var arrivalIsChecked = $("#sameArrival").is(':checked');

						if(departureIsChecked && arrivalIsChecked){
						//상차지와 하차지가 같은경우
							//$.alert(0);
							for(var i = 0; i < Number(inputCount); i++){
								$("#innerLocationDetail").append(carInfoArr[i]);
							}
							$("#innerLocationDetail").append(departure);
							$("#innerLocationDetail").append(arrival);
						}else if(departureIsChecked && !arrivalIsChecked){
						//상차지는 같고 하차지가 다른경우
							//$.alert(1);
							$("#innerLocationDetail").append(departure);
							for(var i = 0; i < Number(inputCount); i++){
								$("#innerLocationDetail").append(carInfoArr[i]);
								$("#innerLocationDetail").append(arrival);
							}
						}else if(!departureIsChecked && arrivalIsChecked){
						//상차지가 다르고 하차지가 같은경우
							//$.alert(2);
							for(var i = 0; i < Number(inputCount); i++){
								$("#innerLocationDetail").append(carInfoArr[i]);
								$("#innerLocationDetail").append(departure);
							}
							$("#innerLocationDetail").append(arrival);
						}else if(!departureIsChecked && !arrivalIsChecked){
						//상차지도 다르고 하차지도 다른경우
							//$.alert(3);
							for(var i = 0; i < Number(inputCount); i++){
								$("#innerLocationDetail").append(carInfoArr[i]);
								$("#innerLocationDetail").append(departure);
								$("#innerLocationDetail").append(arrival);
							}
						}


						if(obj == ""){
							//생성이 끝났으면 입력 되어 잇던 주소를 입력 한다.		
							if(Number(addressCount) > 0){
								$("#innerLocationDetail").find(".product-use-item").each(function(index,element){
									$(this).find("input").each(function(innerindex,innerelement){
										$(this).val(addressInfoArr[index][innerindex]);
									});
								});
							}
						}


							
				}else{

					var isChecked = $(obj).is(':checked');
					
					if(isChecked){
						$(obj).prop("checked",false);
					}else{
						$(obj).prop("checked",true);
					}
				}
			});

		}else{

			$("#innerLocationDetail").html("");
			
			var inputCount = $("#car").children().length;
			var carInfoArr = new Array(inputCount); 

			$("#car").children().each(function(index,element){
				var setInfo = $(carInfo);
				$(this).find("input").each(function(innerindex,innerelement){
					$(setInfo).find("P").eq(innerindex).html($(this).val())
				});
				$(setInfo).find("P").last().html($(this).find("select").eq(0).val()=="OC"?"중고차":"신차");
				carInfoArr[index] = $(setInfo);
				//$.alert($(carInfoArr[index]).html());
			});
			

				var departureIsChecked = $("#sameDeparture").is(':checked');
				var arrivalIsChecked = $("#sameArrival").is(':checked');

				if(departureIsChecked && arrivalIsChecked){
				//상차지와 하차지가 같은경우
					//$.alert(0);
					for(var i = 0; i < Number(inputCount); i++){
						$("#innerLocationDetail").append(carInfoArr[i]);
					}
					$("#innerLocationDetail").append(departure);
					$("#innerLocationDetail").append(arrival);
				}else if(departureIsChecked && !arrivalIsChecked){
				//상차지는 같고 하차지가 다른경우
					//$.alert(1);
					$("#innerLocationDetail").append(departure);
					for(var i = 0; i < Number(inputCount); i++){
						$("#innerLocationDetail").append(carInfoArr[i]);
						$("#innerLocationDetail").append(arrival);
					}
				}else if(!departureIsChecked && arrivalIsChecked){
				//상차지가 다르고 하차지가 같은경우
					//$.alert(2);
					for(var i = 0; i < Number(inputCount); i++){
						$("#innerLocationDetail").append(carInfoArr[i]);
						$("#innerLocationDetail").append(departure);
					}
					$("#innerLocationDetail").append(arrival);
				}else if(!departureIsChecked && !arrivalIsChecked){
				//상차지도 다르고 하차지도 다른경우
					//$.alert(3);
					for(var i = 0; i < Number(inputCount); i++){
						$("#innerLocationDetail").append(carInfoArr[i]);
						$("#innerLocationDetail").append(departure);
						$("#innerLocationDetail").append(arrival);
					}
				}


		}
				
	}else{

	
		$("#innerLocationDetail").html("");
		var inputCount = $("#car").children().length;
		var carInfoArr = new Array(inputCount); 

		$("#car").children().each(function(index,element){
			var setInfo = $(carInfo);
			$(this).find("input").each(function(innerindex,innerelement){
				$(setInfo).find("P").eq(innerindex).html($(this).val())
			});
			$(setInfo).find("P").last().html($(this).find("select").eq(0).val()=="OC"?"중고차":"신차");
			carInfoArr[index] = $(setInfo);
			//$.alert($(carInfoArr[index]).html());
		});
		

			var departureIsChecked = $("#sameDeparture").is(':checked');
			var arrivalIsChecked = $("#sameArrival").is(':checked');

			if(departureIsChecked && arrivalIsChecked){
			//상차지와 하차지가 같은경우
				//$.alert(0);
				for(var i = 0; i < Number(inputCount); i++){
					$("#innerLocationDetail").append(carInfoArr[i]);
				}
				$("#innerLocationDetail").append(departure);
				$("#innerLocationDetail").append(arrival);
			}else if(departureIsChecked && !arrivalIsChecked){
			//상차지는 같고 하차지가 다른경우
				//$.alert(1);
				$("#innerLocationDetail").append(departure);
				for(var i = 0; i < Number(inputCount); i++){
					$("#innerLocationDetail").append(carInfoArr[i]);
					$("#innerLocationDetail").append(arrival);
				}
			}else if(!departureIsChecked && arrivalIsChecked){
			//상차지가 다르고 하차지가 같은경우
				//$.alert(2);
				for(var i = 0; i < Number(inputCount); i++){
					$("#innerLocationDetail").append(carInfoArr[i]);
					$("#innerLocationDetail").append(departure);
				}
				$("#innerLocationDetail").append(arrival);
			}else if(!departureIsChecked && !arrivalIsChecked){
			//상차지도 다르고 하차지도 다른경우
				//$.alert(3);
				for(var i = 0; i < Number(inputCount); i++){
					$("#innerLocationDetail").append(carInfoArr[i]);
					$("#innerLocationDetail").append(departure);
					$("#innerLocationDetail").append(arrival);
				}
			}

			if(!isChange){
				if(Number(addressCount) > 0){
					$("#innerLocationDetail").find(".product-use-item").each(function(index,element){
						$(this).find("input").each(function(innerindex,innerelement){
							$(this).val(addressInfoArr[index][innerindex]);
						});
					});
				}
			}

			
	}

	//공통으로 설정 되는 부분은 경매진행 하는 차량이 2대 이상인 경우에만...
	if(carCount > 1){
		if(departureIsChecked && arrivalIsChecked){
			$(".product-use-item").find("span").each(function(index,element){
				if($(this).html() == "출발지" || $(this).html() == "도착지"){
					$(this).html("공통"+$(this).html()+"(동일한 "+$(this).html()+"로 설정 됩니다.)");
				} 
			});
		}else if(departureIsChecked && !arrivalIsChecked){
			$(".product-use-item").find("span").each(function(index,element){
				if($(this).html() == "출발지"){
					$(this).html("공통"+$(this).html()+"(동일한 "+$(this).html()+"로 설정 됩니다.)");
				} 
			});
		}else if(!departureIsChecked && arrivalIsChecked){
			$(".product-use-item").find("span").each(function(index,element){
				if($(this).html() == "도착지"){
					$(this).html("공통"+$(this).html()+"(동일한 "+$(this).html()+"로 설정 됩니다.)");
				} 
			});
		}
	}
	
}
    


var map, marker;
var markerArr = [];
var selectedObj;

function goShowModal(obj){


	//return false;

	
	if($(obj).prev().prev().val() == ""){

		$.alert("주소가 입력 되지 않았습니다.",function(a){
		
		});
		return false;
	}else{

		selectedObj = $(obj);
		
		$(selectedObj).parent().next().show();
		$(selectedObj).parent().next().find("ul").css("display","block");
		
		$.ajax({
			method:"GET",
			url:"https://apis.openapi.sk.com/tmap/pois?version=1&format=json&callback=result",
			async:false,
			data:{
				"appKey" : "${appKey}",
				"searchKeyword" : $(obj).prev().prev().val(),
				"resCoordType" : "EPSG3857",
				"reqCoordType" : "WGS84GEO",
				"count" : 10
			},
			success:function(response){

				
				var resultpoisData = response.searchPoiInfo.pois.poi;
				
				// 기존 마커, 팝업 제거
				if(markerArr.length > 0){
					for(var i in markerArr){
						markerArr[i].setMap(null);
					}
				}
				var innerHtml ="";	// Search Reulsts 결과값 노출 위한 변수
				var positionBounds = new Tmapv2.LatLngBounds();		//맵에 결과물 확인 하기 위한 LatLngBounds객체 생성
				
				for(var k in resultpoisData){
					
					var noorLat = Number(resultpoisData[k].noorLat);
					var noorLon = Number(resultpoisData[k].noorLon);
					var name = resultpoisData[k].name;

					var upperAddrName = resultpoisData[k].upperAddrName;
					var middleAddrName = resultpoisData[k].middleAddrName;
					var lowerAddrName = resultpoisData[k].lowerAddrName;
					var detailAddrName = resultpoisData[k].detailAddrName;
					var firstNo = resultpoisData[k].firstNo;
					var secondNo = resultpoisData[k].secondNo == "" ? "" : "-"+resultpoisData[k].secondNo;

					var pointCng = new Tmapv2.Point(noorLon, noorLat);
					var projectionCng = new Tmapv2.Projection.convertEPSG3857ToWGS84GEO(pointCng);
					
					var lat = projectionCng._lat;
					var lon = projectionCng._lng;
					
					var markerPosition = new Tmapv2.LatLng(lat, lon);
					
					marker = new Tmapv2.Marker({
				 		position : markerPosition,
				 		icon : "http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_" + k + ".png",
						iconSize : new Tmapv2.Size(24, 38),
						title : name,
				 	});
					
					innerHtml += '<li  onclick="javascript:inputValue(\''+name+" ("+upperAddrName+" "+middleAddrName+" "+lowerAddrName+" "+detailAddrName+" "+firstNo+secondNo+")"+'\',\''+lon+'\',\''+lat+'\')">';
					innerHtml += '<img src="http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_' + k + '.png" style="vertical-align:middle;"/>';
					innerHtml += '<span>'+name+'</span>';
					innerHtml += '<span style="display:block; color:#3f3f3f;" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;('+upperAddrName+" "+middleAddrName+" "+lowerAddrName+" "+detailAddrName+" "+firstNo+secondNo+')</span>';
					innerHtml += '</li>';

					markerArr.push(marker);
					positionBounds.extend(markerPosition);	// LatLngBounds의 객체 확장
				}
				
				$(selectedObj).parent().next().find("ul").html(innerHtml);
				
			},
			error:function(request,status,error){
				console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
			}
		});
		
	}
	
}

	function inputValue(address,px,py){
		
		$(selectedObj).prev().prev().val(address);
		$(selectedObj).parent().next().find("ul").css("display","none");
		$(selectedObj).prev().val(px+","+py);
		$(selectedObj).parent().next().css('display','none');
	    			
	}

	function prevPageConfirm(){
		
		homeLoader.show();
		//document.location.href = "/allocation/main.do";
		history.go(-1);
		
	}


	function removeCarInfo(obj){

		if($("#car").children().length > 1){

			$.confirm("이 차량의 정보를 삭제 하시겠습니까?",function(a){
				if(a){
					$(obj).parent().parent().parent().parent().parent().remove();
					$("#carCount").val($("#carCount").val()-1);
					setCarCategory($("#category").children().eq(1),"car");
					$.alert("삭제되었습니다.",function(a){
					});		
				}else{
					$.alert("삭제가 취소 되었습니다.",function(a){
					});
				}
			});
		}else{
			$.alert("차량정보를 삭제 할 수 없습니다.",function(a){
				
			});

		}

	}


	  function setCarCategory(obj,categoryId){

	    	var inputCount = $("#car").children().length;
			$("#category").children().each(function(index,element){
				$(this).removeClass("active");
			});
			$(obj).addClass("active");

        	carCount = $("#carCount").val();
       	
			isChange = true;
			var carLen = $("#car").children().length;
			if(carLen < carCount){
				//기존의 차량대수보다 많아지는경우 많아진만큼 추가 한다.
				for(var i = 0; i < carCount-carLen; i++){
					$("#car").append(contents);
				}
			}else if(carLen > carCount){
				//기존의 차량대수보다 적어지는 경우
				$("#car").children().each(function(index,element){
					if(index > carCount-1){
						$(this).remove();
					}
				});
			}else{
				
			}
			
			$("#"+categoryId).css("display","");
		
		currentStatus = categoryId;
				
	   }


	  function bidAccept(corId,crbId,status){



			$.confirm("해당 입찰로 낙찰 결정 하시겠습니까?(이 동작은 취소 할 수 없습니다.)",function(a){
				
				if(a){
					homeLoader.show();
					$.ajax({ 
			    		type: 'post' ,
			    		url : "/bid/update-bid-status.do" ,
			    		dataType : 'json' ,
			    		data : {
			    			corId : corId,
			    			crbId : crbId,
			    			crbStatus : status
			    		},
			    		success : function(data, textStatus, jqXHR)
			    		{
			    			if(data.resultCode == "0000"){
			    				homeLoader.hide();
			    				$.alert("낙찰 되었습니다.",function(a){
			    					window.location.reload();
				    			});
			    			}else if(data.resultCode == "E010" || data.resultCode == "E011"){
			    				homeLoader.hide();
			    				$.alert(data.resultMsg,function(a){
			    					window.location.reload();
				    			});
			    			}else{
			    				homeLoader.hide();
			    				$.alert("낙찰  하는데 실패 했습니다. 관리자에게 문의 하세요.",function(a){
			    					window.location.reload();
				    			});
			    				
			    			}
			    		} ,
			    		error : function(xhRequest, ErrorText, thrownError) {
			    		}
			    	}); 
					
				}
			});

			


			
		}






function goDriverInfoPage(crbId){

	$.confirm("낙찰 기사님의 정보를 확인 하시겠습니까?",function(a){
		if(a){
			homeLoader.show();
			document.location.href = "/allocation/driver.do?crbId="+crbId;
		}
	});
	
}


function backKeyController(str){
	
	//homeLoader.show();
	//document.location.href="/carrier/group-list.do?allocationId=${paramMap.groupId}&allocationStatus=Y";
	prevPageConfirm()
	
}
	




function updateCorStatus(obj, corId,corStatus){

	$.confirm("해당 경매건을 취소 처리 하시겠습니까?",function(a){
	
		if(a){
			homeLoader.show();
			$.ajax({ 
				type: 'post' ,
				url : "/allocation/updateCorStatus.do" ,
				dataType : 'json' ,
				async : false,
				data : {
					corId : corId,
					corStatus : corStatus
				},
				success : function(data, textStatus, jqXHR)
				{
					homeLoader.hide();
					if(data.resultCode == "0000"){
						$.alert("해당 경매건이 취소 되었습니다.", function(a){
							if(a){
								//document.location.href="/allocation/mainBidSuccessList.do";
								document.location.reload();
							}
						});
					}else{

						if(data.resultCode != "E002"){
							$.alert("취소하는데 실패 했습니다. 관리자에게 문의 하세요.", function(a){
								if(a){
									document.location.reload();
								}
							});							
						}else{
							$.alert(data.resultMsg, function(a){
								if(a){
									document.location.reload();
								}
							});

						}
						
						
					}
				} ,
				error : function(xhRequest, ErrorText, thrownError) {
				}
			});

		}


	});
	
}


function editCor(obj, corId){

	$.confirm("해당 경매건을 수정 하시겠습니까?",function(a){
		if(a){
			homeLoader.show();
			document.location.href = "/allocation/editCor.do?corId="+corId;
		}
	});

}


	
    </script>

	<div
		class="content-container withads themedetails companydetails writing pb100">
		<header class="clearfix nb">
			<div class="search-icon">
				<!-- <a style="cursor:pointer;" onclick="javascript:history.go(-1);" ><img src="/img/back-icon.png" alt=""></a> -->
				<!-- <a style="cursor:pointer;" href="/menu/menu.do" ><img src="/img/back-icon.png" alt=""></a> -->
				<a style="cursor: pointer;" onclick="javascript:prevPageConfirm();"><img
					src="/img/back-icon.png" alt=""></a>
			</div>
			<div class="page-title txt-medium">견적요청 상세</div>
			<div class="menu-bar pull-right">

				<!-- <a href="#"><img src="/img/open-icon.png" alt=""></a> -->
				<!-- <a href="/adviser/adviser.do"><img src="/img/home-pink-icon.png" alt=""></a> -->
				<!-- <a href="/allocation/main.do"><img src="/img/delete-x-icon.png" alt=""></a> -->
				<a onclick="javascript:prevPageConfirm();"><img
					src="/img/delete-x-icon.png" alt=""></a>




			</div>
		</header>

		<div class="company-details-container">
			<div class="allocation-views">
				<!-- <ul id="category">
                        <li class="active" onclick="javascript:changeCategory(this,'base');"><a>기본정보입력</a></li>
                        <li onclick="javascript:changeCategory(this,'car');"><a>차량정보입력</a></li>
                        <li onclick="javascript:changeCategory(this,'location');"><a>주소정보입력</a></li>
                        <li onclick="javascript:changeCategory(this,'confirm');"><a>내용확인</a></li>
                        <li onclick="javascript:changeCategory(this,'request');"><a>경매요청</a></li>
                    </ul> -->
				<!-- <ul id="category">
                        <li class="active"><a>기본정보입력</a></li>
                        <li><a>차량정보입력</a></li>
                        <li><a>주소정보입력</a></li>
                        <li><a>내용확인</a></li>
                        <li><a>경매요청</a></li>
                    </ul> -->
			</div>


			<div class="divider black"></div>

			<div id="total">


				<div id="baseInfo">
					<div style="padding: 3%;">
						<div
							style="width: 100%; overflow: hidden; border: 1px solid #eaeaea; border-bottom: none;">
							<div style="float: left; width: 50%;">
								<div class="certNum" style="margin-top: 5%; width: 100%;">
									<div style="text-align: right;">
										<div style="text-align: left; margin-left: 2.5%;">
											<span class="d-tbc" style="font-weight: bold;">출발일</span>
										</div>
										<p style="width: 90%; padding: 0%; margin: 0%;" onfocus="">${consignRequest.cor_departure_dt}</p>
									</div>
								</div>
							</div>
							<div style="float: left; width: 50%;">
								<div class="certNum" style="margin-top: 5%; width: 100%;">
									<div style="text-align: right;">
										<div style="text-align: left; margin-left: 2.5%;">
											<span class="d-tbc" style="font-weight: bold;">출발시간</span>
										</div>
										<p style="width: 90%; padding: 0%; margin: 0%;" onfocus="">${consignRequest.cor_departure_time}</p>
									</div>
								</div>
							</div>
						</div>
						<div
							style="width: 100%; overflow: hidden; border: 1px solid #eaeaea; border-top: none; border-bottom: none;">
							<div class="certNum" style="margin-top: 5%; width: 100%;">
								<div style="text-align: right;">
									<div style="text-align: left; margin-left: 2.5%;">
										<span style="color: #F00;"></span> <span class="d-tbc"
											style="font-weight: bold;">제목</span>
									</div>
									<p style="width: 90%; padding: 0%; margin: 0%;" onfocus="">${consignRequest.cor_title}</p>
								</div>
							</div>
						</div>
						<div
							style="width: 100%; overflow: hidden; border: 1px solid #eaeaea; border-top: none; border-bottom: none;">
							<div style="float: left; width: 50%;">
								<div class="certNum" style="margin-top: 5%; width: 100%;">
									<div style="text-align: right;">
										<div style="text-align: left; margin-left: 2.5%;">
											<span class="d-tbc" style="font-weight: bold;">탁송방식</span>
										</div>
										<p style="width: 90%; padding: 0%; margin: 0%;" onfocus="">${consignRequest.carrier_type_desc}</p>
									</div>
								</div>
							</div>
							<div style="float: left; width: 50%;">
								<div class="certNum" style="margin-top: 5%; width: 100%;">
									<div style="text-align: right;">
										<div style="text-align: left; margin-left: 2.5%;">
											<span class="d-tbc" style="font-weight: bold;">차량대수</span>
										</div>
										<p style="width: 90%; padding: 0%; margin: 0%;" onfocus="">${consignRequest.cor_car_cnt}</p>
									</div>
								</div>
							</div>
						</div>
						<div
							style="width: 100%; overflow: hidden; padding-bottom: 3%; border: 1px solid #eaeaea; border-top: none; border-bottom: none;">
							<div style="float: left; width: 50%;">
								<div class="certNum" style="margin-top: 5%; width: 100%;">
									<div style="text-align: right;">
										<div style="text-align: left; margin-left: 2.5%;">
											<span style="color: #F00;"></span> <span class="d-tbc"
												style="font-weight: bold;">경매방식</span>
										</div>
										<p style="width: 90%; padding: 0%; margin: 0%;" onfocus="">${consignRequest.cor_auction_type_desc}</p>
									</div>
								</div>
							</div>
							<c:if test="${consignRequest.cor_auction_type eq 'DR'}">
								<div class="auctionAmount" style="float: left; width: 50%;">
									<div class="certNum" style="margin-top: 5%; width: 100%;">
										<div style="text-align: right;">
											<div style="text-align: left; margin-left: 2.5%;">
												<span style="color: #F00;"></span> <span class="d-tbc"
													style="font-weight: bold;">금액</span>
											</div>
											<p style="width: 90%; padding: 0%; margin: 0%;" onfocus="">${consignRequest.cor_auction_amount}</p>
										</div>
									</div>
								</div>
							</c:if>
						</div>
						<div
							style="width: 100%; overflow: hidden; padding-bottom: 3%; border: 1px solid #eaeaea; border-top: none;">
							<div style="float: left; width: 50%;">
								<div class="certNum" style="margin-top: 5%; width: 100%;">
									<div style="text-align: right;">
										<div style="text-align: left; margin-left: 2.5%;">
											<span style="color: #F00;"></span> <span class="d-tbc"
												style="font-weight: bold;">낙찰결정방식</span>
										</div>
										<p style="width: 90%; padding: 0%; margin: 0%;" onfocus="">${consignRequest.cor_bid_decision_type_desc}</p>
									</div>
								</div>
							</div>
							<div style="float: left; width: 50%;">
								<div class="certNum" style="margin-top: 5%; width: 100%;">
									<div style="text-align: right;">
										<div style="text-align: left; margin-left: 2.5%;">
											<span style="color: #F00;"></span> <span class="d-tbc"
												style="font-weight: bold;">낙찰결정시간</span>
										</div>
										<p style="width: 90%; padding: 0%; margin: 0%;" onfocus="">${consignRequest.cor_bid_decision_time}분</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<c:forEach var="data" items="${consignRequestDetailList}"
					varStatus="status">
					<div style="padding: 3%;">
						<div style="border: 1px solid #ff463c;">
							<div id="carInfo">
								<div style="padding: 3%;">
									<div
										style="width: 100%; overflow: hidden; border: 1px solid #eaeaea; border-bottom: none;">
										<div style="float: left; width: 50%;">
											<div class="certNum" style="margin-top: 5%; width: 100%;">
												<div style="text-align: right;">
													<div style="text-align: left; margin-left: 2.5%;">
														<span class="d-tbc" style="font-weight: bold;">제조사</span>
													</div>
													<p style="width: 90%; padding: 0%; margin: 0%;" onfocus="">${data.crd_car_maker}</p>
												</div>
											</div>
										</div>
										<div style="float: left; width: 50%;">
											<div class="certNum" style="margin-top: 5%; width: 100%;">
												<div style="text-align: right;">
													<div style="text-align: left; margin-left: 2.5%;">
														<span class="d-tbc" style="font-weight: bold;">차종(모델명)</span>
													</div>
													<p style="width: 90%; padding: 0%; margin: 0%;" onfocus="">${data.crd_car_kind}</p>
												</div>
											</div>
										</div>
									</div>
									<div
										style="width: 100%; overflow: hidden; border: 1px solid #eaeaea; border-top: none; border-bottom: none;">
										<div style="float: left; width: 50%;">
											<div class="certNum" style="margin-top: 5%; width: 100%;">
												<div style="text-align: right;">
													<div style="text-align: left; margin-left: 2.5%;">
														<span class="d-tbc" style="font-weight: bold;">세부차종(트림)</span>
													</div>
													<p style="width: 90%; padding: 0%; margin: 0%;" onfocus="">${data.crd_car_trim}</p>
												</div>
											</div>
										</div>
										<div style="float: left; width: 50%;">
											<div class="certNum" style="margin-top: 5%; width: 100%;">
												<div style="text-align: right;">
													<div style="text-align: left; margin-left: 2.5%;">
														<span class="d-tbc" style="font-weight: bold;">차량번호</span>
													</div>
													<p style="width: 90%; padding: 0%; margin: 0%;" onfocus="">${data.crd_car_num}</p>
												</div>
											</div>
										</div>
									</div>
									<div
										style="width: 100%; overflow: hidden; padding-bottom: 3%; border: 1px solid #eaeaea; border-top: none;">
										<div style="float: left; width: 50%;">
											<div class="certNum" style="margin-top: 5%; width: 100%;">
												<div style="text-align: right;">
													<div style="text-align: left; margin-left: 2.5%;">
														<span style="color: #F00;"></span> <span class="d-tbc"
															style="font-weight: bold;">차대번호</span>
													</div>
													<p style="width: 90%; padding: 0%; margin: 0%;" onfocus="">${data.crd_car_id_num}</p>
												</div>
											</div>
										</div>
										<div style="float: left; width: 50%;">
											<div class="certNum" style="margin-top: 5%; width: 100%;">
												<div style="text-align: right;">
													<div style="text-align: left; margin-left: 2.5%;">
														<span style="color: #F00;"></span> <span class="d-tbc"
															style="font-weight: bold;">차량구분</span>
													</div>
													<p style="width: 90%; padding: 0%; margin: 0%;" onfocus="">${data.crd_car_type_desc}</p>
												</div>
											</div>
										</div>
									</div>
									<div style="text-align: center;">
										<c:forEach var="fileData" items="${consignRequestFileList}"
											varStatus="status">
											<c:if test="${data.crd_id == fileData.crf_crd_id}">
												<div style="margin-top: 3%; display: inline-block;">
													<img
														src="${customer_server_path}${customer_thumb_image_path}${fileData.crf_file_path}"
														style="width: 100px;">
												</div>
											</c:if>
										</c:forEach>
									</div>
								</div>
							</div>
							<div id="address">
								<div class="productsinuse"
									style="clear: both; padding: 0 3% 3% 3%;">
									<div id="" class="certNum  product-use-list"
										style="width: 100%; padding: 0;">
										<div class="product-use-item"
											style="text-align: right; padding: 0;">
											<div style="text-align: left; margin-left: 2.5%;">
												<span class="d-tbc" style="font-weight: bold;">출발지</span>
											</div>
											<p
												style="width: 100%; padding-bottom: 3%; padding-right: 8%; margin: 0%;"
												onfocus="">${data.crd_departure}</p>
											<p style="display: none;">${data.crd_departure_latlng}</p>
										</div>
									</div>
								</div>
								<div class="productsinuse"
									style="clear: both; padding: 0 3% 3% 3%;">
									<div id="" class="certNum  product-use-list"
										style="width: 100%; padding: 0;">
										<div class="product-use-item"
											style="text-align: right; padding: 0;">
											<div style="text-align: left; margin-left: 2.5%;">
												<span class="d-tbc" style="font-weight: bold;">도착지</span>
											</div>
											<p
												style="width: 100%; padding-bottom: 3%; padding-right: 8%; margin: 0%;"
												onfocus="">${data.crd_arrival}</p>
											<p style="display: none;">${data.crd_arrival_latlng}</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</c:forEach>


				<div class="add-ticker-container ">
					<div class="divider"></div>
					<div class="ticker-list-container content-box">
						<c:if test="${consignRequest.cor_auction_type eq 'RC'}">
							<span class="title">입찰 목록(역경매:최저가순)</span>
						</c:if>
						<c:if test="${consignRequest.cor_auction_type eq 'DR'}">
							<span class="title">입찰 목록(오픈형:입찰참여순)</span>
						</c:if>
						<div class="ticker-list">
							<c:if test="${fn:length(consignRequestBidList) == 0}">
								<div class="no-result content-box ">
									<div class="notfound" style="margin-top: 50px;">
										<span>등록된 입찰건이 없습니다</span> <img src="/img/alert-icon.png"
											alt="">
									</div>
								</div>
								<div class="divider"
									style="margin-top: 50px; background-color: #fff;"></div>
							</c:if>
							<c:if test="${fn:length(consignRequestBidList) != 0}">
								<c:forEach var="data" items="${consignRequestBidList}"
									varStatus="status">
									<div class="ticker-item">
										<%-- <span>${fn:length(consignRequestBidList)-(status.index)}차 입찰</span> --%>
										<c:if test="${data.crb_status == 'N'}">
											<div class="productsinuse"
												style="display: inline-block; position: absolute; right: 0; top: 25px;">
												<div class="certNum product-use-list"
													style="margin-top: 10%; width: 100%;">
													<div class="product-use-item"
														style="text-align: center; border: none; position: static;">
														<a
															onclick="javascript:bidAccept('${data.cor_id}','${data.crb_id}','Y');"
															class="product-use-search inlined" style="width: 100px;">낙찰</a>
													</div>
												</div>
											</div>
										</c:if>
										<c:if test="${data.crb_status == 'Y'}">
											<div class="productsinuse"
												style="display: inline-block; position: absolute; right: 0; top: 25px;">
												<div class="certNum product-use-list"
													style="margin-top: 10%; width: 100%;">
													<div class="product-use-item"
														style="text-align: center; border: none; position: static;">
														<!-- <a onclick="javascript:$.alert('낙찰된 입찰건입니다.');" class="product-use-search inlined" style="width:100px;">낙찰완료</a> -->
														<a onclick="javascript:goDriverInfoPage('${data.crb_id}')"
															class="product-use-search inlined" style="width: 100px;">기사정보확인</a>
													</div>
												</div>
											</div>
										</c:if>
										<c:if test="${data.crb_status == 'F'}">
											<div class="productsinuse"
												style="display: inline-block; position: absolute; right: 0; top: 25px;">
												<div class="certNum product-use-list"
													style="margin-top: 10%; width: 100%;">
													<div class="product-use-item"
														style="text-align: center; border: none; position: static;">
														<a onclick="javascript:$.alert('이 건은 입찰에 실패 했습니다.');"
															class="product-use-search inlined"
															style="border: 1px solid #666; border-radius: 3px; width: 100px; background-color: #fff; color: #666;">낙찰실패</a>
													</div>
												</div>
											</div>
										</c:if>
										<span>입찰가 : ${data.amount} 원</span>
										<c:if
											test="${data.crb_dvr_address != null && data.crb_dvr_address != ''}">
											<span>입찰시 위치 : ${data.crb_dvr_address}</span>
										</c:if>
										<c:if
											test="${data.crb_dvr_address == null || data.crb_dvr_address == ''}">
											<span>입찰시 위치 정보 없음</span>
										</c:if>
										<span>${data.crb_reg_dt_str} ${data.crb_reg_dt_si}</span>
										<%-- <c:if test="${data.crb_status == 'Y'}">
		                                    	<a onclick="javascript:$.alert('낙찰된 입찰건입니다.');" class="" style="position:absolute; right:0; color:#ff463c;">낙찰완료</a>
		                                    </c:if> --%>
									</div>
								</c:forEach>
							</c:if>
						</div>
					</div>


					<%-- <div class="assigned-setting">
	                            <span class="title">입찰 참여액 입력 </span>
	                            <div class="price-input-box">
	                                <div class="direct-input">
	                                    <input type="text" onkeyup="javascript:getNumber(this);" inputmode="numeric" placeholder="입찰 참여액" style="text-align:right;" value="0">
	                                    <a onclick="javascript:goBid(this,'${paramMap.corId}');" class="btn-addnotif">입찰 참여</a>
	                                </div>
	                            </div>
	                        </div> --%>
				</div>


			</div>


		</div>


		<c:if
			test="${consignRequest.cor_status eq 'WW' or consignRequest.cor_status eq 'II'}">
			<div class="fixed-bottomarea">
				<c:if test="${consignRequest.cor_status eq 'WW'}">
					<div style="width: 50%; display: inline-block;">
						<a
							href="javascript:updateCorStatus(this,'${consignRequest.cor_id}','CC')">경매취소</a>
					</div>
					<div style="width: 50%; display: inline-block;">
						<a href="javascript:editCor(this,'${consignRequest.cor_id}')">경매수정</a>
					</div>
				</c:if>
				<c:if test="${consignRequest.cor_status eq 'II'}">
					<div style="width: 100%; display: inline-block;">
						<a
							href="javascript:updateCorStatus(this,'${consignRequest.cor_id}','CC')">경매취소</a>
					</div>
				</c:if>

			</div>
		</c:if>


	</div>




	<script src="/js/vendor/jquery-1.11.2.min.js"></script>
	<!-- <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script> -->
	<script src="/js/vendor/bootstrap.min.js"></script>
	<!-- veiwport for countnumber -->
	<script src="/js/jquery.viewportchecker.js"></script>
	<script src="/js/main.js"></script>
	<script src="/js/alert.js"></script>
	<script
		src="https://apis.openapi.sk.com/tmap/jsv2?version=1&appKey=${appKey}"></script>
	<script src="/js/vendor/jquery.loading-indicator.min.js"></script>
	<script>

    $(document).ready(function(){
    
    	homeLoader = $('body').loadingIndicator({
			useImage: false,
			showOnInit : false
		}).data("loadingIndicator");
	
    	//homeLoader.show();
    	//homeLoader.hide();
    	var scroll_top=$(document).scrollTop();
    	$(".loading-indicator-wrapper").css("top",scroll_top);
    	$(".loading-indicator-wrapper").css("height","100%");
    	
    });

    

    

    </script>
</body>
</html>
