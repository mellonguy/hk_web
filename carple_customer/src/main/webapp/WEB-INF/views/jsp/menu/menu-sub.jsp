<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko">
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>

<body style="background-color: #f2f2f2;">

	<script type="text/javascript">  

var homeLoader;

$(document).ready(function(){  

	
});

$(window).on("scroll", function(){ 
	var scroll_top=$(this).scrollTop();
	$(".loading-indicator-wrapper").css("top",scroll_top);
	$(".loading-indicator-wrapper").css("height","100%");
});



function kakaoLogin(){

	//document.location.href = "https://kauth.kakao.com/oauth/authorize?client_id=63f484a87cbebd51c62a6c6c93501a01&redirect_uri=http://localhost:8091/kakao/kakaoLogin.do&response_type=code&auth_type=reauthenticate";
	//homeLoader.show();

	if(window.Android != null){
		//homeLoader.show();
		window.Android.kakaoLogin("${Redirect_URI_app}");
		//$.alert("1");
	}else{
		document.location.href = "https://kauth.kakao.com/oauth/authorize?client_id=${RestApiKey}&redirect_uri=${Redirect_URI}&response_type=code&auth_type=reauthenticate";
	}



	
	//document.location.href = "https://kauth.kakao.com/oauth/authorize?client_id=${RestApiKey}&redirect_uri=${Redirect_URI}&response_type=code&auth_type=reauthenticate";
	
}


function naverLogin(){

			
		if(window.Android != null){
			//homeLoader.show();
			window.Android.naverLogin("${Redirect_URI_app}");
			//$.alert("1");
		}else{
			document.location.href = "https://nid.naver.com/oauth2.0/authorize?client_id=${clientId}&response_type=code&redirect_uri=${callbackUrl}&state=${state}";
		}

	
	
}


function naverlogout(){

	window.Android.naverLogout("test");
	
}

function backKeyController(str){
	
	//document.location.href="/carrier/group-list.do?allocationId=${paramMap.groupId}&allocationStatus=Y";
	if(window.Android != null){
		window.Android.toastShort("뒤로가기 버튼으로 앱을 종료 할 수 없습니다.");
	}
	
	
}



</script>


	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

	<div class="content-container">
		<header class="bg-pink clearfix">
			<div class="search-icon">
				<a href="#"></a>
			</div>
			<div class="menu-bar pull-right">
				<!-- <a style="cursor:pointer;" href="/adviser/adviser.do"><img src="/img/close-white-icon.png" alt=""></a> -->
				<!-- <a style="cursor:pointer;" onclick="javascript:history.go(-1);"><img src="/img/close-white-icon.png" alt=""></a> -->
			</div>
		</header>
		<div class="menu-container bg-pink" style="text-align: center;">
			<!-- <span class="img-holder">
                    <img src="/img/user-img.png" alt="">
                </span> -->



			<!-- <a class="kakao txt-bold" style="cursor:pointer;" onclick="javascript:kakaoLogin();">
                    카카오계정으로 로그인
                </a> -->


			<!-- <img onclick="javascript:naverLogin();" src="/img/kakao_login_large_wide.png" style="width:100%;" alt=""> -->

			<img onclick="javascript:kakaoLogin();"
				src="/img/kakao_login_large_narrow.png"
				style="width: 100%; height: 75px; margin-top: 90px; margin-bottom: 62px;"
				alt="">
			<!-- <img onclick="javascript:naverLogin();" src="/img/kakao_login_large_wide.png" style="width:100%;" alt=""> -->
			<!-- <img onclick="javascript:naverLogin();" src="/img/kakao_login_medium_narrow.png" style="width:100%;" alt=""> -->
			<!-- <img onclick="javascript:naverLogin();" src="/img/kakao_login_medium_wide.png" style="width:100%;" alt=""> -->




			<img onclick="javascript:naverLogin();" src="/img/naver_login.PNG"
				style="width: 100%; height: 75px;" alt="">






			<!-- <img onclick="javascript:naverLogin();" style=" background-image:url(/img/naver_login.PNG); background-repeat:no-repeat;" /> -->




			<!-- <a class="kakao txt-bold" style="cursor:pointer; margin-top:5%; background-image:url(/img/naver_id_login.png);" onclick="javascript:naverLogin();">
                    네이버 아이디로 로그인
                </a> -->

		</div>

		<div style="width: 100%; height: 100%;">

			<!-- <div onclick="javascript:naverlogout();" style="width:371px; height:45px; background-color:#03c75a; background-image:url(/img/naver_id_login.png); background-repeat:no-repeat;">
                </div> -->



		</div>


		<!-- <div class="main-menu-container">
                <a href="primary.do" class="menu-link">
                    <img src="/img/coin-icon.png" alt="">
                    <span class="text">포인트충전</span>
                </a>
                <a href="notification.do" class="menu-link">
                    <img src="/img/gear-icon.png" alt="">
                    <span class="text">알림설정</span>
                </a>
                <a href="roboadvisor.do" class="menu-link">
                    <img src="/img/list-icon.png" alt="">
                    <span class="text">이용중 상품</span>
                </a>
                <a href="widgetsetting.do" class="menu-link">
                    <img src="/img/layer-icon.png" alt="">
                    <span class="text">위젯설정</span>
                </a>
                <a href="coupon.do" class="menu-link">
                    <img src="/img/cut-icon.png" alt="">
                    <span class="text">쿠폰함</span>
                </a>
                <a href="support.do" class="menu-link">
                    <img src="/img/support-icon.png" alt="">
                    <span class="text">고객지원센터</span>
                </a>
            </div> -->
	</div>

	<!-- <div style ="text-align:center; height:100%;">
				<img style="width:45%; " src="/img/nqlonglogo.jpg"> 
			</div> -->

	<div class="writing" style="height: 100px;">
		<div class="fixed-bottomarea" style="height: auto;">
			<!-- <div style="width:100%; display:inline-block;"><a href="javascript:insertDriver(this,'')">회원 가입 신청</a></div> -->
			<div class="search_footer" role="contentinfo">
				<div class="footer">
					<footer role="contentinfo">

						<div class="footer_info" style="text-align: center;">
							<p class="info"
								onclick="javascript:homeLoader.show(); document.location.href='/login/terms-mobile.do'"
								style="display: inline-block">이용약관 및 개인정보처리방침</p>
							<address class="addr"
								style="font-weight: 200; font-size: 12px; margin-bottom: 0px;">
								상호명:주식회사 내셔널컨퀘스트<br>서울시 강남구 헌릉로 569길 21-30, 4층 407호(세곡동,
								드림하이 오피스텔) <br>대표자:임경숙 Tel:1577-5268 Fax:02-426-3508<br>
								사업자등록번호:551-81-01923<br> ⓒ National Conquest.
							</address>
						</div>
						<!-- <div style="width:100%; display:inline-block; text-align:center; background-color:#1E1D1D;"><img style="width:45%; " src="/img/nqlonglogo.jpg"></div> -->
					</footer>
				</div>
			</div>


		</div>
	</div>
	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
	<script src="/js/vendor/bootstrap.min.js"></script>
	<!-- veiwport for countnumber -->
	<script src="/js/jquery.viewportchecker.js"></script>
	<script src="/js/alert.js"></script>
	<script src="/js/main.js"></script>
	<script src="/js/vendor/jquery.loading-indicator.min.js"></script>
	<script>

    $(document).ready(function(){
    
    	homeLoader = $('body').loadingIndicator({
			useImage: false,
			showOnInit : false
		}).data("loadingIndicator");
	
    	//homeLoader.show();
    	//homeLoader.hide();
    	var scroll_top=$(document).scrollTop();
    	$(".loading-indicator-wrapper").css("top",scroll_top);
    	$(".loading-indicator-wrapper").css("height","100%");
    	
    });

    

    

    </script>
</body>
</html>
