<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!doctype html>
<html lang="ko">
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>

<body class="" style="overscroll-behavior: contain;">

	<script type="text/javascript">  

var contents = "";
var carCount = 0;
var carInfo = "";
var departure = "";
var arrival = "";
var address = "";
var addressInfoArr;
var baseInfo = "";
var homeLoader;

var currentStatus = "base";
$(document).ready(function(){

	$("#total").children().each(function(index,element){
		$(this).css("display","none");
	});
	$("#base").css("display","")
	contents = $("#car").html();

	carInfo = $("#carInfo").html();
	address = $("#address").html();
	departure = $("#departure").html();
	arrival = $("#arrival").html();
	baseInfo = $("#baseInfo").html();
	$("#innerLocationDetail").html("");
		
	//$(".modal-field").css("display","none");

	
});


$(window).on("scroll", function(){ 
	var scroll_top=$(this).scrollTop();
	$(".loading-indicator-wrapper").css("top",scroll_top);
	$(".loading-indicator-wrapper").css("height","100%");
});

/*
$(window).bind("pageshow", function (event) {
	if (event.originalEvent.persisted || (window.performance && window.performance.navigation.type == 2)) {
		// 뒤로가기로 페이지 로드 시
		document.location.reload();

	}else {
		// 새로운 페이지 로드 시
		//$.alert("eee");
	}
});
*/

function backKeyController(str){
	
	homeLoader.show();
	document.location.href='/menu/menu.do';
	
}

	
    </script>

	<div class="content-container withads newsdetails themedetails">
		<header class="clearfix nb">
			<div class="search-icon">
				<a
					onclick="javascript:homeLoader.show(); document.location.href='/menu/menu-list.do';"><img
					src="/img/back-icon.png" alt=""></a>
			</div>
			<div class="page-title txt-medium">앱정보</div>
		</header>

		<div class="company-details-container">
			<div class="company-views"></div>


			<div class="market-list" style="margin-top: 5%;">
				<a href="/menu/appVersionInfo.do" class="market-item"> <span
					class="condition">앱 버전 정보</span> <!-- <span class="date">2018/03/26</span> -->
				</a> <a href="/login/terms-mobile.do" class="market-item"> <span
					class="condition">이용약관 및 개인정보 처리방침</span> <!-- <span class="date">2018/03/26</span> -->
				</a> <a href="/menu/appInfo.do" class="market-item"> <span
					class="condition">사고시 대처요령</span> <!-- <span class="date">2018/03/26</span> -->
				</a> <a href="/menu/osLicense.do" class="market-item"> <span
					class="condition">오픈소스 라이선스</span> <!-- <span class="date">2018/03/26</span> -->
				</a>

			</div>






			<div class="view-all">
				<!-- <a href="news.html">전체 보기</a> -->
			</div>
		</div>
	</div>

	<div class="writing" style="height: 100px;">
		<div class="fixed-bottomarea" style="height: auto;">
			<!-- <div style="width:100%; display:inline-block;"><a href="javascript:insertDriver(this,'')">회원 가입 신청</a></div> -->
			<div class="search_footer" role="contentinfo">
				<div class="footer">
					<footer role="contentinfo">

						<div class="footer_info" style="text-align: center;">
							<p class="info"
								onclick="javascript:homeLoader.show(); document.location.href='/login/terms-mobile.do'"
								style="display: inline-block">이용약관 및 개인정보처리방침</p>
							<address class="addr"
								style="font-weight: 200; font-size: 12px; margin-bottom: 0px;">
								상호명:주식회사 내셔널컨퀘스트<br>서울시 강남구 헌릉로 569길 21-30, 4층 407호(세곡동,
								드림하이 오피스텔) <br>대표자:임경숙 Tel:1577-5268 Fax:02-426-3508<br>
								사업자등록번호:551-81-01923<br> ⓒ National Conquest.
							</address>
						</div>
						<!-- <div style="width:100%; display:inline-block; text-align:center; background-color:#1E1D1D;"><img style="width:45%; " src="/img/nqlonglogo.jpg"></div> -->
					</footer>
				</div>
			</div>


		</div>
	</div>




	<script src="/js/vendor/jquery-1.11.2.min.js"></script>
	<!-- <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script> -->
	<script src="/js/vendor/bootstrap.min.js"></script>
	<!-- veiwport for countnumber -->
	<script src="/js/jquery.viewportchecker.js"></script>
	<script src="/js/main.js"></script>
	<script src="/js/alert.js"></script>
	<script
		src="https://apis.openapi.sk.com/tmap/jsv2?version=1&appKey=${appKey}"></script>
	<script src="/js/vendor/jquery.loading-indicator.min.js"></script>
	<script>

    $(document).ready(function(){
    
    	homeLoader = $('body').loadingIndicator({
			useImage: false,
			showOnInit : false
		}).data("loadingIndicator");
	
    	//homeLoader.show();
    	//homeLoader.hide();
    	var scroll_top=$(document).scrollTop();
    	$(".loading-indicator-wrapper").css("top",scroll_top);
    	$(".loading-indicator-wrapper").css("height","100%");
    	
    });

    

    

    </script>
</body>
</html>
