<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko">
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
<body>

	<script type="text/javascript">  
var homeLoader;
var deviceToken = "";
$(document).ready(function(){  

	try {
		if(window.Android != null){
    		deviceToken = getDeviceToken();
    		
			if(typeof deviceToken == "undefined" || deviceToken == null || deviceToken == ""){
				window.location.reload();
			}
			
			if(window.Android != null && deviceToken != ""){
				//updateDriverDeviceToken(deviceToken,"aos");
				updateDeviceToken(deviceToken);
				pageMoveStop = false;
			}
			
    	}else{
    	
    	}
	}catch(exception){
	    
	}finally {
		
	}
	
	
	
  
});



function getDeviceToken(){
	
	var token = "";
	try{
		if(window.Android != null){
			token = window.Android.getDeviceToken("test");
		}else if(window.webkit.messageHandlers != null){
			window.webkit.messageHandlers.scriptHandler.postMessage("getDeviceToken");
		}
	}catch(exception){

		
	}finally{
		
	}
	return token;
}  



$(window).on("scroll", function(){ 
	var scroll_top=$(this).scrollTop();
	$(".loading-indicator-wrapper").css("top",scroll_top);
	$(".loading-indicator-wrapper").css("height","100%");
});

function logout(){

	$.confirm("로그아웃 하시겠습니까?",function(a){

		if(a){

			try {
				if(window.GerionJrpr != null){
					window.GerionJrpr.logoutForApp("");
				}
				if(window.external != null){
		            window.external.logoutForPC("");
		        }
			}catch(exception){
			    
			}finally {

				if(window.Android != null){

					if("${user.mem_join_type}" == "kakao"){
						window.Android.kakaoLogout("test");
						document.location.href = "/${user.mem_join_type}/logoutByKakaoUserInfo.do";
					}else if("${user.mem_join_type}" == "naver"){
						window.Android.naverLogout("test");
						document.location.href = "/${user.mem_join_type}/logoutByNaverUserInfo.do";
					}else{
						document.location.href = "/${user.mem_join_type}/logout.do";
					}
				}else{
					document.location.href = "/${user.mem_join_type}/logout.do";
				}
	
			}

		}
		
	});

	
}
    

function loginForApp(userId){
	window.GerionJrpr.loginForApp(userId, "");
}



function logoutForApp(){
	window.GerionJrpr.logoutForApp("");
}    


var resultData;
var dataLength;


function getApiData(){

/* 
	$.alert("testtest",function(a){

	});
 */

 $.alert("시작",function(a){

	 $.ajax({ 
			type: 'post' ,
			url : "/menu/getapidata.do" ,
			dataType : 'json' ,
			async : true,
			data : {
				
			},
			success : function(data, textStatus, jqXHR)
			{

				resultData = data.resultData;
				dataLength = resultData.length;

				startInterval(dataLength);
				
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});

});

	

	
}


var intervalVal;

function startInterval(max){


	intervalVal = setInterval( function() {

		var randomVal = Math.floor((Math.random()*max));
		var resultStr = "["+resultData[randomVal].accType+"]\n";
		resultStr += resultData[randomVal].accDate +"\t"+resultData[randomVal].accHour+"\n"
		resultStr += resultData[randomVal].roadNM +"\n"
		resultStr += resultData[randomVal].startEndTypeCode+"\n";
		resultStr += resultData[randomVal].accPointNM +"\n";
		
		$.alert(resultStr,function(a){
					
		});
			
	}, 10000);


	
}


function stopInterval(){


	clearInterval(intervalVal);
	$.alert("중지",function(a){
		
	});
	
}


function movePage(addr){

	homeLoader.show();
	document.location.href = addr;
	
}


function backKeyController(str){
	
	//document.location.href="/carrier/group-list.do?allocationId=${paramMap.groupId}&allocationStatus=Y";
	if(window.Android != null){
		window.Android.toastShort("뒤로가기 버튼으로 앱을 종료 할 수 없습니다.");
	}
	
	
}



function updateDeviceToken(memDeviceKey){


	if(memDeviceKey != null && memDeviceKey != ""){
		
		$.ajax({ 
			type: 'post' ,
			url : "/member/updateMemDeviceKey.do" ,
			dataType : 'json' ,
			async : true,
			data : {
				memDeviceKey : memDeviceKey
			},
			success : function(data, textStatus, jqXHR)
			{
				//$.alert("success");
				resultData = data.resultData;
			} ,
			error : function(xhRequest, ErrorText, thrownError) {

				//$.alert("fail");
				
			}
		});

	}

}




function goMenuPage(){

	document.location.href="/menu/menu-list.do";
	
}


    
</script>



	<div class="content-container">
		<header class="bg-pink clearfix">
			<div class="search-icon" onclick="javascript:goMenuPage();">
				<a href="#"
					style='margin-top: 30%; width: 14px; height: 14px; zoom: 2.0; background-image: url("data:image/svg+xml;charset=US-ASCII,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22iso-8859-1%22%3F%3E%3C!DOCTYPE%20svg%20PUBLIC%20%22-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%22%20%22http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%22%3E%3Csvg%20version%3D%221.1%22%20id%3D%22Layer_1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%20x%3D%220px%22%20y%3D%220px%22%20%20width%3D%2214px%22%20height%3D%2214px%22%20viewBox%3D%220%200%2014%2014%22%20style%3D%22enable-background%3Anew%200%200%2014%2014%3B%22%20xml%3Aspace%3D%22preserve%22%3E%3Cpath%20d%3D%22M1%2C4h12c0.553%2C0%2C1-0.447%2C1-1s-0.447-1-1-1H1C0.447%2C2%2C0%2C2.447%2C0%2C3S0.447%2C4%2C1%2C4z%20M13%2C6H1C0.447%2C6%2C0%2C6.447%2C0%2C7%20c0%2C0.553%2C0.447%2C1%2C1%2C1h12c0.553%2C0%2C1-0.447%2C1-1C14%2C6.447%2C13.553%2C6%2C13%2C6z%20M13%2C10H1c-0.553%2C0-1%2C0.447-1%2C1s0.447%2C1%2C1%2C1h12%20c0.553%2C0%2C1-0.447%2C1-1S13.553%2C10%2C13%2C10z%22%2F%3E%3C%2Fsvg%3E")'></a>
			</div>
			<div class="menu-bar pull-right">
				<!-- <a style="cursor:pointer;" href="/adviser/adviser.do"><img src="/img/close-white-icon.png" alt=""></a> -->
				<!-- <a style="cursor:pointer;" onclick="javascript:history.go(-1);"><img src="/img/close-white-icon.png" alt=""></a> -->
			</div>
		</header>
		<div class="menu-container bg-pink">
			<span class="img-holder"> <img src="${user.mem_photo}" alt="">
			</span>
			<div class="qoute-emotion-holder">
				<span class="qoute txt-bold"> 안녕하세요. <br>
					${user.mem_nick_name}님<br> 잡아! 에 오신것을 환영 합니다.
				</span> <span class="emotion txt-bold"> <%-- ${usernick} --%>
				</span>
			</div>

			<div class="clearfix">
				<!-- <span class="label-text">보유포인트</span> -->
				<%-- <a href="purchase-history.do" style="width:80%;">
                        <span class="points roboto">
                            ${userPoint}
                            <img src="/img/arrow-white-right-icon.png" alt="">
                        </span>
                    </a> --%>
				<a onclick="javascript:logout();" class="log-out">로그아웃</a>
			</div>
		</div>
		<div class="main-menu-container">
			<a onclick="javascript:movePage('/allocation/main.do');"
				class="menu-link"> <img src="/img/list-icon.png" alt=""> <span
				class="text">차량탁송</span>
			</a>

			<!-- <a href="getapidata.do" class="menu-link"> -->
			<a onclick="javascript:getApiData();" class="menu-link"> <img
				src="/img/coin-icon.png" alt=""> <span class="text">한국도로공사
					API 호출</span>
			</a>
			<!-- <a href="notification.do" class="menu-link"> -->
			<a onclick="javascript:stopInterval();" class="menu-link"> <img
				src="/img/gear-icon.png" alt=""> <span class="text">정지</span>
			</a>

			<!-- <a href="widgetsetting.do" class="menu-link">
                    <img src="/img/layer-icon.png" alt="">
                    <span class="text">위젯설정</span>
                </a>
                <a href="coupon.do" class="menu-link">
                    <img src="/img/cut-icon.png" alt="">
                    <span class="text">쿠폰함</span>
                </a>
                <a href="support.do" class="menu-link">
                    <img src="/img/support-icon.png" alt="">
                    <span class="text">고객지원센터</span>
                </a> -->
		</div>
	</div>




	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
	<script src="/js/vendor/bootstrap.min.js"></script>
	<!-- veiwport for countnumber -->
	<script src="/js/jquery.viewportchecker.js"></script>
	<script src="/js/alert.js"></script>
	<script src="/js/main.js"></script>
	<script src="/js/vendor/jquery.loading-indicator.min.js"></script>
	<script>
    
    $(document).ready(function(){
    
    	homeLoader = $('body').loadingIndicator({
			useImage: false,
			showOnInit : false
		}).data("loadingIndicator");
	
    	//homeLoader.show();
    	//homeLoader.hide();
    	var scroll_top=$(document).scrollTop();
    	$(".loading-indicator-wrapper").css("top",scroll_top);
    	$(".loading-indicator-wrapper").css("height","100%");

    	
    });

    </script>
</html>
