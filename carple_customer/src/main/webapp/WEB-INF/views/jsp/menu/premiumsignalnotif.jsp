<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko">
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>

<body class="bg-gray">

	<script type="text/javascript">  

$(document).ready(function(){
	
	//getNewsData("${searchWord}");
	
});


function selectInterestItem(itemCd,marketCd,obj){
	
	$.ajax({ 
		type: 'post' ,
		url : "/interest/selectInterestItem.do" ,
		dataType : 'json' ,
		data : {
			itemCd : itemCd,
			marketCd : marketCd
		},
		success : function(data, textStatus, jqXHR)
		{
			if(data.resultCode == "0000"){
				insertInterestItem(itemCd,marketCd,obj);
			}else{
				deleteInterestItem(itemCd,marketCd,obj);
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	}); 
	
	
}    
    
function insertInterestItem(itemCd,marketCd,obj){
	
	$.ajax({ 
		type: 'post' ,
		url : "/interest/insertInterestItem.do" ,
		dataType : 'json' ,
		data : {
			itemSrtCd : itemCd,
			marketCd : marketCd
		},
		success : function(data, textStatus, jqXHR)
		{
			if(data.resultCode == "0000"){
				$(obj).addClass('heart-full');
			}else if(data.resultCode == "E002"){
					alert("로그인 되지 않음");
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	}); 
}    
    
  

function deleteInterestItem(itemCd,marketCd,obj){
	
	$.ajax({ 
		type: 'post' ,
		url : "/interest/deleteInterestItem.do" ,
		dataType : 'json' ,
		data : {
			itemSrtCd : itemCd,
			marketCd : marketCd
		},
		success : function(data, textStatus, jqXHR)
		{
			if(data.resultCode == "0000"){
				$(obj).removeClass('heart-full');
			}else if(data.resultCode == "E002"){
					alert("로그인 되지 않음");
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	}); 
}    


function goCompanyPage(itemSrtCd,marketCd){
	
	document.location.href = "/company/company.do?itemSrtCd="+itemSrtCd+"&marketCd="+marketCd;
	
}


function goSearch(){
	
	document.location.href = "/menu/premiumsignalnotif.do?searchWord="+$("#searchWord").val();
}

function searchCancel(){
	
	$("#searchWord").val("");
	
}


</script>

	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

	<div class="content-container productsinuse premiumsignalnotif">
		<header class="clearfix">
			<div class="search-icon">
				<a href="/menu/menu.do"><img src="/img/back-icon.png" alt=""></a>
			</div>
			<div class="page-title txt-medium">이용중 상품</div>
			<div class="menu-bar pull-right">
				<a href="/adviser/adviser.do"><img src="/img/home-pink-icon.png"
					alt=""></a>
			</div>
		</header>
		<div class="header-links clearfix">
			<ul>
				<li><a href="/menu/roboadvisor.do">로보어드바이저</a></li>
				<li><a href="/menu/confirmrecomm.do">추천가 확인</a></li>
				<li class="active"><a href="/menu/premiumsignalnotif.do"> <span>프리미엄</span>
						<span>신호알림</span>
				</a></li>
			</ul>
		</div>
		<div class="divider"></div>
		<div class="search-area-box">
			<div class="input-area">
				<a href="javascript:goSearch();" class="search"></a> <input
					type="text" id="searchWord" placeholder="종목명 입력"
					value="${searchWord}">
			</div>
			<a href="javascript:searchCancel();" class="cancel">취소</a>
		</div>
		<div class="sort-area clearfix">
			<a href="#">정렬</a>
		</div>
		<div class="box-item-holder">
			<c:forEach var="data" items="${premiumSignalList}" varStatus="status">
				<div class="box-item"
					onclick="javascript:goCompanyPage('${data.item_srt_cd}','${data.market_cd}'); ">
					<div class="item-top-info clearfix">
						<div class="details-area">
							<span class="name">${data.item_name}</span>
						</div>
						<c:if test="${data.cmpprevddPrc >= 0}">
							<div class="value-area red">${data.trdPrc}</div>
						</c:if>
						<c:if test="${data.cmpprevddPrc < 0}">
							<div class="value-area blue">${data.trdPrc}</div>
						</c:if>
						<c:if test="${data.cmpprevddPrc >= 0}">
							<div class="percent-area red">
								<span class="percent">+300.85%</span> <span class="eval up">200,750</span>
							</div>
						</c:if>
						<c:if test="${data.cmpprevddPrc < 0}">
							<div class="percent-area blue">
								<span class="percent">${data.cmpprevddPer}%</span> <span
									class="eval down">${data.cmpprevddPrc}</span>
							</div>
						</c:if>
					</div>
				</div>
			</c:forEach>





			<!--  <div class="box-item ">
                    <div class="item-top-info clearfix">
                        <div class="details-area">
                            <span class="name">티플랙스</span>
                        </div>
                        <div class="value-area red">
                            300,000
                        </div>
                        <div class="percent-area red">
                            <span class="percent">+300.85%</span>
                            <span class="eval up">200,750</span>
                        </div>
                    </div>
                </div>
                <div class="box-item ">
                    <div class="item-top-info clearfix">
                        <div class="details-area">
                            <span class="name">티플랙스</span>
                        </div>
                        <div class="value-area">
                            300,000
                        </div>
                        <div class="percent-area">
                            <span class="percent">+300.85%</span>
                            <span class="eval">200,750</span>
                        </div>
                    </div>
                </div>
                <div class="box-item ">
                    <div class="item-top-info clearfix">
                        <div class="details-area">
                            <span class="name">티플랙스</span>
                        </div>
                        <div class="value-area blue">
                            300,000
                        </div>
                        <div class="percent-area blue">
                            <span class="percent">+300.85%</span>
                            <span class="eval down">200,750</span>
                        </div>
                    </div>
                </div>
                <div class="box-item ">
                    <div class="item-top-info clearfix">
                        <div class="details-area">
                            <span class="name">삼성전자</span>
                        </div>
                        <div class="value-area red">
                            300,000
                        </div>
                        <div class="percent-area red">
                            <span class="percent">+300.85%</span>
                            <span class="eval up">200,750</span>
                        </div>
                    </div>
                </div> -->
		</div>

	</div>


	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
	<script src="/js/vendor/bootstrap.min.js"></script>
	<!-- veiwport for countnumber -->
	<script src="/js/jquery.viewportchecker.js"></script>
	<script src="/js/main.js"></script>
</body>
</html>
