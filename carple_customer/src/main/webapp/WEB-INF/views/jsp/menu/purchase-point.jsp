<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko">
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>

<body class="bg-gray">

	<script type="text/javascript">  
  
function goPurchasePage(){

	document.location.href = "/menu/purchase-history.do";	
	
}
    
    
    
    
    
    
function purchasePoint(reason,point){
	
	//alert(reason);
	//alert(point);
	//document.location.href = "";
	
	
}    
    
    
</script>

	<div class="content-container">
		<header class="clearfix">
			<div class="search-icon">
				<a style="cursor: pointer;" onclick="javascript:history.go(-1);"><img
					src="/img/back-icon.png" alt=""></a>
			</div>
			<div class="page-title txt-medium">포인트 충전 완료</div>
			<div class="menu-bar pull-right">
				<a href="/adviser/adviser.do"><img src="/img/home-pink-icon.png"
					alt=""></a>
			</div>
		</header>
		<div class="purchase-complete">
			<div class="charged">
				<span class="roboto-m"> ${point}P </span> 충전되어
			</div>
			<div class="total">
				총 <span class="roboto-m">${userPoint}P</span> 보유 중 입니다.
			</div>
			<div class="btn-holder">
				<a href="/menu/primary.do">확인</a>
			</div>
		</div>
	</div>


	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
	<script src="/js/vendor/bootstrap.min.js"></script>
	<!-- veiwport for countnumber -->
	<script src="/js/jquery.viewportchecker.js"></script>
	<script src="/js/main.js"></script>
</body>
</html>
