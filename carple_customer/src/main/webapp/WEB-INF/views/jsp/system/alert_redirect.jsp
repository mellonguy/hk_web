<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title></title>
<meta name="description" content="">
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
<!-- <meta name="viewport" content="width=720px, user-scalable=no"> -->
<link rel="stylesheet" href="/css/bootstrap.min.css">
<link rel="stylesheet" href="/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="/css/owl.carousel.min.css">
<link rel="stylesheet" href="/css/owl.theme.default.min.css">
<link rel="stylesheet" href="/css/notosanskr.css">
<link rel="stylesheet" href="/css/main.css?v=1">
<script src="/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<script src="/js/vendor/jquery-1.11.2.min.js"></script>
<c:set var="prefix" value="" />
<script src="/js/vendor/jquery-1.11.2.min.js"></script>
<script src="/js/alert.js"></script>
</head>
<body style="width: 100%;">
	<script type="text/javascript">
<c:if test="${!empty scriptMsg}">

$.alert('${scriptMsg}',function(a){
	if(a){
		location.href = '${pageContext.request.contextPath}${prefix}${redirectUrl}';
	}
});
</c:if>

/* <c:choose>
<c:when test="${fn:contains(redirectUrl, '/account/viewMonthDriverDeduct.do' )}">
	opener.window.location.reload();
  self.close();
</c:when>

<c:otherwise>
  location.href = '${pageContext.request.contextPath}${prefix}${redirectUrl}';
</c:otherwise>
</c:choose> */

</script>

</body>
</html>
