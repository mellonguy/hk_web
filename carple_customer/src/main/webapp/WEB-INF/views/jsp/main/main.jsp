<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!doctype html>
<html lang="ko">
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
<body class="black">

	<script type="text/javascript">  
    
    
    function selectInterestItem(itemCd,marketCd,obj){
    	
    	$.ajax({ 
    		type: 'post' ,
    		url : "/interest/selectInterestItem.do" ,
    		dataType : 'json' ,
    		data : {
    			itemCd : itemCd,
    			marketCd : marketCd
    		},
    		success : function(data, textStatus, jqXHR)
    		{
    			if(data.resultCode == "0000"){
    				insertInterestItem(itemCd,marketCd,obj);
    			}else{
    				deleteInterestItem(itemCd,marketCd,obj);
    			}
    		} ,
    		error : function(xhRequest, ErrorText, thrownError) {
    		}
    	}); 
    	
    	
    }    
        
    function insertInterestItem(itemCd,marketCd,obj){
    	
    	$.ajax({ 
    		type: 'post' ,
    		url : "/interest/insertInterestItem.do" ,
    		dataType : 'json' ,
    		data : {
    			itemSrtCd : itemCd,
    			marketCd : marketCd
    		},
    		success : function(data, textStatus, jqXHR)
    		{
    			if(data.resultCode == "0000"){
    				$(obj).addClass('heart-full');
    			}else if(data.resultCode == "E002"){
    					alert("로그인 되지 않음");
    			}
    		} ,
    		error : function(xhRequest, ErrorText, thrownError) {
    		}
    	}); 
    }    
        
      

    function deleteInterestItem(itemCd,marketCd,obj){
    	
    	$.ajax({ 
    		type: 'post' ,
    		url : "/interest/deleteInterestItem.do" ,
    		dataType : 'json' ,
    		data : {
    			itemSrtCd : itemCd,
    			marketCd : marketCd
    		},
    		success : function(data, textStatus, jqXHR)
    		{
    			if(data.resultCode == "0000"){
    				$(obj).removeClass('heart-full');
    			}else if(data.resultCode == "E002"){
    					alert("로그인 되지 않음");
    			}
    		} ,
    		error : function(xhRequest, ErrorText, thrownError) {
    		}
    	}); 
    }    

    
    
    
    
    
    
    
    
    
    </script>
	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->


	<div
		class="content-container withads black themedetails companydetails">
		<header class="clearfix nb">
			<div class="search-icon">
				<!-- <a style="cursor:pointer;" onclick="javascript:history.go(-1);" ><img src="/img/back-icon.png" alt=""></a> -->
				<a style="cursor: pointer;" href="/main/main.do"><img
					src="/img/back-icon.png" alt=""></a>
			</div>
			<div class="page-title txt-medium white">견적요청</div>
			<div class="menu-bar pull-right">

				<!-- <a href="#"><img src="/img/open-icon.png" alt=""></a> -->
				<!-- <a href="/adviser/adviser.do"><img src="/img/home-pink-icon.png" alt=""></a> -->
				<a href="/main/main.do"><img src="/img/delete-x-icon.png" alt=""></a>





			</div>
		</header>
		<!-- <div class="forads sz-2 clearfix">
                <div class="goal-box area-box active">
                    <div class="area-left red">
                        <span>90포인트</span>
                    </div>
                    <div class="area-right red">
                        <span>추천가 확인하기</span>
                    </div>
                </div>
                <a href="#" class="area-box"><img src="/img/ads2.png" alt=""></a>
            </div> -->
		<div class="company-details-container">
			<%-- <div class="company-name">
                    <span class="name txt-bold">${itemMap.item_name} ${itemMap.item_srt_cd}</span>
                    <!-- <a href="#" class="heart"></a> -->
                    <c:if test="${itemMap.interest_item_cd != null}">
                    	<a onclick="javascript:selectInterestItem('${itemMap.item_srt_cd}','${itemMap.market_cd}',this)" class="heart heart-full"></a>
                    </c:if>
                    <c:if test="${itemMap.interest_item_cd == null}">
                    	<a onclick="javascript:selectInterestItem('${itemMap.item_srt_cd}','${itemMap.market_cd}',this)" class="heart"></a>
                    </c:if>
                </div> --%>
			<!-- <div class="newsdetails">
                    <div class="headerarea">
                        <a href="#" class="absolute">주락펴락 시그널 3호</a>
                        <a href="#" class="text-right">5G 테마 관련 4종목 발굴</a>
                    </div>
                </div> -->
			<div class="company-views">
				<ul>
					<li class="active"><a href="">기본정보입력</a></li>
					<li><a href="">차량정보입력</a></li>
					<li><a href="">상하차지정보입력</a></li>
					<li><a href="">내용확인</a></li>
					<li><a href="">견적요청</a></li>
				</ul>
			</div>


			<div class="divider black"></div>


		</div>
	</div>




	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
	<script src="/js/vendor/bootstrap.min.js"></script>
	<!-- veiwport for countnumber -->
	<script src="/js/jquery.viewportchecker.js"></script>
	<script src="/js/main.js"></script>
</body>
</html>
