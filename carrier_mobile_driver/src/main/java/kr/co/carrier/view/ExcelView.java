package kr.co.carrier.view;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;


public class ExcelView extends AbstractExcelView {

	@Override
	protected void buildExcelDocument(Map<String, Object> model,
			SXSSFWorkbook workbook, HttpServletRequest req, HttpServletResponse res)
			throws Exception {
		Sheet sheet = workbook.createSheet();

		List<Map<String, Object>> dataList = (List<Map<String, Object>>)model.get("list");
		Map<String, String> excelMap = (Map<String, String>)model.get("excelMap");
		
		Row row = null;
        Cell cell = null;
        int r = 0;
        int c = 0;
        
        CellStyle style = workbook.createCellStyle();
        style.setFillForegroundColor(IndexedColors.SKY_BLUE.index);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        
        //Create header cells
        row = sheet.createRow(r++);
        
        Iterator<String> iteratorKey = excelMap.keySet( ).iterator( );
		while(iteratorKey.hasNext()){
			String key = iteratorKey.next();
			//System.out.println(key+","+excelMap.get(key));
	        cell = row.createCell(c++);
	        cell.setCellStyle(style);
	        cell.setCellValue(excelMap.get(key));
		}

        for ( Map<String, Object> dataMap:dataList ) {
        	row = sheet.createRow(r++);
            c = 0;
            Iterator<String> iteratorKey2 = excelMap.keySet( ).iterator( );
			while(iteratorKey2.hasNext()){
				String key = iteratorKey2.next();
				row.createCell(c++).setCellValue(dataMap.get(key)+"");
			}
        }
        
        for(int i = 0 ; i < excelMap.size(); i++)
            sheet.autoSizeColumn(i, true);


	}

}
