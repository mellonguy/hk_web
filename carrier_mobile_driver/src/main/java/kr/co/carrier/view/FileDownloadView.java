package kr.co.carrier.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.servlet.view.AbstractView;

import kr.co.carrier.service.DriverDeductInfoService;
import kr.co.carrier.service.DriverService;
import kr.co.carrier.service.PaymentInfoService;
import kr.co.carrier.utils.WebUtils;
 
public class FileDownloadView extends AbstractView {
 
	
	
	
    public FileDownloadView() {
        // 객체가 생성될 때 Content Type을 다음과 같이 변경 
        setContentType("application/download; charset=utf-8");
    }
 
    @Override
    protected void renderMergedOutputModel(
            Map<String, Object> model, 
            HttpServletRequest request, 
            HttpServletResponse response
            ) throws Exception {
         
    	
    	HttpSession session = request.getSession();
		Map userSessionMap = (Map)session.getAttribute("user");
    	
    	
        Map<String, Object> fileInfo = (Map<String, Object>) model.get("downloadFile"); // 넘겨받은 모델(파일 정보)
         
        String fileUploadPath = (String) fileInfo.get("fileUploadPath");    // 파일 업로드 경로
        String fileLogicName = (String) fileInfo.get("fileLogicName");      // 파일 논리명(화면에 표시될 파일 이름)
        String filePhysicName = (String) fileInfo.get("filePhysicName");    // 파일 물리명(실제 저장된 파일 이름)
         
        //filePhysicName="deductlist.xlsx";
        //fileLogicName = "deductlist.xlsx";
        //System.out.println("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n1111111111111111111111");
        //System.out.println("11111111111111111111111111111111");
        //System.out.println(fileUploadPath);
        //System.out.println("11111111111111111111111111111111");
        //System.out.println("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n11111111111111111111111");
        
        
        
        File file = new File(fileUploadPath, filePhysicName);
        
        response.setContentType(getContentType());
        response.setContentLength((int) file.length());
        String userAgent = request.getHeader("User-Agent");
        boolean ie = userAgent.indexOf("MSIE") > -1;
        String fileName = null;
 
        if(ie) {
            fileName = URLEncoder.encode(fileLogicName, "utf-8");
        } else {
            fileName = new String(fileLogicName.getBytes("utf-8"), "iso-8859-1");
        }
 
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
        response.setHeader("Content-Transfer-Encoding", "binary");
        OutputStream out = response.getOutputStream();
       
        FileInputStream fis = null;
 
        try {
            fis = new FileInputStream(file);
            FileCopyUtils.copy(fis, out);
        } finally {
            if(fis != null)
                try {
                    fis.close();
                } catch(IOException ex) {
                }
        }
        out.flush();
    }
}
