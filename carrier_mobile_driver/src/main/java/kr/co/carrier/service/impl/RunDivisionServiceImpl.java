package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.RunDivisionMapper;
import kr.co.carrier.service.RunDivisionService;

@Service("runDivisionService")
public class RunDivisionServiceImpl implements RunDivisionService{

	
	@Resource(name="runDivisionMapper")
	private RunDivisionMapper runDivisionMapper;
	
	
	public List<Map<String, Object>>selectRunDivisionInfoList() throws Exception{
		return runDivisionMapper.selectRunDivisionInfoList();
	}
}
