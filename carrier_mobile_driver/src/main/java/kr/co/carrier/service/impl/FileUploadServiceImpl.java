package kr.co.carrier.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileUploadException;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import kr.co.carrier.service.FileUploadService;
import kr.co.carrier.utils.WebUtils;
import net.sourceforge.tess4j.Tesseract;

/**
 * <pre>
 * Club Class
 * </pre>
 * 
 * @ClassName   : FileUploadServiceImpl.java
 * @Description : 클래스 설명을 기술합니다.
 * @author ymajng 
 * @since 2016.1.19
 * @version 1.0
 * @see kr.co.baseball.heroes.admin.service.impl.FileUploadServiceImpl
 * @Modification Information
 * 
 * <pre>
 *     since          author              description
 *  ===========    =============    ===========================
 *  2016.1.19      ymajng        최초생성
 * </pre>
 */
@Service("fileUploadService")
public class FileUploadServiceImpl implements FileUploadService {

    //@Value("#{appProp['upload.base.path1']}")
    //private String uploadBasePath1;
    
    //@Value("#{appProp['upload.base.path2']}")
    //private String uploadBasePath2;
	
    @Value("#{appProp['upload.file.ext']}")
    private String uploadFileExt;
    
    @Value("#{appProp['upload.image.ext']}")
    private String uploadImageExt;

    /*
     * @see kr.co.baseball.heroes.admin.service.FileUploadService#upload(String subDir, MultipartFile mf)
     */
	public Map<String, Object> upload(String rootDir, String subDir, MultipartFile mf) throws Exception
	{		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		Long fileSize = mf.getSize();
		String fileName = mf.getOriginalFilename();		
		String ext = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length());
		
		//System.out.println("=========================1 " + ext + " / " + isRightImageExt(ext));
	
		//확장자 검사
		if(isRightFileExt(ext) == false)
			throw new FileUploadException("업로드 불가 파일입니다.");
		else
		{	
			String uploadDir = rootDir + "/" + subDir;
			String saveName = "/" + WebUtils.getNow("yyyy") + "/" + WebUtils.getNow("MM") + "/" + WebUtils.getNow("dd");
			String newFilename = getFilename(ext);
			
			//System.out.println("=========================1 " + uploadDir);
			//System.out.println("=========================1 " + saveName);
			//System.out.println("=========================1 " + newFilename);			
						
			File saveDir = new File(uploadDir + saveName);
			
			if(saveDir.exists() == false){
				saveDir.mkdirs();
			}
				
			saveName = saveName + "/" + newFilename;
			
			//mf.transferTo(new File(uploadDir + saveName));
			
			File newFile = new File(uploadDir + saveName);
			
			mf.transferTo(newFile);
			
			//final Tesseract instance =  new Tesseract();
			//instance.setLanguage("eng");
			//System.out.println(instance.doOCR(newFile));
			
			
			resultMap.put("FILE_SIZE", fileSize);
			resultMap.put("ORG_NAME", fileName);
			resultMap.put("SAVE_NAME", saveName);
		}
		
		return resultMap;
	}

	
		
	
	public Map<String, Object> upload(String rootDir, String subDir,String fileName,XSSFWorkbook wb) throws Exception
	{		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		
		
		try {
			
			String ext = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length());
			
			//System.out.println("=========================1 " + ext + " / " + isRightImageExt(ext));
		
			//확장자 검사
			if(isRightFileExt(ext) == false)
				throw new FileUploadException("업로드 불가 파일입니다.");
			else
			{	
				String uploadDir = rootDir + "/" + subDir;
				String saveName = "/" + WebUtils.getNow("yyyy") + "/" + WebUtils.getNow("MM") + "/" + WebUtils.getNow("dd");
				String newFilename = getFilename(ext);
				
				//System.out.println("=========================1 " + uploadDir);
				//System.out.println("=========================1 " + saveName);
				//System.out.println("=========================1 " + newFilename);			
							
				File saveDir = new File(uploadDir + saveName);
				
				if(saveDir.exists() == false){
					saveDir.mkdirs();
				}
					
				saveName = saveName + "/" + newFilename;
				
				FileOutputStream outputStream = new FileOutputStream(new File(uploadDir + saveName));
				
	        	wb.write(outputStream);
				wb.close();
				
				
				resultMap.put("ORG_NAME", fileName);
				resultMap.put("SAVE_NAME", saveName);
			}
			
			
		}catch(Exception e) {
			
			e.printStackTrace();
			
		}
		
		return resultMap;
	}
	
	
	
	
	
	
	
	
	
	
	
    /*
     * @see kr.co.baseball.heroes.admin.service.FileUploadService#uploadImage(String subDir, MultipartFile mf)
     */
	public Map<String, Object> uploadImage(String rootDir, String subDir, MultipartFile mf) throws Exception
	{
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		Long fileSize = mf.getSize();
		String fileName = mf.getOriginalFilename();		
		String ext = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length());
		
		//System.out.println("=========================2 " + ext + " / " + isRightImageExt(ext));
		
	
		//이미지 확장자 검사
		if(isRightImageExt(ext) == false)
			throw new FileUploadException("업로드 불가 파일입니다.");
		else
		{	
			String uploadDir = rootDir + "/" + subDir;
			String saveName = "/" + WebUtils.getNow("yyyy") + "/" + WebUtils.getNow("MM") + "/" + WebUtils.getNow("dd");
			String newFilename = getFilename(ext);
			
			//System.out.println("=========================2 " + uploadDir);
			//System.out.println("=========================2 " + saveName);
			//System.out.println("=========================2 " + newFilename);			
						
			File saveDir = new File(uploadDir + saveName);
			
			if(saveDir.exists() == false)			
				saveDir.mkdirs();
			
			saveName = saveName + "/" + newFilename;
			
			mf.transferTo(new File(uploadDir + saveName));
			
			resultMap.put("FILE_SIZE", fileSize);
			resultMap.put("ORG_NAME", fileName);
			resultMap.put("SAVE_NAME", saveName);
		}
		
		return resultMap;
	}
    
	public String getFilename(String fileExt)
	{	
		return UUID.randomUUID().toString() + "." + fileExt;
	}
	
	public boolean isRightFileExt(String fileExt)
	{	
		boolean result = false;
		String[] exts = uploadFileExt.split(",");
		
		//System.out.println("=========================1 " + uploadFileExt);
		
		for(String ext : exts)
		{
			if(ext.equals(fileExt.toLowerCase()))
			{
				result = true;
				break;
			}
		}
		
		return isRightImageExt(fileExt) || result;
	}
	
	public boolean isRightImageExt(String fileExt)
	{	
		boolean result = false;
		String[] exts = uploadImageExt.split(",");
		
		//System.out.println("=========================2 " + uploadImageExt);
		
		for(String ext : exts)
		{
			//System.out.println("=========================2 " + ext);
			
			if(ext.equals(fileExt.toLowerCase()))
			{
				result = true;
				break;
			}
		}
		
		return result;
	}
	
	public List<Map<String, Object>> uploadList(HttpServletRequest request, String rootDir, String subDir) throws Exception
	{
		ArrayList<Map<String, Object>> resutlList = new ArrayList<Map<String, Object>>();
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
		
		String seq[] = multipartRequest.getParameterValues("file_seq");
		String del[] = multipartRequest.getParameterValues("file_del_yn");
		String content[] = multipartRequest.getParameterValues("file_content");
		
		List<MultipartFile> fileList = multipartRequest.getFiles("multi_file");
		
		if(seq != null && seq.length > 0)
		{			
			for(int i=0; i<seq.length; i++)
			{	
				//System.out.println("=========================U "+i+" : " + seq[i]);
				//System.out.println("=========================U "+i+" : " + del[i]);
				//System.out.println("=========================U "+i+" : " + content[i]);
				
				//삭제 대상
				if("Y".equals(del[i]))
				{	
					Map<String, Object> resultMap = new HashMap<String, Object>();
					
					resultMap.put("FILE_MODE", "DEL");
					resultMap.put("FILE_SEQ", seq[i]);
					
					resutlList.add(resultMap);
				}
				//등록,수정
				else
				{
					Map<String, Object> resultMap = new HashMap<String, Object>();				
					MultipartFile mf = fileList.get(i);					
					
					if(mf != null && mf.getSize() > 0)
					{
						//파일 업로드
						resultMap = upload(rootDir, subDir, mf);
						
						//수정
						if(seq[i] != null && !"".equals(seq[i]))
						{
							resultMap.put("FILE_MODE", "MOD");
							resultMap.put("FILE_SEQ", seq[i]);
							resultMap.put("FILE_CONTENT", content[i]);
						}
						else
						{
							resultMap.put("FILE_MODE", "ADD");
							resultMap.put("FILE_CONTENT", content[i]);
						}					

					}
					//내용 수정 only
					else
					{
						resultMap.put("FILE_MODE", "MOD");
						resultMap.put("FILE_SEQ", seq[i]);
						resultMap.put("FILE_CONTENT", content[i]);
						
						resultMap.put("FILE_SIZE", "");
						resultMap.put("ORG_NAME", "");
						resultMap.put("SAVE_NAME", "");
					}
					
					//System.out.println("=========================I "+i+" : " + resultMap.get("FILE_NAME"));
					//System.out.println("=========================I "+i+" : " + content[i + updateIdx]);
					
					resutlList.add(resultMap);
				}
			}
		}
		
		return resutlList;
	}
	
	public List<Map<String, Object>> uploadImageList(HttpServletRequest request, String rootDir, String subDir) throws Exception
	{
		ArrayList<Map<String, Object>> resutlList = new ArrayList<Map<String, Object>>();
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
		
		String seq[] = multipartRequest.getParameterValues("file_seq");
		String del[] = multipartRequest.getParameterValues("file_del_yn");
		String content[] = multipartRequest.getParameterValues("file_content");
		
		List<MultipartFile> fileList = multipartRequest.getFiles("multi_file");
		
		if(seq != null && seq.length > 0)
		{			
			for(int i=0; i<seq.length; i++)
			{	
				//System.out.println("=========================U "+i+" : " + seq[i]);
				//System.out.println("=========================U "+i+" : " + del[i]);
				//System.out.println("=========================U "+i+" : " + content[i]);
				
				//삭제 대상
				if("Y".equals(del[i]))
				{	
					Map<String, Object> resultMap = new HashMap<String, Object>();
					
					resultMap.put("FILE_MODE", "DEL");
					resultMap.put("FILE_SEQ", seq[i]);
					
					resutlList.add(resultMap);
				}
				//등록,수정
				else
				{
					Map<String, Object> resultMap = new HashMap<String, Object>();				
					MultipartFile mf = fileList.get(i);					
					
					if(mf != null && mf.getSize() > 0)
					{
						//파일 업로드
						resultMap = uploadImage(rootDir, subDir, mf);
						
						//수정
						if(seq[i] != null && !"".equals(seq[i]))
						{
							resultMap.put("FILE_MODE", "MOD");
							resultMap.put("FILE_SEQ", seq[i]);
							resultMap.put("FILE_CONTENT", content[i]);
						}
						else
						{
							resultMap.put("FILE_MODE", "ADD");
							resultMap.put("FILE_CONTENT", content[i]);
						}					

					}
					//내용 수정 only
					else
					{
						resultMap.put("FILE_MODE", "MOD");
						resultMap.put("FILE_SEQ", seq[i]);
						resultMap.put("FILE_CONTENT", content[i]);
						
						resultMap.put("FILE_SIZE", "");
						resultMap.put("ORG_NAME", "");
						resultMap.put("SAVE_NAME", "");
					}
					
					//System.out.println("=========================I "+i+" : " + resultMap.get("FILE_NAME"));
					//System.out.println("=========================I "+i+" : " + content[i + updateIdx]);
					
					resutlList.add(resultMap);
				}
			}
		}
		
		return resutlList;
	}
}
