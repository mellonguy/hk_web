package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.CarInfoMapper;
import kr.co.carrier.service.CarInfoService;
import kr.co.carrier.vo.CarInfoVO;

@Service("carInfoService")
public class CarInfoServiceImpl implements CarInfoService {

	
	@Resource(name="carInfoMapper")
	private CarInfoMapper carInfoMapper;
	
	public Map<String, Object> selectCarInfo(Map<String, Object> map) throws Exception{
		return carInfoMapper.selectCarInfo(map);
	}
	
	public List<Map<String, Object>> selectCarInfoList(Map<String, Object> map) throws Exception{
		return carInfoMapper.selectCarInfoList(map);
	}
	
	public int selectCarInfoListCount(Map<String, Object> map) throws Exception{
		return carInfoMapper.selectCarInfoListCount(map);
	}
	
	public int insertCarInfo(CarInfoVO carInfoVO) throws Exception{
		return carInfoMapper.insertCarInfo(carInfoVO);
	}
	
	public void updateCarInfo(CarInfoVO carInfoVO) throws Exception{
		carInfoMapper.updateCarInfo(carInfoVO);
	}
	
	public void deleteCarInfo(Map<String, Object> map) throws Exception{
		carInfoMapper.deleteCarInfo(map);
	}
	
	public void updateCarInfoDriver(Map<String, Object> map) throws Exception{
		carInfoMapper.updateCarInfoDriver(map);
	}
	
	public void updateCarKind(Map<String, Object> map) throws Exception{
		carInfoMapper.updateCarKind(map);
	}
	
	public void updateCarIdNum(Map<String, Object> map) throws Exception{
		carInfoMapper.updateCarIdNum(map);
	}
	
	public void updateCarNum(Map<String, Object> map) throws Exception{
		carInfoMapper.updateCarNum(map);
	}
	
	
	public void updateCarInfoDriverEtc(Map<String, Object> map) throws Exception{
		carInfoMapper.updateCarInfoDriverEtc(map);
	}
	
}
