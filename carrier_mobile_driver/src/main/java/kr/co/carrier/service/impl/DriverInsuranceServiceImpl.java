package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.DriverInsuranceMapper;
import kr.co.carrier.service.DriverInsuranceService;
import kr.co.carrier.vo.DriverInsuranceVO;

@Service("driverInsuranceService")
public class DriverInsuranceServiceImpl implements DriverInsuranceService{

	
	@Resource(name="driverInsuranceMapper")
	private DriverInsuranceMapper driverInsuranceMapper;
	
	
	public Map<String, Object> selectDriverInsurance(Map<String, Object> map) throws Exception{
		return driverInsuranceMapper.selectDriverInsurance(map);
	}
	
	public List<Map<String, Object>> selectDriverInsuranceList(Map<String, Object> map) throws Exception{
		return driverInsuranceMapper.selectDriverInsuranceList(map);
	}
	
	public int selectDriverInsuranceCount(Map<String, Object> map) throws Exception{
		return driverInsuranceMapper.selectDriverInsuranceCount(map);
	}
	
	public int insertDriverInsurance(DriverInsuranceVO driverInsuranceVO) throws Exception{
		return driverInsuranceMapper.insertDriverInsurance(driverInsuranceVO);
	}
	
	public void updateDriverInsurance(DriverInsuranceVO driverInsuranceVO) throws Exception{
		driverInsuranceMapper.updateDriverInsurance(driverInsuranceVO);
	}
	
	
	
	
	
	
	
	
}
