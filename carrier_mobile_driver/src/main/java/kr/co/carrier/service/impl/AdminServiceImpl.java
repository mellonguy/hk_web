package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.AdminMapper;
import kr.co.carrier.service.AdminService;
import kr.co.carrier.vo.AdminVO;

@Service("adminService")
public class AdminServiceImpl implements AdminService{
	
	@Resource(name="adminMapper")
	private AdminMapper adminMapper;
	
	public Map<String, Object> selectAdmin(Map<String, Object> map) throws Exception{
		return adminMapper.selectAdmin(map);
	}
	
	public List<Map<String, Object>> selectAdminList(Map<String, Object> map) throws Exception{
		return adminMapper.selectAdminList(map);
	}
	
	public int selectAdminListCount(Map<String, Object> map) throws Exception{
		return adminMapper.selectAdminListCount(map);
	}
	
	public int insertAdmin(AdminVO adminVO) throws Exception{
		return adminMapper.insertAdmin(adminVO);
	}
	
	public void updateAdminAccessDt(Map<String, Object> map) throws Exception{
		adminMapper.updateAdminAccessDt(map);
	}
	
	public void updateAdmin(AdminVO adminVO) throws Exception{
		adminMapper.updateAdmin(adminVO);
	}
	
	
}
