package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.AllocationMapper;
import kr.co.carrier.service.AllocationService;
import kr.co.carrier.vo.AllocationVO;

@Service("allocationService")
public class AllocationServiceImpl implements AllocationService{

	@Resource(name="allocationMapper")
	private AllocationMapper allocationMapper;
	
	
	public Map<String, Object> selectAllocation(Map<String, Object> map) throws Exception{
		return allocationMapper.selectAllocation(map);
	}
	
	public List<Map<String, Object>> selectAllocationList(Map<String, Object> map) throws Exception{
		return allocationMapper.selectAllocationList(map);
	}
	
	public int selectAllocationListCount(Map<String, Object> map) throws Exception{
		return allocationMapper.selectAllocationListCount(map);
	}
	
	public int insertAllocation(AllocationVO allocationVO) throws Exception{
		return allocationMapper.insertAllocation(allocationVO);
	}
	
	public void updateAllocationListOrder(Map<String, Object> map) throws Exception{
		allocationMapper.updateAllocationListOrder(map);
	}
	
	public void updateAllocation(AllocationVO allocationVO) throws Exception{
		allocationMapper.updateAllocation(allocationVO);
	}
	
	public Map<String, Object> selectAllocationByListOrder(Map<String, Object> map) throws Exception{
		return allocationMapper.selectAllocationByListOrder(map);
	}
	
	public void updateAllocationByMap(Map<String, Object> map) throws Exception{
		allocationMapper.updateAllocationByMap(map);
	}
	
	public void updateAllocationStatus(Map<String, Object> map) throws Exception{
		allocationMapper.updateAllocationStatus(map);
	}
	
	public int selectAllocationMainListCount(Map<String, Object> map) throws Exception{
		return allocationMapper.selectAllocationMainListCount(map);
	}
	
	public void updateAllocationForSendReceipt(Map<String, Object> map) throws Exception{
		allocationMapper.updateAllocationForSendReceipt(map);
	}
	
	public void updateAllocationReceiptSkip(Map<String, Object> map) throws Exception{
		allocationMapper.updateAllocationReceiptSkip(map);
	}
	
	public void updateAllocationReceiptSkipReason(Map<String, Object> map) throws Exception{
		allocationMapper.updateAllocationReceiptSkipReason(map);
	}
	
	public List<Map<String, Object>> selectAllocationListGroup(Map<String, Object> map) throws Exception{
		return allocationMapper.selectAllocationListGroup(map);
	}
	
	public List<Map<String, Object>> selectAllocationListByDepartureDtAndDriverCnt(Map<String, Object> map) throws Exception{
		return allocationMapper.selectAllocationListByDepartureDtAndDriverCnt(map);
	}
	
	public List<Map<String, Object>> selectAllocationListForCal(Map<String, Object> map) throws Exception{
		return allocationMapper.selectAllocationListForCal(map);
	}
	
	public void updateAllocationPicturePointerR(Map<String, Object> map) throws Exception{
		allocationMapper.updateAllocationPicturePointerR(map);
	}
	public void updateAllocationPicturePointerD(Map<String, Object> map) throws Exception{
		allocationMapper.updateAllocationPicturePointerD(map);
	}
	public void updateAllocationPicturePointerI(Map<String, Object> map) throws Exception{
		allocationMapper.updateAllocationPicturePointerI(map);
	}
	
	public int selectAllocationCountByPicturePointer(Map<String, Object> map) throws Exception{
		return allocationMapper.selectAllocationCountByPicturePointer(map);
	}
	
	public List<Map<String, Object>> selectAllocationListByBatchStatusId(Map<String, Object> map) throws Exception{
		return allocationMapper.selectAllocationListByBatchStatusId(map);
	}
	
	public void updateMissingSend (Map<String,Object>map) throws Exception{
		
		allocationMapper.updateMissingSend(map);
	}
	public List<Map<String, Object>> selectGetBatchStausIdForPickUp(Map<String, Object> map) throws Exception{
		return allocationMapper.selectGetBatchStausIdForPickUp(map);
	}
}
