package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.SalesPurchaseDivisionMapper;
import kr.co.carrier.service.SalesPurchaseDivisionService;

@Service("salesPurchaseDivisionService")
public class SalesPurchaseDivisionServiceImpl implements SalesPurchaseDivisionService{

	
	@Resource(name="salesPurchaseDivisionMapper")
	private SalesPurchaseDivisionMapper salesPurchaseDivisionMapper;
	
	
	public List<Map<String, Object>>selectSalesPurchaseDivisionInfoList() throws Exception{
	
		return salesPurchaseDivisionMapper.selectSalesPurchaseDivisionInfoList();
		
	}
	
	
	
	
	
}
