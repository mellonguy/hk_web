package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.PaymentDivisionMapper;
import kr.co.carrier.service.PaymentDivisionService;

@Service("paymentDivisionService")
public class PaymentDivisionServiceImpl implements PaymentDivisionService{

	
	@Resource(name="paymentDivisionMapper")
	private PaymentDivisionMapper paymentDivisionMapper;
	
	
	public List<Map<String, Object>>selectPaymentDivisionInfoList() throws Exception{
		return paymentDivisionMapper.selectPaymentDivisionInfoList();
	}
}
