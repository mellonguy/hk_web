package kr.co.carrier.service;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.AllocationVO;

public interface AllocationService {

	public Map<String, Object> selectAllocation(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectAllocationList(Map<String, Object> map) throws Exception;
	public int selectAllocationListCount(Map<String, Object> map) throws Exception;
	public int insertAllocation(AllocationVO allocationVO) throws Exception;
	public void updateAllocationListOrder(Map<String, Object> map) throws Exception;
	public void updateAllocation(AllocationVO allocationVO) throws Exception;
	public Map<String, Object> selectAllocationByListOrder(Map<String, Object> map) throws Exception;
	public void updateAllocationByMap(Map<String, Object> map) throws Exception;
	public void updateAllocationStatus(Map<String, Object> map) throws Exception;
	public int selectAllocationMainListCount(Map<String, Object> map) throws Exception;
	public void updateAllocationForSendReceipt(Map<String, Object> map) throws Exception;
	public void updateAllocationReceiptSkip(Map<String, Object> map) throws Exception;
	public void updateAllocationReceiptSkipReason(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectAllocationListGroup(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectAllocationListByDepartureDtAndDriverCnt(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectAllocationListForCal(Map<String, Object> map) throws Exception;
	public void updateAllocationPicturePointerR(Map<String, Object> map) throws Exception;
	public void updateAllocationPicturePointerD(Map<String, Object> map) throws Exception;
	public void updateAllocationPicturePointerI(Map<String, Object> map) throws Exception;
	public int selectAllocationCountByPicturePointer(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectAllocationListByBatchStatusId(Map<String, Object> map) throws Exception;
	public void updateMissingSend (Map<String,Object>map) throws Exception;
	public List<Map<String, Object>> selectGetBatchStausIdForPickUp(Map<String, Object> map) throws Exception;
	
}
