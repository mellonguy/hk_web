package kr.co.carrier.service;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.AdminVO;

public interface AdminService {

	public Map<String, Object> selectAdmin(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectAdminList(Map<String, Object> map) throws Exception;
	public int selectAdminListCount(Map<String, Object> map) throws Exception;
	public int insertAdmin(AdminVO adminVO) throws Exception;
	public void updateAdminAccessDt(Map<String, Object> map) throws Exception;
	public void updateAdmin(AdminVO adminVO) throws Exception;
	
	
	
	
	
}
