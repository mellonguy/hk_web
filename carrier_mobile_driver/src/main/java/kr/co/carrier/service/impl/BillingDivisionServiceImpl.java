package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.BillingDivisionMapper;
import kr.co.carrier.service.BillingDivisionService;

@Service("billingDivisionService")
public class BillingDivisionServiceImpl implements BillingDivisionService{

	
	@Resource(name="billingDivisionMapper")
	private BillingDivisionMapper billingDivisionMapper;
	
	
	public List<Map<String, Object>>selectBillingDivisionInfoList() throws Exception{
		return billingDivisionMapper.selectBillingDivisionInfoList();
	}
}
