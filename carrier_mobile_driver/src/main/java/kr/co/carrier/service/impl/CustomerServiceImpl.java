package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.CustomerMapper;
import kr.co.carrier.service.CustomerService;
import kr.co.carrier.vo.CustomerVO;

@Service("customerService")
public class CustomerServiceImpl implements CustomerService{

	
	@Resource(name="customerMapper")
	private CustomerMapper customerMapper;
	
	public Map<String, Object> selectCustomer(Map<String, Object> map) throws Exception{
		return customerMapper.selectCustomer(map);
	}
	
	public List<Map<String, Object>> selectCustomerList(Map<String, Object> map) throws Exception{
		return customerMapper.selectCustomerList(map);
	}
	
	public int selectCustomerListCount(Map<String, Object> map) throws Exception{
		return customerMapper.selectCustomerListCount(map);
	}
	
	public int insertCustomer(CustomerVO customerVO) throws Exception{
		return customerMapper.insertCustomer(customerVO);
	}
	
	public void updateCustomer(CustomerVO customerVO) throws Exception{
		customerMapper.updateCustomer(customerVO);
	}
	
	public List<Map<String, Object>> selectChargeList(Map<String, Object> map) throws Exception{
		return customerMapper.selectChargeList(map);
	}

	public Map <String,Object>selectChargePhoneNum (Map<String,Object>map)throws Exception{
		return customerMapper.selectChargePhoneNum(map);
	}
}

