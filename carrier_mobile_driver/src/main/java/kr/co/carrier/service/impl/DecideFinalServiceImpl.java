package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.DecideFinalMapper;
import kr.co.carrier.service.DecideFinalService;
import kr.co.carrier.vo.DecideFinalVO;

@Service("decideFinalService")
public class DecideFinalServiceImpl implements DecideFinalService {

	
	
	@Resource(name="decideFinalMapper")
	private DecideFinalMapper decideFinalMapper;
	
	
	public Map<String, Object> selectDecideFinal(Map<String, Object> map) throws Exception{
		return decideFinalMapper.selectDecideFinal(map);
	}
	
	public List<Map<String, Object>> selectDecideFinalList(Map<String, Object> map) throws Exception{
		return decideFinalMapper.selectDecideFinalList(map);
	}
	
	public int selectDecideFinalListCount(Map<String, Object> map) throws Exception{
		return decideFinalMapper.selectDecideFinalListCount(map);
	}
	
	public int insertDecideFinal(DecideFinalVO decideFinalVO) throws Exception{
		return decideFinalMapper.insertDecideFinal(decideFinalVO);
	}
	
	public void deleteDecideFinal(Map<String, Object> map) throws Exception{
		decideFinalMapper.deleteDecideFinal(map);
	}
	
	public void updateDecideFinalStatus(Map<String, Object> map) throws Exception{
		decideFinalMapper.updateDecideFinalStatus(map);
	}
	
	public Map<String, Object> selectDecideFinalByDecideMonth(Map<String, Object> map) throws Exception{
		return decideFinalMapper.selectDecideFinalByDecideMonth(map);
	}
	
	
	
	
	
	
}
