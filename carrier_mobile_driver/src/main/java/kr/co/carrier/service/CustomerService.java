package kr.co.carrier.service;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.CustomerVO;

public interface CustomerService {

	
	
	
	public Map<String, Object> selectCustomer(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectCustomerList(Map<String, Object> map) throws Exception;
	public int selectCustomerListCount(Map<String, Object> map) throws Exception;
	public int insertCustomer(CustomerVO customerVO) throws Exception;
	public void updateCustomer(CustomerVO customerVO) throws Exception;
	public List<Map<String, Object>> selectChargeList(Map<String, Object> map) throws Exception;
	public Map <String,Object>selectChargePhoneNum (Map<String,Object>map)throws Exception;
	
	
	
}
