package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.PayDivisionMapper;
import kr.co.carrier.service.PayDivisionService;

@Service("payDivisionService")
public class PayDivisionServiceImpl implements PayDivisionService{

	
	@Resource(name="payDivisionMapper")
	private PayDivisionMapper payDivisionMapper;
	
	
	public List<Map<String, Object>>selectPayDivisionInfoList() throws Exception{
		return payDivisionMapper.selectPayDivisionInfoList();
	}
}
