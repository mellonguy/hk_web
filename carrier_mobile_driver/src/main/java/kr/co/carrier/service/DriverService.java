package kr.co.carrier.service;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.DriverVO;

public interface DriverService {

	
	public Map<String, Object> selectDriver(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectDriverList(Map<String, Object> map) throws Exception;
	public int selectDriverListCount(Map<String, Object> map) throws Exception;
	public int insertDriver(DriverVO driverVO) throws Exception;
	public void updateDriver(DriverVO driverVO) throws Exception;
	public void updateDriverDeviceToken(Map<String, Object> map) throws Exception;
	public void updateDriverLatLng(Map<String, Object> map) throws Exception;
	public void updateDriverPassWord(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectAllEmployee(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectAllDriver(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectAllEmp(Map<String, Object> map) throws Exception;
	public int selectDriverIdDuplicationCheck(Map<String, Object> map) throws Exception;	
	
	
}
