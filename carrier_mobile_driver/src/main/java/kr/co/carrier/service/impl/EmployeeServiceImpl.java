package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.EmployeeMapper;
import kr.co.carrier.service.EmployeeService;
import kr.co.carrier.vo.EmployeeVO;

@Service("employeeService")
public class EmployeeServiceImpl implements EmployeeService{

	@Resource(name="employeeMapper")
	private EmployeeMapper employeeMapper;
	
	
	public Map<String, Object> selectEmployee(Map<String, Object> map) throws Exception{
		return employeeMapper.selectEmployee(map);
	}
	
	public List<Map<String, Object>> selectEmployeeList(Map<String, Object> map) throws Exception{
		return employeeMapper.selectEmployeeList(map);
	}
	
	public int selectEmployeeListCount(Map<String, Object> map) throws Exception{
		return employeeMapper.selectEmployeeListCount(map);
	}
	
	public int insertEmployee(EmployeeVO employeeVO) throws Exception{
		return employeeMapper.insertEmployee(employeeVO);
	}
	
	public void updateEmployee(EmployeeVO employeeVO) throws Exception{
		employeeMapper.updateEmployee(employeeVO);
	}
	
	public void updateEmployeeApproveYn(Map<String, Object> map) throws Exception{
		employeeMapper.updateEmployeeApproveYn(map);
	}
	
	public void updateEmployeeRole(Map<String, Object> map) throws Exception{
		employeeMapper.updateEmployeeRole(map);
	}
	
	public void updateEmployeeAllocation(Map<String, Object> map) throws Exception{
		employeeMapper.updateEmployeeAllocation(map);
	}
	
	public void updateEmployeeStatus(Map<String, Object> map) throws Exception{
		employeeMapper.updateEmployeeStatus(map);
	}
	
	
	
	
}
