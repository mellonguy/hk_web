package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.AdjustmentMapper;
import kr.co.carrier.service.AdjustmentService;

@Service("adjustmentService")
public class AdjustmentServiceImpl implements AdjustmentService{

	
	@Resource(name="adjustmentMapper")
	private AdjustmentMapper adjustmentMapper;
	
	
	
	public List<Map<String, Object>> selectCustomerList(Map<String, Object> map) throws Exception{
		return adjustmentMapper.selectCustomerList(map);
	}
	
	public int selectCustomerListCount(Map<String, Object> map) throws Exception{
		return adjustmentMapper.selectCustomerListCount(map);
	}
	
	public List<Map<String, Object>> selectAllocationListByCustomerId(Map<String, Object> map) throws Exception{
		return adjustmentMapper.selectAllocationListByCustomerId(map);
	}
	
	public int selectAllocationCountByCustomerId(Map<String, Object> map) throws Exception{
		return adjustmentMapper.selectAllocationCountByCustomerId(map);
	}
	
	public Map<String, Object> selectAllocationByAllocationId(Map<String, Object> map) throws Exception{
		return adjustmentMapper.selectAllocationByAllocationId(map);
	}
	
	public Map<String, Object> selectTotalStatistics(Map<String, Object> map) throws Exception{
		return adjustmentMapper.selectTotalStatistics(map);
	}
	
	public List<Map<String, Object>> selectAllocationListByCustomerIdForBillPublish(Map<String, Object> map) throws Exception{
		return adjustmentMapper.selectAllocationListByCustomerIdForBillPublish(map);
	}
	
	public void updateCarInfoListForBillPublish(Map<String, Object> map) throws Exception{
		adjustmentMapper.updateCarInfoListForBillPublish(map);
	}
	
	public void updateCarInfoForBillPublish(Map<String, Object> map) throws Exception{
		adjustmentMapper.updateCarInfoForBillPublish(map);
	}
	
	public void updateCarInfoBillingStatus(Map<String, Object> map) throws Exception{
		adjustmentMapper.updateCarInfoBillingStatus(map);
	}

	public int selectAllocationCountBybillPublishRequestId(Map<String, Object> map) throws Exception{
		return adjustmentMapper.selectAllocationCountBybillPublishRequestId(map);
	}
	public List<Map<String, Object>> selectAllocationListBybillPublishRequestId(Map<String, Object> map) throws Exception{
		return adjustmentMapper.selectAllocationListBybillPublishRequestId(map);
	}
	
	public int selectAnotherListCount(Map<String, Object> map) throws Exception{
		return adjustmentMapper.selectAnotherListCount(map);
	}
	public List<Map<String, Object>> selectAnotherList(Map<String, Object> map) throws Exception{
		return adjustmentMapper.selectAnotherList(map);
	}
	
	public int selectAllocationCountByPaymentPartnerId(Map<String, Object> map) throws Exception{
		return adjustmentMapper.selectAllocationCountByPaymentPartnerId(map);
	}
	
	public List<Map<String, Object>> selectAllocationListByPaymentPartnerId(Map<String, Object> map) throws Exception{
		return adjustmentMapper.selectAllocationListByPaymentPartnerId(map);
	}
	
	public Map<String, Object> selectAnotherTotalStatistics(Map<String, Object> map) throws Exception{
		return adjustmentMapper.selectAnotherTotalStatistics(map);
	}
	
	public int selectFinalListCount(Map<String, Object> map) throws Exception{
		return adjustmentMapper.selectFinalListCount(map);
	}
	
	public List<Map<String, Object>> selectFinalList(Map<String, Object> map) throws Exception{
		return adjustmentMapper.selectFinalList(map);
	}
	
	public Map<String, Object> selectFinalTotalStatistics(Map<String, Object> map) throws Exception{
		return adjustmentMapper.selectFinalTotalStatistics(map);
	}
	
	public int selectAllocationCountByPaymentPartnerIdAndDecideMonth(Map<String, Object> map) throws Exception{
		return adjustmentMapper.selectAllocationCountByPaymentPartnerIdAndDecideMonth(map);
	}
	
	public List<Map<String, Object>> selectAllocationListByPaymentPartnerIdAndDecideMonth(Map<String, Object> map) throws Exception{
		return adjustmentMapper.selectAllocationListByPaymentPartnerIdAndDecideMonth(map);
	}
	
	public List<Map<String, Object>> selectAnotherListForDecideFinal(Map<String, Object> map) throws Exception{
		return adjustmentMapper.selectAnotherListForDecideFinal(map);
	}
	
	
	public void updateCarInfoDebtorCreditorId(Map<String, Object> map) throws Exception{
		adjustmentMapper.updateCarInfoDebtorCreditorId(map);
	}
	
	public void updateCarInfoDebtorCreditorIdByBillPublishRequstId(Map<String, Object> map) throws Exception{
		adjustmentMapper.updateCarInfoDebtorCreditorIdByBillPublishRequstId(map);
	}
	
	
	
	
}
