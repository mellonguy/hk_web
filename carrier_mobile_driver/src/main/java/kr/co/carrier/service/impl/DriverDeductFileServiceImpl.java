package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.DriverDeductFileMapper;
import kr.co.carrier.service.DriverDeductFileService;
import kr.co.carrier.service.DriverDeductInfoService;
import kr.co.carrier.service.DriverService;
import kr.co.carrier.service.FileUploadService;
import kr.co.carrier.service.PaymentInfoService;
import kr.co.carrier.vo.DriverDeductFileVO;

@Service("driverDeductFileService")
public class DriverDeductFileServiceImpl implements DriverDeductFileService{

	
	@Resource(name="driverDeductFileMapper")
	private DriverDeductFileMapper driverDeductFileMapper;
	
	
	@Autowired
    private DriverDeductInfoService driverDeductInfoService;
    
	
	@Autowired
    private DriverService driverService;
	
	@Autowired
    private PaymentInfoService paymentInfoService;
	
	@Autowired
	private FileUploadService fileUploadService;
	
	
	@Value("#{appProp['upload.base.path']}")
    private String rootDir;    
    private String subDir;
	
	
	
	public int insertDriverDeductFile(DriverDeductFileVO driverDeductfileVO) throws Exception{
		return driverDeductFileMapper.insertDriverDeductFile(driverDeductfileVO);
	}
	
	public List<Map<String, Object>> selectDriverDeductFileList(Map<String, Object> map) throws Exception{
		return driverDeductFileMapper.selectDriverDeductFileList(map);
	}
	
	public Map<String, Object> selectDriverDeductFile(Map<String, Object> map) throws Exception{
		return driverDeductFileMapper.selectDriverDeductFile(map);
	}
	
	public void deleteDriverDeductFile(Map<String, Object> map) throws Exception{
		driverDeductFileMapper.deleteDriverDeductFile(map);
	}
	
	
	
	
	
}
