package kr.co.carrier.service;

import java.util.List;
import java.util.Map;

public interface AllocationDivisionService {

	
	public List<Map<String, Object>>selectAllocationDivisionInfoList() throws Exception;
	
	
}
