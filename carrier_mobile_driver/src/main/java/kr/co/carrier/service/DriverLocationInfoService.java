package kr.co.carrier.service;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.DriverLocationInfoVO;

public interface DriverLocationInfoService {

	
	public Map<String, Object> selectDriverLocationInfo(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectDriverLocationInfoList(Map<String, Object> map) throws Exception;
	public int selectDriverLocationInfoListCount(Map<String, Object> map) throws Exception;
	public int insertDriverLocationInfo(DriverLocationInfoVO driverLocationInfoVO) throws Exception;
	public void deleteDriverLocationInfoList(Map<String, Object> map) throws Exception;
	
	
	
	
	
	
	
	
	
	
	
	
	
}
