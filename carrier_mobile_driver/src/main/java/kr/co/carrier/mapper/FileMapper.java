package kr.co.carrier.mapper;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.FileVO;

public interface FileMapper {

	public int insertFile(FileVO fileVO) throws Exception;	
	public List<Map<String, Object>> selectFileList(Map<String, Object> map) throws Exception;
	public void deleteFile(Map<String, Object> map) throws Exception;
	
//	public int selectFileCount(Map<String, Object> map) throws Exception;
//	public Map<String, Object> selectFile(Map<String, Object> map) throws Exception;

	
	
}
