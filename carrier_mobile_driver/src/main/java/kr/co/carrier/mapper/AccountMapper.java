package kr.co.carrier.mapper;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.AccountVO;

public interface AccountMapper {

	public Map<String, Object> selectAccount(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectAccountList(Map<String, Object> map) throws Exception;
	public int selectAccountListCount(Map<String, Object> map) throws Exception;
	public int insertAccount(AccountVO accountVO) throws Exception;
	public void deleteAccount(Map<String, Object> map) throws Exception;
	
	
	
	
	
	
	
	
	
	
	
	
	
}
