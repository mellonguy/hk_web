package kr.co.carrier.mapper;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.DriverDeductFileVO;

public interface DriverDeductFileMapper {

	
	public int insertDriverDeductFile(DriverDeductFileVO driverDeductfileVO) throws Exception;	
	public List<Map<String, Object>> selectDriverDeductFileList(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectDriverDeductFile(Map<String, Object> map) throws Exception;
	public void deleteDriverDeductFile(Map<String, Object> map) throws Exception;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
