package kr.co.carrier.mapper;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.SendSmsVO;

public interface SendSmsMapper {

	public Map<String, Object> selectSendSms(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectSendSmsList(Map<String, Object> map) throws Exception;
	public int selectSendSmsListCount(Map<String, Object> map) throws Exception;
	public int insertSendSms(SendSmsVO sendSmsVO) throws Exception;
	public void deleteSendSms(Map<String, Object> map) throws Exception;
	public int insertSendCertNumForApp(Map<String, Object> map) throws Exception;
	public int selectCompareCertNum(Map<String, Object> map) throws Exception;	
	
	
}
