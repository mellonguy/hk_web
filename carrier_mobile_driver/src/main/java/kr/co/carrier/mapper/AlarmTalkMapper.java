package kr.co.carrier.mapper;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.AlarmTalkVO;

public interface AlarmTalkMapper {

	
	
	public Map<String, Object> selectAlarmTalk(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectAlarmTalkList(Map<String, Object> map) throws Exception;
	public int selectAlarmTalkListCount(Map<String, Object> map) throws Exception;
	public int insertAlarmTalk(AlarmTalkVO alarmTalkVO) throws Exception;
	public void deleteAlarmTalk(Map<String, Object> map) throws Exception;
	
	
	
	
	
}
