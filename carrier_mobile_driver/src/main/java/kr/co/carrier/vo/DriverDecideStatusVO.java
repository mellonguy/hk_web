package kr.co.carrier.vo;

public class DriverDecideStatusVO {


	
	
	private String driverDecideStatusId;
	private String driverId;
	private String listStatus;
	private String decideMonth;
	private String regDt;
	public String getDriverDecideStatusId() {
		return driverDecideStatusId;
	}
	public void setDriverDecideStatusId(String driverDecideStatusId) {
		this.driverDecideStatusId = driverDecideStatusId;
	}
	public String getDriverId() {
		return driverId;
	}
	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
	public String getListStatus() {
		return listStatus;
	}
	public void setListStatus(String listStatus) {
		this.listStatus = listStatus;
	}
	public String getDecideMonth() {
		return decideMonth;
	}
	public void setDecideMonth(String decideMonth) {
		this.decideMonth = decideMonth;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	
	
	
}
