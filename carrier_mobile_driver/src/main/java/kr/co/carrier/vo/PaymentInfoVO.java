package kr.co.carrier.vo;

public class PaymentInfoVO {

	
	private String allocationId;
	private String amount;
	private String billForPayment;
	private String billingDivision;
	private String billingDt;
	private String deductionRate;
	private String etc;
	private String payment;
	private String paymentDivision;
	private String paymentDt;
	private String paymentKind;
	private String paymentPartner;
	private String paymentPartnerId;
	private String regDt;
	public String getAllocationId() {
		return allocationId;
	}
	public void setAllocationId(String allocationId) {
		this.allocationId = allocationId;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getBillForPayment() {
		return billForPayment;
	}
	public void setBillForPayment(String billForPayment) {
		this.billForPayment = billForPayment;
	}
	public String getBillingDivision() {
		return billingDivision;
	}
	public void setBillingDivision(String billingDivision) {
		this.billingDivision = billingDivision;
	}
	public String getBillingDt() {
		return billingDt;
	}
	public void setBillingDt(String billingDt) {
		this.billingDt = billingDt;
	}
	public String getDeductionRate() {
		return deductionRate;
	}
	public void setDeductionRate(String deductionRate) {
		this.deductionRate = deductionRate;
	}
	public String getEtc() {
		return etc;
	}
	public void setEtc(String etc) {
		this.etc = etc;
	}
	public String getPayment() {
		return payment;
	}
	public void setPayment(String payment) {
		this.payment = payment;
	}
	public String getPaymentDivision() {
		return paymentDivision;
	}
	public void setPaymentDivision(String paymentDivision) {
		this.paymentDivision = paymentDivision;
	}
	public String getPaymentDt() {
		return paymentDt;
	}
	public void setPaymentDt(String paymentDt) {
		this.paymentDt = paymentDt;
	}
	public String getPaymentKind() {
		return paymentKind;
	}
	public void setPaymentKind(String paymentKind) {
		this.paymentKind = paymentKind;
	}
	public String getPaymentPartner() {
		return paymentPartner;
	}
	public void setPaymentPartner(String paymentPartner) {
		this.paymentPartner = paymentPartner;
	}
	public String getPaymentPartnerId() {
		return paymentPartnerId;
	}
	public void setPaymentPartnerId(String paymentPartnerId) {
		this.paymentPartnerId = paymentPartnerId;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
