package kr.co.carrier.vo;

public class CustomerPersonInChargeVO {

	
	private String address;						//주소
	private String customerId;                 //거래처코드
	private String department;                  //담당자부서
	private String email;                       //이메일
	private String etc;                         //비고
	private String name;                        //담당자이름
	private String personInChargeId;         //담당자아이디
	private String phoneNum;                   //담당자연락처
	private String regDt;                      //등록일
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEtc() {
		return etc;
	}
	public void setEtc(String etc) {
		this.etc = etc;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPersonInChargeId() {
		return personInChargeId;
	}
	public void setPersonInChargeId(String personInChargeId) {
		this.personInChargeId = personInChargeId;
	}
	public String getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	
	
	
	
	
	
}
