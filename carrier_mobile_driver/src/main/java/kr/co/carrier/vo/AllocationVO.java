package kr.co.carrier.vo;

public class AllocationVO {

	private String listOrder;
	private String allocationId;
	private String companyId;
	private String companyName;
	private String inputDt;
	private String comment;
	private String customerId;
	private String customerName;
	private String chargeName;
	private String chargeId;
	private String chargePhone;
	private String chargeAddr;
	private String customerSignificantData;
	private String carCnt;
	private String memo;
	private String regDt;
	private String allocationStatus;
	private String registerId;
	private String registerName;
	private String profit;
	private String batchStatus;
	private String batchStatusId;
	private String batchIndex;
	private String missingSend="N";
	
	
	
	
	public String getMissingSend() {
		return missingSend;
	}
	public void setMissingSend(String missingSend) {
		this.missingSend = missingSend;
	}
	public String getListOrder() {
		return listOrder;
	}
	public void setListOrder(String listOrder) {
		this.listOrder = listOrder;
	}
	public String getAllocationId() {
		return allocationId;
	}
	public void setAllocationId(String allocationId) {
		this.allocationId = allocationId;
	}
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getInputDt() {
		return inputDt;
	}
	public void setInputDt(String inputDt) {
		this.inputDt = inputDt;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getChargeName() {
		return chargeName;
	}
	public void setChargeName(String chargeName) {
		this.chargeName = chargeName;
	}
	public String getChargeId() {
		return chargeId;
	}
	public void setChargeId(String chargeId) {
		this.chargeId = chargeId;
	}
	public String getChargePhone() {
		return chargePhone;
	}
	public void setChargePhone(String chargePhone) {
		this.chargePhone = chargePhone;
	}
	public String getChargeAddr() {
		return chargeAddr;
	}
	public void setChargeAddr(String chargeAddr) {
		this.chargeAddr = chargeAddr;
	}
	public String getCustomerSignificantData() {
		return customerSignificantData;
	}
	public void setCustomerSignificantData(String customerSignificantData) {
		this.customerSignificantData = customerSignificantData;
	}
	public String getCarCnt() {
		return carCnt;
	}
	public void setCarCnt(String carCnt) {
		this.carCnt = carCnt;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getRegisterId() {
		return registerId;
	}
	public void setRegisterId(String registerId) {
		this.registerId = registerId;
	}
	public String getRegisterName() {
		return registerName;
	}
	public void setRegisterName(String registerName) {
		this.registerName = registerName;
	}
	public String getProfit() {
		return profit;
	}
	public void setProfit(String profit) {
		this.profit = profit;
	}
	public String getAllocationStatus() {
		return allocationStatus;
	}
	public void setAllocationStatus(String allocationStatus) {
		this.allocationStatus = allocationStatus;
	}
	public String getBatchStatus() {
		return batchStatus;
	}
	public void setBatchStatus(String batchStatus) {
		this.batchStatus = batchStatus;
	}
	public String getBatchStatusId() {
		return batchStatusId;
	}
	public void setBatchStatusId(String batchStatusId) {
		this.batchStatusId = batchStatusId;
	}
	public String getBatchIndex() {
		return batchIndex;
	}
	public void setBatchIndex(String batchIndex) {
		this.batchIndex = batchIndex;
	}
	
	
	
	
	
}
