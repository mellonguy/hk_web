package kr.co.carrier.vo;

public class DriverInsuranceVO {

	
	private String deductDividedPayments;					//공제분할구분
	private String deductMaximum;                          //공제 대물한도(원)
	private String deductTotalPayments;                   //공제총납부금
	private String deductionRate;                          //적용요율
	private String driverId;                               //기사코드
	private String etc;                                     //비고
	private String expirationDt;                           //만기일
	private String insuranceFifthPaymentDt;              //5차납부일
	private String insuranceFifthPayments;                //5차납부금
	private String insuranceFirstPaymentDt;              //1차납부일
	private String insuranceFirstPayments;                //1차납부금
	private String insuranceFourthPaymentDt;             //4차납부일
	private String insuranceFourthPayments;               //4차납부금
	private String insuranceId;                            //보험정보 아이디
	private String insuranceSecondPaymentDt;             //2차납부일
	private String insuranceSecondPayments;               //2차납부금
	private String insuranceSixthPaymentDt;              //6차납부일
	private String insuranceSixthPayments;                //6차납부금
	private String insuranceThirdPaymentDt;              //3차납부일
	private String insuranceThirdPayments;                //3차납부금
	private String luggageDividedPayments;                //적재물분할구분
	private String luggageMaximum;                         //적재물보상한도
	private String luggageSelfPay;                        //적재물자기부담금
	private String luggageSpecialContract;                //적재물특약사항
	private String luggageTotalPayments;                  //적재물총납부금
	private String regDt;                                  //등록일
	private String startDt;                                //시작일
	private String updateDt;                               //수정일
	public String getDeductDividedPayments() {
		return deductDividedPayments;
	}
	public void setDeductDividedPayments(String deductDividedPayments) {
		this.deductDividedPayments = deductDividedPayments;
	}
	public String getDeductMaximum() {
		return deductMaximum;
	}
	public void setDeductMaximum(String deductMaximum) {
		this.deductMaximum = deductMaximum;
	}
	public String getDeductTotalPayments() {
		return deductTotalPayments;
	}
	public void setDeductTotalPayments(String deductTotalPayments) {
		this.deductTotalPayments = deductTotalPayments;
	}
	public String getDeductionRate() {
		return deductionRate;
	}
	public void setDeductionRate(String deductionRate) {
		this.deductionRate = deductionRate;
	}
	public String getDriverId() {
		return driverId;
	}
	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
	public String getEtc() {
		return etc;
	}
	public void setEtc(String etc) {
		this.etc = etc;
	}
	public String getExpirationDt() {
		return expirationDt;
	}
	public void setExpirationDt(String expirationDt) {
		this.expirationDt = expirationDt;
	}
	public String getInsuranceFifthPaymentDt() {
		return insuranceFifthPaymentDt;
	}
	public void setInsuranceFifthPaymentDt(String insuranceFifthPaymentDt) {
		this.insuranceFifthPaymentDt = insuranceFifthPaymentDt;
	}
	public String getInsuranceFifthPayments() {
		return insuranceFifthPayments;
	}
	public void setInsuranceFifthPayments(String insuranceFifthPayments) {
		this.insuranceFifthPayments = insuranceFifthPayments;
	}
	public String getInsuranceFirstPaymentDt() {
		return insuranceFirstPaymentDt;
	}
	public void setInsuranceFirstPaymentDt(String insuranceFirstPaymentDt) {
		this.insuranceFirstPaymentDt = insuranceFirstPaymentDt;
	}
	public String getInsuranceFirstPayments() {
		return insuranceFirstPayments;
	}
	public void setInsuranceFirstPayments(String insuranceFirstPayments) {
		this.insuranceFirstPayments = insuranceFirstPayments;
	}
	public String getInsuranceFourthPaymentDt() {
		return insuranceFourthPaymentDt;
	}
	public void setInsuranceFourthPaymentDt(String insuranceFourthPaymentDt) {
		this.insuranceFourthPaymentDt = insuranceFourthPaymentDt;
	}
	public String getInsuranceFourthPayments() {
		return insuranceFourthPayments;
	}
	public void setInsuranceFourthPayments(String insuranceFourthPayments) {
		this.insuranceFourthPayments = insuranceFourthPayments;
	}
	public String getInsuranceId() {
		return insuranceId;
	}
	public void setInsuranceId(String insuranceId) {
		this.insuranceId = insuranceId;
	}
	public String getInsuranceSecondPaymentDt() {
		return insuranceSecondPaymentDt;
	}
	public void setInsuranceSecondPaymentDt(String insuranceSecondPaymentDt) {
		this.insuranceSecondPaymentDt = insuranceSecondPaymentDt;
	}
	public String getInsuranceSecondPayments() {
		return insuranceSecondPayments;
	}
	public void setInsuranceSecondPayments(String insuranceSecondPayments) {
		this.insuranceSecondPayments = insuranceSecondPayments;
	}
	public String getInsuranceSixthPaymentDt() {
		return insuranceSixthPaymentDt;
	}
	public void setInsuranceSixthPaymentDt(String insuranceSixthPaymentDt) {
		this.insuranceSixthPaymentDt = insuranceSixthPaymentDt;
	}
	public String getInsuranceSixthPayments() {
		return insuranceSixthPayments;
	}
	public void setInsuranceSixthPayments(String insuranceSixthPayments) {
		this.insuranceSixthPayments = insuranceSixthPayments;
	}
	public String getInsuranceThirdPaymentDt() {
		return insuranceThirdPaymentDt;
	}
	public void setInsuranceThirdPaymentDt(String insuranceThirdPaymentDt) {
		this.insuranceThirdPaymentDt = insuranceThirdPaymentDt;
	}
	public String getInsuranceThirdPayments() {
		return insuranceThirdPayments;
	}
	public void setInsuranceThirdPayments(String insuranceThirdPayments) {
		this.insuranceThirdPayments = insuranceThirdPayments;
	}
	public String getLuggageDividedPayments() {
		return luggageDividedPayments;
	}
	public void setLuggageDividedPayments(String luggageDividedPayments) {
		this.luggageDividedPayments = luggageDividedPayments;
	}
	public String getLuggageMaximum() {
		return luggageMaximum;
	}
	public void setLuggageMaximum(String luggageMaximum) {
		this.luggageMaximum = luggageMaximum;
	}
	public String getLuggageSelfPay() {
		return luggageSelfPay;
	}
	public void setLuggageSelfPay(String luggageSelfPay) {
		this.luggageSelfPay = luggageSelfPay;
	}
	public String getLuggageSpecialContract() {
		return luggageSpecialContract;
	}
	public void setLuggageSpecialContract(String luggageSpecialContract) {
		this.luggageSpecialContract = luggageSpecialContract;
	}
	public String getLuggageTotalPayments() {
		return luggageTotalPayments;
	}
	public void setLuggageTotalPayments(String luggageTotalPayments) {
		this.luggageTotalPayments = luggageTotalPayments;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getStartDt() {
		return startDt;
	}
	public void setStartDt(String startDt) {
		this.startDt = startDt;
	}
	public String getUpdateDt() {
		return updateDt;
	}
	public void setUpdateDt(String updateDt) {
		this.updateDt = updateDt;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
