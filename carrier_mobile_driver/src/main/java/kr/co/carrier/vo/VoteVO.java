package kr.co.carrier.vo;

public class VoteVO {

	
	
	private String voteId;
	private String electionId;
	private String voterId;
	private String voterName;
	private String pickCandidateId;
	private String regDt;
	private String pickCandidateName;
	private String pickCandidateAssign;
	
	
	public String getVoteId() {
		return voteId;
	}
	public void setVoteId(String voteId) {
		this.voteId = voteId;
	}
	public String getElectionId() {
		return electionId;
	}
	public void setElectionId(String electionId) {
		this.electionId = electionId;
	}
	public String getVoterId() {
		return voterId;
	}
	public void setVoterId(String voterId) {
		this.voterId = voterId;
	}
	public String getVoterName() {
		return voterName;
	}
	public void setVoterName(String voterName) {
		this.voterName = voterName;
	}
	
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getPickCandidateId() {
		return pickCandidateId;
	}
	public void setPickCandidateId(String pickCandidateId) {
		this.pickCandidateId = pickCandidateId;
	}
	public String getPickCandidateName() {
		return pickCandidateName;
	}
	public void setPickCandidateName(String pickCandidateName) {
		this.pickCandidateName = pickCandidateName;
	}
	public String getPickCandidateAssign() {
		return pickCandidateAssign;
	}
	public void setPickCandidateAssign(String pickCandidateAssign) {
		this.pickCandidateAssign = pickCandidateAssign;
	}
	
	
}
