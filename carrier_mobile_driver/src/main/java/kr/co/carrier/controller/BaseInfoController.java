package kr.co.carrier.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import kr.co.carrier.service.AccountService;
import kr.co.carrier.service.BillingDivisionService;
import kr.co.carrier.service.CompanyService;
import kr.co.carrier.service.CustomerService;
import kr.co.carrier.service.DriverInsuranceService;
import kr.co.carrier.service.DriverService;
import kr.co.carrier.service.EmployeeService;
import kr.co.carrier.service.FileService;
import kr.co.carrier.service.PersonInChargeService;
import kr.co.carrier.service.SalesPurchaseDivisionService;
import kr.co.carrier.utils.PagingUtils;
import kr.co.carrier.utils.ParamUtils;
import kr.co.carrier.utils.ResultApi;
import kr.co.carrier.utils.WebUtils;
import kr.co.carrier.vo.AccountVO;
import kr.co.carrier.vo.CompanyVO;
import kr.co.carrier.vo.CustomerPersonInChargeVO;
import kr.co.carrier.vo.CustomerVO;
import kr.co.carrier.vo.DriverInsuranceVO;
import kr.co.carrier.vo.DriverVO;
import kr.co.carrier.vo.EmployeeVO;

@Controller
@RequestMapping(value="/baseinfo")
public class BaseInfoController {

	
	
	
    @Autowired
    private EmployeeService empService;
    
    @Autowired
    private DriverService driverService;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private CompanyService companyService;
	
    @Autowired
    private DriverInsuranceService driverInsuranceService;
	
    
    @Autowired
    private PersonInChargeService personInChargeService;
    
    @Autowired
	private FileService fileService;
	
    @Autowired
    private AccountService accountService;
    
    @Autowired
    private BillingDivisionService billingDivisionService;
    
    @Autowired
    private SalesPurchaseDivisionService salesPurchaseDivisionService;
    
	@Value("#{appProp['upload.base.path']}")
    private String rootDir;    
    private String subDir;
    
    
	@RequestMapping(value = "/employee", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView employee(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			//String searchType = request.getParameter("searchType") == null  ? "" : request.getParameter("searchType").toString();
			String searchWord = request.getParameter("searchWord") == null  ? "" : request.getParameter("searchWord").toString();
			
			//paramMap.put("searchType", searchType);
			paramMap.put("searchWord", searchWord);
			
			
			int totalCount = empService.selectEmployeeListCount(paramMap);
			PagingUtils.setPageing(request, totalCount, paramMap);
			
			List<Map<String, Object>> empList = empService.selectEmployeeList(paramMap);
			
			for(int i = 0; i < empList.size(); i++) {
				Map<String, Object> emp = empList.get(i);
				
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("contentId", emp.get("emp_id").toString());
	      		map.put("categoryType", "employee");
	      		List<Map<String, Object>> fileList = fileService.selectFileList(map);
	      		//emp.put("fileList", fileList)
	      		empList.get(i).put("fileList", fileList);
			}
			
			mav.addObject("userSessionMap",  userSessionMap);
			mav.addObject("listData",  empList);
			mav.addObject("paramMap", paramMap);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
						
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	@RequestMapping(value = "/add-employee", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView addemployee(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			Map<String, Object> weekMap = new HashMap<String, Object>();
			Map<String, Object> monthMap = new HashMap<String, Object>();
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			mav.addObject("userSessionMap",  userSessionMap);
			
			Date date = new Date();
			DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
			String formattedDate = dateFormat.format(date);
			mav.addObject("serverTime", formattedDate );
			
			Map<String,Object> map = new HashMap<String,Object>();
			List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
			mav.addObject("companyList", companyList);
						
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	@RequestMapping(value = "/insert-employee", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView insertEmployee(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute EmployeeVO employeeVO) throws Exception {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(employeeVO.getEmpRole().equals("")) {
				employeeVO.setEmpRole("U");
			}
			
			//퇴사일이 지정되어 있는경우 재직 상태를 퇴사로 변경한다.
			/*if(employeeVO.getResignDt() != null && !employeeVO.getResignDt().equals("")) {
				employeeVO.setEmpStatus(BaseAppConstants.EMPLOYEE_STATUS_RESIGN);
			}*/
			
			if(userSessionMap != null){				//사용자 로그인이 되어 있으면
				Map<String, Object> map = new HashMap<String, Object>();
				
				map.put("duplicateChk", "true");
				map.put("empId", employeeVO.getEmpId());
				
				Map<String, Object> employeeMap = empService.selectEmployee(map);
				
				if(employeeMap != null){
					WebUtils.messageAndRedirectUrl(mav, "이미 사용중인 아이디 입니다.", "/baseinfo/add-employee.do");
				}else{
					if(employeeVO.getDeductionRate().equals("")) {
						employeeVO.setDeductionRate("0");
					}
					
					employeeVO.setRegId(userSessionMap.get("emp_id").toString());
					employeeVO.setRegName(userSessionMap.get("emp_name").toString());
					empService.insertEmployee(employeeVO);		
					subDir = "employee";
					fileService.insertFile(rootDir, subDir, employeeVO.getEmpId(), request);
					WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/baseinfo/employee.do");
				}
			}else {						//로그인이 되어 있지 않은경우 로그인 화면으로 이동 한다.
				
				String status = request.getParameter("status") == null ? "": request.getParameter("status").toString();
				
				if(status.equals("noLogin")) {
					Map<String, Object> map = new HashMap<String, Object>();
					
					map.put("duplicateChk", "true");
					map.put("empId", employeeVO.getEmpId());
					
					Map<String, Object> employeeMap = empService.selectEmployee(map);
					
					if(employeeMap != null){
						WebUtils.messageAndRedirectUrl(mav, "이미 사용중인 아이디 입니다.", "/baseinfo/add-employee.do");
					}else{
						if(employeeVO.getDeductionRate() == null || employeeVO.getDeductionRate().equals("")) {
							employeeVO.setDeductionRate("0");
						}
						employeeVO.setRegId("self");
						employeeVO.setRegName("self");
						empService.insertEmployee(employeeVO);		
						subDir = "employee";
						fileService.insertFile(rootDir, subDir, employeeVO.getEmpId(), request);
						WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/index.do");
					}
				}else {
					WebUtils.messageAndRedirectUrl(mav, "로그인이 되지 않았습니다. 로그인 화면으로 이동 합니다.", "/index.do");	
				}
					
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	@RequestMapping(value = "/edit-employee", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView editEmployee(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute EmployeeVO employeeVO) throws Exception {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			mav.addObject("userSessionMap",  userSessionMap);
			
			Map<String, Object> map = new HashMap<String, Object>();
			
			List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
			mav.addObject("companyList", companyList);
			
			map.put("duplicateChk", "true");
			map.put("empId", employeeVO.getEmpId());
			Map<String, Object> employeeMap = empService.selectEmployee(map);
			
      		map.put("contentId", employeeVO.getEmpId());
      		map.put("categoryType", "employee");
			
      		List<Map<String, Object>> fileList = fileService.selectFileList(map);
      		employeeMap.put("fileList", fileList);
			mav.addObject("empMap",employeeMap);
			
		}catch(Exception e){
			e.printStackTrace();
		}

		return mav;
	}

	@RequestMapping(value = "/update-employee", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView updateEmployee(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute EmployeeVO employeeVO) throws Exception {
		
		try{
			
				if(employeeVO.getDeductionRate().equals("")) {
					employeeVO.setDeductionRate("0");
				}
				if(employeeVO.getEmpRole().equals("")) {
					employeeVO.setEmpRole("U");
				}
				
				//퇴사일이 지정되어 있는경우 재직 상태를 퇴사로 변경한다.
				/*if(employeeVO.getResignDt() != null && !employeeVO.getResignDt().equals("")) {
					employeeVO.setEmpStatus(BaseAppConstants.EMPLOYEE_STATUS_RESIGN);
				}*/
				
				empService.updateEmployee(employeeVO);
				
				MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
		    	List<MultipartFile> fileList = multipartRequest.getFiles("bbsFile");
				
		    	if(fileList != null && fileList.size() > 0){		//업로드 되는 파일이 존재 하면........
		    		//기존의 파일을 삭제 한다.
		    		Map<String, Object> map = new HashMap<String, Object>();
		    		map.put("contentId", employeeVO.getEmpId());
		      		map.put("categoryType", "employee");
		    		
		    		fileService.deleteFile(map);
		    		//새파일을 저장한다.
					subDir = "employee";
					fileService.insertFile(rootDir, subDir, employeeVO.getEmpId(), request);
		    	}
		    		
				WebUtils.messageAndRedirectUrl(mav, "수정되었습니다.", "/baseinfo/employee.do");
		}catch(Exception e){
			e.printStackTrace();
		}

		return mav;
	}	
		
	@RequestMapping(value = "/driver", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView driver(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			String searchType = request.getParameter("searchType") == null  ? "" : request.getParameter("searchType").toString();
			String searchWord = request.getParameter("searchWord") == null  ? "" : request.getParameter("searchWord").toString();
			
			paramMap.put("searchType", searchType);
			paramMap.put("searchWord", searchWord);
			
			int totalCount = driverService.selectDriverListCount(paramMap);
			PagingUtils.setPageing(request, totalCount, paramMap);
			
			List<Map<String, Object>> driverList = driverService.selectDriverList(paramMap);
			
			for(int i = 0; i < driverList.size(); i++) {
				Map<String, Object> driver = driverList.get(i);
				
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("contentId", driver.get("driver_id").toString());
	      		map.put("categoryType", "employee");
	      		List<Map<String, Object>> fileList = fileService.selectFileList(map);
	      		//emp.put("fileList", fileList)
	      		driverList.get(i).put("fileList", fileList);
			}
			
			mav.addObject("listData",  driverList);
			mav.addObject("paramMap", paramMap);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
						
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	@RequestMapping(value = "/add-driver", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView adddriver(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			Map<String,Object> map = new HashMap<String,Object>();
			List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
			mav.addObject("companyList", companyList);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	@RequestMapping(value = "/insert-driver", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView insertDriver(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute DriverVO driverVO) throws Exception {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){				//사용자 로그인이 되어 있으면
				
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("duplicateChk", "true");
				map.put("driverId", driverVO.getDriverId());
				
				Map<String, Object> driverMap = driverService.selectDriver(map);
				
				if(driverMap != null){
					WebUtils.messageAndRedirectUrl(mav, "이미 사용중인 아이디 입니다.", "/baseinfo/add-driver.do");
				}else{
					driverService.insertDriver(driverVO);
					subDir = "driver";
					fileService.insertFile(rootDir, subDir, driverVO.getDriverId(), request);
					WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/baseinfo/driver.do");
				}
			
			}else {
				
				String status = request.getParameter("status") == null ? "": request.getParameter("status").toString();
				
				if(status.equals("noLogin")) {
					
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("duplicateChk", "true");
					map.put("driverId", driverVO.getDriverId());
					
					Map<String, Object> driverMap = driverService.selectDriver(map);
					
					if(driverMap != null){
						WebUtils.messageAndRedirectUrl(mav, "이미 사용중인 아이디 입니다.", "/baseinfo/add-driver.do");
					}else{
						driverService.insertDriver(driverVO);
						subDir = "driver";
						fileService.insertFile(rootDir, subDir, driverVO.getDriverId(), request);
						WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/index.do");
					}
					
				}else {
					WebUtils.messageAndRedirectUrl(mav, "로그인이 되지 않았습니다. 로그인 화면으로 이동 합니다.", "/index.do");	
				}
				
				
				
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	@RequestMapping(value = "/driver-excel-insert", method = RequestMethod.POST)
	public ModelAndView driverExcelInsert(ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception{
		
		ResultApi resultApi = new ResultApi();
		
		try{
			
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
	    	List<MultipartFile> fileList = multipartRequest.getFiles("bbsFile");
			
	    	if(fileList != null && fileList.size() > 0){
	    		for(MultipartFile mFile : fileList){	
	    			if(mFile != null && mFile.getSize() > 0){
	    				
	    				Long fileSize = mFile.getSize();
	    				String fileName = mFile.getOriginalFilename();		
	    				String ext = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length());
	    				File file = new File(mFile.getOriginalFilename());
	    				mFile.transferTo(file);
	    				
	    				
	    				XSSFWorkbook wb = null;
	    				XSSFRow row;
	    				XSSFCell cell;
	    				try {//엑셀 파일 오픈
	    					wb = new XSSFWorkbook(new FileInputStream(file));
	    				} catch (FileNotFoundException e) {
	    					e.printStackTrace();
	    				} catch (IOException e) {
	    					e.printStackTrace();
	    				}
	    				XSSFSheet sheet = wb.getSheetAt(0);         
	    		         
	    				//int rows = sheet.getPhysicalNumberOfRows();
	    				//int cells = sheet.getRow(0).getPhysicalNumberOfCells();
	    				int rows = sheet.getLastRowNum();
	    				int cells = sheet.getRow(3).getLastCellNum();
	    				
	    				FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
	    				
	    				 for (int r = 3; r < rows; r++) {
	    					 DriverVO driverVO = new DriverVO();
	    					 DriverInsuranceVO driverInsuranceVO = new DriverInsuranceVO();
	    					 	String driverId = "";
	    						driverId = "DRI"+UUID.randomUUID().toString().replaceAll("-", "");
	    						driverVO.setDriverId(driverId);
	    						String uuid = UUID.randomUUID().toString().replaceAll("-","");
								String insuranceId = "INS" + uuid;
								driverInsuranceVO.setInsuranceId(insuranceId);
								driverInsuranceVO.setDriverId(driverId);
	    			        	row = sheet.getRow(r); // row 가져오기
	    			        	if (row != null) {
	    			        		for (int c = 0; c < cells; c++) {
	    			        			cell = row.getCell(c);
	    			        			if (cell != null) {
	    			        				String value = null;
	    									switch (cell.getCellType()) {
	    									   	case XSSFCell.CELL_TYPE_FORMULA:
	    									   		if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_NUMERIC){
	    									   			value = "" + (int)cell.getNumericCellValue();
	    									   		}else if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_STRING){
	    									   			value = "" + cell.getStringCellValue(); 
	    									   		}
	    									   		value = cell.getCellFormula();
	    									   		break;
	    									   	case XSSFCell.CELL_TYPE_NUMERIC:
	    									   		value = "" + (int)cell.getNumericCellValue();
	    									   		break;
	    									   	case XSSFCell.CELL_TYPE_STRING:
	    									   		value = "" + cell.getStringCellValue();
	    									   		break;
	    									   	case XSSFCell.CELL_TYPE_BLANK:
	    									   		value = "";
	    									   		break;
	    									   	case XSSFCell.CELL_TYPE_ERROR:
	    									   		value = "" + cell.getErrorCellValue();
	    									   		break;
	    									   	default:
	    									}
	    									
	    									int nIndex = cell.getColumnIndex();
	    									if(nIndex == 0){
	    										if(value.equals("")){
	    											driverVO.setDriverOwner(value);
	    											break;
	    										}else{
	    											driverVO.setDriverOwner(value);
	    										}
	    									}else if(nIndex == 1){
	    										driverVO.setDriverName(value);
	    									}else if(nIndex == 2){
	    										driverVO.setPhoneNum(value);
	    									}else if(nIndex == 3){
	    										driverVO.setResidentRegistrationNumber(value);
	    									}
	    									else if(nIndex == 4){
	    										driverVO.setDriverLicense(value);
	    									}
	    									else if(nIndex == 5){
	    										if(value.contains("법인")){
	    											driverVO.setCompanyKind("00");	
	    										}else if(value.contains("개인")){
	    											driverVO.setCompanyKind("01");
	    										}else{
	    											driverVO.setCompanyKind("02");
	    										}
	    									}
	    									else if(nIndex == 6){
	    										driverVO.setBusinessLicenseNumber(value);
	    									}
	    									else if(nIndex == 7){
	    										driverVO.setBusinessCondition(value);
	    									}
	    									else if(nIndex == 8){
	    										driverVO.setBusinessKind(value);
	    									}
	    									else if(nIndex == 9){
	    										driverVO.setAddress(value);
	    									}
	    									else if(nIndex == 10){
	    										driverVO.setAssignCompany(value);
	    									}
	    									else if(nIndex == 11){
	    										
	    										if(value.contains("직영")){
	    											driverVO.setDriverKind("00");	
	    										}else if(value.contains("지입")){
	    											driverVO.setDriverKind("01");
	    										}else{
	    											driverVO.setDriverKind("02");
	    										}
	    									}
	    									else if(nIndex == 12){
	    										driverVO.setJoinDt(value);
	    									}
	    									else if(nIndex == 13){
	    										driverVO.setResignDt(value);
	    									}
	    									else if(nIndex == 14){
	    										driverVO.setDeductionRate(value);
	    									}
	    									else if(nIndex == 15){
	    										driverVO.setDriverBalance(value);
	    									}
	    									else if(nIndex == 16){
	    										driverVO.setDriverDeposit(value);
	    									}
	    									else if(nIndex == 17){
	    										String[] bankInfo = value.split("/");
	    										if(bankInfo.length > 0){
	    											for(int k = 0; k < bankInfo.length; k++){
	    												if(k==0){
	    													driverVO.setBankName(bankInfo[k]);	
	    												}else if(k==1){
	    													driverVO.setDepositor(bankInfo[k]);	
	    												}else if(k==2){
	    													driverVO.setAccountNumber(bankInfo[k]);	
	    												}
	    														
	    											}
	    										}
	    									}
	    									else if(nIndex == 18){
	    										driverVO.setCarKind(value);
	    									}
	    									else if(nIndex == 19){
	    										driverVO.setCarNickname(value);
	    									}
	    									else if(nIndex == 20){
	    										if(value.contains("셀프")){
	    											driverVO.setCarAssignCompany("S");
	    										}else if(value.contains("캐리어")){
	    											driverVO.setCarAssignCompany("C");
	    										}
	    									}
	    									else if(nIndex == 21){
	    										driverVO.setCarNum(value);
	    									}
	    									else if(nIndex == 22){
	    										driverVO.setCarIdNum(value);
	    									}else if(nIndex == 23){
	    										driverInsuranceVO.setStartDt(value);
	    									}
	    									else if(nIndex == 24){
	    										driverInsuranceVO.setExpirationDt(value);
	    									}
	    									else if(nIndex == 25){
	    										driverInsuranceVO.setDeductionRate(value);
	    									}
	    									else if(nIndex == 26){
	    										driverInsuranceVO.setDeductMaximum(value);
	    									}
	    									else if(nIndex == 27){
	    										driverInsuranceVO.setDeductDividedPayments(value);
	    									}
	    									else if(nIndex == 28){
	    										driverInsuranceVO.setDeductTotalPayments(value);
	    									}
	    									else if(nIndex == 29){
	    										driverInsuranceVO.setLuggageMaximum(value);
	    									}
	    									else if(nIndex == 30){
	    										driverInsuranceVO.setLuggageSelfPay(value);
	    									}
	    									else if(nIndex == 31){
	    										driverInsuranceVO.setLuggageDividedPayments(value);
	    									}
	    									else if(nIndex == 32){
	    										driverInsuranceVO.setLuggageTotalPayments(value);
	    									}
	    									else if(nIndex == 33){
	    										driverInsuranceVO.setLuggageSpecialContract(value);
	    									}
	    									else if(nIndex == 34){
	    										driverInsuranceVO.setInsuranceFirstPaymentDt(value);
	    									}
	    									else if(nIndex == 35){
	    										driverInsuranceVO.setInsuranceFirstPayments(value);
	    									}
	    									else if(nIndex == 36){
	    										driverInsuranceVO.setInsuranceSecondPaymentDt(value);
	    									}
	    									else if(nIndex == 37){
	    										driverInsuranceVO.setInsuranceSecondPayments(value);
	    									}
	    									else if(nIndex == 38){
	    										driverInsuranceVO.setInsuranceThirdPaymentDt(value);
	    									}
	    									else if(nIndex == 39){
	    										driverInsuranceVO.setInsuranceThirdPayments(value);
	    									}
	    									else if(nIndex == 40){
	    										driverInsuranceVO.setInsuranceFourthPaymentDt(value);
	    									}
	    									else if(nIndex == 41){
	    										driverInsuranceVO.setInsuranceFourthPayments(value);
	    									}
	    									else if(nIndex == 42){
	    										driverInsuranceVO.setInsuranceFifthPaymentDt(value);
	    									}
	    									else if(nIndex == 43){
	    										driverInsuranceVO.setInsuranceFifthPayments(value);
	    									}
	    									else if(nIndex == 44){
	    										driverInsuranceVO.setInsuranceSixthPaymentDt(value);
	    									}
	    									else if(nIndex == 45){
	    										driverInsuranceVO.setInsuranceSixthPayments(value);
	    									}
	    									else if(nIndex == 46){
	    										driverVO.setEtc(value);
	    									}
	    									
	    			        			} else {
	    			        				System.out.print("[null]\t");
	    			        			}
	    			        		} // for(c) 문
	    			 //       		System.out.print("\n");
	    			        		
	    			        		driverService.insertDriver(driverVO);
	    			        		driverInsuranceService.insertDriverInsurance(driverInsuranceVO);
	    			        		
	    			        	}
	    			        	
	    			        } // for(r) 문
	    				
	    			}
	    		}
	    	}else{
	    		resultApi.setResultCode("1111"); 	//file 없음
	    	}
			
			//resultApi = fileService.insertFile(rootDir, subDir,uuid,request);
			WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/baseinfo/driver.do");
		}catch(Exception e){
			WebUtils.messageAndRedirectUrl(mav, "등록하는데 실패 하였습니다.관리자에게 문의 하세요.", "/baseinfo/driver.do");
			e.printStackTrace();
		}
		
		return mav;
	}
	
	
	@RequestMapping(value = "/edit-driver", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView editDriver(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute DriverVO driverVO) throws Exception {
		
		try{
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("duplicateChk", "true");
			map.put("driverId", driverVO.getDriverId());
			Map<String, Object> driverMap = driverService.selectDriver(map);
			
			
			map.put("contentId", driverVO.getDriverId());
      		map.put("categoryType", "driver");
			
      		List<Map<String, Object>> fileList = fileService.selectFileList(map);
      		driverMap.put("fileList", fileList);
			
			mav.addObject("driverMap",driverMap);
		}catch(Exception e){
			e.printStackTrace();
		}

		return mav;
	}

	@RequestMapping(value = "/update-driver", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView updateDriver(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute DriverVO driverVO) throws Exception {
		
		try{
			
			
			if(driverVO.getRegisterStatus() == null || driverVO.getRegisterStatus().equals("")) {
				driverVO.setRegisterStatus("N");
			}
			
			driverService.updateDriver(driverVO);
			
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
	    	List<MultipartFile> fileList = multipartRequest.getFiles("bbsFile");
			
	    	if(fileList != null && fileList.size() > 0 && !fileList.get(0).getOriginalFilename().equals("")){		//업로드 되는 파일이 존재 하면........
	    		//기존의 파일을 삭제 한다.
	    		Map<String, Object> map = new HashMap<String, Object>();
	    		map.put("contentId", driverVO.getDriverId());
	      		map.put("categoryType", "driver");
	    		fileService.deleteFile(map);
	    		//새파일을 저장한다.
				subDir = "driver";
				fileService.insertFile(rootDir, subDir, driverVO.getDriverId(), request);
	    	}
			
			WebUtils.messageAndRedirectUrl(mav, "수정되었습니다.", "/baseinfo/driver.do");
		}catch(Exception e){
			e.printStackTrace();
		}

		return mav;
	}	
	
	@RequestMapping(value = "/getDriverInfo", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi getDriverInfo(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		ResultApi result = new ResultApi();
		try{
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("driverId", request.getParameter("driverId").toString());
			Map<String, Object> driverMap = driverService.selectDriver(map);
			result.setResultData(driverMap);
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}

		return result;
	}
	
	
	
	
	@RequestMapping(value = "/customer", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView customer(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			String searchType = request.getParameter("searchType") == null  ? "" : request.getParameter("searchType").toString();
			String searchWord = request.getParameter("searchWord") == null  ? "" : request.getParameter("searchWord").toString();
			
			paramMap.put("searchType", searchType);
			paramMap.put("searchWord", searchWord);
			
			int totalCount = customerService.selectCustomerListCount(paramMap);
			PagingUtils.setPageing(request, totalCount, paramMap);
			
			List<Map<String, Object>> customerList = customerService.selectCustomerList(paramMap);
			mav.addObject("listData",  customerList);
			mav.addObject("paramMap", paramMap);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
						
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	@RequestMapping(value = "/add-customer", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView addcustomer(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
			mav.addObject("billingDivisionList",  billingDivisionList);
			
			List<Map<String, Object>> salesPurchaseDivisionList = salesPurchaseDivisionService.selectSalesPurchaseDivisionInfoList();
			mav.addObject("salesPurchaseDivisionList",  salesPurchaseDivisionList);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	@RequestMapping(value = "/insert-customer", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView insertCustomer(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute CustomerVO customerVO) throws Exception {
		
		try{
			
			String customerId = "";
			customerId = "CUS"+UUID.randomUUID().toString().replaceAll("-", "");
			customerVO.setCustomerId(customerId);
			Map<String, Object> map = new HashMap<String, Object>();
			
			map.put("duplicateChk", "true");
			map.put("customerId", customerVO.getCustomerId());
			
			Map<String, Object> customerMap = customerService.selectCustomer(map);
			
			if(customerMap != null){
				WebUtils.messageAndRedirectUrl(mav, "이미 사용중인 아이디 입니다.", "/baseinfo/add-customer.do");
			}else{
				customerService.insertCustomer(customerVO);		
				
				subDir = "customer";
				fileService.insertFile(rootDir, subDir, customerId, request);
				
				JSONParser jsonParser = new JSONParser();
				String chargeList = request.getParameter("personInChargeInfo");
				String driverList = request.getParameter("driverInfo");
				map.put("customerId", customerId);
				personInChargeService.deletePersonInCharge(map);
				//driverService.deleteDriver(map);
				
				if(!chargeList.equals("")) {
					JSONObject jsonObject = (JSONObject) jsonParser.parse(chargeList);
					JSONArray jsonArray = (JSONArray) jsonObject.get("chargeList");
					
					for(int i = 0; i < jsonArray.size(); i++){
						CustomerPersonInChargeVO pVO = new CustomerPersonInChargeVO();
						JSONObject obj = (JSONObject)jsonArray.get(i);
						String personInChargeId = "";
						personInChargeId = "PIC"+UUID.randomUUID().toString().replaceAll("-", "");
						pVO.setPersonInChargeId(personInChargeId);
						pVO.setCustomerId(customerId);
						pVO.setDepartment(obj.get("department").toString());
						pVO.setName(obj.get("name").toString());
						pVO.setPhoneNum(obj.get("phoneNum").toString());
						pVO.setAddress(obj.get("address").toString());
						pVO.setEmail(obj.get("email").toString());
						pVO.setEtc(obj.get("etc").toString());
						personInChargeService.insertPersonInCharge(pVO);
					}
				}
				
				if(!driverList.equals("")) {
					JSONObject jsonObject = (JSONObject) jsonParser.parse(driverList);
					JSONArray jsonArray = (JSONArray) jsonObject.get("driverList");
					
					for(int i = 0; i < jsonArray.size(); i++){
						DriverVO dVO = new DriverVO();
						JSONObject obj = (JSONObject)jsonArray.get(i);
						String driverId = "";
						driverId = "DRI"+UUID.randomUUID().toString().replaceAll("-", "");
						dVO.setDriverId(driverId);
						dVO.setDriverOwner(customerVO.getCustomerName());
						//dVO.setCustomerId(customerId);
						dVO.setPhoneNum(obj.get("phone_num").toString());
						dVO.setDriverName(obj.get("driver_name").toString());
						dVO.setCarKind(obj.get("car_kind").toString());
						dVO.setCarNum(obj.get("car_num").toString());
						dVO.setDriverPwd("1234");		//거래처에 속한 기사의 경우 앱에 로그인 하지 않으므로 비밀번호 사용 안함.....
						driverService.insertDriver(dVO);
					}
				}
				
				WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/baseinfo/customer.do");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	@RequestMapping(value = "/edit-customer", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView editCustomer(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute CustomerVO customerVO) throws Exception {
		
		try{
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("duplicateChk", "true");
			map.put("customerId", customerVO.getCustomerId());
			Map<String, Object> customerMap = customerService.selectCustomer(map);
			
			map.put("contentId", customerVO.getCustomerId());
      		map.put("categoryType", "customer");
			
      		List<Map<String, Object>> fileList = fileService.selectFileList(map);
      		customerMap.put("fileList", fileList);
			List<Map<String, Object>> personInChargeList = personInChargeService.selectPersonInChargeList(map);
			List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
			List<Map<String, Object>> salesPurchaseDivisionList = salesPurchaseDivisionService.selectSalesPurchaseDivisionInfoList();
			//List<Map<String, Object>> driverList = driverService.selectDriverListByCustomerId(map);
			
			mav.addObject("salesPurchaseDivisionList",  salesPurchaseDivisionList);
			mav.addObject("billingDivisionList",  billingDivisionList);
			mav.addObject("customerMap",customerMap);
			mav.addObject("personInChargeList",personInChargeList);
			//mav.addObject("driverList",driverList);
		}catch(Exception e){
			e.printStackTrace();
		}

		return mav;
	}
	
	
	@RequestMapping(value = "/view-customer", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView viewCustomer(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute CustomerVO customerVO) throws Exception {
		
		try{
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("duplicateChk", "true");
			map.put("customerId", customerVO.getCustomerId());
			Map<String, Object> customerMap = customerService.selectCustomer(map);
			
			map.put("contentId", customerVO.getCustomerId());
      		map.put("categoryType", "customer");
			
      		List<Map<String, Object>> fileList = fileService.selectFileList(map);
      		customerMap.put("fileList", fileList);
			List<Map<String, Object>> personInChargeList = personInChargeService.selectPersonInChargeList(map);
			//List<Map<String, Object>> driverList = driverService.selectDriverListByCustomerId(map);
			mav.addObject("customerMap",customerMap);
			mav.addObject("personInChargeList",personInChargeList);
			//mav.addObject("driverList",driverList);
		}catch(Exception e){
			e.printStackTrace();
		}

		return mav;
	}
	

	@RequestMapping(value = "/update-customer", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView updateCustomer(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute CustomerVO customerVO) throws Exception {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				customerVO.setUpdater(userSessionMap.get("emp_name").toString());
			}
			
			customerService.updateCustomer(customerVO);
			
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
	    	List<MultipartFile> fileList = multipartRequest.getFiles("bbsFile");
			
	    	if(fileList != null && fileList.size() > 0 && !fileList.get(0).getOriginalFilename().equals("")){		//업로드 되는 파일이 존재 하면........
	    		//기존의 파일을 삭제 한다.
	    		Map<String, Object> map = new HashMap<String, Object>();
	    		map.put("contentId", customerVO.getCustomerId());
	      		map.put("categoryType", "customer");
	    		fileService.deleteFile(map);
	    		//새파일을 저장한다.
				subDir = "customer";
				fileService.insertFile(rootDir, subDir, customerVO.getCustomerId(), request);
	    	}
			
			Map<String, Object> map = new HashMap<String, Object>();
			JSONParser jsonParser = new JSONParser();
			String chargeList = request.getParameter("personInChargeInfo");
			String driverList = request.getParameter("driverInfo");
			String customerId = customerVO.getCustomerId();
			
			map.put("customerId", customerId);
			personInChargeService.deletePersonInCharge(map);
			//driverService.deleteDriver(map);
			
			if(!chargeList.equals("")) {
				JSONObject jsonObject = (JSONObject) jsonParser.parse(chargeList);
				JSONArray jsonArray = (JSONArray) jsonObject.get("chargeList");
				for(int i = 0; i < jsonArray.size(); i++){
					CustomerPersonInChargeVO pVO = new CustomerPersonInChargeVO();
					JSONObject obj = (JSONObject)jsonArray.get(i);
					String personInChargeId = "";
					personInChargeId = "PIC"+UUID.randomUUID().toString().replaceAll("-", "");
					pVO.setPersonInChargeId(personInChargeId);
					pVO.setCustomerId(customerId);
					pVO.setDepartment(obj.get("department").toString());
					pVO.setName(obj.get("name").toString());
					pVO.setPhoneNum(obj.get("phoneNum").toString());
					pVO.setAddress(obj.get("address").toString());
					pVO.setEmail(obj.get("email").toString());
					pVO.setEtc(obj.get("etc").toString());
					personInChargeService.insertPersonInCharge(pVO);
				}	
			}
			
			if(!driverList.equals("")) {
				JSONObject jsonObject = (JSONObject) jsonParser.parse(driverList);
				JSONArray jsonArray = (JSONArray) jsonObject.get("driverList");
				
				for(int i = 0; i < jsonArray.size(); i++){
					DriverVO dVO = new DriverVO();
					JSONObject obj = (JSONObject)jsonArray.get(i);
					String driverId = "";
					driverId = "DRI"+UUID.randomUUID().toString().replaceAll("-", "");
					dVO.setDriverId(driverId);
					//dVO.setCustomerId(customerId);
					dVO.setDriverOwner(customerVO.getCustomerName());
					dVO.setPhoneNum(obj.get("phone_num").toString());
					dVO.setDriverName(obj.get("driver_name").toString());
					dVO.setCarKind(obj.get("car_kind").toString());
					dVO.setCarNum(obj.get("car_num").toString());
					dVO.setDriverPwd("1234");		//거래처에 속한 기사의 경우 앱에 로그인 하지 않으므로 비밀번호 사용 안함.....
					driverService.insertDriver(dVO);
				}
			}
			
			WebUtils.messageAndRedirectUrl(mav, "수정되었습니다.", "/baseinfo/customer.do");
		}catch(Exception e){
			e.printStackTrace();
		}

		return mav;
	}	
	
	@RequestMapping(value = "/customer-excel-insert", method = RequestMethod.POST)
	public ModelAndView customerExcelInsert(ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception{
		
		ResultApi resultApi = new ResultApi();
		
		try{
			
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
	    	List<MultipartFile> fileList = multipartRequest.getFiles("bbsFile");
			
	    	if(fileList != null && fileList.size() > 0){
	    		for(MultipartFile mFile : fileList){	
	    			if(mFile != null && mFile.getSize() > 0){
	    				
	    				Long fileSize = mFile.getSize();
	    				String fileName = mFile.getOriginalFilename();		
	    				String ext = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length());
	    				File file = new File(mFile.getOriginalFilename());
	    				mFile.transferTo(file);
	    				
	    				
	    				XSSFWorkbook wb = null;
	    				XSSFRow row;
	    				XSSFCell cell;
	    				try {//엑셀 파일 오픈
	    					wb = new XSSFWorkbook(new FileInputStream(file));
	    				} catch (FileNotFoundException e) {
	    					e.printStackTrace();
	    				} catch (IOException e) {
	    					e.printStackTrace();
	    				}
	    				XSSFSheet sheet = wb.getSheetAt(0);         
	    		         
	    				//int rows = sheet.getPhysicalNumberOfRows();
	    				//int cells = sheet.getRow(0).getPhysicalNumberOfCells();
	    				int rows = sheet.getLastRowNum();
	    				int cells = sheet.getRow(1).getLastCellNum();
	    				
	    				FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
	    				
	    				 for (int r = 1; r <= rows; r++) {
	    					 CustomerVO customerVO = new CustomerVO();
	    					 CustomerPersonInChargeVO pvo = new CustomerPersonInChargeVO();
	    					 	String customerId = "";
	    						customerId = "CUS"+UUID.randomUUID().toString().replaceAll("-", "");
	    						customerVO.setCustomerId(customerId);
	    						pvo.setCustomerId(customerId);
	    			        	row = sheet.getRow(r); // row 가져오기
	    			        	if (row != null) {
	    			        		for (int c = 0; c < cells; c++) {
	    			        			cell = row.getCell(c);
	    			        			if (cell != null) {
	    			        				String value = null;
	    									switch (cell.getCellType()) {
	    									   	case XSSFCell.CELL_TYPE_FORMULA:
	    									   		if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_NUMERIC){
	    									   			value = "" + (int)cell.getNumericCellValue();
	    									   		}else if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_STRING){
	    									   			value = "" + cell.getStringCellValue(); 
	    									   		}
	    									   		value = cell.getCellFormula();
	    									   		break;
	    									   	case XSSFCell.CELL_TYPE_NUMERIC:
	    									   		value = "" + (int)cell.getNumericCellValue();
	    									   		break;
	    									   	case XSSFCell.CELL_TYPE_STRING:
	    									   		value = "" + cell.getStringCellValue();
	    									   		break;
	    									   	case XSSFCell.CELL_TYPE_BLANK:
	    									   		value = "";
	    									   		break;
	    									   	case XSSFCell.CELL_TYPE_ERROR:
	    									   		value = "" + cell.getErrorCellValue();
	    									   		break;
	    									   	default:
	    									}
	    									
	    									int nIndex = cell.getColumnIndex();
	    									if(nIndex == 0){
	    										if(value.equals("")){
	    											customerVO.setCustomerName(value);
	    											break;
	    										}else{
	    											customerVO.setCustomerName(value);
	    										}
	    									}else if(nIndex == 1){
	    										customerVO.setCustomerOwnerName(value);
	    									}else if(nIndex == 2){
	    										if(value.contains("법인")){
	    											customerVO.setCustomerKind("00");	
	    										}else if(value.contains("개인")){
	    											customerVO.setCustomerKind("01");
	    										}else if(value.contains("외국인")){
	    											customerVO.setCustomerKind("02");
	    										}else{
	    											customerVO.setCustomerKind("03");
	    										}
	    									}else if(nIndex == 3){
	    										customerVO.setBusinessLicenseNumber(value);
	    									}else if(nIndex == 4){
	    										customerVO.setCorporationRegistrationNumber(value);
	    									}else if(nIndex == 5){
	    										customerVO.setAddress(value);
	    									}else if(nIndex == 6){
	    										customerVO.setAddressDetail(value);
	    									}else if(nIndex == 7){
	    										customerVO.setBusinessCondition(value);
	    									}else if(nIndex == 8){
	    										customerVO.setBusinessKind(value);
	    									}else if(nIndex == 9){
	    										customerVO.setPhone(value);
	    									}else if(nIndex == 10){
	    										customerVO.setFaxNum(value);
	    									}else if(nIndex == 11){
	    										if(value.contains("현금매출")){
	    											customerVO.setSalesPurchaseDivision("00");	
	    										}else if(value.contains("현금영수증매출")){
	    											customerVO.setSalesPurchaseDivision("01");
	    										}else if(value.contains("현금매입")){
	    											customerVO.setSalesPurchaseDivision("02");
	    										}else if(value.contains("현금영수증매입")){
	    											customerVO.setSalesPurchaseDivision("03");
	    										}else if(value.contains("카드매출")){
	    											customerVO.setSalesPurchaseDivision("04");
	    										}else if(value.contains("카드매입")){
	    											customerVO.setSalesPurchaseDivision("05");
	    										}else if(value.contains("매출")){
	    											customerVO.setSalesPurchaseDivision("06");
	    										}else if(value.contains("매입")){
	    											customerVO.setSalesPurchaseDivision("07");
	    										}else if(value.contains("매출/매입")){
	    											customerVO.setSalesPurchaseDivision("08");
	    										}else if(value.contains("기타")){
	    											customerVO.setSalesPurchaseDivision("09");
	    										}
	    									}else if(nIndex == 12){
	    										if(value.contains("미발행")){
	    											customerVO.setBillingDivision("00");	
	    										}else if(value.contains("세금계산서")){
	    											customerVO.setBillingDivision("01");
	    										}else if(value.contains("세금계산서(일괄)")){
	    											customerVO.setBillingDivision("02");
	    										}else if(value.contains("계산서")){
	    											customerVO.setBillingDivision("03");
	    										}else if(value.contains("간이계산서")){
	    											customerVO.setBillingDivision("04");
	    										}else if(value.contains("신용카드")){
	    											customerVO.setBillingDivision("05");
	    										}else if(value.contains("직불카드")){
	    											customerVO.setBillingDivision("06");
	    										}else if(value.contains("현금영수증")){
	    											customerVO.setBillingDivision("07");
	    										}else if(value.contains("간이영수증")){
	    											customerVO.setBillingDivision("08");
	    										}
	    									}else if(nIndex == 13){
	    										customerVO.setBillingEmail(value);
	    									}
	    									else if(nIndex == 14){
	    										customerVO.setBankName(value);
	    									}
	    									else if(nIndex == 15){
	    										customerVO.setDepositor(value);
	    									}else if(nIndex == 16){
	    										customerVO.setAccountNumber(value);
	    									}else if(nIndex == 17){
	    										customerVO.setPaymentDt(value);
	    									}else if(nIndex == 18){
	    										customerVO.setSignificantData(value);
	    									}else if(nIndex == 19){
	    										customerVO.setEtc(value);
	    									}else if(nIndex == 20){
	    										if(value != null && !value.equals("")) {
	    											pvo.setDepartment(value);	
	    										}else {
	    											pvo.setDepartment("");
	    										}
	    									}else if(nIndex == 21){
	    										if(value != null && !value.equals("")) {
	    											pvo.setName(value);
	    										}else {
	    											pvo.setName("");
		    										pvo.setPersonInChargeId("");
	    										}
	    									}else if(nIndex == 22){
	    										if(value != null && !value.equals("")) {
	    											pvo.setPhoneNum(value);	
	    										}else {
	    											pvo.setPhoneNum("");
	    										}
	    									}else if(nIndex == 23){
	    										if(value != null && !value.equals("")) {
	    											pvo.setAddress(value);	
	    										}else {
	    											pvo.setAddress("");
	    										}
	    										
	    									}else if(nIndex == 24){
	    										if(value != null && !value.equals("")) {
	    											pvo.setEmail(value);	
	    										}else {
	    											pvo.setEmail("");
	    										}
	    										
	    									}else if(nIndex == 25){
	    										if(value != null && !value.equals("")) {
	    											pvo.setEtc(value);	
	    										}else {
	    											pvo.setEtc("");
	    										}
	    									}
	    									
	    			        			} else {
	    			        				System.out.print("[null]\t");
	    			        			}
	    			        		} // for(c) 문
	    			 //       		System.out.print("\n");
	    			        		customerService.insertCustomer(customerVO);
	    			        		if(pvo.getName().equals("")) {
	    			        			pvo.setPersonInChargeId("PIC"+UUID.randomUUID().toString().replaceAll("-", ""));
		    			        		personInChargeService.insertPersonInCharge(pvo);	
	    			        		}
	    			        		
	    			        	}
	    			        	
	    			        } // for(r) 문
	    				
	    			}
	    		}
	    	}else{
	    		resultApi.setResultCode("1111"); 	//file 없음
	    	}
			
			//resultApi = fileService.insertFile(rootDir, subDir,uuid,request);
			WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/carrier/list.do?allocationStatus=Y");
		}catch(Exception e){
			WebUtils.messageAndRedirectUrl(mav, "등록하는데 실패 하였습니다.관리자에게 문의 하세요.", "/carrier/list.do?allocationStatus=Y");
			e.printStackTrace();
		}
		
		return mav;
	}
	
	
	
	
	
	
	@RequestMapping(value = "/company", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView company(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			String searchType = request.getParameter("searchType") == null  ? "" : request.getParameter("searchType").toString();
			String searchWord = request.getParameter("searchWord") == null  ? "" : request.getParameter("searchWord").toString();
			
			paramMap.put("searchType", searchType);
			paramMap.put("searchWord", searchWord);
			
			int totalCount = companyService.selectCompanyListCount(paramMap);
			PagingUtils.setPageing(request, totalCount, paramMap);
			
			List<Map<String, Object>> companyList = companyService.selectCompanyList(paramMap);
			
			for(int i = 0; i < companyList.size(); i++) {
				Map<String, Object> emp = companyList.get(i);
				
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("contentId", emp.get("company_id").toString());
	      		map.put("categoryType", "company");
	      		List<Map<String, Object>> fileList = fileService.selectFileList(map);
	      		companyList.get(i).put("fileList", fileList);
			}
			
			mav.addObject("listData",  companyList);
			mav.addObject("paramMap", paramMap);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
						
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	@RequestMapping(value = "/add-company", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView addcompany(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			Map<String, Object> weekMap = new HashMap<String, Object>();
			Map<String, Object> monthMap = new HashMap<String, Object>();
			
			Date date = new Date();
			DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
			
			String formattedDate = dateFormat.format(date);
			
			mav.addObject("serverTime", formattedDate );
			
			mav.addObject("test", "test");
						
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	@RequestMapping(value = "/insert-company", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView insertCompany(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute CompanyVO companyVO) throws Exception {
		
		try{
			
			String companyId = "";
			companyId = "COM"+UUID.randomUUID().toString().replaceAll("-", "");
			companyVO.setCompanyId(companyId);
			
			Map<String, Object> map = new HashMap<String, Object>();
			
			map.put("duplicateChk", "true");
			map.put("companyId", companyVO.getCompanyId());
			
			Map<String, Object> companyMap = companyService.selectCompany(map);
			
			if(companyMap != null){
				WebUtils.messageAndRedirectUrl(mav, "이미 사용중인 아이디 입니다.", "/baseinfo/add-company.do");
			}else{
				companyService.insertCompany(companyVO);		
				subDir = "company";
				fileService.insertFile(rootDir, subDir, companyId, request);
				
				JSONParser jsonParser = new JSONParser();
				String accountList = request.getParameter("accountInfo");
				accountService.deleteAccount(map);
				
				if(!accountList.equals("")) {
					JSONObject jsonObject = (JSONObject) jsonParser.parse(accountList);
					JSONArray jsonArray = (JSONArray) jsonObject.get("accountList");
					for(int i = 0; i < jsonArray.size(); i++){
						AccountVO accountVO = new AccountVO();
						JSONObject obj = (JSONObject)jsonArray.get(i);
						String accountId = "ACI"+UUID.randomUUID().toString().replaceAll("-", "");
						accountVO.setAccountId(accountId);
						accountVO.setCompanyId(companyId);
						accountVO.setBankName(obj.get("bankName").toString());
						accountVO.setDepositor(obj.get("depositor").toString());
						accountVO.setAccountNumber(obj.get("accountNumber").toString());
						accountVO.setAccountName(obj.get("accountName").toString());
						accountService.insertAccount(accountVO);
					}	
				}
				WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/baseinfo/company.do");
				
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	@RequestMapping(value = "/edit-company", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView editCompany(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute CompanyVO companyVO) throws Exception {
		
		try{
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("duplicateChk", "true");
			map.put("companyId", companyVO.getCompanyId());
			Map<String, Object> companyMap = companyService.selectCompany(map);
			
			map.put("contentId", companyVO.getCompanyId());
      		map.put("categoryType", "company");
			
      		List<Map<String, Object>> fileList = fileService.selectFileList(map);
      		companyMap.put("fileList", fileList);
      		
      		List<Map<String, Object>> accountList = accountService.selectAccountList(map);
      		companyMap.put("accountList", accountList);
      		
			mav.addObject("companyMap",companyMap);
		}catch(Exception e){
			e.printStackTrace();
		}

		return mav;
	}

	@RequestMapping(value = "/update-company", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView updateCompany(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute CompanyVO companyVO) throws Exception {
		
		try{
			companyService.updateCompany(companyVO);
			
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
	    	List<MultipartFile> fileList = multipartRequest.getFiles("bbsFile");
			
			if(fileList != null && fileList.size() > 0 && !fileList.get(0).getOriginalFilename().equals("")){		//업로드 되는 파일이 존재 하면........
	    		//기존의 파일을 삭제 한다.
	    		Map<String, Object> map = new HashMap<String, Object>();
	    		map.put("contentId", companyVO.getCompanyId());
	      		map.put("categoryType", "company");
	    		fileService.deleteFile(map);
	    		//새파일을 저장한다.
				subDir = "company";
				fileService.insertFile(rootDir, subDir, companyVO.getCompanyId(), request);
	    	}
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("companyId", companyVO.getCompanyId());
			
			JSONParser jsonParser = new JSONParser();
			String accountList = request.getParameter("accountInfo");
			accountService.deleteAccount(map);
			
			if(!accountList.equals("")) {
				JSONObject jsonObject = (JSONObject) jsonParser.parse(accountList);
				JSONArray jsonArray = (JSONArray) jsonObject.get("accountList");
				
				for(int i = 0; i < jsonArray.size(); i++){
					AccountVO accountVO = new AccountVO();
					JSONObject obj = (JSONObject)jsonArray.get(i);
					String accountId = "ACI"+UUID.randomUUID().toString().replaceAll("-", "");
					accountVO.setAccountId(accountId);
					accountVO.setCompanyId(companyVO.getCompanyId());
					accountVO.setBankName(obj.get("bankName").toString());
					accountVO.setDepositor(obj.get("depositor").toString());
					accountVO.setAccountNumber(obj.get("accountNumber").toString());
					accountVO.setAccountName(obj.get("accountName").toString());
					accountService.insertAccount(accountVO);
				}	
			}
			
			WebUtils.messageAndRedirectUrl(mav, "수정되었습니다.", "/baseinfo/company.do");
		}catch(Exception e){
			e.printStackTrace();
		}

		return mav;
	}	
	
	
	@RequestMapping(value = "/getCompanyInfo", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi getCompanyInfo(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		ResultApi result = new ResultApi();
		try{
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("companyId", request.getParameter("companyId").toString());
			Map<String, Object> companyMap = companyService.selectCompany(map);
			result.setResultData(companyMap);
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}

		return result;
	}
	
	
	

	
	
	
	@RequestMapping(value = "/getCustomerList", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi getCustomerList(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		ResultApi result = new ResultApi();
		try{
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("customerName", request.getParameter("customerName").toString());
			List<Map<String, Object>> customerMap = customerService.selectCustomerList(map);
			result.setResultData(customerMap);
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}

		return result;
	}
	
	
	
	@RequestMapping(value = "/getCustomerInfo", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi getCustomerInfo(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		ResultApi result = new ResultApi();
		try{
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("companyId", request.getParameter("companyId").toString());
			Map<String, Object> companyMap = companyService.selectCompany(map);
			result.setResultData(companyMap);
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}

		return result;
	}

	
	@RequestMapping(value = "/getDriverList", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi getDriverList(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		ResultApi result = new ResultApi();
		try{
			
			Map<String, Object> map = new HashMap<String, Object>();
			
			map.put("customerId", request.getParameter("customerId").toString());
			
			//List<Map<String, Object>> driverList = driverService.selectDriverListByCustomerId(map);
			//result.setResultData(driverList);
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}

		return result;
	}
	
	
	
	
	
	
	@RequestMapping( value="/excel_download", method = RequestMethod.GET )
	public ModelAndView excel_download(ModelAndView mav,
			HttpServletRequest request ,HttpServletResponse response) throws Exception {
		
		ModelAndView view = new ModelAndView();
		
		try{
			Map<String, Object> paramMap = new HashMap<String, Object>();
			List<Map<String, Object>> list = null;

			String varNameList[] = null;
			String titleNameList[] = null;
			
			int totalCount = 0;
			String mode = "";
					
			if(request.getParameter("mode") != null && !request.getParameter("mode").equals("")){
				mode = request.getParameter("mode").toString();
			}
			
			paramMap.put("searchType", " ");
			paramMap.put("searchWord", "all");
			
			
			if(mode.equals("driver")){
				totalCount = driverService.selectDriverListCount(paramMap);
				paramMap.put("startRownum", 0);
				paramMap.put("numOfRows", totalCount);
				list = driverService.selectDriverList(paramMap);
				titleNameList = new String[]{"소유주","이름","연락처","주민번호","자격증번호","사업자구분","사업자번호","업태","종목","사업장주소","소속","구분","입사일","퇴사일","적용요율","지입료(원)","예치금(원)","계좌정보","차종","약칭","차량소속","차량번호","차고지","차대번호"};
				varNameList = new String[]{"driver_owner","driver_name","phone_num","resident_registration_number","driver_license","company_kind","business_license_number","business_condition","business_kind","address","assign_company","driver_kind","join_dt","resign_dt","deduction_rate","driver_balance","driver_deposit","bank_name","car_kind","car_nickname","car_assign_company","car_num","car_garage","car_id_num"};
				
			}else if(mode.equals("customer")){
				totalCount = customerService.selectCustomerListCount(paramMap);
				paramMap.put("startRownum", 0);
				paramMap.put("numOfRows", totalCount);
				list = customerService.selectCustomerList(paramMap);				
				titleNameList = new String[]{"회사명","대표자명","회사구분(sheet1참조)","사업자번호","법인등록번호","주소","상세주소","업태","종목","대표전화","팩스","매출매입구분(sheet1참조)","세금계산서(sheet1참조)","세금계산서이메일","은행명","예금주","계좌번호","결제일","특이사항","비고"};
				varNameList = new String[]{"customer_name","customer_owner_name","customer_kind","business_license_number","corporation_registration_number","address","address_detail","business_condition","business_kind","phone","fax_num","sales_purchase_division","billing_division","billing_email","bank_name","depositor","account_number","payment_dt","significant_data","etc"};
			}
			
			view.setViewName("excelDownloadView");
			view.addObject("list", list);
			view.addObject("varNameList", varNameList);
			view.addObject("titleNameList", titleNameList);
			view.addObject("excelName", mode+"-list.xlsx");	
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return view;
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
