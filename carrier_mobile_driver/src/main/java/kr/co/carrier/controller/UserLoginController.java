package kr.co.carrier.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.co.carrier.service.AdminService;
import kr.co.carrier.service.CompanyService;
import kr.co.carrier.service.DriverLocationInfoService;
import kr.co.carrier.service.DriverService;
import kr.co.carrier.service.EmployeeService;
import kr.co.carrier.service.UserService;
import kr.co.carrier.utils.ResultApi;
import kr.co.carrier.vo.DriverLocationInfoVO;
	

@Controller
public class UserLoginController {

	private static final Logger logger = LoggerFactory.getLogger(UserLoginController.class);
	
	@Autowired
	AdminService adminService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	CompanyService companyService;
	
	@Autowired
	DriverService driverService;
	
	@Autowired
	DriverLocationInfoService driverLocationInfoService;
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */

	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView index(ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception{
		
		try{
			
			Map<String,Object> map = new HashMap<String,Object>();
			List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
			mav.addObject("companyList", companyList);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return mav;
	}
	
	@RequestMapping(value = "/nopermission", method = RequestMethod.GET)
	public ModelAndView nopermission(ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception{
		
		try{
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return mav;
	}
	
	@RequestMapping(value = "/loginPersist", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi loginPersist(
			HttpServletRequest request, 
			HttpServletResponse response,
			HttpSession session, 
			@RequestParam String driverId,
			@RequestParam String driverPwd
			) throws Exception{
			ResultApi result = new ResultApi();
			
			try{

				Map<String, Object> map = new HashMap<String, Object>();
		    	map.put("driverId", driverId);
		    	map.put("driverPwd", driverPwd);
			/* map.put("forUpdatePassWord", "5"); */
		    	
		    	
		    //	Map<String, Object> userMap = adminService.selectAdmin(map);
		    	Map<String, Object> userMap = driverService.selectDriver(map);
		    	
		    	
		    	//guid
		    	if(userMap == null){
		    		result.setResultCode("E001");
		    	}else{
		    		
		    		if(userMap.get("driver_status") != null && !userMap.get("driver_status").toString().equals("")){
		    		
		    			if(userMap.get("driver_status").toString().equals("01")) {
		    				result.setResultCode("0000");
						    session.setAttribute(driverId, userMap);	//로그인 성공
					    	session.setAttribute("user", userMap);		
		    			}else {
		    				result.setResultCode("E002");
		    				
		    				
		    			}
		    			
		    			
		    		}
		    		
		    			
		    	}
		    	
			}catch( Exception e){
				result.setResultCode("E999");
				e.printStackTrace();
			}
			
			return result;
	}
	
	
	
	@RequestMapping(value = "/updateDriverDeviceToken", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updateDriverDeviceToken(
			HttpServletRequest request, 
			HttpServletResponse response,
			HttpSession session
			) throws Exception{
			ResultApi result = new ResultApi();
			
			try{

				Map userSessionMap = (Map)session.getAttribute("user");
				
				String token = request.getParameter("token") != null && !request.getParameter("token").toString().equals("") ? request.getParameter("token").toString() : "";  
				String deviceOs = request.getParameter("deviceOs") != null && !request.getParameter("deviceOs").toString().equals("") ? request.getParameter("deviceOs").toString() : "";
				
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("driverId", userSessionMap.get("driver_id").toString());
		    	map.put("token", token);
		    	map.put("deviceOs", deviceOs);

		    	driverService.updateDriverDeviceToken(map);
		    	
			}catch( Exception e){
				result.setResultCode("E999");
				e.printStackTrace();
			}
			
			return result;
	}
	
	
	@RequestMapping(value = "/updateDriverDeviceLocation", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updateDriverDeviceLocation(
			HttpServletRequest request, 
			HttpServletResponse response,
			HttpSession session
			) throws Exception{
			ResultApi result = new ResultApi();
			
			try{

				Map userSessionMap = (Map)session.getAttribute("user");
				Map<String, Object> map = new HashMap<String, Object>();
				
				String location = request.getParameter("location") != null && !request.getParameter("location").toString().equals("") ? request.getParameter("location").toString() : "";  
				String lat = "";
				String lng = "";
				
				String[] locationArr = location.split(",");
				if(locationArr.length > 1) {
					lat = locationArr[0];
					lng = locationArr[1];
					map.put("lat", lat);
			    	map.put("lng", lng);
				}else {
					
				}
				
				String driverId = request.getParameter("driverId") != null && !request.getParameter("driverId").toString().equals("") ? request.getParameter("driverId").toString() : "";
				
				if(driverId.equals("")) {
					if(userSessionMap != null) {
						map.put("driverId", userSessionMap.get("driver_id").toString());
					}	
				}else {
					map.put("driverId", driverId);
				}
				
				driverService.updateDriverLatLng(map);
				DriverLocationInfoVO driverLocationInfoVO = new DriverLocationInfoVO();
				driverLocationInfoVO.setLocationInfoId("DLI"+UUID.randomUUID().toString().replaceAll("-",""));
				driverLocationInfoVO.setDriverId(map.get("driverId") != null ? map.get("driverId").toString() : "");
				driverLocationInfoVO.setDriverName("");
				driverLocationInfoVO.setLat(lat);
				driverLocationInfoVO.setLng(lng);
				
				if(!driverLocationInfoVO.getDriverId().equals("")) {
					driverLocationInfoService.insertDriverLocationInfo(driverLocationInfoVO);	
				}
				
				
			}catch( Exception e){
				result.setResultCode("E999");
				//e.printStackTrace();
			}
			
			return result;
	}
	
	
	@RequestMapping(value = "/updateDriverPassword", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updateDriverPassword(
			HttpServletRequest request, 
			HttpServletResponse response,
			HttpSession session
			) throws Exception{
			ResultApi result = new ResultApi();
			
			try{

				String driverId = request.getParameter("driverId") != null && !request.getParameter("driverId").toString().equals("") ? request.getParameter("driverId").toString() : "";
				String driverPwd = request.getParameter("driverPwd") != null && !request.getParameter("driverPwd").toString().equals("") ? request.getParameter("driverPwd").toString() : "";
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("driverId", driverId);
				map.put("forUpdatePassWord", "");
				Map<String, Object> driverMap = driverService.selectDriver(map); 
				
				if(driverMap != null) {
					map.put("driverPwd",driverPwd);
					driverService.updateDriverPassWord(map);
				}else {
					result.setResultCode("E000");
					result.setResultMsg("아이디를 찾을 수 없습니다.");
				}
				
			}catch( Exception e){
				result.setResultCode("E999");
				e.printStackTrace();
			}
			
			return result;
	}
	
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpSession session) {
	//	UserSessionVO  userSessionVO  = (UserSessionVO)session.getAttribute(BaseAppConstants.USER_SESSION_KEY);
		//loginManagerService.removeSession(userSessionVO.getUserId());
		session.invalidate();
		return "redirect:/carrier/login-page.do?&status=logout";
	}
	
	
	
	
}
