package kr.co.carrier.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import kr.co.carrier.service.AdjustmentService;
import kr.co.carrier.service.AllocationDivisionService;
import kr.co.carrier.service.AllocationFileService;
import kr.co.carrier.service.AllocationService;
import kr.co.carrier.service.BillingDivisionService;
import kr.co.carrier.service.CarInfoService;
import kr.co.carrier.service.CheckDetailService;
import kr.co.carrier.service.CompanyService;
import kr.co.carrier.service.DebtorCreditorService;
import kr.co.carrier.service.DecideFinalService;
import kr.co.carrier.service.DriverBillingFileService;
import kr.co.carrier.service.DriverBillingStatusService;
import kr.co.carrier.service.DriverDecideStatusService;
import kr.co.carrier.service.DriverDeductFileService;
import kr.co.carrier.service.DriverDeductInfoService;
import kr.co.carrier.service.DriverService;
import kr.co.carrier.service.ElectionService;
import kr.co.carrier.service.FileUploadService;
import kr.co.carrier.service.LowViolationService;
import kr.co.carrier.service.PayDivisionService;
import kr.co.carrier.service.PaymentDivisionService;
import kr.co.carrier.service.PaymentInfoService;
import kr.co.carrier.service.PersonInChargeService;
import kr.co.carrier.service.RunDivisionService;
import kr.co.carrier.service.SendSmsService;
import kr.co.carrier.service.VoteService;
import kr.co.carrier.utils.BaseAppConstants;
import kr.co.carrier.utils.MailUtils;
import kr.co.carrier.utils.PagingUtils;
import kr.co.carrier.utils.ParamUtils;
import kr.co.carrier.utils.ResultApi;
import kr.co.carrier.utils.WebUtils;
import kr.co.carrier.vo.AllocationFileVO;
import kr.co.carrier.vo.AllocationVO;
import kr.co.carrier.vo.CarCheckDetailVO;
import kr.co.carrier.vo.DebtorCreditorVO;
import kr.co.carrier.vo.DriverBillingStatusVO;
import kr.co.carrier.vo.DriverDecideStatusVO;
import kr.co.carrier.vo.DriverVO;
import kr.co.carrier.vo.ElectionVO;
import kr.co.carrier.vo.VoteVO;

@Controller
@RequestMapping(value="/carrier")
public class CarrierController {

	private static final Logger logger = LoggerFactory.getLogger(CarrierController.class);
	
	
	 @Autowired
	    private AllocationService allocationService;
		
	    @Autowired
	    private CompanyService companyService;
	    
	    @Autowired
	    private DriverService driverService;
	    
	    @Autowired
	    private CarInfoService carInfoService;
	    
	    @Autowired
	    private BillingDivisionService billingDivisionService;
	    
	    @Autowired
	    private AllocationDivisionService allocationDivisionService;
	    
	    @Autowired
	    private RunDivisionService runDivisionService;

	    @Autowired
	    private PaymentDivisionService paymentDivisionService;
	    
	    @Autowired
	    private PayDivisionService payDivisionService;
	    
	    @Autowired
	    private PaymentInfoService paymentInfoService;
	
	    @Autowired
	    private AllocationFileService allocationFileService;
	
	    @Autowired
		private FileUploadService fileUploadService;
	    
	    @Autowired
	    private CheckDetailService checkDetailService;
	    
	    @Autowired
	    private PersonInChargeService personInChargeService;
	    
	    @Autowired
	    private DriverDeductInfoService driverDeductInfoService;
	    
	    @Autowired
	    private DriverDeductFileService driverDeductFileService;
	  
	    @Autowired
	    private DriverDecideStatusService driverDecideStatusService;
	    
	    
	    @Autowired
	    private DecideFinalService decideFinalService;
	    
	    @Autowired
	    private DriverBillingFileService driverBillingFileService;
	    
	    @Autowired
	    private DriverBillingStatusService driverBillingStatusService;
	    
	    @Autowired
	    private LowViolationService lowViolationService;
	    	    
	    @Autowired
	    private ElectionService electionService;
	    
	    @Autowired
	    private VoteService voteService;
	    
	    @Autowired
	    private DebtorCreditorService debtorCreditorService;
	    
	    @Autowired
	    private AdjustmentService adjustmentService;
	    
	    @Autowired
	    private SendSmsService sendSmsService;
	    
	    
	    @Value("#{appProp['upload.base.path']}")
	    private String rootDir;    
	    private String subDir;
	    
	    
	@RequestMapping(value = "/index", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView adviserDetail(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
			
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	@RequestMapping(value = "/login-page", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView login_page(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user"); 
			Map<String, Object> userMap = null;	
			
			if(userSessionMap != null) {
				//WebUtils.messageAndRedirectUrl(mav, "자동 로그인 되었습니다.", "/carrier/main.do");
				response.sendRedirect(request.getContextPath()+"/carrier/main.do");	
			}
			
			
			//WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/allocation/combination.do?forOpen=N");
			//mav.setViewName("/menu/menu-sub");
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	@RequestMapping(value = "/changePassWord", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView changePassWord(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	
	
	
	@RequestMapping(value = "/main", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView main(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
			
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("allocationStatus", "A");
				map.put("driverId", userSessionMap.get("driver_id").toString());
				int aList = allocationService.selectAllocationMainListCount(map); 
				map.put("allocationStatus", "Y");
				int yList = allocationService.selectAllocationMainListCount(map);
				int lowViolationListCount = lowViolationService.selectLowViolationListCount(map);
				
				/*map.put("allocationStatus", "F");
				int fList = allocationService.selectAllocationMainListCount(map);*/
				
				mav.addObject("aList",aList );
				mav.addObject("yList", yList);
				mav.addObject("lowViolationList", lowViolationListCount);
				/*mav.addObject("fList", fList);*/
				mav.addObject("driverId", userSessionMap.get("driver_id").toString());
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	@RequestMapping(value = "/sub-main", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView subMain(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
			
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("allocationStatus", "A");
				map.put("driverId", userSessionMap.get("driver_id").toString());
				int aList = allocationService.selectAllocationMainListCount(map); 
				map.put("allocationStatus", "Y");
				int yList = allocationService.selectAllocationMainListCount(map);
				int lowViolationListCount = lowViolationService.selectLowViolationListCount(map);
				
				/*map.put("allocationStatus", "F");
				int fList = allocationService.selectAllocationMainListCount(map);*/
				
				mav.addObject("aList",aList );
				mav.addObject("yList", yList);
				mav.addObject("lowViolationList", lowViolationListCount);
				/*mav.addObject("fList", fList);*/
				mav.addObject("driverId", userSessionMap.get("driver_id").toString());
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	@RequestMapping(value = "/allocation-list", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView allocationList(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
			
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	@RequestMapping(value = "/car-check-detail", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView carCheckDetail(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				String allocationStatus = request.getParameter("allocationStatus");
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				Map<String, Object> map = new HashMap<String, Object>();
				List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
				List<Map<String, Object>> driverList = driverService.selectDriverList(map);
				List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
				List<Map<String, Object>> allocationDivisionList = allocationDivisionService.selectAllocationDivisionInfoList();
				List<Map<String, Object>> runDivisionList = runDivisionService.selectRunDivisionInfoList();
				List<Map<String, Object>> paymentDivisionList = paymentDivisionService.selectPaymentDivisionInfoList();
				List<Map<String, Object>> payDivisionList = payDivisionService.selectPayDivisionInfoList();
				
				map.put("allocationId", request.getParameter("allocationId").toString());
				map.put("driverId", userSessionMap.get("driver_id").toString());
				Map<String, Object> allocationMap = allocationService.selectAllocation(map);
				map.put("batchStatusId", allocationMap.get("batch_status_id"));
				List<Map<String, Object>> allocationList = allocationService.selectAllocationList(map);
				
				for(int i = 0; i < allocationList.size(); i++) {
					Map<String, Object> allocation = allocationList.get(i); 
					map.put("allocationId", allocation.get("allocation_id").toString());
					Map<String, Object> carInfo = carInfoService.selectCarInfo(map);
					allocation.put("carInfo", carInfo);
					List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(map);
					allocation.put("paymentInfoList", paymentInfoList);
					allocationList.set(i,allocation);
				}
					mav.addObject("allocationMap",  allocationMap);
					mav.addObject("allocationList",  allocationList);
					mav.addObject("billingDivisionList",  billingDivisionList);
					mav.addObject("allocationDivisionList",  allocationDivisionList);
					mav.addObject("runDivisionList",  runDivisionList);
					mav.addObject("paymentDivisionList",  paymentDivisionList);
					mav.addObject("payDivisionList",  payDivisionList);
					mav.addObject("driverList",  driverList);
					mav.addObject("companyList",  companyList);
					mav.addObject("paramMap", paramMap);
					mav.addObject("allocationStatus", allocationStatus);
					map.put("allocationStatus", allocationStatus);
					List<Map<String, Object>> allocationFileList = allocationFileService.selectAllocationFileList(map);
					mav.addObject("allocationFileList", allocationFileList);	
					mav.addObject("driverId", userSessionMap.get("driver_id").toString());
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	@RequestMapping(value = "/car-check-detail-forGroup", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView carCheckDetailForGroup(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				String allocationStatus = request.getParameter("allocationStatus");
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				Map<String, Object> map = new HashMap<String, Object>();
				List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
				List<Map<String, Object>> driverList = driverService.selectDriverList(map);
				List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
				List<Map<String, Object>> allocationDivisionList = allocationDivisionService.selectAllocationDivisionInfoList();
				List<Map<String, Object>> runDivisionList = runDivisionService.selectRunDivisionInfoList();
				List<Map<String, Object>> paymentDivisionList = paymentDivisionService.selectPaymentDivisionInfoList();
				List<Map<String, Object>> payDivisionList = payDivisionService.selectPayDivisionInfoList();
				
				map.put("allocationId", request.getParameter("allocationId").toString());
				map.put("driverId", userSessionMap.get("driver_id").toString());
				Map<String, Object> allocationMap = allocationService.selectAllocation(map);
				map.put("batchStatusId", allocationMap.get("batch_status_id"));
				List<Map<String, Object>> allocationList = allocationService.selectAllocationList(map);
				
				for(int i = 0; i < allocationList.size(); i++) {
					Map<String, Object> allocation = allocationList.get(i); 
					map.put("allocationId", allocation.get("allocation_id").toString());
					Map<String, Object> carInfo = carInfoService.selectCarInfo(map);
					allocation.put("carInfo", carInfo);
					List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(map);
					allocation.put("paymentInfoList", paymentInfoList);
					allocationList.set(i,allocation);
				}
					mav.addObject("allocationMap",  allocationMap);
					mav.addObject("allocationList",  allocationList);
					mav.addObject("billingDivisionList",  billingDivisionList);
					mav.addObject("allocationDivisionList",  allocationDivisionList);
					mav.addObject("runDivisionList",  runDivisionList);
					mav.addObject("paymentDivisionList",  paymentDivisionList);
					mav.addObject("payDivisionList",  payDivisionList);
					mav.addObject("driverList",  driverList);
					mav.addObject("companyList",  companyList);
					mav.addObject("paramMap", paramMap);
					mav.addObject("allocationStatus", allocationStatus);
					map.put("allocationStatus", allocationStatus);
					List<Map<String, Object>> allocationFileList = allocationFileService.selectAllocationFileList(map);
					mav.addObject("allocationFileList", allocationFileList);	
					mav.addObject("driverId", userSessionMap.get("driver_id").toString());
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	@RequestMapping(value = "/car-check-detail-batch-forGroup", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView carCheckDetailBatchForGroup(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				
				String allocationIdArr = request.getParameter("allocationId").toString();
				String[] allocationId = allocationIdArr.split(",");
				String groupId = allocationId[0];
				
				String allocationStatus = request.getParameter("allocationStatus");
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				Map<String, Object> map = new HashMap<String, Object>();
				List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
				List<Map<String, Object>> driverList = driverService.selectDriverList(map);
				List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
				List<Map<String, Object>> allocationDivisionList = allocationDivisionService.selectAllocationDivisionInfoList();
				List<Map<String, Object>> runDivisionList = runDivisionService.selectRunDivisionInfoList();
				List<Map<String, Object>> paymentDivisionList = paymentDivisionService.selectPaymentDivisionInfoList();
				List<Map<String, Object>> payDivisionList = payDivisionService.selectPayDivisionInfoList();
				
				map.put("allocationId", allocationId[0]);
				map.put("driverId", userSessionMap.get("driver_id").toString());
				Map<String, Object> allocationMap = allocationService.selectAllocation(map);				
				List<Map<String, Object>> allocationList = allocationService.selectAllocationList(map);
				
				for(int i = 0; i < allocationList.size(); i++) {
					Map<String, Object> allocation = allocationList.get(i); 
					map.put("allocationId", allocation.get("allocation_id").toString());
					Map<String, Object> carInfo = carInfoService.selectCarInfo(map);
					allocation.put("carInfo", carInfo);
					List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(map);
					allocation.put("paymentInfoList", paymentInfoList);
					allocationList.set(i,allocation);
				}
				
				List<Map<String, Object>> groupList = new ArrayList<Map<String, Object>>();
				for(int i = 0; i < allocationId.length; i++) {
					Map<String, Object> selectMap = new HashMap<String, Object>();
					selectMap.put("allocationId", allocationId[i]);
					selectMap.put("driverId", userSessionMap.get("driver_id").toString());
					selectMap.put("allocationStatus", allocationStatus);
					groupList.add(allocationService.selectAllocation(selectMap));
				}
				
					mav.addObject("allocationMap",  allocationMap);
					mav.addObject("allocationList",  allocationList);
					mav.addObject("billingDivisionList",  billingDivisionList);
					mav.addObject("allocationDivisionList",  allocationDivisionList);
					mav.addObject("runDivisionList",  runDivisionList);
					mav.addObject("paymentDivisionList",  paymentDivisionList);
					mav.addObject("payDivisionList",  payDivisionList);
					mav.addObject("driverList",  driverList);
					mav.addObject("companyList",  companyList);
					mav.addObject("paramMap", paramMap);
					mav.addObject("allocationStatus", allocationStatus);
					map.put("allocationStatus", allocationStatus);
					List<Map<String, Object>> allocationFileList = allocationFileService.selectAllocationFileList(map);
					mav.addObject("allocationFileList", allocationFileList);	
					mav.addObject("driverId", userSessionMap.get("driver_id").toString());
					mav.addObject("allocationIdArr", allocationIdArr);
					mav.addObject("groupId",  groupId);
					mav.addObject("groupList",  groupList);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	
	
	
	@RequestMapping(value = "/list", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView list(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
			
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				String allocationStatus = request.getParameter("allocationStatus").toString();
				
				/*String startDt = request.getParameter("startDt") == null ? WebUtils.getNow("yyyy-MM-dd") : request.getParameter("startDt").toString();
				String endDt = request.getParameter("endDt") == null ? WebUtils.getNow("yyyy-MM-dd") : request.getParameter("endDt").toString();
				if(startDt.equals("")) {
					startDt = WebUtils.getNow("yyyy-MM-dd");
				}
				if(endDt.equals("")) {
					endDt = WebUtils.getNow("yyyy-MM-dd");
				}
				paramMap.put("startDt", startDt);
				paramMap.put("endDt", endDt);*/
				
				
				paramMap.put("allocationStatus", allocationStatus);
				paramMap.put("driverId", userSessionMap.get("driver_id").toString());
				int totalCount = allocationService.selectAllocationListCount(paramMap);
				PagingUtils.setPageing(request, totalCount, paramMap);
				/*List<Map<String, Object>> allocationList = allocationService.selectAllocationList(paramMap);*/
				
				List<Map<String, Object>> allocationList = allocationService.selectAllocationListGroup(paramMap);
				
				
				Map<String, Object> map = new HashMap<String, Object>();
				List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
				List<Map<String, Object>> driverList = driverService.selectDriverList(map);
				List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
				mav.addObject("billingDivisionList",  billingDivisionList);
				mav.addObject("driverList",  driverList);
				mav.addObject("companyList",  companyList);
				mav.addObject("listData",  allocationList);
				map.put("driverId", userSessionMap.get("driver_id").toString());
				mav.addObject("paramMap", paramMap);
				mav.addObject("driverId", userSessionMap.get("driver_id").toString());	
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	@RequestMapping(value = "/finish-list", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView finishList(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
			
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				String allocationStatus = request.getParameter("allocationStatus").toString();
				
				//$("#selectType").val()
				
				String selectType = request.getParameter("selectType") == null || request.getParameter("selectType").toString().equals("") ? "D" : request.getParameter("selectType").toString();
				String startDt = request.getParameter("startDt") == null || request.getParameter("startDt").toString().equals("") ? WebUtils.getNow("yyyy-MM-dd") : request.getParameter("startDt").toString();
				String endDt = request.getParameter("endDt") == null ? WebUtils.getNow("yyyy-MM-dd") : request.getParameter("endDt").toString();
				if(startDt.equals("")) {
					startDt = WebUtils.getNow("yyyy-MM-dd");
				}
				if(endDt.equals("")) {
					endDt = WebUtils.getNow("yyyy-MM-dd");
				}
				
				if(selectType.equals("D")) {
					endDt = startDt;				//선택일 조회시 시작일과 종료일은 같고
				}else{
												//전체월 조회시 시작일은 해당월의 1일이고 종료일은 말일
					startDt = startDt.split("-")[0]+"-"+startDt.split("-")[1]+"-01";
					endDt = startDt.split("-")[0]+"-"+startDt.split("-")[1]+"-"+WebUtils.getLastDate("yyyy-mm-dd",Integer.parseInt(startDt.split("-")[0]),Integer.parseInt(startDt.split("-")[1]));
					
				}
				
				
				paramMap.put("startDt", startDt);
				paramMap.put("endDt", endDt);
				paramMap.put("selectType", selectType);
				
				paramMap.put("allocationStatus", allocationStatus);
				paramMap.put("driverId", userSessionMap.get("driver_id").toString());
				int totalCount = allocationService.selectAllocationListCount(paramMap);
				PagingUtils.setPageing(request, totalCount, paramMap);
				//List<Map<String, Object>> allocationList = allocationService.selectAllocationList(paramMap);
				List<Map<String, Object>> allocationList = allocationService.selectAllocationListGroup(paramMap);
				
				if(selectType.equals("M")) {
					startDt = startDt+" ~ "+endDt;
					paramMap.put("startDt", startDt);
				}
				Map<String, Object> map = new HashMap<String, Object>();
				List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
				List<Map<String, Object>> driverList = driverService.selectDriverList(map);
				List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
				mav.addObject("billingDivisionList",  billingDivisionList);
				mav.addObject("driverList",  driverList);
				mav.addObject("companyList",  companyList);
				mav.addObject("listData",  allocationList);
				map.put("driverId", userSessionMap.get("driver_id").toString());
				mav.addObject("paramMap", paramMap);
				mav.addObject("driverId", userSessionMap.get("driver_id").toString());	
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@RequestMapping(value = "/group-list", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView groupList(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
			
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				String allocationStatus = request.getParameter("allocationStatus").toString();
				String allocationId = request.getParameter("allocationId").toString();
				String groupId = request.getParameter("allocationId").toString();
				
				paramMap.put("allocationStatus", allocationStatus);
				paramMap.put("driverId", userSessionMap.get("driver_id").toString());
				paramMap.put("allocationId",allocationId);
				paramMap.put("groupId",groupId);
				
				Map<String, Object> allocation = allocationService.selectAllocation(paramMap);
				paramMap.put("driverCnt",allocation.get("driver_cnt") != null && !allocation.get("driver_cnt").toString().equals("") ? allocation.get("driver_cnt").toString() : "1");
				paramMap.put("departureDt",allocation.get("departure_dt").toString());
				List<Map<String, Object>> allocationList = allocationService.selectAllocationListByDepartureDtAndDriverCnt(paramMap);
				
				
				Map<String, Object> map = new HashMap<String, Object>();
				List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
				List<Map<String, Object>> driverList = driverService.selectDriverList(map);
				List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
				mav.addObject("billingDivisionList",  billingDivisionList);
				mav.addObject("driverList",  driverList);
				mav.addObject("companyList",  companyList);
				mav.addObject("listData",  allocationList);
				map.put("driverId", userSessionMap.get("driver_id").toString());
				mav.addObject("paramMap", paramMap);
				mav.addObject("allocationStatus", allocationStatus);
				mav.addObject("driverId", userSessionMap.get("driver_id").toString());
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	@RequestMapping(value = "/group-list-finish", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView groupListFinish(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
			
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				String allocationStatus = request.getParameter("allocationStatus").toString();
				String allocationId = request.getParameter("allocationId").toString();
				String groupId = request.getParameter("allocationId").toString();
				
				paramMap.put("allocationStatus", allocationStatus);
				paramMap.put("driverId", userSessionMap.get("driver_id").toString());
				paramMap.put("allocationId",allocationId);
				paramMap.put("groupId",groupId);
				
				Map<String, Object> allocation = allocationService.selectAllocation(paramMap);
				paramMap.put("driverCnt",allocation.get("driver_cnt").toString());
				paramMap.put("departureDt",allocation.get("departure_dt").toString());
				List<Map<String, Object>> allocationList = allocationService.selectAllocationListByDepartureDtAndDriverCnt(paramMap);
				
				
				Map<String, Object> map = new HashMap<String, Object>();
				List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
				List<Map<String, Object>> driverList = driverService.selectDriverList(map);
				List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
				mav.addObject("billingDivisionList",  billingDivisionList);
				mav.addObject("driverList",  driverList);
				mav.addObject("companyList",  companyList);
				mav.addObject("listData",  allocationList);
				map.put("driverId", userSessionMap.get("driver_id").toString());
				mav.addObject("paramMap", paramMap);
				mav.addObject("allocationStatus", allocationStatus);
				mav.addObject("driverId", userSessionMap.get("driver_id").toString());
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	
	
	@RequestMapping(value = "/allocation-detail", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView allocationDetail(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response)  throws Exception{
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				
				String allocationStatus = request.getParameter("allocationStatus");
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				Map<String, Object> map = new HashMap<String, Object>();
				List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
				List<Map<String, Object>> driverList = driverService.selectDriverList(map);
				List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
				List<Map<String, Object>> allocationDivisionList = allocationDivisionService.selectAllocationDivisionInfoList();
				List<Map<String, Object>> runDivisionList = runDivisionService.selectRunDivisionInfoList();
				List<Map<String, Object>> paymentDivisionList = paymentDivisionService.selectPaymentDivisionInfoList();
				List<Map<String, Object>> payDivisionList = payDivisionService.selectPayDivisionInfoList();
				
				
				map.put("allocationId", request.getParameter("allocationId").toString());
				map.put("driverId", userSessionMap.get("driver_id").toString());
				Map<String, Object> allocationMap = allocationService.selectAllocation(map);
				map.put("batchStatusId", allocationMap.get("batch_status_id"));
				List<Map<String, Object>> allocationList = allocationService.selectAllocationList(map);
				List<Map<String, Object>> batchStatusIdList = allocationService.selectGetBatchStausIdForPickUp(map);
				
				if(allocationStatus.equals("R")) {
					map.put("picturePointerR", allocationMap.get("picture_pointer_r").toString());	
				}else if(allocationStatus.equals("D")) {
					map.put("picturePointerD", allocationMap.get("picture_pointer_d").toString());
				}else if(allocationStatus.equals("I")) {
					map.put("picturePointerI", allocationMap.get("picture_pointer_i").toString());
				}else if(allocationStatus.equals("F")) {
					map.put("picturePointerR", allocationMap.get("picture_pointer_r").toString());
					map.put("picturePointerD", allocationMap.get("picture_pointer_d").toString());
					map.put("picturePointerI", allocationMap.get("picture_pointer_i").toString());
				}
				
				String departurePoint = "";
				String arrivalPoint = "";
				
				for(int i = 0; i < allocationList.size(); i++) {
					Map<String, Object> allocation = allocationList.get(i); 
					map.put("allocationId", allocation.get("allocation_id").toString());
					Map<String, Object> carInfo = carInfoService.selectCarInfo(map);
					
					String departure = carInfo.get("departure_addr") != null && !carInfo.get("departure_addr").toString().equals("") ? carInfo.get("departure_addr").toString() : "";
					String arrival = carInfo.get("arrival_addr") != null && !carInfo.get("arrival_addr").toString().equals("") ? carInfo.get("arrival_addr").toString() : "";
					
					
					try {
						departure = WebUtils.getAddressInfo(departure);
						arrival = WebUtils.getAddressInfo(arrival);
						departurePoint = WebUtils.getAddressToLatLng(departure);
						arrivalPoint = WebUtils.getAddressToLatLng(arrival);
					}catch(Exception e) {
						departurePoint = "";
						arrivalPoint = "";
						//e.printStackTrace();
					}finally {
						
					}
					
					Map<String, Object> checkDetailInfo = checkDetailService.selectCheckDetail(map); 
					allocation.put("carInfo", carInfo);
					allocation.put("checkDetailInfo", checkDetailInfo);
					List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(map);
					allocation.put("paymentInfoList", paymentInfoList);
					
					if(allocation != null && allocation.get("charge_id") != null && !allocation.get("charge_id").toString().equals("")) {
						Map<String, Object> personInChargeMap = new HashMap<String, Object>();
						personInChargeMap.put("personInChargeId", allocation.get("charge_id").toString());
						Map<String, Object> personInCharge = personInChargeService.selectPersonInChargeByPersonInChargeId(personInChargeMap);
						if(personInCharge != null) {
							String department = personInCharge.get("department") != null && !personInCharge.get("department").toString().equals("") ? personInCharge.get("department").toString() : ""; 
							allocation.put("department", department);	
						}
					//	String department = personInChargeService.selectPersonInChargeByPersonInChargeId(personInChargeMap).get("department").toString();
					//	String department = personInChargeService.selectPersonInChargeByPersonInChargeId(personInChargeMap).get("department") != null && !personInChargeService.selectPersonInChargeByPersonInChargeId(personInChargeMap).get("department").toString().equals("") ? personInChargeService.selectPersonInChargeByPersonInChargeId(personInChargeMap).get("department").toString():"";
					//	allocation.put("department", department);
					}
					allocationList.set(i,allocation);
				}
					mav.addObject("allocationMap",  allocationMap);
					mav.addObject("allocationList",  allocationList);
					mav.addObject("billingDivisionList",  billingDivisionList);
					mav.addObject("allocationDivisionList",  allocationDivisionList);
					mav.addObject("runDivisionList",  runDivisionList);
					mav.addObject("paymentDivisionList",  paymentDivisionList);
					mav.addObject("payDivisionList",  payDivisionList);
					mav.addObject("driverList",  driverList);
					mav.addObject("companyList",  companyList);
					mav.addObject("paramMap", paramMap);
					mav.addObject("allocationStatus", allocationStatus);
					mav.addObject("departurePoint", departurePoint);
					mav.addObject("arrivalPoint", arrivalPoint);
					mav.addObject("batchStatusIdList", batchStatusIdList);
					mav.addObject("receiptSkipYn", allocationMap.get("receipt_skip_yn").toString());
					map.put("allocationStatus", allocationStatus);
					List<Map<String, Object>> allocationFileList = allocationFileService.selectAllocationFileList(map);
					mav.addObject("allocationFileList", allocationFileList);	
					mav.addObject("driverId", userSessionMap.get("driver_id").toString());
					
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	@RequestMapping(value = "/allocation-detail-forGroup", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView allocationDetailForGroup(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				
				String allocationStatus = request.getParameter("allocationStatus");
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				Map<String, Object> map = new HashMap<String, Object>();
				List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
				List<Map<String, Object>> driverList = driverService.selectDriverList(map);
				List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
				List<Map<String, Object>> allocationDivisionList = allocationDivisionService.selectAllocationDivisionInfoList();
				List<Map<String, Object>> runDivisionList = runDivisionService.selectRunDivisionInfoList();
				List<Map<String, Object>> paymentDivisionList = paymentDivisionService.selectPaymentDivisionInfoList();
				List<Map<String, Object>> payDivisionList = payDivisionService.selectPayDivisionInfoList();
				
				map.put("allocationId", request.getParameter("allocationId").toString());
				map.put("driverId", userSessionMap.get("driver_id").toString());
				Map<String, Object> allocationMap = allocationService.selectAllocation(map);
				map.put("batchStatusId", allocationMap.get("batch_status_id"));
				List<Map<String, Object>> allocationList = allocationService.selectAllocationList(map);
				List<Map<String, Object>> batchStatusIdList = allocationService.selectGetBatchStausIdForPickUp(map);
				
				if(allocationStatus.equals("R")) {
					map.put("picturePointerR", allocationMap.get("picture_pointer_r").toString());	
				}else if(allocationStatus.equals("D")) {
					map.put("picturePointerD", allocationMap.get("picture_pointer_d").toString());
				}else if(allocationStatus.equals("I")) {
					map.put("picturePointerI", allocationMap.get("picture_pointer_i").toString());
				}
				
				String departurePoint = "";
				String arrivalPoint = "";
				
				for(int i = 0; i < allocationList.size(); i++) {
					Map<String, Object> allocation = allocationList.get(i); 
					map.put("allocationId", allocation.get("allocation_id").toString());
					Map<String, Object> carInfo = carInfoService.selectCarInfo(map);
					Map<String, Object> checkDetailInfo = checkDetailService.selectCheckDetail(map); 
					allocation.put("carInfo", carInfo);
					
					String departure = carInfo.get("departure_addr") != null && !carInfo.get("departure_addr").toString().equals("") ? carInfo.get("departure_addr").toString() : "";
					String arrival = carInfo.get("arrival_addr") != null && !carInfo.get("arrival_addr").toString().equals("") ? carInfo.get("arrival_addr").toString() : "";
					
					
					try {
						departure = WebUtils.getAddressInfo(departure);
						arrival = WebUtils.getAddressInfo(arrival);
						departurePoint = WebUtils.getAddressToLatLng(departure);
						arrivalPoint = WebUtils.getAddressToLatLng(arrival);
					}catch(Exception e) {
						departurePoint = "";
						arrivalPoint = "";
						//e.printStackTrace();
					}finally {
						
					}
					
					allocation.put("checkDetailInfo", checkDetailInfo);
					List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(map);
					allocation.put("paymentInfoList", paymentInfoList);
					
					if(allocation != null && allocation.get("charge_id") != null && !allocation.get("charge_id").toString().equals("")) {
						Map<String, Object> personInChargeMap = new HashMap<String, Object>();
						personInChargeMap.put("personInChargeId", allocation.get("charge_id").toString());
						Map<String, Object> personInCharge = personInChargeService.selectPersonInChargeByPersonInChargeId(personInChargeMap);
						if(personInCharge != null) {
							String department = personInCharge.get("department") != null && !personInCharge.get("department").toString().equals("") ? personInCharge.get("department").toString() : ""; 
							allocation.put("department", department);	
						}
					//	String department = personInChargeService.selectPersonInChargeByPersonInChargeId(personInChargeMap).get("department").toString();
					//	String department = personInChargeService.selectPersonInChargeByPersonInChargeId(personInChargeMap).get("department") != null && !personInChargeService.selectPersonInChargeByPersonInChargeId(personInChargeMap).get("department").toString().equals("") ? personInChargeService.selectPersonInChargeByPersonInChargeId(personInChargeMap).get("department").toString():"";
					//	allocation.put("department", department);
					}
					allocationList.set(i,allocation);
				}
					mav.addObject("allocationMap",  allocationMap);
					mav.addObject("allocationList",  allocationList);
					mav.addObject("billingDivisionList",  billingDivisionList);
					mav.addObject("allocationDivisionList",  allocationDivisionList);
					mav.addObject("runDivisionList",  runDivisionList);
					mav.addObject("paymentDivisionList",  paymentDivisionList);
					mav.addObject("payDivisionList",  payDivisionList);
					mav.addObject("driverList",  driverList);
					mav.addObject("companyList",  companyList);
					mav.addObject("paramMap", paramMap);
					mav.addObject("allocationStatus", allocationStatus);
					mav.addObject("departurePoint", departurePoint);
					mav.addObject("arrivalPoint", arrivalPoint);
					mav.addObject("batchStatusIdList", batchStatusIdList);
					mav.addObject("receiptSkipYn", allocationMap.get("receipt_skip_yn").toString());
					
					map.put("picturePointerR", allocationMap.get("picture_pointer_r").toString());
					map.put("picturePointerD", allocationMap.get("picture_pointer_d").toString());
					map.put("picturePointerI", allocationMap.get("picture_pointer_i").toString());
					map.put("allocationStatus", allocationStatus);
					List<Map<String, Object>> allocationFileList = allocationFileService.selectAllocationFileList(map);
					mav.addObject("allocationFileList", allocationFileList);	
					mav.addObject("driverId", userSessionMap.get("driver_id").toString());
					
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	@RequestMapping(value = "/upLiftCheck", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi customerExcelInsert(ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception{
		
		ResultApi resultApi = new ResultApi();
		
		try{
			
			String allocationIdArr = request.getParameter("allocationId").toString();
			String allocationId[] = allocationIdArr.split(",");
			String allocationStatus = request.getParameter("currentStatus").toString();
			String customerId = request.getParameter("customerId") != null && !request.getParameter("customerId").toString().equals("") ? request.getParameter("customerId").toString():"";
			
			subDir = "allocation";
			if(allocationStatus.equals("Y")){		//파일을 업 로드 하는 경우 진행 상태이다.
	    		allocationStatus = "R";
			 }else if(allocationStatus.equals("S")){
				 allocationStatus = "D";
			 }else if(allocationStatus.equals("P")){
				 allocationStatus = "D";
			 }
			
			if(allocationStatus.equals("I") && !customerId.equals("CUSa8b74209a826425698ef189f61f08ff6")){
				
				String fileName = "signature.png";
		    	String sign = StringUtils.split(request.getParameter("sign"), ",")[1];

		        String uploadDir = rootDir + "/" + subDir;
				String saveName = "/" + WebUtils.getNow("yyyy") + "/" + WebUtils.getNow("MM") + "/" + WebUtils.getNow("dd");
				String newFilename = UUID.randomUUID().toString()+".png";
							
				File saveDir = new File(uploadDir + saveName);
				
				if(saveDir.exists() == false){
					saveDir.mkdirs();
				}
				saveName = saveName + "/" + newFilename;
				FileUtils.writeByteArrayToFile(new File(uploadDir + saveName), Base64.decodeBase64(sign));
				
				for(int i = 0 ; i < allocationId.length; i++) {
			    	AllocationFileVO allocationFileVO = new AllocationFileVO();
					allocationFileVO.setAllocationId(allocationId[i]);
					allocationFileVO.setAllocationStatus(allocationStatus);
					allocationFileVO.setCategoryType(subDir);
					allocationFileVO.setAllocationFileId("FLE"+UUID.randomUUID().toString().replaceAll("-", ""));
					allocationFileVO.setAllocationFileNm(fileName);
					allocationFileVO.setAllocationFilePath(saveName);
					allocationFileService.insertAllocationFile(allocationFileVO);
				}
				
			}else {
				MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
				allocationFileService.insertAllocationFile(rootDir, subDir, allocationIdArr, allocationStatus, multipartRequest);
				
			}
		
			for(int i = 0 ; i < allocationId.length; i++) {
				Map<String, Object> updateMap = new HashMap<String, Object>();
				updateMap.put("allocationId", allocationId[i]);
				updateMap.put("picturePointer", allocationId[0]);
				if(allocationStatus.equals("R")) {
					allocationService.updateAllocationPicturePointerR(updateMap);	
				}else if(allocationStatus.equals("D")) {
					allocationService.updateAllocationPicturePointerD(updateMap);	
				}else if(allocationStatus.equals("I")) {
					allocationService.updateAllocationPicturePointerI(updateMap);	
				} 
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return resultApi;
	}
	
		
	@RequestMapping(value = "/lift-check", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView liftCheck(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				
				String allocationId = request.getParameter("allocationId").toString();
				String allocationStatus = request.getParameter("allocationStatus").toString();
				String save = request.getParameter("save") != null && !request.getParameter("save").toString().equals("") ? request.getParameter("save").toString():""; 
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				Map<String, Object> map = new HashMap<String, Object>();
				List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
				List<Map<String, Object>> driverList = driverService.selectDriverList(map);
				List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
				List<Map<String, Object>> allocationDivisionList = allocationDivisionService.selectAllocationDivisionInfoList();
				List<Map<String, Object>> runDivisionList = runDivisionService.selectRunDivisionInfoList();
				List<Map<String, Object>> paymentDivisionList = paymentDivisionService.selectPaymentDivisionInfoList();
				List<Map<String, Object>> payDivisionList = payDivisionService.selectPayDivisionInfoList();
			
				map.put("allocationId", allocationId);
				map.put("driverId", userSessionMap.get("driver_id").toString());
				map.put("allocationStatus", allocationStatus);
				Map<String, Object> allocationMap = allocationService.selectAllocation(map);
				map.put("batchStatusId", allocationMap.get("batch_status_id"));
				List<Map<String, Object>> allocationList = allocationService.selectAllocationList(map);
				
				if(allocationStatus.equals("R")) {
					map.put("picturePointerR", allocationMap.get("picture_pointer_r").toString());	
				}else if(allocationStatus.equals("D")) {
					map.put("picturePointerD", allocationMap.get("picture_pointer_d").toString());
				}else if(allocationStatus.equals("I")) {
					map.put("picturePointerI", allocationMap.get("picture_pointer_i").toString());
				}
				
				List<Map<String, Object>> allocationFileList = allocationFileService.selectAllocationFileList(map);
				
				for(int i = 0; i < allocationList.size(); i++) {
					Map<String, Object> allocation = allocationList.get(i); 
					map.put("allocationId", allocation.get("allocation_id").toString());
					Map<String, Object> carInfo = carInfoService.selectCarInfo(map);
					allocation.put("carInfo", carInfo);
					List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(map);
					allocation.put("paymentInfoList", paymentInfoList);
					allocationList.set(i,allocation);
				}
					mav.addObject("allocationMap",  allocationMap);
					mav.addObject("allocationList",  allocationList);
					mav.addObject("billingDivisionList",  billingDivisionList);
					mav.addObject("allocationDivisionList",  allocationDivisionList);
					mav.addObject("runDivisionList",  runDivisionList);
					mav.addObject("paymentDivisionList",  paymentDivisionList);
					mav.addObject("payDivisionList",  payDivisionList);
					mav.addObject("driverList",  driverList);
					mav.addObject("companyList",  companyList);
					mav.addObject("paramMap", paramMap);
					mav.addObject("allocationFileList", allocationFileList);
					mav.addObject("driverId", userSessionMap.get("driver_id").toString());
					mav.addObject("save", save);
					
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	@RequestMapping(value = "/lift-check-forGroup", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView liftCheckForGroup(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				
				String allocationIdArr = request.getParameter("allocationId").toString();
				String[] allocationId = allocationIdArr.split(",");
				String groupId = allocationId[0];
				String allocationStatus = request.getParameter("allocationStatus").toString();
				String save = request.getParameter("save") != null && !request.getParameter("save").toString().equals("") ? request.getParameter("save").toString():"";
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				Map<String, Object> map = new HashMap<String, Object>();
				List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
				List<Map<String, Object>> driverList = driverService.selectDriverList(map);
				List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
				List<Map<String, Object>> allocationDivisionList = allocationDivisionService.selectAllocationDivisionInfoList();
				List<Map<String, Object>> runDivisionList = runDivisionService.selectRunDivisionInfoList();
				List<Map<String, Object>> paymentDivisionList = paymentDivisionService.selectPaymentDivisionInfoList();
				List<Map<String, Object>> payDivisionList = payDivisionService.selectPayDivisionInfoList();
			
				map.put("allocationId", allocationId[0]);
				map.put("driverId", userSessionMap.get("driver_id").toString());
				map.put("allocationStatus", allocationStatus);
				Map<String, Object> allocationMap = allocationService.selectAllocation(map);
				List<Map<String, Object>> allocationList = allocationService.selectAllocationList(map);
				if(allocationStatus.equals("R")) {
					map.put("picturePointerR", allocationMap.get("picture_pointer_r").toString());	
				}else if(allocationStatus.equals("D")) {
					map.put("picturePointerD", allocationMap.get("picture_pointer_d").toString());
				}else if(allocationStatus.equals("I")) {
					map.put("picturePointerI", allocationMap.get("picture_pointer_i").toString());
				}
				List<Map<String, Object>> allocationFileList = allocationFileService.selectAllocationFileList(map);
				
				for(int i = 0; i < allocationList.size(); i++) {
					Map<String, Object> allocation = allocationList.get(i); 
					map.put("allocationId", allocation.get("allocation_id").toString());
					Map<String, Object> carInfo = carInfoService.selectCarInfo(map);
					allocation.put("carInfo", carInfo);
					List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(map);
					allocation.put("paymentInfoList", paymentInfoList);
					allocationList.set(i,allocation);
				}
					mav.addObject("allocationMap",  allocationMap);
					mav.addObject("allocationList",  allocationList);
					mav.addObject("billingDivisionList",  billingDivisionList);
					mav.addObject("allocationDivisionList",  allocationDivisionList);
					mav.addObject("runDivisionList",  runDivisionList);
					mav.addObject("paymentDivisionList",  paymentDivisionList);
					mav.addObject("payDivisionList",  payDivisionList);
					mav.addObject("driverList",  driverList);
					mav.addObject("companyList",  companyList);
					mav.addObject("paramMap", paramMap);
					mav.addObject("allocationFileList", allocationFileList);
					mav.addObject("driverId", userSessionMap.get("driver_id").toString());
					mav.addObject("allocationIdArr", allocationIdArr);
					mav.addObject("groupId",  groupId);
					mav.addObject("save", save);
					
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	@RequestMapping(value = "/lift-check-batch-forGroup", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView liftCheckBatchForGroup(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				
				String allocationIdArr = request.getParameter("allocationId").toString();
				String allocationId[] = allocationIdArr.split(",");
				String groupId = allocationId[0];
				String allocationStatus = request.getParameter("allocationStatus").toString();
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				Map<String, Object> map = new HashMap<String, Object>();
				List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
				List<Map<String, Object>> driverList = driverService.selectDriverList(map);
				List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
				List<Map<String, Object>> allocationDivisionList = allocationDivisionService.selectAllocationDivisionInfoList();
				List<Map<String, Object>> runDivisionList = runDivisionService.selectRunDivisionInfoList();
				List<Map<String, Object>> paymentDivisionList = paymentDivisionService.selectPaymentDivisionInfoList();
				List<Map<String, Object>> payDivisionList = payDivisionService.selectPayDivisionInfoList();
			
				map.put("allocationId", allocationId[0]);
				map.put("driverId", userSessionMap.get("driver_id").toString());
				map.put("allocationStatus", allocationStatus);
				Map<String, Object> allocationMap = allocationService.selectAllocation(map);
				List<Map<String, Object>> allocationList = allocationService.selectAllocationList(map);
				
				List<Map<String, Object>> groupList = new ArrayList<Map<String, Object>>();
				for(int i = 0; i < allocationId.length; i++) {
					Map<String, Object> selectMap = new HashMap<String, Object>();
					selectMap.put("allocationId", allocationId[i]);
					selectMap.put("driverId", userSessionMap.get("driver_id").toString());
					selectMap.put("allocationStatus", allocationStatus);
					groupList.add(allocationService.selectAllocation(selectMap));
				}
				
				if(allocationStatus.equals("R")) {
					map.put("picturePointerR", allocationMap.get("picture_pointer_r").toString());	
				}else if(allocationStatus.equals("D")) {
					map.put("picturePointerD", allocationMap.get("picture_pointer_d").toString());
				}else if(allocationStatus.equals("I")) {
					map.put("picturePointerI", allocationMap.get("picture_pointer_i").toString());
				}
				
				List<Map<String, Object>> allocationFileList = allocationFileService.selectAllocationFileList(map);
				
				for(int i = 0; i < allocationList.size(); i++) {
					Map<String, Object> allocation = allocationList.get(i); 
					map.put("allocationId", allocation.get("allocation_id").toString());
					Map<String, Object> carInfo = carInfoService.selectCarInfo(map);
					allocation.put("carInfo", carInfo);
					List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(map);
					allocation.put("paymentInfoList", paymentInfoList);
					allocationList.set(i,allocation);
				}
				
					mav.addObject("allocationIdArr",  allocationIdArr);
					mav.addObject("allocationMap",  allocationMap);
					mav.addObject("allocationList",  allocationList);
					mav.addObject("billingDivisionList",  billingDivisionList);
					mav.addObject("allocationDivisionList",  allocationDivisionList);
					mav.addObject("runDivisionList",  runDivisionList);
					mav.addObject("paymentDivisionList",  paymentDivisionList);
					mav.addObject("payDivisionList",  payDivisionList);
					mav.addObject("driverList",  driverList);
					mav.addObject("companyList",  companyList);
					mav.addObject("paramMap", paramMap);
					mav.addObject("allocationFileList", allocationFileList);
					mav.addObject("driverId", userSessionMap.get("driver_id").toString());
					mav.addObject("groupId", groupId);
					mav.addObject("groupList", groupList);
					
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	
	@RequestMapping(value = "/insertCheckDetail", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi insertCheckDetail(ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute CarCheckDetailVO carCheckDetailVO) throws Exception{
		
		ResultApi resultApi = new ResultApi();
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				String allocationId = request.getParameter("allocationId").toString();
				String[] allocationIdArr = allocationId.split(",");
				String groupId = allocationIdArr[0];
				
				String allocationStatus = request.getParameter("allocationStatus").toString();
				subDir = "allocation";
				if(allocationStatus.equals("Y")){		//파일을 업 로드 하는 경우 진행 상태이다.
		    		allocationStatus = "R";
				 }else if(allocationStatus.equals("S")){
					 allocationStatus = "I";
				 }else if(allocationStatus.equals("P")){
					 allocationStatus = "D";
				 }
				
				String fileName = "signature.png";
		    	String sign = StringUtils.split(request.getParameter("sign"), ",")[1];

		        String uploadDir = rootDir + "/" + subDir;
				String saveName = "/" + WebUtils.getNow("yyyy") + "/" + WebUtils.getNow("MM") + "/" + WebUtils.getNow("dd");
				String newFilename = UUID.randomUUID().toString()+".png";
							
				File saveDir = new File(uploadDir + saveName);
				
				if(saveDir.exists() == false){
					saveDir.mkdirs();
				}
				saveName = saveName + "/" + newFilename;
				FileUtils.writeByteArrayToFile(new File(uploadDir + saveName), Base64.decodeBase64(sign));
				
				for(int i = 0 ; i < allocationIdArr.length; i++) {
			    	AllocationFileVO allocationFileVO = new AllocationFileVO();
					allocationFileVO.setAllocationId(allocationIdArr[i]);
					allocationFileVO.setAllocationStatus(allocationStatus);
					allocationFileVO.setCategoryType(subDir);
					allocationFileVO.setAllocationFileId("FLE"+UUID.randomUUID().toString().replaceAll("-", ""));
					allocationFileVO.setAllocationFileNm(fileName);
					allocationFileVO.setAllocationFilePath(saveName);
					allocationFileService.insertAllocationFile(allocationFileVO);	
				}
				
				for(int i = 0 ; i < allocationIdArr.length; i++) {
					Map<String, Object> updateMap = new HashMap<String, Object>();
					updateMap.put("allocationId", allocationIdArr[i]);
					updateMap.put("picturePointer", allocationIdArr[0]);
					if(allocationStatus.equals("R")) {
						allocationService.updateAllocationPicturePointerR(updateMap);	
					}else if(allocationStatus.equals("D")) {
						allocationService.updateAllocationPicturePointerD(updateMap);	
					}else if(allocationStatus.equals("I")) {
						allocationService.updateAllocationPicturePointerI(updateMap);	
					} 
				}
				
				for(int i = 0 ; i < allocationIdArr.length; i++) {
					carCheckDetailVO.setAllocationId(allocationIdArr[i]);
					carCheckDetailVO.setCarCheckDetailId("CCD"+UUID.randomUUID().toString().replaceAll("-", ""));
					checkDetailService.insertCheckDetail(carCheckDetailVO);
				}
				
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("allocationId", allocationIdArr[0]);
				map.put("driverId", userSessionMap.get("driver_id").toString());
				map.put("allocationStatus", allocationStatus);
				Map<String, Object> allocationMap = allocationService.selectAllocation(map);
				if(allocationStatus.equals("R")) {
					map.put("picturePointerR", allocationMap.get("picture_pointer_r").toString());	
				}else if(allocationStatus.equals("D")) {
					map.put("picturePointerD", allocationMap.get("picture_pointer_d").toString());
				}else if(allocationStatus.equals("I")) {
					map.put("picturePointerI", allocationMap.get("picture_pointer_i").toString());
				}
				
				List<Map<String, Object>> allocationFileList = allocationFileService.selectAllocationFileList(map);
				mav.addObject("allocationFileList", allocationFileList);
				resultApi.setResultData(allocationFileList);
			}
			
			
			
			
			
			
		
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return resultApi;
	}
	
	
	
	@RequestMapping(value = "/insertReceipt", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi insertReceipt(ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute CarCheckDetailVO carCheckDetailVO) throws Exception{
		
		ResultApi resultApi = new ResultApi();
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			String allocationId = request.getParameter("allocationId").toString();
			String[] allocationIdArr = allocationId.split(",");
			String groupId = allocationIdArr[0];
			
			String allocationStatus = request.getParameter("allocationStatus").toString();
			/*String phoneNumber = request.getParameter("phoneNumber") != null && !request.getParameter("phoneNumber").toString().equals("")  ? request.getParameter("phoneNumber").toString() : "";
			String mailAddress = request.getParameter("mailAddress") != null && !request.getParameter("mailAddress").toString().equals("")  ? request.getParameter("mailAddress").toString() : "";*/
			
			//기존에 메일주소와 전화번호를 기사님이 현장에서 받았으나 배차 입력에 작성된 연락처와 메일 주소로 인수증을 전송 하도록(인수증을 전송 해야 하는건만) 변경..
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("allocationId", allocationIdArr[0]);
			map.put("driverId", userSessionMap.get("driver_id").toString());
			
			Map<String, Object> allocation = allocationService.selectAllocation(map);
			
			String phoneNumber = allocation.get("receipt_phone") != null && !allocation.get("receipt_phone").toString().equals("") ? allocation.get("receipt_phone").toString():"";
			String mailAddress = allocation.get("receipt_email") != null && !allocation.get("receipt_email").toString().equals("") ? allocation.get("receipt_email").toString():"";
			
			subDir = "allocation";
			allocationStatus = "I";
			String fileName = "receipt.png";
	    	String receipt = StringUtils.split(request.getParameter("receipt"), ",")[1];
	        String uploadDir = rootDir + "/" + subDir;
			String saveName = "/" + WebUtils.getNow("yyyy") + "/" + WebUtils.getNow("MM") + "/" + WebUtils.getNow("dd");
			String newFilename = UUID.randomUUID().toString()+".png";
			File saveDir = new File(uploadDir + saveName);
			
			if(saveDir.exists() == false){
				saveDir.mkdirs();
			}
			saveName = saveName + "/" + newFilename;
			FileUtils.writeByteArrayToFile(new File(uploadDir + saveName), Base64.decodeBase64(receipt));
			//File file = new File(uploadDir + saveName);
			resultApi.setResultData(saveName);
			resultApi.setResultDataSub(phoneNumber);
			
			for(int i = 0 ; i < allocationIdArr.length; i++) {
		    	AllocationFileVO allocationFileVO = new AllocationFileVO();
				allocationFileVO.setAllocationId(allocationIdArr[i]);
				allocationFileVO.setAllocationStatus(allocationStatus);
				allocationFileVO.setCategoryType(subDir);
				allocationFileVO.setAllocationFileId("FLE"+UUID.randomUUID().toString().replaceAll("-", ""));
				allocationFileVO.setAllocationFileNm(fileName);
				allocationFileVO.setAllocationFilePath(saveName);
				allocationFileService.insertAllocationFile(allocationFileVO);	
			}
			
			for(int i = 0 ; i < allocationIdArr.length; i++) {
				Map<String, Object> updateMap = new HashMap<String, Object>();
				updateMap.put("allocationId", allocationIdArr[i]);
				updateMap.put("picturePointer", allocationIdArr[0]);
				if(allocationStatus.equals("R")) {
					allocationService.updateAllocationPicturePointerR(updateMap);	
				}else if(allocationStatus.equals("D")) {
					allocationService.updateAllocationPicturePointerD(updateMap);	
				}else if(allocationStatus.equals("I")) {
					allocationService.updateAllocationPicturePointerI(updateMap);	
				} 
			}
			
			if(!mailAddress.equals("")) {
				MailUtils.mailSend(mailAddress, "test", "test",saveName,fileName);
				System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"+mailAddress+"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");	
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return resultApi;
	}
	
	
	
	
	@RequestMapping(value = "/getDistance", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi getDistance(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			String departure =  WebUtils.getAddressInfo(request.getParameter("departureAddr").toString()); 
			String arrival = WebUtils.getAddressInfo(request.getParameter("arrivalAddr").toString());
			
			//상차지 또는 하차지의 주소로 주소 정보를 가져 올 수 없는 경우 경로보기를 진행 할 수 없다.
			if(WebUtils.getAddressResult(departure) == 0) {
				result.setResultCode("0002");
				return result;
			}
			
			if(WebUtils.getAddressResult(arrival) == 0) {
				result.setResultCode("0003");
				return result;
			}
			
			String resultStr = "&start="+WebUtils.getAddressToLatLng(departure)+"&destination="+WebUtils.getAddressToLatLng(arrival);
	        
			String[] departurePointArr = WebUtils.getAddressToLatLng(departure).split(",");
			String[] arrivalPointArr = WebUtils.getAddressToLatLng(arrival).split(",");
			
			
			BufferedReader    oBufReader = null;
	        HttpURLConnection httpConn   = null;
	        
	        //search : 0:최단,2: 추천,3:무료,4:자동차 전용 제외
	        //String strEncodeUrl = "http://map.naver.com/spirra/findCarRoute.nhn?route=route3&output=json&result=web3&coord_type=naver&search=0&car=0&mileage=12.4&start="+departure+","+departureAdd+"&destination="+destination+","+destinationAdd;
	        
	        //String strEncodeUrl = "https://map.naver.com/spirra/findCarRoute.nhn?route=route3&output=json&result=web3&coord_type=naver&search=2&car=0&mileage=12.4"+resultStr+"&via=";
	        
	        
	        String strEncodeUrl = "https://apis.openapi.sk.com/tmap/routes?&appKey=l7xx6b701ff5595c4e3b859e42f57de0c975&"
	        		+ "&version=1&endX="+arrivalPointArr[0]+"&endY="+arrivalPointArr[1]
	        		+ "&startX="+departurePointArr[0]+"&startY="+departurePointArr[1]+"&carType=5";
	        
	        
	        URL oOpenURL = new URL(strEncodeUrl);
	      
	        httpConn =  (HttpURLConnection) oOpenURL.openConnection();          
	        httpConn.setRequestMethod("GET");          
	        httpConn.connect(); 
	        Thread.sleep(100);
	        oBufReader = new BufferedReader(new InputStreamReader(oOpenURL.openStream()));
	        
	        JSONParser jsonParser = new JSONParser();
	        JSONObject jsonObject = (JSONObject)jsonParser.parse(oBufReader);
	        
	        String jsonString = jsonObject.toJSONString();
	        
	        Object obj = JSONValue.parse(jsonString);
	        JSONObject jsonObj = (JSONObject)obj;
	        
	        //JSONArray list = (JSONArray)jsonObject.get("routes");
	        //JSONObject jsonObj2 = (JSONObject)list.get(0); 
	        //JSONObject summary = (JSONObject)jsonObj2.get("summary");
	        
	        
	        JSONArray list = (JSONArray)jsonObject.get("features");
	        JSONObject jsonObj2 = (JSONObject)list.get(0); 
	        JSONObject summary = (JSONObject)jsonObj2.get("geometry"); 
	        JSONObject properties = (JSONObject)jsonObj2.get("properties");
	        String pointType = properties.get("pointType").toString();
	        String  totalDistance = properties.get("totalDistance").toString();
	        
	        DecimalFormat form = new DecimalFormat("#.#");
	        float distanceF = Float.valueOf(totalDistance)/1000;
	        String distance =String.valueOf(form.format(distanceF));

	        Map<String, Object> map = new HashMap<String, Object>();
	        map.put("distance", distance);
	        map.put("departure", WebUtils.getAddressToLngLat(departure));
	        map.put("arrival", WebUtils.getAddressToLngLat(arrival));
	        
			result.setResultData(map);
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	@RequestMapping(value = "/routeView", method = RequestMethod.GET)
	public ModelAndView routeView(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute AllocationVO allocationVO) throws Exception {
		
		try{
			
			String departure = request.getParameter("departure"); 
			String arrival = request.getParameter("arrival");
			
			String departureArr[] = departure.split(",");
			String arrivalArr[] = arrival.split(",");
			
			Map<String,Double> map = new HashMap<String,Double>();
			
			map.put("departure_x", Double.parseDouble(departureArr[0]));
			map.put("departure_y", Double.parseDouble(departureArr[1]));
			map.put("arrival_x", Double.parseDouble(arrivalArr[0]));
			map.put("arrival_y", Double.parseDouble(arrivalArr[1]));
			
			if(map.get("departure_x")  > map.get("arrival_x")){
    			map.put("center_x",map.get("departure_x")-((map.get("departure_x")-map.get("arrival_x"))/2));
    		}else{
    			map.put("center_x",map.get("arrival_x")-((map.get("arrival_x")-map.get("departure_x"))/2));
    		}
    		if(map.get("departure_y")  > map.get("arrival_x")){
    			map.put("center_y",map.get("departure_y")-((map.get("departure_y")-map.get("arrival_y"))/2));
    		}else{
    			map.put("center_y",map.get("arrival_y")-((map.get("arrival_y")-map.get("departure_y"))/2));
    		}
			
    		mav.addObject("map", map);
    		mav.addObject("departure", departure);
    		mav.addObject("arrival", arrival);
    		
    		
    		String jsonStrings = new String();
    		try {
    			String buf;
    	        URL url = new URL("https://naveropenapi.apigw.ntruss.com/map-direction/v1/driving?start="+departureArr[1]+","+departureArr[0]+"&goal="+arrivalArr[1]+","+arrivalArr[0]+"&option=");
    	        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
    	        conn.setRequestMethod("GET");
    	        conn.setRequestProperty("X-Requested-With", "curl");
    	        conn.setRequestProperty("X-NCP-APIGW-API-KEY-ID", "t1kzgrc3sv");
    	        conn.setRequestProperty("X-NCP-APIGW-API-KEY", "CXGKmrSguaEOzEBc1bQy7Egqiq4gsdoat4vLeo2z");
    	        
    	        BufferedReader br = new BufferedReader(new InputStreamReader(
    	                conn.getInputStream(), "UTF-8"));
    	        while ((buf = br.readLine()) != null) {
    	            jsonStrings += buf;
    	        }	
    	        
    	        JSONParser jsonParser = new JSONParser();
    	        JSONObject jsonObject = (JSONObject)jsonParser.parse(jsonStrings);
    	        String jsonString = jsonObject.toJSONString();
    	        Object obj = JSONValue.parse(jsonString);
    	        //JSONObject jsonObj = (JSONObject)obj;
    	        
    	        	JSONObject route = (JSONObject)jsonObject.get("route");
        	        JSONArray traoptimal = (JSONArray)route.get("traoptimal");
        	        JSONObject jsonObj2 = (JSONObject)traoptimal.get(0);
        	        //JSONObject summary = (JSONObject)jsonObj2.get("summary");
        	        JSONArray pathList = (JSONArray)jsonObj2.get("path");
        	        
        	        List<Map<String, Double>> posList = new ArrayList<Map<String, Double>>();
        	        for(int i = 0; i < pathList.size(); i++) {
        	        	Map<String, Double> pos = new HashMap<String, Double>();
        	        	JSONArray path = (JSONArray)pathList.get(i);
        	        	pos.put("x",Double.parseDouble(path.get(1).toString()));
        	        	pos.put("y",Double.parseDouble(path.get(0).toString()));
        	        	posList.add(pos);
        	        //	System.out.println(pos);
        	        }
        	        mav.addObject("posList", posList);
    	        
    		}catch(Exception e) {
    			e.printStackTrace();
    		}
    		
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	@RequestMapping(value = "/deleteAllocationFile", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi deleteAllocationFile(ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception{
		
		ResultApi resultApi = new ResultApi();
		
		try{
			
			String allocationId = request.getParameter("allocationId").toString();
			String[] allocationIdArr = allocationId.split(",");
			String groupId = allocationIdArr[0];
			String[] deleteFileIdArr = request.getParameterValues("deleteFileIdArr");
			
			String customerId = request.getParameter("customerId") != null && !request.getParameter("customerId").toString().equals("") ? request.getParameter("customerId").toString():"";
			
			int deletedFileCnt = 0;
			int notDeletedFileCnt = 0;
			Map<String, Object> returnMap = new HashMap<String, Object>();
			
			for(int j = 0; j < allocationIdArr.length; j++) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("allocationId", allocationIdArr[j]);
				
				Map<String, Object> allocation = allocationService.selectAllocation(map);
				
				//int allocationCount = allocationService.selectAllocationCountByPicturePointer(allocation);
				
				for(int i = 0; i < deleteFileIdArr.length; i++) {
					map.put("allocationFileId", deleteFileIdArr[i]);
					Map<String, Object> allocationFile = allocationFileService.selectAllocationFile(map);
					if(allocationFile != null) {		//해당파일이 존재 하는지 확인 하고 파일이 존재 하면 파일을 삭제 한다.
						
						if(!allocationFile.get("allocation_status").toString().equals("I")) {		//인수단계의 파일(서명파일,인수증)은 삭제 할 수 없다.
							subDir = "allocation";
							String uploadDir = rootDir + "/" + subDir;
							File file = new File(uploadDir+allocationFile.get("allocation_file_path").toString());
							if( file.exists() ){
					            if(file.delete()){
					                System.out.println("파일삭제 성공");
					            }else{
					                System.out.println("파일삭제 실패");
					            }
					        }else{
					            System.out.println("파일이 존재하지 않습니다.");
					        }
							deletedFileCnt++;
							allocationFileService.deleteAllocationFile(map);	
						}else if(allocationFile.get("allocation_status").toString().equals("I") && customerId.equals("CUSa8b74209a826425698ef189f61f08ff6")) {	
							subDir = "allocation";
							String uploadDir = rootDir + "/" + subDir;
							File file = new File(uploadDir+allocationFile.get("allocation_file_path").toString());
							if( file.exists() ){
					            if(file.delete()){
					                System.out.println("파일삭제 성공");
					            }else{
					                System.out.println("파일삭제 실패");
					            }
					        }else{
					            System.out.println("파일이 존재하지 않습니다.");
					        }
							deletedFileCnt++;
							allocationFileService.deleteAllocationFile(map);	
						}else {			//인수단계의 파일은 삭제 할 수 없다.
							notDeletedFileCnt++;
						}
						
					}

				}
				
				
			}
			
			
			
			
			returnMap.put("deletedFileCnt", deletedFileCnt);
			returnMap.put("notDeletedFileCnt", notDeletedFileCnt);
			
			resultApi.setResultData(returnMap);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return resultApi;
	}
	
	
	
	
	
	@RequestMapping(value = "/cal-list", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView calList(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null && !userSessionMap.get("driver_kind").toString().equals("00")){
			
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				mav.addObject("paramMap", paramMap);
				
				paramMap.put("driverId", userSessionMap.get("driver_id").toString());
				paramMap.put("driverKind", userSessionMap.get("driver_kind").toString());
				
				//paramMap.put("driverId", "rudwhdl");
				paramMap.put("forUpdatePassWord", "");
				Map<String, Object> driverMap = driverService.selectDriver(paramMap);
				int deductionRate = Integer.parseInt(driverMap.get("deduction_rate").toString());
				List<Map<String, Object>> anotherCalInfoList = paymentInfoService.selectAnotherCalInfo(paramMap);
				
				/*for(int i = 0; i < anotherCalInfoList.size(); i++) {
					Map<String, Object> map = anotherCalInfoList.get(i);
					int total = Integer.parseInt(map.get("amount").toString().replaceAll(",", ""));
					map.put("total", total);
					//공제율 적용해서 계산
					//int deduction = total-(deductionRate*total/100);
					int deduction = deductionRate*total/100;
					map.put("deduction", deduction);
					String decideMonth = map.get("decide_month").toString();
					map.put("item", "1");
					//map.put("driverId", "mincj10");
					map.put("driverId", userSessionMap.get("driver_id").toString());
					map.put("decideMonth", decideMonth);
					String item1 = driverDeductInfoService.selectDriverDeductInfoByItem(map).get("amount").toString();
					map.put("item1", item1);
					Map<String, Object> ddAmount = paymentInfoService.selectAnotherCalDDInfo(map);
					map.put("ddAmount", ddAmount);
					//청구내역
					int result = deduction+Integer.parseInt(item1.replaceAll(",", ""))+Integer.parseInt(ddAmount.get("ddAmount").toString().replaceAll(",", ""));
					
					map.put("result", String.format("%,d", result));
					//공급가액
					int sum = total-result;
					map.put("sum", String.format("%,d", sum));
					int vat = sum/10;
					map.put("vat", String.format("%,d", vat));
					int subTotal = sum+vat;
					map.put("subTotal", String.format("%,d", subTotal));
					map.put("item", "2");
					String item2 = driverDeductInfoService.selectDriverDeductInfoByItem(map).get("amount").toString();
					map.put("item2", String.format("%,d", item2));
					map.put("item2",item2);
					int finalAmount = subTotal-Integer.parseInt(item2.replaceAll(",", ""));
					map.put("finalAmount", String.format("%,d", finalAmount));
					anotherCalInfoList.set(i,map);
				}*/
				
				mav.addObject("anotherCalInfoList",anotherCalInfoList);
				mav.addObject("driverId", userSessionMap.get("driver_id").toString());
				mav.addObject("paramMap",paramMap);
				
				
			}else if(userSessionMap.get("driver_kind").toString().equals("00")){
				
				WebUtils.messageAndRedirectUrl(mav, "해당 메뉴를 사용 할 수 없습니다.", "/carrier/main.do");
				
			}else {
				
				
				
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	@RequestMapping(value = "/cal-item", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView calItem(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
			
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				mav.addObject("paramMap", paramMap);
				
				paramMap.put("driverId", userSessionMap.get("driver_id").toString());
				//paramMap.put("driverId", "mincj10");
				paramMap.put("forUpdatePassWord", "");
				Map<String, Object> driverMap = driverService.selectDriver(paramMap);
				int deductionRate = Integer.parseInt(driverMap.get("deduction_rate").toString());
				
				List<Map<String, Object>> driverDeductInfoList = driverDeductInfoService.selectDriverDeductInfoListByItem(paramMap);

				
				List<Map<String, Object>> anotherCalInfoList = paymentInfoService.selectAnotherCalInfo(paramMap);				
				for(int i = 0; i < anotherCalInfoList.size(); i++) {
					Map<String, Object> map = anotherCalInfoList.get(i);
					//선택한 월의 정보만...
					if(map.get("decide_month").toString().equals(paramMap.get("decideMonth").toString())) {
						int total = Integer.parseInt(map.get("amount").toString().replaceAll(",", ""));
						map.put("total", total);
						//공제율 적용해서 계산
						//int deduction = total-(deductionRate*total/100);
						int deduction = deductionRate*total/100;
						map.put("deduction", String.format("%,d", deduction));
						map.put("deductionRate", deductionRate);
						String decideMonth = map.get("decide_month").toString();
						map.put("item", paramMap.get("item").toString());
						map.put("driverId", userSessionMap.get("driver_id").toString());
						//map.put("driverId", "mincj10");
						map.put("decideMonth", decideMonth);
						//int item1 = driverDeductInfoService.selectDriverDeductInfoByItem(map);
						Map<String, Object> ddAmount = paymentInfoService.selectAnotherCalDDInfo(map);
						map.put("ddAmount", ddAmount);
						mav.addObject("map",  map);
					}
					
					
					
					
				}
				
				
				//decideMonth="+decideMonth+"&item="+item
				
				
				mav.addObject("listData",  driverDeductInfoList);
				mav.addObject("driverId", userSessionMap.get("driver_id").toString());
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	@RequestMapping(value = "/cal-list-detail", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView calListDetail(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null && !userSessionMap.get("driver_kind").toString().equals("00")){
			
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				mav.addObject("paramMap", paramMap);
				
				String decideMonth = request.getParameter("decideMonth");
				
				paramMap.put("driverId", userSessionMap.get("driver_id").toString());
				paramMap.put("driverKind", userSessionMap.get("driver_kind").toString());
				
				//paramMap.put("driverId", "rudwhdl");
				paramMap.put("decideMonth", decideMonth);
				
				List<Map<String, Object>> anotherCalDetail= allocationService.selectAllocationListForCal(paramMap);
				Map<String, Object> decideFinalMap = decideFinalService.selectDecideFinalByDecideMonth(paramMap);
				Map<String, Object> driverDecideStatus = driverDecideStatusService.selectDriverDecideStatus(paramMap);
				
				List<Map<String, Object>> driverDeductFileList = driverDeductFileService.selectDriverDeductFileList(paramMap);
				Map<String, Object> driverDeductFile = new HashMap<String, Object>();
				
					if(driverDeductFileList != null && driverDeductFileList.size() > 0) {
						driverDeductFile = driverDeductFileList.get(0);
					}
				
				mav.addObject("listData", anotherCalDetail);
				mav.addObject("driverDecideStatus", driverDecideStatus);
				mav.addObject("decideFinalMap", decideFinalMap);
				mav.addObject("driverDeductFile", driverDeductFile);
				
				mav.addObject("driverId", userSessionMap.get("driver_id").toString());
				
				
			}else if(userSessionMap.get("driver_kind").toString().equals("00")){
				
				WebUtils.messageAndRedirectUrl(mav, "해당 메뉴를 사용 할 수 없습니다.", "/carrier/main.do");
				
			}else {
				
				
				
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	@RequestMapping(value = "/insertDriverDecideStatus", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi insertDriverDecideStatus(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			
			if(userSessionMap != null){
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				mav.addObject("paramMap", paramMap);
				
				String decideMonth = request.getParameter("decideMonth");
				DriverDecideStatusVO statusVO = new DriverDecideStatusVO();
				
				statusVO.setDecideMonth(decideMonth);
				statusVO.setDriverDecideStatusId("DDI"+UUID.randomUUID().toString().replaceAll("-", ""));
				statusVO.setDriverId(userSessionMap.get("driver_id").toString());
				statusVO.setListStatus("Y");
			
				driverDecideStatusService.insertDriverDecideStatus(statusVO);
				
			}else {
				
				result.setResultCode("E000");
			}
			
				
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	
	
	
	
	
	@RequestMapping(value = "/image-view", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView imageView(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
			
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				mav.addObject("paramMap", paramMap);
				mav.addObject("driverId", userSessionMap.get("driver_id").toString());
			}else {
   				System.out.println("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n userSessionMap is null\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
   			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	

	@RequestMapping(value = "/downloadDriverDeductInfo", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView downloadDriverDeductInfo(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		Map<String, Object> fileMap = new HashMap<String, Object>();
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
			
				String selectMonth = request.getParameter("selectMonth") == null ? "2019-05" : request.getParameter("selectMonth").toString();
   	   			String driverId = userSessionMap.get("driver_id").toString();
   	   		
   	   			Map<String, Object> map = new HashMap<String, Object>();
   	   			map.put("decideMonth", selectMonth);
   	   			map.put("driverId", driverId);
   	   			/*map.put("driverId", "rudwhdl");*/
   	   			
   	   			
   	   			List<Map<String, Object>> driverDeductFile = driverDeductFileService.selectDriverDeductFileList(map);
   	   			
   	   			if(driverDeductFile != null && driverDeductFile.size() >= 1) {
   	   				mav.addObject("driverDeductFile", driverDeductFile.get(0));
   	   			}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		//return new ModelAndView("downloadView", "downloadFile", fileMap);
		return mav;
	}
	
	
	
	
	@RequestMapping(value = "/viewBillInfo", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView viewBillInfo(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		//Map<String, Object> fileMap = new HashMap<String, Object>();
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
			
				String selectMonth = request.getParameter("selectMonth").toString();
   	   			String driverId = userSessionMap.get("driver_id").toString();
   	   			//driverId = "caosys";
   	   			
   	   			Map<String, Object> map = new HashMap<String, Object>();
   	   			map.put("decideMonth", selectMonth);
   	   			map.put("driverId", driverId);
   	   			
   	   			Map<String, Object> driverDeductFile = driverDeductFileService.selectDriverDeductFile(map);
   	   			
   	   			
   	   			//공제 내역서 생성이 되었다면 공급가액,부가세,총액의 정보가 있겠다..
   	   			if(driverDeductFile != null && driverDeductFile.get("driver_id") != null) {
   	   				//공급가액
   	   				String supplyVal = driverDeductFile.get("supply_val").toString();
   	   				//부가세액
   	   				String vatVal = driverDeductFile.get("vat_val").toString();
   	   				//총액
   	   				String totalVal = driverDeductFile.get("total_val").toString();
   	   				String supplyValCom = driverDeductFile.get("supply_val_com").toString();
   	   				String vatValCom = driverDeductFile.get("vat_val_com").toString();
   	   				String totalValCom = driverDeductFile.get("total_val_com").toString();
	   	   			map.put("supplyVal", supplyVal);
	   	   			map.put("vatVal", vatVal);
				   	map.put("totalVal", totalVal);
				   	map.put("supplyValCom", supplyValCom);
				   	map.put("vatValCom", vatValCom);
				   	map.put("totalValCom", totalValCom);
				   	map.put("billingDivision", "P");
				   	//파일을 생성 하고 생성된 파일을 서버에 저장.....	
				   	
				   	
				   	Map<String, Object> driverBillingFile = driverBillingFileService.selectDriverBillingFile(map);
				   	Map<String, Object> driverBillingStatus = driverBillingStatusService.selectDriverBillingStatus(map);
				   	
				   	Map<String, Object> fileMap = new HashMap<String, Object>();
				   	
				   	//----------------------------------------------------------------------------------
				   	//기존에 생성된 파일이 있으면 삭제 하고 다시 생성 하되 계산서를 전송 한 경우에는 새로 생성 하지 않도록 수정..
				   	//----------------------------------------------------------------------------------
				   	
				   	if(driverBillingStatus != null && driverBillingStatus.get("billing_status").equals("Y")) {
				   		fileMap = driverBillingFile;
				   	}else {
				   		if(driverBillingFile != null && driverBillingFile.get("driver_id") != null) {
					   		
					   		File removeFile = new File(rootDir+"/billing_list"+driverBillingFile.get("driver_billing_file_path").toString());
							if(removeFile.exists()){
								removeFile.delete();
							}
							driverBillingFileService.deleteDriverBillingFile(driverBillingFile);
							fileMap = driverBillingFileService.makeDriverBillingFile(map);	
					   	}else {
					   		fileMap = driverBillingFileService.makeDriverBillingFile(map);
					   	}
				   		
				   	}
				   	
				   	mav.addObject("fileMap", fileMap);
				   	mav.addObject("driverBillingStatus", driverBillingStatus);
				   	mav.addObject("map", map);
   	   			}
   	   			
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		//return new ModelAndView("downloadView", "downloadFile", fileMap);
		return mav;
	}
	
	
	@RequestMapping(value = "/insertDriverBillingStatus", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi insertDriverBillingStatus(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			
			if(userSessionMap != null){
				
				String driverId = userSessionMap.get("driver_id").toString();
				//driverId = "caosys";
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				String decideMonth = paramMap.get("decideMonth").toString();
				paramMap.put("driverId", driverId);
				mav.addObject("paramMap", paramMap);
				
				Map<String, Object> driverDeductFile = driverDeductFileService.selectDriverDeductFile(paramMap);
				
				if(driverDeductFile != null && driverDeductFile.get("driver_id") != null) {
					DriverBillingStatusVO billingVO = new DriverBillingStatusVO();
					billingVO.setSupplyVal(driverDeductFile.get("supply_val").toString());
					billingVO.setTotalVal(driverDeductFile.get("total_val").toString());
					billingVO.setVatVal(driverDeductFile.get("vat_val").toString());
					billingVO.setDecideMonth(decideMonth);
					billingVO.setDriverBillingStatusId("DBS"+UUID.randomUUID().toString().replaceAll("-", ""));
					billingVO.setDriverId(driverId);
					billingVO.setBillingStatus("Y");
					driverBillingStatusService.insertDriverBillingStatus(billingVO);	
					
					//공급받는자 보관용 파일을 생성 한다.
	   	   			
	   	   			Map<String, Object> map = new HashMap<String, Object>();
	   	   			map.put("decideMonth", decideMonth);
	   	   			map.put("driverId", driverId);
					map.put("supplyVal", driverDeductFile.get("supply_val").toString());
	   	   			map.put("vatVal", driverDeductFile.get("vat_val").toString());
				   	map.put("totalVal", driverDeductFile.get("total_val").toString());
				   	map.put("billingDivision", "O");
					driverBillingFileService.makeDriverBillingFileOtherSide(map);
					
				}else {
					result.setResultCode("E000");
				}
				
			}else {
				
				result.setResultCode("E000");
			}
			
				
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	
	
	
	
	
	@RequestMapping( value="/excel_download", method = RequestMethod.GET )
	public ModelAndView excel_download(ModelAndView mav,
			HttpServletRequest request ,HttpServletResponse response) throws Exception {
		
		ModelAndView view = new ModelAndView();
		
	try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				System.out.println("session is not null");
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				mav.addObject("paramMap", paramMap);
				
				List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

				String varNameList[] = null;
				String titleNameList[] = null;

				
				String selectMonth = request.getParameter("selectMonth");
				
				paramMap.put("driverId", userSessionMap.get("driver_id").toString());
				paramMap.put("decideMonth", selectMonth);
				
				list = allocationService.selectAllocationListForCal(paramMap);
				
				Map<String, Object> decideFinalMap = decideFinalService.selectDecideFinalByDecideMonth(paramMap);
				
				//최종확정이 된 경우 엑셀 다운로드시 금액을 표시 한다. 2019-04월의 경우 최종확정기능이 없었고 이미 처리 된 이후 이므로 금액까지 표시 해 준다.
				if(selectMonth.equals("2019-04") || decideFinalMap != null && decideFinalMap.get("decide_final_id") != null) {
					titleNameList = new String[]{"출발일","상차지","하차지","차종","차대번호","차량번호","계약번호","금액"};
					varNameList = new String[]{"departure_dt","departure","arrival","car_kind","car_id_num","car_num","contract_num","allocation_amount"};					
				}else {
					titleNameList = new String[]{"출발일","상차지","하차지","차종","차대번호","차량번호","계약번호"};
					varNameList = new String[]{"departure_dt","departure","arrival","car_kind","car_id_num","car_num","contract_num"};	
				}
				view.setViewName("excelDownloadView");
				view.addObject("list", list);
				view.addObject("varNameList", varNameList);
				view.addObject("titleNameList", titleNameList);
				view.addObject("excelName", "탁송내역("+selectMonth+").xlsx");
				
			}else {
				System.out.println("session is null");
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				mav.addObject("paramMap", paramMap);
				
				List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

				String varNameList[] = null;
				String titleNameList[] = null;

				String selectMonth = request.getParameter("selectMonth");
				paramMap.put("decideMonth", selectMonth);
				
				list = allocationService.selectAllocationListForCal(paramMap);
				
				Map<String, Object> decideFinalMap = decideFinalService.selectDecideFinalByDecideMonth(paramMap);
				
				//최종확정이 된 경우 엑셀 다운로드시 금액을 표시 한다. 2019-04월의 경우 최종확정기능이 없었고 이미 처리 된 이후 이므로 금액까지 표시 해 준다.
				if(selectMonth.equals("2019-04") || decideFinalMap != null && decideFinalMap.get("decide_final_id") != null) {
					titleNameList = new String[]{"출발일","상차지","하차지","차종","차대번호","차량번호","계약번호","금액"};
					varNameList = new String[]{"departure_dt","departure","arrival","car_kind","car_id_num","car_num","contract_num","allocation_amount"};					
				}else {
					titleNameList = new String[]{"출발일","상차지","하차지","차종","차대번호","차량번호","계약번호"};
					varNameList = new String[]{"departure_dt","departure","arrival","car_kind","car_id_num","car_num","contract_num"};	
				}
				
				view.setViewName("excelDownloadView");
				view.addObject("list", list);
				view.addObject("varNameList", varNameList);
				view.addObject("titleNameList", titleNameList);
				view.addObject("excelName", "탁송내역("+selectMonth+").xlsx");
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		
		
		
		return view;
		
	}
	
	
	@RequestMapping(value = "/bill_download", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView billDownload(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		Map<String, Object> fileMap = new HashMap<String, Object>();
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			String selectMonth = request.getParameter("selectMonth") == null ? "2019-07" : request.getParameter("selectMonth").toString();
   			String driverId = request.getParameter("driverId").toString();
   		
   			Map<String, Object> map = new HashMap<String, Object>();
   			map.put("decideMonth", selectMonth);
   			map.put("driverId", driverId);
   			//P : 공급자 보관용, O : 공급받는자 보관용 
   			map.put("billingDivision", "P");
   			
   			List<Map<String, Object>> driverBillingFile = driverBillingFileService.selectDriverBillingFileList(map);
   			
   			if(driverBillingFile != null && driverBillingFile.size() >= 1) {
   				mav.addObject("driverBillingFile", driverBillingFile.get(0));
   			}
			
   			fileMap.put("fileUploadPath", rootDir+"/billing_list");
			fileMap.put("fileLogicName", driverBillingFile.get(0).get("driver_billing_file_nm").toString());
			fileMap.put("filePhysicName", driverBillingFile.get(0).get("SAVE_NAME").toString());
			fileMap.put("driverId", driverId);
			fileMap.put("selectMonth", selectMonth);
   			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return new ModelAndView("downloadView", "downloadFile", fileMap);
		//return mav;
	}
	
	
	
	@RequestMapping(value = "/lowViolationList", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView lowViolationList(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			String driverId = userSessionMap.get("driver_id").toString();
   		
   			Map<String, Object> map = new HashMap<String, Object>();
   			map.put("driverId", driverId);
   			
   			List<Map<String, Object>> lowViolationList = lowViolationService.selectLowViolationList(map);
   			
   			mav.addObject("lowViolationList",lowViolationList );
   			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		
		return mav;
	}
	
	
	@RequestMapping(value = "/lowViolation-detail", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView lowViolationDetail(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				
				String lowViolationId = request.getParameter("lowViolationId");
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("lowViolationId", lowViolationId);
				Map<String, Object> lowViolation = lowViolationService.selectLowViolation(map);
				
				//목록을 확인 하면 읽음처리
				if(lowViolation != null && lowViolation.get("read_yn").toString().equals("N")) {
					map.put("readYn", "Y");
					map.put("driverId", userSessionMap.get("driver_id").toString());
					lowViolationService.updateLowViolationReadYn(map);	
				}
				
				lowViolation = lowViolationService.selectLowViolation(map);
				mav.addObject("lowViolation",  lowViolation);
					
				mav.addObject("driverId", userSessionMap.get("driver_id").toString());
					
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	@RequestMapping(value = "/updatePaymentYn", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updatePaymentYn(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			
			if(userSessionMap != null){
				
				String lowViolationId = request.getParameter("lowViolationId");
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				mav.addObject("paramMap", paramMap);
				
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("lowViolationId", lowViolationId);
				map.put("paymentYn", "Y");
				map.put("driverId", userSessionMap.get("driver_id").toString());
				//납부완료처리
				lowViolationService.updateLowViolationPaymentYn(map);
				
			}else {
				
				result.setResultCode("E000");
			}
			
				
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	
	@RequestMapping(value = "/election-list", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView electionList(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("driverId", userSessionMap.get("driver_id").toString());
				map.put("forUpdatePassWord", "");
				
				Map<String, Object> driver = driverService.selectDriver(map);
				
				map.put("carAssignCompany", driver.get("car_assign_company").toString());
				
				List<Map<String, Object>> electionList = electionService.selectElectionList(map);
				mav.addObject("driver", driver);
				mav.addObject("electionList", electionList);
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	
	
	@RequestMapping(value = "/viewElection", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView viewElection(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			String electionId = request.getParameter("electionId").toString();
			
			if(userSessionMap != null){
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("driverId", userSessionMap.get("driver_id").toString());
				map.put("forUpdatePassWord", "");
				Map<String, Object> driver = driverService.selectDriver(map);
				map.put("electionId", electionId);
				
				Map<String, Object> election = electionService.selectElection(map);
				mav.addObject("driver", driver);
				mav.addObject("election", election);
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	
	@RequestMapping(value = "/goVote", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView goVote(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			String electionId = request.getParameter("electionId").toString();
			
			if(userSessionMap != null){
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("driverId", userSessionMap.get("driver_id").toString());
				map.put("forUpdatePassWord", "");
				Map<String, Object> driver = driverService.selectDriver(map);
				map.put("electionId", electionId);
				
				Map<String, Object> election = electionService.selectElection(map);
				
				
				List<Map<String, Object>> candidateList = null;
				
				if(election.get("candidate_division").toString().equals("A")) {
					
					candidateList = voteService.selectCandidateListA(paramMap);
				}else if(election.get("candidate_division").toString().equals("O")) {
					candidateList = voteService.selectCandidateListO(paramMap);
					
				}else if(election.get("candidate_division").toString().equals("D")) {
					candidateList = voteService.selectCandidateListD(paramMap);
					
				}else if(election.get("candidate_division").toString().equals("S")) {
					candidateList = voteService.selectCandidateListS(paramMap);
					
				}else if(election.get("candidate_division").toString().equals("C")) {
					candidateList = voteService.selectCandidateListC(paramMap);
					
				}
				
				mav.addObject("driver", driver);
				mav.addObject("election", election);
				mav.addObject("candidateList", candidateList);
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	@RequestMapping(value = "/insertVote", method = {RequestMethod.POST})
	 @ResponseBody
		public ResultApi insertVote(Locale locale,ModelAndView mav, HttpServletRequest request, 
				HttpServletResponse response,@ModelAttribute ElectionVO electionVO) throws Exception {
			
		 
		 	ResultApi result = new ResultApi();
		 
			try{
				
				
				HttpSession session = request.getSession();
				Map userSessionMap = (Map)session.getAttribute("user");
				
				String electionId = request.getParameter("electionId").toString();
				String voteInfoList = request.getParameter("voteInfoList").toString();
				
				
				if(userSessionMap != null) {
					
					
					JSONParser jsonParser = new JSONParser();
					JSONObject jsonObject = (JSONObject) jsonParser.parse(voteInfoList);
					JSONArray jsonArray = (JSONArray) jsonObject.get("voteInfoList");
					
					for(int i = 0; i < jsonArray.size(); i++){
						JSONObject voteInfo = (JSONObject)jsonArray.get(i);
						VoteVO voteVO = new VoteVO();	
						voteVO.setElectionId(electionId);
						voteVO.setPickCandidateAssign(voteInfo.get("assign").toString());
						voteVO.setPickCandidateId(voteInfo.get("candidateId").toString());
						voteVO.setPickCandidateName(voteInfo.get("name").toString());
						voteVO.setVoteId("VOT"+UUID.randomUUID().toString().replaceAll("-", ""));
						voteVO.setVoterId(userSessionMap.get("driver_id").toString());
						voteVO.setVoterName(userSessionMap.get("driver_name").toString());
						voteService.insertVote(voteVO);
					}
					
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("electionId", electionId);
					electionService.updateElectionJoinCount(map);

				}else {
					result.setResultCode("E000");
				}
				
				
			}catch(Exception e){
				e.printStackTrace();
			}
			

			return result;
		}
	
	
	
	
	
	@RequestMapping(value = "/updatePaymentInfo", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updatePaymentInfo(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			
			if(userSessionMap != null){
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				mav.addObject("paramMap", paramMap);
				
				String allocationId = paramMap.get("allocationId").toString();
				String paymentKind = paramMap.get("paymentKind").toString();
				
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("allocationId", allocationId);
				map.put("driverId", userSessionMap.get("driver_id").toString());
				Map<String, Object> allocationMap = allocationService.selectAllocation(map);
				
				if(allocationMap == null) {
					result.setResultCode("0001"); 
				}else {
					
					List<Map<String, Object>> allocationList = allocationService.selectAllocationList(map);
					
					Map<String, Object> payment = new HashMap<String, Object>();
					payment.put("allocationId", allocationId);
					List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(payment);
					
					if(paymentInfoList != null && paymentInfoList.size() > 0){
						for(int k = 0; k < paymentInfoList.size(); k++) {
							Map<String, Object> paymentInfo = paymentInfoList.get(k);
							if(paymentInfo.get("payment_division").toString().equals("01")) {		//매출처 
																	
									String debtorCreditorIdForD = "DCI"+UUID.randomUUID().toString().replaceAll("-","");
									
									//매출로 넘어 가지 않았고, 차변에 입력 되지 않은 경우에만.....
									//완료 되었고 현장수금 또는 현금 건인 경우 해당 배차를 확정 하고 차변에 입력 한다.
									if(allocationList.get(0).get("billing_status").toString().equals("N") && (allocationList.get(0).get("debtor_creditor_id") == null || allocationList.get(0).get("debtor_creditor_id").toString().equals(""))) {
											
										//현장 수금건 탁송 완료시 해당 건에 대해 확정 및 매출로 넘기고 ....
										String billPublishRequestId = "BPR"+UUID.randomUUID().toString().replaceAll("-","");
										Map<String, Object> carInfoUpdateMap = new HashMap<String, Object>();
										carInfoUpdateMap.put("allocationId", allocationList.get(0).get("allocation_id").toString());
										carInfoUpdateMap.put("billPublishRequestId", billPublishRequestId);
										carInfoUpdateMap.put("decideStatus", "Y");
										carInfoUpdateMap.put("billingStatus", "D");
										carInfoUpdateMap.put("debtorCreditorId", debtorCreditorIdForD);
										adjustmentService.updateCarInfoForBillPublish(carInfoUpdateMap);
										//차변에 입력--------------------------------------------------------------------------
										//세금계산서 미 발행건에 대해서 각 건별로 차변에 등록한다.
										DebtorCreditorVO debtorCreditorVO = new DebtorCreditorVO();	
										debtorCreditorVO.setCustomerId(allocationList.get(0).get("customer_id").toString());
										debtorCreditorVO.setDebtorCreditorId(debtorCreditorIdForD);
										debtorCreditorVO.setOccurrenceDt(allocationList.get(0).get("departure_dt").toString());
										debtorCreditorVO.setSummary("운송료 ("+allocationList.get(0).get("departure").toString()+"-"+allocationList.get(0).get("arrival").toString()+")");
										debtorCreditorVO.setTotalVal(allocationList.get(0).get("sales_total").toString());
										debtorCreditorVO.setPublishYn("N");			//미발행건
										debtorCreditorVO.setType(BaseAppConstants.DEBTOR_CREDITOR_TYPE_D);
										debtorCreditorVO.setRegisterId("driver");
										debtorCreditorService.insertDebtorCreditor(debtorCreditorVO);
											
									}
									
										//paymentKind = "DD";
									
										Map<String, Object> paymentUpdateMap = new HashMap<String, Object>();
										paymentUpdateMap.put("payment", "Y");
										paymentUpdateMap.put("paymentDt", allocationList.get(0).get("departure_dt").toString());
										paymentUpdateMap.put("allocationId", allocationList.get(0).get("allocation_id").toString());
										paymentUpdateMap.put("paymentKind", paymentKind);
										paymentUpdateMap.put("billingDivision", "00");
										paymentUpdateMap.put("idx", paymentInfo.get("idx").toString());
										
										//결제 여부 업데이트
										paymentInfoService.updatePaymentInfoForDDFinish(paymentUpdateMap);
										
										//대변 입력(대변에 입력 되어 있우에는 다시 입력 하지 않는다...)
										if(paymentInfo.get("debtor_creditor_id") == null || paymentInfo.get("debtor_creditor_id").toString().equals("")) {
									
											String debtorCreditorId = "DCI"+UUID.randomUUID().toString().replaceAll("-","");
											DebtorCreditorVO debtorCreditorVO = new DebtorCreditorVO();
											debtorCreditorVO.setCustomerId(allocationList.get(0).get("customer_id").toString());
											debtorCreditorVO.setDebtorCreditorId(debtorCreditorId);
											debtorCreditorVO.setOccurrenceDt(allocationList.get(0).get("departure_dt").toString());
											paymentUpdateMap.put("debtorCreditorId", debtorCreditorId);
																			
											String summaryDt = ""; 
											summaryDt ="운송료 ("+allocationList.get(0).get("departure").toString()+" - "+allocationList.get(0).get("arrival").toString()+" 입금)"; 
											debtorCreditorVO.setSummary(summaryDt);
											debtorCreditorVO.setTotalVal(allocationList.get(0).get("sales_total").toString());
											debtorCreditorVO.setPublishYn("N");
											debtorCreditorVO.setType(BaseAppConstants.DEBTOR_CREDITOR_TYPE_C);
											debtorCreditorVO.setRegisterId("driver");
											debtorCreditorVO.setDebtorId(debtorCreditorIdForD);
											//대변에 입력 하고 
											debtorCreditorService.insertDebtorCreditor(debtorCreditorVO);	
											paymentInfoService.updatePaymentInfoDebtorCreditorId(paymentUpdateMap);
										}
									
																		
							}else if(paymentInfo.get("payment_division").toString().equals("02")) {
								
								//paymentKind = "DD";
								
								Map<String, Object> paymentUpdateMap = new HashMap<String, Object>();
								paymentUpdateMap.put("payment", "Y");
								paymentUpdateMap.put("paymentDt", allocationList.get(0).get("departure_dt").toString());
								paymentUpdateMap.put("allocationId", allocationList.get(0).get("allocation_id").toString());
								paymentUpdateMap.put("paymentKind", paymentKind);
								paymentUpdateMap.put("billingDivision", "00");
								paymentUpdateMap.put("idx", paymentInfo.get("idx").toString());
								
								//결제 여부 업데이트(기사)
								paymentInfoService.updatePaymentInfoForDDFinish(paymentUpdateMap);
								
							}
							
							
						}
					
					
				}
				
				
				
			}
				
				
				
			}else {
				
				result.setResultCode("0001");
			}
			
				
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	
	@RequestMapping(value = "/driver-list", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView driverList(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
			
				Map<String, Object> map = new HashMap<String, Object>();
				
				List<Map<String,Object>> allEmployeeList = driverService.selectAllDriver(map);
				
				mav.addObject("allEmployeeList", allEmployeeList);
				/*mav.addObject("fList", fList);*/
				mav.addObject("driverId", userSessionMap.get("driver_id").toString());
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	@RequestMapping(value = "/emp-list", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView empList(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
			
				Map<String, Object> map = new HashMap<String, Object>();
				
				List<Map<String,Object>> allEmployeeList = driverService.selectAllEmp(map);
				
				mav.addObject("allEmployeeList", allEmployeeList);
				/*mav.addObject("fList", fList);*/
				mav.addObject("driverId", userSessionMap.get("driver_id").toString());
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	@RequestMapping(value = "/contact-main", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView contactMain(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
			
				Map<String, Object> map = new HashMap<String, Object>();
				
				/*mav.addObject("fList", fList);*/
				mav.addObject("driverId", userSessionMap.get("driver_id").toString());
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	@RequestMapping(value = "/driver-register", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView driverRegister(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
			
				Map<String, Object> map = new HashMap<String, Object>();
				
				/*mav.addObject("fList", fList);*/
				mav.addObject("driverId", userSessionMap.get("driver_id").toString());
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	//신규 기사 아이디 중복체크 a.jax
	@RequestMapping(value = "/driverIdCheck", method = {RequestMethod.POST,RequestMethod.GET})
	@ResponseBody
	public ResultApi driverIdCheck(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		ResultApi resultApi = new ResultApi();
		try{
			
			
			HttpSession session = request.getSession();
			//Map userSessionMap = (Map)session.getAttribute("user");
	
			Map<String, Object> map = new HashMap<String, Object>();
			String driverId = request.getParameter("driverId").toString();
			map.put("driverId", driverId);				 
				
			int checkCount = driverService.selectDriverIdDuplicationCheck(map);
			
			if(checkCount > 0) {
				
				resultApi.setResultCode("0001"); 
				
			}else {
				
				resultApi.setResultCode("0000");
			
			}
				

			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return resultApi;
	}
	
	
	
	//신규 기사 회원가입 핸드폰 인증번호 전송 a.jax
	@RequestMapping(value = "/driverSendPinNum", method = {RequestMethod.POST,RequestMethod.GET})
	@ResponseBody
	public ResultApi driverSendPinNum(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		ResultApi resultApi = new ResultApi();
		try{
			
			
			HttpSession session = request.getSession();
			//Map userSessionMap = (Map)session.getAttribute("user");
			
	
			Map<String, Object> map = new HashMap<String, Object>();
			String phoneNum= request.getParameter("phoneNum").toString();
			
			map.put("phoneCertId", "PCI"+UUID.randomUUID().toString().replaceAll("-",""));
			map.put("customerId", "CUS0f3a8db538d74ff580efea0530a08bf2");
			map.put("phoneNum",phoneNum);
			map.put("phoneCertNum", generateCertNum());
			map.put("gubun", "D");
			
			sendSmsService.insertSendCertNumForApp(map);
			

			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return resultApi;
	}
	
	//인증번호 만들기 
	public String generateCertNum() {
		int certLength = 4;
		Random random = new Random();		
		
		int range = (int)Math.pow(10, certLength); //10의 6승
		int trim = (int)Math.pow(10,certLength-1); //n승 length -1
			
		int result = random.nextInt(range)+trim;
		if(result>range) {
			
			result = result - trim;
		}
		return String.valueOf(result);
}
	
	//신규 기사 회원가입 인증번호 비교  a.jax
	@RequestMapping(value = "/driverCompareCertNum", method = {RequestMethod.POST,RequestMethod.GET})
	@ResponseBody
	public ResultApi driverCompareCertNum(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		ResultApi resultApi = new ResultApi();
		try{
			
			
			HttpSession session = request.getSession();
			//Map userSessionMap = (Map)session.getAttribute("user");
			
	
			Map<String, Object> map = new HashMap<String, Object>();
			String phoneCertNum= request.getParameter("phoneCertNum").toString().replaceAll(" ", "");
			String today = WebUtils.getNow("yyyy-MM-dd").toString();
			
			map.put("phoneCertNum", phoneCertNum);
			map.put("today", today);
			
			int certNumCount = sendSmsService.selectCompareCertNum(map);
			
			if(certNumCount == 0) {
				resultApi.setResultCode("0001"); 
				
			}else {
				resultApi.setResultCode("0000"); 
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return resultApi;
	}	
	
	//신규 기사 회원가입
	
	@RequestMapping(value = "/insertDriverRegister", method = {RequestMethod.POST,RequestMethod.GET})
	@ResponseBody
	public ResultApi insertDriverRegister(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response, DriverVO driverVO) {
		ResultApi resultApi = new ResultApi();
		try{
			
			
			HttpSession session = request.getSession();
			//Map userSessionMap = (Map)session.getAttribute("user");
			
			
			String driverId = request.getParameter("driverId").toString();
			String driverPwd = request.getParameter("driverPwd").toString();
			String driverName = request.getParameter("driverName").toString();
			String residentRegistrationNumber = request.getParameter("residentRegistrationNumber").toString();
			String phoneNum = request.getParameter("phoneNum").toString();
			String companyKind =request.getParameter("companyKind").toString();
			String businessLicenseNumber = request.getParameter("businessLicenseNumber") == null ? "" :request.getParameter("businessLicenseNumber").toString();
			String driverLicense = request.getParameter("driverLicense") == null ? "" :request.getParameter("driverLicense").toString();
			String carAssignCompany= request.getParameter("carAssignCompany").toString();
			String carKind = request.getParameter("carKind") == null ? "" : request.getParameter("carKind").toString();
			String carIdNum = request.getParameter("carIdNum") == null ? "" :request.getParameter("carIdNum").toString();
					
			
			driverVO.setDriverId(driverId);
			driverVO.setDriverPwd(driverPwd);
			driverVO.setDriverName(driverName);
			driverVO.setDriverKind("00");
			driverVO.setResidentRegistrationNumber(residentRegistrationNumber);
			driverVO.setPhoneNum(WebUtils.getMyPhone(phoneNum));
			driverVO.setCompanyKind(companyKind);
			driverVO.setCompanyKind(companyKind);
			driverVO.setBusinessLicenseNumber(businessLicenseNumber);
			driverVO.setDriverLicense(driverLicense);
			driverVO.setCarAssignCompany(carAssignCompany);
			driverVO.setCarKind(carKind);
			driverVO.setCarIdNum(carIdNum);
			
			driverService.insertDriver(driverVO);
			
			
			
		}catch(Exception e){
			e.printStackTrace();
			resultApi.setResultCode("0001");
			
		}
		
		
		return resultApi;
	}
	
	
}
