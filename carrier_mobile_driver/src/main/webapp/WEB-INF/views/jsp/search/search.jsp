<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko">      
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
 
 <style>
 
 
#searchWord:focus::-webkit-input-placeholder {color: transparent;}  /* chrome */
#searchWord:focus::-moz-placeholder {color: transparent;}           /* firefox */
 
 
 </style>
 
  
<body class="black">


<script type="text/javascript">  
  
$(document).ready(function(){
	
    $('#searchWord').keyup(function (e) {
        var SearchBoxVal = $(this).val();
        var obj = this;
    	 
        if (SearchBoxVal.length >= 2) {
        	
            setTimeout( function() {
            	getAjaxData(SearchBoxVal);
            }, 1000);
        }
        
    });


    $('#searchWord').val("${searchWord}");
    $('#searchWord').focus();
    
	
});
  

  
  
  
  
  
  
  
  
  
  
  
  
  
  
function getAjaxData(searchWord){
	
	
	$.ajax({ 
		type: 'post' ,
		url : "/search/searchItem.do" ,
		dataType : 'json' ,
		data : {
			searchWord : searchWord
		},
		success : function(data, textStatus, jqXHR)
		{
			
			if(data.resultCode == "0000"){
				
				var result = "";
				var resultData = data.resultData;	
				$("#resultArea").html("");
				
				if(resultData.length > 0){
					result += '<div class="suggestion">';
					result += '<div class="suggestions">';
					for(var i = 0; i < resultData.length; i++){
						result += '<a class="item search-box foricon">'; 
						result += '<span class="title" onclick="javascript:goSearchByResult(\''+resultData[i].item_name+'\');">'+resultData[i].item_name+'</span>';
						result += '</a>';
					}	
					result += '</div>';
					result += '</div>';
					$("#resultArea").html(result);
				}else{				//리스트가 없을때 처리
					
				}
				
				
			}else{
				alert("검색오류");	
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	}); 

	
	
	
}
  
  
  
  
function goSearch(){

	if($("#searchWord").val() == ""){
		alert("키워드가 입력되지 않았습니다.");
		return false;
	}else{
		
		document.location.href = "/search/search-result.do?searchWord="+encodeURIComponent($("#searchWord").val());
	}
	
}
  
function goSearchByResult(result){
	if(result != ""){
		document.location.href = "/search/search-result.do?searchWord="+encodeURIComponent(result);	
	}else{
		return false;
	}
	
}

function deleteSearchItem(keywordId,obj){
	
	
	$.ajax({ 
		type: 'post' ,
		url : "/search/deleteSearchItem.do" ,
		dataType : 'json' ,
		data : {
			keywordId : keywordId
		},
		success : function(data, textStatus, jqXHR)
		{
			if(data.resultCode == "0000"){
				
				$(obj).parent().remove();
				
				if($("#searchList").find("div").length == 0){
					$("#deleteAll").remove();
					var result = "";
					result += '<div class="no-result content-box">'; 
					result += '<div class="notfound">';
					result += '<span>최근에 검색한 내역이 없습니다</span>';
					result += '<img src="/img/alert-icon.png" alt="">';
					result += '</div>';
					result += '</div>';
					$("#recent-searches").append(result);
				}
			}else{
				alert("검색어를 삭제 하는데 오류가 발생 하였습니다.\r\n관리자에게 문의 하세요");	
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	}); 
	
}

function deleteSearchItemAll(){
	
	if(confirm("최근 검색어를 모두 삭제 하시겠습니까?")){
		$.ajax({ 
			type: 'post' ,
			url : "/search/deleteSearchItemAll.do" ,
			dataType : 'json' ,
			data : {
			},
			success : function(data, textStatus, jqXHR)
			{
				if(data.resultCode == "0000"){
					$("#deleteAll").remove();
					$("#searchList").remove();
					var result = "";
					result += '<div class="no-result content-box">'; 
					result += '<div class="notfound">';
					result += '<span>최근에 검색한 내역이 없습니다</span>';
					result += '<img src="/img/alert-icon.png" alt="">';
					result += '</div>';
					result += '</div>';
					$("#recent-searches").append(result);
				}else{
					alert("검색어를 삭제 하는데 오류가 발생 하였습니다.\r\n관리자에게 문의 하세요");	
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	 
		
}


</script>



		<c:if test="${searchList != null && fn:length(searchList) != 0}">
			<div class="content-container searchresult black emptyresult searchresult2">
	            <header  style="background-color:#1e212a; position:fixed; top:0px; z-index:10000;">
	            	<!-- <div id="fixedMenu" style="background-color:#1e212a; position:fixed; top:0px; z-index:10000;"> -->
		                <div class="search-icon">
		                    <a  onclick="javascript:history.go(-1);"><img src="/img/back-icon.png" alt=""></a> 
		                </div>
		                <div class="search-container">
		                    <a  onclick="javascript:goSearch();" class="search-input-icon">
		                        <img src="/img/search-input-icon.png" alt="">
		                    </a>
		                    <%-- <input type="text" name="" id="searchWord" placeholder="종목 혹은 테마를 검색해주세요" value="${searchWord}"> --%>
		                    <input type="text" name="" id="searchWord" placeholder="종목 혹은 테마를 검색해주세요" >
		                </div>
		                <div class="cancel">
		                    <!-- <a href="/adviser/adviser.do">취소</a> -->
		                    <a  onclick="javascript:history.go(-1);">취소</a>
		                </div>
	                <!-- </div> -->
	            </header>
	            <div id="resultArea" class="recent-searches suggestion-box" style="margin-top:55px;">
	                <div class="suggestion nb">
	                    <div class="head lbl-head">
	                        <span class="title">실시간 이슈</span>
	                    </div>
	                    <div class="tags-list-box">
	                        <ul>
	                            <li><a href="#">단타</a></li>
	                            <li><a href="#">북미정상회담주</a></li>
	                            <li><a href="#">유가</a></li>
	                            <li><a href="#">비트코인</a></li>
	                            <li><a href="#">철강주</a></li>
	                            <li><a href="#">경협주</a></li>
	                        </ul>
	                    </div>
	                </div>
	                <div class="suggestion">
	                    <div class="head lbl-head">
	                        <span class="title">최근검색어</span>
	                        <a class="delete-all"  onclick="javascript:deleteSearchItemAll()">전체삭제</a>
	                    </div>
	                    <div class="suggestions"  id="searchList">
	                    	<c:forEach var="data" items="${searchList}" varStatus="status">
		                        <a class="item search-box foricon">
		                            <span class="title" onclick="javascript:goSearchByResult('${data.keyword_name}');">${data.keyword_name}</span> 
		                            <span class="icon-box delete"  onclick="javascript:deleteSearchItem('${data.keyword_id}',this);"></span>
		                        </a>
	                        </c:forEach>
	                    </div>
	                    
	                </div>
	            </div>
	        </div>
		</c:if>
		<c:if test="${searchList == null || fn:length(searchList) == 0}">
			<div class="content-container searchresult emptyresult black">
	            <header>
	                <div class="search-icon">
	                    <a  onclick="javascript:history.go(-1);"><img src="/img/back-icon.png" alt=""></a> 
	                </div>
	                <div class="search-container">
	                    <a  onclick="javascript:goSearch();" class="search-input-icon">
	                        <img src="/img/search-input-icon.png" alt="">
	                    </a>
	                    <input type="text" name="" id="" value="종목 혹은 테마를 검색해주세요">
	                </div>
	                <div class="cancel">
	                    <!-- <a href="#">취소</a> -->
	                    <a  onclick="javascript:history.go(-1);">취소</a>
	                </div>
	            </header>
	            <div class="no-result content-box">
	                <div class="notfound black">
	                    <span>최근에 검색한 내역이 없습니다</span>
	                    <img src="/img/alert-icon.png" alt="">
	                </div>
	            </div>
	        </div>
		</c:if>
		

        <%-- <div class="content-container">
            <header>
                <div class="search-icon">
                    <a style="cursor:pointer;" onclick="javascript:goSearch();"><img src="/img/search-icon.png" alt=""></a> 
                </div>
                <div class="search-container">
                    <input type="text" name="searchWord" id="searchWord" placeholder="종목명/코드 또는 키워드 입력">
                </div>
                <div class="cancel">
                    <a href="/adviser/adviser.do">취소</a>
                </div>
            </header>

            <div class="recent-searches" id="recent-searches">
            	<c:if test="${searchList != null && fn:length(searchList) != 0}">
	                <div class="head foricon" id="deleteAll">
	                    <span>최근 검색어</span>
	                    <a href="#" class="delete-all">전체삭제</a>
	                    <a onclick="javascript:deleteSearchItemAll()" class="icon-box delete"></a>
	                </div>
                </c:if>
                <c:if test="${searchList != null && fn:length(searchList) != 0}">
	                <div class="searches" id="searchList">
		                <c:forEach var="data" items="${searchList}" varStatus="status">
							<div class="search-box foricon">
		                        <a style="cursor:pointer;" onclick="javascript:goSearchByResult('${data.keyword_name}')" class="search ">${data.keyword_name}</a>
		                        <a onclick="javascript:deleteSearchItem('${data.keyword_id}',this)" class="icon-box delete"></a>
		                    </div>			
						</c:forEach>
    	            </div>
				</c:if>
                <c:if test="${searchList == null || fn:length(searchList) == 0}">
	                <div class="no-result content-box">
		                <div class="notfound black">
		                    <span>최근에 검색한 내역이 없습니다</span>
		                    <img src="img/alert-icon.png" alt="">
		                </div>
		            </div>
                </c:if>
            </div>
        </div> --%>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      <script src="/js/vendor/bootstrap.min.js"></script>       
    <script src="/js/main.js"></script>       
    </body>
</html>
