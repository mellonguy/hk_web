<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

String BKW_TRADENO			= request.getParameter("BKW_TRADENO");
String BKW_RESULTCD  		= request.getParameter("BKW_RESULTCD");
String BKW_RESULTMSG  	= request.getParameter("BKW_RESULTMSG");
String BKW_PAYTYPE  		= request.getParameter("BKW_PAYTYPE");


%>

<!DOCTYPE html>
<html lang="ko">      
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
    
<body class="bg-gray">

<script type="text/javascript" src="https://pay.duzonpay.com:9143/Script/duzonpay.js"></script>		
<script type="text/javascript">  
  
function goPurchasePage(){

	document.location.href = "/menu/purchase-history.do";	
	
}
    
function purchasePoint(reason,point){
	
	//alert(reason);
	//alert(point);
	//document.location.href = "";
	
	
}    
    
//신용카드 매출전표 보기
function openWindow(url, width, height, taxOptn) {
		var urlOpt = "scrollbars=no, resizable=no, copyhistory=no, location=no, width=" + width + ",height=" + height + ", left=0, top=0";
		window.open(url , 'slipPop', urlOpt);
}    
    
    
</script>

        <div class="content-container">
            <header class="clearfix">
                <div class="search-icon">
                    <a style="cursor:pointer;" href="/menu/primary.do"><img src="/img/back-icon.png" alt=""></a> 
                </div>
                <div class="page-title txt-medium">
                	<c:if test="${payType != 'PA10'}">
                        	포인트 충전 완료
                    </c:if>
                    <c:if test="${payType == 'PA10'}">
                        	입금 대기중
                    </c:if>
                </div>
                <div class="menu-bar pull-right">
                    <a href="/adviser/adviser.do"><img src="/img/home-pink-icon.png" alt=""></a>
                </div>
            </header>
            <div class="purchase-complete">
            	<c:if test="${payType != 'PA10'}">
                	<div class="charged">
	                    <span class="roboto-m">
	                        ${point}P 
	                    </span>
	                    충전되어
	                </div>
	                <div class="total"> 
	                    총 <span class="roboto-m">${userPoint}P</span>  보유 중 입니다.
	                </div>
                </c:if>
                <c:if test="${payType == 'PA10'}">
                	<div class="charged">
	                    <span class="roboto-m">
	                        ${point}P 
	                    </span>
	                    충전을 위해 입금 대기중 입니다.
	                </div>
	                <div class="total"> 
	                    현재 <span class="roboto-m">${userPoint}P</span>  보유 중 입니다.
	                </div>
                </c:if>
                <div class="btn-holder">
                    <a href="/menu/primary.do">확인</a>
                </div>
            </div>
            
            <%-- <input type="button" value="매출전표보기" onClick='javascript:openWindow("https://biz.duzonpay.com:9143/Modules/Bill/ADTS_Bill_Blue.jsp?c_trade_no=<%= BKW_TRADENO %>&trade_type=<%= BKW_PAYTYPE %>", "400", "600");'> --%>
            
        </div>
		

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      <script src="/js/vendor/bootstrap.min.js"></script>       
    <!-- veiwport for countnumber -->
    <script src="/js/jquery.viewportchecker.js"></script>    
    <script src="/js/main.js"></script>       
    </body>
</html>
