<%--
	/*
	 * =============================================
	 * Filename	:	result.jsp
	 * Function	:	결제성공 시 이동하는 페이지
	 * Author		:	All contents Copyright DuzonPay all rights reserved
	 * =============================================
	 */
--%>
<%@ page contentType="text/html;charset=euc-kr" pageEncoding="euc-kr"%>
<%
		/*
     * 1. 결제관련 변수 받아오기
     * 결제요청페이지 pay.jsp의 duzonpay form 안에 선언된 요소들이 모두 넘어옵니다.
     */
    String BKW_TRADENO			= request.getParameter("BKW_TRADENO");
    String BKW_RESULTCD  		= request.getParameter("BKW_RESULTCD");
    String BKW_RESULTMSG  	= request.getParameter("BKW_RESULTMSG");
    String BKW_PAYTYPE  		= request.getParameter("BKW_PAYTYPE");
    
    /*
     * 2. 결제성공시 DB처리
     */     
     if(BKW_RESULTCD.equals("0000")){
     		// 결제 성공시 처리 작업
     		// DB작업은 백노티페이지를 통해서 1차적으로 전송됩니다.
     		// 이곳에서는 결제결과 view 작업을 하시면 됩니다.
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="euc-kr" />
		<title>더존페이 전자결제 샘플 페이지(DuzonPay)</title>
		<!-- 
    	* 3. duzonpay.js를 include 합니다.
    -->
		<script type="text/javascript" src="https://pay.duzonpay.com:9143/Script/duzonpay.js"></script>
		
		<!-- 
			* 4. 신용카드 매출전표 출력 연동을 위한 함수 선언
			* 전표보기 버튼을 생성 후 아래 함수를 호출하면 됩니다.
		-->		
		<script type="text/javascript">
		// 신용카드 매출전표 보기
		function openWindow(url, width, height, taxOptn) {
				var urlOpt = "scrollbars=no, resizable=no, copyhistory=no, location=no, width=" + width + ",height=" + height + ", left=0, top=0";
				window.open(url , 'slipPop', urlOpt);
		}
		</script>
</head>
<body>
	
	<!-- 
	 * 거래 성공 후 결과페이지를 구현하시면 되니다. 
	 * 추가 버튼을 이용해서 카드매출전표도 연동하시면 구매 후 매출전표 출력이 가능합니다.	
	-->
	<table width="650" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td height="75" align="center">
				<span style="font-family:굴림;font-size:12pt;color:#6d4c29;width:100%;line-height:100px;">
					<h2>결제가 정상적으로 이루어졌습니다.</h2>							
				</span>
						카드결제는 아래 [매출전표보기] 버튼을 통해 매출전표를 출력하시면 됩니다.
			</td>
		</tr>
		<tr>
			<td height="75" align="center">				
				<input type="button" value="매출전표보기" onClick='javascript:openWindow("https://biz.duzonpay.com:9143/Modules/Bill/ADTS_Bill_Blue.jsp?c_trade_no=<%= BKW_TRADENO %>&trade_type=<%= BKW_PAYTYPE %>", "400", "600");'>
			</td>
		</tr>					
	</table>
	
</body>
</html>
<%
     }
%>