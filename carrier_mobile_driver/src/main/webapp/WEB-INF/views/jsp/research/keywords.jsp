<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko">      
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
    
    
   <script type="text/javascript">  

$(document).ready(function(){
	
	//getNewsData("${searchWord}");
	
});



var userCategoryList = new Array();
 function insertUserCategory(){
	 
//	 var total = $('#categoryList').find("li").length;

	userCategoryList.length = 0;
	$('#categoryList').find("li").each(function(index,element) {
	    
		if($(this).find("a").html() != ""){
			var categoryObject = new Object();
			categoryObject.category_name = $(this).find("a").html()	
			userCategoryList.push(categoryObject);
		}
		
	 });
	
	 //$("#carInfoVal").val(JSON.stringify({carInfoList : carInfoList}))
	 
	  if(confirm("저장하시겠습니까?")){
		 
		 $.ajax({ 
				type: 'post' ,
				url : "/research/insertUserCategory.do",
				dataType : 'json' ,
				data : {
					userCategoryList : JSON.stringify({userCategoryList : userCategoryList})
				},
				success : function(data, textStatus, jqXHR)
				{
					//alert(data);
					document.location.href = "/research/theme.do";
				} ,
				error : function(xhRequest, ErrorText, thrownError) {
				}
			});		 
		 
	 } 
	 
		
} 


    
</script>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
<body class="black">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div class="content-container black themeedit">
            <header class="clearfix nb">
                <div class="search-icon">
                    <a href="#" class="goback"><img src="/img/back-icon.png" alt=""></a> 
                </div>
                <div class="page-title txt-medium white">
                    키워드 편집
                </div>
                <div class="save pull-right white">
                    <a onclick="javascript:insertUserCategory();">저장</a> 
                </div>
            </header>
            <div class="keywords-box">
                <ul id="categoryList">
	                <c:forEach var="data" items="${userCategoryList}" varStatus="status">
	                	<li><a href="#" id="${data.category_cd}">${data.category_name }</a></li>
	                </c:forEach>
                </ul>
                <div class="text-center arrow-up">
                        <a href="#"><img src="/img/arrow-up-icon.png" alt=""></a>
                </div>
                <div class="keyword-enter input-box">
                    <input type="text" placeholder="예) 삼성전자, 신라젠, 셀트리온 등">
                    <span class="note">키워드를 설정하면 해당하는 기사를 불러옵니다</span>
                </div>
            </div>
            <div class="btn-holder">
                <a href="#" class="keyword-add">추가</a>
            </div>
        </div>


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      <script src="/js/vendor/bootstrap.min.js"></script>       
    <!-- veiwport for countnumber -->
    <script src="/js/jquery.viewportchecker.js"></script>    
    <script src="/js/main.js"></script>       
    </body>
</html>
