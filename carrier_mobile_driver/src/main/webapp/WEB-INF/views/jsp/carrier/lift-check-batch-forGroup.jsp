<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko">      
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
    
<body>


<script type="text/javascript">  
$(document).ready(function(){
	
	/* if("${paramMap.autoClick}" == "Y" && "${paramMap.allocationStatus}" == "R"){
		$("#addPicStatusR").trigger("click");
	}else if("${paramMap.autoClick}" == "Y" && "${paramMap.allocationStatus}" == "D"){
		$("#addPicStatusD").trigger("click");
	}  */
	
});





 function getNewsData(obj,keyword){
	
	 
	// alert($("#selectedStatus").children().length);
	 
	//alert($("#selectedStatus").children().first().get(0));	
	//alert($(obj).parent().get(0));
	
 	 if($("#selectedStatus").children().first().get(0) == $(obj).parent().get(0)){				//전체를 클릭 한경우
					
 		$("#selectedStatus").children().each(function(index,element){
 			$(this).removeClass('selected');
 		});
 		$(obj).parent().addClass('selected');
	}else{
			
	} 
	
	
	
/* 	
	
	$.ajax({ 
		type: 'post' ,
		url : "/research/newsSearch.do?&keyword="+keyword ,
		dataType : 'json' ,
		data : {
			
		},
		success : function(data, textStatus, jqXHR)
		{
			alert(data);
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
	 */
	
	
} 


 function carCheck(id,status){
 	
	 /* $.confirm('사진을 추가 하시겠습니까? ',function(a){
		 if(a){
			 $("#upload").trigger("click");
		 }
	}); */
	
	 $("#upload").trigger("click");
	
}
 
 function fileUpload(allocationId,currentStatus,obj){
	   	
 	//alert($("#upload").val());
 	
 	homeLoader.show();
 	$('#excelForm').ajaxForm({
			url: "/carrier/upLiftCheck.do",
			enctype: "multipart/form-data", 
		    type: "POST",
			dataType: "json",		
			data : {
				allocationId : allocationId,
				currentStatus : currentStatus
		    },
			success: function(data, response, status) {
				var status = data.resultCode;
				if(status == '0000'){
					/* $.alert("이미지가 등록 되었습니다.",function(a){ */
						document.location.href = "/carrier/lift-check-batch-forGroup.do?groupId=${groupId}&allocationId="+allocationId+"&allocationStatus="+currentStatus;
					/* }); */
				}else if(status == '1111'){
							
				}
						
				
						
			},
			error: function() {
				//alertEx("이미지 등록중 오류가 발생하였습니다.");
			}                               
		});
		$("#excelForm").submit();
		var updateHomeLoader = setInterval(function() {
			clearInterval(updateHomeLoader);
			homeLoader.hide();
		}, 300);
    	
 }
 
 
 function allocationAccept(id,status){
 	//alert(allocationId);
 	var msg = "";
 	var nextStatus = "";
 	
 	if(status == "R"){
 		msg = "상차완료";
 		nextStatus = "S";
 	}else if(status == "I"){
 		msg = "인계완료";
 		nextStatus = "P";
 	}else if(status == "D"){
 		msg = "하차완료";
 		nextStatus = "B";
 	}
 	//homeLoader.show();
 	//window.Android.toastShort( "JavscriptInterface Test" );
 	
 	$.confirm(msg+" 하시겠습니까?",function(a){
	   		 if(a){

	   			homeLoader.show();
		   		 
	   			$.ajax({ 
					type: 'post' ,
					url : "/allocation/updateAllocationStatus.do" ,
					dataType : 'json' ,
					async : false,
					data : {
						allocationStatus : nextStatus,
						allocationId : id
					},
					success : function(data, textStatus, jqXHR)
					{
						var result = data.resultCode;
						var resultData = data.resultData;
						if(result == "0000"){
							//alert(msg+"되었습니다.");
							/* $.alert(msg+"되었습니다.",function(a){ */
								var rtnStatus = "";
								if(resultData != null && resultData.resultStatus != null && resultData.resultStatus != ""){
									rtnStatus = resultData.resultStatus;
								}
								//document.location.href = "/carrier/main.do";
								if(rtnStatus == "F"){
									//document.location.href = "/carrier/allocation-detail-forGroup.do?allocationStatus="+rtnStatus+"&allocationId="+id;
									
									document.location.href = "/carrier/group-list.do?&allocationId=${groupId}&allocationStatus=Y";
									
								}else{
									
									//다음 상태로 보내지 않고 승인 완료 상태로 보내도록 수정 한다...
									//document.location.href = "/carrier/group-list.do?&allocationId=${paramMap.groupId}&allocationStatus="+nextStatus;
									
									document.location.href = "/carrier/group-list.do?&allocationId=${groupId}&allocationStatus=Y";
									
									
									//document.location.href = "/carrier/allocation-detail.do?allocationStatus="+nextStatus+"&allocationId="+id;
									//alert("tsetsetset");				//여기 들어 왔음
								}
							/* }); */
						}else if(result == "E000"){
							$.alert("해당 배차를 "+msg+" 할 수 없습니다.",function(a){
								document.location.href = "/carrier/group-list.do?allocationId=${groupId}&allocationStatus=${allocationStatus}";
							});
						}else{
							$.alert("해당 배차를 "+msg+" 하는데 실패 했습니다. 관리자에게 문의 하세요.",function(a){
								document.location.href = "/carrier/group-list.do?allocationId=${groupId}&allocationStatus=${allocationStatus}";
							});
						}
						
					} ,
					error : function(xhRequest, ErrorText, thrownError) {
					}
				});	
	   		 }
	   	});

	   			var updateHomeLoader = setInterval(function() {
	   				clearInterval(updateHomeLoader);
	   				homeLoader.hide();
	   			}, 300);

	   		 	
 }
 
 
 function deleteFile(){
	 
	 var delFileCnt = setRemoveFile(); 
	 var deleteFileIdArr = new Array();
	 if(delFileCnt > 0){
		 $("#fileList").find("img").each(function(index,element){
			 if($(this).hasClass("del") === true){
				 deleteFileIdArr.push($(this).attr("id"));	  
			 }
		});
		 $.confirm(delFileCnt+'장의 사진을 삭제 하시겠습니까?(인수증은 삭제 되지 않습니다.) ',function(a){
			 if(a){
				 homeLoader.show();
				 $.ajax({ 
						type: 'post' ,
						url : "/carrier/deleteAllocationFile.do" ,
						dataType : 'json' ,
						async : false,
						traditional : true,
						data : {
							deleteFileIdArr : deleteFileIdArr,
							allocationId : '${allocationIdArr}'
						},
						success : function(data, textStatus, jqXHR)
						{
							var result = data.resultCode;
							var resultData = data.resultData;
							if(result == "0000"){
								if(resultData.notDeletedFileCnt > 0){
									$.alert("인수단계의 사진 "+resultData.notDeletedFileCnt+"장을 제외한 "+resultData.deletedFileCnt+"장의 파일이 삭제되었습니다.",function(a){
										window.location.reload();
									});	
								}else{
									/* $.alert(resultData.deletedFileCnt+"장의 파일이 삭제되었습니다.",function(a){ */
										window.location.reload();
									/* }); */
								}
								
							}else{
								$.alert("파일을 삭제 하는데 실패 했습니다. 관리자에게 문의 하세요.",function(a){
									window.location.reload();
								});
							}
							
						} ,
						error : function(xhRequest, ErrorText, thrownError) {
						}
					});	
				 var updateHomeLoader = setInterval(function() {
						clearInterval(updateHomeLoader);
						homeLoader.hide();
					}, 300);
				 
			 }
		});
		  
	 }
	 
 }
 
 
 function setRemoveFile(){
	 
	 //alert($("#fileList").find("img").length);
	 var cnt = 0;
	 $("#fileList").find("img").each(function(index,element){
		 if($(this).hasClass("del") === true){
			 cnt++ 
		 }
	});
	 
	 return cnt;
 }
 
 
 var startTime = null;
 var endTime = null; 
 
 function setMod(fileId,obj){

	if($(obj).hasClass("del") === true){
		endTime = new Date().getTime();
		if(endTime-startTime < 500){
			document.location.href  = "/carrier/image-view.do?src="+$(obj).attr("src");
		}else{
			$(obj).removeClass("del");
			$(obj).css("border","none");	
		}
	}else{
		startTime = new Date().getTime();
		$(obj).addClass("del");
		$(obj).css("border","3px solid #EB8B1B");
	}
	if(setRemoveFile() > 0){
		$("#delIcon").css("display","");
	}else{
		$("#delIcon").css("display","none");
	}
	
 }
 
 
 function backKeyController(str){
	 //document.location.href='/carrier/main.do';
	 
	// history.go(-1);
	document.location.href="/carrier/group-list.do?allocationId=${groupId}&allocationStatus=${paramMap.allocationStatus}";
	 
}
 
 
</script>


        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div class="content-container">
            <%-- <jsp:include page="/WEB-INF/views/jsp/common-top.jsp"></jsp:include> --%>
            <!-- <header class="bg-pink clearfix" style="background-color:#fff; border:none; text-align:center; "> -->
            <header class="bg-pink clearfix" style="position:fixed; background-color:#fff; border:none; text-align:center; z-index:3; ">
                <div class="" style="width:19%;">
                    <!-- <a href="javascript:document.location.href='/carrier/main.do'"><img style="zoom: 0.5;" src="/img/back-icon.png" alt=""></a> -->
                    <a href="/carrier/group-list.do?allocationId=${groupId}&allocationStatus=${paramMap.allocationStatus}"><img style="zoom: 0.5;" src="/img/back-icon.png" alt=""></a> 
                </div> 
                <div class="" style="width:60%;">
                	<!-- <img style="width:90%; height:60%;" src="/img/main_logo.png" alt=""> -->
                	<!-- <img style="width:90%; height:60%;" onclick="javascript:document.location.href='/carrier/main.do'" src="/img/main_logo.png" alt=""> -->
                	<img style="width:90%; height:60%;" onclick="javascript:document.location.href='/carrier/group-list.do?allocationId=${groupId}&allocationStatus=${paramMap.allocationStatus}'" src="/img/main_logo.png" alt="">
                </div>
                <div class="" style="width:19%; float:right;">
                   <!-- <a style="cursor:pointer;" href="javascript:logout();"><img style="" src="/img/logout.png" alt=""></a> -->
                </div>
            </header>
            <div style="text-align:right;">${user.driver_name}님 환영 합니다.</div>
            <div class="content-container interest-page loadingthemepage news" style="background-color:#fff">
	            
	            
	            
	            <div style="width:100%; margin-top:30px; height:50px; border-top:1px solid #eee; border-bottom:1px solid #eee;">
                   <div class="menu-container" id="delIcon" style="width:100%; text-align:right; display:none;">
						<%-- <a class="kakao txt-bold" style="background-color:#F8A717; line-height:44px; font-size:15px; border-radius:10px; width:22%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:carCheck('${allocationList[0].allocation_id}','${allocationList[0].allocation_status_cd}');"> --%>
		                   		<img style="width:7%;" src="/img/delete-x-icon.png" onclick="javascript:deleteFile();" />
		               	<!-- </a> -->
                   
                   
	                   <%-- <a class="kakao txt-bold" style="background-color:#F8A717; line-height:44px; font-size:15px; border-radius:10px; width:22%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:carCheck('${allocationList[0].allocation_id}','${allocationList[0].allocation_status_cd}');">
		                   		앨 범
		               	</a>
		               	<a class="kakao txt-bold" style="background-color:#F8A717; line-height:44px; font-size:15px; border-radius:10px; width:22%; display:inline-block; cursor:pointer; margin-top:0px; margin-left:10px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:carCheck('${allocationList[0].allocation_id}','${allocationList[0].allocation_status_cd}');">
		                   		촬 영
		               	</a>
		               	<a class="kakao txt-bold" style="background-color:#615849; color:#fff; line-height:44px; font-size:15px; border-radius:10px; width:22%; display:inline-block; cursor:pointer; margin-top:0px; margin-left:10px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:carCheck('${allocationList[0].allocation_id}','${allocationList[0].allocation_status_cd}');">
		                   		전체선택
		               	</a>
		               	<a class="kakao txt-bold" style="background-color:#615849; color:#fff; line-height:44px; font-size:15px; border-radius:10px; width:22%; display:inline-block; cursor:pointer; margin-top:0px; margin-left:10px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:carCheck('${allocationList[0].allocation_id}','${allocationList[0].allocation_status_cd}');">
		                   		삭 제
		               	</a> --%>
		               	
                   </div>
                   
                </div>
            </div>
            
            <div class="overview-box active content-view" style=" clear:both; margin-top:0px;">
                	
                	
                   <div class="view-details">
                       <div class="overview detail-box">
                         <!-- <span class="price">자량정보</span> -->
                           
                           
                       <div class="detail-box" style="margin-top:0px;">
                           <%-- <div class="other-info d-table">
                               <div class="eps left t-cell">
                                   <span class="label-text">고객명</span>
                                   <span class="value"></span>
                               </div>
                               <div class="per t-cell right">
                                   <span class="label-text">담당자</span>
                                   <span class="value">${allocationList[0].charge_name}</span>
                               </div>
                           </div>
                           <div class="other-info d-table">
                               <div class="eps left t-cell">
                                   <span class="label-text">담당자연락처</span>
                                   <span class="value">${allocationList[0].charge_phone}</span>
                                   <span class="value"><a href="tel:${allocationList[0].charge_phone}">${allocationList[0].charge_phone}</a></span>
                                   <!-- <span class="value"><a href="tel:010-8926-5400">010-8926-5400</a></span> -->
                               </div>
                               <div class="per t-cell right">
                                   <span class="label-text">비고</span>
                                   <span class="value">${allocationList[0].person_in_charge_etc}</span>
                               </div>
                           </div> --%>
                           
                           
                           <c:forEach var="data" items="${groupList}" varStatus="status">
	                           <div class="other-info d-table">
	                               <div class="market-capitaliztion left t-cell" style="border-right:none;">
	                                   <span class="label-text" style="width:30%;">차량(${status.index+1})</span>
	                                   	<%-- <span class="value" id="departureAddr" style="width:85%;">차종:${data.carInfo.car_kind},차대번호:${data.carInfo.car_id_num},차량번호:${data.carInfo.car_num}</span> --%>
	                                   	<span class="value" id="departureAddr" style="width:70%;">${data.car_kind}&nbsp;&nbsp;/&nbsp;&nbsp;${data.car_id_num}&nbsp;&nbsp;/&nbsp;&nbsp;${data.car_num}</span>
	                               </div>
	                           </div>
                           </c:forEach>
                           
                           <%-- <div class="other-info d-table">
                               <div class="market-capitaliztion left t-cell" style="border-right:none;">
                                   <span class="label-text">차량</span>
                                   	<span class="value" id="departureAddr">${allocationList[0].receipt_skip_reason}</span>
                               </div>
                           </div> --%>
                           
                           
                       </div>
                   
                       </div>
                   </div>
                   </div>
            
            
            <div class="search-result news-list content-box">
                    <div class="news-container clearfix" id="fileList">
                		<c:forEach var="fileData" items="${allocationFileList}" varStatus="status">
							<%-- <a target="_blank" href="/files/allocation${fileData.allocation_file_path }"> --%>
								<img style="width:45%; height:30%; margin-top:10px; margin-left:10px;" id="${fileData.allocation_file_id}" onclick="javascript:setMod('${fileData.allocation_file_id}',this);" src="/files/allocation${fileData.allocation_file_path }" alt="${fileData.allocation_file_nm}" title="${fileData.allocation_file_nm}" />
							<!-- </a> -->
						</c:forEach>
                    </div>
                </div>
                
                <div class="menu-container" style="width:100%; text-align:center;">
                
                	<c:if test="${allocationList[0].allocation_status_cd == 'R'}">
                		<c:if test="${fn:length(allocationFileList) >=  allocationList[0].require_pic_cnt || allocationList[0].dept == '현대캐피탈' || allocationList[0].customer_id == 'CUSd5443753dd1245dd88737ae5a016072e'}">
		             		<a class="kakao txt-bold" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:allocationAccept('${allocationIdArr}','${allocationList[0].allocation_status_cd}');">
		                   		<c:if test="${allocationList[0].allocation_status_cd == 'R'}">상차완료</c:if>
		               		</a>
		               		<a class="kakao txt-bold" id="addPicStatusR" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:carCheck('${allocationIdArr}','${allocationList[0].allocation_status_cd}');">
		                   		사진추가
		               		</a>
	               		</c:if>
	               		<c:if test="${fn:length(allocationFileList) < allocationList[0].require_pic_cnt && allocationList[0].dept != '현대캐피탈' && allocationList[0].customer_id != 'CUSd5443753dd1245dd88737ae5a016072e'}">
		               		<a class="kakao txt-bold" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:carCheck('${allocationIdArr}','${allocationList[0].allocation_status_cd}');">
		                   		사진추가
		               		</a>
	               		</c:if>
                	</c:if>
                	<c:if test="${allocationList[0].allocation_status_cd == 'D'}">
                		<c:if test="${fn:length(allocationFileList) >=  allocationList[0].require_dn_pic_cnt || allocationList[0].dept == '현대캐피탈' || allocationList[0].customer_id == 'CUSd5443753dd1245dd88737ae5a016072e'}">
		             		<a class="kakao txt-bold" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:allocationAccept('${allocationIdArr}','${allocationList[0].allocation_status_cd}');">
		                   		<c:if test="${allocationList[0].allocation_status_cd == 'D'}">하차완료</c:if>
		               		</a>
		               		<a class="kakao txt-bold" id="addPicStatusD" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:carCheck('${allocationIdArr}','${allocationList[0].allocation_status_cd}');">
		                   		사진추가
		               		</a>
	               		</c:if>
	               		<c:if test="${fn:length(allocationFileList) < allocationList[0].require_dn_pic_cnt && allocationList[0].dept != '현대캐피탈' && allocationList[0].customer_id != 'CUSd5443753dd1245dd88737ae5a016072e'}">
		               		<a class="kakao txt-bold" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:carCheck('${allocationIdArr}','${allocationList[0].allocation_status_cd}');">
		                   		사진추가
		               		</a>
	               		</c:if>
                	</c:if>
            	</div>
                
                <form id="excelForm" style="display:none" name="excelForm" method="post" enctype="multipart/form-data">
					<input type="file" name="bbsFile"  multiple id="upload" style="display:none; width:300px;" onchange="javascript:fileUpload('${allocationIdArr}','${allocationList[0].allocation_status_cd}',this);">
				</form>
                
                
                
                
        </div>


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      <script src="/js/vendor/bootstrap.min.js"></script>       
    <!-- veiwport for countnumber -->
    <script src="/js/alert.js"></script> 
    <script src="/js/vendor/jquery.form.min.js"></script>
    <script src="/js/main.js"></script>       
    <script src="/js/vendor/jquery.loading-indicator.min.js"></script>
    
    
    
    
    <script>
    
    
    
    
    
    
	var homeLoader;
	
	$(document).ready(function(){
			
		
				homeLoader = $('body').loadingIndicator({
					useImage: false,
					showOnInit : false
				}).data("loadingIndicator");
				
				/* setTimeout(function() {
					homeLoader.show();
					homeLoader.hide();
				}, 1000); */
		
	});
    
    
    
    
    
    
    </script>
    
    
    
    
    
    </body>
</html>
