<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko" style="height:100%;">      
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
<body style="height:100%; text-align: center;">

<script type="text/javascript">  
  
$(document).ready(function(){  

	try {
		if(window.carrierJrpr != null){
			window.carrierJrpr.loginForApp("${kakaoId}", "");
		}
		if(window.external != null){
	        window.external.loginForPC("${kakaoId}", "");
	    }
	}catch(exception){
	    
	}finally {
		
	}
	
	
	
  
});



function logout(){

	if(confirm("로그아웃 하시겠습니까?")){
		
		try {
			if(window.carrierJrpr != null){
				window.carrierJrpr.logoutForApp("");
			}
			if(window.external != null){
	            window.external.logoutForPC("");
	        }
		}catch(exception){
		    
		}finally {
			document.location.href = "/kakao/logout.do";	
		}
	}
}
    

function loginForApp(userId){
	window.carrierJrpr.loginForApp(userId, "");
}



function logoutForApp(){
	window.carrierJrpr.logoutForApp("");
}    
    
    
    
function loginProcess(){
	
	if($("#loginId").val() == ""){
		//alert("아이디가 입력 되지 않았습니다.");
		//return;
		$.alert("아이디가 입력 되지 않았습니다.",function(a){
	    });
		return;
	}
	if($("#loginPwd").val() == ""){
		//alert("패스워드가 입력 되지 않았습니다.");
		//return;
		$.alert("패스워드가 입력 되지 않았습니다.",function(a){
	    });
		return;
	}
	
	$.ajax({ 
        type: 'post' ,
        url : '/loginPersist.do' ,
        dataType : 'json' ,
        async : false,
        data : $('#loginForm').serialize(), 
        
        success : function(jsonData, textStatus, jqXHR) {
        	if(jsonData.resultCode =="0000"){
            	goMainPage();
            	/* $("#myphoto_path").css("background-image","url(/files/member"+ data.resultData.photo_path + ")"); */
        	}else if(jsonData.resultCode =="E001"){
        		//alert("로그인에 실패 하였습니다. 관리자에게 문의 하세요.");
        		$.alert("로그인에 실패 하였습니다. 관리자에게 문의 하세요.",function(a){
        			
        	    });
        	}
        } ,
        error : function(xhRequest, ErrorText, thrownError) {
          //  alertEx("시스템 에러");
        }
    }); 
}

function goMainPage(){
	document.location.href = "/carrier/main.do";
}
    
    
function changePassWord(){


	if($("#loginId").val() == ""){
		$.alert("아이디가 입력되지 않았습니다.",function(a){
		});
		return false;
	}	
	 if($("#loginPwd").val() == ""){
		 $.alert("패스워드가 입력되지 않았습니다.",function(a){
		});
		return false;
    }
 
    var pw = $("#loginPwd").val();
    var num = pw.search(/[0-9]/g);
    var eng = pw.search(/[a-z]/ig);
    var spe = pw.search(/[`~!@@#$%^&*|₩₩₩'₩";:₩/?]/gi);

    if(pw.length < 6 || pw.length > 20){
    	$.alert("패스워드는 6자리 이상이어야 합니다.",function(a){
    	});
     return false;
    }
    if(pw.search(/₩s/) != -1){
    	$.alert("패스워드는 공백업이 입력해주세요.",function(a){
		});
   	  return false;
   	 }
    if(num < 0 || eng < 0 || spe < 0 ){
    	$.alert("영문,숫자, 특수문자를 혼합하여 입력해주세요.",function(a){
		});
      return false;
     }
    
    if($("#driverPwdChk").val() == ""){
    	$.alert("패스워드확인이 입력되지 않았습니다.",function(a){
		});
		return false;
    }
    
    if($("#loginPwd").val() != $("#driverPwdChk").val()){
    	$.alert("패스워드와 패스워드 확인에 입력 한 값이 다릅니다.",function(a){
		});
		return false;
    }
	
	$.ajax({ 
		type: 'post' ,
		url : "/updateDriverPassword.do" ,
		dataType : 'json' ,
		async : false,
		data : {
			driverId : $("#loginId").val(),
			driverPwd : $("#loginPwd").val()
		},
		success : function(data, textStatus, jqXHR)
		{
			var result = data.resultCode;
			var resultData = data.resultData;
			var resultMsg = data.resultMsg;
			
			if(result == "0000"){
				$.alert("패스워드가 변경 되었습니다.",function(a){
					if(a){
						document.location.href = "/carrier/login-page.do";
					}
				});
			}else if(result == "E000"){
				$.alert(resultMsg,function(a){
				});
			}else{
				
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
			
		}
	});
	
	
}
 


function backKeyController(str){
	history.go(-1);	
} 
 
    
</script>



		<div class="content-container">
            <!-- <header class="bg-pink clearfix"> -->
                <!-- <div class="search-icon">
                    <a href="#"></a> 
                </div> -->
                <!-- <div class="" style="width:70%; text-align:center;">
                	<img style="width:100%; height:75%;" src="/img/main_logo.png" alt="">
                </div> -->
                <!-- <div class="menu-bar pull-right">
                    <a style="cursor:pointer;" href="/adviser/adviser.do"><img src="/img/close-white-icon.png" alt=""></a>
                </div> -->
            <!-- </header> -->
            
            <div style=" display:table; margin:0 auto; display:inline-block; width:60%; height:100%;">
            
                	<img style="width:100%; height:75%; margin-top:70%;" src="/img/main_logo.png" alt="">
                
            <form action="" id="loginForm" style="margin-top:40%;">
                        <div class="username d-table">
                            <span class="d-tbc" style="font-weight:bold;">로그인 아이디</span>
                            <div style="margin-top:5%;">
                                <input type="text" placeholder="아이디를 입력해 주세요" class="d-tbc" id="loginId" name="driverId">
                            </div>
                        </div>
                        <div class="password d-table" style="margin-top:10%;">
                            <span class="d-tbc" style="font-weight:bold;">신규 패스워드</span>
                            <div style="margin-top:5%;">
                          	     <input type="password" placeholder="패스워드를 입력해 주세요" class="d-tbc" id="loginPwd" name="driverPwd" >
                            </div>
                        </div>
                        <div class="password d-table" style="margin-top:10%;">
                            <span class="d-tbc" style="font-weight:bold;">신규 패스워드 확인</span>
                            <div style="margin-top:5%;">
                          	     <input type="password" placeholder="패스워드를 입력해 주세요" class="d-tbc" id="driverPwdChk" name="driverPwdChk" >
                            </div>
                        </div>
                       </form>
                       <div class="login-btn-container d-table">
                           <span class="d-tbc"></span>
                           <div style="margin-top:10%; text-align:center;">
                           		<!-- <div style="width:30%; border:3px solid #eee;"> -->
                               	<a style="cursor:pointer;" onclick="javascript:changePassWord();" class="login-btn d-tbc">비밀번호 변경</a>&nbsp;/
                               	<a style="cursor:pointer;" href="/carrier/login-page.do" class="login-btn d-tbc">취소</a>
                               	<!-- </div> -->
                           </div>
                       </div>
                   </div>    
                       
       	</div>




    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      <script src="/js/vendor/bootstrap.min.js"></script>       
    <!-- veiwport for countnumber -->
    <script src="/js/jquery.viewportchecker.js"></script>    
    <script src="/js/alert.js"></script>
    <script src="/js/main.js"></script> 
</body>
</html>
