<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko"  style=" height:100%;">      
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
    
<body style=" height:100%;">


<script type="text/javascript">  


var isPressed = false;
var mode = "normal";
$(document).ready(function(){
	
/* 	var updateToken = setInterval( function() {
		
		if(getDeviceToken() != null && getDeviceToken() != ""){
			updateDriverDeviceToken(getDeviceToken());
			if(window.Android != null){
				window.Android.startService();
			}
			clearInterval(updateToken);
		}
   }, 1000); */
	
	
	$(".mod").css("display","none");
		
});








function changeMode(){
	
	
	$('input:checkbox[name="forLotte"]').each(function(index,element) {
		$(this).prop("checked","");
	 });
	
	if(mode == "normal"){
		mode = "mod";
		$(".mod").css("display","inline-block");
		$(".nomod").css("display","none");
		//$('#container').data('plugin_xpull').options.paused = true;
	}else{
		mode = "normal";
		$(".mod").css("display","none");
		$(".nomod").css("display","");
		//$('#container').data('plugin_xpull').options.paused = false;
	}
	
}



function getDeviceToken(){
	
	var token = "";
	try{
		if(window.Android != null){
			token = window.Android.getDeviceToken("${driverId}");
		}	
	}catch(exception){
		
	}finally{
		
	}
	return token;
}  
  

function updateDriverDeviceToken(token){
	
	
	if(token != ""){
		$.ajax({ 
			type: 'post' ,
			url : "/updateDriverDeviceToken.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				token : token
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					//$.alert("성공",function(a){
					//});
				}else if(result == "E000"){
					
				}else{
					
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
}  



 function getNewsData(obj,keyword){
	
	 
	// alert($("#selectedStatus").children().length);
	 
	//alert($("#selectedStatus").children().first().get(0));	
	//alert($(obj).parent().get(0));
	
 	 if($("#selectedStatus").children().first().get(0) == $(obj).parent().get(0)){				//전체를 클릭 한경우
					
 		$("#selectedStatus").children().each(function(index,element){
 			$(this).removeClass('selected');
 		});
 		$(obj).parent().addClass('selected');
	}else{
			
	} 
	
	
	
/* 	
	
	$.ajax({ 
		type: 'post' ,
		url : "/research/newsSearch.do?&keyword="+keyword ,
		dataType : 'json' ,
		data : {
			
		},
		success : function(data, textStatus, jqXHR)
		{
			alert(data);
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
	 */
	
	
} 

 function goNewsPage(uri){
		
		//var replaceUrlStr = replaceAll(url, "?", "^");
		//replaceUrlStr = replaceAll(replaceUrlStr, "=", "+");
		
		var replaceUrlStr = encodeURIComponent(uri)
		document.location.href = "/news/newsPage.do?newsUri="+replaceUrlStr;
	}  


    
function viewAllocationData(allocationId,allocationStatus){
	document.location.href = "/carrier/allocation-detail-forGroup.do?groupId=${paramMap.groupId}&allocationStatus="+allocationStatus+"&allocationId="+allocationId;
} 
 
 
function viewAllocationList(allocationId,allocationStatus){
	
	document.location.href = "/carrier/group-list.do?allocationId="+allocationId;
	
} 
 
 
 
function logout(){
	
	$.confirm("로그아웃 하시겠습니까?",function(a){
		 if(a){
			 if(window.Android != null){
					window.Android.stopService("${driverId}");
				}	
			 document.location.href = "/logout.do";	
		 }
	});
	
}   
 
 
 
function backKeyController(str){
	history.go(-1);
	//document.location.href="/carrier/list.do?allocationStatus=${paramMap.allocationStatus}";
}
 
function checkAll(){

	if($('input:checkbox[id="checkAll"]').is(":checked")){
		$('input:checkbox[name="forLotte"]').each(function(index,element) {
			$(this).prop("checked","true");
		 });	
	}else{
		$('input:checkbox[name="forLotte"]').each(function(index,element) {
			$(this).prop("checked","");
		 });
	}
}


var allocationIdArr = new Array();
 function makeIdArr(id,status){
	 
	if(mode =="normal"){
		var idArr = "";
		for(var i = 0; i < allocationIdArr.length; i++){
			 idArr += allocationIdArr[i];
			 if(i<allocationIdArr.length-1){
				 idArr += ","; 
		     }
		 }
		 allocationAccept(idArr,status);	
	}else{
		var total = $('input:checkbox[name="forLotte"]:checked').length;
		if(total == 0){
			$.alert("목록이 선택 되지 않았습니다.",function(a){
			});
			 return false;
		 }else{
			var idArr = "";	 
			 $('input:checkbox[name="forLotte"]:checked').each(function(index,element){
				 idArr += $(this).attr("allocationId");
				 if(index < total-1){
					 idArr += ",";
				 }
			});	 
		 }
		allocationAccept(idArr,status);
	}
	 
 }



 
 
 

 
 var selectAllocationStatus = "";
 var selectLotteStatus = false;
 function forShowButton(obj){
	 
//	 alert(test);
	 
	 
//	 alert($(obj).attr("carKind"));
//	 alert($(obj).attr("carNum"));
//	 alert($(obj).attr("carIdNum"));
	 
	 
	 if($(obj).attr("carKind") == ""){
		$.alert("차종이 작성 되지 않았습니다.",function(a){
		});
		$(obj).prop("checked","");
		return false;
	 }
	 
	 if($(obj).attr("carNum") == "" && $(obj).attr("carIdNum") == ""){
			$.alert("차대번호 또는 차량번호 둘 중 하나는 반드시 작성 되어야 합니다.",function(a){
			});
			$(obj).prop("checked","");
			return false;
	 }
	 
	 var total = $('input:checkbox[name="forLotte"]:checked').length;
	 
	 if(total == 0){
		 $("#allocationStatusY").css("display","none");
		 $("#allocationStatusB").css("display","none");
		 $("#allocationStatusP").css("display","none");
		 $("#allocationStatusS").css("display","none");
		 $("#allocationStatusSNL").css("display","none");
	 }else if(total == 1){
		 if($(obj).next().val() == "롯데" || $(obj).next().next().val() == "현대캐피탈" || $(obj).attr("customerId") == "CUSd5443753dd1245dd88737ae5a016072e"){
			 selectLotteStatus = true;
		 }else{
			 selectLotteStatus = false;
		 }
		 
		 selectAllocationStatus = $(obj).attr("allocationStatus");
		 
		 //여기서 롯데와 롯데 아닌거 선택시 버튼구분 하기....
		 if(selectLotteStatus){
			 if(selectAllocationStatus == "Y" || selectAllocationStatus == "R"){
				 $("#allocationStatusY").css("display","inline-block");	 
			 }else if(selectAllocationStatus == "B" || selectAllocationStatus == "I"){
				 $("#allocationStatusP").css("display","inline-block");
			 }else if(selectAllocationStatus == "P"){
				 $("#allocationStatusB").css("display","inline-block");
			 }else if(selectAllocationStatus == "S" || selectAllocationStatus == "D"){
				 $("#allocationStatusS").css("display","inline-block");
			 }	 
		 }else{
			 if(selectAllocationStatus == "Y" || selectAllocationStatus == "R"){
				 $("#allocationStatusY").css("display","inline-block");	 
			 }else if(selectAllocationStatus == "B" || selectAllocationStatus == "I"){
				 $("#allocationStatusP").css("display","inline-block");
			 }else if(selectAllocationStatus == "P"){
				 $("#allocationStatusB").css("display","inline-block");
			 }else if(selectAllocationStatus == "S" || selectAllocationStatus == "D"){
				 /* $("#allocationStatusS").css("display","inline-block"); */
				 $("#allocationStatusSNL").css("display","inline-block");
			 }
		 }
		 
		 
		 
		 
		 
		 
	 }else{
		 
		 
		 if($(obj).next().val() != "롯데" || $(obj).next().next().val() != "현대캐피탈" || $(obj).attr("customerId") != "CUSd5443753dd1245dd88737ae5a016072e"){
			 selectLotteStatus = false;
		 }
		 
		 if(selectAllocationStatus != $(obj).attr("allocationStatus")){			
			$.alert("탁송상태를 확인 해 주세요. 같은 상태의 배차건만 진행 가능 합니다.",function(a){
			});
			 $(obj).prop("checked","");
		 }
		 
	 }
	 
 }
 
 
 
 
 
 function makeIdArrForUpLift(id,status){
	 
	 var msg = "";
	 
	 if(status == "S"){
		 msg = "상차검사";
	 }else if(status == "B"){
		 msg = "하차검사";
	 }else if(status == "R"){
		 msg = "상차검사";
	 }else if(status == "D"){
		 msg = "하차검사";
	 }else if(status == "P"){
		 msg = "인계";
	 }
	 
	 
	 
	 var total = $('input:checkbox[name="forLotte"]:checked').length;
	 
	 if(total == 0){
		$.alert("목록이 선택 되지 않았습니다.",function(a){
		});
		 return false;
	 }else{
		var idArr = "";	 
		var selectStatus = true;
		 $('input:checkbox[name="forLotte"]:checked').each(function(index,element){
			 idArr += $(this).attr("allocationId");
			 if($(this).next().val() != "롯데" && $(this).next().next().val() != "현대캐피탈" && $(this).attr("customerId") == "CUSd5443753dd1245dd88737ae5a016072e"){
				 
				 selectStatus = false;
			 }
			 if(index < total-1){
				 idArr += ",";
			 }
		});	 
	 }
	 
	 if(selectStatus){
		 allocationAccept(idArr,status)
	 }else{
		 /* $.alert("해당 동작은 롯데건만 가능 합니다.",function(a){
		}); */
		if(status == "S" || status == "B" || status == "P"){
			if(status == "S"){
				status = "R";
			}else if(status == "B"){
				status = "D";
			}else if(status == "P"){
				status = "I";
			}
			$.confirm("총 "+total+"건의 "+msg+"를 진행 하시겠습니까?",function(a){
				 if(a){
					 updateAllocationStatus(idArr,status);
					 
					 if(status != "I"){
						 document.location.href = "/carrier/lift-check-batch-forGroup.do?&batchIndex=1&allocationId="+idArr+"&allocationStatus="+status;	 
					 }else{
						 
						 document.location.href = "/carrier/car-check-detail-batch-forGroup.do?&batchIndex=1&allocationId="+idArr+"&allocationStatus="+status;
						 
						 
						 
					 }
					 
					 
					 
				 }else{
					 $.alert("취소 되었습니다.",function(b){
						});
				 }
			});
			
		}else{
			$.alert("해당 동작은 롯데,현대캐피탈,주성네트웍스 배차건만 가능 합니다.",function(a){
			});
		}
		 
		
		
		
	 }
	 
	 
 }
 
 
 function updateAllocationStatus(id,status){
	 

	 var msg = "";
		if(status == "Y"){
			msg = "승인";
		}else if(status == "S"){
			msg = "상차";
			addMsg = "(상차 이후에는 차량정보 '차종,차대번호,차량번호' 를 변경 할 수 없습니다.)";
		}else if(status == "B"){
			msg = "하차";
		}else if(status == "P"){
			msg = "인계";
		}else if(status == "F"){
			msg = "완료";
		}
	 
	 $.ajax({ 
			type: 'post' ,
			url : "/allocation/updateAllocationStatus.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				allocationStatus : status,
				allocationId : id
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					
				}else if(result == "E000"){
					$.alert("해당 배차를 "+msg+" 할 수 없습니다.",function(a){
						document.location.href = "/carrier/list.do?allocationStatus=${allocationStatus}";
						//window.location.reload();
					});
				}else{
					$.alert("해당 배차를 "+msg+" 하는데 실패 했습니다. 관리자에게 문의 하세요.",function(a){
						document.location.href = "/carrier/list.do?allocationStatus=${allocationStatus}";
						//window.location.reload();
					});
				}
				
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
	 
	 
	 
 }
 
 
 
 
 
function allocationAccept(id,status){
	//alert(allocationId);
	var msg = "";
	var addMsg = "";
	if(status == "Y"){
		msg = "승인";
	}else if(status == "S"){
		msg = "상차";
		addMsg = "(상차 이후에는 차량정보 '차종,차대번호,차량번호' 를 변경 할 수 없습니다.)";
	}else if(status == "B"){
		msg = "하차";
	}else if(status == "P"){
		msg = "인계";
	}else if(status == "F"){
		msg = "완료";
	}
	
	
	
	
	$.confirm("해당 배차를 일괄"+msg+" 하시겠습니까?"+addMsg,function(a){
   		 if(a){
   			$.ajax({ 
				type: 'post' ,
				url : "/allocation/updateAllocationStatus.do" ,
				dataType : 'json' ,
				async : false,
				data : {
					allocationStatus : status,
					allocationId : id,
					forLotte : "Y"
				},
				success : function(data, textStatus, jqXHR)
				{
					var result = data.resultCode;
					var resultData = data.resultData;
					if(result == "0000"){
						//alert(msg+"되었습니다.");
						$.alert(msg+"되었습니다.",function(a){
								document.location.href = "/carrier/list.do?allocationStatus=${allocationStatus}";
							//document.location.href = "/carrier/main.do";
							//window.location.reload();
						});
					}else if(result == "E000"){
						$.alert("해당 배차를 "+msg+" 할 수 없습니다.",function(a){
							document.location.href = "/carrier/list.do?allocationStatus=${allocationStatus}";
							//window.location.reload();
						});
					}else{
						$.alert("해당 배차를 "+msg+" 하는데 실패 했습니다. 관리자에게 문의 하세요.",function(a){
							document.location.href = "/carrier/list.do?allocationStatus=${allocationStatus}";
							//window.location.reload();
						});
					}
					
				} ,
				error : function(xhRequest, ErrorText, thrownError) {
				}
			});	
   		 }
   	});
	
}
 
 
 
 
 
 
 
</script>


        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div class="content-container" style=" height:100%; overflow-y:hidden; position:relative;">
            <%-- <jsp:include page="/WEB-INF/views/jsp/common-top.jsp"></jsp:include> --%>
            <header class="bg-pink clearfix" style="position:fixed; background-color:#fff; border:none; text-align:center; ">
                <div class="" style="width:19%;">
                    <a href="javascript:history.go(-1);"><img style="zoom: 0.5;" src="/img/back-icon.png" alt=""></a> 
                </div> 
                <div class="" style="width:60%;">
                	<!-- <img style="width:90%; height:60%;" src="/img/main_logo.png" alt=""> -->
                	<img style="width:90%; height:60%;" onclick="javascript:document.location.href='/carrier/main.do'" src="/img/main_logo.png" alt="">
                </div>
                <div class="" style="width:19%; float:right;">
                   <a style="cursor:pointer;" href="javascript:logout();"><img style="" src="/img/logout.png" alt=""></a>
                </div>
            </header>
            <%-- <div style="text-align:right;">${user.driver_name}님 환영 합니다.</div> --%>
            <div style=" text-align:right; clear:both;">&nbsp;</div>
            <div class="content-container interest-page loadingthemepage news" style="position:fixed; background-color:#fff; clear:both; margin-top:45px;">
	            <!-- <div class="cat-list" style="background-color:#fff">
	                <ul class="clearfix" style="background-color:#fff">
	                	<li><a style="color:#000; width:15%;">출발일</a></li>
	                    <li><a style="color:#000;">상차지</a></li>
	                    <li><a style="color:#000;">하차지</a></li>
	                    <li><a style="color:#000;">차종</a></li>
	                    <li><a style="color:#000;">차대번호</a></li>
	                </ul>
	            </div> -->
	            <div style="width:100%; margin-top:5px; height:50px; border-bottom:1px solid #eee;">
                   <div style="width:15%; text-align:center; height:40px; float:left;  margin-top:-8px;" onclick="javascript:changeMode();">
                   		<div class="nomod" style="margin-top:10px; text-align:center; display:inline-block;">출발일<p style="color:#8B0000">(선택)</p></div>
                   		<div class="mod" style="margin-top:10px; text-align:center; display:inline-block;   margin-top:20px;">선택<!-- <input style="transform : scale(1.5);"  type="checkbox" id="checkAll" onclick="javascript:checkAll();" > --></div>
                   	</div>
	               <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">상차지</div></div>
	               <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">하차지</div></div>
	               <div style="width:27%; text-align:center; height:40px; float:left; margin-top:-8px;"><div style="margin-top:10px; text-align:center; display:inline-block;">차종<p style="color:#8B0000">(차량정보)</p></div></div>
	               <div style="width:18%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">상태</div></div>
                </div>
            </div>
            <div id="container" class="search-result news-list content-box" style="height:100%; margin-top:25%; overflow-y:scroll;">
            	<div class="xpull">
				    <div class="xpull__start-msg">
				        <div class="xpull__start-msg-text">화면을 당겼다 놓으면 새로 고침 됩니다.</div>
				        <div class="xpull__arrow"></div>
				    </div>
				    <div class="xpull__spinner">
				        <div class="xpull__spinner-circle"></div>
				    </div>
				</div>
            	<div class="news-container clearfix" style="height:450%;">
    	       		<c:forEach var="data" items="${listData}" varStatus="status">
    	       			<script> 
    	       			allocationIdArr.push('${data.allocation_id}');
    	       			</script>
			           	<div style="width:100%; margin-top:5px; height:45px; border-bottom:1px solid #eee;">
					        <div style="width:15%; text-align:center; height:50px; float:left;">
					        	<div class="nomod" style="margin-top:10px; text-align:center; display:inline-block;">${data.departure_dt_srt_str}</div>
					        	<div class="mod" style="display:none; margin-top:10px; text-align:center; display:inline-block;">
					        		<input onclick="javascript:forShowButton(this);" allocationId="${data.allocation_id}" carKind="${data.car_kind}" carNum="${data.car_num}" carIdNum="${data.car_id_num}" allocationStatus="${data.allocation_status_cd}" name="forLotte" style="transform : scale(1.5);" type="checkbox"/>
					        		<input name="check" type="hidden" value="${data.department}" />
					        		<input name="check" type="hidden" value="${data.dept}" />
					        	</div>
					        </div>
						    <div style="width:20%; text-align:center; height:50px; float:left;"   onclick="javascript:viewAllocationData('${data.allocation_id}','${data.allocation_status_cd}');"><div style="width:100%; margin-top:10px; text-align:center; display:inline-block; text-overflow:ellipsis; white-space:nowrap; overflow:hidden;"><nobr>${data.departure}</nobr></div></div>
						    <div style="width:20%; text-align:center; height:50px; float:left;"   onclick="javascript:viewAllocationData('${data.allocation_id}','${data.allocation_status_cd}');"><div style="width:100%; margin-top:10px; text-align:center; display:inline-block; text-overflow:ellipsis; white-space:nowrap; overflow:hidden;"><nobr>${data.arrival}</nobr></div></div>
						    <div style="width:27%; text-align:center; height:50px; float:left;"   onclick="javascript:viewAllocationData('${data.allocation_id}','${data.allocation_status_cd}');">
						    <div style="width:100%; <c:if test="${data.car_num == '' && data.car_id_num == ''}">margin-top:10px;</c:if><c:if test="${data.car_num != '' || data.car_id_num != ''}">margin-top:0px;</c:if> text-align:center; display:inline-block; text-overflow:ellipsis; white-space:nowrap; overflow:hidden;">
						    <nobr>${data.car_kind}</nobr>
						    <c:if test="${data.car_num != ''}"><p  style="color:#8B0000">(${data.car_num})</p></c:if>
						    <c:if test="${data.car_num == ''}"><p  style="color:#8B0000">(${data.car_id_num})</p></c:if>
						    </div></div>
						    <div style="width:18%; text-align:center; height:50px; float:left;"   onclick="javascript:viewAllocationData('${data.allocation_id}','${data.allocation_status_cd}');"><div style="margin-top:10px; text-align:center; display:inline-block;">${data.allocation_status_sub}</div></div>
			            </div>
	                </c:forEach>
	                <div class="menu-container" style="width:100%; text-align:center; clear:both;">
	                	<c:if test="${paramMap.allocationStatus == 'A'}">
			                 <%-- <a class="kakao txt-bold" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:makeIdArr('${paramMap.allocationId}','Y');"> --%>
			                 <a class="kakao txt-bold" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:makeIdArr('${paramMap.allocationId}','Y');">
			                    	일괄승인
			                </a>
	                	</c:if>
	                	<%-- <c:if test="${paramMap.allocationStatus == 'Y'}"> --%>
		                	<a class="kakao txt-bold" id="allocationStatusY" style="width:30%; display:none; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:makeIdArrForUpLift('${paramMap.allocationId}','S');">
				                  	일괄상차
				            </a>
				            <a class="kakao txt-bold" id="allocationStatusB" style="width:30%; display:none; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:makeIdArrForUpLift('${paramMap.allocationId}','B');">
				                  	일괄하차
				            </a>
				            <a class="kakao txt-bold" id="allocationStatusP" style="width:30%; display:none; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:makeIdArrForUpLift('${paramMap.allocationId}','P');">
				                  	일괄인계
				            </a>
				            <a class="kakao txt-bold" id="allocationStatusSNL" style="width:30%; display:none; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:makeIdArrForUpLift('${paramMap.allocationId}','B');">
				                  	일괄하차
				            </a>
				            <a class="kakao txt-bold" id="allocationStatusS" style="width:40%; display:none; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:makeIdArrForUpLift('${paramMap.allocationId}','F');">
				                  	일괄하차 및 인계
				            </a>
	                	<%-- </c:if> --%>
	                	
	                	
                	<%-- <c:if test="${allocationList[0].allocation_status_cd == 'Y'}">
	                	<a class="kakao txt-bold" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:carCheck('${allocationList[0].allocation_id}','${allocationList[0].allocation_status_cd}');">
			                 	상차검사
			            </a>
                	</c:if>	
                	<c:if test="${allocationList[0].allocation_status_cd == 'R' or allocationList[0].allocation_status_cd == 'D'}">
	                	<a class="kakao txt-bold" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:goLiftCheckPage('${allocationList[0].allocation_id}','${allocationList[0].allocation_status_cd}');">
			                   	검사진행중
			            </a>
                	</c:if>
                	<c:if test="${allocationList[0].allocation_status_cd == 'S'}">
                		<a class="kakao txt-bold" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:carCheck('${allocationList[0].allocation_id}','${allocationList[0].allocation_status_cd}');">
			                 하차검사
			            </a>
	                	<a class="kakao txt-bold" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:carCheckDetail('${allocationList[0].allocation_id}','${allocationList[0].allocation_status_cd}');">
			              	인계진행
			            </a>
                	</c:if>
                	<c:if test="${allocationList[0].allocation_status_cd == 'I'}">
	                	<a class="kakao txt-bold" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:carCheckDetail('${allocationList[0].allocation_id}','${allocationList[0].allocation_status_cd}');">
			              	인계진행중
			            </a>
                	</c:if>
                	<c:if test="${allocationList[0].allocation_status_cd == 'P'}">
	                	<a class="kakao txt-bold" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:carCheck('${allocationList[0].allocation_id}','${allocationList[0].allocation_status_cd}');">
			                 	하차검사
			            </a>
                	</c:if>
                	<c:if test="${allocationList[0].allocation_status_cd == 'B'}">
	                	<a class="kakao txt-bold" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:carCheckDetail('${allocationList[0].allocation_id}','${allocationList[0].allocation_status_cd}');">
			                 	인계진행
			            </a>
			            <a class="kakao txt-bold" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:carCheckDetailPass('${allocationList[0].allocation_id}','${allocationList[0].allocation_status_cd}');">
			                 	인계생략
			            </a>
                	</c:if>  --%>   
            	</div>
            	
            	
            	
            	
                 </div>
                 
            </div>
            
            
            
            
            
            
            
            
            
		            
		<div style="position:absolute; bottom:0; width:100%; height:7%;"><p style="color:#8B0000">*출발일(선택)을 누르면 체크박스가 표시 됩니다.</p></div>            
        </div>





			










    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      <script src="/js/vendor/bootstrap.min.js"></script>       
    <!-- veiwport for countnumber -->
    <script src="/js/alert.js"></script>
    <script src="/js/main.js"></script>   
        <script src="/js/vendor/xpull.js"></script>       
    
    
    <script>
    
	
	$('#container').xpull({
	    'paused': false, 
	    // Pull threshold - amount in  pixels required to pull to enable release callback
        'pullThreshold':50,
        // Max pull down element - amount in pixels
        maxPullThreshold: 50,
        // timeout in miliseconds after which the loading indicator stops spinning.
        // If set to 0 - the loading will be indefinite
        'spinnerTimeout':1000,
        onPullStart: function(){},
        onPullEnd: function(){},
        callback: function(){window.location.reload()},

	});   
    
    </script>
        
    </body>
</html>
