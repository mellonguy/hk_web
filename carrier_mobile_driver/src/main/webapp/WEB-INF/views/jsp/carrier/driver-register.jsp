<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko"  style=" height:100%;">      
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
<body style=" height:100%;">


<script type="text/javascript">  

$(document).ready(function(){

	$('.modal-field').css('display','none');
	$('.modal-field2').css('display','none');
	
	/*	
	var updateToken = setInterval( function() {
		
 		if(getDeviceToken() != null && getDeviceToken() != ""){
			updateDriverDeviceToken(getDeviceToken());
			if(window.Android != null){
				window.Android.startService();
			}
			clearInterval(updateToken);
		}
   }, 1000);
 
 */


	
});



function getDeviceToken(){
	
	var token = "";
	try{
		if(window.Android != null){
			token = window.Android.getDeviceToken("${driverId}");
		}	
	}catch(exception){
		
	}finally{
		
	}
	return token;
}  
  
/* 
function updateDriverDeviceToken(token){
	
	
	if(token != ""){
		$.ajax({ 
			type: 'post' ,
			url : "/updateDriverDeviceToken.do" ,
			dataType : 'json' ,
			data : {
				token : token
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					//$.alert("성공",function(a){
					//});
				}else if(result == "E000"){
					
				}else{
					
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
}  
 */


	 

 function getNewsData(obj,keyword){
	
	 
	// alert($("#selectedStatus").children().length);
	 
	//alert($("#selectedStatus").children().first().get(0));	
	//alert($(obj).parent().get(0));
	
 	 if($("#selectedStatus").children().first().get(0) == $(obj).parent().get(0)){				//전체를 클릭 한경우
					
 		$("#selectedStatus").children().each(function(index,element){
 			$(this).removeClass('selected');
 		});
 		$(obj).parent().addClass('selected');
	}else{
			
	} 
	
	
	
/* 	
	
	$.ajax({ 
		type: 'post' ,
		url : "/research/newsSearch.do?&keyword="+keyword ,
		dataType : 'json' ,
		data : {
			
		},
		success : function(data, textStatus, jqXHR)
		{
			alert(data);
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
	 */
	
	
} 

 function goNewsPage(uri){
		
		//var replaceUrlStr = replaceAll(url, "?", "^");
		//replaceUrlStr = replaceAll(replaceUrlStr, "=", "+");
		
		var replaceUrlStr = encodeURIComponent(uri)
		document.location.href = "/news/newsPage.do?newsUri="+replaceUrlStr;
	}  


    
function viewAllocationData(allocationId,allocationStatus){
	homeLoader.show();
	document.location.href = "/carrier/allocation-detail.do?allocationStatus="+allocationStatus+"&allocationId="+allocationId;

	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
} 
 
 
function viewAllocationList(allocationId,allocationStatus){
	homeLoader.show();
	document.location.href = "/carrier/group-list.do?allocationId="+allocationId+"&allocationStatus="+allocationStatus;

	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
	
} 
 
 
 
function logout(){
	
	$.confirm("로그아웃 하시겠습니까?",function(a){
		 if(a){
			 if(window.Android != null){
					window.Android.stopService("${driverId}");
				}	
			 homeLoader.show();
			 document.location.href = "/logout.do";	
		 }
	});
	
}   








var selectedObj = new Object();


function selectInputId(){

	if($("#inputID").val() != ""){

		$.ajax({
			type : 'post',
			url : '/carrier/compareCustomerId.do',
			dataType : 'json',
			data : {

				inputID : $("#inputID").val(),
				
			},success : function(data, textStatus, jqXHR) {
		         var result = data.resultCode;
		         	
					if(result == "0000") {

						sendMessage();
					
						}else if (result == "0001") { 
						$.alert("입력한 아이디로 가입된 아이디가 없습니다.", function(a) {
	
					});	
				}  
			},
			error : function(xhRequest, ErrorText, thrownError) {
				
			}
		});


		}else{

			$.alert("아이디가 입력이 되지 않았습니다.");
			


			}
		
}


	selectedObj = new Object(); 

	function sendMessage(){


		selectedObj = new Object();

		
		$.confirm("등록하셨던 번호로 인증번호를 전송하시겠습니까?",function(a){

			 if(a){
					$(".certNum").css('display','block');	
					
					$.ajax({
						type : 'post',
						url : '/carrier/insertCertNumForAPP.do',
						dataType : 'json',
						data : {

							inputID : $("#inputID").val(),
							phoneNum : "",
							customerId : "",
							gubun : 'P',
							
						},success : function(data, textStatus, jqXHR) {
					         var result = data.resultCode;
					         	
								if(result == "0000") {

									$(".certNum").css('display','block');											
								
									}else if (result == "0002") { 
									$.alert("알 수 없는 오류입니다. 관리자에게 문의해주세요.", function(a) {
				
								});	
							}  
						},
						error : function(xhRequest, ErrorText, thrownError) {
							
						}
					});
					
				 
					

					 	
			 }else{

				/* $.alert("취소",function(a){
				}); */
			}
		});
	}
		

	
function sendPinNum(){


	var phoneCertNum = $("#phoneCertNum").val();

	
	if(phoneCertNum != ""){

				$.ajax({
					type : 'post',
					url  : "/carrier/checkCertNumForLogin.do",
				    dataType : 'json',
				    
				       data : {
					       
				    	inputID : $("#inputID").val(),
						phoneNum :  "",
						customerId : "",
						phoneCertNum : phoneCertNum,
						gubun : 'P',
				
						},

				   success : function(data, textStatus, jqXHR)
				    {
					   var resultCode = data.resultCode;
					   var resultData = data.resultData;
					   
					   if(resultCode == "0000"){
						 
							$.alert("인증에 성공하였습니다.", function(a) {
								

								
								$(".changePw").css('display','block');		
								
							});	
						 
					   }else{
						 $.alert("인증번호가 맞지 않습니다. 관리자에게 문의하세요.");
					   }
				   } ,
				  error :function(xhRequest, ErrorText, thrownError){
					  $.alert('알수 없는 오류입니다. 관리자에게 문의 하세요.');
	                  
					  }
				});
	
		}else{

			$.alert("인증번호가 입력되지 않았습니다.");
			return false;
			}
}



function findID(findId){


	document.location.href ="/carrier/sign-Up.do?&findId="+findId;
		
}




function goSingUpPage(){

	document.location.href='/carrier/simpleSign-Up.do?&customerId='+selectedObj.customerId+'&chargeId='+selectedObj.chargeId+'&phoneNum='+selectedObj.phoneNum;
	
}

 
function backKeyController(str){
	//history.go(-1);
	//location.href = document.referrer;
	document.location.href='/carrier/main.do';
}

function goChagnePassword(findId){

	document.location.href ="/carrier/sign-Up.do?&findId="+findId;
	
}

function goSignUpPage(){


	document.location.href='/carrier/sign-Up.do';
	
}


function newPasswordChange(){


	var newPassword = $("#newPassword").val();
	var confirmpw = $("#confirmpw").val();

	
	    var num = newPassword.search(/[0-9]/g);
	    var eng = newPassword.search(/[a-z]/ig);
	    var spe = newPassword.search(/[`~!@@#$%^&*|₩₩₩'₩";:₩/?]/gi);

	    

	    if(newPassword.length < 6 || newPassword.length > 20){
	     $.alert("패스워드는 6자리 이상이어야 합니다.");
	     
	     return false;
	    }
	    if(newPassword.search(/₩s/) != -1){
	        $.alert("패스워드는 공백없이 입력해주세요.");
	        return false;
	       }
	    if(num < 0 || eng < 0 || spe < 0 ){
	      $.alert("영문,숫자, 특수문자를 혼합하여 입력해주세요.");
	      return false;
	     }

	    
	 if(newPassword == confirmpw ){


				$.ajax({
					type : 'post',
					url  : "/carrier/confirmForPassWord.do",
				    dataType : 'json',
				    
				       data : {

						userId : $("#inputID").val(),
						newPassword : newPassword,
						
						},

				   success : function(data, textStatus, jqXHR)
				    {
					   var resultCode = data.resultCode;
					   if(resultCode == "0000"){

						   $.alert("비밀번호가 변경되었습니다.",function(a){
								if(a){
									goSignUpPage();
								}
							});
									
						}				

						   },
				  error :function(xhRequest, ErrorText, thrownError){
	                  $.alert(' @@@@error@@@@ ');
	                  
					  }

				});

			}else if(newPassword ==""){
				$.alert("입력창에 입력이 되지 않았습니다.");
		
			}else{
			$.alert("비밀번호가 같지 않습니다. 다시한번 확인해주세요");

				}
		}


var status ="";

function goWindowPopUp(id){


	status = id;

	window.open("/carrier/addressSearch.do", "a", "width=500, height=800, left=100, top=50"); 
	

	
}

function setAddress(value){

	$.alert(value);
	$.alert(status);

	$("#"+status).val(value);


	
}

 var idCheck ="N";


//중복아이디 체크 
function goCheckId(obj){


	var rtnStatus = true;
	var engNum = /^[a-z]+[a-z0-9]{5,15}$/g;
	
	var driverId = $("#driverId").val().replace(/ /g,"");
	
			if(driverId == ""){
				$.alert("아이디는 필수 입력 사항입니다.");
				rtnStatus = false;
			}else if(driverId.length < 5 || driverId.length > 15){
				 $.alert("아이디는 5자리 이상이어야 합니다.");
				 rtnStatus = false;
			}else if(!engNum.test(driverId)){
				  $.alert('형식에 맞지 않는 아이디입니다. 다시 입력해 주세요.');
				  rtnStatus = false;
			}else{
	
			$.ajax({ 
				type: 'post' ,
				url : "/carrier/driverIdCheck.do" ,
				dataType : 'json' ,
				data : {
					
					driverId : driverId,
				},
				success : function(data, textStatus, jqXHR)
				{
					var result = data.resultCode;
					var resultData = data.resultData;
					if(result == "0000"){
						
						$.alert("사용가능한 아이디입니다.");
						idCheck ="Y";
					}else if(result == "0001"){
		
						$.alert("중복된 아이디입니다. 다시 입력해주세요.");
						 return false;
		
								}
		
					} ,
				error : function(xhRequest, ErrorText, thrownError) {
				}
			});
				
}

	
}


function phoneChk(obj){

	
	var trans_num = $(obj).val().replace(/-/gi,'');
    if(trans_num != null && trans_num != ''){
        if(trans_num.length==11 || trans_num.length==10){   
       	 						var regExp_ctn = /^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})([0-9]{3,4})([0-9]{4})$/;
            if(regExp_ctn.test(trans_num)){
                trans_num = trans_num.replace(/^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})-?([0-9]{3,4})-?([0-9]{4})$/, "$1-$2-$3");                  
                //$(obj).val(trans_num);
                return "0";
            }else{
                return "1";
            }
        }else{
            return "2";
        }
  }


}


function pwdCheck(){


	 var pw = $("#driverPwd").val();
	 var num = pw.search(/[0-9]/g);
	 var eng = pw.search(/[a-z]/ig);
	 var spe = pw.search(/[`~!@@#$%^&*|₩₩₩'₩";:₩/?]/gi);

	 if(pw == ""){
		$.alert("비밀번호가 입력 되지 않았습니다.",function(a){
	 	});
	 	return false;

	 }
	 if(pw.length < 5 || pw.length > 20){
	 	$.alert("비밀번호는 5자리 이상이어야 합니다.",function(a){
	 	});
	 	return false;
	 }
	 if(pw.search(/₩s/) != -1){
	 	$.alert("비밀번호는 공백업이 입력해주세요.",function(a){
	 	});
	 	return false;
	 	 }
	 if(num < 0 || eng < 0 || spe < 0 ){
	 	$.alert("영문,숫자, 특수문자를 혼합하여 입력해주세요.",function(a){
	 	});
	 	return false;
	  }

	 if($("#driverPwdCheck").val() == ""){
	 	$.alert("비밀번호 확인이 입력되지 않았습니다.",function(a){
	 	});
	 	return false;
	 }

	 if($("#driverPwd").val() != $("#driverPwdCheck").val()){
	 	$.alert("비밀번호와 비밀번호 확인에 입력 한 값이 다릅니다.",function(a){
	 	});
	 	return false;
	 } 

	return true;
	
}







function goSendMessage(){



	$.ajax({ 
		type: 'post' ,
		url : "/carrier/driverSendPinNum.do" ,
		dataType : 'json' ,
		data : {
			
			phoneNum : $("#phoneNum").val(),

		},

		success : function(data, textStatus, jqXHR)
		{
			var result = data.resultCode;
			var resultData = data.resultData;
			if(result == "0000"){
				
				$.alert("인증번호를 전송했습니다. 인증번호를 입력해주세요");
					$("#certNumTable").css("display","block");
				
				
			}else if(result == "0001"){

				$.alert("알 수 없는 오류입니다. 관리자에게 문의해주세요.");
				 return false;

						}

			} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
		

	
}

var certNumType ="N";

function goSendCertNum(){



	$.ajax({ 
		type: 'post' ,
		url : "/carrier/driverCompareCertNum.do" ,
		dataType : 'json' ,
		data : {
			
			phoneCertNum : $("#phoneCertNum").val(),

		},

		success : function(data, textStatus, jqXHR)
		{
			var result = data.resultCode;
			var resultData = data.resultData;
			if(result == "0000"){
				
				$.alert("인증에 성공하였습니다.");
				certNumType ="Y";
				
			}else if(result == "0001"){

				$.alert("인증번호를 다시 입력해주세요.");
				 return false;

						}

			} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
		



	
}



function goInsertDriver(){


	var result = phoneChk($("#phoneNum")); 
	if(result != "0"){
		$.alert("휴대전화번호가 유효하지 않습니다.");
		return false;
	}
	
   	
		if($("#driverId").val() == ""){
			
			$.alert("아이디를 입력해주세요",function(a){
		    });
			return;
		}
/* 
		if($("#driverPwd").val() == ""){
	
			$.alert("비밀번호를 입력해주세요.",function(a){
		    });
			return;
		}
		if($("#driverPwdCheck").val() == ""){
	
			$.alert("비밀번호 확인을 입력해주세요.",function(a){
		    });
			return;
		} */


		if(!pwdCheck()){
			return false;
		}

		
		 if($("#driverName").val() == ""){
				
				$.alert("기사명을 입력해주세요.",function(a){
			    });
				return;
			}

		 if($("#residentRegistrationNumber	").val() == ""){
				
				$.alert("주민등록번호을 입력해주세요.",function(a){
			    });
				return;
			}
			
		 if($("#phoneNum").val() == ""){
				
			$.alert("핸드폰번호을 입력해주세요.",function(a){
		    });
			return;
		}
		

			
		 if($("#phoneCertNum").val() == ""){
				
				$.alert("인증번호을 입력해주세요.",function(a){
			    });
				return;
			}
			
			 if(idCheck != "Y"){
				
				$.alert("중복 확인을 눌러주세요.",function(a){
			    });
				return;
			}
		 
			 if(certNumType != "Y"){
				
				$.alert("인증번호 확인을 눌러주세요.",function(a){
			    });
				return;
			}

				if($("#companyKind").val() == ""){
					
					$.alert("사업자 구분을 선택해주세요.",function(a){
				    });
					return;
				}
				if($("#carAssignCompany").val() == ""){
					
					$.alert("차량 소속을 선택해주세요.",function(a){
				    });
					return;
				}


		$.confirm("회원 가입 하시겠습니까?",function(a){
			 if(a){


					if(a){

						$('#insertForm').ajaxForm({
							url: "/carrier/insertDriverRegister.do",
						    type: "POST",
							dataType: "json",		
							data : {

								
						    },
							success: function(data, response, status) {
								var status = data.resultCode;
								if(status == '0000'){
									$.alert("회원 가입이 정상적으로 완료 되었습니다.",function(a){

										document.location.href = "/carrier/login-page.do";
										
									});
								}else if(status == '0001'){
									$.alert("알 수없는 오류입니다. 카카오톡 플러스채팅으로 문의 주세요",function(a){
										document.location.href = "/carrier/login-page.do";
									});			
								}
										
							},
							error: function() {
								$.alert("오류가 발생하였습니다. 관리자에게 문의 하세요.");
							}                               
						});
						
						$("#insertForm").submit();
						
					}


				 
				//$("#insertForm").attr("action","/carrier/insertDriverRegister.do");
			  //  $("#insertForm").submit();	
					

			 	
			 }else{

				 return false;
				}
			 
		});
		

}

function getDistence(){


	$.ajax({
		type : 'post',
		url  : "/carrier/getDistense.do",
	    dataType : 'json',
		async : false,
	       data : {
				
	    	   departureAddress : $("#departureAddress").val(),
			   arrivalAddress : $("#arrivalAddress").val()
 	   		
		
			},

	   success : function(data, textStatus, jqXHR)
	    {
		   var resultCode = data.resultCode;
		   var resultData = data.resultData;
		   
		   if(resultCode == "0000"){

				$('#showDistence').css('display','block');
				$('#getDis').html(resultData+" km");

				
		   }else{

				
		   }
	   } ,
	  error :function(xhnRequest, ErrorText, thrownError){
		  $.alert('알수 없는 오류입니다. 관리자에게 문의 하세요.');
          return false;
		  }
	});

	
}


</script>


        
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
      <body style="" >
        <div class="content-container" style=" height:100%; /* overflow-y:hidden; */">
            <%-- <jsp:include page="/WEB-INF/views/jsp/common-top.jsp"></jsp:include> --%>
            <div class="animsition">
            <header class="bg-pink clearfix" style="position:fixed; background-color:#fff; border:none; text-align:center;  z-index:100000;">
                <div class="" style="width:19%;">
                    <a href="/carrier/login-page.do?&status=logout"><img style="zoom: 0.5;" src="/img/back-icon.png" alt=""></a> 
                </div> 
                <div class="" style="width:60%;">
                	<img style="width:90%; height:60%;" onclick="javascript:homeLoader.show(); document.location.href='/carrier/login-page.do?&status=logout'" src="/img/main_logo.png" alt="">
                </div>
                
               <div class="" style="width:19%; float:right;">
                   <!-- <a style="cursor:pointer;" href="javascript:logout();"><img style="" src="/img/logout.png" alt=""></a> -->
                </div> 
            </header>
            <%-- <div style="text-align:right;">${user.driver_name}님 환영 합니다.</div> --%>
            <form id="insertForm" action="/carrier/main.do">
					<input type="hidden" name="allocationId" id="allocationId" value="">
					<input type="hidden" name="companyId" id="companyId" >
					<input type="hidden" name="companyName" id="companyName" >
					<input type="hidden"  name="inputDt" id="inputDt">
					<input type="hidden" name="registerId" id="registerId" value="${userMap.user_id}">
					<input type="hidden" name="registerName" id="registerName" value="${userMap.name}">
				
		        
            <div style=" text-align:right; clear:both;">&nbsp;</div>
            <br>
	            
	            <div style="clear:both; margin-top:15%;">
		                 	<div id="btn_group" class="certNum" style=" width:100%; margin-top:5%;">
		                 		<div style="text-align:center;">
			                 		<div style="text-align:left; margin-left:2.5%;">
			                 			<span style="color:#F00;">*</span>
											<span class="d-tbc" style="font-weight: bold;">신규 아이디</span>
									</div> 
				         			<input type="text" style=" width:65%; padding:2%; margin-top:2%;" placeholder="아이디"  id="driverId" name="driverId" value="">
			         					<button id="test_btn1"  class="btn btn-warning" style="width:30%;" onclick ="javascirpt:goCheckId(); return false;"  >중복확인</button>
			         			</div>
			      			</div>
						</div>
	            
	            	<div style=" width:100%; overflow:hidden;">
			                 <div style="float:left; width:50%;">
			                 	<div class="certNum" style=" margin-top: 10%; width:100%;">
			                 		<div style="text-align:center;">
				                 		<div style="text-align:left; margin-left:2.5%;">
				                 			<span style="color:#F00;">*</span>
												<span class="d-tbc" style="font-weight: bold;">비밀번호</span>
										</div> 
					         			<input type="password" style=" width:90%; padding:3%; margin-top: 5%;" placeholder="비밀번호" class="d-tbc" id="driverPwd" name="driverPwd" value="" >
				         			</div>
				      			</div>
				      		</div>
				      		<div style="float:left; width:50%;">
			                	<div class="certNum" style=" margin-top: 10%; width:100%;">
			                 		<div style="text-align:center;">
				                 		<div style="text-align:left; margin-left:2.5%;">
				                 			<span style="color:#F00;">*</span>
												<span class="d-tbc" style="font-weight: bold;">비밀번호 확인</span>
										</div> 
					         			<input type="password" style=" width:90%; padding:3%; margin-top: 5%;" placeholder="비밀번호 확인" class="d-tbc" id="driverPwdCheck" name="driverPwdCheck" value="">
				         			</div>
				      			</div>
				      		</div>
				      	</div>
	            
	            
	            
	            
	            
			 			<div style=" width:100%; overflow:hidden;">
			                 <div style="float:left; width:50%;">
			                 	<div class="certNum" style=" margin-top: 10%; width:100%;">
			                 		<div style="text-align:center;">
				                 		<div style="text-align:left; margin-left:2.5%;">
				                 			<span style="color:#F00;">*</span>
												<span class="d-tbc" style="font-weight: bold;">기사명</span>
										</div> 
					         			<input type="text" style=" width:90%; padding:3%; margin-top: 5%;" placeholder="기사명" class="d-tbc" id="driverName" name="driverName" value="" >
				         			</div>
				      			</div>
				      		</div>
				       	
				      		       <div style="float:left; width:50%;">
			                 	<div class="certNum" style=" margin-top: 10%; width:100%;">
			                 		<div style="text-align:center;">
				                 		<div style="text-align:left; margin-left:5%;">
				                 			<span style="color:#F00;">*</span>
												<span class="d-tbc" style="font-weight: bold;">주민등록번호</span>
										</div> 
					         			<input type="text" style=" width:90%; padding:3%; margin-top: 5%;" placeholder="주민등록번호" class="d-tbc" id="residentRegistrationNumber" name="residentRegistrationNumber" value="" >
				         			</div>
				      			</div>
				      		</div>
				      	</div>
							
					<div style=" width:100%; overflow:hidden;">
			          	<div style="float:left; width:60%;">
			                	<div class="certNum" style=" margin-top: 10%; width:100%;">
			                 		<div style="text-align:center;">
				                 		<div style="text-align:left; margin-left:2.5%;">
				                 			<span style="color:#F00;">*</span>
												<span class="d-tbc" style="font-weight: bold;">핸드폰 번호</span>
										</div> 
					         			<input type="text" style=" width:90%; padding:3%; margin-top: 5%;" placeholder="- 빼고 입력해주세요" class="d-tbc" id="phoneNum" name="phoneNum" value="">
				         			</div>
				      			</div>
				      		</div> 
				      		<div style="float:left; width:40%; height:40%;">
				      			<button type="button" class="btn btn-warning"  style="width:70%; margin-top:35%;" onclick="javascript:goSendMessage();">인증 번호 전송</button>
			         		</div>

				     </div>
				     
				   <div id="certNumTable" style=" width:100%; overflow:hidden;  display:none; ">
			          	<div style="float:left; width:60%;">
			                	<div class="certNum" style=" margin-top: 10%; width:100%;">
			                 		<div style="text-align:center;">
				                 		<div style="text-align:left; margin-left:2.5%;">
				                 			<span style="color:#F00;">*</span>
												<span class="d-tbc" style="font-weight: bold;">인증 번호</span>
										</div> 
					         			<input type="text" style=" width:90%; padding:3%; margin-top: 5%;" placeholder="인증번호를 입력해주세요" class="d-tbc" id="phoneCertNum" name="phoneCertNum" value="">
				         			</div>
				      			</div>
				      		</div> 
				      		<div style="float:left; width:40%; height:40%;">
				      			<button type="button" class="btn btn-warning"  style="width:70%; margin-top:35%;" onclick="javascript:goSendCertNum();">인증 번호 확인</button>
			         		</div>
				     </div>
				     
					<div style=" width:100%; overflow:hidden;">
			                 <div style="float:left; width:50%;">
			                 	<div class="certNum" style=" margin-top: 10%; width:100%;">
			                 		<div style="text-align:center;">
				                 		<div style="text-align:left; margin-left:2.5%;">
				                 			<span style="color:#F00;">*</span>
												<span class="d-tbc" style="font-weight: bold;">사업자 구분</span>
										</div> 
					         			<%-- <input type="text" style=" width:90%; padding:3%; margin-top: 5%;" placeholder="기사명" class="d-tbc" id="chargeName" name="chargeName" value="${userMap.name}" readonly onfocus="javascript:$.alert('담당자명은 수정이 불가능합니다.');" > --%>
				         				<select name="companyKind"  id="companyKind" style="width:75%; padding:3%; margin-top:5%;">
										<option value="55"  selected="selected"> 선택</option>
										<option value="00" >법인</option>
										<option value="01" >개인</option>
										<option value="02" >기타</option>
										</select>
				         			</div>
				      			</div>
				      		</div>
				      		<div style="float:left; width:50%;">
			                	<div class="certNum" style=" margin-top: 10%; width:100%;">
			                 		<div style="text-align:center;">
				                 		<div style="text-align:left; margin-left:2.5%;">
				                 			<span style="color:#F00;">*</span>
												<span class="d-tbc" style="font-weight: bold;">사업자 번호</span>
										</div> 
					         			<input type="text" style=" width:90%; padding:3%; margin-top: 5%;" placeholder="사업자번호" class="d-tbc" id="businessLicenseNumber" name="businessLicenseNumber" value="">
				         			</div>
				      			</div>
				      		</div>
				      	</div>
							<div style=" width:100%; overflow:hidden;">
			                 <div style="float:left; width:50%;">
			                 	<div class="certNum" style=" margin-top: 10%; width:100%;">
			                 		<div style="text-align:center;">
				                 		<div style="text-align:left; margin-left:2.5%;">
				                 			<span style="color:#F00;">*</span>
												<span class="d-tbc" style="font-weight: bold;">자격증 번호</span>
										</div> 
					         			<input type="text" style=" width:90%; padding:3%; margin-top: 5%;" placeholder="자격증 번호" class="d-tbc" id="driverLicense" name="driverLicense" value="" >
				         			</div>
				      			</div>
				      		</div>
				      		<div style="float:left; width:50%;">
			                	<div class="certNum" style=" margin-top: 10%; width:100%;">
			                 		<div style="text-align:center;">
				                 		<div style="text-align:left; margin-left:2.5%;">
				                 			<span style="color:#F00;">*</span>
												<span class="d-tbc" style="font-weight: bold;">차량 소속</span>
										</div> 
					         			<%-- <input type="text" style=" width:90%; padding:3%; margin-top: 5%;" placeholder="연락처" class="d-tbc" id="chargePhone" name="chargePhone" value="${userMap.phoneNum}" readonly onfocus="javascript:$.alert('담당자 연락처는 수정이 불가능합니다.');"> --%>
				         			 	<select name="carAssignCompany"  id="carAssignCompany" style="width:75%; padding:3%; margin-top:5%;">
										<option value=""  selected="selected" >선택</option>
										<option value="S" >셀프</option>
										<option value="C" >캐리어</option>
										</select>
				         			</div>
				      			</div>
				      		</div>
				      	</div>
							
							
							
									
					<div style=" width:100%; overflow:hidden;">
			                 <div style="float:left; width:50%;">
			                 	<div class="certNum" style=" margin-top: 10%; width:100%;">
			                 		<div style="text-align:center;">
				                 		<div style="text-align:left; margin-left:5%;">
				                 			<span style="color:#F00;">*</span>
												<span class="d-tbc" style="font-weight: bold;">차종</span>
										</div> 
					         			<input type="text" style=" width:90%; padding:3%; margin-top: 5%;" placeholder="차종" class="d-tbc" id="carKind" name="carKind" value="" >
				         			</div>
				      			</div>
				      		</div>
				      		<div style="float:left; width:50%;">
			                	<div class="certNum" style=" margin-top: 10%; width:100%;">
			                 		<div style="text-align:center;">
				                 		<div style="text-align:left; margin-left:5%;">
				                 			<span style="color:#F00;">*</span>
												<span class="d-tbc" style="font-weight: bold;">차대번호(뒤6자리)</span>
										</div> 
					         			<input type="text" style=" width:90%; padding:3%; margin-top: 5%;" placeholder="차대번호(뒤6자리)" class="d-tbc" id="carIdNum" name="carIdNum" value="" >
				         			</div>
				      			</div>
				      		</div>
				      	</div>
					
				      	<br>
				      	
						<!-- <h5 class="d-tbc" style="font-weight: bold; color:#00000;">	<img class="blinking" src="/img/kakaotalkask.jpg" style ="margin-top:0%; width:8%; height:6%">한국카캐리어(주) 채널을 추가 해주시면 예약확정시 <span style="color:#FE9A2E;">알림톡</span>이 발송됩니다.</h5> -->
							<div class="menu-container" style="width:100%; text-align:center; clear:both;">
								<a class="kakao txt-bold" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:goInsertDriver();">
						        	회원가입
						        </a>
							</div>
				</form>

		</div>
	</div>
        
</body> 
        
        
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      
    <script src="/js/vendor/bootstrap.min.js"></script>       
    <!-- veiwport for countnumber -->
    <script src="/js/alert.js"></script>
    <script src="/js/main.js"></script>   
    <script src="/js/vendor/xpull.js"></script>       
    <script src="/js/vendor/jquery.loading-indicator.min.js"></script>
    <script src="https://apis.openapi.sk.com/tmap/jsv2?version=1&appKey=l7xx6b701ff5595c4e3b859e42f57de0c975"></script>    
        <script src="/js/vendor/jquery.form.min.js"></script>
    
    <script>
    
	
	$('#container').xpull({
	    'paused': false, 
	    // Pull threshold - amount in  pixels required to pull to enable release callback
        'pullThreshold':50,
        // Max pull down element - amount in pixels
        maxPullThreshold: 50,
        // timeout in miliseconds after which the loading indicator stops spinning.
        // If set to 0 - the loading will be indefinite
        'spinnerTimeout':1000,
        onPullStart: function(){},
        onPullEnd: function(){},
        callback: function(){window.location.reload()},

	});   
	
	
	var homeLoader;
	var inputStatus = "departure";
	 
	$(document).ready(function(){
			
		
				homeLoader = $('body').loadingIndicator({
					useImage: false,
					showOnInit : false
				}).data("loadingIndicator");
				
				/* setTimeout(function() {
					homeLoader.show();
					homeLoader.hide();
				}, 1000); */
		
	});


	 


    </script>
        
    
    </body>
</html>
