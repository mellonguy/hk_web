<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko" style=" height:100%;">      
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
    
<body style="background-color:#eee; height:100%;" >

<script type="text/javascript">  
  var pageMoveStop = true;
  var deviceToken = "";

  
$(document).ready(function(){  

	var updateToken = setInterval( function() {

		if(window.Android != null || (window.webkit != null && window.webkit.messageHandlers != null)){

			if(window.Android != null){
				
				window.Android.startService();
				deviceToken = getDeviceToken();
				if(typeof deviceToken == "undefined" || deviceToken == null || deviceToken == ""){
					window.location.reload();
				}
				
				if(window.Android != null && deviceToken != ""){
					updateDriverDeviceToken(deviceToken,"aos");
					pageMoveStop = false;
				}
				
			}else if(window.webkit.messageHandlers != null){

				var message = {
			               'action': 'login',
			               'driverId': '${driverId}'
			            };

				window.webkit.messageHandlers.scriptHandler.postMessage(message);
				var setToken = setInterval( function() {
					getDeviceToken();
					if(window.webkit.messageHandlers != null && deviceToken != ""){
						updateDriverDeviceToken(deviceToken,"ios");
						window.webkit.messageHandlers.scriptHandler.postMessage("sendDriverLocationStart");
						pageMoveStop = false;
						clearInterval(setToken);
					}
					
				}, 300);
					
			}

			

		}else if(window.Android == null && window.webkit == null){
			pageMoveStop = false;
		}

		homeLoader.hide();		
		clearInterval(updateToken);
   }, 1000);
	
	
	/* var updateLocation = setInterval( function() {
	
		if(getDeviceLocation() != null && getDeviceLocation() != ""){
			updateDriverDeviceLocation(getDeviceLocation());
		}
		
    }, 10*1000); */			//1분에 한번
	
  	//alert(deviceToken);
    
    
});  
  

function updateDriverDeviceToken(token,deviceOs){
	
	
	if(token != ""){
		$.ajax({ 
			type: 'post' ,
			url : "/updateDriverDeviceToken.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				token : token,
				deviceOs : deviceOs
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					//$.alert("성공",function(a){
					//});
				}else if(result == "E000"){
					
				}else{
					
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
}  
  
  
function getDeviceToken(){
	
	var token = "";
	try{
		if(window.Android != null){
			
			token = window.Android.getDeviceToken("${driverId}");
			
		}else if(window.webkit.messageHandlers != null){

			window.webkit.messageHandlers.scriptHandler.postMessage("getDeviceToken");
			
		}

	}catch(exception){
		
	}finally{
		
	}
	return token;
}  

function setDeviceToken(result){
	deviceToken = result;
}
  
function updateDriverDeviceLocation(location){
	
	if(location != ""){
		$.ajax({ 
			type: 'post' ,
			url : "/updateDriverDeviceLocation.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				location : location
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					//$.alert("성공",function(a){
					//});
				}else if(result == "E000"){
					
				}else{
					
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
	
}  
  
 function getDeviceLocation(){
	 
	 var latlng = "";
		try{
			if(window.Android != null){
				latlng = window.Android.getDeviceLocation("${kakaoId}");
				//alert(latlng);
			}	
		}catch(exception){
			
		}finally{
			
		}
		return latlng; 
	 
 }
  
  
function getAndroidData(str){
	
//	alert(str);
	
	//updateDriverDeviceLocation(str);
	
} 
 
 
function backKeyController(str){
	
	if(window.Android != null){
		window.Android.toastShort("뒤로가기 버튼으로 앱을 종료 할 수 없습니다.");
	}
	
}
 
 
function goNList(allocationStatus){
	
	//alert("승인 대기 목록");
	if(!pageMoveStop){
		homeLoader.show();
		document.location.href = "/carrier/list.do?allocationStatus="+allocationStatus;	
		var updateHomeLoader = setInterval(function() {
			clearInterval(updateHomeLoader);
			homeLoader.hide();
		}, 300);
	}
	
	
}

function goYList(allocationStatus){
	
	
	//window.Android.toastLong("test");
	//alert("승인 완료 목록");
	if(!pageMoveStop){
		homeLoader.show();
		document.location.href = "/carrier/list.do?allocationStatus="+allocationStatus;	
		var updateHomeLoader = setInterval(function() {
			clearInterval(updateHomeLoader);
			homeLoader.hide();
		}, 300);
	}
	
}

function goFList(allocationStatus){
	
	//alert("탁송 완료 목록");
	if(!pageMoveStop){
		homeLoader.show();
		document.location.href = "/carrier/finish-list.do?allocationStatus="+allocationStatus;	
		var updateHomeLoader = setInterval(function() {
			clearInterval(updateHomeLoader);
			homeLoader.hide();
		}, 300);
	}
	
}

function goLowViolationPage(){

	if(!pageMoveStop){
		homeLoader.show();
		document.location.href = "/carrier/lowViolationList.do";
		var updateHomeLoader = setInterval(function() {
			clearInterval(updateHomeLoader);
			homeLoader.hide();
		}, 300);	
	}
	
}




function goReload(){
	
	document.location.reload();
	/* $.alert("준비중 입니다.",function(a){
	}); */
	
}

function goCalPage(){
	
	
	if(("${user.driver_kind}" != "00" && "${user.driver_kind}" != "03") || "${user.driver_id}" == "sourcream"){
		if(!pageMoveStop){
			homeLoader.show();
			document.location.href = "/carrier/cal-list.do";	
		}	
	}else{
		$.alert("준비중 입니다.",function(a){
		});
	}
	
	
}

    
function logout(){
	
	$.confirm("로그아웃 하시겠습니까?",function(a){
		 if(a){
			 homeLoader.show();
			 if(window.Android != null){
					window.Android.stopService("${driverId}");
				}	
			 document.location.href = "/logout.do";	
		 }
	});
	
}    
    
    
    
function downloadDriverDeduct(){
	//alert(1);
	if(!pageMoveStop){
		//document.location.href = "/carrier/downloadDriverDeductInfo.do?driverId=${user.driver_id}";
		window.Android.openTmap("${driverId}");
	//	document.location.href = "https://apis.openapi.sk.com/tmap/app/execution?&appKey=l7xx6b701ff5595c4e3b859e42f57de0c975";
	}
}    

function viewDriverContact(){


	document.location.href = "/carrier/contact-main.do";

	
}






    
</script>


        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div class="content-container" style="height:100%; overflow-y:hidden;" >
            <!-- <header class="bg-pink clearfix" style="background-color:#fff; border:none; text-align:center; "> -->
            <header class="bg-pink clearfix" style="position:fixed; top:0px; background-color:#fff; border:none; text-align:center; ">
                <div class="" style="width:19%;">
                    <a href="#"></a> 
                </div> 
                <div class="" style="width:60%;">
                	<!-- <img style="width:90%; height:60%;" src="/img/main_logo.png" alt=""> -->
                	<img style="width:90%; height:60%;" onclick="javascript:document.location.href='/carrier/main.do'" src="/img/main_logo.png" alt="">
                </div>
                <div class="" style="width:19%; float:right;">
                   <a style="cursor:pointer;" href="javascript:logout();"><img style="" src="/img/logout.png" alt=""></a>
                </div>
            </header>
            <div style="clear:both;"></div>
            <!-- <div class="menu-container bg-pink">
                <span class="img-holder">
                    <img src="/img/user-img.png" alt="">
                </span>
                 <a class="kakao txt-bold" style="cursor:pointer;" onclick="javascript:kakaoLogin();">
                    카카오계정으로 로그인
                </a>
            </div> -->
            <div style=" text-align:right; clear:both;">&nbsp;</div>
            <div class="main-menu-container" style="margin-top:9%; height:100%; overflow-y:hidden;">
            	<div style=" text-align:right; clear:both;">${user.driver_name}님 환영 합니다.</div>
            	<div  id="container"  style="height:100%; margin-top:2%; background-color:#eee;">
            		<div class="xpull">
					    <div class="xpull__start-msg">
					        <div class="xpull__start-msg-text">화면을 당겼다 놓으면 새로 고침 됩니다.</div>
					        <div class="xpull__arrow"></div>
					    </div>
					    <div class="xpull__spinner">
					        <div class="xpull__spinner-circle"></div>
					    </div>
					</div>
				<div style="margin-top:3%; height:100%;">
                <a href="javascript:goNList('A');" class="menu-link ">
                    <img src="/img/layer-icon.png" alt="">
                    <c:if test="${aList > 0}">
                    	<span class="badge">${aList}</span>
                    </c:if>
                    <span class="text">승인대기</span>
                </a>
                <a href="javascript:goYList('Y');" class="menu-link">
                	<c:if test="${yList > 0}">
                		<span class="badge">${yList}</span>
                	</c:if>
                    <img src="/img/cut-icon.png" alt="">
                    <span class="text">탁송진행중</span>
                </a>
                <a href="javascript:goFList('F');" class="menu-link">
                	<%-- <c:if test="${fList > 0}">
                		<span class="badge">${fList}</span>
                	</c:if> --%>
                    <img src="/img/list-icon.png" alt="">
                    <span class="text">탁송완료</span>
                </a>
                <!-- <div style="clear:both; width:100%; top:0; bottom:0; height:100%; border:1px solid #f00;"></div> -->
                <a href="javascript:goCalPage();" class="menu-link">
                    <img src="/img/list-icon.png" alt="">
                    <span class="text">운송비정산</span>
                </a>
                <!-- <a href="javascript:goLowViolationPage();" class="menu-link"> -->
                <a href="/carrier/sub-main.do" class="menu-link">
                	<c:if test="${lowViolationList > 0}">
                		<span class="badge">${lowViolationList}</span>
                	</c:if>
                    <img src="/img/support-icon.png" alt="">
                    <span class="text">내차관리</span>
                </a>
                <c:if test="${user.emg_num_view_yn eq 'Y'}">
                <a href="javascript:viewDriverContact();" class="menu-link">
                    <img src="/img/list-icon.png" alt="">
                    <span class="text">비상연락망</span>
                </a> 
                </c:if>
                
                <jsp:useBean id="now" class="java.util.Date" />
	            <fmt:parseDate value="2019-12-1300:00:00" pattern="yyyy-MM-ddHH:mm:ss" var="startDate" />
				<fmt:parseDate value="2019-12-1523:59:59" pattern="yyyy-MM-ddHH:mm:ss" var="endDate" />
				<fmt:formatDate value="${now}" pattern="yyyy-MM-ddHH:mm:ss" var="nowDate" />								
				<fmt:formatDate value="${startDate}" pattern="yyyy-MM-ddHH:mm:ss" var="openDate"/>      
				<fmt:formatDate value="${endDate}" pattern="yyyy-MM-ddHH:mm:ss" var="closeDate"/>	
				<%-- <c:if test="${openDate < nowDate && closeDate > nowDate || user.driver_id eq 'sourcream'}"> --%>
				<c:if test="${openDate < nowDate && closeDate > nowDate}">
	                <a href="/carrier/election-list.do" class="menu-link">
	                    <img src="/img/folder-icon.png" alt="">
	                    <span class="text">온라인투표</span>
	                </a>
                </c:if>
                
                <c:if test="${user.driver_id eq 'hk0000'}">
                <!-- <a href="/carrier/downloadDriverDeductInfo.do" class="menu-link"> -->
                
                <a href="javascript:downloadDriverDeduct();" class="menu-link">
                    <img src="/img/list-icon.png" alt="">
                    <span class="text">테스트</span>
                </a>
                </c:if>
                
                <!-- <a href="javascript:goQrCodeScan();" class="menu-link">
                    <img src="/img/list-icon.png" alt="">
                    <span class="text">QR코드 스캔</span>
                </a> -->
                </div>
                </div>
            </div>
        </div>

    <script src="/js/vendor/jquery-1.11.2.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      <script src="/js/vendor/bootstrap.min.js"></script>       
    <!-- veiwport for countnumber -->
    <script src="/js/jquery.viewportchecker.js"></script>  
      <script src="/js/alert.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/vendor/xpull.js"></script>       
    <script src="/js/vendor/jquery.loading-indicator.min.js"></script>
    
    <script>
    
    var homeLoader;
    
    $(document).ready(function(){
    
    	homeLoader = $('body').loadingIndicator({
			useImage: false,
			showOnInit : false
		}).data("loadingIndicator");
	
    	homeLoader.show();
    	
    });
	
	$('#container').xpull({
	    'paused': false, 
	    // Pull threshold - amount in  pixels required to pull to enable release callback
        'pullThreshold':50,
        // Max pull down element - amount in pixels
        maxPullThreshold: 50,
        // timeout in miliseconds after which the loading indicator stops spinning.
        // If set to 0 - the loading will be indefinite
        'spinnerTimeout':1000,
        onPullStart: function(){},
        onPullEnd: function(){},
        callback: function(){window.location.reload()},

	});   
    
    </script>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    </body>
</html>
