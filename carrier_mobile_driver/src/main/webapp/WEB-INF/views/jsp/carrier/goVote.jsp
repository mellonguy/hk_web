<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko"  style=" height:100%;">      
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
    
<body style=" height:100%;">


<script type="text/javascript">  

$(document).ready(function(){
	
/* 	var updateToken = setInterval( function() {
		
		if(getDeviceToken() != null && getDeviceToken() != ""){
			updateDriverDeviceToken(getDeviceToken());
			if(window.Android != null){
				window.Android.startService();
			}
			clearInterval(updateToken);
		}
   }, 1000); */
	
});



function getDeviceToken(){
	
	var token = "";
	try{
		if(window.Android != null){
			token = window.Android.getDeviceToken("${driverId}");
		}	
	}catch(exception){
		
	}finally{
		
	}
	return token;
}  
  

function updateDriverDeviceToken(token){
	
	
	if(token != ""){
		$.ajax({ 
			type: 'post' ,
			url : "/updateDriverDeviceToken.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				token : token
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					//$.alert("성공",function(a){
					//});
				}else if(result == "E000"){
					
				}else{
					
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
}  



 function getNewsData(obj,keyword){
	
	 
	// alert($("#selectedStatus").children().length);
	 
	//alert($("#selectedStatus").children().first().get(0));	
	//alert($(obj).parent().get(0));
	
 	 if($("#selectedStatus").children().first().get(0) == $(obj).parent().get(0)){				//전체를 클릭 한경우
					
 		$("#selectedStatus").children().each(function(index,element){
 			$(this).removeClass('selected');
 		});
 		$(obj).parent().addClass('selected');
	}else{
			
	} 
	
	
	
/* 	
	
	$.ajax({ 
		type: 'post' ,
		url : "/research/newsSearch.do?&keyword="+keyword ,
		dataType : 'json' ,
		data : {
			
		},
		success : function(data, textStatus, jqXHR)
		{
			alert(data);
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
	 */
	
	
} 

 function goNewsPage(uri){
		
		//var replaceUrlStr = replaceAll(url, "?", "^");
		//replaceUrlStr = replaceAll(replaceUrlStr, "=", "+");
		
		var replaceUrlStr = encodeURIComponent(uri)
		document.location.href = "/news/newsPage.do?newsUri="+replaceUrlStr;
	}  


    
function viewAllocationData(decideMonth){
	homeLoader.show();
	document.location.href = "/carrier/cal-list-detail.do?decideMonth="+decideMonth;
	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
} 
 
 
function viewAllocationList(allocationId,allocationStatus){
	homeLoader.show();
	document.location.href = "/carrier/group-list.do?allocationId="+allocationId+"&allocationStatus="+allocationStatus;
	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
} 
 
 
 
function logout(){
	
	$.confirm("로그아웃 하시겠습니까?",function(a){
		 if(a){
			 homeLoader.show();
			 if(window.Android != null){
					window.Android.stopService("${driverId}");
				}	
			 document.location.href = "/logout.do";	
		 }
	});
	
}   
 
 
 
function backKeyController(str){
	//history.go(-1);
	//location.href = document.referrer;
	homeLoader.show();
	document.location.href='/carrier/main.do';
	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
}
 
 
 
function viewItem(decideMonth,item){
	homeLoader.show();
	document.location.href = "/carrier/cal-item.do?decideMonth="+decideMonth+"&item="+item;
	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
} 
 
 
function viewElectionData(electionId){

	homeLoader.show();
	document.location.href = "/carrier/viewElection.do?electionId="+electionId;
	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
	
	
}
 
 







function takeVote(){


	/*
		if($('input:checkbox[id="checkAll"]').is(":checked")){
			$('input:checkbox[name="forDecideStatus"]').each(function(index,element) {
				$(this).prop("checked","true");
			 });	
		}else{
			$('input:checkbox[name="forDecideStatus"]').each(function(index,element) {
				$(this).prop("checked","");
			 });
		}
	*/

		//alert($('input:checkbox[name="forElection"]:checked').length);


		if("${election.candidate_division}"=="A"){
			if($('input:checkbox[name="forElection"]:checked').length < 2){
				$.alert("후보가 전부 선택 되지 않았습니다.",function(a){
				});
				return false;
			}
		}else if("${election.candidate_division}"=="D"){
			if($('input:checkbox[name="forElection"]:checked').length < 1){
				$.alert("후보가 선택 되지 않았습니다.",function(a){
				});
				return false;
			}
		}else{
			if($('input:checkbox[name="forElection"]:checked').length < 1){
				$.alert("후보가 선택 되지 않았습니다.",function(a){
				});
				return false;
			}
		}

		var candidateDivisionO = 0;
		var candidateDivisionS = 0;
		var candidateDivisionC = 0;

		$('input:checkbox[name="forElection"]:checked').each(function(index,element) {
			if($(this).attr("assign") == "O"){
				candidateDivisionO++;
			}else if($(this).attr("assign") == "S"){
				candidateDivisionS++;
			}else if($(this).attr("assign") == "C"){
				candidateDivisionC++;
			}
		 });

		if(candidateDivisionO > 1){
			$.alert("사무실 소속의 후보가 "+candidateDivisionO+"명 선택 되었습니다. 각 소속의 후보는 1명만 선택 가능 합니다.",function(a){
			});
			return false;
		}else if(candidateDivisionS > 1){
			$.alert("셀프 소속의 후보가 "+candidateDivisionS+"명 선택 되었습니다. 각 소속의 후보는 1명만 선택 가능 합니다.",function(a){
			});
			return false;
		}else if(candidateDivisionC > 1){
			$.alert("캐리어 소속의 후보가 "+candidateDivisionC+"명 선택 되었습니다. 각 소속의 후보는 1명만 선택 가능 합니다.",function(a){
			});
			return false;
		}
		
		//후보 선택에 문제가 없으면 선택된 후보를 DB에 저장
		var voteInfoList = new Array();
		
		$('input:checkbox[name="forElection"]:checked').each(function(index,element){
			var voteInfo = new Object();
			voteInfo.assign = $(this).attr("assign");
			voteInfo.candidateId = $(this).attr("candidateId");
			voteInfo.name = $(this).attr("candidateName");
			voteInfoList.push(voteInfo);
		});

		$.confirm("해당 후보에게 투표 하시겠습니까? 이 동작은 취소 할 수 없습니다.",function(a){
			 if(a){
				 homeLoader.show();
				 $.ajax({ 
						type: 'post' ,
						url : "/carrier/insertVote.do" ,
						dataType : 'json' ,
						async : false,
						data : {
							voteInfoList : JSON.stringify({voteInfoList : voteInfoList}),
							electionId : "${election.election_id}"
						},
						success : function(data, textStatus, jqXHR)
						{
							var result = data.resultCode;
							var resultData = data.resultData;
							if(result == "0000"){
								homeLoader.hide();
								$.alert("투표가 완료되었습니다.",function(a){
									document.location.href = "/carrier/election-list.do"
								});
								
							}else if(result == "E000"){
								$.alert("투표를 완료하지 못했습니다. 관리자에게 문의 하세요.",function(a){
									document.location.href = "/carrier/election-list.do"
								});
							}else if(result == "0001"){
								$.alert("조회 하는데 실패 하였습니다.",function(a){
								});
								return false;
							}
						} ,
						error : function(xhRequest, ErrorText, thrownError) {
						}
					});
				 var updateHomeLoader = setInterval(function() {
						clearInterval(updateHomeLoader);
						homeLoader.hide();
					}, 300);
			 }
		});
		
	}























 
</script>


        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div class="content-container" style=" height:auto; overflow-y:hidden;">
            <%-- <jsp:include page="/WEB-INF/views/jsp/common-top.jsp"></jsp:include> --%>
            <header class="bg-pink clearfix" style="position:fixed; background-color:#fff; border:none; text-align:center; height:80px; z-index:100; ">
                <div class="" style="width:19%;">
                    <a href="javascript:history.go(-1);"><img style="zoom: 0.5;" src="/img/back-icon.png" alt=""></a> 
                </div> 
                <div class="" style="width:60%;">
                	<img style="width:90%; height:60%;" onclick="javascript:document.location.href='/carrier/main.do'" src="/img/main_logo.png" alt="">
                </div>
                <div class="" style="width:19%; float:right;">
                   <a style="cursor:pointer;" href="javascript:logout();"><img style="" src="/img/logout.png" alt=""></a>
                </div>
            </header>
            <%-- <div style="text-align:right;">${user.driver_name}님 환영 합니다.</div> --%>
            <div style=" text-align:right; clear:both;">&nbsp;</div>
            <div class="content-container interest-page loadingthemepage news" style="position:fixed; background-color:#fff; clear:both; margin-top:50px;  z-index:100;">
	            <!-- <div class="cat-list" style="background-color:#fff">
	                <ul class="clearfix" style="background-color:#fff">
	                	<li><a style="color:#000; width:15%;">출발일</a></li>
	                    <li><a style="color:#000;">상차지</a></li>
	                    <li><a style="color:#000;">하차지</a></li>
	                    <li><a style="color:#000;">차종</a></li>
	                    <li><a style="color:#000;">차대번호</a></li>
	                </ul>
	            </div> -->
	            <div style="width:100%; margin-top:5px; height:40px; border-bottom:1px solid #eee;">
	               <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">선택</div></div>
                   <div style="width:40%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">소속</div></div>
	               <div style="width:40%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">후보명</div></div>
                </div>
            </div>
            <div id="container" class="search-result news-list content-box" style="height:100%; margin-top:21%; overflow-y:scroll;">
            	<!-- <div class="xpull">
				    <div class="xpull__start-msg">
				        <div class="xpull__start-msg-text">화면을 당겼다 놓으면 새로 고침 됩니다.</div>
				        <div class="xpull__arrow"></div>
				    </div>
				    <div class="xpull__spinner">
				        <div class="xpull__spinner-circle"></div>
				    </div>
				</div> -->
            	<div class="news-container clearfix" style="height:450%; z-index:100000;">
            	
            		<c:if test="${election.candidate_division == 'A' || election.candidate_division == 'O'}">
            		1
	    	       		<c:forEach var="data" items="${candidateList}" varStatus="status">
	    	       				<c:if test="${data.car_assign_company == 'O' && data.candidate_id != 'hk0000'}">
					           	<div style="width:100%; margin-top:5px; height:40px; border-bottom:1px solid #eee; z-index:1;" >
					           		<%-- <div style="width:10%; text-align:center; height:40px; float:left;"><div style="width:100%; margin-top:10px; text-align:center; display:inline-block; text-overflow:ellipsis; white-space:nowrap; overflow:hidden;"><nobr>${data.election_title}</nobr></div></div> --%>
					           		<div style="width:20%; text-align:center; height:40px; float:left;">
					           			<input onclick="javascript:forShowButton(this);"  name="forElection" candidateName="${data.candidate_name}" assign="${data.car_assign_company}" candidateId="${data.candidate_id}" style="transform : scale(1.5); margin-top:14px;" type="checkbox"/>
									</div>
							        <div style="width:40%; text-align:center; height:40px; float:left;">
							        	<div style="margin-top:10px; text-align:center; display:inline-block;">
							        		<c:if test='${data.car_assign_company eq "O" }'>
			                            		본사
			                            	</c:if>
			                            	<c:if test='${data.car_assign_company eq "S" }'>
			                            		셀프
			                            	</c:if>
			                            	<c:if test='${data.car_assign_company eq "C" }'>
			                            		캐리어
			                            	</c:if>
							        	</div>
							        </div>
								    <div style="width:40%; text-align:center; height:40px; float:left;">
								    	<div style="width:100%; margin-top:10px; text-align:center; display:inline-block; text-overflow:ellipsis; white-space:nowrap; overflow:hidden;">
								    		<nobr>${data.candidate_name}</nobr>
								    	</div>
								    </div>
					            </div>
					            </c:if>
		                </c:forEach>
	                </c:if>
	                
	                <c:if test="${election.candidate_division == 'D' || election.candidate_division == 'S'}">
	    	       		<c:forEach var="data" items="${candidateList}" varStatus="status">
	    	       			<c:if test='${data.car_assign_company == "S" && data.car_assign_company == driver.car_assign_company}'>
	    	       			
	    	       				<c:if test="${data.candidate_id != 'sourcream' && data.candidate_id != 'tomylim560' && data.candidate_id != '김춘기' && data.candidate_id != 'DRI5650ac46113b448e975d049e361471ca'}">
						           	<div style="width:100%; margin-top:5px; height:40px; border-bottom:1px solid #eee; z-index:1;" >
						           		<div style="width:20%; text-align:center; height:40px; float:left;">
						           			<input name="forElection" candidateName="${data.candidate_name}" assign="${data.car_assign_company}" candidateId="${data.candidate_id}" style="transform : scale(1.5); margin-top:14px;" type="checkbox"/>
										</div>
								        <div style="width:40%; text-align:center; height:40px; float:left;">
								        	<div style="margin-top:10px; text-align:center; display:inline-block;">
								        		<c:if test='${data.car_assign_company eq "O" }'>
				                            		본사
				                            	</c:if>
				                            	<c:if test='${data.car_assign_company eq "S" }'>
				                            		셀프
				                            	</c:if>
				                            	<c:if test='${data.car_assign_company eq "C" }'>
				                            		캐리어
				                            	</c:if>
								        	</div>
								        </div>
									    <div style="width:40%; text-align:center; height:40px; float:left;">
									    	<div style="width:100%; margin-top:10px; text-align:center; display:inline-block; text-overflow:ellipsis; white-space:nowrap; overflow:hidden;">
									    		<nobr>${data.candidate_name}</nobr>
									    	</div>
									    </div>
						            </div>
					            </c:if>
				            </c:if>
		                </c:forEach>
	                </c:if>
	                <c:if test="${election.candidate_division == 'D' || election.candidate_division == 'C'}">
	    	       		<c:forEach var="data" items="${candidateList}" varStatus="status">
	    	       			<c:if test='${data.car_assign_company == "C" && data.car_assign_company == driver.car_assign_company}'>
	    	       				<c:if test="${data.candidate_id != 'sourcream' && data.candidate_id != 'hkcc' && data.candidate_id != 'ssbhh73' && data.candidate_id != 'min9733' && data.candidate_id != 'yb3722' && data.candidate_id != 'kkd1234' && data.candidate_id != 'Goldstein' && data.candidate_id != 'Richardsch'}">
						           	<div style="width:100%; margin-top:5px; height:40px; border-bottom:1px solid #eee; z-index:1;" >
						           		<div style="width:20%; text-align:center; height:40px; float:left;">
						           			<input onclick="javascript:forShowButton(this);"  name="forElection" candidateName="${data.candidate_name}" assign="${data.car_assign_company}" candidateId="${data.candidate_id}" style="transform : scale(1.5); margin-top:14px;" type="checkbox"/>
										</div>
								        <div style="width:40%; text-align:center; height:40px; float:left;">
								        	<div style="margin-top:10px; text-align:center; display:inline-block;">
								        		<c:if test='${data.car_assign_company eq "O" }'>
				                            		본사
				                            	</c:if>
				                            	<c:if test='${data.car_assign_company eq "S" }'>
				                            		셀프
				                            	</c:if>
				                            	<c:if test='${data.car_assign_company eq "C" }'>
				                            		캐리어
				                            	</c:if>
								        	</div>
								        </div>
									    <div style="width:40%; text-align:center; height:40px; float:left;">
									    	<div style="width:100%; margin-top:10px; text-align:center; display:inline-block; text-overflow:ellipsis; white-space:nowrap; overflow:hidden;">
									    		<nobr>${data.candidate_name}</nobr>
									    	</div>
									    </div>
						            </div>
					            </c:if>
				            </c:if>
		                </c:forEach>
	                </c:if>
	                
	                
	                
	                <div class="menu-container" style="width:100%; text-align:center; clear:both;">
		                 <a class="kakao txt-bold" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:takeVote();">
		                    	투표완료
		                </a>
            		</div>
	                
                 </div>
            </div>
        </div>


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      <script src="/js/vendor/bootstrap.min.js"></script>       
    <!-- veiwport for countnumber -->
    <script src="/js/alert.js"></script>
    <script src="/js/main.js"></script>   
    <script src="/js/vendor/xpull.js"></script>       
    <script src="/js/vendor/jquery.loading-indicator.min.js"></script>
    
    <script>
    
 var homeLoader;
    
    $(document).ready(function(){
    
    	homeLoader = $('body').loadingIndicator({
			useImage: false,
			showOnInit : false
		}).data("loadingIndicator");
	

    	
    });
	
/* 	$('#container').xpull({
	    'paused': false, 
	    // Pull threshold - amount in  pixels required to pull to enable release callback
        'pullThreshold':50,
        // Max pull down element - amount in pixels
        maxPullThreshold: 50,
        // timeout in miliseconds after which the loading indicator stops spinning.
        // If set to 0 - the loading will be indefinite
        'spinnerTimeout':1000,
        onPullStart: function(){},
        onPullEnd: function(){},
        callback: function(){window.location.reload()},

	});   */ 
    
    </script>
        
    </body>
</html>
