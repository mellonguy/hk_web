<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko" style="height:100%;">      
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
<body style="height:100%; text-align: center;">

<script type="text/javascript">  
  
$(document).ready(function(){  

	var userInputId = getCookie("userInputId");
	$("#loginId").val(userInputId);

	var userInputPw = getCookie("userInputPw"); 
	$("#loginPwd").val(userInputPw);

	try {
		if(window.carrierJrpr != null){
			window.carrierJrpr.loginForApp("${kakaoId}", "");
		}
		if(window.external != null){
	        window.external.loginForPC("${kakaoId}", "");
	    }
	}catch(exception){
	    
	}finally {
		
	}

	//아이디와 패스워드가 
	if($("#loginId").val() != "" && $("#loginPwd").val() != "" && getParameter("status") != "logout"){
		loginProcess();
	}	
	
  
});


function getParameter(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}



function getCookie(cookieName) {
    cookieName = cookieName + '=';
    var cookieData = document.cookie;
    var start = cookieData.indexOf(cookieName);
    var cookieValue = '';
    if(start != -1){
        start += cookieName.length;
        var end = cookieData.indexOf(';', start);
        if(end == -1)end = cookieData.length;
        cookieValue = cookieData.substring(start, end);
    }
    return unescape(cookieValue);
}

function setCookie(cookieName, value, exdays){
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var cookieValue = escape(value) + ((exdays==null) ? "" : "; expires=" + exdate.toGMTString());
    document.cookie = cookieName + "=" + cookieValue;
}

function logout(){

	if(confirm("로그아웃 하시겠습니까?")){
		
		try {
			if(window.carrierJrpr != null){
				window.carrierJrpr.logoutForApp("");
			}
			if(window.external != null){
	            window.external.logoutForPC("");
	        }
		}catch(exception){
		    
		}finally {
			document.location.href = "/kakao/logout.do";	
		}
	}
}
    

function loginForApp(userId){
	window.carrierJrpr.loginForApp(userId, "");
}



function logoutForApp(){
	window.carrierJrpr.logoutForApp("");
}    
    
    
    
function loginProcess(){
	
	if($("#loginId").val() == ""){
		//alert("아이디가 입력 되지 않았습니다.");
		//return;
		$.alert("아이디가 입력 되지 않았습니다.",function(a){
	    });
		return;
	}
	if($("#loginPwd").val() == ""){
		//alert("패스워드가 입력 되지 않았습니다.");
		//return;
		$.alert("패스워드가 입력 되지 않았습니다.",function(a){
	    });
		return;
	}

	$.ajax({ 
        type: 'post' ,
        url : '/loginPersist.do' ,
        dataType : 'json' ,
        data : $('#loginForm').serialize(), 
        
        success : function(jsonData, textStatus, jqXHR) {
        	if(jsonData.resultCode =="0000"){

        		setCookie("userInputId", $("#loginId").val(), 365);
        		setCookie("userInputPw", $("#loginPwd").val(), 365);
            	goMainPage();
            	
        	}else if(jsonData.resultCode =="E001"){
        		//alert("로그인에 실패 하였습니다. 관리자에게 문의 하세요.");
        		$.alert("로그인에 실패 하였습니다. 관리자에게 문의 하세요.",function(a){
        			
        	    });
        	}else if(jsonData.resultCode =="E002"){
        		//alert("로그인에 실패 하였습니다. 관리자에게 문의 하세요.");
        		$.alert("휴직 또는 퇴사처리 된 아이디 입니다. 관리자에게 문의 하세요.",function(a){
        			
        	    });
        	}
        } ,
        error : function(xhRequest, ErrorText, thrownError) {
          //  alertEx("시스템 에러");
        }
    }); 
}

function goMainPage(){
	document.location.href = "/carrier/main.do";
}
    
    
function changePassWord(){
	document.location.href = "/carrier/changePassWord.do"
}
    
  


function backKeyController(str){
	if(window.Android != null){
		window.Android.toastShort("뒤로가기 버튼으로 앱을 종료 할 수 없습니다.");
	}	
} 
 
</script>



		<div class="content-container">
            <!-- <header class="bg-pink clearfix"> -->
                <!-- <div class="search-icon">
                    <a href="#"></a> 
                </div> -->
                <!-- <div class="" style="width:70%; text-align:center;">
                	<img style="width:100%; height:75%;" src="/img/main_logo.png" alt="">
                </div> -->
                <!-- <div class="menu-bar pull-right">
                    <a style="cursor:pointer;" href="/adviser/adviser.do"><img src="/img/close-white-icon.png" alt=""></a>
                </div> -->
            <!-- </header> -->
            
            <div style=" display:table; margin:0 auto; display:inline-block; width:60%; height:100%;">
            
                	<img style="width:100%; height:75%; margin-top:70%;" src="/img/main_logo.png" alt="">
                
            <form action="" id="loginForm" style="margin-top:40%;">
                        <div class="username d-table">
                            <span class="d-tbc" style="font-weight:bold;">로그인 아이디</span>
                            <div style="margin-top:5%;">
                                <input type="text" placeholder="아이디를 입력해 주세요" class="d-tbc" id="loginId" name="driverId">
                            </div>
                        </div>
                        <div class="password d-table" style="margin-top:10%;">
                            <span class="d-tbc" style="font-weight:bold;">패스워드</span>
                            <div style="margin-top:5%;">
                          	     <input type="password" placeholder="패스워드를 입력해 주세요" class="d-tbc" id="loginPwd" name="driverPwd"   onkeypress="if(event.keyCode=='13') loginProcess();">
                            </div>
                        </div>
                       </form>
                       <div class="login-btn-container d-table">
                           <span class="d-tbc"></span>
                           <div style="margin-top:10%; text-align:center;">
                           		<!-- <div style="width:30%; border:3px solid #eee;"> -->
                               	<a style="cursor:pointer;" onclick="javascript:loginProcess();" class="login-btn d-tbc">로그인</a>&nbsp;/
                               	<a style="cursor:pointer;" onclick="javascript:changePassWord();" class="login-btn d-tbc">비밀번호 변경</a>
                               	<!-- </div> -->
                           </div>
                       </div>
                       
                       <div class="login-btn-container d-table">
                           <span class="d-tbc"></span>
                           <div style="margin-top:35%; text-align:center;">
                               	<a href="/carrier/driver-register.do" style="cursor:pointer;" class="login-btn d-tbc">회원가입</a>
                           </div>
                       </div> 
                       
                       
                   </div>    
                       
       	</div>




    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      <script src="/js/vendor/bootstrap.min.js"></script>       
    <!-- veiwport for countnumber -->
    <script src="/js/jquery.viewportchecker.js"></script>    
    <script src="/js/alert.js"></script>
    <script src="/js/main.js"></script> 
</body>
</html>
