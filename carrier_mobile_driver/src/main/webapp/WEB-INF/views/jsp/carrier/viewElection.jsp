<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!doctype html>
<html lang="ko">
    <jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
    <body  class="white">
    
    <script type="text/javascript">  

    $(document).ready(function(){
    	
    	//getNewsData("${searchWord}");
    	
    });


 
    
function getDistance(){
    	
	
	homeLoader.show();
	var departureAddr = $("#departureAddr").html();
	var arrivalAddr = $("#arrivalAddr").html();
	
   	$.ajax({ 
   		type: 'post' ,
   		url : "/carrier/getDistance.do" ,
   		dataType : 'json' ,
   		async : false,
   		data : {
   			departureAddr :departureAddr, 
   			arrivalAddr : arrivalAddr
   		},
   		success : function(data, textStatus, jqXHR)
   		{
   			var result = data.resultCode;
   			var resultData = data.resultData;
   			if(result == "0000"){
   				
   				routeView(resultData.departure,resultData.arrival);
   				
   			}else if(result == "0001"){
   				$.alert("조회 하는데 실패 하였습니다.",function(a){
   				});
   				homeLoader.hide();
   			}else if(result == "0002"){
   				$.alert("상차지의 주소가 정확 하지 않거나 차량으로 이동 할 수 없는 경로 입니다.",function(a){
   				});
   				homeLoader.hide();
   			}else if(result == "0003"){
   				$.alert("하차지의 주소가 정확 하지 않거나 차량으로 이동 할 수 없는 경로 입니다.",function(a){
   				});
   				homeLoader.hide();
   			}
   		} ,
   		error : function(xhRequest, ErrorText, thrownError) {
   		}
   	});
   	
    	
}

function routeView(departure,arrival){
    
	document.location.href = "/carrier/routeView.do?&departure="+departure+"&arrival="+arrival;
   	
   	//window.open("/carrier/routeView.do?&departure="+departure+"&arrival="+arrival+",_blank","top=0,left=0,width=1600,height=800,toolbar=0,status=0,scrollbars=1,resizable=0");
   		
}    
    
function backKeyController(str){
	
	history.go(-1);
	//location.href = document.referrer;
}


function goVote(electionId){

	document.location.href = "/carrier/goVote.do?&electionId="+electionId;
	
}
    
    </script>

<div class="content-container withads themedetails companydetails">
            <header class="bg-pink clearfix" style="position:fixed; background-color:#fff; border:none; text-align:center; z-index:3; ">
                <div class="" style="width:19%;">
                    <a href="javascript:history.go(-1);"><img style="zoom: 0.5;" src="/img/back-icon.png" alt=""></a>
                    <!-- <a href="/carrier/main.do"><img style="zoom: 0.5;" src="/img/back-icon.png" alt=""></a> -->
                     
                </div> 
                <div class="" style="width:60%;">
                	<!-- <img style="width:90%; height:60%;" src="/img/main_logo.png" alt=""> -->
                	<img style="width:90%; height:60%;" onclick="javascript:document.location.href='/carrier/main.do'" src="/img/main_logo.png" alt="">
                </div>
                <div class="" style="width:19%; float:right;">
                   <a style="cursor:pointer;" href="javascript:logout();"><img style="" src="/img/logout.png" alt=""></a>
                </div>
            </header>
            <div style="text-align:right;">${user.driver_name}님 환영 합니다.</div>
            <div class="company-details-container" <c:if test="${allocationStatus == 'F'}"> style="margin-bottom:20px;" </c:if>>
                
                
                <%-- <div class="company-views">
                    <ul>
                        <li><a href="/company/company-ticker.do?itemSrtCd=${itemMap.item_srt_cd}&marketCd=${itemMap.market_cd}">시세알림설정</a></li>
                        <li class="active"><a href="/company/company.do?itemSrtCd=${itemMap.item_srt_cd}&marketCd=${itemMap.market_cd}">기업개요</a></li>
                        <li><a href="/company/company-news.do?itemSrtCd=${itemMap.item_srt_cd}&marketCd=${itemMap.market_cd}">뉴스</a></li>
                        <li><a href="/company/theme.do?itemSrtCd=${itemMap.item_srt_cd}&marketCd=${itemMap.market_cd}">테마</a></li>
                        <li><a href="/company/discussion.do?itemSrtCd=${itemMap.item_srt_cd}&marketCd=${itemMap.market_cd}">토론실</a></li>
                    </ul>
                </div> --%>   
                <div class="overview-box active content-view" style=" clear:both; margin-top:50px;">
                	
                	
                   <div class="view-details">
                       <div class="overview detail-box">
                         <span class="price">투표 상세</span>
                           
                           
                       <div class="detail-box" style="border-bottom:2px solid #404040;">
                       
                       	<div class="other-info d-table">
                             <div class="list-category left t-cell" style="border-right:none;">
                             		<span class="label-text" style="width:20%;">선거명</span>
                                 	<span class="value" id="departureAddr"  style="width:80%; font-weight:bold; letter-spacing:1px;">${election.election_title}</span>
							</div>
						</div>
                       
                       
                           <div class="other-info d-table">
                               <div class="eps left t-cell">
                                   <span class="label-text">시작일</span>
                                   <span class="value">${election.start_dt}</span>
                               </div>
                               <div class="per t-cell right">
                                   <span class="label-text">종료일</span>
                                   <span class="value">${election.end_dt}</span>
                               </div>
                           </div>
                           <div class="other-info d-table">
                               <div class="eps left t-cell">
                                   <span class="label-text">참여구분</span>
                                   <span class="value">
	                                   	<c:if test='${election.distribute_division eq "A" }'>
		                            		전체
		                            	</c:if>
		                            	<c:if test='${election.distribute_division eq "O" }'>
		                            		본사 전체
		                            	</c:if>
		                            	<c:if test='${election.distribute_division eq "D" }'>
		                            		기사 전체
		                            	</c:if>
		                            	<c:if test='${election.distribute_division eq "S" }'>
		                            		셀프
		                            	</c:if>
		                            	<c:if test='${election.distribute_division eq "C" }'>
		                            		캐리어
		                            	</c:if>
                                   </span>
                               </div>
                               <div class="per t-cell right">
                                   <span class="label-text">후보구분</span>
                                   <span class="value">
                                   		<c:if test='${election.candidate_division eq "A" }'>
		                            		전체
		                            	</c:if>
		                            	<c:if test='${election.candidate_division eq "O" }'>
		                            		본사 전체
		                            	</c:if>
		                            	<c:if test='${election.candidate_division eq "D" }'>
		                            		기사 전체
		                            	</c:if>
		                            	<c:if test='${election.candidate_division eq "S" }'>
		                            		셀프
		                            	</c:if>
		                            	<c:if test='${election.candidate_division eq "C" }'>
		                            		캐리어
		                            	</c:if>
									</span>
                               </div>
                           </div>
                       </div>
                   
                           <div class="price-info-box">
                           		
                            </div>
                            
                           
                           <div class="other-info d-table" style="border-bottom:none;">
                               <div class="list-category left t-cell" style="border-right:none;">
                                   <span class="label-text" style="width:25%;">선거내용</span>
                                    	<span class="value" id="departureAddr"  style="width:75%;"></span>
                               </div>
                               
                           </div>
                           
                           
                           <div class="other-info d-table" style="text-align:center;">
                          		<textarea style="width:90%;  height:auto; min-height:100px; font-size:15px; background-color:#fff; border:none;" disabled>${election.election_content}</textarea>
                           
                           
                           		<%-- <p class="" id="departureAddr"  style="width:100%; font-size:15px;">${election.election_content}</p> --%>
                           </div>
                            
                          
                           
                       </div>
                   </div>
                   </div>
                   <!-- <div class="divider black"></div> -->
                   
                   <!-- <div class="divider black"></div> -->
                </div>
                
                
                
                <div class="menu-container" style="width:100%; text-align:center; clear:both;">
                	
                	<jsp:useBean id="now" class="java.util.Date" />
                	<fmt:parseDate value="${election.start_dt}${election.start_time}" pattern="yyyy-MM-ddHH:mm:ss" var="startDate" />
					<fmt:parseDate value="${election.end_dt}${election.end_time}" pattern="yyyy-MM-ddHH:mm:ss" var="endDate" />
					<fmt:formatDate value="${now}" pattern="yyyy-MM-ddHH:mm:ss" var="nowDate" />								
					<fmt:formatDate value="${startDate}" pattern="yyyy-MM-ddHH:mm:ss" var="openDate"/>      
					<fmt:formatDate value="${endDate}" pattern="yyyy-MM-ddHH:mm:ss" var="closeDate"/>
					<c:if test="${openDate > nowDate}">
		    			<a class="kakao txt-bold" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:$.alert('투표대기중 입니다.',function(a){});">
				           	투표대기
				        </a>	
					</c:if>	
		    		<c:if test="${openDate < nowDate && closeDate > nowDate}">
                      		<c:if test='${election.vote_cnt > 0}'>
			    				<a class="kakao txt-bold" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:$.alert('투표에 이미 참여 하였습니다.',function(a){});">
					            	투표완료
					            </a>
			    			</c:if>
			    			<c:if test='${election.vote_cnt == 0}'>
			    				<a class="kakao txt-bold" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:goVote('${election.election_id}');">
					            	투표하기
					            </a>	
			    			</c:if>	
					</c:if>	                        	
                     <c:if test="${closeDate < nowDate}">
                     	<c:if test='${election.vote_cnt > 0}'>
		    				<a class="kakao txt-bold" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:$.alert('투표에 이미 참여 하였습니다.',function(a){});">
				            	투표완료
				            </a>
		    			</c:if>
		    			<c:if test='${election.vote_cnt == 0}'>
		    				<a class="kakao txt-bold" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:$.alert('투표가 마감 되었습니다.',function(a){});">
				            	투표마감
				            </a>	
		    			</c:if> 		
					</c:if>
                	
                	
                	
		            
                	
                	
            	</div>
            <!-- <input type="file" style="display:inline-block; width:300px;" id="upload" name="bbsFile" value="일괄입력" class="btn-primary" onchange="javascript:fileUpload(this);"> -->
				<form id="excelForm" style="display:none" name="excelForm" method="post" enctype="multipart/form-data">
					<input type="file" name="bbsFile"  multiple id="upload" style="display:none; width:300px;" onchange="javascript:fileUpload('${allocationList[0].allocation_id}','${allocationList[0].allocation_status_cd}',this);">
				</form>            
                                
            </div>
     
     
	     
     
        <%-- <div id="carCheckImage" style="position:relative;">
        	<input type="hidden" id="allocationId" name="allocationId" value="${allocationList[0].allocation_id}" />
        	<img style="width:100%; margin-bottom:0px;" src="/img/checkCarImage.png" alt="">
        	<input type="checkbox" id="frontBumper" name="frontBumper" style="transform : scale(1.5); position:absolute; top:0%; left:43%;" ><!--앞범퍼  -->
        	<input type="checkbox" style="transform : scale(1.5); position:absolute; top:9%; left:9%;"/><!-- 좌휀다(전) -->
        	<input type="checkbox" style="transform : scale(1.5); position:absolute; top:9%; left:43%;"/><!-- 전판넬 -->
        	<input type="checkbox" style="transform : scale(1.5); position:absolute; top:9%; left:75%;"/><!-- 우휀다(전)  -->
        	<input type="checkbox" style="transform : scale(1.5); position:absolute; top:32%; left:20%;"/><!-- 하우스(좌) -->
        	<input type="checkbox" style="transform : scale(1.5); position:absolute; top:22%; left:44%;"/><!--본넷  -->
        	<input type="checkbox" style="transform : scale(1.5); position:absolute; top:32%; left:75%;"/><!-- 하우스(우) -->
        	<input type="checkbox" style="transform : scale(1.5); position:absolute; top:37.5%; left:0.5%;"/><!--사이드 스텝(좌)  -->
        	<input type="checkbox" style="transform : scale(1.5); position:absolute; top:41%; left:23.1%;"/><!--좌도어(전)  -->
        	<input type="checkbox" style="transform : scale(1.5); position:absolute; top:41%; left:74%;"/><!-- 우도어(전) -->
        	<input type="checkbox" style="transform : scale(1.5); position:absolute; top:37.5%; left:96%;"/><!-- 사이드 스텝(우)  -->
        	<input type="checkbox" style="transform : scale(1.5); position:absolute; top:56.5%; left:23.1%;"/><!-- 좌도어(후) -->
        	<input type="checkbox" style="transform : scale(1.5); position:absolute; top:54%; left:44%;"/><!-- 천정 -->
        	<input type="checkbox" style="transform : scale(1.5); position:absolute; top:56.5%; left:74%;"/><!-- 우도어(후) -->
        	<input type="checkbox" style="transform : scale(1.5); position:absolute; top:78%; left:44%;"/><!-- 트렁크 -->
        	<input type="checkbox" style="transform : scale(1.5); position:absolute; top:86%; left:22%;"/><!-- 좌휀다(후) -->
        	<input type="checkbox" style="transform : scale(1.5); position:absolute; top:84.5%; left:44%;"/><!-- 후판넬 -->
        	<input type="checkbox" style="transform : scale(1.5); position:absolute; top:86%; left:75%;"/><!-- 우휀다(후) -->
        	<input type="checkbox" style="transform : scale(1.5); position:absolute; top:94%; left:43%;"/><!-- 후범퍼 -->
        </div> --%>
        
        

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      <script src="/js/vendor/bootstrap.min.js"></script>       
    <!-- veiwport for countnumber -->
    <script src="/js/jquery.viewportchecker.js"></script>   
    <script src="/js/alert.js"></script> 
    <script src="/js/vendor/jquery.form.min.js"></script>
    
    <script src="/js/vendor/html2canvas.min.js"></script>
    <script src="/js/main.js"></script>       
    <script src="/js/messagebox.js"></script>
    <script src="/js/vendor/jquery.loading-indicator.min.js"></script>
    <script>
    
    var homeLoader;
    
    $(document).ready(function(){
    	
    	//$("#signature-pad").css('display','none');
    	
    	
    	//html2canvas(document.querySelector("#capture")).then(canvas => { document.body.appendChild(canvas) });
    	
    	
    	homeLoader = $('body').loadingIndicator({
			useImage: false,
			showOnInit : false
		}).data("loadingIndicator");

    	
    	
    });
       
    
    function capture(){
    	
    		html2canvas(document.querySelector("#sendImageForCustomer")).then(canvas => { 
    			// jsPDF 객체 생성 생성자에는 가로, 세로 설정, 페이지 크기 등등 설정할 수 있다. 자세한건 문서 참고. 
    			//var doc = new jsPDF('p', 'mm', 'a4');
    			// html2canvas의 canvas를 png로 바꿔준다. 
    			//var imgData = canvas.toDataURL('image/png'); 
    			//Image 코드로 뽑아내기 
    			// image 추가
    			//doc.addImage(imgData, 'PNG', 0, 0); 
    			// pdf로 저장
    			//doc.save('sample-file.pdf');
    			
    			document.body.appendChild(canvas);
    			
    			});
    	
    	
    }
    
    
    
    
    function logout(){
    	
    	$.confirm("로그아웃 하시겠습니까?",function(a){
    		 if(a){
    			 document.location.href = "/logout.do";	
    		 }
    	});
    	
    }  
    
    
    function goLiftCheckPage(allocationId,currentStatus){
    	document.location.href = "/carrier/lift-check.do?allocationId="+allocationId+"&allocationStatus="+currentStatus;
    }
    
    function fileUpload(allocationId,currentStatus,obj){
   	   	
    	//alert($("#upload").val());
    	
    	homeLoader.show();
    	$('#excelForm').ajaxForm({
			url: "/carrier/upLiftCheck.do",
			enctype: "multipart/form-data", 
		    type: "POST",
			dataType: "json",		
			data : {
				allocationId : allocationId,
				currentStatus : currentStatus
		    },
			success: function(data, response, status) {
				var status = data.resultCode;
				if(status == '0000'){
					$.alert("이미지가 등록 되었습니다.",function(a){
						 if(currentStatus == "Y"){			//다음 상태로 진행
							 currentStatus = "R";
		    			 }else if(currentStatus == "S"){		    				 
		    				 currentStatus = "D";
		    			 }else if(currentStatus == "P"){
		    				 currentStatus = "D";
		    			 }
						document.location.href = "/carrier/lift-check.do?allocationId="+allocationId+"&allocationStatus="+currentStatus;
					});
				}else if(status == '1111'){
							
				}
						
			},
			error: function() {
				alertEx("이미지 등록중 오류가 발생하였습니다.");
			}                               
		});
		$("#excelForm").submit();
		var updateHomeLoader = setInterval(function() {
			clearInterval(updateHomeLoader);
			homeLoader.hide();
		}, 300);
    	
    	
    	//$("#excelForm").attr("action","/carrier/upLiftCheck.do");
    	//$("#excelForm").submit();	
       	
    }
    
    
   	function updateAllocationStatus(allocationId,allocationStatus){
   		
   		$.ajax({ 
			type: 'post' ,
			url : "/allocation/updateAllocationStatus.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				allocationStatus : allocationStatus,
				allocationId : allocationId
			},
			success : function(data, textStatus, jqXHR)
			{
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
   		
   	}
    
    function carCheck(id,status){
    	
    	var nextStatus = "";
    	var chkStatus = "";
    	var addMsg = "";
    	
    	if(status == "Y"){
    		chkStatus = "상차검사";
    		addMsg = "(상차검사 이후에는 차량정보를 수정 할 수 없습니다.)";
    		//addMsg += "<br><p  style='color:#8B0000'>*본 절차는 탁송되는 차량의 상태를 확인 하기 위한 목적으로 사용됩니다.</p>";
    		addMsg += "<br><p  style='color:#8B0000'>*본 절차는 탁송되는 차량의 상태를 확인 하기 위한 목적으로 사용되며, 추후 문제 발생시 대응하기 위한 용도로 사용 될 수 있습니다.</p>";
    		var carKind = "${allocationList[0].car_kind}";
    		var carIdNum = "${allocationList[0].car_id_num}";
    		var carNum = "${allocationList[0].car_num}";
    		
    		if(carKind == ""){
    			$.alert("차종이 작성 되지 않았습니다.",function(a){
				});
    			return false;
    		}
    		if(carIdNum == "" && carNum == ""){
    			$.alert("차대번호 또는 차량번호 둘 중 하나는 반드시 작성 되어야 합니다.",function(a){
				});
    			return false;
    		}
		 }else if(status == "S"){
			 chkStatus = "하차검사";
			 addMsg += "";
		 }else if(status == "P"){
			 chkStatus = "하차검사";
		 }
   
  //  	 $.confirm(chkStatus+'를 진행 하시겠습니까?'+addMsg,function(a){
  //  		 if(a){
    			 if(status == "Y"){
    				 nextStatus = "R";
    			 }else if(status == "S"){
    				 nextStatus = "D";
    			 }else if(status == "P"){
    				 nextStatus = "D";
    			 }
    			 
    			 if(status == "S" || status == "Y" || status == "P"){
    				 
    				 homeLoader.show();
    				 updateAllocationStatus(id,nextStatus);
    				 document.location.href = "/carrier/lift-check.do?autoClick=Y&allocationId="+id+"&allocationStatus="+nextStatus;
    				 var updateHomeLoader = setInterval(function() {
    						clearInterval(updateHomeLoader);
    						homeLoader.hide();
    					}, 300);
    				 //$("#upload").trigger("click");			//20190305	상차 또는 하차 검사시 현재 화면에서 촬영 또는 이미지 선택하던 방식에서 페이지 변경 후 선택 하는 방식으로 변경
    				 
    				// $('html').scrollTop(0);
    				// $("#signature-pad").css('z-index','1');
    				// sign.on();
    			 }else{
    				 	 
    			 }
    			 
    //		 }
    //	});
    }
    
    function carCheckDetail(allocationId,status){
    	
    //	$.confirm("인계를 진행 하시겠습니까?",function(a){
    		
    //		if(a){
    			homeLoader.show();
    			updateAllocationStatus(allocationId,"I");
        		document.location.href = "/carrier/car-check-detail.do?allocationId="+allocationId;	
        		var updateHomeLoader = setInterval(function() {
        			clearInterval(updateHomeLoader);
        			homeLoader.hide();
        		}, 300);
    //		}else{
    			//$.alert("인계가 취소 되었습니다.",function(a){
    			//});
    //		}
    	
    	//});
    	
    }
    
    
    function carCheckDetailPass(allocationId,status){
    	
    //	$.confirm("인계를 생략 하시겠습니까?",function(a){
    		
    //		if(a){
    			
    			$.MessageBox({
	   				  input    : true,
	   				  buttonsOrder: "fail done",
		   			  buttonFail: "취소", 
	   				  buttonDone: "확인",
	   				  message  : "인계를 생략 하는 이유를 작성 해 주세요."
	   				}).done(function(data){
	   				  if ($.trim(data)) {
	   					var str = data;
	   					
	   					/* $.confirm("'"+data+"' 해당 내용으로 인계를 생략 하시겠습니까? 이 동작은 취소 할 수 없습니다.",function(b){ */
	   					
	   						/* if(b){ */
	   							homeLoader.show();
	   							$.ajax({ 
	   		   						type: 'post' ,
	   		   						url : "/allocation/updateAllocationReceiptSkip.do" ,
	   		   						dataType : 'json' ,
	   		   						async : false,	
	   		   						data : {
	   		   							receiptSkipReason : str,
	   		   							allocationId : allocationId
	   		   						},
	   		   						success : function(data, textStatus, jqXHR)
	   		   						{
	   		   							var result = data.resultCode;
	   		   							var resultData = data.resultData;
	   		   							if(result == "0000"){
	   		   								/* $.alert("인계가 생략 되었습니다.",function(c){ */
	   		   									updateAllocationStatus(allocationId,"F");
	   		   									//완료 화면으로 이동
	   		   									document.location.href = "/carrier/allocation-detail.do?allocationStatus=F&allocationId="+allocationId;
		   		   								var updateHomeLoader = setInterval(function() {
		   		   								clearInterval(updateHomeLoader);
		   		   								homeLoader.hide();
		   		   							}, 300);
	   		   								/* }); */
	   		   							}else if(result == "E000"){
	   		   								$.alert("인계를 생략 할 수 없습니다.",function(d){
	   		   									window.location.reload();
	   		   								});
	   		   							}else{
	   		   								$.alert("인계를 생략 하는데 실패 했습니다. 관리자에게 문의 하세요.",function(e){
	   		   									window.location.reload();
	   		   								});
	   		   							}
	   		   							
	   		   						} ,
	   		   						error : function(xhRequest, ErrorText, thrownError) {
	   		   						}
	   		   					});
	   						/* } */
	   						
	   					/* }); */
	   					
	   				  } else {
	   					
	   				  }
	   				});
    			
    			
    			
        		//document.location.href = "/carrier/car-check-detail.do?allocationId="+allocationId;	
 //   		}else{
    			
   // 		}
    	
   // 	});
    	
    }
    
  
    function carCheckSkip(allocationId,allocationStatus){
    	
    	var msg = "";
    	var nextStatus = "";
    	if(allocationStatus == "Y"){
    		msg = "상차검사를";
    		nextStatus = "S"
    	}else if(allocationStatus == "D"){
    		msg = "하차검사를";
    		nextStatus = "B"
    	}
    	
    	$.confirm(msg+"생략 하시겠습니까?",function(a){
    		if(a){
    			homeLoader.show();
    			updateAllocationStatus(allocationId,nextStatus);
        		if(nextStatus == "S"){
        			document.location.href = "/carrier/allocation-detail.do?allocationStatus="+nextStatus+"&allocationId="+allocationId;	
        		}else if(nextStatus == "B"){
        			document.location.href = "/carrier/allocation-detail.do?allocationStatus="+nextStatus+"&allocationId="+allocationId;	
        		}
        		var updateHomeLoader = setInterval(function() {
        			clearInterval(updateHomeLoader);
        			homeLoader.hide();
        		}, 300);	
    		}
    	});
    	
    	
    }
    
    
    
    
    function changeCarKind(allocationId){

    	$.MessageBox({
				  input    : true,
				  buttonsOrder: "fail done",
 			  buttonFail: "취소", 
				  buttonDone: "확인",
				  message  : "차종 변경"
				}).done(function(data){
				  if ($.trim(data)) {
					var str = data;
					
					$.confirm("차종을 "+str+"(으)로 변경 하시겠습니까? ",function(b){
					
						if(b){
							homeLoader.show();
							$.ajax({ 
		   						type: 'post' ,
		   						url : "/allocation/updateCarKind.do" ,
		   						dataType : 'json' ,
		   						async : false,
		   						data : {
		   							carKind : str,
		   							allocationId : allocationId
		   						},
		   						success : function(data, textStatus, jqXHR)
		   						{
		   							var result = data.resultCode;
		   							var resultData = data.resultData;
		   							if(result == "0000"){
		   								//$.alert("차종이 변경 되었습니다.",function(c){
		   									//완료 화면으로 이동
		   									document.location.href = "/carrier/allocation-detail.do?allocationStatus=A&allocationId="+allocationId;
		   									var updateHomeLoader = setInterval(function() {
		   										clearInterval(updateHomeLoader);
		   										homeLoader.hide();
		   									}, 300);
		   								//});
		   							}else if(result == "E000"){
		   								$.alert("차종을 변경 할 수 없습니다.",function(d){
		   									window.location.reload();
		   								});
		   							}else{
		   								$.alert("차종을 변경 하는데 실패 했습니다. 관리자에게 문의 하세요.",function(e){
		   									window.location.reload();
		   								});
		   							}
		   							
		   						} ,
		   						error : function(xhRequest, ErrorText, thrownError) {
		   						}
		   					});
						}
						
					});
					
				  } else {
					
				  }
				});
    	
    }



    function changeCarIdNum(allocationId,carIdNum){
    	
    	//alert("${allocationList[0].carInfo.driver_mod_yn}");
    	
    	
    	 if("${allocationList[0].carInfo.driver_mod_yn}" == "Y" || carIdNum == "" || carIdNum == null || carIdNum == undefined || ( carIdNum != null && typeof carIdNum == "object" && !Object.keys(carIdNum).length ) ){
    		//기사가 수정 한 경우 또는 차대번호가 빈값인경우는 입력을 할 수 있도록 한다.
    	}else{
    		//빈값이 아닌경우는 수정 할 수 없도록 한다.
    		$.alert("차대번호를 수정 할 수 없습니다. 배차 담당자에게 문의 하세요.",function(d){
		   	});
    		return false;
    	}

    	$.MessageBox({
			  input    : true,
			  buttonsOrder: "fail done",
		  buttonFail: "취소", 
			  buttonDone: "확인",
			  message  : "차대번호 변경"
			}).done(function(data){
			  if ($.trim(data)) {
				var str = data;
				var check = /[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/;
				if(check.test(str)){
					$.alert("차대번호에 한글이 포함되어 있습니다. 정확한 차대번호를 입력 하세요.",function(d){
					});
					return false;
				}						
				var pattern_spc = /[~!@#$%^&*()_+|<>?:{}]/; // 특수문자
				if(pattern_spc.test(str)){
					$.alert("차대번호에 특수문자가 포함되어 있습니다. 정확한 차대번호를 입력 하세요.",function(d){
					});
					return false;
				}
				
				var regType1 = /^[A-Za-z0-9+]{6,12}$/; 
				if(regType1.test(str)){
					
					$.confirm("차대번호를 "+str+"(으)로 변경 하시겠습니까? ",function(b){
						
						if(b){
							homeLoader.show();
							$.ajax({ 
		   						type: 'post' ,
		   						url : "/allocation/updateCarIdNum.do" ,
		   						dataType : 'json' ,
		   						async : false,
		   						data : {
		   							carIdNum : str,
		   							allocationId : allocationId
		   						},
		   						success : function(data, textStatus, jqXHR)
		   						{
		   							var result = data.resultCode;
		   							var resultData = data.resultData;
		   							if(result == "0000"){
		   								//$.alert("차대번호가 변경 되었습니다.",function(c){
		   									//완료 화면으로 이동
		   									document.location.href = "/carrier/allocation-detail.do?allocationStatus=A&allocationId="+allocationId;
		   									var updateHomeLoader = setInterval(function() {
		   										clearInterval(updateHomeLoader);
		   										homeLoader.hide();
		   									}, 300);
		   								//});
		   							}else if(result == "E000"){
		   								$.alert("차대번호를 변경 할 수 없습니다.",function(d){
		   									window.location.reload();
		   								});
		   							}else{
		   								$.alert("차대번호를 변경 하는데 실패 했습니다. 관리자에게 문의 하세요.",function(e){
		   									window.location.reload();
		   								});
		   							}
		   							
		   						} ,
		   						error : function(xhRequest, ErrorText, thrownError) {
		   						}
		   					});
						}
						
					});
					
				}else{
					$.alert("정확한 차대번호를 입력 하세요.",function(d){
					});
					return false;
				}
				
				
				
			  } else {
				
			  }
			});
    	
    }


    function changeCarNum(allocationId){
    	
    	$.MessageBox({
			  input    : true,
			  buttonsOrder: "fail done",
		  buttonFail: "취소", 
			  buttonDone: "확인",
			  message  : "차량번호 변경"
			}).done(function(data){
			  if ($.trim(data)) {
				var str = data;
				
				$.confirm("차량번호를 "+str+"(으)로 변경 하시겠습니까? ",function(b){
				
					if(b){
						homeLoader.show();
						$.ajax({ 
	   						type: 'post' ,
	   						url : "/allocation/updateCarNum.do" ,
	   						dataType : 'json' ,
	   						async : false,
	   						data : {
	   							carNum : str,
	   							allocationId : allocationId
	   						},
	   						success : function(data, textStatus, jqXHR)
	   						{
	   							var result = data.resultCode;
	   							var resultData = data.resultData;
	   							if(result == "0000"){
	   								//$.alert("차량번호가 변경 되었습니다.",function(c){
	   									//완료 화면으로 이동
	   									document.location.href = "/carrier/allocation-detail.do?allocationStatus=A&allocationId="+allocationId;
	   									var updateHomeLoader = setInterval(function() {
	   										clearInterval(updateHomeLoader);
	   										homeLoader.hide();
	   									}, 300);
	   								//});
	   							}else if(result == "E000"){
	   								$.alert("차량번호를 변경 할 수 없습니다.",function(d){
	   									window.location.reload();
	   								});
	   							}else{
	   								$.alert("차량번호를 변경 하는데 실패 했습니다. 관리자에게 문의 하세요.",function(e){
	   									window.location.reload();
	   								});
	   							}
	   							
	   						} ,
	   						error : function(xhRequest, ErrorText, thrownError) {
	   						}
	   					});
					}
					
				});
				
			  } else {
				
			  }
			});
    	
    }
        
    
    function addContent(allocationId){
    	
    	
    	$.MessageBox({
			  input    : true,
			  buttonsOrder: "fail done",
		  buttonFail: "취소", 
			  buttonDone: "확인",
			  message  : "비고 작성"
			}).done(function(data){
			  if ($.trim(data)) {
				var str = data;
				str = ''+str+''
				$.confirm("비고를 작성 하시겠습니까? ",function(b){
				
					if(b){
						homeLoader.show();
						$.ajax({ 
	   						type: 'post' ,
	   						url : "/allocation/updateCarInfoDriverEtc.do" ,
	   						dataType : 'json' ,
	   						async : false,
	   						data : {
	   							driverEtc : str,
	   							allocationId : allocationId
	   						},
	   						success : function(data, textStatus, jqXHR)
	   						{
	   							var result = data.resultCode;
	   							var resultData = data.resultData;
	   							if(result == "0000"){
	   								//$.alert("비고를 작성 하였습니다.",function(c){
	   									window.location.reload();
	   								//});
	   							}else if(result == "E000"){
	   								$.alert("비고를 작성 할 수 없습니다.",function(d){
	   									window.location.reload();
	   								});
	   							}else{
	   								$.alert("비고를 작성 하는데 실패 했습니다. 관리자에게 문의 하세요.",function(e){
	   									window.location.reload();
	   								});
	   							}
	   							
	   						} ,
	   						error : function(xhRequest, ErrorText, thrownError) {
	   						}
	   					});
					}
					
				});
				
			  } else {
				
			  }
			});
    	
    	
    }
    
    
    
    
    
    
    function allocationAccept(id,status){
    	//alert(allocationId);
    	var msg = "";
    	var addMsg = "";
    	if(status == "Y"){
    		msg = "승인";
    		//addMsg = "(승인 이후에는 차량정보 '차종,차대번호,차량번호' 를 변경 할 수 없습니다.)";
    	}else{
    		msg = "탁송 완료";
    	}
    	
    	//window.Android.toastShort( "JavscriptInterface Test" );
    	
    	//$.confirm("해당 배차를 "+msg+" 하시겠습니까?"+addMsg,function(a){
	   	//	 if(a){
	   		homeLoader.show();
	   			$.ajax({ 
					type: 'post' ,
					url : "/allocation/updateAllocationStatus.do" ,
					dataType : 'json' ,
					async : false,
					data : {
						allocationStatus : status,
						allocationId : id
					},
					success : function(data, textStatus, jqXHR)
					{
						var result = data.resultCode;
						var resultData = data.resultData;
						if(result == "0000"){
							//alert(msg+"되었습니다.");
				//			$.alert(msg+"되었습니다.",function(a){
								//document.location.href = "/carrier/main.do";
								document.location.href = "/carrier/list.do?allocationStatus=${allocationStatus}";
			//				});
						}else if(result == "E000"){
							$.alert("해당 배차를 "+msg+" 할 수 없습니다.",function(a){
								document.location.href = "/carrier/list.do?allocationStatus=${allocationStatus}";
							});
						}else{
							$.alert("해당 배차를 "+msg+" 하는데 실패 했습니다. 관리자에게 문의 하세요.",function(a){
								document.location.href = "/carrier/list.do?allocationStatus=${allocationStatus}";
							});
						}
						
					} ,
					error : function(xhRequest, ErrorText, thrownError) {
					}
				});	
	   			var updateHomeLoader = setInterval(function() {
	   				clearInterval(updateHomeLoader);
	   				homeLoader.hide();
	   			}, 300);
	   	//	 }
	   	//});
    	
    }
    
    
    
 
    
    </script>
    
    
    
    
    
    
    
    
    </body>
</html>
