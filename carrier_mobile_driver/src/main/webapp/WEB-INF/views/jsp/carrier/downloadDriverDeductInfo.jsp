<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko"  style=" height:100%;">      
<head>
    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    	<title></title>
    	<meta name="description" content="">
    	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
    	<!-- <meta name="viewport" content="width=720px, user-scalable=no"> -->
    	<link rel="stylesheet" href="/css/bootstrap.min.css">
    	<link rel="stylesheet" href="/css/bootstrap-theme.min.css">
    	<link rel="stylesheet" href="/css/owl.carousel.min.css">
    	<link rel="stylesheet" href="/css/owl.theme.default.min.css">
    	<link rel="stylesheet" href="/css/notosanskr.css">
    	<link rel="stylesheet" href="/css/main.css">
    	<link rel="stylesheet" href="/css/messagebox.css">
    	<script src="/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    	<script src="/js/vendor/jquery-1.11.2.min.js"></script>
</head>   
<body style=" height:100%;">


<script type="text/javascript">  

$(document).ready(function(){
	
/* 	var updateToken = setInterval( function() {
		
		if(getDeviceToken() != null && getDeviceToken() != ""){
			updateDriverDeviceToken(getDeviceToken());
			if(window.Android != null){
				window.Android.startService();
			}
			clearInterval(updateToken);
		}
   }, 1000); */
	
});



function getDeviceToken(){
	
	var token = "";
	try{
		if(window.Android != null){
			token = window.Android.getDeviceToken("${driverId}");
		}	
	}catch(exception){
		
	}finally{
		
	}
	return token;
}  
  

function updateDriverDeviceToken(token){
	
	
	if(token != ""){
		$.ajax({ 
			type: 'post' ,
			url : "/updateDriverDeviceToken.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				token : token
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					//$.alert("성공",function(a){
					//});
				}else if(result == "E000"){
					
				}else{
					
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
}  



 function getNewsData(obj,keyword){
	
	 
	// alert($("#selectedStatus").children().length);
	 
	//alert($("#selectedStatus").children().first().get(0));	
	//alert($(obj).parent().get(0));
	
 	 if($("#selectedStatus").children().first().get(0) == $(obj).parent().get(0)){				//전체를 클릭 한경우
					
 		$("#selectedStatus").children().each(function(index,element){
 			$(this).removeClass('selected');
 		});
 		$(obj).parent().addClass('selected');
	}else{
			
	} 
	
	
	
/* 	
	
	$.ajax({ 
		type: 'post' ,
		url : "/research/newsSearch.do?&keyword="+keyword ,
		dataType : 'json' ,
		data : {
			
		},
		success : function(data, textStatus, jqXHR)
		{
			alert(data);
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
	 */
	
	
} 

 function goNewsPage(uri){
		
		//var replaceUrlStr = replaceAll(url, "?", "^");
		//replaceUrlStr = replaceAll(replaceUrlStr, "=", "+");
		
		var replaceUrlStr = encodeURIComponent(uri)
		document.location.href = "/news/newsPage.do?newsUri="+replaceUrlStr;
	}  


    
function viewAllocationData(allocationId,allocationStatus){
	document.location.href = "/carrier/cal-list-detail.do";
} 
 
 
function viewAllocationList(allocationId,allocationStatus){
	
	document.location.href = "/carrier/group-list.do?allocationId="+allocationId+"&allocationStatus="+allocationStatus;
	
} 
 
 
 
function logout(){
	
	$.confirm("로그아웃 하시겠습니까?",function(a){
		 if(a){
			 if(window.Android != null){
					window.Android.stopService("${driverId}");
				}	
			 document.location.href = "/logout.do";	
		 }
	});
	
}   
 
 
 
function backKeyController(str){
	//history.go(-1);
	//location.href = document.referrer;
	history.go(-1);
}
 
</script>


        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div class="content-container" style="height:100%; overflow-y:hidden;" >
            <!-- <header class="bg-pink clearfix" style="background-color:#fff; border:none; text-align:center; "> -->
            <header class="bg-pink clearfix" style="position:fixed; top:0px; background-color:#fff; border:none; text-align:center; ">
                <div class="" style="width:19%;">
                    <a href="javascript:history.go(-1);"><img style="zoom: 0.5;" src="/img/back-icon.png" alt=""></a>
                </div> 
                <div class="" style="width:60%;">
                	<!-- <img style="width:90%; height:60%;" src="/img/main_logo.png" alt=""> -->
                	<img style="width:90%; height:60%;" onclick="javascript:document.location.href='/carrier/main.do'" src="/img/main_logo.png" alt="">
                </div>
                <div class="" style="width:19%; float:right;">
                   <a style="cursor:pointer;" href="javascript:logout();"><img style="" src="/img/logout.png" alt=""></a>
                </div>
            </header>
            <div style="clear:both;"></div>
            <!-- <div class="menu-container bg-pink">
                <span class="img-holder">
                    <img src="/img/user-img.png" alt="">
                </span>
                 <a class="kakao txt-bold" style="cursor:pointer;" onclick="javascript:kakaoLogin();">
                    카카오계정으로 로그인
                </a>
            </div> -->
            <div style=" text-align:right; clear:both;">&nbsp;</div>
            <div class="main-menu-container" style="margin-top:9%; height:95%;">
            	<div style=" text-align:right; clear:both;">${user.driver_name}님 환영 합니다.</div>
            	<div  id="container"  style="height:100%; margin-top:2%; background-color:#eee;">
            		
				<div style="margin-top:3%; width:100%; height:95%;">
                	<!-- <iframe src="http://drive.google.com/viewerng/viewer?embedded=true&url=http://52.78.153.148:8081/files/forms/deductlist.xlsx" width="100%" height="100%"></iframe> -->
                	<!-- <iframe src="http://docs.google.com/gview?embedded=true&url=http://52.78.153.148:8081/files/forms/deductlist.xlsx" width="100%" height="100%"></iframe> -->
                
                <%-- <iframe src="http://docs.google.com/gview?embedded=true&url=http://52.78.153.148:8081/files/deduct_list${driverDeductFile.driver_deduct_file_path}" width="100%" height="100%;"></iframe> --%>
                <iframe src="https://view.officeapps.live.com/op/embed.aspx?src=http://www.hkai.co.kr/files/deduct_list${driverDeductFile.driver_deduct_file_path}" width="100%;"  height="100%;" ></iframe>
                
                
                <!-- <iframe src="https://docs.google.com/gview?url=http://writing.engr.psu.edu/workbooks/formal_report_template.doc&embedded=true" width="100%" height="100%"></iframe> -->
                
                <!-- <iframe src="https://docs.google.com/gview?url=http://52.78.153.148:8081/files/forms/test.pptx&embedded=true" width="100%" height="100%"></iframe> -->
                <!-- <iframe src="https://docs.google.com/gview?url=http://52.78.153.148:8081/files/forms/deductlist.xlsx&embedded=true"  width="100%" height="100%" ></iframe> -->
                
                
                </div>
                </div>
            </div>
        </div>


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      <script src="/js/vendor/bootstrap.min.js"></script>       
    <!-- veiwport for countnumber -->
    <script src="/js/alert.js"></script>
    <script src="/js/main.js"></script>   
        <script src="/js/vendor/xpull.js"></script>       
    
    
    <script>
    
	
  
    
    </script>
        
    </body>
</html>
