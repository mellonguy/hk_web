<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

	String adviserGrade = request.getAttribute("adviserGrade").toString();
//out.print(adviserGrade);
%>

<!DOCTYPE html>
<html lang="ko">      
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
    
<body class="black">
   
        
<script type="text/javascript">  


var selectedGrade = "S";

$(document).ready(function(){
	
	//getAdviserList("S",0);
	
});



function goAdviserDetailPage(adviserCd){
	
	document.location.href = "/adviser/adviserDetail.do?adviserCd="+adviserCd;
	
}

function getAdviserList(adviserGrade,idx){
	
	
	$("#adviserList").html("");
	
	$.ajax({ 
		type: 'post' ,
		url : "/adviser/selectAdviserList.do" ,
		dataType : 'json' ,
		async : false,
		data : {
			adviserGrade : adviserGrade
		},
		success : function(data, textStatus, jqXHR)
		{
			
			if(data.resultCode == "0000"){
			
				var result = "";
				var resultData = data.resultData;
				
				$("#adviserGradeSel").children().each(function(index,element){
					$(this).removeClass("active");
				});
				$("#adviserGradeSel").children().eq(idx).addClass("active");
				
				if(resultData.length > 0){
					for(var i = 0; i < resultData.length; i++){
						result += '<a onclick="javascript:goAdviserDetailPage(\''+resultData[i].adviser_cd+'\')" class="item">'; 
						result += '<span class="title">'+resultData[i].adviser_name+'</span>';
						result += '<span class="subinfo">'+resultData[i].adviser_comment+'</span>';
						result += '</span>';
						result += '</a>';
					}	
				}else{				//리스트가 없을때 처리
					
				}
				
				selectedGrade = adviserGrade;
				
				$("#adviserList").append(result);
				
			}else{
				alert("조회 실패");	
			}
			
			
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	}); 
	
}



function altMessge(){
	
	var per = "";
	if(selectedGrade == "S"){
		per = "0.1%";
	}else if(selectedGrade == "A"){
		per = "0.5%";
	}else if(selectedGrade == "B"){
		per = "1%";
	}else if(selectedGrade == "C"){
		per = "3%";
	}else if(selectedGrade == "D"){
		per = "5%";
	}
	
	alert(selectedGrade+"등급 로보어드바이저는 예상수익 상위 "+per+"의 종목을 찾아 냅니다.");
	
}







function goSearch(){
		
	document.location.href = "/search/search.do";
	
}

</script>        
        
        

        <div class="content-container interest-page black roboadvisor-page">
            <header  style="background-color:#1e212a; position:fixed; top:0px; z-index:10000;">
            	<!-- <div id="fixedMenu" style="background-color:#1e212a; position:fixed; top:0px; z-index:10000;"> -->
	                <div class="menu-bar">
	                    <a href="/menu/menu.do"><img src="/img/bar-icon.png" alt=""></a>
	                </div>
	                <div class="notif-icon">
	                    <a href="#"><img src="/img/bell2-icon.png" alt=""></a> 
	                </div>
	                <div class="search-container">
	                    <a href="#" class="search-input-icon">
	                        <img src="/img/search-input-icon.png" alt="">
	                    </a>
	                    <input type="text" name="" id="" onclick="javascript:goSearch();" placeholder="종목 혹은 테마를 검색해주세요">
	                </div>
                <!-- </div> -->
            </header>
            <div class="view-option" style="margin-top:55px;" >
                <ul>
                    <li class="option interest ">
                        <a href="/interest/interest.do">시세알림</a>  
                    </li>
                    <li class="option advisor active">
                        <a href="/adviser/adviser.do">로보어드바이저</a> 
                    </li>
                    <li class="option  research ">
                        <a href="/research/theme.do">테마</a> 
                    </li>
                </ul>
            </div>
            <div class="roboadvisor-stats clearfix">
            		<c:if test="${kospiCmpprevddTpCd == '0' || kospiCmpprevddTpCd == '1' || kospiCmpprevddTpCd == '2'}">
                         <div class="pull-left stat-box red text-center">
		                    <span class="lbl">코스피</span>
		                    <span class="val">${kospiTrdPrc}</span>
		                    <span class="percent">${kospiCmpprevddPrc}&nbsp; +${kospiCmpprevddPer}%</span>
		                </div>
                        </c:if>
                        <c:if test="${kospiCmpprevddTpCd == '3' || kospiCmpprevddTpCd == '6' || kospiCmpprevddTpCd == '7' || kospiCmpprevddTpCd == '8' || kospiCmpprevddTpCd == '9'}">
                         <div class="pull-left stat-box red text-center">
		                    <span class="lbl">코스피</span>
		                    <span class="val">${kospiTrdPrc}</span>
		                    <span class="percent">${kospiCmpprevddPrc}&nbsp; ${kospiCmpprevddPer}%</span>
		                </div>
                        </c:if>
                        <c:if test="${kospiCmpprevddTpCd == '4' || kospiCmpprevddTpCd == '5'}">
                         <div class="pull-left stat-box blue text-center">
		                    <span class="lbl">코스피</span>
		                    <span class="val">${kospiTrdPrc}</span>
		                    <span class="percent">${kospiCmpprevddPrc}&nbsp; ${kospiCmpprevddPer}%</span>
		                </div>
                        </c:if>
  
            
            		<c:if test="${kosdaqCmpprevddTpCd == '0' || kosdaqCmpprevddTpCd == '1' || kosdaqCmpprevddTpCd == '2'}">
                         <div class="pull-left stat-box red text-center">
		                    <span class="lbl">코스닥</span>
		                    <span class="val">${kosdaqTrdPrc }</span>
		                    <span class="percent">${kosdaqCmpprevddPrc }&nbsp; +${kosdaqCmpprevddPer}%</span>
		                </div>
                        </c:if>
                        <c:if test="${kosdaqCmpprevddTpCd == '3' || kosdaqCmpprevddTpCd == '6' || kosdaqCmpprevddTpCd == '7' || kosdaqCmpprevddTpCd == '8' || kosdaqCmpprevddTpCd == '9'}">
                         <div class="pull-left stat-box red text-center">
		                    <span class="lbl">코스닥</span>
		                    <span class="val">${kosdaqTrdPrc }</span>
		                    <span class="percent">${kosdaqCmpprevddPrc }&nbsp; ${kosdaqCmpprevddPer}%</span>
		                </div>
                        </c:if>
                        <c:if test="${kosdaqCmpprevddTpCd == '4' || kosdaqCmpprevddTpCd == '5'}">
                         <div class="pull-left stat-box blue text-center">
		                    <span class="lbl">코스닥</span>
		                    <span class="val">${kosdaqTrdPrc }</span>
		                    <span class="percent">${kosdaqCmpprevddPrc }&nbsp; ${kosdaqCmpprevddPer}%</span>
		                </div>
                        </c:if>
            
               <!--  <div class="pull-left stat-box red text-center">
                    <span class="lbl">코스피</span>
                    <span class="val">2,465.20</span>
                    <span class="percent">4.79 +3.5%</span>
                </div>
                <div class="pull-left stat-box blue text-center">
                    <span class="lbl">코스닥</span>
                    <span class="val">868.00</span>
                    <span class="percent">4.79 +3.5%</span>
                </div> -->
            </div>
            <div class="roboadvisor-event-list">
                <span class="heading" onclick="javascript:altMessge();">로보 어드바이저 종목</span>
                <div class="clearfix">
                    <div class="pull-left leftbox">
                        <ul id="adviserGradeSel">
                            <li class="active"><a href="javascript:getAdviserList('S',0)">S등급</a></li>
                            <li><a onclick="javascript:getAdviserList('A',1)">A등급</a></li>
                            <li><a onclick="javascript:getAdviserList('B',2)">B등급</a></li>
                            <li><a onclick="javascript:getAdviserList('C',3)">C등급</a></li>
                            <li><a onclick="javascript:getAdviserList('D',4)">D등급</a></li>
                        </ul>
                    </div>
                    <div class="pull-left rightbox" id="adviserList">
                        <!-- <a href="#" class="item">
                            <span class="title">주락펴락 시그널-S 3호</span>
                            <span class="subinfo">보락 종목 발굴 후 60% 급등! 바로 다음 남북경협주는?
                            </span>
                        </a>
                        <a href="#" class="item">
                            <span class="title">주락펴락 시그널-S 2호</span>
                            <span class="subinfo">보락 종목 발굴 후 60% 급등!</span>
                        </a>
                        <a href="#" class="item">
                            <span class="title">주락펴락 시그널-S 1호</span>
                            <span class="subinfo">보락 종목 발굴 후 60% 급등!</span>
                        </a>
                        <a href="#" class="item">
                            <span class="title">주락펴락 시그널-S 5호</span>
                            <span class="subinfo">보락 종목 발굴 후 60% </span>
                        </a>
                        <a href="#" class="item">
                            <span class="title">주락펴락 시그널-S 6호</span>
                            <span class="subinfo">보락 종목 발굴 후 60%</span>
                        </a>
                        <a href="#" class="item">
                            <span class="title">주락펴락 시그널-S 6호</span>
                            <span class="subinfo">바로 다음 남북경협주는?</span>
                        </a> -->
                    </div>
                </div>
            </div>
        </div>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      <script src="/js/vendor/bootstrap.min.js"></script>       
    <!-- veiwport for countnumber -->
    <script src="/js/main.js"></script>       
    </body>
</html>
