<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko"  style=" height:100%;">      
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
    
<body style=" height:100%;">


<script type="text/javascript">  

$(document).ready(function(){
	
/* 	var updateToken = setInterval( function() {
		
		if(getDeviceToken() != null && getDeviceToken() != ""){
			updateDriverDeviceToken(getDeviceToken());
			if(window.Android != null){
				window.Android.startService();
			}
			clearInterval(updateToken);
		}
   }, 1000); */
	
});



function getDeviceToken(){
	
	var token = "";
	try{
		if(window.Android != null){
			token = window.Android.getDeviceToken("${driverId}");
		}	
	}catch(exception){
		
	}finally{
		
	}
	return token;
}  
  

function updateDriverDeviceToken(token){
	
	
	if(token != ""){
		$.ajax({ 
			type: 'post' ,
			url : "/updateDriverDeviceToken.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				token : token
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					//$.alert("성공",function(a){
					//});
				}else if(result == "E000"){
					
				}else{
					
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
}  



 function getNewsData(obj,keyword){
	
	 
	// alert($("#selectedStatus").children().length);
	 
	//alert($("#selectedStatus").children().first().get(0));	
	//alert($(obj).parent().get(0));
	
 	 if($("#selectedStatus").children().first().get(0) == $(obj).parent().get(0)){				//전체를 클릭 한경우
					
 		$("#selectedStatus").children().each(function(index,element){
 			$(this).removeClass('selected');
 		});
 		$(obj).parent().addClass('selected');
	}else{
			
	} 
	
	
	
/* 	
	
	$.ajax({ 
		type: 'post' ,
		url : "/research/newsSearch.do?&keyword="+keyword ,
		dataType : 'json' ,
		data : {
			
		},
		success : function(data, textStatus, jqXHR)
		{
			alert(data);
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
	 */
	
	
} 

 function goNewsPage(uri){
		
		//var replaceUrlStr = replaceAll(url, "?", "^");
		//replaceUrlStr = replaceAll(replaceUrlStr, "=", "+");
		
		var replaceUrlStr = encodeURIComponent(uri)
		document.location.href = "/news/newsPage.do?newsUri="+replaceUrlStr;
	}  


    
function viewAllocationData(decideMonth){
	homeLoader.show();
	document.location.href = "/carrier/cal-list-detail.do?decideMonth="+decideMonth;
	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
} 
 
 
function viewAllocationList(allocationId,allocationStatus){
	homeLoader.show();
	document.location.href = "/carrier/group-list.do?allocationId="+allocationId+"&allocationStatus="+allocationStatus;
	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
} 
 
 
 
function logout(){
	
	$.confirm("로그아웃 하시겠습니까?",function(a){
		 if(a){
			 homeLoader.show();
			 if(window.Android != null){
					window.Android.stopService("${driverId}");
				}	
			 document.location.href = "/logout.do";	
		 }
	});
	
}   
 
 
 
function backKeyController(str){
	//history.go(-1);
	//location.href = document.referrer;
	homeLoader.show();
	document.location.href='/carrier/main.do';
}
 
 
 
function viewItem(decideMonth,item){
	homeLoader.show();
	document.location.href = "/carrier/cal-item.do?decideMonth="+decideMonth+"&item="+item;
	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
} 
 
 
function viewElectionData(electionId){

	homeLoader.show();
	document.location.href = "/carrier/viewElection.do?electionId="+electionId;
	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
}
 
 
 
 
</script>


        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div class="content-container" style=" height:auto; overflow-y:hidden;">
            <%-- <jsp:include page="/WEB-INF/views/jsp/common-top.jsp"></jsp:include> --%>
            <header class="bg-pink clearfix" style="position:fixed; background-color:#fff; border:none; text-align:center; ">
                <div class="" style="width:19%;">
                    <a href="/carrier/main.do"><img style="zoom: 0.5;" src="/img/back-icon.png" alt=""></a> 
                </div> 
                <div class="" style="width:60%;">
                	<img style="width:90%; height:60%;" onclick="javascript:document.location.href='/carrier/main.do'" src="/img/main_logo.png" alt="">
                </div>
                <div class="" style="width:19%; float:right;">
                   <a style="cursor:pointer;" href="javascript:logout();"><img style="" src="/img/logout.png" alt=""></a>
                </div>
            </header>
            <%-- <div style="text-align:right;">${user.driver_name}님 환영 합니다.</div> --%>
            <div style=" text-align:right; clear:both;">&nbsp;</div>
            <div class="content-container interest-page loadingthemepage news" style="position:fixed; background-color:#fff; clear:both; margin-top:50px;">
	            <!-- <div class="cat-list" style="background-color:#fff">
	                <ul class="clearfix" style="background-color:#fff">
	                	<li><a style="color:#000; width:15%;">출발일</a></li>
	                    <li><a style="color:#000;">상차지</a></li>
	                    <li><a style="color:#000;">하차지</a></li>
	                    <li><a style="color:#000;">차종</a></li>
	                    <li><a style="color:#000;">차대번호</a></li>
	                </ul>
	            </div> -->
	            <div style="width:100%; margin-top:5px; height:40px; border-bottom:1px solid #eee;">
                   <div style="width:15%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">대상</div></div>
	               <div style="width:40%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">제목</div></div>
	               <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">참여현황</div></div>
	               <div style="width:15%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">상태</div></div>
	               <div style="width:10%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">참여수</div></div>
                </div>
            </div>
            <div id="container" class="search-result news-list content-box" style="height:100%; margin-top:21%; overflow-y:scroll;">
            	<!-- <div class="xpull">
				    <div class="xpull__start-msg">
				        <div class="xpull__start-msg-text">화면을 당겼다 놓으면 새로 고침 됩니다.</div>
				        <div class="xpull__arrow"></div>
				    </div>
				    <div class="xpull__spinner">
				        <div class="xpull__spinner-circle"></div>
				    </div>
				</div> -->
            	<div class="news-container clearfix" style="height:450%;">
    	       		<c:forEach var="data" items="${electionList}" varStatus="status">
			           	<div style="width:100%; margin-top:5px; height:40px; border-bottom:1px solid #eee;"  onclick="javascript:viewElectionData('${data.election_id}');">
					        <div style="width:15%; text-align:center; height:40px; float:left;">
					        	<div style="margin-top:10px; text-align:center; display:inline-block;">
					        		<c:if test='${data.distribute_division eq "A" }'>
	                            		전체
	                            	</c:if>
	                            	<c:if test='${data.distribute_division eq "O" }'>
	                            		본사 전체
	                            	</c:if>
	                            	<c:if test='${data.distribute_division eq "D" }'>
	                            		기사 전체
	                            	</c:if>
	                            	<c:if test='${data.distribute_division eq "S" }'>
	                            		셀프
	                            	</c:if>
	                            	<c:if test='${data.distribute_division eq "C" }'>
	                            		캐리어
	                            	</c:if>
					        	</div>
					        </div>
						    <div style="width:40%; text-align:center; height:40px; float:left;"><div style="width:100%; margin-top:10px; text-align:center; display:inline-block; text-overflow:ellipsis; white-space:nowrap; overflow:hidden;"><nobr>${data.election_title}</nobr></div></div>
						    <div style="width:20%; text-align:center; height:40px; float:left;">
						    	<div style="width:100%; margin-top:10px; text-align:center; display:inline-block; text-overflow:ellipsis; white-space:nowrap; overflow:hidden;">
						    		<nobr>
						    			<c:if test='${data.vote_cnt > 0}'>
						    				참여완료
						    			</c:if>
						    			<c:if test='${data.vote_cnt == 0}'>
						    				미참여
						    			</c:if>
						    		</nobr>
						    	</div>
						    </div>
						    
						    <div style="width:15%; text-align:center; height:40px; float:left;">
						    	<div style="width:100%; margin-top:10px; text-align:center; display:inline-block; text-overflow:ellipsis; white-space:nowrap; overflow:hidden;">
						    		<jsp:useBean id="now" class="java.util.Date" />
	                   				<fmt:parseDate value="${data.start_dt}${data.start_time}" pattern="yyyy-MM-ddHH:mm:ss" var="startDate" />
									<fmt:parseDate value="${data.end_dt}${data.end_time}" pattern="yyyy-MM-ddHH:mm:ss" var="endDate" />
									<fmt:formatDate value="${now}" pattern="yyyy-MM-ddHH:mm:ss" var="nowDate" />								
									<fmt:formatDate value="${startDate}" pattern="yyyy-MM-ddHH:mm:ss" var="openDate"/>      
									<fmt:formatDate value="${endDate}" pattern="yyyy-MM-ddHH:mm:ss" var="closeDate"/>
									<c:if test="${openDate > nowDate}">
		                        		<nobr>투표대기</nobr>
									</c:if>	
						    		<c:if test="${openDate < nowDate && closeDate > nowDate}">
		                        		<nobr>투표중</nobr>	
									</c:if>	                        	
		                        	<c:if test="${closeDate < nowDate}">
		                        		<nobr>투표마감</nobr>
									</c:if>
						    		
						    	</div>
						    </div>
						    <div style="width:10%; text-align:center; height:40px; float:left;">
						    	<div style="width:100%; margin-top:10px; text-align:center; display:inline-block; text-overflow:ellipsis; white-space:nowrap; overflow:hidden;">
						    		<nobr>${data.join_count }</nobr>
						    	</div>
						    </div>
						    
			            </div>
			            
			         <%-- <c:if test="${data.decide_final_y_cnt == data.cnt}"> --%>   
			            <%-- <div class="overview-box active content-view" style=" clear:both; margin-top:10px;">
                	
                	
                   <div class="view-details" style="border-bottom:none;">
                       <div class="overview detail-box">
                         <span class="price">한국카캐리어 청구내역</span>
                           
                       <div class="detail-box" style="border-bottom:2px solid #404040;" onclick="javascript:viewItem('${data.decide_month}','1');">
                           <div class="other-info d-table">
                               <div class="eps left t-cell">
                                   <span class="label-text">합계</span>
                                   <span class="value">${data.result}</span>
                               </div>
                               <div class="per t-cell right">
                               		<span class="label-text"></span>
                                   <span class="value">${data.result}</span>
                               </div>
                           </div>
                       </div>
                   
                           
                           
                           
                       </div>
                   </div>
                   </div> --%>
                   
                   <%-- <div class="overview-box active content-view" style=" clear:both; margin-top:10px;">
                	
                	
                   <div class="view-details" style="border-bottom:none;">
                       <div class="overview detail-box">
                   	<span class="price">세금계산서 발급내역</span>
                           
                       <div class="detail-box" style="border-bottom:2px solid #404040;">
                           
                           <div class="other-info d-table">
                               <div class="eps left t-cell">
                                   <span class="label-text">공급가액</span>
                               </div>
                               <div class="per t-cell right">
                               		<span class="label-text"></span>
                                   <span class="value">${data.sum}</span>
                               </div>
                           </div>
                           <div class="other-info d-table">
                               <div class="eps left t-cell">
                                   <span class="label-text">부가세</span>
                               </div>
                               <div class="per t-cell right">
                               		<span class="label-text"></span>
                                   <span class="value">${data.vat}</span>
                               </div>
                           </div>
                           <div class="other-info d-table">
                               <div class="eps left t-cell">
                                   <span class="label-text">계</span>
                               </div>
                               <div class="per t-cell right">
                               		<span class="label-text"></span>
                                   <span class="value" style="color:#8B0000;">${data.subTotal}</span>
                                    <div style="color:#8B0000; font-size:4px;">※해당 금액으로 세금계산서 발행 부탁 드립니다.</div>		  
                               </div>
                           </div>
                       </div>
                       </div>
                   </div>
                   </div> --%>
			          
			            
			    <%-- <div class="overview-box active content-view" style=" clear:both; margin-top:10px;" onclick="javascript:viewItem('${data.decide_month}','2');">
                   <div class="view-details" style="border-bottom:none;">
                       <div class="overview detail-box">
                         <span class="price">대납 / 선지급 내역</span>
                           
                       <div class="detail-box" style="border-bottom:2px solid #404040;">
                           <div class="other-info d-table">
                               <div class="eps left t-cell">
                                   <span class="label-text">대납총액</span>
                                   <span class="value">${data.result}</span>
                               </div>
                               <div class="per t-cell right">
                               		<span class="label-text"></span>
                                   <span class="value">${data.item2}</span>
                               </div>
                           </div>
                       </div>
                    </div>
                   </div>
                   </div> --%>
                   
                   <%-- <div class="overview-box active content-view" style=" clear:both; margin-top:10px;">
                   <div class="view-details" style="border-bottom:none;">
                       <div class="overview detail-box">  
                       	<span class="price">운송비 최종 지급</span>                         
                       <div class="detail-box" style="border-bottom:2px solid #404040;">
                           <div class="other-info d-table">
                               <div class="eps left t-cell">
                                   <span class="label-text">합계</span>
                                   <span class="value">${data.result}</span>
                               </div>
                               <div class="per t-cell right">
                               		<span class="label-text"></span>
                                   <span class="value">${data.finalAmount}</span>
                               </div>
                           </div>
                       </div>
                    </div>
                   </div>
                   </div> --%>
			            
			            <%-- </c:if> --%>
			            
			            
	                </c:forEach>
                 </div>
            </div>
        </div>


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      <script src="/js/vendor/bootstrap.min.js"></script>       
    <!-- veiwport for countnumber -->
    <script src="/js/alert.js"></script>
    <script src="/js/main.js"></script>   
    <script src="/js/vendor/xpull.js"></script>       
    <script src="/js/vendor/jquery.loading-indicator.min.js"></script>
    
    <script>
    
 var homeLoader;
    
    $(document).ready(function(){
    
    	homeLoader = $('body').loadingIndicator({
			useImage: false,
			showOnInit : false
		}).data("loadingIndicator");
	

    	
    });
	
/* 	$('#container').xpull({
	    'paused': false, 
	    // Pull threshold - amount in  pixels required to pull to enable release callback
        'pullThreshold':50,
        // Max pull down element - amount in pixels
        maxPullThreshold: 50,
        // timeout in miliseconds after which the loading indicator stops spinning.
        // If set to 0 - the loading will be indefinite
        'spinnerTimeout':1000,
        onPullStart: function(){},
        onPullEnd: function(){},
        callback: function(){window.location.reload()},

	});   */ 
    
    </script>
        
    </body>
</html>
