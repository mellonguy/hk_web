<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko"  style=" height:100%;">      
<head>
    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    	<title></title>
    	<meta name="description" content="">
    	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
    	<!-- <meta name="viewport" content="width=720px, user-scalable=no"> -->
    	<link rel="stylesheet" href="/css/bootstrap.min.css">
    	<link rel="stylesheet" href="/css/bootstrap-theme.min.css">
    	<link rel="stylesheet" href="/css/owl.carousel.min.css">
    	<link rel="stylesheet" href="/css/owl.theme.default.min.css">
    	<link rel="stylesheet" href="/css/notosanskr.css">
    	<link rel="stylesheet" href="/css/main.css">
    	<link rel="stylesheet" href="/css/messagebox.css">
    	<script src="/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    	<script src="/js/vendor/jquery-1.11.2.min.js"></script>
</head>   
<body style=" height:100%;">


<script type="text/javascript">  

$(document).ready(function(){
	
/* 	var updateToken = setInterval( function() {
		
		if(getDeviceToken() != null && getDeviceToken() != ""){
			updateDriverDeviceToken(getDeviceToken());
			if(window.Android != null){
				window.Android.startService();
			}
			clearInterval(updateToken);
		}
   }, 1000); */
	
});



function getDeviceToken(){
	
	var token = "";
	try{
		if(window.Android != null){
			token = window.Android.getDeviceToken("${driverId}");
		}	
	}catch(exception){
		
	}finally{
		
	}
	return token;
}  
  

function updateDriverDeviceToken(token){
	
	
	if(token != ""){
		$.ajax({ 
			type: 'post' ,
			url : "/updateDriverDeviceToken.do" ,
			dataType : 'json' ,
			async : false,
			data : {
				token : token
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					//$.alert("성공",function(a){
					//});
				}else if(result == "E000"){
					
				}else{
					
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
}  



 function getNewsData(obj,keyword){
	
	 
	// alert($("#selectedStatus").children().length);
	 
	//alert($("#selectedStatus").children().first().get(0));	
	//alert($(obj).parent().get(0));
	
 	 if($("#selectedStatus").children().first().get(0) == $(obj).parent().get(0)){				//전체를 클릭 한경우
					
 		$("#selectedStatus").children().each(function(index,element){
 			$(this).removeClass('selected');
 		});
 		$(obj).parent().addClass('selected');
	}else{
			
	} 
	
	
	
/* 	
	
	$.ajax({ 
		type: 'post' ,
		url : "/research/newsSearch.do?&keyword="+keyword ,
		dataType : 'json' ,
		data : {
			
		},
		success : function(data, textStatus, jqXHR)
		{
			alert(data);
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
	 */
	
	
} 

 function goNewsPage(uri){
		
		//var replaceUrlStr = replaceAll(url, "?", "^");
		//replaceUrlStr = replaceAll(replaceUrlStr, "=", "+");
		
		var replaceUrlStr = encodeURIComponent(uri)
		document.location.href = "/news/newsPage.do?newsUri="+replaceUrlStr;
}  

 function downloadBill(selectMonth){
	 
		if("${user.driver_kind}" != "00" || "${user.driver_id}" == "sourcream"){
			homeLoader.show();
			document.location.href = "/carrier/bill_download.do?driverId=${user.driver_id}&selectMonth="+selectMonth;
			var updateHomeLoader = setInterval(function() {
				clearInterval(updateHomeLoader);
				homeLoader.hide();
			}, 300);
			 
		}else{
			$.alert("준비중 입니다.",function(a){
			});
		}
		 
		 
	 }
	 
 function fileDownloadDone(str){
		
		homeLoader.hide();
		$.alert("내파일 -> Download -> "+str+" 다운로드가 완료 되었습니다.",function(a){
		});
		
	} 
 
 
 function insertBillingStatus(decideMonth){
	 
	 $.confirm("계산서를 발행 하시겠습니까?",function(a){
		 if(a){
			 
			 $.ajax({ 
					type: 'post' ,
					url : "/carrier/insertDriverBillingStatus.do",
					dataType : 'json' ,
					async : false,
					data : {
						decideMonth : decideMonth
					},
					success : function(data, textStatus, jqXHR){
						var result = data.resultCode;
						var resultData = data.resultData;
						if(result == "0000"){
							$.alert("계산서를 발행 하였습니다.",function(a){
								window.location.reload();
							});
						}else if(result == "E000"){
							$.alert("계산서 발행에 실패 했습니다. 관리자에게 문의 하세요.",function(a){
							});
						}else{
							
						}
					} ,
					error : function(xhRequest, ErrorText, thrownError) {
					}
				});
			 	
		 }
	});
	 
	 
 }
 
 

    
function viewAllocationData(allocationId,allocationStatus){
	document.location.href = "/carrier/cal-list-detail.do";
} 
 
 
function viewAllocationList(allocationId,allocationStatus){
	
	document.location.href = "/carrier/group-list.do?allocationId="+allocationId+"&allocationStatus="+allocationStatus;
	
} 
 
 
 
function logout(){
	
	$.confirm("로그아웃 하시겠습니까?",function(a){
		 if(a){
			 if(window.Android != null){
					window.Android.stopService("${driverId}");
				}	
			 document.location.href = "/logout.do";	
		 }
	});
	
}   
 
 
 
function backKeyController(str){
	//history.go(-1);
	//location.href = document.referrer;
	history.go(-1);
}
 
</script>


        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div class="content-container" style="height:100%; overflow-y:hidden;" >
            <!-- <header class="bg-pink clearfix" style="background-color:#fff; border:none; text-align:center; "> -->
            <header class="bg-pink clearfix" style="position:fixed; top:0px; background-color:#fff; border:none; text-align:center; ">
                <div class="" style="width:19%;">
                    <a href="javascript:history.go(-1);"><img style="zoom: 0.5;" src="/img/back-icon.png" alt=""></a>
                </div> 
                <div class="" style="width:60%;">
                	<!-- <img style="width:90%; height:60%;" src="/img/main_logo.png" alt=""> -->
                	<img style="width:90%; height:60%;" onclick="javascript:document.location.href='/carrier/main.do'" src="/img/main_logo.png" alt="">
                </div>
                <div class="" style="width:19%; float:right;">
                   <a style="cursor:pointer;" href="javascript:logout();"><img style="" src="/img/logout.png" alt=""></a>
                </div>
            </header>
            <div style="clear:both;"></div>
            
            <div style=" text-align:right; clear:both;">&nbsp;</div>
            <div class="main-menu-container" style="margin-top:9%; height:100%; overflow-y:hidden;">
            	<div style=" text-align:right; clear:both;">${user.driver_name}님 환영 합니다.</div>
            	<div  id="container"  style="height:100%; margin-top:2%; background-color:#eee;">
            	
				
					
				<div style="margin-top:3%; height:60%;">
                
                <iframe src="http://docs.google.com/gview?embedded=true&url=http://52.78.153.148:8081/files/billing_list${fileMap.SAVE_NAME}" width="100%" height="1000px;"></iframe>
                                
                <%-- <iframe src="http://docs.google.com/gview?embedded=true&url=http://14.63.39.17:8090/files/billing_list${fileMap.SAVE_NAME}" width="100%" height="100%"></iframe> --%>
                
                <%-- <iframe src="https://view.officeapps.live.com/op/embed.aspx?src=http://www.hkai.co.kr/files/billing_list${fileMap.SAVE_NAME}" width="100%;"  height="100%;" ></iframe> --%>
                
                
                
                
                </div>
                
                <div class="menu-container" style="width:100%; margin-top:0px; text-align:center; clear:both;">
                
                	<c:if test="${driverBillingStatus == null || driverBillingStatus.billing_status != 'Y'}">
		            	<a class="kakao txt-bold" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:insertBillingStatus('${map.decideMonth}');">
							계산서 발행
					    </a>
				    </c:if>
				    <c:if test="${driverBillingStatus != null || driverBillingStatus.billing_status == 'Y'}">
		            	<a class="kakao txt-bold" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:downloadBill('${map.decideMonth}');">
							다운로드
					    </a>
				    </c:if>
				    
				    
				    <%-- <c:if test="${driverDeductFile != null && driverDeductFile.driver_view_flag == 'Y' && paramMap.decideMonth == monthago}">
		        		<a class="kakao txt-bold" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:bill('${paramMap.decideMonth}');">
							세금계산서
					    </a>
				    </c:if> --%>
				    
				    
	            </div>
                
                
                </div>
                
                
                
                
                
                
                
                
            </div>
        </div>


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      <script src="/js/vendor/bootstrap.min.js"></script>       
    <!-- veiwport for countnumber -->
    <script src="/js/alert.js"></script>
    <script src="/js/main.js"></script>   
    <script src="/js/vendor/xpull.js"></script>       
    <script src="/js/vendor/jquery.loading-indicator.min.js"></script>
    
    <script>
    
 var homeLoader;
    
    $(document).ready(function(){
    
    	homeLoader = $('body').loadingIndicator({
			useImage: false,
			showOnInit : false
		}).data("loadingIndicator");
		
    });
  
    
    </script>
        
    </body>
</html>
