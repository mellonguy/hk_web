<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko">      
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
    
<body class="bg-gray">
 
 
      
<script type="text/javascript">          
        
$(document).ready(function(){
	
	$("#lock").change(function(){
        if($("#lock").is(":checked")){
        	$("#interest_lock_yn").prop("checked",true);
        	$("#search_lock_yn").prop("checked",true);
        	$("#alarm_lock_yn").prop("checked",true);
        	$("#premium_lock_yn").prop("checked",true);
        	$("#news_lock_yn").prop("checked",true);
        	alarmSetting("lock","Y");
        }else{
        	$("#interest_lock_yn").prop("checked",false);
        	$("#search_lock_yn").prop("checked",false);
        	$("#alarm_lock_yn").prop("checked",false);
        	$("#premium_lock_yn").prop("checked",false);
        	$("#news_lock_yn").prop("checked",false);
        	alarmSetting("lock","N");
        }
    });
	
	$("#interest_lock_yn").change(function(){
        if($("#interest_lock_yn").is(":checked")){
        	alarmSetting("interest_lock_yn","Y");
        }else{
        	$("#lock").prop("checked",false);
        	alarmSetting("interest_lock_yn","N");
        }
    });
	
	$("#search_lock_yn").change(function(){
        if($("#search_lock_yn").is(":checked")){
        	alarmSetting("search_lock_yn","Y");
        }else{
        	$("#lock").prop("checked",false);
        	alarmSetting("search_lock_yn","N");
        }
    });
	
	$("#alarm_lock_yn").change(function(){
        if($("#alarm_lock_yn").is(":checked")){
        	alarmSetting("alarm_lock_yn","Y");
        }else{
        	$("#lock").prop("checked",false);
        	alarmSetting("alarm_lock_yn","N");
        }
    });
	
	$("#premium_lock_yn").change(function(){
        if($("#premium_lock_yn").is(":checked")){
        	alarmSetting("premium_lock_yn","Y");
        }else{
        	$("#lock").prop("checked",false);
        	alarmSetting("premium_lock_yn","N");
        }
    });
	
	$("#news_lock_yn").change(function(){
        if($("#news_lock_yn").is(":checked")){
        	alarmSetting("news_lock_yn","Y");
        }else{
        	$("#lock").prop("checked",false);
        	alarmSetting("news_lock_yn","N");
        }
    });
	
	
	$("#unlock").change(function(){
        if($("#unlock").is(":checked")){
        	$("#interest_unlock_yn").prop("checked",true);
        	$("#search_unlock_yn").prop("checked",true);
        	$("#interest_unlock_go_yn").prop("checked",true);
        	$("#adviser_unlock_go_yn").prop("checked",true);
        	$("#theme_unlock_go_yn").prop("checked",true);
        	$("#news_unlock_go_yn").prop("checked",true);
        	$("#alarm_unlock_go_yn").prop("checked",true);
        	alarmSetting("unlock","Y");
        }else{
        	$("#interest_unlock_yn").prop("checked",false);
        	$("#search_unlock_yn").prop("checked",false);
        	$("#interest_unlock_go_yn").prop("checked",false);
        	$("#adviser_unlock_go_yn").prop("checked",false);
        	$("#theme_unlock_go_yn").prop("checked",false);
        	$("#news_unlock_go_yn").prop("checked",false);
        	$("#alarm_unlock_go_yn").prop("checked",false);
        	alarmSetting("unlock","N");
        }
    });
	
	$("#interest_unlock_yn").change(function(){
        if($("#interest_unlock_yn").is(":checked")){
        	alarmSetting("interest_unlock_yn","Y");
        }else{
        	$("#unlock").prop("checked",false);
        	alarmSetting("interest_unlock_yn","N");
        }
    });
	
	$("#search_unlock_yn").change(function(){
        if($("#search_unlock_yn").is(":checked")){
        	alarmSetting("search_unlock_yn","Y");
        }else{
        	$("#unlock").prop("checked",false);
        	alarmSetting("search_unlock_yn","N");
        }
    });
	
	$("#interest_unlock_go_yn").change(function(){
        if($("#interest_unlock_go_yn").is(":checked")){
        	alarmSetting("interest_unlock_go_yn","Y");
        }else{
        	$("#unlock").prop("checked",false);
        	alarmSetting("interest_unlock_go_yn","N");
        }
    });
	
	$("#adviser_unlock_go_yn").change(function(){
        if($("#adviser_unlock_go_yn").is(":checked")){
        	alarmSetting("adviser_unlock_go_yn","Y");
        }else{
        	$("#unlock").prop("checked",false);
        	alarmSetting("adviser_unlock_go_yn","N");
        }
    });
	
	$("#theme_unlock_go_yn").change(function(){
        if($("#theme_unlock_go_yn").is(":checked")){
        	alarmSetting("theme_unlock_go_yn","Y");
        }else{
        	$("#unlock").prop("checked",false);
        	alarmSetting("theme_unlock_go_yn","N");
        }
    });
	
	$("#news_unlock_go_yn").change(function(){
        if($("#news_unlock_go_yn").is(":checked")){
        	alarmSetting("news_unlock_go_yn","Y");
        }else{
        	$("#unlock").prop("checked",false);
        	alarmSetting("news_unlock_go_yn","N");
        }
    });
	
	$("#alarm_unlock_go_yn").change(function(){
        if($("#alarm_unlock_go_yn").is(":checked")){
        	alarmSetting("alarm_unlock_go_yn","Y");
        }else{
        	$("#unlock").prop("checked",false);
        	alarmSetting("alarm_unlock_go_yn","N");
        }
    });
	
});
        

function alarmSetting(key,value){
	
	$.ajax({ 
 		type: 'post' ,
 		url : "/menu/widgetUserSetting.do" ,
 		dataType : 'json' ,
 		data : {
 			key : key,
 			value : value
 		},
 		success : function(data, textStatus, jqXHR)
 		{
 			if(data.resultCode == "0000"){
 				//alert("변경");
 			}
 		} ,
 		error : function(xhRequest, ErrorText, thrownError) {
 		}
 	});	

}

       
        
</script>
        

		<div class="content-container">
            <header class="clearfix">
                <div class="search-icon">
                    <a href="/menu/menu.do" class="goback"><img src="/img/back-icon.png" alt=""></a>
                </div>
                <div class="page-title txt-medium">
                    위젯설정
                </div>
                <div class="menu-bar pull-right">
                    <a href="/adviser/adviser.do"><img src="/img/home-pink-icon.png" alt=""></a>
                </div>
            </header>
            <div class="divider"></div>
            <div class="notification-settings content-box">
                <span class="title">잠금화면 관련</span>
                <div class="notif-box">
                    <span class="notif">
                        잠금화면 전체 설정
                    </span>
                    <div class="track-holder">
                        <input type="checkbox" id="lock" hidden  <c:if test="${widgetSettingMap.lock eq 'Y' }">checked</c:if>>
                        <label for="lock"></label>
                    </div>
                </div>
                <div class="divider-3x"></div>
                <span class="title">종목 및 알림</span>
                <div class="notif-box pl22">
                    <span class="notif">
                        관심종목
                    </span>
                    <div class="track-holder">
                        <input type="checkbox" id="interest_lock_yn" hidden   <c:if test="${widgetSettingMap.interest_lock_yn eq 'Y' }">checked</c:if>>
                        <label for="interest_lock_yn"></label>
                    </div>
                </div>
                <div class="notif-box pl22">
                    <span class="notif">
                        검색창
                    </span>
                    <div class="track-holder">
                        <input type="checkbox" id="search_lock_yn" hidden   <c:if test="${widgetSettingMap.search_lock_yn eq 'Y' }">checked</c:if>>
                        <label for="search_lock_yn"></label>
                    </div>
                </div>
                <div class="notif-box pl22">
                    <span class="notif">
                        시세알림
                    </span>
                    <div class="track-holder">
                        <input type="checkbox" id="alarm_lock_yn" hidden  <c:if test="${widgetSettingMap.alarm_lock_yn eq 'Y' }">checked</c:if>>
                        <label for="alarm_lock_yn"></label>
                    </div>
                </div>
                <div class="notif-box pl22">
                    <span class="notif">
                        프리미엄 신호알림
                    </span>
                    <div class="track-holder">
                        <input type="checkbox" id="premium_lock_yn" hidden  <c:if test="${widgetSettingMap.premium_lock_yn eq 'Y' }">checked</c:if>>
                        <label for="premium_lock_yn"></label>
                    </div>
                </div>
                <div class="notif-box pl22">
                    <span class="notif">
                        실시간 뉴스
                    </span>
                    <div class="track-holder">
                        <input type="checkbox" id="news_lock_yn" hidden  <c:if test="${widgetSettingMap.news_lock_yn eq 'Y' }">checked</c:if>>
                        <label for="news_lock_yn"></label>
                    </div>
                </div>
                <div class="divider-3x"></div>
                <span class="title">바탕화면 관련</span>
                <div class="notif-box">
                    <span class="notif">
                        바탕화면  전체 설정
                    </span>
                    <div class="track-holder">
                        <input type="checkbox" id="unlock" hidden  <c:if test="${widgetSettingMap.unlock eq 'Y' }">checked</c:if>>
                        <label for="unlock"></label>
                    </div>
                </div>
                <div class="divider-3x"></div>
                <span class="title">바로가기 및 검색</span>
                <div class="notif-box pl22">
                    <span class="notif">
                        관심종목
                    </span>
                    <div class="track-holder">
                        <input type="checkbox" id="interest_unlock_yn" hidden   <c:if test="${widgetSettingMap.interest_unlock_yn eq 'Y' }">checked</c:if>>
                        <label for="interest_unlock_yn"></label>
                    </div>
                </div>
                <div class="notif-box pl22">
                    <span class="notif">
                        검색창
                    </span>
                    <div class="track-holder">
                        <input type="checkbox" id="search_unlock_yn" hidden   <c:if test="${widgetSettingMap.search_unlock_yn eq 'Y' }">checked</c:if>>
                        <label for="search_unlock_yn"></label>
                    </div>
                </div>
                <div class="notif-box pl22">
                    <span class="notif">
                        관심종목 바로가기
                    </span>
                    <div class="track-holder">
                        <input type="checkbox" id="interest_unlock_go_yn" hidden  <c:if test="${widgetSettingMap.interest_unlock_go_yn eq 'Y' }">checked</c:if>>
                        <label for="interest_unlock_go_yn"></label>
                    </div>
                </div>
                <div class="notif-box pl22">
                    <span class="notif">
                        로보어드바이저 바로가기
                    </span>
                    <div class="track-holder">
                        <input type="checkbox" id="adviser_unlock_go_yn" hidden  <c:if test="${widgetSettingMap.adviser_unlock_go_yn eq 'Y' }">checked</c:if>>
                        <label for="adviser_unlock_go_yn"></label>
                    </div>
                </div>
                <div class="notif-box pl22">
                    <span class="notif">
                        장전 강세테마 바로가기
                    </span>
                    <div class="track-holder">
                        <input type="checkbox" id="theme_unlock_go_yn" hidden  <c:if test="${widgetSettingMap.theme_unlock_go_yn eq 'Y' }">checked</c:if>>
                        <label for="theme_unlock_go_yn"></label>
                    </div>
                </div>
                <div class="notif-box pl22">
                    <span class="notif">
                        뉴스 바로가기
                    </span>
                    <div class="track-holder">
                        <input type="checkbox" id="news_unlock_go_yn" hidden  <c:if test="${widgetSettingMap.news_unlock_go_yn eq 'Y' }">checked</c:if>>
                        <label for="news_unlock_go_yn"></label>
                    </div>
                </div>
                <div class="notif-box pl22">
                    <span class="notif">
                        알림목록 바로가기
                    </span>
                    <div class="track-holder">
                        <input type="checkbox" id="alarm_unlock_go_yn" hidden  <c:if test="${widgetSettingMap.alarm_unlock_go_yn eq 'Y' }">checked</c:if>>
                        <label for="alarm_unlock_go_yn"></label>
                    </div>
                </div>

            </div>

        </div>


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      <script src="/js/vendor/bootstrap.min.js"></script>       
    <!-- veiwport for countnumber -->
    <script src="/js/jquery.viewportchecker.js"></script>    
    <script src="/js/main.js"></script>       
    </body>
</html>
