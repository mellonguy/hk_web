<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SYSTEM </title>
<c:set var="prefix" value=""/>
<script src="/js/vendor/jquery-1.11.2.min.js"></script>
<script src="/js/alert.js"></script>
<script type="text/javascript">

$(document).ready(function(){
	<c:if test="${!empty scriptMsg}">
	$.alert("${scriptMsg}",function(a){
		<c:if test="${!empty redirectUrl}">
			location.href = '${pageContext.request.contextPath}${prefix}${redirectUrl}';
		</c:if>
	});
	</c:if>
});


/* <c:if test="${!empty scriptMsg}">
alert('${scriptMsg}');
</c:if>

<c:if test="${!empty redirectUrl}">
location.href = '${pageContext.request.contextPath}${prefix}${redirectUrl}';
</c:if> */

</script>
</head>
<body></body>
</html>
