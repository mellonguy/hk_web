<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

     
<head>
    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    	
    	<title></title>
    	<meta name="description" content="">
    	
    	<!-- <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">  -->
 		
 		<!-- <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=yes;"> -->
		<!-- <meta name="viewport"content="width=device-width, user-scalable=yes,initial-scale=1.0, maximum-scale=3.0"/> -->
		<!-- <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no"> -->
		<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0;"> -->
   		<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
		<!-- <meta name="viewport" content="width=device-width,initial-scale=1, minimum-scale=1, maximum-scale=2, user-scalable=no"> -->
		<!-- <meta name=viewport content="width=device-width, initial-scale=1"> -->
		<meta name=viewport content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">

    	<!-- <meta name="viewport" content="width=720px, user-scalable=no"> -->
    	
    	<link rel="stylesheet" href="/css/bootstrap.min.css">
    	<link rel="stylesheet" href="/css/animsition.min.css">  
    	<link rel="stylesheet" href="/css/bootstrap-theme.min.css">
    	<link rel="stylesheet" href="/css/owl.carousel.min.css">
    	<link rel="stylesheet" href="/css/owl.theme.default.min.css">
    	<link rel="stylesheet" href="/css/notosanskr.css">
    	<link rel="stylesheet" href="/css/main.css">
    	<link rel="stylesheet" href="/css/messagebox.css">
    	<link rel="stylesheet" href="/css/jquery.loading-indicator.css?v=2">
    	<script src="/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    	<script src="/js/vendor/jquery-1.11.2.min.js"></script>
    	<script src="/js/animsition.min.js"></script>
</head>

