<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko">      
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
    
<body class="bg-gray">
        
        
        
<script type="text/javascript">          
        
$(document).ready(function(){
	
	$("#event_yn").change(function(){
        if($("#event_yn").is(":checked")){
        	alarmSetting("event_yn","Y");
        }else{
        	alarmSetting("event_yn","N");
        }
    });
	
	$("#price_yn").change(function(){
        if($("#price_yn").is(":checked")){
        	alarmSetting("price_yn","Y");
        }else{
        	alarmSetting("price_yn","N");
        }
    });
	
	$("#premium_yn").change(function(){
        if($("#premium_yn").is(":checked")){
        	alarmSetting("premium_yn","Y");
        }else{
        	alarmSetting("premium_yn","N");
        }
    });
	
	$("#theme_yn").change(function(){
        if($("#theme_yn").is(":checked")){
        	alarmSetting("theme_yn","Y");
        }else{
        	alarmSetting("theme_yn","N");
        }
    });
	
	$("#discussion_yn").change(function(){
        if($("#discussion_yn").is(":checked")){
        	alarmSetting("discussion_yn","Y");
        }else{
        	alarmSetting("discussion_yn","N");
        }
    });
	
	$("#recommend_item_yn").change(function(){
        if($("#recommend_item_yn").is(":checked")){
        	alarmSetting("recommend_item_yn","Y");
        }else{
        	alarmSetting("recommend_item_yn","N");
        }
    });
	
});
        
    





function alarmSetting(key,value){
	
	$.ajax({ 
 		type: 'post' ,
 		url : "/menu/alarmSetting.do" ,
 		dataType : 'json' ,
 		data : {
 			key : key,
 			value : value
 		},
 		success : function(data, textStatus, jqXHR)
 		{
 			if(data.resultCode == "0000"){
 				//alert("변경");
 			}
 		} ,
 		error : function(xhRequest, ErrorText, thrownError) {
 		}
 	});	

}

       
        
</script>
        
        
        
        
        
        
        
        
        

        <div class="content-container">
            <header class="clearfix">
                <div class="search-icon">
                    <a style="cursor:pointer;" onclick="javascript:history.go(-1);"><img src="/img/back-icon.png" alt=""></a> 
                </div>
                <div class="page-title txt-medium">
                    알림설정
                </div>
                <div class="menu-bar pull-right">
                    <a href="/adviser/adviser.do"><img src="/img/home-pink-icon.png" alt=""></a>
                </div>
            </header>
            <div class="divider"></div>
            <div class="notification-settings content-box">
                <span class="title">전체 알림</span>
                <div class="notif-box">
                    <span class="notif">
                        이벤트/마케팅 관련 알림
                    </span>
                    <div class="track-holder">
                    <!-- notificationSettingMap -->
                        <input type="checkbox" id="event_yn" hidden <c:if test="${notificationSettingMap.event_yn eq 'Y' }">checked</c:if>>
                        <label for="event_yn"></label>
                    </div>
                </div>
                <div class="divider-3x"></div>
                <span class="title">시세</span>
                <div class="notif-box pl22">
                    <span class="notif">
                        시세 알림
                    </span>
                    <div class="track-holder">
                        <input type="checkbox" id="price_yn" hidden  <c:if test="${notificationSettingMap.price_yn eq 'Y' }">checked</c:if>>
                        <label for="price_yn"></label>
                    </div>
                </div>
                <div class="divider-3x"></div>
                <span class="title">관심 종목 관련</span>
                <div class="notif-box">
                    <span class="notif">
                        프리미엄 분석 알림
                    </span>
                    <div class="track-holder">
                        <input type="checkbox" id="premium_yn" hidden  <c:if test="${notificationSettingMap.premium_yn eq 'Y' }">checked</c:if>>
                        <label for="premium_yn"></label>
                    </div>
                </div>
                <div class="notif-box">
                    <span class="notif">
                        장전 강세테마
                    </span>
                    <div class="track-holder">
                        <input type="checkbox" id="theme_yn" hidden  <c:if test="${notificationSettingMap.theme_yn eq 'Y' }">checked</c:if>>
                        <label for="theme_yn"></label>
                    </div>
                </div>
                <div class="notif-box">
                    <span class="notif">
                        토론실
                    </span>
                    <div class="track-holder">
                        <input type="checkbox" id="discussion_yn" hidden  <c:if test="${notificationSettingMap.discussion_yn eq 'Y' }">checked</c:if>>
                        <label for="discussion_yn"></label>
                    </div>
                </div>
                <div class="notif-box">
                    <span class="notif">
                        종목발굴 일지
                    </span>
                    <div class="track-holder">
                        <input type="checkbox" id="recommend_item_yn" hidden  <c:if test="${notificationSettingMap.recommend_item_yn eq 'Y' }">checked</c:if>>
                        <label for="recommend_item_yn"></label>
                    </div>
                </div>
                <!-- <div class="notif-box pl22">
                    <span class="notif">
                        전문가 종목 추천 알림
                    </span>
                    <div class="track-holder">
                        <input type="checkbox" id="notifexperts" hidden>
                        <label for="notifexperts"></label>
                    </div>
                </div> -->
            </div>

        </div>


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      <script src="/js/vendor/bootstrap.min.js"></script>       
    <!-- veiwport for countnumber -->
    <script src="/js/jquery.viewportchecker.js"></script>    
    <script src="/js/main.js"></script>       
    </body>
</html>
