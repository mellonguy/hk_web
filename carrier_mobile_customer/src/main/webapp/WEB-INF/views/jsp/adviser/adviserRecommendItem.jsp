<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko">      
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
    
<body class="black">
        
<script type="text/javascript">  

$(document).ready(function(){
	
	getAdviserList("S",0);
	getWeek();
});


function getRecommendItem(adviserCd,date){
	
	
	alert(adviserCd);
	alert(date);
	
	
	
}



function getWeek(){
	
	var date = new Date();
	var year = date.getFullYear();
	var month = date.getMonth()+1;
	var day = date.getDate();
	$("#month").html(year+"."+(month<10?"0"+month:month));
	var week = ['일', '월', '화', '수', '목', '금', '토'];
	var dayOfWeek = week[date.getDay()];
	
	var result = "";
	$("#week").html("");
	for(var i = (date.getDay()*(-1)); i < (7-date.getDay()); i++){
		var dateForCal = new Date();	
		dateForCal.setDate(date.getDate()+i);
		
		if(dateForCal.getFullYear() == year && (dateForCal.getMonth()+1) == month && dateForCal.getDate() == day){
			result += '<li class="active">';
		}else{
			result += '<li>';	
		}
		result += '<a onclick="javascript:getRecommendItem(\'${adviser.adviser_cd}\' ,\''+dateForCal.getFullYear()+"-"+((dateForCal.getMonth()+1)<10?"0"+(dateForCal.getMonth()+1):(dateForCal.getMonth()+1))+"-"+(dateForCal.getDate()<10?"0"+dateForCal.getDate():dateForCal.getDate())+'\')">';
		result += '<span class="lbl">'+week[dateForCal.getDay()]+'</span>';
		result += '<span class="date">'+dateForCal.getDate()+'</span>';
		result += '</a>';
		result += '</li>';
	}
	$("#week").html(result);

}


function goAdviserDetailPage(adviserCd){
	
	document.location.href = "/adviser/adviserDetail.do?adviserCd="+adviserCd;
	
}





function getAdviserList(adviserGrade,idx){
	
	
	$("#adviserList").html("");
	
	$.ajax({ 
		type: 'post' ,
		url : "/adviser/selectAdviserList.do" ,
		dataType : 'json' ,
		data : {
			adviserGrade : adviserGrade
		},
		success : function(data, textStatus, jqXHR)
		{
			
			if(data.resultCode == "0000"){
			
				var result = "";
				var resultData = data.resultData;
				
				$("#adviserGradeSel").children().each(function(index,element){
					$(this).removeClass("active");
				});
				$("#adviserGradeSel").children().eq(idx).addClass("active");
				
				if(resultData.length > 0){
					for(var i = 0; i < resultData.length; i++){
						result += '<a onclick="javascript:goAdviserDetailPage(\''+resultData[i].adviser_cd+'\')" class="item">'; 
						result += '<span class="title">'+resultData[i].adviser_name+'</span>';
						result += '<span class="subinfo">'+resultData[i].adviser_comment+'</span>';
						result += '</span>';
						result += '</a>';
					}	
				}else{				//리스트가 없을때 처리
					
				}
				
				$("#adviserList").append(result);
				
			}else{
				alert("조회 실패");	
			}
			
			
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	}); 
	
}











function goSearch(){
		
	document.location.href = "/search/search.do";
	
}



function selectInterestItem(itemCd,marketCd,obj){
	
	$.ajax({ 
		type: 'post' ,
		url : "/interest/selectInterestItem.do" ,
		dataType : 'json' ,
		data : {
			itemCd : itemCd,
			marketCd : marketCd
		},
		success : function(data, textStatus, jqXHR)
		{
			if(data.resultCode == "0000"){
				insertInterestItem(itemCd,marketCd,obj);
			}else{
				deleteInterestItem(itemCd,marketCd,obj);
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	}); 
	
	
}    
    
function insertInterestItem(itemCd,marketCd,obj){
	
	$.ajax({ 
		type: 'post' ,
		url : "/interest/insertInterestItem.do" ,
		dataType : 'json' ,
		data : {
			itemSrtCd : itemCd,
			marketCd : marketCd
		},
		success : function(data, textStatus, jqXHR)
		{
			if(data.resultCode == "0000"){
				$(obj).addClass('heart-full');
			}else if(data.resultCode == "E002"){
					alert("로그인 되지 않음");
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	}); 
}    
    
  

function deleteInterestItem(itemCd,marketCd,obj){
	
	$.ajax({ 
		type: 'post' ,
		url : "/interest/deleteInterestItem.do" ,
		dataType : 'json' ,
		data : {
			itemSrtCd : itemCd,
			marketCd : marketCd
		},
		success : function(data, textStatus, jqXHR)
		{
			if(data.resultCode == "0000"){
				$(obj).removeClass('heart-full');
			}else if(data.resultCode == "E002"){
					alert("로그인 되지 않음");
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	}); 
}    




function purchaseRecommendItem(adviserCd,recommendItemId){
	
	if(confirm("추천가를 확인 하시겠습니까?")){
		$.ajax({ 
	 		type: 'post' ,
	 		url : "/adviser/purchaseRecommendItem.do" ,
	 		dataType : 'json' ,
	 		data : {
	 			adviserCd : adviserCd,
	 			recommendItemId : recommendItemId
	 		},
	 		success : function(data, textStatus, jqXHR)
	 		{
	 			if(data.resultCode == "0000"){
	 				document.location.href="/adviser/adviserRecommendItem.do?adviserCd="+adviserCd;
	 			}else if(data.resultCode == "E003"){
	 				alert("보유한 포인트가 부족 합니다.");
	 				document.location.href="/menu/primary.do";
	 			}else if(data.resultCode == "E004"){
	 				alert("해당 어드바이저가 구매 되지 않았습니다.");
	 				document.location.href="/adviser/adviserDetail.do?adviserCd="+adviserCd;
	 			}
	 		} ,
	 		error : function(xhRequest, ErrorText, thrownError) {
	 		}
	 	});	
	}
 	 
	
	
}









</script>        
        
        <div class="content-container black interest-page eventsearchresult productsinuse">
            <header class="clearfix nb">
                <div class="search-icon">
                    <a onclick="javascript:history.go(-1); return false;"><img src="/img/back-icon.png" alt=""></a> 
                </div>
                <div class="page-title txt-medium white">
                    종목 검색결과
                </div>
                <div class="menu-bar pull-right">
                    <a href="/adviser/adviser.do"><img src="/img/home-pink-icon.png" alt=""></a>
                </div>
            </header>
            <span class="note">
                어드바이저의 분석을 통한 상승예상 종목을 확인하세요
            </span>
            
            	<c:forEach var="data" items="${adviserRecommendItemList}" varStatus="status">
            		<c:if test="${data.purchase_recommend_item_id == null || data.purchase_recommend_item_id == '' }">
                     		<div class="result-list">
				                <div class="interest-item">
				                    <div class="interest-info">
				                        <span class="name">${data.item_name}</span>
				                    </div>
				                    <div class="value red">
				                        ${data.trdPrc}
				                    </div>
				                    <div class="percent-value red">
				                        <span class="percent">${data.cmpprevddPer}%</span>
				                        <span class="value up">${data.cmpprevddPrc}</span>
				                    </div>
				                    <div class="bell-holder">
					                    <c:if test="${data.interest_item_cd != null}">
					                    	<a onclick="javascript:selectInterestItem('${data.item_srt_cd}','${data.market_cd}',this)" class="heart heart-full"></a>
					                    </c:if>
					                    <c:if test="${data.interest_item_cd == null}">
					                    	<a onclick="javascript:selectInterestItem('${data.item_srt_cd}','${data.market_cd}',this)" class="heart"></a>
					                    </c:if>
				                    </div>
				                    <a onclick="javascript:purchaseRecommendItem('${data.adviser_cd}','${data.adviser_recommend_item_cd}');" class="btn-confirm-holder">
				                        추천가 확인
				                    </a>
				                </div>
				            </div>
                     </c:if>
                	<c:if test="${data.purchase_recommend_item_id != null && data.purchase_recommend_item_id != '' }">
            			<div class="recommended-box">    	
	                		<div class="box-item">
			                    <div class="item-top-info clearfix">
			                        <div class="details-area">
			                            <span class="name">${data.item_name}</span>
			                            <span class="subinfo">추천가 ${data.recommend_price}</span>
			                        </div>
			                        <div class="value-area red">
			                            ${data.trdPrc}
			                        </div>
			                        <div class="percent-area red">
			                            <span class="percent">${data.cmpprevddPer}%</span>
			                            <span class="eval up">${data.cmpprevddPrc}</span>
			                        </div>
			                        <div class="heart-box">
			                            <c:if test="${data.interest_item_cd != null}">
					                    	<a onclick="javascript:selectInterestItem('${data.item_srt_cd}','${data.market_cd}',this)" class="heart heart-full"></a>
					                    </c:if>
					                    <c:if test="${data.interest_item_cd == null}">
					                    	<a onclick="javascript:selectInterestItem('${data.item_srt_cd}','${data.market_cd}',this)" class="heart"></a>
					                    </c:if>
			                        </div>
			                    </div>
			                    <div class="line-holder clearfix">
			                        <div class="blue color-box" data-value="">
			                            <span></span>
			                        </div>
			                        <div class="color-box red active" data-value="300">
			                            <span style="width: 300px;"></span>
			                        </div>
			                    </div>
			                    <div class="offense-goal-box clearfix">
			                        <div class="box pull-left">
			                            <span class="lbl">손절가</span>
			                            <span class="points">${data.sell_price}원</span>
			                        </div>
			                        <div class="box pull-right text-right">
			                            <span class="lbl">최초목표가 </span>
			                            <span class="points">${data.target_price}원</span>
			                        </div>
			                    </div>
			                </div>
		                </div>
                     </c:if>
                </c:forEach>
                
                <%-- <c:if test="${adviserGrade == 'S' || adviserGrade == 'A'}">
	                <div class="fixed-bottomarea">
		                <span>인공지능 차트분석 프리미엄 신호알림</span>
		                <a href="#">서비스 신청하기</a>
		            </div>
                </c:if> --%>
                
                <!-- <div class="box-item">
                    <div class="item-top-info clearfix">
                        <div class="details-area">
                            <span class="name">게리온</span>
                        </div>
                        <div class="value-area red">
                            30,000
                        </div>
                        <div class="percent-area red">
                            <span class="percent">+15.05%</span>
                            <span class="eval up">100,750</span>
                        </div>
                        <div class="heart-box">
                            <a href="#" class="heart"></a>
                        </div>
                    </div>
                    <div class="line-holder clearfix">
                        <div class="blue color-box" data-value="">
                            <span></span>
                        </div>
                        <div class="color-box red active" data-value="260">
                            <span style="width: 260px;"></span>
                        </div>
                    </div>
                    <div class="offense-goal-box clearfix">
                        <div class="box pull-left">
                            <span class="lbl">손절가</span>
                            <span class="points">1,840원</span>
                        </div>
                        <div class="box pull-right text-right">
                            <span class="lbl">최초목표가 </span>
                            <span class="points">32,000원</span>
                        </div>
                    </div>
                </div> -->
            
            <!-- <div class="result-list">
                <div class="interest-item">
                    <div class="interest-info">
                        <span class="name">티플랙스</span>
                    </div>
                    <div class="value red">
                        300,000
                    </div>
                    <div class="percent-value red">
                        <span class="percent">+300.85%</span>
                        <span class="value up">200,750</span>
                    </div>
                    <div class="bell-holder">
                        <a href="#" class="heart"></a>
                    </div>
                    <a href="#" class="btn-confirm-holder">
                        추천가 확인
                    </a>
                </div>
                <div class="interest-item">
                    <div class="interest-info">
                        <span class="name">케이디 네이쳐 <br>
                        엔 바이오
                        </span>
                        <span class="info">
                            매수가 대비 
                        <span class="red">+ 70.45%</span>
                        </span>
                    </div>
                    <div class="value red">
                        20,000
                    </div>
                    <div class="percent-value red">
                        <span class="percent">+1.85%</span>
                        <span class="value up">10,750</span>
                    </div>
                    <div class="bell-holder">
                        <a href="#" class="heart"></a>
                    </div>
                    <a href="#" class="btn-confirm-holder">
                        추천가 확인
                    </a>
                </div>
            </div> -->
        </div>
        
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      <script src="/js/vendor/bootstrap.min.js"></script>       
    <!-- veiwport for countnumber -->
    <script src="/js/owl.carousel.min.js"></script>
    <script src="/js/main.js"></script>     
    <script>
        var owl = $('.owl-carousel');
        owl.owlCarousel({
            autoplay:true,
            autoplayTimeout:5000,
            items:1,
            loop:true,
            margin:0,
            nav:false,
            dots:true,
        })
    </script>   
    </body>
</html>
