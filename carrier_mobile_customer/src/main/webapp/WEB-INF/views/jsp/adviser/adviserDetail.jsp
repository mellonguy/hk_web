<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko">      
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>

<body class="black">
        
<script type="text/javascript">  

$(document).ready(function(){
	
	getAdviserList("S",0);
	getWeek();
});


function getRecommendItem(obj,adviserCd,date){
	
	
	$('#week').find("li").each(function(index,element) {
           		$(this).removeClass('active');
           	});
   	  $(obj).parent().addClass('active');
	
	adviserDetailItem(adviserCd,date);
	
	
}



function getWeek(){
	
	var date = new Date();
	var year = date.getFullYear();
	var month = date.getMonth()+1;
	var day = date.getDate();
	$("#month").html(year+"."+(month<10?"0"+month:month));
	var week = ['일', '월', '화', '수', '목', '금', '토'];
	var dayOfWeek = week[date.getDay()];
	
	var result = "";
	$("#week").html("");
	for(var i = (date.getDay()*(-1)); i < (7-date.getDay()); i++){
		var dateForCal = new Date();	
		dateForCal.setDate(date.getDate()+i);
		
		if(dateForCal.getFullYear() == year && (dateForCal.getMonth()+1) == month && dateForCal.getDate() == day){
			
		adviserDetailItem("${adviser.adviser_cd}",dateForCal.getFullYear()+"-"+((dateForCal.getMonth()+1)<10?"0"+(dateForCal.getMonth()+1):(dateForCal.getMonth()+1))+"-"+(dateForCal.getDate()<10?"0"+dateForCal.getDate():dateForCal.getDate()));
			
			result += '<li class="active">';
		}else{
			result += '<li>';	
		}
		result += '<a onclick="javascript:getRecommendItem(this,\'${adviser.adviser_cd}\' ,\''+dateForCal.getFullYear()+"-"+((dateForCal.getMonth()+1)<10?"0"+(dateForCal.getMonth()+1):(dateForCal.getMonth()+1))+"-"+(dateForCal.getDate()<10?"0"+dateForCal.getDate():dateForCal.getDate())+'\')">';
		result += '<span class="lbl">'+week[dateForCal.getDay()]+'</span>';
		result += '<span class="date">'+dateForCal.getDate()+'</span>';
		result += '</a>';
		result += '</li>';
	}
	$("#week").html(result);

}


function adviserDetailItem(adviserCd,createdAt){
	
	
$("#owlList").html("");
	
	$.ajax({ 
		type: 'post' ,
		url : "/adviser/adviserDetailItem.do" ,
		dataType : 'json' ,
		data : {
			adviserCd : adviserCd,
			createdAt : createdAt
		},
		success : function(data, textStatus, jqXHR)
		{
			
			if(data.resultCode == "0000"){
			
				var result = "";
				var resultData = data.resultData;
				
				/* $("#adviserGradeSel").children().each(function(index,element){
					$(this).removeClass("active");
				});
				$("#adviserGradeSel").children().eq(idx).addClass("active"); */
			//	alert(resultData.length);
				if(resultData.length > 0){
					result += '<div class="owl-carousel owl-theme">';
					for(var i = 0; i < resultData.length; i++){
						result += '<div class="title-box item">'; 
						result += '<span class="title">'+resultData[i].item_name+' 상한가</span>';
						result += '<div class="subinfo">';
						result += '<div class="redbox box">';
						result += '<span>발굴  </span>'+resultData[i].created_at_str;
						result += '</div>';
						result += '<div class="bluebox box">';
						result += '<span>상한가달성</span>  '+resultData[i].created_at_str;
						result += '</div>';
						result += '</div>';
						result += '</div>';
					}	
					result += '</div>';
					$("#owlList").append(result);
					
					 var owl = $('.owl-carousel');
				        owl.owlCarousel({
				            autoplay:true,
				            autoplayTimeout:5000,
				            items:1,
				            loop:true,
				            margin:0,
				            nav:false,
				            dots:true,
				        }) 
					
					
					
				}else{				//리스트가 없을때 처리
					
				}
				
				
				
			}else{
				alert("조회 실패");	
			}
			
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	}); 
	
	 

	
}















function goAdviserDetailPage(adviserCd){
	
	document.location.href = "/adviser/adviserDetail.do?adviserCd="+adviserCd;
	
}





function getAdviserList(adviserGrade,idx){
	
	
	$("#adviserList").html("");
	
	$.ajax({ 
		type: 'post' ,
		url : "/adviser/selectAdviserList.do" ,
		dataType : 'json' ,
		data : {
			adviserGrade : adviserGrade
		},
		success : function(data, textStatus, jqXHR)
		{
			
			if(data.resultCode == "0000"){
			
				var result = "";
				var resultData = data.resultData;
				
				$("#adviserGradeSel").children().each(function(index,element){
					$(this).removeClass("active");
				});
				$("#adviserGradeSel").children().eq(idx).addClass("active");
				
				if(resultData.length > 0){
					for(var i = 0; i < resultData.length; i++){
						result += '<a onclick="javascript:goAdviserDetailPage(\''+resultData[i].adviser_cd+'\')" class="item">'; 
						result += '<span class="title">'+resultData[i].adviser_name+'</span>';
						result += '<span class="subinfo">'+resultData[i].adviser_comment+'</span>';
						result += '</span>';
						result += '</a>';
					}	
				}else{				//리스트가 없을때 처리
					
				}
				
				$("#adviserList").append(result);
				
			}else{
				alert("조회 실패");	
			}
			
			
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	}); 
	
}

function purchaseAdviser(adviserCd){

	
	
	if('${msg}' == "사용중"){
		alert("해당 상품을 이용중 입니다.");
		return false;
	}else{
		if(confirm("구매 하시겠습니까?")){
			$.ajax({ 
		 		type: 'post' ,
		 		url : "/adviser/purchaseAdviser.do" ,
		 		dataType : 'json' ,
		 		data : {
		 			adviserCd : adviserCd
		 		},
		 		success : function(data, textStatus, jqXHR)
		 		{
		 			if(data.resultCode == "0000"){
		 				document.location.href="/adviser/adviserDetail.do?adviserCd=${adviser.adviser_cd}";
		 			}else if(data.resultCode == "E003"){
		 				alert("보유한 포인트가 부족 합니다.");
		 				document.location.href="/menu/primary.do";
		 			}
		 		} ,
		 		error : function(xhRequest, ErrorText, thrownError) {
		 		}
		 	});	
		}
	}
	
	
 	 
	
	
}




function insertPurchaseAdviser(adviserCd){
	 
 	$.ajax({ 
 		type: 'post' ,
 		url : "/company/updateDiscussionLikeCnt.do" ,
 		dataType : 'json' ,
 		data : {
 			discussionCd : discussionCd
 		},
 		success : function(data, textStatus, jqXHR)
 		{
 			if(data.resultCode == "0000"){
 				document.location.href="/company/discussion.do?itemSrtCd=${itemSrtCd}&marketCd=${marketCd}";
 			}else if(data.resultCode == "E002"){
 				
 			}
 		} ,
 		error : function(xhRequest, ErrorText, thrownError) {
 		}
 	}); 
 
}





function goDetailPage(adviserCd){
	
	document.location.href = "/adviser/adviserItemDetail.do?adviserCd="+adviserCd;
	
}



function goSearch(){
		
	document.location.href = "/search/search.do";
	
}

</script>        
        
        <div class="content-container black homeadvisordetails">
            <header class="clearfix">
                <div class="search-icon">
                    <a onclick="javascript:history.go(-1); return false;"><img src="/img/back-icon.png" alt=""></a> 
                </div>
                <div class="page-title txt-medium white">
                    <!-- 주락펴락 시그널-S 3호 -->
                    ${adviser.adviser_name}
                </div>
                <div class="menu-bar pull-right">
                    <a href="/adviser/adviser.do"><img src="/img/home-pink-icon.png" alt=""></a>
                </div>
            </header>
            <div class="heading-area">
                <span class="lbl">${adviser.adviser_name}</span>
                <span class="desc">
                    <!-- 거래량, 볼린저밴드, RSI, 주가이평비교 등 <br>
                    5개의 척도로 단기성 중소형주를 발굴합니다. -->
                    ${adviser.adviser_explanation}
                </span>
                <a href="#" class="addme">
                    <img src="/img/add-icon2.png" alt="">
                </a>
            </div>
            <div class="content-area">
                <div class="content-box">
                    <span class="title">
                        종목발굴 일지
                    </span>
                </div>
                <div class="calendar-head clearfix">
                    <span class="pull-left text-left" id="month"></span>
                    <span class="pull-right text-right" onclick="javascript:goDetailPage('${adviser.adviser_cd}');">전체보기</span>
                </div>
                <div class="date-list">
                    <ul class="clearfix" id="week">
                    </ul>
                </div>
                <div class="content-slider" id="owlList">
                    <!-- <div class="owl-carousel owl-theme"> -->
                        <!-- <div class="title-box item">
                            <span class="title">남북경협주 보락 상한가!</span>
                            <div class="subinfo">
                                <div class="redbox box">
                                    <span>발굴</span> 2018.04.25 
                                </div>
                                <div class="bluebox box">
                                    <span>상한가달성</span> 2018.06.16
                                </div>
                            </div>
                        </div> -->
                        <!-- <div class="title-box item">
                            <span class="title">남북경협주 보락 상한가!</span>
                            <div class="subinfo">
                                <div class="redbox box">
                                    <span>발굴</span> 2018.04.25 
                                </div>
                                <div class="bluebox box">
                                    <span>상한가달성</span> 2018.06.16
                                </div>
                            </div>
                        </div>
                        <div class="title-box item">
                            <span class="title">남북경협주 보락 상한가!</span>
                            <div class="subinfo">
                                <div class="redbox box">
                                    <span>발굴</span> 2018.04.25 
                                </div>
                                <div class="bluebox box">
                                    <span>상한가달성</span> 2018.06.16
                                </div>
                            </div>
                        </div>
                        <div class="title-box item">
                            <span class="title">남북경협주 보락 상한가!</span>
                            <div class="subinfo">
                                <div class="redbox box">
                                    <span>발굴</span> 2018.04.25 
                                </div>
                                <div class="bluebox box">
                                    <span>상한가달성</span> 2018.06.16
                                </div>
                            </div>
                        </div>
                        <div class="title-box item">
                            <span class="title">남북경협주 보락 상한가!</span>
                            <div class="subinfo">
                                <div class="redbox box">
                                    <span>발굴</span> 2018.04.25 
                                </div>
                                <div class="bluebox box">
                                    <span>상한가달성</span> 2018.06.16
                                </div>
                            </div>
                        </div>
                        <div class="title-box item">
                            <span class="title">남북경협주 보락 상한가!</span>
                            <div class="subinfo">
                                <div class="redbox box">
                                    <span>발굴</span> 2018.04.25 
                                </div>
                                <div class="bluebox box">
                                    <span>상한가달성</span> 2018.06.16
                                </div>
                            </div>
                        </div> -->
                        <!-- <div class="title-box item">
                            <span class="title">남북경협주 보락 상한가!</span>
                            <div class="subinfo">
                                <div class="redbox box">
                                    <span>발굴</span> 2018.04.25 
                                </div>
                                <div class="bluebox box">
                                    <span>상한가달성</span> 2018.06.16
                                </div>
                            </div>
                        </div> -->
                    <!-- </div> -->
                </div>
                <div class="algorithm-container content-box">
                    <span class="title">
                        로보어드바이저 성향
                        <span class="sub-info">
                            ${adviser.adviser_name}(으)로 <br>
                            상승이 예상되는 종목을 확인하세요.
                        </span>
                    </span>
                    <div class="algorithm-content">
                        <div class="algorithm" style="width:100%;">
                            <div class="bar-holder" data-width="${adviser.adviser_tendency_first}">
                                <span style="${adviser.adviser_tendency_first}%"></span>
                            </div>
                            <div class="clearfix">
                                <span class="pull-left">장기</span>
                                <span class="pull-right">단기</span>
                            </div>
                        </div>
                        <div class="algorithm" style="width:100%;">
                            <div class="bar-holder" data-width="${adviser.adviser_tendency_second}">
                                <span style=""></span>
                            </div>
                            <div class="clearfix">
                                <span class="pull-left">우량주</span>
                                <span class="pull-right">소형주</span>
                            </div>
                        </div>
                        <div class="algorithm" style="width:100%;">
                            <div class="bar-holder" data-width="${adviser.adviser_tendency_third}">
                                <span style=""></span>
                            </div>
                            <div class="clearfix">
                                <span class="pull-left">안정성</span>
                                <span class="pull-right">수익성</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fixed-bottom clearfix">
                    <a onclick="javascript:purchaseAdviser('${adviser.adviser_cd}')" class="pull-left free">${msg}</a>
                    <a href="/adviser/adviserRecommendItem.do?adviserCd=${adviser.adviser_cd}" class="pull-left searchadvisor">어드바이저 종목 검색</a>
                </div>
            </div>
        </div>
        
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      <script src="/js/vendor/bootstrap.min.js"></script>       
    <!-- veiwport for countnumber -->
    <script src="/js/owl.carousel.min.js"></script>
    <script src="/js/main.js"></script>     
    <script>
  /*      var owl = $('.owl-carousel');
        owl.owlCarousel({
            autoplay:true,
            autoplayTimeout:5000,
            items:1,
            loop:true,
            margin:0,
            nav:false,
            dots:true,
        })  */
    </script>   
    </body>
</html>
