<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko" style="height:100%;">      
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
<link rel="stylesheet" href="/css/animsition.min.css">
<body style="height:100%; text-align: center;">

<script type="text/javascript">  
  
$(document).ready(function(){  

	 if("${paramMap.findId}" == ""){ 
		var userInputId = getCookie("userInputId");
		$("#loginId").val(userInputId);

		var userInputPw = getCookie("userInputPw"); 
		$("#loginPwd").val(userInputPw);
		 } 

	try {
		if(window.carrierJrpr != null){
			window.carrierJrpr.loginForApp("${kakaoId}", "");
		}
		if(window.external != null){
	        window.external.loginForPC("${kakaoId}", "");
	    }
	}catch(exception){
	    
	}finally {
		
	}

	
	if($("#loginId").val() != "" && $("#loginPwd").val() != "" && getParameter("status") != "logout"){
		
		loginProcess();
	}	

	
	
	$(".animsition").animsition({
	    inClass: 'zoom-in-lg',
	    outClass: 'zoom-out-lg',
	    inDuration: 1500,
	    outDuration: 800,
	    linkElement: '.animsition-link',
	    // e.g. linkElement: 'a:not([target="_blank"]):not([href^="#"])'
	    loading: true,
	    loadingParentElement: 'body', //animsition wrapper element
	    loadingClass: 'animsition-loading',
	    loadingInner: '', // e.g '<img src="loading.svg" />'
	    timeout: false,
	    timeoutCountdown: 5000,
	    onLoadEvent: true,
	    browser: [ 'animation-duration', '-webkit-animation-duration'],
	    // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
	    // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
	    overlay : false,
	    overlayClass : 'animsition-overlay-slide',
	    overlayParentElement : 'body',
	    transition: function(url){ window.location.href = url; }
	  });


	
  
});


function getParameter(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}


function getCookie(cookieName) {
    cookieName = cookieName + '=';
    var cookieData = document.cookie;
    var start = cookieData.indexOf(cookieName);
    var cookieValue = '';
    if(start != -1){
        start += cookieName.length;
        var end = cookieData.indexOf(';', start);
        if(end == -1)end = cookieData.length;
        cookieValue = cookieData.substring(start, end);
    }
    return unescape(cookieValue);
}

function setCookie(cookieName, value, exdays){
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var cookieValue = escape(value) + ((exdays==null) ? "" : "; expires=" + exdate.toGMTString());
    document.cookie = cookieName + "=" + cookieValue;
}

function logout(){

	if(confirm("로그아웃 하시겠습니까?")){
		
		try {
			if(window.carrierJrpr != null){
				window.carrierJrpr.logoutForApp("");
			}
			if(window.external != null){
	            window.external.logoutForPC("");
	        }
		}catch(exception){
		    
		}finally {
			document.location.href = "/kakao/logout.do";	
		}
	}
}
    

function loginForApp(userId){
	window.carrierJrpr.loginForApp(userId, "");
}



function logoutForApp(){
	window.carrierJrpr.logoutForApp("");
}    
    
    
    
function loginProcess(){
	
	if($("#loginId").val() == ""){
		//alert("아이디가 입력 되지 않았습니다.");
		//return;
		$.alert("아이디가 입력 되지 않았습니다.",function(a){
	    });
		return;
	}
	if($("#loginPwd").val() == ""){
		//alert("패스워드가 입력 되지 않았습니다.");
		//return;
		$.alert("패스워드가 입력 되지 않았습니다.",function(a){
	    });
		return;
	}

	setCookie("userInputId", $("#loginId").val(), 365);
	
	$.ajax({ 
        type: 'post' ,
        url : '/loginPersist.do' ,
        dataType : 'json' ,
        data : $('#loginForm').serialize(), 
        
        success : function(jsonData, textStatus, jqXHR) {
        	if(jsonData.resultCode =="0000"){

        		setCookie("userInputId", $("#loginId").val(), 365);
        		setCookie("userInputPw", $("#loginPwd").val(), 365);
            	goMainPage();
            	
        	}else if(jsonData.resultCode =="E001"){
        		$.alert("로그인에 실패 하였습니다. 관리자에게 문의 하세요.",function(a){
        			
        	    });
        	}else if(jsonData.resultCode =="E002"){
        		$.alert("휴직 또는 퇴사처리 된 아이디 입니다. 관리자에게 문의 하세요.",function(a){
        			
        	    });
        	}
        } ,
        error : function(xhRequest, ErrorText, thrownError) {
          //  alertEx("시스템 에러");
        }
    }); 
}

function goMainPage(){
	document.location.href = "/carrier/main.do";
}
    
    
function changePassWord(){
	document.location.href = "/carrier/changePassWord.do"
}
    
  


function backKeyController(str){
	//if(window.Android != null){
		//history.go(-1);
		
		document.location.href="/carrier/three-option.do";
	//}	
} 

function findForCustomerId(){

	document.location.href="/carrier/change-myinfo.do";
	
}


function goChagnePassword(){

 document.location.href ="/carrier/change-password.do";

	
}



 
</script>



		<!-- <div class="content-container"style="background-image: url(/img/.png); overflow-y:hidden;" > -->
		<div class="content-container"style="overflow-y:hidden;" >
		<!-- style="background-image: url(/img/tree_PNG.png);  -->
            <!-- <header class="bg-pink clearfix"> -->
                <!-- <div class="search-icon">
                    <a href="#"></a> 
                </div> -->
                <!-- <div class="" style="width:70%; text-align:center;">
                	<img style="width:100%; height:75%;" src="/img/main_logo.png" alt="">
                </div> -->
                <!-- <div class="menu-bar pull-right">
                    <a style="cursor:pointer;" href="/adviser/adviser.do"><img src="/img/close-white-icon.png" alt=""></a>
                </div> -->
            <!-- </header> -->
            <div class="" style="width:19%;margin-top:10%; ">
            
                    <a href="/carrier/three-option.do?status=${paramMap.status}"><img style="zoom: 0.5;" src="/img/back-icon.png" alt=""></a> 
                </div> 
            <div style=" display:table; margin:0 auto; display:inline-block; width:60%; height:100%;" 	class="animsition">
            
                	<img style="width:100%; height:75%; margin-top:55%;" src="/img/main_logo.png" alt="">
                
           				 <form action="" id="loginForm" style="margin-top:40%;">
                        <div class="username d-table">
                            <span class="d-tbc" style="font-weight:bold;">로그인 아이디</span>
                            <div style="margin-top:5%;">
                            <c:if test ='${paramMap.findId eq ""}'>
                                <input type="text" style="width:80%; padding:2%; "placeholder="아이디를 입력해 주세요" class="d-tbc" id="loginId" name="loginId" value="">
                            </c:if>
                            
                            <c:if test ='${paramMap.findId ne ""}'>
                                <input type="text" style="width:80%; padding:2%; "placeholder="아이디를 입력해 주세요" class="d-tbc" id="loginId" name="loginId" value="${paramMap.findId}">
                            </c:if>
                            
                            </div>
                            
                        </div>
                        <div class="password d-table" style="margin-top:10%;">
                            <span class="d-tbc" style="font-weight:bold;">패스워드</span>
                            <div style="margin-top:5%;">
                          	     <input type="password"  style="width:80%; padding:2%; "placeholder="패스워드를 입력해 주세요" class="d-tbc" id="loginPwd" name="loginPwd"   onkeypress="if(event.keyCode=='13') loginProcess();">
                            </div>
                        </div>
                       </form>
                       <div class="login-btn-container d-table">
                           <span class="d-tbc"></span>
                           <div style="margin-top:10%; text-align:center; width:100%;">
                           		<!-- <div style="width:30%; border:3px solid #eee;"> -->
                           		<div style="border-radius: 50%; height: 30%; width: 25%; background-color: #CEF6F5; margin-left: 40%;">
						
                               	<a style="cursor:pointer; border-radius: 80%; background: #CEF6F5;" onclick="javascript:loginProcess();" class="login-btn d-tbc">로그인</a>&nbsp;
                               	</div>
                               	<br><br>
                               	
                               	
                               	<div class="password d-table" style="margin-top: 8%; width:100%;">
											<img style=" width:10%; height:6%;" src="/img/icons8-qqq-48.png" />
                               			<a style="cursor: pointer; width: 100%;  background:/*  #CAF4FB; */"
											onclick="javascript:findForCustomerId();" class="">
										&nbsp;<span class="d-tbc" style="font-weight: bold; color: #585858;">아이디가 기억이 나지 않으세요?</span></a>
							</div>
							<br>
                      <div class="password d-table" style="margin-top:0%;">
								<img style=" width:10%; height:6%;" src="/img/icons8-change-48.png" />
									<a style="cursor: pointer; width: 100%;  background:/*  #CAF4FB; */"
								onclick="javascript:goChagnePassword();" class="">
								<span class="d-tbc" style="font-weight: bold; color: #585858;"> 비밀번호를 변경하고 싶으신가요?</span></a>
						</div>
                               	<!-- </div> -->
                           </div>
                       </div>
                   </div>    
     
      	</div>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      
    <script src="/js/vendor/bootstrap.min.js"></script>       
    <!-- veiwport for countnumber -->
    <script src="/js/jquery.viewportchecker.js"></script>    
    <script src="/js/alert.js"></script>
    <script src="/js/main.js"></script> 
    <script src="/js/animsition.min.js"></script>
</body>
</html>
