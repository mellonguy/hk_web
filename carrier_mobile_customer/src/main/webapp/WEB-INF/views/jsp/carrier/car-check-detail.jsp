<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!doctype html>
<html lang="ko">
    <jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
    <body  class="white">
    
    <script type="text/javascript">  
    
    $(document).ready(function(){
    	
    	//getNewsData("${searchWord}");
    	
    });

    function backKeyController(str){
    	
    	history.go(-1);
    	
    }
    
    
    </script>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->


<div class="content-container withads themedetails companydetails">
            <header class="bg-pink clearfix" style="position:fixed; background-color:#fff; border:none; text-align:center; z-index:3; ">
                <div class="" style="width:19%;">
                    <a href="javascript:history.go(-1);"><img style="zoom: 0.5;" src="/img/back-icon.png" alt=""></a> 
                </div> 
                <div class="" style="width:60%;">
                	<!-- <img style="width:90%; height:60%;" src="/img/main_logo.png" alt=""> -->
                	<img style="width:90%; height:60%;" onclick="javascript:document.location.href='/carrier/main.do'" src="/img/main_logo.png" alt="">
                </div>
                <div class="" style="width:19%; float:right;">
                   <a style="cursor:pointer;" href="javascript:logout();"><img style="" src="/img/logout.png" alt=""></a>
                </div>
            </header>
            <div style="text-align:right; clear:both;">${user.driver_name}님 환영 합니다.</div>
            <div class="company-details-container">
                
                <div class="overview-box active content-view" style="width:100%; overflow-x:scroll; clear:both; margin-top:50px;">
                	<form id="checkDetailForm" style="" name="checkDetailForm" method="post" >
                		<div id="sendImageForCustomer">
                		<div style="text-align:center;" ><img style="width:10%; height:10%; display:inline;" src="/img/hkccarrier_logo.png" alt="" /><h4 class="price" style="display:inline;">한국카캐리어 (주) 차량 인수 인계 확인서</h4></div>
		                 <div id="carCheckImage" style="position:relative;">
				        	<input type="hidden" id="allocationId" name="allocationId" value="${allocationList[0].allocation_id}" />
				        	<input type="hidden" id="allocationStatus" name="allocationStatus" value="${allocationList[0].allocation_status_cd}" />
				        	<img style="width:100%; margin-bottom:0px;" src="/img/checkCarImage.png" alt="">
				        	<input type="checkbox" id="frontBumper" name="frontBumper" style="transform : scale(1.5); position:absolute; top:0%; left:43%;" ><!--앞범퍼  -->
				        	<input type="checkbox" id="leftFrontFender" name="leftFrontFender" style="transform : scale(1.5); position:absolute; top:9%; left:9%;"/><!-- 좌휀다(전) -->
				        	<input type="checkbox" id="frontPanel" name="frontPanel" style="transform : scale(1.5); position:absolute; top:9%; left:43%;"/><!-- 전판넬 -->
				        	<input type="checkbox" id="rightFrontFender" name="rightFrontFender" style="transform : scale(1.5); position:absolute; top:9%; left:75%;"/><!-- 우휀다(전)  -->
				        	<input type="checkbox" id="leftHouse" name="leftHouse" style="transform : scale(1.5); position:absolute; top:32%; left:20%;"/><!-- 하우스(좌) -->
				        	<input type="checkbox" id="bonnet" name="bonnet" style="transform : scale(1.5); position:absolute; top:22%; left:44%;"/><!--본넷  -->
				        	<input type="checkbox" id="rightHouse" name="rightHouse" style="transform : scale(1.5); position:absolute; top:32%; left:75%;"/><!-- 하우스(우) -->
				        	<input type="checkbox" id="leftSideStep" name="leftSideStep" style="transform : scale(1.5); position:absolute; top:37.5%; left:0.5%;"/><!--사이드 스텝(좌)  -->
				        	<input type="checkbox" id="leftFrontDoor" name="leftFrontDoor" style="transform : scale(1.5); position:absolute; top:41%; left:23.1%;"/><!--좌도어(전)  -->
				        	<input type="checkbox" id="rightFrontDoor" name="rightFrontDoor" style="transform : scale(1.5); position:absolute; top:41%; left:74%;"/><!-- 우도어(전) -->
				        	<input type="checkbox" id="rightSideStep" name="rightSideStep" style="transform : scale(1.5); position:absolute; top:37.5%; left:96%;"/><!-- 사이드 스텝(우)  -->
				        	<input type="checkbox" id="leftBackDoor" name="leftBackDoor" style="transform : scale(1.5); position:absolute; top:56.5%; left:23.1%;"/><!-- 좌도어(후) -->
				        	<input type="checkbox" id="top" name="top" style="transform : scale(1.5); position:absolute; top:54%; left:44%;"/><!-- 천정 -->
				        	<input type="checkbox" id="rightBackDoor" name="rightBackDoor" style="transform : scale(1.5); position:absolute; top:56.5%; left:74%;"/><!-- 우도어(후) -->
				        	<input type="checkbox" id="trunk" name="trunk" style="transform : scale(1.5); position:absolute; top:78%; left:44%;"/><!-- 트렁크 -->
				        	<input type="checkbox" id="leftBackFender" name="leftBackFender" style="transform : scale(1.5); position:absolute; top:86%; left:22%;"/><!-- 좌휀다(후) -->
				        	<input type="checkbox" id="backPanel" name="backPanel" style="transform : scale(1.5); position:absolute; top:84.5%; left:44%;"/><!-- 후판넬 -->
				        	<input type="checkbox" id="rightBackFender" name="rightBackFender" style="transform : scale(1.5); position:absolute; top:86%; left:75%;"/><!-- 우휀다(후) -->
				        	<input type="checkbox" id="backBumper" name="backBumper" style="transform : scale(1.5); position:absolute; top:94%; left:43%;"/><!-- 후범퍼 -->
				        </div>
                
		                <div class="overview-box active content-view" style=" clear:both; margin-top:50px;">
		                   <div class="view-details">
		                       <div class="overview detail-box">
		                           <!-- <span class="price">확인 사항</span> -->
		                       
		                           <div class="price-info-box">
		                           		<span style="color:#f00;">*알림</span><br>
		                           		<span style="color:#f00; width:100%;">차량 입고시 차량내 "귀중품"은 당사에서 책임지지 않사오니 이점 유의 하여 주시기 바랍니다.</span>
		                           		*아래 사항은 고객님께서 직접 확인 후 체크 하여 주시기 바랍니다.
		                            </div>
		                            <div class="other-info d-table">
		                               <div class="list-category left t-cell" style="border-right:none; ">
		                                   <span class="label-text" style="width:95%; font-size:12px;">1. 고객님께서 주문하신 차종/모델/색상/옵션이 맞습니까?</span>
		                                   <span class="value" style="width:5%;"><input type="checkbox" id="firstCheck" name="firstCheck" style="transform : scale(1.5);"></span>
		                               </div>
		                           </div>
		                           <div class="other-info d-table">
		                               <div class="list-category left t-cell" style="border-right:none; ">
		                                   <span class="label-text" style="width:95%; font-size:12px;">2. 차량의 외부(도색,유리,타이어) 및 내부(내장,시트,선팅)에는 이상이 없습니까?</span>
		                                   <span class="value" style="width:5%;"><input type="checkbox" id="secondCheck" name="secondCheck" style="transform : scale(1.5);"></span>
		                               </div>
		                           </div>
		                           <div class="other-info d-table">
		                               <div class="list-category left t-cell" style="border-right:none; ">
		                                   <span class="label-text" style="width:95%; font-size:12px;">3. 삼각대,예비타이어(일부차종제외),OVM 공구 등이 모두 적재 되어 있습니까?</span>
		                                   <span class="value" style="width:5%;"><input type="checkbox" id="thirdCheck" name="thirdCheck" style="transform : scale(1.5);"></span>
		                               </div>
		                           </div>
		                           <div class="other-info d-table">
		                               <div class="list-category left t-cell" style="border-right:none; ">
		                                   <span class="label-text" style="width:95%; font-size:12px;">4.Welcome Kit(차량매뉴얼,등록증사본,보험가입증명서)가 구비되어 있습니까?</span>
		                                   <span class="value" style="width:5%;"><input type="checkbox" id="fourthCheck" name="fourthCheck" style="transform : scale(1.5);"></span>
		                               </div>
		                           </div>
		                           <div class="price-info-box">
		                           		<p>*차량 인수 확인 서명 후 발생한 차량의 파손,운행 및 관리 책임에 대한 의무가 고객님께 있음을 인지 하여 주시기 바랍니다.</p>
		                           		<span style="margin-top:30px; width:30%; text-align:left;">기사명 : ${user.driver_name}</span>
		                           		<span style="margin-top:30px; width:18%; text-align:left;">서명 : </span>
										<img id="receiptSign" style="display:none; width:40%; height:30%;" src="" alt="" title="" />
		                            </div>
		                           
		                       </div>
		                   </div>
		                   </div>
		                   </div>
               		 </form>
                
                
                	<div id="signature-pad" class="m-signature-pad" style="z-index:-100000">
				        <div class="m-signature-pad--body">
				            <canvas></canvas>
				        </div>
				        <div class="m-signature-pad--footer">
				            <div class="description">서명해 주세요.</div>
				            <button style="padding:8px;" type="button" class="button clear" data-action="clear">지우기</button>
				            <button style="padding:8px;" type="button" class="button cancel" data-action="cancel">취소</button>
				            <button style="padding:8px;" type="button" class="button save" data-action="save" allocationId="${allocationList[0].allocation_id}" allocationStatus = "${allocationList[0].allocation_status_cd}">저장</button>
				        </div>
				    </div>
                
                
                
                
                <%-- 	<div id="carCheckImage" style="position:relative;">
			        	<input type="hidden" id="allocationId" name="allocationId" value="${allocationList[0].allocation_id}" />
			        	<img style=" margin-bottom:50px;" src="/img/checkCarImage.png" alt="">
			        	<input type="checkbox" id="frontBumper" name="frontBumper" style="position:absolute; top:1%; left:85%; transform : scale(2);" ><!--앞범퍼  -->
			        	<input type="checkbox" style="transform : scale(2); position:absolute; top:10%; left:20%;"/><!-- 좌휀다(전) -->
			        	<input type="checkbox" style="transform : scale(2); position:absolute; top:10%; left:86%;"/><!-- 전판넬 -->
			        	<input type="checkbox" style="transform : scale(2); position:absolute; top:10%; left:148%;"/><!-- 우휀다(전)  -->
			        	<input type="checkbox" style="transform : scale(2); position:absolute; top:24%; left:35%;"/><!-- 하우스(좌) -->
			        	<input type="checkbox" style="transform : scale(2); position:absolute; top:22%; left:86%;"/><!--본넷  -->
			        	<input type="checkbox" style="transform : scale(2); position:absolute; top:24%; left:155%;"/><!-- 하우스(우) -->
			        	<input type="checkbox" style="transform : scale(2); position:absolute; top:43%; left:8%;"/><!--사이드 스텝(좌)  -->
			        	<input type="checkbox" style="transform : scale(2); position:absolute; top:43%; left:35%;"/><!--좌도어(전)  -->
			        	<input type="checkbox" style="transform : scale(2); position:absolute; top:43%; left:157%;"/><!-- 우도어(전) -->
			        	<input type="checkbox" style="transform : scale(2); position:absolute; top:43%; left:183%;"/><!-- 사이드 스텝(우)  -->
			        	<input type="checkbox" style="transform : scale(2); position:absolute; top:57%; left:35%;"/><!-- 좌도어(후) -->
			        	<input type="checkbox" style="transform : scale(2); position:absolute; top:51%; left:86%;"/><!-- 천정 -->
			        	<input type="checkbox" style="transform : scale(2); position:absolute; top:57%; left:157%;"/><!-- 우도어(후) -->
			        	<input type="checkbox" style="transform : scale(2); position:absolute; top:73.5%; left:86%;"/><!-- 트렁크 -->
			        	<input type="checkbox" style="transform : scale(2); position:absolute; top:84.5%; left:33%;"/><!-- 좌휀다(후) -->
			        	<input type="checkbox" style="transform : scale(2); position:absolute; top:80%; left:86%;"/><!-- 후판넬 -->
			        	<input type="checkbox" style="transform : scale(2); position:absolute; top:84.5%; left:157%;"/><!-- 우휀다(후) -->
			        	<input type="checkbox" style="transform : scale(2); position:absolute; top:89%; left:85%;"/><!-- 후범퍼 -->
			        </div> --%> 
                	
                </div>
                
                <div class="menu-container" style="width:100%; text-align:center;">
                	<%-- <c:if test="${allocationList[0].allocation_status_cd == 'S'}"> --%>
	                	<a class="kakao txt-bold" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:checkDetailConfirm('${allocationList[0].allocation_id}','${allocationList[0].allocation_status_cd}');">
			                 	고객서명
			            </a>
                	<%-- </c:if> --%>
            	</div>

				<input type="hidden" id="mailAddress" name="mailAddress" value="" />
				<input type="hidden" id="phoneNumber" name="phoneNumber" value="" />


            </div>
     
     </div>
        <%-- <div id="carCheckImage" style="position:relative;">
        	<input type="hidden" id="allocationId" name="allocationId" value="${allocationList[0].allocation_id}" />
        	<img style="width:100%; margin-bottom:0px;" src="/img/checkCarImage.png" alt="">
        	<input type="checkbox" id="frontBumper" name="frontBumper" style="position:absolute; top:0%; left:43%; transform : scale(2);" ><!--앞범퍼  -->
        	<input type="checkbox" style="position:absolute; top:9%; left:9%;"/><!-- 좌휀다(전) -->
        	<input type="checkbox" style="position:absolute; top:9%; left:43%;"/><!-- 전판넬 -->
        	<input type="checkbox" style="position:absolute; top:9%; left:75%;"/><!-- 우휀다(전)  -->
        	<input type="checkbox" style="position:absolute; top:32%; left:20%;"/><!-- 하우스(좌) -->
        	<input type="checkbox" style="position:absolute; top:22%; left:44%;"/><!--본넷  -->
        	<input type="checkbox" style="position:absolute; top:32%; left:75%;"/><!-- 하우스(우) -->
        	<input type="checkbox" style="position:absolute; top:37.5%; left:0.5%;"/><!--사이드 스텝(좌)  -->
        	<input type="checkbox" style="position:absolute; top:41%; left:23.1%;"/><!--좌도어(전)  -->
        	<input type="checkbox" style="position:absolute; top:41%; left:74%;"/><!-- 우도어(전) -->
        	<input type="checkbox" style="position:absolute; top:37.5%; left:96%;"/><!-- 사이드 스텝(우)  -->
        	<input type="checkbox" style="position:absolute; top:56.5%; left:23.1%;"/><!-- 좌도어(후) -->
        	<input type="checkbox" style="position:absolute; top:54%; left:44%;"/><!-- 천정 -->
        	<input type="checkbox" style="position:absolute; top:56.5%; left:74%;"/><!-- 우도어(후) -->
        	<input type="checkbox" style="position:absolute; top:78%; left:44%;"/><!-- 트렁크 -->
        	<input type="checkbox" style="position:absolute; top:86%; left:22%;"/><!-- 좌휀다(후) -->
        	<input type="checkbox" style="position:absolute; top:84.5%; left:44%;"/><!-- 후판넬 -->
        	<input type="checkbox" style="position:absolute; top:86%; left:75%;"/><!-- 우휀다(후) -->
        	<input type="checkbox" style="position:absolute; top:94%; left:43%;"/><!-- 후범퍼 -->
        </div> --%>
        
        

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      <script src="/js/vendor/bootstrap.min.js"></script>       
    <!-- veiwport for countnumber -->
    <script src="/js/jquery.viewportchecker.js"></script>   
    <script src="/js/alert.js"></script> 
    <script src="/js/vendor/jquery.form.min.js"></script>
    <script src="/js/vendor/signature_pad.min.js"></script>
    <script src="/js/vendor/html2canvas.min.js"></script>
    <script src="/js/main.js"></script>       
    <script src="/js/messagebox.js"></script>
    
    <script src="/js/vendor/jquery.loading-indicator.min.js"></script>
    
    <script>
    
    var homeLoader;
    $(document).ready(function(){


    	homeLoader = $('body').loadingIndicator({
			useImage: false,
			showOnInit : false
		}).data("loadingIndicator");
    	
    });
       
    
    function checkDetailConfirm(allocationId,allocationStatus){
    	
    	/* <input type="checkbox" id="frontBumper" name="frontBumper" style="transform : scale(1.5); position:absolute; top:0%; left:43%;" ><!--앞범퍼  -->
    	<input type="checkbox" id="leftFrontFender" name="leftFrontFender" style="transform : scale(1.5); position:absolute; top:9%; left:9%;"/><!-- 좌휀다(전) -->
    	<input type="checkbox" id="frontPanel" name="frontPanel" style="transform : scale(1.5); position:absolute; top:9%; left:43%;"/><!-- 전판넬 -->
    	<input type="checkbox" id="rightFrontFender" name="rightFrontFender" style="transform : scale(1.5); position:absolute; top:9%; left:75%;"/><!-- 우휀다(전)  -->
    	<input type="checkbox" id="leftHouse" name="leftHouse" style="transform : scale(1.5); position:absolute; top:32%; left:20%;"/><!-- 하우스(좌) -->
    	<input type="checkbox" id="bonnet" name="bonnet" style="transform : scale(1.5); position:absolute; top:22%; left:44%;"/><!--본넷  -->
    	<input type="checkbox" id="rightHouse" name="rightHouse" style="transform : scale(1.5); position:absolute; top:32%; left:75%;"/><!-- 하우스(우) -->
    	<input type="checkbox" id="leftSideStep" name="leftSideStep" style="transform : scale(1.5); position:absolute; top:37.5%; left:0.5%;"/><!--사이드 스텝(좌)  -->
    	<input type="checkbox" id="leftFrontDoor" name="leftFrontDoor" style="transform : scale(1.5); position:absolute; top:41%; left:23.1%;"/><!--좌도어(전)  -->
    	<input type="checkbox" id="rightFrontDoor" name="rightFrontDoor" style="transform : scale(1.5); position:absolute; top:41%; left:74%;"/><!-- 우도어(전) -->
    	<input type="checkbox" id="rightSideStep" name="rightSideStep" style="transform : scale(1.5); position:absolute; top:37.5%; left:96%;"/><!-- 사이드 스텝(우)  -->
    	<input type="checkbox" id="leftBackDoor" name="leftBackDoor" style="transform : scale(1.5); position:absolute; top:56.5%; left:23.1%;"/><!-- 좌도어(후) -->
    	<input type="checkbox" id="top" name="top" style="transform : scale(1.5); position:absolute; top:54%; left:44%;"/><!-- 천정 -->
    	<input type="checkbox" id="rightBackDoor" name="rightBackDoor" style="transform : scale(1.5); position:absolute; top:56.5%; left:74%;"/><!-- 우도어(후) -->
    	<input type="checkbox" id="trunk" name="trunk" style="transform : scale(1.5); position:absolute; top:78%; left:44%;"/><!-- 트렁크 -->
    	<input type="checkbox" id="leftBackFender" name="leftBackFender" style="transform : scale(1.5); position:absolute; top:86%; left:22%;"/><!-- 좌휀다(후) -->
    	<input type="checkbox" id="backPanel" name="backPanel" style="transform : scale(1.5); position:absolute; top:84.5%; left:44%;"/><!-- 후판넬 -->
    	<input type="checkbox" id="rightBackFender" name="rightBackFender" style="transform : scale(1.5); position:absolute; top:86%; left:75%;"/><!-- 우휀다(후) -->
    	<input type="checkbox" id="backBumper" name="backBumper" style="transform : scale(1.5); position:absolute; top:94%; left:43%;"/><!-- 후범퍼 --> */
    	
    	
        var statusCnt = 0;
        if(!$("#firstCheck").prop("checked")){
        	//$.alert("첫번째 항목이 체크 되지 않았습니다.",function(a){
        		statusCnt++;
			//});	
        	//return false;
        }
		if(!$("#secondCheck").prop("checked")){
			//$.alert("두번째 항목이 체크 되지 않았습니다.",function(a){
				statusCnt++;
			//});    	
			//return false;
		}
		if(!$("#thirdCheck").prop("checked")){
			//$.alert("세번째 항목이 체크 되지 않았습니다.",function(a){
				statusCnt++;
			//});
			//return false;
		}
		if(!$("#fourthCheck").prop("checked")){
			//$.alert("네번째 항목이 체크 되지 않았습니다.",function(a){
				statusCnt++;
			//});
			//return false;
		}
		
		 var mailAddress = "";
			 var phoneNumber = "";
			 
			 if(statusCnt > 0){
					$.confirm(statusCnt+"개의 항목이 체크 되지 않았습니다. 이대로 진행 하시겠습니까?",function(a){
				   		 if(a){
				   			updateAllocationForSendReceipt(allocationId,mailAddress,phoneNumber)
				   		 }else{
				   		 }
				   	});	
			}else{
				updateAllocationForSendReceipt(allocationId,mailAddress,phoneNumber)
			}
			 
			 
/* 		if(statusCnt > 0){
			$.confirm(statusCnt+"개의 항목이 체크 되지 않았습니다. 이대로 진행 하시겠습니까?",function(a){
		   		 if(a){
		   			 
		   			$.MessageBox({
		   				  input    : true,
		   				  buttonDone: "확인",
		   				  queue: true,
		   				  message  : "인수증을 수신할 메일 주소를 입력 해 주세요."
		   				}).done(function(data){
		   				  if ($.trim(data)) {
		   					mailAddress = data;
		   					$.MessageBox({
				   				  input    : true,
				   				  buttonDone: "확인",
				   				  message  : "인수증을 수신할 전화번호를 입력 해 주세요."
				   				}).done(function(data){
				   				  if ($.trim(data)) {
				   					//메일,전화번호 둘다 입력
				   					phoneNumber = data;
				   					updateAllocationForSendReceipt(allocationId,mailAddress,phoneNumber)
				   				  } else {
				   					  //메일 주소만 입력
				   					updateAllocationForSendReceipt(allocationId,mailAddress,phoneNumber)
				   				  }
				   				});
		   				  } else {
		   					$.MessageBox({
				   				  input    : true,
				   				  buttonDone: "확인",
				   				  message  : "인수증을 수신할 전화번호를 입력 해 주세요."
				   				}).done(function(data){
				   				  if ($.trim(data)) {
				   					 //전화번호만 입력
				   					phoneNumber = data;
				   					updateAllocationForSendReceipt(allocationId,mailAddress,phoneNumber)
				   				  } else {
				   					$.alert("인수증 전송이 취소 되었습니다. \r\n 메일 또는 전화번호 하나는 반드시 입력 되어야 합니다.",function(a){
				   	                });   
				   				  }
				   				});
		   				  }
		   				  
		   				});

		   		 }else{
		   		 }
		   		});	
		}else{
			
   			$.MessageBox({
   				  input    : true,
   				  buttonDone: "확인",
   				  queue: true,
   				  message  : "인수증을 수신할 메일 주소를 입력 해 주세요."
   				}).done(function(data){
   				  if ($.trim(data)) {
   					mailAddress = data;
   					$.MessageBox({
		   				  input    : true,
		   				  buttonDone: "확인",
		   				  message  : "인수증을 수신할 전화번호를 입력 해 주세요."
		   				}).done(function(data){
		   				  if ($.trim(data)) {
		   					//메일,전화번호 둘다 입력
		   					phoneNumber = data;
		   					updateAllocationForSendReceipt(allocationId,mailAddress,phoneNumber);
		   				  } else {
		   					  //메일 주소만 입력
		   					updateAllocationForSendReceipt(allocationId,mailAddress,phoneNumber);
		   				  }
		   				});
   				  } else {
   					$.MessageBox({
		   				  input    : true,
		   				  buttonDone: "확인",
		   				  message  : "인수증을 수신할 전화번호를 입력 해 주세요."
		   				}).done(function(data){
		   				  if ($.trim(data)) {
		   					 //전화번호만 입력
		   					phoneNumber = data;
		   					updateAllocationForSendReceipt(allocationId,mailAddress,phoneNumber);
		   				  } else {
		   					$.alert("인수증 전송이 취소 되었습니다. \r\n 메일 또는 전화번호 하나는 반드시 입력 되어야 합니다.",function(a){
		   	                });   
		   				  }
		   				});
   				  }
   				  
   				});
			
		} */
		
    }
    
 
    function mailChk(obj){
    	
    	 var trans_num = $(obj).val();
         if(trans_num != null && trans_num != ''){
             var regExp_ctn = /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i;
             if(regExp_ctn.test(trans_num)) {            
            	 $(obj).val(trans_num);
            	 return true;
             }else{
                 return false;
             }
       }
    	
    }


    function phoneChk(obj){

         var trans_num = $(obj).val().replace(/-/gi,'');
         if(trans_num != null && trans_num != ''){
             if(trans_num.length==11 || trans_num.length==10){   
            	 						var regExp_ctn = /^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})([0-9]{3,4})([0-9]{4})$/;
                 if(regExp_ctn.test(trans_num)){
                     trans_num = trans_num.replace(/^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})-?([0-9]{3,4})-?([0-9]{4})$/, "$1-$2-$3");                  
                     $(obj).val(trans_num);
                     return true;
                 }else{
                     return false;
                 }
             }else{
                 return false;
             }
       }
       
    }


    
   	function updateAllocationForSendReceipt(allocationId,mailAddress,phoneNumber){
   		
   	 	if(mailAddress != ""){
   			$("#mailAddress").val(mailAddress);
   			if(!mailChk($("#mailAddress"))){
   				$.alert("유효하지 않은 이메일 형식 입니다.",function(a){
				});
   	 			return false;
   	 		}
   		}
   		
   		if(phoneNumber != ""){
   			$("#phoneNumber").val(phoneNumber);
   			if(!phoneChk($("#phoneNumber"))){
   				$.alert("유효하지 않은 전화번호 입니다.",function(a){
				});
   				return false;
   			}
   		}
   		
   		$.ajax({ 
			type: 'post' ,
			url : "/allocation/updateAllocationForSendReceipt.do" ,
			dataType : 'json' ,
			data : {
				allocationId : allocationId,
				mailAddress : $("#mailAddress").val(),
				phoneNumber : $("#phoneNumber").val()
			},
			success : function(data, textStatus, jqXHR)
			{
				var resultData = data.resultData;
				$('html').scrollTop(0);
		   		$("#signature-pad").css('z-index','1');
		   		sign.on();
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
				
				alert("ajax error");
				
			}
		});
   		
   	}
    
    var canvas = $("#signature-pad canvas")[0];
    var sign = new SignaturePad(canvas, {
        minWidth: 2,
        maxWidth: 4,
        penColor: "rgb(0, 0, 0)"
    });
         
    
    $("[data-action]").on("click", function(){
        if ( $(this).data("action")=="clear" ){
            sign.clear();
        }else if ( $(this).data("action")=="save" ){
            if(sign.isEmpty()){
                $.alert("서명이 입력 되지 않았습니다.",function(a){
                	
                });
            }else{
            	var allocationId = $(this).attr("allocationId");
            	var allocationStatus = $(this).attr("allocationStatus");
            $.confirm('인계를 완료 하시겠습니까?',function(a){
           		 if(a){
    			       			
    				if(allocationId != "" && allocationStatus != ""){
    					var nextStatus = "";
    					if(allocationStatus == "S"){
    	    				 nextStatus = "I";
    	    			 }else if(allocationStatus == "I"){
    	    				 nextStatus = "P";
    	    			 }
    					//updateAllocationStatus(allocationId,nextStatus);
    					
    					$('#checkDetailForm').ajaxForm({
    						url: "/carrier/insertCheckDetail.do",
    					    type: "POST",
    						dataType: "json",
    						data : {
    							sign : sign.toDataURL()
    					    },
    						success: function(data, response, status) {
    							var status = data.resultCode;
    							var resultData = data.resultData[0];
    							if(status == '0000'){
    								$.alert("인계가 완료 되었습니다.",function(a){
    									homeLoader.show();
    									sign.clear();
    						        	$("#signature-pad").css('z-index','-10000');
    									$("#receiptSign").attr("src","/files/allocation"+resultData.allocation_file_path);
    									$("#receiptSign").css("display","inline");
    									 
    									html2canvas(document.querySelector("#sendImageForCustomer")).then(canvas => { 
    										$('#checkDetailForm').ajaxForm({
    				    						url: "/carrier/insertReceipt.do",
    				    					    type: "POST",
    				    						dataType: "json",
    				    						data : {
    				    							receipt : canvas.toDataURL(),
    				    							phoneNumber : $("#phoneNumber").val(),
    				    							mailAddress : $("#mailAddress").val()
    				    					    },
    				    						success: function(data, response, status) {
    				    							var resultCode = data.resultCode;
    				    							var fileData = data.resultData;
    				    							var phoneNum = data.resultDataSub;
    				    							/* if(window.Android != null && $("#phoneNumber").val() != ""){
        				    							window.Android.sendMessage($("#phoneNumber").val(),fileData);
        				    						} */
        				    						
    				    							if(window.Android != null && phoneNum != ""){
        				    							window.Android.sendMessage(phoneNum,fileData);
        				    						}
        				    						
    				    							if(resultCode == '0000'){
    				    								var rtnStatus = updateAllocationStatus(allocationId,"P");
    			    									if(rtnStatus == "F"){
    			    										document.location.href = "/carrier/allocation-detail.do?allocationStatus="+rtnStatus+"&allocationId="+allocationId;	
    			    									}else{
    			    										document.location.href = "/carrier/allocation-detail.do?allocationStatus="+nextStatus+"&allocationId="+allocationId;
    			    									}	
    				    							}
    				    						},
    				    						error: function() {
    				    							$.alert("오류가 발생 하였습니다.",function(a){
    				    							});
    				    						}                               
    				    					});
    										$("#checkDetailForm").submit();
    										
    						    			//document.body.appendChild(canvas);
    						    		}); 
    									
    									
    									/* var rtnStatus = updateAllocationStatus(allocationId,"P");
    									if(rtnStatus == "F"){
    										document.location.href = "/carrier/allocation-detail.do?allocationStatus="+rtnStatus+"&allocationId="+allocationId;	
    									}else{
    										document.location.href = "/carrier/allocation-detail.do?allocationStatus="+nextStatus+"&allocationId="+allocationId;
    									} */
    									
    								});
    							}else if(status == '1111'){
    								
    							}
    									
    						},
    						error: function() {
    							$.alert("차량 인계 중 오류가 발생 하였습니다.",function(a){
    							});
    						}                               
    					});
    					$("#checkDetailForm").submit();
    					
    				}	  
           		 }else{
           			$.alert("인계가 취소 되었습니다.",function(a){
           				//sign.clear();
           				//$("#signature-pad").css('z-index','-10000');
                    });
           		 }
           	});
            	
            	
            }
        }else if($(this).data("action") == "cancel"){
        	sign.clear();
        	$("#signature-pad").css('z-index','-10000');        	
        }
    });
     
     
    function resizeCanvas(){
        var canvas = $("#signature-pad canvas")[0];
 
        var ratio =  Math.max(window.devicePixelRatio || 1, 1);
        canvas.width = canvas.offsetWidth * ratio;
        canvas.height = canvas.offsetHeight * ratio;
        canvas.getContext("2d").scale(ratio, ratio);
    }
     
    $(window).on("resize", function(){
        resizeCanvas();
    });

    resizeCanvas();
    
    
    function logout(){
    	
    	$.confirm("로그아웃 하시겠습니까?",function(a){
    		 if(a){
    			 document.location.href = "/logout.do";	
    		 }
    	});
    	
    }  
    
    
    function goLiftCheckPage(allocationId,currentStatus){
    	document.location.href = "/carrier/lift-check.do?allocationId="+allocationId+"&allocationStatus="+currentStatus;
    }
    
    function fileUpload(allocationId,currentStatus,obj){
   	   	
    	//alert($("#upload").val());
    	
    	homeLoader.show();
    	$('#excelForm').ajaxForm({
			url: "/carrier/upLiftCheck.do",
			enctype: "multipart/form-data", 
		    type: "POST",
			dataType: "json",		
			data : {
				allocationId : allocationId,
				currentStatus : currentStatus
		    },
			success: function(data, response, status) {
				var status = data.resultCode;
				if(status == '0000'){
					/* $.alert("이미지가 등록 되었습니다.",function(a){ */
						 if(currentStatus == "Y"){			//다음 상태로 진행
							 currentStatus = "R";
		    			 }else if(currentStatus == "S"){		    				 
		    				 currentStatus = "I";
		    			 }else if(currentStatus == "P"){
		    				 currentStatus = "D";
		    			 }
						document.location.href = "/carrier/lift-check.do?allocationId="+allocationId+"&allocationStatus="+currentStatus;
					/* }); */
				}else if(status == '1111'){
							
				}
						
			},
			error: function() {
				alertEx("이미지 등록중 오류가 발생하였습니다.");
			}                               
		});
		$("#excelForm").submit();
    	
    	
    	
    	//$("#excelForm").attr("action","/carrier/upLiftCheck.do");
    	//$("#excelForm").submit();	
       	
    }
    
    
   	function updateAllocationStatus(allocationId,allocationStatus){
   		
   		var returnStatus = "";
   		
   		$.ajax({ 
			type: 'post' ,
			url : "/allocation/updateAllocationStatus.do" ,
			dataType : 'json' ,
			async: false,
			data : {
				allocationStatus : allocationStatus,
				allocationId : allocationId
			},
			success : function(data, textStatus, jqXHR)
			{
				var resultData = data.resultData;
				if(resultData != null && resultData.resultStatus != null && resultData.resultStatus == "F"){
					returnStatus = resultData.resultStatus;
				} 
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
   		
   		return returnStatus;
   	}
    
    function carCheck(id,status){
    	
    	var nextStatus = "";
    	var chkStatus = "";
    	
    	if(status == "Y"){
    		chkStatus = "상차검사";
		 }else if(status == "S"){
			 chkStatus = "인계";
		 }else if(status == "P"){
			 chkStatus = "하차검사";
		 }
   
    	 $.confirm(chkStatus+'를 진행 하시겠습니까?',function(a){
    		 if(a){
    			 if(status == "Y"){
    				 nextStatus = "R";
    			 }else if(status == "S"){
    				 nextStatus = "I";
    			 }else if(status == "P"){
    				 nextStatus = "D";
    			 }
    			 if(status == "S"){
    				 $('html').scrollTop(0);
    				 $("#signature-pad").css('z-index','1');
    				 sign.on();
    			 }else{
    				 updateAllocationStatus(id,nextStatus);
    				 $("#upload").trigger("click");	 
    			 }
    			 
    		 }
    	});
    }
    
    function allocationAccept(id,status){
    	//alert(allocationId);
    	var msg = "";
    	if(status == "Y"){
    		msg = "승인";
    	}else{
    		msg = "탁송 완료";
    	}
    	
    	//window.Android.toastShort( "JavscriptInterface Test" );
    	
    	$.confirm("해당 배차를 "+msg+" 하시겠습니까?",function(a){
	   		 if(a){
	   			$.ajax({ 
					type: 'post' ,
					url : "/allocation/updateAllocationStatus.do" ,
					dataType : 'json' ,
					data : {
						allocationStatus : status,
						allocationId : id
					},
					success : function(data, textStatus, jqXHR)
					{
						var result = data.resultCode;
						var resultData = data.resultData;
						if(result == "0000"){
							//alert(msg+"되었습니다.");
							/* $.alert(msg+"되었습니다.",function(a){ */
								//document.location.href = "/carrier/main.do";
								document.location.href = "/carrier/list.do?allocationStatus=${allocationStatus}";
							/* }); */
						}else if(result == "E000"){
							$.alert("해당 배차를 "+msg+" 할 수 없습니다.",function(a){
								document.location.href = "/carrier/list.do?allocationStatus=${allocationStatus}";
							});
						}else{
							$.alert("해당 배차를 "+msg+" 하는데 실패 했습니다. 관리자에게 문의 하세요.",function(a){
								document.location.href = "/carrier/list.do?allocationStatus=${allocationStatus}";
							});
						}
						
					} ,
					error : function(xhRequest, ErrorText, thrownError) {
					}
				});	
	   		 }
	   	});
    	
    }


    
    </script>
    
    
    
    
    
    
    
    
    </body>
</html>
