<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko"  style=" height:100%;">      
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
<link rel="stylesheet" href="/css/animsition.min.css">
    
<body style=" height:100%;">


<script type="text/javascript">  

$(document).ready(function(){
	
/* 	var updateToken = setInterval( function() {
		
		if(getDeviceToken() != null && getDeviceToken() != ""){
			updateDriverDeviceToken(getDeviceToken());
			if(window.Android != null){
				window.Android.startService();
			}
			clearInterval(updateToken);
		}
   }, 1000); */


	 $(".animsition").animsition({
		    inClass: 'fade-in-up-lg',
		    outClass: 'fade-out-up-lg',
		    inDuration: 1500,
		    outDuration: 800,
		    linkElement: '.animsition-link',
		    // e.g. linkElement: 'a:not([target="_blank"]):not([href^="#"])'
		    loading: true,
		    loadingParentElement: 'body', //animsition wrapper element
		    loadingClass: 'animsition-loading',
		    loadingInner: '', // e.g '<img src="loading.svg" />'
		    timeout: false,
		    timeoutCountdown: 5000,
		    onLoadEvent: true,
		    browser: [ 'animation-duration', '-webkit-animation-duration'],
		    // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
		    // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
		    overlay : false,
		    overlayClass : 'animsition-overlay-slide',
		    overlayParentElement : 'body',
		    transition: function(url){ window.location.href = url; }
		  });

	
});



function getDeviceToken(){
	
	var token = "";
	try{
		if(window.Android != null){
			token = window.Android.getDeviceToken("${driverId}");
		}	
	}catch(exception){
		
	}finally{
		
	}
	return token;
}  
  
/* 
function updateDriverDeviceToken(token){
	
	
	if(token != ""){
		$.ajax({ 
			type: 'post' ,
			url : "/updateDriverDeviceToken.do" ,
			dataType : 'json' ,
			data : {
				token : token
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					//$.alert("성공",function(a){
					//});
				}else if(result == "E000"){
					
				}else{
					
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
}  
 */


 function getNewsData(obj,keyword){
	
	 
	// alert($("#selectedStatus").children().length);
	 
	//alert($("#selectedStatus").children().first().get(0));	
	//alert($(obj).parent().get(0));
	
 	 if($("#selectedStatus").children().first().get(0) == $(obj).parent().get(0)){				//전체를 클릭 한경우
					
 		$("#selectedStatus").children().each(function(index,element){
 			$(this).removeClass('selected');
 		});
 		$(obj).parent().addClass('selected');
	}else{
			
	} 
	
	
	
/* 	
	
	$.ajax({ 
		type: 'post' ,
		url : "/research/newsSearch.do?&keyword="+keyword ,
		dataType : 'json' ,
		data : {
			
		},
		success : function(data, textStatus, jqXHR)
		{
			alert(data);
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
	 */
	
	
} 

 function goNewsPage(uri){
		
		//var replaceUrlStr = replaceAll(url, "?", "^");
		//replaceUrlStr = replaceAll(replaceUrlStr, "=", "+");
		
		var replaceUrlStr = encodeURIComponent(uri)
		document.location.href = "/news/newsPage.do?newsUri="+replaceUrlStr;
	}  


    
function viewAllocationData(allocationId,allocationStatus){
	homeLoader.show();
	document.location.href = "/carrier/allocation-detail.do?allocationStatus="+allocationStatus+"&allocationId="+allocationId;


	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
	
} 
 
 
function viewAllocationList(allocationId,allocationStatus){
	homeLoader.show();
	document.location.href = "/carrier/group-list.do?allocationId="+allocationId+"&allocationStatus="+allocationStatus;


	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
	
} 
 
 
 
function logout(){
	
	$.confirm("로그아웃 하시겠습니까?",function(a){
		 if(a){
			 if(window.Android != null){
					window.Android.stopService("${driverId}");
				}	
			 homeLoader.show();
			 document.location.href = "/logout.do";	
		 }
	});
	
}   

var selectedObj = new Object();


function comparePhoneNum(){

	if($("#phoneNum").val() != ""){

		$.ajax({
			type : 'post',
			url : '/carrier/comparePhoneNum.do',
			dataType : 'json',
			data : {

				phoneNum : $("#phoneNum").val(),
				
				
			},success : function(data, textStatus, jqXHR) {
		         var result = data.resultCode;
		         	
					if(result == "0000") {

					
						sendMessage(phoneNum);
					
							

						}else if (result == "0001") { 
						$.alert("선택한 핸드폰번호로 가입된 아이디가 없습니다.", function(a) {
	
					});	
				}  
			},
			error : function(xhRequest, ErrorText, thrownError) {
				
			}
		});


		}else{

			$.alert("핸드폰번호가 입력이 되지 않았습니다.");
			


			}
		
}


	selectedObj = new Object(); 

	function sendMessage(phoneNum){


		selectedObj = new Object();

		
		$.confirm("이 번호로 인증번호를 전송하시겠습니까?",function(a){

			 if(a){
					$(".certNum").css('display','block');	
					
					$.ajax({
						type : 'post',
						url : '/carrier/insertCertNumForAPP.do',
						dataType : 'json',
						data : {

							phoneNum : $("#phoneNum").val(),
							customerId : "",
							gubun : 'F',
							
						},success : function(data, textStatus, jqXHR) {
					         var result = data.resultCode;
					         	
								if(result == "0000") {

									$(".certNum").css('display','block');											
								
									}else if (result == "0002") { 
									$.alert("알 수 없는 오류입니다. 관리자에게 문의해주세요.", function(a) {
				
								});	
							}  
						},
						error : function(xhRequest, ErrorText, thrownError) {
							
						}
					});
					
				 
					

					 	
			 }else{

				/* $.alert("취소",function(a){
				}); */
			}
		});
	}
		
	


	
function sendPinNum(){


	var phoneCertNum = $("#phoneCertNum").val();

	
	if(phoneCertNum != ""){

				$.ajax({
					type : 'post',
					url  : "/carrier/checkCertNumForLogin.do",
				    dataType : 'json',
				    
				       data : {
					       
				    	
						phoneNum :  $("#phoneNum").val(),
						customerId : "",
						phoneCertNum : phoneCertNum,
						gubun : 'F',
						
						},

				   success : function(data, textStatus, jqXHR)
				    {
					   var resultCode = data.resultCode;
					   var resultData = data.resultData;
					   
					   if(resultCode == "0000"){
						 
							$.alert("인증에 성공하였습니다.", function(a) {
								
								$(".showID").css('display','block');		

								
								$("#selectID").val(resultData);
								
							});	
						 
					   }else{
						 $.alert("인증번호가 맞지 않습니다. 관리자에게 문의하세요.");
					   }
				   } ,
				  error :function(xhRequest, ErrorText, thrownError){
					  $.alert('알수 없는 오류입니다. 관리자에게 문의 하세요.');
	                  
					  }
				});
	
		}else{

			$.alert("인증번호가 입력되지 않았습니다.");
			return false;
			}
}



function findID(findId){


	document.location.href ="/carrier/sign-Up.do?&findId="+findId;
		
}




function goSingUpPage(){

	document.location.href='/carrier/simpleSign-Up.do?&customerId='+selectedObj.customerId+'&chargeId='+selectedObj.chargeId+'&phoneNum='+selectedObj.phoneNum;
	
}

 
function backKeyController(str){
	//history.go(-1);
	//location.href = document.referrer;
	
	document.location.href='/carrier/three-option.do';
}

function goChagnePassword(findId){

	document.location.href ="/carrier/sign-Up.do?&findId="+findId;
	
}

function goKakaoTalk(){


	//$.alert("준비중입니다.");


	$.confirm("카카오톡 채널로 이동 하시겠습니까?",function(a){
			 if(a){
				if(window.Android != null){
					window.Android.openKakaoChannel();

					
				}else if(webkit.messageHandlers != null){
					webkit.messageHandlers.scriptHandler.postMessage("goKakaoChannel");

				}else{
					$.alert("알수 없는 에러입니다. 관리자에게 문의해주세요");					
				}	
			 }
		});  
		
		
	
}
 
</script>


        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div class="content-container" style=" height:100%; ">
            <%-- <jsp:include page="/WEB-INF/views/jsp/common-top.jsp"></jsp:include> --%>
            <div class="">
           	 <header class="bg-pink clearfix" style="background-color:#fff; border:none; text-align:center; ">
                <div class="" style="width:19%;">
                    <a href="/carrier/three-option.do"><img style="zoom: 0.5;" src="/img/back-icon.png" alt=""></a> 
                </div> 
                <div class="" style="width:60%;">
                	<img style="width:90%; height:60%;" onclick="javascript:homeLoader.show(); document.location.href='/carrier/three-option.do'" src="/img/main_logo.png" alt="">
                </div>
                
               <div class="" style="width:19%; float:right;">
                   <!-- <a style="cursor:pointer;" href="javascript:logout();"><img style="" src="/img/logout.png" alt=""></a> -->
                </div> 
            </header>
            <%-- <div style="text-align:right;">${user.driver_name}님 환영 합니다.</div> --%>
            
		        
            <div style=" text-align:right; clear:both;">&nbsp;</div>
            <br>
	            
                
            <div class="content-container interest-page loadingthemepage news" style="background-color:#fff; clear:both; margin-top:50px;">
	            <!-- <div class="cat-list" style="background-color:#fff">
	                <ul class="clearfix" style="background-color:#fff">
	                	<li><a style="color:#000; width:15%;">출발일</a></li>
	                    <li><a style="color:#000;">상차지</a></li>
	                    <li><a style="color:#000;">하차지</a></li>
	                    <li><a style="color:#000;">차종</a></li>
	                    <li><a style="color:#000;">차대번호</a></li>
	                </ul>
	            </div> -->
	      
                
	            <div style="width:100%; margin-top:5px; height:40px; border-bottom:1px solid #eee;">
	              
	                  <div class="" style="width:100%;">
			      	   		<div class="" style="margin-top: -20%;  margin-left:30%;">
									<span class="d-tbc" style="font-weight: bold; ">※ 한국카캐리어 소개</span>
			        	    </div>
         			  </div>
            
            
  	 <!--     <div id="container" class="search-result news-list content-box" style="height:100%; margin-top:21%; overflow-y:scroll;">
            		<div class="xpull">
				    <div class="xpull__start-msg">
				        <div class="xpull__start-msg-text">화면을 당겼다 놓으면 새로 고침 됩니다.</div>
				        <div class="xpull__arrow"></div>
				    </div>
				    <div class="xpull__spinner">
				        <div class="xpull__spinner-circle"></div>
				    </div>
				</div>
            	<div class="news-container clearfix" style="">
    	 
	                
			                </div>
	                
       				 </div> -->
       				 

			          
			     <div class="animsition" style="margin-top: 10%; ">&nbsp;&nbsp;
						<span class="d-tbc" style="font-weight: bold; margin-top: 10%; color:#FF8000;">언제나 고객을 위한 열정으로 최고를 지향합니다.</span>
						 <br>
						 <p style="margin-top: 5%;">
						 한국카캐리어 고객용어플은 한국카캐리어와 계약을 맺은 거래처들의 탁송 건을 보여주기 위한 
						 한국카캐리어만의 고유한 서비스입니다.<br>
						 핸드폰번호를 입력하시고 회원가입을 하시면 더 많은 <span style="color:#81BEF7;">서비스</span>를 이용할 수 있습니다.
						 
							
						 </p>
												 
									<div class="" style="width:100%; text-align:center; margin-top:5%;">
												
												<div style="width:45%; display:inline-block;">
													<span class="d-tbc" style="font-weight: bold; color:#0000FF;">&lt;&nbsp;메인화면 페이지&nbsp;&gt;</span>
												  	 	<div class="" style="margin-top:5%; width:100%;" >
															<img style=" width:100%; height:55%;" src="/img/showmain.png" />
														</div> 
								        		</div>	 
								        		
								        		
								        		<div style="width:45%; display:inline-block;">	 
									        		<span class="d-tbc" style="font-weight: bold; color:#0000FF;">&lt;&nbsp;탁송이동 페이지&nbsp;&gt;</span>
												    	<div class="" style="margin-top:5%; width:100%;" >
															<img style=" width:100%; height:55%;" src="/img/expic02.jpg" />
														</div> 
												</div>
										</div>
									
									
										 <p style="margin-top: 5%;">
										메인화면페이지에서 대기중 /상차중 /이동중 /하차중 /완료	상태별로 각 
										탁송 건수가 나오고 이동중을 누르고 들어가면, 탁송 이동페이지에서 기사님의
										이동경로를 볼 수가 있습니다. 
										 </p>
										 <p >
										또한, 인수증함 버튼을 누르면, 월별로 조회할 수 있는 메뉴가 나오고, 
										조회할 월을 누르면 각 월별로 배차상세정보가 나옵니다.
										 </p>
				
									<div class="" style="width:100%; text-align:center; margin-top:5%;">
												
												<div style="width:45%; display:inline-block;">
													<span class="d-tbc" style="font-weight: bold; color:#0000FF;">&lt;&nbsp;인수증함 페이지&nbsp;&gt;</span>
												  	 	<div class="" style="margin-top:5%; width:100%;" >
															<img style=" width:100%; height:55%;" src="/img/expic03.jpg" />
														</div> 
								        		</div>	 
								        		
								        		
								        		<div style="width:45%; display:inline-block;">	 
									        		<span class="d-tbc" style="font-weight: bold; color:#0000FF;">&lt;&nbsp;차량조회 페이지&nbsp;&gt;</span>
												    	<div class="" style="margin-top:5%; width:100%;" >
															<img style=" width:100%; height:55%;" src="/img/expic04.jpg" />
														</div> 
												</div>
									</div>
										 <p >
											또한, 차량번호와 차대번호를 입력하시면 보다 쉽고 빠른 조회가 가능 합니다.
											
										 </p>
										  <img style=" width:6%; height:6%;" src="/img/icons8-warning-shield-48.png" />
										 <strong>잠깐! 회원가입을 하지 않아도 <span style="color:#81BEF7;">탁송금액조회</span>가 가능하다는 사실 알고계세요?</strong>
										 
									<div class="" style="width:100%; text-align:center; margin-top:5%;">
												
												<div style="width:45%; display:inline-block;">
													<span class="d-tbc" style="font-weight: bold; color:#0000FF;">&lt;&nbsp;탁송금액조회 페이지&nbsp;&gt;</span>
												  	 	<div class="" style="margin-top:5%; width:100%;" >
															<img style=" width:100%; height:55%;" src="/img/expic05.jpg" />
														</div> 
								        		</div>	 
								        		
								        		<div style="width:45%; display:inline-block;">	 
									        		<span class="d-tbc" style="font-weight: bold; color:#0000FF;">&lt;&nbsp;조회결과 페이지&nbsp;&gt;</span>
												    	<div class="" style="margin-top:5%; width:100%;" >
															<img style=" width:100%; height:55%;" src="/img/expic06.jpg" />
														</div> 
												</div>
									</div>	 
									
										<p style="margin-top: 5%;">
											탁송금액조회 페이지에서 
											출발지와 도착지를 입력해주시면 목록이 뜹니다. 맞는 지역을 선택해주시면 조회결과 페이지에서
											탁송예상금액과 거리가 나옵니다.<br>
											하지만 거리별 오차가 있기 때문에, 자세한 정보는 <span style="color:#81BEF7;"><strong>카카오톡 상담하기</strong></span>를 눌러 주시면
											언제든지 쉽고 빠른 답변을 받으실 수 있습니다!
										 </p>
										 
										 
										 	<div class="" style="width:100%; text-align:center; margin-top:5%;">
												
												<div style="width:45%; display:inline-block;">
													<span class="d-tbc" style="font-weight: bold; color:#0000FF;">&lt;&nbsp;간편탁송예약 페이지&nbsp;&gt;</span>
												  	 	<div class="" style="margin-top:5%; width:100%;" >
															<img style=" width:100%; height:55%;" src="/img/insertAllocation.png" />
														</div> 
								        		</div>	 
								        		
								        		<div style="width:45%; display:inline-block;">	 
									        		<span class="d-tbc" style="font-weight: bold; color:#0000FF;">&lt;&nbsp;탁송예약취소 페이지&nbsp;&gt;</span>
												    	<div class="" style="margin-top:5%; width:100%;" >
															<img style=" width:100%; height:55%;" src="/img/insertAllocationCansle.png" />
														</div> 
												</div>
										</div>
										 
										 
										<p style="margin-top: 5%;">
											간편탁송예약 페이지에서
											출발일 ,출발시간, 출발지, 도착지,차종, 차대번호를 입력하시면 간편하게 탁송을 예약할 수 있습니다.<br>
											예약 확정시 <span style="color:#81BEF7;"><strong>알림톡</strong></span>이 발송이 되며,
											예약조회 페이지에서 확인할 수 있습니다.<br>	
											또한, 단순변심으로 예약을 취소 할 수 있습니다.<br>(단 탁송이 확정되면 플러스채팅으로 문의주세요.)<br>
											오른쪽 탁송상태에 <span style="color:#81BEF7;"><strong>예약취소</strong></span> 버튼을 눌러주시면
											탁송예약이 취소 됩니다.
										 </p>
										 
										 	<img style=" width:6%; height:6%;" src="/img/icons8-warning-shield-48.png" />
										 	<strong>한국카캐리어를 처음 이용하세요?<span style="color:#81BEF7;"></span></strong>
										 	<br>
										 	<div class="" style="width:100%; text-align:center; margin-top:5%;">
										 			<div style="width:45%; display:inline-block;">
													<span class="d-tbc" style="font-weight: bold; color:#0000FF;">&lt;&nbsp;동의 페이지&nbsp;&gt;</span>
												  	 	<div class="" style="margin-top:5%; width:100%;" >
															<img style=" width:100%; height:55%;" src="/img/registerAgree.png" />
														</div> 
								        		</div>	 
								        		
								        		<div style="width:45%; display:inline-block;">	 
									        		<span class="d-tbc" style="font-weight: bold; color:#0000FF;">&lt;&nbsp;회원가입 페이지&nbsp;&gt;</span>
												    	<div class="" style="margin-top:5%; width:100%;" >
															<img style=" width:100%; height:55%;" src="/img/registerPage.png" />
														</div> 
												</div>
										 	</div>
										 	<p style="margin-top: 5%;">
												한국카캐리어를 처음 이용하시려는 고객님께서는 어려운 가입 절차 없이 회원가입이 가능합니다!<br>
												회원 가입 관련해서 문의사항이 있으면 플러스채팅으로 문의주세요.
												 
											</p>
											<div style="margin-top: 5%; margin-left:30%;height: 80%; margin-bottom:10%;" onclick ="javascript:goKakaoTalk();">
												<a >
												<img style="zoom: 0.5;" src="/img/icons8-change-48.png" alt=""></a> 
												<span class="d-tbc" style="font-weight: bold; color: #585858; ">&nbsp;&nbsp;플러스톡으로 이동!</span>
											</div>
										
										<strong>많은 탁송정보를 한눈에! 쉽고 간편한 한국카캐리어!</strong>
										
								<div style="margin-top: 5%; margin-left:30%;height: 80%; margin-bottom:10%;" onclick="javascript:homeLoader.show();document.location.href='/carrier/login-page.do?&optionType=L'">
										<!-- <img style="width:6%; height:6%;" src="/img/icons8-home-64.png" /> -->
										<a >
											<img style="zoom: 0.5;" src="/img/icons8-change-48.png" alt=""></a> 
												<span class="d-tbc" style="font-weight: bold; color: #585858; ">&nbsp;&nbsp;회원가입으로 이동!</span>
								</div>
				
					</div>
			            
	
   						</div>         
    	     	   </div>
     	    </div>
        </div>
        
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      
    <script src="/js/vendor/bootstrap.min.js"></script>       
    <!-- veiwport for countnumber -->
    <script src="/js/alert.js"></script>
    <script src="/js/main.js"></script>   
    <script src="/js/vendor/xpull.js"></script>       
    <script src="/js/vendor/jquery.loading-indicator.min.js"></script>
    <script src="/js/animsition.min.js"></script>
	<script src="/js/vendor/jquery.loading-indicator.min.js"></script>
    <script>
    
	
	$('#container').xpull({
	    'paused': false, 
	    // Pull threshold - amount in  pixels required to pull to enable release callback
        'pullThreshold':50,
        // Max pull down element - amount in pixels
        maxPullThreshold: 50,
        // timeout in miliseconds after which the loading indicator stops spinning.
        // If set to 0 - the loading will be indefinite
        'spinnerTimeout':1000,
        onPullStart: function(){},
        onPullEnd: function(){},
        callback: function(){window.location.reload()},

	});   
	
	
	var homeLoader;
	
	$(document).ready(function(){
			
		
				homeLoader = $('body').loadingIndicator({
					useImage: false,
					showOnInit : false
				}).data("loadingIndicator");
				
				/* setTimeout(function() {
					homeLoader.show();
					homeLoader.hide();
				}, 1000); */
		
	});
    
    </script>
        
    </body>
</html>
