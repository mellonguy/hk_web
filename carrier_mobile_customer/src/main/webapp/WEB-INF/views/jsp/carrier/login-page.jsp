<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%
	
%>

<!DOCTYPE html>
<html lang="ko" style="height: 100%;">

<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
<link rel="stylesheet" href="/css/animsition.min.css">  
<link rel="stylesheet" type="text/css" href="/css/jquery.bxslider.css"/>
<meta name="viewport" content="width=device-width, user-scalable=no">
<body style="height: 100%; text-align: center;">



<script type="text/javascript">
		$(document).ready(function() {

			var userInputId = getCookie("userInputId");
			$("#loginId").val(userInputId);

			try {
				if (window.carrierJrpr != null) {
					window.carrierJrpr.loginForApp("${kakaoId}", "");
				}
				if (window.external != null) {
					window.external.loginForPC("${kakaoId}", "");
				}
			} catch (exception) {

			} finally {

			}

			/* 	$('.bxslider').bxSlider({ 
			
			 auto: true,
			 speed: 500, 
			 pause: 4000, 
			 mode:'fade',
			 autoControls: true, 
			 pager:true, 

			 });
			 */

			$('.slick-items').bxSlider({

			  	auto: true,
			  	speed: 1000,			    
			    slideWidth: 900,
		//		mode:'fade',         // 사라지는 모양
			
				   randomStart:false,   // 이미지 랜덤으로 처리
				   controls:true,      // 좌,우 컨트롤 버튼 숨기기/보이기
				   autoControls:false,  //  슬라이드 시작/멈춤
				   pager:false,         // 하단 이미지 보기 버튼
				   auto:true,           // 자동시작

			});


			  $(".animsition").animsition({
				    inClass: 'fade-in-left',
				    outClass: 'fade-out-left',
				    inDuration: 1500,
				    outDuration: 800,
				    linkElement: '.animsition-link',
				    // e.g. linkElement: 'a:not([target="_blank"]):not([href^="#"])'
				    loading: true,
				    loadingParentElement: 'body', //animsition wrapper element
				    loadingClass: 'animsition-loading',
				    loadingInner: '', // e.g '<img src="loading.svg" />'
				    timeout: false,
				    timeoutCountdown: 5000,
				    onLoadEvent: true,
				    browser: [ 'animation-duration', '-webkit-animation-duration'],
				    // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
				    // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
				    overlay : false,
				    overlayClass : 'animsition-overlay-slide',
				    overlayParentElement : 'body',
				    transition: function(url){ window.location.href = url; }
				  });


			
		});

		function getCookie(cookieName) {
			cookieName = cookieName + '=';
			var cookieData = document.cookie;
			var start = cookieData.indexOf(cookieName);
			var cookieValue = '';
			if (start != -1) {
				start += cookieName.length;
				var end = cookieData.indexOf(';', start);
				if (end == -1)
					end = cookieData.length;
				cookieValue = cookieData.substring(start, end);
			}
			return unescape(cookieValue);
		}

		function setCookie(cookieName, value, exdays) {
			var exdate = new Date();
			exdate.setDate(exdate.getDate() + exdays);
			var cookieValue = escape(value)
					+ ((exdays == null) ? "" : "; expires="
							+ exdate.toGMTString());
			document.cookie = cookieName + "=" + cookieValue;
		}

		function logout() {

			if (confirm("로그아웃 하시겠습니까?")) {

				try {
					if (window.carrierJrpr != null) {
						window.carrierJrpr.logoutForApp("");
					}
					if (window.external != null) {
						window.external.logoutForPC("");
					}
				} catch (exception) {

				} finally {
					document.location.href = "/kakao/logout.do";
				}
			}
		}

		function loginForApp(userId) {
			window.carrierJrpr.loginForApp(userId, "");
		}

		function logoutForApp() {
			window.carrierJrpr.logoutForApp("");
		}

		function loginProcess() {

			
			if ($("#loginPwd").val() == "") {
				//alert("패스워드가 입력 되지 않았습니다.");
				//return;
				$.alert("입력이 되지 않았습니다.", function(a) {
				});
				return;
			}

			setCookie("userInputId", $("#loginId").val(), 365);

			$.ajax({
				type : 'post',
				url : '/loginPersist.do',
				dataType : 'json',
				data : $('#loginForm').serialize(),

				success : function(jsonData, textStatus, jqXHR) {
					if (jsonData.resultCode == "0000") {

						goMainPage();

					} else if (jsonData.resultCode == "E001") {
						//alert("로그인에 실패 하였습니다. 관리자에게 문의 하세요.");
						$.alert("로그인에 실패 하였습니다. 관리자에게 문의 하세요.", function(a) {

						});
					} else if (jsonData.resultCode == "E002") {
						//alert("로그인에 실패 하였습니다. 관리자에게 문의 하세요.");
						$.alert("휴직 또는 퇴사처리 된 아이디 입니다. 관리자에게 문의 하세요.",
								function(a) {

								});
					}
				},
				error : function(xhRequest, ErrorText, thrownError) {
					//  alertEx("시스템 에러");
				}
			});
		}

		/* function goMainPage() {
			document.location.href = "/carrier/main.do";
		} */
		function goCustomerListPage() {

			var certificationNumber =$("#certificationNumber").val().replace(/-/gi,'');
			

			
			document.location.href = "/carrier/customerList.do?&certificationNumber="+certificationNumber+"&optionType="+"${paramMap.optionType}";
		}

		function changePassWord() {
			document.location.href = "/carrier/changePassWord.do"
		}

		function backKeyController(str) {



			var optionType ="${paramMap.optionType}";
				if(optionType =='C'){


					document.location.href = "/carrier/main.do";

			}else{


			document.location.href = "/carrier/three-option.do";
				}

			
			
		/* 	if (window.Android != null) {
				window.Android.toastShort("뒤로가기 버튼으로 앱을 종료 할 수 없습니다.");
			} */

		}

		function goLoginPage(){

		document.location.href ="/carrier/sign-Up.do";
			}

		var arrInput = new Array(0);
		  var arrInputValue = new Array(0);

		
		function goCarNumSearch(){

			arrInput.push(arrInput.length);
			  arrInputValue.push("");
			  display();

			}

		function display() {
			  document.getElementById('parah').innerHTML="";
			  for (intI=0;intI<arrInput.length;intI++) {
			    document.getElementById('parah').innerHTML+=createInput(arrInput[intI], arrInputValue[intI]);
			  }
			}

	/* 	function createInput(id,value) {
			  return "<input  id ='carIdNum' style='width: 80%; padding:2%; text-align:center;'placeholder='차대번호 또는 차량번호로 조회가능합니다'  type='text' id='test "+ id +"' onChange='javascript:saveValue("+ id +",this.value)' value='"+value +"'><br><br>"+"<div style='border-radius: 50%; height: 30%; width: 25%; background-color: #CEF6F5; margin-left: 40%;'><a style='cursor: pointer; width: 100%; border-radius: 80%;  background: #CAF4FB;'onclick='javascript:goSearchCarNum();'><a style='cursor: pointer; width: 100%; border-radius: 80%; background: #CAF4FB;' onclick='javascript:goSearchCarNum();'>조회</span></a>"
			  ;
			}
 */
 

		
	/* 	function phoneChk(obj){

		     var trans_num = $(obj).val().replace(/-/gi,'');
		     if(trans_num != null && trans_num != ''){
		         if(trans_num.length==11 || trans_num.length==10){   
		        	 				var regExp_ctn = /^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})([0-9]{3,4})([0-9]{4})$/;
		             if(regExp_ctn.test(trans_num)){
		                 trans_num = trans_num.replace(/^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})-?([0-9]{3,4})-?([0-9]{4})$/, "$1-$2-$3");                  
		                 $(obj).val(trans_num);
		                 return true;
		             }else{
		                 return false;
		             }
		         }else{
		             return false;
		         }
		   }
		   
		} */

		

		function certification(certificationNumber){

	
			
			
			if ($("#certificationNumber").val() == "") {

					$.alert("입력이 되지 않았습니다.", function(a) {
					
				});
				return false;

				}else{
					
						
					
					$.ajax({
						type : 'post',
						url : '/carrier/certification-page.do',
						dataType : 'json',
						data : {

						certificationNumber : $("#certificationNumber").val(),
									

						},success : function(data, textStatus, jqXHR) {
					         var result = data.resultCode;
					         	
								if (result == "0000") {

									goCustomerListPage();									
								
								}  else if (result == "0001") {
									$.alert("조회 내역이 없습니다. 다시 한번 입력해주세요.", function(a) {

								});
							}  
						},
						error : function(xhRequest, ErrorText, thrownError) {
							
						}
					});
					

					
			}
			
			
			

	}



		function goSearchCarrier(){
			
			homeLoader.show();
			document.location.href = "/carrier/kakaoRouteView.do";


			var updateHomeLoader = setInterval(function() {
				clearInterval(updateHomeLoader);
				homeLoader.hide();
			}, 300);
			
			}


		function goMainPage(){
			
			homeLoader.show();
			document.location.href = "/carrier/three-option.do";


			var updateHomeLoader = setInterval(function() {
				clearInterval(updateHomeLoader);
				homeLoader.hide();
			}, 300);
			
			}


		function goRegPage(){

			document.location.href = "/carrier/agree-for-customer-register.do?&firstRegister=C";


			var updateHomeLoader = setInterval(function() {
				clearInterval(updateHomeLoader);
				homeLoader.hide();
			}, 300);
			

		}

		
	function goSearchCarNum(customerId){

	
			if ($("#carIdNum").val() == "") {

					$.alert("입력이 되지 않았습니다.", function(a) {
					
				});
				return false;

				}else{
					
						
					
					$.ajax({
						type : 'post',
						url : '/carrier/carIdNum-page.do',
						dataType : 'json',
						data : {

							carIdNum : $("#carIdNum").val(),
							customerId :customerId,		

						},success : function(data, textStatus, jqXHR) {
					         var result = data.resultCode; 
					         	
								if (result == "0000") {
									homeLoader.show();
									document.location.href ="/carrier/car-id-num-list.do?&carIdNum="+$('#carIdNum').val()+"&customerId="+"${paramMap.customerId}";							

									
								}  else if (result == "0001") {
									$.alert("조회 내역이 없습니다. 다시 한번 입력해주세요.", function(a) {

								});
							}  
						},
						error : function(xhRequest, ErrorText, thrownError) {
							
						}
					});
					

					
			}
			
			
			

	}


		
		
	</script>



	<div class="content-container">
		<!-- <header class="bg-pink clearfix"> -->
		<!-- <div class="search-icon">
                    <a href="#"></a> 
                </div> -->
		<!-- <div class="" style="width:70%; text-align:center;">
                	<img style="width:100%; height:75%;" src="/img/main_logo.png" alt="">
                </div> -->
		<!-- <div class="menu-bar pull-right">
                    <a style="cursor:pointer;" href="/adviser/adviser.do"><img src="/img/close-white-icon.png" alt=""></a>
                </div> -->
		<!-- </header> -->

		<div class="animsition" style="display: table; margin: 0 auto; display: inline-block; width: 90%; height: 100%;" >

			<img style="width: 100%; height: 75%; margin-top: 20%; margin-bottom: 20%;"
				src="/img/main_logo.png" alt="">

<!--<h4> 대한민국 NO.1 탁송업체</h4> -->
  		
  		<c:if test ="${paramMap.optionType eq 'L' }">
  
  	
<!-- 	  		<div class="slick-items">
			
			   <div><img style=" width:100%; height:100%;" src="/img/detailInfo-customer.png" /> <strong  style="color: #585858;" id='titleText'></strong></div>
			   <div><img style=" width:100%; height:100%;" src="/img/1213.jpg" /> <strong  style="color: #585858;" id='titleText'>한국카캐리어(주)</strong></div>
			    <div><img style=" width:100%; height:20%;" src="/img/hkcar1.png" /><strong  style="color: #585858;  margin-left: 5%;" id='titleText' >보이는 이미지는 실제 한국카캐리어(주) 탁송 사진 입니다.</strong></div>   
				<div><img style=" width:100%; height:20%;" src="/img/hkcar2.jpg" /><strong  style="color: #585858;  margin-left: 5%;" id='titleText' >보이는 이미지는 실제 한국카캐리어(주) 탁송 사진 입니다.</strong></div>   
				<div><img style=" width:100%; height:20%;" src="/img/hkcar3.jpg" /><strong  style="color: #585858;  margin-left: 5%;" id='titleText' >보이는 이미지는 실제 한국카캐리어(주) 탁송 사진 입니다.</strong></div>   
			   <div><img style=" width:100%; height:100%;" src="/img/car1.jpg" /> <strong  style="color: #585858;" id='titleText'>항상 최선으로 보답하는 한국카캐리어(주)</strong></div>
			   <div><img style=" width:100%; height:100%;" src="/img/car2.jpg" /><strong   style="color: #585858;" id='titleText'>국내외 메이커 차량 이동 전문</strong></div>
			   <div><img style=" width:100%; height:100%;" src="/img/car8.jpg" /><strong   style="color: #585858;"id='titleText'>수도권 전시장 딜리버리 전문</strong></div>
			   <div><img style=" width:100%; height:100%;" src="/img/car3.jpg" /><strong id='titleText2'><span  style="color: #585858;">l'll be the best!</span></strong></div>
	  
	  		</div> -->
  		</c:if>
  		
				  	<c:if test ="${paramMap.optionType eq 'C' }">
							 	<div class="slick-items">
									<div><img style=" width:100%; height:20%;" src="/img/kakaotalkask.jpg" /></div>
									<div><img style=" width:100%; height:20%;" src="/img/kakaoLogoRed.png" /></div>
								</div> 
								
				  	</c:if>
							<form action="" id="certificationForm" style="margin-top: 20%;">
				<!--
                       <div class="username d-table">
                             <span class="d-tbc" style="font-weight:bold;">로그인 아이디</span>
                            <div style="margin-top:5%;">
                                <input type="text" placeholder="아이디를 입력해 주세요" class="d-tbc" id="loginId" name="driverId">
                            </div>
                        </div> -->
					</form>
					
					<c:if test ="${paramMap.optionType eq 'L' }">
					
					<div class="" style="margin-top: -20%;">
							<span class="d-tbc" style="font-weight: bold; font-size:1.3em;  color: #585858;">한국카캐리어(주) 간편 회원가입</span></div>
							 <p style="margin-top: 5%;">
									 
									 회원가입을 하기전 !
									 <br>
									 한국카캐리어(주) 고객용 어플리케이션은<br> 한국카캐리어(주)와 계약을 맺은 거래처의 탁송정보를 보여주기 위한 <strong><span style="color:#81BEF7;">서비스 </span></strong>입니다.
									<br><br>
									<!-- <strong><span style="color:#81BEF7;"> 담당자 핸드폰번호</span></strong>가 없으면 가입이 되지 않는 점, <br> -->
								
									한번이라도 한국카캐리어(주) 탁송 서비스를 이용한 적 있으시면 <strong><span style="color:#81BEF7;">핸드폰번호</span></strong>만으로도 가입이 가능합니다.
									<br><br>
									아직 한번도 한국카캐리어(주) 탁송 서비스를 이용한 적 없으시면  밑에 <strong><span style="color:#81BEF7;" onclick ="javascript:homeLoader.show(); goRegPage();" >회원가입</span></strong>을 눌러주세요!
									 <br><br>
									<strong> 감사합니다.</strong>
										
									 </p>
							
						<div class="password d-table" style="margin-top: 5%;">
							<span class="d-tbc" style="font-weight: bold;">핸드폰번호로만 조회가 가능합니다.
							</span> <input type="number" pattern="\d*" style="width: 80%; padding:2%;
								 text-align:center;" placeholder=" ' - '  없이 입력해주세요 " class="d-tbc" id="certificationNumber" name="certificationNumber" onkeypress="if(event.keyCode=='13') certification();">
						</div>
		
		
					<div style="margin-top: 5%; text-align: center;">
						<div style="border-radius: 50%; height: 30%; width: 25%; background-color: #CEF6F5; margin-left: 40%;">
							<a style="cursor: pointer; width: 100%; border-radius: 80%; background: #CEF6F5;" onclick="javascript:certification();" class="">조회</a>
					
						</div>
					</div>
							<div class="password d-table" style="margin-top: 10%; ">
										<!-- <img style="width:6%; height:6%;" src="/img/icons8-home-64.png" /> -->
									<a style="cursor: pointer; width: 100%;  /* background: #CAF4FB; */ "
										onclick="javascript:homeLoader.show(); goRegPage();" class="">
										<span class="d-tbc" style="font-weight: bold; color: #585858;">한국카캐리어(주) 회원가입</span></a>
								</div>
							
					
							<div class="password d-table" style="margin-top: 10%; ">
										<img style="width:6%; height:6%;" src="/img/icons8-home-64.png" />
									<a style="cursor: pointer; width: 100%;  /* background: #CAF4FB; */ "
										onclick="javascript:homeLoader.show(); goMainPage();" class="">
										<span class="d-tbc" style="font-weight: bold; color: #585858;">메인페이지로</span></a>
								</div>
							
					
					</c:if>
					
	
						<c:if test ="${paramMap.optionType eq 'C' }">
						
						
							<div class="" style="margin-top: -15%;">
						<!-- 	<span class="d-tbc" style="font-weight: bold; font-size:1.3em;  color: #585858;">차량번호로 조회</span> -->
							</div>
								<div class="password d-table" style="margin-top: 8%;">
									<span class="d-tbc" style="font-weight: bold;">차대번호 또는 차량번호 
									
										</span> <input type="text" id='carIdNum' style="width: 80%; padding:2%;
										 text-align:center;" placeholder=" 차대번호/차량번호로 조회가능합니다." class="d-tbc" >
							
								</div>
				
				
							<div style="margin-top: 5%; text-align: center;">
								<div style="border-radius: 50%; height: 30%; width: 25%; background-color: #CEF6F5; margin-left: 40%;">
									<a style="cursor: pointer; width: 100%; border-radius: 80%; background: #CEF6F5;" onclick="javascript:goSearchCarNum('${paramMap.customerId}');" class="">조회</a>
							
								</div>
							</div>
							
							
								<div class="password d-table" style="margin-top: 10%; " onclick="javascript:homeLoader.show(); document.location.href='/carrier/main.do'">
										<!-- <img style="width:6%; height:6%;" src="/img/icons8-home-64.png" /> -->
									<a >
									<img style="zoom: 0.5;" src="/img/back-icon.png" alt=""></a> 
										<span class="d-tbc" style="font-weight: bold; color: #585858;">&nbsp;&nbsp;뒤로가기</span>
								</div>
							
							
						</c:if>
						
						
							
							

			
			
			
			
		<!-- 	<div class="password d-table" style="margin-top: 3%;" id ='parah'>
	 			<img style=" width:8%; height:6%;" src="/img/carNum.png" /> 
					<a style="cursor: pointer; width: 100%;  background: /* #CAF4FB; */" onclick="javascript:goCarNumSearch();" class="">
						<span class="d-tbc" style="font-weight: bold; color: #00f;" >차대번호/차량번호로 간편조회 </span></a>
						
				</div> -->
				
			<!-- 	<div class="password d-table" style="margin-top: 5%;">
				<img style=" width:8%; height:6%;" src="/img/finger.png" />
					<a style="cursor: pointer; width: 100%;  background:/*  #CAF4FB; */"
						onclick="javascript:goLoginPage();" class="">
						<span class="d-tbc" style="font-weight: bold; color: #00f;">한국카캐리어(주) 아이디가 있으신가요?</span></a>
				</div> -->
				
				
				
		<!-- 		<div class="password d-table" style="margin-top: 5%;">
				<img style=" width:8%; height:6%;" src="/img/finger.png" />
					<a style="cursor: pointer; width: 100%;  background:/*  #CAF4FB; */"
						onclick="javascript:goSearchCarrier();" class="">
						<span class="d-tbc" style="font-weight: bold; color: #00f;">탁송금액조회</span></a>
				</div> -->
			
			
		</div>

	</div>




	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script>
		window.jQuery
				|| document
						.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')
	</script>
	<script src="/js/vendor/bootstrap.min.js"></script>
	<!-- veiwport for countnumber -->
	<script src="/js/jquery.viewportchecker.js"></script>
	<script src="/js/vendor/jquery.loading-indicator.min.js"></script>
	<script src="/js/alert.js"></script>
	<script src="/js/main.js"></script>
	<script src="/js/slick.min.js"></script>
	<script src="/js/jquery.bxslider.js"></script>
	<script src="/js/animsition.min.js"></script>
	<script src="/js/vendor/jquery.loading-indicator.min.js"></script>
	<script>
	
	var homeLoader;
	
	$(document).ready(function(){
			
		
				homeLoader = $('body').loadingIndicator({
					useImage: false,
					showOnInit : false
				}).data("loadingIndicator");
	
		
	});
    

	</script>
</body>
</html>
