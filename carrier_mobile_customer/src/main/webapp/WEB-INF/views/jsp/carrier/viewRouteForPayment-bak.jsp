<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko"  style=" height:100%;">      
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
<body style=" height:100%;">


<script type="text/javascript">  
var homeLoader;
$(document).ready(function(){
	
	//getNewsData("${searchWord}");
	var updateToken = setInterval( function() {
		clearInterval(updateToken);
		pageMoveStop = false;
		homeLoader.hide();

}, 300);
	
	
});




 function getNewsData(obj,keyword){
	
	 
	// alert($("#selectedStatus").children().length);
	 
	//alert($("#selectedStatus").children().first().get(0));	
	//alert($(obj).parent().get(0));
	
 	 if($("#selectedStatus").children().first().get(0) == $(obj).parent().get(0)){				//전체를 클릭 한경우
					
 		$("#selectedStatus").children().each(function(index,element){
 			$(this).removeClass('selected');
 		});
 		$(obj).parent().addClass('selected');
	}else{
			
	} 
	
	
	
/* 	
	
	$.ajax({ 
		type: 'post' ,
		url : "/research/newsSearch.do?&keyword="+keyword ,
		dataType : 'json' ,
		data : {
			
		},
		success : function(data, textStatus, jqXHR)
		{
			alert(data);
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
	 */
	
	
} 

 function goNewsPage(uri){
		
		//var replaceUrlStr = replaceAll(url, "?", "^");
		//replaceUrlStr = replaceAll(replaceUrlStr, "=", "+");
		
		var replaceUrlStr = encodeURIComponent(uri)
		document.location.href = "/news/newsPage.do?newsUri="+replaceUrlStr;
	}  


    
function viewAllocationData(allocationId,allocationStatus){
	document.location.href = "/carrier/allocation-detail.do?allocationStatus="+allocationStatus+"&allocationId="+allocationId;
} 
 
function logout(){
	
	$.confirm("로그아웃 하시겠습니까?",function(a){
		 if(a){
			 if(window.Android != null){
					window.Android.stopService("${driverId}");
				}	
			 document.location.href = "/logout.do";	
		 }
	});
	
}   
 
 
function backKeyController(str){

	homeLoader.show();
	document.location.href="/carrier/kakaoRouteView.do?call=${call}";


	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
	
} 


function goKakaoTalk(){
	
	//$.alert("준비중입니다.");

 	$.confirm("카카오톡 채널로 이동 하시겠습니까?",function(a){
		 if(a){
			if(window.Android != null){
				window.Android.openKakaoChannel();
			}else if(webkit.messageHandlers != null){
				webkit.messageHandlers.scriptHandler.postMessage("goKakaoChannel");

			}else{
				$.alert("알수 없는 에러입니다. 관리자에게 문의해주세요");					
			}
		 }
	});
	
}



 
</script>


        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div class="content-container" style=" height:100%; /* overflow-y:hidden; */">
            <%-- <jsp:include page="/WEB-INF/views/jsp/common-top.jsp"></jsp:include> --%>
            <header class="bg-pink clearfix" style="position:fixed; background-color:#fff; border:none; text-align:center; z-index:1000; ">
                <div class="" style="width:19%;">
                    <a href="/carrier/kakaoRouteView.do?call=${call}"><img style="zoom: 0.5;" src="/img/back-icon.png" alt=""></a> 
                </div> 
               <div class="" style="width:60%;" onclick ="javascript:homeLoader.show();document.location.href='/carrier/three-option.do'">
                	 <img style="width:90%; height:60%;" src="/img/main_logo.png" alt="">
                </div>
                <div class="" style="width:19%; float:right;">
                   <!-- <a style="cursor:pointer;" href="javascript:logout();"><img style="" src="/img/logout.png" alt=""></a> -->
                </div>
            </header>
            <%-- <div style="text-align:right;">${user.driver_name}님 환영 합니다.</div> --%>
            <div style=" text-align:right; clear:both;">&nbsp;</div>
            <div class="content-container interest-page loadingthemepage news" style="position:fixed; background-color:#fff; clear:both;">
	            <!-- <div style="width:100%; margin-top:5px; height:40px; border-bottom:1px solid #eee;">
                   <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">출발일</div></div>
	               <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">상차지</div></div>
	               <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">하차지</div></div>
	               <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">차종</div></div>
	               <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">차대번호</div></div>
                </div> -->
            </div>
            <div id="container" class="search-result news-list content-box" style="height:100%; margin-top:8%;  overflow-y:scroll; overflow-x:hidden;">
            	<div class="xpull">
				    <div class="xpull__start-msg">
				        <div class="xpull__start-msg-text">화면을 당겼다 놓으면 새로 고침 됩니다.</div>
				        <div class="xpull__arrow"></div>
				    </div>
				    <div class="xpull__spinner">
				        <div class="xpull__spinner-circle"></div>
				    </div>
				</div>
            	<div class="news-container clearfix" style="height:100%;">
    	       		<div id="map" style="width:100%; height:46%;">
					</div>
    	       		<div id="roadview" style="width:100%; height:47%;">
    	       			<!-- <div style="text-align:left; clear:both; color: #AC58FA">&nbsp;검색하신 조건에 맞는 탁송 상세 정보 입니다.&nbsp;</div> -->
						<div id="" style="margin-top:10%; width:100%;text-align:center;">
							<div style="display:inline; width:100%;">탁송 예상 가격 :&nbsp;</div>
								<input id ="carrierPayment" type="text" style="text-align:center; border: 2px solid #FF8000;" value="${price}원" readonly>
							</div>
						<div id="" style="margin-top:5%; width:100%;text-align:center;">
							<div style="display:inline; width:100%;">탁송 예상 거리 :&nbsp;</div>
								<input id ="distence" type="text" style="text-align:center; border: 2px solid #FF8000;"  value="${distance}km"  readonly>
						</div>
						<br>
						<div id="" style="margin-top:5%; width:100%;margin-left:8%;">
							<div style="text-align:left; clear:both; color: #585858;">&nbsp;<strong>위의 예상 가격은 <span style="color:#DF0101;">셀프 </span>가격 입니다.</strong></div><br>
							<div style="text-align:left; clear:both; color: #585858;">&nbsp;<strong>또한,<span style="color :#FE2E2E;"> 탁송 예상 금액</span>이므로</strong></div>
							<div style="text-align:left; clear:both; color: #585858;">&nbsp;거리별 오차가 있을 수 있으니, 캐리어/셀프 관련 단가 </div>
							<div style="text-align:left; clear:both; color: #585858;">&nbsp;정확한 <span style="color:#FE2E2E;"><strong>상담</strong></span>을 원하시면 아래 버튼을 눌러주세요.</div>
						</div>
						
							<div class="password d-table" style="margin-top: 15%; text-align:center; ">
										<img style="width:8%; height:8%;" src="/img/kakaoLOGO.png" />
									<a style="cursor: pointer; width: 100%;" onclick="goKakaoTalk()" class="">
										<span class="d-tbc" style="font-weight: bold; color: #61210B;">카카오톡 상담하기</span></a>
								</div>
					</div>
    	       		
    	       		
                 </div>
            </div>
        </div>


	<!-- <script type="text/javascript" src="http://dapi.kakao.com/v2/maps/sdk.js?appkey=196301395a31adf3319a7ee5f66f17da"></script> -->
    <script type="text/javascript" src="http://dapi.kakao.com/v2/maps/sdk.js?appkey=196301395a31adf3319a7ee5f66f17da&libraries=services"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      
    <script src="/js/vendor/bootstrap.min.js"></script>       
    <script src="/js/vendor/jquery.loading-indicator.min.js"></script>
    <!-- veiwport for countnumber -->
    <script src="/js/alert.js"></script>
    <script src="/js/main.js"></script>   
    <script src="/js/vendor/xpull.js"></script>       
    
    <script>
    
	
/* 	$('#container').xpull({
	    'paused': false, 
	    // Pull threshold - amount in  pixels required to pull to enable release callback
        'pullThreshold':50,
        // Max pull down element - amount in pixels
        maxPullThreshold: 50,
        // timeout in miliseconds after which the loading indicator stops spinning.
        // If set to 0 - the loading will be indefinite
        'spinnerTimeout':1000,
        onPullStart: function(){},
        onPullEnd: function(){},
        callback: function(){window.location.reload()},

	});    */
    
	var container = document.getElementById('map'); //지도를 담을 영역의 DOM 레퍼런스
	var options = { //지도를 생성할 때 필요한 기본 옵션
		center: new daum.maps.LatLng('${map.center_x}','${map.center_y}'), //지도의 중심좌표.
		level: 9 //지도의 레벨(확대, 축소 정도)
	};

	var map = new daum.maps.Map(container, options);
	//map.addOverlayMapTypeId(daum.maps.MapTypeId.ROADVIEW);
	// 지도 레벨은 지도의 확대 수준을 의미합니다
	// 지도 레벨은 1부터 14레벨이 있으며 숫자가 작을수록 지도 확대 수준이 높습니다
	function zoomIn() {        
	    // 현재 지도의 레벨을 얻어옵니다
	    var level = map.getLevel();
	    
	    // 지도를 1레벨 내립니다 (지도가 확대됩니다)
	    map.setLevel(level - 1);
	    
	}    

	function zoomOut() {    
	    // 현재 지도의 레벨을 얻어옵니다
	    var level = map.getLevel(); 
	
	    map.setLevel(level + 1);
	 
	} 
	
	
	daum.maps.event.addListener(map, 'bounds_changed', function() {             
	    
	    // 지도 영역정보를 얻어옵니다 
	    var bounds = map.getBounds();
	    
	    // 영역정보의 남서쪽 정보를 얻어옵니다 
	    var swLatlng = bounds.getSouthWest();
	    
	    // 영역정보의 북동쪽 정보를 얻어옵니다 
	    var neLatlng = bounds.getNorthEast();
	    
	    var message = '<p>영역좌표는 남서쪽 위도, 경도는  ' + swLatlng.toString() + '이고 <br>'; 
	    message += '북동쪽 위도, 경도는  ' + neLatlng.toString() + '입니다 </p>'; 
	    
	});

	var points = [
	    new daum.maps.LatLng('${map.departure_x}','${map.departure_y}'),
	    new daum.maps.LatLng('${map.arrival_x}','${map.arrival_y}'),
	    //new daum.maps.LatLng('37.4540237','127.0008785')				//폰에서 가져온 좌표
	];
	
	var bounds = new daum.maps.LatLngBounds();    

	var i, marker;
	for (i = 0; i < points.length; i++) {
	    // 배열의 좌표들이 잘 보이게 마커를 지도에 추가합니다
	    marker =     new daum.maps.Marker({ position : points[i] });
	    marker.setMap(map);
	    
	    // LatLngBounds 객체에 좌표를 추가합니다
	    bounds.extend(points[i]);
	}
	
//	var iwContent = '<div style="padding:5px;">Hello World! <br><a href="http://map.daum.net/link/map/Hello World!,33.450701,126.570667" style="color:blue" target="_blank">큰지도보기</a> <a href="http://map.daum.net/link/to/Hello World!,33.450701,126.570667" style="color:blue" target="_blank">길찾기</a></div>', // 인포윈도우에 표출될 내용으로 HTML 문자열이나 document element가 가능합니다
//    iwPosition = new daum.maps.LatLng(37.4540237, 127.0008785); //인포윈도우 표시 위치입니다

/* 
// 인포윈도우를 생성합니다
var infowindow = new daum.maps.InfoWindow({
    position : iwPosition, 
    content : iwContent 
});

infowindow.open(map, marker); 
 */	
	
	
	
	$(document).ready(function(){

    	//$("#departure").focus();
    	homeLoader = $('body').loadingIndicator({
			useImage: false,
			showOnInit : false
		}).data("loadingIndicator");
	
    	homeLoader.show();
		
		
		var x = "";
		var y = "";
		
		var linePath = new Array(${fn:length(companyMap.fileList)});
		 <c:forEach var="item" items="${posList}" varStatus="status">
			linePath.push(new daum.maps.LatLng('${item.x}','${item.y}'));
		 	<c:if test='${status.index == fn:length(posList)-1}'>
		 		x = '${item.x}';
		 		y = '${item.y}';
			</c:if>
		</c:forEach> 
		// 지도에 표시할 선을 생성합니다
		var polyline = new daum.maps.Polyline({
		    path: linePath, // 선을 구성하는 좌표배열 입니다
		    strokeWeight: 3, // 선의 두께 입니다
		    strokeColor: '#FF0000', // 선의 색깔입니다
		    //strokeColor: '#8B00FF', // 선의 색깔입니다
		    strokeOpacity: 1, // 선의 불투명도 입니다 1에서 0 사이의 값이며 0에 가까울수록 투명합니다
		    strokeStyle: 'solid' // 선의 스타일입니다
		});
		
		 map.setBounds(bounds);
		 polyline.setMap(map);
		 
 		 /* var roadviewContainer = document.getElementById('roadview'); //로드뷰를 표시할 div */
		 /* var roadview = new daum.maps.Roadview(roadviewContainer); //로드뷰 객체 */
/* 		 var roadviewClient = new daum.maps.RoadviewClient(); //좌표로부터 로드뷰 파노ID를 가져올 로드뷰 helper객체 */

		 var position = new daum.maps.LatLng(x, y);

		  // 특정 위치의 좌표와 가까운 로드뷰의 panoId를 추출하여 로드뷰를 띄운다.
/* 		 roadviewClient.getNearestPanoId(position, 80, function(panoId) {
		     roadview.setPanoId(panoId, position); //panoId와 중심좌표를 통해 로드뷰 실행
		 });
		   */
		 
	});
	
	
	
	
    </script>
    </body>
</html>
