<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko" style=" height:100%;">      
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
    
<body style="background-color:#fff; height:100%;" >

<script type="text/javascript">  
  var pageMoveStop = true;
$(document).ready(function(){  
	
	var updateToken = setInterval( function() {
	
		//deviceToken = getDeviceToken();
		if(getDeviceToken() != null && getDeviceToken() != ""){
			updateDriverDeviceToken(getDeviceToken());
			if(window.Android != null){
				window.Android.startService();
				pageMoveStop = false;
			}
			clearInterval(updateToken);
		}
		if(window.Android == null){
			pageMoveStop = false;
		}
   }, 1000);
	
	
	/* var updateLocation = setInterval( function() {
	
		if(getDeviceLocation() != null && getDeviceLocation() != ""){
			updateDriverDeviceLocation(getDeviceLocation());
		}
		
    }, 10*1000); */			//1분에 한번
	
  	//alert(deviceToken);
});  
  
  
function updateDriverDeviceToken(token){
	
	
	if(token != ""){
		$.ajax({ 
			type: 'post' ,
			url : "/updateDriverDeviceToken.do" ,
			dataType : 'json' ,
			data : {
				token : token
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					//$.alert("성공",function(a){
					//});
				}else if(result == "E000"){
					
				}else{
					
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
}  
  
  
  
function getDeviceToken(){
	
	var token = "";
	try{
		if(window.Android != null){
			token = window.Android.getDeviceToken("${driverId}");
		}	
	}catch(exception){
		
	}finally{
		
	}
	return token;
}  
  
  
function updateDriverDeviceLocation(location){
	
	if(location != ""){
		$.ajax({ 
			type: 'post' ,
			url : "/updateDriverDeviceLocation.do" ,
			dataType : 'json' ,
			data : {
				location : location
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					//$.alert("성공",function(a){
					//});
				}else if(result == "E000"){
					
				}else{
					
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
	
}  
  
 function getDeviceLocation(){
	 
	 var latlng = "";
		try{
			if(window.Android != null){
				latlng = window.Android.getDeviceLocation("${kakaoId}");
				//alert(latlng);
			}	
		}catch(exception){
			
		}finally{
			
		}
		return latlng; 
	 
 }
  
  
function getAndroidData(str){
	
//	alert(str);
	
	//updateDriverDeviceLocation(str);
	
} 
 
 
function backKeyController(str){
	
	
		history.go(-1);
	
	
}
 
 
function goNList(allocationStatus){
	
	//alert("승인 대기 목록");
	if(!pageMoveStop){
		document.location.href = "/carrier/list.do?allocationStatus="+allocationStatus;	
	}
	
	
}

function goYList(allocationStatus){
	
	
	//window.Android.toastLong("test");
	//alert("승인 완료 목록");
	if(!pageMoveStop){
		document.location.href = "/carrier/list.do?allocationStatus="+allocationStatus;	
	}
	
}

function goFList(allocationStatus){
	
	//alert("탁송 완료 목록");
	if(!pageMoveStop){
		document.location.href = "/carrier/finish-list.do?allocationStatus="+allocationStatus;	
	}
	
}

function goReload(){
	
	document.location.reload();
	/* $.alert("준비중 입니다.",function(a){
	}); */
	
}

function goCalPage(){
	
	
	if("${user.driver_kind}" != "00"){
		if(!pageMoveStop){
			document.location.href = "/carrier/cal-list.do";	
		}	
	}else{
		$.alert("준비중 입니다.",function(a){
		});
	}
	
	
}

    
function logout(){
	
	$.confirm("로그아웃 하시겠습니까?",function(a){
		 if(a){
			 if(window.Android != null){
					window.Android.stopService("${driverId}");
				}	
			 document.location.href = "/logout.do";	
		 }
	});
	
}    
    
    
    
function downloadDriverDeduct(){
	//alert(1);
	if(!pageMoveStop){
		document.location.href = "/carrier/downloadDriverDeductInfo.do?driverId=${user.driver_id}";
	}
}    
    
    
</script>


        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div class="content-container" style="height:100%; overflow-y:hidden;" >
            <!-- <header class="bg-pink clearfix" style="background-color:#fff; border:none; text-align:center; "> -->
            <header class="bg-pink clearfix" style="position:fixed; top:0px; background-color:#fff; border:none; text-align:center; ">
                <div class="" style="width:19%;">
                    <a href="#"></a> 
                </div> 
                <div class="" style="width:60%;">
                	<!-- <img style="width:90%; height:60%;" src="/img/main_logo.png" alt=""> -->
                	<img style="width:90%; height:60%;" onclick="javascript:document.location.href='/carrier/main.do'" src="/img/main_logo.png" alt="">
                </div>
                <div class="" style="width:19%; float:right;">
                   <a style="cursor:pointer;" href="javascript:logout();"><img style="" src="/img/logout.png" alt=""></a>
                </div>
            </header>
            <div style="clear:both;"></div>
            <!-- <div class="menu-container bg-pink">
                <span class="img-holder">
                    <img src="/img/user-img.png" alt="">
                </span>
                 <a class="kakao txt-bold" style="cursor:pointer;" onclick="javascript:kakaoLogin();">
                    카카오계정으로 로그인
                </a>
            </div> -->
            <div style=" text-align:right; clear:both;">&nbsp;</div>
            <div class="main-menu-container" style="margin-top:9%; height:100%; overflow-y:hidden;">
            	<div style=" text-align:right; clear:both;">${user.driver_name}님 환영 합니다.</div>
            	<div  id="container"  style="height:100%; margin-top:2%; background-color:#eee;">
            		
				<div style="margin-top:3%; height:100%;">
                	<!-- <iframe src="http://drive.google.com/viewerng/viewer?embedded=true&url=http://52.78.153.148:8081/files/forms/deductlist.xlsx" width="100%" height="100%"></iframe> -->
                	<iframe src="http://docs.google.com/gview?embedded=true&url=http://52.78.153.148:8081/files/forms/deductlist.xlsx" width="100%" height="100%"></iframe>
                
                
                
                </div>
                </div>
            </div>
        </div>

    <script src="/js/vendor/jquery-1.11.2.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      <script src="/js/vendor/bootstrap.min.js"></script>       
    <!-- veiwport for countnumber -->
    <script src="/js/jquery.viewportchecker.js"></script>  
      <script src="/js/alert.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/vendor/xpull.js"></script>       
    
    
    <script>
    
	
	$('#container').xpull({
	    'paused': false, 
	    // Pull threshold - amount in  pixels required to pull to enable release callback
        'pullThreshold':50,
        // Max pull down element - amount in pixels
        maxPullThreshold: 50,
        // timeout in miliseconds after which the loading indicator stops spinning.
        // If set to 0 - the loading will be indefinite
        'spinnerTimeout':1000,
        onPullStart: function(){},
        onPullEnd: function(){},
        callback: function(){window.location.reload()},

	});   
    
    </script>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    </body>
</html>
