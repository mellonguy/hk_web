<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko" style="height:100%;">      
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
<body style="height:100%; text-align: center;">

<script type="text/javascript">  
  
$(document).ready(function(){  

	try {
		if(window.carrierJrpr != null){
			window.carrierJrpr.loginForApp("${kakaoId}", "");
		}
		if(window.external != null){
	        window.external.loginForPC("${kakaoId}", "");
	    }
	}catch(exception){
	    
	}finally {
		
	}
	
	
	
  
});

/*   var pw = $("#userPwd").val();
var num = pw.search(/[0-9]/g);
var eng = pw.search(/[a-z]/ig);
var spe = pw.search(/[`~!@@#$%^&*|₩₩₩'₩";:₩/?]/gi);

if(pw.length < 6 || pw.length > 20){
	$.alert("패스워드는 6자리 이상이어야 합니다.",function(a){
	});
 return false;
}
if(pw.search(/₩s/) != -1){
	$.alert("패스워드는 공백업이 입력해주세요.",function(a){
	});
	  return false;
	 }
if(num < 0 || eng < 0 || spe < 0 ){
	$.alert("영문,숫자, 특수문자를 혼합하여 입력해주세요.",function(a){
	});
  return false;
 }

if($("#userPwdChk").val() == ""){
	$.alert("패스워드확인이 입력되지 않았습니다.",function(a){
	});
	return false;
}

if($("#userPwd").val() != $("#userPwdChk").val()){
	$.alert("패스워드와 패스워드 확인에 입력 한 값이 다릅니다.",function(a){
	});
	return false;
}  */


function logout(){

	if(confirm("로그아웃 하시겠습니까?")){
		
		try {
			if(window.carrierJrpr != null){
				window.carrierJrpr.logoutForApp("");
			}
			if(window.external != null){
	            window.external.logoutForPC("");
	        }
		}catch(exception){
		    
		}finally {
			document.location.href = "/kakao/logout.do";	
		}
	}
}
    

function loginForApp(userId){
	window.carrierJrpr.loginForApp(userId, "");
}



function logoutForApp(){
	window.carrierJrpr.logoutForApp("");
}    
    
    
/*     
function loginProcess(){
	
	if($("#loginId").val() == ""){
		//alert("아이디가 입력 되지 않았습니다.");
		//return;
		$.alert("아이디가 입력 되지 않았습니다.",function(a){
	    });
		return;
	}
	if($("#loginPwd").val() == ""){
		//alert("패스워드가 입력 되지 않았습니다.");
		//return;
		$.alert("패스워드가 입력 되지 않았습니다.",function(a){
	    });
		return;
	}
	
	$.ajax({ 
        type: 'post' ,
        url : '/loginPersist.do' ,
        dataType : 'json' ,
        data : $('#loginForm').serialize(), 
        
        success : function(jsonData, textStatus, jqXHR) {
        	if(jsonData.resultCode =="0000"){
            	goMainPage();
            	/* $("#myphoto_path").css("background-image","url(/files/member"+ data.resultData.photo_path + ")"); 
        	}else if(jsonData.resultCode =="E001"){
        		//alert("로그인에 실패 하였습니다. 관리자에게 문의 하세요.");
        		$.alert("로그인에 실패 하였습니다. 관리자에게 문의 하세요.",function(a){
        			
        	    });
        	}
        } ,
        error : function(xhRequest, ErrorText, thrownError) {
          //  alertEx("시스템 에러");
        }
    }); 
} */


function goMainPage(){
	//document.location.href = "/carrier/main.do";
	document.location.href = "/carrier/three-option.do";
}
    

var selectedObj = new Object();

	var idCheck ="N";

	function idDubleCheck(){


			var engNum = /^[a-z]+[a-z0-9]{5,15}$/g;
			
			var userId =$("#userId").val().replace(/ /g,"");


			
			if(userId == ''){
		
			$.alert("입력이 되지 않았습니다.");


			}else if(userId.length < 5 || userId.length > 15){
				 $.alert("아이디는 5자리 이상이어야 합니다.");

				  return false;
			

			}else if(!engNum.test(userId)){
				  $.alert('형식에 맞지 않는 아이디입니다. 다시 입력해 주세요.');
				  return false;
				  
			 }else{
			
	
			idCheck ="N";
			
			$.ajax({ 
				type: 'post' ,
				url : "/carrier/idDoubleCheck.do" ,
				dataType : 'json' ,
				data : {
					userId : userId
			
				},
				success : function(data, textStatus, jqXHR)
				{
					var result = data.resultCode;
					var resultData = data.resultData;
					var resultMsg = data.resultMsg;
					
					if(result == "0000"){
						$.alert("사용이 가능한 아이디입니다.",function(a){
							if(a){
								idCheck ="Y";
							}
						});
					}else if(result == "0001"){
						$.alert("중복된 아이디가 있습니다. 다시 입력해주세요",function(a){
						});
					}else{
						
					}
				} ,
				error : function(xhRequest, ErrorText, thrownError) {
					$.alert(ErrorText);
				}
				
			});
			
			}


	}


	
function simpleSignUp(){

	selectedObj = new Object();

	
	if(idCheck =="N"){

		 $.alert("중복 확인이 진행 되지 않았습니다.",function(a){
			});
			// 
		
		return false;
	}
	

	if($("#userId").val() == ""){
		$.alert("아이디가 입력되지 않았습니다.",function(a){
		});
		return false;
	}	
	 if($("#userPwd").val() == ""){
		 $.alert("패스워드가 입력되지 않았습니다.",function(a){
		});
		return false;
    }

	    
	 var pw = $("#userPwd").val();
	 var num = pw.search(/[0-9]/g);
	 var eng = pw.search(/[a-z]/ig);
	 var spe = pw.search(/[`~!@@#$%^&*|₩₩₩'₩";:₩/?]/gi);

	 if(pw.length < 6 || pw.length > 20){
	 	$.alert("패스워드는 6자리 이상이어야 합니다.",function(a){
	 	});
	  return false;
	 }
	 if(pw.search(/₩s/) != -1){
	 	$.alert("패스워드는 공백업이 입력해주세요.",function(a){
	 	});
	 	  return false;
	 	 }
	 if(num < 0 || eng < 0 || spe < 0 ){
	 	$.alert("영문,숫자, 특수문자를 혼합하여 입력해주세요.",function(a){
	 	});
	   return false;
	  }

	 if($("#userPwdChk").val() == ""){
	 	$.alert("패스워드확인이 입력되지 않았습니다.",function(a){
	 	});
	 	return false;
	 }

	 if($("#userPwd").val() != $("#userPwdChk").val()){
	 	$.alert("패스워드와 패스워드 확인에 입력 한 값이 다릅니다.",function(a){
	 	});
	 	return false;
	 } 

	
	$.ajax({ 
		type: 'post' ,
		url : "/carrier/insertSimpleSign-Up.do" ,
		dataType : 'json' ,
		data : {
			userId : $("#userId").val(),
			userPwd : $("#userPwd").val(),
			customerId : "${customerId}",
			chargeId : $("#chargeId").val(),
			phoneNum : $("#phoneNum").val()
			
			
		},
		success : function(data, textStatus, jqXHR)
		{
			var result = data.resultCode;
			var resultData = data.resultData;
			var resultMsg = data.resultMsg;
			
			if(result == "0000"){
				$.alert("회원가입이 완료되었습니다.",function(a){
					if(a){
						document.location.href = "/carrier/sign-Up.do";
					}
				});
			}else if(result == "0001"){
				$.alert("중복확인이 되지 않았습니다.",function(a){
				});
			}else{
				
			}
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
			$.alert(ErrorText);
		}
		
	});
	
	
}



    


function backKeyController(str){
	history.go(-1);	
} 
 








</script>



		<div class="content-container">
            <!-- <a href="/carrier/login-page.do"><img style="zoom: 0.5;" src="/img/back-icon.png" alt=""></a> -->
            <a href="/carrier/agree.do?customerId=${paramMap.customerId}&chargeId=${paramMap.chargeId}&phoneNum=${paramMap.phoneNum}&optionType=${paramMap.optionType}"><img style="zoom: 0.5;" src="/img/back-icon.png" alt=""></a> 
            <!-- <header class="bg-pink clearfix"> -->
                <!-- <div class="search-icon">
                    <a href="#"></a> 
                </div> -->
                <!-- <div class="" style="width:70%; text-align:center;">
                	<img style="width:100%; height:75%;" src="/img/main_logo.png" alt="">
                </div> -->
                <!-- <div class="menu-bar pull-right">
                    <a style="cursor:pointer;" href="/adviser/adviser.do"><img src="/img/close-white-icon.png" alt=""></a>
                </div> -->
            <!-- </header> -->
            
            <div style=" display:table; margin:0 auto; display:inline-block; width:60%; height:100%;">
                	<img style="width:100%; height:75%; margin-top:70%;" src="/img/main_logo.png" alt="">
                
            <form action="" id="loginForm" style="margin-top:40%;">
                        <div class="username d-table">
                            <span class="d-tbc" style="font-weight:bold;">새로만들 아이디</span>
                            <div style="margin-top:5%;">
                                <input style="width: 100%; padding:2%;" type="text" placeholder="아이디를 입력해 주세요" class="d-tbc" id="userId" name=userId value="">
                           </div>
                           <br>
                               	<a style="cursor:pointer;" onclick="javascript:idDubleCheck();" class="login-btn d-tbc">중복확인</a>&nbsp;
                            </div>
                            
               
                        <div class="password d-table" style="margin-top:10%;">
                            <span class="d-tbc" style="font-weight:bold;">신규 패스워드</span>
                            <div style="margin-top:5%;">
                          	     <input style="width: 100%; padding:2%;" type="password" placeholder="6자리이상 영문,숫자,특수문자 혼합" class="d-tbc" id="userPwd" name="userPwd" >
                            </div>
                        </div>
                        <div class="password d-table" style="margin-top:10%;">
                            <span class="d-tbc" style="font-weight:bold;">신규 패스워드 확인</span>
                            <div style="margin-top:5%;">
                          	     <input style="width: 100%;padding:2%;" type="password" placeholder="6자리이상 영문,숫자,특수문자 혼합" class="d-tbc" id="userPwdChk" name="userPwd" >
                          	     <input style="width: 100%;" type="hidden" placeholder="" class="d-tbc" id="customerId" name="customerId" value ="${paramMap.customerId}">
                          	     <input style="width: 100%;" type="hidden" placeholder="" class="d-tbc" id="chargeId" name="chargeId" value ="${paramMap.chargeId}">
                          	     <input style="width: 100%;" type="hidden" placeholder="" class="d-tbc" id="phoneNum" name="phoneNum" value ="${paramMap.phoneNum}">
                            
                            </div>
                        </div>
                       </form>
                       <div class="login-btn-container d-table">
                           <span class="d-tbc"></span>
                           <div style="margin-top:10%; text-align:center;">
                           		<!-- <div style="width:30%; border:3px solid #eee;"> -->
                               	<a style="cursor:pointer;" onclick="javascript:simpleSignUp();" class="login-btn d-tbc">간편 회원가입</a>&nbsp;/
                               	<a style="cursor:pointer;" href="/carrier/login-page.do" class="login-btn d-tbc">취소</a>
                               	<!-- </div> -->
                           </div>
                       </div>
                       
                       
                       <div class="password d-table" style="margin-top: 10%; ">
										<img style="width:6%; height:6%;" src="/img/icons8-home-64.png" />
									<a style="cursor: pointer; width: 100%;  /* background: #CAF4FB; */ "
										onclick="javascript:goMainPage();" class="">
										<span class="d-tbc" style="font-weight: bold; color: #585858;">메인페이지로</span></a>
								</div>
                       
                       
                       
                   </div>    
                       
       	</div>




    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      
    <script src="/js/vendor/bootstrap.min.js"></script>       
    <!-- veiwport for countnumber -->
    <script src="/js/jquery.viewportchecker.js"></script>    
    <script src="/js/alert.js"></script>
    <script src="/js/main.js"></script> 
</body>
</html>
