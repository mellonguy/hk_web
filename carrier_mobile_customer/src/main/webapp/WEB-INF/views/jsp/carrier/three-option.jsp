<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%
	
%>

<!DOCTYPE html>
<html lang="ko" style="height: 100%;">

<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="/css/jquery.bxslider.css"/>
<link rel="stylesheet" href="/css/animsition.min.css">  
<meta name="viewport" content="width=device-width, user-scalable=no">
<body style="height: 100%; text-align: center;">
<script type="text/javascript">


<%

if(session.getAttribute("user") != null){

	session.invalidate();
}

%>


		$(document).ready(function() {

			 
			var userInputId = getCookie("userInputId");
			$("#loginId").val(userInputId);

			try {
				if (window.carrierJrpr != null) {
					window.carrierJrpr.loginForApp("${kakaoId}", "");
				}
				if (window.external != null) {
					window.external.loginForPC("${kakaoId}", "");
				}
			} catch (exception) {

			} finally {

			}

			/* 	$('.bxslider').bxSlider({ 
			
			 auto: true,
			 speed: 500, 
			 pause: 4000, 
			 mode:'fade',
			 autoControls: true, 
			 pager:true, 

			 });
			 */

			 
			//session.invalidate();

			$('.slick-items').bxSlider({

			  	auto: true,
			  	speed: 1000,			    
			    slideWidth: 900
		
				

			});


				  $(".animsition").animsition({
					    inClass: 'fade-in-left',
					    outClass: 'fade-out-left',
					    inDuration: 1500,
					    outDuration: 800,
					    linkElement: '.animsition-link',
					    // e.g. linkElement: 'a:not([target="_blank"]):not([href^="#"])'
					    loading: true,
					    loadingParentElement: 'body', //animsition wrapper element
					    loadingClass: 'animsition-loading',
					    loadingInner: '', // e.g '<img src="loading.svg" />'
					    timeout: false,
					    timeoutCountdown: 5000,
					    onLoadEvent: true,
					    browser: [ 'animation-duration', '-webkit-animation-duration'],
					    // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
					    // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
					    overlay : false,
					    overlayClass : 'animsition-overlay-slide',
					    overlayParentElement : 'body',
					    transition: function(url){ window.location.href = url; }
					  });


			 
			
		});

		function getCookie(cookieName) {
			cookieName = cookieName + '=';
			var cookieData = document.cookie;
			var start = cookieData.indexOf(cookieName);
			var cookieValue = '';
			if (start != -1) {
				start += cookieName.length;
				var end = cookieData.indexOf(';', start);
				if (end == -1)
					end = cookieData.length;
				cookieValue = cookieData.substring(start, end);
			}
			return unescape(cookieValue);
		}

		function setCookie(cookieName, value, exdays) {
			var exdate = new Date();
			exdate.setDate(exdate.getDate() + exdays);
			var cookieValue = escape(value)
					+ ((exdays == null) ? "" : "; expires="
							+ exdate.toGMTString());
			document.cookie = cookieName + "=" + cookieValue;
		}

		function logout() {

			if (confirm("로그아웃 하시겠습니까?")) {

				try {
					if (window.carrierJrpr != null) {
						window.carrierJrpr.logoutForApp("");
					}
					if (window.external != null) {
						window.external.logoutForPC("");
					}
				} catch (exception) {

				} finally {
					document.location.href = "/kakao/logout.do";
				}
			}
		}

		function loginForApp(userId) {
			window.carrierJrpr.loginForApp(userId, "");
		}

		function logoutForApp() {
			window.carrierJrpr.logoutForApp("");
		}

		function loginProcess() {

			
			if ($("#loginPwd").val() == "") {
				//alert("패스워드가 입력 되지 않았습니다.");
				//return;
				$.alert("입력이 되지 않았습니다.", function(a) {
				});
				return;
			}

			setCookie("userInputId", $("#loginId").val(), 365);

			$.ajax({
				type : 'post',
				url : '/loginPersist.do',
				dataType : 'json',
				data : $('#loginForm').serialize(),

				success : function(jsonData, textStatus, jqXHR) {
					if (jsonData.resultCode == "0000") {

						goMainPage();

					} else if (jsonData.resultCode == "E001") {
						//alert("로그인에 실패 하였습니다. 관리자에게 문의 하세요.");
						$.alert("로그인에 실패 하였습니다. 관리자에게 문의 하세요.", function(a) {

						});
					} else if (jsonData.resultCode == "E002") {
						//alert("로그인에 실패 하였습니다. 관리자에게 문의 하세요.");
						$.alert("휴직 또는 퇴사처리 된 아이디 입니다. 관리자에게 문의 하세요.",
								function(a) {

								});
					}
				},
				error : function(xhRequest, ErrorText, thrownError) {
					//  alertEx("시스템 에러");
				}
			});
		}

		function goMainPage() {
			document.location.href = "/carrier/main.do";
		}
		function goCustomerListPage(certificationNumber) {
			document.location.href = "/carrier/customerList.do?&certificationNumber="+$("#certificationNumber").val();
		}

		function changePassWord() {
			document.location.href = "/carrier/changePassWord.do"
		}

		function backKeyController(str) {
			if (window.Android != null) {
				window.Android.toastShort("뒤로가기 버튼으로 앱을 종료 할 수 없습니다.");
			}
		}
		function goLoginPage(){

			if("${paramMap.status}" == "logout"){


				document.location.href ="/carrier/sign-Up.do?&status=logout";
				
			}else{


			document.location.href ="/carrier/sign-Up.do";

				}
		
			}

		var arrInput = new Array(0);
		  var arrInputValue = new Array(0);

		
		function goCarNumSearch(){

			arrInput.push(arrInput.length);
			  arrInputValue.push("");
			  display();

			}

		function display() {
			  document.getElementById('parah').innerHTML="";
			  for (intI=0;intI<arrInput.length;intI++) {
			    document.getElementById('parah').innerHTML+=createInput(arrInput[intI], arrInputValue[intI]);
			  }
			}

		function createInput(id,value) {
			  return "<input  id ='carIdNum' style='width: 80%; padding:2%; text-align:center;'placeholder='차대번호 또는 차량번호로 조회가능합니다'  type='text' id='test "+ id +"' onChange='javascript:saveValue("+ id +",this.value)' value='"+value +"'><br><br>"+"<div style='border-radius: 50%; height: 30%; width: 25%; background-color: #CEF6F5; margin-left: 40%;'><a style='cursor: pointer; width: 100%; border-radius: 80%;  background: #CAF4FB;'onclick='javascript:goSearchCarNum();'><a style='cursor: pointer; width: 100%; border-radius: 80%; background: #CAF4FB;' onclick='javascript:goSearchCarNum();'>조회</span></a>"
			  ;
			}

	function goSearchCarNum(){

			if( $("#carIdNum").val()== ""){
				$.alert("입력이 되지 않았습니다.");

				return false;
				
			}else{
					document.location.href ="/carrier/car-id-num-list.do?&carIdNum="+$('#carIdNum').val();
				
			}

}
 

		
	/* 	function phoneChk(obj){

		     var trans_num = $(obj).val().replace(/-/gi,'');
		     if(trans_num != null && trans_num != ''){
		         if(trans_num.length==11 || trans_num.length==10){   
		        	 				var regExp_ctn = /^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})([0-9]{3,4})([0-9]{4})$/;
		             if(regExp_ctn.test(trans_num)){
		                 trans_num = trans_num.replace(/^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})-?([0-9]{3,4})-?([0-9]{4})$/, "$1-$2-$3");                  
		                 $(obj).val(trans_num);
		                 return true;
		             }else{
		                 return false;
		             }
		         }else{
		             return false;
		         }
		   }
		   
		} */

		




		function goSearchCarrier(){

			document.location.href = "/carrier/kakaoRouteView.do";
			
			}

		function goLoginOrSignUp(optionType){


			document.location.href ="/carrier/login-page.do?&optionType="+optionType;


		}
	
		function goSearchForCarIdNum(optionType){

			document.location.href ="/carrier/login-page.do?&optionType="+optionType;

			}

/* 		function goLoginPage(){

				document.location.href ="/carrier/sign-Up.do";

		} */

		function goSearchCarrierPay(){

			$.alert("탁송금액조회는 로그인 후 이용하실수가 있습니다." );
			
			//document.location.href ="/carrier/kakaoRouteView.do";
			

	}


function goKakaoTalk(){

	//$.alert("준비중입니다.");
		$.confirm("카카오톡 채널로 이동 하시겠습니까?",function(a){
			 if(a){
				if(window.Android != null){
					window.Android.openKakaoChannel();
				}else if(webkit.messageHandlers != null){
					webkit.messageHandlers.scriptHandler.postMessage("goKakaoChannel");

				}else{
					$.alert("알수 없는 에러입니다. 관리자에게 문의해주세요");					
				}	
			 }
		}); 
	
			
}


		

		
	</script>

<style>
				.hover1:hover{ box-shadow:200px 0 0 0 rgba(0,0,0,0.5) inset; } 
				.hover2:hover{ box-shadow:-200px 0 0 0 rgba(0,0,0,0.5) inset; } 
				.hover3:hover{ box-shadow: 0 80px 0 0 rgba(0,0,0,0.25) inset, 0 -80px 0 0 rgba(0,0,0,0.25) inset; } 
				.hover4:hover{ box-shadow: 200px 0 0 0 rgba(0,0,0,0.25) inset, -200px 0 0 0 rgba(0,0,0,0.25) inset; }
				
				.btn { display:block;
						 width:200px;
					/* 	 height:40px;  */
						 line-height:40px; 
					/*  border:2px # solid;  */
						 margin:15px auto; 
						  background-color:#848484; 
						 text-align:center; 
						 cursor: pointer; 
						 transition:all 0.9s, color 0.3; 
						 border-radius:50%;
						 } 
						 
				.btn:hover{color:#fff;}

	
</style>


	<div class="content-container" >
		<!-- <header class="bg-pink clearfix"> -->
		<!-- <div class="search-icon">
                    <a href="#"></a> 
                </div> -->
		<!-- <div class="" style="width:70%; text-align:center;">
                	<img style="width:100%; height:75%;" src="/img/main_logo.png" alt="">
                </div> -->
		<!-- <div class="menu-bar pull-right">
                    <a style="cursor:pointer;" href="/adviser/adviser.do"><img src="/img/close-white-icon.png" alt=""></a>
                </div> -->
		<!-- </header> -->

		<div style="display: table; margin: 0 auto; display: inline-block; width: 90%; height: 100%;" 
		class="animsition"  >

			<img style="width: 100%; height: 75%; margin-top: 20%; margin-bottom: 20%;"
				src="/img/main_logo.png" alt="">

  		<!-- 	<h4> 대한민국 NO.1 탁송업체</h4> -->
<!--   		<div class="slick-items">
		
		   <div><img style=" width:100%; height:100%;" src="/img/car1.jpg" /> <strong  style="color: #585858;" id='titleText'>항상 최선으로 보답하는 한국카캐리어(주)</strong></div>
		   <div><img style=" width:100%; height:100%;" src="/img/car2.jpg" /><strong   style="color: #585858;" id='titleText'>국내외 메이커 차량 이동 전문</strong></div>
		   <div><img style=" width:100%; height:100%;" src="/img/car8.jpg" /><strong   style="color: #585858;"id='titleText'>수도권 전시장 딜리버리 전문</strong></div>
		   <div><img style=" width:100%; height:100%;" src="/img/car3.jpg" /><strong id='titleText2'><span  style="color: #585858;">l'll be the best!</span></strong></div>
  
  		</div> -->

  
			


			<div style ="margin-top:5%;" onclick="javascript:homeLoader.show();goLoginOrSignUp('L');" >  
                
                <button class="btn hover1" style="color:#FFFF00;"><strong>회원가입</strong></button>
             </div>
                <br>
   			
   			
   		<!-- 	<div style ="margin-top:5%;" onclick="javascript:goSearchForCarIdNum('C');" >             
                  <button class="btn hover2" style="color:#58ACFA;"><strong>차량번호로 조회</strong></button>
            </div>
			<br> -->
		
		
            <div style ="margin-top:5%;" onclick="javascript:goSearchCarrierPay();" >
          		  <button class="btn hover3" style="color:#FFFF00;"><strong>탁송금액 조회</strong></button>
            </div>
            	<br>
            	
            	
            <div style ="margin-top:5%;" onclick="javascript:goKakaoTalk();" >
          		  <button class="btn hover4" style="color:#FFFF00;"><strong>카카오톡채널 문의</strong></button>
            </div>
                            
                <br>
                
       <!--    <div style ="margin-top:5%;" onclick="javascript:goKakaoTalk();" >
          		 	<button class="btn hover4" style="color:#58ACFA;"><strong>어플소개</strong></button>
            </div>
             -->
            
            
					<div class="password d-table" style="margin-top: 15%;">
<!-- 						<img style=" width:10%; height:6%;" src="/img/icons8-warning-shield-48.png" /> -->
						<img style=" width:8%; height:6%;" src="/img/logout.png" />
					<a style="cursor: pointer; width: 100%;  background:/*  #CAF4FB; */"
						onclick="javascript:homeLoader.show(); goLoginPage();" class="">
						<span class="d-tbc" style="font-weight: bold; color: #585858;">로그인</span></a>
				</div>

   				<div class="password d-table" style="margin-top: 20%; width:100%;">
						<!-- <img style=" width:10%; height:6%;" src="/img/carLogo.jpg" /> -->
                    		<a style="cursor: pointer; width: 100%;  background:/*  #CAF4FB; */"onclick="javascript:homeLoader.show(); document.location.href='/carrier/introduction.do'" class="">
					&nbsp;<span class="d-tbc" style="font-weight: bold; color: #585858;"> 한국카캐리어(주) 소개</span></a>
			</div>
		</div>

	</div>



<!-- <script src="/js/vendor/jquery-1.11.2.min.js"></script> -->
 	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> 
	<script>
		window.jQuery
				|| document
						.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')
	</script>
	<script src="/js/vendor/bootstrap.min.js"></script>
	<!-- veiwport for countnumber -->
	<script src="/js/jquery.viewportchecker.js"></script>
	<script src="/js/alert.js"></script>
	<script src="/js/main.js"></script>
	<script src="/js/slick.min.js"></script>
	<script src="/js/jquery.bxslider.js"></script>
	<script src="/js/animsition.min.js"></script>
	<script src="/js/vendor/jquery.loading-indicator.min.js"></script>
	<script>
var homeLoader;
	
	$(document).ready(function(){
			
		
				homeLoader = $('body').loadingIndicator({
					useImage: false,
					showOnInit : false
				}).data("loadingIndicator");
				
				/* setTimeout(function() {
					homeLoader.show();
					homeLoader.hide();
				}, 1000); */
		
	});
    

	</script>
</body>
</html>
