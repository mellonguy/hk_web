<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko"  style=" height:100%;">      
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
<link rel="stylesheet" href="/css/animsition.min.css">
    
<body style=" height:100%;">


<script type="text/javascript">  



var certStatus = false;
var sendStatus = false;

$(document).ready(function(){


	$('.modal-field').css('display','none');
	
	/*	
	var updateToken = setInterval( function() {
		
 		if(getDeviceToken() != null && getDeviceToken() != ""){
			updateDriverDeviceToken(getDeviceToken());
			if(window.Android != null){
				window.Android.startService();
			}
			clearInterval(updateToken);
		}
   }, 1000);
 
 */

	 $(".animsition").animsition({
		    inClass: 'zoom-in-lg',
		    outClass: 'zoom-out-lg',
		    inDuration: 1500,
		    outDuration: 800,
		    linkElement: '.animsition-link',
		    // e.g. linkElement: 'a:not([target="_blank"]):not([href^="#"])'
		    loading: true,
		    loadingParentElement: 'body', //animsition wrapper element
		    loadingClass: 'animsition-loading',
		    loadingInner: '', // e.g '<img src="loading.svg" />'
		    timeout: false,
		    timeoutCountdown: 5000,
		    onLoadEvent: true,
		    browser: [ 'animation-duration', '-webkit-animation-duration'],
		    // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
		    // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
		    overlay : false,
		    overlayClass : 'animsition-overlay-slide',
		    overlayParentElement : 'body',
		    transition: function(url){ window.location.href = url; }
		  });

	
});



function getDeviceToken(){
	
	var token = "";
	try{
		if(window.Android != null){
			token = window.Android.getDeviceToken("${driverId}");
		}	
	}catch(exception){
		
	}finally{
		
	}
	return token;
}  
  
/* 
function updateDriverDeviceToken(token){
	
	
	if(token != ""){
		$.ajax({ 
			type: 'post' ,
			url : "/updateDriverDeviceToken.do" ,
			dataType : 'json' ,
			data : {
				token : token
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					//$.alert("성공",function(a){
					//});
				}else if(result == "E000"){
					
				}else{
					
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
}  
 */


 function getNewsData(obj,keyword){
	
	 
	// alert($("#selectedStatus").children().length);
	 
	//alert($("#selectedStatus").children().first().get(0));	
	//alert($(obj).parent().get(0));
	
 	 if($("#selectedStatus").children().first().get(0) == $(obj).parent().get(0)){				//전체를 클릭 한경우
					
 		$("#selectedStatus").children().each(function(index,element){
 			$(this).removeClass('selected');
 		});
 		$(obj).parent().addClass('selected');
	}else{
			
	} 
	
	
	
/* 	
	
	$.ajax({ 
		type: 'post' ,
		url : "/research/newsSearch.do?&keyword="+keyword ,
		dataType : 'json' ,
		data : {
			
		},
		success : function(data, textStatus, jqXHR)
		{
			alert(data);
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
	 */
	
	
} 

 function goNewsPage(uri){
		
		//var replaceUrlStr = replaceAll(url, "?", "^");
		//replaceUrlStr = replaceAll(replaceUrlStr, "=", "+");
		
		var replaceUrlStr = encodeURIComponent(uri)
		document.location.href = "/news/newsPage.do?newsUri="+replaceUrlStr;
	}  


    
function viewAllocationData(allocationId,allocationStatus){
	homeLoader.show();
	document.location.href = "/carrier/allocation-detail.do?allocationStatus="+allocationStatus+"&allocationId="+allocationId;

	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
} 
 
 
function viewAllocationList(allocationId,allocationStatus){
	homeLoader.show();
	document.location.href = "/carrier/group-list.do?allocationId="+allocationId+"&allocationStatus="+allocationStatus;

	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
	
} 
 
 
 
function logout(){
	
	$.confirm("로그아웃 하시겠습니까?",function(a){
		 if(a){
			 if(window.Android != null){
					window.Android.stopService("${driverId}");
				}	
			 homeLoader.show();
			 document.location.href = "/logout.do";	
		 }
	});
	
}   



function memCheck(){

	if($("#customerName").val() == ""){
		$.alert("고객명은 필수 입력 사항입니다.");
		return false;
	}else{
		/*
		$.ajax({
			type : 'post',
			url : '/carrier/compareCustomerId.do',
			dataType : 'json',
			data : {

				customerName : $("#customerName").val()
				
			},success : function(data, textStatus, jqXHR) {
		         var result = data.resultCode;
		         	
					if(result == "0000") {

								
					}else if (result == "0001") { 
						$.alert("동일한 회사명이 이미 존재 합니다. 카카오톡채널로 문의 해 주세요.");
						return false;	
				}  
			},
			error : function(xhRequest, ErrorText, thrownError) {
				
			}
		});

		*/



	}
	return true;	
}


function idCheck(){

	var rtnStatus = true;


	var engNum = /^[a-z]+[a-z0-9]{5,15}$/g;
	
	var userId =$("#inputID").val().replace(/ /g,"");

	
	if(userId == ""){
		$.alert("아이디는 필수 입력 사항입니다.");
		rtnStatus = false;
	}else if(userId.length < 5 || userId.length > 15){
		 $.alert("아이디는 5자리 이상이어야 합니다.");
		 rtnStatus = false;
	}else if(!engNum.test(userId)){
		  $.alert('형식에 맞지 않는 아이디입니다. 다시 입력해 주세요.');
		  rtnStatus = false;
	}else{

		$.ajax({
			type : 'post',
			url : '/carrier/compareCustomerId.do',
			dataType : 'json',
			async : false,
			data : {

				inputID : $("#inputID").val(),
				
			},success : function(data, textStatus, jqXHR) {
		         var result = data.resultCode;
		         	
					if(result == "0000") {

						//sendMessage();
						//$.alert("사용 가능");
						
					}else if (result == "0001") { 
						$.alert("동일한 아이디가 이미 존재 합니다.");
						rtnStatus = false;
					}  
			},
			error : function(xhRequest, ErrorText, thrownError) {
				
			}
		});

	}

	return rtnStatus;
}



function phoneChk(obj){

	
		var trans_num = $(obj).val().replace(/-/gi,'');
	    if(trans_num != null && trans_num != ''){
	        if(trans_num.length==11 || trans_num.length==10){   
	       	 						var regExp_ctn = /^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})([0-9]{3,4})([0-9]{4})$/;
	            if(regExp_ctn.test(trans_num)){
	                trans_num = trans_num.replace(/^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})-?([0-9]{3,4})-?([0-9]{4})$/, "$1-$2-$3");                  
	                //$(obj).val(trans_num);
	                return "0";
	            }else{
	                return "1";
	            }
	        }else{
	            return "2";
	        }
	  }

	
}


function pwdCheck(){


	 var pw = $("#userPwd").val();
	 var num = pw.search(/[0-9]/g);
	 var eng = pw.search(/[a-z]/ig);
	 var spe = pw.search(/[`~!@@#$%^&*|₩₩₩'₩";:₩/?]/gi);

	 if(pw == ""){
		$.alert("패스워드가 입력 되지 않았습니다.",function(a){
	 	});
	 	return false;

	 }
	 if(pw.length < 5 || pw.length > 20){
	 	$.alert("패스워드는 5자리 이상이어야 합니다.",function(a){
	 	});
	 	return false;
	 }
	 if(pw.search(/₩s/) != -1){
	 	$.alert("패스워드는 공백업이 입력해주세요.",function(a){
	 	});
	 	return false;
	 	 }
	 if(num < 0 || eng < 0 || spe < 0 ){
	 	$.alert("영문,숫자, 특수문자를 혼합하여 입력해주세요.",function(a){
	 	});
	 	return false;
	  }

	 if($("#userPwdChk").val() == ""){
	 	$.alert("패스워드확인이 입력되지 않았습니다.",function(a){
	 	});
	 	return false;
	 }

	 if($("#userPwd").val() != $("#userPwdChk").val()){
	 	$.alert("패스워드와 패스워드 확인에 입력 한 값이 다릅니다.",function(a){
	 	});
	 	return false;
	 } 

	return true;
	
}



function validation(){

	if(!pwdCheck()){
		return false;
	}

	if(!memCheck()){
		return false;
	}

	if(!idCheck()){
		return false;
	}

	
	
	//alert(1);
	var result = phoneChk($("#phoneNum")); 
	if(result != "0"){
		$.alert("휴대전화번호가 유효하지 않습니다.");
		return false;
	}
	

	if(certStatus){

		if($("#phoneNum").val() != $("#phoneNumCompare").val()){
			
			$.alert("인증된 휴대전화 번호가 아닙니다. 재 인증을 진행 해 주세요.", function(a) {

					if(a){
						document.location.reload();
					}
			});
			
		}else{

			$.confirm("회원 가입 하시겠습니까?",function(a){


				if(a){

					$('#insertForm').ajaxForm({
						url: "/baseinfo/insert-customer.do",
					    type: "POST",
						dataType: "json",		
						data : {

							
					    },
						success: function(data, response, status) {
							var status = data.resultCode;
							if(status == '0000'){
								$.alert("회원 등록이 정상적으로 완료 되었습니다.",function(a){

									document.location.href = "/carrier/sign-Up.do?findId="+$("#inputID").val();
									
								});
							}else if(status == '0001'){
								$.alert("알 수없는 오류입니다. 카카오톡 플러스채팅으로 문의 주세요",function(a){
									document.location.href = "/carrier/three-option.do";
								});			
							}
									
						},
						error: function() {
							$.alert("오류가 발생하였습니다. 관리자에게 문의 하세요.");
						}                               
					});
					
					$("#insertForm").submit();
					
				}
				 
			});
				

		}
		
		
		

		
	}else{


		$.alert("휴대전화가 인증 되지 않았습니다.");
		
	}
	
	


	
}


function insertTest(){

	$('#insertForm').ajaxForm({
		url: "/baseinfo/insert-customer.do",
	    type: "POST",
		dataType: "json",		
		data : {

			
	    },
		success: function(data, response, status) {
			var status = data.resultCode;
			if(status == '0000'){
				$.alert("회원 등록이 정상적으로 완료 되었습니다. 로그인 페이지로 이동 합니다.",function(a){

					document.location.href = "/carrier/sign-Up.do";
					
				});
			}else if(status == '0001'){
				$.alert("알 수없는 오류입니다. 카카오톡 플러스채팅으로 문의 주세요",function(a){
					document.location.href = "/carrier/three-option.do";
				});			
			}
					
		},
		error: function() {
			$.alert("오류가 발생하였습니다. 관리자에게 문의 하세요.");
		}                               
	});
	
	$("#insertForm").submit();




	
}









var selectedObj = new Object();


function selectInputId(){

	if($("#inputID").val() != ""){

		$.ajax({
			type : 'post',
			url : '/carrier/compareCustomerId.do',
			dataType : 'json',
			data : {

				inputID : $("#inputID").val(),
				
			},success : function(data, textStatus, jqXHR) {
		         var result = data.resultCode;
		         	
					if(result == "0000") {

						sendMessage();
					
					}else if (result == "0001") { 
						$.alert("입력한 아이디로 가입된 아이디가 없습니다.", function(a) {
	
						});	
					}  
			},
			error : function(xhRequest, ErrorText, thrownError) {
				
			}
		});


		}else{

			$.alert("아이디가 입력이 되지 않았습니다.");
			


			}
		
}


	selectedObj = new Object(); 

	function sendMessage(obj){


		if($("#phoneNum").val() == ""){
			$.alert("휴대전화가 입력되지 않았습니다.");
			return false;
		}

		if(phoneChk($("#phoneNum")) != "0"){
			$.alert("휴대전화번호가 유효하지 않습니다.");
			return false;
		}
		
		selectedObj = new Object();


		if(!sendStatus){
	
			$.confirm("인증번호를 전송하시겠습니까?",function(a){

				 if(a){
	
						$.ajax({
							type : 'post',
							url : '/carrier/phoneDuplicationChk.do',
							dataType : 'json',
							data : {

								phoneNum : $("#phoneNum").val()
								
								
							},success : function(data, textStatus, jqXHR) {
						         var result = data.resultCode;
						         	
									if(result == "0000") {
										sendStatus = true;
										insertCertNumForAPP();
										
									}else if (result == "0002") { 
										$.alert("알 수 없는 오류입니다. 관리자에게 문의해주세요.", function(a) {
					
										});	
									}else if (result == "0003") { 

										$.confirm("이미 등록 되어 있는 휴대전화입니다. 조회 화면으로 이동 하시겠습니까?.",function(a){
											 if(a){
												 homeLoader.show();
												 document.location.href = "/carrier/login-page.do?&optionType=L"; 	

													var updateHomeLoader = setInterval(function() {
														clearInterval(updateHomeLoader);
														homeLoader.hide();
													}, 300);
											 }else{

											 }
										});


									}  
							},
							error : function(xhRequest, ErrorText, thrownError) {
								
							}
						});
						
									 	
				 }else{

					 
				}
			});

		}else{

			$.alert("인증번호가 이미 전송 되었습니다.");

		}
		

		
	}
		



function insertCertNumForAPP(){


	$.ajax({
		type : 'post',
		url : '/carrier/insertCertNumForAPP.do',
		dataType : 'json',
		data : {

			inputID : $("#inputID").val(),
			phoneNum : $("#phoneNum").val(),
			customerId : "",
			gubun : 'N',
			
		},success : function(data, textStatus, jqXHR) {
	         var result = data.resultCode;
	         	
				if(result == "0000") {
					$.alert("인증번호가 전송 되었습니다.", function(a) {
						$("#phoneNum").attr("readonly",true);
						//인증 후 휴대전화 번호를 변경 하는 경우는 재 인증을 진행 해야 한다.
						$("#phoneNumCompare").val($("#phoneNum").val());
					});
				}else if (result == "0002") { 
					$.alert("알 수 없는 오류입니다. 관리자에게 문의해주세요.", function(a) {

					});	
			}  
		},
		error : function(xhRequest, ErrorText, thrownError) {
			
		}
	});

	
}



	


	
function sendPinNum(){



	if(!certStatus){

		var phoneCertNum = $("#phoneCertNum").val();

		if(phoneCertNum != ""){

					$.ajax({
						type : 'post',
						url  : "/carrier/checkCertNumForLogin.do",
					    dataType : 'json',
					    
					       data : {
						       
					    	inputID : $("#inputID").val(),
							phoneNum :  $("#phoneNum").val(),
							customerId : "",
							phoneCertNum : phoneCertNum,
							gubun : 'N',
					
							},

					   success : function(data, textStatus, jqXHR)
					    {
						   var resultCode = data.resultCode;
						   var resultData = data.resultData;
						   
						   if(resultCode == "0000"){
								$.alert("인증에 성공하였습니다.");
								$("#phoneCertNum").attr("readonly",true);
								certStatus = true;
						   }else{
							 $.alert("인증번호가 맞지 않습니다. 관리자에게 문의하세요.");
						   }
					   } ,
					  error :function(xhRequest, ErrorText, thrownError){
						  $.alert('알수 없는 오류입니다. 관리자에게 문의 하세요.');
		                  
						  }
					});
		
			}else{

			$.alert("인증번호가 입력되지 않았습니다.");
			return false;
		}
		
	}else{

		$.alert("인증이 완료 되었습니다.");
	}

	
}



function findID(findId){


	document.location.href ="/carrier/sign-Up.do?&findId="+findId;
		
}




function goSingUpPage(){

	document.location.href='/carrier/simpleSign-Up.do?&customerId='+selectedObj.customerId+'&chargeId='+selectedObj.chargeId+'&phoneNum='+selectedObj.phoneNum;
	
}

 
function backKeyController(str){
	//history.go(-1);
	//location.href = document.referrer;
	

	
	document.location.href='/carrier/sign-Up.do';


	
}

function goChagnePassword(findId){

	document.location.href ="/carrier/sign-Up.do?&findId="+findId;
	
}

function goSignUpPage(){


	document.location.href='/carrier/sign-Up.do';
	
}


function newPasswordChange(){


	var newPassword = $("#newPassword").val();
	var confirmpw = $("#confirmpw").val();

	
	    var num = newPassword.search(/[0-9]/g);
	    var eng = newPassword.search(/[a-z]/ig);
	    var spe = newPassword.search(/[`~!@@#$%^&*|₩₩₩'₩";:₩/?]/gi);

	    

	    if(newPassword.length < 6 || newPassword.length > 20){
	     $.alert("패스워드는 6자리 이상이어야 합니다.");
	     
	     return false;
	    }
	    if(newPassword.search(/₩s/) != -1){
	        $.alert("패스워드는 공백없이 입력해주세요.");
	        return false;
	       }
	    if(num < 0 || eng < 0 || spe < 0 ){
	      $.alert("영문,숫자, 특수문자를 혼합하여 입력해주세요.");
	      return false;
	     }

	    
	 if(newPassword == confirmpw ){


				$.ajax({
					type : 'post',
					url  : "/carrier/confirmForPassWord.do",
				    dataType : 'json',
				    
				       data : {

						userId : $("#inputID").val(),
						newPassword : newPassword,
						
						},

				   success : function(data, textStatus, jqXHR)
				    {
					   var resultCode = data.resultCode;
					   if(resultCode == "0000"){

						   $.alert("비밀번호가 변경되었습니다.",function(a){
								if(a){
									goSignUpPage();
								}
							});
									
						}				

						   },
				  error :function(xhRequest, ErrorText, thrownError){
	                  $.alert(' @@@@error@@@@ ');
	                  
					  }

				});

			}else if(newPassword ==""){
				$.alert("입력창에 입력이 되지 않았습니다.");
		
			}else{
			$.alert("비밀번호가 같지 않습니다. 다시한번 확인해주세요");

				}
		}



function setAddress(value){






	
}




function goShowModal(obj){
	//$('.modal-field').show();
}


</script>


        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div class="content-container" style=" height:100%; /* overflow-y:hidden; */">
            <%-- <jsp:include page="/WEB-INF/views/jsp/common-top.jsp"></jsp:include> --%>
            <div class="animsition">
            <header class="bg-pink clearfix" style="position:fixed; background-color:#fff; border:none; text-align:center; z-index:100000; ">
                <div class="" style="width:19%;">
                    <a href="/carrier/login-page.do?&optionType=L"><img style="zoom: 0.5;" src="/img/back-icon.png" alt=""></a> 
                </div> 
                <div class="" style="width:60%;">
                	<img style="width:90%; height:60%;" onclick="javascript:homeLoader.show(); document.location.href='/carrier/three-option.do'" src="/img/main_logo.png" alt="">
                </div>
                
               <div class="" style="width:19%; float:right;">
                   <!-- <a style="cursor:pointer;" href="javascript:logout();"><img style="" src="/img/logout.png" alt=""></a> -->
                </div> 
            </header>
            <%-- <div style="text-align:right;">${user.driver_name}님 환영 합니다.</div> --%>
            
		        
            <div style=" text-align:right; clear:both;">&nbsp;</div>
            <br>
	            
                
            <div class="content-container interest-page loadingthemepage news" style="background-color:#fff; margin-top:20px; text-align:center; clear:both;">
                	 <span class="d-tbc" style="font-size:20px; font-weight: bold; color:#00000;">회원 등록</span>
			</div>
						
						
						<form id="insertForm" action="">
						
				 			<div style=" width:100%; overflow:hidden;">
				                 <div style="float:left; width:50%;">
				                 	<div class="certNum" style=" margin-top: 10%; width:100%;">
				                 		<div style="text-align:center;">
					                 		<div style="text-align:left; margin-left:5%;">
												<span class="d-tbc" style="font-weight: bold;"><span style="color:#F00;">*</span>회원구분</span>
											</div> 
												<select name ="customerKind" id ="customerKind" style="background-color:#fff; margin-top:5%; width:90%; padding:3%; border-width:2px; border-style:inset; ">
											            <option value="01">개인</option>
														<!-- <option value="00">법인</option>
											            <option value="02">외국인</option>
											            <option value="03">개인(주민번호)</option> -->
														
												</select> 
					         			</div>
					      			</div>
					      		</div>
					      		<div style="float:left; width:50%;">
				                	<div class="certNum" style=" margin-top: 10%; width:100%;">
				                 		<div style="text-align:center;">
					                 		<div style="text-align:left; margin-left:5%;">
												<span class="d-tbc" style="font-weight: bold;"><span style="color:#F00;">*</span>고객명</span>
											</div> 
						         			<input type="text" style=" width:90%; padding:3%; margin-top: 5%;" placeholder="고객명" class="d-tbc" id="customerName" name="customerName" value="" onkeypress="if(event.keyCode=='13') sendPinNum();">
					         			</div>
					      			</div>
					      		</div>
					      	</div>
					      	<div style=" width:100%; overflow:hidden;">
				                 <div style="float:left; width:50%;">
				                 	<div class="certNum" style=" margin-top: 10%; width:100%;">
				                 		<div style="text-align:center;">
					                 		<div style="text-align:left; margin-left:5%;">
												<span class="d-tbc" style="font-weight: bold;"><span style="color:#F00;">*</span>아이디</span>
											</div> 
						         			<input type="text" style=" width:90%; padding:3%; margin-top: 5%;" placeholder="영문과숫자 5자리이상" class="d-tbc" id="inputID" name="inputID" value="" onkeypress="if(event.keyCode=='13') sendPinNum();">
					         			</div>
					      			</div>
					      		</div>
					      		<div style="float:left; width:50%;">
				                	<div class="certNum" style=" margin-top: 10%; width:100%;">
				                 		<div style="text-align:center;">
					                 		<div style="text-align:left; margin-left:5%;">
												<span class="d-tbc" style="font-weight: bold;">사업자등록번호</span>
											</div> 
						         			<input type="text" style=" width:90%; padding:3%; margin-top: 5%;" placeholder="사업자등록번호" class="d-tbc" id="businessLicenseNumber" name="businessLicenseNumber" value="${phoneCertNum}" onkeypress="if(event.keyCode=='13') sendPinNum();">
					         			</div>
					      			</div>
					      		</div>
					      	</div>
					      	
					      	
					      	
					      	<div style=" width:100%; overflow:hidden;">
				                 <div style="float:left; width:50%;">
				                 	<div class="certNum" style=" margin-top: 10%; width:100%;">
				                 		<div style="text-align:center;">
					                 		<div style="text-align:left; margin-left:5%;">
												<span class="d-tbc" style="font-weight: bold;"><span style="color:#F00;">*</span>패스워드</span>
											</div> 
						         			<input type="password" style=" width:90%; padding:3%; margin-top: 5%;" placeholder="패스워드" class="d-tbc" id="userPwd" name="userPwd" value="" >
					         			</div>
					      			</div>
					      		</div>
					      		<div style="float:left; width:50%;">
				                	<div class="certNum" style=" margin-top: 10%; width:100%;">
				                 		<div style="text-align:center;">
					                 		<div style="text-align:left; margin-left:5%;">
												<span class="d-tbc" style="font-weight: bold;">패스워드확인</span>
											</div> 
						         			<input type="password" style=" width:90%; padding:3%; margin-top: 5%;" placeholder="패스워드확인" class="d-tbc" id="userPwdChk" name="userPwdChk" value="" >
					         			</div>
					      			</div>
					      		</div>
					      	</div>
					      	
					      	
					      	
					      	
					      	
					      	
					      	
					      	<div style=" width:100%; overflow:hidden;">
				                 <div style="float:left; width:50%;">
				                 	<div class="certNum" style=" margin-top: 10%; width:100%;">
				                 		<div style="text-align:center;">
					                 		<div style="text-align:left; margin-left:5%;">
												<span class="d-tbc" style="font-weight: bold;"><span style="color:#F00;">*</span>휴대전화(숫자만입력)</span>
											</div> 
						         			<input type="number" pattern="\d*" style=" width:90%; padding:3%; margin-top: 5%;" placeholder="휴대전화(숫자만입력)" class="d-tbc" id="phoneNum" name="phoneNum" value="">
						         			<input type="hidden" id="phoneNumCompare" id="phoneNumCompare" name="phoneNumCompare" value="">
					         			</div>
					      			</div>
					      		</div>
					      		<div style="float:left; width:50%;">
				                	<div class="certNum" style=" margin-top: 10%; width:100%;">
				                 		<div style="text-align:center;">
					                 		<div style="text-align:left; margin-left:5%;">
												<span class="d-tbc" style="font-weight: bold;">&nbsp;</span>
											</div> 
						         			<input type="button" class="btn-primary" style="width:90%; padding:3%; margin-top: 5%;" value="인증번호전송" class="d-tbc"  name="" value="" onclick="javascript:sendMessage(this);">
					         			</div>
					      			</div>
					      		</div>
					      	</div>
					      	<div style=" width:100%; overflow:hidden;">
				                 <div style="float:left; width:50%;">
				                 	<div class="certNum" style=" margin-top: 10%; width:100%;">
				                 		<div style="text-align:center;">
					                 		<div style="text-align:left; margin-left:5%;">
												<span class="d-tbc" style="font-weight: bold;"><span style="color:#F00;">*</span>인증번호입력</span>
											</div> 
						         			<input type="number" pattern="\d*" style=" width:90%; padding:3%; margin-top: 5%;" placeholder="인증번호입력" class="d-tbc" id="phoneCertNum" name="phoneCertNum" value="" >
					         			</div>
					      			</div>
					      		</div>
					      		<div style="float:left; width:50%;">
				                	<div class="certNum" style=" margin-top: 10%; width:100%;">
				                 		<div style="text-align:center;">
					                 		<div style="text-align:left; margin-left:5%;">
												<span class="d-tbc" style="font-weight: bold;">&nbsp;</span>
											</div> 
						         			<input type="button" class="btn-primary" style="width:90%; padding:3%; margin-top: 5%;" value="인증번호확인" class="d-tbc"  name="" value="" onclick="javascript:sendPinNum();">
					         			</div>
					      			</div>
					      		</div>
					      	</div>
					      	<div style="clear:both; margin-top:5%;">
			                 	<div id="btn_group" class="certNum" style=" width:100%; margin-top:5%;">
			                 		<div style="text-align:center;">
				                 		<div style="text-align:left; margin-left:2.5%;">
				                 			<!-- <span style="color:#F00;">*</span> -->
												<span class="d-tbc" style="font-weight: bold;">주소</span>
										</div> 
					         			<input type="text" style=" width:75%; padding:2%; margin-top:2%;" placeholder="주소 입력"  id="searchKeyword" name="address" value="" onfocus="javascript:$(this).val(''); $(this).next().val('');">
					         				<input type="hidden" >
				         					<button id="test_btn1" class="btn_select" onclick ="javascirpt:initTmap(); return false;" style="width:20%;" >검색</button>
				         			</div>
				      			</div>
							</div>
					      
					      	
					      	
					      	<!-- <div style="clear:both; margin-top:5%;">
			                 	<div class="certNum" style=" width:100%; margin-top:5%;">
			                 		<div style="text-align:center;">
				                 		<div style="text-align:left; margin-left:2.5%;">
											<span class="d-tbc" style="font-weight: bold;"><span style="color:#F00;">*</span>주소</span>
										</div> 
					         			<input type="text" style=" width:95%; padding:2%; margin-top:2%;" placeholder="주소" class="d-tbc" id="address" name="address" value="" onkeypress="if(event.keyCode=='13') sendPinNum();">
				         			</div>
				      			</div>
							</div> -->
							<div class="modal-field">
						            <div class="modal-box">
						          	  <div class="modal-table-container">
						                   <div class="title" style="text-align:center; margin-top:5%;">출발지&nbsp;<strong>검색</strong> 목록</div>
						                      <br>
												<div class="rst_wrap">
													<div class="rst mCustomScrollbar">
														<ul id="searchResult" name="searchResult" >
														</ul>
													</div>
												</div>
						                        
						                </div>
						                <div class="confirmation" >
						                    <div class="confirm" >
						                        <input type="button" value="창닫기" name="" id="departureCansle" onclick ="javascript:$('.modal-field').hide();">
						                    </div>
						                </div>
						            </div>
						        </div> 
							
							
							<div style=" width:100%; overflow:hidden;">
				                 <div style="float:left; width:50%;">
				                 	<div class="certNum" style=" margin-top: 10%; width:100%;">
				                 		<div style="text-align:center;">
					                 		<div style="text-align:left; margin-left:5%;">
												<span class="d-tbc" style="font-weight: bold;">상세주소</span>
											</div> 
						         			<input type="text" style=" width:90%; padding:3%; margin-top: 5%;" placeholder="상세주소" class="d-tbc" id="addressDetail" name="addressDetail" value="" >
					         			</div>
					      			</div>
					      		</div>
					      		
					      	</div>
					      	
					   	<div style=" width:100%; overflow:hidden; ">
				              <div style="float:left; width:80%;">
				                 <div class="certNum" style=" margin-top: 10%; width:100%; ">
				                 		<div style="text-align:center; ">
					                 		<div style="text-align:left; margin-left:5%;">
												<span class="d-tbc" style="font-weight: bold;">생년 월일</span>
											</div> 
												<div style ="display:inline-block; width:80%;">
								         			<input type="date" style=" width:90%; padding:3%; margin-top: 5%; " placeholder="입력" class="d-tbc" id="birthday" name="birthday" value="" >
								         			
								         			<!-- <input type="text" style=" width:90%; padding:3%; margin-top: 5%; " placeholder="입력" class="d-tbc" id="addressDetail" name="addressDetail" value="" > -->
								         			<!-- <input type="text" style=" width:90%; padding:3%; margin-top: 5%; " placeholder="입력" class="d-tbc" id="addressDetail" name="addressDetail" value="" > -->
						         				</div>
					         			</div>
					      			</div>
					      		</div>
					      		
					      	</div>
					      	
					      	
						<div style="clear:both; margin-top:5%;">
			                 		<div style="text-align:center;">
				                 		<div style="text-align:left; margin-left:2.5%;">
				                 			<!-- <span style="color:#F00;">*</span> -->
											<span class="d-tbc" style="font-weight: bold;">e-mail (선택)</span>
										</div> 
					         				<input type="text" style=" width:95%; padding:2%; margin-top:2%;" placeholder="e-mail 입력"  id="searchKeyword" name="email" value="" onfocus="javascript:$(this).val(''); $(this).next().val('');">
				         					<br>
				         					<span class="d-tbc" style="font-weight: bold;"> ※ 추후 한국카캐리어(주) 이벤트 및 할인코드 발급을 위해 입력해주세요</span>
				         			</div>
							</div>
					      	
					      	
					      	
					      	
					      	
					      	
					      	<div style="margin-top: 10%; margin-bottom:10%; width:100%; ">
								<div style="border-radius: 50%; height: 30%; width: 25%; background-color: #CEF6F5; text-align: center; margin:0 auto; ">
									<!-- <a style="cursor: pointer; width: 100%; border-radius: 80%; background: #CEF6F5;" onclick="javascript:certification();" class="">회원가입</a> -->
									
									
									<a style="cursor: pointer; width: 100%; border-radius: 80%; background: #CEF6F5;" onclick="javascript:validation();" class="">회원가입</a>
									<!-- <a style="cursor: pointer; width: 100%; border-radius: 80%; background: #CEF6F5;" onclick="javascript:insertTest();" class="">회원가입</a> -->
							
								</div>
							</div>

					</form>

		</div>
	</div>
        
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      
    <script src="/js/vendor/bootstrap.min.js"></script>       
    <!-- veiwport for countnumber -->
    <script src="/js/alert.js"></script>
    <script src="/js/main.js"></script>   
    <script src="/js/vendor/xpull.js"></script>       
    <script src="/js/vendor/jquery.loading-indicator.min.js"></script>
    <script src="https://apis.openapi.sk.com/tmap/jsv2?version=1&appKey=l7xx6b701ff5595c4e3b859e42f57de0c975"></script>
    <script src="/js/animsition.min.js"></script>
    <script src="/js/vendor/jquery.form.min.js"></script>
    
    <script>
    
	
	$('#container').xpull({
	    'paused': false, 
	    // Pull threshold - amount in  pixels required to pull to enable release callback
        'pullThreshold':50,
        // Max pull down element - amount in pixels
        maxPullThreshold: 50,
        // timeout in miliseconds after which the loading indicator stops spinning.
        // If set to 0 - the loading will be indefinite
        'spinnerTimeout':1000,
        onPullStart: function(){},
        onPullEnd: function(){},
        callback: function(){window.location.reload()},

	});   
	
	
	var homeLoader;
	
	$(document).ready(function(){
			
		
				homeLoader = $('body').loadingIndicator({
					useImage: false,
					showOnInit : false
				}).data("loadingIndicator");
				
				/* setTimeout(function() {
					homeLoader.show();
					homeLoader.hide();
				}, 1000); */
		
	});




	 var map, marker;
	    var markerArr = [];
	    
	    function initTmap(){
	   
	    	// 2. POI 통합 검색 API 요청
	  //  	$(".btn_select").click(function(){
 
	    		var searchKeyword = $('#searchKeyword').val();
	    		$("#searchResult").css("display","block");

	    		$('.modal-field').show();
	    		
	    		$.ajax({
	    			method:"GET",
	    			url:"https://apis.openapi.sk.com/tmap/pois?version=1&format=json&callback=result",
	    			async:false,
	    			data:{
	    				"appKey" : "l7xx6b701ff5595c4e3b859e42f57de0c975",
	    				"searchKeyword" : searchKeyword,
	    				"resCoordType" : "EPSG3857",
	    				"reqCoordType" : "WGS84GEO",
	    				"count" : 10
	    			},
	    			success:function(response){
	    				var resultpoisData = response.searchPoiInfo.pois.poi;

	    				
	    				
	    				// 기존 마커, 팝업 제거
	    				if(markerArr.length > 0){
	    					for(var i in markerArr){
	    						markerArr[i].setMap(null);
	    					}
	    				}
	    				var innerHtml ="";	// Search Reulsts 결과값 노출 위한 변수
	    				var positionBounds = new Tmapv2.LatLngBounds();		//맵에 결과물 확인 하기 위한 LatLngBounds객체 생성

						
	    				
	    				for(var k in resultpoisData){
	    					
	    					var noorLat = Number(resultpoisData[k].noorLat);
	    					var noorLon = Number(resultpoisData[k].noorLon);
	    					var name = resultpoisData[k].name;

	    					var upperAddrName = resultpoisData[k].upperAddrName;
	    					var middleAddrName = resultpoisData[k].middleAddrName;
	    					var lowerAddrName = resultpoisData[k].lowerAddrName;
	    					var detailAddrName = resultpoisData[k].detailAddrName;
	    					var firstNo = resultpoisData[k].firstNo;
	    					var secondNo = resultpoisData[k].secondNo == "" || resultpoisData[k].secondNo == "0" ? "" : "-"+resultpoisData[k].secondNo;
							/* alert(upperAddrName+middleAddrName+lowerAddrName+detailAddrName); */

							
	    					
	    					var pointCng = new Tmapv2.Point(noorLon, noorLat);
	    					var projectionCng = new Tmapv2.Projection.convertEPSG3857ToWGS84GEO(pointCng);
	    					
	    					var lat = projectionCng._lat;
	    					var lon = projectionCng._lng;
	    					
	    					var markerPosition = new Tmapv2.LatLng(lat, lon);
	    					
	    					marker = new Tmapv2.Marker({
	    				 		position : markerPosition,
	    				 		//icon : "http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_a.png",
	    				 		icon : "http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_" + k + ".png",
	    						iconSize : new Tmapv2.Size(24, 38),
	    						title : name,
	    						//map:map
	    				 	});
	    					
	    					//innerHtml += "<li><img src='http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_" + k + ".png' style='vertical-align:middle;'/><span onclick='javascript:inputValue(\''+name+'\')'>"+name+"</span></li>";
	    					//innerHtml += "<li><img src='http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_" + k + ".png' style='vertical-align:middle;'/><span onclick='javascript:inputValue()'>"+name+"</span></li>";
	    					//'<div class="info" onclick="javascript:inputValue(\''+places.road_address_name+'\',\''+places.address_name+'\',\''+places.place_name+'\',\''+places.x+'\',\''+places.y+'\')">' +
							//innerHtml += '<li><img src="http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_' + k + '.png" style="vertical-align:middle;"/><span onclick="javascript:inputValue(\''+name+'\',\''+lon+'\',\''+lat+'\')">'+name+'</span></li>';
							
							//innerHtml += '<li  onclick="javascript:inputValue(\''+name+'\',\''+lon+'\',\''+lat+'\')">';
							
							var fullAddress = upperAddrName+" "+middleAddrName+" "+lowerAddrName+" "+detailAddrName+" "+firstNo+secondNo + " (" +name+ ")";
							
							innerHtml += '<li  onclick="javascript:inputValue(\''+fullAddress+'\',\''+lon+'\',\''+lat+'\')">';
							innerHtml += '<img src="http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_' + k + '.png" style="vertical-align:middle;"/>';
							innerHtml += '<span>'+name+'</span>';
							innerHtml += '<span style="display:block; color:#3f3f3f;" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;('+upperAddrName+" "+middleAddrName+" "+lowerAddrName+" "+detailAddrName+" "+firstNo+secondNo+')</span>';
							innerHtml += '</li>';

	    					markerArr.push(marker);
	    					positionBounds.extend(markerPosition);	// LatLngBounds의 객체 확장
	    				}
	    				
	    				$("#searchResult").html(innerHtml);	//searchResult 결과값 노출
	    				//map.panToBounds(positionBounds);	// 확장된 bounds의 중심으로 이동시키기
	    				//map.zoomOut();
	    				
	    			},
	    			error:function(request,status,error){
	    				console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	    			}
	    		});
	    	//});

	   }

		    	
	   	function inputValue(address,px,py){
	    			
	   				$("#searchKeyword").val(address);
	   				$("#searchKeyword").next().val(px+","+py);
	   				$("#searchResult").css("display","none");
	   				$('.modal-field').css('display','none');

	   	}

    
    </script>
        
    </body>
</html>
