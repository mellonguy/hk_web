<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko"  style=" height:100%;">      
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script> -->
<!-- <script src="http://code.jquery.com/jquery-latest.js"></script> -->
<link rel="stylesheet" href="/css/animsition.min.css">
<link rel="stylesheet" type="text/css" href="/css/jquery.bxslider.css"/>
<link rel="stylesheet" href="/css/Chart.css">
<%-- <canvas id="myChart" width="400" height="250"  style='display:hidden;'></canvas> --%> 

<body style=" height:100%;">



<script type="text/javascript">  

$(document).ready(function(){
	


	/*	
	var updateToken = setInterval( function() {
		
 		if(getDeviceToken() != null && getDeviceToken() != ""){
			updateDriverDeviceToken(getDeviceToken());
			if(window.Android != null){
				window.Android.startService();
			}
			clearInterval(updateToken);
		}
   }, 1000);
 
 */

	 $(".animsition").animsition({
		    inClass: 'zoom-in-lg',
		    outClass: 'zoom-out-lg',
		    inDuration: 1500,
		    outDuration: 800,
		    linkElement: '.animsition-link',
		    // e.g. linkElement: 'a:not([target="_blank"]):not([href^="#"])'
		    loading: true,
		    loadingParentElement: 'body', //animsition wrapper element
		    loadingClass: 'animsition-loading',
		    loadingInner: '', // e.g '<img src="loading.svg" />'
		    timeout: false,
		    timeoutCountdown: 5000,
		    onLoadEvent: true,
		    browser: [ 'animation-duration', '-webkit-animation-duration'],
		    // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
		    // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
		    overlay : false,
		    overlayClass : 'animsition-overlay-slide',
		    overlayParentElement : 'body',
		    transition: function(url){ window.location.href = url; }
		  });



	 $('.slick-items').bxSlider({
		   mode:'fade',         // 사라지는 모양
		   speed:500,           // 이미지변환 속도 기본 500
		   randomStart:false,   // 이미지 랜덤으로 처리
		   controls:true,      // 좌,우 컨트롤 버튼 숨기기/보이기
		   autoControls:false,  //  슬라이드 시작/멈춤
		   pager:false,         // 하단 이미지 보기 버튼
		   auto:true,           // 자동시작
		  
		  });
	  
});



function getDeviceToken(){
	
	var token = "";
	try{
		if(window.Android != null){
			token = window.Android.getDeviceToken("${driverId}");
		}	
	}catch(exception){
		
	}finally{
		
	}
	return token;
}  
  
/* 
function updateDriverDeviceToken(token){
	
	
	if(token != ""){
		$.ajax({ 
			type: 'post' ,
			url : "/updateDriverDeviceToken.do" ,
			dataType : 'json' ,
			data : {
				token : token
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					//$.alert("성공",function(a){
					//});
				}else if(result == "E000"){
					
				}else{
					
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
}  
 */


 function getNewsData(obj,keyword){
	
	 
	// alert($("#selectedStatus").children().length);
	 
	//alert($("#selectedStatus").children().first().get(0));	
	//alert($(obj).parent().get(0));
	
 	 if($("#selectedStatus").children().first().get(0) == $(obj).parent().get(0)){				//전체를 클릭 한경우
					
 		$("#selectedStatus").children().each(function(index,element){
 			$(this).removeClass('selected');
 		});
 		$(obj).parent().addClass('selected');
	}else{
			
	} 
	
	
	
/* 	
	
	$.ajax({ 
		type: 'post' ,
		url : "/research/newsSearch.do?&keyword="+keyword ,
		dataType : 'json' ,
		data : {
			
		},
		success : function(data, textStatus, jqXHR)
		{
			alert(data);
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
	 */
	
	
} 

 function goNewsPage(uri){
		
		//var replaceUrlStr = replaceAll(url, "?", "^");
		//replaceUrlStr = replaceAll(replaceUrlStr, "=", "+");
		
		var replaceUrlStr = encodeURIComponent(uri)
		document.location.href = "/news/newsPage.do?newsUri="+replaceUrlStr;
	}  


    
function viewAllocationData(allocationId,allocationStatus){
	homeLoader.show();
	document.location.href = "/carrier/allocation-detail.do?allocationStatus="+allocationStatus+"&allocationId="+allocationId;

	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
	
} 
 
 
function viewAllocationList(allocationId,allocationStatus){
	homeLoader.show();
	document.location.href = "/carrier/group-list.do?allocationId="+allocationId+"&allocationStatus="+allocationStatus;

	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
	
	
} 
 
 
 
function logout(){
	
	$.confirm("로그아웃 하시겠습니까?",function(a){
		 if(a){
			 if(window.Android != null){
					window.Android.stopService("${driverId}");
				}	
			 homeLoader.show();
			 document.location.href = "/logout.do";	
		 }
	});
	
}   











var selectedObj = new Object();


function selectInputId(){

	if($("#inputID").val() != ""){

		$.ajax({
			type : 'post',
			url : '/carrier/compareCustomerId.do',
			dataType : 'json',
			data : {

				inputID : $("#inputID").val(),
				
			},success : function(data, textStatus, jqXHR) {
		         var result = data.resultCode;
		         	
					if(result == "0000") {

						sendMessage();
					
						}else if (result == "0001") { 
						$.alert("입력한 아이디로 가입된 아이디가 없습니다.", function(a) {
	
					});	
				}  
			},
			error : function(xhRequest, ErrorText, thrownError) {
				
			}
		});


		}else{

			$.alert("아이디가 입력이 되지 않았습니다.");
			


			}
		
}


	selectedObj = new Object(); 

	function sendMessage(){


		selectedObj = new Object();

		
		$.confirm("등록하셨던 번호로 인증번호를 전송하시겠습니까?",function(a){

			 if(a){
					$(".certNum").css('display','block');	
					
					$.ajax({
						type : 'post',
						url : '/carrier/insertCertNumForAPP.do',
						dataType : 'json',
						data : {

							inputID : $("#inputID").val(),
							phoneNum : "",
							customerId : "",
							gubun : 'P',
							
						},success : function(data, textStatus, jqXHR) {
					         var result = data.resultCode;
					         	
								if(result == "0000") {

									$(".certNum").css('display','block');											
								
									}else if (result == "0002") { 
									$.alert("알 수 없는 오류입니다. 관리자에게 문의해주세요.", function(a) {
				
								});	
							}  
						},
						error : function(xhRequest, ErrorText, thrownError) {
							
						}
					});
	
					 	
			 }else{

				/* $.alert("취소",function(a){
				}); */
			}
		});
	}
		
	




function findID(findId){


	document.location.href ="/carrier/sign-Up.do?&findId="+findId;
		
}


function logout(){
	
	$.confirm("로그아웃 하시겠습니까?",function(a){
		 if(a){
			 homeLoader.show();
			 if(window.Android != null){
					window.Android.stopService("${driverId}");
				}	
			 document.location.href = "/logout.do";	
		 }
	});
	
}    
    
    


function goSingUpPage(){

	document.location.href='/carrier/simpleSign-Up.do?&customerId='+selectedObj.customerId+'&chargeId='+selectedObj.chargeId+'&phoneNum='+selectedObj.phoneNum;
	
}

 
function backKeyController(str){
	//history.go(-1);
	//location.href = document.referrer;
	
	homeLoader.show();
	
	document.location.href='/carrier/allocation-detail.do?&allocationStatus=F&allocationId='+"${allocationMap.allocation_id}";

	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
	
	
}

function goChagnePassword(findId){

	document.location.href ="/carrier/sign-Up.do?&findId="+findId;
	
}

function goSignUpPage(){


	document.location.href='/carrier/sign-Up.do';
	
}

function goMileagePage(){

	homeLoader.show();
	document.location.href='/carrier/mileage-page.do?&mileageStatus=U';

	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
	
}

function goBackIcon(allocationId){


	homeLoader.show();
	document.location.href="/carrier/allocation-detail.do?&allocationStatus=F&allocationId="+allocationId;

	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
	
	
}



function goReviewPage(){
	
		if(!$(':input:radio[name=servicePoints]:checked').val()){   
			  $.alert("탁송 서비스를 평가해주세요.");
			   return false;
			   
			}
		
		 if($("#reviewText").val() == ""){
			 $.alert("내용을 입력해주세요.");
			   return false;
			}



			$('reviewForm').submit();

}




</script> 


        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div class="content-container" style=" height:100%; ">
            <%-- <jsp:include page="/WEB-INF/views/jsp/common-top.jsp"></jsp:include> --%>
            <div class="animsition">
            <header class="bg-pink clearfix" style="position:fixed; background-color:#fff; border:none; text-align:center; z-index :2000;">
                <div class="" style="width:19%;">
                    <a onclick ="goBackIcon('${paramMap.allocationId}');"><img style="zoom: 0.5;" src="/img/back-icon.png" alt=""></a> 
                </div> 
                <div class="" style="width:60%;">
                	<img style="width:90%; height:60%;" onclick="javascript:homeLoader.show(); document.location.href='/carrier/main.do'" src="/img/main_logo.png" alt="">
                </div>
                
               <div class="" style="width:19%; float:right;">
                  <a style="cursor:pointer;" href="javascript:logout();"><img style="" src="/img/logout.png" alt=""></a>
                  
                </div> 
            </header>
            <%-- <div style="text-align:right;">${user.driver_name}님 환영 합니다.</div> --%>
            
		        
            <div style=" text-align:right; clear:both;">&nbsp;</div>
            <br>
	            
                
            <div class="content-container interest-page loadingthemepage news" style="background-color:#fff; clear:both; margin-top:50px; overflow-y:scroll;  ">
	            <!-- <div class="cat-list" style="background-color:#fff">
	                <ul class="clearfix" style="background-color:#fff">
	                	<li><a style="color:#000; width:15%;">출발일</a></li>
	                    <li><a style="color:#000;">상차지</a></li>
	                    <li><a style="color:#000;">하차지</a></li>
	                    <li><a style="color:#000;">차종</a></li>
	                    <li><a style="color:#000;">차대번호</a></li>
	                </ul>
	            </div> -->
	      	 <div style="width:100%; margin-top:-5%; height:40px; border-bottom:0px solid #eee; " >
                
	            <div style="width:100%; margin-top:0%; height:40px; border-bottom:0px solid #eee; text-align:center;">
	               	<span class="" style="font-weight: bold; margin-top: 5%;  color:#00000; width:40%;">${userMap.customer_name}님의 <strong>리뷰페이지</strong></span>
                  	<div style="width:100%; text-align:center; height:40px; float:left; margin-top:-5%; ">
                  	</div>
              	</div>
              	
           	
            </div>
            
            
  	 <!--     <div id="container" class="search-result news-list content-box" style="height:100%; margin-top:21%; overflow-y:scroll;">
            		<div class="xpull">
				    <div class="xpull__start-msg">
				        <div class="xpull__start-msg-text">화면을 당겼다 놓으면 새로 고침 됩니다.</div>
				        <div class="xpull__arrow"></divs>
				    </div>
				    <div class="xpull__spinner">
				        <div class="xpull__spinner-circle"></div>
				    </div>
				</div>
            	<div class="news-container clearfix" style="">
    	 
	                
			                </div>
	                
       				 </div> -->
       				 
			 <form name="reviewForm" action="insert-Review.do" method="post" onSubmit="return goReviewPage();">
				 <input type="hidden" name="allocationId" id="allocationId" value="${allocationMap.allocation_id}" >
						<div style=" width:100%; ">
					   		<div style="text-align:left; margin-left:2.5%;">
						    	<span style="color:#F00;">o</span>
									<span class="d-tbc" style="font-weight: bold;">리뷰 등록</span>
							</div> 
							
			               	<div style="clear:both; margin-top:5%;">
		                 	<div class="certNum" style=" width:100%; margin-top:5%;">
		                 		<div style="text-align:center;">
			                 		<div style="text-align:left; margin-left:2.5%;">
										<span class="d-tbc" style="font-weight: bold;">출발일 : ${allocationMap.departure_dt}</span><br>
										<span class="d-tbc" style="font-weight: bold;">상차지 : ${allocationMap.departure}</span><br>
										<span class="d-tbc" style="font-weight: bold;">하차지 : ${allocationMap.arrival}</span><br>
										<span class="d-tbc" style="font-weight: bold;">차종 : ${allocationMap.car_kind}</span><br><br>
										<strong><span class="d-tbc" style="font-weight: bold;"><span style="color:#FF4000;">소중한 고객</span>님의 리뷰는 힘이 됩니다.</span></strong><br>
										<span class="d-tbc" style="font-weight: bold;">(리뷰 작성시 해당 탁송건의 대해 <span style="color:#2E9AFE;">마일리지</span>가 적립됩니다.)</span>
									</div> 
									<div style="clear:both; margin-top:5%; text-align:right; margin-right :10%;  ">
									
									<!-- <span class="d-tbc" style="font-weight: bold;">탁송 평점 :</span>
											<input type="radio" value="" name="allocationPoints" class="allocationPoints1" id="check_All">
											<input type="radio" value="" name="allocationPoints" class="allocationPoints2">
											<input type="radio" value="" name="allocationPoints" class="allocationPoints3">
											<input type="radio" value="" name="allocationPoints" class="allocationPoints4">
											<input type="radio" value="" name="allocationPoints" class="allocationPoints5">
											
											<br> -->
											
											
										<span class="d-tbc" style="font-weight: bold; ">한국카캐리어(주) 탁송 서비스를 평가해주세요!</span>
											<br><br>
											
												<div style="margin-bottom:-2%;">
													<span class="d-tbc" style="font-weight: bold; ">&nbsp;&nbsp;1&nbsp;&nbsp;2&nbsp;&nbsp;3&nbsp;&nbsp;4&nbsp;&nbsp;5</span>
											    </div>
													<img style="width:13%; " src="/img/hkicon2.png" alt="">
													<span class="d-tbc" style="font-weight: bold; ">탁송 서비스 평점 :</span>
													<input type="radio" value="1" name="servicePoints">
													<input type="radio" value="2" name="servicePoints">
													<input type="radio" value="3" name="servicePoints">
													<input type="radio" value="4" name="servicePoints">
													<input type="radio" value="5" name="servicePoints">
												
								   </div>	 
			         			
			         				
			         			<textarea id ="reviewText" style="width:95%; margin-top:5%; "rows="12" cols="30"  maxlength ="100" name="reviewText" placeholder="내용을 입력 해 주세요 최대 (100자)"></textarea>
			         			</div>
			      			</div>
						</div>
								
							
							<div id ="btn_group" style ="margin-top:10%; text-align:center; margin-bottom:10%; ">
								<input type="hidden" style="width :0px; ">
								<button  class="btn_select" style="width:25%; " >리뷰 등록</button>
							</div>
					
			
							</div>	
						</form>	
		           </div>
		            
         	</div>
           

        
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      
    <script src="/js/vendor/bootstrap.min.js"></script>       
    
    <!-- veiwport for countnumber -->
    <script src="/js/alert.js"></script>
    
    <script src="/js/main.js"></script>   
    <script src="/js/vendor/xpull.js"></script>       
    <script src="/js/jquery.bxslider.js"></script>
    <script src="/js/vendor/jquery.loading-indicator.min.js"></script>
    <script src="/js/animsition.min.js"></script>
	<script src="/js/Chart.js"></script>
    
    <script>
    
	
	$('#container').xpull({
	    'paused': false, 
	    // Pull threshold - amount in  pixels required to pull to enable release callback
        'pullThreshold':50,
        // Max pull down element - amount in pixels
        maxPullThreshold: 50,
        // timeout in miliseconds after which the loading indicator stops spinning.
        // If set to 0 - the loading will be indefinite
        'spinnerTimeout':1000,
        onPullStart: function(){},
        onPullEnd: function(){},
        callback: function(){window.location.reload()},

	});   
	
	
	var homeLoader;
	
	$(document).ready(function(){
			
		
				homeLoader = $('body').loadingIndicator({
					useImage: false,
					showOnInit : false
				}).data("loadingIndicator");
				
				/* setTimeout(function() {
					homeLoader.show();
					homeLoader.hide();
				}, 1000); */



				
			});



	$("#check_All").click(function(){

		if($("#check_All").is(":checked")){
			
			$("input[name=allocationPoints]").prop("checked",true);
			

		}else{
			$(".allocationPoints").prop("checked",false);

			}

		});
	
	

    
    </script>
        
    </body>
</html>
