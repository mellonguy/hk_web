<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko"  style=" height:100%;">      
<link rel="stylesheet" href="/css/bootstrap-datepicker.css">
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
    
<body style=" height:100%;">


<script type="text/javascript">  

$(document).ready(function(){
	
	var updateToken = setInterval( function() {
		clearInterval(updateToken);
		pageMoveStop = false;
		homeLoader.hide();

}, 300);
	
});



function getDeviceToken(){
	
	var token = "";
	try{
		if(window.Android != null){
			token = window.Android.getDeviceToken("${driverId}");
		}	
	}catch(exception){
		
	}finally{
		
	}
	return token;
}  
  

function updateDriverDeviceToken(token){
	
	
	if(token != ""){
		$.ajax({ 
			type: 'post' ,
			url : "/updateDriverDeviceToken.do" ,
			dataType : 'json' ,
			data : {
				token : token
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					//$.alert("성공",function(a){
					//});
				}else if(result == "E000"){
					
				}else{
					
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
}  



 function getNewsData(obj,keyword){
	
	 
	// alert($("#selectedStatus").children().length);
	 
	//alert($("#selectedStatus").children().first().get(0));	
	//alert($(obj).parent().get(0));
	
 	 if($("#selectedStatus").children().first().get(0) == $(obj).parent().get(0)){				//전체를 클릭 한경우
					
 		$("#selectedStatus").children().each(function(index,element){
 			$(this).removeClass('selected');
 		});
 		$(obj).parent().addClass('selected');
	}else{
			
	} 
	
	
	
/* 	
	
	$.ajax({ 
		type: 'post' ,
		url : "/research/newsSearch.do?&keyword="+keyword ,
		dataType : 'json' ,
		data : {
			
		},
		success : function(data, textStatus, jqXHR)
		{
			alert(data);
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
	 */
	
	
} 

 function goNewsPage(uri){
		
		//var replaceUrlStr = replaceAll(url, "?", "^");
		//replaceUrlStr = replaceAll(replaceUrlStr, "=", "+");
		
		var replaceUrlStr = encodeURIComponent(uri)
		document.location.href = "/news/newsPage.do?newsUri="+replaceUrlStr;
	}  


    
function viewAllocationData(allocationId,allocationStatus){
	document.location.href = "/carrier/allocation-detail.do?allocationStatus="+allocationStatus+"&allocationId="+allocationId;
} 
 
 
function viewAllocationList(allocationId,allocationStatus){
	
	document.location.href = "/carrier/group-list-finish.do?allocationId="+allocationId+"&allocationStatus="+allocationStatus;
	
} 
 
 
 
function logout(){
	
	$.confirm("로그아웃 하시겠습니까?",function(a){
		 if(a){
			 if(window.Android != null){
					window.Android.stopService("${driverId}");
				}	
				 document.location.href = "/logout.do";	
		 }
	});
	
}   
 
 


function backKeyController(str){
	
	homeLoader.show();
	history.go(-1);

	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
	//document.location.href="/carrier/main.do";
}
 
 
 
function selectTypeChange(obj){
	
	if($("#selectType").val() == "M"){
		$("#selectType").val("D")
		$(obj).html("선택일");
	}else{
		$("#selectType").val("M")
		$(obj).html("월전체");
	}
	
} 


function goList(month){

	homeLoader.show();
	document.location.href = "/carrier/cal-list.do?&searchWord="+$('#forYear').val()+"-"+month;

	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
	

//document.location.href = "/carrier/cal-list.do?&customerId="+customerId;
	
}

function goSearchYearType(){
	
	homeLoader.show();
	document.location.href ="/carrier/finish-list.do?&yearType="+$('#forYear').val();
	
	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
	
	
}

</script>


        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div class="content-container" style=" height:auto; overflow-y:hidden;">
            <%-- <jsp:include page="/WEB-INF/views/jsp/common-top.jsp"></jsp:include> --%>
            <header class="bg-pink clearfix" style="position:fixed; background-color:#fff; border:none; text-align:center; z-index:10000;">
                <div class="" style="width:19%;">
                    <a onclick="javascript:homeLoader.show(); history.go(-1); homeLoader.hide();"><img style="zoom: 0.5;" src="/img/back-icon.png" alt=""></a> 
                </div> 
                <div class="" style="width:60%;">
                	<!-- <img style="width:90%; height:60%;" src="/img/main_logo.png" alt=""> -->
                	<img style="width:90%; height:60%;" onclick="javascript:homeLoader.show(); document.location.href='/carrier/main.do' homeLoader.hide();" src="/img/main_logo.png" alt="">
                </div>
                <div class="" style="width:19%; float:right;">
                   <a style="cursor:pointer;" href="javascript:logout();"><img style="" src="/img/logout.png" alt=""></a>
                </div>
                
            </header>
            <%-- <div style="text-align:right;">${user.driver_name}님 환영 합니다.</div> --%>
            <div style=" text-align:right; clear:both;">&nbsp;</div>
            <div class="content-container interest-page loadingthemepage news" style="position:fixed; background-color:#fff; clear:both; margin-top:50px;">
            	<div class="span5 col-md-5" id="sandbox-container" style="width:100%; height:20%;">
	            	 <div class="input-append date" style="">
						<div style="display:inline; width:100%;">년도 조회</div>
						<div style="display:inline; width:100%;">:&nbsp;&nbsp;&nbsp;		 
							<select name ="forYear" id ="forYear" style="width:40%; padding:2%;">
									<option value="" selected>년도</option>
									<c:forEach var="data" items="${yearList}" varStatus="status">
										<option value="${data.yearType}" <c:if test='${paramMap.yearType eq data.yearType}'>selected</c:if>> ${data.yearType}</option>
									</c:forEach>
							</select> 
						</div>
					<div class="menu-container" style="display:inline; width:60%; text-align:right; clear:both;">
						
							<a class="kakao txt-bold" style="width:80%; display:inline; text-align:right; line-height:32px; cursor:pointer; margin-top:0px; margin-bottom:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:goSearchYearType($('#forYear').val());">
			                 		<img style=" width:6%; height:6%;" src="/img/magnifier.png" />
			                 		 조회 
			            	</a>
			            	<input id="selectType" type="hidden" value="${paramMap.selectType}">
						</div>
					</div> 
				</div>
	      <!--       <div style="width:100%; margin-top:5px; height:40px; border-bottom:1px solid #eee;">
                   <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">출발일</div></div>
	               <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">상차지</div></div>
	               <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">하차지</div></div>
	               <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">차종</div></div>
	               <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">차대번호</div></div>
                </div> -->
            </div>
            <div id="container" class="search-result news-list content-box" style="height:100%; margin-top:30%; overflow-y:scroll;">
            	<div class="xpull">
				    <div class="xpull__start-msg">
				        <div class="xpull__start-msg-text">화면을 당겼다 놓으면 새로 고침 됩니다.</div>
				        <div class="xpull__arrow"></div>
				    </div>
				    <div class="xpull__spinner">
				        <div class="xpull__spinner-circle"></div>
				    </div>
				</div>
            	<div class="news-container clearfix" style="height:100%;">
            	        <div class="main-menu-container" style="margin-top:9%; height:100%; overflow-y:hidden;">
            	        
    	<%--        		<c:forEach var="data" items="${listData}" varStatus="status">
	    	       		<c:if test="${data.groupCarCnt == 1}">
			           		<div style="width:100%; margin-top:5px; height:40px; border-bottom:1px solid #eee;"  onclick="javascript:viewAllocationData('${data.allocation_id}','${data.allocation_status_cd}');">
				                <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">${data.departure_dt}</div></div>
					            <div style="width:20%; text-align:center; height:40px; float:left;"><div style="width:100%; margin-top:10px; text-align:center; display:inline-block; text-overflow:ellipsis; white-space:nowrap; overflow:hidden;"><nobr>${data.departure}</nobr></div></div>
					            <div style="width:20%; text-align:center; height:40px; float:left;"><div style="width:100%; margin-top:10px; text-align:center; display:inline-block; text-overflow:ellipsis; white-space:nowrap; overflow:hidden;"><nobr>${data.arrival}</nobr></div></div>
					            <div style="width:20%; text-align:center; height:40px; float:left;"><div style="width:100%; margin-top:10px; text-align:center; display:inline-block; text-overflow:ellipsis; white-space:nowrap; overflow:hidden;"><nobr>${data.car_kind}</nobr></div></div>
					            <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">${data.car_id_num}</div></div>
			                </div>
			             </c:if>
			              <c:if test="${data.groupCarCnt > 1}">
			           		<div style="width:100%; margin-top:5px; height:40px; border-bottom:1px solid #eee;"  onclick="javascript:viewAllocationList('${data.allocation_id}','${data.allocation_status_cd}');">
				                <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">${data.departure_dt}</div></div>
				                <div style="width:20%; text-align:center; height:40px; float:left;"><div style="width:100%; margin-top:10px; text-align:center; display:inline-block; text-overflow:ellipsis; white-space:nowrap; overflow:hidden;"><nobr>${data.departure}</nobr></div></div>
					            <div style="width:20%; text-align:center; height:40px; float:left;"><div style="width:100%; margin-top:10px; text-align:center; display:inline-block; text-overflow:ellipsis; white-space:nowrap; overflow:hidden;"><nobr>${data.arrival}</nobr></div></div>
					            <div style="width:80%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">${data.driver_cnt}회전</div></div>
			                </div>
			             </c:if>
	                </c:forEach> --%>
	                	<div style="margin-top:0%; height:60%;">
			
		
              <a href="javascript:goList('01');" class="menu-link " style=" width:31%; display:inline-block; ">
                    <!-- <img src="/img/calender1.png" style ="width:60%; height:70%;"alt=""> -->
                    <span class="text">1월  </span>
                </a>
                
                <a href="javascript:goList('02');" class="menu-link"style=" width:31%; display:inline-block; ">
                  <!--   <img src="/img/calender1.png" alt="" style ="width:60%; height:100%;"> -->
                    <span class="text">2월</span>
                </a>
                
                <a href="javascript:goList('03');" class="menu-link" style="width:31%; display:inline-block; ">
                 <!--    <img src="/img/calender1.png" alt="" style ="width:60%; height:100%;"> -->
                    <span class="text">3월</span>
                </a>
                
                  <a href="javascript:goList('04');" class="menu-link" style=" width:31%; display:inline-block; ">
                    <!-- <img src="/img/calender1.png" alt="" style ="width:60%; height:70%;"> -->
                    <span class="text">4월</span>
                </a>
                  <a href="javascript:goList('05');" class="menu-link" style="width:31%; display:inline-block; ">
                    <!-- <img src="/img/calender1.png" alt="" style ="width:60%; height:70%;"> -->
                    <span class="text">5월</span>
                </a>
                  <a href="javascript:goList('06');" class="menu-link" style="width:31%; display:inline-block; ">
                    <!-- <img src="/img/calender1.png" alt="" style ="width:60%; height:70%;"> -->
                    <span class="text">6월</span>
                </a>
                  <a href="javascript:goList('07');" class="menu-link" style="width:31%; display:inline-block; ">
                   <!--  <img src="/img/calender1.png" alt="" style ="width:60%; height:70%;"> -->
                    <span class="text">7월 </span>
                </a>
                  <a href="javascript:goList('08');" class="menu-link" style="width:31%; display:inline-block; ">
                    <!-- <img src="/img/calender1.png" alt="" style ="width:60%; height:70%;"> -->
                    <span class="text">8월</span>
                </a>
                  <a href="javascript:goList('09');" class="menu-link" style="width:31%; display:inline-block; ">
                    <!-- <img src="/img/calender1.png" alt="" style ="width:60%; height:70%;"> -->
                    <span class="text">9월</span>
                </a>
                  <a href="javascript:goList('10');" class="menu-link" style="width:31%; display:inline-block; ">
                    <!-- <img src="/img/calender1.png" alt="" style ="width:60%; height:70%;"> -->
                    <span class="text">10월</span>
                </a>
                 <a href="javascript:goList('11');" class="menu-link" style="width:31%; display:inline-block; ">
                    <!-- <img src="/img/calender1.png" alt="" style ="width:60%; height:70%;"> -->
                    <span class="text">11월</span>
                </a>
                 <a href="javascript:goList('12');" class="menu-link" style="width:31%; display:inline-block; ">
                    <!-- <img src="/img/calender1.png" alt="" style ="width:60%; height:70%;"> -->
                    <span class="text">12월</span>
                </a>
                
                 
                </div> 
                 </div>
                 </div>
                 
                 
            </div>
        </div>



    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      
    <script src="/js/vendor/bootstrap.min.js"></script>       
    <!-- veiwport for countnumber -->
    <script src="/js/alert.js"></script>
    <script src="/js/main.js"></script>   
    <script src="/js/vendor/xpull.js"></script>
    <script src="/js/vendor/bootstrap-datepicker.js"></script>       
    <script src="/js/vendor/jquery.loading-indicator.min.js"></script>
    
    <!--  -->
    
    <script>
			 var homeLoader;
    
    $(document).ready(function(){
    
    	homeLoader = $('body').loadingIndicator({
			useImage: false,
			showOnInit : false
		}).data("loadingIndicator");
	
    	homeLoader.show();
    	
    });
	
	$('#container').xpull({
	    'paused': false, 
	    // Pull threshold - amount in  pixels required to pull to enable release callback
        'pullThreshold':50,
        // Max pull down element - amount in pixels
        maxPullThreshold: 50,
        // timeout in miliseconds after which the loading indicator stops spinning.
        // If set to 0 - the loading will be indefinite
        'spinnerTimeout':1000,
        onPullStart: function(){},
        onPullEnd: function(){},
        callback: function(){window.location.reload()},

	});   
	
	$('#datepick').datepicker({
		format: "yyyy-mm-dd",
	    clearBtn: true,
	    autoclose: true,
	    language: "kr",
	    todayHighlight: true
	}).on('changeDate', function (selected) {
	   var selectedDate = selected.format(0,"yyyy-mm-dd");
	   //alert($("#selectType").val());
	   document.location.href = "/carrier/finish-list.do?allocationStatus=F&startDt="+selectedDate+"&selectType="+$("#selectType").val();
	   
	});
    
    </script>
        
    </body>
</html>
