<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.util.Date" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko"  style=" height:100%;">      
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
<body style=" height:100%;">


<script type="text/javascript">  
var pageMoveStop = true;
$(document).ready(function(){  
	
/* 	var updateToken = setInterval( function() {
			clearInterval(updateToken);
			pageMoveStop = false;
			homeLoader.hide();
	
   }, 300); */
	
	
});



function getDeviceToken(){
	
	var token = "";
	try{
		if(window.Android != null){
			token = window.Android.getDeviceToken("${driverId}");
		}	
	}catch(exception){
		
	}finally{
		
	}
	return token;
}  
  

/* function updateDriverDeviceToken(token){
	
	
	if(token != ""){
		$.ajax({ 
			type: 'post' ,
			url : "/updateDriverDeviceToken.do" ,
			dataType : 'json' ,
			data : {
				token : token
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					//$.alert("성공",function(a){
					//});
				}else if(result == "E000"){
					
				}else{
					
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
}   

*/



 function getNewsData(obj,keyword){
	
	 
	// alert($("#selectedStatus").children().length);
	 
	//alert($("#selectedStatus").children().first().get(0));	
	//alert($(obj).parent().get(0));
	
 	 if($("#selectedStatus").children().first().get(0) == $(obj).parent().get(0)){				//전체를 클릭 한경우
					
 		$("#selectedStatus").children().each(function(index,element){
 			$(this).removeClass('selected');
 		});
 		$(obj).parent().addClass('selected');
	}else{
			
	} 
	
	
	
/* 	
	
	$.ajax({ 
		type: 'post' ,
		url : "/research/newsSearch.do?&keyword="+keyword ,
		dataType : 'json' ,
		data : {
			
		},
		success : function(data, textStatus, jqXHR)
		{
			alert(data);
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
	 */
	
	
} 

 function goNewsPage(uri){
		
		//var replaceUrlStr = replaceAll(url, "?", "^");
		//replaceUrlStr = replaceAll(replaceUrlStr, "=", "+");
		
		var replaceUrlStr = encodeURIComponent(uri)
		document.location.href = "/news/newsPage.do?newsUri="+replaceUrlStr;
	}  


    
function viewAllocationData(allocationId){
	
	homeLoader.show();
	document.location.href = "http://52.78.153.148:8080/files/allocation${paramMap.allocation_file_path}";

	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
} 
 
 
function viewAllocationList(allocationId,allocationStatus){
	homeLoader.show();
	document.location.href = "/carrier/group-list.do?allocationId="+allocationId+"&allocationStatus="+allocationStatus;

	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
	
} 
 
 
 
function logout(){
	
	$.confirm("로그아웃 하시겠습니까?",function(a){
		 if(a){
			 homeLoader.show();
			 if(window.Android != null){
					window.Android.stopService("${driverId}");
				}	
				 document.location.href = "/logout.do";	

				var updateHomeLoader = setInterval(function() {
					clearInterval(updateHomeLoader);
					homeLoader.hide();
				}, 300);
		 }
	});
	
}   
 
 
 function downloadDriverDeductInfo(selectMonth){
	 
	 document.location.href = "/carrier/downloadDriverDeductInfo.do?selectMonth="+selectMonth;
	 
 }
 
	function bill(selectMonth){
		
	 document.location.href = "/carrier/viewBillInfo.do?selectMonth="+selectMonth;
	 
 }
 
 
 
 
 function downloadExcel(selectMonth){
	 
	if("${user.driver_kind}" != "00" || "${user.driver_id}" == "sourcream"){
		homeLoader.show();
		document.location.href = "/carrier/excel_download.do?driverId=${user.driver_id}&selectMonth="+selectMonth;
		if(window.Android == null){
			homeLoader.hide();
		}
		 
	}else{
		$.alert("준비중 입니다.",function(a){
		});
	}
	 
	 
	 
	 
 }
 
 
 function insertDriverDecideStatus(selectMonth){
	 
	 
	 
	 $.confirm(selectMonth+"월의 리스트를 확정 하시겠습니까?",function(a){
		 if(a){
			 
			 $.ajax({ 
					type: 'post' ,
					url : "/carrier/insertDriverDecideStatus.do" ,
					dataType : 'json' ,
					data : {
						decideMonth : selectMonth
					},
					success : function(data, textStatus, jqXHR)
					{
						var result = data.resultCode;
						var resultData = data.resultData;
						if(result == "0000"){
							$.alert("확정 되었습니다.",function(a){
								window.location.reload();
							});
						}else if(result == "E000"){
							$.alert("로그인 정보를 찾을 수 없습니다.",function(a){
								document.location.href = "/logout.do";
							});
						}else{
							$.alert("확정 하는데 실패 했습니다. 관리자에게 문의 하세요.",function(a){
								
							});
						}
					} ,
					error : function(xhRequest, ErrorText, thrownError) {
					}
				});
			 
			 
		 }
	});
	 

	 
	 
 }
 
 
 
 
function backKeyController(str){
	history.go(-1);
	//location.href = document.referrer;
	//document.location.href='/carrier/main.do';
	
	//document.location.href='/carrier/main.do';
	
}
 
 
function fileDownloadDone(str){
	
	homeLoader.hide();
	$.alert("내파일 -> Download -> "+str+" 다운로드가 완료 되었습니다.",function(a){
	});
	
} 
 

var startTime = null;
var endTime = null; 

function goImgPage(filePath,obj){


	if(endTime-startTime < 500){
		document.location.href  = "/carrier/image-view.do?src=http://52.78.153.148:8080/files/allocation"+filePath;
		var updateHomeLoader = setInterval(function() {
			clearInterval(updateHomeLoader);
			homeLoader.hide();
		}, 300);
	}else{
		$(obj).removeClass("del");
		$(obj).css("border","none");	
	}



}



function goAllocationInfo(allocationId){

	homeLoader.show();
	 document.location.href="/carrier/allocation-detail.do?allocationStatus=F&allocationId="+allocationId; 

		var updateHomeLoader = setInterval(function() {
			clearInterval(updateHomeLoader);
			homeLoader.hide();
		}, 300);


	}

</script>


        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div class="content-container" style=" height:auto; overflow-y:hidden;">
            <%-- <jsp:include page="/WEB-INF/views/jsp/common-top.jsp"></jsp:include> --%>
            <header class="bg-pink clearfix" style="position:fixed; background-color:#fff; border:none; text-align:center; z-index:10000;">
                <div class="" style="width:19%;">
                    <a href="javascript:history.go(-1);"><img style="zoom: 0.5;" src="/img/back-icon.png" alt=""></a> 
                </div> 
                <div class="" style="width:60%;">
                	<img style="width:90%; height:60%;" onclick="javascript:homeLoader.show(); document.location.href='/carrier/main.do'" src="/img/main_logo.png" alt="">
                </div>
                <div class="" style="width:19%; float:right;">
                   <a style="cursor:pointer;" href="javascript:logout();"><img style="" src="/img/logout.png" alt=""></a>
                </div>
            </header>
            <%-- <div style="text-align:right;">${user.driver_name}님 환영 합니다.</div> --%>
            <div style=" text-align:right; clear:both;">&nbsp;</div>
            <div class="content-container interest-page loadingthemepage news" style="position:fixed; background-color:#fff; clear:both; margin-top:35px; z-index:10000;">
	          <br>
	                    <div class="input-append date" style="width:100%;">
						<div style="display:inline; width:15%;">&nbsp;&nbsp;인수증&nbsp;검색</div>
						<div style="display:inline; width:40%;">:&nbsp;&nbsp;<input style="width:45%; text-align:center;"  type="text" class="span2" id="datepick" value="${paramMap.searchType}"><span class="add-on"><i class="icon-th"></i></span></div>
						<div class="menu-container" style="display:inline; width:40%; text-align:right; clear:both;  padding:0;">
							
							<a class="kakao txt-bold" style="width:19%; display:inline; text-align:right; line-height:32px; cursor:pointer; margin-top:0px; margin-bottom:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:selectTypeChange(this);">
			                 <img style=" width:6%; height:6%;" src="/img/magnifier.png" />
			                 	 조회
			            	</a>
			            	<input id="selectType" type="hidden" value="${paramMap.searchType}">
						</div>
					</div> 
	 
	          <div style=" text-align:left; clear:both; color: #00f;">&nbsp;※&nbsp;기사님 핸드폰기종에 따라 인수증의 크기가 달라집니다.</div>
	          
	            <!-- <div class="cat-list" style="background-color:#fff">
	                <ul class="clearfix" style="background-color:#fff">
	                	<li><a style="color:#000; width:15%;">출발일</a></li>
	                    <li><a style="color:#000;">상차지</a></li>
	                    <li><a style="color:#000;">하차지</a></li>
	                    <li><a style="color:#000;">차종</a></li>
	                    <li><a style="color:#000;">차대번호</a></li>
	                </ul>
	            </div> -->
	            <div style="width:100%; margin-top:5%; height:40px; border-bottom:1px solid #eee;">
                   <div style="width:20%;  text-align:center; height:40px; float:left;"><div style="margin-top:-20px; text-align:center; display:inline-block;">차대번호</div></div>
	               <div style="width:20%;  text-align:center; height:40px; float:left;"><div style="margin-top:-20px; text-align:center; display:inline-block;">상차지</div></div>
	               <div style="width:20%;  text-align:center; height:40px; float:left;"><div style="margin-top:-20px; text-align:center; display:inline-block;">하차지</div></div>
	               <div style="width:18%;  text-align:center; height:40px; float:left;"><div style="margin-top:-20px; text-align:center; display:inline-block;">차종</div></div>
	               <div style="width:22%;  text-align:center; height:40px; float:left;"><div style="margin-top:-20px; text-align:center; display:inline-block;">인수증</div></div>
                </div>
            </div>
            <div id="container" class="search-result news-list content-box" style="height:100%; margin-top:21%; overflow-y:scroll;">
            	<!-- <div class="xpull">
				    <div class="xpull__start-msg">
				        <div class="xpull__start-msg-text">화면을 당겼다 놓으면 새로 고침 됩니다.</div>
				        <div class="xpull__arrow"></div>
				    </div>
				    <div class="xpull__spinner">
				        <div class="xpull__spinner-circle"></div>
				    </div>
				</div> -->
            	<div class="news-container clearfix" style="height:auto; margin-top:-8%;">
    	       		<c:forEach var="data" items="${listData}" varStatus="status">
			           		<div style="width:100%; margin-top:10px; height:40px; border-bottom:1px solid #eee;" >
				                <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">
				                
				                <c:if test ="${data.car_id_num eq ''}">
				                -
				                </c:if>
				                <c:if test ="${data.car_id_num ne''}">
				                ${data.car_id_num}
				                </c:if>
				                
				                
				             	</div>
				                
				                </div>
						                <div id ="insuClick">
							            <div style="width:20%; text-align:center; height:40px; float:left;" onclick ="javascriipt:goAllocationInfo('${data.allocation_id}');"><div style="width:100%; margin-top:10px; text-align:center; display:inline-block; text-overflow:ellipsis; white-space:nowrap; overflow:hidden;"><nobr>${data.departure}</nobr></div></div>
							            <div style="width:20%; text-align:center; height:40px; float:left;" onclick ="javascriipt:goAllocationInfo('${data.allocation_id}');"><div style="width:100%; margin-top:10px; text-align:center; display:inline-block; text-overflow:ellipsis; white-space:nowrap; overflow:hidden;"><nobr>${data.arrival}</nobr></div></div>
							            <div style="width:20%; text-align:center; height:40px; float:left;" onclick ="javascriipt:goAllocationInfo('${data.allocation_id}');"><div style="width:100%; margin-top:10px; text-align:center; display:inline-block; text-overflow:ellipsis; white-space:nowrap; overflow:hidden;"><nobr>${data.car_kind}</nobr></div></div>
							          
							<%--    	<c:if test ="${data.file_allocation_id != '' || data.file_allocation_id ne null}"> --%>
							        <div style="width:20%; text-align:center; height:40px; float:left;" onclick ="javascript:homeLoader.show();goImgPage('${data.allocation_file_path}',this);">
							        	<div style="margin-top:-5px; text-align:center; display:inline-block;">
					                   			<a class=""  id="" style="width:100%; margin-top:10px; text-align:center; display:inline-block; text-overflow:ellipsis; white-space:nowrap; overflow:hidden;">
									      				<img style=" width:25%; height:58%;" src="/img/magnifier.png"  />보기
							         			 </a> 
					      				<%-- <img id="imgpage"style="width:70%; height:60%; margin-left:15%; padding:0;"  src="http://52.78.153.148:8080/files/allocation${data.allocation_file_path}"  title="${data.allocation_file_nm}"  onclick ="javascript:goImgPage('${data.allocation_file_id}',this);" /> --%> 
					            	</div>
					            </div>
			<%-- 		         </c:if> --%>
					     <%--           <c:if test ="${data.file_allocation_id == '' || data.file_allocation_id eq null}">
							        <div style="width:20%; text-align:center; height:40px; float:left;" >
							        	<div style="margin-top:-5px; text-align:center; display:inline-block;">
					                   			-
					      				<img id="imgpage"style="width:70%; height:60%; margin-left:15%; padding:0;"  src="http://52.78.153.148:8080/files/allocation${data.allocation_file_path}"  title="${data.allocation_file_nm}"  onclick ="javascript:goImgPage('${data.allocation_file_id}',this);" /> 
					            	</div>
					            </div>
					         </c:if> --%>
					            
					            
					        </div>
			             </div>
	                </c:forEach>
                 </div>
            </div>
            
            
            <%-- <c:if test="${decideFinalMap != null}"> --%>
	            <div class="menu-container" style="width:100%; margin-top:0px; text-align:center; clear:both;">
	         
	         	 <%--<a class="kakao txt-bold" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:downloadExcel('${paramMap.decideMonth}');">
						엑셀다운로드
				    </a>
				     --%>
				
				    <jsp:useBean id="now" class="java.util.Date" />
				    <%-- <fmt:formatDate value="${now}" pattern="yyyyMMddHHmm" var="nowDate" /> --%>
				    <fmt:formatDate value="${now}" pattern="ddHHmm" var="nowDate" />
				    <fmt:formatDate value="${now}" pattern="yyyy-MM" var="nowMonth" />
				    <c:set var="monthago" value="<%=new Date(new Date().getTime() - 60*60*24*1000*24)%>"/>
					<fmt:formatDate value="${monthago}" pattern="yyyy-MM" var="monthago"/>
				    <c:if test="${120019 < nowDate && 170000 > nowDate}">
				    <%-- <c:if test="${091558 < nowDate && 170000 > nowDate}"> --%>
						<c:if test="${paramMap.decideMonth != nowMonth && paramMap.decideMonth == monthago}">    
						    <c:if test="${driverDecideStatus == null}">
							    <a class="kakao txt-bold" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:insertDriverDecideStatus('${paramMap.decideMonth}');">
									정산 확정
								</a>
						    </c:if>
					    </c:if>
				    </c:if>
				    <c:if test="${driverDeductFile != null && driverDeductFile.driver_view_flag == 'Y'}">
		        		<a class="kakao txt-bold" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:downloadDriverDeductInfo('${paramMap.decideMonth}');">
							정산내역서
					    </a>
				    </c:if>
				    <%-- <c:if test="${driverDeductFile != null && driverDeductFile.driver_view_flag == 'Y' && paramMap.decideMonth == monthago}"> --%>
				    <c:if test="${driverDeductFile != null && driverDeductFile.driver_view_flag == 'Y' && paramMap.decideMonth != '2019-04' && paramMap.decideMonth != '2019-05' && paramMap.decideMonth != '2019-06'}">
		        		<a class="kakao txt-bold" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:bill('${paramMap.decideMonth}');">
							세금계산서
					    </a>
				    </c:if>
				    
				    
	            </div>
            <%-- </c:if> --%>
            
            <%-- <c:if test="${decideFinalMap == null}">
	            <div class="menu-container" style="width:100%; margin-top:0px; text-align:center; clear:both;">
	            	<a class="kakao txt-bold" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:downloadExcel('${paramMap.decideMonth}');">
						엑셀다운로드
				    </a>
				    <c:if test="${driverDecideStatus == null}">
					    <a class="kakao txt-bold" style="width:30%; display:inline-block; cursor:pointer; margin-top:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:insertDriverDecideStatus('${paramMap.decideMonth}');">
							정산 확정
					    </a>
				    </c:if>
	            </div>
            </c:if> --%>
            
            
        </div>




    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      
    <script src="/js/vendor/bootstrap.min.js"></script>       
    <!-- veiwport for countnumber -->
    <script src="/js/alert.js"></script>
    <script src="/js/main.js"></script>   
    <script src="/js/vendor/xpull.js"></script>       
    <script src="/js/vendor/jquery.loading-indicator.min.js"></script>
    <script src="/js/vendor/bootstrap-datepicker.js"></script>     
    
    <script>
    
 var homeLoader;
    
    $(document).ready(function(){
    
    	homeLoader = $('body').loadingIndicator({
			useImage: false,
			showOnInit : false
		}).data("loadingIndicator");

    	//homeLoader.show();
		
    });
	
/* 	 $('#container').xpull({
	    'paused': false, 
	    // Pull threshold - amount in  pixels required to pull to enable release callback
        'pullThreshold':50,
        // Max pull down element - amount in pixels
        maxPullThreshold: 50,
        // timeout in miliseconds after which the loading indicator stops spinning.
        // If set to 0 - the loading will be indefinite
        'spinnerTimeout':1000,
        onPullStart: function(){},
        onPullEnd: function(){},
        callback: function(){window.location.reload()},

	});  */
	
	$('#datepick').datepicker({
		format: "yyyy-mm-dd",
	    clearBtn: true,
	    autoclose: true,
	    language: "kr",
	    todayHighlight: true
	}).on('changeDate', function (selected) {
	   var selectedDate = selected.format(0,"yyyy-mm-dd");
	   //alert($("#selectType").val());
	   document.location.href = "/carrier/cal-list-detail.do?&searchType="+$("#datepick").val();
	   
	});
    </script>
        
    </body>
</html>
