<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
	
%>

<!DOCTYPE html>
<html lang="ko" style="height: 100%;">
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
<link rel="stylesheet" href="/css/animsition.min.css">
<body style="height: 100%;">


	<script type="text/javascript">  

$(document).ready(function(){

	$('.modal-field').css('display','none');
	$('.modal-field2').css('display','none');
	$('.showTmapDistence').css('display','none');
	
	/*	
	var updateToken = setInterval( function() {
		
 		if(getDeviceToken() != null && getDeviceToken() != ""){
			updateDriverDeviceToken(getDeviceToken());
			if(window.Android != null){
				window.Android.startService();
			}
			clearInterval(updateToken);
		}
   }, 1000);
 
 */

	 $(".animsition").animsition({
		    inClass: 'zoom-in-lg',
		    outClass: 'zoom-out-lg',
		    inDuration: 1500,
		    outDuration: 800,
		    linkElement: '.animsition-link',
		    // e.g. linkElement: 'a:not([target="_blank"]):not([href^="#"])'
		    loading: true,
		    loadingParentElement: 'body', //animsition wrapper element
		    loadingClass: 'animsition-loading',
		    loadingInner: '', // e.g '<img src="loading.svg" />'
		    timeout: false,
		    timeoutCountdown: 5000,
		    onLoadEvent: true,
		    browser: [ 'animation-duration', '-webkit-animation-duration'],
		    // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
		    // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
		    overlay : false,
		    overlayClass : 'animsition-overlay-slide',
		    overlayParentElement : 'body',
		    transition: function(url){ window.location.href = url; }
		  });

	
});



function getDeviceToken(){
	
	var token = "";
	try{
		if(window.Android != null){
			token = window.Android.getDeviceToken("${driverId}");
		}	
	}catch(exception){
		
	}finally{
		
	}
	return token;
}  
  
/* 
function updateDriverDeviceToken(token){
	
	
	if(token != ""){
		$.ajax({ 
			type: 'post' ,
			url : "/updateDriverDeviceToken.do" ,
			dataType : 'json' ,
			data : {
				token : token
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					//$.alert("성공",function(a){
					//});					
				}else{
					
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
}  
 */


 function getNewsData(obj,keyword){
	
	 
	// alert($("#selectedStatus").children().length);
	 
	//alert($("#selectedStatus").children().first().get(0));	
	//alert($(obj).parent().get(0));
	
 	 if($("#selectedStatus").children().first().get(0) == $(obj).parent().get(0)){				//전체를 클릭 한경우
					
 		$("#selectedStatus").children().each(function(index,element){
 			$(this).removeClass('selected');
 		});
 		$(obj).parent().addClass('selected');
	}else{
			
	} 
	
	
	
/* 	
	
	$.ajax({ 
		type: 'post' ,
		url : "/research/newsSearch.do?&keyword="+keyword ,
		dataType : 'json' ,
		data : {
			
		},
		success : function(data, textStatus, jqXHR)
		{
			alert(data);
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
	 */
	
	
} 

 function goNewsPage(uri){
		
		//var replaceUrlStr = replaceAll(url, "?", "^");
		//replaceUrlStr = replaceAll(replaceUrlStr, "=", "+");
		
		var replaceUrlStr = encodeURIComponent(uri)
		document.location.href = "/news/newsPage.do?newsUri="+replaceUrlStr;
	}  


    
function viewAllocationData(allocationId,allocationStatus){
	homeLoader.show();
	document.location.href = "/carrier/allocation-detail.do?allocationStatus="+allocationStatus+"&allocationId="+allocationId;

	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
} 
 
 
function viewAllocationList(allocationId,allocationStatus){
	homeLoader.show();
	document.location.href = "/carrier/group-list.do?allocationId="+allocationId+"&allocationStatus="+allocationStatus;

	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
	
} 
 
 
 
function logout(){
	
	$.confirm("로그아웃 하시겠습니까?",function(a){
		 if(a){
			 if(window.Android != null){
					window.Android.stopService("${driverId}");
				}	
			 homeLoader.show();
			 document.location.href = "/logout.do";	
		 }
	});
	
}   








var selectedObj = new Object();


function selectInputId(){

	if($("#inputID").val() != ""){

		$.ajax({
			type : 'post',
			url : '/carrier/compareCustomerId.do',
			dataType : 'json',
			data : {

				inputID : $("#inputID").val(),
				
			},success : function(data, textStatus, jqXHR) {
		         var result = data.resultCode;
		         	
					if(result == "0000") {

						sendMessage();
					
						}else if (result == "0001") { 
						$.alert("입력한 아이디로 가입된 아이디가 없습니다.", function(a) {
	
					});	
				}  
			},
			error : function(xhRequest, ErrorText, thrownError) {
				
			}
		});


		}else{

			$.alert("아이디가 입력이 되지 않았습니다.");
			


			}
		
}


	selectedObj = new Object(); 

	function sendMessage(){


		selectedObj = new Object();

		
		$.confirm("등록하셨던 번호로 인증번호를 전송하시겠습니까?",function(a){

			 if(a){
					$(".certNum").css('display','block');	
					
					$.ajax({
						type : 'post',
						url : '/carrier/insertCertNumForAPP.do',
						dataType : 'json',
						data : {

							inputID : $("#inputID").val(),
							phoneNum : "",
							customerId : "",
							gubun : 'P',
							
						},success : function(data, textStatus, jqXHR) {
					         var result = data.resultCode;
					         	
								if(result == "0000") {

									$(".certNum").css('display','block');											
								
									}else if (result == "0002") { 
									$.alert("알 수 없는 오류입니다. 관리자에게 문의해주세요.", function(a) {
				
								});	
							}  
						},
						error : function(xhRequest, ErrorText, thrownError) {
							
						}
					});
					
				 
					

					 	
			 }else{

				/* $.alert("취소",function(a){
				}); */
			}
		});
	}
		
	


	
function sendPinNum(){


	var phoneCertNum = $("#phoneCertNum").val();

	
	if(phoneCertNum != ""){

				$.ajax({
					type : 'post',
					url  : "/carrier/checkCertNumForLogin.do",
				    dataType : 'json',
				    
				       data : {
					       
				    	inputID : $("#inputID").val(),
						phoneNum :  "",
						customerId : "",
						phoneCertNum : phoneCertNum,
						gubun : 'P',
				
						},

				   success : function(data, textStatus, jqXHR)
				    {
					   var resultCode = data.resultCode;
					   var resultData = data.resultData;
					   
					   if(resultCode == "0000"){
						 
							$.alert("인증에 성공하였습니다.", function(a) {
								

								
								$(".changePw").css('display','block');		
								
							});	
						 
					   }else{
						 $.alert("인증번호가 맞지 않습니다. 관리자에게 문의하세요.");
					   }
				   } ,
				  error :function(xhRequest, ErrorText, thrownError){
					  $.alert('알수 없는 오류입니다. 관리자에게 문의 하세요.');
	                  
					  }
				});
	
		}else{

			$.alert("인증번호가 입력되지 않았습니다.");
			return false;
			}
}



function findID(findId){


	document.location.href ="/carrier/sign-Up.do?&findId="+findId;
		
}




function goSingUpPage(){

	document.location.href='/carrier/simpleSign-Up.do?&customerId='+selectedObj.customerId+'&chargeId='+selectedObj.chargeId+'&phoneNum='+selectedObj.phoneNum;
	
}

 
function backKeyController(str){
	//history.go(-1);
	//location.href = document.referrer;
	document.location.href='/carrier/main.do';
}

function goChagnePassword(findId){

	document.location.href ="/carrier/sign-Up.do?&findId="+findId;
	
}

function goSignUpPage(){


	document.location.href='/carrier/sign-Up.do';
	
}


function newPasswordChange(){


	var newPassword = $("#newPassword").val();
	var confirmpw = $("#confirmpw").val();

	
	    var num = newPassword.search(/[0-9]/g);
	    var eng = newPassword.search(/[a-z]/ig);
	    var spe = newPassword.search(/[`~!@@#$%^&*|₩₩₩'₩";:₩/?]/gi);

	    

	    if(newPassword.length < 6 || newPassword.length > 20){
	     $.alert("패스워드는 6자리 이상이어야 합니다.");
	     
	     return false;
	    }
	    if(newPassword.search(/₩s/) != -1){
	        $.alert("패스워드는 공백없이 입력해주세요.");
	        return false;
	       }
	    if(num < 0 || eng < 0 || spe < 0 ){
	      $.alert("영문,숫자, 특수문자를 혼합하여 입력해주세요.");
	      return false;
	     }

	    
	 if(newPassword == confirmpw ){


				$.ajax({
					type : 'post',
					url  : "/carrier/confirmForPassWord.do",
				    dataType : 'json',
				    
				       data : {

						userId : $("#inputID").val(),
						newPassword : newPassword,
						
						},

				   success : function(data, textStatus, jqXHR)
				    {
					   var resultCode = data.resultCode;
					   if(resultCode == "0000"){

						   $.alert("비밀번호가 변경되었습니다.",function(a){
								if(a){
									goSignUpPage();
								}
							});
									
						}				

						   },
				  error :function(xhRequest, ErrorText, thrownError){
	                  $.alert(' @@@@error@@@@ ');
	                  
					  }

				});

			}else if(newPassword ==""){
				$.alert("입력창에 입력이 되지 않았습니다.");
		
			}else{
			$.alert("비밀번호가 같지 않습니다. 다시한번 확인해주세요");

				}
		}


var status ="";

function goWindowPopUp(id){


	status = id;

	window.open("/carrier/addressSearch.do", "a", "width=500, height=800, left=100, top=50"); 
	

	
}

function setAddress(value){

	$.alert(value);
	$.alert(status);

	$("#"+status).val(value);


	
}




function goShowModal(obj){

	if($("#searchKeyword").val() == ""){

	}else{

	$('.modal-field').show();
	
		}


	

	
}


function goShowModal2(obj){
	
	if($("#searchKeyword2").val() == ""){

	}else{

	$('.modal-field2').show();
	
		}

}



function goDepartureInit(obj){


	var departureAddressVal =$('#departureAddress').val(); 
	var arrivalAddressVal= $('#arrivalAddress').val(); 

	inputStatus='departure'; 

	$(obj).next().val('');
	$('#searchKeyword').val('');

	$('#showDistence').css('display','none');
	
	 $('#searchResult2').html('');

		if(arrivalAddressVal == ""){
			
				$('#searchKeyword2').val('');
		
			}

 

}


function goArrivalInit(obj){


	var departureAddressVal =$('#departureAddress').val(); 
	var arrivalAddressVal= $('#arrivalAddress').val(); 
	
	inputStatus='arrival'; 
	

	$('#searchKeyword2').val('');
	$(obj).next().val('');

	$('#showDistence').css('display','none');
	
	$('#searchResult').html(''); 

		if(departureAddressVal == ""){
			
			$('#searchKeyword').val('');
	
		}

		
	}


/*
 

function goReservationConsignment(){



	if($("#departureDt").val() == ""){

		$.alert("출발일이 입력 되지 않았습니다.",function(a){
	    });
		return;
	}
	if($("#searchKeyword").val() == ""){

		$.alert("출발지가 입력 되지 않았습니다.",function(a){
	    });
		return;
	}

	if($("#searchKeyword2").val() == ""){

		$.alert("도착지가 입력 되지 않았습니다.",function(a){
	    });
		return;
	}
	
	
	if($.confirm("등록 하시겠습니까?"+"\r\n")){


		$("#insertForm").attr("action","/carrier/insert-allocation.do");
		$("#insertForm").submit();	

		}else{
				$.alert("취소됨");
				return false;
			}

	
	
	} 
*/


function goReservationConsignment(){
	   	
		if($("#departureDt").val() == ""){
			
			$.alert("출발일을 설정해주세요",function(a){
		    });
			return;
		}

		if($("#departureAddress").val() == ""){
	
			$.alert("출발지를 목록에서 선택해주세요.",function(a){
		    });
			return;
		}
		if($("#arrivalAddress").val() == ""){
	
			$.alert("도착지를 목록에서 선택해주세요.",function(a){
		    });
			return;
		}

		 if($("#carKind").val() == ""){
				
			$.alert("차종을 입력해주세요.",function(a){
		    });
			return;
		}
		
		if($("#carIdNum").val() == "" && $("#carNum").val() == ""){
			
			$.alert("차대번호 또는 차량번호를 입력해주세요.",function(a){
		    });
			return;
		}
		
		/* if($("#carNum").val() == ""){
			
			$.alert("차량번호를 입력해주세요.",function(a){
		    });
			return;
		} */

 	
 /* 		if($("#chooseMileage").val() == "" ){	 		

 			$.alert("마일리지 사용없이 예약하시겠습니까?",function(a){
		    });
			return;
	
 		 }
	

 */			

		if($("#departureTime").val() != ""){ //빈문자열 아닐때

			 $.confirm($("#departureTime").val()+"  으로 탁송 예약 하시겠습니까?" ,function(a){

					if(a){
						finalReservation();
					}
			 });

	
		}else{ //빈문자열일때

			 $.confirm("탁송 예약 하시겠습니까?" ,function(a){
				 
				 if(a){
					finalReservation();
				 }		

			 });
	
	}


		 
}

function finalReservation(){


			 $('#insertForm').ajaxForm({
					url: "/carrier/insert-allocation.do",
				    type: "POST",
					dataType: "json",		
					data : {

						
				    },
					success: function(data, response, status) {
						var status = data.resultCode;
						if(status == '0000'){
							$.alert("예약이 정상적으로 등록 되었습니다. 예약 확정시 알림톡이 발송됩니다. ",function(a){

								document.location.href = "/carrier/main.do";
								
							});
						}else if(status == '0001'){
							$.alert("알 수없는 오류입니다. 카카오톡 플러스채팅으로 문의 주세요",function(a){
								return;			

							});			
						}
								
					},
					error: function() {
						alertEx("오류가 발생하였습니다. 관리자에게 문의주세요.");
					}                               
				});
				
				$("#insertForm").submit();

			 
}





function goReWriteReservation(){   //탁송 정보 수정할 때 


	if($("#departureDt").val() == ""){
		
		$.alert("출발일을 설정해주세요",function(a){
	    });
		return;
	}

	if($("#departureAddress").val() == ""){

		$.alert("출발지를 목록에서 선택해주세요.",function(a){
	    });
		return;
	}
	if($("#arrivalAddress").val() == ""){

		$.alert("도착지를 목록에서 선택해주세요.",function(a){
	    });
		return;
	}

	 if($("#carKind").val() == ""){
			
		$.alert("차종을 입력해주세요.",function(a){
	    });
		return;
	}
	
	if($("#carIdNum").val() == "" && $("#carNum").val() == ""){
		
		$.alert("차대번호 또는 차량번호를 입력해주세요.",function(a){
	    });
		return;
	}
	


	$.confirm("탁송 정보 수정 하시겠습니까?",function(a){
		 if(a){

			 $('#insertForm').ajaxForm({
					url: "/carrier/update-reservation.do",
				    type: "POST",
					dataType: "json",		
					data : {

					
				    },
					success: function(data, response, status) {
						var status = data.resultCode;
						if(status == '0000'){
							$.alert("정상적으로 정보 수정이 되었습니다. ",function(a){

								document.location.href = "/carrier/main.do";
								
							});
						}else if(status == '0001'){
							$.alert("알 수없는 오류입니다. 카카오톡 플러스채팅으로 문의 주세요",function(a){
								return;			

							});			
						}else if(status == 'E001'){
							$.alert("기사님이 지정되었으므로, 탁송 정보를 수정하실 수 없습니다.",function(a){
								return;			

							});
						}
								
					},
					error: function() {
						alertEx("오류가 발생하였습니다. 관리자에게 문의주세요.");
					}                               
				});
				
				$("#insertForm").submit();

			 	
		 }
		 
	});


	
}






function getDistence(){


	$.ajax({
		type : 'post',
		url  : "/carrier/getDistense.do",
	    dataType : 'json',
		async : false,
	       data : {
				
	    	   departureAddress : $("#departureAddress").val(),
			   arrivalAddress : $("#arrivalAddress").val()
 	   		
		
			},

	   success : function(data, textStatus, jqXHR)
	    {
		   var resultCode = data.resultCode;
		   var resultData = data.resultData;
		   
		   if(resultCode == "0000"){

				$('#showDistence').css('display','block');
				$('#getDis').html(resultData+" km");

				
		   }else{

				
		   }
	   } ,
	  error :function(xhnRequest, ErrorText, thrownError){
		  $.alert('알수 없는 오류입니다. 관리자에게 문의 하세요.');
          return false;
		  }
	});

	
}



    	
function showDistance(){
	
	//homeLoader.show();


		var departure =$("#departureAddress").val();
		var arrival =$("#arrivalAddress").val();
		
		if(departure == ""){

			$.alert("출발지목록에서 선택해주세요.");

		}else if(arrival == ""){

			$.alert("도착지목록에서 선택해주세요.");

		}else{
			homeLoader.show();

		

		 $('#insertForm').ajaxForm({
					url: "/carrier/tmpConsignmentReservation.do",
				    type: "POST",
					dataType: "json",		
					data : {
			
						
				    },
					success: function(data, response, status) {
						var status = data.resultCode;
						var result = data.resultData;
						if(status == '0000'){

						document.location.href ="/carrier/loadView.do?&departure="+$("#departureAddress").val()+"&arrival="+$("#arrivalAddress").val()+"&tempId="+result.tempConsignmentId;
					
						}else if(status == '0001'){
							$.alert("알 수없는 오류입니다. 카카오톡 플러스채팅으로 문의 주세요",function(a){
								return;			

							});			
						}
								
					},
					error: function() {
						alertEx("오류가 발생하였습니다. 관리자에게 문의주세요.");
					}                               
				});
				
				//$("#insertForm").submit();

			
			
			//window.open("/carrier/loadView.do?&departure="+$("#departureAddress").val()+"&arrival="+$("#arrivalAddress").val(),"_blank","top=240,left=660,width=600,height=700,toolbar=0,status=0,scrollbars=1,resizable=0");

	/* 		var updateHomeLoader = setInterval(function() {
				clearInterval(updateHomeLoader);
				homeLoader.hide();
			}, 300); */
			
		}
			
	}
   	

var contents = "";
var carCount = 0;
var carInfo = "";
var departure = "";
var arrival = "";
var address = "";
var addressInfoArr;
var baseInfo = "";




var currentStatus = "base";
$(document).ready(function(){

	$("#total").children().each(function(index,element){
		$(this).css("display","none");
	});
	$("#base").css("display","")
	contents = $("#car").html();

	carInfo = $("#carInfo").html();
	address = $("#address").html();
	departure = $("#departure").html();
	arrival = $("#arrival").html();
	baseInfo = $("#baseInfo").html();
	$("#innerLocationDetail").html("");
		
	//$(".modal-field").css("display","none");

	
})


function changeCategory(obj,categoryId){

	var temcategoryId = categoryId;
	//var divCount = $(".content-container").find(".tab").length;

	var maxdiv = true;
	var carCount = $("#carCount").val();
	var text ="";


	
	 $("#category").children().each(function(index,element){
	     $(this).removeClass("active");
	  });

		$(this).addClass("active");

		if(carCount != 0){
			
			$("div[id^='tab-']").css("display","none");
 			$("#tab-"+categoryId).css("display","");
			
 			
 			//var carText = $("#car").html("");
 			        

		}else if(carCount > 9){
			
			$alert("10대 이상의 탁송 예약건은 플러스톡으로 문의 부탁드립니다.");
			
		}else{

		$.alert("차량 대수를 입력해주세요");

			}
 			
}



function prevCategory(obj,category){
    
	var tabIdx = 0;
	$("#category").children().each(function(index,element){
		if($(this).attr("class") == "active"){
			//$(this).next().trigger("click");
			tabIdx = index-1;
		} 
	});

	var categoryId = "";

	if(currentStatus == "request"){
		categoryId = "confirm";
		$("#first").css("display","none");
		$("#middle").css("display","");
		$("#last").css("display","none");
    }else if(currentStatus == "confirm"){
		categoryId = "location";
		$("#first").css("display","none");
		$("#middle").css("display","");
		$("#last").css("display","none");
    }else if(currentStatus == "location"){
		categoryId = "car";
		$("#first").css("display","none");
		$("#middle").css("display","");
		$("#last").css("display","none");
    }else if(currentStatus == "car"){
		categoryId = "base";
		$("#first").css("display","");
		$("#middle").css("display","none");
		$("#last").css("display","none");
    }

	changeCategory($("#category").children().eq(tabIdx),categoryId);
	
}




function nextCategory(obj,category){

	var tabIdx = 0;
	$("#category").children().each(function(index,element){
		if($(this).attr("class") == "active"){
			//$(this).next().trigger("click");
			tabIdx = index+1;
		} 
	});

	//$.alert(currentStatus);
	
	var categoryId = "";
	if(currentStatus == "base"){
		categoryId = "car";
		$("#first").css("display","none");
		$("#middle").css("display","");
		$("#last").css("display","none");
    }else if(currentStatus == "car"){
		categoryId = "location";
		$("#first").css("display","none");
		$("#middle").css("display","");
		$("#last").css("display","none");
    }else if(currentStatus == "location"){
		categoryId = "confirm";
		$("#first").css("display","none");
		$("#middle").css("display","none");
		$("#last").css("display","");
    }else if(currentStatus == "confirm"){
		categoryId = "request";
		$("#first").css("display","none");
		$("#middle").css("display","none");
		$("#last").css("display","");
    }  
	changeCategory($("#category").children().eq(tabIdx),categoryId);
	 }




	function insertRequest(obj,id){



		
		$.confirm("이 내용으로 탁송예약을 하시겠습니까?",function(a){
			if(a){


				
				if($("#departureDt").val() == ""){
					
					$.alert("출발일을 설정해주세요",function(a){
				    });
					return;
				}

				if($("#departureAddr").val() == ""){
			
					$.alert("출발지를 입력해주세요.",function(a){
				    });
					return;
				}
				
				if($("#departurePoint").val() == ""){

					$.alert("출발지를 목록에서 선택해주세요.",function(a){
				    });
					return;
				}

				
				if($("#arrivalAddr").val() == ""){
			
					$.alert("도착지를 입력해주세요.",function(a){
				    });
					return;
				}

				if($("#arrivalPoint").val() == ""){

					$.alert("도착지를 목록에서 선택해주세요.",function(a){
				    });
					return;
				}

				 if($("#carKind").val() == ""){
						
					$.alert("차종을 입력해주세요.",function(a){
				    });
					return;
				}
				
				if($("#carIdNum").val() == "" && $("#carNum").val() == ""){
					
					$.alert("차대번호 또는 차량번호를 입력해주세요.",function(a){
				    });
					return;
				}
				//$("#departureTime").val()
				
			
	
				
				var contentLen = $("#confirm").find("p").length;

				var contentArr = new Array(contentLen);


				
				$("#confirm").find("p").each(function(index,element){
					contentArr[index] = $(this).html();
					
				});
					
				var subcontentArr =new Array(contentArr.slice(5));
					//$.alert(contentArr);
					console.log(contentArr);
					//console.log(subcontentArr);
				
					$.confirm("출발시간이 "+$("#departureTime").val()+"이 맞습니까? (오전/오후 확인해주세요)",function(a){
					if(a){
				
				
				
				$.ajax({ 
					type: 'post' ,
					url : "/carrier/many-allocation.do" ,
					dataType : 'json' ,
					traditional : true,
					data : {
						contentArr : contentArr,
						subcontentArr :subcontentArr,
						
					},
					success : function(data, textStatus, jqXHR)
					{
						var result = data.resultCode;
						var resultData = data.resultData;
						if(result == "0000"){
							$.alert("예약이 정상적으로 등록 되었습니다. 예약 확정시 알림톡이 발송됩니다. ",function(a){

								document.location.href = "/carrier/main.do";
								
							});

							
						}else if(result == "0001"){
							$.alert("알 수없는 오류입니다. 카카오톡 플러스채팅으로 문의 주세요",function(a){
								return;			

							});
						}
					} ,
					error : function(xhRequest, ErrorText, thrownError) {
					}
				});
					

			}
				});
					
				
	 		}
		});
	}


    
	var isChange = false;

	   function changeCategory(obj,categoryId){

	    	var inputCount = $("#car").children().length;
			$("#category").children().each(function(index,element){
				$(this).removeClass("active");
			});
			$(obj).addClass("active");

				if(categoryId != ""){
					
					$("#total").children().each(function(index,element){
						$(this).css("display","none");
					})
					
					if(categoryId == "car"){

						var tmp = carCount;

				    	if($("#carCount").val() == 0 && carCount == 0){
				    		$.alert("차량대수가 입력 되지 않았습니다.",function(a){
							});
				    		$("#"+categoryId).css("display","");
				    		isChange = false;
							return;
							
				    	}else if($("#carCount").val() != 0 && carCount == 0){
				        	if($("#carCount").val() < 1){
				        		$("#carCount").val(1)
				            }else if($("#carCount").val() > 10){
				        		$("#carCount").val(10)
				            }
				    		carCount = $("#carCount").val();

				    		$("#car").html("");
							for(var i = 0; i < carCount; i++){
								$("#car").append(contents);
							}
							$("#"+categoryId).css("display","");
							isChange = true;
				        }else if(carCount != 0 && carCount != $("#carCount").val()){
							//기존 정보가 변경 되는경우
				        	carCount = $("#carCount").val();
				        	$.confirm("차량대수가 변경 되었습니다. 적용 하시겠습니까?",function(a){
								if(a){
									isChange = true;
									var carLen = $("#car").children().length;
									if(carLen < carCount){
										//기존의 차량대수보다 많아지는경우 많아진만큼 추가 한다.
										for(var i = 0; i < carCount-carLen; i++){
											$("#car").append(contents);
										}
									}else if(carLen > carCount){
										//기존의 차량대수보다 적어지는 경우
										$("#car").children().each(function(index,element){
											if(index > carCount-1){
												$(this).remove();
											}
										});
									}else{
										//그럴리는 없겠지만...
										$("#car").html("");
										for(var i = 0; i < carCount; i++){
											$("#car").append(contents);
										}
									}
									
									$("#"+categoryId).css("display","");
								}else{
									isChange = false;
									$("#carCount").val(tmp);
									carCount = tmp;
									$("#"+categoryId).css("display","");
								}
							});

				        	
				        }else if(carCount != 0 && carCount == $("#carCount").val()){
				        	isChange = false;
				        	$("#"+categoryId).css("display","");
					    }

					}else if(categoryId != "car"){

						if(categoryId == "location"){

							//$.alert(carCount);
							if(Number(carCount) == 0){

								$.alert("차량정보가 입력 되지 않았습니다.",function(a){
								});
								$("#category").children().each(function(index,element){
									$(this).removeClass("active");
								});
								$("#category").children().first().addClass("active");
								$("#base").css("display","");
								return false;
							}else{

								//$("#innerLocationDetail").html("");
								
								if(Number($("#carCount").val()) <= 1){
									$("#locationSetting").css("display","none");
								}else{
									$("#locationSetting").css("display","");
								}
								$("#"+categoryId).css("display","");

								var carInfoArr = new Array(inputCount); 

								$("#car").children().each(function(index,element){
									var setInfo = $(carInfo);
									$(this).find("input").each(function(innerindex,innerelement){
										$(setInfo).find("P").eq(innerindex).html($(this).val());
									});
									carInfoArr[index] = $(setInfo);
									//$.alert($(carInfoArr[index]).html());
								});

								/*
								for(var i = 0; i < Number(inputCount); i++){
									$("#innerLocationDetail").append(carInfoArr[i]);
								}
								$("#innerLocationDetail").append(departure);
								$("#innerLocationDetail").append(arrival);
								*/
									
							}

							if(currentStatus == "car"){
								getCheckBoxVal("");
							}
							
						}else if(categoryId == "confirm"){

							$("#confirm").html("");
							var baseInfoArr = new Array(7);
							baseInfoArr[0] = $("#base").find("input").eq(0).val();
							baseInfoArr[1] = $("#base").find("input").eq(1).val();
				
							baseInfoArr[2] = $("#base").find("input").eq(3).val();
							baseInfoArr[3] = $("#base").find("input").eq(4).val();
							
							baseInfoArr[4] = carCount;
							baseInfoArr[5] = "";
							
							//if($("#base").find("select").eq(1).val() == "PR"){
							//baseInfoArr[5] = "우선순위결정";
							//}else if($("#base").find("select").eq(1).val() == "OA"){
							//	baseInfoArr[5] = "선착순결정";
							//}
							
							//baseInfoArr[6] = $("#base").find("select").eq(2).val()+"분";

							var setBaseInfo = $(baseInfo);
							
							$(setBaseInfo).find("p").each(function(index,element){
								$(this).html(baseInfoArr[index]);
							});
							var departureIsChecked = $("#sameDeparture").is(':checked');
							var arrivalIsChecked = $("#sameArrival").is(':checked');
							$("#confirm").append($(setBaseInfo));
							
							//차량 관련
							var inputCount = $("#car").children().length;
							 //console.log(inputCount);
							 
							var carInfoArr = new Array(inputCount); 
							$("#car").children().each(function(index,element){
								var setInfo = $(carInfo);

								$(this).find("input").each(function(innerindex,innerelement){
									$(setInfo).find("P").eq(innerindex).html($(this).val())
								});
									
								carInfoArr[index] = $(setInfo);
							});

							//주소 관련			
							var addressCount = $("#innerLocationDetail").find(".product-use-item").length;
							addressInfoArr = new Array(addressCount);
							var innerDetail = $("#innerLocationDetail").clone();
							 
							if(Number(addressCount) > 0){
								$(innerDetail).find(".product-use-item").each(function(index,element){
									
									addressInfoArr[index] = new Array(2);
									
									$(this).find("input").each(function(innerindex,innerelement){
										addressInfoArr[index][innerindex] = $(this).val();
									});
										
								});
							}



							
							if(departureIsChecked && arrivalIsChecked){
								//출발지와 도착지가 같은경우
								for(var i = 0; i < Number(inputCount); i++){
									$("#confirm").append(carInfoArr[i]);
									$(" #confirm").append($(address));
								}
								$("#confirm").find(".product-use-item").each(function(index,element){
									$(this).find("p").each(function(innerindex,innerelement){
										$(this).html(addressInfoArr[index%2][innerindex]);
									});
								});
													
							}else if(departureIsChecked && !arrivalIsChecked){
								//출발지가 같고 도착지가 다른경우
								for(var i = 0; i < Number(inputCount); i++){
									$("#confirm").append(carInfoArr[i]);
									$("#confirm").append($(address));
								}
								var count = 0;
								$("#confirm").find(".product-use-item").each(function(index,element){							
									$(this).find("p").each(function(innerindex,innerelement){
										if(index%2 == 0){
											$(this).html(addressInfoArr[0][innerindex]);
										}else{
											$(this).html(addressInfoArr[index-count][innerindex]);
										}
									});
									if(index%2 != 0){
										count++;
									}
								});
							
							}else if(!departureIsChecked && arrivalIsChecked){
								//출발지가 다르고 도착지가 같은경우
								for(var i = 0; i < Number(inputCount); i++){
									$("#confirm").append(carInfoArr[i]);
									$("#confirm").append($(address));
								}
								var count = 0;
								$("#confirm").find(".product-use-item").each(function(index,element){							
									$(this).find("p").each(function(innerindex,innerelement){
										if(index%2 == 0){
											$(this).html(addressInfoArr[index-count][innerindex]);
										}else{
											$(this).html(addressInfoArr[addressInfoArr.length-1][innerindex]);
										}
									});
									if(index%2 == 0){
										count++;
									}
								});
							
							}else if(!departureIsChecked && !arrivalIsChecked){
								//출발지와 도착지가 다른경우
								for(var i = 0; i < Number(inputCount); i++){
									$("#confirm").append(carInfoArr[i]);
									$("#confirm").append($(address));
								}
								$("#confirm").find(".product-use-item").each(function(index,element){
									$(this).find("p").each(function(innerindex,innerelement){
										$(this).html(addressInfoArr[index][innerindex]);
									});
								});
							} 
							$("#"+categoryId).css("display","");
						}else{
							$("#"+categoryId).css("display","");
						}
					}
								
					
				}else{

				}
				

		currentStatus = categoryId;
				
	   }
	    


	   function showMessage(obj){


	   	//alert($(obj).next().val());

	   	
	   	if($(obj).next().val() == "PR" || $(obj).next().val() == "OA"){

	   		if($(obj).next().val() == "PR"){
	   			$.alert("우선순위 결정 : 입찰 후 낙찰 결정시 1순위~5순위까지 결정 하고 1순위부터 차례로 낙찰 확정기회를 부여 하여 해당 차례에 낙찰 확정 하는 입찰 건에 최종 낙찰되는 경매 방식 입니다.",function(a){
	   			});
	   		}else if($(obj).next().val() == "OA"){
	   			$.alert("선착순 결정 : 입찰 후 낙찰 결정시 1~5개의 입찰 건에 낙찰 확정 기회를 부여 하고 가장 먼저 낙찰 확정 하는 입찰 건에 최종 낙찰되는 경매 방식 입니다.",function(a){
	   			});
	   		}

	   	}else if($(obj).next().find("input").attr("id") == "sameDeparture"){

	   		$.alert("탁송하는 모든 차량의 출발지가 동일한경우에 사용 합니다. 출발지가 다른경우 각각 입력 할 수 있습니다.",function(a){
	   		});

	   	}else if($(obj).next().find("input").attr("id") == "sameArrival"){

	   		$.alert("탁송하는 모든 차량의 도착지가 동일한경우에 사용 합니다. 도착지가 다른경우 각각 입력 할 수 있습니다.",function(a){
	   		});

	   	}else{

	   		$.alert("우선순위 결정의 경우 후 순위 입찰에 기회가 부여 되는 시간이고 선착순 결정의 경우 낙찰 확정기회가 부여 되는 시간을 의미합니다.",function(a){

	   		});
	   		
	   	}
	   	
	   }




	   function getCheckBoxVal(obj){


	   	var addressCount = $("#innerLocationDetail").find(".product-use-item").length;
	   	var innerDetail = $("#innerLocationDetail").clone();

	   	var isInput = false;

	   	
	   	if(Number(addressCount) > 0){
	   		addressInfoArr = new Array(addressCount);			
	   		$(innerDetail).find(".product-use-item").each(function(index,element){
	   			addressInfoArr[index] = new Array(2);
	   			$(this).find("input").each(function(innerindex,innerelement){
	   				addressInfoArr[index][innerindex] = $(this).val();
	   				if($(this).val() != ""){
	   					isInput = true;
	   				}
	   			});
	   		});
	   	}

	   	if(obj != ""){

	   		if(isInput){
	   			$.confirm("주소 정보가 초기화 됩니다. 적용 하시겠습니까?",function(a){
	   				if(a){

	   					$("#innerLocationDetail").html("");
	   					
	   					var inputCount = $("#car").children().length;
	   					var carInfoArr = new Array(inputCount); 

	   					$("#car").children().each(function(index,element){
	   						var setInfo = $(carInfo);
	   						$(this).find("input").each(function(innerindex,innerelement){
	   							$(setInfo).find("P").eq(innerindex).html($(this).val())
	   						});
	   						carInfoArr[index] = $(setInfo);
	   						//$.alert($(carInfoArr[index]).html());
	   					});
	   					
	   					

	   						var departureIsChecked = $("#sameDeparture").is(':checked');
	   						var arrivalIsChecked = $("#sameArrival").is(':checked');

	   						if(departureIsChecked && arrivalIsChecked){
	   						//상차지와 하차지가 같은경우
	   							//$.alert(0);
	   							for(var i = 0; i < Number(inputCount); i++){
	   								$("#innerLocationDetail").append(carInfoArr[i]);
	   							}
	   							$("#innerLocationDetail").append(departure);
	   							$("#innerLocationDetail").append(arrival);
	   						}else if(departureIsChecked && !arrivalIsChecked){
	   						//상차지는 같고 하차지가 다른경우
	   							//$.alert(1);
	   							$("#innerLocationDetail").append(departure);
	   							for(var i = 0; i < Number(inputCount); i++){
	   								$("#innerLocationDetail").append(carInfoArr[i]);
	   								$("#innerLocationDetail").append(arrival);
	   							}
	   						}else if(!departureIsChecked && arrivalIsChecked){
	   						//상차지가 다르고 하차지가 같은경우
	   							//$.alert(2);
	   							for(var i = 0; i < Number(inputCount); i++){
	   								$("#innerLocationDetail").append(carInfoArr[i]);
	   								$("#innerLocationDetail").append(departure);
	   							}
	   							$("#innerLocationDetail").append(arrival);
	   						}else if(!departureIsChecked && !arrivalIsChecked){
	   						//상차지도 다르고 하차지도 다른경우
	   							//$.alert(3);
	   							for(var i = 0; i < Number(inputCount); i++){
	   								$("#innerLocationDetail").append(carInfoArr[i]);
	   								$("#innerLocationDetail").append(departure);
	   								$("#innerLocationDetail").append(arrival);
	   							}
	   						}


	   						if(obj == ""){
	   							//생성이 끝났으면 입력 되어 잇던 주소를 입력 한다.		
	   							if(Number(addressCount) > 0){
	   								$("#innerLocationDetail").find(".product-use-item").each(function(index,element){
	   									$(this).find("input").each(function(innerindex,innerelement){
	   										$(this).val(addressInfoArr[index][innerindex]);
	   									});
	   								});
	   							}
	   						}


	   							
	   				}else{

	   					var isChecked = $(obj).is(':checked');
	   					
	   					if(isChecked){
	   						$(obj).prop("checked",false);
	   					}else{
	   						$(obj).prop("checked",true);
	   					}
	   				}
	   			});

	   		}else{

	   			$("#innerLocationDetail").html("");
	   			
	   			var inputCount = $("#car").children().length;
	   			var carInfoArr = new Array(inputCount); 

	   			$("#car").children().each(function(index,element){
	   				var setInfo = $(carInfo);
	   				$(this).find("input").each(function(innerindex,innerelement){
	   					$(setInfo).find("P").eq(innerindex).html($(this).val())
	   				});
	   				carInfoArr[index] = $(setInfo);
	   				//$.alert($(carInfoArr[index]).html());
	   			});
	   			

	   				var departureIsChecked = $("#sameDeparture").is(':checked');
	   				var arrivalIsChecked = $("#sameArrival").is(':checked');

	   				if(departureIsChecked && arrivalIsChecked){
	   				//상차지와 하차지가 같은경우
	   					//$.alert(0);
	   					for(var i = 0; i < Number(inputCount); i++){
	   						$("#innerLocationDetail").append(carInfoArr[i]);
	   					}
	   					$("#innerLocationDetail").append(departure);
	   					$("#innerLocationDetail").append(arrival);
	   				}else if(departureIsChecked && !arrivalIsChecked){
	   				//상차지는 같고 하차지가 다른경우
	   					//$.alert(1);
	   					$("#innerLocationDetail").append(departure);
	   					for(var i = 0; i < Number(inputCount); i++){
	   						$("#innerLocationDetail").append(carInfoArr[i]);
	   						$("#innerLocationDetail").append(arrival);
	   					}
	   				}else if(!departureIsChecked && arrivalIsChecked){
	   				//상차지가 다르고 하차지가 같은경우
	   					//$.alert(2);
	   					for(var i = 0; i < Number(inputCount); i++){
	   						$("#innerLocationDetail").append(carInfoArr[i]);
	   						$("#innerLocationDetail").append(departure);
	   					}
	   					$("#innerLocationDetail").append(arrival);
	   				}else if(!departureIsChecked && !arrivalIsChecked){
	   				//상차지도 다르고 하차지도 다른경우
	   					//$.alert(3);
	   					for(var i = 0; i < Number(inputCount); i++){
	   						$("#innerLocationDetail").append(carInfoArr[i]);
	   						$("#innerLocationDetail").append(departure);
	   						$("#innerLocationDetail").append(arrival);
	   					}
	   				}


	   		}
	   				
	   	}else{

	   	
	   		$("#innerLocationDetail").html("");
	   		var inputCount = $("#car").children().length;
	   		var carInfoArr = new Array(inputCount); 

	   		$("#car").children().each(function(index,element){
	   			var setInfo = $(carInfo);
	   			$(this).find("input").each(function(innerindex,innerelement){
	   				$(setInfo).find("P").eq(innerindex).html($(this).val())
	   			});
	   			carInfoArr[index] = $(setInfo);
	   			//$.alert($(carInfoArr[index]).html());
	   		});
	   		

	   			var departureIsChecked = $("#sameDeparture").is(':checked');
	   			var arrivalIsChecked = $("#sameArrival").is(':checked');

	   			if(departureIsChecked && arrivalIsChecked){
	   			//상차지와 하차지가 같은경우
	   				//$.alert(0);
	   				for(var i = 0; i < Number(inputCount); i++){
	   					$("#innerLocationDetail").append(carInfoArr[i]);
	   				}
	   				$("#innerLocationDetail").append(departure);
	   				$("#innerLocationDetail").append(arrival);
	   			}else if(departureIsChecked && !arrivalIsChecked){
	   			//상차지는 같고 하차지가 다른경우
	   				//$.alert(1);
	   				$("#innerLocationDetail").append(departure);
	   				for(var i = 0; i < Number(inputCount); i++){
	   					$("#innerLocationDetail").append(carInfoArr[i]);
	   					$("#innerLocationDetail").append(arrival);
	   				}
	   			}else if(!departureIsChecked && arrivalIsChecked){
	   			//상차지가 다르고 하차지가 같은경우
	   				//$.alert(2);
	   				for(var i = 0; i < Number(inputCount); i++){
	   					$("#innerLocationDetail").append(carInfoArr[i]);
	   					$("#innerLocationDetail").append(departure);
	   				}
	   				$("#innerLocationDetail").append(arrival);
	   			}else if(!departureIsChecked && !arrivalIsChecked){
	   			//상차지도 다르고 하차지도 다른경우
	   				//$.alert(3);
	   				for(var i = 0; i < Number(inputCount); i++){
	   					$("#innerLocationDetail").append(carInfoArr[i]);
	   					$("#innerLocationDetail").append(departure);
	   					$("#innerLocationDetail").append(arrival);
	   				}
	   			}

	   			if(!isChange){
	   				if(Number(addressCount) > 0){
	   					$("#innerLocationDetail").find(".product-use-item").each(function(index,element){
	   						$(this).find("input").each(function(innerindex,innerelement){
	   							$(this).val(addressInfoArr[index][innerindex]);
	   						});
	   					});
	   				}
	   			}

	   			
	   	}

	   	//공통으로 설정 되는 부분은 경매진행 하는 차량이 2대 이상인 경우에만...
	   	if(carCount > 1){
	   		if(departureIsChecked && arrivalIsChecked){
	   			$(".product-use-item").find("span").each(function(index,element){
	   				if($(this).html() == "출발지" || $(this).html() == "도착지"){
	   					$(this).html("공통"+$(this).html()+"(동일한 "+$(this).html()+"로 설정 됩니다.)");
	   				} 
	   			});
	   		}else if(departureIsChecked && !arrivalIsChecked){
	   			$(".product-use-item").find("span").each(function(index,element){
	   				if($(this).html() == "출발지"){
	   					$(this).html("공통"+$(this).html()+"(동일한 "+$(this).html()+"로 설정 됩니다.)");
	   				} 
	   			});
	   		}else if(!departureIsChecked && arrivalIsChecked){
	   			$(".product-use-item").find("span").each(function(index,element){
	   				if($(this).html() == "도착지"){
	   					$(this).html("공통"+$(this).html()+"(동일한 "+$(this).html()+"로 설정 됩니다.)");
	   				} 
	   			});
	   		}
	   	}
	   	
	   }
	       





	   var map, marker;
	   var markerArr = [];
	   var selectedObj;

	   function goShowModal(obj){


	   	//return false;

	   	
	   	if($(obj).prev().prev().val() == ""){

	   		$.alert("주소가 입력 되지 않았습니다.",function(a){
	   		
	   		});
	   		return false;
	   	}else{

	   		selectedObj = $(obj);
	   		
	   		$(selectedObj).parent().next().show();
	   		$(selectedObj).parent().next().find("ul").css("display","block");
	   		
	   		$.ajax({
	   			method:"GET",
	   			url:"https://apis.openapi.sk.com/tmap/pois?version=1&format=json&callback=result",
	   			async:false,
	   			data:{
	   				"appKey" : "l7xx6b701ff5595c4e3b859e42f57de0c975",
	   				"searchKeyword" : $(obj).prev().prev().val(),
	   				"resCoordType" : "EPSG3857",
	   				"reqCoordType" : "WGS84GEO",
	   				"count" : 10
	   			},
	   			success:function(response){

	   				
	   				var resultpoisData = response.searchPoiInfo.pois.poi;
	   				
	   				// 기존 마커, 팝업 제거
	   				if(markerArr.length > 0){
	   					for(var i in markerArr){
	   						markerArr[i].setMap(null);
	   					}
	   				}
	   				var innerHtml ="";	// Search Reulsts 결과값 노출 위한 변수
	   				var positionBounds = new Tmapv2.LatLngBounds();		//맵에 결과물 확인 하기 위한 LatLngBounds객체 생성
	   				
	   				for(var k in resultpoisData){
	   					
	   					var noorLat = Number(resultpoisData[k].noorLat);
	   					var noorLon = Number(resultpoisData[k].noorLon);
	   					var name = resultpoisData[k].name;

	   					var upperAddrName = resultpoisData[k].upperAddrName;
	   					var middleAddrName = resultpoisData[k].middleAddrName;
	   					var lowerAddrName = resultpoisData[k].lowerAddrName;
	   					var detailAddrName = resultpoisData[k].detailAddrName;
	   					var firstNo = resultpoisData[k].firstNo;
	   					var secondNo = resultpoisData[k].secondNo == "" ? "" : "-"+resultpoisData[k].secondNo;

	   					var pointCng = new Tmapv2.Point(noorLon, noorLat);
	   					var projectionCng = new Tmapv2.Projection.convertEPSG3857ToWGS84GEO(pointCng);
	   					
	   					var lat = projectionCng._lat;
	   					var lon = projectionCng._lng;
	   					
	   					var markerPosition = new Tmapv2.LatLng(lat, lon);
	   					
	   					marker = new Tmapv2.Marker({
	   				 		position : markerPosition,
	   				 		icon : "http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_" + k + ".png",
	   						iconSize : new Tmapv2.Size(24, 38),
	   						title : name,
	   				 	});
	   					
	   					innerHtml += '<li  onclick="javascript:inputValue(\''+name+" ("+upperAddrName+" "+middleAddrName+" "+lowerAddrName+" "+detailAddrName+" "+firstNo+secondNo+")"+'\',\''+lon+'\',\''+lat+'\')">';
	   					innerHtml += '<img src="http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_' + k + '.png" style="vertical-align:middle;"/>';
	   					innerHtml += '<span>'+name+'</span>';
	   					innerHtml += '<span style="display:block; color:#3f3f3f;" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;('+upperAddrName+" "+middleAddrName+" "+lowerAddrName+" "+detailAddrName+" "+firstNo+secondNo+')</span>';
	   					innerHtml += '</li>';

	   					markerArr.push(marker);
	   					positionBounds.extend(markerPosition);	// LatLngBounds의 객체 확장
	   				}
	   				
	   				$(selectedObj).parent().next().find("ul").html(innerHtml);
	   				
	   			},
	   			error:function(request,status,error){
	   				console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	   			}
	   		});
	   		
	   	}
	   	
	   }

	   	function inputValue(address,px,py){
	   		
	   		$(selectedObj).prev().prev().val(address);
	   		$(selectedObj).parent().next().find("ul").css("display","none");
	   		$(selectedObj).prev().val(px+","+py);
	   		$(selectedObj).parent().next().css('display','none');
	   	    			
	   	}

	   	function prevPageConfirm(){
	   		$.confirm("이 페이지를 나가면 작성한 내용을 잃게 됩니다. 정말 이동 하시겠습니까?",function(a){
	   			if(a){
	   				document.location.href = "/menu/menu.do";
	   			}else{
	   			}
	   		});
	   	}











</script>

<style>
		.allocation-views{
		   /* margin-top: 35px; */
		   white-space: nowrap;
		   overflow: auto;
		   /* padding-bottom: 6px; */
		}
		.allocation-views ul{
		   padding: 0;
		   list-style: none;
		   margin: 20px;
		   padding:0 20px;
		   /* border-bottom: 2px solid #eaeaea; */
		}
		
		
		.allocation-views ul li{
		   display: inline-block;
		}
		.allocation-views ul li a{
		   display: block;
		   position: relative;
		   padding:10px;
		   text-align: center;
		   font-size:15px;
		   color: #999999;
		}
		.allocation-views ul li.active a{
		   font-weight: 800;
		   color: #333;
		}
		.allocation-views ul li.active a:after{
		   position: absolute;
		   content:'';
		   width: 100%;
		   height: 3px;
		   background: #A9D0F5;
		   left: 0;
		   bottom: 0;
		}




</style>
<div class="content-container withads themedetails companydetails writing pb100">
	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<body onload="" style="">

	<div class="content-container" style="height: 100%;">
		<%-- <jsp:include page="/WEB-INF/views/jsp/common-top.jsp"></jsp:include> --%>
		<div class="animsition">
			<header class="bg-pink clearfix"
				style="position: fixed; background-color: #fff; border: none; text-align: center; z-index: 100000;">
				<div class="" style="width: 19%;">
					<a href="/carrier/main.do"><img style="zoom: 0.5;"
						src="/img/back-icon.png" alt=""></a>
				</div>
				<div class="" style="width: 60%;">
					<img style="width: 90%; height: 60%;"
						onclick="javascript:homeLoader.show(); document.location.href='/carrier/main.do'"
						src="/img/main_logo.png" alt="">
				</div>

				<div class="" style="width: 19%; float: right;">
					<!-- <a style="cursor:pointer;" href="javascript:logout();"><img style="" src="/img/logout.png" alt=""></a> -->
				</div>
			</header>
			<%-- <div style="text-align:right;">${user.driver_name}님 환영 합니다.</div> --%>
			<form id="insertForm" action="/carrier/main.do">
				<c:if test="${paramMap.allocationId eq null}">
					<input type="hidden" name="allocationId" id="allocationId" value="">
				</c:if>
				<c:if test="${paramMap.allocationId ne null}">
					<input type="hidden" name="allocationId" id="allocationId" value="${paramMap.allocationId}">
				</c:if>

				<input type="hidden" name="companyId" id="companyId" value=""> 
				<input type="hidden" name="companyName" id="companyName"> 
				<input type="hidden" name="inputDt" id="inputDt"> 
				<input type="hidden" name="registerId" id="registerId" value="${userMap.user_id}"> 
				<input type="hidden" name="registerName" id="registerName" value="${userMap.name}">
				<input type="hidden" name="profit" id="profit" value="">
				<!--순이익  -->
				<input type="hidden" name="customerId" id="customerId"> 
				<input type="hidden" name="chargeId" id="chargeId" value="${userMap.charge_id}">
			    <input type="hidden"	name="chargeAddr" id="chargeAddr"> 
			    <input type="hidden" name="customerSignificantData" id="customerSignificantData">
				<input type="hidden" name="carCnt" id="carCnt" value="1"> 
				<c:if test="${paramMap.allocationId eq null}">
					<input type="hidden" name="allocationStatus" id="allocationStatus" value="W"> 
				</c:if>
				<c:if test="${paramMap.allocationId ne null}">
					<input type="hidden" name="allocationStatus" id="allocationStatus" value="${allocationMap.allocation_status}"> 
				</c:if>
				
				<input type="hidden" name="batchStatus" 	id="batchStatus"> 
				<input type="hidden" name="paymentInputStatus" id="paymentInputStatus">
				
				<!-- 배차정보 입력,수정시 결제정보(업체청구액,기사지급액)가 입력 되어 있지 않은경우 기사가 지정 되어도 미배차 상태에서 진행 할 수 없도록 한다.  -->

				<!-- 한줄입력 삭제 -->
				<!-- <input type="hidden"  name="comment" id="comment"> -->
				<input type="hidden" name="memo" id="memo" value=""> 
				<input type="hidden" name="carInfoVal" id="carInfoVal" value=""> 
				<input type="hidden" name="paymentInfoVal" id="paymentInfoVal" value="">
				


				<div style="text-align: right; clear: both;">&nbsp;</div>
				<br>
				

				<div class="content-container interest-page loadingthemepage news"
					style="background-color: #fff; margin-top: 20px; text-align: center; clear: both;">
					<!-- <span class="d-tbc" style="font-weight: bold; color:#00000;">간편 탁송 예약</span> -->
					<div class="allocation-views">
	                    <ul id="category">
	                        <li class="active"><a>기본정보입력</a></li>
	                        <li><a>차량정보입력</a></li>
	                        <li><a>주소정보입력</a></li>
	                        <li><a>내용확인</a></li>
	                        <li><a>탁송예약</a></li>
	                    </ul>
	                </div>  
				</div>

	  	 <div id="total">
	                <div id="base" style="">
			              <div style="width: 100%; overflow: hidden;">
							<div style="float: left; width: 50%;">
								<div class="certNum" style="margin-top: 10%; width: 100%">
									<div style="text-align: center;">
										<div style="text-align: left; margin-left: 5%;">
											<span class="d-tbc" style="font-weight: bold;">담당자</span>
										</div>
										<input type="text"
											style="width: 90%; padding: 3%; margin-top: 5%;"
											placeholder="담당자" class="d-tbc" id="chargeName"
											name="chargeName" value="${userMap.name}" readonly
											onfocus="javascript:$.alert('담당자명은 수정이 불가능합니다.');">
									</div>
								</div>
							</div>
							<div style="float: left; width: 50%;">
								<div class="certNum" style="margin-top: 10%; width: 100%;">
									<div style="text-align: center;">
										<div style="text-align: left; margin-left: 5%;">
											<span class="d-tbc" style="font-weight: bold;">담당자 연락처</span>
										</div>
										<input type="text"
											style="width: 90%; padding: 3%; margin-top: 5%;"
											placeholder="담당자 연락처" class="d-tbc" id="chargePhone"
											name="chargePhone" value="${userMap.phoneNum}" readonly
											onfocus="javascript:$.alert('담당자 연락처는 수정이 불가능합니다.');">
									</div>
								</div>
							</div>
						</div>
						<div style="clear: both; margin-top: 5%;">
							<div class="certNum" style="width: 100%; margin-top: 5%;">
								<div style="text-align: center;">
									<div style="text-align: left; margin-left: 2.5%;">
										<span class="d-tbc" style="font-weight: bold;">고객명</span>
									</div>
									<input type="text"
										style="width: 95%; padding: 2%; margin-top: 2%;"
										placeholder="고객명" class="d-tbc" id="customerName"
										name="customerName" value="${userMap.customer_name}" readonly
										onfocus="javascript:$.alert('고객명은 수정이 불가능합니다.');">
								</div>
							</div>
						</div>
		                <div style="width: 100%; overflow: hidden;">
							<div style="float: left; width: 50%;">
								<div class="certNum" style="margin-top: 10%; width: 100%;">
									<div style="text-align: center;">
										<div style="text-align: left; margin-left: 2.5%;">
										<span style="color: #F00;">*</span> <span class="d-tbc" style="font-weight: bold;">출발일입력</span>
									</div>
										<%-- <input type="date" style="width: 90%; padding: 3%; margin-top: 5%;" placeholder="출발일" class="d-tbc" id="departureDt" name="departureDt" value="${today}"  onchange="$.alert(this.value);"> --%>
										<input type="date" style="width: 90%; padding: 3%; margin-top: 5%;" placeholder="출발일" class="d-tbc" id="departureDt" name="departureDt" value="${today}" >
									</div>
								</div>
							</div>
							<div style="float: left; width: 50%;">
								<div class="certNum" style="margin-top: 10%; width: 100%;">
									<div style="text-align: center;">
										<div style="text-align: left; margin-left: 2.5%;">
											<span style="color: #F00;">*</span> <span class="d-tbc" style="font-weight: bold;">출발시간</span>
										</div>
										<%-- <input type="time" style="width: 90%; padding: 3%; margin-top: 5%;" placeholder="출발시간" class="d-tbc" id="departureTime" name="departureTime" value="${now}" onchange="$.alert(this.value);"> --%>
										<input type="time" style="width: 90%; padding: 3%; margin-top: 5%;" placeholder="출발시간" class="d-tbc" id="departureTime" name="departureTime" value="${now}" >
									</div>
								</div>
							</div>
						</div>      
		                      
		                <div style="width: 100%; overflow: hidden;">
							<div style="float: left; width: 50%;">
								<div class="certNum" style="margin-top: 10%; width: 100%;">
									<div style="text-align: center;">
										<div style="text-align: left; margin-left: 2.5%;">
											<span style="color: #F00;">*</span> <span class="d-tbc" style="font-weight: bold;">차량대수(1~9대까지 입력)</span>
										</div>
										<input type="number" style="width: 90%; padding: 3%; margin-top: 5%;" placeholder="차량대수" class="d-tbc" id="carCount" name="carCount" value="1" min="1" max="10" onfocus="">
									</div>
								</div>
							</div>
						</div> 
							
					</div>
					
				<!--	차량관련 -->
					<div id="car" style="display:none;">
						<!-- <div name="carInfo"> -->
						<div>
			                <div style="width: 100%; overflow: hidden;">
								<div style="float: left; width: 50%;">
									<div class="certNum" style="margin-top: 10%; width: 100%;">
										<div style="text-align: center;">
											<div style="text-align: left; margin-left: 2.5%;">
												<span style="color: #F00;">*</span> <span class="d-tbc" style="font-weight: bold;">차종(모델명)</span>
											</div>
											<input type="text" style="width: 90%; padding: 3%; margin-top: 5%;" placeholder="차종(모델명)" class="d-tbc" name="carKind" id="carKind" value="" onfocus="">
										</div>
									</div>
								</div>
								<div style="float: left; width: 50%;">
										<div class="certNum" style="margin-top: 10%; width: 100%;">
											<div style="text-align: center;">
												<div style="text-align: left; margin-left: 2.5%;">
													<span style="color: #F00;">*</span> <span class="d-tbc" style="font-weight: bold;">차량번호</span>
											</div>
											<input type="text" style="width: 90%; padding: 3%; margin-top: 5%;" placeholder="차량번호" class="d-tbc" name="carNum" id="carNum" value="" onfocus="">
										</div>
									</div>
								</div>
							</div> 
							
							<div style="width: 100%; overflow: hidden;">
								<div style="float: left; width: 50%;">
									<div class="certNum" style="margin-top: 10%; width: 100%;">
										<div style="text-align: center;">
											<div style="text-align: left; margin-left: 2.5%;">
											<span class="d-tbc" style="font-weight: bold;">차대번호</span>
										</div>
											<input type="text" style="width: 90%; padding: 3%; margin-top: 5%;" placeholder="차대번호" class="d-tbc" name="carIdNum" id="carIdNum" value=""  onfocus="">
										</div>
									</div>
								</div>
								<c:if test ="${customerDetailMap.control_payment eq 'Y'}">	
									<div style="float: left; width: 50%;">
										<div class="certNum" style="margin-top: 10%; width: 100%;">
											<div style="text-align: center;">
												<div style="text-align: left; margin-left: 2.5%;">
													<span style="color: #F00;">*</span> <span class="d-tbc" style="font-weight: bold;">탁송료</span>
											</div>
												<input type="text" style="width: 90%; padding: 3%; margin-top: 5%;" placeholder="단가입력" class="d-tbc" name="carNum" id="carNum" value="" onfocus="">
										</div>
									</div>
								</div>
							</c:if>
							<c:if test ="${customerDetailMap.control_payment ne 'Y'}">	
									<div style="float: left; width: 50%;  display:none;">
										<div class="certNum" style="margin-top: 10%; width: 100%;">
											<div style="text-align: center;">
												<div style="text-align: left; margin-left: 2.5%;">
													<span style="color: #F00;">*</span> <span class="d-tbc" style="font-weight: bold;">탁송료</span>
											</div>
												<input type="text" style="width: 90%; padding: 3%; margin-top: 5%;" placeholder="단가입력" class="d-tbc" name="carNum" id="carNum" value="" onfocus="">
										</div>
									</div>
								</div>
							</c:if>
						</div>
							<div style="width: 100%; overflow: hidden;">
								<div style="float: left; width: 50%;">
									<div class="certNum" style="margin-top: 10%; width: 100%;">
										<div style="text-align: center;">
											<div style="text-align: left; margin-left: 2.5%;">
											<span class="d-tbc" style="font-weight: bold;">차량구분</span>
										</div>
												<div class="carCategory" style=" padding: 5%;">
							                		<select name="carNewOld" id="carNewOld" style="width:95%; padding: 5%;">
														<option value="" >선택</option>
														<option value="O"  >중고차</option>
														<option value="N"  >신차</option>
													</select>
							                </div>
										</div>
									</div>
								</div>
						
						</div>
							
							
					<!-- 	 <div style="float: left; width: 50%;">
								<div class="certNum" style="margin-top: 10%; width: 100%;">
									<div id="showDistence" 	style="float: left; width: 50%; display:none;">
											<div class="certNum" style="margin-top: 10%; width: 100%;">
												<div style="text-align: center; margin-right: 10%;">
													<div id="btn_group"style="text-align: right; margin-left: 5%;">
														<span class="d-tbc" style="font-weight: bold;">검색 예상 거리</span><br>
															<c:if test="${paramMap.allocationId eq null}">
														   	 <button id="test_btn1" class="btn_select" style="width: 50%; float:left;" onclick="javascirpt:showDistance();">경로 보기</button> 
														</c:if>
													</div> 
													<div style="text-align: right; margin-left: 5%; margin-top: 10%;">
														<span class="d-tbc" id="getDis" style="font-weight: bold; color: #FF4000;"></span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>  -->
									<div style="clear: both; margin-top: 5%;">
												<div class="certNum" style="width: 100%; margin-top: 5%;">
													<div style="text-align: center;">
														<div style="text-align: left; margin-left: 2.5%;">
															<span class="d-tbc" style="font-weight: bold;">비고 또는 탁송메모 작성</span>
														</div>
															<!--  <textarea id="customerEtc" name="customerEtc" rows="3" cols="55" style=" margin-top: 5%;"
																placeholder="비고 내용을 입력해주세요."  onfocus=""></textarea> -->
															<input type="text" style="width: 90%; padding: 3%; margin-top: 5%;" placeholder="탁송메모를 작성해주세요" class="d-tbc" name="carNum" id="carNum" maxlength="100" value="" onfocus="">
													</div>
												</div>
											</div>
								
								
						</div>
						 
					</div>
					
					<div id="location" class="notification-settings content-box" style="display:none;">
		                
		                
		                <div id="locationSetting" style="width: 100%; overflow: hidden;">
							<div class="notif-box pl22" style="padding:0 15px;">
			                    <div onclick="javascript:showMessage(this);">
			                    	<span class="d-tbc" style="line-height:70px; font-weight: bold;">출발지 정보 동일</span>
			                    	<span style="margin-left:5%;"><img style="width:15px; height:15px; cursor:pointer;" src="/img/question-icon.png" onclick="" alt="" title=""></span>
			                    </div>
			                    <div class="track-holder">
			                        <input type="checkbox" id="sameDeparture" hidden checked onchange="javascript:getCheckBoxVal(this);" />
			                        <!-- <input type="checkbox" id="sameDeparture" hidden checked onchange="javascript:getCheckBoxVal();" /> -->
			                        <label for="sameDeparture"></label>
			                    </div>
			                </div>

			                <div class="notif-box pl22" style="padding:0 15px;">
			                    <div onclick="javascript:showMessage(this);">
			                    	<span class="d-tbc" style="line-height:70px; font-weight: bold;">도착지 정보 동일</span>
			                    	<span style="margin-left:5%;"><img style="width:15px; height:15px; cursor:pointer;" src="/img/question-icon.png" onclick="" alt="" title=""></span>
			                    </div>
			                    <div class="track-holder">
			                        <input type="checkbox" id="sameArrival" hidden checked onchange="javascript:getCheckBoxVal(this);" />
			                        <!-- <input type="checkbox" id="sameArrival" hidden checked onchange="javascript:getCheckBoxVal();" /> -->
			                        <label for="sameArrival"></label>
			                    </div>
			                </div>
						</div>      
		                      
		                <div id="innerLocationDetail">   
		                		
		                  	<div id="carInfo">
			                	<div style="padding:3%;">	
				                	<div style="width: 100%; overflow: hidden;  border:6px solid #A9D6FA; border-bottom:none;">
										<div style="float: left; width: 50%;">
											<div class="certNum" style="margin-top: 5%; width: 100%;">
												<div style="text-align: right;">
													<div style="text-align: left; margin-left: 2.5%;">
													<span class="d-tbc" style="font-weight: bold;">차종</span>
												</div>
													<p style="width: 90%; padding: 0%; margin: 0%;" onfocus=""></p>
												</div>
											</div>
										</div>
										<div style="float: left; width: 50%;">
											<div class="certNum" style="margin-top: 5%; width: 100%;">
												<div style="text-align: right;">
													<div style="text-align: left; margin-left: 2.5%;">
														<span class="d-tbc" style="font-weight: bold;">차량번호</span>
													</div>
													<p style="width: 90%; padding: 0%; margin: 0%;" onfocus=""></p>
												</div>
											</div>
										</div>
									</div> 
									<div style="width: 100%; overflow: hidden;  border:6px solid #A9D6FA;  border-top:none;  border-bottom:none;">
										<div style="float: left; width: 50%;">
											<div class="certNum" style="margin-top: 5%; width: 100%;">
												<div style="text-align: right;">
													<div style="text-align: left; margin-left: 2.5%;">
													<span class="d-tbc" style="font-weight: bold;">차대번호</span>
												</div>
													<p style="width: 90%; padding: 0%; margin: 0%;" onfocus=""></p>
												</div>
											</div>
										</div>
											<c:if test ="${customerDetailMap.control_payment eq 'Y'}">
												<div style="float: left; width: 50%;" >
														<div class="certNum" style="margin-top: 5%; width: 100%;">
															<div style="text-align: right;">
																<div style="text-align: left; margin-left: 2.5%;">
																	<span class="d-tbc" style="font-weight: bold;">탁송료</span>
																</div>
																<p style="width: 90%; padding: 0%; margin: 0%;" onfocus=""></p>
															</div>
														</div>
													</div>
											</c:if>
											<c:if test ="${customerDetailMap.control_payment ne 'Y'}">
												<div style="float: left; width: 50%; display:none;" >
														<div class="certNum" style="margin-top: 5%; width: 100%;">
															<div style="text-align: right;">
																<div style="text-align: left; margin-left: 2.5%;">
																	<span class="d-tbc" style="font-weight: bold;">탁송료</span>
																</div>
																<p style="width: 90%; padding: 0%; margin: 0%;" onfocus=""></p>
															</div>
														</div>
													</div>
											</c:if>
										</div> 
										<div style="width: 100%; overflow: hidden;  border:6px solid #A9D6FA;  border-top:none;  border-bottom:none;">
											<div style="clear: both; margin-top: 5%;">
													<div class="certNum" style="width: 100%; margin-top: 5%;">
														<div style="text-align: center;">
															<div style="text-align: left; margin-left: 2.5%;">
																<span class="d-tbc" style="font-weight: bold;">비고 또는 탁송메모 작성</span>
															</div>
																<p style="width: 90%; padding: 0%; margin: 0%; text-align:right;" onfocus="" ></p>
																	<!-- </p>-->
														</div>
													</div>
												</div>
										</div>
									
								</div>
									
							</div>	
							
							
							
							<div id="address">
								<div class="productsinuse" style="clear: both; padding:0 3% 3% 3%;">
									<div id="" class="certNum  product-use-list" style="width: 100%; padding:0;">
										<div class="product-use-item" style="text-align: right; padding:0; border:6px solid #A9D6FA;  border-top:none;  border-bottom:none;">
											<div style="text-align: left; margin-left: 2.5%;">
												<span class="d-tbc" style="font-weight: bold;">출발지</span>
											</div>
											<p style="width: 100%; padding-bottom: 3%; padding-right:8%; margin: 0%;" onfocus=""></p>
											<p style="display:none;"></p>											
										
											<div style="text-align: right;">
													<div style="text-align: left; margin-left: 2.5%;">
													<span class="d-tbc" style="font-weight: bold;">츨발지 담당자명</span>
												</div>
													<p style="width: 90%; padding: 0%; margin: 0%;" onfocus=""></p>
												</div>
												<div style="text-align: right;">
													<div style="text-align: left; margin-left: 2.5%;">
													<span class="d-tbc" style="font-weight: bold;">출발지 연락처</span>
												</div>
													<p style="width: 90%; padding: 0%; margin: 0%;" onfocus=""></p>
												</div>
										
										</div>
										
									</div>
								</div>
								
								
								<div class="productsinuse" style="clear: both; padding:0 3% 3% 3%;">
									<div id="" class="certNum  product-use-list" style="width: 100%; padding:0;">
										<div class="product-use-item" style="text-align: right; padding:0; border:6px solid #A9D6FA;  border-top:none;  border-bottom:none;">
											<div style="text-align: left; margin-left: 2.5%;">
												<span class="d-tbc" style="font-weight: bold;">도착지</span>
											</div>
											<p style="width: 100%; padding-bottom: 3%; padding-right:8%; margin: 0%;" onfocus=""></p>
											<p style="display:none;"></p>	
											
											
												<div style="text-align: right;">
													<div style="text-align: left; margin-left: 2.5%;">
													<span class="d-tbc" style="font-weight: bold;">도착지 담당자명</span>
												</div>
													<p style="width: 90%; padding: 0%; margin: 0%;" onfocus=""></p>
												</div>
												<div style="text-align: right; border-bottom:6px solid #A9D6FA;">
													<div style="text-align: left; margin-left: 2.5%; ">
													<span class="d-tbc" style="font-weight: bold;">도착지 연락처</span>
												</div>
													<p style="width: 90%; padding: 0%; margin: 0%;" onfocus=""></p>
												</div>
																					
										</div>
									</div>
								</div>
							</div>
							
							
							
							<div id="departure">
				                <div class="productsinuse" style="clear: both;">
									<div id="" class="certNum  product-use-list" style="width: 100%; padding:0;">
										<div class="product-use-item" style="text-align: center;">
											<div style="text-align: left; margin-left: 2.5%;">
												<span style="color: #F00;">*</span> <span class="d-tbc" style="font-weight: bold;">출발지</span><span></span>
											</div>
											<input type="text" style="width: 75%; padding: 2%; margin-top: 2%;" placeholder="출발지" class="d-tbc " id="departureAddr" name="departureAddr" value="" onfocus="">
											<input id="departurePoint" type="hidden" value="">
											<a onclick="javascript:goShowModal(this);" class="product-use-search inlined btn_select" style="position:static; margin-top:-5px; line-height:39px;">검색</a>
									      	<div style="width: 100%; overflow: hidden;">
												<div style="float: left; width: 50%;">
													<div class="certNum" style="margin-top: 10%; width: 100%;">
														<div style="text-align: center;">
															<div style="text-align: left; margin-left: 5%;">
																<span class="d-tbc" style="font-weight: bold;">출발지 담당자명</span>
															</div>
																<input type="text"
																	style="width: 90%; padding: 3%; margin-top: 5%;"
																	placeholder="담당자명" class="d-tbc" id="departurePersonInCharge"
																	name="departurePersonInCharge"
																	value="${tempConsignmentMap.departure_person_in_charge}">
														</div>
														
													</div>
												</div>
												<div style="float: left; width: 50%;">
													<div class="certNum" style="margin-top: 10%; width: 100%;">
														<div style="text-align: center;">
															<div style="text-align: left; margin-left: 5%;">
																<span class="d-tbc" style="font-weight: bold;">출발지 연락처</span>
															</div>
																<input type="number" pattern="\d*"
																	style="width: 90%; padding: 3%; margin-top: 5%;"
																	placeholder="연락처" class="d-tbc" id="departurePhone"
																	name="departurePhone" value="">
														
														</div>
													</div>
												</div>
											</div>
										</div>
										
										<div class="modal-field" style="display:none;">
											<div class="modal-box">
												<div class="modal-table-container">
													<div class="title" style="text-align: center; margin-top: 5%;">
														<strong>검색</strong> 목록
													</div>
													<br>
													<div class="rst_wrap">
														<div class="rst mCustomScrollbar">
															<!-- <ul id="searchResult" name="searchResult"> -->
															<ul name="searchResult">
															</ul>
														</div>
													</div>
						
												</div>
												<div class="confirmation">
													<div class="confirm" style="text-align: center;">
														<input type="button" value="창닫기" name="" onclick="javascript:$(this).parent().parent().parent().parent().hide();">
													</div>
												</div>
											</div>
										</div>
									
									</div>
								</div>
							</div>
							<div id="arrival">
								<div class="productsinuse" style="clear: both;">
									<div id="" class="certNum  product-use-list"
										style="width: 100%; padding:0;">
										<div class="product-use-item" style="text-align: center;">
											<div style="text-align: left; margin-left: 2.5%;">
												<span style="color: #F00;">*</span> <span class="d-tbc"
													style="font-weight: bold;">도착지</span>
											</div>
											<input type="text" style="width: 75%; padding: 2%; margin-top: 2%;" placeholder="도착지" class="d-tbc" id="arrivalAddr" name="arrivalAddr" value="" onfocus="">
											<input id="arrivalPoint" type="hidden" value="">
											<a onclick="javascript:goShowModal(this);" class="product-use-search inlined btn_select" style="position:static; margin-top:-5px; line-height:39px;">검색</a>
											<div style="width: 100%; overflow: hidden;">
												<div style="float: left; width: 50%;">
													<div class="certNum" style="margin-top: 10%; width: 100%;">
														<div style="text-align: center;">
															<div style="text-align: left; margin-left: 5%;">
																<span class="d-tbc" style="font-weight: bold;">도착지 담당자명</span>
															</div>
															<input type="text"
																	style="width: 90%; padding: 3%; margin-top: 5%;"
																	placeholder="담당자명" class="d-tbc" id="arrivalPersonInCharge"
																	name="arrivalPersonInCharge"
																	value="${tempConsignmentMap.arrival_person_in_charge}">
														</div>
													</div>
												</div>
												<div style="float: left; width: 50%;">
													<div class="certNum" style="margin-top: 10%; width: 100%;">
														<div style="text-align: center;">
															<div style="text-align: left; margin-left: 5%;">
																<span class="d-tbc" style="font-weight: bold;">도착지 연락처</span>
															</div>
														
																<input type="number" pattern="\d*"
																	style="width: 90%; padding: 3%; margin-top: 5%;"
																	placeholder="연락처" class="d-tbc" id="arrivalPhone"
																	name="arrivalPhone" value="${tempConsignmentMap.arrival_phone}">
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="modal-field"  style="display:none;">
											<div class="modal-box">
												<div class="modal-table-container">
													<div class="title" style="text-align: center; margin-top: 5%;">
														<strong>검색</strong> 목록
													</div>
													<br>
													<div class="rst_wrap">
														<div class="rst mCustomScrollbar">
															<!-- <ul id="searchResult" name="searchResult"> -->
															<ul name="searchResult">
															</ul>
														</div>
													</div>
						
												</div>
												<div class="confirmation">
													<div class="confirm" style="text-align: center;">
														<input type="button" value="창닫기" name="" onclick="javascript:$(this).parent().parent().parent().parent().hide();">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							
							
							      
		                </div>   
		                
		                
						
					
					</div>
					
					
					<div id="confirm" style="display:none;">
		                
		                <div id="baseInfo">
			            	<div style="padding:3%;">	
			            	       	<div style="width: 100%; overflow: hidden;  border:0px solid #eaeaea; ">
										<div style="float: left; width: 50%;">
											<div class="certNum" style="margin-top: 5%; width: 100%;">
												<div style="text-align: right;">
													<div style="text-align: left; margin-left: 2.5%;">
													<span class="d-tbc" style="font-weight: bold;">고객명</span>
												</div>
													<p style="width: 90%; padding: 0%; margin: 0%;" onfocus=""></p>
												</div>
											</div>
										</div>
										<div style="float: left; width: 50%;">
											<div class="certNum" style="margin-top: 5%; width: 100%;">
												<div style="text-align: right;">
													<div style="text-align: left; margin-left: 2.5%;">
														<span class="d-tbc" style="font-weight: bold;">담당자 번호</span>
													</div>
													<p style="width: 90%; padding: 0%; margin: 0%;" onfocus=""></p>
												</div>
											</div>
										</div>
									</div>
			            	
			            	
				                	<div style="width: 100%; overflow: hidden;  border:0px solid #eaeaea; border-bottom:none;">
										<div style="float: left; width: 50%;">
											<div class="certNum" style="margin-top: 5%; width: 100%;">
												<div style="text-align: right;">
													<div style="text-align: left; margin-left: 2.5%;">
													<span class="d-tbc" style="font-weight: bold;">출발일</span>
												</div>
													<p style="width: 90%; padding: 0%; margin: 0%;" onfocus=""></p>
												</div>
											</div>
										</div>
										<div style="float: left; width: 50%;">
											<div class="certNum" style="margin-top: 5%; width: 100%;">
												<div style="text-align: right;">
													<div style="text-align: left; margin-left: 2.5%;">
														<span class="d-tbc" style="font-weight: bold;">출발시간</span>
													</div>
													<p style="width: 90%; padding: 0%; margin: 0%;" onfocus=""></p>
												</div>
											</div>
										</div>
									</div>
								 
									<div style="width: 100%; overflow: hidden;  border:0px solid #eaeaea;  border-top:none;  border-bottom:none;">
										<div style="float: left; width: 50%;">
											<div class="certNum" style="margin-top: 5%; width: 100%;">
												<div style="text-align: right;">
													<div style="text-align: left; margin-left: 2.5%;">
													<span class="d-tbc" style="font-weight: bold;">차량대수</span>
												</div>
													<p style="width: 90%; padding: 0%; margin: 0%;" onfocus=""></p>
												</div>
											</div>
										</div>
										
									</div> 
								
								</div>   
		                </div>
		                
					</div>
					
					<div id="request" style="display:none;">
		            
					</div>
				
				
				
		
				
				</div>
				<!-- <h5 class="d-tbc" style="font-weight: bold; color:#00000;">	<img class="blinking" src="/img/kakaotalkask.jpg" style ="margin-top:0%; width:8%; height:6%">한국카캐리어(주) 채널을 추가 해주시면 예약확정시 <span style="color:#FE9A2E;">알림톡</span>이 발송됩니다.</h5> -->

				<div style="margin-top: 5%; text-align: center; display: inline-block; margin-left: 20%; width: 50%; margin-bottom: 20%;">
					<div style="border-radius: 50%; height: 30%; width: 40%; background-color: #CEF6F5; margin-left: 40%;">
					
					</div>
				</div>
			</form>
	
		</div>
	</div>
	 <div class="fixed-bottomarea">
	                <!-- <a href="javascript:nextCategory(this,'')">다음단계로</a> -->
	                <div id="first">
	                	<div style="width:100%; display:inline-block;"><a href="javascript:nextCategory(this,'')">다음단계로</a></div>
	                </div>
	                <div id="middle" style="display:none;">
	                	<div style="width:50%; display:inline-block;"><a href="javascript:prevCategory(this,'')">이전단계로</a></div><div style="width:50%; display:inline-block;"><a href="javascript:nextCategory(this,'')">다음단계로</a></div>
	                </div>
	                <div id="last" style="display:none;">
	                	<div style="width:50%; display:inline-block;"><a href="javascript:prevCategory(this,'')">이전단계로</a></div><div style="width:50%; display:inline-block;"><a href="javascript:insertRequest(this,'')">탁송예약</a></div>
	                </div>
	            </div>
</body>

</div>
<script
	src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
<script src="/js/vendor/bootstrap.min.js"></script>
<!-- veiwport for countnumber -->
<script src="/js/alert.js"></script>
<script src="/js/main.js"></script>
<script src="/js/vendor/xpull.js"></script>
<script src="/js/vendor/jquery.loading-indicator.min.js"></script>
<script src="https://apis.openapi.sk.com/tmap/jsv2?version=1&appKey=l7xx6b701ff5595c4e3b859e42f57de0c975"></script>
<script src="/js/animsition.min.js"></script>
<script src="/js/vendor/jquery.form.min.js"></script>

<script>
    
	
	$('#container').xpull({
	    'paused': false, 
	    // Pull threshold - amount in  pixels required to pull to enable release callback
        'pullThreshold':50,
        // Max pull down element - amount in pixels
        maxPullThreshold: 50,
        // timeout in miliseconds after which the loading indicator stops spinning.
        // If set to 0 - the loading will be indefinite
        'spinnerTimeout':1000,
        onPullStart: function(){},
        onPullEnd: function(){},
        callback: function(){window.location.reload()},

	});   
	
	
	var homeLoader;
	var inputStatus = "departure";
	 
	$(document).ready(function(){
			
		
				homeLoader = $('body').loadingIndicator({
					useImage: false,
					showOnInit : false
				}).data("loadingIndicator");
				
				/* setTimeout(function() {
					homeLoader.show();
					homeLoader.hide();
				}, 1000); */
		
	});


	


    </script>


</body>
</html>
