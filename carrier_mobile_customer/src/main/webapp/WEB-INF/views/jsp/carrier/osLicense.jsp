<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> 
	<html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="stylesheet" href="/test/css/owl.carousel.min.css">
        <link rel="stylesheet" href="/test/css/owl.theme.default.min.css">
        <link rel="stylesheet" href="/test/css/animate.css">
        <link rel="stylesheet" href="/test/css/bootstrap.min.css">
        <link rel="stylesheet" href="/test/css/font-awesome.min.css">
        <link rel="stylesheet" href="/test/css/main.css">
        <link rel="stylesheet" href="/test/css/crossbrowser.css">
        <link rel="stylesheet" href="/css/animsition.min.css">
        <script src="/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    
    
<script type ="text/javascript">
        

		function backKeyController(str) {

			document.location.href = "/carrier/sub-main.do";
			
	
		}

        </script>
    <body class="reg">
        
        
        
        <!-- <div class="animsition"> -->
        
        
        <header class="bg-pink clearfix" style="position:fixed; background-color:#fff; border:none; text-align:center; top:0px; line-height:55px; height:55px; width:100%; z-index:1;">
                <div class="" style="width:19%; position: relative; display: inline-block; z-index:1;  float:left;">
                    <a onclick="javascript:document.location.href = '/carrier/sub-main.do'"><img style="zoom: 0.5;" src="/img/back-icon.png" alt=""></a> 
                </div> 
                <div class="" style="width:60%; height:87%; position: relative; display: inline-block; float:left;">
                	<img style="width:90%; height:70%;" onclick="javascript:homeLoader.show(); document.location.href='/carrier/main.do'" src="/img/main_logo.png" alt="">
                </div>
               <div class="" style="width:19%; position: relative;display: inline-block; float:right;">
                   <!-- <a style="cursor:pointer;" href="javascript:logout();"><img style="" src="/img/logout.png" alt=""></a> -->
                </div> 
            </header>
        
        
        
        
        <div class="container-fluid NP">
            <div class="container register-page">
                
                <div class="register-form">
                    
                    <div class="step-contents">
                        <div class="step-heading text-center">오픈소스 라이선스 </div>
                        
                        <div class="step-contents-item">
                            <div class="step-checkbox">
                                <div class="i-table">
                                    <div class="t-cell"><!-- <input type="checkbox" name="" id="acceptTerms" class="" style="transform : scale(1.5);"> --></div>
                                    <div class="t-cell">
                                        <label for="acceptTerms" style="font-size:10px;">한국카캐리어 어플리케이션에서 사용중인 오픈소스 입니다.<br> 
																			오픈소스 개발에 참여 해 주신 모든분들께 감사의 말씀 드립니다.
										</label>
                                    </div>
                                </div>
                            </div>
                            <div class="step-details-box" style="height:500px; padding:10px;">
                            	<p>
								-jQuery Boilerplate - v3.3.1<br>
								(http://jqueryboilerplate.com , MIT License)
								</p>
                                <p>
								-jquery 1.11.2<br>
								(https://www.npmjs.org/package/jquery , MIT License)
								</p>
								<p>
								-JSTL - JavaServer Pages Standard Tag Library 1.2<br>
								(http://java.sun.com/products/jsp/jstl/ , Common Development and Distribution License 1.0)
								</p>
								<p>
								-jQuery.Form<br>
								(http://github.com/malsup/form/ , MIT License)
								</p>
								<p>
								-Font-Awesome 4.7.0<br>
								(http://github.com/FortAwesome/Font-Awesome/ , MIT License)
								</p>
								<p>
								-SwipeView<br>
								(http://github.com/cubiq/SwipeView/ , MIT License)
								</p>
								<p>
								-Apache Jakarta Commons Discovery 0.2<br>
								(http://jakarta.apache.org/commons/discovery/ , Apache License 1.1)
								</p>
								<p>
								-mybatis-spring 1.2.2<br>
								(http://code.google.com/p/mybatis/ , Apache License 2.0)
								</p>
								<p>
								-alert<br>
								(https://github.com/ydq/alert , MIT License)
								</p>
								<p>
								-mybatis 3.1.1<br>
								(http://code.google.com/p/mybatis/ , Apache License 2.0)
								</p>
								<p>
								-Animsition<br>
								(https://github.com/blivesta/animsition , MIT License)
								</p>
								<p>
								-bxslider-4<br>
								(https://github.com/stevenwanderski/bxslider-4 , MIT License)
								</p>
								<p>
								-moment 2.24.0<br>
								(http://github.com/moment/moment/ , MIT License)
								</p>
								<p>
								-cryptoJs_aes<br>
								( https://code.google.com/archive/p/crypto-js/, The 3-Clause BSD License )
								</p>
								<p>
								-mustache<br>
								(https://mustache.github.io/ , MIT License )
								</p>
								<p>
								-postscribe<br>
								( https://krux.github.io/postscrib , MIT License )
								</p>
								<p>
								-pulltorefreshjs<br>
								( https://www.boxfactura.com/pulltorefresh.js/ , MIT License )
								</p>
								<p>
								-Swiper 4.3.2<br>
								( https://swiperjs.com/ , MIT License )
								</p>
								<p>
								-jqueryrotate<br>
								( http://jqueryrotate.com , MIT license )
								</p>
								<p>
								-Spring Web Services 4.2.7<br>
								(http://repo.maven.apache.org/maven2//org/springframework/spring-oxm/ , Apache License 2.0)
								</p>
								<p>
								-atinject 1-svn<br>
								(http://code.google.com/p/atinject/ , Apache License 2.0)
								</p>
								<p>
								-Apache ServiceMix :: Bundles :: spring-tx<br>
								(http://servicemix.apache.org/bundles-pom/org.apache.servicemix.bundles.spring-tx/ , Apache License 2.0)
								</p>
								<p>
								-Lucy Xss Servlet Filter 2.0.0<br>
								(http://repo.maven.apache.org/maven2//com/navercorp/lucy/lucy-xss-servlet/ , Apache License 2.0)
								</p>
								<p>
								-Apache Jakarta Commons Discovery 0.4<br>
								(http://jakarta.apache.org/commons/discovery/ , Apache License 2.0)
								</p>
								<p>
								-Atomikos Util 3.8.0<br>
								(http://www.atomikos.com/ate/atomikos-util/ , Apache License 2.0)
								</p>
								<p>
								-respond<br>
								(http://github.com/madoublet/respond/ , MIT License)
								</p>
								<p>
								-Legion of the Bouncy Castle<br>
								(http://repo1.maven.org/maven2/bouncycastle/bcprov-jdk15/ , MIT License)
								</p>
								<p>
								-Spring Web Services 2.4.0<br>
								(http://repo.spring.io/libs-release-local//org/springframework/ws/spring-ws-security/ , Apache License 2.0)
								</p>
								<p>
								-quartz_scheduler<br>
								(http://github.com/muellerc/quartz_scheduler/ , MIT License)
								</p>
								<p>
								-Apache XML Xalan-Java 2.6.0<br>
								(http://xml.apache.org/xalan-j/index.html , Apache License 2.0)
								</p>
								<p>
								-jQuery<br>
								(http://jszip.org/redist/jquery , MIT License)
								</p>
								<p>
								-Lucy XSS Filter 1.6.3<br>
								(http://repo.maven.apache.org/maven2//com/navercorp/lucy/lucy-xss/ , Apache License 2.0)
								</p>
								<p>
								-Transactions API 3.8.0<br>
								(http://www.atomikos.com/ate/transactions-api/ , Apache License 2.0)
								</p>
								<p>
								-Apache ServiceMix :: Bundles :: spring-beans<br>
								(http://servicemix.apache.org/bundles-pom/org.apache.servicemix.bundles.spring-beans/ , Apache License 2.0)
								</p>
								<p>
								-Apache POI - org.apache.poi:poi-contrib 3.13<br>
								(http://repo1.maven.org/maven2/org/apache/poi/poi-contrib/ , Apache License 2.0)
								</p>
								<p>
								-jquery-migrate-rails 1.2.1<br>
								(http://github.com/krishnasrihari/jquery-migrate-rails/ , MIT License)
								</p>
								<p>
								-Apache Commons IO (for Apache Directory Studio) 2.0.1<br>
								(http://directory.apache.org/studio/parent-libraries/org.apache.commons.io/ , Apache License 2.0)
								</p>
								<p>
								-semantic-ui-daterangepicker 1.3.20<br>
								(http://github.com/BreadMaker/semantic-ui-daterangepicker/ , MIT License)
								</p>
								<p>
								-HttpMime 4.5.2<br>
								(http://repo.maven.apache.org/maven2//org/apache/httpcomponents/httpmime/ , Apache License 2.0)
								</p>
								<p>
								-Apache Commons FileUpload 1.2.2<br>
								(http://jakarta.apache.org/commons/fileupload/ , Apache License 2.0)
								</p>
								<p>
								-jquery-slimscroll<br>
								(http://github.com/rochal/jQuery-slimScroll/ , MIT License)
								</p>
								<p>
								-Apache Commons Collections 4.1<br>
								(http://repo.maven.apache.org/maven2//org/apache/commons/commons-collections4/ , Apache License 2.0)
								</p>
								<p>
								-Jackson-annotations 2.8.0<br>
								(http://repo.maven.apache.org/maven2//com/fasterxml/jackson/jaxrs/jackson-jaxrs-json-provider/ , Apache License 2.0)
								</p>
								<p>
								-Apache Jakarta Commons Pool 1.5.4<br>
								(http://jakarta.apache.org/commons/pool/ , Apache License 2.0)
								</p>
								<p>
								-Apache Web Services Axis 1.4<br>
								(http://ws.apache.org/axis/ , Apache License 2.0)
								</p>
								<p>
								-Eclipse Communication Framework (ECF)<br>
								(http://www.eclipse.org/ecf/ , Eclipse Public License 1.0)
								</p>
								<p>
								-footable-on-rails<br>
								(http://rubygems.org/gems/footable-on-rails , MIT License)
								</p>
								<p>
								-Apache Log4j 1.2.14<br>
								(http://logging.apache.org/log4j/1.2/ , Apache License 2.0)
								</p>
								<p>
								-Apache Commons Text<br>
								(https://mvnrepository.com/artifact/org.apache.commons/commons-text/1.1 , Apache License 2.0)
								</p>
								<p>
								-json-lib 2.2.2<br>
								(http://repo1.maven.org/maven2/net/sf/json-lib/json-lib/ , Apache License 2.0)
								</p>
								<p>
								-pace 1.0.2<br>
								(http://github.com/HubSpot/pace/ , MIT License)
								</p>
								<p>
								-Passay Library 1.1.0<br>
								(http://repo.maven.apache.org/maven2//org/passay/passay/ , GNU Lesser General Public License v2.1 or later)
								</p>
								<p>
								-htmlify master-20130801<br>
								(http://github.com/Nurdok/htmlify/ , MIT License)
								</p>
								<p>
								-metisMenu 2.0.2<br>
								(http://github.com/onokumus/metisMenu/ , MIT License)
								</p>
								<p>
								-bootstrap-datepicker 1.3.0<br>
								(http://github.com/n9/bootstrap-datepicker/ , Apache License 2.0)
								</p>
								<p>
								-mybatis<br>
								(http://code.google.com/p/mybatis/ , Apache License 2.0)
								</p>
								<p>
								-underscore<br>
								(http://github.com/amdjs/underscore/ , MIT License)
								</p>
								<p>
								-mockito 1.9.0<br>
								(http://code.google.com/p/mockito/ , MIT License)
								</p>
								<p>
								-Apache ServiceMix :: Bundles :: spring-context-support<br>
								(http://servicemix.apache.org/bundles-pom/org.apache.servicemix.bundles.spring-context-support/ , Apache License 2.0)
								</p>
								<p>
								-Apache POI<br>
								(http://github.com/orione/POI/ , Apache License 2.0)
								</p>
								<p>
								-SLF4J API Module<br>
								(http://repo1.maven.org/maven2/org/slf4j/slf4j-api/ , MIT License)
								</p>
								<p>								
								-JDOM 2.0.5<br>
								(http://github.com/hunterhacker/jdom/ , Jdom License)
								</p>
								<p>
								-Apache ServiceMix :: Bundles :: spring-aspects<br>
								(http://servicemix.apache.org/bundles-pom/org.apache.servicemix.bundles.spring-aspects/ , Apache License 2.0)
								</p>
								<p>
								-Apache XMLBeans 2.6.0<br>
								(http://xmlbeans.apache.org/ , Apache License 2.0)
								</p>
								<p>
								-jquery 3.1.1<br>
								(https://github.com/jquery/jquery , MIT License)
								</p>
								<p>
								-Apache POI - org.apache.poi:poi-ooxml-schemas 3.16-beta1<br>
								(http://repo.maven.apache.org/maven2//org/apache/poi/poi-ooxml-schemas/ , Apache License 2.0)
								</p>
								<p>
								-bootstrap-css-only 3.3.7<br>
								(https://www.npmjs.org/package/bootstrap-css-only , MIT License)
								</p>
								<p>
								-Logback Core Module<br>
								(http://repo1.maven.org/maven2/ch/qos/logback/logback-core/ , GNU Lesser General Public License v2.1 or later)
								</p>
								<p>
								-cri-lms trunk-20121204-svn<br>
								(http://code.google.com/p/cri-lms/ , MIT License)
								</p>
								<p>
								-Apache POI - org.apache.poi:poi-ooxml 3.16-beta1<br>
								(http://repo.maven.apache.org/maven2//org/apache/poi/poi-ooxml/ , Apache License 2.0)
								</p>
								<p>
								-Commons DBCP 1.4<br>
								(http://commons.apache.org/dbcp/ , Apache License 2.0)
								</p>
								<p>
								-Json-lib4Spring 1.0.2<br>
								(http://repo1.maven.org/maven2/net/sf/json-lib/json-lib-ext-spring/ , Apache License 2.0)
								</p>
								<p>
								-Logback Classic Module<br>
								(http://repo1.maven.org/maven2/ch/qos/logback/logback-classic/ , GNU Lesser General Public License v2.1 or later)
								</p>
								<p>								
								-Apache ServiceMix :: Bundles :: spring-expression<br>
								(http://servicemix.apache.org/bundles-pom/org.apache.servicemix.bundles.spring-expression/ , Apache License 2.0)
								</p>
								<p>
								-EZMorph 1.0.4<br>
								(http://sourceforge.net/projects/ezmorph/ , Apache License 2.0)
								</p>
								<p>
								-AspectJ runtime<br>
								(http://repo1.maven.org/maven2/aspectj/aspectjrt/ , Eclipse Public License 1.0)
								</p>
								<p>
								-Spring Security<br>
								(http://spring.io/spring-security , Apache License 2.0)
								</p>
								<p>
								-AOP Alliance (Java/J2EE AOP standard) 1.0<br>
								(http://aopalliance.sourceforge.net , Public Domain)
								</p>
								<p>
								-Cryptacular Library<br>
								(http://repo.maven.apache.org/maven2//org/cryptacular/cryptacular/ , GNU Lesser General Public License v2.1 or later)
								</p>
								<p>
								-Spring Web Services<br>
								(http://sourceforge.net/projects/springframework/ , Apache License 2.0)
								</p>
								<p>
								-app.io<br>
								(https://www.npmjs.org/package/app.io , MIT License)
								</p>
								<p>
								-Transactions JTA 3.8.0<br>
								(http://www.atomikos.com/ate/transactions-jta/ , Apache License 2.0)
								</p>
								<p>
								-JSON in Java<br>
								(http://www.json.org/java/index.html , JSON License)
								</p>
								<p>
								-jquery-qrcode 1.0<br>
								(http://github.com/gcusnieux/jquery-qrcode/ , MIT License)
								</p>
								<p>
								-Apache Commons Collections 3.2<br>
								(http://commons.apache.org/collections/ , Apache License 2.0)
								</p>
								<p>
								-bootstrap-daterangepicker-dmz 1.3.21<br>
								(http://github.com/m1shan/bootstrap-daterangepicker/ , MIT License)
								</p>
								<p>
								-Guava: Google Core Libraries for Java 20.0<br>
								(https://maven.repository.redhat.com/ga/com/google/guava/guava/ , Apache License 2.0)
								</p>
								<p>
								-HttpCore NIO 4.4.4<br>
								(http://repo.maven.apache.org/maven2/org/apache/httpcomponents/httpcore-nio/ , Apache License 2.0)
								</p>
								<p>
								-Java Cryptography Extension (JCE) Unlimited Strength Jurisdiction Policy 8<br>
								(http://www.oracle.com/technetwork/java/javase/downloads/jce-7-download-432124.html , Oracle Java SE and JavaFX License)
								</p>
								<p>
								-jquery<br>
								(http://github.com/jquery/jquery/ , MIT License)
								</p>
								<p>
								-Bootstrap 3.3.7<br>
								(http://github.com/twbs/bootstrap/ , MIT License)
								</p>
								<p>
								-excanvas.js<br>
								(https://www.npmjs.org/package/excanvas.js , ISC License)
								</p>
								<p>
								-json-simple 1.1.1<br>
								(http://code.google.com/p/json-simple/ , Apache License 2.0)
								</p>
								<p>
								-jquery-bbq 1.2.1<br>
								(http://github.com/cowboy/jquery-bbq/ , MIT License)
								</p>
								<p>
								-Apache Jakarta Slide 1.0.9<br>
								(http://jakarta.apache.org/slide/index.html , Apache License 1.1)
								</p>
								<p>
								-Transactions JDBC 3.8.0<br>
								(http://www.atomikos.com/ate/transactions-jdbc-deprecated/ , Apache License 2.0)
								</p>
								<p>
								-Apache Commons Lang 2.4<br>
								(http://commons.apache.org/proper/commons-lang/ , Apache License 2.0)
								</p>
								<p>
								-JCL 1.1.1 implemented over SLF4J<br>
								(http://repo1.maven.org/maven2/org/slf4j/jcl-over-slf4j/ , Apache License 2.0)
								</p>
								<p>
								<p>								
								-Apache ServiceMix :: Bundles :: spring-webmvc<br>
								(http://servicemix.apache.org/bundles-pom/org.apache.servicemix.bundles.spring-webmvc/ , Apache License 2.0)
								</p>
								<p>
								-Apache Jakarta Commons BeanUtils 1.7.0<br>
								(http://jakarta.apache.org/commons/beanutils/index.html , Apache License 2.0)
								</p>
								<p>
								-MyBatis parent POM 1.3.1<br>
								(http://repo.maven.apache.org/maven2//org/mybatis/mybatis-spring/ , Apache License 2.0)
								</p>
								<p>
								-jackson-databind 2.8.3<br>
								(http://repo.maven.apache.org/maven2//com/fasterxml/jackson/core/jackson-databind/ , Apache License 2.0)
								</p>
								<p>
								-JQuery File Upload Plugin<br>
								(http://www.nuget.org/packages/JQuery_File_Upload_Plugin , MIT License)
								</p>
								<p>
								-jquery-validation 1.16.0<br>
								(http://github.com/jzaefferer/jquery-validation/ , MIT License)
								</p>
								<p>
								-bootstrap-datepicker 1.3.0<br>
								(http://github.com/eternicode/bootstrap-datepicker/ , Apache License 2.0)
								</p>
								<p>
								-Apache Jakarta Commons Codec 1.10<br>
								(http://commons.apache.org/proper/commons-codec/ , Apache License 2.0)
								</p>
								<p>
								-Web Services Description Language for Java Toolkit - WSDL4J 1.6.3<br>
								(http://sourceforge.net/projects/wsdl4j/ , Common Public License 1.0)
								</p>
								<p>
								-wiz-dev-project trunk-20130103-svn<br>
								(http://code.google.com/p/wiz-dev-project/ , Apache License 2.0)
								</p>
								<p>
								-Apache Log4j 1.2.13<br>
								(http://logging.apache.org/log4j/1.2/ , Apache License 2.0)
								</p>
								<p>
								-simple java captcha 1.2.1<br>
								(http://sourceforge.net/projects/simplecaptcha/ , BSD 3-clause "New" or "Revised" License)
								</p>
								<p>
								-HttpAsyncClient 4.1.1<br>
								(http://repo.maven.apache.org/maven2//org/apache/httpcomponents/httpasyncclient/ , Apache License 2.0)
								</p>
								<p>
								-Streaming API for XML (StAX) - JSR-173 1.0.1<br>
								(https://mvnrepository.com/artifact/stax/stax , Apache License 2.0)
								</p>
								<p>
								-AspectJ weaver<br>
								(http://repo1.maven.org/maven2/aspectj/aspectjweaver/ , Eclipse Public License 1.0)
								</p>
								<p>
								-blueimp-gallery-rails<br>
								(http://rubygems.org/gems/blueimp-gallery-rails , MIT License)
								</p>
								<p>
								-aspectj-tools<br>
								(http://repo1.maven.org/maven2/aspectj/aspectj-tools/ , Eclipse Public License 1.0)
								</p>
								<p>
								-Apache Jakarta Commons Logging 1.0.4<br>
								(http://jakarta.apache.org/commons/logging/ , Apache License 2.0)
								</p>
								<p>
								-Apache HttpComponents Core 4.4.4<br>
								(http://repo.maven.apache.org/maven2//org/apache/httpcomponents/httpcomponents-core/ , Apache License 2.0)
								</p>
								<p>
								-bootstrap-datepicker<br>
								(http://github.com/eternicode/bootstrap-datepicker/ , Apache License 2.0)
								</p>
								<p>
								-jasypt: java simplified encryption 1.9.2<br>
								(http://sourceforge.net/projects/jasypt/ , Apache License 2.0)
								</p>
								<p>								
								-Apache ServiceMix :: Bundles :: spring-context<br>
								(http://servicemix.apache.org/bundles-pom/org.apache.servicemix.bundles.spring-context/ , Apache License 2.0)
								</p>
								<p>
								-ubermensch YouTubeMashUp_1.0-svn<br>
								(http://code.google.com/p/ubermensch/ , Apache License 2.0)
								</p>
								<p>
								-Apache Xerces Java 1 XML Parser 2.8.0<br>
								(http://xerces.apache.org/xerces-j/ , Apache License 2.0)
								</p>
								<p>
								-jquery 2.1.1<br>
								(http://www.nuget.org/packages/jQuery , MIT License)
								</p>
								<p>
								-pama-core trunk-20120702-svn<br>
								(http://code.google.com/p/pama-core/ , Apache License 2.0)
								</p>
								<p>
								-FooTable 2.0.3<br>
								(http://github.com/bradvin/FooTable/ , MIT License)
								</p>
								<p>
								-Apache Commons Lang<br>
								(http://commons.apache.org/proper/commons-lang/ , Apache License 2.0)
								</p>
								<p>
								-Apache HttpClient 4.5.2<br>
								(https://hc.apache.org/ , Apache License 2.0)
								</p>
								<p>
								-Spring-aop<br>
								(http://github.com/pigalon/Spring-aop/ , Apache License 2.0)
								</p>
								<p>
								-TransactionsEssentials 3.8.0<br>
								(http://www.atomikos.com/Main/TransactionsEssentials , Apache License 2.0)
								</p>
								<p>
								-Eclipse IDE for Java EE Developers<br>
								(http://www.eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/neon1 , Eclipse Public License 1.0)
								</p>
								<p>
								-unirest-java 1.4.9<br>
								(http://repo.maven.apache.org/maven2//com/mashape/unirest/unirest-java/ , MIT License)
								</p>
								<p>
								-jackson-core<br>
								(http://github.com/FasterXML/jackson-core/ , Apache License 2.0)
								</p>
								<p>
								-jQuery UI - org.webjars:jquery-ui 1.12.1<br>
								(http://repo.maven.apache.org/maven2//org/webjars/jquery-ui/ , MIT License)
								</p>
								<p>
								-JASYPT: Java Simplified Encryption 1.9.2<br>
								(http://www.jasypt.org , Apache License 2.0)
								</p>
								<p>
								-amoy-xmpp-implement trunk-20121111-svn<br>
								(http://code.google.com/p/amoy-xmpp-implement/ , Apache License 2.0)
								</p>
								<p>
								-JCL - Spring<br>
								(http://jcloader.sourceforge.net/jcl-spring , Apache License 2.0)
								</p>
								<p>
								-JCL 1.2 Implemented Over SLF4J<br>
								(http://www.slf4j.org/ , MIT License)
								</p>
								<p>
								-ldapjdk - Netscape Directory SDK for JAVA 4.0<br>
								(http://netscape.com/ , Netscape Public License v1.1)
								</p>
								<p>
								-FindBugs-jsr305 - com.google.code.findbugs:jsr305 1.3.9<br>
								(http://repo1.maven.org/maven2/com/google/code/findbugs/jsr305/ , Apache License 2.0)
								</p>
								<p>
								-Google APIs Client Library for Java<br>
								(http://repo1.maven.org/maven2/com/google/api-client/google-api-client/ , Apache License 2.0)
								</p>
								<p>
								-Guava: Google Core Libraries for Java 20.0<br>
								(https://maven.repository.redhat.com/ga/com/google/guava/guava/ , Apache License 2.0)
								</p>
								<p>
								-Apache ServiceMix :: Bundles :: spring-xml
								(http://repo.maven.apache.org/maven2/org/apache/servicemix/bundles/org.apache.servicemix.bundles.spring-xml/ , Apache License 2.0)
								</p>
								<p>
								-Firebase Dynamic Links API v1-rev962-1.25.0<br>
								(https://mvnrepository.com/artifact/com.google.apis/google-api-services-firebasedynamiclinks/v1-rev912-1.25.0 , Apache License 2.0)
								</p>
								<p>
								-Google OAuth Client Library for Java<br>
								(http://repo1.maven.org/maven2/com/google/oauth-client/google-oauth-client/ , Apache License 2.0)
								</p>
								<p>
								-Spring LDAP 1.2-RC1<br>
								(http://sourceforge.net/projects/springframework/ , Apache License 2.0)
								</p>
								<p>
								-jquery 1.12.4<br>
								(https://www.npmjs.org/package/jquery , MIT License)
								</p>
								<p>
								-CryptoJS 3.1.2<br>
								(http://github.com/sytelus/CryptoJS/ , MIT License)
								</p>
								<p>
								-Apache ServiceMix :: Bundles :: spring-web<br>
								(http://servicemix.apache.org/bundles-pom/org.apache.servicemix.bundles.spring-web/ , Apache License 2.0)
								</p>
								<p>
								-Apache ServiceMix :: Bundles :: spring-orm<br>
								(http://servicemix.apache.org/bundles-pom/org.apache.servicemix.bundles.spring-orm/ , Apache License 2.0)
								</p>
								<p>
								-Apache Jakarta Taglibs 1.2.5<br>
								(http://tomcat.apache.org/taglibs/standard/ , Apache License 2.0)
								</p>
								<p>
								-jqueryrotate-slevomat-fork<br>
								(https://www.npmjs.org/package/jqueryrotate-slevomat-fork , MIT License)
								</p>
								<p>
								-Commons IO 2.5<br>
								(https://maven.repository.redhat.com/ga//commons-io/commons-io/ , Apache License 2.0)
								</p>
								<p>
								-Apache ServiceMix :: Bundles :: spring-oxm<br>
								(http://servicemix.apache.org/bundles-pom/org.apache.servicemix.bundles.spring-oxm/ , Apache License 2.0)
								</p>
								<p>
								-Guava: Google Core Libraries for Java (JDK5 Backport) 17.0<br>
								(http://code.google.com/p/guava-libraries/guava-jdk5 , Apache License 2.0)
								</p>
								<p>
								-postscribe 2.0.8<br>
								(https://www.npmjs.org/package/postscribe , MIT License)
								</p>
								<p>
								-Jackson 2 extensions to the Google HTTP Client Library for Java.<br>
								(http://code.google.com/p/google-http-java-client/google-http-client-jackson2/ , Apache License 2.0)
								</p>
								<p>
								-Google HTTP Client Library for Java - com.google.http-client:google-http-client<br>
								(http://repo1.maven.org/maven2/com/google/api/client/google-api-client/ , Apache License 2.0)
								</p>
								<p>
								-Apache Commons Collections 2.1<br>
								(http://commons.apache.org/collections/ , Apache License 1.1)
								</p>
								<p>
								-SLF4J LOG4J-12 Binding 1.6.1<br>
								(http://repo1.maven.org/maven2/org/slf4j/slf4j-log4j12/ , MIT License)
								</p>
								<p>
								-Apache ORO 2.0.8<br>
								(http://jakarta.apache.org/oro/index.html , Apache License 1.1)
								</p>
								<p>
								-WebWork - opensymphony 2.2<br>
								(http://repo1.maven.org/maven2/opensymphony/webwork/ , Apache License 2.0)
								</p>
								<p>
								-jWebUnit 1.2<br>
								(http://sourceforge.net/projects/jwebunit/ , GNU Lesser General Public License v3.0 or later)
								</p>
								<p>
								-stop 5.1.10<br>
								(http://repo1.maven.org/maven2/jetty/stop/ , Apache License 2.0)
								</p>
								<p>
								-Hibernate Validator 4.2.0.Final<br>
								(http://www.hibernate.org/412.html , Apache License 2.0)
								</p>
								<p>
								-Spring Web - org.springframework:spring-web 3.2.8<br>
								(https://github.com/SpringSource/spring-framework , Apache License 2.0)
								</p>
								<p>
								-Apache Tomcat 8.5<br>
								(http://jakarta.apache.org/tomcat/index.html , Apache License 2.0)
								</p>
								<p>
								-Spring Framework: Beans 3.2.8<br>
								(https://github.com/SpringSource/spring-framework , Apache License 2.0)
								</p>
								<p>
								-syntaxhighlighter 3.0.83<br>
								(http://code.google.com/p/syntaxhighlighter/ , MIT License)
								</p>
								<p>
								-Commons IO 2.2<br>
								(http://commons.apache.org/proper/commons-io/ , Apache License 2.0)
								</p>
								<p>
								-Apache Log4j 1.2.17<br>
								(http://logging.apache.org/log4j/1.2/ , Apache License 2.0)
								</p>
								<p>
								-XStream Library 1.4.1<br>
								(http://xstream.codehaus.org/ , BSD 3-clause "New" or "Revised" License)
								</p>
								<p>
								-Apache HttpClient 4.4.1<br>
								(https://repository.apache.org/content/repositories/releases//org/apache/httpcomponents/httpclient/ , Apache License 2.0)
								</p>
								<p>
								-mybatis 3.1.1<br>
								(http://code.google.com/p/mybatis/ , Apache License 2.0)
								</p>
								<p>
								-Apache Commons Digester 1.6<br>
								(http://jakarta.apache.org/commons/digester/index.html , Apache License 2.0)
								</p>
								<p>
								-Apache Tiles 2.2.2<br>
								(http://tiles.apache.org/ , Apache License 2.0)
								</p>
								<p>
								-Apache XMLBeans 2.3.0<br>
								(http://xmlbeans.apache.org/ , Apache License 2.0)
								</p>
								<p>
								-Apache Jakarta Commons EL 1.0<br>
								(http://commons.apache.org/proper/commons-el/ , Apache License 1.1)
								</p>
								<p>
								-JSCH 0.1.52<br>
								(http://repo.maven.apache.org/maven2//com/jcraft/jsch/ , JSch License)
								</p>
								<p>
								-Apache POI 3.9<br>
								(http://poi.apache.org/ , Apache License 2.0)
								</p>
								<p>
								-JDOM 1.0<br>
								(http://www.jdom.org , Jdom License)
								</p>
								<p>
								-Woodstox 4.4.1<br>
								(http://repo.maven.apache.org/maven2//org/codehaus/woodstox/woodstox-core-asl/ , Apache License 2.0)
								</p>
								<p>
								-OGNL 2.6.5<br>
								(https://commons.apache.org/proper/commons-ognl/ , BSD 3-clause "New" or "Revised" License)
								</p>
								<p>
								-DOM4J - Flexible XML Framework for Java 1.6.1<br>
								(http://dom4j.sourceforge.net , dom4j License (BSD 2.0 +))
								</p>
								<p>
								-javax.annotation API 1.2<br>
								(http://jcp.org/en/jsr/detail?id=250 , Sun GPL With Classpath Exception v2.0)
								</p>
								<p>
								-Apache-Jakarta Velocity Tools 2.0<br>
								(http://velocity.apache.org/tools/devel/ , Apache License 1.1)
								</p>
								<p>
								-MyBatis parent POM 1.2.0<br>
								(http://www.mybatis.org/spring/ , Apache License 2.0)
								</p>
								<p>
								-spring-webmvc 3.2.8<br>
								(https://github.com/SpringSource/spring-framework , Apache License 2.0)
								</p>
								<p>
								-Apache CXF Core 3.0.4<br>
								(http://repo.maven.apache.org/maven2//org/apache/cxf/cxf-core/ , Apache License 2.0)
								</p>
								<p>
								-json-simple 1.1<br>
								(http://code.google.com/p/json-simple/ , Apache License 2.0)
								</p>
								<p>
								-Apache Jakarta Commons Configuration 1.9<br>
								(http://commons.apache.org/configuration/ , Apache License 2.0)
								</p>
								<p>
								-Apache Jakarta Commons Validator 1.3.1<br>
								(http://commons.apache.org/proper/commons-validator , Apache License 2.0)
								</p>
								<p>
								-xml-apis<br>
								(http://repo1.maven.org/maven2/xerces/xmlParserAPIs/ , Apache License 2.0)
								</p>
								<p>
								-Spring Framework: JDBC 3.2.8<br>
								(https://github.com/SpringSource/spring-framework , Apache License 2.0)
								</p>
								<p>
								-Base64Coder - an open-source Base64 encoder decoder in Java r30<br>
								(http://www.source-code.biz/base64coder/java/ , MIT License)
								</p>
								<p>
								-OSCore - opensymphony 2.2.4<br>
								(http://www.opensymphony.com/oscore/ , Open Symphony 1.1 License)
								</p>
								<p>
								-Data Mapper for Jackson 1.9.13<br>
								(http://jackson.codehaus.org , Apache License 2.0)
								</p>
								<p>
								-Spring Modules 0.8<br>
								(http://java.net/projects/springmodules , Apache License 2.0)
								</p>
								<p>
								-Apache Jakarta Velocity 1.6.2<br>
								(http://velocity.apache.org/ , Apache License 2.0)
								</p>
								<p>
								-Common Annotations - JSR-250<br>
								(http://jsr250.dev.java.net/ , Common Development and Distribution License 1.0)
								</p>
								<p>
								-spring-core 3.2.8<br>
								(https://github.com/SpringSource/spring-framework , Apache License 2.0)
								</p>
								<p>
								-Apache CXF Runtime HTTP Transport 3.0.4<br>
								(http://repo.maven.apache.org/maven2//org/apache/cxf/cxf-rt-transports-http/ , Apache License 2.0)
								</p>
								<p>
								-AspectJ weaver 1.7.4<br>
								(http://www.aspectj.org , Eclipse Public License 1.0)
								</p>
								<p>
								-beanvalidation-api 1.0.0.GA<br>
								(http://github.com/beanvalidation/beanvalidation-api/ , Apache License 2.0)
								</p>
								<p>
								-spring-context 3.2.8<br>
								(https://github.com/SpringSource/spring-framework , Apache License 2.0)
								</p>
								<p>								
								-Apache HttpComponents Core 4.4.1<br>
								(http://repo.maven.apache.org/maven2//org/apache/httpcomponents/httpcomponents-core/ , Apache License 2.0)
								</p>
								<p>
								-spring-expression 3.2.8<br>
								(https://github.com/SpringSource/spring-framework , Apache License 2.0)
								</p>
								<p>
								-DD_belatedPNG 0.0.8a<br>
								(http://www.dillerdesign.com/experiment/DD_belatedPNG/ , MIT License)
								</p>
								<p>
								-Spring Framework: ORM 3.2.8<br>
								(https://github.com/SpringSource/spring-framework , Apache License 2.0)
								</p>
								<p>
								-Apache CXF Runtime JAX-RS Frontend 3.0.4<br>
								(http://repo.maven.apache.org/maven2//org/apache/cxf/cxf-rt-frontend-jaxrs/ , Apache License 2.0)
								</p>
								<p>
								-Apache Commons FileUpload<br>
								(http://jakarta.apache.org/commons/fileupload/ , Apache License 2.0)
								</p>
								<p>
								-JsUnit 2.2<br>
								(http://sourceforge.net/projects/jsunit/ , Mozilla Public License 1.1)
								</p>
								<p>
								-Jackson 1.9.13<br>
								(http://jackson.codehaus.org , Apache License 2.0)
								</p>
								<p>
								-javax.ws.rs-api 2.0.1<br>
								(http://repo.maven.apache.org/maven2//javax/ws/rs/javax.ws.rs-api/ , Sun GPL With Classpath Exception v2.0)
								</p>
								<p>
								-Spring Framework: AOP 3.2.8<br>
								(https://github.com/SpringSource/spring-framework , Apache License 2.0)
								</p>
								<p>
								-Commons DBCP 1.3<br>
								(http://commons.apache.org/dbcp/ , Apache License 2.0)
								</p>
								<p>
								-XML Pull Parsing API parent<br>
								(https://maven.repository.redhat.com/ga//xmlpull/xmlpull-parent/ , Public Domain)
								</p>
								<p>
								-Daum open editor 7.2.6<br>
								(http://code.google.com/p/daumopeneditor/ , GNU Lesser General Public License v3.0 or later)
								</p>
								<p>
								-Mozilla Rhino: JavaScript for Java 1.5R4.1<br>
								(http://www.mozilla.org/rhino/ , Mozilla Public License 2.0)
								</p>
								<p>
								-Apache Jakarta Commons Codec 1.5<br>
								(http://jakarta.apache.org/commons/codec/ , Apache License 2.0)
								</p>
								<p>
								-RIFE/Continuations 0.0.1<br>
								(http://repo1.maven.org/maven2/org/rifers/rife-continuations/ , GNU Lesser General Public License v2.1 or later)
								</p>
								<p>
								-Apache HttpClient 4.1.2<br>
								(http://jakarta.apache.org/commons/httpclient/ , Apache License 2.0)
								</p>
								<p>
								-XPP - XML Pull Parser 3-1.1.4c<br>
								(http://www.extreme.indiana.edu/xgws/xsoap/xpp/ , Indiana University Extreme! Lab Software License)
								</p>
								<p>
								-SLF4J API Module 1.6.1<br>
								(http://repo1.maven.org/maven2/org/slf4j/slf4j-api/ , MIT License)
								</p>
								<p>
								-Spring Framework: Transaction 3.2.8<br>
								(https://github.com/SpringSource/spring-framework , Apache License 2.0)
								</p>
								<p>
								-JUnit 3.8.1<br>
								(http://www.junit.org/ , Common Public License 1.0)
								</p>
								<p>
								-start 5.1.10<br>
								(http://repo1.maven.org/maven2/jetty/start/ , Apache License 2.0)
								</p>
								<p>
								-httpunit 1.5.4<br>
								(http://sourceforge.net/projects/httpunit/ , MIT License)
								</p>
								<p>
								-JSON in Java 1.0.0<br>
								(http://repo1.maven.org/maven2/com/unboundid/components/json/ , JSON License)
								</p>
								<p>
								-XWork 1.1<br>
								(http://repo1.maven.org/maven2/opensymphony/xwork/ , Apache License 2.0)
								</p>
								<p>
								-Stax2 API 3.1.4<br>
								(http://wiki.fasterxml.com/WoodstoxStax2 , BSD 2-clause "Simplified" License)
								</p>
								<p>
								-XmlSchema Core 2.2.1<br>
								(http://repo.maven.apache.org/maven2//org/apache/ws/xmlschema/xmlschema-core/ , Apache License 2.0)
								</p>
								<p>
								-jquery 3.4.1<br>
								(http://github.com/jquery/jquery/ , MIT License)
								</p>
								<p>
								-Project Lombok<br>
								(http://projectlombok.org , MIT License)
								</p>
								<p>
								-RxJava<br>
								(http://github.com/Netflix/RxJava/ , Apache License 2.0)
								</p>
								<p>
								-Android Arch Common<br>
								(https://mvnrepository.com/artifact/androidx.arch.core/core-common/2.0.0 , Apache License 2.0)
								</p>
								<p>
								-Android Lifecycle Common<br>
								(https://mvnrepository.com/artifact/androidx.lifecycle/lifecycle-common/2.0.0 , Apache License 2.0)
								</p>
								<p>
								-OkHttp Logging Interceptor<br>
								(https://mvnrepository.com/artifact/com.squareup.okhttp3/logging-interceptor/4.0.0 , Apache License 2.0)
								</p>
								<p>
								-AutoValue Annotations<br>
								(http://repo.maven.apache.org/maven2//com/jakewharton/auto/value/auto-value-annotations/ , Apache License 2.0)
								</p>
								<p>
								-Adapter: RxJava 2<br>
								(https://mvnrepository.com/artifact/com.squareup.retrofit2/adapter-rxjava2/2.6.2 , Apache License 2.0)
								</p>
								<p>
								-google-gson<br>
								(http://code.google.com/p/google-gson/ , Apache License 2.0)
								</p>
								<p>
								-annotations 13.0<br>
								(http://www.jetbrains.org , Apache License 2.0)
								</p>
								<p>
								-ORM Lite 5.0<br>
								(http://repo.maven.apache.org/maven2//com/j256/ormlite/ormlite-jdbc/ , ISC License)
								</p>
								<p>
								-Converter: Gson<br>
								(http://repo.maven.apache.org/maven2//com/squareup/retrofit/converter-gson/ , Apache License 2.0)
								</p>
								<p>
								-Android Support Library Collections<br>
								(https://mvnrepository.com/artifact/androidx.collection/collection/1.0.0 , Apache License 2.0)
								</p>
								<p>
								-Glide Disk LRU Cache Library<br>
								(https://mvnrepository.com/artifact/com.github.bumptech.glide/disklrucache/4.10.0 , Apache License 2.0)
								</p>
								<p>
								-react-native-fetch-blob<br>
								(https://www.npmjs.org/package/react-native-fetch-blob , MIT License)
								</p>
								<p>
								-OkHttp URLConnection<br>
								(https://github.com/square/okhttp/okhttp-urlconnection , Apache License 2.0)
								</p>
								<p>
								-Kotlin Standard Library<br>
								(https://mvnrepository.com/artifact/org.jetbrains.kotlin/kotlin-stdlib/1.3.40 , Apache License 2.0)
								</p>
								<p>
								-Android ConstraintLayout Solver<br>
								(https://mvnrepository.com/artifact/androidx.constraintlayout/constraintlayout-solver/1.1.3 , Apache License 2.0)
								</p>
								<p>
								-Glide Annotations<br>
								(https://mvnrepository.com/artifact/com.github.bumptech.glide/annotations/4.10.0 , Apache License 2.0)
								</p>
								<p>
								-reactive-streams<br>
								(http://github.com/reactive-streams/reactive-streams/ , Creative Commons Zero v1.0 Universal)
								</p>
								<p>
								-Android - platform - frameworks - base<br>
								(http://developer.android.com/index.html , Apache License 2.0)
								</p>
								<p>
								-okhttp<br>
								(http://code.google.com/p/okhttp/ , Apache License 2.0)
								</p>
								<p>
								-react-native-taobao-baichuan-api<br>
								(https://www.npmjs.org/package/react-native-taobao-baichuan-api , ISC License)
								</p>
								<p>
								-gradle-grails-wrapper<br>
								(http://mirrors.ibiblio.org/maven2/com/connorgarvey/gradle/gradle-grails-wrapper/ , Apache License 2.0)
								</p>
								<p>
								-Android Studio<br>
								(https://developer.android.com/sdk/installing/bundle.html , Apache License 2.0)
								</p>
								<p>
								-Retrofit<br>
								(http://github.com/square/retrofit/retrofit/ , Apache License 2.0)
								</p>
								<p>
								-Retrofit Mock Adapter<br>
								(http://github.com/square/retrofit/retrofit-mock/ , Apache License 2.0)
								</p>
								<p>
								-Converter: Java Scalars<br>
								(http://repo.maven.apache.org/maven2//com/squareup/retrofit2/converter-scalars/ , Apache License 2.0)
								</p>
								<p>
								-Kotlin Standard Library Common<br>
								(https://mvnrepository.com/artifact/org.jetbrains.kotlin/kotlin-stdlib-common/1.3.40 , Apache License 2.0)
								</p>
								<p>
								-ORMLite Android<br>
								(https://mvnrepository.com/artifact/com.j256.ormlite/ormlite-android/5.0 , ISC License)
								</p>
								<p>
								-Data Binding Base Library<br>
								(https://mvnrepository.com/artifact/androidx.databinding/databinding-common/3.5.0 , Apache License 2.0)
								</p>
								<p>
								-ZXing Java SE extensions 3.3.0<br>
								(http://repo.maven.apache.org/maven2//com/google/zxing/javase/ , Apache License 2.0)                  
                                </p>
                            </div>
                        </div>
                        
                        
                        
                    </div>
                    
                    
                    
                </div>
            </div>
        </div>
        
        <script src="/test/js/vendor/jquery-1.11.2.min.js"></script>
        <script src="/test/js/owl.carousel.min.js"></script>
        <script src="/test/js/vendor/bootstrap.min.js"></script>
        <script src="/test/js/main.js"></script>
        <script src="/js/alert.js"></script>
        <script src="/js/vendor/jquery.loading-indicator.min.js"></script>
        <script src="/js/animsition.min.js"></script>
        <script>

        var homeLoader;
        
            $(document).ready(function(){
                
                $('.reg-select select').on('change', function(){
                    var selVal = $(this).val();
                    $(this).closest('.reg-select').find('span').text(selVal);
                });
                $('.reg-type').on('click',function(){
                    var isShow = $(this).data('show');
                    if(isShow){
                        $('.ref-full-member').show();
                    }else{
                        $('.ref-full-member').hide();
                    }
                });

            	$(".animsition").animsition({
        		    inClass: 'zoom-in-lg',
        		    outClass: 'zoom-out-lg',
        		    inDuration: 1500,
        		    outDuration: 800,
        		    linkElement: '.animsition-link',
        		    // e.g. linkElement: 'a:not([target="_blank"]):not([href^="#"])'
        		    loading: true,
        		    loadingParentElement: 'body', //animsition wrapper element
        		    loadingClass: 'animsition-loading',
        		    loadingInner: '', // e.g '<img src="loading.svg" />'
        		    timeout: false,
        		    timeoutCountdown: 5000,
        		    onLoadEvent: true,
        		    browser: [ 'animation-duration', '-webkit-animation-duration'],
        		    // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
        		    // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
        		    overlay : false,
        		    overlayClass : 'animsition-overlay-slide',
        		    overlayParentElement : 'body',
        		    transition: function(url){ window.location.href = url; }
        		  });


            	homeLoader = $('body').loadingIndicator({
					useImage: false,
					showOnInit : false
				}).data("loadingIndicator");


                
            });


            function goSingUpPage(){

            	if(!$('input:checkbox[id="acceptTerms"]').is(":checked")){
            		$.alert("이용약관에 동의 해 주세요.");
            		return false;
            	}
            	
            	if(!$('input:checkbox[id="acceptPersonal"]').is(":checked")){
            		$.alert("개인정보 수집 및 이용에 동의 해 주세요.");
            		return false;
            	}

            	$.confirm("회원 가입을 진행 하시겠습니까?",function(a){
           		 if(a){
           			document.location.href='/carrier/simpleSign-Up.do?&customerId=${paramMap.customerId}&chargeId=${paramMap.chargeId}&phoneNum=${paramMap.phoneNum}&optionType=${paramMap.optionType}';
           		           	
           		 }
           		});

            }


            
        </script>
        
        
        <!-- </div> -->
        
        
    </body>
</html>

