<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko"  style=" height:100%;">      
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
 <link rel="stylesheet" href="/css/animsition.min.css">
<body style=" height:100%;">


<script type="text/javascript">  

$(document).ready(function(){
	
/* 	var updateToken = setInterval( function() {
		
		if(getDeviceToken() != null && getDeviceToken() != ""){
			updateDriverDeviceToken(getDeviceToken());
			if(window.Android != null){
				window.Android.startService();
			}
			clearInterval(updateToken);
		}
   }, 1000); */
	
});



function getDeviceToken(){
	
	var token = "";
	try{
		if(window.Android != null){
			token = window.Android.getDeviceToken("${driverId}");
		}	
	}catch(exception){
		
	}finally{
		
	}
	return token;
}  
  

/* function updateDriverDeviceToken(token){
	
	
	if(token != ""){
		$.ajax({ 
			type: 'post' ,
			url : "/updateDriverDeviceToken.do" ,
			dataType : 'json' ,
			data : {
				token : token
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					//$.alert("성공",function(a){
					//});
				}else if(result == "E000"){
					
				}else{
					
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
}  
 */


 function getNewsData(obj,keyword){
	
	 
	// alert($("#selectedStatus").children().length);
	 
	//alert($("#selectedStatus").children().first().get(0));	
	//alert($(obj).parent().get(0));
	
 	 if($("#selectedStatus").children().first().get(0) == $(obj).parent().get(0)){				//전체를 클릭 한경우
					
 		$("#selectedStatus").children().each(function(index,element){
 			$(this).removeClass('selected');
 		});
 		$(obj).parent().addClass('selected');
	}else{
			
	} 
	
	
	
/* 	
	
	$.ajax({ 
		type: 'post' ,
		url : "/research/newsSearch.do?&keyword="+keyword ,
		dataType : 'json' ,
		data : {
			
		},
		success : function(data, textStatus, jqXHR)
		{
			alert(data);
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
	 */
	
	
} 

 function goNewsPage(uri){
		
		//var replaceUrlStr = replaceAll(url, "?", "^");
		//replaceUrlStr = replaceAll(replaceUrlStr, "=", "+");
		
		var replaceUrlStr = encodeURIComponent(uri)
		document.location.href = "/news/newsPage.do?newsUri="+replaceUrlStr;
	}  


    
function viewAllocationData(allocationId,allocationStatus){
	homeLoader.show();
	document.location.href = "/carrier/allocation-detail.do?allocationStatus="+allocationStatus+"&allocationId="+allocationId;

	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
} 
 
 
function viewAllocationList(allocationId,allocationStatus){
	homeLoader.show();
	
	document.location.href = "/carrier/group-list.do?allocationId="+allocationId+"&allocationStatus="+allocationStatus;

	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
	
} 
 
 
 
function logout(){
	
	$.confirm("로그아웃 하시겠습니까?",function(a){
		 if(a){
			 if(window.Android != null){
					window.Android.stopService("${driverId}");
				}	
			 homeLoader.show();
			 document.location.href = "/logout.do";	
		 }
	});
	
}   

var selectedObj = new Object();

function sendMessage(phoneNum,customerId,chargeId){

	/* var phoneNum = $(obj).prev().find("P").html();
	var customerId = */
	selectedObj = new Object();
	selectedObj.phoneNum = phoneNum; 
	selectedObj.customerId = customerId;
	selectedObj.chargeId = chargeId;
	
	$.confirm("이 번호로 인증번호를 전송하시겠습니까?",function(a){

		 if(a){

				$.ajax({
					type : 'post',
					url : '/carrier/insertCertNumForAPP.do',
					dataType : 'json',
					data : {

						phoneNum : phoneNum,
						customerId : customerId
					
						
					},success : function(data, textStatus, jqXHR) {
				         var result = data.resultCode;
				         	
							if(result == "0000") {

								$(".certNum").css('display','block');											
							
								}else if (result == "0002") { 
								$.alert("선택한 핸드폰번호로 가입된 아이디가 있습니다.", function(a) {
			
							});	
						}  
					},
					error : function(xhRequest, ErrorText, thrownError) {
						
					}
				});
				
			 
				

				 	
		 }else{

			/* $.alert("취소",function(a){
			}); */
		}
	});
}
	


	
function sendPinNum(){


	var phoneCertNum = $("#phoneCertNum").val();

	
	if(phoneCertNum != ""){

				$.ajax({
					type : 'post',
					url  : "/carrier/checkCertNumForLogin.do",
				    dataType : 'json',
				    
				       data : {
					       
				    	
						phoneNum : selectedObj.phoneNum,
						customerId : selectedObj.customerId,
						phoneCertNum :phoneCertNum,
						chargeId : selectedObj.chargeId
						},

				   success : function(data, textStatus, jqXHR)
				    {
					   var resultCode = data.resultCode;
					   if(resultCode == "0000"){
						 
							$.alert("인증에 성공하였습니다.", function(a) {

								goSingUpPage();
							});	
						 
					   }else{
						 $.alert("인증번호가 맞지 않습니다. 관리자에게 문의하세요.");
					   }
				   } ,
				  error :function(xhRequest, ErrorText, thrownError){
					  $.alert('알수 없는 오류입니다. 관리자에게 문의 하세요.');
	                  
					  }
				});
	
		}else{

			$.alert("인증번호가 입력되지 않았습니다.");
			return false;
			}
}


function goSingUpPage(){

	document.location.href='/carrier/simpleSign-Up.do?&customerId='+selectedObj.customerId+'&chargeId='+selectedObj.chargeId+'&phoneNum='+selectedObj.phoneNum;
	
}

 
function backKeyController(str){

	//history.go(-1);

	document.location.href ="/carrier/login-page.do?&optionType=C";
	
	//location.href = document.referrer;
/* 	 if(window.Android != null){
			window.Android.toastShort("뒤로가기 버튼으로 앱을 종료 할 수 없습니다.");
		} */
}


	function goMainPage(allocationId){

		document.location.href ="/carrier/main.do?&allocationId="+allocationId;
	}


	function goLoginPage(){


		document.location.href ="/carrier/login-page.do?&optionType=C";
	
	}



</script>


        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
  <div class="content-container" style=" height:100%; overflow-y:hidden;">
  	<div class="animsition">
            <%-- <jsp:include page="/WEB-INF/views/jsp/common-top.jsp"></jsp:include> --%>
            <header class="bg-pink clearfix" style="position:fixed; background-color:#fff; border:none; text-align:center; ">
                <div class="" style="width:19%;">
                    <a onclick="javascript:goLoginPage();"><img style="zoom: 0.5;" src="/img/back-icon.png" alt=""></a> 
                </div> 
                <div class="" style="width:60%;">
                	<img style="width:90%; height:60%;" onclick="javascript:homeLoader.show(); document.location.href='/carrier/main.do'" src="/img/main_logo.png" alt="">
                </div>
               <div class="" style="width:19%; float:right;">
                   <!-- <a style="cursors:pointer;" href="javascript:logout();"><img style="" src="/img/logout.png" alt=""></a> -->
                </div> 
            </header>
            
            <%-- <div style="text-align:right;">${user.driver_name}님 환영 합니다.</div> --%>
    		<%-- <div style=" text-align:right; clear:both;">&nbsp;</div>
            <br>
	              <div style="width:100%; margin-top:20px; height:40px; ">
                   <div style="width:50%; text-align:center; height:40px; float:left;">
                   <div style="margin-top:10px; text-align:center; width :40%; height:50%;display:inline-block;">거래처명 :</div> ${paramMap.customerName}</div>
                </div>  --%>
		        
            <div style=" text-align:right; clear:both;">&nbsp;</div>
            <br>
	              <%-- <div style="width:100%; margin-top:20px; height:40px; ">
                   <div style="width:50%; text-align:center; height:40px; float:left;">
                   <div style="margin-top:10px; text-align:center; width :40%; height:50%;display:inline-block;">거래처명 :</div> ${paramMap.customerName}</div>
                </div>  --%>
            <div class="content-container interest-page loadingthemepage news" style="position:fixed; background-color:#fff; clear:both; margin-top:50px;">
	            <!-- <div class="cat-list" style="background-color:#fff">
	                <ul class="clearfix" style="background-color:#fff">
	                	<li><a style="color:#000; width:15%;">출발일</a></li>
	                    <li><a style="color:#000;">상차지</a></li>
	                    <li><a style="color:#000;">하차지</a></li>
	                    <li><a style="color:#000;">차종</a></li>
	                    <li><a style="color:#000;">차대번호</a></li>
	                </ul>
	            </div> -->
	            
	            <div style="width:100%; margin-top:5px; height:40px; border-bottom:1px solid #eee;">
                   <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">차대번호</div></div>
	               <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">차량번호</div></div>
	               <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">차종</div></div>
	               <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">상차지</div></div>
	               <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">하차지</div></div>
                </div>
            </div>
            <div id="container" class="search-result news-list content-box" style="height:100%; margin-top:28%; overflow-y:scroll;">
            	<div class="xpull">
				    <div class="xpull__start-msg">
				        <div class="xpull__start-msg-text">화면을 당겼다 놓으면 새로 고침 됩니다.</div>
				        <div class="xpull__arrow"></div>
				    </div>
				    <div class="xpull__spinner">
				        <div class="xpull__spinner-circle"></div>
				    </div>
				</div>
            	<div class="news-container clearfix" style="">
    	       		<c:forEach var="data" items="${listData}" varStatus="status" >
    	       			
	    	       		<%-- <c:if test="${data.groupCarCnt == 1}"> --%>
			           		<%-- <div style="width:100%; margin-top:5px; height:40px; border-bottom:1px solid #eee;"  onclick="javascript:viewAllocationData('${data.allocation_id}','${data.allocation_status_cd}');"> --%>
				                <div  style="width:20%; text-align:center; height:50px; padding:2%; float:left; display:inline-block;" onclick="javascript:goMainPage('${data.allocation_id}');"> 
				              		 <c:if test ="${data.car_id_num eq ''}">
						        			 -
						        	 </c:if>
							        <c:if test ="${data.car_id_num ne ''}">
						        	   ${data.car_id_num}
						        	</c:if>
				                </div>
				                
				                <div style="width:20%; text-align:center; height:50px; padding:2%; float:left; display:inline-block;"onclick="javascript:goMainPage('${data.allocation_id}');">
				               		 <c:if test ="${data.car_num eq ''}">
					        			-
					        		 </c:if>
					          		<c:if test ="${data.car_num ne ''}">
					        			  ${data.car_num}
					        		</c:if>
				                </div>
				                
					             <div style="width:20%; text-align:center; height:50px; padding:2%; float:left; display:inline-block;"onclick="javascript:goMainPage('${data.allocation_id}');">
					             ${data.car_kind}</div>
					             
					             <div  style="width:20%; text-align:center; height:50px;  padding:2%; float:left; display:inline-block;"onclick="javascript:goMainPage('${data.allocation_id}');">
					             ${data.departure}</div>
					             
								<div  style="width:20%; text-align:center; height:50px;  padding:2%; float:left; display:inline-block;"onclick="javascript:goMainPage('${data.allocation_id}');">
					             ${data.arrival}</div>
					            <%-- <div style="width:20%; text-align:center; height:40px; float:left;"><div style="width:100%; margin-top:10px; text-align:center; display:inline-block; text-overflow:ellipsis; white-space:nowrap; overflow:hidden;"><nobr>${data.phone_num}</nobr></div></div> --%>
			               
			                
			        <%--      </c:if> --%>
			              <%-- <c:if test="${data.groupCarCnt > 1}"> --%>
<%-- 			           		<div style="width:100%; margin-top:5px; height:40px; border-bottom:1px solid #eee;"  onclick="javascript:viewAllocationList('${data.allocation_id}','${data.allocation_status_cd}');">
 --%>				             <%--    <div style="width:20%; text-align:center; height:40px; float:left;">
 									<div style="margin-top:10px; text-align:center; display:inline-block;">${data.name}</div></div>
			                		<div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">${data.phone_num}</div></div> --%>
					
					<%--             <div style="width:80%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">${data.phone_num}회전</div></div> --%>
			         <%--     </c:if> --%>
	               
	                     </c:forEach>
	                
			                </div>
	                
	    
       
                 </div>
            </div>
         </div>
<!--         </div>
 -->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      <script src="/js/vendor/bootstrap.min.js"></script>       
    <!-- veiwport for countnumber -->
    <script src="/js/alert.js"></script>
    <script src="/js/main.js"></script>   
    <script src="/js/vendor/xpull.js"></script>
    <script src="/js/animsition.min.js"></script>      
    <script src="/js/vendor/jquery.loading-indicator.min.js"></script>
    
    <script>
    
	
	$('#container').xpull({
	    'paused': false, 
	    // Pull threshold - amount in  pixels required to pull to enable release callback
        'pullThreshold':50,
        // Max pull down element - amount in pixels
        maxPullThreshold: 50,
        // timeout in miliseconds after which the loading indicator stops spinning.
        // If set to 0 - the loading will be indefinite
        'spinnerTimeout':1000,
        onPullStart: function(){},
        onPullEnd: function(){},
        callback: function(){window.location.reload()},

	});   
	
	
	var homeLoader;
	
	$(document).ready(function(){
			
		
				homeLoader = $('body').loadingIndicator({
					useImage: false,
					showOnInit : false
				}).data("loadingIndicator");
				
				/* setTimeout(function() {
					homeLoader.show();
					homeLoader.hide();
				}, 1000); */

				$(".animsition").animsition({
				    inClass: 'zoom-in-lg',
				    outClass: 'zoom-out-lg',
				    inDuration: 1500,
				    outDuration: 800,
				    linkElement: '.animsition-link',
				    // e.g. linkElement: 'a:not([target="_blank"]):not([href^="#"])'
				    loading: true,
				    loadingParentElement: 'body', //animsition wrapper element
				    loadingClass: 'animsition-loading',
				    loadingInner: '', // e.g '<img src="loading.svg" />'
				    timeout: false,
				    timeoutCountdown: 5000,
				    onLoadEvent: true,
				    browser: [ 'animation-duration', '-webkit-animation-duration'],
				    // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
				    // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
				    overlay : false,
				    overlayClass : 'animsition-overlay-slide',
				    overlayParentElement : 'body',
				    transition: function(url){ window.location.href = url; }
				  });

				
		
	});
    
    </script>
        
    </body>
</html>
