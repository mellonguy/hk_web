<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko"  style=" height:100%;">      
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
<link rel="stylesheet" href="/css/animsition.min.css">
  <meta name="viewport" content="width=device-width, user-scalable=no">
<body style=" height:100%;">


<script type="text/javascript">  
var pageMoveStop = true;
var lastPage = false;

$(document).ready(function(){
/* 	 	var updateToken = setInterval(function() {
		clearInterval(updateToken);
		pageMoveStop = false;
		homeLoader.hide();

	}, 300);  */


		
		$(window).scroll(function (){
			if(pageMoveStop && !lastPage){
				if($(window).scrollTop()+100 >= $(document).height() - $(window).height()){
					pageMoveStop = false;			 
					homeLoader.show();
					setTimeout(function(){
						getDataList();
						homeLoader.hide();
						pageMoveStop = true;  
			        }, 300);
					
					
				}
			}
		});

				
	  



});

var curpage = 2;

function getDataList(){


			$.ajax({ 
				type: 'post' ,
				url : "/carrier/getFinishInfoDataList.do" ,
				dataType : 'json' ,
				async : false,
				data : {
					cPage : curpage,
					searchWord : "${paramMap.searchWord}",
					customerId : "",
					
				},
				success : function(data, textStatus, jqXHR)
				{
					
					var result = data.resultCode;
					var resultData = data.resultData;
					var resultDataSub = data.resultDataSub;
						
					if(result == "0000"){

						if(curpage >= resultDataSub.totalPage){
							$.alert("마지막페이지입니다.");
							lastPage = true;
						}
						
						var result = "";
						curpage++;
						for(var i = 0;i < resultData.length; i++){
					
					
							result += '<div style="width:100%; margin-top:5%; height:40px; border-bottom:1px solid #eee;"  onclick="javascript:viewAllocationData(\'F\',\''+resultData[i].allocation_id+'\');">';
							result += '<div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">'+resultData[i].departureDt+'</div></div>';
							result += '<div style="width:20%; text-align:center; height:40px; float:left;">';
		
								if(resultData[i].car_id_num == ""){
									result += '<div style="margin-top:10px; text-align:center; display:inline-block;">-</div>';
								}else{
									result += '<div style="margin-top:10px; text-align:center; display:inline-block;">'+resultData[i].car_id_num+'</div>';
								}
								result += '</div>';
								result += '<div style="width:20%; text-align:center; height:40px; float:left;">';	

								if(resultData[i].car_num ==""){
									result += '<div style="margin-top:10px; text-align:center; display:inline-block;">-</div>';
								}else{

									result += '<div style="margin-top:10px; text-align:center; display:inline-block;">'+resultData[i].car_num+'</div>';

									}

								result += '</div>';
								result += '<div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block; width:100%; white-space:nowrap; text-overflow:ellipsis; overflow:hidden;">'+resultData[i].departure+'</div></div>';
								result += '<div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block; width:100%; white-space:nowrap; text-overflow:ellipsis; overflow:hidden;">'+resultData[i].arrival+'</div></div>';				
								result += '</div>';


								
								
					}


					$("#container").append(result);
				
					
	
					}else if(result == "E000"){
						
					}else{
						
					}

					
				} ,
				error : function(xhRequest, ErrorText, thrownError) {
					
				}
			});	
			


	//
	
}

function getDeviceToken(){
	
	var token = "";
	try{
		if(window.Android != null){
			token = window.Android.getDeviceToken("${driverId}");
		}	
	}catch(exception){
		
	}finally{
		
	}
	return token;
}  
  
/* 
function updateDriverDeviceToken(token){
	
	
	if(token != ""){
		$.ajax({ 
			type: 'post' ,
			url : "/updateDriverDeviceToken.do" ,
			dataType : 'json' ,
			data : {
				token : token
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					//$.alert("성공",function(a){
					//});
				}else if(result == "E000"){
					
				}else{
					
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
}  
 */



 

 function getNewsData(obj,keyword){
	
	 
	// alert($("#selectedStatus").children().length);
	 
	//alert($("#selectedStatus").children().first().get(0));	
	//alert($(obj).parent().get(0));
	
 	 if($("#selectedStatus").children().first().get(0) == $(obj).parent().get(0)){				//전체를 클릭 한경우
					
 		$("#selectedStatus").children().each(function(index,element){
 			$(this).removeClass('selected');
 		});
 		$(obj).parent().addClass('selected');
	}else{
			
	} 
	
	
	
/* 	
	
	$.ajax({ 
		type: 'post' ,
		url : "/research/newsSearch.do?&keyword="+keyword ,
		dataType : 'json' ,
		data : {
			
		},
		success : function(data, textStatus, jqXHR)
		{
			alert(data);
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
	 */
	
	
} 

 function goNewsPage(uri){
		
		//var replaceUrlStr = replaceAll(url, "?", "^");
		//replaceUrlStr = replaceAll(replaceUrlStr, "=", "+");
		
		var replaceUrlStr = encodeURIComponent(uri)
		document.location.href = "/news/newsPage.do?newsUri="+replaceUrlStr;
	}  


    
function viewAllocationData(allocationStatus,allocationId){
	
	homeLoader.show();
	
	document.location.href = "/carrier/allocation-detail.do?&allocationStatus="+allocationStatus+"&allocationId="+allocationId;

	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
} 
 
 
function viewAllocationList(allocationId,allocationStatus){
	
	homeLoader.show();
	document.location.href = "/carrier/group-list.do?allocationId="+allocationId+"&allocationStatus="+allocationStatus;

	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
	
} 
 
 
 
function logout(){
	
	$.confirm("로그아웃 하시겠습니까?",function(a){
		 if(a){
			 homeLoader.show();
			 if(window.Android != null){
					window.Android.stopService("${driverId}");
				}	
			 	document.location.href = "/logout.do";	
		 }
	});
	
}   
 
 
 
function backKeyController(str){
	//history.go(-1);
	//location.href = document.referrer;
	homeLoader.show();
	document.location.href ="/carrier/finish-list.do";

	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
}
 
 
 
function viewItem(decideMonth,item){
	homeLoader.show();
	document.location.href = "/carrier/cal-item.do?decideMonth="+decideMonth+"&item="+item;

	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
} 
 
 
	function selectTypeChange(searchWord,searchType){

	 // homeLoader.show();
	 	homeLoader.show();
		document.location.href = "/carrier/cal-list.do?&&searchWord="+"${paramMap.searchWord}"+"&searchType="+$("#searchType").val();

		var updateHomeLoader = setInterval(function() {
			clearInterval(updateHomeLoader);
			homeLoader.hide();
		}, 300);

	 }
 

 
</script>


        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div class="content-container" style=" height:auto; overflow-y:hidden;">
            <%-- <jsp:include page="/WEB-INF/views/jsp/common-top.jsp"></jsp:include> --%>
            <header class="bg-pink clearfix" style="position:fixed; background-color:#fff; border:none; text-align:center; ">
                <div class="" style="width:19%;">
                    <!-- <a onclick="javascript:history.go(-1);"><img style="zoom: 0.5;" src="/img/back-icon.png" alt=""></a> -->
                    
                    <a onclick="javascript:history.go(-1);"><img style="zoom: 0.5;" src="/img/back-icon.png" alt=""></a>
                    
                    <!-- location.href = document.referrer; -->
                     
                </div> 
                <div class="" style="width:60%;">
                	<img style="width:90%; height:60%;" onclick="javascript:document.location.href='/carrier/main.do'" src="/img/main_logo.png" alt="">
                </div>
                <div class="" style="width:19%; float:right;">
                
                   <a style="cursor:pointer;" href="javascript:logout();"><img style="" src="/img/logout.png" alt=""></a>
               
                </div>
            </header>

            <%-- <div style="text-align:right;">${user.driver_name}님 환영 합니다.</div> --%>
            <div style=" text-align:right; clear:both;">&nbsp;</div>
                            
            <div class="content-container interest-page loadingthemepage news" style="position:fixed; background-color:#fff; clear:both; margin-top:25px;">
                            <div style=" text-align:right; clear:both; margin-top:3%;">${customer.customer_name}&nbsp;${paramMap.searchWord}월 &nbsp;<br>탁송완료수 : &nbsp;<strong><span  style="color: #00f;">${allocationCount.cnt}</span></strong>&nbsp;건입니다.&nbsp;</div>
	              <br>
	              <div class="input-append date" style="">
						<div style="display:inline; width:15%;">&nbsp;&nbsp;검색</div>
						<div style="display:inline; width:40%;">:&nbsp;&nbsp;<input style="width:60%; text-align:center; padding:1%;"  placeholder="차량번호/차대번호/상차지/하차지" type="text" class="span2" id="searchType" value="${paramMap.searchType}"><span class="add-on"><i class="icon-th"></i></span></div>
						<div class="menu-container" style="display:inline; width:10%; text-align:right; clear:both;  padding:0;">
							<a class="kakao txt-bold" style="width:19%; display:inline-block; text-align:right; line-height:32px; cursor:pointer; margin-top:0px; margin-bottom:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:selectTypeChange(this);">
			                 	 조회
			            	</a>
			            	<input id="selectType" type="hidden" value="${paramMap.searchType}">
						</div>
					</div> 
	           
	           
	            <!-- <div class="cat-list" style="background-color:#fff">
	                <ul class="clearfix" style="background-color:#fff">
	                	<li><a style="color:#000; width:15%;">출발일</a></li>
	                    <li><a style="color:#000;">상차지</a></li>
	                    <li><a style="color:#000;">하차지</a></li>
	                    <li><a style="color:#000;">차종</a></li>
	                    <li><a style="color:#000;">차대번호</a></li>
	                </ul>
	            </div> -->
	            <div style="width:100%; margin-top:5px; height:40px; border-bottom:1px solid #eee;">
                   <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">출발일</div></div>
	               <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">차대번호</div></div>
	               <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">차량번호</div></div>
	               <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">상차지</div></div>
	               <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">하차지</div></div>
	               <!-- <div style="width:33%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">탁송완료건수</div></div> -->
                </div>
            </div>
            <div id="container" class="search-result news-list content-box" style="height:100%; margin-top:38%; overflow-y:scroll;">
            	<!-- <div class="xpull">
				    <div class="xpull__start-msg">
				        <div class="xpull__start-msg-text">화면을 당겼다 놓으면 새로 고침 됩니다.</div>
				        <div class="xpull__arrow"></div>
				    </div>
				    <div class="xpull__spinner">
				        <div class="xpull__spinner-circle"></div>
				    </div>
				</div> -->
            	<div class="news-container clearfix" style="height:450%;">
    	       		<c:forEach var="data" items="${allocaitonList}" varStatus="status">
			           	<div style="width:100%; margin-top:5%; height:40px; border-bottom:1px solid #eee;"  onclick="javascript:viewAllocationData('F','${data.allocation_id}');">
					        <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">${data.departureDt}</div></div>
					        <div style="width:20%; text-align:center; height:40px; float:left;">
					       	  
					       		  <c:if test ="${data.car_id_num eq ''}">
					      		 	 <div style="margin-top:10px; text-align:center; display:inline-block;">-</div>
					      	 	 </c:if>
					      	 	 
					       	 	<c:if test ="${data.car_id_num ne ''}">
					      		 	 <div style="margin-top:10px; text-align:center; display:inline-block;">${data.car_id_num}</div>
					      	 	 </c:if>
					       </div>
					        <div style="width:20%; text-align:center; height:40px; float:left;">
					        	  
					      		  <c:if test ="${data.car_num eq ''}">
					        		<div style="margin-top:10px; text-align:center; display:inline-block;">-</div>
					        	</c:if>
					        
					          	<c:if test ="${data.car_num ne ''}">
					        		<div style="margin-top:10px; text-align:center; display:inline-block;">${data.car_num}</div>
					        	</c:if>
					        	
					      </div>
					        
					    
					        <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block; width:100%; white-space:nowrap; text-overflow:ellipsis; overflow:hidden;">${data.departure}</div></div>
					        <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block; width:100%; white-space:nowrap; text-overflow:ellipsis; overflow:hidden;">${data.arrival}</div></div>
						    <%-- <div style="width:33%; text-align:center; height:40px; float:left;"><div style="width:100%; margin-top:10px; text-align:center; display:inline-block; text-overflow:ellipsis; white-space:nowrap; overflow:hidden;"><nobr>${data.cnt}</nobr></div></div> --%>
				
			            </div>
			            
			      
			            
	                </c:forEach>
                 </div>
            </div>
        </div>


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      
    <script src="/js/vendor/bootstrap.min.js"></script>       
    <!-- veiwport for countnumber -->
    <script src="/js/alert.js"></script>
    <script src="/js/main.js"></script>   
    <script src="/js/vendor/xpull.js"></script>       
    <script src="/js/vendor/jquery.loading-indicator.min.js"></script>
    <script src="/js/animsition.min.js"></script>
    <script>
    
 var homeLoader;
    
    $(document).ready(function(){
    
    	homeLoader = $('body').loadingIndicator({
			useImage: false,
			showOnInit : false
		}).data("loadingIndicator");
	
    	
    	//homeLoader.show();
    	
    	
    });
	
/* 	$('#container').xpull({
	    'paused': false, 
	    // Pull threshold - amount in  pixels required to pull to enable release callback
        'pullThreshold':50,
        // Max pull down element - amount in pixels
        maxPullThreshold: 50,
        // timeout in miliseconds after which the loading indicator stops spinning.
        // If set to 0 - the loading will be indefinite
        'spinnerTimeout':1000,
        onPullStart: function(){},
        onPullEnd: function(){},
        callback: function(){window.location.reload()},

	});   
     */
    </script>
        
    </body>
</html>
