<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%

%>

<!DOCTYPE html>
<html lang="ko" style=" height:100%;">      
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
<link rel="stylesheet" href="/css/animsition.min.css">  
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<body style="background-color:#fff; height:100%;" >

<script type="text/javascript">  
  var pageMoveStop = true;
$(document).ready(function(){  
	

	var updateToken = setInterval( function() {
			clearInterval(updateToken);
			pageMoveStop = false;
			homeLoader.hide();
	
   }, 300);
	
	
	
	/* var updateLocation = setInterval( function() {
	
		if(getDeviceLocation() != null && getDeviceLocation() != ""){
			updateDriverDeviceLocation(getDeviceLocation());
		}
		
    }, 10*1000); */			//1분에 한번
	
  	//alert(deviceToken);
    

	  $(".animsition").animsition({
			inClass: 'fade-in-down-lg',
		    outClass: 'fade-out-down-lg',
		    inDuration: 1500,
		    outDuration: 800,
		    linkElement: '.animsition-link',
		    // e.g. linkElement: 'a:not([target="_blank"]):not([href^="#"])'
		    loading: true,
		    loadingParentElement: 'body', //animsition wrapper element
		    loadingClass: 'animsition-loading',
		    loadingInner: '', // e.g '<img src="loading.svg" />'
		    timeout: false,
		    timeoutCountdown: 5000,
		    onLoadEvent: true,
		    browser: [ 'animation-duration', '-webkit-animation-duration'],
		    // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
		    // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
		    overlay : false,
		    overlayClass : 'animsition-overlay-slide',
		    overlayParentElement : 'body',
		    transition: function(url){ window.location.href = url; }
		  });
});  

  /* 
  
function updateDriverDeviceToken(token){

	
	if(token != ""){
		$.ajax({ 
			type: 'post' ,
			url : "/updateDriverDeviceToken.do" ,
			dataType : 'json' ,
			data : {
				token : token
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					//$.alert("성공",function(a){
					//});
				}else if(result == "E000"){
					
				}else{
					
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}

}  
   */
  
  
function getDeviceToken(){
	
	var token = "";
	try{
		if(window.Android != null){
			token = window.Android.getDeviceToken("${driverId}");
		}	
	}catch(exception){
		
	}finally{
		
	}
	return token;
}  
  
  
function updateDriverDeviceLocation(location){
	
	if(location != ""){
		$.ajax({ 
			type: 'post' ,
			url : "/updateDriverDeviceLocation.do" ,
			dataType : 'json' ,
			data : {
				location : location
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					//$.alert("성공",function(a){
					//});
				}else if(result == "E000"){
					
				}else{
					
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
	
}  

function goSearchMonth(){
	
	

document.location.href ="/carrier/finish-list.do";

	
}



function goKakaoAskTalk(){


	//$.alert("준비중입니다.");
	
	 
		$.confirm("카카오톡 채널로 이동 하시겠습니까?",function(a){
			 if(a){
				if(window.Android != null){
					window.Android.openKakaoChannel();
				}else if(webkit.messageHandlers != null){
					webkit.messageHandlers.scriptHandler.postMessage("goKakaoChannel");

				}else{
					$.alert("알수 없는 에러입니다. 관리자에게 문의해주세요");					
				}	
			 }
		}); 
	





	
}




  
 function getDeviceLocation(){
	 
	 var latlng = "";
		try{
			if(window.Android != null){
				latlng = window.Android.getDeviceLocation("${kakaoId}");
				//alert(latlng);
			}	
		}catch(exception){
			
		}finally{
			
		}
		return latlng; 
	 
 }
  
  
function getAndroidData(str){
	
//	alert(str);
	
	//updateDriverDeviceLocation(str);
	
} 
 
 
function backKeyController(str){
	
/*  	if(window.Android != null){
		window.Android.toastShort("뒤로가기 버튼으로 앱을 종료 할 수 없습니다.");
	} 
 */


 document.location.href='/carrier/main.do';
	
}
 
 
function goNList(allocationStatus){
	
	//alert("승인 대기 목록");
	if(!pageMoveStop){
		homeLoader.show();
		document.location.href = "/carrier/list.do?allocationStatus="+allocationStatus;	

		var updateHomeLoader = setInterval(function() {
			clearInterval(updateHomeLoader);
			homeLoader.hide();
		}, 300);
		
	}
	
	
}

function goYList(allocationStatus){
	
	
	//window.Android.toastLong("test");
	//alert("승인 완료 목록");
	if(!pageMoveStop){
		homeLoader.show();
		document.location.href = "/carrier/list.do?allocationStatus="+allocationStatus;	

		var updateHomeLoader = setInterval(function() {
			clearInterval(updateHomeLoader);
			homeLoader.hide();
		}, 300);
		
	}
	
}

function goFList(allocationStatus){
	
	//alert("탁송 완료 목록");
	if(!pageMoveStop){
		homeLoader.show();
		document.location.href = "/carrier/finish-list.do?allocationStatus="+allocationStatus;	

		var updateHomeLoader = setInterval(function() {
			clearInterval(updateHomeLoader);
			homeLoader.hide();
		}, 300);
		
	}
	
}

function goLowViolationPage(){

	if(!pageMoveStop){
		homeLoader.show();
		document.location.href = "/carrier/lowViolationList.do";	

		var updateHomeLoader = setInterval(function() {
			clearInterval(updateHomeLoader);
			homeLoader.hide();
		}, 300);
		
	}
	
}




function goReload(){
	
	document.location.reload();
	/* $.alert("준비중 입니다.",function(a){
	}); */
	
}

function goCalPage(){
	
	
	if("${user.driver_kind}" != "00" || "${user.driver_id}" == "sourcream"){
		if(!pageMoveStop){
			homeLoader.show();
			document.location.href = "/carrier/cal-list.do";	

			var updateHomeLoader = setInterval(function() {
				clearInterval(updateHomeLoader);
				homeLoader.hide();
			}, 300);
			
		}	
	}else{
		$.alert("준비중 입니다.",function(a){
		});
	}
	
	
}

    
function logout(){
	
	$.confirm("로그아웃 하시겠습니까?",function(a){
		 if(a){
			 homeLoader.show();
			 if(window.Android != null){
					window.Android.stopService("${driverId}");
				}	
			 document.location.href = "/logout.do";	
		 }
	});
	
}    
    
    
    
function downloadDriverDeduct(){
	//alert(1);
	if(!pageMoveStop){
		//document.location.href = "/carrier/downloadDriverDeductInfo.do?driverId=${user.driver_id}";
		window.Android.openTmap("${driverId}");
	//	document.location.href = "https://apis.openapi.sk.com/tmap/app/execution?&appKey=l7xx6b701ff5595c4e3b859e42f57de0c975";
	}
}    


function goCarrierSearch(){

	//var result = $.confirm('확인을 누르시면 로그아웃이 되고, 탁송금액조회페이지로 이동 됩니다.');

		//if(result) { 
		homeLoader.show();
		document.location.href ="/carrier/kakaoRouteView.do?&call=sub-main";

		var updateHomeLoader = setInterval(function() {
			clearInterval(updateHomeLoader);
			homeLoader.hide();
		}, 300);
		
		//} else { 

	
	//}
    

}



function goShowLicense(){

homeLoader.show();
document.location.href ="/carrier/osLicense.do";

var updateHomeLoader = setInterval(function() {
	clearInterval(updateHomeLoader);
	homeLoader.hide();
}, 300);


	
}

function goMyPage(){

	homeLoader.show();
	document.location.href ="/carrier/myProfile-Page.do";

	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
	
	
}


function goShowInfomation(){

	homeLoader.show();
	document.location.href ="/carrier/hk-infomation.do";

	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
	
	
	
}

</script>


        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div class="content-container" style="height:100%; overflow-y:hidden;" >
            <!-- <header class="bg-pink clearfix" style="background-color:#fff; border:none; text-align:center; "> -->
            <header class="bg-pink clearfix" style="position:fixed; top:0px; background-color:#fff; border:none; text-align:center; ">
                <div class="" style="width:19%;">
                    <a onclick="javascript:homeLoader.show(); document.location.href='/carrier/main.do'"><img style="zoom: 0.5;" src="/img/back-icon.png" alt=""></a> 
                </div> 
                <div class="" style="width:60%;">
                	<!-- <img style="width:90%; height:60%;" src="/img/main_logo.png" alt=""> -->
                	<img style="width:90%; height:60%;" onclick="javascript:homeLoader.show(); document.location.href='/carrier/main.do'" src="/img/main_logo.png" alt="">
                </div>
                <div class="" style="width:19%; float:right;">
                   <a style="cursor:pointer;" href="javascript:logout();"><img style="" src="/img/logout.png" alt=""></a>
                </div>
            
            </header>
      
            <div style="clear:both;"></div>
            <!-- <div class="menu-container bg-pink">
                <span class="img-holder">
                    <img src="/img/user-img.png" alt="">
                </span>
                 <a class="kakao txt-bold" style="cursor:pointer;" onclick="javascript:kakaoLogin();">
                    카카오계정으로 로그인
                </a>
            </div> -->
            <div style=" text-align:right; clear:both;">&nbsp;</div>
            <div class="main-menu-container" style="margin-top:9%; height:100%; overflow-y:hidden;">
            	<div style=" text-align:right; clear:both;">${customer.customer_name}&nbsp;${customer.name}님 환영 합니다.</div>
            	<div  id="container"  style="height:100%; margin-top:2%; background-color:#eee;">
            		<div class="xpull">
					    <div class="xpull__start-msg">
					        <div class="xpull__start-msg-text">화면을 당겼다 놓으면 새로 고침 됩니다.</div>
					        <div class="xpull__arrow"></div>
					    </div>
					    <div class="xpull__spinner">
					        <div class="xpull__spinner-circle"></div>
					    </div>
					</div>
					  <div class="animsition">
				<div style="margin-top:3%; height:100%;">
                
            <%--     <a href="javascript:goLowViolationPage();" class="menu-link">
                	<c:if test="${lowViolationList > 0}">
                		<span class="badge">${lowViolationList}</span>
                	</c:if>
                    <img src="/img/list-icon.png" alt="">
                    <span class="text">과태료관리</span>
                </a> --%>
                <a href="javascript:goKakaoAskTalk();" class="menu-link">
                	<%-- <c:if test="${lowViolationList > 0}">
                		<span class="badge">${lowViolationList}</span>
                	</c:if> --%>
                    <!-- <img src="/img/support-icon.png" alt=""> -->
                    <span class="text">카카오톡채널<br> 문의</span>
                </a>    
                <a href="javascript:goSearchMonth();" class="menu-link">
                	<%-- <c:if test="${lowViolationList > 0}">
                		<span class="badge">${lowViolationList}</span>
                	</c:if> --%>
                    <!-- <img src="/img/calendaricon.png" alt=""> -->
                    <span class="text">탁송 내역 <br>조회</span>
                </a>
                   <a href="javascript:goCarrierSearch();" class="menu-link">
                	<%-- <c:if test="${lowViolationList > 0}">
                		<span class="badge">${lowViolationList}</span>
                	</c:if> --%>
                    <!-- <img src="/img/calendaricon.png" alt=""> -->
                    <span class="text">탁송 금액 <br>조회</span>
                </a>
                  <a href="javascript:goShowLicense();" class="menu-link">
                	<%-- <c:if test="${lowViolationList > 0}">
                		<span class="badge">${lowViolationList}</span>
                	</c:if> --%>
                    <!-- <img src="/img/calendaricon.png" alt=""> -->
                    <span class="text">오픈소스<br>라이선스</span>
                </a>
                 <%--   <a href="javascript:goShowInfomation();" class="menu-link">
                	<c:if test="${lowViolationList > 0}">
                		<span class="badge">${lowViolationList}</span>
                	</c:if>
                    <!-- <img src="/img/calendaricon.png" alt=""> -->
                    <span class="text">공지<br>사항</span>
                </a>
                 --%>
              <!-- <a href="javascript:goMyPage();" class="menu-link"> -->
                <%-- 	<c:if test="${lowViolationList > 0}">
                		<span class="badge">${lowViolationList}</span>
                	</c:if> --%>
                    <!-- <img src="/img/calendaricon.png" alt=""> -->
                    <!-- <span class="text">마일리지<br>페이지</span> -->
                <!-- </a>  -->
             <!--     <a href="javascript:goCarrierSearch();" class="menu-link">
                    <img src="/img/calendaricon.png" alt="">
                    <span class="text">탁송 금액 조회</span>
                </a> -->
      
                
                <!-- <a href="javascript:goQrCodeScan();" class="menu-link">
                    <img src="/img/list-icon.png" alt="">
                    <span class="text">QR코드 스캔</span>
                </a> -->
                		</div>
                	</div>
            	</div>
              </div>  
        </div>

    <script src="/js/vendor/jquery-1.11.2.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      <script src="/js/vendor/bootstrap.min.js"></script>       
    <!-- veiwport for countnumber -->
    <script src="/js/jquery.viewportchecker.js"></script>  
      <script src="/js/alert.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/vendor/xpull.js"></script>       
    <script src="/js/vendor/jquery.loading-indicator.min.js"></script>
    <script src="/js/animsition.min.js"></script>
    <script>
    
    var homeLoader;
    
    $(document).ready(function(){


    	homeLoader = $('body').loadingIndicator({
			useImage: false,
			showOnInit : false
		}).data("loadingIndicator");
	
    	homeLoader.show();
   
    });
	
	/* $('#container').xpull({
	    'paused': false, 
	    // Pull threshold - amount in  pixels required to pull to enable release callback
        'pullThreshold':50,
        // Max pull down element - amount in pixels
        maxPullThreshold: 50,
        // timeout in miliseconds after which the loading indicator stops spinning.
        // If set to 0 - the loading will be indefinite
        'spinnerTimeout':1000,
        onPullStart: function(){},
        onPullEnd: function(){},
        callback: function(){window.location.reload()},

	}); */   
    
    </script>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    </body>
</html>
