<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
	
%>

<!DOCTYPE html>
<html lang="ko" style="height: 100%;">
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
<link rel="stylesheet" href="/css/animsition.min.css">
<body style="height: 100%;">


	<script type="text/javascript">  

$(document).ready(function(){

	$('.modal-field').css('display','none');
	$('.modal-field2').css('display','none');
	$('.showTmapDistence').css('display','none');
	
	/*	
	var updateToken = setInterval( function() {
		
 		if(getDeviceToken() != null && getDeviceToken() != ""){
			updateDriverDeviceToken(getDeviceToken());
			if(window.Android != null){
				window.Android.startService();
			}
			clearInterval(updateToken);
		}
   }, 1000);
 
 */

	 $(".animsition").animsition({
		    inClass: 'zoom-in-lg',
		    outClass: 'zoom-out-lg',
		    inDuration: 1500,
		    outDuration: 800,
		    linkElement: '.animsition-link',
		    // e.g. linkElement: 'a:not([target="_blank"]):not([href^="#"])'
		    loading: true,
		    loadingParentElement: 'body', //animsition wrapper element
		    loadingClass: 'animsition-loading',
		    loadingInner: '', // e.g '<img src="loading.svg" />'
		    timeout: false,
		    timeoutCountdown: 5000,
		    onLoadEvent: true,
		    browser: [ 'animation-duration', '-webkit-animation-duration'],
		    // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
		    // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
		    overlay : false,
		    overlayClass : 'animsition-overlay-slide',
		    overlayParentElement : 'body',
		    transition: function(url){ window.location.href = url; }
		  });

	
});



function getDeviceToken(){
	
	var token = "";
	try{
		if(window.Android != null){
			token = window.Android.getDeviceToken("${driverId}");
		}	
	}catch(exception){
		
	}finally{
		
	}
	return token;
}  
  
/* 
function updateDriverDeviceToken(token){
	
	
	if(token != ""){
		$.ajax({ 
			type: 'post' ,
			url : "/updateDriverDeviceToken.do" ,
			dataType : 'json' ,
			data : {
				token : token
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					//$.alert("성공",function(a){
					//});					
				}else{
					
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
}  
 */


 function getNewsData(obj,keyword){
	
	 
	// alert($("#selectedStatus").children().length);
	 
	//alert($("#selectedStatus").children().first().get(0));	
	//alert($(obj).parent().get(0));
	
 	 if($("#selectedStatus").children().first().get(0) == $(obj).parent().get(0)){				//전체를 클릭 한경우
					
 		$("#selectedStatus").children().each(function(index,element){
 			$(this).removeClass('selected');
 		});
 		$(obj).parent().addClass('selected');
	}else{
			
	} 
	
	
	
/* 	
	
	$.ajax({ 
		type: 'post' ,
		url : "/research/newsSearch.do?&keyword="+keyword ,
		dataType : 'json' ,
		data : {
			
		},
		success : function(data, textStatus, jqXHR)
		{
			alert(data);
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
	 */
	
	
} 

 function goNewsPage(uri){
		
		//var replaceUrlStr = replaceAll(url, "?", "^");
		//replaceUrlStr = replaceAll(replaceUrlStr, "=", "+");
		
		var replaceUrlStr = encodeURIComponent(uri)
		document.location.href = "/news/newsPage.do?newsUri="+replaceUrlStr;
	}  


    
function viewAllocationData(allocationId,allocationStatus){
	homeLoader.show();
	document.location.href = "/carrier/allocation-detail.do?allocationStatus="+allocationStatus+"&allocationId="+allocationId;

	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
} 
 
 
function viewAllocationList(allocationId,allocationStatus){
	homeLoader.show();
	document.location.href = "/carrier/group-list.do?allocationId="+allocationId+"&allocationStatus="+allocationStatus;

	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);
	
} 
 
 
 
function logout(){
	
	$.confirm("로그아웃 하시겠습니까?",function(a){
		 if(a){
			 if(window.Android != null){
					window.Android.stopService("${driverId}");
				}	
			 homeLoader.show();
			 document.location.href = "/logout.do";	
		 }
	});
	
}   








var selectedObj = new Object();


function selectInputId(){

	if($("#inputID").val() != ""){

		$.ajax({
			type : 'post',
			url : '/carrier/compareCustomerId.do',
			dataType : 'json',
			data : {

				inputID : $("#inputID").val(),
				
			},success : function(data, textStatus, jqXHR) {
		         var result = data.resultCode;
		         	
					if(result == "0000") {

						sendMessage();
					
						}else if (result == "0001") { 
						$.alert("입력한 아이디로 가입된 아이디가 없습니다.", function(a) {
	
					});	
				}  
			},
			error : function(xhRequest, ErrorText, thrownError) {
				
			}
		});


		}else{

			$.alert("아이디가 입력이 되지 않았습니다.");
			


			}
		
}


	selectedObj = new Object(); 

	function sendMessage(){


		selectedObj = new Object();

		
		$.confirm("등록하셨던 번호로 인증번호를 전송하시겠습니까?",function(a){

			 if(a){
					$(".certNum").css('display','block');	
					
					$.ajax({
						type : 'post',
						url : '/carrier/insertCertNumForAPP.do',
						dataType : 'json',
						data : {

							inputID : $("#inputID").val(),
							phoneNum : "",
							customerId : "",
							gubun : 'P',
							
						},success : function(data, textStatus, jqXHR) {
					         var result = data.resultCode;
					         	
								if(result == "0000") {

									$(".certNum").css('display','block');											
								
									}else if (result == "0002") { 
									$.alert("알 수 없는 오류입니다. 관리자에게 문의해주세요.", function(a) {
				
								});	
							}  
						},
						error : function(xhRequest, ErrorText, thrownError) {
							
						}
					});
					
				 
					

					 	
			 }else{

				/* $.alert("취소",function(a){
				}); */
			}
		});
	}
		
	


	
function sendPinNum(){


	var phoneCertNum = $("#phoneCertNum").val();

	
	if(phoneCertNum != ""){

				$.ajax({
					type : 'post',
					url  : "/carrier/checkCertNumForLogin.do",
				    dataType : 'json',
				    
				       data : {
					       
				    	inputID : $("#inputID").val(),
						phoneNum :  "",
						customerId : "",
						phoneCertNum : phoneCertNum,
						gubun : 'P',
				
						},

				   success : function(data, textStatus, jqXHR)
				    {
					   var resultCode = data.resultCode;
					   var resultData = data.resultData;
					   
					   if(resultCode == "0000"){
						 
							$.alert("인증에 성공하였습니다.", function(a) {
								

								
								$(".changePw").css('display','block');		
								
							});	
						 
					   }else{
						 $.alert("인증번호가 맞지 않습니다. 관리자에게 문의하세요.");
					   }
				   } ,
				  error :function(xhRequest, ErrorText, thrownError){
					  $.alert('알수 없는 오류입니다. 관리자에게 문의 하세요.');
	                  
					  }
				});
	
		}else{

			$.alert("인증번호가 입력되지 않았습니다.");
			return false;
			}
}



function findID(findId){


	document.location.href ="/carrier/sign-Up.do?&findId="+findId;
		
}




function goSingUpPage(){

	document.location.href='/carrier/simpleSign-Up.do?&customerId='+selectedObj.customerId+'&chargeId='+selectedObj.chargeId+'&phoneNum='+selectedObj.phoneNum;
	
}

 
function backKeyController(str){
	//history.go(-1);
	//location.href = document.referrer;
	document.location.href='/carrier/main.do';
}

function goChagnePassword(findId){

	document.location.href ="/carrier/sign-Up.do?&findId="+findId;
	
}

function goSignUpPage(){


	document.location.href='/carrier/sign-Up.do';
	
}


function newPasswordChange(){


	var newPassword = $("#newPassword").val();
	var confirmpw = $("#confirmpw").val();

	
	    var num = newPassword.search(/[0-9]/g);
	    var eng = newPassword.search(/[a-z]/ig);
	    var spe = newPassword.search(/[`~!@@#$%^&*|₩₩₩'₩";:₩/?]/gi);

	    

	    if(newPassword.length < 6 || newPassword.length > 20){
	     $.alert("패스워드는 6자리 이상이어야 합니다.");
	     
	     return false;
	    }
	    if(newPassword.search(/₩s/) != -1){
	        $.alert("패스워드는 공백없이 입력해주세요.");
	        return false;
	       }
	    if(num < 0 || eng < 0 || spe < 0 ){
	      $.alert("영문,숫자, 특수문자를 혼합하여 입력해주세요.");
	      return false;
	     }

	    
	 if(newPassword == confirmpw ){


				$.ajax({
					type : 'post',
					url  : "/carrier/confirmForPassWord.do",
				    dataType : 'json',
				    
				       data : {

						userId : $("#inputID").val(),
						newPassword : newPassword,
						
						},

				   success : function(data, textStatus, jqXHR)
				    {
					   var resultCode = data.resultCode;
					   if(resultCode == "0000"){

						   $.alert("비밀번호가 변경되었습니다.",function(a){
								if(a){
									goSignUpPage();
								}
							});
									
						}				

						   },
				  error :function(xhRequest, ErrorText, thrownError){
	                  $.alert(' @@@@error@@@@ ');
	                  
					  }

				});

			}else if(newPassword ==""){
				$.alert("입력창에 입력이 되지 않았습니다.");
		
			}else{
			$.alert("비밀번호가 같지 않습니다. 다시한번 확인해주세요");

				}
		}


var status ="";

function goWindowPopUp(id){


	status = id;

	window.open("/carrier/addressSearch.do", "a", "width=500, height=800, left=100, top=50"); 
	

	
}

function setAddress(value){

	$.alert(value);
	$.alert(status);

	$("#"+status).val(value);


	
}




function goShowModal(obj){

	if($("#searchKeyword").val() == ""){

	}else{

	$('.modal-field').show();
	
		}


	

	
}


function goShowModal2(obj){
	
	if($("#searchKeyword2").val() == ""){

	}else{

	$('.modal-field2').show();
	
		}

}



function goDepartureInit(obj){


	var departureAddressVal =$('#departureAddress').val(); 
	var arrivalAddressVal= $('#arrivalAddress').val(); 

	inputStatus='departure'; 

	$(obj).next().val('');
	$('#searchKeyword').val('');

	$('#showDistence').css('display','none');
	
	 $('#searchResult2').html('');

		if(arrivalAddressVal == ""){
			
				$('#searchKeyword2').val('');
		
			}

 

}


function goArrivalInit(obj){


	var departureAddressVal =$('#departureAddress').val(); 
	var arrivalAddressVal= $('#arrivalAddress').val(); 
	
	inputStatus='arrival'; 
	

	$('#searchKeyword2').val('');
	$(obj).next().val('');

	$('#showDistence').css('display','none');
	
	$('#searchResult').html(''); 

		if(departureAddressVal == ""){
			
			$('#searchKeyword').val('');
	
		}

		
	}


/*
 

function goReservationConsignment(){



	if($("#departureDt").val() == ""){

		$.alert("출발일이 입력 되지 않았습니다.",function(a){
	    });
		return;
	}
	if($("#searchKeyword").val() == ""){

		$.alert("출발지가 입력 되지 않았습니다.",function(a){
	    });
		return;
	}

	if($("#searchKeyword2").val() == ""){

		$.alert("도착지가 입력 되지 않았습니다.",function(a){
	    });
		return;
	}
	
	
	if($.confirm("등록 하시겠습니까?"+"\r\n")){


		$("#insertForm").attr("action","/carrier/insert-allocation.do");
		$("#insertForm").submit();	

		}else{
				$.alert("취소됨");
				return false;
			}

	
	
	} 
*/


function goReservationConsignment(){
	   	
		if($("#departureDt").val() == ""){
			
			$.alert("출발일을 설정해주세요",function(a){
		    });
			return;
		}

		if($("#departureAddress").val() == ""){
	
			$.alert("출발지를 목록에서 선택해주세요.",function(a){
		    });
			return;
		}
		if($("#arrivalAddress").val() == ""){
	
			$.alert("도착지를 목록에서 선택해주세요.",function(a){
		    });
			return;
		}

		 if($("#carKind").val() == ""){
				
			$.alert("차종을 입력해주세요.",function(a){
		    });
			return;
		}
		
		if($("#carIdNum").val() == "" && $("#carNum").val() == ""){
			
			$.alert("차대번호 또는 차량번호를 입력해주세요.",function(a){
		    });
			return;
		}
		
		/* if($("#carNum").val() == ""){
			
			$.alert("차량번호를 입력해주세요.",function(a){
		    });
			return;
		} */

 	
 /* 		if($("#chooseMileage").val() == "" ){	 		

 			$.alert("마일리지 사용없이 예약하시겠습니까?",function(a){
		    });
			return;
	
 		 }
	

 */			

		if($("#departureTime").val() != ""){ //빈문자열 아닐때

			 $.confirm($("#departureTime").val()+"  으로 탁송 예약 하시겠습니까?" ,function(a){

					if(a){
						finalReservation();
					}
			 });

	
		}else{ //빈문자열일때

			 $.confirm("탁송 예약 하시겠습니까?" ,function(a){
				 
				 if(a){
					finalReservation();
				 }		

			 });
	
	}


		 
}

function finalReservation(){


	var carCount = "${paramMap.carCount}";
	var nowCount = "${paramMap.nowCount}";
	
				$('#insertForm').ajaxForm({
					url: "/carrier/insert-allocation.do",
				    type: "POST",
					dataType: "json",		
					data : {

						
				    },
					success: function(data, response, status) {
						var status = data.resultCode;
						if(status == '0000'){
							
			
							if(carCount != nowCount){
								$.alert(carCount+"대중"+nowCount+"건 예약이 정상적으로 등록 되었습니다. ",function(a){
								nowCount++
								document.location.href = "/carrier/consignment-Reservation-many.do?&carCount="+carCount+"&nowCount="+nowCount;
								});
								
							}else{
									
									$.alert("예약이 정상적으로 등록 되었습니다. 예약 확정시 알림톡이 발송됩니다. ",function(a){
										document.location.href = "/carrier/main.do";
								});
							}
							
						
						}else if(status == '0001'){
							$.alert("알 수없는 오류입니다. 카카오톡 플러스채팅으로 문의 주세요",function(a){
								return;			

							});			
						}
								
					},
					error: function() {
						alertEx("오류가 발생하였습니다. 관리자에게 문의주세요.");
					}                               
				});
				
				$("#insertForm").submit();

			 
}





function goReWriteReservation(){   //탁송 정보 수정할 때 


	if($("#departureDt").val() == ""){
		
		$.alert("출발일을 설정해주세요",function(a){
	    });
		return;
	}

	if($("#departureAddress").val() == ""){

		$.alert("출발지를 목록에서 선택해주세요.",function(a){
	    });
		return;
	}
	if($("#arrivalAddress").val() == ""){

		$.alert("도착지를 목록에서 선택해주세요.",function(a){
	    });
		return;
	}

	 if($("#carKind").val() == ""){
			
		$.alert("차종을 입력해주세요.",function(a){
	    });
		return;
	}
	
	if($("#carIdNum").val() == "" && $("#carNum").val() == ""){
		
		$.alert("차대번호 또는 차량번호를 입력해주세요.",function(a){
	    });
		return;
	}
	


	$.confirm("탁송 정보 수정 하시겠습니까?",function(a){
		 if(a){

			 $('#insertForm').ajaxForm({
					url: "/carrier/update-reservation.do",
				    type: "POST",
					dataType: "json",		
					data : {

					
				    },
					success: function(data, response, status) {
						var status = data.resultCode;
						if(status == '0000'){
							$.alert("정상적으로 정보 수정이 되었습니다. ",function(a){

								document.location.href = "/carrier/main.do";
								
							});
						}else if(status == '0001'){
							$.alert("알 수없는 오류입니다. 카카오톡 플러스채팅으로 문의 주세요",function(a){
								return;			

							});			
						}else if(status == 'E001'){
							$.alert("기사님이 지정되었으므로, 탁송 정보를 수정하실 수 없습니다.",function(a){
								return;			

							});
						}
								
					},
					error: function() {
						alertEx("오류가 발생하였습니다. 관리자에게 문의주세요.");
					}                               
				});
				
				$("#insertForm").submit();

			 	
		 }
		 
	});


	
}






function getDistence(){


	$.ajax({
		type : 'post',
		url  : "/carrier/getDistense.do",
	    dataType : 'json',
		async : false,
	       data : {
				
	    	   departureAddress : $("#departureAddress").val(),
			   arrivalAddress : $("#arrivalAddress").val()
 	   		
		
			},

	   success : function(data, textStatus, jqXHR)
	    {
		   var resultCode = data.resultCode;
		   var resultData = data.resultData;
		   
		   if(resultCode == "0000"){

				$('#showDistence').css('display','block');
				$('#getDis').html(resultData+" km");

				
		   }else{

				
		   }
	   } ,
	  error :function(xhnRequest, ErrorText, thrownError){
		  $.alert('알수 없는 오류입니다. 관리자에게 문의 하세요.');
          return false;
		  }
	});

	
}



    	
function showDistance(){
	
	//homeLoader.show();
	var carCount = "${paramMap.carCount}";
	var nowCount = "${paramMap.nowCount}";
	

		var departure =$("#departureAddress").val();
		var arrival =$("#arrivalAddress").val();
		
		if(departure == ""){

			$.alert("출발지목록에서 선택해주세요.");

		}else if(arrival == ""){

			$.alert("도착지목록에서 선택해주세요.");

		}else{
			homeLoader.show();

		

		 $('#insertForm').ajaxForm({
					url: "/carrier/tmpConsignmentReservation.do",
				    type: "POST",
					dataType: "json",		
					data : {
			
						
				    },
					success: function(data, response, status) {
						var status = data.resultCode;
						var result = data.resultData;
						if(status == '0000'){

						document.location.href ="/carrier/loadView-many.do?&departure="+$("#departureAddress").val()+"&arrival="+$("#arrivalAddress").val()+"&tempId="+result.tempConsignmentId+"&carCount="+carCount+"&nowCount="+nowCount;
					
						}else if(status == '0001'){
							$.alert("알 수없는 오류입니다. 카카오톡 플러스채팅으로 문의 주세요",function(a){
								return;			

							});			
						}
								
					},
					error: function() {
						alertEx("오류가 발생하였습니다. 관리자에게 문의주세요.");
					}                               
				});
				
				//$("#insertForm").submit();







			
			
			//window.open("/carrier/loadView.do?&departure="+$("#departureAddress").val()+"&arrival="+$("#arrivalAddress").val(),"_blank","top=240,left=660,width=600,height=700,toolbar=0,status=0,scrollbars=1,resizable=0");

	/* 		var updateHomeLoader = setInterval(function() {
				clearInterval(updateHomeLoader);
				homeLoader.hide();
			}, 300); */
			
		}
			
	}
   	







</script>



	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<body onload="initTmap();" style="">

	<div class="content-container" style="height: 100%;">
		<%-- <jsp:include page="/WEB-INF/views/jsp/common-top.jsp"></jsp:include> --%>
		<div class="animsition">
			<header class="bg-pink clearfix"
				style="position: fixed; background-color: #fff; border: none; text-align: center; z-index: 100000;">
				<div class="" style="width: 19%;">
					<a href="/carrier/main.do"><img style="zoom: 0.5;"
						src="/img/back-icon.png" alt=""></a>
				</div>
				<div class="" style="width: 60%;">
					<img style="width: 90%; height: 60%;"
						onclick="javascript:homeLoader.show(); document.location.href='/carrier/main.do'"
						src="/img/main_logo.png" alt="">
				</div>

				<div class="" style="width: 19%; float: right;">
					<!-- <a style="cursor:pointer;" href="javascript:logout();"><img style="" src="/img/logout.png" alt=""></a> -->
				</div>
			</header>
			<%-- <div style="text-align:right;">${user.driver_name}님 환영 합니다.</div> --%>
			<form id="insertForm" action="/carrier/main.do">
				<c:if test="${paramMap.allocationId eq null}">
					<input type="hidden" name="allocationId" id="allocationId" value="">
				</c:if>
				<c:if test="${paramMap.allocationId ne null}">
					<input type="hidden" name="allocationId" id="allocationId" value="${paramMap.allocationId}">
				</c:if>

				<input type="hidden" name="companyId" id="companyId" value=""> 
				<input type="hidden" name="companyName" id="companyName"> 
				<input type="hidden" name="inputDt" id="inputDt"> 
				<input type="hidden" name="registerId" id="registerId" value="${userMap.user_id}"> 
				<input type="hidden" name="registerName" id="registerName" value="${userMap.name}">
				<input type="hidden" name="profit" id="profit" value="">
				<!--순이익  -->
				<input type="hidden" name="customerId" id="customerId"> 
				<input type="hidden" name="chargeId" id="chargeId" value="${userMap.charge_id}">
			    <input type="hidden"	name="chargeAddr" id="chargeAddr"> 
			    <input type="hidden" name="customerSignificantData" id="customerSignificantData">
				<input type="hidden" name="carCnt" id="carCnt" value="1"> 
				<c:if test="${paramMap.allocationId eq null}">
					<input type="hidden" name="allocationStatus" id="allocationStatus" value="W"> 
				</c:if>
				<c:if test="${paramMap.allocationId ne null}">
					<input type="hidden" name="allocationStatus" id="allocationStatus" value="${allocationMap.allocation_status}"> 
				</c:if>
				
				<input type="hidden" name="batchStatus" 	id="batchStatus"> 
				<input type="hidden" name="paymentInputStatus" id="paymentInputStatus">
				
				<!-- 배차정보 입력,수정시 결제정보(업체청구액,기사지급액)가 입력 되어 있지 않은경우 기사가 지정 되어도 미배차 상태에서 진행 할 수 없도록 한다.  -->

				<!-- 한줄입력 삭제 -->
				<!-- <input type="hidden"  name="comment" id="comment"> -->
				<input type="hidden" name="memo" id="memo" value=""> 
				<input type="hidden" name="carInfoVal" id="carInfoVal" value=""> 
				<input type="hidden" name="paymentInfoVal" id="paymentInfoVal" value="">
				


				<div style="text-align: right; clear: both;">&nbsp;</div>
				<br>


				<div class="content-container interest-page loadingthemepage news"
					style="background-color: #fff; margin-top: 20px; text-align: center; clear: both;">
					<!-- <span class="d-tbc" style="font-weight: bold; color:#00000;">간편 탁송 예약</span> -->
					<h4 class="d-tbc" style="font-weight: bold; color: #00000;">
						<img class="blinking" src="/img/fireworker.jpg"
							style="margin-top: 0%; width: 8%; height: 6%">다수 탁송 예약 ${paramMap.nowCount} 건
					</h4>
				</div>




				<div style="width: 100%; overflow: hidden;">
					<div style="float: left; width: 50%;">
						<div class="certNum" style="margin-top: 10%; width: 100%;">
							<div style="text-align: center;">
								<div style="text-align: left; margin-left: 5%;">
									<span class="d-tbc" style="font-weight: bold;">담당자</span>
								</div>
								<input type="text"
									style="width: 90%; padding: 3%; margin-top: 5%;"
									placeholder="담당자" class="d-tbc" id="chargeName"
									name="chargeName" value="${userMap.name}" readonly
									onfocus="javascript:$.alert('담당자명은 수정이 불가능합니다.');">
							</div>
						</div>
					</div>
					<div style="float: left; width: 50%;">
						<div class="certNum" style="margin-top: 10%; width: 100%;">
							<div style="text-align: center;">
								<div style="text-align: left; margin-left: 5%;">
									<span class="d-tbc" style="font-weight: bold;">담당자 연락처</span>
								</div>
								<input type="text"
									style="width: 90%; padding: 3%; margin-top: 5%;"
									placeholder="담당자 연락처" class="d-tbc" id="chargePhone"
									name="chargePhone" value="${userMap.phoneNum}" readonly
									onfocus="javascript:$.alert('담당자 연락처는 수정이 불가능합니다.');">
							</div>
						</div>
					</div>
				</div>
				<div style="clear: both; margin-top: 5%;">
					<div class="certNum" style="width: 100%; margin-top: 5%;">
						<div style="text-align: center;">
							<div style="text-align: left; margin-left: 2.5%;">
								<span class="d-tbc" style="font-weight: bold;">고객명</span>
							</div>
							<input type="text"
								style="width: 95%; padding: 2%; margin-top: 2%;"
								placeholder="고객명" class="d-tbc" id="customerName"
								name="customerName" value="${userMap.customer_name}" readonly
								onfocus="javascript:$.alert('고객명은 수정이 불가능합니다.');">
						</div>
					</div>
				</div>

				<%-- 		<div style=" width:100%; overflow:hidden;">
			                 <div style="float:left; width:50%;">
			                 	<div class="certNum" style=" margin-top: 10%; width:100%;">
			                 		<div style="text-align:center;">
				                 		<div style="text-align:left; margin-left:5%;">
											<span class="d-tbc" style="font-weight: bold;">운행정보</span>
										</div> 
					         			<input type="text" style=" width:90%; padding:3%; margin-top: 5%;" placeholder="셀프" class="d-tbc" id="phoneCertNum" name="phoneCertNum" value="셀프" readonly>
				         			</div>
				      			</div>
				      		</div> 
				      		<div style="float:left; width:50%;">
			                	<div class="certNum" style=" margin-top: 10%; width:100%;">
			                 		<div style="text-align:center;">
				                 		<div style="text-align:left; margin-left:5%;">
											<span class="d-tbc" style="font-weight: bold;">고객명</span>
										</div> 
					         			<input type="text" style=" width:90%; padding:3%; margin-top: 5%;" placeholder="고객명" class="d-tbc" id="phoneCertNum" name="phoneCertNum" value="${phoneCertNum}" onkeypress="if(event.keyCode=='13') sendPinNum();">
				         			</div>
				      			</div>
				      		</div>
				      	</div> --%>

		
				<%-- <div style=" width:100%; overflow:hidden;">
			                 <div style="float:left; width:50%;">
			                 	<div class="certNum" style=" margin-top: 10%; width:100%;">
			                 		<div style="text-align:center;">
				                 		<div style="text-align:left; margin-left:2.5%;">
				                 			<span style="color:#F00;">*</span>
												<span class="d-tbc" style="font-weight: bold;">탁송 구분</span>
										</div> 
					         			<input type="text" style=" width:90%; padding:3%; margin-top: 5%;" placeholder="기사명" class="d-tbc" id="chargeName" name="chargeName" value="${userMap.name}" readonly onfocus="javascript:$.alert('담당자명은 수정이 불가능합니다.');" >
				         				<select name="companyKind"  id="companyKind" style="width:75%; padding:3%; margin-top:5%;">
										<option value=""  selected="selected"> 선택</option>
										<option value="S" >셀프</option>
										<option value="C" >캐리어</option>
										</select>
				         			</div>
				      			</div>
				      		</div>
				      		<div style="float:left; width:50%;">
			                	<div class="certNum" style=" margin-top: 10%; width:100%;">
			                 		<div style="text-align:center;">
				                 		<div style="text-align:left; margin-left:2.5%; margin-bottom:8%;">
				                 			<span style="color:#F00;">*</span>
												<span class="d-tbc" style="font-weight: bold;"> 셀프/캐리어 소개</span>
										</div> 
					         					<span class="d-tbc" style="font-weight: bold; "onclick="javascript:goSelfAndCarrier();"> 셀프와 캐리어가 헷갈린다면..? </span>
				         			</div>
				      			</div>
				      		</div>
				      	</div> --%>
		



				<div style="width: 100%; overflow: hidden;">
					<div style="float: left; width: 50%;">
						<div class="certNum" style="margin-top: 10%; width: 100%;">
							<div style="text-align: center;">
								<div style="text-align: left; margin-left: 5%;">
									<span style="color: #F00;">*</span> <span class="d-tbc"
										style="font-weight: bold;">출발일</span>
								</div>
								<c:if test="${paramMap.allocationId eq null && paramMap.tempId eq null}">
									<input type="date"
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="출발일" class="d-tbc" id="departureDt"
										name="departureDt" value="">
								</c:if>
								<c:if test="${paramMap.allocationId ne null}">
									<input type="date"
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="출발일" class="d-tbc" id="departureDt"
										name="departureDt" value="${allocationMap.departure_dt}">
								</c:if>
								
								<c:if test="${paramMap.tempId ne null}">
									<input type="date"
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="출발일" class="d-tbc" id="departureDt"
										name="departureDt" value="${tempConsignmentMap.departure_dt}">
								</c:if>
								
	
							</div>
						</div>
					</div>
					<div style="float: left; width: 50%;">
						<div class="certNum" style="margin-top: 10%; width: 100%;">
							<div style="text-align: center;">
								<div style="text-align: left; margin-left: 5%;">
									<span style="color: #F00;">*</span> <span class="d-tbc"
										style="font-weight: bold;">출발시간</span>
								</div>
								<c:if test="${paramMap.allocationId eq null && paramMap.tempId eq null}">
									<input type="time"
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="출발시간" class="d-tbc" id="departureTime"
										name="departureTime" value="">
								</c:if>
								<c:if test="${paramMap.allocationId ne null}">
									<input type="time"
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="출발시간" class="d-tbc" id="departureTime"
										name="departureTime" value="${allocationMap.departure_time}">
								</c:if>
								<c:if test="${paramMap.tempId ne null}">
									<input type="time"
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="출발시간" class="d-tbc" id="departureTime"
										name="departureTime" value="${tempConsignmentMap.departure_time}">
								</c:if>
							
							</div>
						</div>
					</div>
				</div>
				<div style="clear: both; margin-top: 5%;">
					<div id="btn_group" class="certNum"
						style="width: 100%; margin-top: 5%;">
						<div style="text-align: center;">
							<div style="text-align: left; margin-left: 2.5%;">
								<span style="color: #F00;">*</span> <span class="d-tbc"
									style="font-weight: bold;">출발지</span>
							</div>
							<c:if test="${paramMap.allocationId eq null && paramMap.tempId eq null}">
								<input type="text"
									style="width: 78%; padding: 2%; margin-top: 2%;"
									placeholder="출발지 입력" id="searchKeyword" name="departureAddr"
									value="" onfocus="javascript:goDepartureInit(this);">
							</c:if>

							<c:if test="${paramMap.allocationId ne null}">
								<input type="text"
									style="width: 78%; padding: 2%; margin-top: 2%;"
									placeholder="출발지 입력" id="searchKeyword" name="departureAddr"
									value="${allocationMap.departure_addr}"
									onfocus="javascript:goDepartureInit(this);">
							</c:if>
							<c:if test="${paramMap.tempId ne null}">
								<input type="text"
									style="width: 78%; padding: 2%; margin-top: 2%;"
									placeholder="출발지 입력" id="searchKeyword" name="departureAddr"
									value="${tempConsignmentMap.departure_addr}"
									onfocus="javascript:goDepartureInit(this);">
							</c:if>
							
						
							<input id="departureAddress" type="hidden" value="">
							<button id="test_btn1" class="btn_select" style="width: 15%;"
								onclick="javascirpt:goShowModal(); return false;">검색</button>
						</div>
					</div>
				</div>
				<div class="modal-field">
					<div class="modal-box">
						<div class="modal-table-container">
							<div class="title" style="text-align: center; margin-top: 5%;">
								출발지&nbsp;<strong>검색</strong> 목록
							</div>
							<br>
							<div class="rst_wrap">
								<div class="rst mCustomScrollbar">
									<ul id="searchResult" name="searchResult">
									</ul>
								</div>
							</div>

						</div>
						<div class="confirmation">
							<div class="confirm" style="text-align: center;">
								<input type="button" value="창닫기" name="" id="departureCansle"
									onclick="javascript:$('.modal-field').hide();">
							</div>
						</div>
					</div>
				</div>
				<div style="width: 100%; overflow: hidden;">
					<div style="float: left; width: 50%;">
						<div class="certNum" style="margin-top: 10%; width: 100%;">
							<div style="text-align: center;">
								<div style="text-align: left; margin-left: 5%;">
									<span class="d-tbc" style="font-weight: bold;">출발지 담당자명</span>
								</div>
								<c:if test="${paramMap.allocationId eq null && paramMap.tempId eq null}">
									<input type="text"
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="담당자명" class="d-tbc" id="departurePersonInCharge"
										name="departurePersonInCharge" value="">
								</c:if>
								<c:if test="${paramMap.allocationId ne null}">
									<input type="text"
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="담당자명" class="d-tbc" id="departurePersonInCharge"
										name="departurePersonInCharge"
										value="${allocationMap.departure_person_in_charge}">
								</c:if>
								<c:if test="${paramMap.tempId ne null}">
									<input type="text"
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="담당자명" class="d-tbc" id="departurePersonInCharge"
										name="departurePersonInCharge"
										value="${tempConsignmentMap.departure_person_in_charge}">
								</c:if>
								
							</div>
						</div>
					</div>
					<div style="float: left; width: 50%;">
						<div class="certNum" style="margin-top: 10%; width: 100%;">
							<div style="text-align: center;">
								<div style="text-align: left; margin-left: 5%;">
									<span class="d-tbc" style="font-weight: bold;">출발지 연락처</span>
								</div>
								<c:if test="${paramMap.allocationId eq null && paramMap.tempId eq null}">
									<input type="number" pattern="\d*"
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="연락처" class="d-tbc" id="departurePhone"
										name="departurePhone" value="">
								</c:if>
								<c:if test="${paramMap.allocationId ne null}">
									<input type="number" pattern="\d*"
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="연락처" class="d-tbc" id="departurePhone"
										name="departurePhone" value="${allocationMap.departure_phone}">
								</c:if>
								<c:if test="${paramMap.tempId ne null}">
									<input type="number" pattern="\d*"
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="연락처" class="d-tbc" id="departurePhone"
										name="departurePhone" value="${tempConsignmentMap.departure_phone}">
								</c:if>


							</div>
						</div>
					</div>
				</div>
				<div style="clear: both; margin-top: 5%;">
					<div id="btn_group" class="certNum"
						style="width: 100%; margin-top: 5%;">
						<div style="text-align: center;">
							<div style="text-align: left; margin-left: 2.5%;">
								<span style="color: #F00;">*</span> <span class="d-tbc"
									style="font-weight: bold;">도착지</span>
							</div>
							<c:if test="${paramMap.allocationId eq null && paramMap.tempId eq null}">
								<input type="text"
									style="width: 78%; padding: 2%; margin-top: 2%;"
									placeholder="도착지 입력" class="d-tbc" id="searchKeyword2"
									name="arrivalAddr" value=""
									onfocus="javascript:goArrivalInit(this)">
							</c:if>
							<c:if test="${paramMap.allocationId ne null}">
								<input type="text"
									style="width: 78%; padding: 2%; margin-top: 2%;"
									placeholder="도착지 입력" class="d-tbc" id="searchKeyword2"
									name="arrivalAddr" value="${allocationMap.arrival_addr}"
									onfocus="javascript:goArrivalInit(this)">
							</c:if>
							<c:if test="${paramMap.tempId ne null}">
								<input type="text"
									style="width: 78%; padding: 2%; margin-top: 2%;"
									placeholder="도착지 입력" class="d-tbc" id="searchKeyword2"
									name="arrivalAddr" value="${tempConsignmentMap.arrival_addr}"
									onfocus="javascript:goArrivalInit(this)">
							</c:if>
							
							<input id="arrivalAddress" type="hidden" value="">
							<button id="test_btn1" type="button" class="btn_select2"
								style="width: 15%;"
								onclick="javascirpt:goShowModal2(); return false;">검색</button>
						</div>
					</div>
				</div>
				<div style="width: 100%; overflow: hidden;">
					<div style="float: left; width: 50%;">
						<div class="certNum" style="margin-top: 10%; width: 100%;">
							<div style="text-align: center;">
								<div style="text-align: left; margin-left: 5%;">
									<span class="d-tbc" style="font-weight: bold;">도착지 담당자명</span>
								</div>
								<c:if test="${paramMap.allocationId eq null && paramMap.tempId eq null}">
									<input type="text"
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="담당자명" class="d-tbc" id="arrivalPersonInCharge"
										name="arrivalPersonInCharge" value="">
								</c:if>
								<c:if test="${paramMap.allocationId ne null}">
									<input type="text"
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="담당자명" class="d-tbc" id="arrivalPersonInCharge"
										name="arrivalPersonInCharge"
										value="${allocationMap.arrival_person_in_charge}">
								</c:if>
								
									<c:if test="${paramMap.tempId ne null}">
									<input type="text"
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="담당자명" class="d-tbc" id="arrivalPersonInCharge"
										name="arrivalPersonInCharge"
										value="${tempConsignmentMap.arrival_person_in_charge}">
								</c:if>
							</div>
						</div>
					</div>
					<div style="float: left; width: 50%;">
						<div class="certNum" style="margin-top: 10%; width: 100%;">
							<div style="text-align: center;">
								<div style="text-align: left; margin-left: 5%;">
									<span class="d-tbc" style="font-weight: bold;">도착지 연락처</span>
								</div>
								<c:if test="${paramMap.allocationId eq null && paramMap.tempId eq null}">
									<input type="number" pattern="\d*"
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="연락처" class="d-tbc" id="arrivalPhone"
										name="arrivalPhone" value="">
								</c:if>
								<c:if test="${paramMap.allocationId ne null}">
									<input type="number" pattern="\d*"
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="연락처" class="d-tbc" id="arrivalPhone"
										name="arrivalPhone" value="${allocationMap.arrival_phone}">
								</c:if>
									<c:if test="${paramMap.tempId ne null}">
									<input type="number" pattern="\d*"
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="연락처" class="d-tbc" id="arrivalPhone"
										name="arrivalPhone" value="${tempConsignmentMap.arrival_phone}">
								</c:if>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-field2">
					<div class="modal-box">
						<div class="modal-table-container">
							<div class="title" style="text-align: center; margin-top: 5%;">
								도착지&nbsp;<strong>검색</strong> 목록
							</div>
							<br>
							<div class="rst_wrap">
								<div class="rst mCustomScrollbar">
									<ul id="searchResult2" name="searchResult2">
									</ul>
								</div>
							</div>

						</div>
						<div class="confirmation">
							<div class="confirm" style="text-align: center;">
								<input type="button" value="창닫기" name="" id="departureCansle"
									onclick="javascript:$('.modal-field2').hide();">
							</div>
						</div>
					</div>
				</div>

				<div style="width: 100%; overflow: hidden;">
					<div style="float: left; width: 50%;">
						<div class="certNum" style="margin-top: 10%; width: 100%;">
							<div style="text-align: center;">
								<div style="text-align: left; margin-left: 5%;">
									<span style="color: #F00;">*</span> <span class="d-tbc"
										style="font-weight: bold;">차종</span>
								</div>
								<c:if test="${paramMap.allocationId eq null && paramMap.tempId eq null}">
									<input type="text"
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="차종" class="d-tbc" id="carKind" name="carKind"
										value="">
								</c:if>
								<c:if test="${paramMap.allocationId ne null}">
									<input type="text"
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="차종" class="d-tbc" id="carKind" name="carKind"
										value="${allocationMap.car_kind}">
								</c:if>
								
									<c:if test="${paramMap.tempId ne null}">
									<input type="text"
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="차종" class="d-tbc" id="carKind" name="carKind"
										value="${tempConsignmentMap.car_kind}">
								</c:if>
							</div>
						</div>
					</div>
					<div style="float: left; width: 50%;">
						<div class="certNum" style="margin-top: 10%; width: 100%;">
							<div style="text-align: center;">
								<div style="text-align: left; margin-left: 5%;">
									<span style="color: #F00;">*</span> <span class="d-tbc"
										style="font-weight: bold;">차대번호(뒤6자리)</span>
								</div>
								<c:if test="${paramMap.allocationId eq null && paramMap.tempId eq null}">
									<input type="text"
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="차대번호(뒤6자리)" class="d-tbc" id="carIdNum"
										name="carIdNum" value="">
								</c:if>
								<c:if test="${paramMap.allocationId ne null}">
									<input type="text"
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="차대번호(뒤6자리)" class="d-tbc" id="carIdNum"
										name="carIdNum" value="${allocationMap.car_id_num}">
								</c:if>
								<c:if test="${paramMap.tempId ne null}">
									<input type="text"
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="차대번호(뒤6자리)" class="d-tbc" id="carIdNum"
										name="carIdNum" value="${tempConsignmentMap.car_id_num}">
								</c:if>
								
							</div>
						</div>
					</div>
				</div>
				<div style="width: 100%; overflow: hidden;">
					<div style="float: left; width: 50%;">
						<div class="certNum" style="margin-top: 10%; width: 100%;">
							<div style="text-align: center;">
								<div style="text-align: left; margin-left: 5%;">
									<span style="color: #F00;">*</span> <span class="d-tbc"
										style="font-weight: bold;">차량번호</span>
								</div>
								<c:if test="${paramMap.allocationId eq null && paramMap.tempId eq null}">
									<input type="text"
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="차량번호" class="d-tbc" id="carNum" name="carNum"
										value="">
								</c:if>
								<c:if test="${paramMap.allocationId ne null}">
									<input type="text"
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="차량번호" class="d-tbc" id="carNum" name="carNum"
										value="${allocationMap.car_num}">
								</c:if>
								
									<c:if test="${paramMap.tempId ne null}">
									<input type="text"
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="차량번호" class="d-tbc" id="carNum" name="carNum"
										value="${tempConsignmentMap.car_num}">
								</c:if>
							</div>
						</div>
					</div>

					<div id="showDistence" 	style="float: left; width: 50%; display:none;">
						<div class="certNum" style="margin-top: 10%; width: 100%;">
							<div style="text-align: center; margin-right: 10%;">
								<div id="btn_group"style="text-align: right; margin-left: 5%;">
									<span class="d-tbc" style="font-weight: bold;">검색 예상 거리</span><br>
										<c:if test="${paramMap.allocationId eq null}">
									   	 <button id="test_btn1" class="btn_select" style="width: 50%; float:left;" onclick="javascirpt:showDistance();">경로 보기</button> 
									</c:if>
								</div> 
								<div style="text-align: right; margin-left: 5%; margin-top: 10%;">
									<!-- <input type="text" style=" width:90%; padding:3%; margin-top: 5%;" placeholder="예상거리" class="d-tbc" id="carNum" name="carNum" value="" > -->
									<span class="d-tbc" id="getDis"
										style="font-weight: bold; color: #FF4000;"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
		<c:if test ="${customerDetailMap.control_payment eq 'Y'}">	
			<div style="width: 100%; overflow: hidden;">
					<div style="float: left; width: 50%;">
						<div class="certNum" style="margin-top: 10%; width: 100%;">
							<div style="text-align: center;">
								<div style="text-align: left; margin-left: 5%;">
									<span style="color: #F00;"></span> <span class="d-tbc"
										style="font-weight: bold;">탁송료</span>
								</div>
								<c:if test="${paramMap.allocationId eq null}">
									<input type="number" pattern="\d*"
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="금액을 입력해주세요." class="d-tbc" id="customerPayment" name="customerPayment"
										value="">
								</c:if>
								<c:if test="${paramMap.allocationId ne null}">
									<input type="number" pattern="\d*"
										style="width: 90%; padding: 3%; margin-top: 5%;"
										placeholder="금액을 입력해주세요." class="d-tbc" id="customerPayment" name="customerPayment"
										value="${allocationMap.sales_total}">
								</c:if>
								
						
							</div>
						</div>
					</div>
				</div>
			</c:if>
				
			<div style="clear: both; margin-top: 5%;">
					<div class="certNum" style="width: 100%; margin-top: 5%;">
						<div style="text-align: center;">
							<div style="text-align: left; margin-left: 2.5%;">
								<span class="d-tbc" style="font-weight: bold;">비고 작성</span>
							</div>
								<c:if test="${paramMap.allocationId eq null}">
									<textarea id="customerEtc" name="customerEtc" rows="6" cols="60" style=" margin-top: 5%;"
									placeholder="비고 내용을 입력해주세요." ></textarea>
							</c:if	>
							<c:if test="${paramMap.allocationId ne null}">
							<textarea id="customerEtc" name="customerEtc" rows="6" cols="60" style=" margin-top: 5%;"
									placeholder="비고 내용을 입력해주세요." >${allocationMap.etc}</textarea>
							</c:if>
						</div>
					</div>
				</div>





				<%--      <c:if test ="${customerDetailMap.customer_kind eq '01' }">
				      	<div id="mileageBox" style=" width:100%; overflow:hidden;">
			                 <div style="float:left; width:50%;">
			                 	<div class="certNum" style=" margin-top: 10%; width:100%;">
			                 		<div style="text-align:center;">
				                 		<div style="text-align:left; margin-left:5%;">
				                 			<span style="color:#F00;"></span>
												<span class="d-tbc" style="font-weight: bold;">보유 마일리지</span>
										</div> 
					         			<input type="text" style=" width:90%; padding:3%; margin-top: 5%;" placeholder="마일리지가 없습니다." class="d-tbc" id="" name="" 
					         			value="${mileageCount.total}M ">
				         			</div>
				      			</div>
				      		</div>
				      	
						    <div style="float:left; width:50%;">
			                 	<div class="certNum" style=" margin-top: 10%; width:100%;">
			                 	<c:if test='${mileageCount.total > "10000"}'>
			                 		<div id ="availableMileage" style="text-align:center;">
				                 		<div style="text-align:left; margin-left:5%;">
				                 			<span style="color:#F00;"></span>
												<span class="d-tbc" style="font-weight: bold;">사용 가능 마일리지 </span>
										</div> 
							         		<select name ="chooseMileage" id ="chooseMileage" style="width:85%; padding:4%; margin-top: 5%;">
												<option value="" selected>선택</option>
													<c:forEach var="val" begin="1000" end="${paramMap.availableMileage}" step="1000" varStatus="status">
										    				<option value="${val}" ><fmt:formatNumber value="${val}" groupingUsed="true"/> M</option>
													</c:forEach>
											</select> 
				         				</div>
				         			</c:if>
						         			
						         	<div id ="availableMileage" style="text-align:center;">
						                 		<div style="text-align:left; margin-left:5%;">
						                 			<span style="color:#F00;"></span>
														<span class="d-tbc" style="font-weight: bold;">사용 가능 마일리지 </span>
												</div> 
											<input type="text" style=" width:90%; padding:3%; margin-top: 5%;" placeholder="마일리지" class="d-tbc" id="mileage" name="mileage" value="" >	
						         	</div>
						         			
				         		<c:if test='${mileageCount.total < "10000" || mileageCount eq null }'>
			                 		<div id ="canNotUseMileage" style="text-align:center; margin-top: 20%;">
				                 		<div style="text-align:left; margin-left:5%;">
				                 			<span style="color:#F00;"></span>
												<span class="d-tbc" style="font-weight: bold;">
													마일리지는 10,000M <br>이상부터 사용 가능합니다.
												</span>
										</div> 
				         				</div>
				         			</c:if>
				      			</div>
				      	</div>
				      		
				    </div>
				 </c:if>	 --%>
				<p style="font-size: 12px; margin-top: 5%; text-align: center;">
					<span style="color: #F00;">*</span>차대번호 또는 차량번호 둘 중 하나는 반드시 작성 되어야 합니다.<br><br>
					<span style="color: #F00;">*</span>입력하신 정보와 실제 정보가 <strong>다를 경우</strong> 탁송이 <strong><span style="color:red;">취소</span></strong>될 수 있습니다.
				</p>
				<br>

				<!-- <h5 class="d-tbc" style="font-weight: bold; color:#00000;">	<img class="blinking" src="/img/kakaotalkask.jpg" style ="margin-top:0%; width:8%; height:6%">한국카캐리어(주) 채널을 추가 해주시면 예약확정시 <span style="color:#FE9A2E;">알림톡</span>이 발송됩니다.</h5> -->

				<div
					style="margin-top: 5%; text-align: center; display: inline-block; margin-left: 20%; width: 50%; margin-bottom: 20%;">
					<div
						style="border-radius: 50%; height: 30%; width: 40%; background-color: #CEF6F5; margin-left: 40%;">
						<c:if test="${paramMap.allocationId eq null}">
							<a
								style="cursor: pointer; width: 100%; border-radius: 80%; background: #CEF6F5;"
								onclick="javascript:goReservationConsignment();" class="">탁송
								예약</a>
						</c:if>
						<c:if test="${paramMap.allocationId ne null}">
							<a
								style="cursor: pointer; width: 100%; border-radius: 80%; background: #CEF6F5;"
								onclick="javascript:goReWriteReservation();" class="">정보
								&nbsp;수정</a>
						</c:if>
						
						
						
						
					</div>
				</div>
			</form>

		</div>
	</div>

</body>


<script
	src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
<script src="/js/vendor/bootstrap.min.js"></script>
<!-- veiwport for countnumber -->
<script src="/js/alert.js"></script>
<script src="/js/main.js"></script>
<script src="/js/vendor/xpull.js"></script>
<script src="/js/vendor/jquery.loading-indicator.min.js"></script>
<script src="https://apis.openapi.sk.com/tmap/jsv2?version=1&appKey=l7xx6b701ff5595c4e3b859e42f57de0c975"></script>
<script src="/js/animsition.min.js"></script>
<script src="/js/vendor/jquery.form.min.js"></script>

<script>
    
	
	$('#container').xpull({
	    'paused': false, 
	    // Pull threshold - amount in  pixels required to pull to enable release callback
        'pullThreshold':50,
        // Max pull down element - amount in pixels
        maxPullThreshold: 50,
        // timeout in miliseconds after which the loading indicator stops spinning.
        // If set to 0 - the loading will be indefinite
        'spinnerTimeout':1000,
        onPullStart: function(){},
        onPullEnd: function(){},
        callback: function(){window.location.reload()},

	});   
	
	
	var homeLoader;
	var inputStatus = "departure";
	 
	$(document).ready(function(){
			
		
				homeLoader = $('body').loadingIndicator({
					useImage: false,
					showOnInit : false
				}).data("loadingIndicator");
				
				/* setTimeout(function() {
					homeLoader.show();
					homeLoader.hide();
				}, 1000); */
		
	});


	    var map, marker;
	    var markerArr = [];
	    
	    function initTmap(){
	   
	    	// 2. POI 통합 검색 API 요청
	    	$(".btn_select").click(function(){

	    		if($('#searchKeyword').val() == ""){	
		    	//	$.alert("출발지가 입력되지 않았습니다.");
					return false;
		    	}
	    		
	    		var searchKeyword = $('#searchKeyword').val();
	    		$("#searchResult").css("display","block");

	    		$.ajax({
	    			method:"GET",
	    			url:"https://apis.openapi.sk.com/tmap/pois?version=1&format=json&callback=result",
	    			async:false,
	    			data:{
	    				"appKey" : "l7xx6b701ff5595c4e3b859e42f57de0c975",
	    				"searchKeyword" : searchKeyword,
	    				"resCoordType" : "EPSG3857",
	    				"reqCoordType" : "WGS84GEO",
	    				"count" : 10
	    			},
	    			success:function(response){
	    				var resultpoisData = response.searchPoiInfo.pois.poi;

	    				
	    				
	    				// 기존 마커, 팝업 제거
	    				if(markerArr.length > 0){
	    					for(var i in markerArr){
	    						markerArr[i].setMap(null);
	    					}
	    				}
	    				var innerHtml ="";	// Search Reulsts 결과값 노출 위한 변수
	    				var positionBounds = new Tmapv2.LatLngBounds();		//맵에 결과물 확인 하기 위한 LatLngBounds객체 생성
	    				
	    				for(var k in resultpoisData){
	    					
	    					var noorLat = Number(resultpoisData[k].noorLat);
	    					var noorLon = Number(resultpoisData[k].noorLon);
	    					var name = resultpoisData[k].name;

	    					var upperAddrName = resultpoisData[k].upperAddrName;
	    					var middleAddrName = resultpoisData[k].middleAddrName;
	    					var lowerAddrName = resultpoisData[k].lowerAddrName;
	    					var detailAddrName = resultpoisData[k].detailAddrName;
	    					var firstNo = resultpoisData[k].firstNo;
	    					var secondNo = resultpoisData[k].secondNo == "" ? "" : "-"+resultpoisData[k].secondNo;
							/* alert(upperAddrName+middleAddrName+lowerAddrName+detailAddrName); */

							
	    					
	    					var pointCng = new Tmapv2.Point(noorLon, noorLat);
	    					var projectionCng = new Tmapv2.Projection.convertEPSG3857ToWGS84GEO(pointCng);
	    					
	    					var lat = projectionCng._lat;
	    					var lon = projectionCng._lng;
	    					
	    					var markerPosition = new Tmapv2.LatLng(lat, lon);
	    					
	    					marker = new Tmapv2.Marker({
	    				 		position : markerPosition,
	    				 		//icon : "http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_a.png",
	    				 		icon : "http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_" + k + ".png",
	    						iconSize : new Tmapv2.Size(24, 38),
	    						title : name,
	    						//map:map
	    				 	});
	    					
	    					//innerHtml += "<li><img src='http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_" + k + ".png' style='vertical-align:middle;'/><span onclick='javascript:inputValue(\''+name+'\')'>"+name+"</span></li>";
	    					//innerHtml += "<li><img src='http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_" + k + ".png' style='vertical-align:middle;'/><span onclick='javascript:inputValue()'>"+name+"</span></li>";
	    					//'<div class="info" onclick="javascript:inputValue(\''+places.road_address_name+'\',\''+places.address_name+'\',\''+places.place_name+'\',\''+places.x+'\',\''+places.y+'\')">' +
							//innerHtml += '<li><img src="http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_' + k + '.png" style="vertical-align:middle;"/><span onclick="javascript:inputValue(\''+name+'\',\''+lon+'\',\''+lat+'\')">'+name+'</span></li>';
							
							innerHtml += '<li  onclick="javascript:inputValue(\''+upperAddrName+" "+middleAddrName+" "+lowerAddrName+" "+detailAddrName+" "+firstNo+secondNo+'\',\''+lon+'\',\''+lat+'\')">';    					
							innerHtml += '<img src="http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_' + k + '.png" style="vertical-align:middle;"/>';
							innerHtml += '<span>'+name+'</span>';
							innerHtml += '<span style="display:block; color:#3f3f3f;" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;('+upperAddrName+" "+middleAddrName+" "+lowerAddrName+" "+detailAddrName+" "+firstNo+secondNo+')</span>';
							innerHtml += '</li>';

	    					
	    					
	    					markerArr.push(marker);
	    					positionBounds.extend(markerPosition);	// LatLngBounds의 객체 확장
	    				}
	    				
	    				$("#searchResult").html(innerHtml);	//searchResult 결과값 노출
	    				//map.panToBounds(positionBounds);	// 확장된 bounds의 중심으로 이동시키기
	    				//map.zoomOut();
	    				
	    			},
	    			error:function(request,status,error){
	    				console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	    			}
	    		});
	    	});


	$(".btn_select2").click(function(){

				if($('#searchKeyword2').val() == ""){	
		    		//$.alert("도착지가 입력되지 않았습니다.");
					return false;
		    	}
		
	    		var searchKeyword = $('#searchKeyword2').val();
	    		$("#searchResult2").css("display","block");

	    		$.ajax({
	    			method:"GET",
	    			url:"https://apis.openapi.sk.com/tmap/pois?version=1&format=json&callback=result",
	    			async:false,
	    			data:{
	    				"appKey" : "l7xx6b701ff5595c4e3b859e42f57de0c975",
	    				"searchKeyword" : searchKeyword,
	    				"resCoordType" : "EPSG3857",
	    				"reqCoordType" : "WGS84GEO",
	    				"count" : 10
	    			},
	    			success:function(response){
	    				var resultpoisData = response.searchPoiInfo.pois.poi;

	    				
	    				
	    				// 기존 마커, 팝업 제거
	    				if(markerArr.length > 0){
	    					for(var i in markerArr){
	    						markerArr[i].setMap(null);
	    					}
	    				}
	    				var innerHtml ="";	// Search Reulsts 결과값 노출 위한 변수
	    				var positionBounds = new Tmapv2.LatLngBounds();		//맵에 결과물 확인 하기 위한 LatLngBounds객체 생성
	    				
	    				for(var k in resultpoisData){
	    					
	    					var noorLat = Number(resultpoisData[k].noorLat);
	    					var noorLon = Number(resultpoisData[k].noorLon);
	    					var name = resultpoisData[k].name;

	    					var upperAddrName = resultpoisData[k].upperAddrName;
	    					var middleAddrName = resultpoisData[k].middleAddrName;
	    					var lowerAddrName = resultpoisData[k].lowerAddrName;
	    					var detailAddrName = resultpoisData[k].detailAddrName;
	    					var firstNo = resultpoisData[k].firstNo;
	    					var secondNo = resultpoisData[k].secondNo == "" ? "" : "-"+resultpoisData[k].secondNo;
							/* alert(upperAddrName+middleAddrName+lowerAddrName+detailAddrName); */

							
	    					
	    					var pointCng = new Tmapv2.Point(noorLon, noorLat);
	    					var projectionCng = new Tmapv2.Projection.convertEPSG3857ToWGS84GEO(pointCng);
	    					
	    					var lat = projectionCng._lat;
	    					var lon = projectionCng._lng;
	    					
	    					var markerPosition = new Tmapv2.LatLng(lat, lon);
	    					
	    					marker = new Tmapv2.Marker({
	    				 		position : markerPosition,
	    				 		//icon : "http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_a.png",
	    				 		icon : "http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_" + k + ".png",
	    						iconSize : new Tmapv2.Size(24, 38),
	    						title : name,
	    						//map:map
	    				 	});
	    					
	    					//innerHtml += "<li><img src='http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_" + k + ".png' style='vertical-align:middle;'/><span onclick='javascript:inputValue(\''+name+'\')'>"+name+"</span></li>";
	    					//innerHtml += "<li><img src='http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_" + k + ".png' style='vertical-align:middle;'/><span onclick='javascript:inputValue()'>"+name+"</span></li>";
	    					//'<div class="info" onclick="javascript:inputValue(\''+places.road_address_name+'\',\''+places.address_name+'\',\''+places.place_name+'\',\''+places.x+'\',\''+places.y+'\')">' +
							//innerHtml += '<li><img src="http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_' + k + '.png" style="vertical-align:middle;"/><span onclick="javascript:inputValue(\''+name+'\',\''+lon+'\',\''+lat+'\')">'+name+'</span></li>';
							
							innerHtml += '<li  onclick="javascript:inputValue(\''+upperAddrName+" "+middleAddrName+" "+lowerAddrName+" "+detailAddrName+" "+firstNo+secondNo+'\',\''+lon+'\',\''+lat+'\')">';    					
							innerHtml += '<img src="http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_' + k + '.png" style="vertical-align:middle;"/>';
							innerHtml += '<span>'+name+'</span>';
							innerHtml += '<span style="display:block; color:#3f3f3f;" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;('+upperAddrName+" "+middleAddrName+" "+lowerAddrName+" "+detailAddrName+" "+firstNo+secondNo+')</span>';
							innerHtml += '</li>';

	    					
	    					
	    					markerArr.push(marker);
	    					positionBounds.extend(markerPosition);	// LatLngBounds의 객체 확장
	    				}
	    				
	    				$("#searchResult2").html(innerHtml);	//searchResult 결과값 노출
	    				//map.panToBounds(positionBounds);	// 확장된 bounds의 중심으로 이동시키기
	    				//map.zoomOut();
	    				
	    			},
	    			error:function(request,status,error){
	    				console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	    			}
	    		});
	    	});

	    }
	    	
	    	function inputValue(address,px,py){


				//alert(inputStatus);
	  
	    		
	    		var depatureTemp =$("#searchKeyword").val();
	    		var arrivalTemp =$("#searchKeyword2").val();
				var departureAddress =$("#departureAddress").val();
				var arrivalAddress =$("#arrivalAddress").val();
						

	    			if(inputStatus == "departure" ){

	    				
	    				$("#searchKeyword").val(address);
	    				$("#searchResult").css("display","none");
	    				$("#searchKeyword").next().val(px+","+py);
	    				$('.modal-field').css('display','none');

    					inputStatus = "arrival";

    	
	    				
	    			}else{

	    				
	    				$("#searchKeyword2").val(address);
	    				$("#searchResult2").css("display","none");
	    				$("#searchKeyword2").next().val(px+","+py);
	    				$('.modal-field2').css('display','none');
	    				
	    					
	    			}

				 	if(!departureAddress == "" && arrivalAddress == ""){
					 	
				 		homeLoader.show();
				 		
				 		 setTimeout(function() {
					 		 
								getDistence(departureAddress,arrivalAddress);
								homeLoader.hide();
							},500);
							


					} 


	    			
			    			
	    	}


    </script>


</body>
</html>
