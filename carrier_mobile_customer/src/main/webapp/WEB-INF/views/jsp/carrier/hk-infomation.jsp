<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
	
%>

<!DOCTYPE html>
<html lang="ko" style="height: 100%;">
<link rel="stylesheet" href="/css/bootstrap-datepicker.css">
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>

<body style="height: 100%;">


	<script type="text/javascript">
		$(document).ready(function() {

			var updateToken = setInterval(function() {
				clearInterval(updateToken);
				pageMoveStop = false;
				homeLoader.hide();

			}, 300);

		});

		function getDeviceToken() {

			var token = "";
			try {
				if (window.Android != null) {
					token = window.Android.getDeviceToken("${driverId}");
				}
			} catch (exception) {

			} finally {

			}
			return token;
		}

/* 		function updateDriverDeviceToken(token) {

			if (token != "") {
				$.ajax({
					type : 'post',
					url : "/updateDriverDeviceToken.do",
					dataType : 'json',
					data : {
						token : token
					},
					success : function(data, textStatus, jqXHR) {
						var result = data.resultCode;
						var resultData = data.resultData;
						if (result == "0000") {
							//$.alert("성공",function(a){
							//});
						} else if (result == "E000") {

						} else {

						}
					},
					error : function(xhRequest, ErrorText, thrownError) {
					}
				});
			}

		} */

		function getNewsData(obj, keyword) {

			// alert($("#selectedStatus").children().length);

			//alert($("#selectedStatus").children().first().get(0));	
			//alert($(obj).parent().get(0));

			if ($("#selectedStatus").children().first().get(0) == $(obj)
					.parent().get(0)) { //전체를 클릭 한경우

				$("#selectedStatus").children().each(function(index, element) {
					$(this).removeClass('selected');
				});
				$(obj).parent().addClass('selected');
			} else {

			}

			/* 	
			
			 $.ajax({ 
			 type: 'post' ,
			 url : "/research/newsSearch.do?&keyword="+keyword ,
			 dataType : 'json' ,
			 data : {
			
			 },
			 success : function(data, textStatus, jqXHR)
			 {
			 alert(data);
			 } ,
			 error : function(xhRequest, ErrorText, thrownError) {
			 }
			 });
			
			 */

		}

		function goNewsPage(uri) {

			//var replaceUrlStr = replaceAll(url, "?", "^");
			//replaceUrlStr = replaceAll(replaceUrlStr, "=", "+");

			var replaceUrlStr = encodeURIComponent(uri)
			document.location.href = "/news/newsPage.do?newsUri="
					+ replaceUrlStr;
		}

		function viewAllocationData(allocationId, allocationStatus) {
			homeLoader.show();
			document.location.href = "/carrier/allocation-detail.do?allocationStatus="
					+ allocationStatus + "&allocationId=" + allocationId;

			var updateHomeLoader = setInterval(function() {
				clearInterval(updateHomeLoader);
				homeLoader.hide();
			}, 300);
		}

		function viewAllocationList(allocationId, allocationStatus) {
			homeLoader.show();
			document.location.href = "/carrier/group-list.do?allocationId="
					+ allocationId + "&allocationStatus=" + allocationStatus;

			var updateHomeLoader = setInterval(function() {
				clearInterval(updateHomeLoader);
				homeLoader.hide();
			}, 300);
			
		}

		function logout() {

			$.confirm("로그아웃 하시겠습니까?", function(a) {
				if (a) {
					if (window.Android != null) {
						window.Android.stopService("${driverId}");
					}
					homeLoader.show();
					document.location.href = "/logout.do";	
				}
			});

		}

		function backKeyController(str) {

			homeLoader.show();
			//location.href = document.referrer; 
			document.location.href='/carrier/main.do';

			var updateHomeLoader = setInterval(function() {
				clearInterval(updateHomeLoader);
				homeLoader.hide();
			}, 300);
			
		}

		function goSearchMonthAllocation() {

			document.location.href = "/carrier/finish-list.do";

		}

		function routeView(departure, arrival) {

			document.location.href = "/carrier/routeView.do?&departure="
					+ departure + "&arrival=" + arrival;

		}

		function viewReview(allocationId){

			document.location.href ="/carrier/review-view.do?&allocationId="+allocationId;

		}

		function goWriteReview(customerId){

			document.location.href ="/carrier/review-list.do?&customerId="+customerId;
	
			}

		function getSearchDistance(driverId) {

			homeLoader.show();

			var driverId = driverId;

			document.location.href = "/carrier/routeView.do?&driverId="
					+ driverId;


			var updateHomeLoader = setInterval(function() {
				clearInterval(updateHomeLoader);
				homeLoader.hide();
			}, 300);
			
			/* 	
			 $.ajax({ 
			 type: 'post' ,
			 url : "/carrier/showDistance.do" ,
			 dataType : 'json' ,
			 async : false,
			 data : {

			 driverId : driverId
			
			 },
			 success : function(data, textStatus, jqXHR)
			 {
			 var result = data.resultCode;
			 var resultData = data.resultData;
			 if(result == "0000"){
			
			 routeView(resultData.departure,resultData.arrival);
			
			 }else if(result == "0001"){
			 $.alert("조회 하는데 실패 하였습니다.",function(a){
			 });
			 homeLoader.hide();
			 }else if(result == "0002"){
			 $.alert("에러",function(a){
			 });
			 homeLoader.hide();
			 }else if(result == "0003"){
			 $.alert("에러",function(a){
			 });
			 homeLoader.hide();
			 }
			 } ,
			 error : function(xhRequest, ErrorText, thrownError) {
			 }
			 });
			 */

		}
	</script>


	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
	<div class="content-container"
		style="height: 100%; overflow-y: hidden;">
		<%-- <jsp:include page="/WEB-INF/views/jsp/common-top.jsp"></jsp:include> --%>
		<header class="bg-pink clearfix"
			style="position: fixed; background-color: #fff; border: none; text-align: center;">
				<div class="" style="width: 19%;">
							<a onclick="javascript:homeLoader.show(); document.location.href='/carrier/main.do'"> <img style="zoom: 0.5;" src="/img/back-icon.png" alt=""></a>
				</div>
				<div class="" style="width: 60%;">
							<a onclick="javasript:homeLoader.show(); document.location.href='/carrier/main.do'"> <img style="zoom: 0.5;" src="/img/main_logo.png" alt=""></a>
				</div>

			<div class="" style="width: 19%; float: right;">
				<a style="cursor: pointer;" href="javascript:logout();">
				<img style="" src="/img/logout.png" alt=""></a>
			</div>

		</header>


		<%-- <div style="text-align:right;">${user.driver_name}님 환영 합니다.</div> --%>
		<div style="text-align: right; clear: both;">&nbsp;</div>

		<div class="content-container interest-page loadingthemepage news" style="background-color: #fff; clear: both; margin-top: 50px; overflow-y:scroll;  ">
	<%-- <div id="container" class="search-result news-list content-box"
			style="height:100%;<c:if test="${paramMap.allocationStatus == 'F'}"> margin-top:30%;</c:if><c:if test="${paramMap.allocationStatus != 'F'}"> margin-top:20%;</c:if> overflow-y:scroll;">
	 --%>
	 
	 
	 
    <%--      <div class="input-append date" style="width:100%;">
						<div style="display:inline; width:15%;">&nbsp;&nbsp;인수증&nbsp;검색</div>
						<div style="display:inline; width:40%;">:&nbsp;&nbsp;<input style="width:45%; text-align:center;"  type="text" class="span2" id="datepick" value="${paramMap.searchType}"><span class="add-on"><i class="icon-th"></i></span></div>
						<div class="menu-container" style="display:inline; width:40%; text-align:right; clear:both;  padding:0;">
							
							<a class="kakao txt-bold" style="width:19%; display:inline; text-align:right; line-height:32px; cursor:pointer; margin-top:0px; margin-bottom:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:selectTypeChange(this);">
			                 <img style=" width:6%; height:6%;" src="/img/magnifier.png" />
			                 	 조회
			            	</a>
			            	<input id="selectType" type="hidden" value="${paramMap.searchType}">
						</div>
					</div> --%> 
      	 		<%-- <div class="input-append date" style="width:100%;">
						<div style="display:inline; width:15%;">&nbsp;&nbsp;&nbsp;리뷰&nbsp;조회</div>
						<div style="display:inline; width:40%;">:&nbsp;&nbsp;<input style="width:45%; text-align:center;"  type="text" class="span2" id="datepick" value="${paramMap.searchDate}"><span class="add-on"><i class="icon-th"></i></span></div>
						<div class="menu-container" style="display:inline; width:40%; text-align:right; clear:both;  padding:0;">
							
							<a class="kakao txt-bold" style="width:19%; display:inline; text-align:right; line-height:32px; cursor:pointer; margin-top:0px; margin-bottom:0px; padding-left:0px; text-align:center; background-image:none;" >
			                 <img style=" width:6%; height:6%;" src="/img/magnifier.png" />
			                 	 조회
			                 	 
			            	</a>
			            	<input id="selectType" type="hidden" value="${paramMap.searcshType}">
						</div>
					</div> --%> 
					
						<div class="menu-container"
							style="display: inline; width: 20%; text-align: center; clear: both; ">
							<!-- <img style="width: 8%; height: 6%;" src="/img/magnifier.png" /> -->
							<%-- <a class="kakao txt-bold"
								style="width: 70%; display: inline-block; text-align: left; line-height: 32px; cursor: pointer; margin-top: 0px; margin-bottom: 0px; padding-left: 0px; text-align: center; background-image: none;"
								onclick="javascript:goWriteReview('${listData[0].customer_id}')"> 내가 쓴 리뷰만 보기 Click! </a>  --%>
						</div>
					<div>
					
						<div style="margin-top:0%; width:100%; text-align:center;">
							<span class="d-tbc" style="font-weight: bold; ">한국카캐리어(주) 공지사항</span>
						</div>
						
					</div>
					
			<!-- <div class="cat-list" style="background-color:#fff">
	                <ul class="clearfix" style="background-color:#fff">
	                	<li><a style="color:#000; width:15%;">출발일</a></li>
	                    <li><a style="color:#000;">상차지</a></li>
	                    <li><a style="color:#000;">하차지</a></li>
	                    <li><a style="color:#000;">차종</a></li>
	                    <li><a style="color:#000;">차대번호</a></li>
	                </ul>
	            </div> -->
			<!--  <a class="kakao txt-bold" style="width:19%; display:inline-block; text-align:right; line-height:32px; cursor:pointer; margin-top:0px; margin-bottom:0px; padding-left:0px; text-align:center; background-image:none;" onclick="javascript:goSearchYearType($('#forYear').val());">
			                 		 조회 
			            	</a> -->

			<div style="width: 100%; margin-top: 5%; height: 40px; border-bottom: 1px solid #eee;">
				<div style="width: 31%; text-align: center; height: 40px; float: left;">
					<div
						style="margin-top: 10px; text-align: center; display: inline-block;">등록일</div>
				</div>
			
				
				<div style="width: 32%; text-align: center; height: 40px; float: left;">
					<div style="margin-top: 10px; text-align: center; display: inline-block;">공지 내용</div>
				</div>
					<div style="width: 31%; text-align: center; height: 40px; float: left;">
						<div style="margin-top: 10px; text-align: center; display: inline-block;">읽기</div>
					</div>
			</div>
		</div>


		<div id="container" class="search-result news-list content-box"
			style="height:100%; margin-top:0%; overflow-y:scroll;">

			<div class="xpull">
				<div class="xpull__start-msg">
					<div class="xpull__start-msg-text">화면을 당겼다 놓으면 새로 고침 됩니다.</div>
					<div class="xpull__arrow"></div>
				</div>
				<div class="xpull__spinner">
					<div class="xpull__spinner-circle"></div>
				</div>
			</div>
			<div class="news-container clearfix" style="height: 100%;">
				<c:forEach var="data" items="${listData}" varStatus="status">

					<div
						style="width: 100%; margin-top: 5px; height: 40px; border-bottom: 1px solid #eee;">
						<div
							style="width: 31%; text-align: center; height: 40px; float: left;">
							<div
								style="margin-top: 10px; text-align: center; display: inline-block;">${data.regDt}</div>
						</div>
						<div
							style="width: 32%; text-align: center; height: 40px; float: left;">
							<div
								onclick="javascript:viewReview('${data.allocation_id}');"
								style="width: 100%; margin-top: 10px; text-align: center; display: inline-block; text-overflow: ellipsis; white-space: nowrap; overflow: hidden;">
								<nobr>${data.points_text}</nobr>
							</div>
						</div>
						<div
							style="width: 31%; text-align: center; height: 40px; float: left;">
							<div
								onclick="javascript:viewReview('${data.allocation_id}');"
								style="width: 100%; margin-top: 10px; text-align: center; display: inline-block; text-overflow: ellipsis; white-space: nowrap; overflow: hidden;">
								<nobr><span style="color:#FF4000;">${data.points}</span>점</nobr>
								
								
								
							</div>
						</div>
				

			
					</div>

					<%--   <c:if test="${data.groupCarCnt > 1}">
			           		<div style="width:100%; margin-top:5px; height:40px; border-bottom:1px solid #eee;"  onclick="javascript:viewAllocationList('${data.allocation_id}','${data.allocation_status_cd}');">
				                <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">${data.departure_dt}</div></div>
				                <div style="width:20%; text-align:center; height:40px; float:left;"><div style="width:100%; margin-top:10px; text-align:center; display:inline-block; text-overflow:ellipsis; white-space:nowrap; overflow:hidden;"><nobr>${data.departure}</nobr></div></div>
					            <div style="width:20%; text-align:center; height:40px; float:left;"><div style="width:100%; margin-top:10px; text-align:center; display:inline-block; text-overflow:ellipsis; white-space:nowrap; overflow:hidden;"><nobr>${data.arrival}</nobr></div></div>
					            <div style="width:80%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">${data.driver_cnt}회전</div></div>
			                </div>
			             </c:if> --%>
				</c:forEach>


			</div>
		</div>
	</div>


	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script>
		window.jQuery
				|| document
						.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')
	</script>
	<script src="/js/vendor/bootstrap.min.js"></script>
	<!-- veiwport for countnumber -->
	<script src="/js/alert.js"></script>
	<script src="/js/main.js"></script>
	<script src="/js/vendor/xpull.js"></script>
	<script src="/js/vendor/jquery.loading-indicator.min.js"></script>
    <script src="/js/vendor/bootstrap-datepicker.js"></script>
	<script>
		$('#container').xpull({
			'paused' : false,
			// Pull threshold - amount in  pixels required to pull to enable release callback
			'pullThreshold' : 50,
			// Max pull down element - amount in pixels
			maxPullThreshold : 50,
			// timeout in miliseconds after which the loading indicator stops spinning.
			// If set to 0 - the loading will be indefinite
			'spinnerTimeout' : 1000,
			onPullStart : function() {
			},
			onPullEnd : function() {
			},
			callback : function() {
				window.location.reload()
			},

		});

		var homeLoader;

		$(document).ready(function() {

			homeLoader = $('body').loadingIndicator({
				useImage : false,
				showOnInit : false
			}).data("loadingIndicator");

			/* setTimeout(function() {
				homeLoader.show();
				homeLoader.hide();
			}, 1000); */

		});

		
		$('#datepick').datepicker({
			format: "yyyy-mm-dd",
		    clearBtn: true,
		    autoclose: true,
		    language: "kr",
		    todayHighlight: true
		}).on('changeDate', function (selected) {
		   var selectedDate = selected.format(0,"yyyy-mm-dd");
		   //alert($("#selectType").val());
		   document.location.href = "reservation-ConsignmentList.do?&regType=A"+"&searchDate="+$("#datepick").val();
		   
		});
	</script>

</body>
</html>
