<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko" style=" height:100%;">      
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="/css/jquery.bxslider.css"/>
<body style="background-color:#fff; height:100%;" >
<script type="text/javascript">  
  var pageMoveStop = true;
$(document).ready(function(){  
	
	var updateToken = setInterval( function() {
			clearInterval(updateToken);
			pageMoveStop = false;
			homeLoader.hide();
			 fncClearTime();
   			initTimer();


	
   }, 300);
	

 var iSecond; //초단위로 환산
 var timerchecker = null



 function fncClearTime() {
	    iSecond = 7200;
	}
	 
	Lpad = function(str, len) {
	    str = str + "";
	    while (str.length < len) {
	        str = "0" + str;
	    }
	    return str;
	}
	 
	initTimer = function() {
	    var timer = document.getElementById("logoutTimer");
	    rHour = parseInt(iSecond / 3600);
	    rHour = rHour % 60;
	 
	    rMinute = parseInt(iSecond / 60);
	    rMinute = rMinute % 60;
	    rSecond = iSecond % 60;
	 
	    if (iSecond > 0) {
	        timer.innerHTML = "&nbsp;" + Lpad(rHour, 2) + "시간 " + Lpad(rMinute, 2)
	                + "분 " + Lpad(rSecond, 2) + "초 ";
	        iSecond--;
	        timerchecker = setTimeout("initTimer()", 1000); // 1초 간격으로 체크
	    } else {
	    	window.location.href = "/logout.do";
	    }
	}
	 
/* 	function refreshTimer() {
	    var xhr = initAjax();
	    xhr.open("POST", "/jsp_std/kor/util/window_reload2.jsp", false);
	    xhr.send();
	    fncClearTime();
	}
	 */
/*
	 function logoutUser() {
	    clearTimeout(timerchecker);
	    var xhr = initAjax();
	    xhr.open("POST", "/mail/user.public.do?method=logout", false);
	    xhr.send();
	    location.reload();
	}
*/



	 /*
	function initAjax() { // 브라우저에 따른 AjaxObject 인스턴스 분기 처리
	    var xmlhttp;
	    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
	        xmlhttp = new XMLHttpRequest();
	    } else {// code for IE6, IE5
	        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    return xmlhttp;
	}*/
 




	
	/* var updateLocation = setInterval( function() {
	
		if(getDeviceLocation() != null && getDeviceLocation() != ""){
			updateDriverDeviceLocation(getDeviceLocation());
		}
		
    }, 10*1000); */			//1분에 한번
	
  	//alert(deviceToken);

/* 	$('.slick-items').bxSlider({

	  	auto: true,
	  	speed: 1000,			    
	    slideWidth: 900

	    
	
	});
 */

	$('.menu_2' ).hide();

	$('.slick-items').bxSlider({
		   mode:'fade',         // 사라지는 모양
		   speed:500,           // 이미지변환 속도 기본 500
		   randomStart:false,   // 이미지 랜덤으로 처리
		   controls:true,      // 좌,우 컨트롤 버튼 숨기기/보이기
		   autoControls:false,  //  슬라이드 시작/멈춤
		   pager:false,         // 하단 이미지 보기 버튼
		   auto:true,           // 자동시작
		  
		  });



//$(".bx-viewport").css("height","340px")


	  
});  
  
/*   
function updateDriverDeviceToken(token){
	

	if(token != ""){
		$.ajax({ 
			type: 'post' ,
			url : "/updateDriverDeviceToken.do" ,
			dataType : 'json' ,
			data : {
				token : token
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					//$.alert("성공",function(a){
					//});
				}else if(result == "E000"){
					
				}else{
					
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
}  
   */
  
  
function getDeviceToken(){
	
	var token = "";
	try{
		if(window.Android != null){
			token = window.Android.getDeviceToken("${driverId}");
		}	
	}catch(exception){
		
	}finally{
		
	}
	return token;
}  
  
  
function updateDriverDeviceLocation(location){
	
	if(location != ""){
		$.ajax({ 
			type: 'post' ,
			url : "/updateDriverDeviceLocation.do" ,
			dataType : 'json' ,
			data : {
				location : location
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					//$.alert("성공",function(a){
					//});
				}else if(result == "E000"){
					
				}else{
					
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});	
	}
	
	
}  
  
 function getDeviceLocation(){
	 
	 var latlng = "";
		try{
			if(window.Android != null){
				latlng = window.Android.getDeviceLocation("${kakaoId}");
				//alert(latlng);
			}	
		}catch(exception){
			
		}finally{
			
		}
		return latlng; 
	 
 }
  
  
function getAndroidData(str){
	
//	alert(str);
	
	//updateDriverDeviceLocation(str);
	
} 
 

<%

String test = "Y";

if(session.getAttribute("user") == null){
	
	test = "N";
	
}

%>

	var userLoginType = "<%=test%>";

	
function backKeyController(str){


	var allocationId = "${paramMap.allocationId}";
		
	if(allocationId != ""){

		document.location.href ="/carrier/car-id-num-list.do?&carIdNum=${listData.car_id_num}&customerId=${paramMap.customerId}";

		}else{
			

	 if(window.Android != null){
		window.Android.toastShort("뒤로가기 버튼으로 앱을 종료 할 수 없습니다.");
	}
			}
	
	 
	
}
 
 
function goList(allocationStatus,customerId,allocationId){
	
	//alert("승인 대기 목록");
	if(!pageMoveStop){
		homeLoader.show();
		document.location.href = "/carrier/list.do?allocationStatus="+allocationStatus+"&customerId=${paramMap.customerId}"+"&allocationId="+allocationId;	

		var updateHomeLoader = setInterval(function() {
			clearInterval(updateHomeLoader);
			homeLoader.hide();
		}, 300);
	}
	
	
}

function goBPIDcntList(BPIDcnt){
	
	//alert("탁송 완료 목록");
	if(!pageMoveStop){
		homeLoader.show();
		document.location.href = "/carrier/list.do?BPIDcnt="+BPIDcnt;

		var updateHomeLoader = setInterval(function() {
			clearInterval(updateHomeLoader);
			homeLoader.hide();
		}, 300);	
	}
	
}


function goFList(allocationStatus){
	
	//alert("탁송 완료 목록");
	if(!pageMoveStop){
		homeLoader.show();
		document.location.href = "/carrier/finish-list.do?allocationStatus="+allocationStatus;	


		var updateHomeLoader = setInterval(function() {
			clearInterval(updateHomeLoader);
			homeLoader.hide();
		}, 300);
		
	}
	
}


function goLowViolationPage(){

	if(!pageMoveStop){
		homeLoader.show();
		document.location.href = "/carrier/lowViolationList.do";


		var updateHomeLoader = setInterval(function() {
			clearInterval(updateHomeLoader);
			homeLoader.hide();
		}, 300);
			
	}
	
}




function goReload(){
	
	document.location.reload();
	/* $.alert("준비중 입니다.",function(a){
	}); */
	
}

function goCalPage(){
	
	
	if("${user.driver_kind}" != "00" || "${user.driver_id}" == "sourcream"){
		if(!pageMoveStop){
			homeLoader.show();
			document.location.href = "/carrier/cal-list.do";	

			var updateHomeLoader = setInterval(function() {
				clearInterval(updateHomeLoader);
				homeLoader.hide();
			}, 300);
			
		}	
	}else{
		$.alert("준비중 입니다.",function(a){
		});
	}
	
	
}

    
function logout(){
	
	$.confirm("로그아웃 하시겠습니까?",function(a){
		 if(a){
			 homeLoader.show();
			 if(window.Android != null){
					window.Android.stopService("${driverId}");
				}	
			 		document.location.href = "/logout.do";	
		 }
	});
	
}    
    
    
    
function downloadDriverDeduct(){
	//alert(1);
	if(!pageMoveStop){
		//document.location.href = "/carrier/downloadDriverDeductInfo.do?driverId=${user.driver_id}";
		window.Android.openTmap("${driverId}");
	//	document.location.href = "https://apis.openapi.sk.com/tmap/app/execution?&appKey=l7xx6b701ff5595c4e3b859e42f57de0c975";
	}
}    

function viewDriverContact(){


	document.location.href = "/carrier/contact-main.do";

	
}

function goMainPage(){

homeLoader.show();	
document.location.href ="/carrier/main.do";

var updateHomeLoader = setInterval(function() {
	clearInterval(updateHomeLoader);
	homeLoader.hide();
}, 300);

}

function goMainPage2(){

	homeLoader.show();	
	//document.location.href ="/carrier/main.do?&allocationId=${paramMap.allocationId}";
	document.location.href ="/carrier/three-option.do";


	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);

	
}

function  goListForAllocationId(allocationStatus,allocationId){

	if(!pageMoveStop){
		homeLoader.show();
		document.location.href = "/carrier/list.do?allocationStatus="+allocationStatus+"&allocationId=${paramMap.allocationId}";	

		var updateHomeLoader = setInterval(function() {
			clearInterval(updateHomeLoader);
			homeLoader.hide();
		}, 300);
		

		
	}
	
}

function goReciptBox(customerId){

	if(!pageMoveStop){
		homeLoader.show();	
		document.location.href ="/carrier/cal-item.do?&customerId=${paramMap.customerId}";

		var updateHomeLoader = setInterval(function() {
			clearInterval(updateHomeLoader);
			homeLoader.hide();
		}, 300);

	}
}

function goKakaoTalkChat(){



	//$.alert("준비중입니다.");
	
	$.confirm("카카오톡 채널로 이동 하시겠습니까?",function(a){
			 if(a){
				if(window.Android != null){
					window.Android.openKakaoChannel();
				}else if(webkit.messageHandlers != null){
					webkit.messageHandlers.scriptHandler.postMessage("goKakaoChannel");

				}else{
					$.alert("알수 없는 에러입니다. 관리자에게 문의해주세요");					
				}	
			 }
		});  
		

}


function goSignUpPage(){


	document.location.href ="/carrier/login-page.do?&optionType=L";
}


function goCarNumList(){

	homeLoader.show();
	
	document.location.href ="/carrier/login-page.do?optionType=C";


	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);

	
}


function goConsignmentReservation(){

	homeLoader.show();

	document.location.href="/carrier/consignment-Reservation.do";

	var updateHomeLoader = setInterval(function() {
		clearInterval(updateHomeLoader);
		homeLoader.hide();
	}, 300);

	
}

function viewReview(allocationId){
	
	homeLoader.show();
	document.location.href ="/carrier/review-view.do?&allocationId="+allocationId;

}


function ShowSubMenu( strId ){
	var lySubMenu = $( strId );

	if( lySubMenu.first().is( ":hidden" ) ){
		$( strId ).slideDown( 300 );
	}
	else{
		$( strId ).slideUp( 300 );
	}
}

function goMenyConsignmentReservation(){


    	$.MessageBox({
				  input    : true,
				  buttonsOrder: "fail done",
 			  buttonFail: "취소", 
				  buttonDone: "확인",
				  message  : "차량 몇대를 예약 하시겠습니까? (숫자로만 입력해주세요)"
				}).done(function(data){
				  if ($.trim(data)) {
					var str = data;
					var check = /[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/;
					if(check.test(str)){
						$.alert("입력창에 한글이 포함되어 있습니다. 정확한 대수를 입력 하세요.",function(d){
						});
						return false;
					}	
					var pattern_spc = /[~!@#$%^&*()_+|<>?:{}]/; // 특수문자
					if(pattern_spc.test(str)){
						$.alert("입력창에 특수문자가 포함되어 있습니다. 정확한 대수를 입력 하세요.",function(d){
						});
						return false;
					}
					
				//	var regType1 = /^[A-Za-z0-9+]{6,12}$/; 
					//if(regType1.test(str)){
					if(Number(str) > 9){
						$.alert("10대 이상의 예약건은 1:1 문의로 예약 주세요.");
						
					}else{

		   				document.location.href = "/carrier/consignment-Reservation-many.do?&carCount="+str+"&nowCount=1";
		   								
		   			
					}
			
					
				  } else {
					
				  }
				});


		  


			
}



function goInsertReservation(){

	document.location.href="/carrier/car-reservation.do";

}


				



</script>


        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div class="content-container" style="height:100%; /* overflow-y:hidden; */" >
            <!-- <header class="bg-pink clearfix" style="background-color:#fff; border:none; text-align:center; "> -->
            <header class="bg-pink clearfix" style="position:fixed; top:0px; background-color:#fff; border:none; text-align:center; z-index:10000;">
               <c:if test='${paramMap.LoginType eq "customerIdLogin"}'>
               
               		<c:if test ='${paramMap.allocationId ne ""}'>
               			<div class="" style="width: 19%;"> 
               			 		<a onclick="javasript:homeLoader.show();document.location.href='/carrier/car-id-num-list.do?&carIdNum=${listData.car_id_num}&customerId=${paramMap.customerId}'">
									<img style="zoom: 0.5;" src="/img/back-icon.png" alt="">
								</a>
							</div>
               			</c:if>
               <c:if test ='${paramMap.allocationId eq ""}'>
                <div class="" style="width:19%;" onclick ="javascript:document.location.href='/carrier/sub-main.do' ">
            	   <img style="width:50%; height:60%;"  src="/img/hamburger_menu.png" alt="">
                </div> 
                </c:if>
                
                
                </c:if>
                <c:if test='${paramMap.LoginType eq "carIdNumLogin"}'>
                   <a onclick="javascript:history.go(-1);"><img style="zoom: 0.5;" src="/img/back-icon.png" alt=""></a> 
                   </c:if>
                   
                	<c:if test='${paramMap.LoginType eq "customerIdLogin"}'>
                <div class="" style="width:60%;">
                	<img style="width:90%; height:60%;" onclick="javascript:goMainPage()" src="/img/main_logo.png" alt="">
                	 </div>
                	<div class="" style="width:19%; float:right;">
                   <a style="cursor:pointer;" href="javascript:logout();"><img style="" src="/img/logout.png" alt=""></a>
                </div>
                	 </c:if>
               <c:if test='${paramMap.LoginType eq "carIdNumLogin"}'>
               	<div class="" style="width:60%;">
                	<img style="width:90%; height:60%; margin-left:20%;" onclick="javascript:goMainPage2()" src="/img/main_logo.png" alt=""> 
                 	</div>
                 	   <div class="" style="width:19%; float:right;">
                   <!-- <a style="cursor:pointer;" href="javascript:logout();"><img style="" src="/img/logout.png" alt=""></a> -->
                </div> 
                
                 </c:if>
                
               
                
            </header>
            <div style="clear:both;"></div>
           
            <!-- <div class="menu-container bg-pink">
                <span class="img-holder">
                    <img src="/img/user-img.png" alt="">
                </span>
                 <a class="kakao txt-bold" style="cursor:pointer;" onclick="javascript:kakaoLogin();">
                    카카오계정으로 로그인
                </a>
            </div> -->

            <div style=" text-align:right; clear:both;">&nbsp;</div>
            <div class="main-menu-container" style="margin-top:5%; height:100%; ">
            
           <br>
           
           		<c:if test='${paramMap.LoginType eq "customerIdLogin"}'>
                     <div style=" text-align:left; clear:both; color: #585858;"> <strong><span class="text" style="color: #2ECCFA;">&nbsp;${customer.customer_name}</span>&nbsp;${listData.departure_dt}&nbsp;</strong>탁송건 입니다.</div>
                   </c:if>
                   
				<div style=" text-align:left; clear:both; color: #585858;"> <strong><span id="logoutTimer" class="text" style="color: #FA8258;"> <a href="javascript:refreshTimer();"></a>&nbsp;</span></strong>후 자동 로그아웃</div>
				    
				    <c:if test='${paramMap.LoginType eq "carIdNumLogin"}'>
                     <div style=" text-align:left; clear:both; color: #585858;">&nbsp;&nbsp;탁송위치에 따라 <span class="text" style="color: #2ECCFA;">파란색</span>으로 표시됩니다.</div>
                     </c:if>

				<c:if test='${paramMap.LoginType eq "customerIdLogin"}'>
            		<div style=" text-align:right; clear:both;">
            				<img class="blinking" src="/img/fireworker.jpg" style ="margin-top:0%;  float:right; width:8%; height:6%">
            			<strong>${customer.name}</strong>님 환영 합니다.
            		</div>
            	</c:if>
            
            <c:if test='${paramMap.LoginType eq "carIdNumLogin"}'>
            	<div style=" text-align:right; clear:both;">
            		<img class="blinking" src="/img/fireworker.jpg" style ="margin-top:0%;  float:right; width:8%; height:6%">
            		방문객님 환영 합니다.
            	</div>
            </c:if>
            	
        
                     
            	<div  id="container"  style="height:100%; margin-top:2%; background-color:#fff;">
            		<div class="xpull">
					    <div class="xpull__start-msg">
					        <div class="xpull__start-msg-text">화면을 당겼다 놓으면 새로 고침 됩니다.</div>
					        <div class="xpull__arrow"></div>
					    </div>
					    <div class="xpull__spinner">
					        <div class="xpull__spinner-circle"></div>
					    </div>
					</div>
					
					<div style="margin-top:0%; height:40%; ">
					
					<!-- <div><img style=" width:100%; height:100%;" src="/img/mainimg.png" /></div> -->
					
					
						<c:if test='${paramMap.LoginType eq "customerIdLogin"}'>
				
							<br><br>
						<c:if test ='${paramMap.allocationId eq ""}'>
						
							<%-- <div style="margin-top:-5%; width:100%; text-align:center;">
								<span class="d-tbc" style="font-weight: bold; color:#FF8000; "><span class="blinking">🎁</span>소중한 고객님들의 실시간 리뷰 후기<span class="blinking">🎁</span></span>
							</div>
						
							<div style="width: 100%; margin-top: 5%; height: 40px; border-bottsom: 1px solid #eee;">
								<div style="width: 31%; text-align: center; height: 40px; float: left;">
									<div
										style="margin-top: 10px; text-align: center; display: inline-block;">🎉<span class="d-tbc" style="font-weight: bold; color:#DF0101; ">등록일</span></div>
								</div>
							
								<div style="width: 32%; text-align: center; height: 40px; float: left;">
									<div style="margin-top: 10px; text-align: center; display: inline-block;">✨<span class="d-tbc" style="font-weight: bold; color:#088A08; ">리뷰 내용</span></div>
								</div>
									<div style="width: 31%; text-align: center; height: 40px; float: left;">
										<div style="margin-top: 10px; text-align: center; display: inline-block;"><span class="d-tbc" style="font-weight: bold; color:#8904B1; ">👀평점(5점 만점)</span></div>
									</div>
							</div>
									
					<c:forEach var="data" items="${reviewList}" varStatus="status">
		
							<div	style="width: 100%; margin-top: 5px; height: 40px; border-bottom: 1px solid #eee;">
								<div
									style="width: 31%; text-align: center; height: 40px; float: left;">
									<div
										style="margin-top: 10px; text-align: center; display: inline-block;">${data.regDt}</div>
								</div>
								<div
									style="width: 32%; text-align: center; height: 40px; float: left;">
									<div
										onclick="javascript:viewReview('${data.allocation_id}');"
										style="width: 100%; margin-top: 10px; text-align: center; display: inline-block; text-overflow: ellipsis; white-space: nowrap; overflow: hidden;">
										<nobr>${data.points_text}</nobr>
									</div>
								</div>
								<div
									style="width: 31%; text-align: center; height: 40px; float: left;">
									<div
										onclick="javascript:viewReview('${data.allocation_id}');"
										style="width: 100%; margin-top: 10px; text-align: center; display: inline-block; text-overflow: ellipsis; white-space: nowrap; overflow: hidden;">
										<nobr><span style="color:#FF4000;">${data.points}</span>점</nobr>
										
									</div>
								</div>
						
							</div>
		
						</c:forEach>
						 --%>
						
						
					<div class="slick-items" >
						     	 <div><img style=" width:100%; height:20%;" src="/img/1213.jpg" /><strong class="afterShow"  style="color: #585858;  margin-left: 5%; display:none;" id='titleText' >실제 한국카캐리어(주) 탁송 사진 입니다.</strong></div>   
							   	 <div><img style=" width:100%; height:20%;" src="/img/hkcar1.png" /><strong class="afterShow"  style="color: #585858;  margin-left: 5%; display:none;" id='titleText' >실제 한국카캐리어(주) 탁송 사진 입니다.</strong></div>   
							   	 <div><img style=" width:100%; height:20%;" src="/img/hkcar2.jpg" /><strong class="afterShow"  style="color: #585858;  margin-left: 5%; display:none;" id='titleText' >실제 한국카캐리어(주) 탁송 사진 입니다.</strong></div>   
							   	 <div><img style=" width:100%; height:20%;" src="/img/hkcar3.jpg" /><strong class="afterShow"  style="color: #585858;  margin-left: 5%; display:none;" id='titleText' >실제 한국카캐리어(주) 탁송 사진 입니다.</strong></div>   
							   	 <div><img style=" width:100%; height:20%;" src="/img/hkcar4.jpg" /><strong class="afterShow"  style="color: #585858;  margin-left: 5%; display:none;" id='titleText' >보이는 이미지는 실제 한국카캐리어(주) 탁송 사진 입니다.</strong></div>   
							   	 <div><img style=" width:100%; height:20%;" src="/img/hkcar5.jpg" /><strong class="afterShow"  style="color: #585858;  margin-left: 5%; display:none;" id='titleText' >보이는 이미지는 실제 한국카캐리어(주) 탁송 사진 입니다.</strong></div>   
							   	 <div><img style=" width:100%; height:20%;" src="/img/car1.jpg" /><strong class="afterShow"  style="color: #585858;  margin-left: 5%; display:none;" id='titleText' >실제 한국카캐리어(주) 탁송 사진 입니다.</strong></div>   
							   	 <div><img style=" width:100%; height:20%;" src="/img/car2.jpg" /><strong class="afterShow"  style="color: #585858;  margin-left: 5%; display:none;" id='titleText' >실제 한국카캐리어(주) 탁송 사진 입니다.</strong></div>   
							   	 <div><img style=" width:100%; height:20%;" src="/img/hkcar6.jpg" /><strong class="afterShow"  style="color: #585858;  margin-left: 5%; display:none;" id='titleText' >보이는 이미지는 실제 한국카캐리어(주) 탁송 사진 입니다.</strong></div>   
							   	 <div><img style=" width:100%; height:20%;" src="/img/hkcar7.jpg" /><strong class="afterShow"  style="color: #585858;  margin-left: 5%; display:none;" id='titleText' >보이는 이미지는 실제 한국카캐리어(주) 사진 입니다.</strong></div>   
							   	 <div><img style=" width:100%; height:20%;" src="/img/car8.jpg" /><strong class="afterShow"  style="color: #585858;  margin-left: 5%; display:none;" id='titleText' >실제 한국카캐리어(주) 탁송 사진 입니다.</strong></div>   
							     <div><img style=" width:100%; height:100%;" src="/img/kakaotalkask.jpg" /></div>
						  
  							</div> 
  						</c:if>
  						
  						
  						<c:if test ='${paramMap.allocationId ne ""}'>
  						    <%--  	<div class="" style="width:25%; margin-top:-5%;" onclick="javascript:document.location.href='/carrier/car-id-num-list.do?&carIdNum=${listData.car_id_num}&customerId=${paramMap.customerId}'">
                   		 			<img style="zoom: 0.5;" src="/img/back-icon.png" alt="">&nbsp;뒤로가기
               			 		</div> 
               			 		 --%>
               			 		
               			 		<span class="d-tbc" style="font-weight: bold; margin-left :20%; ">차대번호<strong>&nbsp;${listData.car_id_num}&nbsp;</strong>에 대한 탁송 건입니다.</span> 
               			 		
						  		 <div class="certNum" style="margin-top:5%;   margin-left :20%;">
									<span class="d-tbc" style="font-weight: bold;">상차지</span> 
										<br>  
								
								         <input type="text" style="width: 60%; padding:2%; margin-top: 2%;  border: 2px solid #FF8000;" placeholder="상차지 " class="d-tbc" id="departure" name="departure" value="${listData.departure}" readonly>
								        </div>
								            
								  <div class="showID" style="margin-top: 5%;   margin-left :20%;">
									<span class="d-tbc" style="font-weight: bold;">하차지</span> 
														<br>  
								         
								         <input type="text" style="width: 60%; padding:2%; margin-top: 2%;  border: 2px solid #FF8000;" placeholder="하차지 " class="d-tbc" id="arrival" name="arrival" value="${listData.arrival}" readonly>
						 			
						 			</div>
						 			
						 	 <div class="showID" style="margin-top: 5%;   margin-left :20%;">
									<span class="d-tbc" style="font-weight: bold;">차종</span> 
														<br>  
											    
								         <input type="text" style="width: 60%; padding:2%; margin-top: 2%;  border: 2px solid #FF8000;" placeholder="차종 " class="d-tbc" id="carKind" name="carKind" value="${listData.car_kind}" readonly>
								            
						 			</div>
						 </c:if>
  						
			 		</c:if>
					
					<%-- 	<c:if test='${paramMap.LoginType eq "carIdNumLogin"}'>
							<br><br>
							<div class="slick-items">
								<div><img style=" width:100%; height:20%;" src="/img/kakaotalkask.jpg" /></div>
								<div><img style=" width:100%; height:20%;" src="/img/kakaoLogoRed.png" /></div>
							</div>
					</c:if> --%>
					
					</div>
					
     <!--  <strong  style="color: #2ECCFA;" id='titleText'> 님의 탁송상태별 내역입니다.</strong> -->
     
		      			  <div style="clear:both; margin-top:10%; width:20%; height:5%; text-align:center; " onclick="javascript: homeLoader.show(); document.location.href ='/carrier/kakaoRouteView.do?&call=main' ">
			               <!--   	<img src="/img/wonicon.png" alt="" style ="margin-top:-2%; width:60%; height: 50px; ">
			                 		<br> -->
				                 	 <div id="btn_group" style="margin-left:0%; margin-top:10%; text-align:center; display:inline; "> 
					                 	<strong><button id="test_btn2"  style="margin-left:1%; "> 탁송 금액 조회</button></strong>
				                 	</div> 
				                 	
			                  </div> 
			                  
			             <div style=" margin-top:-10%; width:20%; height:5%; margin-left:20%;" onclick="javascript:goKakaoTalkChat()"> 
	                 		
		                 	 <div id="btn_group" style="margin-left:0%; margin-top:5%; text-align:center; color: #FF4000; border: 3px solid;"> 
			                 	<strong><button id="test_btn2"  style="margin-left:1%; color: #FF4000;">어플 관련 문의</button></strong>
		                 	</div> 
	                  </div>
		      			 
			                 
			                 
			                 
			 <!--         <div style=" margin-top:-10%; width:20%; height:5%; margin-left:20%;" onclick="javascript: homeLoader.show(); document.location.href ='/carrier/review-list.do?&reviewTotalList=Y' "> 
	                 		<img src="/img/main_icon_m04.jpg" alt="" style ="margin-left:15%; margin-top:-2%; width:60%; height: 50px; ">
	                 		<br>
		                 	 <div id="btn_group" style="margin-left:0%; margin-top:5%; text-align:center;"> 
			                 	<strong><button id="test_btn2"  style="margin-left:1%; ">리뷰조회</button></strong>
		                 	</div> 
	                  </div>  -->
	                 
	        <%--      <c:if test ="${customer.customer_kind eq '01' }">
	                     <div style=" margin-top:-11%; width:20%; height:5%; float:right; " onclick="javascript: homeLoader.show(); document.location.href ='/carrier/myProfile-Page.do' ">
	                 		<!-- <img src="/img/main_icon0007.jpg" alt="" style ="margin-top:0%; width:100%; height: 50px; ">
	                 		<br> -->
		                 	 <div id="btn_group" style="margin-left:0%; margin-top:0%; text-align:center;"> 
			                 	<strong><button id="test_btn2"  style="margin-left:-12%; width:100%; margin-top:10%; width:80%;">마일리지</button></strong>
		                 	</div>
	            	     </div>
	          	   </c:if> --%>    
	          	   
     		<c:if test='${paramMap.LoginType eq "customerIdLogin"}'>
				<div style="margin-top:10%; /* height:60%; */">
			
	
              <a href="javascript:goList('Y','${paramMap.customerId}','${paramMap.allocationId}');" class="menu-link " style=" width:18%; display:inline-block ; /* background:#F78181; */">
                    <img src="/img/caicon3.png" style ="width:60%; height:70%;"alt="">
                    	<span class="text">대기중 <br><strong>${listData.Ycnt}</strong> 
                    </span>
                </a>
                
                <a href="javascript:goList('R','${paramMap.customerId}','${paramMap.allocationId}');" class="menu-link"style=" width:18%; display:inline-block; ;/* background:#F7BE81; */">
                  	  <img src="/img/caricon2.png" alt="" style ="width:60%; height:70%;">
                    <span class="text">상차중<br><strong>${listData.Rcnt}</strong></span>
                </a>
                
                <a href="javascript:goList('S','${paramMap.customerId}','${paramMap.allocationId}');" class="menu-link" style="width:18%; display:inline-block; /* background:#BEF781; */">
                  	  <img src="/img/caricon4.png" alt="" style ="width:60%; height:70%;">
                    <span class="text">이동중<br><strong>${listData.Scnt}</strong></span>
                </a>
                
                  <a href="javascript:goList('B','${paramMap.customerId}','${paramMap.allocationId}');" class="menu-link" style=" width:18%; display:inline-block; /* background:#A9E2F3; */">
                    	<img src="/img/caricon5.png" alt="" style ="width:60%; height:70%;">
                    <span class="text">하차중<br><strong>${listData.BPIDcnt}</strong></span>
                </a>
                  <a href="javascript:goList('F','${paramMap.customerId}','${paramMap.allocationId}');" class="menu-link" style="width:18%; display:inline-block; /* background:#BCA9F5; */">
                  	  <img src="/img/caricon6.png" alt="" style ="width:60%; height:70%;">
                    <span class="text">완료 <br><strong>${listData.Fcnt}</strong></span>
                </a>
                 
                 
                 
                 	 
                 	  <!-- </div>  -->
                 <!-- <div id="btn_group" style="margin-left:20%; margin-top:0%; width:55%; display:inline-block;"> 
                  	<img src="/img/hkicon2.png" alt="" style ="width:20%; height:50%;">
                  	<br>
                  		<strong><button id="test_btn1" onclick="javascript:goCarNumList();">차량번호로 조회</button></strong>
                   </div>
                   
                       <div id="btn_group" style="margin-left:20%; margin-top:-20%; float:right; display:inline-block;"> 
			                 <div style ="display:inline-block; margin-top:0%; width:55%; height:30%; margin-right:0%;" onclick="javascript:goKakaoTalkChat()">
			                    <img src="/img/kakaotalkIcon.png" alt="" style ="margin-top:0%;  text-align:right; width:100%;">
			                     <button id="test_btn3" class="blinking">1:1문의</button>
                    	</div>
                    </div> -->
                </div> 
                
                <div style="clear:both;">
                    <div style="margin-top:3%; width:20%; height:40%; float:left;" onclick="javascript:goCarNumList();">
	                 	<img src="/img/hkicon2.png" id="imgIcon" alt="" style ="margin-top:-2%; width:100%; height: 66px; ">
	                 		<br>
		                 	 <div id="btn_group" style="margin-left:0%; margin-top:0%; text-align:center;"> 
			                 	<strong><button id="test_btn2"  style="margin-left:1%;">차량조회</button></strong>
		                 	</div>
	                 </div>
	                 
	                 
	   <!--               <button onclick="ShowSubMenu('#sub1')">+</button> 
									<div class="menu_2" id="sub1">
										<div>메뉴 1-1</div>
										<div>메뉴 1-2</div>
									</div> 
	                  -->
	                 
	                 
	                 
	                 <!-- 이전 예약은 showSubMenu onclick 살리면  됨-->
                 	 <div style="margin-top:3%; width:20%; height:40%; float:left;" onclick="javascript:goInsertReservation();"><!-- onclick="javascript:ShowSubMenu('#sub1');" >-->
	                 		<img src="/img/heart.jpg" id="imgIcon" alt="" style ="margin-top:-2%; width:100%; height: 66px; ">
	                 		<br>
		                 	 <div id="btn_group" style="margin-left:0%; margin-top:0%; text-align:center;  height: 66px;"> 
			                 	<strong><button id="test_btn2"  style="margin-left:1%;">탁송예약</button></strong><br>
			                 		<div class="menu_2" id="sub1">
										<div>1️⃣<strong><button id="test_btn2"  style="margin-top:5%; margin-left:1%; " onclick="javascript:goConsignmentReservation();">일반예약</button></strong></div>
										<div>2️⃣<strong><button id="test_btn2"  style="margin-top:0%; margin-left:1%;" onclick="javascript:goMenyConsignmentReservation();">다수예약</button></strong></div>
									</div>
		                 	</div>
	                 </div>  
	                 						
											
                	 <div style="margin-top:3%; width:20%; height:40%; float:left;" onclick="javascript: homeLoader.show(); document.location.href ='/carrier/reservation-ConsignmentList.do?&regType=A' ">
                	 			<div style="text-align:center; height:65px;">
	                 				<img src="/img/reservationList.png" id="modIcon" alt="" style ="margin-top:-2%; width:75%; height: 65px; ">
	                 			</div>
	                 		<!-- <br> -->
		                 	 <div id="btn_group" style="margin-left:0%; margin-top:0%; text-align:center;"> 
		                 		<strong><button id="test_btn2"  style="margin-left:1%;">예약조회</button></strong>
	                 		</div>
	                 </div> 
	           
	                 <div style="margin-top:3%; width:20%; height:40%; float:left;" onclick="javascript:goReciptBox('${paramMap.customerId}')">
	                 			<div style="text-align:center; height:65px;">
	                 				<img src="/img/mailboxinsu.png" id="modIcon" alt="" style ="margin-top:-2%; width:75%; height: 65px; ">
	                 			</div>
	                 		<!-- <br> -->
		                 	 <div id="btn_group" style="margin-left:0%; margin-top:0%; text-align:center;"> 
		                 		<strong><button id="test_btn2"  style="margin-left:1%;">인수증함</button></strong>
	                 		</div>
	                 </div>
	                 
		                 <div style="margin-top:3%; width:20%; height:40%; float:left;" onclick="javascript:goKakaoTalkChat();">
		                 		<div style="text-align:center; height:65px;">
		                 			<img src="/img/kakaotalkIcon.png" alt="" style ="margin-top:-2%; width:80%; height: 65px; ">
		                 		</div>
		                 		<!-- <br> -->
			                 	 <div id="btn_group" style="margin-left:0%; margin-top:0%; text-align:center;"> 
				                 	<strong><button id="test_btn2" class="blinking" style="margin-left:1%;">1:1문의</button></strong>
			                 	</div>
		                 </div>
	                 
		               </div>
	                </c:if>
	                
	      <!--           
	      <div style="width: 100%; margin-top: 5%; height: 40px; border-bottom: 1px solid #eee;">
				<div style="width: 31%; text-align: center; height: 40px; float: left;">
					<div
						style="margin-top: 10px; text-align: center; display: inline-block;">등록일</div>
				</div>
			
				
				<div style="width: 32%; text-align: center; height: 40px; float: left;">
					<div style="margin-top: 10px; text-align: center; display: inline-block;">리뷰 내용</div>
				</div>
					<div style="width: 31%; text-align: center; height: 40px; float: left;">
						<div style="margin-top: 10px; text-align: center; display: inline-block;">평점(5점 만점)</div>
					</div>
			</div> -->
          				
	              <%-- <c:forEach var="data" items="${reviewList}" varStatus="status">
					<div
						style="width: 100%; margin-top: 5px; height: 40px; border-bottom: 1px solid #eee;">
						<div
							style="width: 31%; text-align: center; height: 40px; float: left;">
							<div
								style="margin-top: 10px; text-align: center; display: inline-block;">${data.regDt}</div>
						</div>
						<div
							style="width: 32%; text-align: center; height: 40px; float: left;">
							<div
								onclick="javascript:viewReview('${data.allocation_id}');"
								style="width: 100%; margin-top: 10px; text-align: center; display: inline-block; text-overflow: ellipsis; white-space: nowrap; overflow: hidden;">
								<nobr>${data.points_text}</nobr>
							</div>
						</div>
						<div
							style="width: 31%; text-align: center; height: 40px; float: left;">
							<div
								onclick="javascript:viewReview('${data.allocation_id}');"
								style="width: 100%; margin-top: 10px; text-align: center; display: inline-block; text-overflow: ellipsis; white-space: nowrap; overflow: hidden;">
								<nobr><span style="color:#FF4000;">${data.points}</span>점</nobr>
								
							</div>
						</div>
				

			
					</div>

			
				</c:forEach>			   --%>
				</div>
       
                   <!--  (Icon made by Freepik from www.flaticon.com) -->
    	 </div>
            
    </div>

    <script src="/js/vendor/jquery-1.11.2.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>     
     <script src="/js/vendor/bootstrap.min.js"></script>       
    <!-- veiwport for countnumber -->
    <script src="/js/jquery.viewportchecker.js"></script>  
      <script src="/js/alert.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/vendor/xpull.js"></script>
    <script src="/js/slick.min.js"></script>       
    <script src="/js/jquery.bxslider.js"></script>
    <script src="/js/vendor/jquery.loading-indicator.min.js"></script>
    <script src="/js/messagebox.js"></script>
    <script>
    
    var homeLoader;
    
    $(document).ready(function(){
    
    	homeLoader = $('body').loadingIndicator({
			useImage: false,
			showOnInit : false
		}).data("loadingIndicator");
	
    	homeLoader.show();
    	$(".afterShow").css("display","");

    });
	
	$('#container').xpull({
	    'paused': false, 
	    // Pull threshold - amount in  pixels required to pull to enable release callback
        'pullThreshold':50,
        // Max pull down element - amount in pixels
        maxPullThreshold: 50,
        // timeout in miliseconds after which the loading indicator stops spinning.
        // If set to 0 - the loading will be indefinite
        'spinnerTimeout':1000,
        onPullStart: function(){},
        onPullEnd: function(){},
        callback: function(){window.location.reload()},

	});   


	setTimeout(function(){ 
		$("#modIcon").css("height",$("#imgIcon").css("height"));
	}, 100);
	
	
	 //alert($("#imgIcon").css("height"));

	//setTimeout(function(){ alert($("#imgIcon").css("height"));}, 10);


	
    </script>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    </body>
</html>
