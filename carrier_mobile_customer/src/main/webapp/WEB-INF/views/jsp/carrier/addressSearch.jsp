<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko"  style=" height:100%;">  
  
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
<link rel="stylesheet" href="/css/animsition.min.css">
<body style=" height:100%;">


<script type="text/javascript">  

$(document).ready(function(){
	
	//getNewsData("${searchWord}");

	var updateToken = setInterval( function() {
			clearInterval(updateToken);
			pageMoveStop = false;
			homeLoader.hide();
	
   }, 300);

	$(".animsition").animsition({
	    inClass: 'flip-in-x-fr',
	    outClass: 'flip-out-x-fr',
	    inDuration: 1500,
	    outDuration: 800,
	    linkElement: '.animsition-link',
	    // e.g. linkElement: 'a:not([target="_blank"]):not([href^="#"])'
	    loading: true,
	    loadingParentElement: 'body', //animsition wrapper element
	    loadingClass: 'animsition-loading',
	    loadingInner: '', // e.g '<img src="loading.svg" />'
	    timeout: false,
	    timeoutCountdown: 5000,
	    onLoadEvent: true,
	    browser: [ 'animation-duration', '-webkit-animation-duration'],
	    // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
	    // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
	    overlay : false,
	    overlayClass : 'animsition-overlay-slide',
	    overlayParentElement : 'body',
	    transition: function(url){ window.location.href = url; }
	  });
	
	
});




 function getNewsData(obj,keyword){
	
	 
	// alert($("#selectedStatus").children().length);
	 
	//alert($("#selectedStatus").children().first().get(0));	
	//alert($(obj).parent().get(0));
	
 	 if($("#selectedStatus").children().first().get(0) == $(obj).parent().get(0)){				//전체를 클릭 한경우
					
 		$("#selectedStatus").children().each(function(index,element){
 			$(this).removeClass('selected');
 		});
 		$(obj).parent().addClass('selected');
	}else{
			
	} 
	
	
	
/* 	
	
	$.ajax({ 
		type: 'post' ,
		url : "/research/newsSearch.do?&keyword="+keyword ,
		dataType : 'json' ,
		data : {
			
		},
		success : function(data, textStatus, jqXHR)
		{
			alert(data);
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
	 */
	
	
} 

 function goNewsPage(uri){
		
		//var replaceUrlStr = replaceAll(url, "?", "^");
		//replaceUrlStr = replaceAll(replaceUrlStr, "=", "+");
		
		var replaceUrlStr = encodeURIComponent(uri)
		document.location.href = "/news/newsPage.do?newsUri="+replaceUrlStr;
	}  


    
function viewAllocationData(allocationId,allocationStatus){
	document.location.href = "/carrier/allocation-detail.do?allocationStatus="+allocationStatus+"&allocationId="+allocationId;
} 
 
function logout(){
	
	$.confirm("로그아웃 하시겠습니까?",function(a){
		 if(a){
			 if(window.Android != null){
					window.Android.stopService("${driverId}");
				}	
			 document.location.href = "/logout.do";	
		 }
	});
	
}   
 
 
function backKeyController(str){

	 self.close();

	 
} 


function goRouteView(){


	var departure =$("#departureAddress").val();
	var arrival =$("#arrivalAddress").val();
	
	if(departure == ""){

		$.alert("출발지목록에서 선택해주세요.");

	}else if(arrival == ""){

		$.alert("도착지목록에서 선택해주세요.");

	}else{
		homeLoader.show();
		document.location.href ="/carrier/viewRouteForPayment.do?&call=${call}&departure="+$("#departureAddress").val()+"&arrival="+$("#arrivalAddress").val();
		var updateHomeLoader = setInterval(function() {
			clearInterval(updateHomeLoader);
			homeLoader.hide();
		}, 300);
	}
		
}





function goDepartureInit(obj){


	var departureAddressVal =$('#departureAddress').val(); 
	var arrivalAddressVal= $('#arrivalAddress').val(); 

	inputStatus='departure'; 

	$(obj).next().val('');
	$('#searchKeyword').val('');

	
	 $('#searchResult2').html('');

		if(arrivalAddressVal == ""){
			
				$('#searchKeyword2').val('');
		
			}

 

}



function goArrivalInit(obj){


	var departureAddressVal =$('#departureAddress').val(); 
	var arrivalAddressVal= $('#arrivalAddress').val(); 
	
	inputStatus='arrival'; 
	

	$('#searchKeyword2').val('');
	$(obj).next().val('');
	
	$('#searchResult').html(''); 

		if(departureAddressVal == ""){
			
			$('#searchKeyword').val('');
	
		}

		
	}
	


</script>


        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <body onload="initTmap();" style="" >
        
       <div class="content-container" style=" height:100%;   background-color: rgba(255,255,255,0.8); ">
        <%--     <jsp:include page="/WEB-INF/views/jsp/common-top.jsp"></jsp:include> --%>
              <div class="animsition">
          		  <header class="bg-pink clearfix" style="position:fixed; z-index:500000; background-color:#fff; border:none; text-align:center; ">
          		  
                <div class="" style="width:19%;">
                	<c:if test='${call != null && call == "sub-main"}'>
                    	<a href="/carrier/sub-main.do"><img style="zoom: 0.5;" src="/img/back-icon.png" alt=""></a>
                    </c:if>
                    <c:if test='${call == null || call == ""}'>
                    	<a href="/carrier/three-option.do"><img style="zoom: 0.5;" src="/img/back-icon.png" alt=""></a>
                    </c:if>
                </div> 
                <div class="" style="width:60%;" onclick ="javascript:homeLoader.show(); document.location.href='/carrier/three-option.do'">
                	<img style="width:90%; height:60%;" src="/img/main_logo.png" alt="">
                </div>
                <div class="" style="width:19%; float:right;">
                   <!-- <a style="cursor:pointer;" href="javascript:logout();"><img style="" src="/img/logout.png" alt=""></a> -->
                </div>
            </header>
            <style>
							        /*     .map_wrap, .map_wrap * {margin:0;padding:0;font-family:'Malgun Gothic',dotum,'돋움',sans-serif;font-size:12px;}
							.map_wrap a, .map_wrap a:hover, .map_wrap a:active{color:#000;text-decoration: none;}
							.map_wrap {position:relative;width:100%;height:500px;}
							#menu_wrap {position:absolute;top:0;left:0;bottom:0;width:75%;margin:10px 0 30px 10px;padding:5px;overflow-y:auto;background:rgba(255, 255, 255, 0.9);z-index: 1; font-size:12px;border-radius: 10px;}
							.bg_white {background:#fff;}
							#menu_wrap hr {display: block; height: 1px;border: 0; border-top: 2px solid #5F5F5F;margin:3px 0;}
							#menu_wrap .option{text-align: center;}
							#menu_wrap .option p {margin:10px 0;}  
							#menu_wrap .option button {margin-left:5px;}
							#placesList li {list-style: none;}
							#placesList .item {position:relative;border-bottom:1px solid #888;overflow: hidden;cursor: pointer;min-height: 65px;}
							#placesList .item span {display: block;margin-top:4px;}
							#placesList .item h5, #placesList .item .info {text-overflow: ellipsis;overflow: hidden;white-space: nowrap;}
							#placesList .item .info{padding:10px 0 10px 55px;}
							#placesList .info .gray {color:#8a8a8a;}
							#placesList .info .jibun {padding-left:26px;background:url(https://t1.daumcdn.net/localimg/localimages/07/mapapidoc/places_jibun.png) no-repeat;}
							#placesList .info .tel {color:#009900;}
							#placesList .item .markerbg {float:left;position:absolute;width:36px; height:37px;margin:10px 0 0 10px;background:url(https://t1.daumcdn.net/localimg/localimages/07/mapapidoc/marker_number_blue.png) no-repeat;}
							#placesList .item .marker_1 {background-position: 0 -10px;}
							#placesList .item .marker_2 {background-position: 0 -56px;}
							#placesList .item .marker_3 {background-position: 0 -102px}
							#placesList .item .marker_4 {background-position: 0 -148px;}
							#placesList .item .marker_5 {background-position: 0 -194px;}
							#placesList .item .marker_6 {background-position: 0 -240px;}
							#placesList .item .marker_7 {background-position: 0 -286px;}
							#placesList .item .marker_8 {background-position: 0 -332px;}
							#placesList .item .marker_9 {background-position: 0 -378px;}
							#placesList .item .marker_10 {background-position: 0 -423px;}
							#placesList .item .marker_11 {background-position: 0 -470px;}
							#placesList .item .marker_12 {background-position: 0 -516px;}
							#placesList .item .marker_13 {background-position: 0 -562px;}
							#placesList .item .marker_14 {background-position: 0 -608px;}
							#placesList .item .marker_15 {background-position: 0 -654px;}
							#pagination {margin:10px auto;text-align: center;}
							#pagination a {display:inline-block;margin-right:10px;}
							#pagination .on {font-weight: bold; cursor: default;color:#777;} */
							
							
						 	body{
							 
								/* background-image: linear-gradient(rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.1)), url('/img/hkcarrierLogo.png'); */
						
							} 
							
            </style>
					
           
            <div style=" text-align:right; clear:both;">&nbsp;</div>
            <div class="content-container interest-page loadingthemepage news" style="position:fixed; background-color:#fff; clear:both;">
	            <!-- <div style="width:100%; margin-top:5px; height:40px; border-bottom:1px solid #eee;">
                   <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">출발일</div></div>
	               <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">상차지</div></div>
	               <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">하차지</div></div>
	               <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">차종</div></div>
	               <div style="width:20%; text-align:center; height:40px; float:left;"><div style="margin-top:10px; text-align:center; display:inline-block;">차대번호</div></div>
                </div> -->
            </div>
            <div id="container" class="search-result news-list content-box" style="height:100%; margin-top:8%; /* overflow-y:scroll; overflow-x:hidden; */ ">
            	<div class="xpull">
				    <div class="xpull__start-msg">
				        <div class="xpull__start-msg-text">화면을 당겼다 놓으면 새로 고침 됩니다.</div>
				        <div class="xpull__arrow"></div>
				    </div>
				    <div class="xpull__spinner">
				        <div class="xpull__spinner-circle"></div>
				    </div>
				</div>
	     	<!-- 	 <div class="map_wrap">
						  <div id="map" style="width:100%;height:100%;position:relative;overflow:hidden;"></div>
						    <div id="menu_wrap" class="bg_white">
						        <div class="option" style="height:8%;">
						            <div>
						                <form onsubmit="searchPlaces(); return false;">키워드 : &nbsp;
						                <input type="text" value="" id="keyword" size="13" style="padding:2%;"> 
						                    <button type="submit">검색하기</button> 
						                </form>
						            </div>
						        </div>
						        <hr>
						        <ul id="placesList"></ul>
						        <div id="pagination"></div>
						    </div>
						</div> -->
						<div style=" text-align:center; margin-top:5%;">
							<h4><strong>탁송&nbsp;금액&nbsp;조회</strong></h4>
					</div>
			<div style="text-align:center; margin-top:10%;">
			출발지 : &nbsp;<input type="text" style="width:60%; padding:1%; text-align:center;" class="text_custom" onfocus="javascript:goDepartureInit(this) "  id="searchKeyword" name="searchKeyword" value="" placeholder="출발지입력" >	
				<input id="departureAddress" type="hidden" value="" >
				<button id="btn_select">검색</button>
			</div>
				<div>
					<div style="width: 100%; float:left;">
						<div class="title" style="text-align:center; margin-top:5%;">출발지&nbsp;<strong>검색</strong> 목록</div>
						<div class="rst_wrap">
							<div class="rst mCustomScrollbar">
								<ul id="searchResult" name="searchResult" >
								</ul>
							</div>
						</div>
					</div>
					<!-- <div id="map_div" class="map_wrap" style="float:left"></div> -->
				</div>
			<div style="text-align:center; margin-top:10%;">
			도착지 : &nbsp;<input type="text" style="width:60%; padding:1%; text-align:center;" class="text_custom" onfocus="javascript:goArrivalInit(this)"  id="searchKeyword2" name="searchKeyword" value=""  placeholder="도착지입력" >	
				<input id="arrivalAddress" type="hidden" value="" >
					<button id="btn_select2">검색</button>
			</div>
				<div>
					<div style="width: 100%; float:left;">
						<div class="title" style="text-align:center; margin-top:5%;">도착지&nbsp;<strong>검색</strong> 목록</div>
						<div class="rst_wrap">
							<div class="rst mCustomScrollbar">
								<ul id="searchResult2" name="searchResult" >
								</ul>
							</div>
						</div>
					</div>
					<!-- <div id="map_div" class="map_wrap" style="float:left"></div> -->
				</div>		
				
				<!--  	<div id="" style="margin-top:20%; width:80%; margin-left:20%;">
					<div style="display:inline; width:60%;">출발지 :&nbsp;</div>
						<input id ="searchKeyword" type="text" style="padding:1%; width:100%; text-align:center" placeholder="출발지입력은 검색창에서"  value="" onfocus="javascript:this.value='';inputStatus='departure';" readonly>
						<input id ="departureAddress" type="hidden" value="" >
						 	<input type="text" style="width:60%; padding:1%; text-align:center;" class="text_custom" id="searchKeyword" name="searchKeyword" value="" placeholder="출발지/도착지입력" >	
									<button id="btn_select">검색</button>
						    <button id="depatureSave" type="submit" class="btn btn-success" style="display:inline;">저장</button> 
					</div>

						
					<div id="" style="margin-top:5%; width:80%; margin-left:20%;">
					<div style="display:inline; width:60%;">도착지 :&nbsp;</div>
						<input id="searchKeyword" type="text" style="padding:1%; width:100%; text-align:center" placeholder="도착지입력은 검색창에서"  value="" onfocus="javascript:this.value='';inputStatus='arrival';" readonly>
						<input id="arrivalAddress" type="hidden" value="" >
							<input type="text" style="width:60%; padding:1%; text-align:center;" class="text_custom" id="searchKeyword" name="searchKeyword" value="" placeholder="출발지/도착지입력" >	
									<button id="btn_select">검색</button>
						<button id="arrivalSave" type="submit" class="btn btn-success" style.="display:inline;">저장</button>
					</div> -->
					
					<br>
					 
					 
				<!-- 	<div style="margin-top:1%; margin-left:40%;" onclick="javascript:goRouteView();">
							<img style=" width:40%; height:40%;" src="/img/hkicon1.png" />
					</div> --> 
					
					
						<div style="margin-top: 5%; text-align: center; display:inline-block; margin-left:25%; width:50%;">
							<div style="border-radius: 50%; height: 30%; width: 25%; background-color: #CEF6F5; margin-left: 40%;">
							<a style="cursor: pointer; width: 100%; border-radius: 80%; background: #CEF6F5;"  onclick="javascript:goRouteView();" class="">조회</a>
					
						</div>
					</div>
					
							<!-- <div id="search" style="text-align:right;" onclick="javascript:goRouteView();"> 
							
			                 		<img style=" width:6%; height:6%;" src="/img/icons8-search-48.png" />
			                 		 탁송금액조회 
			            	</div> -->


								
								
								
            	</div>
            </div>
            
        </div>
			
</body> 

								
								
								
	<!-- <script type="text/javascript" src="http://dapi.kakao.com/v2/maps/sdk.js?appkey=196301395a31adf3319a7ee5f66f17da&libraries=services"></script> -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>     
    <script src="/js/vendor/bootstrap.min.js"></script>       
    <script src="/js/vendor/jquery.form.min.js"></script>
    <script src="/js/animsition.min.js"></script>
    <script src="/js/vendor/html2canvas.min.js"></script>
    <script src="/js/messagebox.js"></script>
    <script src="/js/vendor/jquery.loading-indicator.min.js"></script>
    <!-- <script src="https://apis.openapi.sk.com/tmap/js?version=1&format=javascript&appKey=l7xx6b701ff5595c4e3b859e42f57de0c975"></script> -->
     <!-- <script src="https://apis.openapi.sk.com/tmap/jsv2?version=1&appKey=l7xx6b701ff5595c4e3b859e42f57de0c975"></script> -->
    <script src="https://apis.openapi.sk.com/tmap/jsv2?version=1&appKey=l7xx6b701ff5595c4e3b859e42f57de0c975"></script>
    <!-- veiwport for countnumber -->
    <script src="/js/alert.js"></script>
    <script src="/js/main.js"></script>   
    <script src="/js/vendor/xpull.js"></script>       
    
    <script>
    
 var homeLoader;

 var inputStatus = "departure";


 
 
    $(document).ready(function(){
    
    	//$("#departure").focus();
    	homeLoader = $('body').loadingIndicator({
			useImage: false,
			showOnInit : false
		}).data("loadingIndicator");
	
    	homeLoader.show();

    	var updateHomeLoader = setInterval(function() {
    		clearInterval(updateHomeLoader);
    		homeLoader.hide();
    	}, 300);
        	
    	
    	
    });



    var map, marker;
    var markerArr = [];
    function initTmap(){
     	// 1. 지도 띄우기
   /*  	map = new Tmapv2.Map("map_div", {
        	
    		center: new Tmapv2.LatLng(37.570028, 126.986072),
    		width : "70%",
    		height : "400px",
    		zoom : 15,
    		zoomControl : true,
    		scrollwheel : true
    		
    	}); */
    	
    	// 2. POI 통합 검색 API 요청
    	$("#btn_select").click(function(){
    		
    		var searchKeyword = $('#searchKeyword').val();
    		$("#searchResult").css("display","block");

    		$.ajax({
    			method:"GET",
    			url:"https://apis.openapi.sk.com/tmap/pois?version=1&format=json&callback=result",
    			async:false,
    			data:{
    				"appKey" : "l7xx6b701ff5595c4e3b859e42f57de0c975",
    				"searchKeyword" : searchKeyword,
    				"resCoordType" : "EPSG3857",
    				"reqCoordType" : "WGS84GEO",
    				"count" : 10
    			},
    			success:function(response){
    				var resultpoisData = response.searchPoiInfo.pois.poi;

    				
    				
    				// 기존 마커, 팝업 제거
    				if(markerArr.length > 0){
    					for(var i in markerArr){
    						markerArr[i].setMap(null);
    					}
    				}
    				var innerHtml ="";	// Search Reulsts 결과값 노출 위한 변수
    				var positionBounds = new Tmapv2.LatLngBounds();		//맵에 결과물 확인 하기 위한 LatLngBounds객체 생성
    				
    				for(var k in resultpoisData){
    					
    					var noorLat = Number(resultpoisData[k].noorLat);
    					var noorLon = Number(resultpoisData[k].noorLon);
    					var name = resultpoisData[k].name;

    					var upperAddrName = resultpoisData[k].upperAddrName;
    					var middleAddrName = resultpoisData[k].middleAddrName;
    					var lowerAddrName = resultpoisData[k].lowerAddrName;
    					var detailAddrName = resultpoisData[k].detailAddrName;
    					var firstNo = resultpoisData[k].firstNo;
    					var secondNo = resultpoisData[k].secondNo == "" ? "" : "-"+resultpoisData[k].secondNo;
						/* alert(upperAddrName+middleAddrName+lowerAddrName+detailAddrName); */

						
    					
    					var pointCng = new Tmapv2.Point(noorLon, noorLat);
    					var projectionCng = new Tmapv2.Projection.convertEPSG3857ToWGS84GEO(pointCng);
    					
    					var lat = projectionCng._lat;
    					var lon = projectionCng._lng;
    					
    					var markerPosition = new Tmapv2.LatLng(lat, lon);
    					
    					marker = new Tmapv2.Marker({
    				 		position : markerPosition,
    				 		//icon : "http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_a.png",
    				 		icon : "http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_" + k + ".png",
    						iconSize : new Tmapv2.Size(24, 38),
    						title : name,
    						//map:map
    				 	});
    					
    					//innerHtml += "<li><img src='http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_" + k + ".png' style='vertical-align:middle;'/><span onclick='javascript:inputValue(\''+name+'\')'>"+name+"</span></li>";
    					//innerHtml += "<li><img src='http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_" + k + ".png' style='vertical-align:middle;'/><span onclick='javascript:inputValue()'>"+name+"</span></li>";
    					//'<div class="info" onclick="javascript:inputValue(\''+places.road_address_name+'\',\''+places.address_name+'\',\''+places.place_name+'\',\''+places.x+'\',\''+places.y+'\')">' +
						//innerHtml += '<li><img src="http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_' + k + '.png" style="vertical-align:middle;"/><span onclick="javascript:inputValue(\''+name+'\',\''+lon+'\',\''+lat+'\')">'+name+'</span></li>';
						
						innerHtml += '<li  onclick="javascript:inputValue(\''+name+'\',\''+lon+'\',\''+lat+'\')">';    					
						innerHtml += '<img src="http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_' + k + '.png" style="vertical-align:middle;"/>';
						innerHtml += '<span>'+name+'</span>';
						innerHtml += '<span style="display:block; color:#3f3f3f;" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;('+upperAddrName+" "+middleAddrName+" "+lowerAddrName+" "+detailAddrName+" "+firstNo+secondNo+')</span>';
						innerHtml += '</li>';

    					
    					
    					markerArr.push(marker);
    					positionBounds.extend(markerPosition);	// LatLngBounds의 객체 확장
    				}
    				
    				$("#searchResult").html(innerHtml);	//searchResult 결과값 노출
    				//map.panToBounds(positionBounds);	// 확장된 bounds의 중심으로 이동시키기
    				//map.zoomOut();
    				
    			},
    			error:function(request,status,error){
    				console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
    			}
    		});
    	});



 	$("#btn_select2").click(function(){
    		
    		var searchKeyword = $('#searchKeyword2').val();
    		$("#searchResult2").css("display","block");
    		$.ajax({
    			method:"GET",
    			url:"https://apis.openapi.sk.com/tmap/pois?version=1&format=json&callback=result",
    			async:false,
    			data:{
    				"appKey" : "l7xx6b701ff5595c4e3b859e42f57de0c975",
    				"searchKeyword" : searchKeyword,
    				"resCoordType" : "EPSG3857",
    				"reqCoordType" : "WGS84GEO",
    				"count" : 10
    			},
    			success:function(response){
    				var resultpoisData = response.searchPoiInfo.pois.poi;

    				
    				
    				// 기존 마커, 팝업 제거
    				if(markerArr.length > 0){
    					for(var i in markerArr){
    						markerArr[i].setMap(null);
    					}
    				}
    				var innerHtml ="";	// Search Reulsts 결과값 노출 위한 변수
    				var positionBounds = new Tmapv2.LatLngBounds();		//맵에 결과물 확인 하기 위한 LatLngBounds객체 생성
    				
    				for(var k in resultpoisData){
    					
    					var noorLat = Number(resultpoisData[k].noorLat);
    					var noorLon = Number(resultpoisData[k].noorLon);
    					var name = resultpoisData[k].name;

    					var upperAddrName = resultpoisData[k].upperAddrName;
    					var middleAddrName = resultpoisData[k].middleAddrName;
    					var lowerAddrName = resultpoisData[k].lowerAddrName;
    					var detailAddrName = resultpoisData[k].detailAddrName;
    					var firstNo = resultpoisData[k].firstNo;
    					var secondNo = resultpoisData[k].secondNo == "" ? "" : "-"+resultpoisData[k].secondNo;

						//alert("upperAddrName="+upperAddrName+"middleAddrName="+middleAddrName+"lowerAddrName="+lowerAddrName+"detailAddrName="+detailAddrName);
		
						
    					
    					var pointCng = new Tmapv2.Point(noorLon, noorLat);
    					var projectionCng = new Tmapv2.Projection.convertEPSG3857ToWGS84GEO(pointCng);
    					
    					var lat = projectionCng._lat;
    					var lon = projectionCng._lng;
    					
    					var markerPosition = new Tmapv2.LatLng(lat, lon);
    					
    					marker = new Tmapv2.Marker({
    				 		position : markerPosition,
    				 		//icon : "http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_a.png",
    				 		icon : "http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_" + k + ".png",
    						iconSize : new Tmapv2.Size(24, 38),
    						title : name,
    						//map:map
    				 	});
    					
    					//innerHtml += "<li><img src='http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_" + k + ".png' style='vertical-align:middle;'/><span onclick='javascript:inputValue(\''+name+'\')'>"+name+"</span></li>";
    					//innerHtml += "<li><img src='http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_" + k + ".png' style='vertical-align:middle;'/><span onclick='javascript:inputValue()'>"+name+"</span></li>";
    					//'<div class="info" onclick="javascript:inputValue(\''+places.road_address_name+'\',\''+places.address_name+'\',\''+places.place_name+'\',\''+places.x+'\',\''+places.y+'\')">' +
    					
    					
						innerHtml += '<li  onclick="javascript:inputValue(\''+name+'\',\''+lon+'\',\''+lat+'\')">';    					
						innerHtml += '<img src="http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_' + k + '.png" style="vertical-align:middle;"/>';
						innerHtml += '<span>'+name+'</span>';
						innerHtml += '<span style="display:block; color:#3f3f3f;" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;('+upperAddrName+" "+middleAddrName+" "+lowerAddrName+" "+detailAddrName+" "+firstNo+secondNo+')</span>';
						innerHtml += '</li>';

    					
    					markerArr.push(marker);
    					positionBounds.extend(markerPosition);	// LatLngBounds의 객체 확장
    				}
    				
    				$("#searchResult2").html(innerHtml);	//searchResult 결과값 노출
    				//map.panToBounds(positionBounds);	// 확장된 bounds의 중심으로 이동시키기
    				//map.zoomOut();
    				
    			},
    			error:function(request,status,error){
    				console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
    			}
    		});
    	});


    	
    }
    

    
    
/* function inputValue(address,px,py){

	var depatureTemp =$("#departure").val();
	var arrivalTemp=$("#arrival").val();

    
	if(inputStatus == "departure" || depatureTemp == ""){

		//document.getElementById('depature').setAttribute('value',title);
	
					
		$("#departure").val(address);
		$("#departure").next().val(px+","+py);
		// $("#departure").val(addressName);
		//$("#departure").val(placeName);
	 
		inputStatus = "arrival";

			
	}else{


		$("#arrival").val(address);
		$("#arrival").next().val(px+","+py); 
		/* $("#arrival").val(addressName);
		$("#arrival").val(placeName);
		}


} */


function inputValue(address,px,py){

	var depatureTemp =$("#searchKeyword").val();
	var arrivalTemp =$("#searchKeyword2").val();



		if(inputStatus == "departure" || depatureTemp == ""){


		
			$("#searchKeyword").val(address);
			$("#searchKeyword").next().val(px+","+py);
			$("#searchResult").css("display","none");
		 
				inputStatus = "arrival";


				
		}else{


			$("#searchKeyword2").val(address);
			$("#searchKeyword2").next().val(px+","+py); 
			$("#searchResult2").css("display","none");



		}

		





	

/* 

	
	var depatureTemp =$("#searchKeyword").val();
	var arrivalTemp =$("#searchKeyword2").val();

    
	if(inputStatus == "departure" || depatureTemp == ""){

		//document.getElementById('depature').setAttribute('value',title);
	
		$("#searchKeyword").val(address);
		$("#searchKeyword").next().val(px+","+py);
		$("#searchResult").css("display","none");
	 
			inputStatus = "arrival";
			
	}else{


		$("#searchKeyword2").val(address);
		$("#searchKeyword2").next().val(px+","+py); 
		$("#searchResult2").css("display","none");



	}

 */




	


}
	


	
/* 	$('#container').xpull({
	    'paused': false, 
	    // Pull threshold - amount in  pixels required to pull to enable release callback
        'pullThreshold':50,
        // Max pull down element - amount in pixels
        maxPullThreshold: 50,
        // timeout in miliseconds after which the loading indicator stops spinning.
        // If set to 0 - the loading will be indefinite
        'spinnerTimeout':1000,
        onPullStart: function(){},
        onPullEnd: function(){},
        callback: function(){window.location.reload()},

	});    */

	// 마커를 담을 배열입니다
	/* var markers = [];

	var mapContainer = document.getElementById('map'), // 지도를 표시할 div 
	    mapOption = {
	        center: new kakao.maps.LatLng(37.566826, 126.9786567), // 지도의 중심좌표
	        level: 3 // 지도의 확대 레벨
	    };  

	// 지도를 생성합니다    
	var map = new kakao.maps.Map(mapContainer, mapOption); 

	// 장소 검색 객체를 생성합니다
	var ps = new kakao.maps.services.Places();  

	// 검색 결과 목록이나 마커를 클릭했을 때 장소명을 표출할 인포윈도우를 생성합니다
	var infowindow = new kakao.maps.InfoWindow({zIndex:1});

	// 키워드로 장소를 검색합니다
	searchPlaces();

	// 키워드 검색을 요청하는 함수입니다
	function searchPlaces() {

	    var keyword = document.getElementById('keyword').value;

	
	    if (!keyword.replace(/^\s+|\s+$/g, '')) {
	       //$.alert('키워드를 입력해주세요!');
	        return false;
	    }

	    // 장소검색 객체를 통해 키워드로 장소검색을 요청합니다
	    ps.keywordSearch( keyword, placesSearchCB); 
	}

	// 장소검색이 완료됐을 때 호출되는 콜백함수 입니다
	function placesSearchCB(data, status, pagination) {
	    if (status === kakao.maps.services.Status.OK) {

	        // 정상적으로 검색이 완료됐으면
	        // 검색 목록과 마커를 표출합니다
	        displayPlaces(data);

	        // 페이지 번호를 표출합니다
	        displayPagination(pagination);

	    } else if (status === kakao.maps.services.Status.ZERO_RESULT) {

	        $.alert('검색 결과가 존재하지 않습니다.');
	        return;

	    } else if (status === kakao.maps.services.Status.ERROR) {

	        $.alert('검색 결과 중 오류가 발생했습니다.');
	        return;

	    }
	}
 */
	// 검색 결과 목록과 마커를 표출하는 함수입니다
/* 	function displayPlaces(places) {

	    var listEl = document.getElementById('placesList'), 
	    menuEl = document.getElementById('menu_wrap'),
	    fragment = document.createDocumentFragment(), 
	    bounds = new kakao.maps.LatLngBounds(), 
	    listStr = '';
	    
	    // 검색 결과 목록에 추가된 항목들을 제거합니다
	    removeAllChildNods(listEl);

	    // 지도에 표시되고 있는 마커를 제거합니다
	    removeMarker();
	    
	    	for ( var i=0; i<places.length; i++ ) {

			//alert("places[i].y="+places[i].y+"/places[i].x="+places[i].x);
	        // 마커를 생성하고 지도에 표시합니다
	        
	        var placePosition = new kakao.maps.LatLng(places[i].y, places[i].x),
	        
	            marker = addMarker(placePosition, i), 
	            itemEl = getListItem(i, places[i]); // 검색 결과 항목 Element를 생성합니다

		
	            
	        // 검색된 장소 위치를 기준으로 지도 범위를 재설정하기위해
	        // LatLngBounds 객체에 좌표를 추가합니다
	        bounds.extend(placePosition);

	        // 마커와 검색결과 항목에 mouseover 했을때
	        // 해당 장소에 인포윈도우에 장소명을 표시합니다
	        // mouseout 했을 때는 인포윈도우를 닫습니다
	        (function(marker, title) {
	            kakao.maps.event.addListener(marker, 'mouseover', function() {
	                displayInfowindow(marker, title);
	            });

	            kakao.maps.event.addListener(marker, 'mouseout', function() {
	                infowindow.close();
	            });

	            itemEl.onmouseover =  function () {
	                displayInfowindow(marker, title);
	            };

	            itemEl.onmouseout =  function () {
	                infowindow.close();
	            };
	        })(marker, places[i].place_name);

	        fragment.appendChild(itemEl);
	    }

	    // 검색결과 항목들을 검색결과 목록 Elemnet에 추가합니다
	    listEl.appendChild(fragment);
	    menuEl.scrollTop = 0;

	    // 검색된 장소 위치를 기준으로 지도 범위를 재설정합니다
	    map.setBounds(bounds);
		}

	 */
		
	// 검색결과 항목을 Element로 반환하는 함수입니다
/* 	function getListItem(index, places) {

		
	    var el = document.createElement('li'),
	    itemStr = '<span class="markerbg marker_' + (index+1) + '" ></span>' +
	                '<div class="info" onclick="javascript:inputValue(\''+places.road_address_name+'\',\''+places.address_name+'\',\''+places.place_name+'\',\''+places.x+'\',\''+places.y+'\')">' +
	                '   <h5>' + places.place_name + '</h5>';
 
	    if (places.road_address_name) {
	        itemStr += '    <span >' + places.road_address_name + '</span>' +
	                    '   <span class="jibun gray">' +  places.address_name  + '</span>';
	        
	        
	    } else {
	        itemStr += '    <span>' +  places.address_name  + '</span>'; 
		
	    }
	                 
	      itemStr += '  <span class="tel">' + places.phone  + '</span>' +
	                '</div>';           
	    			
		
	    el.innerHTML = itemStr;
	    el.className = 'item';

	    return el;
	}





	// 마커를 생성하고 지도 위에 마커를 표시하는 함수입니다
	function addMarker(position, idx, title) {
	    var imageSrc = 'https://t1.daumcdn.net/localimg/localimages/07/mapapidoc/marker_number_blue.png', // 마커 이미지 url, 스프라이트 이미지를 씁니다
	        imageSize = new kakao.maps.Size(36, 37),  // 마커 이미지의 크기
	        imgOptions =  {
	            spriteSize : new kakao.maps.Size(36, 691), // 스프라이트 이미지의 크기
	            spriteOrigin : new kakao.maps.Point(0, (idx*46)+10), // 스프라이트 이미지 중 사용할 영역의 좌상단 좌표
	            offset: new kakao.maps.Point(13, 37) // 마커 좌표에 일치시킬 이미지 내에서의 좌표
	        },
	        markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize, imgOptions),
	            marker = new kakao.maps.Marker({
	            position: position, // 마커의 위치
	            image: markerImage 
	        });

	    marker.setMap(map); // 지도 위에 마커를 표출합니다
	    markers.push(marker);  // 배열에 생성된 마커를 추가합니다

	    return marker;
	}

	// 지도 위에 표시되고 있는 마커를 모두 제거합니다
	function removeMarker() {
	    for ( var i = 0; i < markers.length; i++ ) {
	        markers[i].setMap(null);
	    }   
	    markers = [];
	}

	// 검색결과 목록 하단에 페이지번호를 표시는 함수입니다
	function displayPagination(pagination) {
	    var paginationEl = document.getElementById('pagination'),
	        fragment = document.createDocumentFragment(),
	        i; 

	    // 기존에 추가된 페이지번호를 삭제합니다
	    while (paginationEl.hasChildNodes()) {
	        paginationEl.removeChild (paginationEl.lastChild);
	    }

	    for (i=1; i<=pagination.last; i++) {
	        var el = document.createElement('a');
	        el.href = "#";
	        el.innerHTML = i;

	        if (i===pagination.current) {
	            el.className = 'on';
	        } else {
	            el.onclick = (function(i) {
	                return function() {
	                    pagination.gotoPage(i);
	                }
	            })(i);
	        }

	        fragment.appendChild(el);
	    }
	    paginationEl.appendChild(fragment);
	}

	// 검색결과 목록 또는 마커를 클릭했을 때 호출되는 함수입니다
	// 인포윈도우에 장소명을 표시합니다


	
		function displayInfowindow(marker, title) {
		    var content = '<div style="padding:5px;z-index:1;">' + title + '</div>';
	
			var depatureTemp ="";
			var arrivalTemp="";

			  	
	    infowindow.setContent(content);
	    infowindow.open(map, marker);

		}


		
		function inputValue(roadAddressName,addressName,placeName,px,py){

			var depatureTemp =$("#departure").val();
			var arrivalTemp=$("#arrival").val();


		
		      
			if(inputStatus == "departure" || depatureTemp == ""){

				//document.getElementById('depature').setAttribute('value',title);
			
							
				$("#departure").val(roadAddressName);
				$("#departure").val(addressName);
				$("#departure").val(placeName);
				$("#departure").next().val(px+","+py);
				
				inputStatus = "arrival";

					
			}else{


				$("#arrival").val(roadAddressName);
				$("#arrival").val(addressName);
				$("#arrival").val(placeName);
				$("#arrival").next().val(px+","+py)

			
				}
			
			
		}
	 // 검색결과 목록의 자식 Element를 제거하는 함수입니다		
	function removeAllChildNods(el) {   
	    while (el.hasChildNodes()) {
	        el.removeChild (el.lastChild);
	    }
	}
     */

		 
	
	 
	 

	
    </script>
    </body>
</html>
