<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko"  style=" height:100%;">      
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="https://topopen.tmap.co.kr/tmaplibv20/theme/default/style_effect.css">
<link rel="stylesheet" href="https://topopen.tmap.co.kr/tmaplibv20/theme/default/style.css" type="text/css">


<script src="https://apis.openapi.sk.com/tmap/js?version=1&format=javascript&appKey=${tmapAppKey}"></script>
<script src="http://dapi.kakao.com/v2/maps/sdk.js?appkey=196301395a31adf3319a7ee5f66f17da"></script>


<script src="https://topopen.tmap.co.kr/tmaplibv20/Core.js?vn=Release1.18.25"></script>
<script src="https://topopen.tmap.co.kr/tmaplibv20/Format.js?vn=Release1.18.25"></script>
<script src="https://topopen.tmap.co.kr/tmaplibv20/Add-on.js?vn=Release1.18.25"></script>

<body style=" height:100%;">


<script type="text/javascript">  

$(document).ready(function(){
	
	//getNewsData("${searchWord}");
	
	
/* 	var roadviewContainer = document.getElementById('roadview'); //로드뷰를 표시할 div
	var roadview = new kakao.maps.Roadview(roadviewContainer); //로드뷰 객체
	var roadviewClient = new kakao.maps.RoadviewClient(); //좌표로부터 로드뷰 파노ID를 가져올 로드뷰 helper객체

	var position = new kakao.maps.LatLng();

	// 특정 위치의 좌표와 가까운 로드뷰의 panoId를 추출하여 로드뷰를 띄운다.
	roadviewClient.getNearestPanoId(position, 50, function(panoId) {
	    roadview.setPanoId(panoId, position); //panoId와 중심좌표를 통해 로드뷰 실행
	});
	 */
});


function getNewsData(obj,keyword){
	
	 
	// alert($("#selectedStatus").children().length);
	 
	//alert($("#selectedStatus").children().first().get(0));	
	//alert($(obj).parent().get(0));
	
 	 if($("#selectedStatus").children().first().get(0) == $(obj).parent().get(0)){				//전체를 클릭 한경우
					
 		$("#selectedStatus").children().each(function(index,element){
 			$(this).removeClass('selected');
 		});
 		$(obj).parent().addClass('selected');
	}else{
			
	} 
	
	
	
/* 	
	
	$.ajax({ 
		type: 'post' ,
		url : "/research/newsSearch.do?&keyword="+keyword ,
		dataType : 'json' ,
		data : {
			
		},
		success : function(data, textStatus, jqXHR)
		{
			alert(data);
		} ,
		error : function(xhRequest, ErrorText, thrownError) {
		}
	});
	
	 */
	
	
} 

 function goNewsPage(uri){
		
		//var replaceUrlStr = replaceAll(url, "?", "^");
		//replaceUrlStr = replaceAll(replaceUrlStr, "=", "+");
		
		var replaceUrlStr = encodeURIComponent(uri)
		document.location.href = "/news/newsPage.do?newsUri="+replaceUrlStr;
	}  


    
function viewAllocationData(allocationId,allocationStatus){
	document.location.href = "/carrier/allocation-detail.do?allocationStatus="+allocationStatus+"&allocationId="+allocationId;
} 
 
function logout(){
	
	$.confirm("로그아웃 하시겠습니까?",function(a){
		 if(a){
			 if(window.Android != null){
					window.Android.stopService("${driverId}");
				}	
			 document.location.href = "/logout.do";	
		 }
	});
	
}   
 
 
function backKeyController(str){

		document.location.href = "/carrier/consignment-Reservation.do?&tempId="+"${tempId}";
} 


function goBackPage(){

	document.location.href = "/carrier/consignment-Reservation.do?&tempId="+"${tempId}";
	
}


</script>


        <div class="content-container" style=" height:100%; /* overflow-y:hidden; */">

            <header class="bg-pink clearfix" style="position:fixed; background-color:#fff; border:none; text-align:center; z-index:1000; ">
                <div class="" style="width:19%;">
                    <a href="javascript:goBackPage();"><img style="zoom: 0.5;" src="/img/back-icon.png" alt=""></a> 
                </div> 
               <div class="" style="width:60%;" onclick ="javascript:homeLoader.show();document.location.href='/carrier/main.do'">
                	 <img style="width:90%; height:60%;" src="/img/main_logo.png" alt="">
                </div>
                <div class="" style="width:19%; float:right;">

                </div>
            </header>

            <div style=" text-align:right; clear:both;">&nbsp;</div>
            <div class="content-container interest-page loadingthemepage news" style="position:fixed; background-color:#fff; clear:both;">
            </div>
            <div id="container" class="search-result news-list content-box" style="height:100%; margin-top:8%;  overflow-y:scroll; overflow-x:hidden;">
            	<div class="xpull">
				    <div class="xpull__start-msg">
				        <div class="xpull__start-msg-text">화면을 당겼다 놓으면 새로 고침 됩니다.</div>
				        <div class="xpull__arrow"></div>
				    </div>
				    <div class="xpull__spinner">
				        <div class="xpull__spinner-circle"></div>
				    </div>
				</div>
            	<div class="news-container clearfix" style="height:100%;">
            	
    	       		<!-- <div id="map" style="width:100%; height:46%;">
					</div> -->
					<div id="map_div" class="map_wrap tmMap" style="width:100%; height:50%;">
					
					</div>
					
    	 			  <div id="roadview" style="width:100%; height:47%;">
							<div id="" style="margin-top:10%; width:100%;text-align:left;">
								<div style="display:inline; width:100%; margin-left: 5%; color:#A4A4A4;" >검색한 상차지  :&nbsp;</div>
										<span class="d-tbc" style="font-weight: bold; ">${tempConsignmentMap.departure_addr}</span> 
								</div>
							<div id="" style="margin-top:5%; width:100%;text-align:left;">
								<div style="display:inline; width:100%; margin-left: 5%; color:#A4A4A4;">검색한 하차지 :&nbsp;</div>
									<span class="d-tbc" style="font-weight: bold;">${tempConsignmentMap.arrival_addr}</span>									
							</div>
							<div id="" style="margin-top:5%; width:100%;text-align:left;">
								<div style="display:inline; width:100%; margin-left: 5%; color:#A4A4A4;">예상 &nbsp;&nbsp;거리 &nbsp;:&nbsp;&nbsp;&nbsp;</div>
										<span class="d-tbc" style="font-weight: bold;">${distance}km</span>
							</div>
							<div id="" style="margin-top:5%; width:100%;text-align:left;">
								<div style="display:inline; width:100%; margin-left: 5%; color:#A4A4A4;">탁송 예상 가격 :&nbsp;</div>
										<span class="d-tbc" style="font-weight: bold;">${price}원</span>
							</div>
							
							
						<br>
						
					</div>
    	     
    			
    	       		
                 </div>
            </div>
        </div>


    
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>      
    <script src="/js/vendor/bootstrap.min.js"></script>       
    <script src="/js/vendor/jquery.loading-indicator.min.js"></script>
    <script src="/js/alert.js"></script>
    <script src="/js/main.js"></script>   
    <script src="/js/vendor/xpull.js"></script>       
    
    
    
    <script>
    
	
	var homeLoader;
	var map, markerLayer,marker,tData,vectorLayer,popup;
	var zoomDepth = 17;
	var searchStatus = true;
	var mLastUrlString = ''; // 가장 최근에 불러온 메인 화면 URL 저장
	var mIsNeedRefresh = false; // 새로고침 필요여부(검색페이지 로드 시 Anchor 가 변하지 않아 강제 새로고침 필요)
	
	$(document).ready(function(){

    	//$("#departure").focus();
    	homeLoader = $('body').loadingIndicator({
			useImage: false,
			showOnInit : false
		}).data("loadingIndicator");
	
    	homeLoader.show();
/*     	
		var updateToken = setInterval( function() {
			clearInterval(updateToken);
			pageMoveStop = false;
			homeLoader.hide();

		}, 300);
		
 */


		initTmap();

		loadMainContents($.urlAnchor()); // 메인 화면 불러오기
		$(window).on('hashchange', function(evt) {
			// URL #(앵커) 변경시 호출됨
			loadMainContents($.urlAnchor()); // 메인 화면 불러오기
		});
		$(document).on('click', 'a', function(evt) {
			// a Link 클릭시 호출됨
			var strHref = $(this).attr('href');
			if ('#' + $.urlAnchor() == strHref) {
				// URL 이 같으면 a 링크 클릭해도 hashchange 이벤트가 발생하지 않기 때문에 임의로 함수호출
				loadMainContents($.urlAnchor()); // 메인 화면 불러오기
			}
		});
		

		// 검색어 창 이벤트 설정
		$('#search_keyword').keyup(function() {
			searchMenuTitle($(this).val());
		});
		$('#search_keyword').keydown(function(evt) {
			if (evt.keyCode == 13) {
				$('#search_button').trigger('click');
			}
		});

		// 검색 버튼 이벤트 설정
		$('#search_button').click(function() {
			loadSearchIndex($('#search_keyword').val());
		});


		drawRoute("${departure}","${arrival}");

		 


		 
	});




	 function getNewsData(obj,keyword){

	 	 if($("#selectedStatus").children().first().get(0) == $(obj).parent().get(0)){				//전체를 클릭 한경우
						
	 		$("#selectedStatus").children().each(function(index,element){
	 			$(this).removeClass('selected');
	 		});
	 		$(obj).parent().addClass('selected');
		}else{
				
		} 
		
	} 

	 function goNewsPage(uri){
			
			//var replaceUrlStr = replaceAll(url, "?", "^");
			//replaceUrlStr = replaceAll(replaceUrlStr, "=", "+");
			
			var replaceUrlStr = encodeURIComponent(uri)
			document.location.href = "/news/newsPage.do?newsUri="+replaceUrlStr;
		}  


	    
	function viewAllocationData(allocationId,allocationStatus){
		document.location.href = "/carrier/allocation-detail.do?allocationStatus="+allocationStatus+"&allocationId="+allocationId;
	} 
	 
	function logout(){
		
		$.confirm("로그아웃 하시겠습니까?",function(a){
			 if(a){
				 if(window.Android != null){
						window.Android.stopService("${driverId}");
					}	
				 document.location.href = "/logout.do";	
			 }
		});
		
	}   
	 
	 
/* 	function backKeyController(str){

		homeLoader.show();
		document.location.href="/carrier/kakaoRouteView.do?call=${call}";


		var updateHomeLoader = setInterval(function() {
			clearInterval(updateHomeLoader);
			homeLoader.hide();
		}, 300);
		
	}  */


	function goKakaoTalk(){
		
		//$.alert("준비중입니다.");

	 	$.confirm("카카오톡 채널로 이동 하시겠습니까?",function(a){
			 if(a){
				if(window.Android != null){
					window.Android.openKakaoChannel();
				}else if(webkit.messageHandlers != null){
					webkit.messageHandlers.scriptHandler.postMessage("goKakaoChannel");

				}else{
					$.alert("알수 없는 에러입니다. 관리자에게 문의해주세요");					
				}
			 }
		});
		
	}


	//페이지가 로딩이 된 후 호출하는 함수입니다.
	function initTmap(){

		map = new Tmap.Map({
			div:'map_div',
			/* width : "900px",
			height : "900px", */
		});
		map.setCenter(new Tmap.LonLat("127.00118587318626", "37.457399").transform("EPSG:4326", "EPSG:3857"), zoomDepth);


		tData = new Tmap.TData();

		
		map.events.register("click", map, onClick);


		markerLayer = new Tmap.Layer.Markers();//마커 레이어 생성
		map.addLayer(markerLayer);//map에 마커 레이어 추가

		vectorLayer = new Tmap.Layer.Vector("vectorLayerID");
		map.addLayer(vectorLayer);

		/* markerLayer = new Tmap.Layer.Markers();
		map.addLayer(markerLayer);
		   
		var lonlat = new Tmap.LonLat( 127.02557976548207 , 37.457399).transform("EPSG:4326", "EPSG:3857");
		var size = new Tmap.Size(24, 38);
		var offset = new Tmap.Pixel(-(size.w / 2), -(size.h));
		var icon = new Tmap.Icon('http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_a.png',size, offset);
		
		marker = new Tmap.Marker(lonlat, icon);
		markerLayer.addMarker(marker); */

		
	} 

	var departure = new Object();
	var arrival = new Object();

	function onClick(e){


		var lonlat = map.getLonLatFromViewPortPx(e.xy).transform("EPSG:3857", "EPSG:4326");//클릭한 부분의 ViewPorPx를 LonLat로 변환합니다
		var result ='클릭한 위치의 좌표는'+lonlat+'입니다.';

		markerLayer.clearMarkers();
		markerLayer.removeMarker(marker); // 기존 마커 삭제
		
		if(map.getZoom() < 18){
			zoomDepth = 18
			map.setCenter(new Tmap.LonLat(lonlat.lon, lonlat.lat).transform("EPSG:4326", "EPSG:3857"), zoomDepth);

		}else{

			if(searchStatus){
				setLocation(lonlat,lonlat.lon,lonlat.lat);
			}
			
		}

		
		
			  
	}



	function searchStart(obj){

		

		$.ajax({
			method:"GET",
			url:"https://apis.openapi.sk.com/tmap/pois?version=1&format=xml&callback=result",// POI 통합검색 api 요청 url입니다.
			async:false,
			data:{
				"searchKeyword" : $(obj).val(),//검색 키워드
				"resCoordType" : "EPSG3857",//응답 좌표계 유형
				"reqCoordType" : "WGS84GEO",//요청 좌표계 유형
				"appKey" : "${tmapAppKey}",// 실행을 위한 키입니다. 발급받으신 AppKey(appKey)를 입력하세요.
				"count" : 10//페이지당 출력되는 개수를 지정
			},
			//데이터 로드가 성공적으로 완료되었을 때 발생하는 함수입니다.
			success:function(response){
				prtcl = response;
				
				// 2. 기존 마커, 팝업 제거
				if(markerLayer != null) {
					markerLayer.clearMarkers();
					map.removeAllPopup();
				}
				
				// 3. POI 마커 표시
				markerLayer = new Tmap.Layer.Markers();//마커 레이어 생성
				map.addLayer(markerLayer);//map에 마커 레이어 추가
				var size = new Tmap.Size(24, 38);//아이콘 크기 설정
				var offset = new Tmap.Pixel(-(size.w / 2), -(size.h));//아이콘 중심점 설정
				var maker;
				var popup;
				var prtclString = new XMLSerializer().serializeToString(prtcl);//xml to String	
				xmlDoc = $.parseXML( prtclString ),
				$xml = $( xmlDoc ),
				$intRate = $xml.find("poi");
				var innerHtml ="";
				$intRate.each(function(index, element) {
				   	var name = element.getElementsByTagName("name")[0].childNodes[0].nodeValue;
				   	var id = element.getElementsByTagName("id")[0].childNodes[0].nodeValue;
				   	var content ="<div style=' position: relative; border-bottom: 1px solid #dcdcdc; line-height: 18px; padding: 0 35px 2px 0;'>"+
								    "<div style='font-size: 12px; line-height: 15px;'>"+
								        "<span style='display: inline-block; width: 14px; height: 14px;  vertical-align: middle; margin-right: 5px;'></span>"+name+
								    "</div>"+
								 "</div>";
					var lon = element.getElementsByTagName("noorLon")[0].childNodes[0].nodeValue;
					var lat = element.getElementsByTagName("noorLat")[0].childNodes[0].nodeValue;
					var icon = new Tmap.Icon('http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_'+index+'.png',size, offset);//마커 아이콘 설정
					var lonlat = new Tmap.LonLat(lon, lat);//좌표설정

					var location = get4326LonLat(lon, lat);
					//alert(location.lon);
					//alert(location.lat);
					
				   	innerHtml+="<div style='cursor:pointer;'  onclick='javascript:setLocation(\""+location+"\",\""+location.lon+"\",\""+location.lat+"\")'><img src='http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_"+index+".png' style='vertical-align:middle'/><span>"+name+"</span></div>";
					
					marker = new Tmap.Marker(lonlat, icon);//마커생성
					markerLayer.addMarker(marker);//마커레이어에 마커 추가

					//marker.events.register("click", marker, onMarkerClick);
					
					popup = new Tmap.Popup("p1", lonlat, new Tmap.Size(120, 50), content, false);//마우스 오버 팝업
					popup.autoSize = true;//Contents 내용에 맞게 Popup창의 크기를 재조정할지 여부를 결정
					map.addPopup(popup);//map에 popup추가
					popup.hide();//마커에 마우스가 오버되기 전까진 popup을 숨김
					//마커 이벤트등록
				    marker.events.register("mouseover", popup, onOverMarker);
				    //마커에 마우스가 오버되었을 때 발생하는 이벤트 함수입니다.
				    function onOverMarker(evt) {
				      this.show(); //마커에 마우스가 오버되었을 때 팝업이 보입니다. 
				    }
				    //마커 이벤트 등록
				    marker.events.register("mouseout", popup, onOutMarker);
				    //마커에 마우스가 아웃되었을 때 발생하는 함수입니다.
				    function onOutMarker(evt) {
				      this.hide(); //마커에 마우스가 없을땐 팝업이 숨겨집니다.
				    }
			   });
			$("#searchResult").html(innerHtml);
				map.zoomToExtent(markerLayer.getDataExtent());//마커레이어의 영역에 맞게 map을 zoom합니다.
				zoomDepth = map.getZoom();
				
			},
			//요청 실패시 콘솔창에서 에러 내용을 확인할 수 있습니다.
			error:function(request,status,error){
				console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
			}
		});

		
	}

	function onMarkerClick(){

		
	}


	function searchInit(){


		if(confirm("초기화 하시겠습니까?")){

			searchStatus = true;
			//if(markerLayer != null) {
				markerLayer.clearMarkers();
				markerLayer.removeMarker(marker); // 기존 마커 삭제
			//}

			vectorLayer.removeAllFeatures();
			$("#searchKeyword").val('');
			$("#searchResult").html("");

			map.removeAllPopup();
			
			departure = new Object();
			arrival = new Object();
		}
		
	}



	function setLocation(lonlat,lon,lat){


		//markerLayer.removeMarker(marker); // 기존 마커 삭제

		
		map.setCenter(new Tmap.LonLat(lon, lat).transform("EPSG:4326", "EPSG:3857"), zoomDepth);	

		var size = new Tmap.Size(24, 38);//아이콘 크기 설정
		var offset = new Tmap.Pixel(-(size.w / 2), -(size.h));//아이콘 중심점 설정
		var iconD,iconA;

		iconD = new Tmap.Icon('/img/pin_b_m_d.png',size, offset);//마커 아이콘 설정
		iconA = new Tmap.Icon('/img/pin_b_m_a.png',size, offset);//마커 아이콘 설정
		
		
		
		setTimeout(function() {

			if(isEmpty(departure)){
				if(confirm("해당 위치를 출발지로 지정 하시겠습니까?")){

					markerLayer.clearMarkers();
					markerLayer.removeMarker(marker); // 기존 마커 삭제
					
					departure.lon = lon;
					departure.lat = lat;
					iconD = new Tmap.Icon('/img/pin_b_m_d.png',size, offset);//마커 아이콘 설정
					$("#searchKeyword").val("");
					$("#searchResult").html("");
					marker = new Tmap.Marker(new Tmap.LonLat(lon, lat).transform("EPSG:4326", "EPSG:3857"), iconD);//마커 생성
					markerLayer.addMarker(marker);
					
				}else{

				}

				
			}else if(!isEmpty(departure) && isEmpty(arrival)){
				if(confirm("해당 위치를 도착지로 지정 하시겠습니까?")){

					markerLayer.clearMarkers();
					markerLayer.removeMarker(marker); // 기존 마커 삭제

					arrival.lon = lon;
					arrival.lat = lat;

					var s_lonLat = new Tmap.LonLat(departure.lon, departure.lat); //시작 좌표입니다.   
					var e_lonLat = new Tmap.LonLat(arrival.lon, arrival.lat); //도착 좌표입니다.

					var optionObj = {
							reqCoordType:"WGS84GEO", //요청 좌표계 옵셥 설정입니다.
							resCoordType:"EPSG3857",  //응답 좌표계 옵셥 설정입니다.
							trafficInfo:"Y"
			             }
					
					tData.getRoutePlan(s_lonLat, e_lonLat, optionObj);//경로 탐색 데이터를 콜백 함수를 통해 XML로 리턴합니다.
					
					tData.events.register("onComplete", tData, onComplete);//데이터 로드가 성공적으로 완료되었을 때 발생하는 이벤트를 등록합니다.
					tData.events.register("onProgress", tData, onProgress);//데이터 로드중에 발생하는 이벤트를 등록합니다.
					tData.events.register("onError", tData, onError);//데이터 로드가 실패했을 떄 발생하는 이벤트를 등록합니다.
					$("#searchKeyword").val("");
					$("#searchResult").html("");

					
					marker = new Tmap.Marker(new Tmap.LonLat(departure.lon, departure.lat).transform("EPSG:4326", "EPSG:3857"), iconD);//마커 생성
					markerLayer.addMarker(marker);

					marker = new Tmap.Marker(new Tmap.LonLat(arrival.lon, arrival.lat).transform("EPSG:4326", "EPSG:3857"), iconA);//마커 생성
					markerLayer.addMarker(marker);
						
				}else{


				}
				
				
				
			}else if(!isEmpty(departure) && !isEmpty(arrival)){

				searchStatus = true;
				//초기화 하고 이 함수를 다시 호출 한다.
				//searchInit();
				if(markerLayer != null) {
					markerLayer.clearMarkers();
					markerLayer.removeMarker(marker); // 기존 마커 삭제
				}
		
				vectorLayer.removeAllFeatures();
				$("#searchKeyword").val("");
				$("#searchResult").html("");
				departure = new Object();
				arrival = new Object();
				
				setLocation(lonlat,lon,lat);
				
			}


			
		}, 200);






		
		

		
	}



	function drawRoute(departureLonLat,arrivalLonLat){


		markerLayer.clearMarkers();
		markerLayer.removeMarker(marker); // 기존 마커 삭제

		departure.lon = departureLonLat.split(',')[0];
		departure.lat = departureLonLat.split(',')[1];
		arrival.lon = arrivalLonLat.split(',')[0];
		arrival.lat = arrivalLonLat.split(',')[1];
		
		var s_lonLat = new Tmap.LonLat(departure.lon, departure.lat); //시작 좌표입니다.   
		var e_lonLat = new Tmap.LonLat(arrival.lon, arrival.lat); //도착 좌표입니다.

		var optionObj = {
				reqCoordType:"WGS84GEO", //요청 좌표계 옵셥 설정입니다.
				resCoordType:"EPSG3857",  //응답 좌표계 옵셥 설정입니다.
				trafficInfo:"Y",
				searchOption : 4
             }
		
		tData.getRoutePlan(s_lonLat, e_lonLat, optionObj);//경로 탐색 데이터를 콜백 함수를 통해 XML로 리턴합니다.
		
		tData.events.register("onComplete", tData, onComplete);//데이터 로드가 성공적으로 완료되었을 때 발생하는 이벤트를 등록합니다.
		tData.events.register("onProgress", tData, onProgress);//데이터 로드중에 발생하는 이벤트를 등록합니다.
		tData.events.register("onError", tData, onError);//데이터 로드가 실패했을 떄 발생하는 이벤트를 등록합니다.
		
		marker = new Tmap.Marker(new Tmap.LonLat(departure.lon, departure.lat).transform("EPSG:4326", "EPSG:3857"), iconD);//마커 생성
		markerLayer.addMarker(marker);

		marker = new Tmap.Marker(new Tmap.LonLat(arrival.lon, arrival.lat).transform("EPSG:4326", "EPSG:3857"), iconA);//마커 생성
		markerLayer.addMarker(marker);

	}









	
	function onComplete(){


		searchStatus = false;
		
		
		console.log(this.responseXML); 
	  
	  var totalDistance = this.responseXML.getElementsByTagName("tmap:totalDistance")[0].firstChild.data;
	  var totalTime = this.responseXML.getElementsByTagName("tmap:totalTime")[0].firstChild.data;

	  var hour = parseInt(totalTime/3600);
	  var min = parseInt((totalTime%3600)/60);
	  var sec = totalTime%60;

	  var test = "";
		if(hour > 0){
			test += hour + "시간"; 
		}
		if(min > 0){
			test += min + "분"; 
		}
		if(sec > 0){
			test += sec + "초"; 
		}

		totalDistance = totalDistance/1000;
		//alert(totalDistance);
		//alert(totalTime);
	  
	  var trafficColors = {
				extractStyles:true,
				trafficDefaultColor:"#000000", //교통 정보가 없을 때
				trafficType1Color:"#009900", //원할
				trafficType2Color:"#8E8111", //지체
				trafficType3Color:"#FF0000"  //정체

			};

		var kmlForm = new Tmap.Format.KML(trafficColors).readTraffic(this.responseXML);
		vectorLayer.addFeatures(kmlForm);
			  
		map.zoomToExtent(vectorLayer.getDataExtent());

		var center = map.getCenter().transform(PR_3857,PR_4326 );


		
		var content ="<div style=' position: relative; border-bottom: 1px solid #dcdcdc; line-height: 18px; padding: 0 35px 2px 0;'>"+
	  "<div style='font-size: 12px; line-height: 15px;'>"+
	      "<span style='display: inline-block; width: 14px; height: 14px; vertical-align: middle; margin-right: 5px;'>경로 정보</span>"+
	  "</div>"+
	"</div>"+
	"<div style='position: relative; padding-top: 5px; display:inline-block'>"+
	  "<div style='display:inline-block; margin-left:5px; vertical-align: top;'>"+
	  	"<span style='font-size: 12px; margin-left:2px; margin-bottom:2px; display:block;'>총 거리 : "+totalDistance.toFixed(1)+"km</span>"+
	  	/* "<span style='font-size: 12px; color:#888; margin-left:2px; margin-bottom:2px; display:block;'>(우)100-999  (지번)을지로2가 11</span>"+ */
	  	/* "<span style='font-size: 12px; margin-left:2px;'><a href='https://developers.sktelecom.com/' target='blank'>개발자센터</a></span>"+ */
	  	"<span style='font-size: 12px; margin-left:2px;'> 총 소요시간 : "+test+"</span>"+
	  "</div>"+
	"</div>";
		popup = new Tmap.Popup("p1",
		                new Tmap.LonLat(center.lon, center.lat).transform("EPSG:4326", "EPSG:3857"),//Popup 이 표출될 맵 좌표
		                new Tmap.Size(180, 50),//Popup 크기
		                content,//Popup 표시될 text
		                //onPopupClose//close클릭시 실행할 콜백 함수
		                false
		                );
		popup.setBorder("1px solid #8d8d8d");//popup border 조절
		popup.autoSize=true;//popup 사이즈 자동 조절		                         
		//map.addPopup(popup);//map에 popup 추가

		homeLoader.hide();

		
	 }



	function onPopupClose(evt) {
		//select.unselectAll();
	}


	function onProgress(){

	}

	function onError(){
		alert("onError");
	}





	function isEmpty(obj) {

	  for(var prop in obj){
	       if(obj.hasOwnProperty(prop)){
	      	 return false;
	       }
	  }
	  return true;
	}





	var PR_3857 = new Tmap.Projection("EPSG:3857");		// Google Mercator 좌표계인 EPSG:3857
	var PR_4326 = new Tmap.Projection("EPSG:4326");	

	function get4326LonLat(x, y){
		return new Tmap.LonLat(x, y).transform(PR_3857, PR_4326);
	}

	function get3857LonLat(x, y){
		return new Tmap.LonLat(x, y).transform(PR_4326, PR_3857);
	}






	//검색어 자동완성
	function searchMenuTitle(searchKeyword) {
		var searchCategory = ''; // 검색범위(Web, WebV2 Android, iOS, WebService)

		if (searchKeyword && searchKeyword != '') {
			searchCategory = $('#side_search .dropdown_wrapper a').html()
					.trim(); // 검색범위(Web, WebV2, Android, iOS, WebService)

			// 검색창에 검색 범위목록 복제
			$('#search_result dl').html(
					$('#tit_' + searchCategory.replace(" ", "_"))
							.siblings('ul').clone().addClass('filetree')
							.wrapAll("<div/>").parent().html());
			$('#search_result ul, #search_result li').css('display', 'none');

			// 검색어가 존재하는 항목만 표시
			$('#search_result a').each(
					function() {
						var aTagText = $(this).text();
						if (aTagText.toLowerCase().indexOf(
								searchKeyword.toLowerCase()) != -1) {
							$(this).parents('ul, li').css('display', 'block');
						}
					});
			$('#search_result').show();
		} else {
			// 자동 검색창 숨기기
			$('#search_result').hide();
			$('#search_result dl').html("");
		}
	}

	//Index 검색결과 페이지 메인에 로드
	function loadSearchIndex(searchKeyword) {
		var searchCategory = '';

		var resultHtml = '';

		if (searchKeyword && searchKeyword != '') {
			searchCategory = $('#side_search .dropdown_wrapper a').html()
					.trim(); // 검색범위(Web, WebV2 Android, iOS, WebService)

			setSelectedMenuCSS('search_result');
			$('#contents')
					.load(
							'/search_result.html',
							function(response, status, xhr) {
								var resultLinkGuide = '';
								var resultLinkSample = '';
								var resultLinkDocs = '';
								var resultLinkUsecase = '';

								var resultCnt = 0;
								var eachCnt = 0;
								if (status == "success") {
									// 메인 페이지 로드 성공 시 작업

									// 검색 페이지 로드시 Anchor 가 변하지 않아 임의로 페이지 새로고침 필요
									mIsNeedRefresh = true;

									// 카테고리 표시
									$('#divIndexCategory').html(searchCategory);
									$('#divIndexCategory').addClass(
											searchCategory.replace(/\s/gi, ""));

									// 각 메뉴 복제하여 로드
									$('#indexResultGuide')
											.html(
													$(
															'#tit_'
																	+ searchCategory
																			.replace(
																					" ",
																					"_"))
															.siblings('ul')
															.find(
																	'li > span:contains("Guide")')
															.siblings('ul')
															.clone().addClass(
																	'filetree')
															.wrapAll("<div/>")
															.parent().html());
									$('#indexResultSample')
											.html(
													$(
															'#tit_'
																	+ searchCategory
																			.replace(
																					" ",
																					"_"))
															.siblings('ul')
															.find(
																	'li > span:contains("Sample")')
															.siblings('ul')
															.clone().addClass(
																	'filetree')
															.wrapAll("<div/>")
															.parent().html());
									$('#indexResultDocs')
											.html(
													$(
															'#tit_'
																	+ searchCategory
																			.replace(
																					" ",
																					"_"))
															.siblings('ul')
															.find(
																	'li > span:contains("Docs")')
															.siblings('ul')
															.clone().addClass(
																	'filetree')
															.wrapAll("<div/>")
															.parent().html());
									$('#indexResultUsecase')
											.html(
													$(
															'#tit_'
																	+ searchCategory
																			.replace(
																					" ",
																					"_"))
															.siblings('ul')
															.find(
																	'li > span:contains("Use case")')
															.siblings('ul')
															.clone().addClass(
																	'filetree')
															.wrapAll("<div/>")
															.parent().html());
									$('#tbodyResult ul, #tbodyResult li').css(
											'display', 'none');

									// Guide 검색 결과 생성
									eachCnt = 0;
									$('#indexResultGuide a')
											.each(
													function() {
														var aTagText = $(this)
																.text();
														if (aTagText
																.toLowerCase()
																.indexOf(
																		searchKeyword
																				.toLowerCase()) != -1) {
															//resultHtml += '<dd>' + $(this).parent().html() + '</dd>';
															$(this)
																	.parents(
																			'ul, li')
																	.css(
																			'display',
																			'block');
															resultCnt++;
															eachCnt++;
														}
													});
									if (eachCnt == 0) {
										$('#search_guide').css('display',
												'none');
										$('#search_guide_list').css('display',
												'none');
									}

									// Sample 검색 결과 생성
									eachCnt = 0;
									$('#indexResultSample a')
											.each(
													function() {
														var aTagText = $(this)
																.text();
														if (aTagText
																.toLowerCase()
																.indexOf(
																		searchKeyword
																				.toLowerCase()) != -1) {
															//resultHtml += '<dd>' + $(this).parent().html() + '</dd>';
															$(this)
																	.parents(
																			'ul, li')
																	.css(
																			'display',
																			'block');
															resultCnt++;
															eachCnt++;
														}
													});
									if (eachCnt == 0) {
										$('#search_sample').css('display',
												'none');
										$('#search_sample_list').css('display',
												'none');
									}

									// Docs 검색 결과 생성
									eachCnt = 0;
									$('#indexResultDocs a')
											.each(
													function() {
														var aTagText = $(this)
																.text();
														if (aTagText
																.toLowerCase()
																.indexOf(
																		searchKeyword
																				.toLowerCase()) != -1) {
															//resultHtml += '<dd>' + $(this).parent().html() + '</dd>';
															$(this)
																	.parents(
																			'ul, li')
																	.css(
																			'display',
																			'block');
															resultCnt++;
															eachCnt++;
														}
													});
									if (eachCnt == 0) {
										$('#search_docs')
												.css('display', 'none');
										$('#search_docs_list').css('display',
												'none');
									}

									// Use case 검색 결과 생성
									if (searchCategory == "Web"
											|| searchCategory == "WebV2") {
										eachCnt = 0;
										$('#indexResultUsecase a')
												.each(
														function() {
															var aTagText = $(
																	this)
																	.text();
															if (aTagText
																	.toLowerCase()
																	.indexOf(
																			searchKeyword
																					.toLowerCase()) != -1) {
																//resultHtml += '<dd>' + $(this).parent().html() + '</dd>';
																$(this)
																		.parents(
																				'ul, li')
																		.css(
																				'display',
																				'block');
																resultCnt++;
																eachCnt++;
															}
														});
										if (eachCnt == 0) {
											$('#search_usecase').css('display',
													'none');
											$('#search_usecase_list').css(
													'display', 'none');
										}
									}

									if (resultCnt > 0) {
										// 검색결과가 존재할 경우
										// Index 결과검색 테마 적용
										$('#tbodyResult a').addClass(
												'h_underline');
										$('#tbodyResult a')
												.each(
														function() {
															$(this)
																	.html(
																			'· <span>'
																					+ $(
																							this)
																							.html()
																					+ '</span>');
														});
									} else {
										$('#tbodyResult')
												.html(
														'<td style="padding:100px 20px">검색 결과가 없습니다.</td>');
									}

									// 스크롤 맨 위로 이동
									$('main').animate({
										scrollTop : 0
									}, 100);
								}
							});
		}
	}

	//메인페이지 로드 함수
	function loadMainContents(urlAnchorString) {
		var urlString = null; // (URL?Prameter) : .html 없음
		var urlWithHtml = null; // 메인 페이지에 띄울 실제 요청 주소
		var anchorIndex = -1; // 앵커 인덱스
		var anchorString = null; // 앵커 스트링

		// 자동 검색창 초기화 - START
		$('#search_keyword').val('');
		$('#search_result').hide();
		$('#search_result dl').html("");
		// 자동 검색창 초기화 - END

		if (urlAnchorString) {
			// URL 과 책갈피 정보 파싱 - START
			anchorIndex = urlAnchorString.indexOf('.'); // 페이지 URL 과 책갈피 정보는 "." 으로  구분됨
			if (anchorIndex != -1) {
				// 책갈피 정보가 있는 경우
				urlString = urlAnchorString.substring(0, anchorIndex);
				anchorString = urlAnchorString.substring(anchorIndex + 1,
						urlAnchorString.length);
			} else {
				// 책갈피 정보가 없는 경우
				urlString = urlAnchorString;
			}
			// URL 과 책갈피 정보 파싱 - END

			if (mIsNeedRefresh || mLastUrlString != urlString) {
				// 새로고침이 필요하거나 새로운 URL 일 경우 작업

				// 새로고침 필요여부 초기화
				mIsNeedRefresh = false;

				// 실제 요청 주소 만들기
				if (urlString.indexOf('?') != -1) {
					// 파라미터가 있을 경우
					urlWithHtml = "/"
							+ urlString.substring(0, urlString.indexOf('?'))
							+ ".html"
							+ urlString.substring(urlString.indexOf('?'),
									urlString.length); // ?(파라미터 구분자) 앞에 .html 끼워 넣음
				} else {
					// 파라미터가 없을 경우
					urlWithHtml = "/" + urlString + ".html";
				}

				$('#contents')
						.load(
								urlWithHtml,
								function(response, status, xhr) {
									if (status == "success") {
										// 메인 페이지 로드 성공 시 작업

										// 최근 페이지 URL 저장( 중복 로드 방지 )
										mLastUrlString = urlString;

										// 선택된 메뉴 표시
										setSelectedMenuCSS(urlString,
												anchorString);

										// 책갈피 위치로 이동
										if (anchorString) {
											$.moveScrollById(anchorString);
										} else {
											$('#contents.main').animate({
												scrollTop : 0
											}, 500, 'swing', function() {
												$('#top_btn').fadeOut();
											});
										}
									} else {
										// 기본 페이지 로드
										location.href = "/main.html#web/sample/TotalSample";
										// 			            setSelectedMenuCSS('web/sample/TotalSample');
										// 			            $('#contents').load('/web/sample/TotalSample.html');
									}
								});
			} else {
				// 기존 URL 일 경우 작업

				// 선택된 메뉴 표시
				setSelectedMenuCSS(urlString, anchorString);

				// 책갈피 위치로 이동
				if (anchorString) {
					$.moveScrollById(anchorString);
				}
			}
		}
	}

	//좌측 메뉴 선택된 항목 설정
	function setSelectedMenuCSS(urlString, anchorString) {
		var htmlIndex = -1;
		var menuId = '';

		if (urlString) {

			// 메뉴 아이디 구하기
			if (urlString.indexOf("?") != -1) {
				// 파라미터가 있을 경우
				menuId = urlString.substring(0, urlString.indexOf("?"))
						.replace(/\//gi, "_"); // 슬레시를 언더바로 치환
			} else {
				// 파라미터가 없을 경우
				menuId = urlString.replace(/\//gi, "_"); // 슬레시를 언더바로 치환
			}

			// 앵커가 존재한다면 메뉴 아이디 뒤에 붙임
			if (anchorString) {
				menuId = menuId + "_" + anchorString; // anchor 에서 # 제거하고 메뉴 아이디 뒤에 붙이기
			}

			$('#' + menuId).closest('li').parents('li').removeClass(
					'expandable');
			$('#' + menuId).closest('li').parents('li').addClass('collapsable');

			$('#' + menuId).parents('ul').css('display', 'block');
			$('#' + menuId).parents('ul').each(function(index, element) {
				$(element).siblings('div').removeClass('expandable-hitarea');
				$(element).siblings('div').addClass('collapsable-hitarea');
			});

			$('#tree_wrap a').removeClass("on");
			$('#' + menuId).addClass("on");

			//메뉴 스크롤 이동 (스크롤이 펼쳐지는 시간이 클라이언트마다 다르기에 1초 후 작동)
			setTimeout(function() {
				$("#tree_wrap").mCustomScrollbar("scrollTo", '#' + menuId);
			}, 1000);

		}
	}

	//Html id 로 위치를 찾아서 스크롤
	$.moveScrollById = function(id) {
		setTimeout(function() {
			var position = $('#' + id).offset(); // 위치값

			if (position) {
				$('main').animate({
					scrollTop : (position.top + $('main').scrollTop())
				}, 100); // 이동
			}
		}, 300);//처음 로딩시 offset의 위치값이 달라지므로 0.3초 딜레이
	}

	//URL 에서 앵커 추출
	$.urlAnchor = function() {
		var sharpIndex = window.location.href.indexOf('#');
		var result = null;
		if (sharpIndex != -1) {
			result = window.location.href.substring(sharpIndex + 1,
					window.location.href.length);
		}
		if (result == null) {
			return null;
		} else {
			return result || '';
		}
	}

		
	
    </script>
    </body>
</html>
