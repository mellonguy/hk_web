$(document).ready(function(){
    var banner1 = $('.slider-banner-1');
    var banner2 = $('.slider-banner-2');
    var marketingSLider = $('.marketing-items-slider');
    var investmetnSlider = $('#investmentSlider');
    var bestCertShot = $('.bestCertShot');
    tabsOpener($('.participate-tabs-control ul li.active a').data('target'));
    $('.marketing-nav a').on('click',function(e){
        e.stopPropagation();
        e.preventDefault();
        marketingSLider.trigger('next.owl.carousel');
    });
    
    var location = document.location.href;
    if(!(location.indexOf("nopermission") > -1)){
    	banner1.owlCarousel({
            loop:true,
            margin:0,
            dots: true,
            items:1,
            startPosition: 2,
            smartSpeed: 800,
             autoplay: true,
             autoplayTimeout: 5000,
             autoplaySpeed: 1200
        });
        banner2.owlCarousel({
            loop:true,
            margin:0,
            dots: true,
            items:1,
            startPosition: 2,
            smartSpeed: 800,
             autoplay: true,
             autoplayTimeout: 3500,
             autoplaySpeed: 1200
        });
        marketingSLider.owlCarousel({
            loop:true,
            margin:0,
            dots: false,
            items:1,
            smartSpeed: 800
        });
        bestCertShot.owlCarousel({
            loop:true,
            dots: true,
            items:3,
            smartSpeed: 800,
            responsive : {
                0 : {
                    items : 1
                },
                768 : {
                	items : 3
                },
                1201: {
                	items: 4
                }
            }
        });
        investmetnSlider.owlCarousel({
            loop:false,
            margin:52,
            dots: true,
            items:3,
            smartSpeed: 800,
            responsive : {
                0 : {
                    items : 1
                },
                768 : {
                	items : 3
                }
            }
        });
    }
    

    $('.selectedItem select').on('change',function(){
       //var selVal = $(this).text();
    	var selVal = $(this).find("option:selected").text();
       $(this).closest('.cus-select').find('span').text(selVal);
    });
//    $('.selectOption li').on('click',function(){
//        var myText = $(this).text();
//        console.log(myText);
//        
//        $(this).parents().eq(2).find('.selectedItem').text(myText)
//        $(this).parent().fadeOut();
//    })
    $('#upload').on('change',function(){
        var myPath = $(this).val();
        var myFileName  = myPath.replace(/^.*\\/, "");
        $(this).parent().find('.fileName').text(myFileName);
    })
    $('.archive-page-halloffame-slider-item-header a.open-pop').on('click', function(e){
        e.stopPropagation();
        e.preventDefault();
        $(this).closest('.menu-button').find('.slide-popover').addClass('active');
    });
    $(document).mouseup(function(e) 
    {
        var container = $(".slide-popover");
        var menCont = $(".content-box .left-content");

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0) 
        {
            container.removeClass('active');
        }
        
        if (!menCont.is(e.target) && menCont.has(e.target).length === 0 && e.target.id != "mobMenu") 
        {
        	if(checkIfMobile() <= 1200){
        		menCont.removeClass('active');
            	$('#mobMenu').removeClass('open');
            	$('#mobMenu').attr('data-menu-status', 'close');
        	}
        	
        }
    });
    $('.participate-tabs-control ul li a').on('click', function(e){
        e.stopPropagation();
        e.preventDefault();
        var target = $(this).data('target');
        $('.participate-tabs-control ul li').removeClass('active');
        $(this).closest('li').addClass('active');
        tabsOpener(target);
    })
    // $('.gotoMain2nd .tableList tbody tr').on('click',function(){
    //     var myLink = $(this).find('a').attr('href');
    //    window.location.href = myLink;
    // })
    // $('.showRankingDetails tr').on('click',function(){
    //     $('.gamingDetails').fadeOut();
    //     $('.rankingDetails').fadeIn();
    // });
    // $('.showGamingDetails tr').on('click',function(){
    //     $('.gamingDetails').fadeIn();
    //     $('.rankingDetails').fadeOut();
    // });

    $('.showDetails .tableList tbody tr').on('click',function(){
        var myDetails = $(this).attr('id');
        $('.viewDetailsBox .rows').stop().fadeOut('slow');
        $("."+myDetails).stop().fadeIn('slow');
    })
    $('.faqBox').on('click',function(){
        var state = $(this).attr('data-state');
        if(state == 'open'){
            $(this).attr('data-state', 'close');
            $(this).find('.answerBox').stop().slideUp();
        }else{
            $(this).attr('data-state', 'open');
            $(this).closest('.faqList').find('.faqBox[data-state="open"] .answerBox').not(this).stop().slideUp();
            $(this).closest('.faqList').find('.faqBox[data-state="open"]').not(this).attr('data-state', 'close');
            $(this).find('.answerBox').stop().slideDown();
        }
    });
    $('.burger').on('click', function(){
    	var menuStatus = $(this).attr('data-menu-status');
    	if (menuStatus == 'close') {
            $(this).addClass('open');
            $(this).attr('data-menu-status', 'open');
            $(".content-box .left-content").addClass("active");
        } else {
            $(this).removeClass('open');
            $(this).attr('data-menu-status', 'close');
            $(".content-box .left-content").removeClass("active");
        }
    });
});

function checkIfMobile(){
	var wWidth = $(window).width();
	return wWidth;
}
function tabsOpener(target){
    $('.participate-tabs-content .participate-tabs-content-item').removeClass('active');
    $('.participate-tabs-content .participate-tabs-content-item[data-identity='+target+']').addClass('active');
}
function basename(path) {
    return path.replace(/\\/g,'/').replace( /.*\//, '' );
}

function jsDayCheck(strValue)
{
    
    var chk1 = /^(19|20)\d{2}-([1-9]|1[012])-([1-9]|[12][0-9]|3[01])$/;
    var chk2 = /^(19|20)\d{2}\/([0][1-9]|1[012])\/(0[1-9]|[12][0-9]|3[01])$/;

    //-------------------------------------------------------------------------------
    // 존재하는 날자(유효한 날자) 인지 체크
    //-------------------------------------------------------------------------------
    var bDateCheck = true;
    var arrDate = strValue.split("-");
    var nYear = Number(arrDate[0]);
    var nMonth = Number(arrDate[1]);
    var nDay = Number(arrDate[2]);

    if (nYear < 1900 || nYear > 3000)
    { // 사용가능 하지 않은 년도 체크
        bDateCheck = false;
    }

    if (nMonth < 1 || nMonth > 12)
    { // 사용가능 하지 않은 달 체크
        bDateCheck = false;
    }

    // 해당달의 마지막 일자 구하기
    var nMaxDay = new Date(new Date(nYear, nMonth, 1) - 86400000).getDate();
    if (nDay < 1 || nDay > nMaxDay)
    { // 사용가능 하지 않은 날자 체크
        bDateCheck = false;
    }

    if(bDateCheck == false) 
    { 
       alert("존재하지 않은 년월일을 입력하셨습니다. 다시한번 확인해주세요");
       return bDateCheck;
    }else{
    	return bDateCheck;
    }
}














