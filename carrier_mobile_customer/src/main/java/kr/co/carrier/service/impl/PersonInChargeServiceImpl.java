package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.PersonInChargeMapper;
import kr.co.carrier.service.PersonInChargeService;
import kr.co.carrier.vo.CustomerPersonInChargeVO;

@Service("personInChargeService")
public class PersonInChargeServiceImpl implements PersonInChargeService{

	
	@Resource(name="personInChargeMapper")
	private PersonInChargeMapper personInchargeMapper;
	
	
	public Map<String, Object> selectPersonInCharge(Map<String, Object> map) throws Exception{
		return personInchargeMapper.selectPersonInCharge(map);
	}
	
	public List<Map<String, Object>> selectPersonInChargeList(Map<String, Object> map) throws Exception{
		return personInchargeMapper.selectPersonInChargeList(map);
	}
	
	public int selectPersonInChargeListCount(Map<String, Object> map) throws Exception{
		return personInchargeMapper.selectPersonInChargeListCount(map);
	}
	
	public int insertPersonInCharge(CustomerPersonInChargeVO customerPersonInChargeVO) throws Exception{
		return personInchargeMapper.insertPersonInCharge(customerPersonInChargeVO);
	}
	
	public void updatePersonInCharge(CustomerPersonInChargeVO customerPersonInChargeVO) throws Exception{
		personInchargeMapper.updatePersonInCharge(customerPersonInChargeVO);
	}
	
	public void deletePersonInCharge(Map<String, Object> map) throws Exception{
		personInchargeMapper.deletePersonInCharge(map);
	}
	
	public Map<String, Object> selectPersonInChargeByPersonInChargeId(Map<String, Object> map) throws Exception{
		return personInchargeMapper.selectPersonInChargeByPersonInChargeId(map);
	}
	
}
