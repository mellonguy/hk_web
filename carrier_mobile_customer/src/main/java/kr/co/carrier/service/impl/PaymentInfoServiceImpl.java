package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.PaymentInfoMapper;
import kr.co.carrier.service.PaymentInfoService;
import kr.co.carrier.vo.PaymentInfoVO;

@Service("paymentInfoService")
public class PaymentInfoServiceImpl implements PaymentInfoService {

	
	@Resource(name="paymentInfoMapper")
	private PaymentInfoMapper paymentInfoMapper;
	
	
	
	public List<Map<String, Object>> selectPaymentInfoList(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.selectPaymentInfoList(map);
	}
	
	public int insertPaymentInfo(PaymentInfoVO paymentInfoVO) throws Exception{
		return paymentInfoMapper.insertPaymentInfo(paymentInfoVO);
	}
	
	public void deletePaymentInfo(Map<String, Object> map) throws Exception{
		paymentInfoMapper.deletePaymentInfo(map);
	}
	
	public List<Map<String, Object>> selectAnotherCalInfo(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.selectAnotherCalInfo(map);
	}
	
	public Map<String, Object> selectAnotherCalDDInfo(Map<String, Object> map) throws Exception{
		return paymentInfoMapper.selectAnotherCalDDInfo(map);
	}
	
	public void updatePaymentInfoDecideStatus(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updatePaymentInfoDecideStatus(map);
	}
	
	public void updatePaymentInfoForDDFinish(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updatePaymentInfoForDDFinish(map);
	}
	
	public void updatePaymentInfoDebtorCreditorId(Map<String, Object> map) throws Exception{
		paymentInfoMapper.updatePaymentInfoDebtorCreditorId(map);
	}
	
	
	
	
}
