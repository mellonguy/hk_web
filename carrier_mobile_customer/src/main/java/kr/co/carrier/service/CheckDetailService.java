package kr.co.carrier.service;

import java.util.Map;

import kr.co.carrier.vo.CarCheckDetailVO;

public interface CheckDetailService {

	
	
	public Map<String, Object> selectCheckDetail(Map<String, Object> map) throws Exception; 
	public void insertCheckDetail(CarCheckDetailVO carCheckDetailVO) throws Exception;
	public void updateCheckDetail(CarCheckDetailVO carCheckDetailVO) throws Exception;
	
	
	
}
