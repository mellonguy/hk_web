package kr.co.carrier.service;

import java.util.Map;

public interface AlarmTalkService {

	
	public String alarmTalkSend(Map<String, Object> map)throws Exception;
	public String smsSend(Map<String, Object> map)throws Exception;
	public String yun(Map<String, Object> allocationMap,Map<String, Object> map)throws Exception;	
}
