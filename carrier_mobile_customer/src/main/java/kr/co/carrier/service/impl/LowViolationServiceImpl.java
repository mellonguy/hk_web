package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.LowViolationMapper;
import kr.co.carrier.service.LowViolationService;
import kr.co.carrier.vo.LowViolationVO;

@Service("lowViolationService")
public class LowViolationServiceImpl implements LowViolationService{

	
	
	@Resource(name="lowViolationMapper")
	private LowViolationMapper lowViolationMapper;
	
	
	public Map<String, Object> selectLowViolation(Map<String, Object> map) throws Exception{
		return lowViolationMapper.selectLowViolation(map);
	}
	
	public List<Map<String, Object>> selectDriverList(Map<String, Object> map) throws Exception{
		return lowViolationMapper.selectDriverList(map);
	}
	
	public int selectDriverListCount(Map<String, Object> map) throws Exception{
		return lowViolationMapper.selectDriverListCount(map);
	}
	
	public List<Map<String, Object>> selectLowViolationList(Map<String, Object> map) throws Exception{
		return lowViolationMapper.selectLowViolationList(map);
	}
	
	public int selectLowViolationListCount(Map<String, Object> map) throws Exception{
		return lowViolationMapper.selectLowViolationListCount(map);
	}
	
	public int insertLowViolation(LowViolationVO lowViolationVO) throws Exception{
		return lowViolationMapper.insertLowViolation(lowViolationVO);
	}
	
	public void deleteLowViolation(Map<String, Object> map) throws Exception{
		lowViolationMapper.deleteLowViolation(map);
	}
	
	public void updateLowViolationReadYn(Map<String, Object> map) throws Exception{
		lowViolationMapper.updateLowViolationReadYn(map);
	}
	
	public void updateLowViolationPaymentYn(Map<String, Object> map) throws Exception{
		lowViolationMapper.updateLowViolationPaymentYn(map);
	}
	
	
	
	
}
