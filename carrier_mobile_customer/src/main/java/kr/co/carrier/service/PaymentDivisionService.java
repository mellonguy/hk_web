package kr.co.carrier.service;

import java.util.List;
import java.util.Map;

public interface PaymentDivisionService {

	
	public List<Map<String, Object>>selectPaymentDivisionInfoList() throws Exception;
	
	
}
