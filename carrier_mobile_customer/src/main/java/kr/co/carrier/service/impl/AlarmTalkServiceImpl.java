package kr.co.carrier.service.impl;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.swing.InputMap;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.AllocationFileMapper;
import kr.co.carrier.service.AlarmTalkService;
import kr.co.carrier.service.AllocationFileService;
import kr.co.carrier.service.AllocationService;
import kr.co.carrier.service.CustomerService;
import kr.co.carrier.service.PersonInChargeService;
import kr.co.carrier.service.SendSmsService;
import kr.co.carrier.utils.BaseAppConstants;
import kr.co.carrier.utils.MultipartUtility;
import kr.co.carrier.utils.WebUtils;
import kr.co.carrier.vo.SendSmsVO;

@Service("alarmTalkService")
public class AlarmTalkServiceImpl implements AlarmTalkService{

	@Value("#{appProp['upload.base.path']}")
    private String rootDir;    
    private String subDir = "bbs";	
	
    private static String histUrl; // sms/alarmTalk url
    
    @Value("#{appProp['hist.url']}")
    public void setKey(String value) {
      histUrl = value;
    }
	
	@Autowired
	private CustomerService customerService;
	
	
	@Autowired
	private SendSmsService sendSmsService;
	
	@Autowired
	private AllocationFileService allocationFileService;
	
	@Autowired
	private AllocationService allocationService;

	@Autowired
	private AllocationFileMapper allocationFileMapper;
	
	@Autowired
	private PersonInChargeService personInChargeService; 
	
	
	public String alarmTalkSend(Map<String, Object> map)throws Exception{
        
        
		StringBuilder urlBuilder = new StringBuilder(histUrl+"/sms/histmsg");
		StringBuilder urlBuilderFT = new StringBuilder(histUrl+"/sms/histmsg");
		String result = "";
		String status = "AT";	
		String alarmTalkSendResult ="";
		  
		
		try {
		
		
			String resultMsg = "";
			//알림톡 발송 구분		1:기사배정,2:상차완료,3:하차완료,4:인계완료,5:탁송완료,6탁송취소,7:배차정보(기사님에게),8:배차승인(배차담당직원)
			
			urlBuilder.append("?");
			urlBuilder.append(URLEncoder.encode("uid","UTF-8") + "=" + URLEncoder.encode(BaseAppConstants.COMPANY_TYPE_NATIONAL, "UTF-8") + "&");
			
			
			if(map.get("gubun").toString().equals("1")) {			//기사배정
	
				resultMsg += "차종 : ";
				resultMsg += map.get("car_kind") != null && !map.get("car_kind").toString().equals("") ? map.get("car_kind").toString():"미정";
				resultMsg += "\r\n";
				resultMsg += "차대번호 : ";
				resultMsg += map.get("car_id_num") != null && !map.get("car_id_num").toString().equals("") ?  map.get("car_id_num").toString():"미정";
				resultMsg += "\r\n";
				resultMsg += "출발일 : "+ map.get("departure_dt").toString()+"\r\n";		
				resultMsg += "하차지 : "+ map.get("arrival").toString()+"\r\n";
				resultMsg += "기사명 : "+ map.get("driver_name").toString()+"\r\n";
				resultMsg += "연락처 : "+ map.get("phone_num").toString()+"\r\n";
				resultMsg += "해당 차량이 한국카캐리어 "+ map.get("driver_name").toString()+"기사님께 배정 되었습니다."+"\r\n";
				resultMsg += "";
				
				urlBuilder.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode(resultMsg, "UTF-8") + "&");
				urlBuilder.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
				urlBuilder.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_000", "UTF-8") + "&");
				//hkc_000
			}else if(map.get("gubun").toString().equals("2")) {	//상차완료
				
				resultMsg += "차종 : ";
				resultMsg += map.get("car_kind") != null && !map.get("car_kind").toString().equals("") ? map.get("car_kind").toString():"미정";
				resultMsg += "\r\n";
				resultMsg += "차대번호 : ";
				resultMsg += map.get("car_id_num") != null && !map.get("car_id_num").toString().equals("") ?  map.get("car_id_num").toString():"미정";
				resultMsg += "\r\n";		
				resultMsg += "하차지 : "+ map.get("arrival").toString()+"\r\n";
				resultMsg += "고객님의 차량이 안전하게 상차 되었습니다.";
				urlBuilder.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode(resultMsg, "UTF-8") + "&");
				urlBuilder.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("AT", "UTF-8") + "&");
				
				//차종,차대번호, 하차지 정보를 알림톡 발송시 내용에 추가(차량이 많은 경우 확인 할 수 있도록 함)
				urlBuilder.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_009", "UTF-8") + "&");
				
				//hkc_001
			}else if(map.get("gubun").toString().equals("3")) {	//하차완료
				
				/*urlBuilder.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode( resultMsg+"문서 알림.\r\n 도착하였습니다.\r\n결재/합의 바랍니다.", "UTF-8") + "&");
				urlBuilder.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
				urlBuilder.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hanway01_2", "UTF-8") + "&");*/
			}else if(map.get("gubun").toString().equals("4")) {	//인계완료
				
				/*urlBuilder.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode( resultMsg+"문서 알림.\r\n 도착하였습니다.\r\n결재/합의 바랍니다.", "UTF-8") + "&");
				urlBuilder.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
				urlBuilder.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hanway01_2", "UTF-8") + "&");*/
			}else if(map.get("gubun").toString().equals("5")) {	//탁송완료
				
				
				resultMsg += "차종 : ";
				resultMsg += map.get("car_kind") != null && !map.get("car_kind").toString().equals("") ? map.get("car_kind").toString():"미정";
				resultMsg += "\r\n";
				resultMsg += "차대번호 : ";
				resultMsg += map.get("car_id_num") != null && !map.get("car_id_num").toString().equals("") ?  map.get("car_id_num").toString():"미정";
				resultMsg += "\r\n";
				//resultMsg += "출발일 : "+ map.get("departure_dt").toString()+"\r\n";		
				resultMsg += "하차지 : "+ map.get("arrival").toString()+"\r\n";
				
				
				
				resultMsg += "차량이 고객님께 안전하게 전달 되었습니다."+"\r\n";
				resultMsg += "저희 한국카캐리어를 이용해 주셔서 감사합니다."+"\r\n";
				resultMsg += "많은 이용 부탁 드립니다."+"\r\n";
				if(map.get("send_account_info_yn").toString().equals("Y")){
					if(map.get("payment_kind").toString().equals("DD")) {
						resultMsg += "탁송료는 기사님께 지급 부탁 드립니다.";
					}else {
						if(map.get("payment_kind").toString().equals("CA") && map.get("billing_division").toString().equals("00")) {
							if(map.get("customer_kind").toString().equals("00")) {
								resultMsg += "입금계좌 : 1005-804-185508\r\n";
								resultMsg += "우리은행 (주)엔씨로직스\r\n";	
							/*}else if(map.get("customer_kind").toString().equals("01")) {
								resultMsg += "입금계좌 : 356-1328-4613-53\r\n";
								resultMsg += "농협 임경숙\r\n";	
							}*/
							}else if(map.get("customer_kind").toString().equals("01")) {
								resultMsg += "입금계좌 : 1005-804-185508\r\n";
								resultMsg += "우리은행 (주)엔씨로직스\r\n";	
							}
						}
					}	
				}
				urlBuilder.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode( resultMsg, "UTF-8") + "&");
				urlBuilder.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
				urlBuilder.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_006", "UTF-8") + "&");
				//hkc_002
			}else if(map.get("gubun").toString().equals("6")) {	//탁송취소
				/*#{고객명}님이 요청하신
				차종 : #{차종}
				하차지 :#{시도 시군구} 
				탁송이 취소 되었습니다.*/
				urlBuilder.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode( resultMsg+"문서 알림.\r\n 도착하였습니다.\r\n결재/합의 바랍니다.", "UTF-8") + "&");
				urlBuilder.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
				urlBuilder.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_005", "UTF-8") + "&");
				//hkc_005
			}else if(map.get("gubun").toString().equals("7")) {	//배차정보
				/*#{기사명} 기사님께 배차 정보가 도착 했습니다.
				상차지 : #{시도 시군구}
				 상차지담당자명 : #{이름}
				 담당자연락처 : #{전화번호}
				하차지 : #{시도 시군구}
				하차지담당자명 : #{이름}
				 담당자연락처 : #{전화번호}
				차종 : #{차종}
				차량번호 : #{차량번호}
				차대번호 : #{차대번호}
				오늘도 안전운행 하세요.*/
				
				urlBuilder.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode( resultMsg+"문서 알림.\r\n 도착하였습니다.\r\n결재/합의 바랍니다.", "UTF-8") + "&");
				urlBuilder.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
				urlBuilder.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_003", "UTF-8") + "&");
				//hkc_003
			}else if(map.get("gubun").toString().equals("8")) {	//배차승인
				/*#{기사명}기사님이 
				상차지 : #{시도 시군구}
				하차지 : #{시도 시군구}
				차종 : #{차종}
				차량번호 : #{차량번호}
				차대번호 : #{차대번호}
				배차를 승인 했습니다.*/
				
				urlBuilder.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode( resultMsg+"문서 알림.\r\n 도착하였습니다.\r\n결재/합의 바랍니다.", "UTF-8") + "&");
				urlBuilder.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
				urlBuilder.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_004", "UTF-8") + "&");
				//hkc_004
				
			}else if(map.get("gu_bun").toString().equals("9")) {
				/*
				 탁송
				 
				  
				  
				  
				  
				  */
				
				
				
			}
				
			
			int getHour = Integer.parseInt(WebUtils.getNow("HH"));
			
			//출발일이 현재일과 2일 차이 인경우에만 
			String departureDt = "";
			departureDt = map.get("departure_dt") == null ? "":map.get("departure_dt").toString() ;
			long calDateDays = 0L; 
			
			
			if(!departureDt.equals("")) {
				
				String nowDt =WebUtils.getNow("yyyy-MM-dd");
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				Date departureDt_toDate = format.parse(departureDt);
				Date nowDt_toDate = format.parse(nowDt);
				
				long calDate = nowDt_toDate.getTime() - departureDt_toDate.getTime(); 
				calDateDays = calDate / ( 24*60*60*1000);
				
				//탁송 예약을 하는 경우 2일 이후가 될 수 있으므로 절댓값 부분 수정.
				//calDateDays = Math.abs(calDateDays);
			}
			
			//오전 8시에서 오후 10시 사이에만 플러스 친구 톡을 발송 한다.
			if(getHour > 8 && getHour < 22 && calDateDays <= 2) {
				
				JSONObject jsonObjectForSms = new JSONObject();
				
				
				
				String customerPhone = map.get("customer_phone").toString().replaceAll("-", "");
				customerPhone = customerPhone.replaceAll(" ", "");
				customerPhone = customerPhone.trim();
				String[] customerPhoneArr = customerPhone.split(";");
				
				if(customerPhoneArr.length > 1) {
					//두 곳으로 보내는 경우 같은 번호면 하나만 보낸다....
					if(customerPhoneArr[0].equals(customerPhoneArr[1])) {
						customerPhone = customerPhoneArr[0];
					}
				}
				
				//urlBuilder.append(URLEncoder.encode("rcvphns","UTF-8") + "=" + URLEncoder.encode("01089265400", "UTF-8") + "&");
				urlBuilder.append(URLEncoder.encode("rcvphns","UTF-8") + "=" + URLEncoder.encode(customerPhone, "UTF-8") + "&");
				
				urlBuilder.append(URLEncoder.encode("profileKey","UTF-8") + "=" + URLEncoder.encode("dd3f81c615756d611bd9fb625ca58f2717d4ca15", "UTF-8") + "&");
						
				URL url = new URL(urlBuilder.toString());
		        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		        conn.setDoOutput(true);
		        conn.setRequestMethod("POST");
		        conn.setRequestProperty("Content-Type", "application/json");        
		        conn.setDoOutput(true);
	
		        OutputStream os = conn.getOutputStream();
		        os.flush();
		        os.close();
	
		        int responseCode = conn.getResponseCode();
		        System.out.println("\nSending 'POST' request to URL : " + url);
		        System.out.println("Response Code : " + responseCode);
		        
		        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		        String inputLine;
		        StringBuffer response = new StringBuffer();
	
		        while ((inputLine = in.readLine()) != null) {
		            response.append(inputLine);
		        }
		        in.close();
		        System.out.println(response.toString());
		        
		        JSONParser jsonParser = new JSONParser();
	            JSONArray list = (JSONArray)jsonParser.parse(response.toString());
	            JSONObject jsonObject = (JSONObject) list.get(0);
	            jsonObjectForSms = jsonObject;
	            
				
			}
			
			
		}catch(Exception e) {
			e.printStackTrace();
		}

        return "jsonView";
}
	
	
	

	public String smsSend(Map<String, Object> map)throws Exception{
		
		StringBuilder urlBuilderAT = new StringBuilder(histUrl+"/sms/histsms");

		String resultMsg = "";
		String result = "";
		//알림톡 발송 구분		1:기사배정,2:상차완료,3:하차완료,4:인계완료,5:탁송완료,6탁송취소,7:배차정보(기사님에게),8:배차승인(배차담당직원)
		
		urlBuilderAT.append("?");
		urlBuilderAT.append(URLEncoder.encode("uid","UTF-8") + "=" + URLEncoder.encode(BaseAppConstants.COMPANY_TYPE_NATIONAL, "UTF-8") + "&");
		
		String customerId = map.get("customer_id").toString();
		
		Map<String, Object> searchMap = new HashMap<String, Object>();
		searchMap.put("customerId", customerId);
		
		Map<String, Object> customerMap = customerService.selectCustomer(searchMap);
		
		if(customerMap != null) {
				
				
				if(map.get("gubun").toString().equals("1")) {			//기사배정
		
					if(customerMap.get("sms_yn").toString().equals("Y")) {
						
						//티케이물류   
						//if(customerId.equals("CUS0f3a8db538d74ff580efea0530a08bf2")) {
						if((customerId.equals("CUSa8b74209a826425698ef189f61f08ff6") && map.get("departure").toString().contains("티케이물류"))) {
							
							resultMsg += "ㅁ NH농협캐피탈 탁송안내\r\n";
							if(map.get("person_in_charge") != null && !map.get("person_in_charge").toString().equals("")) {
								resultMsg += "- 고객명 : ";
								resultMsg += map.get("person_in_charge") != null && !map.get("person_in_charge").toString().equals("") ? map.get("person_in_charge").toString():"";	
								resultMsg += "\r\n";
							}
							resultMsg += "- 차  종 : ";
							resultMsg += map.get("car_kind") != null && !map.get("car_kind").toString().equals("") ? map.get("car_kind").toString():"미정";
							resultMsg += "\r\n";
							resultMsg += "- 출발일 : "+ map.get("departure_dt").toString()+"\r\n";
							resultMsg += "- 출발지 : 경기 화성시\r\n";
							resultMsg += "- 하차지 : "+ map.get("arrival").toString()+"\r\n";
							resultMsg += "- 기사명 : "+ map.get("driver_name").toString()+"\r\n";
							resultMsg += "- 연락처 : "+ map.get("phone_num").toString()+"\r\n";
							resultMsg += "해당 차량이 기사님께 배정 되었습니다."+"\r\n";
							
						}
						
						urlBuilderAT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode(resultMsg, "UTF-8") + "&");
						urlBuilderAT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
						
					}
					
					
					//hkc_000
				}else if(map.get("gubun").toString().equals("2")) {	//상차완료
					
					//티케이물류 상차 완료는 사용 안함   
					if(customerId.equals("CUS0f3a8db538d74ff580efea0530a08bf2")) {
					//if(customerId.equals("CUSa8b74209a826425698ef189f61f08ff6") && map.get("departure").toString().contains("티케이물류")) {
						
						resultMsg += "ㅁ NH농협캐피탈 탁송안내\r\n";
						resultMsg += "- 차  종 : ";
						resultMsg += map.get("car_kind") != null && !map.get("car_kind").toString().equals("") ? map.get("car_kind").toString():"미정";
						resultMsg += "\r\n";
						resultMsg += "- 하차지 : "+ map.get("arrival").toString()+"\r\n";
						resultMsg += "고객님의 차량이 안전하게 상차 되었습니다.";
						
					}
					
					urlBuilderAT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode( resultMsg, "UTF-8") + "&");
					
					//hkc_001
				}else if(map.get("gubun").toString().equals("3")) {	//하차완료
		
					
				}else if(map.get("gubun").toString().equals("4")) {	//인계완료
					
					
				}else if(map.get("gubun").toString().equals("5")) {	//탁송완료
					
					
					if(customerMap.get("sms_yn").toString().equals("Y")) {
						
						//if(customerId.equals("CUS0f3a8db538d74ff580efea0530a08bf2")) {
						//고객이 티케이물류이고 상차지가 티케이물류인 건(2차탁송)   
						if(customerId.equals("CUSa8b74209a826425698ef189f61f08ff6") && map.get("departure").toString().contains("티케이물류")) {
							
							resultMsg += "ㅁ NH농협캐피탈 탁송안내\r\n";
							if(map.get("person_in_charge") != null && !map.get("person_in_charge").toString().equals("")) {
								resultMsg += "- 고객명 : ";
								resultMsg += map.get("person_in_charge") != null && !map.get("person_in_charge").toString().equals("") ? map.get("person_in_charge").toString():"";
								resultMsg += "\r\n";
							}
							resultMsg += "- 차   종 : ";
							resultMsg += map.get("car_kind") != null && !map.get("car_kind").toString().equals("") ? map.get("car_kind").toString():"미정";
							resultMsg += "\r\n";
							resultMsg += "- 하차지 : "+ map.get("arrival").toString()+"\r\n";
							resultMsg += "해당 차량의 탁송이 완료 되었습니다."+"\r\n";
							
						}
						
						urlBuilderAT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode(resultMsg, "UTF-8") + "&");
						urlBuilderAT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
						
					}
				
				}else if(map.get("gubun").toString().equals("6")) {	//탁송취소
					
					urlBuilderAT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode( resultMsg+"문서 알림.\r\n 도착하였습니다.\r\n결재/합의 바랍니다.", "UTF-8") + "&");
					urlBuilderAT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
					urlBuilderAT.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_005", "UTF-8") + "&");
				
					
				}else if(map.get("gubun").toString().equals("7")) {	//배차정보
				
					urlBuilderAT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode( resultMsg+"문서 알림.\r\n 도착하였습니다.\r\n결재/합의 바랍니다.", "UTF-8") + "&");
					urlBuilderAT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
					urlBuilderAT.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_003", "UTF-8") + "&");
				
					
				}else if(map.get("gubun").toString().equals("8")) {	//배차승인
				
					urlBuilderAT.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode( resultMsg+"문서 알림.\r\n 도착하였습니다.\r\n결재/합의 바랍니다.", "UTF-8") + "&");
					urlBuilderAT.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("at", "UTF-8") + "&");
					urlBuilderAT.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_004", "UTF-8") + "&");
					
				}
				
				int getHour = Integer.parseInt(WebUtils.getNow("HH"));
				String nowDt = 	WebUtils.getNow("yyyy-MM-dd");
				
				String arrivalPhone = map.get("arrival_phone") == null || map.get("arrival_phone").toString().equals("") ? "":map.get("arrival_phone").toString();
				String callback = customerMap.get("call_back_for_sms") == null || customerMap.get("call_back_for_sms").toString().equals("") ? "" : customerMap.get("call_back_for_sms").toString();
				
				
				arrivalPhone = arrivalPhone.replaceAll("-", "");
				arrivalPhone = arrivalPhone.replaceAll(" ", "");
				arrivalPhone = arrivalPhone.replaceAll("/", ";");
				arrivalPhone = arrivalPhone.trim();
				
				// 하차지 담당자(고객)의 전화 번호가 없는경우 알림톡을 발송 하도록 설정...........
				if(arrivalPhone.equals("") || callback.equals("")) {
					//this.alarmTalkSend(map);
				    return "jsonView";
				}
							
				
				if(!resultMsg.equals("")) {
					
					urlBuilderAT.append(URLEncoder.encode("rcvphns","UTF-8") + "=" + URLEncoder.encode(arrivalPhone, "UTF-8") + "&");
					//urlBuilderAT.append(URLEncoder.encode("rcvphns","UTF-8") + "=" + URLEncoder.encode("01048644300", "UTF-8") + "&");
					urlBuilderAT.append(URLEncoder.encode("callback","UTF-8") + "=" + URLEncoder.encode(callback, "UTF-8") + "&");
					urlBuilderAT.append(URLEncoder.encode("subject","UTF-8") + "=" + URLEncoder.encode("기사배정안내", "UTF-8") + "&");
					
					URL url = new URL(urlBuilderAT.toString());
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					conn.setDoOutput(true);
					conn.setRequestMethod("POST");
					conn.setRequestProperty("Content-Type", "application/json");        
					conn.setDoOutput(true);
						
					OutputStream os = conn.getOutputStream();
					os.flush();
					os.close();
					int responseCode = conn.getResponseCode();
					System.out.println("\nSending 'POST' request to URL : " + url);
					System.out.println("Response Code : " + responseCode);
						
					BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
					String inputLine = "";
					StringBuffer response = new StringBuffer();
					while ((inputLine = in.readLine()) != null) {
						response.append(inputLine);
					}
					in.close();
					System.out.println(response.toString());
					
					if(responseCode == 200 && response.toString().equals("0")) {
						
						String sendSmsId = "SMS"+UUID.randomUUID().toString().replaceAll("-","");
						SendSmsVO sendSmsVO = new SendSmsVO();
						sendSmsVO.setCallback(callback);
						sendSmsVO.setCustomerId(customerId);
						sendSmsVO.setMsg(resultMsg);
						sendSmsVO.setRcvphns(arrivalPhone);
						sendSmsVO.setSendSmsId(sendSmsId);
						sendSmsVO.setType(map.get("gubun").toString());
						sendSmsVO.setAllocationId(map.get("allocation_id").toString());
						
						try {
							sendSmsService.insertSendSms(sendSmsVO);	
						}catch(Exception e) {
							e.printStackTrace();
						}
						
					}
					
				}
		}	
		
		return "jsonView";
		
		
	}
	
	
	public static String fileToBinary(File file) {
	    String out = new String();
	    FileInputStream fis = null;
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	 
	    try {
	        fis = new FileInputStream(file);
	    } catch (FileNotFoundException e) {
	    	e.printStackTrace();
	        System.out.println("Exception position : FileUtil - fileToString(File file)");
	    }
	 
	    int len = 0;
	    byte[] buf = new byte[1024];
	    try {
	        while ((len = fis.read(buf)) != -1) {
	            baos.write(buf, 0, len);
	        }
	 
	        byte[] fileArray = baos.toByteArray();
	        out = new String(base64Enc(fileArray));
	        fis.close();
	        baos.close();
	        
	        
	        StringBuilder urlBuilder = new StringBuilder(histUrl+"/sms/add_msg_image");
	        urlBuilder.append("?");
	        urlBuilder.append(URLEncoder.encode("image","UTF-8") + "=" + out);
	        
	        URL url = new URL(urlBuilder.toString());
	        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	        conn.setDoOutput(true);
	        conn.setRequestMethod("POST");
	        conn.setRequestProperty("Content-Type", "multipart/form-data");
	        conn.setRequestProperty("HIST-uid", BaseAppConstants.COMPANY_TYPE_NATIONAL);
	        conn.setRequestProperty("HIST-profileKey", "dd3f81c615756d611bd9fb625ca58f2717d4ca15");
	        conn.setDoOutput(true);

	        OutputStream os = conn.getOutputStream();
	        os.flush();
	        os.close();
	        
	        int responseCode = conn.getResponseCode();
	        System.out.println("\nSending 'POST' request to URL : " + url);
	        System.out.println("Response Code : " + responseCode);
	        
	        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	        String inputLine;
	        StringBuffer response = new StringBuffer();

	        while ((inputLine = in.readLine()) != null) {
	            response.append(inputLine);
	        }
	        in.close();
	        System.out.println(response.toString());
	        
	        
	    } catch (IOException e) {
	        System.out.println("Exception position : FileUtil - fileToString(File file)");
	    }
	 
	    return out;
	}


	public static byte[] base64Enc(byte[] buffer) {
	    return Base64.encodeBase64(buffer);
	}

	
	public String fileUpload(File file) throws Exception {
		
		String response = "";
		
		String imgOriginalPath = file.getAbsolutePath() ;         //원본 이미지 경로
        String imgTargetPath =rootDir+"/allocationSend/"+file.getName();           //새 이미지 파일명
        String ext = file.getName().substring(file.getName().lastIndexOf(".")+1,file.getName().length());
        String imgFormat       = ext;
        int newWidth        =720;
        int newHeight       =720;
        String mainPosition    =   "";
        
        BufferedImage image;
        int imageWidth;
        int imageHeight;
        double ratio;
        int w;
        int h;
		
		try {
			
			String urlApi = histUrl+"/sms/add_msg_image";
			
			Map<String, String> headers = new HashMap<String, String>();
	        headers.put("HIST-uid", BaseAppConstants.COMPANY_TYPE_NATIONAL);
	        headers.put("HIST-profileKey", "dd3f81c615756d611bd9fb625ca58f2717d4ca15");
	        
	        MultipartUtility multipartUtility = new MultipartUtility(urlApi, headers);

	        // File part
	        //File file = new File("/img/test.jpg");        // 업로드할 이미지
	        multipartUtility.addFilePart("image", file);

	        response = multipartUtility.finish();
	        System.out.println("response : " + response);
			
	        JSONParser jsonParser = new JSONParser();
	         JSONObject jsonObject = (JSONObject) jsonParser.parse(response);
	        
	        //String fileName = file.getName();
	        //String ext = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length());
	        if(!jsonObject.get("code").toString().equals("success")) {
                
                try {
                   
                   if(jsonObject.get("code").toString().equals("fail")) {      
                      
                      //사진 사이즈 조정 후 재 전송
                      image =ImageIO.read(new File(imgOriginalPath));
                      imageWidth = image.getWidth(null);
                      imageHeight= image.getHeight(null);
                      
                     
                      w= newWidth;
                      h= newHeight;
                         
                      
                      Image resizeImage = image.getScaledInstance(w,h, image.SCALE_SMOOTH);
                      
                      //변경 이미지 저장 
                      BufferedImage newImage = new BufferedImage(w, h,BufferedImage.TYPE_INT_RGB);
                      Graphics g = newImage.getGraphics();
                      g.drawImage(resizeImage, 0,0,null);
                      g.dispose();
                      
                      File saveDir = new File(rootDir+"/allocationSend");
                      
                      if(saveDir.exists()==false) {
                         saveDir.mkdir();
                      }
                   
                      File newFile = new File(imgTargetPath);
                      
                      ImageIO.write(newImage, imgFormat,newFile);
                      //this.fileUpload(newFile);
                    
                      multipartUtility = new MultipartUtility(urlApi, headers);
                      multipartUtility.addFilePart("image", newFile);
                      response = multipartUtility.finish();
                      System.out.println("response : " + response);
                      
                   }else {
                      
                   }   
                   
                }catch(Exception e) {
                   e.printStackTrace();
                   //나중에 생각 하고......
                }
                
             }else {
                return response;
             }
         
	        
	        
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return response;
		
	}
	
	
	
	// 카카오 서버에 파일 업로드
	
	public String yun(Map<String, Object> allocationMap,Map<String, Object> map)throws Exception{

		try {
			String allocationId = allocationMap.get("allocation_id").toString();
			String driverId =allocationMap.get("driver_id").toString();
			String customerId = allocationMap.get("customer_id").toString();
		
			
			
			Map<String, Object> searchMap = new HashMap<String, Object>();
			searchMap.put("allocationId", allocationId);
			searchMap.put("driverId", driverId);
			
			Map<String, Object> allocationFile = allocationFileService.selectAllocationFileForSendReceipt(searchMap);
			
			if(allocationFile != null) {
				
				File file = new File(rootDir+"/allocation"+allocationFile.get("allocation_file_path").toString());
				
				
				//파일 업로드........................
				String result = this.fileUpload(file);
				String resultMsg = "";
				
				
				resultMsg += allocationMap.get("car_kind") != null &&!allocationMap.get("car_kind").toString().equals("")?"차종 : "+allocationMap.get("car_kind").toString()+"\r\n":"";
		        resultMsg += allocationMap.get("car_id_num") != null &&!allocationMap.get("car_id_num").toString().equals("")?"차대번호 : "+allocationMap.get("car_id_num").toString()+"\r\n":""; 
		        resultMsg += allocationMap.get("car_num") != null &&!allocationMap.get("car_num").toString().equals("")?"차량번호 : "+allocationMap.get("car_num").toString()+"\r\n":""; 
		        resultMsg += allocationMap.get("arrival") != null &&!allocationMap.get("arrival").toString().equals("")?"하차지 : "+allocationMap.get("arrival").toString()+"\r\n":""; 
		        resultMsg +="위 차량의 인수증이 도착 했습니다.";
		        
		        System.out.println("result" +result);

		        Map<String, Object> resultMap = new HashMap<String, Object>();
		        
		        resultMap.put("resultMsg", resultMsg);
		        resultMap.put("senderId", (String) allocationMap.get("driver_id"));
		        resultMap.put("allocationFileId", allocationFile.get("allocation_file_id"));
		        
				if(!result.equals("")) {
	                JSONParser jsonParser = new JSONParser();
	                JSONObject jsonObject = null; 
	                
	               try{
	                	 jsonObject = (JSONObject) jsonParser.parse(result);
	                	
	                   if(jsonObject.get("code").toString().equals("success")) {      //이미지 업로드 결과가 성공이면....
	                	   JSONObject obj = (JSONObject)jsonParser.parse(jsonObject.get("data").toString());
	                	   resultMap.put("img_url",obj.get("img_url").toString());
	                	   this.yunSub(resultMap, customerId,allocationMap); 
	                	   
	                   }else {
	                	   
	                   }   
	                   
	                }catch(Exception e) { //json 형식이 아니기 때문에..
	                   e.printStackTrace();
	                		   
	                }
	                
	             }
				
			}
				
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
        return "jsonView";
	}
	
	//파일 업로드 성공시 담당자에게 친구톡 발송
	public String yunSub(Map<String, Object> resultMap, String customerId, Map<String, Object> allocationMap) throws Exception{
		
		
		try {
			
			StringBuilder urlBuilder = new StringBuilder(histUrl+"/sms/histmsg");
			urlBuilder.append("?");
			urlBuilder.append(URLEncoder.encode("uid","UTF-8") + "=" + URLEncoder.encode(BaseAppConstants.COMPANY_TYPE_NATIONAL, "UTF-8") + "&");
			
			Map <String ,Object> map= new HashMap<String, Object>();
			
			String senderId =  resultMap.get("senderId").toString();
			String allocationFileId= resultMap.get("allocationFileId").toString();
			String allocationId = allocationMap.get("allocation_id").toString();
			String driverId =allocationMap.get("driver_id").toString();
			
			map.put("allocationId", allocationId);
			map.put("driverId", driverId);
			
			
			System.out.println("allocaionMap"+allocationMap);
			
			Map<String,Object> chargeMap = customerService.selectChargePhoneNum(allocationMap);
			
			String chargePhone = chargeMap.get("phone_num").toString().replaceAll("-","");
			chargePhone = chargePhone.replaceAll(" ","");
			chargePhone = chargePhone.trim();
			
			
		
			List<Map<String,Object>> chargeList = customerService.selectChargeList(allocationMap);
			
			map.put("chargePhone", chargePhone);
			
            urlBuilder.append(URLEncoder.encode("msgType","UTF-8") + "=" + URLEncoder.encode("ft", "UTF-8") + "&");
            urlBuilder.append(URLEncoder.encode("talkImgUrl","UTF-8") + "=" + URLEncoder.encode(resultMap.get("img_url").toString(), "UTF-8") + "&");
            urlBuilder.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode(resultMap.get("resultMsg").toString(), "UTF-8") + "&");
	        urlBuilder.append(URLEncoder.encode("template","UTF-8") + "=" + URLEncoder.encode("hkc_010", "UTF-8") + "&");
	        urlBuilder.append(URLEncoder.encode("adMsgYn","UTF-8") + "=" + URLEncoder.encode("N", "UTF-8") + "&");  
	        urlBuilder.append(URLEncoder.encode("rcvphns","UTF-8") + "=" + URLEncoder.encode(chargePhone, "UTF-8") + "&");
	        urlBuilder.append(URLEncoder.encode("profileKey","UTF-8") + "=" + URLEncoder.encode("dd3f81c615756d611bd9fb625ca58f2717d4ca15", "UTF-8") + "&");
	        
	        
	        System.out.println("urlBuilder.toString="+urlBuilder.toString());
 	        
	        
				        //친구톡을 발송 한다.......
				        URL url = new URL(urlBuilder.toString());
						HttpURLConnection conn = (HttpURLConnection) url.openConnection();
						conn.setDoOutput(true);
						conn.setRequestMethod("POST");
						conn.setRequestProperty("Content-Type", "application/json");        
						conn.setDoOutput(true);

						OutputStream os = conn.getOutputStream();
						os.flush();
						os.close();

						int responseCode = conn.getResponseCode();
						System.out.println("\nSending 'POST' request to URL : " + url);
						System.out.println("Response Code : " + responseCode);
						        
						BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
						String inputLine;
						StringBuffer response = new StringBuffer();
						
						while ((inputLine = in.readLine()) != null) {
							response.append(inputLine);
						}
						in.close();
						System.out.println("response"+response.toString());

						
					    String result = response.toString();
					    JSONParser jsonParser = new JSONParser();
				        JSONArray jsonArray = null;
				          
				    	Map<String, Object> updateMap = new HashMap<String, Object>();
				    	updateMap.put("allocationId", allocationId);
				    	updateMap.put("missingSend"," ");
				    	
				       	   
  				        int getHour= Integer.parseInt(WebUtils.getNow("HH")); 

					    if(getHour>12){	
					    	
					    String nextDate =WebUtils.getNextDate(Integer.parseInt(WebUtils.getNow("yyyy")), Integer.parseInt(WebUtils.getNow("MM")), Integer.parseInt(WebUtils.getNow("dd")),"yyyyMMdd");
					    urlBuilder.append(URLEncoder.encode("snddttm","UTF-8") + "=" + URLEncoder.encode(nextDate+"090500", "UTF-8") + "&");
						
					    }else{
					    urlBuilder.append(URLEncoder.encode("snddttm","UTF-8") + "=" + URLEncoder.encode(WebUtils.getNow("yyyyMMdd")+"090500", "UTF-8") + "&");
							 
					    }
				   	
				    	
				    	Map<String, Object> insertMap = null;
				    	
				        try {
				        				
				        		jsonArray =(JSONArray) jsonParser.parse(result);
				        	
						          for(int i = 0; i < jsonArray.size(); i++) {
						            	 JSONObject jsonObject =(JSONObject)jsonArray.get(i);
						            	 insertMap = new HashMap<String, Object>();
							        	 insertMap.put("senderId", senderId);                        
							        	 insertMap.put("allocationFileId", allocationFileId);  
							        	 insertMap.put("reserveYn"," ");
							        	 insertMap.put("allocationId", allocationId);
							        	 insertMap.put("sendAllocationFileId", "SFI"+UUID.randomUUID().toString().replaceAll("-", ""));
							       	     JSONObject obj = (JSONObject)jsonParser.parse(jsonObject.get("data").toString());
							        	 insertMap.put("chargePhone", obj.get("phn").toString());
							        	 insertMap.put("msgId", obj.get("msgid").toString());
							        	
							        	 String phoneNum = "";
							        	 	
							        	 
								      for(int j=0; j<chargeList.size(); j++) {
								        	 		
								        	 Map<String, Object> charge = chargeList.get(j);
								        	 phoneNum = charge.get("phone_num").toString();
								        	 phoneNum = phoneNum.replaceAll("-", "");
								        	 phoneNum = phoneNum.replaceAll(" ", "");
								        	 phoneNum = phoneNum.trim();
									         if(phoneNum.contains(obj.get("phn").toString())) {
									        	insertMap.put("personInChargeId", charge.get("person_in_charge_id").toString());
									        	  
									        }else{ 
									        	
									        }
									    }
								      
							          
							        if(jsonObject.get("code").toString().equals("success")){ 
							    		
							        	 insertMap.put("reserveYn","N");
							        	 insertMap.put("resultCd", jsonObject.get("message").toString().substring(0,4));
							        	 allocationFileService.insertCaSendAllocationFile(insertMap);
							        	 
							        	 updateMap.put("missingSend","Y");
									 	 allocationService.updateMissingSend(updateMap);
									 	 
									 	  
									 	 
							        }else if(jsonObject.get("code").toString().equals("fail")) {	
							     			
							        		if(jsonObject.get("message").toString().contains("K101")){
							     	    	   
							        		  insertMap.put("reserveYn","N");
							        		  insertMap.put("resultCd", jsonObject.get("message").toString().substring(0,4));
							        		  System.out.println(jsonObject.get("message").toString()+"메세지를 전송할 수 없음 ");
							        		 
									       }else if(jsonObject.get("message").toString().contains("K104")){
									    	   insertMap.put("reserveYn","N");
									    	   insertMap.put("resultCd", jsonObject.get("message").toString().substring(0,4));
									           System.out.println(jsonObject.get("message").toString()+"유효하지 않은 사용자 전화번호 ");
									       	 
									       }else if(jsonObject.get("message").toString().contains("K999")){
									    	   insertMap.put("reserveYn","N");
									    	   insertMap.put("resultCd", jsonObject.get("message").toString().substring(0,4));
									    	   System.out.println(jsonObject.get("message").toString()+"시스템 오류 ");
									       	 
									       }else if(jsonObject.get("message").toString().contains("K301")){
									    	   insertMap.put("reserveYn","N");
									    	   insertMap.put("resultCd", jsonObject.get("message").toString().substring(0,4));
									    	   System.out.println(jsonObject.get("message").toString()+"메세지 전송 실패");
									       	 
									       }else if(jsonObject.get("message").toString().contains("K998")){
									    	   insertMap.put("reserveYn","N");
									    	   insertMap.put("resultCd", jsonObject.get("message").toString().substring(0,4));
									    	   System.out.println(jsonObject.get("message").toString()+"(기타오류로 메시지 전송 실패)");
								    	 
									       }else if(jsonObject.get("message").toString().contains("K303")){  
									    	   
									    	   
									    	   int startNum = urlBuilder.indexOf("rcvphns=");    
									    	   int endNum = urlBuilder.indexOf("&", startNum);
									    	   
									    	   String front = urlBuilder.substring(0,startNum);
									    	   String back = urlBuilder.substring(endNum+1);
									    	   String resultUrl = front+back+"rcvphns="+obj.get("phn").toString();
									    	   
									    	   System.out.println("resultUrl"+resultUrl);
									
									  			url = new URL(resultUrl);
												conn = (HttpURLConnection) url.openConnection();
												conn.setDoOutput(true);
												conn.setRequestMethod("POST");
												conn.setRequestProperty("Content-Type", "application/json");  
												conn.setDoOutput(true);

												os = conn.getOutputStream();
												os.flush();
												os.close();

												responseCode = conn.getResponseCode();
												System.out.println("\nSending 'POST' request to URL : " + url);
												System.out.println("Response Code : " + responseCode);
												
												
												in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
												inputLine = "";
												response = new StringBuffer();
												
												
												while ((inputLine = in.readLine()) != null) {
													response.append(inputLine);
												}
												
												in.close();
												System.out.println("response"+response.toString());
									 			  
												insertMap.put("reserveYn","Y"); //예약발송일때만 'Y'
												insertMap.put("resultCd", jsonObject.get("message").toString().substring(0,4));
									        	
							             }
							         
							        		allocationFileService.insertCaSendAllocationFile(insertMap);
							        		
							        	}else{ 
							        		
							        		insertMap.put("resultCd", "R999");
						            	 	allocationFileService.insertCaSendAllocationFile(insertMap);
						            	 	
						            	 	updateMap.put("missingSend", "F");
						            	 	allocationService.updateMissingSend(updateMap);
						             }
						      }
							
							
						}catch(Exception e) { 
							
							e.printStackTrace();
							
							insertMap = new HashMap<String, Object>();
				        	insertMap.put("senderId", senderId);                        
				        	insertMap.put("allocationFileId", allocationFileId);  
				        	insertMap.put("reserveYn","N");
				        	insertMap.put("allocationId", allocationId);
				        	insertMap.put("sendAllocationFileId", "SFI"+UUID.randomUUID().toString().replaceAll("-", ""));
				        	insertMap.put("chargePhone", "");
				        	insertMap.put("resultCd", "777");
				        	insertMap.put("personInChargeId", "");
		            	 	allocationFileService.insertCaSendAllocationFile(insertMap);
							
							updateMap.put("missingSend", "F");
							allocationService.updateMissingSend(updateMap);
						}
				        
				            
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		
		return "";
	}
	
}
	
