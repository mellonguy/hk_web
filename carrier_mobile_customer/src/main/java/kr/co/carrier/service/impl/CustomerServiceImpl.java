package kr.co.carrier.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.CustomerMapper;
import kr.co.carrier.service.CustomerService;
import kr.co.carrier.vo.CustomerAppVO;
import kr.co.carrier.vo.CustomerVO;

@Service("customerService")
public class CustomerServiceImpl implements CustomerService{

	
	@Resource(name="customerMapper")
	private CustomerMapper customerMapper;
	
	public Map<String, Object> selectCustomer(Map<String, Object> map) throws Exception{
		return customerMapper.selectCustomer(map);
	}
	
	public List<Map<String, Object>> selectCustomerList(Map<String, Object> map) throws Exception{
		return customerMapper.selectCustomerList(map);
	}
	
	public int selectCustomerListCount(Map<String, Object> map) throws Exception{
		return customerMapper.selectCustomerListCount(map);
	}
	
	public int insertCustomer(CustomerVO customerVO) throws Exception{
		return customerMapper.insertCustomer(customerVO);
	}
	
	public void updateCustomer(CustomerVO customerVO) throws Exception{
		customerMapper.updateCustomer(customerVO);
	}
	
	public List<Map<String, Object>> selectChargeList(Map<String, Object> map) throws Exception{
		return customerMapper.selectChargeList(map);
	}

	public Map <String,Object>selectChargePhoneNum (Map<String,Object>map)throws Exception{
		return customerMapper.selectChargePhoneNum(map);
	}
	public List<Map<String, Object>> selectChargePhoneAndBusinessLicenseNumber(Map<String, Object> map) throws Exception{
		return customerMapper.selectChargePhoneAndBusinessLicenseNumber(map);
	}
	public void updateVaildYnForCertNum(Map<String,Object>map) throws Exception{
		customerMapper.updateVaildYnForCertNum(map);
	
	}
	public int insertSimpleSignUp(CustomerAppVO customerAppVO) throws Exception{
		return customerMapper.insertSimpleSignUp(customerAppVO);
	}
	public Map<String,Object>selectCaUseCustomerAppByCustomer(Map<String,Object>map)throws Exception{
		return customerMapper.selectCaUseCustomerAppByCustomer(map);
	}
	public Map<String,Object>selectidDoubleCheck(Map<String,Object>map)throws Exception{
		return customerMapper.selectidDoubleCheck(map);
	}
	public Map<String,Object>selectCustomerAllocationStatusList(Map<String,Object>map)throws Exception{
		return customerMapper.selectCustomerAllocationStatusList(map);
	
	}
	public Map<String, Object> selectCustomerDetailInfo(Map<String,Object>map)throws Exception{
		return customerMapper.selectCustomerDetailInfo(map);
		
	}
	public List<Map<String, Object>> selectAllocationStatus(Map<String, Object> map) throws Exception{
		return customerMapper.selectAllocationStatus(map);
	}
	public  List<Map<String, Object>> selectCustomerCheckForCanNotBeDoubleCert(Map<String,Object>map)throws Exception{
		return customerMapper.selectCustomerCheckForCanNotBeDoubleCert(map);
	}
	public  List<Map<String, Object>> selectSearchForAllocation(Map<String,Object>map)throws Exception{
		return customerMapper.selectSearchForAllocation(map);
	}
	public  List<Map<String, Object>> selectCarIdNumList(Map<String,Object>map)throws Exception{
		return customerMapper.selectCarIdNumList(map);
	}
	public  Map<String, Object> selectCarIdNumForMain(Map<String,Object>map)throws Exception{
		return customerMapper.selectCarIdNumForMain(map);
	}
	public Map<String, Object> selectAllocationCntMonth(Map<String,Object>map)throws Exception{
		return customerMapper.selectAllocationCntMonth(map);
	}
	public List<Map<String, Object>> selectReciptBoxList(Map<String,Object>map)throws Exception{
		return customerMapper.selectReciptBoxList(map);
	}
	public Map<String, Object>selectShowDistenceToDriverLocation(Map<String,Object>map)throws Exception{
		return customerMapper.selectShowDistenceToDriverLocation(map);
	}
	public Map<String, Object>selectChangeId(Map<String,Object>map)throws Exception{
		return customerMapper.selectChangeId(map);
	}
	public Map<String, Object>selectChagePasswordById(Map<String,Object>map)throws Exception{
		return customerMapper.selectChagePasswordById(map);
	}
	public void updateCreatePassword(Map<String, Object> map) throws Exception{
		 customerMapper.updateCreatePassword(map);
	}
	public Map<String, Object>selectCustomerIdAndCustomerAPP(Map<String,Object>map)throws Exception{
		return customerMapper.selectCustomerIdAndCustomerAPP(map);
	}
	
	public Map<String, Object> selectCustomerByCustomerName(Map<String, Object> map) throws Exception{
		return customerMapper.selectCustomerByCustomerName(map);
	}
	
	public Map<String, Object> selectPhoneDuplicationChk(Map<String, Object> map) throws Exception{
		return customerMapper.selectPhoneDuplicationChk(map);
	}
	
	public List<Map<String, Object>> selectMileageList	(Map<String,Object>map)throws Exception{
		return customerMapper.selectMileageList(map);
	}
	public Map<String, Object> selectMileageCount(Map<String, Object> map) throws Exception{
		return customerMapper.selectMileageCount(map);
	}
	public List<Map<String, Object>> selectAllocationGraph(Map<String,Object>map)throws Exception{
		return customerMapper.selectAllocationGraph(map);
	}
	public int insertReview(Map<String,Object>map)throws Exception{
		return customerMapper.insertReview(map);
	}
	
	public List<Map<String, Object>> selectReviewList(Map<String,Object>map)throws Exception{
		return customerMapper.selectReviewList(map);
	}

	
	public int insertMileageCustomer(Map<String, Object> map) throws Exception{
		
		Map<String,Object> mileageMap = new HashMap<String, Object>();

			try {
			
			String mileageId ="MIL"+UUID.randomUUID().toString().replaceAll("-","");
			
			//마일리지 구분  1: 어플로 탁송 예약 2: 회원가입적립 3:탁송 완료 적립  4 : 기타 
				
				
			//1: 어플로 탁송 예약	
			if(map.get("gubun").toString().equals("1")) {
			
			
			mileageMap.put("mileageId",mileageId);
			mileageMap.put("customerId",map.get("customerId").toString());
			mileageMap.put("customerName", map.get("customerName").toString());
			mileageMap.put("mileages", map.get("mileages").toString());
			mileageMap.put("mileageStatus","U");
			mileageMap.put("mileageText","탁송의뢰사용");
			mileageMap.put("allocationId", map.get("allocationId").toString());
			
			
			
		//	2: 회원가입적립
		}else if(map.get("gubun").toString().equals("2")) {
			
			
			mileageMap.put("mileageId", mileageId);
			mileageMap.put("customerId",map.get("customerId").toString());
			mileageMap.put("customerName", map.get("customerName").toString());
			mileageMap.put("mileages", "3000");
			mileageMap.put("mileageStatus","A");
			mileageMap.put("mileageText", "회원가입적립");
			mileageMap.put("allocationId", "");
			
			//3:리뷰 작성 적립 
			
		}else if(map.get("gubun").toString().equals("3")) {
			
			mileageMap.put("mileageId", mileageId);
			mileageMap.put("customerId",map.get("customerId").toString());
			mileageMap.put("customerName", map.get("customerName").toString());
			mileageMap.put("mileages", map.get("mileages").toString());
			mileageMap.put("mileageStatus", "A");
			mileageMap.put("mileageText", "마일리지적립");
			mileageMap.put("allocationId", map.get("allocationId").toString());
			
			
		}else if(map.get("gubun").toString().equals("4")) {
			
			
			
			
			
		}
			
			
			
			
			
			 
		}catch(Exception e) {
			e.printStackTrace();
			
		}
		
		
		return customerMapper.insertMileageCustomer(mileageMap);
	}
}