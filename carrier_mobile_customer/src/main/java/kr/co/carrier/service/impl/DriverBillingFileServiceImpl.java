package kr.co.carrier.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFCreationHelper;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFPicture;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.DriverBillingFileMapper;
import kr.co.carrier.service.CompanyService;
import kr.co.carrier.service.DriverBillingFileService;
import kr.co.carrier.service.DriverService;
import kr.co.carrier.service.FileService;
import kr.co.carrier.service.FileUploadService;
import kr.co.carrier.utils.WebUtils;
import kr.co.carrier.vo.DriverBillingFileVO;

@Service("driverBillingFileService")
public class DriverBillingFileServiceImpl implements DriverBillingFileService{

	
	@Resource(name="driverBillingFileMapper")
	private DriverBillingFileMapper driverBillingFileMapper;
	
	
	@Autowired
    private DriverService driverService;
	
	@Autowired
	private FileUploadService fileUploadService;
	
	@Autowired
	private FileService fileService;
	
	@Autowired
	private CompanyService companyService;
	
	
	@Value("#{appProp['upload.base.path']}")
    private String rootDir;    
    private String subDir;
	
	
	public Map<String, Object> selectDriverBillingFile(Map<String, Object> map) throws Exception{
		return driverBillingFileMapper.selectDriverBillingFile(map);
	}
	
	public List<Map<String, Object>> selectDriverBillingFileList(Map<String, Object> map) throws Exception{
		return driverBillingFileMapper.selectDriverBillingFileList(map);
	}
	
	public int selectDriverBillingFileListCount(Map<String, Object> map) throws Exception{
		return driverBillingFileMapper.selectDriverBillingFileListCount(map);
	}
	
	public int insertDriverBillingFile(DriverBillingFileVO driverBillingFileVO) throws Exception{
		return driverBillingFileMapper.insertDriverBillingFile(driverBillingFileVO);
	}
	
	public void deleteDriverBillingFile(Map<String, Object> map) throws Exception{
		driverBillingFileMapper.deleteDriverBillingFile(map);
	}
	
	
	public Map<String, Object> makeDriverBillingFile(Map<String, Object> paramMap) throws Exception{
		
		
		Map<String, Object> fileMap = new HashMap<String, Object>();
		
		try {
		
			 	File file = new File(rootDir+"/forms", "billing_provider.xlsx");
		        XSSFWorkbook wb = null;
		        
		    	XSSFRow row;
				XSSFCell cell;
				try {//엑셀 파일 오픈
					wb = new XSSFWorkbook(new FileInputStream(file));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
		    	
				XSSFSheet sheet = wb.getSheetAt(0);         
		        
				CellStyle style = wb.createCellStyle();
				style.setFillForegroundColor(IndexedColors.PALE_BLUE.getIndex());
		        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		        style.setAlignment(CellStyle.ALIGN_CENTER);
		        style.setBorderBottom(CellStyle.BORDER_DOUBLE);
		        style.setBorderTop(CellStyle.BORDER_DOUBLE);
		        style.setBorderLeft(CellStyle.BORDER_DOUBLE);
		        style.setBorderRight(CellStyle.BORDER_DOUBLE);
				
		        int rows = sheet.getLastRowNum();
				int cells = sheet.getRow(0).getLastCellNum();
				
				
				FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
				
				Map<String, Object> map = new HashMap<String, Object>();
		    	map.put("driverId", paramMap.get("driverId").toString());
				//map.put("driverId", "caosys");
		    	
		    	map.put("forUpdatePassWord", "");
		    	Map<String, Object> driver = driverService.selectDriver(map);
		    	
		    	String companyId = "";
		    	
		    	//소속이 지정 되어 있으면
		    	if(driver.get("assign_company") != null && !driver.get("assign_company").toString().equals("")) {
		    		companyId = driver.get("assign_company").toString();
		    	}else {
		    		//소속이 지정되어 있지 않으면 무조건 한국카캐리어(주) 소속으로.... 
		    		companyId = "company2";
		    	}
		    	Map<String, Object> searchMap = new HashMap<String, Object>();
		    	searchMap.put("companyId", companyId);
		    	Map<String, Object> companyInfo = companyService.selectCompany(searchMap);
		    	
		    	String businessLicenseNumber = companyInfo.get("business_license_number").toString().replaceAll("-", "");	//사업자등록번호
		    	String[] businessLicenseNumberArr = businessLicenseNumber.split("");
		    	String companyName = companyInfo.get("company_name").toString().replaceAll("-", "");	//상호
		    	String companyOwnerName = companyInfo.get("company_owner_name").toString().replaceAll("-", "");	//대표자명
		    	String address = companyInfo.get("address").toString().replaceAll("-", "")+" "+companyInfo.get("address_detail").toString().replaceAll("-", "");	//주소
		    	String businessCondition = companyInfo.get("business_condition").toString().replaceAll("-", "");	//업태
		    	String businessKind = companyInfo.get("business_kind").toString().replaceAll("-", "");	//종목
		    	
          		map.put("contentId", paramMap.get("driverId").toString());
          		map.put("categoryType", "driverSeal");
				List<Map<String, Object>> driverSeal = fileService.selectFileList(map);
		    	
				if(driver != null && driver.get("driver_id") != null) {
					
					 for (int r = 2; r <= rows; r++) {
						 
				        	row = sheet.getRow(r); // row 가져오기
				        	if (row != null) {
				        		for (int c = 0; c < cells; c++) {
				        			cell = row.getCell(c);
				        			if (cell != null) {
				        				String value = "";
										switch (cell.getCellType()) {
										   	case XSSFCell.CELL_TYPE_FORMULA:
										   		if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_NUMERIC){
										   			value = "" + (int)cell.getNumericCellValue();
										   		}else if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_STRING){
										   			value = "" + cell.getStringCellValue(); 
										   		}
										   		value = cell.getCellFormula();
										   		break;
										   	case XSSFCell.CELL_TYPE_NUMERIC:
										   		value = "" + (int)cell.getNumericCellValue();
										   		break;
										   	case XSSFCell.CELL_TYPE_STRING:
										   		value = "" + cell.getStringCellValue();
										   		break;
										   	case XSSFCell.CELL_TYPE_BLANK:
										   		value = "";
										   		break;
										   	case XSSFCell.CELL_TYPE_ERROR:
										   		value = "" + cell.getErrorCellValue();
										   		break;
										   	default:
										}
										
										if(value != null && !value.equals("")) {
											value = value.trim();
										}
										int nIndex = cell.getColumnIndex();
									
									if(r==2) {
										//등록번호
										if(nIndex == 3){
											cell = row.getCell(3);
					        				cell.setCellValue(driver.get("business_license_number") != null ? driver.get("business_license_number").toString():"");
										}
										
										//한국카캐리어 소속이 아닌경우 소속된 회사가 공급받는자가 된다......
										if(!companyId.equals("company2")) {
											
											if(nIndex == 16){
												cell = row.getCell(nIndex);
												cell.setCellValue(businessLicenseNumberArr[0]);
											}
											if(nIndex == 17){
												cell = row.getCell(nIndex);
												cell.setCellValue(businessLicenseNumberArr[1]);
											}
											if(nIndex == 18){
												cell = row.getCell(nIndex);
												cell.setCellValue(businessLicenseNumberArr[2]);
											}
											
											if(nIndex == 21){
												cell = row.getCell(nIndex);
												cell.setCellValue(businessLicenseNumberArr[3]);
											}
											if(nIndex == 22){
												cell = row.getCell(nIndex);
												cell.setCellValue(businessLicenseNumberArr[4]);
											}
											
											if(nIndex == 25){
												cell = row.getCell(nIndex);
												cell.setCellValue(businessLicenseNumberArr[5]);
											}
											if(nIndex == 26){
												cell = row.getCell(nIndex);
												cell.setCellValue(businessLicenseNumberArr[6]);
											}
											if(nIndex == 27){
												cell = row.getCell(nIndex);
												cell.setCellValue(businessLicenseNumberArr[7]);
											}
											if(nIndex == 28){
												cell = row.getCell(nIndex);
												cell.setCellValue(businessLicenseNumberArr[8]);
											}
											if(nIndex == 29){
												cell = row.getCell(nIndex);
												cell.setCellValue(businessLicenseNumberArr[9]);
											}
											
										}
										
										
									}else if(r==3) {
										if(nIndex == 3){
											cell = row.getCell(3);
											cell.setCellValue(driver.get("business_name") != null ? driver.get("business_name").toString():"");
										}
										if(nIndex == 9){
											cell = row.getCell(9);
											cell.setCellValue(driver.get("owner_name") != null ? driver.get("owner_name").toString():"");
										}
										
										if(nIndex == 12){
											
											//직인이 등록 되어 있는경우 직인을 첨부 한다.
											if(driverSeal != null && driverSeal.size() > 0 && driverSeal.get(0).get("file_id") != null) {
												InputStream inputStream = new FileInputStream(rootDir+"/driverSeal"+driverSeal.get(0).get("file_path").toString());
									            byte[] bytes = IOUtils.toByteArray(inputStream);
									            int pictureIdx = wb.addPicture(bytes, XSSFWorkbook.PICTURE_TYPE_PNG);
									            inputStream.close();
									            XSSFCreationHelper helper = wb.getCreationHelper();
									            XSSFDrawing drawing = sheet.createDrawingPatriarch();
									            XSSFClientAnchor anchor = helper.createClientAnchor();
									            
									            // 이미지를 출력할 CELL 위치 선정
									            anchor.setCol1(nIndex);
									            anchor.setRow1(r);
									            
									            // 이미지 그리기
									            XSSFPicture pict = drawing.createPicture(anchor, pictureIdx);
									            
									            // 이미지 사이즈 비율 설정
									            pict.resize();
									            
									            
											}
											
											cell = row.getCell(9);
											cell.setCellValue(driver.get("owner_name") != null ? driver.get("owner_name").toString():"");
										}
										
										//한국카캐리어 소속이 아닌경우 소속된 회사가 공급받는자가 된다......
										if(!companyId.equals("company2")) {
											if(nIndex == 16){
												cell = row.getCell(nIndex);
												cell.setCellValue(companyName);
											}
											if(nIndex == 25){
												cell = row.getCell(nIndex);
												cell.setCellValue(companyOwnerName);
											}
										}
										
									}else if(r==4) {
										if(nIndex == 3){
											cell = row.getCell(3);
					        				cell.setCellValue(driver.get("address") != null ? driver.get("address").toString():""+" "+driver.get("address_detail") != null ? driver.get("address_detail").toString():"");
										}
										
										//한국카캐리어 소속이 아닌경우 소속된 회사가 공급받는자가 된다......
										if(!companyId.equals("company2")) {
											if(nIndex == 16){
												cell = row.getCell(nIndex);
												cell.setCellValue(address);
											}
										}
										
									}else if(r==5) {	
										if(nIndex == 3){
											cell = row.getCell(3);
					        				cell.setCellValue(driver.get("business_condition") != null ? driver.get("business_condition").toString():"");
										}
										if(nIndex == 9){
											cell = row.getCell(9);
					        				cell.setCellValue(driver.get("business_kind") != null ? driver.get("business_kind").toString():"");
										}
										
										//한국카캐리어 소속이 아닌경우 소속된 회사가 공급받는자가 된다......
										if(!companyId.equals("company2")) {
											if(nIndex == 16){
												cell = row.getCell(nIndex);
												cell.setCellValue(businessCondition);
											}
											
											if(nIndex == 23){
												cell = row.getCell(nIndex);
												cell.setCellValue(businessKind);
											}
											
										}
										
									}else if(r==8) {
										boolean startFlag = false;
										String decideMonth = paramMap.get("decideMonth").toString();
										
										if(nIndex == 0){
											cell = row.getCell(0);
					        				cell.setCellValue(decideMonth.split("-")[0].substring(2));
										}else if(nIndex == 1){
											cell = row.getCell(1);
					        				cell.setCellValue(decideMonth.split("-")[1]);
										}else if(nIndex == 2){
											cell = row.getCell(2);
					        				cell.setCellValue(WebUtils.getLastDate("yyyy-MM", Integer.parseInt(decideMonth.split("-")[0]), Integer.parseInt(decideMonth.split("-")[1])));
										}else if(nIndex == 3){
											char[] supplyVal = paramMap.get("supplyVal").toString().toCharArray(); 
											int empty = 10-supplyVal.length;
											cell = row.getCell(3);
					        				cell.setCellValue(String.valueOf(empty));
										}else if(nIndex == 4){
											char[] supplyVal = paramMap.get("supplyVal").toString().toCharArray(); 
											int len = supplyVal.length;
											len=len-1;
											for(int i = 13; i > 3; i--) {
												if(len >= 0) {
													if(len >= 0) {
														cell = row.getCell(i);
														cell.setCellValue(Character.toString(supplyVal[len]));
													}
													len=len-1;	
												}else {
													break;
												}
											}
											c = 13;	
										}else if(nIndex == 14){
											char[] vatVal = paramMap.get("vatVal").toString().toCharArray(); 
											int len = vatVal.length;
											len=len-1;
											for(int i = 22; i > 13; i--) {
												if(len >= 0) {
													if(len >= 0) {
														cell = row.getCell(i);
														cell.setCellValue(Character.toString(vatVal[len]));
													}
													len=len-1;
												}else {
													break;
												}
											}
											c = 23;	
										}
										
									}else if(r==10) {	
										if(nIndex == 2){
											cell = row.getCell(2);
					        				cell.setCellValue(paramMap.get("decideMonth").toString().split("-")[1]+"월 운송료");
										}else if(nIndex == 18){
											cell = row.getCell(18);
					        				cell.setCellValue(Integer.parseInt(paramMap.get("supplyVal").toString()));
										}else if(nIndex == 23){
											cell = row.getCell(23);
					        				cell.setCellValue(Integer.parseInt(paramMap.get("vatVal").toString()));
										}
									}
										
				        			} else {
				        				//System.out.print("[null]\t");
				        			}
				        		} // for(c) 문
				        		

				        	}
				        	
				        }
					
					
				}
		    	
				 
					XSSFFormulaEvaluator.evaluateAllFormulaCells(wb);
					DriverBillingFileVO driverBillingFileVO = new DriverBillingFileVO();
					fileMap = fileUploadService.upload(rootDir, "billing_list","("+paramMap.get("decideMonth").toString().split("-")[1]+"월 공급자보관용)"+driver.get("driver_name").toString()+".xlsx", wb);
					driverBillingFileVO.setCategoryType("billing_list");
					driverBillingFileVO.setDecideMonth(paramMap.get("decideMonth").toString());
					driverBillingFileVO.setDriverBillingFileId("FLE"+UUID.randomUUID().toString().replaceAll("-", ""));
					driverBillingFileVO.setDriverBillingFileNm(fileMap.get("ORG_NAME").toString());
					driverBillingFileVO.setDriverBillingFilePath(fileMap.get("SAVE_NAME").toString());
					driverBillingFileVO.setDriverId(paramMap.get("driverId").toString());
					driverBillingFileVO.setBillingDivision("P");
					this.insertDriverBillingFile(driverBillingFileVO);
			
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return fileMap;
		
	}
	
	
public Map<String, Object> makeDriverBillingFileOtherSide(Map<String, Object> paramMap) throws Exception{
		
		
		Map<String, Object> fileMap = new HashMap<String, Object>();
		
		try {
		
			 	File file = new File(rootDir+"/forms", "billing_otherside.xlsx");
		        XSSFWorkbook wb = null;
		        
		    	XSSFRow row;
				XSSFCell cell;
				try {//엑셀 파일 오픈
					wb = new XSSFWorkbook(new FileInputStream(file));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
		    	
				XSSFSheet sheet = wb.getSheetAt(0);         
		        
				CellStyle style = wb.createCellStyle();
				style.setFillForegroundColor(IndexedColors.PALE_BLUE.getIndex());
		        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		        style.setAlignment(CellStyle.ALIGN_CENTER);
		        style.setBorderBottom(CellStyle.BORDER_DOUBLE);
		        style.setBorderTop(CellStyle.BORDER_DOUBLE);
		        style.setBorderLeft(CellStyle.BORDER_DOUBLE);
		        style.setBorderRight(CellStyle.BORDER_DOUBLE);
				
		        int rows = sheet.getLastRowNum();
				int cells = sheet.getRow(0).getLastCellNum();
				
				
				FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
				
				Map<String, Object> map = new HashMap<String, Object>();
		    	map.put("driverId", paramMap.get("driverId").toString());
				//map.put("driverId", "caosys");
		    	
		    	map.put("forUpdatePassWord", "");
		    	Map<String, Object> driver = driverService.selectDriver(map);
				
		    	String companyId = "";
		    	
		    	//소속이 지정 되어 있으면
		    	if(driver.get("assign_company") != null && !driver.get("assign_company").toString().equals("")) {
		    		companyId = driver.get("assign_company").toString();
		    	}else {
		    		companyId = "company2";
		    	}
		    	
		    	Map<String, Object> searchMap = new HashMap<String, Object>();
		    	searchMap.put("companyId", companyId);
		    	Map<String, Object> companyInfo = companyService.selectCompany(searchMap);
		    	
		    	String businessLicenseNumber = companyInfo.get("business_license_number").toString().replaceAll("-", "");	//사업자등록번호
		    	String[] businessLicenseNumberArr = businessLicenseNumber.split("");
		    	String companyName = companyInfo.get("company_name").toString().replaceAll("-", "");	//상호
		    	String companyOwnerName = companyInfo.get("company_owner_name").toString().replaceAll("-", "");	//대표자명
		    	String address = companyInfo.get("address").toString().replaceAll("-", "")+" "+companyInfo.get("address_detail").toString().replaceAll("-", "");	//주소
		    	String businessCondition = companyInfo.get("business_condition").toString().replaceAll("-", "");	//업태
		    	String businessKind = companyInfo.get("business_kind").toString().replaceAll("-", "");	//종목
		    	
		    	map.put("contentId", paramMap.get("driverId").toString());
          		map.put("categoryType", "driverSeal");
				List<Map<String, Object>> driverSeal = fileService.selectFileList(map);
		    	
		    	
				if(driver != null && driver.get("driver_id") != null) {
					
					 for (int r = 2; r <= rows; r++) {
						 
				        	row = sheet.getRow(r); // row 가져오기
				        	if (row != null) {
				        		for (int c = 0; c < cells; c++) {
				        			cell = row.getCell(c);
				        			if (cell != null) {
				        				String value = "";
										switch (cell.getCellType()) {
										   	case XSSFCell.CELL_TYPE_FORMULA:
										   		if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_NUMERIC){
										   			value = "" + (int)cell.getNumericCellValue();
										   		}else if(evaluator.evaluateFormulaCell(cell)==XSSFCell.CELL_TYPE_STRING){
										   			value = "" + cell.getStringCellValue(); 
										   		}
										   		value = cell.getCellFormula();
										   		break;
										   	case XSSFCell.CELL_TYPE_NUMERIC:
										   		value = "" + (int)cell.getNumericCellValue();
										   		break;
										   	case XSSFCell.CELL_TYPE_STRING:
										   		value = "" + cell.getStringCellValue();
										   		break;
										   	case XSSFCell.CELL_TYPE_BLANK:
										   		value = "";
										   		break;
										   	case XSSFCell.CELL_TYPE_ERROR:
										   		value = "" + cell.getErrorCellValue();
										   		break;
										   	default:
										}
										
										if(value != null && !value.equals("")) {
											value = value.trim();
										}
										int nIndex = cell.getColumnIndex();
									
									if(r==2) {
										//등록번호
										if(nIndex == 3){
											cell = row.getCell(3);
					        				cell.setCellValue(driver.get("business_license_number") != null ? driver.get("business_license_number").toString():"");
										}
										
										//한국카캐리어 소속이 아닌경우 소속된 회사가 공급받는자가 된다......
										if(!companyId.equals("company2")) {
											
											if(nIndex == 16){
												cell = row.getCell(nIndex);
												cell.setCellValue(businessLicenseNumberArr[0]);
											}
											if(nIndex == 17){
												cell = row.getCell(nIndex);
												cell.setCellValue(businessLicenseNumberArr[1]);
											}
											if(nIndex == 18){
												cell = row.getCell(nIndex);
												cell.setCellValue(businessLicenseNumberArr[2]);
											}
											
											if(nIndex == 21){
												cell = row.getCell(nIndex);
												cell.setCellValue(businessLicenseNumberArr[3]);
											}
											if(nIndex == 22){
												cell = row.getCell(nIndex);
												cell.setCellValue(businessLicenseNumberArr[4]);
											}
											
											if(nIndex == 25){
												cell = row.getCell(nIndex);
												cell.setCellValue(businessLicenseNumberArr[5]);
											}
											if(nIndex == 26){
												cell = row.getCell(nIndex);
												cell.setCellValue(businessLicenseNumberArr[6]);
											}
											if(nIndex == 27){
												cell = row.getCell(nIndex);
												cell.setCellValue(businessLicenseNumberArr[7]);
											}
											if(nIndex == 28){
												cell = row.getCell(nIndex);
												cell.setCellValue(businessLicenseNumberArr[8]);
											}
											if(nIndex == 29){
												cell = row.getCell(nIndex);
												cell.setCellValue(businessLicenseNumberArr[9]);
											}
											
										}
										
										
									}else if(r==3) {
										if(nIndex == 3){
											cell = row.getCell(3);
											cell.setCellValue(driver.get("business_name") != null ? driver.get("business_name").toString():"");
										}
										if(nIndex == 9){
											cell = row.getCell(9);
											cell.setCellValue(driver.get("owner_name") != null ? driver.get("owner_name").toString():"");
										}
										
										
										if(nIndex == 12){
											
											//직인이 등록 되어 있는경우 직인을 첨부 한다.
											if(driverSeal != null && driverSeal.size() > 0 && driverSeal.get(0).get("file_id") != null) {
												InputStream inputStream = new FileInputStream(rootDir+"/driverSeal"+driverSeal.get(0).get("file_path").toString());
									            byte[] bytes = IOUtils.toByteArray(inputStream);
									            int pictureIdx = wb.addPicture(bytes, XSSFWorkbook.PICTURE_TYPE_JPEG);
									            inputStream.close();
									            XSSFCreationHelper helper = wb.getCreationHelper();
									            XSSFDrawing drawing = sheet.createDrawingPatriarch();
									            XSSFClientAnchor anchor = helper.createClientAnchor();
									            
									            // 이미지를 출력할 CELL 위치 선정
									            anchor.setCol1(nIndex);
									            anchor.setRow1(r);
									            
									            // 이미지 그리기
									            XSSFPicture pict = drawing.createPicture(anchor, pictureIdx);
									            
									            // 이미지 사이즈 비율 설정
									            pict.resize();
									            
									            
											}
										
										}
										
										//한국카캐리어 소속이 아닌경우 소속된 회사가 공급받는자가 된다......
										if(!companyId.equals("company2")) {
											if(nIndex == 16){
												cell = row.getCell(nIndex);
												cell.setCellValue(companyName);
											}
											if(nIndex == 25){
												cell = row.getCell(nIndex);
												cell.setCellValue(companyOwnerName);
											}
										}
										
										
									}else if(r==4) {
										if(nIndex == 3){
											cell = row.getCell(3);
					        				cell.setCellValue(driver.get("address") != null ? driver.get("address").toString():""+" "+driver.get("address_detail") != null ? driver.get("address_detail").toString():"");
										}
										
										//한국카캐리어 소속이 아닌경우 소속된 회사가 공급받는자가 된다......
										if(!companyId.equals("company2")) {
											if(nIndex == 16){
												cell = row.getCell(nIndex);
												cell.setCellValue(address);
											}
										}
										
									}else if(r==5) {	
										if(nIndex == 3){
											cell = row.getCell(3);
					        				cell.setCellValue(driver.get("business_condition") != null ? driver.get("business_condition").toString():"");
										}
										if(nIndex == 9){
											cell = row.getCell(9);
					        				cell.setCellValue(driver.get("business_kind") != null ? driver.get("business_kind").toString():"");
										}
										
										//한국카캐리어 소속이 아닌경우 소속된 회사가 공급받는자가 된다......
										if(!companyId.equals("company2")) {
											if(nIndex == 16){
												cell = row.getCell(nIndex);
												cell.setCellValue(businessCondition);
											}
											
											if(nIndex == 23){
												cell = row.getCell(nIndex);
												cell.setCellValue(businessKind);
											}
											
										}
										
									}else if(r==8) {
										boolean startFlag = false;
										String decideMonth = paramMap.get("decideMonth").toString();
										
										if(nIndex == 0){
											cell = row.getCell(0);
					        				cell.setCellValue(decideMonth.split("-")[0].substring(2));
										}else if(nIndex == 1){
											cell = row.getCell(1);
					        				cell.setCellValue(decideMonth.split("-")[1]);
										}else if(nIndex == 2){
											cell = row.getCell(2);
					        				cell.setCellValue(WebUtils.getLastDate("yyyy-MM", Integer.parseInt(decideMonth.split("-")[0]), Integer.parseInt(decideMonth.split("-")[1])));
										}else if(nIndex == 3){
											char[] supplyVal = paramMap.get("supplyVal").toString().toCharArray(); 
											int empty = 10-supplyVal.length;
											cell = row.getCell(3);
					        				cell.setCellValue(String.valueOf(empty));
										}else if(nIndex == 4){
											char[] supplyVal = paramMap.get("supplyVal").toString().toCharArray(); 
											int len = supplyVal.length;
											len=len-1;
											for(int i = 13; i > 3; i--) {
												if(len >= 0) {
													if(len >= 0) {
														cell = row.getCell(i);
														cell.setCellValue(Character.toString(supplyVal[len]));
													}
													len=len-1;	
												}else {
													break;
												}
											}
											c = 13;	
										}else if(nIndex == 14){
											char[] vatVal = paramMap.get("vatVal").toString().toCharArray(); 
											int len = vatVal.length;
											len=len-1;
											for(int i = 22; i > 13; i--) {
												if(len >= 0) {
													if(len >= 0) {
														cell = row.getCell(i);
														cell.setCellValue(Character.toString(vatVal[len]));
													}
													len=len-1;
												}else {
													break;
												}
											}
											c = 23;	
										}
										
									}else if(r==10) {	
										if(nIndex == 2){
											cell = row.getCell(2);
					        				cell.setCellValue(paramMap.get("decideMonth").toString().split("-")[1]+"월 운송료");
										}else if(nIndex == 18){
											cell = row.getCell(18);
					        				cell.setCellValue(Integer.parseInt(paramMap.get("supplyVal").toString()));
										}else if(nIndex == 23){
											cell = row.getCell(23);
					        				cell.setCellValue(Integer.parseInt(paramMap.get("vatVal").toString()));
										}
									}
										
				        			} else {
				        				//System.out.print("[null]\t");
				        			}
				        		} // for(c) 문
				        		

				        	}
				        	
				        }
					
					
				}
		    	
				 
					XSSFFormulaEvaluator.evaluateAllFormulaCells(wb);
					DriverBillingFileVO driverBillingFileVO = new DriverBillingFileVO();
					fileMap = fileUploadService.upload(rootDir, "billing_list","("+paramMap.get("decideMonth").toString().split("-")[1]+"월 공급받는자보관용)"+driver.get("driver_name").toString()+".xlsx", wb);
					driverBillingFileVO.setCategoryType("billing_list");
					driverBillingFileVO.setDecideMonth(paramMap.get("decideMonth").toString());
					driverBillingFileVO.setDriverBillingFileId("FLE"+UUID.randomUUID().toString().replaceAll("-", ""));
					driverBillingFileVO.setDriverBillingFileNm(fileMap.get("ORG_NAME").toString());
					driverBillingFileVO.setDriverBillingFilePath(fileMap.get("SAVE_NAME").toString());
					driverBillingFileVO.setDriverId(paramMap.get("driverId").toString());
					driverBillingFileVO.setBillingDivision("O");
					
					this.insertDriverBillingFile(driverBillingFileVO);
			
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return fileMap;
		
	}
	
	
	
	
	
	
	
}
