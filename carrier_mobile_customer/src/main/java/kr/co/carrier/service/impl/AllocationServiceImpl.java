package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.AllocationMapper;
import kr.co.carrier.service.AllocationService;
import kr.co.carrier.vo.AllocationVO;

@Service("allocationService")
public class AllocationServiceImpl implements AllocationService{

	@Resource(name="allocationMapper")
	private AllocationMapper allocationMapper;
	
	
	public Map<String, Object> selectAllocation(Map<String, Object> map) throws Exception{
		return allocationMapper.selectAllocation(map);
	}
	
	public List<Map<String, Object>> selectAllocationList(Map<String, Object> map) throws Exception{
		return allocationMapper.selectAllocationList(map);
	}
	
	public int selectAllocationListCount(Map<String, Object> map) throws Exception{
		return allocationMapper.selectAllocationListCount(map);
	}
	
	public int insertAllocation(AllocationVO allocationVO) throws Exception{
		return allocationMapper.insertAllocation(allocationVO);
	}
	
	public void updateAllocationListOrder(Map<String, Object> map) throws Exception{
		allocationMapper.updateAllocationListOrder(map);
	}
	
	public void updateAllocation(AllocationVO allocationVO) throws Exception{
		allocationMapper.updateAllocation(allocationVO);
	}
	
	public Map<String, Object> selectAllocationByListOrder(Map<String, Object> map) throws Exception{
		return allocationMapper.selectAllocationByListOrder(map);
	}
	
	public void updateAllocationByMap(Map<String, Object> map) throws Exception{
		allocationMapper.updateAllocationByMap(map);
	}
	
	public void updateAllocationStatus(Map<String, Object> map) throws Exception{
		allocationMapper.updateAllocationStatus(map);
	}
	
	public int selectAllocationMainListCount(Map<String, Object> map) throws Exception{
		return allocationMapper.selectAllocationMainListCount(map);
	}
	
	public void updateAllocationForSendReceipt(Map<String, Object> map) throws Exception{
		allocationMapper.updateAllocationForSendReceipt(map);
	}
	
	public void updateAllocationReceiptSkip(Map<String, Object> map) throws Exception{
		allocationMapper.updateAllocationReceiptSkip(map);
	}
	
	public void updateAllocationReceiptSkipReason(Map<String, Object> map) throws Exception{
		allocationMapper.updateAllocationReceiptSkipReason(map);
	}
	
	public List<Map<String, Object>> selectAllocationListGroup(Map<String, Object> map) throws Exception{
		return allocationMapper.selectAllocationListGroup(map);
	}
	
	public List<Map<String, Object>> selectAllocationListByDepartureDtAndDriverCnt(Map<String, Object> map) throws Exception{
		return allocationMapper.selectAllocationListByDepartureDtAndDriverCnt(map);
	}
	
	public List<Map<String, Object>> selectAllocationListForCal(Map<String, Object> map) throws Exception{
		return allocationMapper.selectAllocationListForCal(map);
	}
	
	public void updateAllocationPicturePointerR(Map<String, Object> map) throws Exception{
		allocationMapper.updateAllocationPicturePointerR(map);
	}
	public void updateAllocationPicturePointerD(Map<String, Object> map) throws Exception{
		allocationMapper.updateAllocationPicturePointerD(map);
	}
	public void updateAllocationPicturePointerI(Map<String, Object> map) throws Exception{
		allocationMapper.updateAllocationPicturePointerI(map);
	}
	
	public int selectAllocationCountByPicturePointer(Map<String, Object> map) throws Exception{
		return allocationMapper.selectAllocationCountByPicturePointer(map);
	}
	
	public List<Map<String, Object>> selectAllocationListByBatchStatusId(Map<String, Object> map) throws Exception{
		return allocationMapper.selectAllocationListByBatchStatusId(map);
	}
	
	public void updateMissingSend (Map<String,Object>map) throws Exception{
		allocationMapper.updateMissingSend(map);
	}

	public void deleteAllocation(Map<String, Object> map) throws Exception{
		allocationMapper.deleteAllocation(map);
	}
	public void deleteCarInfo(Map<String, Object> map) throws Exception{
		allocationMapper.deleteCarInfo(map);
	}
	public void deletePaymentInfo(Map<String, Object> map) throws Exception{
		allocationMapper.deletePaymentInfo(map);
	}
	public void deleteMileage(Map<String, Object> map) throws Exception{
		allocationMapper.deleteMileage(map);
	}
	
	
	public Map<String, Object> selectFindCompanyIdForCustomerApp(Map<String, Object> map) throws Exception{
		return allocationMapper.selectFindCompanyIdForCustomerApp(map);
	}
	
	public int insertConsignmentReservationTemp(Map<String, Object> map) throws Exception{
		return allocationMapper.insertConsignmentReservationTemp(map);
	}
	
	public Map<String, Object> selectTempConsignmentReservation(Map<String, Object> map) throws Exception{
		return allocationMapper.selectTempConsignmentReservation(map);
	}
	
	 public String paymentPrice(String distance) throws Exception{
		 
		
			
		// int test =	 Integer.parseInt(String.valueOf(Math.round(finalDistance)));
		 
		 
		 
		 
		 
		 int test =  Integer.parseInt(distance);
		 /*
	
		if(0<test && test <9999) {
			
				test = 50000;
	
			}else if(10000<= test && test<19999){
				
				test = 60000;
			}else if(20000<= test && test<29999){
			
				test = 70000;
			}else if(30000<= test && test<39999){
				
				test = 80000;
			}else if(40000<= test && test<49999){
				
				test = 90000;
				
			}else if(50000<= test && test <99999) {
				
			    double finalDistance = Math.ceil(test/1000);
				int highPrice =Integer.parseInt(String.valueOf(Math.round(finalDistance)));
				test = highPrice * 2000;
				
			}else if(100000<= test && test <149999) {
		   
				double finalDistance = Math.ceil(test/1000);
				int highPrice =Integer.parseInt(String.valueOf(Math.round(finalDistance)));
				test = highPrice * 1600;
								
			}else if(150000<= test && test<199999) {
				
				double finalDistance = Math.ceil(test/1000);
				int highPrice =Integer.parseInt(String.valueOf(Math.round(finalDistance)));
				test = highPrice * 1500;
				
			}else if(200000<= test && test<249999){
				
				double finalDistance = Math.ceil(test/1000);
				int highPrice =Integer.parseInt(String.valueOf(Math.round(finalDistance)));
				test = highPrice * 1450;
				
			}else if(250000<= test && test<299999) {
				
				double finalDistance = Math.ceil(test/1000);
				int highPrice =Integer.parseInt(String.valueOf(Math.round(finalDistance)));
				test = highPrice * 1400;
				
			}else if(300000<= test && test<349999) {
				
				double finalDistance = Math.ceil(test/1000);
				int highPrice =Integer.parseInt(String.valueOf(Math.round(finalDistance)));
				test = highPrice * 1350;
				
			}else if(350000<= test && test<399999) {
				
				double finalDistance = Math.ceil(test/1000);
				int highPrice =Integer.parseInt(String.valueOf(Math.round(finalDistance)));
				test = highPrice * 1300;
				
			}else if(400000<= test && test<449999) {
				
				double finalDistance = Math.ceil(test/1000);
				int highPrice =Integer.parseInt(String.valueOf(Math.round(finalDistance)));
				test = highPrice * 1250;
				
			}else if(450000<= test && test<499999) {
				
				double finalDistance = Math.ceil(test/1000);
				int highPrice =Integer.parseInt(String.valueOf(Math.round(finalDistance)));
				test = highPrice * 1200;
				
			}else if(500000<= test && test<549999) {
				
				double finalDistance = Math.ceil(test/1000);
				int highPrice =Integer.parseInt(String.valueOf(Math.round(finalDistance)));
				test = highPrice * 1150;
				
			}else if(550000<= test && test<599999) {
				
				double finalDistance = Math.ceil(test/1000);
				int highPrice =Integer.parseInt(String.valueOf(Math.round(finalDistance)));
				test = highPrice * 1100;
				
			}
		
			if(100000 <= test && test <= 200000 ) {
	        	  test = test+20000;
	        }
			*/
		 
		 
		 
		 
		 
		 	if(0 < test && test <= 10000) {
				test = 50000;
			}else if(10000 < test && test <= 15000){
				test = 60000;
			}else if(15000 < test && test <= 20000){
				test = 70000;
			}else if(20000 < test && test <= 25000){
				test = 80000;
			}else if(25000 < test && test <= 30000){
				test = 90000;
			}else if(30000 < test && test <= 35000) {
				test = 100000;
			}else if(35000 < test && test <= 40000) {
				test = 110000;
			}else if(40000 < test && test <= 45000) {
				test = 120000;
			}else if(45000 < test && test <= 50000) {
				test = 130000;
			}else if(50000 < test && test <= 55000) {
				test = 140000;
			}else if(55000 < test && test <= 60000) {
				test = 150000;
			}else if(60000 < test && test <= 65000) {
				test = 160000;
			}else if(65000 < test && test <= 70000) {
				test = 170000;
			}else if(70000 < test && test <= 75000) {
				test = 180000;
			}else if(75000 < test && test <= 80000) {
				test = 190000;
			}else if(80000 < test && test <= 90000) {
				test = 200000;
			}else if(90000 < test && test <= 100000) {
				test = 210000;
			}else if(100000 < test && test <= 110000) {
				test = 220000;
			}else if(110000 < test && test <= 120000) {
				test = 230000;
			}else if(120000 < test && test <= 130000) {
				test = 240000;
			}else if(130000 < test && test <= 140000) {
				test = 250000;
			}else if(140000 < test && test <= 150000) {
				test = 260000;
			}else if(150000 < test && test <= 160000) {
				test = 270000;
			}else if(160000 < test && test <= 170000) {
				test = 280000;
			}else if(170000 < test && test <= 180000) {
				test = 290000;
			}else if(180000 < test && test <= 190000) {
				test = 300000;
			}else if(190000 < test && test <= 200000) {
				test = 310000;
			}else if(200000 < test && test <= 210000) {
				test = 320000;
			}else if(210000 < test && test <= 220000) {
				test = 330000;
			}else if(220000 < test && test <= 230000) {
				test = 340000;
			}else if(230000 < test && test <= 240000) {
				test = 350000;
			}else if(240000 < test && test <= 250000) {
				test = 360000;
			}else if(250000 < test && test <= 260000) {
				test = 370000;
			}else if(260000 < test && test <= 270000) {
				test = 380000;
			}else if(270000 < test && test <= 280000) {
				test = 390000;
			}else if(280000 < test && test <= 290000) {
				test = 400000;
			}else if(290000 < test && test <= 300000) {
				test = 410000;
			}else if(300000 < test && test <= 310000) {
				test = 420000;
			}else if(310000 < test && test <= 320000) {
				test = 430000;
			}else if(320000 < test && test <= 330000) {
				test = 440000;
			}else if(330000 < test && test <= 340000) {
				test = 450000;
			}else if(340000 < test && test <= 350000) {
				test = 460000;
			}else if(350000 < test && test <= 360000) {
				test = 470000;
			}else if(360000 < test && test <= 370000) {
				test = 480000;
			}else if(370000 < test && test <= 380000) {
				test = 490000;
			}else if(380000 < test && test <= 390000) {
				test = 500000;
			}else if(390000 < test && test <= 400000) {
				test = 510000;
			}else if(400000 < test && test <= 410000) {
				test = 520000;
			}else if(410000 < test && test <= 420000) {
				test = 530000;
			}else if(420000 < test && test <= 430000) {
				test = 540000;
			}else if(430000 < test && test <= 440000) {
				test = 550000;
			}else if(440000 < test && test <= 450000) {
				test = 560000;
			}else if(450000 < test && test <= 460000) {
				test = 570000;
			}else if(460000 < test && test <= 470000) {
				test = 580000;
			}else if(470000 < test && test <= 480000) {
				test = 590000;
			}else if(480000 < test && test <= 490000) {
				test = 600000;
			}else if(490000 < test && test <= 500000) {
				test = 610000;
			}else if(500000 < test && test <= 510000) {
				test = 620000;
			}else if(510000 < test && test <= 520000) {
				test = 630000;
			}else if(520000 < test && test <= 530000) {
				test = 640000;
			}else if(530000 < test && test <= 540000) {
				test = 650000;
			}else if(540000 < test && test <= 550000) {
				test = 660000;
			}else if(550000 < test && test <= 560000) {
				test = 670000;
			}else if(560000 < test && test <= 570000) {
				test = 680000;
			}else if(570000 < test && test <= 580000) {
				test = 690000;
			}else if(580000 < test && test <= 590000) {
				test = 700000;
			}else if(590000 < test && test <= 600000) {
				test = 710000;
			}
		 	
		 	/*
			if(100000 <= test && test <= 200000 ) {
	        	  test = test+20000;
	        }
		 	*/
		 
		 String payment =String.valueOf(test);
		
		 return payment;
	 }
	
	
}
