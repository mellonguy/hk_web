package kr.co.carrier.service;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.EmployeeVO;

public interface EmployeeService {

	
	public Map<String, Object> selectEmployee(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectEmployeeList(Map<String, Object> map) throws Exception;
	public int selectEmployeeListCount(Map<String, Object> map) throws Exception;
	public int insertEmployee(EmployeeVO employeeVO) throws Exception;
	public void updateEmployee(EmployeeVO employeeVO) throws Exception;
	public void updateEmployeeApproveYn(Map<String, Object> map) throws Exception;
	public void updateEmployeeRole(Map<String, Object> map) throws Exception;
	public void updateEmployeeAllocation(Map<String, Object> map) throws Exception;
	public void updateEmployeeStatus(Map<String, Object> map) throws Exception;
	
}
