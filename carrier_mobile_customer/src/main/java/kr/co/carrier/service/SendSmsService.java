package kr.co.carrier.service;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.SendSmsVO;

public interface SendSmsService {

	
	
	public Map<String, Object> selectSendSms(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectSendSmsList(Map<String, Object> map) throws Exception;
	public int selectSendSmsListCount(Map<String, Object> map) throws Exception;
	public int insertSendSms(SendSmsVO sendSmsVO) throws Exception;
	public int insertSendSms(SendSmsVO sendSmsVO,Map<String, Object> map) throws Exception;
	public void deleteSendSms(Map<String, Object> map) throws Exception;
	public int insertSendCertNumForApp(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectCheckCertNumForLogin(Map<String, Object> map) throws Exception; 
	
	
	
	
	
	
	
}
