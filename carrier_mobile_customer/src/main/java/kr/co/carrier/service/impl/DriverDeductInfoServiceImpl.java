package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.DriverDeductInfoMapper;
import kr.co.carrier.service.DriverDeductInfoService;
import kr.co.carrier.vo.DriverDeductInfoVO;

@Service("driverDeductInfoService")
public class DriverDeductInfoServiceImpl implements DriverDeductInfoService{

	
	
	@Resource(name="driverDeductInfoMapper")
	private DriverDeductInfoMapper driverDeductInfoMapper;
	
	
	public Map<String, Object> selectDriverDeductInfo(Map<String, Object> map) throws Exception{
		return driverDeductInfoMapper.selectDriverDeductInfo(map);
	}
	
	public List<Map<String, Object>> selectDriverDeductInfoList(Map<String, Object> map) throws Exception{
		return driverDeductInfoMapper.selectDriverDeductInfoList(map);
	}
	
	public int selectDriverDeductInfoListCount(Map<String, Object> map) throws Exception{
		return driverDeductInfoMapper.selectDriverDeductInfoListCount(map);
	}
	
	public int insertDriverDeductInfo(DriverDeductInfoVO driverDeductInfoVO) throws Exception{
		return driverDeductInfoMapper.insertDriverDeductInfo(driverDeductInfoVO);
	}
	
	public void deleteDriverDeductInfo(Map<String, Object> map) throws Exception{
		driverDeductInfoMapper.deleteDriverDeductInfo(map);
	}
	
	public void updateDriverDeductInfo(DriverDeductInfoVO driverDeductInfoVO) throws Exception{
		driverDeductInfoMapper.updateDriverDeductInfo(driverDeductInfoVO);
	}
	
	public Map<String, Object> selectDriverDeductInfoByItem(Map<String, Object> map) throws Exception{
		return driverDeductInfoMapper.selectDriverDeductInfoByItem(map);
	}
	
	public List<Map<String, Object>> selectDriverDeductInfoListByItem(Map<String, Object> map) throws Exception{
		return driverDeductInfoMapper.selectDriverDeductInfoListByItem(map);
	}
	
	public List<Map<String, Object>> selectDriverList(Map<String, Object> map) throws Exception{
		return driverDeductInfoMapper.selectDriverList(map);
	}
	public int selectMonthDriverDeductListCount(Map<String, Object> map) throws Exception{
		return driverDeductInfoMapper.selectMonthDriverDeductListCount(map);
	}
	
	public List<Map<String, Object>> selectMonthDriverDeductList(Map<String, Object> map) throws Exception{
		return driverDeductInfoMapper.selectMonthDriverDeductList(map);
	}
	
	
	
	
}
