package kr.co.carrier.service;

import java.util.List;
import java.util.Map;

public interface BillingDivisionService {

	
	public List<Map<String, Object>>selectBillingDivisionInfoList() throws Exception;
	
	
}
