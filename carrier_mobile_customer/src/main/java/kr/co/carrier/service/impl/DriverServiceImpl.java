package kr.co.carrier.service.impl;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.DriverMapper;
import kr.co.carrier.service.DriverService;
import kr.co.carrier.vo.DriverVO;

@Service("driverService")
public class DriverServiceImpl implements DriverService{

	@Resource(name="driverMapper")
	private DriverMapper driverMapper;
	
	public Map<String, Object> selectDriver(Map<String, Object> map) throws Exception{
		return driverMapper.selectDriver(map);
	}
	
	public List<Map<String, Object>> selectDriverList(Map<String, Object> map) throws Exception{
		return driverMapper.selectDriverList(map);
	}
	
	public int selectDriverListCount(Map<String, Object> map) throws Exception{
		return driverMapper.selectDriverListCount(map);
	}
	
	public int insertDriver(DriverVO driverVO) throws Exception{
		return driverMapper.insertDriver(driverVO);
	}
	
	public void updateDriver(DriverVO driverVO) throws Exception{
		driverMapper.updateDriver(driverVO);
	}
	
	public void updateDriverDeviceToken(Map<String, Object> map) throws Exception{
		driverMapper.updateDriverDeviceToken(map);
	}
	
	public void updateDriverLatLng(Map<String, Object> map) throws Exception{
		driverMapper.updateDriverLatLng(map);
	}
	
	public void updateDriverPassWord(Map<String, Object> map) throws Exception{
		driverMapper.updateDriverPassWord(map);
	}
	
	public List<Map<String, Object>> selectAllEmployee(Map<String, Object> map) throws Exception{
		return driverMapper.selectAllEmployee(map);
	}
	
	public List<Map<String, Object>> selectAllDriver(Map<String, Object> map) throws Exception{
		return driverMapper.selectAllDriver(map);
	}
	
	public List<Map<String, Object>> selectAllEmp(Map<String, Object> map) throws Exception{
		return driverMapper.selectAllEmp(map);
	}
	
	
	
	
	
	
}
