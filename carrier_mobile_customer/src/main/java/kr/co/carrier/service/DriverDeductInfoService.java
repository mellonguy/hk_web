package kr.co.carrier.service;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.DriverDeductInfoVO;

public interface DriverDeductInfoService {
	
	
	public List<Map<String, Object>> selectDriverList(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectDriverDeductInfo(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectDriverDeductInfoList(Map<String, Object> map) throws Exception;
	public int selectDriverDeductInfoListCount(Map<String, Object> map) throws Exception;
	public int insertDriverDeductInfo(DriverDeductInfoVO driverDeductInfoVO) throws Exception;
	public void deleteDriverDeductInfo(Map<String, Object> map) throws Exception;
	public void updateDriverDeductInfo(DriverDeductInfoVO driverDeductInfoVO) throws Exception;
	public Map<String, Object> selectDriverDeductInfoByItem(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectDriverDeductInfoListByItem(Map<String, Object> map) throws Exception;
	public int selectMonthDriverDeductListCount(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectMonthDriverDeductList(Map<String, Object> map) throws Exception;
	
	
	
	
	
}
