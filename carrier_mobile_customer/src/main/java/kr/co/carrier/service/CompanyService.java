package kr.co.carrier.service;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.CompanyVO;

public interface CompanyService {

	
	
	public Map<String, Object> selectCompany(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectCompanyList(Map<String, Object> map) throws Exception;
	public int selectCompanyListCount(Map<String, Object> map) throws Exception;
	public int insertCompany(CompanyVO companyVO) throws Exception;
	public void updateCompany(CompanyVO companyVO) throws Exception;
	
	
	
	
}
