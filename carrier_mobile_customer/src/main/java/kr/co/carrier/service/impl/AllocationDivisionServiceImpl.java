package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.AllocationDivisionMapper;
import kr.co.carrier.service.AllocationDivisionService;

@Service("allocationDivisionService")
public class AllocationDivisionServiceImpl implements AllocationDivisionService{

	
	@Resource(name="allocationDivisionMapper")
	private AllocationDivisionMapper allocationDivisionMapper;
	
	
	public List<Map<String, Object>>selectAllocationDivisionInfoList() throws Exception{
		return allocationDivisionMapper.selectAllocationDivisionInfoList();
	}
}
