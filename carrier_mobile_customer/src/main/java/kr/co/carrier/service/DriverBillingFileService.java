package kr.co.carrier.service;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.DriverBillingFileVO;

public interface DriverBillingFileService {

	
	
	public Map<String, Object> selectDriverBillingFile(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectDriverBillingFileList(Map<String, Object> map) throws Exception;
	public int selectDriverBillingFileListCount(Map<String, Object> map) throws Exception;
	public int insertDriverBillingFile(DriverBillingFileVO driverBillingFileVO) throws Exception;
	public void deleteDriverBillingFile(Map<String, Object> map) throws Exception;
	public Map<String, Object> makeDriverBillingFile(Map<String, Object> map) throws Exception;
	public Map<String, Object> makeDriverBillingFileOtherSide(Map<String, Object> map) throws Exception;
	
	
	
	
	
	
	
	
	
	
}
