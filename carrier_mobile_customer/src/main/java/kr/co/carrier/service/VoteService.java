package kr.co.carrier.service;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.VoteVO;

public interface VoteService {

	
	
	
	public Map<String, Object> selectVote(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectVoteList(Map<String, Object> map) throws Exception;
	public int selectVoteListCount(Map<String, Object> map) throws Exception;
	public int insertVote(VoteVO voteVO) throws Exception;
	public void deleteVote(Map<String, Object> map) throws Exception;
	
	public int selectVoteCount(Map<String, Object> map) throws Exception;
	
	public List<Map<String, Object>> selectCandidateListA(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectCandidateListO(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectCandidateListD(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectCandidateListS(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectCandidateListC(Map<String, Object> map) throws Exception;
	
	
	
	
	
	
}
