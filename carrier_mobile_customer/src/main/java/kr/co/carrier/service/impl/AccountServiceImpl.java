package kr.co.carrier.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.AccountMapper;
import kr.co.carrier.service.AccountService;
import kr.co.carrier.vo.AccountVO;

@Service("accountService")
public class AccountServiceImpl implements AccountService{

	
	@Resource(name="accountMapper")
	private AccountMapper accountMapper;
	
	public Map<String, Object> selectAccount(Map<String, Object> map) throws Exception{
		return accountMapper.selectAccount(map);
	}
	
	public List<Map<String, Object>> selectAccountList(Map<String, Object> map) throws Exception{
		return accountMapper.selectAccountList(map);
	}
	
	public int selectAccountListCount(Map<String, Object> map) throws Exception{
		return accountMapper.selectAccountListCount(map);
	}
	
	public int insertAccount(AccountVO accountVO) throws Exception{
		return accountMapper.insertAccount(accountVO);
	}
		
	public void deleteAccount(Map<String, Object> map) throws Exception{
		accountMapper.deleteAccount(map);
	}
	public List<Map<String, Object>> selectForYear(Map<String, Object> map) throws Exception{
		return accountMapper.selectForYear(map);
	}
}
