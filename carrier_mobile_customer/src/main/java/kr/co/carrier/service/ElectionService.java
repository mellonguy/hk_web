package kr.co.carrier.service;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.ElectionVO;

public interface ElectionService {

	
	
	public Map<String, Object> selectElection(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectElectionList(Map<String, Object> map) throws Exception;
	public int selectElectionListCount(Map<String, Object> map) throws Exception;
	public int insertElection(ElectionVO electionVO) throws Exception;
	public void deleteElection(Map<String, Object> map) throws Exception;
	public void updateElectionJoinCount(Map<String, Object> map) throws Exception;
	
	
	
	
	
	
	
	
	
	
	
	
	
}
