package kr.co.carrier.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import kr.co.carrier.mapper.SendSmsMapper;
import kr.co.carrier.service.SendSmsService;
import kr.co.carrier.utils.BaseAppConstants;
import kr.co.carrier.vo.SendSmsVO;

@Service("sendSmsService")
public class SendSmsServiceImpl implements SendSmsService {

  private static String histUrl; // sms/alarmTalk url
  
  @Value("#{appProp['hist.url']}")
  public void setKey(String value) {
    histUrl = value;
  }
	@Resource(name="sendSmsMapper")
	private SendSmsMapper sendSmsMapper;
	
	
	public Map<String, Object> selectSendSms(Map<String, Object> map) throws Exception{
		return sendSmsMapper.selectSendSms(map);
	}
	
	public List<Map<String, Object>> selectSendSmsList(Map<String, Object> map) throws Exception{
		return sendSmsMapper.selectSendSmsList(map);
	}
	
	public int selectSendSmsListCount(Map<String, Object> map) throws Exception{
		return sendSmsMapper.selectSendSmsListCount(map);
	}
	
	public int insertSendSms(SendSmsVO sendSmsVO) throws Exception{

		StringBuilder urlBuilder = new StringBuilder(histUrl+"/sms/histsms");
		String result="";
		
		urlBuilder.append("?");
		urlBuilder.append(URLEncoder.encode("uid","UTF-8") + "=" + URLEncoder.encode(BaseAppConstants.COMPANY_TYPE_NATIONAL, "UTF-8") + "&");
		//메세지 발송 구분		1:기사배정,2:상차완료,3:하차완료,4:인계완료,5:탁송완료,6탁송취소,7:배차정보(기사님에게),8:배차승인(배차담당직원),9:탁송예약
		
		
		try {
			
			if(sendSmsVO.getType().equals("9")) {
				
				
				sendSmsVO.setCallback("15775268");
			
				String resultMsg ="";	
				
				
				String rcvphns = "01041885268;01048644300;01031123162;01080040891;";			
				//String[] chargePhoneArr ="01041885268"
				
				String callback ="15775268";
				
				
				resultMsg +="어플로 등록된 배차건 \r\n";
				resultMsg +="어플로 등록된 배차건 \r\n";
				resultMsg +="예약이 등록되었습니다.";
				
	            urlBuilder.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode(resultMsg,"UTF-8") + "&");
		        urlBuilder.append(URLEncoder.encode("rcvphns","UTF-8") + "=" + URLEncoder.encode(rcvphns, "UTF-8") + "&");
		        urlBuilder.append(URLEncoder.encode("callback","UTF-8") + "=" + URLEncoder.encode(callback, "UTF-8") + "&");

		    	sendSmsVO.setCallback(callback);
		    	sendSmsVO.setRcvphns(rcvphns);
		    	sendSmsVO.setMsg(resultMsg);
		    	
		        URL url = new URL(urlBuilder.toString());
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setDoOutput(true);
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=euc-kr");        
				conn.setDoOutput(true);
					
				OutputStream os = conn.getOutputStream();
				os.flush();
				os.close();
				int responseCode = conn.getResponseCode();
				System.out.println("\nSending 'POST' request to URL : " + url);
				System.out.println("Response Code : " + responseCode);
					
				BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String inputLine = "";
				StringBuffer response = new StringBuffer();
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				System.out.println(response.toString());
		        	       
				
			}
			
			
			
			
			
			
		}catch(Exception e){
			e.printStackTrace();
			
			
		}
		
		return sendSmsMapper.insertSendSms(sendSmsVO);
	}

	
	public int insertSendSms(SendSmsVO sendSmsVO,Map<String, Object> map) throws Exception{

		StringBuilder urlBuilder = new StringBuilder(histUrl+"/sms/histsms");
		String result="";
		
		urlBuilder.append("?");
		urlBuilder.append(URLEncoder.encode("uid","UTF-8") + "=" + URLEncoder.encode(BaseAppConstants.COMPANY_TYPE_NATIONAL, "UTF-8") + "&");
		//메세지 발송 구분		1:기사배정,2:상차완료,3:하차완료,4:인계완료,5:탁송완료,6탁송취소,7:배차정보(기사님에게),8:배차승인(배차담당직원),9:탁송예약
		
		
		try {
			
			if(sendSmsVO.getType().equals("9")) {
				
				
				sendSmsVO.setCallback("15775268");
			
				String resultMsg ="";	
				
				
				String rcvphns = "01041885268;01031123162;01041525268;01054383159;";			
				//String[] chargePhoneArr ="01041885268"
				
				String callback ="15775268";
				
				resultMsg += map.get("customerName").toString()+"\r\n";
				resultMsg += map.get("departure").toString()+"->"+map.get("arrival").toString()+"\r\n";
				resultMsg += map.get("carKind").toString()+"\r\n";
				resultMsg +="어플 예약";
				
	            urlBuilder.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode(resultMsg,"UTF-8") + "&");
		        urlBuilder.append(URLEncoder.encode("rcvphns","UTF-8") + "=" + URLEncoder.encode(rcvphns, "UTF-8") + "&");
		        urlBuilder.append(URLEncoder.encode("callback","UTF-8") + "=" + URLEncoder.encode(callback, "UTF-8") + "&");

		    	sendSmsVO.setCallback(callback);
		    	sendSmsVO.setRcvphns(rcvphns);
		    	sendSmsVO.setMsg(resultMsg);
		    	
		        URL url = new URL(urlBuilder.toString());
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setDoOutput(true);
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=euc-kr");        
				conn.setDoOutput(true);
					
				OutputStream os = conn.getOutputStream();
				os.flush();
				os.close();
				int responseCode = conn.getResponseCode();
				System.out.println("\nSending 'POST' request to URL : " + url);
				System.out.println("Response Code : " + responseCode);
					
				BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String inputLine = "";
				StringBuffer response = new StringBuffer();
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				System.out.println(response.toString());
		        	       
				
			}
			
			
			
			
			
			
		}catch(Exception e){
			e.printStackTrace();
			
			
		}
		
		return sendSmsMapper.insertSendSms(sendSmsVO);
	}

	
	
	
	
	
	
	
	
	
	
	
	public void deleteSendSms(Map<String, Object> map) throws Exception{
		
	}
	
	public Map<String, Object> selectCheckCertNumForLogin(Map<String, Object> map) throws Exception{
		return sendSmsMapper.selectCheckCertNumForLogin(map);
	}
	

	public int insertSendCertNumForApp(Map<String, Object> map) throws Exception{
		
		StringBuilder urlBuilder = new StringBuilder(histUrl+"/sms/histsms");
		String result="";
		
		urlBuilder.append("?");
		urlBuilder.append(URLEncoder.encode("uid","UTF-8") + "=" + URLEncoder.encode(BaseAppConstants.COMPANY_TYPE_NATIONAL, "UTF-8") + "&");
		
		
		try {
			
			String phoneNum = map.get("phoneNum").toString().replaceAll("-", "");
			phoneNum = phoneNum.replaceAll(" ", "");
			phoneNum = phoneNum.trim();
			
			
			String resultMsg ="";	
			
			resultMsg +="[한국카캐리어(주)] 인증번호 : ";
			resultMsg +="[";
			resultMsg += map.get("phoneCertNum").toString();
			resultMsg +="]"+"\r\n";
			resultMsg +="를 정확히 입력해주세요.";
			
            urlBuilder.append(URLEncoder.encode("msg","UTF-8") + "=" + URLEncoder.encode(resultMsg,"UTF-8") + "&");
	        urlBuilder.append(URLEncoder.encode("rcvphns","UTF-8") + "=" + URLEncoder.encode(phoneNum, "UTF-8") + "&");
	        urlBuilder.append(URLEncoder.encode("callback","UTF-8") + "=" + URLEncoder.encode("15775268", "UTF-8") + "&");
	        urlBuilder.append(URLEncoder.encode("subject","UTF-8") + "=" + URLEncoder.encode("인증번호가 도착했습니다.", "UTF-8") + "&");

	    	
	        map.put("phoneNum", phoneNum);
	        
	        URL url = new URL(urlBuilder.toString());
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=euc-kr");        
			conn.setDoOutput(true);
				
			OutputStream os = conn.getOutputStream();
			os.flush();
			os.close();
			int responseCode = conn.getResponseCode();
			System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);
				
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String inputLine = "";
			StringBuffer response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			System.out.println(response.toString());
	        	       
	        
		}catch(Exception e) {
		e.printStackTrace();	
			
		}
		
		
		return sendSmsMapper.insertSendCertNumForApp(map);
	}
	
	
	
	
	
	
	
	
	
	
}
