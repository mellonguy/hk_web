package kr.co.carrier.view;

import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

public class ExcelDownloadView extends AbstractExcelView {

	@Override
	protected void buildExcelDocument(Map<String, Object> model,
			SXSSFWorkbook workbook, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Sheet sheet = workbook.createSheet();

		List<Map<String, Object>> dataList = (List<Map<String, Object>>)model.get("list");
		String varNameList[] = (String[])model.get("varNameList");
		String titleNameList[] = (String[])model.get("titleNameList");
		String excelName = (String)model.get("excelName");
		
		Row row = null;
        Cell cell = null;
        int r = 0;
        int c = 0;
        
        CellStyle style = workbook.createCellStyle();
        style.setFillForegroundColor(IndexedColors.SKY_BLUE.index);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        
        //Create header cells
        row = sheet.createRow(r++);
        
        /*
        Iterator<String> iteratorKey = excelMap.keySet( ).iterator( );
		while(iteratorKey.hasNext()){
			String key = iteratorKey.next();
			//System.out.println(key+","+excelMap.get(key));
	        cell = row.createCell(c++);
	        cell.setCellStyle(style);
	        cell.setCellValue(excelMap.get(key));
		}
		*/
		
        //title
		for(int i=0; i<titleNameList.length; i++)
		{
			cell = row.createCell(c++);
	        cell.setCellStyle(style);
	        cell.setCellValue(titleNameList[i]);
		}

        for ( Map<String, Object> dataMap:dataList ) {
        	row = sheet.createRow(r++);
            c = 0;
            
            for(int i=0; i<varNameList.length; i++)
    		{
            	row.createCell(c++).setCellValue(dataMap.get(varNameList[i])+"");
    		}
            
            /*
            Iterator<String> iteratorKey2 = excelMap.keySet( ).iterator( );
			while(iteratorKey2.hasNext()){
				String key = iteratorKey2.next();
				row.createCell(c++).setCellValue(dataMap.get(key)+"");
			}
			*/
        }
        
        for(int i = 0 ; i < titleNameList.length; i++)
        {
//            sheet.autoSizeColumn(i, true);
            sheet.autoSizeColumn(i);
            sheet.setColumnWidth(i, (sheet.getColumnWidth(i))+512 );

        }
        
        String header = getBrowser(request);

        if (header.contains("MSIE")) {
               String docName = URLEncoder.encode(excelName,"UTF-8").replaceAll("\\+", "%20");
               response.setHeader("Content-Disposition", "attachment;filename=" + docName + ";");
        } else if (header.contains("Firefox")) {
               String docName = new String(excelName.getBytes("UTF-8"), "ISO-8859-1");
               response.setHeader("Content-Disposition", "attachment; filename=\"" + docName + "\"");
        } else if (header.contains("Opera")) {
               String docName = new String(excelName.getBytes("UTF-8"), "ISO-8859-1");
               response.setHeader("Content-Disposition", "attachment; filename=\"" + docName + "\"");
        } else if (header.contains("Chrome")) {
               String docName = new String(excelName.getBytes("UTF-8"), "ISO-8859-1");
               response.setHeader("Content-Disposition", "attachment; filename=\"" + docName + "\"");
        }
        response.setHeader("Content-Type", "application/octet-stream");
        response.setHeader("Content-Transfer-Encoding", "binary;");
        response.setHeader("Pragma", "no-cache;");
        response.setHeader("Expires", "-1;");
        response.setContentType("Application/Msexcel");
       // response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName);
	}
	
	
	private String getBrowser(HttpServletRequest request) {

        String header =request.getHeader("User-Agent");
        if (header.contains("MSIE")) {
               return "MSIE";
        } else if(header.contains("Chrome")) {
               return "Chrome";
        } else if(header.contains("Opera")) {
               return "Opera";
        }
        return "Firefox";

  }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
