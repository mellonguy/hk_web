package kr.co.carrier.controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import kr.co.carrier.service.AllocationService;
import kr.co.carrier.utils.WebUtils;

@Controller
@RequestMapping(value="/main")
public class MainController {

	private static final Logger logger = LoggerFactory.getLogger(MainController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	
	@Autowired
	private AllocationService allocationService;
	
	
	
	
	
	@RequestMapping(value = "/getting-started", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView main(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		logger.info("Welcome home! The client locale is {}.", locale);
		

		try{
			
		}catch(Exception e){
			e.printStackTrace();
		}
	
		return mav;
	}

	@RequestMapping(value = "/slider-page", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView sliderPage(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		logger.info("Welcome home! The client locale is {}.", locale);
		

		try{
			
			
	        
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		mav.addObject("serverTime", formattedDate );
		
		return mav;
	}
	
	
	
	

	

	
	@RequestMapping(value = "/analysis", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView analysis(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		mav.addObject("serverTime", formattedDate );
		
		return mav;
	}
	
	
	

	
	
	
	
	@RequestMapping(value = "/excavation", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView excavation(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		mav.addObject("serverTime", formattedDate );
		
		return mav;
	}
	
	
	

	
	@RequestMapping(value = "/index", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView index(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try {
		
			
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return mav;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
