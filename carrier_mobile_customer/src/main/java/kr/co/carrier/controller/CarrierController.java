package kr.co.carrier.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.mysql.fabric.xmlrpc.base.Array;

import kr.co.carrier.service.AccountService;
import kr.co.carrier.service.AdjustmentService;
import kr.co.carrier.service.AllocationDivisionService;
import kr.co.carrier.service.AllocationFileService;
import kr.co.carrier.service.AllocationService;
import kr.co.carrier.service.BillingDivisionService;
import kr.co.carrier.service.CarInfoService;
import kr.co.carrier.service.CheckDetailService;
import kr.co.carrier.service.CompanyService;
import kr.co.carrier.service.CustomerService;
import kr.co.carrier.service.DebtorCreditorService;
import kr.co.carrier.service.DecideFinalService;
import kr.co.carrier.service.DriverBillingFileService;
import kr.co.carrier.service.DriverBillingStatusService;
import kr.co.carrier.service.DriverDecideStatusService;
import kr.co.carrier.service.DriverDeductFileService;
import kr.co.carrier.service.DriverDeductInfoService;
import kr.co.carrier.service.DriverService;
import kr.co.carrier.service.ElectionService;
import kr.co.carrier.service.FileUploadService;
import kr.co.carrier.service.LowViolationService;
import kr.co.carrier.service.PayDivisionService;
import kr.co.carrier.service.PaymentDivisionService;
import kr.co.carrier.service.PaymentInfoService;
import kr.co.carrier.service.PersonInChargeService;
import kr.co.carrier.service.RunDivisionService;
import kr.co.carrier.service.SendSmsService;
import kr.co.carrier.service.VoteService;
import kr.co.carrier.utils.BaseAppConstants;
import kr.co.carrier.utils.MailUtils;
import kr.co.carrier.utils.PagingUtils;
import kr.co.carrier.utils.ParamUtils;
import kr.co.carrier.utils.ResultApi;
import kr.co.carrier.utils.WebUtils;
import kr.co.carrier.vo.AllocationFileVO;
import kr.co.carrier.vo.AllocationVO;
import kr.co.carrier.vo.CarCheckDetailVO;
import kr.co.carrier.vo.CarInfoVO;
import kr.co.carrier.vo.CustomerAppVO;
import kr.co.carrier.vo.DebtorCreditorVO;
import kr.co.carrier.vo.DriverBillingStatusVO;
import kr.co.carrier.vo.DriverDecideStatusVO;
import kr.co.carrier.vo.ElectionVO;
import kr.co.carrier.vo.PaymentInfoVO;
import kr.co.carrier.vo.SendSmsVO;
import kr.co.carrier.vo.VoteVO;

@Controller
@RequestMapping(value="/carrier")
public class CarrierController {

	private static final Logger logger = LoggerFactory.getLogger(CarrierController.class);
	
	
	 @Autowired
	    private AllocationService allocationService;
		
	    @Autowired
	    private CompanyService companyService;
	    
	    @Autowired
	    private DriverService driverService;
	    
	    @Autowired
	    private CarInfoService carInfoService;
	    
	    @Autowired
	    private BillingDivisionService billingDivisionService;
	    
	    @Autowired
	    private AllocationDivisionService allocationDivisionService;
	    
	    @Autowired
	    private RunDivisionService runDivisionService;

	    @Autowired
	    private PaymentDivisionService paymentDivisionService;
	    
	    @Autowired
	    private PayDivisionService payDivisionService;
	    
	    @Autowired
	    private PaymentInfoService paymentInfoService;
	
	    @Autowired
	    private AllocationFileService allocationFileService;
	
	    @Autowired
		private FileUploadService fileUploadService;
	    
	    @Autowired
	    private CheckDetailService checkDetailService;
	    
	    @Autowired
	    private PersonInChargeService personInChargeService;
	    
	    @Autowired
	    private DriverDeductInfoService driverDeductInfoService;
	    
	    @Autowired
	    private DriverDeductFileService driverDeductFileService;
	  
	    @Autowired
	    private DriverDecideStatusService driverDecideStatusService;
	    
	    
	    @Autowired
	    private DecideFinalService decideFinalService;
	    
	    @Autowired
	    private DriverBillingFileService driverBillingFileService;
	    
	    @Autowired
	    private DriverBillingStatusService driverBillingStatusService;
	    
	    @Autowired
	    private LowViolationService lowViolationService;
	    	    
	    @Autowired
	    private ElectionService electionService;
	    
	    @Autowired
	    private VoteService voteService;
	    
	    @Autowired
	    private DebtorCreditorService debtorCreditorService;
	    
	    @Autowired
	    private AdjustmentService adjustmentService;
	    
	    @Autowired
	    private CustomerService customerService;
	    
	    @Autowired
	    private SendSmsService sendSmsService;
	    
	    @Autowired
	    private AccountService accountService;
	    
	    @Value("#{appProp['upload.base.path']}")
	    private String rootDir;    
	    private String subDir;
	    
	    
	@RequestMapping(value = "/index", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView adviserDetail(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
			
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	@RequestMapping(value = "/login-page", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView login_page(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user"); 
			Map<String, Object> userMap = null;	
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			String optionType= request.getParameter("optionType") == null || request.getParameter("optionType").toString().equals("") ? "": request.getParameter("optionType").toString();
			
			
			if(userSessionMap != null && (paramMap.get("optionType")!=null &&!paramMap.get("optionType").toString().equals("C"))) {
				//WebUtils.messageAndRedirectUrl(mav, "자동 로그인 되었습니다.", "/carrier/main.do");
				response.sendRedirect(request.getContextPath()+"/carrier/main.do");	
			}else {
				
				if(optionType.equals("C")) {
					
					String customerId =userSessionMap.get("customer_id").toString();
					paramMap.put("customerId", customerId);
					
					
				}
				
				
			}
			mav.addObject("paramMap",paramMap);
			
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	@RequestMapping(value = "/certification-page", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi certification_page(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			String certificationNumber= request.getParameter("certificationNumber").toString().replaceAll("-", "");
			
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			paramMap.put("certificationNumber", certificationNumber);
			List<Map<String,Object>> phoneAndChargeList = 	customerService.selectChargePhoneAndBusinessLicenseNumber(paramMap);
		
			
			if (phoneAndChargeList.size() >0) {
				
				result.setResultCode("0000");
				result.setResultMsg("성공");
				
			}else {
				result.setResultCode("0001");
				result.setResultMsg("실패");
				
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0003");
		}
		
		
		return result;
	}
	
	
	@RequestMapping(value = "/carIdNum-page", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi carIdNumPage(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			String carIdNum= request.getParameter("carIdNum").toString().replaceAll("-", "");
			String customerId= request.getParameter("customerId").toString();
			
			
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			paramMap.put("carIdNum", carIdNum);
			paramMap.put("customerId", customerId);
		 	List<Map<String,Object>> carIdNumList = customerService.selectCarIdNumList(paramMap);
		
			
			if (carIdNumList.size() >0) {
				
				result.setResultCode("0000");
				result.setResultMsg("성공");
				
			}else {
				result.setResultCode("0001");
				result.setResultMsg("실패");
				
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0003");
		}
		
		
		return result;
	}
	
	
	
	
	@RequestMapping(value = "/customerList", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView customer_list(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			
			String certificationNumber= request.getParameter("certificationNumber").toString();
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			paramMap.put("certificationNumber", certificationNumber);
			List<Map<String,Object>> phoneAndChargeList = 	customerService.selectChargePhoneAndBusinessLicenseNumber(paramMap);
			paramMap.put("customerName", phoneAndChargeList.get(0).get("customer_name").toString());
			
			String phoneNum = WebUtils.getMaskedPhoneNum(phoneAndChargeList.get(0).get("phone_num").toString());
			String name =WebUtils.getMaskedName(phoneAndChargeList.get(0).get("name").toString());
			
			paramMap.put("phoneNum", phoneNum);
			paramMap.put("name", name);
			
			
			
			mav.addObject("listData",phoneAndChargeList);
			mav.addObject("paramMap",paramMap);
				

		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	@RequestMapping(value = "/car-id-num-list", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView carIdNumList(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			
			String carIdNum= request.getParameter("carIdNum").toString().replaceAll(" ", "");
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			paramMap.put("carIdNum", carIdNum);
			
			
			
		 	List<Map<String,Object>> carIdNumList = customerService.selectCarIdNumList(paramMap);
		 	paramMap.put("allocationId", carIdNumList.get(0).get("allocation_id").toString());
			
				
			
			mav.addObject("listData",carIdNumList);
			mav.addObject("paramMap",paramMap);
				

		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	//(거래처아이디로 인증번호 전송)
	@RequestMapping(value = "/insertCertNumForAPP", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi insert_certNum(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();

		try{
			
			Map<String,Object> map = new HashMap<String, Object>();
			
			String inputID= request.getParameter("inputID") == null || request.getParameter("inputID").toString().equals("") ? "": request.getParameter("inputID").toString();
			String phoneNum= request.getParameter("phoneNum") == null || request.getParameter("phoneNum").toString().equals("") ? "": request.getParameter("phoneNum").toString().replaceAll("-", "");
			String customerId= request.getParameter("customerId") == null || request.getParameter("customerId").toString().equals("") ? "": request.getParameter("customerId").toString();
			String gubun= request.getParameter("gubun").toString();
			
 	
 			map.put("userId", inputID);
			map.put("guBun", gubun);
			
			if(phoneNum.equals("")) {
				
				Map<String,Object> phoneMap =customerService.selectChagePasswordById(map);
				phoneNum = phoneMap.get("phone_num").toString();
				
			}
			
			map.put("phoneNum", phoneNum);
			
			if(customerId.equals("") && !gubun.equals("N")) {
				Map<String,Object> idMap =customerService.selectChangeId(map);
				customerId = idMap.get("customer_id").toString();
			}
			
			map.put("customerId", customerId);
			
			if(gubun.equals("C")) {
				
				List<Map<String, Object>> certNumDoubleCheckList = customerService.selectCustomerCheckForCanNotBeDoubleCert(map);
				
				if (certNumDoubleCheckList.isEmpty()) {
				
					customerService.updateVaildYnForCertNum(map);
						
				map.put("phoneCertId", "PCI"+UUID.randomUUID().toString().replaceAll("-",""));
				map.put("phoneCertNum",generateCertNum());
				map.put("guBun",gubun);
				map.put("resultCode", "0000");
				
				sendSmsService.insertSendCertNumForApp(map);
				result.setResultData(map);
				result.setResultCode("0000");
				
				
				}else {
					map.put("resultCode", "0002");
					result.setResultData(map);
					result.setResultCode("0002");
					
				}
			
			}
			
			
			if(!gubun.equals("C")){
				
				customerService.updateVaildYnForCertNum(map);
				
				map.put("phoneCertId", "PCI"+UUID.randomUUID().toString().replaceAll("-",""));
				map.put("phoneCertNum",generateCertNum());
				map.put("guBun",gubun);
				map.put("resultCode", "0000");
				
				sendSmsService.insertSendCertNumForApp(map);
				result.setResultData(map);
				result.setResultCode("0000");
				
				
				
				
			}
		
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		
		return result;

	
	}
	
	
	public String generateCertNum() {
		int certLength = 4;
		Random random = new Random();		
		
		int range = (int)Math.pow(10, certLength); //10의 6승
		int trim = (int)Math.pow(10,certLength-1); //n승 length -1
			
		int result = random.nextInt(range)+trim;
		if(result>range) {
			
			result = result - trim;
		}
		return String.valueOf(result);
}
	
	
	
	//(인증번호 비교)
	@RequestMapping(value = "/checkCertNumForLogin", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi check_certNum(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();

		try{

			Map<String,Object> map = new HashMap<String, Object>();
			
			String inputID= request.getParameter("inputID") == null || request.getParameter("inputID").toString().equals("") ? "": request.getParameter("inputID").toString();
			String phoneCertNum =request.getParameter("phoneCertNum").toString();
			String customerId= request.getParameter("customerId") == null || request.getParameter("customerId").toString().equals("") ? "": request.getParameter("customerId").toString();
			String phoneNum= request.getParameter("phoneNum") == null || request.getParameter("phoneNum").toString().equals("") ? "": request.getParameter("phoneNum").toString().replaceAll("-","");
			String gubun= request.getParameter("gubun").toString();
			
			map.put("phoneCertNum", phoneCertNum);
			map.put("userId", inputID);
			map.put("gubun", gubun);
			
			if(phoneNum.equals("")) {
				
				Map<String,Object> phoneMap =customerService.selectChagePasswordById(map);
				
				phoneNum = phoneMap.get("phone_num").toString();
				
			}
			
			map.put("phoneNum", phoneNum);
				
			Map<String,Object> idmap =customerService.selectChangeId(map);
			
			if(customerId.equals("") && !gubun.equals("N")) {
				
				customerId = idmap.get("customer_id").toString();
				
			}
			
			map.put("customerId", customerId);
			
			 if(gubun.equals("C")) {
					
					
					Map<String,Object> compareMap = sendSmsService.selectCheckCertNumForLogin(map);
					if(phoneCertNum.equals(compareMap.get("phone_cert_num").toString())) {
						
						result.setResultCode("0000");
						result.setResultMsg("성공");
						//result.setResultData(findId);	
						
							 
						}else{
								
						result.setResultCode("0001");
			            result.setResultMsg("실패");
						
						}
						
			}else if(!gubun.equals("C") && !gubun.equals("N")) {
				
				String customerIdNull ="";
				String findId =idmap.get("user_id").toString();
				map.put("customerId", customerIdNull);
				Map<String,Object> compareMap = sendSmsService.selectCheckCertNumForLogin(map);
				if(phoneCertNum.equals(compareMap.get("phone_cert_num").toString())) {
					
					result.setResultCode("0000");
					result.setResultMsg("성공");
					result.setResultData(findId);	
					
						 
				}else{
					
					result.setResultCode("0001");
					result.setResultMsg("실패");
				
				}
				
			}else {
				
				String customerIdNull ="";
				map.put("customerId", customerIdNull);
				Map<String,Object> compareMap = sendSmsService.selectCheckCertNumForLogin(map);
				if(phoneCertNum.equals(compareMap.get("phone_cert_num").toString())) {
					result.setResultCode("0000");
					result.setResultMsg("성공");
				}else{
					result.setResultCode("0001");
					result.setResultMsg("실패");
				}
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0002");
		}
		
		
		return result;

	
	}
	
	
	
	@RequestMapping(value = "/sign-Up", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView sign_Up(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			String findId= request.getParameter("findId") == null || request.getParameter("findId").toString().equals("") ? "": request.getParameter("findId").toString();
			String status= request.getParameter("status") == null || request.getParameter("status").toString().equals("") ? "": request.getParameter("status").toString();
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
		
			
			List<Map<String,Object>> phoneAndChargeList = 	customerService.selectChargePhoneAndBusinessLicenseNumber(paramMap);
			paramMap.put("findId", findId);
			paramMap.put("status", status);
			
			
			mav.addObject("listData",phoneAndChargeList);
			mav.addObject("paramMap",paramMap);
				
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
		//이용 약관 및 개인정보 처리 지침 동의
		@RequestMapping(value = "/agree", method = {RequestMethod.POST,RequestMethod.GET})
		public ModelAndView agree(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
			
			try{
				

				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
				String customerId = request.getParameter("customerId") != null && !request.getParameter("customerId").toString().equals("") ? request.getParameter("customerId").toString().replaceAll("-",""):"";
				String certificationNumber = request.getParameter("certificationNumber") != null && !request.getParameter("certificationNumber").toString().equals("") ? request.getParameter("certificationNumber").toString().replaceAll("-",""):"";
				String phoneNum = request.getParameter("phoneNum") != null && !request.getParameter("phoneNum").toString().equals("") ? request.getParameter("phoneNum").toString().replaceAll("-",""):"";
				
				//회원 가입시 페이지를 뒤로 가기 한 경우  certificationNumber 가 없어서.... 둘중 하나는 있으니까...
				if(certificationNumber.equals("")) {
					certificationNumber = phoneNum;
				}
				if(phoneNum.equals("")) {
					phoneNum = certificationNumber;
				}
				
				Map<String, Object> customer = customerService.selectCustomer(paramMap);
				
				paramMap.put("certificationNumber", certificationNumber);
				paramMap.put("phoneNum", phoneNum);
				paramMap.put("customerId", customerId);
				
				mav.addObject("paramMap",paramMap);
				mav.addObject("customerId",customerId);
				mav.addObject("customer",customer);
					
				
			}catch(Exception e){
				e.printStackTrace();
			}
			
			
			return mav;
		}
		
		
		
		//이용 약관 및 개인정보 처리 지침 동의
		@RequestMapping(value = "/osLicense", method = {RequestMethod.POST,RequestMethod.GET})
		public ModelAndView openSourceLicense(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
			
			try{

			}catch(Exception e){
				e.printStackTrace();
			}
					
					
			return mav;
		}
		
		//공지사항
		@RequestMapping(value = "/hk-infomation", method = {RequestMethod.POST,RequestMethod.GET})
		public ModelAndView hkInfomation(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
			
			try{

				
				
				HttpSession session = request.getSession();
				Map userSessionMap = (Map)session.getAttribute("user");
				
				if(userSessionMap != null){
				
					Map<String,Object> paramMap =ParamUtils.getParamToMap(request);
//					String searchType = request.getParameter("searchType") == null || request.getParameter("searchType").toString().equals("") ? WebUtils.getNow("yyyy-MM-dd"): request.getParameter("searchType").toString();
//					paramMap.put("searchType", searchType);
					
					List<Map<String,Object>> reviewList = customerService.selectReviewList(paramMap);
		
					
					
					
					mav.addObject("listData", reviewList);
					mav.addObject("paramMap", paramMap);
					
				
				}
				
				
				
			}catch(Exception e){
				e.printStackTrace();
			}
					
					
			return mav;
		}
		
		
		
		
		
	//회원가입
	@RequestMapping(value = "/simpleSign-Up", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView simple_SignUp(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			

			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
		
			String customerId =request.getParameter("customerId").toString();
			String chargeId =request.getParameter("chargeId").toString();
			String phoneNum =request.getParameter("phoneNum").toString().replaceAll("-","");
			
			
			
			
			paramMap.put("customerId", customerId);
			paramMap.put("chargeId", chargeId);
			paramMap.put("phoneNum", phoneNum);
			
			mav.addObject("paramMap",paramMap);
			mav.addObject("customerId",customerId);
				
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	@RequestMapping(value = "/idDoubleCheck", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi idDoubleCheck(ModelAndView mav,
	HttpServletRequest request,CustomerAppVO customerAppVO) throws Exception{
		
		ResultApi result = new ResultApi();

		try{

			Map<String,Object> map = new HashMap<String, Object>();
			String userId =request.getParameter("userId").toString();
			
			map.put("userId", userId);
			Map<String,Object> checkMap = customerService.selectidDoubleCheck(map);	
			
			if(checkMap == null) {
				
				
				result.setResultCode("0000");
				result.setResultMsg("성공");
			
			}else {
				
				result.setResultCode("0001");
				result.setResultMsg("실패");
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0002");
		}
		
		
		return result;

	
	}
	
	
	
	
	
	@RequestMapping(value = "/insertSimpleSign-Up", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi insertSimpleSign_Up(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response,
			CustomerAppVO customerAppVO) throws Exception{
		
		ResultApi result = new ResultApi();

		try{

			Map<String,Object> map = new HashMap<String, Object>();
			

			String userId =request.getParameter("userId").toString();
			String userPwd = request.getParameter("userPwd").toString();
			String customerId =request.getParameter("customerId").toString();
			String chargeId =request.getParameter("chargeId").toString();
			String phoneNum =request.getParameter("phoneNum").toString();
			
		
			map.put("userId", userId);
			map.put("userPwd", userPwd);
			map.put("customerId", customerId);
			map.put("chargeId", chargeId);
			map.put("phoneNum", phoneNum);
			
			//customerAppVO.setCustomerName(customerName);
			


				result.setResultCode("0000");
				result.setResultMsg("성공");
				customerService.insertSimpleSignUp(customerAppVO);
			
				
//				//회원가입한 모두에게 마일리지 3000포인트 지급
//				
//				Map<String,Object> mileageMap = new HashMap<String, Object>();
//				mileageMap.put("customerId",customerId);
//				mileageMap.put("customerName", customerAppVO.getCustomerName());
//				mileageMap.put("gubun","2");
//				
//				customerService.insertMileageCustomer(mileageMap);
//			
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0002");
		}
		
		
		return result;

	
	}
	
	
	

	
	
	@RequestMapping(value = "/main", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView main(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
			
		
				
		
			
		try{
			

			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null) {
	
				
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			String customerId =userSessionMap.get("customer_id").toString();
		    String searchWord = WebUtils.getNow("yyyy-MM-dd").toString();
			String phoneNum =userSessionMap.get("phone_num").toString();
			String chargeId =userSessionMap.get("charge_id").toString();
			String allocationId= request.getParameter("allocationId") == null || request.getParameter("allocationId").equals("") ? "" :request.getParameter("allocationId").toString();
			
			paramMap.put("customerId",customerId);
			paramMap.put("searchWord",searchWord);
			paramMap.put("phoneNum",phoneNum);
			paramMap.put("chargeId",chargeId);
			paramMap.put("allocationId", allocationId);
			paramMap.put("LoginType", "customerIdLogin");
		
			
			Map<String,Object> customerDetailMap =customerService.selectCustomerDetailInfo(paramMap);
			Map<String,Object> customerStatUs = customerService.selectCustomerAllocationStatusList(paramMap);
			
			List<Map<String,Object>> reviewList = customerService.selectReviewList(paramMap);
			
			mav.addObject("paramMap",paramMap);
			mav.addObject("listData",customerStatUs);
			mav.addObject("customer",customerDetailMap);
			mav.addObject("reviewList",reviewList);
			
			
			
			
			
			
		}else {
		
			Map<String,Object> map = new HashMap<String, Object>();
			
			
			String allocationId= request.getParameter("allocationId").toString();
			
			map.put("allocationId", allocationId);
			map.put("LoginType", "carIdNumLogin");
			
			
			Map<String,Object> mainB =  customerService.selectCarIdNumForMain(map);
			
			map.put("carIdNum", mainB.get("car_id_num").toString());
			mav.addObject("listData",mainB);		
			mav.addObject("paramMap",map);		
			
			
		}
		
					
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	@RequestMapping(value = "/sub-main", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView subMain(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		HttpSession session = request.getSession();
		Map userSessionMap = (Map)session.getAttribute("user");
		try{
			
			
			if(userSessionMap != null){
				
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				String customerId =userSessionMap.get("customer_id").toString();
			    String searchWord = WebUtils.getNow("yyyy-MM-dd").toString();
				String phoneNum =userSessionMap.get("phone_num").toString();
				String chargeId =userSessionMap.get("charge_id").toString();
			   
				
				paramMap.put("customerId",customerId);
				paramMap.put("searchWord",searchWord);
				paramMap.put("phoneNum",phoneNum);
				paramMap.put("chargeId",chargeId);
				
			
				Map<String,Object> customerDetailMap =customerService.selectCustomerDetailInfo(paramMap);
				

				//Map<String,Object> customerStatUs = customerService.selectCustomerAllocationStatusList(paramMap);
				//mav.addObject("paramMap",paramMap);
				//mav.addObject("listData",customerStatUs);
				mav.addObject("customer",customerDetailMap);
			
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	

	@RequestMapping(value = "/goKakaoTalk", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi go_KakaoTalk(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response) throws Exception{
		
		ResultApi result = new ResultApi();

		try{

			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
				
			Map<String,Object> map = new HashMap<String, Object>();
			
			//String customerId =userSessionMap.get("customer_id").toString();
			
			

			   String strEncodeUrl ="http://pf.kakao.com/_hxbkxkj/chat";
		        
		        URL oOpenURL = new URL(strEncodeUrl);
		   
		        
				result.setResultData(map);
			
			


			
			
			
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		
		return result;

	
	}
	
	@RequestMapping(value = "/allocation-list", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView allocationList(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
			
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	@RequestMapping(value = "/change-myinfo", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView changeMyInfo(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
	
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	@RequestMapping(value = "/change-password", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView changePassword(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
		
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	@RequestMapping(value = "/comparePhoneNum", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi comparePhoneNum(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			String phoneNum= request.getParameter("phoneNum").toString().replaceAll("-", "");
			
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			paramMap.put("phoneNum", phoneNum);
			Map<String,Object> changeIdMap = customerService.selectChangeId(paramMap);
		
			
			if (changeIdMap != null) {
				
				result.setResultCode("0000");
				result.setResultMsg("성공");
				
			}else {
				result.setResultCode("0001");
				result.setResultMsg("실패");
				
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0003");
		}
		
		
		return result;
	}
	
	
	@RequestMapping(value = "/compareCustomerId", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi compareCustomerId(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			String inputID= request.getParameter("inputID").toString();
			String changePwd = request.getParameter("changePwd") == null || request.getParameter("changePwd").equals("") ? "" :request.getParameter("changePwd").toString();;
			
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			paramMap.put("userId", inputID);
			Map<String,Object> compareIdMap = customerService.selectChagePasswordById(paramMap);
		
			
			
			if (compareIdMap == null && changePwd.equals("")) {
				
				result.setResultCode("0000");
				result.setResultMsg("성공");
				
			}else if(changePwd.equals("P")) {
				
				result.setResultCode("0000");
				result.setResultMsg("성공");
			
			
			
			}else {
				result.setResultCode("0001");
				result.setResultMsg("실패");
				
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0003");
		}
		
		
		return result;
	}
	
	
	//비밀번호 변경 a.jax
	@RequestMapping(value = "/confirmForPassWord", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi confirmForPassWord(ModelAndView mav, 
			HttpServletRequest request, HttpServletResponse response)throws Exception {
		ResultApi result = new ResultApi();

		try {

			Map<String, Object> map = new HashMap<String, Object>();
			String newPassword =request.getParameter("newPassword").toString();
			String userId =request.getParameter("userId").toString();			
			map.put("newPassword", newPassword);
			map.put("userId", userId);

			customerService.updateCreatePassword(map);
			
			result.setResultCode("0000");
			result.setResultMsg("성공");
			
			
		} catch (Exception e) {
			e.printStackTrace();
			result.setResultCode("0001");
		}

		return result;

	}
	
	@RequestMapping(value = "/car-check-detail", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView carCheckDetail(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				String allocationStatus = request.getParameter("allocationStatus");
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				Map<String, Object> map = new HashMap<String, Object>();
				List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
				List<Map<String, Object>> driverList = driverService.selectDriverList(map);
				List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
				List<Map<String, Object>> allocationDivisionList = allocationDivisionService.selectAllocationDivisionInfoList();
				List<Map<String, Object>> runDivisionList = runDivisionService.selectRunDivisionInfoList();
				List<Map<String, Object>> paymentDivisionList = paymentDivisionService.selectPaymentDivisionInfoList();
				List<Map<String, Object>> payDivisionList = payDivisionService.selectPayDivisionInfoList();
				
				map.put("allocationId", request.getParameter("allocationId").toString());
				map.put("driverId", userSessionMap.get("driver_id").toString());
				Map<String, Object> allocationMap = allocationService.selectAllocation(map);
				map.put("batchStatusId", allocationMap.get("batch_status_id"));
				List<Map<String, Object>> allocationList = allocationService.selectAllocationList(map);
				
				for(int i = 0; i < allocationList.size(); i++) {
					Map<String, Object> allocation = allocationList.get(i); 
					map.put("allocationId", allocation.get("allocation_id").toString());
					Map<String, Object> carInfo = carInfoService.selectCarInfo(map);
					allocation.put("carInfo", carInfo);
					List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(map);
					allocation.put("paymentInfoList", paymentInfoList);
					allocationList.set(i,allocation);
				}
					mav.addObject("allocationMap",  allocationMap);
					mav.addObject("allocationList",  allocationList);
					mav.addObject("billingDivisionList",  billingDivisionList);
					mav.addObject("allocationDivisionList",  allocationDivisionList);
					mav.addObject("runDivisionList",  runDivisionList);
					mav.addObject("paymentDivisionList",  paymentDivisionList);
					mav.addObject("payDivisionList",  payDivisionList);
					mav.addObject("driverList",  driverList);
					mav.addObject("companyList",  companyList);
					mav.addObject("paramMap", paramMap);
					mav.addObject("allocationStatus", allocationStatus);
					map.put("allocationStatus", allocationStatus);
					List<Map<String, Object>> allocationFileList = allocationFileService.selectAllocationFileList(map);
					mav.addObject("allocationFileList", allocationFileList);	
					mav.addObject("driverId", userSessionMap.get("driver_id").toString());
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	@RequestMapping(value = "/car-check-detail-forGroup", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView carCheckDetailForGroup(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				String allocationStatus = request.getParameter("allocationStatus");
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				Map<String, Object> map = new HashMap<String, Object>();
				List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
				List<Map<String, Object>> driverList = driverService.selectDriverList(map);
				List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
				List<Map<String, Object>> allocationDivisionList = allocationDivisionService.selectAllocationDivisionInfoList();
				List<Map<String, Object>> runDivisionList = runDivisionService.selectRunDivisionInfoList();
				List<Map<String, Object>> paymentDivisionList = paymentDivisionService.selectPaymentDivisionInfoList();
				List<Map<String, Object>> payDivisionList = payDivisionService.selectPayDivisionInfoList();
				
				map.put("allocationId", request.getParameter("allocationId").toString());
				map.put("driverId", userSessionMap.get("driver_id").toString());
				Map<String, Object> allocationMap = allocationService.selectAllocation(map);
				map.put("batchStatusId", allocationMap.get("batch_status_id"));
				List<Map<String, Object>> allocationList = allocationService.selectAllocationList(map);
				
				for(int i = 0; i < allocationList.size(); i++) {
					Map<String, Object> allocation = allocationList.get(i); 
					map.put("allocationId", allocation.get("allocation_id").toString());
					Map<String, Object> carInfo = carInfoService.selectCarInfo(map);
					allocation.put("carInfo", carInfo);
					List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(map);
					allocation.put("paymentInfoList", paymentInfoList);
					allocationList.set(i,allocation);
				}
					mav.addObject("allocationMap",  allocationMap);
					mav.addObject("allocationList",  allocationList);
					mav.addObject("billingDivisionList",  billingDivisionList);
					mav.addObject("allocationDivisionList",  allocationDivisionList);
					mav.addObject("runDivisionList",  runDivisionList);
					mav.addObject("paymentDivisionList",  paymentDivisionList);
					mav.addObject("payDivisionList",  payDivisionList);
					mav.addObject("driverList",  driverList);
					mav.addObject("companyList",  companyList);
					mav.addObject("paramMap", paramMap);
					mav.addObject("allocationStatus", allocationStatus);
					map.put("allocationStatus", allocationStatus);
					List<Map<String, Object>> allocationFileList = allocationFileService.selectAllocationFileList(map);
					mav.addObject("allocationFileList", allocationFileList);	
					mav.addObject("driverId", userSessionMap.get("driver_id").toString());
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	@RequestMapping(value = "/car-check-detail-batch-forGroup", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView carCheckDetailBatchForGroup(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				
				String allocationIdArr = request.getParameter("allocationId").toString();
				String[] allocationId = allocationIdArr.split(",");
				String groupId = allocationId[0];
				
				String allocationStatus = request.getParameter("allocationStatus");
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				Map<String, Object> map = new HashMap<String, Object>();
				List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
				List<Map<String, Object>> driverList = driverService.selectDriverList(map);
				List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
				List<Map<String, Object>> allocationDivisionList = allocationDivisionService.selectAllocationDivisionInfoList();
				List<Map<String, Object>> runDivisionList = runDivisionService.selectRunDivisionInfoList();
				List<Map<String, Object>> paymentDivisionList = paymentDivisionService.selectPaymentDivisionInfoList();
				List<Map<String, Object>> payDivisionList = payDivisionService.selectPayDivisionInfoList();
				
				map.put("allocationId", allocationId[0]);
				map.put("driverId", userSessionMap.get("driver_id").toString());
				Map<String, Object> allocationMap = allocationService.selectAllocation(map);				
				List<Map<String, Object>> allocationList = allocationService.selectAllocationList(map);
				
				for(int i = 0; i < allocationList.size(); i++) {
					Map<String, Object> allocation = allocationList.get(i); 
					map.put("allocationId", allocation.get("allocation_id").toString());
					Map<String, Object> carInfo = carInfoService.selectCarInfo(map);
					allocation.put("carInfo", carInfo);
					List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(map);
					allocation.put("paymentInfoList", paymentInfoList);
					allocationList.set(i,allocation);
				}
				
				List<Map<String, Object>> groupList = new ArrayList<Map<String, Object>>();
				for(int i = 0; i < allocationId.length; i++) {
					Map<String, Object> selectMap = new HashMap<String, Object>();
					selectMap.put("allocationId", allocationId[i]);
					selectMap.put("driverId", userSessionMap.get("driver_id").toString());
					selectMap.put("allocationStatus", allocationStatus);
					groupList.add(allocationService.selectAllocation(selectMap));
				}
				
					mav.addObject("allocationMap",  allocationMap);
					mav.addObject("allocationList",  allocationList);
					mav.addObject("billingDivisionList",  billingDivisionList);
					mav.addObject("allocationDivisionList",  allocationDivisionList);
					mav.addObject("runDivisionList",  runDivisionList);
					mav.addObject("paymentDivisionList",  paymentDivisionList);
					mav.addObject("payDivisionList",  payDivisionList);
					mav.addObject("driverList",  driverList);
					mav.addObject("companyList",  companyList);
					mav.addObject("paramMap", paramMap);
					mav.addObject("allocationStatus", allocationStatus);
					map.put("allocationStatus", allocationStatus);
					List<Map<String, Object>> allocationFileList = allocationFileService.selectAllocationFileList(map);
					mav.addObject("allocationFileList", allocationFileList);	
					mav.addObject("driverId", userSessionMap.get("driver_id").toString());
					mav.addObject("allocationIdArr", allocationIdArr);
					mav.addObject("groupId",  groupId);
					mav.addObject("groupList",  groupList);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	
	
	
	@RequestMapping(value = "/list", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView list(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{ 
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			
			if(userSessionMap != null){
			
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				String allocationStatus = request.getParameter("allocationStatus").toString() == null || request.getParameter("allocationStatus").equals("") ? "" :request.getParameter("allocationStatus").toString();
				String searchWord =WebUtils.getNow("yyyy-MM-dd").toString();
				String allocationId= request.getParameter("allocationId") == null || request.getParameter("allocationId").equals("") ? "" :request.getParameter("allocationId").toString();
				//String searchWord ="2020-06-25";
				//String BPIDcnt = request.getParameter("BPIDcnt").toString()  == null || request.getParameter("BPIDcnt").equals("") ? "" :request.getParameter("BPIDcnt").toString();
				
				
				/*String startDt = request.getParameter("startDt") == null ? WebUtils.getNow("yyyy-MM-dd") : request.getParameter("startDt").toString();
				String endDt = request.getParameter("endDt") == null ? WebUtils.getNow("yyyy-MM-dd") : request.getParameter("endDt").toString();
				if(startDt.equals("")) {
					startDt = WebUtils.getNow("yyyy-MM-dd");
				}
				if(endDt.equals("")) {
					endDt = WebUtils.getNow("yyyy-MM-dd");
				}
				paramMap.put("startDt", startDt);
				paramMap.put("endDt", endDt);*/
				
				
				paramMap.put("allocationStatus", allocationStatus);
				paramMap.put("customerId", userSessionMap.get("customer_id").toString());
				paramMap.put("searchWord", searchWord);
				paramMap.put("allocationId", allocationId);
				paramMap.put("LoginType", "customerIdLogin");
				
				//paramMap.put("BPIDcnt", BPIDcnt);
				
				List<Map<String,Object>> allocationStatusList = customerService.selectAllocationStatus(paramMap);
				
 				//int totalCount = allocationService.selectAllocationListCount(paramMap);
				//PagingUtils.setPageing(request, totalCount, paramMap);
				
				mav.addObject("listData",allocationStatusList);
				mav.addObject("paramMap",paramMap);
				
				
				
			}else {
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				String allocationStatus = request.getParameter("allocationStatus").toString() == null || request.getParameter("allocationStatus").equals("") ? "" :request.getParameter("allocationStatus").toString();
				String searchWord =WebUtils.getNow("yyyy-MM-dd").toString();
				String allocationId =request.getParameter("allocationId").toString();
			
				paramMap.put("allocationStatus", allocationStatus);
				paramMap.put("searchWord", searchWord);
				paramMap.put("allocationId", allocationId);
				paramMap.put("LoginType", "carIdNumLogin");
				
				
				List<Map<String,Object>> allocationStatusList = customerService.selectAllocationStatus(paramMap);
				
				mav.addObject("listData",allocationStatusList);
				mav.addObject("paramMap",paramMap);
				
				
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	
	
	
	
	@RequestMapping(value = "/finish-list", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView finishList(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				//String allocationStatus = request.getParameter("allocationStatus").toString() == null || request.getParameter("allocationStatus").equals("") ? "" :request.getParameter("allocationStatus").toString();
				String searchWord =WebUtils.getNow("yyyy-MM-dd").toString();
				String yearType = request.getParameter("yearType") == null || request.getParameter("yearType").toString().equals("")? WebUtils.getNow("yyyy") : request.getParameter("yearType").toString();
				
				//paramMap.put("allocationStatus", allocationStatus);
				paramMap.put("customerId", userSessionMap.get("customer_id").toString());
				paramMap.put("searchWord", searchWord);
				paramMap.put("LoginType", "customerIdLogin");
				paramMap.put("yearType", yearType);
				
				
				List<Map<String,Object>> yearMap = accountService.selectForYear(paramMap);
				//paramMap.put("BPIDcnt", BPIDcnt);
				
				//List<Map<String,Object>> allocationStatusList = customerService.selectAllocationStatus(paramMap);
				
 				//int totalCount = allocationService.selectAllocationListCount(paramMap);
				//PagingUtils.setPageing(request, totalCount, paramMap);
				
				//mav.addObject("listData",allocationStatusList);
				mav.addObject("paramMap",paramMap);
				mav.addObject("yearList", yearMap);
				
				
			}else {
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				String searchWord =WebUtils.getNow("yyyy-MM-dd").toString();
				String yearType = request.getParameter("yearType") == null || request.getParameter("yearType").toString().equals("")? WebUtils.getNow("yyyy") : request.getParameter("yearType").toString();
			
				paramMap.put("searchWord", searchWord);
				paramMap.put("LoginType", "carNumLogin");
				paramMap.put("yearType", yearType);
				
				
				List<Map<String,Object>> yearMap = accountService.selectForYear(paramMap);
				
				//String allocationStatus = request.getParameter("allocationStatus").toString() == null || request.getParameter("allocationStatus").equals("") ? "" :request.getParameter("allocationStatus").toString();
				//String searchWord =WebUtils.getNow("yyyy-MM-dd").toString();
				//String allocationId =request.getParameter("allocationId").toString();
			
				//paramMap.put("allocationStatus", allocationStatus);
				//paramMap.put("searchWord", searchWord);
				//paramMap.put("allocationId", allocationId);
				
				
				List<Map<String,Object>> allocationStatusList = customerService.selectAllocationStatus(paramMap);
				
				mav.addObject("listData",allocationStatusList);
				mav.addObject("paramMap",paramMap);
				mav.addObject("yearList", yearMap);
				
				
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@RequestMapping(value = "/group-list", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView groupList(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
			
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				String allocationStatus = request.getParameter("allocationStatus").toString();
				String allocationId = request.getParameter("allocationId").toString();
				String groupId = request.getParameter("allocationId").toString();
				
				paramMap.put("allocationStatus", allocationStatus);
				paramMap.put("driverId", userSessionMap.get("driver_id").toString());
				paramMap.put("allocationId",allocationId);
				paramMap.put("groupId",groupId);
				
				Map<String, Object> allocation = allocationService.selectAllocation(paramMap);
				paramMap.put("driverCnt",allocation.get("driver_cnt") != null && !allocation.get("driver_cnt").toString().equals("") ? allocation.get("driver_cnt").toString() : "1");
				paramMap.put("departureDt",allocation.get("departure_dt").toString());
				List<Map<String, Object>> allocationList = allocationService.selectAllocationListByDepartureDtAndDriverCnt(paramMap);
				
				
				Map<String, Object> map = new HashMap<String, Object>();
				List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
				List<Map<String, Object>> driverList = driverService.selectDriverList(map);
				List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
				mav.addObject("billingDivisionList",  billingDivisionList);
				mav.addObject("driverList",  driverList);
				mav.addObject("companyList",  companyList);
				mav.addObject("listData",  allocationList);
				map.put("driverId", userSessionMap.get("driver_id").toString());
				mav.addObject("paramMap", paramMap);
				mav.addObject("allocationStatus", allocationStatus);
				mav.addObject("driverId", userSessionMap.get("driver_id").toString());
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	@RequestMapping(value = "/group-list-finish", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView groupListFinish(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
			
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				String allocationStatus = request.getParameter("allocationStatus").toString();
				String allocationId = request.getParameter("allocationId").toString();
				String groupId = request.getParameter("allocationId").toString();
				
				paramMap.put("allocationStatus", allocationStatus);
				paramMap.put("driverId", userSessionMap.get("driver_id").toString());
				paramMap.put("allocationId",allocationId);
				paramMap.put("groupId",groupId);
				
				Map<String, Object> allocation = allocationService.selectAllocation(paramMap);
				paramMap.put("driverCnt",allocation.get("driver_cnt").toString());
				paramMap.put("departureDt",allocation.get("departure_dt").toString());
				List<Map<String, Object>> allocationList = allocationService.selectAllocationListByDepartureDtAndDriverCnt(paramMap);
				
				
				Map<String, Object> map = new HashMap<String, Object>();
				List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
				List<Map<String, Object>> driverList = driverService.selectDriverList(map);
				List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
				mav.addObject("billingDivisionList",  billingDivisionList);
				mav.addObject("driverList",  driverList);
				mav.addObject("companyList",  companyList);
				mav.addObject("listData",  allocationList);
				map.put("driverId", userSessionMap.get("driver_id").toString());
				mav.addObject("paramMap", paramMap);
				mav.addObject("allocationStatus", allocationStatus);
				mav.addObject("driverId", userSessionMap.get("driver_id").toString());
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	
	
	@RequestMapping(value = "/allocation-detail", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView allocationDetail(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response)  throws Exception{
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				
				String allocationStatus = request.getParameter("allocationStatus");
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				Map<String, Object> map = new HashMap<String, Object>();
				List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
				List<Map<String, Object>> driverList = driverService.selectDriverList(map);
				List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
				List<Map<String, Object>> allocationDivisionList = allocationDivisionService.selectAllocationDivisionInfoList();
				List<Map<String, Object>> runDivisionList = runDivisionService.selectRunDivisionInfoList();
				List<Map<String, Object>> paymentDivisionList = paymentDivisionService.selectPaymentDivisionInfoList();
				List<Map<String, Object>> payDivisionList = payDivisionService.selectPayDivisionInfoList();
				
				map.put("allocationId", request.getParameter("allocationId").toString());
				//map.put("driverId", driverList.get(0).get("driver_id").toString());
				Map<String, Object> allocationMap = allocationService.selectAllocation(map);
				map.put("batchStatusId", allocationMap.get("batch_status_id"));
				List<Map<String, Object>> allocationList = allocationService.selectAllocationList(map);
				paramMap.put("LoginType", "customerIdLogin");
				if(allocationStatus.equals("R")) {
					map.put("picturePointerR", allocationMap.get("picture_pointer_r").toString());	
				}else if(allocationStatus.equals("D")) {
					map.put("picturePointerD", allocationMap.get("picture_pointer_d").toString());
				}else if(allocationStatus.equals("I")) {
					map.put("picturePointerI", allocationMap.get("picture_pointer_i").toString());
				}else if(allocationStatus.equals("F")) {
					map.put("picturePointerR", allocationMap.get("picture_pointer_r").toString());
					map.put("picturePointerD", allocationMap.get("picture_pointer_d").toString());
					map.put("picturePointerI", allocationMap.get("picture_pointer_i").toString());
				}
				
				String departurePoint = "";
				String arrivalPoint = "";
				
				for(int i = 0; i < allocationList.size(); i++) {
					Map<String, Object> allocation = allocationList.get(i); 
					map.put("allocationId", allocation.get("allocation_id").toString());
					Map<String, Object> carInfo = carInfoService.selectCarInfo(map);
					
					String departure = carInfo.get("departure_addr") != null && !carInfo.get("departure_addr").toString().equals("") ? carInfo.get("departure_addr").toString() : "";
					String arrival = carInfo.get("arrival_addr") != null && !carInfo.get("arrival_addr").toString().equals("") ? carInfo.get("arrival_addr").toString() : "";
					
					
					try {
						departure = WebUtils.getAddressInfo(departure);
						arrival = WebUtils.getAddressInfo(arrival);
						departurePoint = WebUtils.getAddressToLatLng(departure);
						arrivalPoint = WebUtils.getAddressToLatLng(arrival);
					}catch(Exception e) {
						departurePoint = "";
						arrivalPoint = "";
						//e.printStackTrace();
					}finally {
						
					}
					
					Map<String, Object> checkDetailInfo = checkDetailService.selectCheckDetail(map); 
					allocation.put("carInfo", carInfo);
					allocation.put("checkDetailInfo", checkDetailInfo);
					List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(map);
					allocation.put("paymentInfoList", paymentInfoList);
					
					if(allocation != null && allocation.get("charge_id") != null && !allocation.get("charge_id").toString().equals("")) {
						Map<String, Object> personInChargeMap = new HashMap<String, Object>();
						personInChargeMap.put("personInChargeId", allocation.get("charge_id").toString());
						Map<String, Object> personInCharge = personInChargeService.selectPersonInChargeByPersonInChargeId(personInChargeMap);
						if(personInCharge != null) {
							String department = personInCharge.get("department") != null && !personInCharge.get("department").toString().equals("") ? personInCharge.get("department").toString() : ""; 
							allocation.put("department", department);	
						}
					//	String department = personInChargeService.selectPersonInChargeByPersonInChargeId(personInChargeMap).get("department").toString();
					//	String department = personInChargeService.selectPersonInChargeByPersonInChargeId(personInChargeMap).get("department") != null && !personInChargeService.selectPersonInChargeByPersonInChargeId(personInChargeMap).get("department").toString().equals("") ? personInChargeService.selectPersonInChargeByPersonInChargeId(personInChargeMap).get("department").toString():"";
					//	allocation.put("department", department);
					}
					allocationList.set(i,allocation);
				}
					mav.addObject("allocationMap",  allocationMap);
					mav.addObject("allocationList",  allocationList);
					mav.addObject("billingDivisionList",  billingDivisionList);
					mav.addObject("allocationDivisionList",  allocationDivisionList);
					mav.addObject("runDivisionList",  runDivisionList);
					mav.addObject("paymentDivisionList",  paymentDivisionList);
					mav.addObject("payDivisionList",  payDivisionList);
					mav.addObject("driverList",  driverList);
					mav.addObject("companyList",  companyList);
					mav.addObject("paramMap", paramMap);
					mav.addObject("allocationStatus", allocationStatus);
					mav.addObject("departurePoint", departurePoint);
					mav.addObject("arrivalPoint", arrivalPoint);
					mav.addObject("receiptSkipYn", allocationMap.get("receipt_skip_yn").toString());
					map.put("allocationStatus", allocationStatus);
					List<Map<String, Object>> allocationFileList = allocationFileService.selectAllocationFileList(map);
					mav.addObject("allocationFileList", allocationFileList);	
					
					//mav.addObject("driverId", driverList.get(0).get("driver_id").toString());
			}else {
				
				String allocationStatus = request.getParameter("allocationStatus");
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				Map<String, Object> map = new HashMap<String, Object>();
				List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
				List<Map<String, Object>> driverList = driverService.selectDriverList(map);
				List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
				List<Map<String, Object>> allocationDivisionList = allocationDivisionService.selectAllocationDivisionInfoList();
				List<Map<String, Object>> runDivisionList = runDivisionService.selectRunDivisionInfoList();
				List<Map<String, Object>> paymentDivisionList = paymentDivisionService.selectPaymentDivisionInfoList();
				List<Map<String, Object>> payDivisionList = payDivisionService.selectPayDivisionInfoList();
				
				map.put("allocationId", request.getParameter("allocationId").toString());
				//map.put("driverId", driverList.get(0).get("driver_id").toString());
				Map<String, Object> allocationMap = allocationService.selectAllocation(map);
				map.put("batchStatusId", allocationMap.get("batch_status_id"));
				List<Map<String, Object>> allocationList = allocationService.selectAllocationList(map);
				paramMap.put("LoginType", "carIdNumLogin");
				if(allocationStatus.equals("R")) {
					map.put("picturePointerR", allocationMap.get("picture_pointer_r").toString());	
				}else if(allocationStatus.equals("D")) {
					map.put("picturePointerD", allocationMap.get("picture_pointer_d").toString());
				}else if(allocationStatus.equals("I")) {
					map.put("picturePointerI", allocationMap.get("picture_pointer_i").toString());
				}else if(allocationStatus.equals("F")) {
					map.put("picturePointerR", allocationMap.get("picture_pointer_r").toString());
					map.put("picturePointerD", allocationMap.get("picture_pointer_d").toString());
					map.put("picturePointerI", allocationMap.get("picture_pointer_i").toString());
				}
				
				String departurePoint = "";
				String arrivalPoint = "";
				
				for(int i = 0; i < allocationList.size(); i++) {
					Map<String, Object> allocation = allocationList.get(i); 
					map.put("allocationId", allocation.get("allocation_id").toString());
					Map<String, Object> carInfo = carInfoService.selectCarInfo(map);
					
					String departure = carInfo.get("departure_addr") != null && !carInfo.get("departure_addr").toString().equals("") ? carInfo.get("departure_addr").toString() : "";
					String arrival = carInfo.get("arrival_addr") != null && !carInfo.get("arrival_addr").toString().equals("") ? carInfo.get("arrival_addr").toString() : "";
					
					
					try {
						departure = WebUtils.getAddressInfo(departure);
						arrival = WebUtils.getAddressInfo(arrival);
						departurePoint = WebUtils.getAddressToLatLng(departure);
						arrivalPoint = WebUtils.getAddressToLatLng(arrival);
					}catch(Exception e) {
						departurePoint = "";
						arrivalPoint = "";
						//e.printStackTrace();
					}finally {
						
					}
					
					Map<String, Object> checkDetailInfo = checkDetailService.selectCheckDetail(map); 
					allocation.put("carInfo", carInfo);
					allocation.put("checkDetailInfo", checkDetailInfo);
					List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(map);
					allocation.put("paymentInfoList", paymentInfoList);
					
					if(allocation != null && allocation.get("charge_id") != null && !allocation.get("charge_id").toString().equals("")) {
						Map<String, Object> personInChargeMap = new HashMap<String, Object>();
						personInChargeMap.put("personInChargeId", allocation.get("charge_id").toString());
						Map<String, Object> personInCharge = personInChargeService.selectPersonInChargeByPersonInChargeId(personInChargeMap);
						if(personInCharge != null) {
							String department = personInCharge.get("department") != null && !personInCharge.get("department").toString().equals("") ? personInCharge.get("department").toString() : ""; 
							allocation.put("department", department);	
						}
					//	String department = personInChargeService.selectPersonInChargeByPersonInChargeId(personInChargeMap).get("department").toString();
					//	String department = personInChargeService.selectPersonInChargeByPersonInChargeId(personInChargeMap).get("department") != null && !personInChargeService.selectPersonInChargeByPersonInChargeId(personInChargeMap).get("department").toString().equals("") ? personInChargeService.selectPersonInChargeByPersonInChargeId(personInChargeMap).get("department").toString():"";
					//	allocation.put("department", department);
					}
					allocationList.set(i,allocation);
				}
					mav.addObject("allocationMap",  allocationMap);
					mav.addObject("allocationList",  allocationList);
					mav.addObject("billingDivisionList",  billingDivisionList);
					mav.addObject("allocationDivisionList",  allocationDivisionList);
					mav.addObject("runDivisionList",  runDivisionList);
					mav.addObject("paymentDivisionList",  paymentDivisionList);
					mav.addObject("payDivisionList",  payDivisionList);
					mav.addObject("driverList",  driverList);
					mav.addObject("companyList",  companyList);
					mav.addObject("paramMap", paramMap);
					mav.addObject("allocationStatus", allocationStatus);
					mav.addObject("departurePoint", departurePoint);
					mav.addObject("arrivalPoint", arrivalPoint);
					mav.addObject("receiptSkipYn", allocationMap.get("receipt_skip_yn").toString());
					map.put("allocationStatus", allocationStatus);
					List<Map<String, Object>> allocationFileList = allocationFileService.selectAllocationFileList(map);
					mav.addObject("allocationFileList", allocationFileList);	
				
				
				
				
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	@RequestMapping(value = "/allocation-detail-forGroup", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView allocationDetailForGroup(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				
				String allocationStatus = request.getParameter("allocationStatus");
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				Map<String, Object> map = new HashMap<String, Object>();
				List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
				List<Map<String, Object>> driverList = driverService.selectDriverList(map);
				List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
				List<Map<String, Object>> allocationDivisionList = allocationDivisionService.selectAllocationDivisionInfoList();
				List<Map<String, Object>> runDivisionList = runDivisionService.selectRunDivisionInfoList();
				List<Map<String, Object>> paymentDivisionList = paymentDivisionService.selectPaymentDivisionInfoList();
				List<Map<String, Object>> payDivisionList = payDivisionService.selectPayDivisionInfoList();
				
				map.put("allocationId", request.getParameter("allocationId").toString());
				map.put("driverId", userSessionMap.get("driver_id").toString());
				Map<String, Object> allocationMap = allocationService.selectAllocation(map);
				map.put("batchStatusId", allocationMap.get("batch_status_id"));
				List<Map<String, Object>> allocationList = allocationService.selectAllocationList(map);
				
				if(allocationStatus.equals("R")) {
					map.put("picturePointerR", allocationMap.get("picture_pointer_r").toString());	
				}else if(allocationStatus.equals("D")) {
					map.put("picturePointerD", allocationMap.get("picture_pointer_d").toString());
				}else if(allocationStatus.equals("I")) {
					map.put("picturePointerI", allocationMap.get("picture_pointer_i").toString());
				}
				
				String departurePoint = "";
				String arrivalPoint = "";
				
				for(int i = 0; i < allocationList.size(); i++) {
					Map<String, Object> allocation = allocationList.get(i); 
					map.put("allocationId", allocation.get("allocation_id").toString());
					Map<String, Object> carInfo = carInfoService.selectCarInfo(map);
					Map<String, Object> checkDetailInfo = checkDetailService.selectCheckDetail(map); 
					allocation.put("carInfo", carInfo);
					
					String departure = carInfo.get("departure_addr") != null && !carInfo.get("departure_addr").toString().equals("") ? carInfo.get("departure_addr").toString() : "";
					String arrival = carInfo.get("arrival_addr") != null && !carInfo.get("arrival_addr").toString().equals("") ? carInfo.get("arrival_addr").toString() : "";
					
					
					try {
						departure = WebUtils.getAddressInfo(departure);
						arrival = WebUtils.getAddressInfo(arrival);
						departurePoint = WebUtils.getAddressToLatLng(departure);
						arrivalPoint = WebUtils.getAddressToLatLng(arrival);
					}catch(Exception e) {
						departurePoint = "";
						arrivalPoint = "";
						//e.printStackTrace();
					}finally {
						
					}
					
					allocation.put("checkDetailInfo", checkDetailInfo);
					List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(map);
					allocation.put("paymentInfoList", paymentInfoList);
					
					if(allocation != null && allocation.get("charge_id") != null && !allocation.get("charge_id").toString().equals("")) {
						Map<String, Object> personInChargeMap = new HashMap<String, Object>();
						personInChargeMap.put("personInChargeId", allocation.get("charge_id").toString());
						Map<String, Object> personInCharge = personInChargeService.selectPersonInChargeByPersonInChargeId(personInChargeMap);
						if(personInCharge != null) {
							String department = personInCharge.get("department") != null && !personInCharge.get("department").toString().equals("") ? personInCharge.get("department").toString() : ""; 
							allocation.put("department", department);	
						}
					//	String department = personInChargeService.selectPersonInChargeByPersonInChargeId(personInChargeMap).get("department").toString();
					//	String department = personInChargeService.selectPersonInChargeByPersonInChargeId(personInChargeMap).get("department") != null && !personInChargeService.selectPersonInChargeByPersonInChargeId(personInChargeMap).get("department").toString().equals("") ? personInChargeService.selectPersonInChargeByPersonInChargeId(personInChargeMap).get("department").toString():"";
					//	allocation.put("department", department);
					}
					allocationList.set(i,allocation);
				}
					mav.addObject("allocationMap",  allocationMap);
					mav.addObject("allocationList",  allocationList);
					mav.addObject("billingDivisionList",  billingDivisionList);
					mav.addObject("allocationDivisionList",  allocationDivisionList);
					mav.addObject("runDivisionList",  runDivisionList);
					mav.addObject("paymentDivisionList",  paymentDivisionList);
					mav.addObject("payDivisionList",  payDivisionList);
					mav.addObject("driverList",  driverList);
					mav.addObject("companyList",  companyList);
					mav.addObject("paramMap", paramMap);
					mav.addObject("allocationStatus", allocationStatus);
					mav.addObject("departurePoint", departurePoint);
					mav.addObject("arrivalPoint", arrivalPoint);
					
					mav.addObject("receiptSkipYn", allocationMap.get("receipt_skip_yn").toString());
					
					map.put("picturePointerR", allocationMap.get("picture_pointer_r").toString());
					map.put("picturePointerD", allocationMap.get("picture_pointer_d").toString());
					map.put("picturePointerI", allocationMap.get("picture_pointer_i").toString());
					map.put("allocationStatus", allocationStatus);
					List<Map<String, Object>> allocationFileList = allocationFileService.selectAllocationFileList(map);
					mav.addObject("allocationFileList", allocationFileList);	
					mav.addObject("driverId", userSessionMap.get("driver_id").toString());
					
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	@RequestMapping(value = "/upLiftCheck", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi customerExcelInsert(ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception{
		
		ResultApi resultApi = new ResultApi();
		
		try{
			
			String allocationIdArr = request.getParameter("allocationId").toString();
			String allocationId[] = allocationIdArr.split(",");
			String allocationStatus = request.getParameter("currentStatus").toString();
			subDir = "allocation";
			if(allocationStatus.equals("Y")){		//파일을 업 로드 하는 경우 진행 상태이다.
	    		allocationStatus = "R";
			 }else if(allocationStatus.equals("S")){
				 allocationStatus = "D";
			 }else if(allocationStatus.equals("P")){
				 allocationStatus = "D";
			 }
			
			if(allocationStatus.equals("I")){
				
				String fileName = "signature.png";
		    	String sign = StringUtils.split(request.getParameter("sign"), ",")[1];

		        String uploadDir = rootDir + "/" + subDir;
				String saveName = "/" + WebUtils.getNow("yyyy") + "/" + WebUtils.getNow("MM") + "/" + WebUtils.getNow("dd");
				String newFilename = UUID.randomUUID().toString()+".png";
							
				File saveDir = new File(uploadDir + saveName);
				
				if(saveDir.exists() == false){
					saveDir.mkdirs();
				}
//				saveName = saveName + "/" + newFilename;
				FileUtils.writeByteArrayToFile(new File(uploadDir + saveName), Base64.decodeBase64(sign));
				
				for(int i = 0 ; i < allocationId.length; i++) {
			    	AllocationFileVO allocationFileVO = new AllocationFileVO();
					allocationFileVO.setAllocationId(allocationId[i]);
					allocationFileVO.setAllocationStatus(allocationStatus);
					allocationFileVO.setCategoryType(subDir);
					allocationFileVO.setAllocationFileId("FLE"+UUID.randomUUID().toString().replaceAll("-", ""));
					allocationFileVO.setAllocationFileNm(fileName);
					allocationFileVO.setAllocationFilePath(saveName);
					allocationFileService.insertAllocationFile(allocationFileVO);
				}
				
			}else {
				MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
				allocationFileService.insertAllocationFile(rootDir, subDir, allocationIdArr, allocationStatus, multipartRequest);
				
			}
		
			for(int i = 0 ; i < allocationId.length; i++) {
				Map<String, Object> updateMap = new HashMap<String, Object>();
				updateMap.put("allocationId", allocationId[i]);
				updateMap.put("picturePointer", allocationId[0]);
				if(allocationStatus.equals("R")) {
					allocationService.updateAllocationPicturePointerR(updateMap);	
				}else if(allocationStatus.equals("D")) {
					allocationService.updateAllocationPicturePointerD(updateMap);	
				}else if(allocationStatus.equals("I")) {
					allocationService.updateAllocationPicturePointerI(updateMap);	
				} 
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return resultApi;
	}
	
		
	@RequestMapping(value = "/lift-check", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView liftCheck(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				
				String allocationId = request.getParameter("allocationId").toString();
				String allocationStatus = request.getParameter("allocationStatus").toString();
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				Map<String, Object> map = new HashMap<String, Object>();
				List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
				List<Map<String, Object>> driverList = driverService.selectDriverList(map);
				List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
				List<Map<String, Object>> allocationDivisionList = allocationDivisionService.selectAllocationDivisionInfoList();
				List<Map<String, Object>> runDivisionList = runDivisionService.selectRunDivisionInfoList();
				List<Map<String, Object>> paymentDivisionList = paymentDivisionService.selectPaymentDivisionInfoList();
				List<Map<String, Object>> payDivisionList = payDivisionService.selectPayDivisionInfoList();
			
				map.put("allocationId", allocationId);
				map.put("driverId", userSessionMap.get("driver_id").toString());
				map.put("allocationStatus", allocationStatus);
				Map<String, Object> allocationMap = allocationService.selectAllocation(map);
				map.put("batchStatusId", allocationMap.get("batch_status_id"));
				List<Map<String, Object>> allocationList = allocationService.selectAllocationList(map);
				
				if(allocationStatus.equals("R")) {
					map.put("picturePointerR", allocationMap.get("picture_pointer_r").toString());	
				}else if(allocationStatus.equals("D")) {
					map.put("picturePointerD", allocationMap.get("picture_pointer_d").toString());
				}else if(allocationStatus.equals("I")) {
					map.put("picturePointerI", allocationMap.get("picture_pointer_i").toString());
				}
				
				List<Map<String, Object>> allocationFileList = allocationFileService.selectAllocationFileList(map);
				
				for(int i = 0; i < allocationList.size(); i++) {
					Map<String, Object> allocation = allocationList.get(i); 
					map.put("allocationId", allocation.get("allocation_id").toString());
					Map<String, Object> carInfo = carInfoService.selectCarInfo(map);
					allocation.put("carInfo", carInfo);
					List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(map);
					allocation.put("paymentInfoList", paymentInfoList);
					allocationList.set(i,allocation);
				}
					mav.addObject("allocationMap",  allocationMap);
					mav.addObject("allocationList",  allocationList);
					mav.addObject("billingDivisionList",  billingDivisionList);
					mav.addObject("allocationDivisionList",  allocationDivisionList);
					mav.addObject("runDivisionList",  runDivisionList);
					mav.addObject("paymentDivisionList",  paymentDivisionList);
					mav.addObject("payDivisionList",  payDivisionList);
					mav.addObject("driverList",  driverList);
					mav.addObject("companyList",  companyList);
					mav.addObject("paramMap", paramMap);
					mav.addObject("allocationFileList", allocationFileList);
					mav.addObject("driverId", userSessionMap.get("driver_id").toString());
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	@RequestMapping(value = "/lift-check-forGroup", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView liftCheckForGroup(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				
				String allocationIdArr = request.getParameter("allocationId").toString();
				String[] allocationId = allocationIdArr.split(",");
				String groupId = allocationId[0];
				String allocationStatus = request.getParameter("allocationStatus").toString();
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				Map<String, Object> map = new HashMap<String, Object>();
				List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
				List<Map<String, Object>> driverList = driverService.selectDriverList(map);
				List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
				List<Map<String, Object>> allocationDivisionList = allocationDivisionService.selectAllocationDivisionInfoList();
				List<Map<String, Object>> runDivisionList = runDivisionService.selectRunDivisionInfoList();
				List<Map<String, Object>> paymentDivisionList = paymentDivisionService.selectPaymentDivisionInfoList();
				List<Map<String, Object>> payDivisionList = payDivisionService.selectPayDivisionInfoList();
			
				map.put("allocationId", allocationId[0]);
				map.put("driverId", userSessionMap.get("driver_id").toString());
				map.put("allocationStatus", allocationStatus);
				Map<String, Object> allocationMap = allocationService.selectAllocation(map);
				List<Map<String, Object>> allocationList = allocationService.selectAllocationList(map);
				if(allocationStatus.equals("R")) {
					map.put("picturePointerR", allocationMap.get("picture_pointer_r").toString());	
				}else if(allocationStatus.equals("D")) {
					map.put("picturePointerD", allocationMap.get("picture_pointer_d").toString());
				}else if(allocationStatus.equals("I")) {
					map.put("picturePointerI", allocationMap.get("picture_pointer_i").toString());
				}
				List<Map<String, Object>> allocationFileList = allocationFileService.selectAllocationFileList(map);
				
				for(int i = 0; i < allocationList.size(); i++) {
					Map<String, Object> allocation = allocationList.get(i); 
					map.put("allocationId", allocation.get("allocation_id").toString());
					Map<String, Object> carInfo = carInfoService.selectCarInfo(map);
					allocation.put("carInfo", carInfo);
					List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(map);
					allocation.put("paymentInfoList", paymentInfoList);
					allocationList.set(i,allocation);
				}
					mav.addObject("allocationMap",  allocationMap);
					mav.addObject("allocationList",  allocationList);
					mav.addObject("billingDivisionList",  billingDivisionList);
					mav.addObject("allocationDivisionList",  allocationDivisionList);
					mav.addObject("runDivisionList",  runDivisionList);
					mav.addObject("paymentDivisionList",  paymentDivisionList);
					mav.addObject("payDivisionList",  payDivisionList);
					mav.addObject("driverList",  driverList);
					mav.addObject("companyList",  companyList);
					mav.addObject("paramMap", paramMap);
					mav.addObject("allocationFileList", allocationFileList);
					mav.addObject("driverId", userSessionMap.get("driver_id").toString());
					mav.addObject("allocationIdArr", allocationIdArr);
					mav.addObject("groupId",  groupId);
					
					
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	@RequestMapping(value = "/lift-check-batch-forGroup", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView liftCheckBatchForGroup(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				
				String allocationIdArr = request.getParameter("allocationId").toString();
				String allocationId[] = allocationIdArr.split(",");
				String groupId = allocationId[0];
				String allocationStatus = request.getParameter("allocationStatus").toString();
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				Map<String, Object> map = new HashMap<String, Object>();
				List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
				List<Map<String, Object>> driverList = driverService.selectDriverList(map);
				List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
				List<Map<String, Object>> allocationDivisionList = allocationDivisionService.selectAllocationDivisionInfoList();
				List<Map<String, Object>> runDivisionList = runDivisionService.selectRunDivisionInfoList();
				List<Map<String, Object>> paymentDivisionList = paymentDivisionService.selectPaymentDivisionInfoList();
				List<Map<String, Object>> payDivisionList = payDivisionService.selectPayDivisionInfoList();
			
				map.put("allocationId", allocationId[0]);
				map.put("driverId", userSessionMap.get("driver_id").toString());
				map.put("allocationStatus", allocationStatus);
				Map<String, Object> allocationMap = allocationService.selectAllocation(map);
				List<Map<String, Object>> allocationList = allocationService.selectAllocationList(map);
				
				List<Map<String, Object>> groupList = new ArrayList<Map<String, Object>>();
				for(int i = 0; i < allocationId.length; i++) {
					Map<String, Object> selectMap = new HashMap<String, Object>();
					selectMap.put("allocationId", allocationId[i]);
					selectMap.put("driverId", userSessionMap.get("driver_id").toString());
					selectMap.put("allocationStatus", allocationStatus);
					groupList.add(allocationService.selectAllocation(selectMap));
				}
				
				if(allocationStatus.equals("R")) {
					map.put("picturePointerR", allocationMap.get("picture_pointer_r").toString());	
				}else if(allocationStatus.equals("D")) {
					map.put("picturePointerD", allocationMap.get("picture_pointer_d").toString());
				}else if(allocationStatus.equals("I")) {
					map.put("picturePointerI", allocationMap.get("picture_pointer_i").toString());
				}
				
				List<Map<String, Object>> allocationFileList = allocationFileService.selectAllocationFileList(map);
				
				for(int i = 0; i < allocationList.size(); i++) {
					Map<String, Object> allocation = allocationList.get(i); 
					map.put("allocationId", allocation.get("allocation_id").toString());
					Map<String, Object> carInfo = carInfoService.selectCarInfo(map);
					allocation.put("carInfo", carInfo);
					List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(map);
					allocation.put("paymentInfoList", paymentInfoList);
					allocationList.set(i,allocation);
				}
				
					mav.addObject("allocationIdArr",  allocationIdArr);
					mav.addObject("allocationMap",  allocationMap);
					mav.addObject("allocationList",  allocationList);
					mav.addObject("billingDivisionList",  billingDivisionList);
					mav.addObject("allocationDivisionList",  allocationDivisionList);
					mav.addObject("runDivisionList",  runDivisionList);
					mav.addObject("paymentDivisionList",  paymentDivisionList);
					mav.addObject("payDivisionList",  payDivisionList);
					mav.addObject("driverList",  driverList);
					mav.addObject("companyList",  companyList);
					mav.addObject("paramMap", paramMap);
					mav.addObject("allocationFileList", allocationFileList);
					mav.addObject("driverId", userSessionMap.get("driver_id").toString());
					mav.addObject("groupId", groupId);
					mav.addObject("groupList", groupList);
					
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	
	@RequestMapping(value = "/insertCheckDetail", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi insertCheckDetail(ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute CarCheckDetailVO carCheckDetailVO) throws Exception{
		
		ResultApi resultApi = new ResultApi();
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				String allocationId = request.getParameter("allocationId").toString();
				String[] allocationIdArr = allocationId.split(",");
				String groupId = allocationIdArr[0];
				
				String allocationStatus = request.getParameter("allocationStatus").toString();
				subDir = "allocation";
				if(allocationStatus.equals("Y")){		//파일을 업 로드 하는 경우 진행 상태이다.
		    		allocationStatus = "R";
				 }else if(allocationStatus.equals("S")){
					 allocationStatus = "I";
				 }else if(allocationStatus.equals("P")){
					 allocationStatus = "D";
				 }
				
				String fileName = "signature.png";
		    	String sign = StringUtils.split(request.getParameter("sign"), ",")[1];

		        String uploadDir = rootDir + "/" + subDir;
				String saveName = "/" + WebUtils.getNow("yyyy") + "/" + WebUtils.getNow("MM") + "/" + WebUtils.getNow("dd");
				String newFilename = UUID.randomUUID().toString()+".png";
							
				File saveDir = new File(uploadDir + saveName);
				
				if(saveDir.exists() == false){
					saveDir.mkdirs();
				}
				saveName = saveName + "/" + newFilename;
				FileUtils.writeByteArrayToFile(new File(uploadDir + saveName), Base64.decodeBase64(sign));
				
				for(int i = 0 ; i < allocationIdArr.length; i++) {
			    	AllocationFileVO allocationFileVO = new AllocationFileVO();
					allocationFileVO.setAllocationId(allocationIdArr[i]);
					allocationFileVO.setAllocationStatus(allocationStatus);
					allocationFileVO.setCategoryType(subDir);
					allocationFileVO.setAllocationFileId("FLE"+UUID.randomUUID().toString().replaceAll("-", ""));
					allocationFileVO.setAllocationFileNm(fileName);
					allocationFileVO.setAllocationFilePath(saveName);
					allocationFileService.insertAllocationFile(allocationFileVO);	
				}
				
				for(int i = 0 ; i < allocationIdArr.length; i++) {
					Map<String, Object> updateMap = new HashMap<String, Object>();
					updateMap.put("allocationId", allocationIdArr[i]);
					updateMap.put("picturePointer", allocationIdArr[0]);
					if(allocationStatus.equals("R")) {
						allocationService.updateAllocationPicturePointerR(updateMap);	
					}else if(allocationStatus.equals("D")) {
						allocationService.updateAllocationPicturePointerD(updateMap);	
					}else if(allocationStatus.equals("I")) {
						allocationService.updateAllocationPicturePointerI(updateMap);	
					} 
				}
				
				for(int i = 0 ; i < allocationIdArr.length; i++) {
					carCheckDetailVO.setAllocationId(allocationIdArr[i]);
					carCheckDetailVO.setCarCheckDetailId("CCD"+UUID.randomUUID().toString().replaceAll("-", ""));
					checkDetailService.insertCheckDetail(carCheckDetailVO);
				}
				
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("allocationId", allocationIdArr[0]);
				map.put("driverId", userSessionMap.get("driver_id").toString());
				map.put("allocationStatus", allocationStatus);
				Map<String, Object> allocationMap = allocationService.selectAllocation(map);
				if(allocationStatus.equals("R")) {
					map.put("picturePointerR", allocationMap.get("picture_pointer_r").toString());	
				}else if(allocationStatus.equals("D")) {
					map.put("picturePointerD", allocationMap.get("picture_pointer_d").toString());
				}else if(allocationStatus.equals("I")) {
					map.put("picturePointerI", allocationMap.get("picture_pointer_i").toString());
				}
				
				List<Map<String, Object>> allocationFileList = allocationFileService.selectAllocationFileList(map);
				mav.addObject("allocationFileList", allocationFileList);
				resultApi.setResultData(allocationFileList);
			}
			
			
			
			
			
			
		
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return resultApi;
	}
	
	
	
	@RequestMapping(value = "/insertReceipt", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi insertReceipt(ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute CarCheckDetailVO carCheckDetailVO) throws Exception{
		
		ResultApi resultApi = new ResultApi();
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			String allocationId = request.getParameter("allocationId").toString();
			String[] allocationIdArr = allocationId.split(",");
			String groupId = allocationIdArr[0];
			
			String allocationStatus = request.getParameter("allocationStatus").toString();
			/*String phoneNumber = request.getParameter("phoneNumber") != null && !request.getParameter("phoneNumber").toString().equals("")  ? request.getParameter("phoneNumber").toString() : "";
			String mailAddress = request.getParameter("mailAddress") != null && !request.getParameter("mailAddress").toString().equals("")  ? request.getParameter("mailAddress").toString() : "";*/
			
			//기존에 메일주소와 전화번호를 기사님이 현장에서 받았으나 배차 입력에 작성된 연락처와 메일 주소로 인수증을 전송 하도록(인수증을 전송 해야 하는건만) 변경..
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("allocationId", allocationIdArr[0]);
			map.put("driverId", userSessionMap.get("driver_id").toString());
			
			Map<String, Object> allocation = allocationService.selectAllocation(map);
			
			String phoneNumber = allocation.get("receipt_phone") != null && !allocation.get("receipt_phone").toString().equals("") ? allocation.get("receipt_phone").toString():"";
			String mailAddress = allocation.get("receipt_email") != null && !allocation.get("receipt_email").toString().equals("") ? allocation.get("receipt_email").toString():"";
			
			subDir = "allocation";
			allocationStatus = "I";
			String fileName = "receipt.png";
	    	String receipt = StringUtils.split(request.getParameter("receipt"), ",")[1];
	        String uploadDir = rootDir + "/" + subDir;
			String saveName = "/" + WebUtils.getNow("yyyy") + "/" + WebUtils.getNow("MM") + "/" + WebUtils.getNow("dd");
			String newFilename = UUID.randomUUID().toString()+".png";
			File saveDir = new File(uploadDir + saveName);
			
			if(saveDir.exists() == false){
				saveDir.mkdirs();
			}
			saveName = saveName + "/" + newFilename;
			FileUtils.writeByteArrayToFile(new File(uploadDir + saveName), Base64.decodeBase64(receipt));
			//File file = new File(uploadDir + saveName);
			resultApi.setResultData(saveName);
			resultApi.setResultDataSub(phoneNumber);
			
			for(int i = 0 ; i < allocationIdArr.length; i++) {
		    	AllocationFileVO allocationFileVO = new AllocationFileVO();
				allocationFileVO.setAllocationId(allocationIdArr[i]);
				allocationFileVO.setAllocationStatus(allocationStatus);
				allocationFileVO.setCategoryType(subDir);
				allocationFileVO.setAllocationFileId("FLE"+UUID.randomUUID().toString().replaceAll("-", ""));
				allocationFileVO.setAllocationFileNm(fileName);
				allocationFileVO.setAllocationFilePath(saveName);
				allocationFileService.insertAllocationFile(allocationFileVO);	
			}
			
			for(int i = 0 ; i < allocationIdArr.length; i++) {
				Map<String, Object> updateMap = new HashMap<String, Object>();
				updateMap.put("allocationId", allocationIdArr[i]);
				updateMap.put("picturePointer", allocationIdArr[0]);
				if(allocationStatus.equals("R")) {
					allocationService.updateAllocationPicturePointerR(updateMap);	
				}else if(allocationStatus.equals("D")) {
					allocationService.updateAllocationPicturePointerD(updateMap);	
				}else if(allocationStatus.equals("I")) {
					allocationService.updateAllocationPicturePointerI(updateMap);	
				} 
			}
			
			if(!mailAddress.equals("")) {
				MailUtils.mailSend(mailAddress, "test", "test",saveName,fileName);
				System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"+mailAddress+"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");	
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return resultApi;
	}
	
	
	
	@RequestMapping(value = "/showDistance", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi showDistance(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response 
			) throws Exception{
		
		
		ResultApi result = new ResultApi();

		try{
			
			Map <String,Object> map = new HashMap<String,Object>();
			String driverId = request.getParameter("driverId").toString();
					
			map.put("driverId", driverId);
			
			
			//상차지 또는 하차지의 주소로 주소 정보를 가져 올 수 없는 경우 경로보기를 진행 할 수 없다.
			
			
			Map <String,Object> locationMap = customerService.selectShowDistenceToDriverLocation(map);
			
			/*
			 * if(WebUtils.getAddressResult(arrival) == 0) { result.setResultCode("0002");
			 * return result; }
			 * 
			 * String[] arrivalPointArr = WebUtils.getAddressToLatLng(arrival).split(",");
			 */
			
			
			BufferedReader    oBufReader = null;
	        HttpURLConnection httpConn   = null;
	        
	        //search : 0:최단,2: 추천,3:무료,4:자동차 전용 제외
	        //String strEncodeUrl = "http://map.naver.com/spirra/findCarRoute.nhn?route=route3&output=json&result=web3&coord_type=naver&search=0&car=0&mileage=12.4&start="+departure+","+departureAdd+"&destination="+destination+","+destinationAdd;
	        
	        //String strEncodeUrl = "https://map.naver.com/spirra/findCarRoute.nhn?route=route3&output=json&result=web3&coord_type=naver&search=2&car=0&mileage=12.4"+resultStr+"&via=";
	        
//	        
//	        String strEncodeUrl = "https://apis.openapi.sk.com/tmap/routes?&appKey=l7xx6b701ff5595c4e3b859e42f57de0c975&"
//	        		+ "&version=1&endX="+arrivalPointArr[0]+"&endY="+arrivalPointArr[1]
//	        		+ "&startX="+departurePointArr[0]+"&startY="+departurePointArr[1]+"&carType=5";
//	        
//	        
//	        URL oOpenURL = new URL(strEncodeUrl);
////	      
//	        httpConn =  (HttpURLConnection) oOpenURL.openConnection();          
//	        httpConn.setRequestMethod("GET");          
//	        httpConn.connect(); 
//	        Thread.sleep(100);
//	        oBufReader = new BufferedReader(new InputStreamReader(oOpenURL.openStream()));
	        
	        JSONParser jsonParser = new JSONParser();
	        JSONObject jsonObject = (JSONObject)jsonParser.parse(oBufReader);
	        
	        String jsonString = jsonObject.toJSONString();
	        
	        Object obj = JSONValue.parse(jsonString);
	        JSONObject jsonObj = (JSONObject)obj;
	        
	        //JSONArray list = (JSONArray)jsonObject.get("routes");
	        //JSONObject jsonObj2 = (JSONObject)list.get(0); 
	        //JSONObject summary = (JSONObject)jsonObj2.get("summary");
	        
	        
	        JSONArray list = (JSONArray)jsonObject.get("features");
	        JSONObject jsonObj2 = (JSONObject)list.get(0); 
	        JSONObject summary = (JSONObject)jsonObj2.get("geometry"); 
	        JSONObject properties = (JSONObject)jsonObj2.get("properties");
	        String pointType = properties.get("pointType").toString();
	        String  totalDistance = properties.get("totalDistance").toString();
	        
	        DecimalFormat form = new DecimalFormat("#.#");
	        float distanceF = Float.valueOf(totalDistance)/1000;
	        String distance =String.valueOf(form.format(distanceF));


	        map.put("distance", distance);
	       // map.put("departure", WebUtils.getAddressToLngLat(departure));
	 //       map.put("arrival", WebUtils.getAddressToLngLat(arrival));
	        
			result.setResultData(map);
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	
	
	
	
	@RequestMapping(value = "/getDistance", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi getDistance(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			
			String departure =  WebUtils.getAddressInfo(request.getParameter("departureAddr").toString()); 
			String arrival = WebUtils.getAddressInfo(request.getParameter("arrivalAddr").toString());
			
		
			
			//상차지 또는 하차지의 주소로 주소 정보를 가져 올 수 없는 경우 경로보기를 진행 할 수 없다.
			if(WebUtils.getAddressResult(departure) == 0) {
				result.setResultCode("0002");
				return result;
			}
			
			if(WebUtils.getAddressResult(arrival) == 0) {
				result.setResultCode("0003");
				return result;
			}
			
			String resultStr = "&start="+WebUtils.getAddressToLatLng(departure)+"&destination="+WebUtils.getAddressToLatLng(arrival);
	        
			String[] departurePointArr = WebUtils.getAddressToLatLng(departure).split(",");
			String[] arrivalPointArr = WebUtils.getAddressToLatLng(arrival).split(",");
			
			
			BufferedReader    oBufReader = null;
	        HttpURLConnection httpConn   = null;
	        
	        //search : 0:최단,2: 추천,3:무료,4:자동차 전용 제외
	        //String strEncodeUrl = "http://map.naver.com/spirra/findCarRoute.nhn?route=route3&output=json&result=web3&coord_type=naver&search=0&car=0&mileage=12.4&start="+departure+","+departureAdd+"&destination="+destination+","+destinationAdd;
	        
	        //String strEncodeUrl = "https://map.naver.com/spirra/findCarRoute.nhn?route=route3&output=json&result=web3&coord_type=naver&search=2&car=0&mileage=12.4"+resultStr+"&via=";
	        
	        
	        String strEncodeUrl = "https://apis.openapi.sk.com/tmap/routes?&appKey=l7xx6b701ff5595c4e3b859e42f57de0c975&"
	        		+ "&version=1&endX="+arrivalPointArr[0]+"&endY="+arrivalPointArr[1]
	        		+ "&startX="+departurePointArr[0]+"&startY="+departurePointArr[1]+"&carType=5";
	        
	   
	        
	        
	        URL oOpenURL = new URL(strEncodeUrl);
	      
	        httpConn =  (HttpURLConnection) oOpenURL.openConnection();          
	        httpConn.setRequestMethod("GET");          
	        httpConn.connect(); 
	        Thread.sleep(100);
	        oBufReader = new BufferedReader(new InputStreamReader(oOpenURL.openStream()));
	        
	        JSONParser jsonParser = new JSONParser();
	        JSONObject jsonObject = (JSONObject)jsonParser.parse(oBufReader);
	        
	        String jsonString = jsonObject.toJSONString();
	        
	        Object obj = JSONValue.parse(jsonString);
	        JSONObject jsonObj = (JSONObject)obj;
	        
	        //JSONArray list = (JSONArray)jsonObject.get("routes");
	        //JSONObject jsonObj2 = (JSONObject)list.get(0); 
	        //JSONObject summary = (JSONObject)jsonObj2.get("summary");
	        
	        
	        JSONArray list = (JSONArray)jsonObject.get("features");
	        JSONObject jsonObj2 = (JSONObject)list.get(0); 
	        JSONObject summary = (JSONObject)jsonObj2.get("geometry"); 
	        JSONObject properties = (JSONObject)jsonObj2.get("properties");
	        String pointType = properties.get("pointType").toString();
	        String  totalDistance = properties.get("totalDistance").toString();
	        
	        DecimalFormat form = new DecimalFormat("#.#");
	        float distanceF = Float.valueOf(totalDistance)/1000;
	        String distance =String.valueOf(form.format(distanceF));

	        Map<String, Object> map = new HashMap<String, Object>();
	        map.put("distance", distance);
	        map.put("departure", WebUtils.getAddressToLngLat(departure));
	        map.put("arrival", WebUtils.getAddressToLngLat(arrival));
	        
			result.setResultData(map);
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	@RequestMapping(value = "/routeView", method = RequestMethod.GET)
	public ModelAndView routeView(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute AllocationVO allocationVO) throws Exception {
		
		try{
			
			String driverId =request.getParameter("driverId").toString();		
			
	
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("driverId",driverId);
			
			Map<String,Object> locationMap =customerService.selectShowDistenceToDriverLocation(map);
			
			
			if(locationMap != null) {
				
				
			
				map.put("location_x", Double.parseDouble(locationMap.get("lat").toString()));
				map.put("location_y", Double.parseDouble(locationMap.get("lng").toString()));
				map.put("center_x",map.get("location_x"));
				map.put("center_y",map.get("location_y"));
		
				
	    		mav.addObject("map", map);
				
			}else {
				
				StringBuffer sb1 = new StringBuffer();

				
				
			
				sb1.append("<!DOCTYPE html>");
				sb1.append("<html lang=\"ko\"  style=\" height:100%;\"> ");
				sb1.append("<jsp:include page=\"/WEB-INF/views/jsp/common-header.jsp\"></jsp:include>");
				sb1.append("<body style=\" height:100%;\">");
				sb1.append("<script src='/js/vendor/jquery-1.11.2.min.js'></script>");
				sb1.append("<script src='/js/alert.js'></script>");
				sb1.append("<script type='text/javascript'>");
				//sb1.append("alert('기사님의 이동경로를 불러오지 못했습니다.');");
				sb1.append("$.alert('기사님의 이동경로를 불러오지 못했습니다.');");;
				sb1.append("		setTimeout(function() { ");
				sb1.append("history.go(-1);");
				sb1.append("			}, 3000);");
		
				sb1.append("</script>");
				sb1.append("</body>");
				  
				response.setContentType("text/html; charset=UTF-8");
				PrintWriter out1 = response.getWriter();
				out1.println(sb1);
				out1.flush();
		        
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	
	
	

	
	
	// 탁송 예약 임시저장 

	@RequestMapping(value = "/tmpConsignmentReservation", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi tempConsignmentReservation(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
		
			String departureDt= request.getParameter("departureDt") == null || request.getParameter("departureDt").toString().equals("") ? "": request.getParameter("departureDt").toString();
			String departureTime= request.getParameter("departureTime") == null || request.getParameter("departureTime").toString().equals("") ? "": request.getParameter("departureTime").toString();
			String departurePersonInCharge= request.getParameter("departurePersonInCharge") == null || request.getParameter("departurePersonInCharge").toString().equals("") ? "": request.getParameter("departurePersonInCharge").toString();
			String departurePhone= request.getParameter("departurePhone") == null || request.getParameter("departurePhone").toString().equals("") ? "": request.getParameter("departurePhone").toString();
			String arrivalPersonInCharge= request.getParameter("arrivalPersonInCharge") == null || request.getParameter("arrivalPersonInCharge").toString().equals("") ? "": request.getParameter("arrivalPersonInCharge").toString();
			String arrivalPhone= request.getParameter("arrivalPhone") == null || request.getParameter("arrivalPhone").toString().equals("") ? "": request.getParameter("arrivalPhone").toString();
			String carKind= request.getParameter("carKind") == null || request.getParameter("carKind").toString().equals("") ? "": request.getParameter("carKind").toString();
			String carIdNum= request.getParameter("carIdNum") == null || request.getParameter("carIdNum").toString().equals("") ? "": request.getParameter("carIdNum").toString();
			String carNum= request.getParameter("carNum") == null || request.getParameter("carNum").toString().equals("") ? "": request.getParameter("carNum").toString();
			
			String departureAddr = request.getParameter("departureAddr").toString();
			String departure =	departureAddr.split(" ")[0]+" "+departureAddr.split(" ")[1];
			
			String arrivalAddr = request.getParameter("arrivalAddr").toString();
			String arrival = arrivalAddr.split(" ")[0]+" "+arrivalAddr.split(" ")[1];
			
			
			Map<String,Object> insertMap = new HashMap<String,Object>();
			insertMap.put("tempConsignmentId", "TCI"+UUID.randomUUID().toString().replaceAll("-", ""));
			insertMap.put("departureDt",departureDt);
			insertMap.put("departureTime", departureTime);
			insertMap.put("departurePersonInCharge", departurePersonInCharge);
			insertMap.put("departurePhone", departurePhone);
			insertMap.put("departure", departure);
			insertMap.put("departureAddr", departureAddr);
			insertMap.put("arrival", arrival);
			insertMap.put("arrivalAddr", arrivalAddr);
			insertMap.put("arrivalPersonInCharge", arrivalPersonInCharge);
			insertMap.put("arrivalPhone", arrivalPhone);
			insertMap.put("carKind", carKind);
			insertMap.put("carIdNum", carIdNum);
			insertMap.put("carNum", carNum);
			allocationService.insertConsignmentReservationTemp(insertMap);
			
			


			result.setResultData(insertMap);
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	
	
	@RequestMapping(value = "/loadView", method = RequestMethod.GET)
	public ModelAndView loadView(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute AllocationVO allocationVO) throws Exception {
		
		try{
		
    	        
			String departure = request.getParameter("departure"); 
			String arrival = request.getParameter("arrival");
			String departureArr[] = departure.split(",");
			String arrivalArr[] = arrival.split(",");
			String tempId = request.getParameter("tempId").toString();
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			BufferedReader    oBufReader = null;
		    HttpURLConnection httpConn   = null;
		
		    String strEncodeUrl = "https://apis.openapi.sk.com/tmap/routes?&appKey=l7xx6b701ff5595c4e3b859e42f57de0c975&"
		    		+ "&version=1&endX="+arrivalArr[0]+"&endY="+arrivalArr[1]
		    		+ "&startX="+departureArr[0]+"&startY="+departureArr[1]+"&carType=5&searchOption=4";
		    
		    URL oOpenURL = new URL(strEncodeUrl);
		  
		    httpConn =  (HttpURLConnection) oOpenURL.openConnection();          
		    httpConn.setRequestMethod("GET");          
		    httpConn.connect(); 
		    Thread.sleep(100);
		    oBufReader = new BufferedReader(new InputStreamReader(oOpenURL.openStream()));
		    
		    JSONParser jsonParser = new JSONParser();
		    JSONObject jsonObject = (JSONObject)jsonParser.parse(oBufReader);
		    
		    String jsonString = jsonObject.toJSONString();
		    
		    
		    Object obj = JSONValue.parse(jsonString);
		    JSONObject jsonObj = (JSONObject)obj;
		    
		    JSONArray list = (JSONArray)jsonObject.get("features");
		    JSONObject jsonObj2 = (JSONObject)list.get(0); 
		    
		    JSONObject summary = (JSONObject)jsonObj2.get("geometry"); 
		    JSONObject properties = (JSONObject)jsonObj2.get("properties");
		    
		    String pointType = properties.get("pointType").toString();
		    String  totalDistance = properties.get("totalDistance").toString();
		    
		    DecimalFormat form = new DecimalFormat("#.#");
		    //DecimalFormat form = new DecimalFormat("#.#");
		    float distanceF = Float.valueOf(totalDistance)/1000;
		    String distance =String.valueOf(form.format(distanceF));
		
		    String price = allocationService.paymentPrice(totalDistance);
		    double finalDistance = Math.ceil(Integer.parseInt(totalDistance)/1000);
  	      	String finalPrice =WebUtils.toNumFormat(Integer.parseInt(price));
		    
  	      Map<String,Object> tempConsignmentMap = allocationService.selectTempConsignmentReservation(paramMap); 
  	      	
  	      	
  	      	
  	      	mav.addObject("distance", finalDistance);
	        mav.addObject("price", finalPrice);
	        mav.addObject("tempConsignmentMap", tempConsignmentMap);
  	      	
		    //map.put("departure", WebUtils.getAddressToLngLat(departure));
		    //map.put("arrival", WebUtils.getAddressToLngLat(arrival));

  	      	
		   // mav.addObject("departureT", WebUtils.getAddressToLngLat(departure));
		    //mav.addObject("arrivalT", WebUtils.getAddressToLngLat(arrival));
		    
	        mav.addObject("distance", distance);
		    mav.addObject("departure", departure);
		    mav.addObject("arrival", arrival);
		    mav.addObject("departureArr", departureArr);
		    mav.addObject("arrivalArr", arrivalArr);
		    mav.addObject("tempId", tempId);
		    
    		String tmapAppKey = BaseAppConstants.TMAP_API_APPKEY;
			mav.addObject("tmapAppKey", tmapAppKey);
    		
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	
	@RequestMapping(value = "/loadView-many", method = RequestMethod.GET)
	public ModelAndView loadViewMany(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute AllocationVO allocationVO) throws Exception {
		
		try{
		
    	        
			String departure = request.getParameter("departure"); 
			String arrival = request.getParameter("arrival");
			String departureArr[] = departure.split(",");
			String arrivalArr[] = arrival.split(",");
			String tempId = request.getParameter("tempId").toString();
			String nowCount = request.getParameter("nowCount").toString();
			String carCount = request.getParameter("carCount").toString();
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			BufferedReader    oBufReader = null;
		    HttpURLConnection httpConn   = null;
		
		    String strEncodeUrl = "https://apis.openapi.sk.com/tmap/routes?&appKey=l7xx6b701ff5595c4e3b859e42f57de0c975&"
		    		+ "&version=1&endX="+arrivalArr[0]+"&endY="+arrivalArr[1]
		    		+ "&startX="+departureArr[0]+"&startY="+departureArr[1]+"&carType=5&searchOption=4";
		    
		    URL oOpenURL = new URL(strEncodeUrl);
		  
		    httpConn =  (HttpURLConnection) oOpenURL.openConnection();          
		    httpConn.setRequestMethod("GET");          
		    httpConn.connect(); 
		    Thread.sleep(100);
		    oBufReader = new BufferedReader(new InputStreamReader(oOpenURL.openStream()));
		    
		    JSONParser jsonParser = new JSONParser();
		    JSONObject jsonObject = (JSONObject)jsonParser.parse(oBufReader);
		    
		    String jsonString = jsonObject.toJSONString();
		    
		    
		    Object obj = JSONValue.parse(jsonString);
		    JSONObject jsonObj = (JSONObject)obj;
		    
		    JSONArray list = (JSONArray)jsonObject.get("features");
		    JSONObject jsonObj2 = (JSONObject)list.get(0); 
		    
		    JSONObject summary = (JSONObject)jsonObj2.get("geometry"); 
		    JSONObject properties = (JSONObject)jsonObj2.get("properties");
		    
		    String pointType = properties.get("pointType").toString();
		    String  totalDistance = properties.get("totalDistance").toString();
		    
		    DecimalFormat form = new DecimalFormat("#.#");
		    //DecimalFormat form = new DecimalFormat("#.#");
		    float distanceF = Float.valueOf(totalDistance)/1000;
		    String distance =String.valueOf(form.format(distanceF));
		
		    String price = allocationService.paymentPrice(totalDistance);
		    double finalDistance = Math.ceil(Integer.parseInt(totalDistance)/1000);
  	      	String finalPrice =WebUtils.toNumFormat(Integer.parseInt(price));
		    
  	      Map<String,Object> tempConsignmentMap = allocationService.selectTempConsignmentReservation(paramMap); 
  	      	
  	      	
  	      	
  	      	mav.addObject("distance", finalDistance);
	        mav.addObject("price", finalPrice);
	        mav.addObject("tempConsignmentMap", tempConsignmentMap);
  	      	
		    //map.put("departure", WebUtils.getAddressToLngLat(departure));
		    //map.put("arrival", WebUtils.getAddressToLngLat(arrival));

  	      	
		   // mav.addObject("departureT", WebUtils.getAddressToLngLat(departure));
		    //mav.addObject("arrivalT", WebUtils.getAddressToLngLat(arrival));
		    
	        mav.addObject("distance", distance);
		    mav.addObject("departure", departure);
		    mav.addObject("arrival", arrival);
		    mav.addObject("departureArr", departureArr);
		    mav.addObject("arrivalArr", arrivalArr);
		    mav.addObject("tempId", tempId);
		    mav.addObject("nowCount", nowCount);
		    mav.addObject("carCount", carCount);
		    
    		String tmapAppKey = BaseAppConstants.TMAP_API_APPKEY;
			mav.addObject("tmapAppKey", tmapAppKey);
    		
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	
	
	@RequestMapping(value = "/kakaoRouteView", method = RequestMethod.GET)
	public ModelAndView kakaoRouteView(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute AllocationVO allocationVO) throws Exception {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
	
			Map<String,Object> map = new HashMap<String,Object>();
			
			String call = request.getParameter("call") != null && !request.getParameter("call").toString().equals("") ? request.getParameter("call").toString():""; 
			
			
			if(userSessionMap != null) {
//				map.put("location_x", Double.parseDouble(locationMap.get("lat").toString()));
//				map.put("location_y", Double.parseDouble(locationMap.get("lng").toString()));
//				map.put("center_x",map.get("location_x"));
//				map.put("center_y",map.get("location_y"));		
	    		mav.addObject("map", map);
				
			}else {
					
			
			}
			
			mav.addObject("call", call);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	@RequestMapping(value = "/deleteAllocationFile", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi deleteAllocationFile(ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception{
		
		ResultApi resultApi = new ResultApi();
		
		try{
			
			String allocationId = request.getParameter("allocationId").toString();
			String[] allocationIdArr = allocationId.split(",");
			String groupId = allocationIdArr[0];
			String[] deleteFileIdArr = request.getParameterValues("deleteFileIdArr");
			int deletedFileCnt = 0;
			int notDeletedFileCnt = 0;
			Map<String, Object> returnMap = new HashMap<String, Object>();
			
			for(int j = 0; j < allocationIdArr.length; j++) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("allocationId", allocationIdArr[j]);
				
				Map<String, Object> allocation = allocationService.selectAllocation(map);
				
				//int allocationCount = allocationService.selectAllocationCountByPicturePointer(allocation);
				
				for(int i = 0; i < deleteFileIdArr.length; i++) {
					map.put("allocationFileId", deleteFileIdArr[i]);
					Map<String, Object> allocationFile = allocationFileService.selectAllocationFile(map);
					if(allocationFile != null) {		//해당파일이 존재 하는지 확인 하고 파일이 존재 하면 파일을 삭제 한다.
						
						if(!allocationFile.get("allocation_status").toString().equals("I")) {		//인수단계의 파일(서명파일,인수증)은 삭제 할 수 없다.
							subDir = "allocation";
							String uploadDir = rootDir + "/" + subDir;
							File file = new File(uploadDir+allocationFile.get("allocation_file_path").toString());
							if( file.exists() ){
					            if(file.delete()){
					                System.out.println("파일삭제 성공");
					            }else{
					                System.out.println("파일삭제 실패");
					            }
					        }else{
					            System.out.println("파일이 존재하지 않습니다.");
					        }
							deletedFileCnt++;
							allocationFileService.deleteAllocationFile(map);	
						}else {			//인수단계의 파일은 삭제 할 수 없다.
							notDeletedFileCnt++;
						}
						
					}

				}
				
				
			}
			
			
			
			
			returnMap.put("deletedFileCnt", deletedFileCnt);
			returnMap.put("notDeletedFileCnt", notDeletedFileCnt);
			
			resultApi.setResultData(returnMap);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return resultApi;
	}
	
	
	
	
	
	@RequestMapping(value = "/cal-list", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView calList(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
			
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				String customerId = userSessionMap.get("customer_id").toString();
				String searchWord = request.getParameter("searchWord").toString();
				String searchType = request.getParameter("searchType") == null || request.getParameter("searchType").toString().equals("") ? "": request.getParameter("searchType").toString();
				
				paramMap.put("customerId", customerId);
				paramMap.put("searchType", searchType);
				paramMap.put("searchWord", searchWord);
	
				
				Map<String, Object> customer = customerService.selectCustomer(paramMap);
				

				Map<String,Object> allocaitonCount= customerService.selectAllocationCntMonth(paramMap);
				PagingUtils.setPageing(request, Integer.parseInt(allocaitonCount.get("cnt").toString()), paramMap);
				List<Map<String,Object>> allocationList = customerService.selectSearchForAllocation(paramMap);
				
				
				mav.addObject("paramMap", paramMap);
				mav.addObject("allocationCount", allocaitonCount);
				mav.addObject("allocaitonList", allocationList);
				mav.addObject("customer", customer);
	
			}else {
				
				
				
				
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	
	@RequestMapping(value = "/getFinishInfoDataList", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi getFinishInfoDataList(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			//String curpage = request.getParameter("cPage").toString();				
			String customerId = userSessionMap.get("customer_id").toString();
			String searchWord = request.getParameter("searchWord")== null || request.getParameter("searchWord").toString().equals("") ? "": request.getParameter("searchWord").toString();
			
			
			//paramMap.put("startRownum", curpage);
			paramMap.put("customerId", customerId);
			paramMap.put("searchWord", searchWord);
			
			Map<String,Object> allocaitonCount= customerService.selectAllocationCntMonth(paramMap);
			PagingUtils.setPageing(request, Integer.parseInt(allocaitonCount.get("cnt").toString()), paramMap);
			List<Map<String,Object>> allocationList = customerService.selectSearchForAllocation(paramMap);
			
			int dataCnt = Integer.parseInt(allocaitonCount.get("cnt").toString()); 
			String total = Integer.toString(Math.round(dataCnt/20)).toString();
		
			
			paramMap.put("total", total);
			
			
			result.setResultData(allocationList);
			result.setResultDataSub(paramMap);
			
			   
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	
	
	
	@RequestMapping(value = "/cal-item", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView calItem(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				//String allocationStatus = request.getParameter("allocationStatus").toString() == null || request.getParameter("allocationStatus").equals("") ? "" :request.getParameter("allocationStatus").toString();
				String searchWord =WebUtils.getNow("yyyy-MM-dd").toString();
				String yearType = request.getParameter("yearType") == null || request.getParameter("yearType").toString().equals("")? WebUtils.getNow("yyyy") : request.getParameter("yearType").toString();
				
				//paramMap.put("allocationStatus", allocationStatus);
				paramMap.put("customerId", userSessionMap.get("customer_id").toString());
				paramMap.put("searchWord", searchWord);
				paramMap.put("LoginType", "customerIdLogin");
				paramMap.put("yearType", yearType);
				
				
				List<Map<String,Object>> yearMap = accountService.selectForYear(paramMap);

				mav.addObject("paramMap",paramMap);
				mav.addObject("yearList", yearMap);
			
			}else {
				
				
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	
	
	
	@RequestMapping(value = "/cal-list-detail", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView calListDetail(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
			
				
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				String customerId = userSessionMap.get("customer_id").toString();
				String searchType = request.getParameter("searchType") == null || request.getParameter("searchType").toString().equals("") ? WebUtils.getNow("yyyy-MM-dd"): request.getParameter("searchType").toString(); 
				//String searchWord = request.getParameter("searchWord").toString();
				
				paramMap.put("customerId", customerId);
				paramMap.put("searchType", searchType);
				
				
				List<Map<String,Object>> reciptBoxList = customerService.selectReciptBoxList(paramMap);

			 
				mav.addObject("paramMap", paramMap);
				mav.addObject("listData", reciptBoxList);
				
				
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	@RequestMapping(value = "/insertDriverDecideStatus", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi insertDriverDecideStatus(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			
			if(userSessionMap != null){
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				mav.addObject("paramMap", paramMap);
				
				String decideMonth = request.getParameter("decideMonth");
				DriverDecideStatusVO statusVO = new DriverDecideStatusVO();
				
				statusVO.setDecideMonth(decideMonth);
				statusVO.setDriverDecideStatusId("DDI"+UUID.randomUUID().toString().replaceAll("-", ""));
				statusVO.setDriverId(userSessionMap.get("driver_id").toString());
				statusVO.setListStatus("Y");
			
				driverDecideStatusService.insertDriverDecideStatus(statusVO);
				
			}else {
				
				result.setResultCode("E000");
			}
			
				
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	
	
	
	
	
	@RequestMapping(value = "/image-view", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView imageView(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
			
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				mav.addObject("paramMap", paramMap);
				//mav.addObject("driverId", userSessionMap.get("driver_id").toString());
			}else {
   				System.out.println("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n userSessionMap is null\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
   			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	

	@RequestMapping(value = "/reservation-ConsignmentList", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView reservationConsignmentList(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		//Map<String, Object> fileMap = new HashMap<String, Object>();

		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				String searchDate = request.getParameter("searchDate") == null || request.getParameter("searchDate").toString().equals("") ? WebUtils.getNow("yyyy-MM-dd").toString() : request.getParameter("searchDate").toString();					
				String searchWord = request.getParameter("searchWord") == null || request.getParameter("searchWord").toString().equals("") ? "D" : request.getParameter("searchWord").toString();					
				String allocationStatus = request.getParameter("allocationStatus") == null || request.getParameter("allocationStatus").toString().equals("") ? "": request.getParameter("allocationStatus").toString();
				
				
			
				paramMap.put("customerId",userSessionMap.get("customer_id").toString());
				paramMap.put("chargeId",userSessionMap.get("charge_id").toString());
				paramMap.put("searchDate",searchDate);
				paramMap.put("allocationStatus",allocationStatus);
				paramMap.put("searchWord",searchWord);
				
				
				List<Map<String,Object>> allocationStatusList = customerService.selectAllocationStatus(paramMap);
   	   			
				mav.addObject("listData",allocationStatusList);
				mav.addObject("paramMap",paramMap);
   	   	
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		

		return mav;
	}
	
	@RequestMapping(value = "/canseleAllocation", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi canseleAllocation(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			
			if(userSessionMap != null){
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
					if(paramMap != null ) {
						
						allocationService.deleteMileage(paramMap);
						allocationService.deleteAllocation(paramMap);
						allocationService.deleteCarInfo(paramMap);
						allocationService.deletePaymentInfo(paramMap);
						
						
						
					}else {
						
						result.setResultCode("0001");
						
					}
				
					
				}
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	/*
	
	@RequestMapping(value = "/viewRouteForPayment", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView viewRouteForPayment(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		

		
try{
			
			String departure = request.getParameter("departure"); 
			String arrival = request.getParameter("arrival");
			
			String departureArr[] = departure.split(",");
			String arrivalArr[] = arrival.split(",");
			
			Map<String,Double> map = new HashMap<String,Double>();
			
			String call = request.getParameter("call") != null && !request.getParameter("call").toString().equals("") ? request.getParameter("call").toString():"";
			
			map.put("departure_x", Double.parseDouble(departureArr[1]));
			map.put("departure_y", Double.parseDouble(departureArr[0]));
			map.put("arrival_x", Double.parseDouble(arrivalArr[1]));
			map.put("arrival_y", Double.parseDouble(arrivalArr[0]));
			
			if(map.get("departure_x")  > map.get("arrival_x")){
    			map.put("center_x",map.get("departure_x")-((map.get("departure_x")-map.get("arrival_x"))/2));
    		}else{
    			map.put("center_x",map.get("arrival_x")-((map.get("arrival_x")-map.get("departure_x"))/2));
    		}
    		if(map.get("departure_y")  > map.get("arrival_x")){
    			map.put("center_y",map.get("departure_y")-((map.get("departure_y")-map.get("arrival_y"))/2));
    		}else{
    			map.put("center_y",map.get("arrival_y")-((map.get("arrival_y")-map.get("departure_y"))/2));
    		}
			
    		mav.addObject("map", map);
    		mav.addObject("departure", departure);
    		mav.addObject("arrival", arrival);
    		mav.addObject("call", call);
    		
    		String jsonStrings = new String();
    		try {
    			String buf;
    	        URL url = new URL("https://naveropenapi.apigw.ntruss.com/map-direction/v1/driving?cartype=5&start="+departureArr[0]+","+departureArr[1]+"&goal="+arrivalArr[0]+","+arrivalArr[1]+"&option=trafast:traoptimal");
    	        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
    	        conn.setRequestMethod("GET");
    	        conn.setRequestProperty("X-Requested-With", "curl");
    	        conn.setRequestProperty("X-NCP-APIGW-API-KEY-ID", "t1kzgrc3sv");
    	        conn.setRequestProperty("X-NCP-APIGW-API-KEY", "CXGKmrSguaEOzEBc1bQy7Egqiq4gsdoat4vLeo2z");
    	        
    	        BufferedReader br = new BufferedReader(new InputStreamReader(
    	                conn.getInputStream(), "UTF-8"));
    	        while ((buf = br.readLine()) != null) {
    	            jsonStrings += buf;
    	        }	
    	        
    	        JSONParser jsonParser = new JSONParser();
    	        JSONObject jsonObject = (JSONObject)jsonParser.parse(jsonStrings);
    	        String jsonString = jsonObject.toJSONString();
    	        
    	       // System.out.println(jsonString);
    	        
    	        Object obj = JSONValue.parse(jsonString);
    	        //JSONObject jsonObj = (JSONObject)obj;
    	        
    	        	JSONObject route = (JSONObject)jsonObject.get("route");
    	        	
        	        JSONArray traoptimal = (JSONArray)route.get("traoptimal");
        	        JSONObject jsonObj2 = (JSONObject)traoptimal.get(0);
        	        
        	        JSONObject summary = (JSONObject)jsonObj2.get("summary");
        	        
        	        int distance = Integer.parseInt(summary.get("distance").toString());
        	        
        	        
        	        JSONArray pathList = (JSONArray)jsonObj2.get("path");
        	       
        	      String price = allocationService.paymentPrice(Integer.toString(distance));
        	       
        	      double finalDistance = Math.ceil(distance/1000);
        	      
        	      String finalPrice =WebUtils.toNumFormat(Integer.parseInt(price));
        	       
        	      
        	       
        	        List<Map<String, Double>> posList = new ArrayList<Map<String, Double>>();
        	        for(int i = 0; i < pathList.size(); i++) {
        	        	Map<String, Double> pos = new HashMap<String, Double>();
        	        	JSONArray path = (JSONArray)pathList.get(i);
        	        	pos.put("x",Double.parseDouble(path.get(1).toString()));
        	        	pos.put("y",Double.parseDouble(path.get(0).toString()));
        	        	posList.add(pos);
        	        //	System.out.println(pos);
        	        }
        	        
        	        mav.addObject("posList", posList);
        	        mav.addObject("distance", finalDistance);
        	        mav.addObject("price", finalPrice);
        	      //  mav.addObject("price", price);
    	        
    		}catch(Exception e) {
    			e.printStackTrace();
    		}
    		
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	*/
	

	@RequestMapping(value = "/viewRouteForPayment", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView viewRouteForPayment(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		
		
		try{
					
			/*
			String departure = WebUtils.getAddressInfo(request.getParameter("departureAddr").toString());
			String arrival = WebUtils.getAddressInfo(request.getParameter("arrivalAddr").toString());
			String resultStr = "&start="+WebUtils.getAddressToLatLng(departure)+"&destination="+WebUtils.getAddressToLatLng(arrival);
			String[] departurePointArr = WebUtils.getAddressToLatLng(departure).split(",");
			String[] arrivalPointArr = WebUtils.getAddressToLatLng(arrival).split(",");
			*/
			
			String departure = request.getParameter("departure"); 
			String arrival = request.getParameter("arrival");
			String departureArr[] = departure.split(",");
			String arrivalArr[] = arrival.split(",");
			
			BufferedReader    oBufReader = null;
		    HttpURLConnection httpConn   = null;
		
		    String strEncodeUrl = "https://apis.openapi.sk.com/tmap/routes?&appKey=l7xx6b701ff5595c4e3b859e42f57de0c975&"
		    		+ "&version=1&endX="+arrivalArr[0]+"&endY="+arrivalArr[1]
		    		+ "&startX="+departureArr[0]+"&startY="+departureArr[1]+"&carType=5&searchOption=4";
		    
		    URL oOpenURL = new URL(strEncodeUrl);
		  
		    httpConn =  (HttpURLConnection) oOpenURL.openConnection();          
		    httpConn.setRequestMethod("GET");          
		    httpConn.connect(); 
		    Thread.sleep(100);
		    oBufReader = new BufferedReader(new InputStreamReader(oOpenURL.openStream()));
		    
		    JSONParser jsonParser = new JSONParser();
		    JSONObject jsonObject = (JSONObject)jsonParser.parse(oBufReader);
		    
		    String jsonString = jsonObject.toJSONString();
		    
		    
		    Object obj = JSONValue.parse(jsonString);
		    JSONObject jsonObj = (JSONObject)obj;
		    
		    JSONArray list = (JSONArray)jsonObject.get("features");
		    JSONObject jsonObj2 = (JSONObject)list.get(0); 
		    
		    JSONObject summary = (JSONObject)jsonObj2.get("geometry"); 
		    JSONObject properties = (JSONObject)jsonObj2.get("properties");
		    
		    String pointType = properties.get("pointType").toString();
		    String  totalDistance = properties.get("totalDistance").toString();
		    
		    DecimalFormat form = new DecimalFormat("#.#");
		    float distanceF = Float.valueOf(totalDistance)/1000;
		    String distance =String.valueOf(form.format(distanceF));
		
		    String price = allocationService.paymentPrice(totalDistance);
		    double finalDistance = Math.ceil(Integer.parseInt(totalDistance)/1000);
  	      	String finalPrice =WebUtils.toNumFormat(Integer.parseInt(price));
		    
  	      	mav.addObject("distance", finalDistance);
	        mav.addObject("price", finalPrice);
  	      	
		    //map.put("departure", WebUtils.getAddressToLngLat(departure));
		    //map.put("arrival", WebUtils.getAddressToLngLat(arrival));
		    
		    mav.addObject("departure", departure);
		    mav.addObject("arrival", arrival);
		    mav.addObject("departureArr", departureArr);
		    mav.addObject("arrivalArr", arrivalArr);
		    
    		String tmapAppKey = BaseAppConstants.TMAP_API_APPKEY;
			mav.addObject("tmapAppKey", tmapAppKey);
			
			
    		
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	
	
	
	
	
	
	
	
	@RequestMapping(value = "/getDistense", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi getDistense(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			
			if(userSessionMap != null){
				
//				String departure = WebUtils.getAddressInfo(request.getParameter("departureAddress").toString());
//				String arrival = WebUtils.getAddressInfo(request.getParameter("arrivalAddress").toString());

				String departure = request.getParameter("departureAddress"); 
				String arrival = request.getParameter("arrivalAddress");
				
				String departureArr[] = departure.split(",");
				String arrivalArr[] = arrival.split(",");
				
				//String resultStr = "&start="+WebUtils.getAddressToLatLng(departure)+"&destination="+WebUtils.getAddressToLatLng(arrival);
		        
//				String[] departurePointArr = WebUtils.getAddressToLatLng(departure).split(",");
//				String[] arrivalPointArr = WebUtils.getAddressToLatLng(arrival).split(",");
				
				BufferedReader    oBufReader = null;
		        HttpURLConnection httpConn   = null;
		        

		  
		        String strEncodeUrl = "https://apis.openapi.sk.com/tmap/routes?&appKey=l7xx6b701ff5595c4e3b859e42f57de0c975&"
		        		+ "&version=1&endX="+arrivalArr[0]+"&endY="+arrivalArr[1]
		        		+ "&startX="+departureArr[0]+"&startY="+departureArr[1]+"&carType=5";
		        
		        URL oOpenURL = new URL(strEncodeUrl);
		      
		        httpConn =  (HttpURLConnection) oOpenURL.openConnection();          
		        httpConn.setRequestMethod("GET");          
		        httpConn.connect(); 
		        Thread.sleep(100);
		        oBufReader = new BufferedReader(new InputStreamReader(oOpenURL.openStream()));
		        
		        JSONParser jsonParser = new JSONParser();
		        JSONObject jsonObject = (JSONObject)jsonParser.parse(oBufReader);
		        
		        String jsonString = jsonObject.toJSONString();
		        
		        //System.out.println(jsonString);
		        
		        
		        Object obj = JSONValue.parse(jsonString);
		        JSONObject jsonObj = (JSONObject)obj;
		        
		        JSONArray list = (JSONArray)jsonObject.get("features");
		        JSONObject jsonObj2 = (JSONObject)list.get(0); 
		        
		        JSONObject summary = (JSONObject)jsonObj2.get("geometry"); 
		        JSONObject properties = (JSONObject)jsonObj2.get("properties");
		        
		        String pointType = properties.get("pointType").toString();
		        String  totalDistance = properties.get("totalDistance").toString();
		        
		        DecimalFormat form = new DecimalFormat("#.#");
		        float distanceF = Float.valueOf(totalDistance)/1000;
		        String distance =String.valueOf(form.format(distanceF));

				//Map<String, Object> map = new HashMap<String, Object>();
				//map.put("distance", distance);
		        //map.put("departure", WebUtils.getAddressToLngLat(departure));
		        //map.put("arrival", WebUtils.getAddressToLngLat(arrival));
		        
				result.setResultData(distance);
				
				
				
			}else {
				
				result.setResultCode("E000");
			}
			
				
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	
	
	
	@RequestMapping(value = "/insertDriverBillingStatus", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi insertDriverBillingStatus(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			
			if(userSessionMap != null){
				
				String driverId = userSessionMap.get("driver_id").toString();
				//driverId = "caosys";
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				String decideMonth = paramMap.get("decideMonth").toString();
				paramMap.put("driverId", driverId);
				mav.addObject("paramMap", paramMap);
				
				Map<String, Object> driverDeductFile = driverDeductFileService.selectDriverDeductFile(paramMap);
				
				if(driverDeductFile != null && driverDeductFile.get("driver_id") != null) {
					DriverBillingStatusVO billingVO = new DriverBillingStatusVO();
					billingVO.setSupplyVal(driverDeductFile.get("supply_val").toString());
					billingVO.setTotalVal(driverDeductFile.get("total_val").toString());
					billingVO.setVatVal(driverDeductFile.get("vat_val").toString());
					billingVO.setDecideMonth(decideMonth);
					billingVO.setDriverBillingStatusId("DBS"+UUID.randomUUID().toString().replaceAll("-", ""));
					billingVO.setDriverId(driverId);
					billingVO.setBillingStatus("Y");
					driverBillingStatusService.insertDriverBillingStatus(billingVO);	
					
					//공급받는자 보관용 파일을 생성 한다.
	   	   			
	   	   			Map<String, Object> map = new HashMap<String, Object>();
	   	   			map.put("decideMonth", decideMonth);
	   	   			map.put("driverId", driverId);
					map.put("supplyVal", driverDeductFile.get("supply_val").toString());
	   	   			map.put("vatVal", driverDeductFile.get("vat_val").toString());
				   	map.put("totalVal", driverDeductFile.get("total_val").toString());
				   	map.put("billingDivision", "O");
					driverBillingFileService.makeDriverBillingFileOtherSide(map);
					
				}else {
					result.setResultCode("E000");
				}
				
			}else {
				
				result.setResultCode("E000");
			}
			
				
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	
	
	
	
	
	@RequestMapping( value="/excel_download", method = RequestMethod.GET )
	public ModelAndView excel_download(ModelAndView mav,
			HttpServletRequest request ,HttpServletResponse response) throws Exception {
		
		ModelAndView view = new ModelAndView();
		
	try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				System.out.println("session is not null");
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				mav.addObject("paramMap", paramMap);
				
				List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

				String varNameList[] = null;
				String titleNameList[] = null;

				
				String selectMonth = request.getParameter("selectMonth");
				
				paramMap.put("driverId", userSessionMap.get("driver_id").toString());
				paramMap.put("decideMonth", selectMonth);
				
				list = allocationService.selectAllocationListForCal(paramMap);
				
				Map<String, Object> decideFinalMap = decideFinalService.selectDecideFinalByDecideMonth(paramMap);
				
				//최종확정이 된 경우 엑셀 다운로드시 금액을 표시 한다. 2019-04월의 경우 최종확정기능이 없었고 이미 처리 된 이후 이므로 금액까지 표시 해 준다.
				if(selectMonth.equals("2019-04") || decideFinalMap != null && decideFinalMap.get("decide_final_id") != null) {
					titleNameList = new String[]{"출발일","상차지","하차지","차종","차대번호","차량번호","계약번호","금액"};
					varNameList = new String[]{"departure_dt","departure","arrival","car_kind","car_id_num","car_num","contract_num","allocation_amount"};					
				}else {
					titleNameList = new String[]{"출발일","상차지","하차지","차종","차대번호","차량번호","계약번호"};
					varNameList = new String[]{"departure_dt","departure","arrival","car_kind","car_id_num","car_num","contract_num"};	
				}
				view.setViewName("excelDownloadView");
				view.addObject("list", list);
				view.addObject("varNameList", varNameList);
				view.addObject("titleNameList", titleNameList);
				view.addObject("excelName", "탁송내역("+selectMonth+").xlsx");
				
			}else {
				System.out.println("session is null");
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				mav.addObject("paramMap", paramMap);
				
				List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

				String varNameList[] = null;
				String titleNameList[] = null;

				String selectMonth = request.getParameter("selectMonth");
				paramMap.put("decideMonth", selectMonth);
				
				list = allocationService.selectAllocationListForCal(paramMap);
				
				Map<String, Object> decideFinalMap = decideFinalService.selectDecideFinalByDecideMonth(paramMap);
				
				//최종확정이 된 경우 엑셀 다운로드시 금액을 표시 한다. 2019-04월의 경우 최종확정기능이 없었고 이미 처리 된 이후 이므로 금액까지 표시 해 준다.
				if(selectMonth.equals("2019-04") || decideFinalMap != null && decideFinalMap.get("decide_final_id") != null) {
					titleNameList = new String[]{"출발일","상차지","하차지","차종","차대번호","차량번호","계약번호","금액"};
					varNameList = new String[]{"departure_dt","departure","arrival","car_kind","car_id_num","car_num","contract_num","allocation_amount"};					
				}else {
					titleNameList = new String[]{"출발일","상차지","하차지","차종","차대번호","차량번호","계약번호"};
					varNameList = new String[]{"departure_dt","departure","arrival","car_kind","car_id_num","car_num","contract_num"};	
				}
				
				view.setViewName("excelDownloadView");
				view.addObject("list", list);
				view.addObject("varNameList", varNameList);
				view.addObject("titleNameList", titleNameList);
				view.addObject("excelName", "탁송내역("+selectMonth+").xlsx");
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		
		
		
		return view;
		
	}
	
	
	@RequestMapping(value = "/bill_download", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView billDownload(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		Map<String, Object> fileMap = new HashMap<String, Object>();
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			String selectMonth = request.getParameter("selectMonth") == null ? "2019-07" : request.getParameter("selectMonth").toString();
   			String driverId = request.getParameter("driverId").toString();
   		
   			Map<String, Object> map = new HashMap<String, Object>();
   			map.put("decideMonth", selectMonth);
   			map.put("driverId", driverId);
   			//P : 공급자 보관용, O : 공급받는자 보관용 
   			map.put("billingDivision", "P");
   			
   			List<Map<String, Object>> driverBillingFile = driverBillingFileService.selectDriverBillingFileList(map);
   			
   			if(driverBillingFile != null && driverBillingFile.size() >= 1) {
   				mav.addObject("driverBillingFile", driverBillingFile.get(0));
   			}
			
   			fileMap.put("fileUploadPath", rootDir+"/billing_list");
			fileMap.put("fileLogicName", driverBillingFile.get(0).get("driver_billing_file_nm").toString());
			fileMap.put("filePhysicName", driverBillingFile.get(0).get("SAVE_NAME").toString());
			fileMap.put("driverId", driverId);
			fileMap.put("selectMonth", selectMonth);
   			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return new ModelAndView("downloadView", "downloadFile", fileMap);
		//return mav;
	}
	
	
	
	@RequestMapping(value = "/lowViolationList", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView lowViolationList(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			String driverId = userSessionMap.get("driver_id").toString();
   		
   			Map<String, Object> map = new HashMap<String, Object>();
   			map.put("driverId", driverId);
   			
   			List<Map<String, Object>> lowViolationList = lowViolationService.selectLowViolationList(map);
   			
   			mav.addObject("lowViolationList",lowViolationList );
   			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		
		return mav;
	}
	
	
	@RequestMapping(value = "/introduction", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView introduction(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				
				String lowViolationId = request.getParameter("lowViolationId");
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("lowViolationId", lowViolationId);
				Map<String, Object> lowViolation = lowViolationService.selectLowViolation(map);
				
				//목록을 확인 하면 읽음처리
				if(lowViolation != null && lowViolation.get("read_yn").toString().equals("N")) {
					map.put("readYn", "Y");
					map.put("driverId", userSessionMap.get("driver_id").toString());
					lowViolationService.updateLowViolationReadYn(map);	
				}
				
				lowViolation = lowViolationService.selectLowViolation(map);
				mav.addObject("lowViolation",  lowViolation);
					
				mav.addObject("driverId", userSessionMap.get("driver_id").toString());
					
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	@RequestMapping(value = "/updatePaymentYn", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updatePaymentYn(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			
			if(userSessionMap != null){
				
				String lowViolationId = request.getParameter("lowViolationId");
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				mav.addObject("paramMap", paramMap);
				
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("lowViolationId", lowViolationId);
				map.put("paymentYn", "Y");
				map.put("driverId", userSessionMap.get("driver_id").toString());
				//납부완료처리
				lowViolationService.updateLowViolationPaymentYn(map);
				
			}else {
				
				result.setResultCode("E000");
			}
			
				
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	@RequestMapping(value = "/myProfile-Page", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView myProfilePage(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				//paramMap.put("userId",userSessionMap.get("user_id").toString());
				
		
				paramMap.put("customerId", userSessionMap.get("customer_id").toString());
					
				
				List<Map<String,Object>> mileageList = customerService.selectMileageList(paramMap);
				Map<String,Object> mileageCount = customerService.selectMileageCount(paramMap);
				
				
				if(mileageCount == null) {
					
				
				}
				
				
				List<Map<String,Object>> graphList = customerService.selectAllocationGraph(paramMap);
	
			    JSONObject json = new JSONObject();
				
			    json.put("graphList", graphList);
			    
				
				mav.addObject("userMap",  userSessionMap);
				mav.addObject("listData",  mileageList);
				mav.addObject("mileageCount",  mileageCount);
				mav.addObject("graphList",  graphList);
		
			
				
				
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	
	
	@RequestMapping(value = "/mileage-page", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView mileagePage(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				

				paramMap.put("customerId", userSessionMap.get("customer_id").toString());
					
				
				List<Map<String,Object>> mileageList = customerService.selectMileageList(paramMap);
				Map<String,Object> mileageCount = customerService.selectMileageCount(paramMap);			
				
				if(mileageCount == null) {
					

				}
				
				mav.addObject("userMap",  userSessionMap);
				mav.addObject("listData",  mileageList);
				mav.addObject("paramMap",  paramMap);
				mav.addObject("mileageCount",  mileageCount);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	@RequestMapping(value = "/consignment-Reservation", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView consignmentReservation(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				
				//paramMap.put("userId",userSessionMap.get("user_id").toString());
				
				//Map<String,Object> chargeMap = customerService.selectCustomerIdAndCustomerAPP(paramMap);
				
			
				paramMap.put("phoneNum",userSessionMap.get("phone_num").toString());
				paramMap.put("chargeId",userSessionMap.get("charge_id").toString());
				paramMap.put("customerId", userSessionMap.get("customer_id").toString());
			
				Map<String, Object> allocationMap = allocationService.selectAllocation(paramMap);
				Map<String,Object> customerDetailMap =customerService.selectCustomerDetailInfo(paramMap);
				Map<String,Object> mileageCount = customerService.selectMileageCount(paramMap);
				Map<String,Object> tempConsignmentMap = allocationService.selectTempConsignmentReservation(paramMap); 
				
				 
				if(mileageCount == null) {
					
		
					
										
				}else {
				
				
				int tempMileage = Integer.parseInt(mileageCount.get("total").toString().replaceAll(",", ""));
				int availableMileage = (tempMileage / 1000 )*1000;
				paramMap.put("availableMileage", availableMileage);
				
				
				}
				
					
				mav.addObject("userMap",  userSessionMap);
				mav.addObject("mileageCount",  mileageCount);
				mav.addObject("paramMap",  paramMap);
				mav.addObject("customerDetailMap",  customerDetailMap);
				mav.addObject("allocationMap",  allocationMap);
				mav.addObject("tempConsignmentMap", tempConsignmentMap);
				
				
			}
			
		}catch(Exception e){
 			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	//탁송 예약 
	
	
	@RequestMapping(value = "/car-reservation", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView carReservation(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
			
				paramMap.put("phoneNum",userSessionMap.get("phone_num").toString());
				paramMap.put("chargeId",userSessionMap.get("charge_id").toString());
				paramMap.put("customerId", userSessionMap.get("customer_id").toString());
			
				Map<String, Object> allocationMap = allocationService.selectAllocation(paramMap);
				Map<String,Object> customerDetailMap =customerService.selectCustomerDetailInfo(paramMap);
				Map<String,Object> mileageCount = customerService.selectMileageCount(paramMap);
				Map<String,Object> tempConsignmentMap = allocationService.selectTempConsignmentReservation(paramMap); 
				
				 
				if(mileageCount == null) {
		
					
										
				}else {
				
				
				int tempMileage = Integer.parseInt(mileageCount.get("total").toString().replaceAll(",", ""));
				int availableMileage = (tempMileage / 1000 )*1000;
				paramMap.put("availableMileage", availableMileage);
				
				
				}
				
					
				mav.addObject("userMap",  userSessionMap);
				mav.addObject("mileageCount",  mileageCount);
				mav.addObject("paramMap",  paramMap);
				mav.addObject("customerDetailMap",  customerDetailMap);
				mav.addObject("allocationMap",  allocationMap);
				mav.addObject("tempConsignmentMap", tempConsignmentMap);
				
				
			}
			
		}catch(Exception e){
 			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	
	
	
	
	@RequestMapping(value = "/consignment-Reservation-many", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView consignmentReservationMany(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				
				//paramMap.put("userId",userSessionMap.get("user_id").toString());
				
				//Map<String,Object> chargeMap = customerService.selectCustomerIdAndCustomerAPP(paramMap);
				
			
				paramMap.put("phoneNum",userSessionMap.get("phone_num").toString());
				paramMap.put("chargeId",userSessionMap.get("charge_id").toString());
				paramMap.put("customerId", userSessionMap.get("customer_id").toString());
			
				Map<String, Object> allocationMap = allocationService.selectAllocation(paramMap);
				Map<String,Object> customerDetailMap =customerService.selectCustomerDetailInfo(paramMap);
				Map<String,Object> mileageCount = customerService.selectMileageCount(paramMap);
				Map<String,Object> tempConsignmentMap = allocationService.selectTempConsignmentReservation(paramMap); 
				
				 
				if(mileageCount == null) {
					
		
					
										
				}else {
				
				
				int tempMileage = Integer.parseInt(mileageCount.get("total").toString().replaceAll(",", ""));
				int availableMileage = (tempMileage / 1000 )*1000;
				paramMap.put("availableMileage", availableMileage);
				
				
				}
				
					
				mav.addObject("userMap",  userSessionMap);
				mav.addObject("mileageCount",  mileageCount);
				mav.addObject("paramMap",  paramMap);
				mav.addObject("customerDetailMap",  customerDetailMap);
				mav.addObject("allocationMap",  allocationMap);
				mav.addObject("tempConsignmentMap", tempConsignmentMap);
				
				
			}
			
		}catch(Exception e){
 			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	
	
	
	@RequestMapping(value = "/insert-allocation", method = {RequestMethod.POST,RequestMethod.GET})
	@ResponseBody
	public ResultApi insertAllocation(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,AllocationVO allocationVO , CarInfoVO carInfoVO ) throws Exception {
		
		ResultApi resultApi = new ResultApi();
		
		try{
			
			
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			Map<String, Object> map = new HashMap<String, Object>();
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			map.put("customerId", userMap.get("customer_id").toString());
			Map<String,Object> companyMap = allocationService.selectFindCompanyIdForCustomerApp(map);			
			
			
			String allocationId = "ALO"+UUID.randomUUID().toString().replaceAll("-","");
			String batchStatusId = "BAT"+UUID.randomUUID().toString().replaceAll("-","");
			String customerPayment= request.getParameter("customerPayment") == null ? "" :request.getParameter("customerPayment").toString();
			String customerEtc = request.getParameter("customerEtc") == null  ? "" : request.getParameter("customerEtc").toString();
		
			
			String departureAddr = request.getParameter("departureAddr").toString();
			String departure =	departureAddr.split(" ")[0]+" "+departureAddr.split(" ")[1];
			
			String arrivalAddr = request.getParameter("arrivalAddr").toString();
			String arrival = arrivalAddr.split(" ")[0]+" "+arrivalAddr.split(" ")[1];
			 
			allocationVO.setAllocationId(allocationId);
			allocationVO.setCustomerId(userMap.get("customer_id").toString());
					
					if(companyMap == null || companyMap.get("company_id") == null || companyMap.get("company_id").toString().equals("")) {
						
						allocationVO.setCompanyId("company2");
						allocationVO.setCompanyName("한국카캐리어(주)");
					}else {
						
						allocationVO.setCompanyId(companyMap.get("company_id").toString());
						allocationVO.setCompanyName(companyMap.get("company_name").toString());
					}
		
			allocationVO.setBatchStatus("N");
			allocationVO.setBatchStatusId(batchStatusId);
			allocationVO.setBatchIndex("0");
			allocationVO.setInputDt(WebUtils.getNow("yyyy-MM-dd").toString() + " ("+WebUtils.getDateDay(WebUtils.getNow("yyyy-MM-dd").toString(),"yyyy-MM-dd")+")");
			allocationVO.setRegType("A");
			
			
			//allocationService.insertAllocation(allocationVO);
			

			carInfoVO.setAllocationId(allocationId);
			carInfoVO.setDistanceType("C");
			carInfoVO.setCarrierType("S");
			carInfoVO.setRequireDnPicCnt("1");
			carInfoVO.setRequirePicCnt("1");
			carInfoVO.setDeparture(departure);
			carInfoVO.setArrival(arrival);
			carInfoVO.setReceiptSendYn("N");
			
			
			if(!customerPayment.equals("")) {
				carInfoVO.setSalesTotal(customerPayment);
				carInfoVO.setVat("0");
				carInfoVO.setPrice(customerPayment);
			}
			
			
			if(!customerEtc.equals("")) {
				carInfoVO.setEtc(customerEtc);
			}
			
			
			
			//carInfoService.insertCarInfo(carInfoVO);
			
			String paymentKind = "";
			String billingDivision = "";
			
			for(int i = 0; i < 3; i++) {
				
				PaymentInfoVO paymentInfoVO = new PaymentInfoVO();
				
				paymentInfoVO.setAllocationId(allocationId);
				paymentInfoVO.setDecideMonth("");	
				paymentInfoVO.setPaymentDivision("0"+(i+1));
				if(i == 0) {
					paymentInfoVO.setPaymentPartnerId(userMap.get("customer_id").toString());
					paymentInfoVO.setPaymentPartner(userMap.get("customer_name").toString());
					
					
					Map<String, Object> customer = customerService.selectCustomer(map);
					
					paymentKind = customer.get("payment_kind").toString();
					billingDivision = customer.get("billing_kind").toString();
					
					paymentInfoVO.setPaymentKind(paymentKind);
					paymentInfoVO.setBillingDivision(billingDivision);	
					
				}else {
					if(paymentKind.equals("DD")) {
						paymentInfoVO.setPaymentKind(paymentKind);
						paymentInfoVO.setBillingDivision(billingDivision);
					}else {
						paymentInfoVO.setPaymentKind("AT");

						paymentInfoVO.setBillingDivision("02");
					}	
					
				}
				paymentInfoVO.setPayment("N");
				paymentInfoVO.setEtc("");
				paymentInfoVO.setBillingStatus("N");
				paymentInfoVO.setSelectedStatus("N");
				paymentInfoVO.setDecideFinal("N");
				paymentInfoVO.setVatIncludeYn("N");
				paymentInfoVO.setVatExcludeYn("N");
				paymentInfoVO.setDecideStatus("N");
				
				if(!customerPayment.equals("") && paymentInfoVO.getPaymentDivision().equals("01")) {
					paymentInfoVO.setAmount(customerPayment);
				}
				
				//paymentInfoService.insertPaymentInfo(paymentInfoVO);	
			}
			
			
				SendSmsVO sendSmsVO = new SendSmsVO();
				String sendSmsId = "SMS"+UUID.randomUUID().toString().replaceAll("-","");
				
				
				sendSmsVO.setAllocationId(allocationId);
				sendSmsVO.setSendSmsId(sendSmsId);
				sendSmsVO.setType("9");
				sendSmsVO.setCustomerId(userMap.get("customer_id").toString());
	
				//sendSmsVO.setCallback("1577-5268");
				
				Map<String, Object> carInfoMap = new HashMap<String, Object>();
				carInfoMap.put("customerName", allocationVO.getCustomerName().toString());
				carInfoMap.put("departure", carInfoVO.getDeparture().toString());
				carInfoMap.put("arrival", carInfoVO.getArrival().toString());
				carInfoMap.put("carKind", carInfoVO.getCarKind().toString());
			
				
				//sendSmsService.insertSendSms(sendSmsVO,carInfoMap);
				resultApi.setResultCode("0000");
				
	
				String chooseMileage = request.getParameter("chooseMileage")== null || request.getParameter("chooseMileage").toString().equals("") ? "" : request.getParameter("chooseMileage").toString();
				
				if(!chooseMileage.equals("")) {
					
			
				Map<String,Object> mileageMap = new HashMap<String, Object>();
				//mileageMap.put("mileageId",  "MIL"+UUID.randomUUID().toString().replaceAll("-",""));
				mileageMap.put("customerId",userMap.get("customer_id").toString());
				mileageMap.put("customerName", userMap.get("customer_name").toString());
				mileageMap.put("mileages", chooseMileage);
				//mileageMap.put("mileageStatus","U");
				//mileageMap.put("mileageText","탁송의뢰사용");
				mileageMap.put("allocationId",allocationId);
				mileageMap.put("gubun","1");
				
				customerService.insertMileageCustomer(mileageMap);
				
				
				
				
				
				}else {
					
					
				}
			
				BufferedReader    oBufReader = null;
		        HttpURLConnection httpConn   = null;
				String strEncodeUrl = "http://52.78.153.148:8080/allocation/sendAllocationSocket.do?allocationId="+allocationId;
		        URL oOpenURL = new URL(strEncodeUrl);
		        httpConn =  (HttpURLConnection) oOpenURL.openConnection();          
		        httpConn.setRequestMethod("POST");          
		        httpConn.connect(); 
		        Thread.sleep(100);
		        oBufReader = new BufferedReader(new InputStreamReader(oOpenURL.openStream()));
			
				
			//WebUtils.messageAndRedirectUrl(mav,"예약이 정상적으로 등록 되었습니다.\r\n 예약 확정시 알림톡이 발송됩니다.", "/carrier/main.do");
			
			
		}catch(Exception e){
			e.printStackTrace();
			resultApi.setResultCode("0001");
			
			
			//WebUtils.messageAndRedirectUrl(mav,"알 수 없는 오류입니다. 카카오톡 채널로 문의하세요.", "/carrier/main.do");
		}
		

		return resultApi;
	}
	
	
	
	
	//탁송 예약 new 버전
	
	
	@RequestMapping(value = "/many-allocation", method = {RequestMethod.POST,RequestMethod.GET})
	@ResponseBody
	public ResultApi manyAllocation(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,AllocationVO allocationVO , CarInfoVO carInfoVO ) throws Exception {
		
		ResultApi resultApi = new ResultApi();
		
		try{
			
			
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			Map<String, Object> map = new HashMap<String, Object>();
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
 			map.put("customerId", userMap.get("customer_id").toString());
			
			String[] contentArr =request.getParameterValues("contentArr");
			String[] subcontentArr =request.getParameter("subcontentArr").split(",");
			
			List<String> contentList = Arrays.asList(contentArr); 
			List<String> subcontentList = Arrays.asList(subcontentArr); 
				
			
			int carCount = Integer.valueOf(contentList.get(4)); //차량대수 ex)2건이면 2번 인설트
			
			
			
			for(int v =0; v<carCount; v++ ) {
					
				int gap = 5;
				
				   
	                String departureDt = contentList.get(2).toString(); //출발일
	                String departureTime = contentList.get(3).toString(); //출발시간
	                
					String carKind = contentArr[(v*13)+gap];  //차종
					String carNum = contentArr[(v*13)+1+gap];  //차량번호
					String carIdNum = contentArr[(v*13)+2+gap];  //차대번호
					String customerPayment = contentArr[(v*13)+3+gap];  //탁송료
					String customerEtc = contentArr[(v*13)+4+gap];     //비고
					 
					
					
					String departureAddr = contentArr[(v*13)+5+gap];   //출발지주소
					String departurePersonInCharge = contentArr[(v*13)+7+gap]; //출발지담당자
					String departurePhone = contentArr[(v*13)+8+gap];  //출발지연락처
					 
					String arrivalAddr = contentArr[(v*13)+9+gap];  //도착지주소
					String arrivalPersonInCharge = contentArr[(v*13)+11+gap];  //도착지담당자
					String arrivalPhone = contentArr[(v*13)+12+gap];   //도착지연락처
					
					
					
					
					Map<String,Object> companyMap = allocationService.selectFindCompanyIdForCustomerApp(map);			
					
					String allocationId = "ALO"+UUID.randomUUID().toString().replaceAll("-","");
					String batchStatusId = "BAT"+UUID.randomUUID().toString().replaceAll("-","");
					//String customerPayment= request.getParameter("customerPayment") == null ? "" :request.getParameter("customerPayment").toString();
					//String customerEtc = request.getParameter("customerEtc") == null  ? "" : request.getParameter("customerEtc").toString();
					
				//	String departureAddr = request.getParameter("departureAddr").toString();
					String departure =	departureAddr.split(" ")[0]+" "+departureAddr.split(" ")[1];
					
					//String arrivalAddr = request.getParameter("arrivalAddr").toString();
					String arrival = arrivalAddr.split(" ")[0]+" "+arrivalAddr.split(" ")[1];
					 
					allocationVO.setAllocationId(allocationId);
					allocationVO.setCustomerId(userMap.get("customer_id").toString());
							
							if(companyMap == null || companyMap.get("company_id") == null || companyMap.get("company_id").toString().equals("")) {
								
								allocationVO.setCompanyId("company2");
								allocationVO.setCompanyName("한국카캐리어(주)");
							}else {
								
								allocationVO.setCompanyId(companyMap.get("company_id").toString());
								allocationVO.setCompanyName(companyMap.get("company_name").toString());
							}
				
					allocationVO.setBatchStatus("N");
					allocationVO.setBatchStatusId(batchStatusId);
					allocationVO.setBatchIndex("0");
					allocationVO.setCustomerName(userMap.get("customer_name").toString());
					allocationVO.setInputDt(WebUtils.getNow("yyyy-MM-dd").toString() + " ("+WebUtils.getDateDay(WebUtils.getNow("yyyy-MM-dd").toString(),"yyyy-MM-dd")+")");
					allocationVO.setRegType("A");
					allocationVO.setAllocationStatus("W");
					allocationVO.setCarCnt("1");
					allocationVO.setRegisterId(userMap.get("charge_id").toString());
					//allocationVO.setChargeName(userMap.get("name").toString());
					allocationVO.setChargeId(userMap.get("charge_id").toString());
					allocationVO.setRegisterName(userMap.get("name").toString());
					allocationVO.setChargeName(userMap.get("name").toString());
					allocationVO.setChargePhone(userMap.get("phoneNum").toString());
					
					
						allocationService.insertAllocation(allocationVO);
					
					carInfoVO.setAllocationId(allocationId);
					carInfoVO.setDistanceType("C");
					carInfoVO.setCarrierType("S");
					carInfoVO.setRequireDnPicCnt("1");
					carInfoVO.setRequirePicCnt("1");
					carInfoVO.setDeparture(departure);
					carInfoVO.setArrival(arrival);
					carInfoVO.setReceiptSendYn("N");
					carInfoVO.setDepartureAddr(departureAddr);
					carInfoVO.setArrivalAddr(arrivalAddr);
					carInfoVO.setCarKind(carKind);
					carInfoVO.setCarNum(carNum);
					carInfoVO.setCarIdNum(carIdNum);
					carInfoVO.setDeparturePersonInCharge(departurePersonInCharge);
					carInfoVO.setDeparturePhone(departurePhone);
					carInfoVO.setArrivalPersonInCharge(arrivalPersonInCharge);
					carInfoVO.setArrivalPhone(arrivalPhone);
					carInfoVO.setDepartureDt(departureDt);
					carInfoVO.setDepartureTime(departureTime);
					
					
					
					if(!customerPayment.equals("")) {
						carInfoVO.setSalesTotal(customerPayment);
						carInfoVO.setVat("0");
						carInfoVO.setPrice(customerPayment);
					}
					
					
					if(!customerEtc.equals("")) {
						carInfoVO.setEtc(customerEtc);
					}
					
					
					carInfoService.insertCarInfo(carInfoVO);
					
					String paymentKind = "";
					String billingDivision = "";
					
					for(int i = 0; i < 3; i++) {
						
						PaymentInfoVO paymentInfoVO = new PaymentInfoVO();
						
						paymentInfoVO.setAllocationId(allocationId);
						paymentInfoVO.setDecideMonth("");	
						paymentInfoVO.setPaymentDivision("0"+(i+1));
						if(i == 0) {
							paymentInfoVO.setPaymentPartnerId(userMap.get("customer_id").toString());
							paymentInfoVO.setPaymentPartner(userMap.get("customer_name").toString());
							
							
							Map<String, Object> customer = customerService.selectCustomer(map);
							
							paymentKind = customer.get("payment_kind").toString();
							billingDivision = customer.get("billing_kind").toString();
							
							paymentInfoVO.setPaymentKind(paymentKind);
							paymentInfoVO.setBillingDivision(billingDivision);	
							
						}else {
							if(paymentKind.equals("DD")) {
								paymentInfoVO.setPaymentKind(paymentKind);
								paymentInfoVO.setBillingDivision(billingDivision);
							}else {
								paymentInfoVO.setPaymentKind("AT");

								paymentInfoVO.setBillingDivision("02");
							}	
							
						}
						paymentInfoVO.setPayment("N");
						paymentInfoVO.setEtc("");
						paymentInfoVO.setBillingStatus("N");
						paymentInfoVO.setSelectedStatus("N");
						paymentInfoVO.setDecideFinal("N");
						paymentInfoVO.setVatIncludeYn("N");
						paymentInfoVO.setVatExcludeYn("N");
						paymentInfoVO.setDecideStatus("N");
						
						if(!customerPayment.equals("") && paymentInfoVO.getPaymentDivision().equals("01")) {
							paymentInfoVO.setAmount(customerPayment);
						}
						
						paymentInfoService.insertPaymentInfo(paymentInfoVO);	
					}
					
					
						SendSmsVO sendSmsVO = new SendSmsVO();
						String sendSmsId = "SMS"+UUID.randomUUID().toString().replaceAll("-","");
						
						
						sendSmsVO.setAllocationId(allocationId);
						sendSmsVO.setSendSmsId(sendSmsId);
						sendSmsVO.setType("9");
						sendSmsVO.setCustomerId(userMap.get("customer_id").toString());
			
						//sendSmsVO.setCallback("1577-5268");
						
						Map<String, Object> carInfoMap = new HashMap<String, Object>();
						carInfoMap.put("customerName", allocationVO.getCustomerName().toString());
						carInfoMap.put("departure", carInfoVO.getDeparture().toString());
						carInfoMap.put("arrival", carInfoVO.getArrival().toString());
						carInfoMap.put("carKind", carInfoVO.getCarKind().toString());
						
						sendSmsService.insertSendSms(sendSmsVO,carInfoMap);
						resultApi.setResultCode("0000");
						
			
						String chooseMileage = request.getParameter("chooseMileage")== null || request.getParameter("chooseMileage").toString().equals("") ? "" : request.getParameter("chooseMileage").toString();
						
							if(!chooseMileage.equals("")) {
								
						
							Map<String,Object> mileageMap = new HashMap<String, Object>();
							//mileageMap.put("mileageId",  "MIL"+UUID.randomUUID().toString().replaceAll("-",""));
							mileageMap.put("customerId",userMap.get("customer_id").toString());
							mileageMap.put("customerName", userMap.get("customer_name").toString());
							mileageMap.put("mileages", chooseMileage);
							//mileageMap.put("mileageStatus","U");
							//mileageMap.put("mileageText","탁송의뢰사용");
							mileageMap.put("allocationId",allocationId);
							mileageMap.put("gubun","1");
							
							customerService.insertMileageCustomer(mileageMap);
							
							
							}else {
								
								
							}
						
						BufferedReader    oBufReader = null;
				        HttpURLConnection httpConn   = null;
						String strEncodeUrl = "http://52.78.153.148:8080/allocation/sendAllocationSocket.do?allocationId="+allocationId;
				        URL oOpenURL = new URL(strEncodeUrl);
				        httpConn =  (HttpURLConnection) oOpenURL.openConnection();          
				        httpConn.setRequestMethod("POST");          
				        httpConn.connect(); 
				        Thread.sleep(100);
				        oBufReader = new BufferedReader(new InputStreamReader(oOpenURL.openStream()));
					
	            
				
			}
			
	
			
				
			//WebUtils.messageAndRedirectUrl(mav,"예약이 정상적으로 등록 되었습니다.\r\n 예약 확정시 알림톡이 발송됩니다.", "/carrier/main.do");
			
			
		}catch(Exception e){
			e.printStackTrace();
			resultApi.setResultCode("0001");
			
			
			//WebUtils.messageAndRedirectUrl(mav,"알 수 없는 오류입니다. 카카오톡 채널로 문의하세요.", "/carrier/main.do");
		}
		

		return resultApi;
	}
	
	
	
	
	
	
	@RequestMapping(value = "/update-reservation", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updateReservation(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response,
			@ModelAttribute CarInfoVO carInfoVO ) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			if(userSessionMap != null){
			
				if(paramMap.get("allocationStatus").equals("W") || paramMap.get("allocationStatus").equals("N") || paramMap.get("allocationStatus").equals("V")) {
					
						
					
					String departurePersonInCharge = request.getParameter("departurePersonInCharge") == null || request.getParameter("departurePersonInCharge").toString().equals("") ? "" : request.getParameter("departurePersonInCharge").toString();
					String departurePhone =request.getParameter("departurePhone") == null || request.getParameter("departurePhone").toString().equals("") ? "" : request.getParameter("departurePhone").toString();
					String arrivalPersonInCharge =request.getParameter("arrivalPersonInCharge") == null || request.getParameter("arrivalPersonInCharge").toString().equals("") ? "" : request.getParameter("arrivalPersonInCharge").toString();
					String arrivalPhone =request.getParameter("arrivalPhone") == null || request.getParameter("arrivalPhone").toString().equals("") ? "" : request.getParameter("arrivalPhone").toString();
					String carIdNum = request.getParameter("carIdNum")== null || request.getParameter("carIdNum").toString().equals("") ? "" : request.getParameter("carIdNum").toString(); 
					String carNum = request.getParameter("carNum")== null || request.getParameter("carNum").toString().equals("") ? "" : request.getParameter("carNum").toString(); 
					String carKind = request.getParameter("carKind")== null || request.getParameter("carKind").toString().equals("") ? "" : request.getParameter("carKind").toString(); 
					String customerPayment = request.getParameter("customerPayment")== null || request.getParameter("customerPayment").toString().equals("") ? "" : request.getParameter("customerPayment").toString(); 
					String customerEtc = request.getParameter("customerEtc")== null || request.getParameter("customerEtc").toString().equals("") ? "" : request.getParameter("customerEtc").toString(); 
					String departureAddr = request.getParameter("departureAddr").toString();
					String departure =	departureAddr.split(" ")[0]+" "+departureAddr.split(" ")[1];
					
					String arrivalAddr = request.getParameter("arrivalAddr").toString();
					String arrival = arrivalAddr.split(" ")[0]+" "+arrivalAddr.split(" ")[1];
							
							carInfoVO.setDepartureDt(paramMap.get("departureDt").toString());
							carInfoVO.setDepartureTime(paramMap.get("departureTime").toString());
							carInfoVO.setDeparturePersonInCharge(departurePersonInCharge);
							carInfoVO.setDeparturePhone(departurePhone);
							carInfoVO.setArrivalPersonInCharge(arrivalPersonInCharge);
							carInfoVO.setArrivalPhone(arrivalPhone);
							carInfoVO.setCarIdNum(carIdNum);
							carInfoVO.setCarKind(carKind);
							carInfoVO.setCarNum(carNum);
							carInfoVO.setDeparture(departure);
							carInfoVO.setDepartureAddr(departureAddr);
							carInfoVO.setArrival(arrival);
							carInfoVO.setArrivalAddr(arrivalAddr);
							carInfoVO.setSalesTotal(customerPayment);
							carInfoVO.setEtc(customerEtc);
							carInfoVO.setAllocationId(paramMap.get("allocationId").toString());
							
							carInfoService.updateReservationCarInfo(carInfoVO);
						
							
							
							if(paramMap.get("allocationStatus").equals("V")) {
								
								
								//allocationService.updateAllocationStatusV(paramMap);
								
								SendSmsVO sendSmsVO = new SendSmsVO();
								String sendSmsId = "SMS"+UUID.randomUUID().toString().replaceAll("-","");
								
								
								sendSmsVO.setAllocationId(paramMap.get("allocationId").toString());
								sendSmsVO.setSendSmsId(sendSmsId);
								sendSmsVO.setType("9");
								sendSmsVO.setCustomerId(userSessionMap.get("customer_id").toString());
					
								//sendSmsVO.setCallback("1577-5268");
								
								Map<String, Object> carInfoMap = new HashMap<String, Object>();
								carInfoMap.put("customerName", userSessionMap.get("customer_name").toString());
								carInfoMap.put("departure", carInfoVO.getDeparture().toString());
								carInfoMap.put("arrival", carInfoVO.getArrival().toString());
								carInfoMap.put("carKind", carInfoVO.getCarKind().toString());
							
								
								sendSmsService.insertSendSms(sendSmsVO,carInfoMap);
							}
							
							
							
							
							
					
				}else {
					
				//WebUtils.messageAndRedirectUrl(mav, "기사님이 지정됬으므로, \r\n 탁송 정보를 수정하실 수 없습니다.","/carrier/main.do");
					result.setResultCode("E001");
					
				}
				
				
			}else {
				
				result.setResultCode("E000");
			}
			
				
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	
	
	@RequestMapping(value = "/addressSearch", method = RequestMethod.GET)
	public ModelAndView addressSearch(Locale 
			locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute AllocationVO allocationVO) throws Exception {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
	
			Map<String,Object> map = new HashMap<String,Object>();
			
			String call = request.getParameter("call") != null && !request.getParameter("call").toString().equals("") ? request.getParameter("call").toString():""; 
			
			
			if(userSessionMap != null) {
//				map.put("location_x", Double.parseDouble(locationMap.get("lat").toString()));
//				map.put("location_y", Double.parseDouble(locationMap.get("lng").toString()));
//				map.put("center_x",map.get("location_x"));
//				map.put("center_y",map.get("location_y"));		
	    		mav.addObject("map", map);
				
			}else {
					
			
			}
			
			mav.addObject("call", call);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	
	
	@RequestMapping(value = "/searchCarrier", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView searchCarrier(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			// String electionId = request.getParameter("electionId").toString();
			
			if(userSessionMap != null){
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
//				Map<String, Object> map = new HashMap<String, Object>();
//				map.put("driverId", userSessionMap.get("driver_id").toString());
//				map.put("forUpdatePassWord", "");
//				Map<String, Object> driver = driverService.selectDriver(map);
//				map.put("electionId", electionId);
//				
//				Map<String, Object> election = electionService.selectElection(map);
//				mav.addObject("driver", driver);
//				mav.addObject("election", election);
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	

	
	
	@RequestMapping(value = "/insertVote", method = {RequestMethod.POST})
	 @ResponseBody
		public ResultApi insertVote(Locale locale,ModelAndView mav, HttpServletRequest request, 
				HttpServletResponse response,@ModelAttribute ElectionVO electionVO) throws Exception {
			
		 
		 	ResultApi result = new ResultApi();
		 
			try{
				
				
				HttpSession session = request.getSession();
				Map userSessionMap = (Map)session.getAttribute("user");
				
				String electionId = request.getParameter("electionId").toString();
				String voteInfoList = request.getParameter("voteInfoList").toString();
				
				
				if(userSessionMap != null) {
					
					
					JSONParser jsonParser = new JSONParser();
					JSONObject jsonObject = (JSONObject) jsonParser.parse(voteInfoList);
					JSONArray jsonArray = (JSONArray) jsonObject.get("voteInfoList");
					
					for(int i = 0; i < jsonArray.size(); i++){
						JSONObject voteInfo = (JSONObject)jsonArray.get(i);
						VoteVO voteVO = new VoteVO();	
						voteVO.setElectionId(electionId);
						voteVO.setPickCandidateAssign(voteInfo.get("assign").toString());
						voteVO.setPickCandidateId(voteInfo.get("candidateId").toString());
						voteVO.setPickCandidateName(voteInfo.get("name").toString());
						voteVO.setVoteId("VOT"+UUID.randomUUID().toString().replaceAll("-", ""));
						voteVO.setVoterId(userSessionMap.get("driver_id").toString());
						voteVO.setVoterName(userSessionMap.get("driver_name").toString());
						voteService.insertVote(voteVO);
					}
					
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("electionId", electionId);
					electionService.updateElectionJoinCount(map);

				}else {
					result.setResultCode("E000");
				}
				
				
			}catch(Exception e){
				e.printStackTrace();
			}
			

			return result;
		}
	
	
	
	
	
	@RequestMapping(value = "/updatePaymentInfo", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updatePaymentInfo(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			
			if(userSessionMap != null){
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				mav.addObject("paramMap", paramMap);
				
				String allocationId = paramMap.get("allocationId").toString();
				String paymentKind = paramMap.get("paymentKind").toString();
				
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("allocationId", allocationId);
				map.put("driverId", userSessionMap.get("driver_id").toString());
				Map<String, Object> allocationMap = allocationService.selectAllocation(map);
				
				if(allocationMap == null) {
					result.setResultCode("0001"); 
				}else {
					
					List<Map<String, Object>> allocationList = allocationService.selectAllocationList(map);
					
					Map<String, Object> payment = new HashMap<String, Object>();
					payment.put("allocationId", allocationId);
					List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(payment);
					
					if(paymentInfoList != null && paymentInfoList.size() > 0){
						for(int k = 0; k < paymentInfoList.size(); k++) {
							Map<String, Object> paymentInfo = paymentInfoList.get(k);
							if(paymentInfo.get("payment_division").toString().equals("01")) {		//매출처 
																	
									String debtorCreditorIdForD = "DCI"+UUID.randomUUID().toString().replaceAll("-","");
									
									//매출로 넘어 가지 않았고, 차변에 입력 되지 않은 경우에만.....
									//완료 되었고 현장수금 또는 현금 건인 경우 해당 배차를 확정 하고 차변에 입력 한다.
									if(allocationList.get(0).get("billing_status").toString().equals("N") && (allocationList.get(0).get("debtor_creditor_id") == null || allocationList.get(0).get("debtor_creditor_id").toString().equals(""))) {
											
										//현장 수금건 탁송 완료시 해당 건에 대해 확정 및 매출로 넘기고 ....
										String billPublishRequestId = "BPR"+UUID.randomUUID().toString().replaceAll("-","");
										Map<String, Object> carInfoUpdateMap = new HashMap<String, Object>();
										carInfoUpdateMap.put("allocationId", allocationList.get(0).get("allocation_id").toString());
										carInfoUpdateMap.put("billPublishRequestId", billPublishRequestId);
										carInfoUpdateMap.put("decideStatus", "Y");
										carInfoUpdateMap.put("billingStatus", "D");
										carInfoUpdateMap.put("debtorCreditorId", debtorCreditorIdForD);
										adjustmentService.updateCarInfoForBillPublish(carInfoUpdateMap);
										//차변에 입력--------------------------------------------------------------------------
										//세금계산서 미 발행건에 대해서 각 건별로 차변에 등록한다.
										DebtorCreditorVO debtorCreditorVO = new DebtorCreditorVO();	
										debtorCreditorVO.setCustomerId(allocationList.get(0).get("customer_id").toString());
										debtorCreditorVO.setDebtorCreditorId(debtorCreditorIdForD);
										debtorCreditorVO.setOccurrenceDt(allocationList.get(0).get("departure_dt").toString());
										debtorCreditorVO.setSummary("운송료 ("+allocationList.get(0).get("departure").toString()+"-"+allocationList.get(0).get("arrival").toString()+")");
										debtorCreditorVO.setTotalVal(allocationList.get(0).get("sales_total").toString());
										debtorCreditorVO.setPublishYn("N");			//미발행건
										debtorCreditorVO.setType(BaseAppConstants.DEBTOR_CREDITOR_TYPE_D);
										debtorCreditorVO.setRegisterId("driver");
										debtorCreditorService.insertDebtorCreditor(debtorCreditorVO);
											
									}
									
										//paymentKind = "DD";
									
										Map<String, Object> paymentUpdateMap = new HashMap<String, Object>();
										paymentUpdateMap.put("payment", "Y");
										paymentUpdateMap.put("paymentDt", allocationList.get(0).get("departure_dt").toString());
										paymentUpdateMap.put("allocationId", allocationList.get(0).get("allocation_id").toString());
										paymentUpdateMap.put("paymentKind", paymentKind);
										paymentUpdateMap.put("billingDivision", "00");
										paymentUpdateMap.put("idx", paymentInfo.get("idx").toString());
										
										//결제 여부 업데이트
										paymentInfoService.updatePaymentInfoForDDFinish(paymentUpdateMap);
										
										//대변 입력(대변에 입력 되어 있우에는 다시 입력 하지 않는다...)
										if(paymentInfo.get("debtor_creditor_id") == null || paymentInfo.get("debtor_creditor_id").toString().equals("")) {
									
											String debtorCreditorId = "DCI"+UUID.randomUUID().toString().replaceAll("-","");
											DebtorCreditorVO debtorCreditorVO = new DebtorCreditorVO();
											debtorCreditorVO.setCustomerId(allocationList.get(0).get("customer_id").toString());
											debtorCreditorVO.setDebtorCreditorId(debtorCreditorId);
											debtorCreditorVO.setOccurrenceDt(allocationList.get(0).get("departure_dt").toString());
											paymentUpdateMap.put("debtorCreditorId", debtorCreditorId);
																			
											String summaryDt = ""; 
											summaryDt ="운송료 ("+allocationList.get(0).get("departure").toString()+" - "+allocationList.get(0).get("arrival").toString()+" 입금)"; 
											debtorCreditorVO.setSummary(summaryDt);
											debtorCreditorVO.setTotalVal(allocationList.get(0).get("sales_total").toString());
											debtorCreditorVO.setPublishYn("N");
											debtorCreditorVO.setType(BaseAppConstants.DEBTOR_CREDITOR_TYPE_C);
											debtorCreditorVO.setRegisterId("driver");
											debtorCreditorVO.setDebtorId(debtorCreditorIdForD);
											//대변에 입력 하고 
											debtorCreditorService.insertDebtorCreditor(debtorCreditorVO);	
											paymentInfoService.updatePaymentInfoDebtorCreditorId(paymentUpdateMap);
										}
									
																		
							}else if(paymentInfo.get("payment_division").toString().equals("02")) {
								
								//paymentKind = "DD";
								
								Map<String, Object> paymentUpdateMap = new HashMap<String, Object>();
								paymentUpdateMap.put("payment", "Y");
								paymentUpdateMap.put("paymentDt", allocationList.get(0).get("departure_dt").toString());
								paymentUpdateMap.put("allocationId", allocationList.get(0).get("allocation_id").toString());
								paymentUpdateMap.put("paymentKind", paymentKind);
								paymentUpdateMap.put("billingDivision", "00");
								paymentUpdateMap.put("idx", paymentInfo.get("idx").toString());
								
								//결제 여부 업데이트(기사)
								paymentInfoService.updatePaymentInfoForDDFinish(paymentUpdateMap);
								
							}
							
							
						}
					
					
				}
				
				
				
			}
				
				
				
			}else {
				
				result.setResultCode("0001");
			}
			
				
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	
	@RequestMapping(value = "/three-option", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView threeOption(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
		   	Map<String, Object> map = new HashMap<String, Object>();
			
		   	Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
						
		   	mav.addObject("paramMap",paramMap);
					
		   
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	
	@RequestMapping(value = "/contact-main", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView contactMain(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
			
				Map<String, Object> map = new HashMap<String, Object>();
				
				/*mav.addObject("fList", fList);*/
				mav.addObject("driverId", userSessionMap.get("driver_id").toString());
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	@RequestMapping(value = "/customer-register", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView customerRegister(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
			
				
				Map<String, Object> map = new HashMap<String, Object>();
				mav.addObject("driverId", userSessionMap.get("driver_id").toString());
				
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	@RequestMapping(value = "/review-register", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView reviewRegister(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				Map<String, Object> allocationMap = allocationService.selectAllocation(paramMap);				
				
				
				mav.addObject("allocationMap",  allocationMap);
				mav.addObject("userMap",  userSessionMap);
				mav.addObject("paramMap",  paramMap);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	

	@RequestMapping(value = "/insert-Review", method = RequestMethod.POST)
	
	public ModelAndView insertReview(ModelAndView mav,
	HttpServletRequest request,HttpServletResponse response) throws Exception{
		
		

		try{

			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			Map<String,Object> paramMap = new HashMap<String, Object>();
			
			
			if(userSessionMap != null){
			
			paramMap = ParamUtils.getParamToMap(request);
			
			Map<String, Object> allocationMap = allocationService.selectAllocation(paramMap);	
			
 			String reviewId ="RIV"+UUID.randomUUID().toString().replaceAll("-","");
			String customerId  = userSessionMap.get("customer_id").toString();
			String userId = userSessionMap.get("user_id").toString();
			String customerName = userSessionMap.get("customer_name").toString();
			String pointsText = paramMap.get("reviewText").toString();
			String servicePoints = paramMap.get("servicePoints").toString();
			String batchStatusId = allocationMap.get("batch_status_id").toString();
			
			paramMap.put("reviewId", reviewId);
			paramMap.put("customerId",customerId);
			paramMap.put("userId", userId);
			paramMap.put("customerName", customerName);
			paramMap.put("pointsText", pointsText);
			paramMap.put("points", servicePoints);
			paramMap.put("batchStatusId", batchStatusId);
			
			customerService.insertReview(paramMap);

				Map<String,Object> carinfoMap = carInfoService.selectUseMileage(paramMap);
			
				if(carinfoMap.get("price") != null && !carinfoMap.get("price").equals("0")) {			
					Map<String,Object> mileageMap = new HashMap<String, Object>();
					mileageMap.put("customerId",customerId);
					mileageMap.put("customerName", customerName);
					mileageMap.put("mileages",carinfoMap.get("insertMileage").toString());
					mileageMap.put("allocationId",paramMap.get("allocationId").toString());
					mileageMap.put("gubun","3");
					customerService.insertMileageCustomer(mileageMap);
				}

			WebUtils.messageAndRedirectUrl(mav, "리뷰가 등록되었습니다. 마이페이지에서 마일리지를 확인해주세요.", "/carrier/allocation-detail.do?&allocationStatus=F&allocationId="+paramMap.get("allocationId").toString());			
			
			}else {
				
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
			
		}
		
		
		return mav;

	
	}

	
	@RequestMapping(value = "/review-list", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView reviewList(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
			
				Map<String,Object> paramMap =ParamUtils.getParamToMap(request);
//				String searchType = request.getParameter("searchType") == null || request.getParameter("searchType").toString().equals("") ? WebUtils.getNow("yyyy-MM-dd"): request.getParameter("searchType").toString();
//				paramMap.put("searchType", searchType);
				
				List<Map<String,Object>> reviewList = customerService.selectReviewList(paramMap);
	
				
				mav.addObject("listData", reviewList);
				mav.addObject("paramMap", paramMap);
				
				
			}else {
				

			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	//지울내용
	@RequestMapping(value = "/main-reservation", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView mainreservation(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
			
				paramMap.put("phoneNum",userSessionMap.get("phone_num").toString());
				paramMap.put("chargeId",userSessionMap.get("charge_id").toString());
				paramMap.put("customerId", userSessionMap.get("customer_id").toString());
			
				Map<String, Object> allocationMap = allocationService.selectAllocation(paramMap);
				Map<String,Object> customerDetailMap =customerService.selectCustomerDetailInfo(paramMap);
				Map<String,Object> mileageCount = customerService.selectMileageCount(paramMap);
				Map<String,Object> tempConsignmentMap = allocationService.selectTempConsignmentReservation(paramMap); 
				
				 
				if(mileageCount == null) {
		
					
										
				}else {
				
				
				int tempMileage = Integer.parseInt(mileageCount.get("total").toString().replaceAll(",", ""));
				int availableMileage = (tempMileage / 1000 )*1000;
				paramMap.put("availableMileage", availableMileage);
				
				
				}
				
					
				mav.addObject("userMap",  userSessionMap);
				mav.addObject("mileageCount",  mileageCount);
				mav.addObject("paramMap",  paramMap);
				mav.addObject("customerDetailMap",  customerDetailMap);
				mav.addObject("allocationMap",  allocationMap);
				mav.addObject("tempConsignmentMap", tempConsignmentMap);
				
				
			}
			
		}catch(Exception e){
 			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	
	@RequestMapping(value = "/review-view", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView reviewView(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
			
				Map<String,Object> paramMap =ParamUtils.getParamToMap(request);
				
				Map<String, Object> allocationMap = allocationService.selectAllocation(paramMap);
				List<Map<String,Object>> reviewList = customerService.selectReviewList(paramMap);
				
				String call = request.getParameter("call") != null && !request.getParameter("call").toString().equals("") ? request.getParameter("call").toString():"";
				String customerName =WebUtils.getMaskedName(allocationMap.get("customer_name").toString());
				String carKind = WebUtils.getMaskedName(allocationMap.get("car_kind").toString());
				
				paramMap.put("carKind", carKind);
				paramMap.put("customerName", customerName);
				paramMap.put("call", call);
				
				mav.addObject("listData", reviewList);
				mav.addObject("allocationMap", allocationMap);
				mav.addObject("paramMap", paramMap);
				mav.addObject("call", call);
				
				
				
			}else {

			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	@RequestMapping(value = "/agree-for-customer-register", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView agreeCustomerRegister(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
			
				Map<String, Object> map = new HashMap<String, Object>();
				mav.addObject("driverId", userSessionMap.get("driver_id").toString());
				
				
			}else {
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				
				mav.addObject("paramMap",paramMap);
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	@RequestMapping(value = "/phoneDuplicationChk", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi phoneDuplicationChk(ModelAndView mav,
	HttpServletRequest request,HttpServletResponse response) throws Exception{
		
		ResultApi result = new ResultApi();

		try{

			Map<String,Object> map = new HashMap<String, Object>();
			String phoneNum =request.getParameter("phoneNum").toString();
			
			map.put("phoneNum", phoneNum);
			Map<String,Object> checkMap = customerService.selectPhoneDuplicationChk(map);				
			if(checkMap == null) {
				
				
				result.setResultCode("0000");
				result.setResultMsg("성공");
			
			}else {
				
				result.setResultCode("0003");
				result.setResultMsg("실패");
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0002");
		}
		
		
		return result;

	
	}
	
	//차량 다수 예약 controller
	@RequestMapping(value = "/insertAllocationForCarCount", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi insertAllocationForCarCount(ModelAndView mav,
	HttpServletRequest request,HttpServletResponse response ,AllocationVO allocationVO, Locale locale, CarInfoVO carInfoVO) throws Exception{
		
		ResultApi resultApi = new ResultApi();

		try{
			
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			Map<String,Object> map = new HashMap<String, Object>();
			
			
			String carCount = request.getParameter("carCount").toString();
				
			int count = Integer.parseInt(carCount);
			
			
			for(int v=0; v< count; v++) {
				
				map.put("customerId", userMap.get("customer_id").toString());
				Map<String,Object> companyMap = allocationService.selectFindCompanyIdForCustomerApp(map);			
				
				
				String allocationId = "ALO"+UUID.randomUUID().toString().replaceAll("-","");
				String batchStatusId = "BAT"+UUID.randomUUID().toString().replaceAll("-","");
				String customerPayment= request.getParameter("customerPayment") == null ? "" :request.getParameter("customerPayment").toString();
				String customerEtc = request.getParameter("customerEtc") == null  ? "" : request.getParameter("customerEtc").toString();
				
				String departureAddr = "";
				String departure ="";
				
				String arrivalAddr = "";
				String arrival = "";
				
				
				 
				allocationVO.setAllocationId(allocationId);
				allocationVO.setCustomerId(userMap.get("customer_id").toString());
						
						if(companyMap == null || companyMap.get("company_id") == null || companyMap.get("company_id").toString().equals("")) {
							
							allocationVO.setCompanyId("company2");
							allocationVO.setCompanyName("한국카캐리어(주)");
						}else {
							
							allocationVO.setCompanyId(companyMap.get("company_id").toString());
							allocationVO.setCompanyName(companyMap.get("company_name").toString());
						}
			
				allocationVO.setBatchStatus("N");
				allocationVO.setBatchStatusId(batchStatusId);
				allocationVO.setBatchIndex("0");
				
				allocationVO.setInputDt(WebUtils.getNow("yyyy-MM-dd").toString() + " ("+WebUtils.getDateDay(WebUtils.getNow("yyyy-MM-dd").toString(),"yyyy-MM-dd")+")");
				allocationVO.setRegType("A");
				allocationVO.setAllocationStatus("V");
				allocationVO.setCustomerName(userMap.get("customer_name").toString());
				allocationVO.setChargeName(userMap.get("name").toString());
				
				allocationService.insertAllocation(allocationVO);
				
				carInfoVO.setAllocationId(allocationId);
				carInfoVO.setDistanceType("C");
				carInfoVO.setCarrierType("S");
				carInfoVO.setRequireDnPicCnt("1");
				carInfoVO.setRequirePicCnt("1");
				carInfoVO.setDeparture(departure);
				carInfoVO.setArrival(arrival);
				carInfoVO.setReceiptSendYn("N");
				
				
				
				if(!customerPayment.equals("")) {
					carInfoVO.setSalesTotal(customerPayment);
					carInfoVO.setVat("0");
					carInfoVO.setPrice(customerPayment);
				}
				
				
				if(!customerEtc.equals("")) {
					carInfoVO.setEtc(customerEtc);
				}
				
				
				
				carInfoService.insertCarInfo(carInfoVO);
				
				String paymentKind = "";
				String billingDivision = "";
				
				for(int i = 0; i < 3; i++) {
					
					PaymentInfoVO paymentInfoVO = new PaymentInfoVO();
					
					paymentInfoVO.setAllocationId(allocationId);
					paymentInfoVO.setDecideMonth("");	
					paymentInfoVO.setPaymentDivision("0"+(i+1));
					if(i == 0) {
						paymentInfoVO.setPaymentPartnerId(userMap.get("customer_id").toString());
						paymentInfoVO.setPaymentPartner(userMap.get("customer_name").toString());
						
						
						Map<String, Object> customer = customerService.selectCustomer(map);
						
						paymentKind = customer.get("payment_kind").toString();
						billingDivision = customer.get("billing_kind").toString();
						
						paymentInfoVO.setPaymentKind(paymentKind);
						paymentInfoVO.setBillingDivision(billingDivision);	
						
					}else {
						if(paymentKind.equals("DD")) {
							paymentInfoVO.setPaymentKind(paymentKind);
							paymentInfoVO.setBillingDivision(billingDivision);
						}else {
							paymentInfoVO.setPaymentKind("AT");

							paymentInfoVO.setBillingDivision("02");
						}	
						
					}
					paymentInfoVO.setPayment("N");
					paymentInfoVO.setEtc("");
					paymentInfoVO.setBillingStatus("N");
					paymentInfoVO.setSelectedStatus("N");
					paymentInfoVO.setDecideFinal("N");
					paymentInfoVO.setVatIncludeYn("N");
					paymentInfoVO.setVatExcludeYn("N");
					paymentInfoVO.setDecideStatus("N");
					
					if(!customerPayment.equals("") && paymentInfoVO.getPaymentDivision().equals("01")) {
						paymentInfoVO.setAmount(customerPayment);
					}
					
					paymentInfoService.insertPaymentInfo(paymentInfoVO);	
				}
				
				
					SendSmsVO sendSmsVO = new SendSmsVO();
					String sendSmsId = "SMS"+UUID.randomUUID().toString().replaceAll("-","");
					
					
					sendSmsVO.setAllocationId(allocationId);
					sendSmsVO.setSendSmsId(sendSmsId);
					sendSmsVO.setCustomerId(userMap.get("customer_id").toString());
		
					//sendSmsVO.setCallback("1577-5268");
					
					Map<String, Object> carInfoMap = new HashMap<String, Object>();
					carInfoMap.put("customerName", allocationVO.getCustomerName().toString());
					carInfoMap.put("departure", "");
					carInfoMap.put("arrival", "");
					carInfoMap.put("carKind", "");
				
					
					//sendSmsService.insertSendSms(sendSmsVO,carInfoMap);
					resultApi.setResultCode("0000");
					
		
					String chooseMileage = request.getParameter("chooseMileage")== null || request.getParameter("chooseMileage").toString().equals("") ? "" : request.getParameter("chooseMileage").toString();
					
					if(!chooseMileage.equals("")) {
						
				
					Map<String,Object> mileageMap = new HashMap<String, Object>();
					//mileageMap.put("mileageId",  "MIL"+UUID.randomUUID().toString().replaceAll("-",""));
					mileageMap.put("customerId",userMap.get("customer_id").toString());
					mileageMap.put("customerName", userMap.get("customer_name").toString());
					mileageMap.put("mileages", chooseMileage);
					//mileageMap.put("mileageStatus","U");
					//mileageMap.put("mileageText","탁송의뢰사용");
					mileageMap.put("allocationId",allocationId);
					mileageMap.put("gubun","1");
					
					customerService.insertMileageCustomer(mileageMap);
					
					
					
					
					
					}else {
						
						
					}
				
					//BufferedReader    oBufReader = null;
			        //HttpURLConnection httpConn   = null;
					//String strEncodeUrl = "http://52.78.153.148:8080/allocation/sendAllocationSocket.do?allocationId="+allocationId;
			        //URL oOpenURL = new URL(strEncodeUrl);
			        //httpConn =  (HttpURLConnection) oOpenURL.openConnection();          
			        //httpConn.setRequestMethod("POST");          
			        //httpConn.connect(); 
			        //Thread.sleep(100);
			        //oBufReader = new BufferedReader(new InputStreamReader(oOpenURL.openStream()));
				
				
				
			}
			


			
		}catch(Exception e){
			e.printStackTrace();
			resultApi.setResultCode("0002");
		}
		
		
		return resultApi;

	
	}
	
	
	
	
	
		
	
	
}
