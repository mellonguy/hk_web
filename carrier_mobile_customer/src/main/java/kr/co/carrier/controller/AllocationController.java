package kr.co.carrier.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.co.carrier.mapper.CustomerMapper;
import kr.co.carrier.service.AdjustmentService;
import kr.co.carrier.service.AlarmTalkService;
import kr.co.carrier.service.AllocationDivisionService;
import kr.co.carrier.service.AllocationFileService;
import kr.co.carrier.service.AllocationService;
import kr.co.carrier.service.BillingDivisionService;
import kr.co.carrier.service.CarInfoService;
import kr.co.carrier.service.CompanyService;
import kr.co.carrier.service.CustomerService;
import kr.co.carrier.service.DebtorCreditorService;
import kr.co.carrier.service.DriverService;
import kr.co.carrier.service.PayDivisionService;
import kr.co.carrier.service.PaymentDivisionService;
import kr.co.carrier.service.PaymentInfoService;
import kr.co.carrier.service.PersonInChargeService;
import kr.co.carrier.service.RunDivisionService;
import kr.co.carrier.utils.BaseAppConstants;
import kr.co.carrier.utils.PagingUtils;
import kr.co.carrier.utils.ParamUtils;
import kr.co.carrier.utils.ResultApi;
import kr.co.carrier.utils.WebUtils;
import kr.co.carrier.vo.AllocationVO;
import kr.co.carrier.vo.CarInfoVO;
import kr.co.carrier.vo.CustomerVO;
import kr.co.carrier.vo.DebtorCreditorVO;
import kr.co.carrier.vo.PaymentInfoVO;

@Controller
@RequestMapping(value="/allocation")
public class AllocationController {

	
	@Value("#{appProp['upload.base.path']}")
    private String rootDir;    
    private String subDir = "bbs";	
	
    
    @Autowired
    private AllocationService allocationService;
	
    @Autowired
    private CompanyService companyService;
    
    @Autowired
    private DriverService driverService;
    
    @Autowired
    private CarInfoService carInfoService;
    
    @Autowired
    private BillingDivisionService billingDivisionService;
    
    @Autowired
    private AllocationDivisionService allocationDivisionService;
    
    @Autowired
    private RunDivisionService runDivisionService;

    @Autowired
    private PaymentDivisionService paymentDivisionService;
    
    @Autowired
    private PayDivisionService payDivisionService;
    
    @Autowired
    private PaymentInfoService paymentInfoService;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private PersonInChargeService personInChargeService;
    
    @Autowired
    private AllocationFileService allocationFileService;
    
    @Autowired
    private AlarmTalkService alarmTalkService;
    
    @Autowired
    private DebtorCreditorService debtorCreditorService;
    
    @Autowired
    private AdjustmentService adjustmentService;
    
    
	@RequestMapping(value = "/combination", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView archive(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			/*if(paramMap.get("searchType") != null && !paramMap.get("searchType").equals("")){
				if(paramMap.get("searchType").equals("carrier")){
					if(paramMap.get("searchWord").equals("셀프")){
						paramMap.put("searchWord", "S");
					}else{
						paramMap.put("searchWord", "C");
					}
				}else if(paramMap.get("searchType").equals("distance")){
					if(paramMap.get("searchWord").equals("시내")){
						paramMap.put("searchWord", "C");
					}else if(paramMap.get("searchWord").equals("상행")){
						paramMap.put("searchWord", "U");
					}else if(paramMap.get("searchWord").equals("하행")){
						paramMap.put("searchWord", "D");
					}else if(paramMap.get("searchWord").equals("픽업")){
						paramMap.put("searchWord", "P");
					}
				}
			}*/
			
			String startDt = request.getParameter("startDt") == null ? "" : request.getParameter("startDt").toString();
			String endDt = request.getParameter("endDt") == null ? "" : request.getParameter("endDt").toString();
			
			/*if(startDt.equals("")) {
				startDt = WebUtils.getNow("yyyy-MM-dd");
			}
			if(endDt.equals("")) {
				endDt = WebUtils.getNow("yyyy-MM-dd");
			}*/
			String location = "combination";
			paramMap.put("location", location);
			paramMap.put("startDt", startDt);
			paramMap.put("endDt", endDt);
			
			int totalCount = allocationService.selectAllocationListCount(paramMap);
			PagingUtils.setPageing(request, totalCount, paramMap);
			paramMap.put("carrierType", "");
			List<Map<String, Object>> allocationList = allocationService.selectAllocationList(paramMap);
			Map<String, Object> map = new HashMap<String, Object>();
			List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
			List<Map<String, Object>> driverList = driverService.selectDriverList(map);
			List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
			mav.addObject("billingDivisionList",  billingDivisionList);
			mav.addObject("userMap",  userMap);
			mav.addObject("driverList",  driverList);
			mav.addObject("companyList",  companyList);
			mav.addObject("listData",  allocationList);
			
			String forOpen = request.getParameter("forOpen")==null?"":request.getParameter("forOpen").toString();
			mav.addObject("forOpen", forOpen);
			mav.addObject("paramMap", paramMap);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
			//dddddddd
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	@RequestMapping(value = "/self", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView self(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
		
			
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			paramMap.put("carrierType", "S");
			String allocationStatus = request.getParameter("allocationStatus")==null?"N":request.getParameter("allocationStatus").toString();
			paramMap.put("allocationStatus", allocationStatus);
			String startDt = request.getParameter("startDt") == null ? "" : request.getParameter("startDt").toString();
			String endDt = request.getParameter("endDt") == null ? "" : request.getParameter("endDt").toString();
			/*if(startDt.equals("")) {
				startDt = WebUtils.getNow("yyyy-MM-dd");
			}
			if(endDt.equals("")) {
				endDt = WebUtils.getNow("yyyy-MM-dd");
			}*/
			String location = "self";
			paramMap.put("location", location);
			paramMap.put("startDt", startDt);
			paramMap.put("endDt", endDt);
			int totalCount = allocationService.selectAllocationListCount(paramMap);
			PagingUtils.setPageing(request, totalCount, paramMap);
		
			
			String forOrder = request.getParameter("forOrder")==null?"":request.getParameter("forOrder").toString();
			if(!forOrder.equals("")){
				String[] sort = forOrder.split("\\^"); 
				paramMap.put("forOrder", sort[0]);
				paramMap.put("order", sort[1]);
				mav.addObject("order",  sort[1]);
			}
			
			List<Map<String, Object>> allocationList = allocationService.selectAllocationList(paramMap);
			Map<String, Object> map = new HashMap<String, Object>();
			List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
			List<Map<String, Object>> driverList = driverService.selectDriverList(map);
			List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
			mav.addObject("billingDivisionList",  billingDivisionList);
			mav.addObject("driverList",  driverList);
			mav.addObject("companyList",  companyList);
			String listOrder = "";
			String allocationId = "";
			for(int i = 0; i < allocationList.size(); i++){
				Map<String, Object> allocation = allocationList.get(i);
				listOrder += allocation.get("list_order").toString();
				allocationId += allocation.get("allocation_id").toString();
				if(i < allocationList.size()-1){
					listOrder += ",";	
					allocationId += ",";
				}
			}
			
			mav.addObject("userMap",  userMap);
			mav.addObject("allocationId",  allocationId);
			mav.addObject("listOrder",  listOrder);
			mav.addObject("listData",  allocationList);
			mav.addObject("paramMap", paramMap);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	@RequestMapping(value = "/carrier", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView carrier(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			paramMap.put("carrierType", "C");
			String allocationStatus = request.getParameter("allocationStatus")==null?"N":request.getParameter("allocationStatus").toString();
			paramMap.put("allocationStatus", allocationStatus);
			String startDt = request.getParameter("startDt") == null ? "" : request.getParameter("startDt").toString();
			String endDt = request.getParameter("endDt") == null ? "" : request.getParameter("endDt").toString();
			/*if(startDt.equals("")) {
				startDt = WebUtils.getNow("yyyy-MM-dd");
			}
			if(endDt.equals("")) {
				endDt = WebUtils.getNow("yyyy-MM-dd");
			}*/
			String location = "carrier";
			paramMap.put("location", location);
			paramMap.put("startDt", startDt);
			paramMap.put("endDt", endDt);
			int totalCount = allocationService.selectAllocationListCount(paramMap);
			PagingUtils.setPageing(request, totalCount, paramMap);
			
			String forOrder = request.getParameter("forOrder")==null?"":request.getParameter("forOrder").toString();
			if(!forOrder.equals("")){
				String[] sort = forOrder.split("\\^");
				paramMap.put("forOrder", sort[0]);
				paramMap.put("order", sort[1]);
				mav.addObject("order",  sort[1]);
			}
			List<Map<String, Object>> allocationList = allocationService.selectAllocationList(paramMap);
			Map<String, Object> map = new HashMap<String, Object>();
			List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
			List<Map<String, Object>> driverList = driverService.selectDriverList(map);
			List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
			mav.addObject("billingDivisionList",  billingDivisionList);
			mav.addObject("driverList",  driverList);
			mav.addObject("companyList",  companyList);
			String listOrder = "";
			String allocationId = "";
			for(int i = 0; i < allocationList.size(); i++){
				Map<String, Object> allocation = allocationList.get(i);
				listOrder += allocation.get("list_order").toString();
				allocationId += allocation.get("allocation_id").toString();
				if(i < allocationList.size()-1){
					listOrder += ",";	
					allocationId += ",";
				}
			}
			
			mav.addObject("userMap",  userMap);
			mav.addObject("allocationId",  allocationId);
			mav.addObject("listOrder",  listOrder);
			mav.addObject("listData",  allocationList);
			mav.addObject("paramMap", paramMap);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
						
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	@RequestMapping(value = "/insert-allocation", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView insertAllocation(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute AllocationVO allocationVO) throws Exception {
		
		try{
			
			Map<String, Object> map = new HashMap<String, Object>();
			
			map.put("duplicateChk", "true");
			
			String carInfoVal = request.getParameter("carInfoVal").toString();
			String paymentInfoVal = request.getParameter("paymentInfoVal").toString();
			
			if(!carInfoVal.equals("")){
				JSONParser jsonParser = new JSONParser();
				JSONObject jsonObject = (JSONObject) jsonParser.parse(carInfoVal);
				JSONArray jsonArray = (JSONArray) jsonObject.get("carInfoList");	
				String batchStatusId = "BAT"+UUID.randomUUID().toString().replaceAll("-","");
				for(int i = 0; i < jsonArray.size(); i++){
					
					String allocationId = "ALO"+UUID.randomUUID().toString().replaceAll("-","");
					allocationVO.setAllocationId(allocationId);
					JSONObject carInfo = (JSONObject)jsonArray.get(i);
					CarInfoVO carInfoVO = new CarInfoVO();
					carInfoVO.setAllocationId(allocationId);
					carInfoVO.setCarrierType(carInfo.get("carrier_type").toString());
					carInfoVO.setDistanceType(carInfo.get("distance_type").toString());
					carInfoVO.setDepartureDt(carInfo.get("departure_dt").toString());
					carInfoVO.setDepartureTime(carInfo.get("departure_time").toString());
					carInfoVO.setDriverId(carInfo.get("driver_id").toString());
					map.put("driverId", carInfo.get("driver_id").toString());
					Map<String, Object> driver = driverService.selectDriver(map);
					if(driver != null){
						carInfoVO.setDriverName(driver.get("driver_name").toString());	
					}
					if(driver != null || !carInfo.get("driver_name").toString().equals("")) {		//신규 배차 등록시 기사가 배정 되어 있으면 미배차가 아니고 배차된 상태이다.
						allocationVO.setAllocationStatus("Y");
					}else {
						allocationVO.setAllocationStatus("N");
					}
					if(allocationVO.getBatchStatus().equals("N")) {//개별								//일괄 등록과 개별 등록 구분
						allocationVO.setBatchStatusId("BAT"+UUID.randomUUID().toString().replaceAll("-",""));
						allocationVO.setBatchIndex(String.valueOf(0));
					}else if(allocationVO.getBatchStatus().equals("Y")) {//일괄
						allocationVO.setBatchStatusId(batchStatusId);
						allocationVO.setBatchIndex(String.valueOf(i));
					}
					allocationService.insertAllocation(allocationVO);
					carInfoVO.setCarKind(carInfo.get("car_kind").toString());
					carInfoVO.setCarIdNum(carInfo.get("car_id_num").toString());
					carInfoVO.setCarNum(carInfo.get("car_num").toString());
					carInfoVO.setContractNum(carInfo.get("contract_num").toString());
					carInfoVO.setTowDistance(carInfo.get("tow_distance").toString());
					carInfoVO.setAccidentYn(carInfo.get("accident_yn").toString());
					carInfoVO.setEtc(carInfo.get("etc").toString());
					carInfoVO.setDeparture(carInfo.get("departure").toString());
					carInfoVO.setDepartureAddr(carInfo.get("departure_addr").toString());
					carInfoVO.setDeparturePersonInCharge(carInfo.get("departure_person_in_charge").toString());
					carInfoVO.setDeparturePhone(carInfo.get("departure_phone").toString());
					carInfoVO.setArrival(carInfo.get("arrival").toString());
					carInfoVO.setArrivalAddr(carInfo.get("arrival_addr").toString());
					carInfoVO.setArrivalPersonInCharge(carInfo.get("arrival_person_in_charge").toString());
					carInfoVO.setArrivalPhone(carInfo.get("arrival_phone").toString());
					carInfoService.insertCarInfo(carInfoVO);
					
					String carInfoIndex = carInfo.get("index").toString();
					if(!paymentInfoVal.equals("")){
						JSONParser jsonPaymentParser = new JSONParser();
						JSONObject jsonPaymentObject = (JSONObject) jsonPaymentParser.parse(paymentInfoVal);
						JSONArray jsonPaymentArray = (JSONArray) jsonPaymentObject.get("paymentInfoList");	
						for(int j = 0; j < jsonPaymentArray.size(); j++){
							JSONObject paymentInfo = (JSONObject)jsonPaymentArray.get(j);
							if(paymentInfo.get("index").toString().equals(carInfoIndex)) {
								PaymentInfoVO paymentInfoVO = new PaymentInfoVO();
								paymentInfoVO.setAllocationId(allocationId);
								paymentInfoVO.setAmount(paymentInfo.get("amount").toString());
								paymentInfoVO.setBillForPayment(paymentInfo.get("bill_for_payment").toString());
								paymentInfoVO.setBillingDivision(paymentInfo.get("billing_division").toString());
								paymentInfoVO.setBillingDt(paymentInfo.get("billing_dt").toString());
								paymentInfoVO.setDeductionRate(paymentInfo.get("deduction_rate").toString());
								paymentInfoVO.setEtc(paymentInfo.get("etc").toString());
								paymentInfoVO.setPayment(paymentInfo.get("payment").toString());
								paymentInfoVO.setPaymentDivision(paymentInfo.get("payment_division").toString());
								paymentInfoVO.setPaymentDt(paymentInfo.get("payment_dt").toString());
								paymentInfoVO.setPaymentKind(paymentInfo.get("payment_kind").toString());
								paymentInfoVO.setPaymentPartner(paymentInfo.get("payment_partner").toString());
								paymentInfoVO.setPaymentPartnerId(paymentInfo.get("payment_partner_id").toString());
								paymentInfoService.insertPaymentInfo(paymentInfoVO);
							}
						}
					}
					
				}
			}
			
			WebUtils.messageAndRedirectUrl(mav, "등록되었습니다.", "/allocation/combination.do?forOpen=N");

		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	@RequestMapping(value = "/update-order", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updateListOrder(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			String oldListOrder = request.getParameter("oldListOrder").toString();
			String newlistOrder = request.getParameter("newlistOrder").toString();
			String allocationId = request.getParameter("allocationId").toString();
			Map<String,Object> map = new HashMap<String, Object>();
			String[] oldListOrderArr = oldListOrder.split(",");
			String[] newlistOrderArr = newlistOrder.split(",");
			String[] allocationIdArr = allocationId.split(",");
			int arrLen = oldListOrderArr.length;
			List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
			if(oldListOrderArr.length != newlistOrderArr.length){
				result.setResultCode("0002");
			}else{
				boolean changed = false;
				for(int i = 0; i < arrLen; i++){
					String listOrder = newlistOrderArr[i];
					map.put("listOrder", newlistOrderArr[i]);
					Map<String, Object> allocationMap = allocationService.selectAllocationByListOrder(map);
					for(int j = 0; j < arrLen; j++){
						if(listOrder.equals(oldListOrderArr[j].toString())){
							if(!allocationIdArr[j].toString().equals(allocationMap.get("allocation_id"))){
								changed = true;
								break;
							}
						}
					}
					list.add(allocationMap);
				}
				
				if(!changed){
					if(list.size() != arrLen){
						result.setResultCode("0003");
					}
					map.clear();
					for(int i = 0; i < list.size(); i++){
						Map<String, Object> allocationMap = list.get(i);
						map.put("oldlistOrder", oldListOrderArr[i]);
						map.put("allocationId",allocationMap.get("allocation_id").toString());
						allocationService.updateAllocationListOrder(map);
					}
	
				}else{
					result.setResultCode("0004");
				}
			}
		
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	
	@RequestMapping(value = "/update-allocation-sub", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updateAllocationSub(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			Map<String, Object> map = new HashMap<String, Object>();		
				map.put("inputDt", request.getParameter("inputDt").toString());
				map.put("carrierType", request.getParameter("carrierType").toString());
				map.put("distanceType", request.getParameter("distanceType").toString());
				map.put("departureDt", request.getParameter("departureDt").toString());
				map.put("departureTime", request.getParameter("departureTime").toString());
				map.put("customerName", request.getParameter("customerName").toString());
				map.put("carKind", request.getParameter("carKind").toString());
				map.put("carIdNum", request.getParameter("carIdNum").toString());
				map.put("carNum", request.getParameter("carNum").toString());
				map.put("departure", request.getParameter("departure").toString());
				map.put("arrival", request.getParameter("arrival").toString());
				map.put("driverName", request.getParameter("driverName").toString());
				map.put("carCnt", request.getParameter("carCnt").toString());
				map.put("amount", request.getParameter("amount").toString());
				map.put("paymentKind", request.getParameter("paymentKind").toString());
				map.put("allocationId", request.getParameter("allocationId").toString());
				allocationService.updateAllocationByMap(map);
	
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	@RequestMapping(value = "/selectAllocationInfo", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi selectAllocationInfo(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			Map<String, Object> map = new HashMap<String, Object>();		
				map.put("allocationId", request.getParameter("allocationId").toString());
				Map<String, Object> allocationMap = allocationService.selectAllocation(map);
				List<Map<String, Object>> carInfoList = carInfoService.selectCarInfoList(map);
				List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(map);
				
				result.setResultData(allocationMap);
				result.setResultDataSub(carInfoList);
				result.setResultDataThird(paymentInfoList);
				
				
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	@RequestMapping(value = "/updateAllocationStatus", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updateAllocationStatus(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response, CustomerVO customerVO 
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
	
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			String allocationStatus = request.getParameter("allocationStatus").toString();
			String allocationId = request.getParameter("allocationId").toString();
			String[] allocationIdArr = allocationId.split(",");
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			String senderId = userSessionMap.get("driver_id").toString();
			String sendAllocationFileId = "SFI"+UUID.randomUUID().toString().replaceAll("-","");
		
			
			for(int i = 0; i < allocationIdArr.length; i++){
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("allocationId", allocationIdArr[i]);
				map.put("driverId", userSessionMap.get("driver_id").toString());
				
				Map<String, Object> allocationMap = allocationService.selectAllocation(map);

				if(allocationMap == null) {
					
					result.setResultCode("E000");//해당 배차를 승인 할 수 없는경우 
				}else {
					//map.put("batchStatusId", allocationMap.get("batch_status_id"));
					List<Map<String, Object>> allocationList = allocationService.selectAllocationList(map);
					for(int j = 0; j < allocationList.size(); j++) {
						Map<String, Object> updateMap = new HashMap<String, Object>();
						
						//if(allocationIdArr.length == 1 && (allocationStatus.equals("B") || allocationStatus.equals("P"))) {			//하차검사 완료 또는 인계 완료시
						if(allocationStatus.equals("B") || allocationStatus.equals("P")) {			//하차검사 완료 또는 인계 완료시
							Map<String, Object> selectMapI = new HashMap<String, Object>();
							//selectMap.put("allocationId", allocationList.get(j).get("allocation_id").toString());
							selectMapI.put("picturePointerI", allocationList.get(j).get("picture_pointer_i").toString());
							selectMapI.put("allocationStatus", "I");
							selectMapI.put("resultStatus", "");
							List<Map<String, Object>> iStatusFileList = allocationFileService.selectAllocationFileList(selectMapI);
							Map<String, Object> selectMapD = new HashMap<String, Object>();
							selectMapD.put("picturePointerD", allocationList.get(j).get("picture_pointer_d").toString());
							selectMapD.put("allocationStatus", "D");
							
							//하차 완료되면........
							
							//인수증 자동 전송을 사용 하는 업체인지 확인 하고...
							Map<String, Object> customerSelectMap = new HashMap<String, Object>();
							
							customerSelectMap.put("customerId", allocationMap.get("customer_id").toString());
							
					
							
							Map <String,Object> customer = customerService.selectCustomer(customerSelectMap);
							
							
							if(allocationStatus.equals("B") && customer.get("auto_send_yn").toString().equals("Y") ) {
								alarmTalkService.yun(allocationMap,map);
							
								
								
							}
						
							
							
							List<Map<String, Object>> dStatusFileList = allocationFileService.selectAllocationFileList(selectMapD);
							if(iStatusFileList.size() >= 1 && dStatusFileList.size() >= Integer.parseInt(allocationList.get(j).get("require_dn_pic_cnt").toString())) {			//둘다 완료 된 경우는 완료 상태로 바꿔준다.
								allocationStatus = "F";
								selectMapD.put("resultStatus", "F");
							}
							
							/*
							if(allocationStatus.equals("P")) {
								if(allocationMap.get("dept").toString().equals("현대캐피탈")) {
									allocationStatus = "F";
									selectMapD.put("resultStatus", "F");	
								}
							}
							*/
							
							//하차 완료 상태이고 인수증 전송을 하지 않아도 되는 탁송건은 완료된 건.
							/*if(allocationStatus.equals("B") && allocationList.get(j).get("receipt_send_yn").toString().equals("N")) {
								allocationStatus = "F";
								selectMap.put("resultStatus", "F");
							}*/
						
							
							//String personInChargeStr = allocationMap.get("arrival_person_in_charge") != null && !allocationMap.get("arrival_person_in_charge").toString().equals("") ? allocationMap.get("arrival_person_in_charge").toString() : "";
							
							
									
							
								result.setResultData(selectMapD);
						}
						
						//String test = allocationList.get(j).get("allocation_status").toString();
						
						if(allocationList.get(j).get("allocation_status_cd").toString().equals("B") || allocationList.get(j).get("allocation_status_cd").toString().equals("P")) {
							if(request.getParameter("forLotte") != null && request.getParameter("forLotte").toString().equals("Y")) {			//롯데건인 경우 상차,하차,인계를 생략 할 수 있다.
								if(allocationStatus.equals("B") || allocationList.get(j).get("allocation_status_cd").toString().equals("P")) {		//B 상태에서 P 로 변경 하는경우
									allocationStatus = "F";
								}else if(allocationStatus.equals("P") || allocationList.get(j).get("allocation_status_cd").toString().equals("B")) {	//P 상태에서 B 로 변경 하는경우
									allocationStatus = "F";
								} 
							}
							
						}
						
						if(allocationStatus.equals("P")) {
							if(allocationMap.get("dept").toString().equals("현대캐피탈")) {
									allocationStatus = "F";
							}	
						}
						
						updateMap.put("allocationStatus", allocationStatus);
						updateMap.put("allocationId", allocationList.get(j).get("allocation_id").toString());
						allocationService.updateAllocationStatus(updateMap);	
						
						Map<String, Object> findCustomerMap = new HashMap<String, Object>();
						findCustomerMap.put("customerId", allocationList.get(j).get("customer_id").toString());
						Map<String, Object> customer = customerService.selectCustomer(findCustomerMap);
						
						Map<String, Object> alarmTalkMap = new HashMap<String, Object>();
						alarmTalkMap.put("departure", allocationMap.get("departure").toString());
						alarmTalkMap.put("arrival", allocationMap.get("arrival").toString());
						alarmTalkMap.put("driver_name", allocationMap.get("driver_name"));
						alarmTalkMap.put("car_kind", allocationMap.get("car_kind").toString());
						alarmTalkMap.put("car_id_num", allocationMap.get("car_id_num").toString());
						alarmTalkMap.put("phone_num", userSessionMap.get("phone_num").toString());
						alarmTalkMap.put("customer_phone", allocationMap.get("charge_phone").toString());
						alarmTalkMap.put("alarm_talk_yn", customer.get("alarm_talk_yn").toString());
						alarmTalkMap.put("send_account_info_yn", customer.get("send_account_info_yn").toString());
						alarmTalkMap.put("departure_dt", allocationMap.get("departure_dt").toString());
						alarmTalkMap.put("customer_id", customer.get("customer_id").toString());
						alarmTalkMap.put("allocation_id", allocationList.get(j).get("allocation_id").toString());
						alarmTalkMap.put("arrival_phone", allocationMap.get("arrival_phone") != null && !allocationMap.get("arrival_phone").toString().equals("") ? allocationMap.get("arrival_phone").toString():"");
						
						
						//티케이물류 문자 발송시 문자 내용에 고객명 추가.
						String personInChargeStr = allocationMap.get("arrival_person_in_charge") != null && !allocationMap.get("arrival_person_in_charge").toString().equals("") ? allocationMap.get("arrival_person_in_charge").toString() : "";
						personInChargeStr = personInChargeStr.replaceAll(" ", "");
						personInChargeStr = personInChargeStr.trim();
						
						String[] personInCharge = personInChargeStr.split(";");  
						alarmTalkMap.put("person_in_charge",personInCharge[0]);
						
						Map<String, Object> payment = new HashMap<String, Object>();
						payment.put("allocationId", allocationList.get(j).get("allocation_id").toString());
						List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(payment);
						String paymentKind = "";
						String billingDivision = "";
						if(paymentInfoList != null && paymentInfoList.size() > 0){
							for(int k = 0; k < paymentInfoList.size(); k++) {
								Map<String, Object> paymentInfo = paymentInfoList.get(k);
								if(paymentInfo.get("payment_division").toString().equals("01")) {		//매출처 
									if(paymentInfo.get("payment_kind") != null) {
										
										
										String debtorCreditorIdForD = "DCI"+UUID.randomUUID().toString().replaceAll("-","");
										
										
										/*********************************************************************
										현금건인 경우에만 현재 분기를 탈 수 있도록 수정할것***************************************
										**********************************************************************/
										
										//매출로 넘어 가지 않았고, 차변에 입력 되지 않은 경우에만.....
										//완료 되었고 현장수금 또는 현금 건인 경우 해당 배차를 확정 하고 차변에 입력 한다.
										if(allocationList.get(j).get("billing_status").toString().equals("N") && (allocationList.get(j).get("debtor_creditor_id") == null || allocationList.get(j).get("debtor_creditor_id").toString().equals(""))) {
											if(allocationStatus.equals("F") && (paymentInfo.get("payment_kind").toString().equals("DD") || paymentInfo.get("payment_kind").toString().equals("CA")) ) {
												
												
												//현금건인 경우는 확정으로만 넘기고 현금 입금시 매출로 넘기도록 수정
												if(paymentInfo.get("payment_kind").toString().equals("CA")) {
												
													Map<String, Object> carInfoUpdateMap = new HashMap<String, Object>();
													carInfoUpdateMap.put("allocationId", allocationList.get(j).get("allocation_id").toString());
													carInfoUpdateMap.put("billingStatus", "N");
													carInfoUpdateMap.put("decideStatus", "Y");
													adjustmentService.updateCarInfoForBillPublish(carInfoUpdateMap);
													
												}else {
													//현장 수금건 탁송 완료시 해당 건에 대해 확정 및 매출로 넘기고 ....
													String billPublishRequestId = "BPR"+UUID.randomUUID().toString().replaceAll("-","");
													Map<String, Object> carInfoUpdateMap = new HashMap<String, Object>();
													carInfoUpdateMap.put("allocationId", allocationList.get(j).get("allocation_id").toString());
													carInfoUpdateMap.put("billPublishRequestId", billPublishRequestId);
													carInfoUpdateMap.put("decideStatus", "Y");
													carInfoUpdateMap.put("billingStatus", "D");
													carInfoUpdateMap.put("debtorCreditorId", debtorCreditorIdForD);
													adjustmentService.updateCarInfoForBillPublish(carInfoUpdateMap);
													//차변에 입력--------------------------------------------------------------------------
													//세금계산서 미 발행건에 대해서 각 건별로 차변에 등록한다.
													DebtorCreditorVO debtorCreditorVO = new DebtorCreditorVO();	
													debtorCreditorVO.setCustomerId(allocationList.get(j).get("customer_id").toString());
													debtorCreditorVO.setDebtorCreditorId(debtorCreditorIdForD);
													debtorCreditorVO.setOccurrenceDt(allocationList.get(j).get("departure_dt").toString());
													debtorCreditorVO.setSummary("운송료 ("+allocationList.get(j).get("departure").toString()+"-"+allocationList.get(j).get("arrival").toString()+")");
													debtorCreditorVO.setTotalVal(allocationList.get(j).get("sales_total").toString());
													debtorCreditorVO.setPublishYn("N");			//미발행건
													debtorCreditorVO.setType(BaseAppConstants.DEBTOR_CREDITOR_TYPE_D);
													debtorCreditorVO.setRegisterId("driver");
													debtorCreditorService.insertDebtorCreditor(debtorCreditorVO);
												}
																								
											}
										}
										if(paymentInfo.get("payment_kind").toString().equals("DD")) {		//결제 방법이 기사 수금이면
											paymentKind = "DD";
											
											//현장수금건인 경우 탁송 완료시 입금일과 결제 여부를 업데이트 하고 대변에 입력 한다.
											if(allocationStatus.equals("F")) {
												
												Map<String, Object> paymentUpdateMap = new HashMap<String, Object>();
												
												paymentUpdateMap.put("payment", "Y");
												paymentUpdateMap.put("paymentDt", allocationList.get(j).get("departure_dt").toString());
												paymentUpdateMap.put("allocationId", allocationList.get(j).get("allocation_id").toString());
												paymentUpdateMap.put("idx", paymentInfo.get("idx").toString());
												//결제 여부 업데이트
												paymentInfoService.updatePaymentInfoForDDFinish(paymentUpdateMap);
												
												//대변 입력(대변에 입력 되어 있우에는 다시 입력 하지 않는다...)
												if(paymentInfo.get("debtor_creditor_id") == null || paymentInfo.get("debtor_creditor_id").toString().equals("")) {
											
													String debtorCreditorId = "DCI"+UUID.randomUUID().toString().replaceAll("-","");
													DebtorCreditorVO debtorCreditorVO = new DebtorCreditorVO();
													debtorCreditorVO.setCustomerId(allocationList.get(j).get("customer_id").toString());
													debtorCreditorVO.setDebtorCreditorId(debtorCreditorId);
													debtorCreditorVO.setOccurrenceDt(allocationList.get(j).get("departure_dt").toString());
													paymentUpdateMap.put("debtorCreditorId", debtorCreditorId);
																					
													String summaryDt = ""; 
													summaryDt ="운송료 ("+allocationList.get(j).get("departure").toString()+" - "+allocationList.get(j).get("arrival").toString()+" 입금)"; 
													debtorCreditorVO.setSummary(summaryDt);
													debtorCreditorVO.setTotalVal(allocationList.get(j).get("sales_total").toString());
													debtorCreditorVO.setPublishYn("N");
													debtorCreditorVO.setType(BaseAppConstants.DEBTOR_CREDITOR_TYPE_C);
													debtorCreditorVO.setRegisterId("driver");
													debtorCreditorVO.setDebtorId(debtorCreditorIdForD);
													//대변에 입력 하고 
													debtorCreditorService.insertDebtorCreditor(debtorCreditorVO);	
													paymentInfoService.updatePaymentInfoDebtorCreditorId(paymentUpdateMap);
												}
											
											}
											
										}else {
											paymentKind = paymentInfo.get("payment_kind").toString();
										}
										if(paymentInfo.get("billing_division") != null) {
											billingDivision = paymentInfo.get("billing_division").toString();
										}
									}										
								}
							}
						}
						alarmTalkMap.put("payment_kind",paymentKind);
						alarmTalkMap.put("customer_kind",customer.get("customer_kind").toString());
						alarmTalkMap.put("billing_division",billingDivision);
						
						if(allocationStatus.equals("S")) {		//상차 완료 된 경우
							alarmTalkMap.put("gubun", "2");			//알림톡 발송 구분		1:기사배정,2:상차완료,3:하차완료,4:인계완료,5:탁송완료,6탁송취소,7:배차정보(기사님에게),8:배차승인(배차담당직원)
							if(customer.get("alarm_talk_yn").toString().equals("Y")) {
								alarmTalkService.alarmTalkSend(alarmTalkMap);	
							}
							
							if(customer.get("sms_yn").toString().equals("Y")) {
								alarmTalkService.smsSend(alarmTalkMap);
							}
							
						}
						
						if(allocationStatus.equals("F")) {
							
							alarmTalkMap.put("gubun", "5");			//알림톡 발송 구분		1:기사배정,2:상차완료,3:하차완료,4:인계완료,5:탁송완료,6탁송취소,7:배차정보(기사님에게),8:배차승인(배차담당직원)
							if(customer.get("alarm_talk_yn").toString().equals("Y")) {
								alarmTalkService.alarmTalkSend(alarmTalkMap);	
							}
							
							if(customer.get("sms_yn").toString().equals("Y")) {
								alarmTalkService.smsSend(alarmTalkMap);
							}
							
							BufferedReader    oBufReader = null;
					        HttpURLConnection httpConn   = null;
							 
							String strEncodeUrl = "http://52.78.153.148:8080/allocation/sendSocketMessage.do?allocationId="+allocationList.get(j).get("allocation_id").toString()+"&driverId="+userSessionMap.get("driver_id").toString();
					        URL oOpenURL = new URL(strEncodeUrl);
					        httpConn =  (HttpURLConnection) oOpenURL.openConnection();          
					        httpConn.setRequestMethod("POST");          
					        httpConn.connect(); 
					        Thread.sleep(100);
					        oBufReader = new BufferedReader(new InputStreamReader(oOpenURL.openStream()));

					        //완료된 탁송건에 대해 확정을 진행 한다.
					        map.put("decideStatus", "Y");
					        map.put("paymentDivision", "02");
					        
					        if(allocationMap.get("hc_yn").toString().equals("N")) {
					        	if(allocationMap.get("departure_month") != null && !allocationMap.get("departure_month").toString().equals("")) {
						        	map.put("decideMonth", allocationMap.get("departure_month").toString());
						        }else {
						        	map.put("decideMonth", WebUtils.getNow("yyyy-MM"));	
						        }
					        	
					        }else {
					        	//현대캐피탈건의 확정월은 도착일 기준 전월21일~당월20일이므로....... 도착을 찍을때
					        	
					        	/*
					        	if(allocationMap.get("departure_month") != null && !allocationMap.get("departure_month").toString().equals("")) {
						        	map.put("decideMonth", allocationMap.get("departure_month").toString());
						        }else {
						        	map.put("decideMonth", WebUtils.getNow("yyyy-MM"));	
						        }
					        	String.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(allocationMap.get("departure_dt_str").toString()));    	
					        	*/
					        	
					        	if(Integer.parseInt(String.valueOf(WebUtils.getNow("dd"))) > 20) {
					        		map.put("decideMonth", WebUtils.getNextMonth());
					        	}else {
					        		map.put("decideMonth", WebUtils.getNow("yyyy-MM"));
					        	}
					        	
					        	
					        	
					        }
					        
					        
					        
					        
					        paymentInfoService.updatePaymentInfoDecideStatus(map);
					        
					        
						}
						
					}	
				}
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	@RequestMapping(value = "/update-allocationAllData", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView updateAllocationAllData(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute AllocationVO allocationVO) throws Exception {
		
		try{
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("duplicateChk", "true");
			
			String returnPage = "";
			if(request.getParameter("returnPage") != null && !request.getParameter("returnPage").toString().equals("")){
				returnPage = request.getParameter("returnPage").toString();
			}else{
				returnPage = "combination.do";
			}
			
			map.put("allocationId", allocationVO.getAllocationId());
			carInfoService.deleteCarInfo(map);
			paymentInfoService.deletePaymentInfo(map);
			
			String carInfoVal = request.getParameter("carInfoVal").toString();
			String paymentInfoVal = request.getParameter("paymentInfoVal").toString();
			
			if(!carInfoVal.equals("")){
				JSONParser jsonParser = new JSONParser();
				JSONObject jsonObject = (JSONObject) jsonParser.parse(carInfoVal);
				JSONArray jsonArray = (JSONArray) jsonObject.get("carInfoList");	
				JSONObject carInfoF = (JSONObject)jsonArray.get(0);
				//allocationVO.setCarrierType(carInfoF.get("carrier_type").toString());
				
				for(int i = 0; i < jsonArray.size(); i++){
					JSONObject carInfo = (JSONObject)jsonArray.get(i);
					CarInfoVO carInfoVO = new CarInfoVO();
					carInfoVO.setAllocationId(allocationVO.getAllocationId());
					carInfoVO.setCarrierType(carInfo.get("carrier_type").toString());
					carInfoVO.setDistanceType(carInfo.get("distance_type").toString());
					carInfoVO.setDepartureDt(carInfo.get("departure_dt").toString());
					carInfoVO.setDepartureTime(carInfo.get("departure_time").toString());
					carInfoVO.setDriverId(carInfo.get("driver_id").toString());
					map.put("driverId", carInfo.get("driver_id").toString());
					Map<String, Object> driver = driverService.selectDriver(map);
					if(driver != null){
						carInfoVO.setDriverName(driver.get("driver_name").toString());	
					}
					carInfoVO.setCarKind(carInfo.get("car_kind").toString());
					carInfoVO.setCarIdNum(carInfo.get("car_id_num").toString());
					carInfoVO.setCarNum(carInfo.get("car_num").toString());
					carInfoVO.setContractNum(carInfo.get("contract_num").toString());
					carInfoVO.setTowDistance(carInfo.get("tow_distance").toString());
					carInfoVO.setAccidentYn(carInfo.get("accident_yn").toString());
					carInfoVO.setEtc(carInfo.get("etc").toString());
					carInfoVO.setDeparture(carInfo.get("departure").toString());
					carInfoVO.setDepartureAddr(carInfo.get("departure_addr").toString());
					carInfoVO.setDeparturePersonInCharge(carInfo.get("departure_person_in_charge").toString());
					carInfoVO.setDeparturePhone(carInfo.get("departure_phone").toString());
					carInfoVO.setArrival(carInfo.get("arrival").toString());
					carInfoVO.setArrivalAddr(carInfo.get("arrival_addr").toString());
					carInfoVO.setArrivalPersonInCharge(carInfo.get("arrival_person_in_charge").toString());
					carInfoVO.setArrivalPhone(carInfo.get("arrival_phone").toString());
					carInfoService.insertCarInfo(carInfoVO);
				}
			}
			
			if(!paymentInfoVal.equals("")){
				JSONParser jsonParser = new JSONParser();
				JSONObject jsonObject = (JSONObject) jsonParser.parse(paymentInfoVal);
				JSONArray jsonArray = (JSONArray) jsonObject.get("paymentInfoList");	
				
				for(int i = 0; i < jsonArray.size(); i++){
					JSONObject paymentInfo = (JSONObject)jsonArray.get(i);
					PaymentInfoVO paymentInfoVO = new PaymentInfoVO();
					
					paymentInfoVO.setAllocationId(allocationVO.getAllocationId());
					paymentInfoVO.setAmount(paymentInfo.get("amount").toString());
					paymentInfoVO.setBillForPayment(paymentInfo.get("bill_for_payment").toString());
					paymentInfoVO.setBillingDivision(paymentInfo.get("billing_division").toString());
					paymentInfoVO.setBillingDt(paymentInfo.get("billing_dt").toString());
					paymentInfoVO.setDeductionRate(paymentInfo.get("deduction_rate").toString());
					paymentInfoVO.setEtc(paymentInfo.get("etc").toString());
					paymentInfoVO.setPayment(paymentInfo.get("payment").toString());
					paymentInfoVO.setPaymentDivision(paymentInfo.get("payment_division").toString());
					paymentInfoVO.setPaymentDt(paymentInfo.get("payment_dt").toString());
					paymentInfoVO.setPaymentKind(paymentInfo.get("payment_kind").toString());
					paymentInfoVO.setPaymentPartner(paymentInfo.get("payment_partner").toString());
					paymentInfoVO.setPaymentPartnerId(paymentInfo.get("payment_partner_id").toString());
					
					paymentInfoService.insertPaymentInfo(paymentInfoVO);
					
				}
			}
			
			allocationService.updateAllocation(allocationVO);
			WebUtils.messageAndRedirectUrl(mav, "수정되었습니다.", "/allocation/"+returnPage);

		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	

	@RequestMapping(value = "/allocation-register", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView allocationRegister(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		try{
			
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			String today = "";			
			today = WebUtils.getNow("yyyy-MM-dd (E)");
			paramMap.put("today", today);
			
			Map<String, Object> map = new HashMap<String, Object>();
			List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
			List<Map<String, Object>> driverList = driverService.selectDriverList(map);
			List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
			List<Map<String, Object>> allocationDivisionList = allocationDivisionService.selectAllocationDivisionInfoList();
			List<Map<String, Object>> runDivisionList = runDivisionService.selectRunDivisionInfoList();
			List<Map<String, Object>> paymentDivisionList = paymentDivisionService.selectPaymentDivisionInfoList();
			List<Map<String, Object>> payDivisionList = payDivisionService.selectPayDivisionInfoList();
			
			mav.addObject("billingDivisionList",  billingDivisionList);
			mav.addObject("allocationDivisionList",  allocationDivisionList);
			mav.addObject("runDivisionList",  runDivisionList);
			mav.addObject("paymentDivisionList",  paymentDivisionList);
			mav.addObject("payDivisionList",  payDivisionList);
			mav.addObject("userMap",  userMap);
			mav.addObject("driverList",  driverList);
			mav.addObject("companyList",  companyList);
			
			mav.addObject("paramMap", paramMap);
			mav.addObject("parameters", ParamUtils.makeParameterQuery(request,  new String[]{"searchType", "searchWord"}));
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	
	@RequestMapping(value = "/allocation-view", method = RequestMethod.GET)
	public ModelAndView view(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute AllocationVO allocationVO) throws Exception{
		
		
		try{
			
			HttpSession session = request.getSession(); 
			//Map userMap = (Map)session.getAttribute("user");
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			Map<String, Object> map = new HashMap<String, Object>();
			List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
			List<Map<String, Object>> driverList = driverService.selectDriverList(map);
			List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
			List<Map<String, Object>> allocationDivisionList = allocationDivisionService.selectAllocationDivisionInfoList();
			List<Map<String, Object>> runDivisionList = runDivisionService.selectRunDivisionInfoList();
			List<Map<String, Object>> paymentDivisionList = paymentDivisionService.selectPaymentDivisionInfoList();
			List<Map<String, Object>> payDivisionList = payDivisionService.selectPayDivisionInfoList();
		
			map.put("allocationId", request.getParameter("allocationId").toString());
			Map<String, Object> allocationMap = allocationService.selectAllocation(map);
			map.put("batchStatusId", allocationMap.get("batch_status_id"));
			List<Map<String, Object>> allocationList = allocationService.selectAllocationList(map);
			
			for(int i = 0; i < allocationList.size(); i++) {
				Map<String, Object> allocation = allocationList.get(i); 
				map.put("allocationId", allocation.get("allocation_id").toString());
				Map<String, Object> carInfo = carInfoService.selectCarInfo(map);
				allocation.put("carInfo", carInfo);
				List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(map);
				allocation.put("paymentInfoList", paymentInfoList);
				allocationList.set(i,allocation);
			}
				mav.addObject("allocationMap",  allocationMap);
				mav.addObject("allocationList",  allocationList);
				mav.addObject("billingDivisionList",  billingDivisionList);
				mav.addObject("allocationDivisionList",  allocationDivisionList);
				mav.addObject("runDivisionList",  runDivisionList);
				mav.addObject("paymentDivisionList",  paymentDivisionList);
				mav.addObject("payDivisionList",  payDivisionList);
				mav.addObject("driverList",  driverList);
				mav.addObject("companyList",  companyList);
				mav.addObject("paramMap", paramMap);
				
				
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return mav;
	}
	
	
	
	@RequestMapping(value = "/allocation-mod", method = RequestMethod.GET)
	public ModelAndView mod(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute AllocationVO allocationVO) throws Exception{
		
		
		try{
			
			HttpSession session = request.getSession(); 
			//Map userMap = (Map)session.getAttribute("user");
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			
			Map<String, Object> map = new HashMap<String, Object>();
			List<Map<String, Object>> companyList = companyService.selectCompanyList(map);
			List<Map<String, Object>> driverList = driverService.selectDriverList(map);
			List<Map<String, Object>> billingDivisionList = billingDivisionService.selectBillingDivisionInfoList();
			List<Map<String, Object>> allocationDivisionList = allocationDivisionService.selectAllocationDivisionInfoList();
			List<Map<String, Object>> runDivisionList = runDivisionService.selectRunDivisionInfoList();
			List<Map<String, Object>> paymentDivisionList = paymentDivisionService.selectPaymentDivisionInfoList();
			List<Map<String, Object>> payDivisionList = payDivisionService.selectPayDivisionInfoList();
		
			map.put("allocationId", request.getParameter("allocationId").toString());
			Map<String, Object> allocationMap = allocationService.selectAllocation(map);
			map.put("batchStatusId", allocationMap.get("batch_status_id"));
			List<Map<String, Object>> allocationList = allocationService.selectAllocationList(map);
			
			for(int i = 0; i < allocationList.size(); i++) {
				Map<String, Object> allocation = allocationList.get(i); 
				map.put("allocationId", allocation.get("allocation_id").toString());
				Map<String, Object> carInfo = carInfoService.selectCarInfo(map);
				allocation.put("carInfo", carInfo);
				List<Map<String, Object>> paymentInfoList = paymentInfoService.selectPaymentInfoList(map);
				allocation.put("paymentInfoList", paymentInfoList);
				allocationList.set(i,allocation);
			}
				mav.addObject("allocationMap",  allocationMap);
				mav.addObject("allocationList",  allocationList);
				mav.addObject("billingDivisionList",  billingDivisionList);
				mav.addObject("allocationDivisionList",  allocationDivisionList);
				mav.addObject("runDivisionList",  runDivisionList);
				mav.addObject("paymentDivisionList",  paymentDivisionList);
				mav.addObject("payDivisionList",  payDivisionList);
				mav.addObject("driverList",  driverList);
				mav.addObject("companyList",  companyList);
				mav.addObject("paramMap", paramMap);
				
				
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return mav;
	}
	
	
	
	
	@RequestMapping(value = "/update-allocation", method = RequestMethod.GET)
	public ModelAndView updateAllocation(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response,@ModelAttribute AllocationVO allocationVO) throws Exception {
		
		try{
			
			Map<String, Object> map = new HashMap<String, Object>();
			
			String carInfoVal = request.getParameter("carInfoVal").toString();
			String paymentInfoVal = request.getParameter("paymentInfoVal").toString();
			String location = request.getParameter("location") == null ? "" : request.getParameter("location").toString(); 
			
			if(!carInfoVal.equals("")){
				JSONParser jsonParser = new JSONParser();
				JSONObject jsonObject = (JSONObject) jsonParser.parse(carInfoVal);
				JSONArray jsonArray = (JSONArray) jsonObject.get("carInfoList");	
				String batchStatusId = "BAT"+UUID.randomUUID().toString().replaceAll("-","");
				for(int i = 0; i < jsonArray.size(); i++){
					
					JSONObject carInfo = (JSONObject)jsonArray.get(i);
					String allocationId = carInfo.get("allocation_id").toString();			//운행정보는 이미 id가 있으므로 바로 업데이트 한다.
					allocationVO.setAllocationId(allocationId);
					CarInfoVO carInfoVO = new CarInfoVO();
					carInfoVO.setAllocationId(allocationId);
					carInfoVO.setCarrierType(carInfo.get("carrier_type").toString());
					carInfoVO.setDistanceType(carInfo.get("distance_type").toString());
					carInfoVO.setDepartureDt(carInfo.get("departure_dt").toString());
					carInfoVO.setDepartureTime(carInfo.get("departure_time").toString());
					
					String driverCancel = carInfo.get("driver_cancel_yn").toString();				//기사 배정이 취소 된 경우 view단에서 데이터를 넘기지 않는다.
					
					carInfoVO.setDriverId(carInfo.get("driver_id").toString());
					map.put("driverId", carInfo.get("driver_id").toString());
					Map<String, Object> driver = driverService.selectDriver(map);
					if(driver != null){
						carInfoVO.setDriverName(driver.get("driver_name").toString());	
					}
					if(driver != null || !carInfo.get("driver_name").toString().equals("")) {		//배차 등록시 기사가 배정 되어 있으면 미배차가 아니고 배차된 상태이다.
						allocationVO.setAllocationStatus("Y");
					}else {
						allocationVO.setAllocationStatus("N");
					}
					
					if(allocationVO.getBatchStatus().equals("N")) {//개별								//일괄 등록과 개별 등록 구분
						allocationVO.setBatchStatusId("BAT"+UUID.randomUUID().toString().replaceAll("-",""));
						allocationVO.setBatchIndex("0");
					}else if(allocationVO.getBatchStatus().equals("Y")) {//일괄
						allocationVO.setBatchStatusId(batchStatusId);
						allocationVO.setBatchIndex(String.valueOf(i));
					}
					allocationService.updateAllocation(allocationVO);
					carInfoVO.setCarKind(carInfo.get("car_kind").toString());
					carInfoVO.setCarIdNum(carInfo.get("car_id_num").toString());
					carInfoVO.setCarNum(carInfo.get("car_num").toString());
					carInfoVO.setContractNum(carInfo.get("contract_num").toString());
					carInfoVO.setTowDistance(carInfo.get("tow_distance").toString());
					carInfoVO.setAccidentYn(carInfo.get("accident_yn").toString());
					carInfoVO.setEtc(carInfo.get("etc").toString());
					carInfoVO.setDeparture(carInfo.get("departure").toString());
					carInfoVO.setDepartureAddr(carInfo.get("departure_addr").toString());
					carInfoVO.setDeparturePersonInCharge(carInfo.get("departure_person_in_charge").toString());
					carInfoVO.setDeparturePhone(carInfo.get("departure_phone").toString());
					carInfoVO.setArrival(carInfo.get("arrival").toString());
					carInfoVO.setArrivalAddr(carInfo.get("arrival_addr").toString());
					carInfoVO.setArrivalPersonInCharge(carInfo.get("arrival_person_in_charge").toString());
					carInfoVO.setArrivalPhone(carInfo.get("arrival_phone").toString());
					carInfoService.updateCarInfo(carInfoVO);
					
					String carInfoIndex = carInfo.get("index").toString();
					if(!paymentInfoVal.equals("")){
						JSONParser jsonPaymentParser = new JSONParser();
						JSONObject jsonPaymentObject = (JSONObject) jsonPaymentParser.parse(paymentInfoVal);
						JSONArray jsonPaymentArray = (JSONArray) jsonPaymentObject.get("paymentInfoList");	
						map.put("allocationId", allocationId);
						paymentInfoService.deletePaymentInfo(map);				//결제정보의 수 및 결제처의 정보가 변경 될 수 있으므로 기존의 결제정보를 삭제 하고 다시 등록 한다. 
						for(int j = 0; j < jsonPaymentArray.size(); j++){
							JSONObject paymentInfo = (JSONObject)jsonPaymentArray.get(j);
							if(paymentInfo.get("index").toString().equals(carInfoIndex)) {
								PaymentInfoVO paymentInfoVO = new PaymentInfoVO();
								paymentInfoVO.setAllocationId(allocationId);
								paymentInfoVO.setAmount(paymentInfo.get("amount").toString());
								paymentInfoVO.setBillingDivision(paymentInfo.get("billing_division").toString());
								paymentInfoVO.setBillingDt(paymentInfo.get("billing_dt").toString());
								paymentInfoVO.setDeductionRate(paymentInfo.get("deduction_rate").toString());
								paymentInfoVO.setEtc(paymentInfo.get("etc").toString());
								paymentInfoVO.setPayment(paymentInfo.get("payment").toString());
								paymentInfoVO.setPaymentDivision(paymentInfo.get("payment_division").toString());
								paymentInfoVO.setPaymentDt(paymentInfo.get("payment_dt").toString());
								paymentInfoVO.setPaymentKind(paymentInfo.get("payment_kind").toString());
								paymentInfoVO.setPaymentPartner(paymentInfo.get("payment_partner").toString());
								paymentInfoVO.setPaymentPartnerId(paymentInfo.get("payment_partner_id").toString());
								if(paymentInfo.get("payment_division").toString().equals("02") && driverCancel.equals("Y")){		//기사 배정이 취소 된경우
									paymentInfoVO.setPaymentPartner("");
									paymentInfoVO.setPaymentPartnerId("");	
									paymentInfoVO.setDeductionRate("");
									paymentInfoVO.setAmount("");
									paymentInfoVO.setPaymentDt("");
									paymentInfoVO.setBillingDt("");
									paymentInfoVO.setBillForPayment("");
									paymentInfoVO.setPayment("");
									paymentInfoVO.setBillingDivision("");
									paymentInfoVO.setPaymentKind("");
								}
								paymentInfoService.insertPaymentInfo(paymentInfoVO);
							}
						}
					}
					
				}
			}
			
			if(location.equals("")) {
				WebUtils.messageAndRedirectUrl(mav, "수정되었습니다.", "/allocation/combination.do");	
			}else {
				if(location.equals("combination")) {
					WebUtils.messageAndRedirectUrl(mav, "수정되었습니다.", "/allocation/combination.do");
				}else if(location.equals("self")) {
					WebUtils.messageAndRedirectUrl(mav, "수정되었습니다.", "/allocation/self.do");
				}else if(location.equals("carrier")) {
					WebUtils.messageAndRedirectUrl(mav, "수정되었습니다.", "/allocation/carrier.do");
				}
			}
			
			

		}catch(Exception e){
			e.printStackTrace();
		}
		

		return mav;
	}
	
	
	
	
	
	@RequestMapping(value = "/setDriver", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi setDriver(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			Map<String, Object> map = new HashMap<String, Object>();
			
			String allocationId = request.getParameter("allocationId").toString();
			String[] allocationIdArr = allocationId.split(",");
			
			for(int i = 0; i < allocationIdArr.length; i++){
				String driverId = request.getParameter("driverId").toString();	
				map.put("allocationId", allocationIdArr[i]);
				Map<String, Object> allocationMap = allocationService.selectAllocation(map);
				map.put("batchStatusId", allocationMap.get("batch_status_id"));
				List<Map<String, Object>> allocationList = allocationService.selectAllocationList(map);	
				if(allocationList.size() > 0) {
					map.put("driverId", driverId);
					Map<String, Object> driverMap = driverService.selectDriver(map);
					if(driverMap != null) {
						for(int j = 0; j < allocationList.size(); j++) {
							map.put("allocationId", allocationList.get(j).get("allocation_id").toString());
							map.put("driverId", driverId);
							map.put("driverName", driverMap.get("driver_name"));
							carInfoService.updateCarInfoDriver(map);
							map.put("allocationStatus", BaseAppConstants.ALLOCATION_STATUS_Y);
							allocationService.updateAllocationStatus(map);
							map.put("allocationStatus", "");
						}	
					}else {
						result.setResultCode("0003");
						result.setResultMsg("기사 정보를 찾을 수 없습니다.");
					}	
				}else {
					result.setResultCode("0002");
					result.setResultMsg("운행 정보를 찾을 수 없습니다.");
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	
	@RequestMapping(value = "/updateAllocationForSendReceipt", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updateAllocationForSendReceipt(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
	
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			String allocationId = request.getParameter("allocationId").toString();
			String[] allocationIdArr = allocationId.split(",");
			String groupId = allocationIdArr[0];
			
			String mailAddress = request.getParameter("mailAddress").toString();
			String phoneNumber = request.getParameter("phoneNumber").toString();

			for(int i = 0; i < allocationIdArr.length; i++) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("allocationId", allocationIdArr[i]);
				map.put("mailAddress", mailAddress);
				map.put("phoneNumber", phoneNumber);			
				allocationService.updateAllocationForSendReceipt(map);	
			}
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	
	@RequestMapping(value = "/updateAllocationReceiptSkip", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updateAllocationReceiptSkip(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
	
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			String allocationId = request.getParameter("allocationId").toString();
			String[] allocationIdArr = allocationId.split(",");
			String groupId = allocationIdArr[0];
			
			String receiptSkipReason = request.getParameter("receiptSkipReason").toString();
			
			for(int i = 0; i < allocationIdArr.length; i++) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("allocationId", allocationIdArr[i]);
				map.put("receiptSkipReason", receiptSkipReason);
				map.put("receiptSkipYn", "Y");			
				allocationService.updateAllocationReceiptSkip(map);
			}
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}

	
	
	@RequestMapping(value = "/updateCarKind", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updateCarKind(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
	
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			String allocationId = request.getParameter("allocationId").toString();
			String carKind = request.getParameter("carKind").toString();
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("allocationId", allocationId);
			map.put("driverId", userSessionMap.get("driver_id").toString());
			
			Map<String, Object> allocationMap = allocationService.selectAllocation(map);			
			//픽업건이 아닌경우는 해당건만 업데이트 한다.
			if(!allocationMap.get("batch_status").toString().equals("P")) {
				map.put("carKind", carKind);
				carInfoService.updateCarKind(map);	
			}else {
				
				//픽업건인 경우 
				map.put("batchStatusId", allocationMap.get("batch_status_id"));
				List<Map<String, Object>> allocationList = allocationService.selectAllocationListByBatchStatusId(map);
				
				if(allocationList.size() > 0) {
					for(int i = 0; i < allocationList.size(); i++) {
						Map<String, Object> updateMap = new HashMap<String, Object>();
						updateMap.put("carKind", carKind);
						updateMap.put("allocationId", allocationList.get(i).get("allocation_id").toString());
						carInfoService.updateCarKind(updateMap);
					}
					
				}
					
			}
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	@RequestMapping(value = "/updateCarIdNum", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updateCarIdNum(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
	
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			String allocationId = request.getParameter("allocationId").toString();
			String carIdNum = request.getParameter("carIdNum").toString();
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("allocationId", allocationId);
			map.put("driverId", userSessionMap.get("driver_id").toString());
			
			Map<String, Object> allocationMap = allocationService.selectAllocation(map);			
			//픽업건이 아닌경우는 해당건만 업데이트 한다.
			if(!allocationMap.get("batch_status").toString().equals("P")) {
				map.put("carIdNum", carIdNum);
				map.put("driverModYn", "Y");
				map.put("modDriver", userSessionMap.get("driver_id").toString());
				carInfoService.updateCarIdNum(map);
				
			}else {
				
				//픽업건인 경우 
				map.put("batchStatusId", allocationMap.get("batch_status_id"));
				List<Map<String, Object>> allocationList = allocationService.selectAllocationListByBatchStatusId(map);
				
				if(allocationList.size() > 0) {
					for(int i = 0; i < allocationList.size(); i++) {
						Map<String, Object> updateMap = new HashMap<String, Object>();
						updateMap.put("carIdNum", carIdNum);
						updateMap.put("driverModYn", "Y");
						updateMap.put("modDriver", userSessionMap.get("driver_id").toString());
						updateMap.put("allocationId", allocationList.get(i).get("allocation_id").toString());
						carInfoService.updateCarIdNum(updateMap);
					}
					
				}
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	@RequestMapping(value = "/updateCarNum", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updateCarNum(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
	
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			String allocationId = request.getParameter("allocationId").toString();
			String carNum = request.getParameter("carNum").toString();
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("allocationId", allocationId);
			map.put("driverId", userSessionMap.get("driver_id").toString());
			
			Map<String, Object> allocationMap = allocationService.selectAllocation(map);			
			//픽업건이 아닌경우는 해당건만 업데이트 한다.
			if(!allocationMap.get("batch_status").toString().equals("P")) {
				map.put("carNum", carNum);
				carInfoService.updateCarNum(map);
			}else {
				
				//픽업건인 경우 
				map.put("batchStatusId", allocationMap.get("batch_status_id"));
				List<Map<String, Object>> allocationList = allocationService.selectAllocationListByBatchStatusId(map);
				if(allocationList.size() > 0) {
					for(int i = 0; i < allocationList.size(); i++) {
						Map<String, Object> updateMap = new HashMap<String, Object>();
						updateMap.put("carNum", carNum);
						updateMap.put("allocationId", allocationList.get(i).get("allocation_id").toString());
						carInfoService.updateCarNum(updateMap);
					}
				}
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	@RequestMapping(value = "/updateCarInfoDriverEtc", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updateCarInfoDriverEtc(ModelAndView mav,
			Model model,HttpServletRequest request ,HttpServletResponse response
			) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
	
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			String allocationId = request.getParameter("allocationId").toString();
			String driverEtc = request.getParameter("driverEtc").toString();
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("allocationId", allocationId);
			map.put("driverEtc", driverEtc);
			carInfoService.updateCarInfoDriverEtc(map);
			
		}catch(Exception e){
			e.printStackTrace();
			result.setResultCode("0001");
		}
		
		return result;
	}
	
	
	
	
	
}
