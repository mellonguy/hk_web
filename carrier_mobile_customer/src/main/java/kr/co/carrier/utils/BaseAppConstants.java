package kr.co.carrier.utils;

public class BaseAppConstants {

	public static final int NUM_OF_ROWS = 15;
	public static final int NUM_OF_ROWS_CAL_LIST = 20;
	public static final int NUM_OF_ROWS_FOR_PLAYER_LIST = 15;
	public static final int NUM_OF_ROWS_FOR_END_LIST = 50;
	public static final int NUM_OF_PAGES =10;
	public static final String CHARACTER_SET = "UTF-8";
	public static final String ADMIN_SESSION_KEY = "admin";
	public static final String USER_SESSION_KEY = "user";
	public static final String USER_TEMP_SESSION_KEY = "temp_user";
	public static final String GAME_CODE = "01";
	public static final String GAME_PARTICIPATION_DATE = "gameParticipationDate";
	public static final String GAME_PARTICIPATION_DATE_TIME = "gameParticipationDateTime";
	public static final String TIME_REMAINING = "time_remaining";
	public static final String REAL_USER_MAP = "realUserMap"; 
	public static final String DOUBLE_DOWN_LIST = "doubleDownList";  
	public static final String SYSTEM_NM = "system";  
	public static final String ALLOCATION_STATUS_Y = "Y";
	public static final String ALLOCATION_STATUS_N = "N";
	
	public static final String DEBTOR_CREDITOR_TYPE_D = "D";	//차변
	public static final String DEBTOR_CREDITOR_TYPE_C = "C";	//대변
	
	public static final String USER_STATUS_WITHDRAW = "04";
	public static final String USER_STATUS_RESTORE = "01";
	public static final String BBS_TYPE_NOTICE = "notice";
	public static final String BBS_TYPE_FREEBOARD = "freeBoard";
	public static final String BBS_TYPE_TOTOCERT = "totoCert";
	public static final String BBS_TYPE_DISCUSS = "discuss";
	public static final String BBS_TYPE_STRATEGY = "strategy";
	public static final String BBS_TYPE_ARCHIVE = "archive";
	public static final String BBS_TYPE_FAQ = "faq";
	
	public static final String COMPANY_TYPE_HKCARCARRIER = "HKCARCARRIER";  // 회사타입 : 한국카캐리어 
	public static final String COMPANY_TYPE_NATIONAL = "NATIONAL"; //회사타입: 네셔털컨퀘스트
	
	public static final String EMPLOYEE_STATUS_WORK = "01";
	public static final String EMPLOYEE_STATUS_LEAVEOFABSENCE = "02";
	public static final String EMPLOYEE_STATUS_RESIGN = "03";
	
	public static final int POSSIBLE_USE_PICKBALL = 200000;
	
	public static final String TMAP_API_APPKEY = "l7xx6b701ff5595c4e3b859e42f57de0c975";
	
	
	
}
