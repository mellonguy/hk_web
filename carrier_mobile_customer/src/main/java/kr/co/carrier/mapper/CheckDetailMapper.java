package kr.co.carrier.mapper;

import java.util.Map;

import kr.co.carrier.vo.CarCheckDetailVO;

public interface CheckDetailMapper {

	public Map<String, Object> selectCheckDetail(Map<String, Object> map) throws Exception; 
	public int insertCheckDetail(CarCheckDetailVO carCheckDetailVO) throws Exception;
	public void updateCheckDetail(CarCheckDetailVO carCheckDetailVO) throws Exception;
	
	
	
}
