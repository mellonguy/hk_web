package kr.co.carrier.mapper;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.CarInfoVO;

public interface CarInfoMapper {

	public Map<String, Object> selectCarInfo(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectCarInfoList(Map<String, Object> map) throws Exception;
	public int selectCarInfoListCount(Map<String, Object> map) throws Exception;
	public int insertCarInfo(CarInfoVO carInfoVO) throws Exception;
	public void updateCarInfo(CarInfoVO carInfoVO) throws Exception;
	public void updateReservationCarInfo(CarInfoVO carInfoVO) throws Exception;
	public void deleteCarInfo(Map<String, Object> map) throws Exception;
	public void updateCarInfoDriver(Map<String, Object> map) throws Exception;
	public void updateCarKind(Map<String, Object> map) throws Exception;
	public void updateCarIdNum(Map<String, Object> map) throws Exception;
	public void updateCarNum(Map<String, Object> map) throws Exception;
	public void updateCarInfoDriverEtc(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectUseMileage(Map<String, Object> map) throws Exception; 
	
	
	
	
}
