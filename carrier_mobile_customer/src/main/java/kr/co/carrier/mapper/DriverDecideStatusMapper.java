package kr.co.carrier.mapper;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.DriverDecideStatusVO;

public interface DriverDecideStatusMapper {

	
	
	public Map<String, Object> selectDriverDecideStatus(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectDriverDecideStatusList(Map<String, Object> map) throws Exception;
	public int selectDriverDecideStatusListCount(Map<String, Object> map) throws Exception;
	public int insertDriverDecideStatus(DriverDecideStatusVO driverDecideStatusVO) throws Exception;
	public void deleteDriverDecideStatus(Map<String, Object> map) throws Exception;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
