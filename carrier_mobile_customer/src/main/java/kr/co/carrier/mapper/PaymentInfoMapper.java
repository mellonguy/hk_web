package kr.co.carrier.mapper;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.PaymentInfoVO;

public interface PaymentInfoMapper {

	
	public List<Map<String, Object>> selectPaymentInfoList(Map<String, Object> map) throws Exception;
	public int insertPaymentInfo(PaymentInfoVO paymentInfoVO) throws Exception;
	public void deletePaymentInfo(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectAnotherCalInfo(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectAnotherCalDDInfo(Map<String, Object> map) throws Exception;
	public void updatePaymentInfoDecideStatus(Map<String, Object> map) throws Exception;
	public void updatePaymentInfoForDDFinish(Map<String, Object> map) throws Exception;
	public void updatePaymentInfoDebtorCreditorId(Map<String, Object> map) throws Exception;
	
	
	
	
	
	
	
}
