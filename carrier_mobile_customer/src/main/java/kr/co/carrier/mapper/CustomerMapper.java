package kr.co.carrier.mapper;

import java.util.List;
import java.util.Map;

import kr.co.carrier.vo.CustomerAppVO;
import kr.co.carrier.vo.CustomerVO;

public interface CustomerMapper {

	
	public Map<String, Object> selectCustomer(Map<String, Object> map) throws Exception; 
	public List<Map<String, Object>> selectCustomerList(Map<String, Object> map) throws Exception;
	public int selectCustomerListCount(Map<String, Object> map) throws Exception;
	public int insertSimpleSignUp(CustomerAppVO customerAppVO) throws Exception;
	public int insertCustomer(CustomerVO customerVO) throws Exception;
	public void updateCustomer(CustomerVO customerVO) throws Exception;
	public void updateVaildYnForCertNum(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectChargeList(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectChargePhoneAndBusinessLicenseNumber(Map<String, Object> map) throws Exception;
	public Map<String,Object>selectChargePhoneNum(Map<String,Object>map)throws Exception;
	public Map<String,Object>selectCaUseCustomerAppByCustomer(Map<String,Object>map)throws Exception;
	public Map<String,Object>selectidDoubleCheck(Map<String,Object>map)throws Exception;
	public Map<String,Object>selectCustomerAllocationStatusList(Map<String,Object>map)throws Exception;
	public Map<String, Object> selectCustomerDetailInfo(Map<String,Object>map)throws Exception;
	public List<Map<String, Object>> selectAllocationStatus(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectCustomerCheckForCanNotBeDoubleCert(Map<String,Object>map)throws Exception;
	public List<Map<String, Object>> selectSearchForAllocation(Map<String,Object>map)throws Exception;
	public List<Map<String, Object>> selectCarIdNumList(Map<String,Object>map)throws Exception;
	public Map<String, Object> selectCarIdNumForMain(Map<String,Object>map)throws Exception;
	public Map<String, Object> selectAllocationCntMonth(Map<String,Object>map)throws Exception;
	public List<Map<String, Object>> selectReciptBoxList(Map<String,Object>map)throws Exception;
	public Map<String, Object>selectShowDistenceToDriverLocation(Map<String,Object>map)throws Exception;
	public Map<String, Object>selectChangeId(Map<String,Object>map)throws Exception;
	public Map<String, Object>selectChagePasswordById(Map<String,Object>map)throws Exception;
	public void updateCreatePassword(Map<String, Object> map) throws Exception;
	public Map<String, Object>selectCustomerIdAndCustomerAPP(Map<String,Object>map)throws Exception;
	public Map<String, Object> selectCustomerByCustomerName(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectPhoneDuplicationChk(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectMileageList	(Map<String,Object>map)throws Exception;
	public Map<String, Object> selectMileageCount(Map<String, Object> map) throws Exception;
	public int insertMileageCustomer(Map<String, Object> map) throws Exception;
	public List<Map<String, Object>> selectAllocationGraph(Map<String,Object>map)throws Exception;
	public int insertReview(Map<String,Object>map)throws Exception;
	public List<Map<String, Object>> selectReviewList(Map<String,Object>map)throws Exception;
	
}
