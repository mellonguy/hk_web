package kr.co.carrier.interceptor;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import kr.co.carrier.utils.WebUtils;

public class UserLoginCheckInterceptor extends HandlerInterceptorAdapter {

	private static final Logger logger = LoggerFactory.getLogger(UserLoginCheckInterceptor.class);
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	 public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object hadler) throws Exception {

		boolean isMobile = WebUtils.isPhone(request.getHeader("User-Agent"));			
		
		Map<String, Object> userSessionMap = (Map<String, Object>)request.getSession().getAttribute("user");

		Map<String, Object> userMap = null;
		
		try{
			
			if(userSessionMap == null){
				
				
				String status = request.getParameter("status") == null ? "": request.getParameter("status").toString();
				
				if(status.equals("noLogin")) {
					
				}else {
					response.sendRedirect(request.getContextPath()+"/nopermission.do");	
					return false;
				}
				
				/*if("POST".equalsIgnoreCase(request.getMethod())){
			    	if(isAjaxRequest(request)){
			    		response.sendError(499);
			    	}else{
			    		response.sendRedirect(request.getContextPath() +"/index.do");
			    		response.sendRedirect(request.getContextPath()+"/nopermission.do");
			    	}
			    	 return false;

				}else{
					String url = SslUtils.toSslUrl(request, request.getRequestURI().toString());
					if ( request.getQueryString() != null ){
						url = url + "?" + request.getQueryString();
					}
					response.sendRedirect(request.getContextPath()+ "/index.do?returnUrl=" + java.net.URLEncoder.encode(url));
					//response.sendRedirect(request.getContextPath()+ "/index.do");
					
					
				return false;
				}*/
				
			}else{
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
				
//		String test = request.getRequestURL().toString();
	
		return true;
		
	 }
	
	private boolean isAjaxRequest(HttpServletRequest req) {
        return req.getHeader("AJAX") != null && req.getHeader("AJAX").equals(Boolean.TRUE.toString());
	}
	
	
	
	
	
	
	
	
	
	
	
}
