package kr.co.carrier.vo;

public class AllocationFileVO {

	
	
	private String allocationFileId;
	private String allocationId;
	private String allocationFilePath;
	private String allocationFileNm;
	private String categoryType;
	private String allocationStatus;
	private String regDt;
	public String getAllocationFileId() {
		return allocationFileId;
	}
	public void setAllocationFileId(String allocationFileId) {
		this.allocationFileId = allocationFileId;
	}
	public String getAllocationId() {
		return allocationId;
	}
	public void setAllocationId(String allocationId) {
		this.allocationId = allocationId;
	}
	public String getAllocationFilePath() {
		return allocationFilePath;
	}
	public void setAllocationFilePath(String allocationFilePath) {
		this.allocationFilePath = allocationFilePath;
	}
	public String getAllocationFileNm() {
		return allocationFileNm;
	}
	public void setAllocationFileNm(String allocationFileNm) {
		this.allocationFileNm = allocationFileNm;
	}
	public String getCategoryType() {
		return categoryType;
	}
	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}
	public String getAllocationStatus() {
		return allocationStatus;
	}
	public void setAllocationStatus(String allocationStatus) {
		this.allocationStatus = allocationStatus;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	
		
}
