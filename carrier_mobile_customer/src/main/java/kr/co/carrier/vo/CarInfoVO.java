package kr.co.carrier.vo;

public class CarInfoVO {

	
	private String accidentYn;
	private String allocationId;
	private String arrival;
	private String arrivalAddr;
	private String arrivalPersonInCharge;
	private String arrivalPhone;
	private String carIdNum;
	private String carKind;
	private String carNum;
	private String salesTotal;
	private String carrierType;
	private String contractNum;
	private String departure;
	private String departureAddr;
	private String departureDt;
	private String departurePersonInCharge;
	private String departurePhone;
	private String departureTime;
	private String distanceType;
	private String driverId;
	private String driverName;
	private String etc;
	private String customerEtc;
	private String regDt;
	private String towDistance;
	private String requirePicCnt;
	private String requireDnPicCnt;
	private String receiptSendYn;
	private String price;
	private String vat;
	
	
	



	public String getVat() {
		return vat;
	}



	public void setVat(String vat) {
		this.vat = vat;
	}



	public String getPrice() {
		return price;
	}



	public void setPrice(String price) {
		this.price = price;
	}



	public String getSalesTotal() {
		return salesTotal;
	}



	public void setSalesTotal(String salesTotal) {
		this.salesTotal = salesTotal;
	}



	public String getCustomerEtc() {
		return customerEtc;
	}



	public void setCustomerEtc(String customerEtc) {
		this.customerEtc = customerEtc;
	}



	public String getReceiptSendYn() {
		return receiptSendYn;
	}



	public void setReceiptSendYn(String receiptSendYn) {
		this.receiptSendYn = receiptSendYn;
	}



	public String getAccidentYn() {
		return accidentYn;
	}
	
	
	
	public String getRequireDnPicCnt() {
		return requireDnPicCnt;
	}



	public void setRequireDnPicCnt(String requireDnPicCnt) {
		this.requireDnPicCnt = requireDnPicCnt;
	}



	public String getDriverId() {
		return driverId;
	}



	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}



	public void setAccidentYn(String accidentYn) {
		this.accidentYn = accidentYn;
	}
	public String getAllocationId() {
		return allocationId;
	}
	public void setAllocationId(String allocationId) {
		this.allocationId = allocationId;
	}
	public String getArrival() {
		return arrival;
	}
	public void setArrival(String arrival) {
		this.arrival = arrival;
	}
	public String getArrivalAddr() {
		return arrivalAddr;
	}
	public void setArrivalAddr(String arrivalAddr) {
		this.arrivalAddr = arrivalAddr;
	}
	public String getArrivalPersonInCharge() {
		return arrivalPersonInCharge;
	}
	public void setArrivalPersonInCharge(String arrivalPersonInCharge) {
		this.arrivalPersonInCharge = arrivalPersonInCharge;
	}
	public String getArrivalPhone() {
		return arrivalPhone;
	}
	public void setArrivalPhone(String arrivalPhone) {
		this.arrivalPhone = arrivalPhone;
	}
	public String getCarIdNum() {
		return carIdNum;
	}
	public void setCarIdNum(String carIdNum) {
		this.carIdNum = carIdNum;
	}
	public String getCarKind() {
		return carKind;
	}
	public void setCarKind(String carKind) {
		this.carKind = carKind;
	}
	public String getCarNum() {
		return carNum;
	}
	public void setCarNum(String carNum) {
		this.carNum = carNum;
	}
	public String getCarrierType() {
		return carrierType;
	}
	public void setCarrierType(String carrierType) {
		this.carrierType = carrierType;
	}
	public String getContractNum() {
		return contractNum;
	}
	public void setContractNum(String contractNum) {
		this.contractNum = contractNum;
	}
	public String getDeparture() {
		return departure;
	}
	public void setDeparture(String departure) {
		this.departure = departure;
	}
	public String getDepartureAddr() {
		return departureAddr;
	}
	public void setDepartureAddr(String departureAddr) {
		this.departureAddr = departureAddr;
	}
	public String getDepartureDt() {
		return departureDt;
	}
	public void setDepartureDt(String departureDt) {
		this.departureDt = departureDt;
	}
	public String getDeparturePersonInCharge() {
		return departurePersonInCharge;
	}
	public void setDeparturePersonInCharge(String departurePersonInCharge) {
		this.departurePersonInCharge = departurePersonInCharge;
	}
	public String getDeparturePhone() {
		return departurePhone;
	}
	public void setDeparturePhone(String departurePhone) {
		this.departurePhone = departurePhone;
	}
	public String getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}
	public String getDistanceType() {
		return distanceType;
	}
	public void setDistanceType(String distanceType) {
		this.distanceType = distanceType;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getEtc() {
		return etc;
	}
	public void setEtc(String etc) {
		this.etc = etc;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getTowDistance() {
		return towDistance;
	}
	public void setTowDistance(String towDistance) {
		this.towDistance = towDistance;
	}
	public String getRequirePicCnt() {
		return requirePicCnt;
	}
	public void setRequirePicCnt(String requirePicCnt) {
		this.requirePicCnt = requirePicCnt;
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
