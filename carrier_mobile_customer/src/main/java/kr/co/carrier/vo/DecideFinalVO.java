package kr.co.carrier.vo;

public class DecideFinalVO {

	
	private String decideFinalId;
	private String paymentPartnerId;
	private String paymentPartnerName;
	private String amount;
	private String decideFinalStatus;
	private String regDt;
	private String empId;
	private String empName;
	private String comment;
	private String decideMonth;
	
	public String getDecideFinalId() {
		return decideFinalId;
	}
	public void setDecideFinalId(String decideFinalId) {
		this.decideFinalId = decideFinalId;
	}
	public String getPaymentPartnerId() {
		return paymentPartnerId;
	}
	public void setPaymentPartnerId(String paymentPartnerId) {
		this.paymentPartnerId = paymentPartnerId;
	}
	public String getPaymentPartnerName() {
		return paymentPartnerName;
	}
	public void setPaymentPartnerName(String paymentPartnerName) {
		this.paymentPartnerName = paymentPartnerName;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getDecideFinalStatus() {
		return decideFinalStatus;
	}
	public void setDecideFinalStatus(String decideFinalStatus) {
		this.decideFinalStatus = decideFinalStatus;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getDecideMonth() {
		return decideMonth;
	}
	public void setDecideMonth(String decideMonth) {
		this.decideMonth = decideMonth;
	}
	
	
	
	
	
	
	
	
}
