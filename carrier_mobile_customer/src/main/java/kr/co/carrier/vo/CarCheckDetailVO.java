package kr.co.carrier.vo;

public class CarCheckDetailVO {

	private String carCheckDetailId;
	private String allocationId;
	private String allocationStatus;
	private boolean frontBumper;
	private boolean leftFrontFender;
	private boolean frontPanel;
	private boolean rightFrontFender;
	private boolean leftHouse;
	private boolean bonnet;
	private boolean rightHouse;
	private boolean leftSideStep;
	private boolean leftFrontDoor;
	private boolean rightFrontDoor;
	private boolean top;
	private boolean leftBackDoor;
	private boolean rightBackDoor;
	private boolean rightSideStep;
	private boolean leftBackFender;
	private boolean trunk;
	private boolean backPanel;
	private boolean backBumper;
	private boolean rightBackFender;
	private boolean firstCheck;
	private boolean secondCheck;
	private boolean thirdCheck;
	private boolean fourthCheck;
	private String regDt;
	public String getCarCheckDetailId() {
		return carCheckDetailId;
	}
	public void setCarCheckDetailId(String carCheckDetailId) {
		this.carCheckDetailId = carCheckDetailId;
	}
	public String getAllocationId() {
		return allocationId;
	}
	public void setAllocationId(String allocationId) {
		this.allocationId = allocationId;
	}
	public String getAllocationStatus() {
		return allocationStatus;
	}
	public void setAllocationStatus(String allocationStatus) {
		this.allocationStatus = allocationStatus;
	}
	
	
	
	public boolean isFrontBumper() {
		return frontBumper;
	}
	public void setFrontBumper(boolean frontBumper) {
		this.frontBumper = frontBumper;
	}
	public boolean isLeftFrontFender() {
		return leftFrontFender;
	}
	public void setLeftFrontFender(boolean leftFrontFender) {
		this.leftFrontFender = leftFrontFender;
	}
	public boolean isFrontPanel() {
		return frontPanel;
	}
	public void setFrontPanel(boolean frontPanel) {
		this.frontPanel = frontPanel;
	}
	public boolean isRightFrontFender() {
		return rightFrontFender;
	}
	public void setRightFrontFender(boolean rightFrontFender) {
		this.rightFrontFender = rightFrontFender;
	}
	public boolean isLeftHouse() {
		return leftHouse;
	}
	public void setLeftHouse(boolean leftHouse) {
		this.leftHouse = leftHouse;
	}
	public boolean isBonnet() {
		return bonnet;
	}
	public void setBonnet(boolean bonnet) {
		this.bonnet = bonnet;
	}
	public boolean isRightHouse() {
		return rightHouse;
	}
	public void setRightHouse(boolean rightHouse) {
		this.rightHouse = rightHouse;
	}
	public boolean isLeftSideStep() {
		return leftSideStep;
	}
	public void setLeftSideStep(boolean leftSideStep) {
		this.leftSideStep = leftSideStep;
	}
	public boolean isLeftFrontDoor() {
		return leftFrontDoor;
	}
	public void setLeftFrontDoor(boolean leftFrontDoor) {
		this.leftFrontDoor = leftFrontDoor;
	}
	public boolean isRightFrontDoor() {
		return rightFrontDoor;
	}
	public void setRightFrontDoor(boolean rightFrontDoor) {
		this.rightFrontDoor = rightFrontDoor;
	}
	public boolean isTop() {
		return top;
	}
	public void setTop(boolean top) {
		this.top = top;
	}
	public boolean isLeftBackDoor() {
		return leftBackDoor;
	}
	public void setLeftBackDoor(boolean leftBackDoor) {
		this.leftBackDoor = leftBackDoor;
	}
	public boolean isRightBackDoor() {
		return rightBackDoor;
	}
	public void setRightBackDoor(boolean rightBackDoor) {
		this.rightBackDoor = rightBackDoor;
	}
	public boolean isRightSideStep() {
		return rightSideStep;
	}
	public void setRightSideStep(boolean rightSideStep) {
		this.rightSideStep = rightSideStep;
	}
	public boolean isLeftBackFender() {
		return leftBackFender;
	}
	public void setLeftBackFender(boolean leftBackFender) {
		this.leftBackFender = leftBackFender;
	}
	public boolean isTrunk() {
		return trunk;
	}
	public void setTrunk(boolean trunk) {
		this.trunk = trunk;
	}
	public boolean isBackPanel() {
		return backPanel;
	}
	public void setBackPanel(boolean backPanel) {
		this.backPanel = backPanel;
	}
	public boolean isBackBumper() {
		return backBumper;
	}
	public void setBackBumper(boolean backBumper) {
		this.backBumper = backBumper;
	}
	public boolean isRightBackFender() {
		return rightBackFender;
	}
	public void setRightBackFender(boolean rightBackFender) {
		this.rightBackFender = rightBackFender;
	}
	public boolean isFirstCheck() {
		return firstCheck;
	}
	public void setFirstCheck(boolean firstCheck) {
		this.firstCheck = firstCheck;
	}
	public boolean isSecondCheck() {
		return secondCheck;
	}
	public void setSecondCheck(boolean secondCheck) {
		this.secondCheck = secondCheck;
	}
	public boolean isThirdCheck() {
		return thirdCheck;
	}
	public void setThirdCheck(boolean thirdCheck) {
		this.thirdCheck = thirdCheck;
	}
	public boolean isFourthCheck() {
		return fourthCheck;
	}
	public void setFourthCheck(boolean fourthCheck) {
		this.fourthCheck = fourthCheck;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	
	
	
}
