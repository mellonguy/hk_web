package kr.co.carrier.vo;

public class CustomerVO {

	
	private String accountNumber;									//계좌번호		
	private String address;                                         //주소
	private String arrival;                                         //도착지
	private String arrivalSub;                                     //도착지 서브
	private String bankName;                                       //은행명
	private String billingDivision;                                //세금계산서
	private String billingEmail;                                   //세금계산서이메일
	private String businessCondition;                              //업태
	private String businessKind;                                   //종목
	private String businessLicenseNumber;                   //사업자등록번호
	private String corporationRegistrationNumber;                 //법인등록번호
	private String customerId;                                     //거래처코드
	private String customerKind;                                   //회사구분
	private String customerName;                                   //거래처명
	private String customerOwnerName;                             //대표자명
	private String departure;                                       //출발
	private String departureSub;                                   //출발지 서브
	private String depositor;                                       //예금주
	private String etc;                                             //비고
	private String faxNum;                                         //팩스
	private String phoneNum;                                       //대표전화
	private String regDt;                                          //등록일
	private String salesPurchaseDivision;                         //매입매출구분
	private String significantData;                                //특이사항
	private String updateDt;                                       //수정일
	private String updater;										//수정한담당자
	private String paymentDt;									//결제일
	private String accounter;										//회계담당자
	private String corporateRegistrationLicense;			//사업자등록증파일아이디
	private String phone;										//대표전화
	private String addressDetail;								//상세주소
	private String personInCharge;							//담당자
	private String autoSendYn; 								//인수증 자동 발송 (Y:사용,N:미사용)
	
	private String alarmTalkYn;
	private String sendAccountInfoYn;
	private String companyId;
	private String paymentKind;
	private String billingKind;
	private String smsYn;
	private String blackListYn;
	private String regType;
	
	
	public String getAlarmTalkYn() {
		return alarmTalkYn;
	}
	public void setAlarmTalkYn(String alarmTalkYn) {
		this.alarmTalkYn = alarmTalkYn;
	}
	public String getSendAccountInfoYn() {
		return sendAccountInfoYn;
	}
	public void setSendAccountInfoYn(String sendAccountInfoYn) {
		this.sendAccountInfoYn = sendAccountInfoYn;
	}
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	public String getPaymentKind() {
		return paymentKind;
	}
	public void setPaymentKind(String paymentKind) {
		this.paymentKind = paymentKind;
	}
	public String getBillingKind() {
		return billingKind;
	}
	public void setBillingKind(String billingKind) {
		this.billingKind = billingKind;
	}
	public String getSmsYn() {
		return smsYn;
	}
	public void setSmsYn(String smsYn) {
		this.smsYn = smsYn;
	}
	public String getBlackListYn() {
		return blackListYn;
	}
	public void setBlackListYn(String blackListYn) {
		this.blackListYn = blackListYn;
	}
	public String getRegType() {
		return regType;
	}
	public void setRegType(String regType) {
		this.regType = regType;
	}
	public String getAutoSendYn() {
		return autoSendYn;
	}
	public void setAutoSendYn(String autoSendYn) {
		this.autoSendYn = autoSendYn;
	}
	public String getPaymentDt() {
		return paymentDt;
	}
	public void setPaymentDt(String paymentDt) {
		this.paymentDt = paymentDt;
	}
	public String getAccounter() {
		return accounter;
	}
	public void setAccounter(String accounter) {
		this.accounter = accounter;
	}
	public String getCorporateRegistrationLicense() {
		return corporateRegistrationLicense;
	}
	public void setCorporateRegistrationLicense(String corporateRegistrationLicense) {
		this.corporateRegistrationLicense = corporateRegistrationLicense;
	}
	public String getUpdater() {
		return updater;
	}
	public void setUpdater(String updater) {
		this.updater = updater;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getArrival() {
		return arrival;
	}
	public void setArrival(String arrival) {
		this.arrival = arrival;
	}
	public String getArrivalSub() {
		return arrivalSub;
	}
	public void setArrivalSub(String arrivalSub) {
		this.arrivalSub = arrivalSub;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBillingDivision() {
		return billingDivision;
	}
	public void setBillingDivision(String billingDivision) {
		this.billingDivision = billingDivision;
	}
	public String getBillingEmail() {
		return billingEmail;
	}
	public void setBillingEmail(String billingEmail) {
		this.billingEmail = billingEmail;
	}
	public String getBusinessCondition() {
		return businessCondition;
	}
	public void setBusinessCondition(String businessCondition) {
		this.businessCondition = businessCondition;
	}
	public String getBusinessKind() {
		return businessKind;
	}
	public void setBusinessKind(String businessKind) {
		this.businessKind = businessKind;
	}
	
	public String getBusinessLicenseNumber() {
		return businessLicenseNumber;
	}
	public void setBusinessLicenseNumber(String businessLicenseNumber) {
		this.businessLicenseNumber = businessLicenseNumber;
	}
	public String getCorporationRegistrationNumber() {
		return corporationRegistrationNumber;
	}
	public void setCorporationRegistrationNumber(String corporationRegistrationNumber) {
		this.corporationRegistrationNumber = corporationRegistrationNumber;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getCustomerKind() {
		return customerKind;
	}
	public void setCustomerKind(String customerKind) {
		this.customerKind = customerKind;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerOwnerName() {
		return customerOwnerName;
	}
	public void setCustomerOwnerName(String customerOwnerName) {
		this.customerOwnerName = customerOwnerName;
	}
	public String getDeparture() {
		return departure;
	}
	public void setDeparture(String departure) {
		this.departure = departure;
	}
	public String getDepartureSub() {
		return departureSub;
	}
	public void setDepartureSub(String departureSub) {
		this.departureSub = departureSub;
	}
	public String getDepositor() {
		return depositor;
	}
	public void setDepositor(String depositor) {
		this.depositor = depositor;
	}
	public String getEtc() {
		return etc;
	}
	public void setEtc(String etc) {
		this.etc = etc;
	}
	public String getFaxNum() {
		return faxNum;
	}
	public void setFaxNum(String faxNum) {
		this.faxNum = faxNum;
	}
	
	public String getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getSalesPurchaseDivision() {
		return salesPurchaseDivision;
	}
	public void setSalesPurchaseDivision(String salesPurchaseDivision) {
		this.salesPurchaseDivision = salesPurchaseDivision;
	}
	public String getSignificantData() {
		return significantData;
	}
	public void setSignificantData(String significantData) {
		this.significantData = significantData;
	}
	public String getUpdateDt() {
		return updateDt;
	}
	public void setUpdateDt(String updateDt) {
		this.updateDt = updateDt;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAddressDetail() {
		return addressDetail;
	}
	public void setAddressDetail(String addressDetail) {
		this.addressDetail = addressDetail;
	}
	public String getPersonInCharge() {
		return personInCharge;
	}
	public void setPersonInCharge(String personInCharge) {
		this.personInCharge = personInCharge;
	}
	
	
}
