package kr.co.carrier.vo;

public class DriverBillingStatusVO {

	
	
	private String driverBillingStatusId;
	private String driverId;
	private String billingStatus;
	private String decideMonth;
	private String supplyVal;
	private String vatVal;
	private String totalVal;
	private String regDt;
	public String getDriverBillingStatusId() {
		return driverBillingStatusId;
	}
	public void setDriverBillingStatusId(String driverBillingStatusId) {
		this.driverBillingStatusId = driverBillingStatusId;
	}
	public String getDriverId() {
		return driverId;
	}
	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
	public String getBillingStatus() {
		return billingStatus;
	}
	public void setBillingStatus(String billingStatus) {
		this.billingStatus = billingStatus;
	}
	public String getDecideMonth() {
		return decideMonth;
	}
	public void setDecideMonth(String decideMonth) {
		this.decideMonth = decideMonth;
	}
	public String getSupplyVal() {
		return supplyVal;
	}
	public void setSupplyVal(String supplyVal) {
		this.supplyVal = supplyVal;
	}
	public String getVatVal() {
		return vatVal;
	}
	public void setVatVal(String vatVal) {
		this.vatVal = vatVal;
	}
	public String getTotalVal() {
		return totalVal;
	}
	public void setTotalVal(String totalVal) {
		this.totalVal = totalVal;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	
	
	
	
}
