package kr.co.carrier.vo;

public class DriverVO {

	
	private String accountNumber;										//계좌번호
	private String address;                                             //사업장주소
	private String assignCompany;                                      //소속
	private String bankName;                                           //은행명
	private String businessCondition;                                  //업태
	private String businessKind;                                       //종목
	private String carAssignCompany;                                  //차량소속
	private String carIdNum;                                          //차대번호
	private String carKind;                                            //차종
	private String carNickname;                                        //약칭
	private String carNum;                                             //차량번호
	private String companyKind;                                        //사업자구분
	private String businessLicenseNumber;                       //사업자등록번호
	private String deductionRate;                                      //적용요율
	private String depositor;                                           //예금주
	private String driverBalance;                                      //지입료
	private String driverDeposit;                                      //예치금
	private String driverId;                                           //기사코드
	private String driverKind;                                         //기사구분
	private String driverLicense;                                      //자격증번호
	private String driverName;                                         //기사명
	private String driverOwner;										//소유주
	private String etc;                                                 //비고
	private String joinDt;                                             //입사일
	private String phoneNum;                                           //연락처
	private String regDt;                                              //등록일
	private String residentRegistrationNumber;                        //주민등록번호
	private String resignDt;                                           //퇴사일
	private String carGarage;											//차고지
	private String bondedCar;											//보세차량
	private String addressDetail;										//사업장주소상세
	private String registerStatus;									//신고상태
	private String insurance;											//보험가입여부
	private String phoneNumA;
	private String phoneNumB;
	private String driverPwd;
	
	public String getCarGarage() {
		return carGarage;
	}
	public void setCarGarage(String carGarage) {
		this.carGarage = carGarage;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAssignCompany() {
		return assignCompany;
	}
	public void setAssignCompany(String assignCompany) {
		this.assignCompany = assignCompany;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBusinessCondition() {
		return businessCondition;
	}
	public void setBusinessCondition(String businessCondition) {
		this.businessCondition = businessCondition;
	}
	public String getBusinessKind() {
		return businessKind;
	}
	public void setBusinessKind(String businessKind) {
		this.businessKind = businessKind;
	}
	public String getCarAssignCompany() {
		return carAssignCompany;
	}
	public void setCarAssignCompany(String carAssignCompany) {
		this.carAssignCompany = carAssignCompany;
	}
	public String getCarIdNum() {
		return carIdNum;
	}
	public void setCarIdNum(String carIdNum) {
		this.carIdNum = carIdNum;
	}
	public String getCarKind() {
		return carKind;
	}
	public void setCarKind(String carKind) {
		this.carKind = carKind;
	}
	public String getCarNickname() {
		return carNickname;
	}
	public void setCarNickname(String carNickname) {
		this.carNickname = carNickname;
	}
	public String getCarNum() {
		return carNum;
	}
	public void setCarNum(String carNum) {
		this.carNum = carNum;
	}
	public String getCompanyKind() {
		return companyKind;
	}
	public void setCompanyKind(String companyKind) {
		this.companyKind = companyKind;
	}
	
	public String getBusinessLicenseNumber() {
		return businessLicenseNumber;
	}
	public void setBusinessLicenseNumber(String businessLicenseNumber) {
		this.businessLicenseNumber = businessLicenseNumber;
	}
	public String getDeductionRate() {
		return deductionRate;
	}
	public void setDeductionRate(String deductionRate) {
		this.deductionRate = deductionRate;
	}
	public String getDepositor() {
		return depositor;
	}
	public void setDepositor(String depositor) {
		this.depositor = depositor;
	}
	public String getDriverBalance() {
		return driverBalance;
	}
	public void setDriverBalance(String driverBalance) {
		this.driverBalance = driverBalance;
	}
	public String getDriverDeposit() {
		return driverDeposit;
	}
	public void setDriverDeposit(String driverDeposit) {
		this.driverDeposit = driverDeposit;
	}
	public String getDriverId() {
		return driverId;
	}
	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
	public String getDriverKind() {
		return driverKind;
	}
	public void setDriverKind(String driverKind) {
		this.driverKind = driverKind;
	}
	public String getDriverLicense() {
		return driverLicense;
	}
	public void setDriverLicense(String driverLicense) {
		this.driverLicense = driverLicense;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getEtc() {
		return etc;
	}
	public void setEtc(String etc) {
		this.etc = etc;
	}
	
	public String getJoinDt() {
		return joinDt;
	}
	public void setJoinDt(String joinDt) {
		this.joinDt = joinDt;
	}
	public String getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getResidentRegistrationNumber() {
		return residentRegistrationNumber;
	}
	public void setResidentRegistrationNumber(String residentRegistrationNumber) {
		this.residentRegistrationNumber = residentRegistrationNumber;
	}
	public String getResignDt() {
		return resignDt;
	}
	public void setResignDt(String resignDt) {
		this.resignDt = resignDt;
	}
	public String getDriverOwner() {
		return driverOwner;
	}
	public void setDriverOwner(String driverOwner) {
		this.driverOwner = driverOwner;
	}
	public String getBondedCar() {
		return bondedCar;
	}
	public void setBondedCar(String bondedCar) {
		this.bondedCar = bondedCar;
	}
	public String getAddressDetail() {
		return addressDetail;
	}
	public void setAddressDetail(String addressDetail) {
		this.addressDetail = addressDetail;
	}
	public String getRegisterStatus() {
		return registerStatus;
	}
	public void setRegisterStatus(String registerStatus) {
		this.registerStatus = registerStatus;
	}
	public String getInsurance() {
		return insurance;
	}
	public void setInsurance(String insurance) {
		this.insurance = insurance;
	}
	public String getPhoneNumA() {
		return phoneNumA;
	}
	public void setPhoneNumA(String phoneNumA) {
		this.phoneNumA = phoneNumA;
	}
	public String getPhoneNumB() {
		return phoneNumB;
	}
	public void setPhoneNumB(String phoneNumB) {
		this.phoneNumB = phoneNumB;
	}
	public String getDriverPwd() {
		return driverPwd;
	}
	public void setDriverPwd(String driverPwd) {
		this.driverPwd = driverPwd;
	}
	
	
	
	
	
}
