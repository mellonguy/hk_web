package kr.co.carrier.vo;

public class DebtorCreditorVO {

	
	private String debtorCreditorId;		//아이디
	private String customerId;			//거래처코드
	private String type;						//차변 대변 구분 D:차변(받을돈),C:대변(받은돈)
	private String occurrenceDt;			//발생일(날짜)
	private String totalVal;					//총액
	private String summary;				//적요
	private String regDt;					//등록일
	private String publishYn;				//발행 미발행 구분(세금계산서 발행건과 발행 하지 않는건 구분 하기 위한 용도)
	private String registerId;				//등록자(기사 수금인 경우 driver)
	private String debtorId;				//대변에 입력 된 경우 대변에 대응 하는 차변 아이디.
	
	
	public String getDebtorCreditorId() {
		return debtorCreditorId;
	}
	public void setDebtorCreditorId(String debtorCreditorId) {
		this.debtorCreditorId = debtorCreditorId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getOccurrenceDt() {
		return occurrenceDt;
	}
	public void setOccurrenceDt(String occurrenceDt) {
		this.occurrenceDt = occurrenceDt;
	}
	public String getTotalVal() {
		return totalVal;
	}
	public void setTotalVal(String totalVal) {
		this.totalVal = totalVal;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getPublishYn() {
		return publishYn;
	}
	public void setPublishYn(String publishYn) {
		this.publishYn = publishYn;
	}
	public String getRegisterId() {
		return registerId;
	}
	public void setRegisterId(String registerId) {
		this.registerId = registerId;
	}
	public String getDebtorId() {
		return debtorId;
	}
	public void setDebtorId(String debtorId) {
		this.debtorId = debtorId;
	}
	
}
