package kr.co.carrier.vo;

public class EmployeeVO {

	private String idx;
	private String empId;
	private String empName;
	private String joinDt;
	private String resignDt;
	private String salary;
	private String support;
	private String empPwd;
	private String empStatus;
	private String regDt;
	private String empPosition;
	private String kakaoId;
	private String fcmToken;
	private String companyId;
	private String empRole;
	private String empEngName;
	private String birth;
	private String empSex;
	private String solarLunar;
	private String marriageYn;
	private String weddingAnniversary;
	private String phonePersonal;
	private String phoneWork;
	private String phoneWorkSub;
	private String emailPersonal;
	private String emailWork;
	private String address;
	private String addressDetail;
	private String dept;
	private String deptEngName;
	private String empEngPosition;
	private String empGrade;
	private String probationYn;
	private String deductionRate;
	private String probationStartDt;
	private String probationEndDt;
	private String note;
	private String regId;
	private String regName;
	private String allocation;
	private String approveYn;
	
	
	public String getIdx() {
		return idx;
	}
	public void setIdx(String idx) {
		this.idx = idx;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getJoinDt() {
		return joinDt;
	}
	public void setJoinDt(String joinDt) {
		this.joinDt = joinDt;
	}
	public String getResignDt() {
		return resignDt;
	}
	public void setResignDt(String resignDt) {
		this.resignDt = resignDt;
	}
	public String getSalary() {
		return salary;
	}
	public void setSalary(String salary) {
		this.salary = salary;
	}
	public String getSupport() {
		return support;
	}
	public void setSupport(String support) {
		this.support = support;
	}
	public String getEmpPwd() {
		return empPwd;
	}
	public void setEmpPwd(String empPwd) {
		this.empPwd = empPwd;
	}
	public String getEmpStatus() {
		return empStatus;
	}
	public void setEmpStatus(String empStatus) {
		this.empStatus = empStatus;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getEmpPosition() {
		return empPosition;
	}
	public void setEmpPosition(String empPosition) {
		this.empPosition = empPosition;
	}
	public String getKakaoId() {
		return kakaoId;
	}
	public void setKakaoId(String kakaoId) {
		this.kakaoId = kakaoId;
	}
	public String getFcmToken() {
		return fcmToken;
	}
	public void setFcmToken(String fcmToken) {
		this.fcmToken = fcmToken;
	}
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	public String getEmpRole() {
		return empRole;
	}
	public void setEmpRole(String empRole) {
		this.empRole = empRole;
	}
	public String getEmpEngName() {
		return empEngName;
	}
	public void setEmpEngName(String empEngName) {
		this.empEngName = empEngName;
	}
	public String getBirth() {
		return birth;
	}
	public void setBirth(String birth) {
		this.birth = birth;
	}
	public String getEmpSex() {
		return empSex;
	}
	public void setEmpSex(String empSex) {
		this.empSex = empSex;
	}
	public String getSolarLunar() {
		return solarLunar;
	}
	public void setSolarLunar(String solarLunar) {
		this.solarLunar = solarLunar;
	}
	public String getMarriageYn() {
		return marriageYn;
	}
	public void setMarriageYn(String marriageYn) {
		this.marriageYn = marriageYn;
	}
	public String getWeddingAnniversary() {
		return weddingAnniversary;
	}
	public void setWeddingAnniversary(String weddingAnniversary) {
		this.weddingAnniversary = weddingAnniversary;
	}
	public String getPhonePersonal() {
		return phonePersonal;
	}
	public void setPhonePersonal(String phonePersonal) {
		this.phonePersonal = phonePersonal;
	}
	public String getPhoneWork() {
		return phoneWork;
	}
	public void setPhoneWork(String phoneWork) {
		this.phoneWork = phoneWork;
	}
	public String getPhoneWorkSub() {
		return phoneWorkSub;
	}
	public void setPhoneWorkSub(String phoneWorkSub) {
		this.phoneWorkSub = phoneWorkSub;
	}
	public String getEmailPersonal() {
		return emailPersonal;
	}
	public void setEmailPersonal(String emailPersonal) {
		this.emailPersonal = emailPersonal;
	}
	public String getEmailWork() {
		return emailWork;
	}
	public void setEmailWork(String emailWork) {
		this.emailWork = emailWork;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAddressDetail() {
		return addressDetail;
	}
	public void setAddressDetail(String addressDetail) {
		this.addressDetail = addressDetail;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getDeptEngName() {
		return deptEngName;
	}
	public void setDeptEngName(String deptEngName) {
		this.deptEngName = deptEngName;
	}
	public String getEmpEngPosition() {
		return empEngPosition;
	}
	public void setEmpEngPosition(String empEngPosition) {
		this.empEngPosition = empEngPosition;
	}
	public String getEmpGrade() {
		return empGrade;
	}
	public void setEmpGrade(String empGrade) {
		this.empGrade = empGrade;
	}
	public String getProbationYn() {
		return probationYn;
	}
	public void setProbationYn(String probationYn) {
		this.probationYn = probationYn;
	}
	public String getDeductionRate() {
		return deductionRate;
	}
	public void setDeductionRate(String deductionRate) {
		this.deductionRate = deductionRate;
	}
	public String getProbationStartDt() {
		return probationStartDt;
	}
	public void setProbationStartDt(String probationStartDt) {
		this.probationStartDt = probationStartDt;
	}
	public String getProbationEndDt() {
		return probationEndDt;
	}
	public void setProbationEndDt(String probationEndDt) {
		this.probationEndDt = probationEndDt;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	public String getRegName() {
		return regName;
	}
	public void setRegName(String regName) {
		this.regName = regName;
	}
	public String getAllocation() {
		return allocation;
	}
	public void setAllocation(String allocation) {
		this.allocation = allocation;
	}
	public String getApproveYn() {
		return approveYn;
	}
	public void setApproveYn(String approveYn) {
		this.approveYn = approveYn;
	}
	
	
	
	
}
