package kr.co.carrier.vo;

public class CustomerAppVO {

private String 	userId;
private String 	userPwd;
private String 	customerName;
private String 	etc;
private String 	regDt;
private String 	customerId;
private String 	chargeId;
private String 	phoneNum;
private String 	email;
private String 	birthday;




public String getBirthday() {
	return birthday;
}
public void setBirthday(String birthday) {
	this.birthday = birthday;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getChargeId() {
	return chargeId;
}
public void setChargeId(String chargeId) {
	this.chargeId = chargeId;
}
public String getPhoneNum() {
	return phoneNum;
}
public void setPhoneNum(String phoneNum) {
	this.phoneNum = phoneNum;
}
public String getUserId() {
	return userId;
}
public void setUserId(String userId) {
	this.userId = userId;
}
public String getUserPwd() {
	return userPwd;
}
public void setUserPwd(String userPwd) {
	this.userPwd = userPwd;
}
public String getCustomerName() {
	return customerName;
}
public void setCustomerName(String customerName) {
	this.customerName = customerName;
}
public String getEtc() {
	return etc;
}
public void setEtc(String etc) {
	this.etc = etc;
}

public String getRegDt() {
	return regDt;
}
public void setRegDt(String regDt) {
	this.regDt = regDt;
}
public String getCustomerId() {
	return customerId;
}
public void setCustomerId(String customerId) {
	this.customerId = customerId;
}



	
	
	
}
