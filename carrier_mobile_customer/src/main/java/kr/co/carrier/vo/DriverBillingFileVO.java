package kr.co.carrier.vo;

public class DriverBillingFileVO {

	
	
	private String driverBillingFileId;
	private String driverId;
	private String driverBillingFilePath;
	private String driverBillingFileNm;
	private String categoryType;
	private String decideMonth;
	private String regDt;
	private String billingDivision;
		
	public String getDriverBillingFileId() {
		return driverBillingFileId;
	}
	public void setDriverBillingFileId(String driverBillingFileId) {
		this.driverBillingFileId = driverBillingFileId;
	}
	public String getDriverId() {
		return driverId;
	}
	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
	public String getDriverBillingFilePath() {
		return driverBillingFilePath;
	}
	public void setDriverBillingFilePath(String driverBillingFilePath) {
		this.driverBillingFilePath = driverBillingFilePath;
	}
	public String getDriverBillingFileNm() {
		return driverBillingFileNm;
	}
	public void setDriverBillingFileNm(String driverBillingFileNm) {
		this.driverBillingFileNm = driverBillingFileNm;
	}
	public String getCategoryType() {
		return categoryType;
	}
	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}
	public String getDecideMonth() {
		return decideMonth;
	}
	public void setDecideMonth(String decideMonth) {
		this.decideMonth = decideMonth;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getBillingDivision() {
		return billingDivision;
	}
	public void setBillingDivision(String billingDivision) {
		this.billingDivision = billingDivision;
	}
	
}
