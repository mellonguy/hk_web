<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!doctype html>
<html lang="ko">
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>

<body class="" style="overscroll-behavior: contain;">

	<script type="text/javascript">  

var homeLoader;
$(document).ready(function(){


});

$(window).on("scroll", function(){ 
	var scroll_top=$(this).scrollTop();
	$(".loading-indicator-wrapper").css("top",scroll_top);
	$(".loading-indicator-wrapper").css("height","100%");
});


var rgx1 = /\D/g;  // /[^0-9]/g 와 같은 표현
var rgx2 = /(\d+)(\d{3})/;


function setComma(inNum){
     
     var outNum;
     outNum = inNum; 
     while (rgx2.test(outNum)) {
          outNum = outNum.replace(rgx2, '$1' + ',' + '$2');
      }
     return outNum;

}

function comma(str) {
    str = String(str);
    return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
}

function getNumber(obj){
	
     var num01;
     var num02;
     num01 = obj.value;
     num02 = num01.replace(rgx1,"");
     num01 = setComma(num02);
     obj.value =  num01;
     
}



    function selectInterestItem(itemCd,marketCd,obj){
    	
    	$.ajax({ 
    		type: 'post' ,
    		url : "/interest/selectInterestItem.do" ,
    		dataType : 'json' ,
    		data : {
    			itemCd : itemCd,
    			marketCd : marketCd
    		},
    		success : function(data, textStatus, jqXHR)
    		{
    			if(data.resultCode == "0000"){
    				insertInterestItem(itemCd,marketCd,obj);
    			}else{
    				deleteInterestItem(itemCd,marketCd,obj);
    			}
    		} ,
    		error : function(xhRequest, ErrorText, thrownError) {
    		}
    	}); 
    	
    	
    }    
        
    function insertInterestItem(itemCd,marketCd,obj){
    	
    	$.ajax({ 
    		type: 'post' ,
    		url : "/interest/insertInterestItem.do" ,
    		dataType : 'json' ,
    		data : {
    			itemSrtCd : itemCd,
    			marketCd : marketCd
    		},
    		success : function(data, textStatus, jqXHR)
    		{
    			if(data.resultCode == "0000"){
    				$(obj).addClass('heart-full');
    			}else if(data.resultCode == "E002"){
    					alert("로그인 되지 않음");
    			}
    		} ,
    		error : function(xhRequest, ErrorText, thrownError) {
    		}
    	}); 
    }    
        
      

    function deleteInterestItem(itemCd,marketCd,obj){
    	
    	$.ajax({ 
    		type: 'post' ,
    		url : "/interest/deleteInterestItem.do" ,
    		dataType : 'json' ,
    		data : {
    			itemSrtCd : itemCd,
    			marketCd : marketCd
    		},
    		success : function(data, textStatus, jqXHR)
    		{
    			if(data.resultCode == "0000"){
    				$(obj).removeClass('heart-full');
    			}else if(data.resultCode == "E002"){
    					alert("로그인 되지 않음");
    			}
    		} ,
    		error : function(xhRequest, ErrorText, thrownError) {
    		}
    	}); 
    }    


    function prevCategory(obj,category){
        
    	var tabIdx = 0;
		$("#category").children().each(function(index,element){
			if($(this).attr("class") == "active"){
				//$(this).next().trigger("click");
				tabIdx = index-1;
			} 
		});

		var categoryId = "";

		if(currentStatus == "request"){
    		categoryId = "confirm";
			$("#first").css("display","none");
			$("#middle").css("display","");
			$("#last").css("display","none");
        }else if(currentStatus == "confirm"){
    		categoryId = "location";
    		$("#first").css("display","none");
			$("#middle").css("display","");
			$("#last").css("display","none");
        }else if(currentStatus == "location"){
    		categoryId = "car";
    		$("#first").css("display","none");
			$("#middle").css("display","");
			$("#last").css("display","none");
        }else if(currentStatus == "car"){
    		categoryId = "base";
    		$("#first").css("display","");
			$("#middle").css("display","none");
			$("#last").css("display","none");
        }

    	changeCategory($("#category").children().eq(tabIdx),categoryId);
		
    }


    
 	function nextCategory(obj,category){

		var tabIdx = 0;
		$("#category").children().each(function(index,element){
			if($(this).attr("class") == "active"){
				//$(this).next().trigger("click");
				tabIdx = index+1;
			} 
		});
	
		//$.alert(currentStatus);
		
    	var categoryId = "";
    	if(currentStatus == "base"){
    		categoryId = "car";
			$("#first").css("display","none");
			$("#middle").css("display","");
			$("#last").css("display","none");
        }else if(currentStatus == "car"){
    		categoryId = "location";
    		$("#first").css("display","none");
			$("#middle").css("display","");
			$("#last").css("display","none");
        }else if(currentStatus == "location"){
    		categoryId = "confirm";
    		$("#first").css("display","none");
			$("#middle").css("display","none");
			$("#last").css("display","");
        }else if(currentStatus == "confirm"){
    		categoryId = "request";
    		$("#first").css("display","none");
			$("#middle").css("display","none");
			$("#last").css("display","");
        }  
    	changeCategory($("#category").children().eq(tabIdx),categoryId);
 	 }


function validation(obj){

	var contentLen = $("#confirm").find("p").length;
	var contentArr = new Array(contentLen);
	
	$("#confirm").find("p").each(function(index,element){
		contentArr[index] = $.trim($(this).html())+"";					
	});

	if(contentArr[2] == ""){
		$.alert("제목이 작성되지 않았습니다.",function(a){
			var categoryId = "base";
    		$("#first").css("display","");
			$("#middle").css("display","none");
			$("#last").css("display","none");
			changeCategory($("#category").children().eq(0),categoryId);
		});
		return false;
	}
	
	var carCount = Number(contentArr[4]);

	for(var v = 0; v < carCount; v++){
		var gap = Number(7);

		if(contentArr[(v*10)+gap] == ""){
			$.alert((v+1)+"번째 차량의 제조사가 작성되지 않았습니다.",function(a){
				var categoryId = "car";
				$("#first").css("display","none");
				$("#middle").css("display","");
				$("#last").css("display","none");
				changeCategory($("#category").children().eq(1),categoryId);
			});
			return false;
		}else if(contentArr[(v*10)+1+gap] == ""){
			$.alert((v+1)+"번째 차량의 차종(모델명)이(가) 작성되지 않았습니다.",function(a){
				var categoryId = "car";
				$("#first").css("display","none");
				$("#middle").css("display","");
				$("#last").css("display","none");
				changeCategory($("#category").children().eq(1),categoryId);
			});
			return false;
			
		}else if(contentArr[(v*10)+2+gap] == ""){
			$.alert((v+1)+"번째 차량의 세부차종(트림)이(가) 작성되지 않았습니다.",function(a){
				var categoryId = "car";
				$("#first").css("display","none");
				$("#middle").css("display","");
				$("#last").css("display","none");
				changeCategory($("#category").children().eq(1),categoryId);
			});
			return false;
			
		}else if(contentArr[(v*10)+3+gap] == ""){
			$.alert((v+1)+"번째 차량의 차량번호이(가) 작성되지 않았습니다.",function(a){
				var categoryId = "car";
				$("#first").css("display","none");
				$("#middle").css("display","");
				$("#last").css("display","none");
				changeCategory($("#category").children().eq(1),categoryId);
			});
			return false;
			
		}else if(contentArr[(v*10)+4+gap] == ""){
			//차대번호
			
		}else if(contentArr[(v*10)+6+gap] == ""){
			$.alert((v+1)+"번째 차량의 출발지이(가) 작성되지 않았습니다.",function(a){
				var categoryId = "location";
	    		$("#first").css("display","none");
				$("#middle").css("display","");
				$("#last").css("display","none");
				changeCategory($("#category").children().eq(2),categoryId);
			});
			return false;
			
		}else if(contentArr[(v*10)+7+gap] == ""){
			$.alert((v+1)+"번째 차량의 출발지이(가) 검색되지 않았습니다. 주소를 검색 해 주세요.",function(a){
				var categoryId = "location";
	    		$("#first").css("display","none");
				$("#middle").css("display","");
				$("#last").css("display","none");
				changeCategory($("#category").children().eq(2),categoryId);
			});
			return false;
			
		}else if(contentArr[(v*10)+8+gap] == ""){
			$.alert((v+1)+"번째 차량의 도착지이(가) 작성되지 않았습니다.",function(a){
				var categoryId = "location";
	    		$("#first").css("display","none");
				$("#middle").css("display","");
				$("#last").css("display","none");
				changeCategory($("#category").children().eq(2),categoryId);
			});
			return false;
			
		}else if(contentArr[(v*10)+9+gap] == ""){
			$.alert((v+1)+"번째 차량의 도착지이(가) 검색되지 않았습니다. 주소를 검색 해 주세요.",function(a){
				var categoryId = "location";
	    		$("#first").css("display","none");
				$("#middle").css("display","");
				$("#last").css("display","none");
				changeCategory($("#category").children().eq(2),categoryId);
			});
			return false;
			
		}

	}

	$.confirm("이 내용으로 경매요청을 진행 하시겠습니까?",function(a){
			if(a){
				insertRequest(obj,contentArr)
			}
	});
}



 	
function insertRequest(obj,contentArr){


		$.ajax({ 
			type: 'post' ,
			url : "/allocation/insert-allocation.do" ,
			dataType : 'json' ,
			traditional : true,
			data : {
				contentArr : contentArr
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				if(result == "0000"){
					$.alert("견적 요청이 정상적으로 등록 되었습니다.",function(a){
						document.location.href = "/allocation/main.do";
					});
				}else if(result == "E001"){
					$.alert("알 수없는 오류입니다. 카카오톡 플러스채팅으로 문의 주세요",function(a){
						return;	
					});
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});

 	 			
}
 		


	    
    
var isChange = false;

   function changeCategory(obj,categoryId){

    	var inputCount = $("#car").children().length;
		$("#category").children().each(function(index,element){
			$(this).removeClass("active");
		});
		$(obj).addClass("active");

			if(categoryId != ""){
				
				$("#total").children().each(function(index,element){
					$(this).css("display","none");
				})
				
				if(categoryId == "car"){

					var tmp = carCount;

			    	if($("#carCount").val() == 0 && carCount == 0){
			    		$.alert("차량대수가 입력 되지 않았습니다.",function(a){
						});
			    		$("#"+categoryId).css("display","");
			    		isChange = false;
						return;
						
			    	}else if($("#carCount").val() != 0 && carCount == 0){
			        	if($("#carCount").val() < 1){
			        		$("#carCount").val(1)
			            }else if($("#carCount").val() > 10){
			        		$("#carCount").val(10)
			            }
			    		carCount = $("#carCount").val();

			    		$("#car").html("");
						for(var i = 0; i < carCount; i++){
							$("#car").append(contents);
						}
						$("#"+categoryId).css("display","");
						isChange = true;
			        }else if(carCount != 0 && carCount != $("#carCount").val()){
						//기존 정보가 변경 되는경우
			        	carCount = $("#carCount").val();
			        	$.confirm("차량대수가 변경 되었습니다. 적용 하시겠습니까?",function(a){
							if(a){
								isChange = true;
								var carLen = $("#car").children().length;
								if(carLen < carCount){
									//기존의 차량대수보다 많아지는경우 많아진만큼 추가 한다.
									for(var i = 0; i < carCount-carLen; i++){
										$("#car").append(contents);
									}
								}else if(carLen > carCount){
									//기존의 차량대수보다 적어지는 경우
									$("#car").children().each(function(index,element){
										if(index > carCount-1){
											$(this).remove();
										}
									});
								}else{
									//그럴리는 없겠지만...
									$("#car").html("");
									for(var i = 0; i < carCount; i++){
										$("#car").append(contents);
									}
								}
								
								$("#"+categoryId).css("display","");
							}else{
								isChange = false;
								$("#carCount").val(tmp);
								carCount = tmp;
								$("#"+categoryId).css("display","");
							}
						});

			        	
			        }else if(carCount != 0 && carCount == $("#carCount").val()){
			        	isChange = false;
			        	$("#"+categoryId).css("display","");
				    }

				}else if(categoryId != "car"){

					if(categoryId == "location"){

						//$.alert(carCount);
						if(Number(carCount) == 0){

							$.alert("차량정보가 입력 되지 않았습니다.",function(a){
							});
							$("#category").children().each(function(index,element){
								$(this).removeClass("active");
							});
							$("#category").children().first().addClass("active");
							$("#base").css("display","");
							return false;
						}else{

							//$("#innerLocationDetail").html("");
							
							if(Number($("#carCount").val()) <= 1){
								$("#locationSetting").css("display","none");
							}else{
								$("#locationSetting").css("display","");
							}
							$("#"+categoryId).css("display","");

							var carInfoArr = new Array(inputCount); 

							$("#car").children().each(function(index,element){
								var setInfo = $(carInfo);
								$(this).find("input").each(function(innerindex,innerelement){
									$(setInfo).find("P").eq(innerindex).html($(this).val());
								});
								$(setInfo).find("P").last().html($(this).find("select").eq(0).val()=="OC"?"중고차":"신차");
								carInfoArr[index] = $(setInfo);
								//$.alert($(carInfoArr[index]).html());
							});

							/*
							for(var i = 0; i < Number(inputCount); i++){
								$("#innerLocationDetail").append(carInfoArr[i]);
							}
							$("#innerLocationDetail").append(departure);
							$("#innerLocationDetail").append(arrival);
							*/
								
						}

						if(currentStatus == "car"){
							getCheckBoxVal("");
						}
						
					}else if(categoryId == "confirm"){

						$("#confirm").html("");
						var baseInfoArr = new Array(7);
						baseInfoArr[0] = $("#base").find("input").eq(0).val();
						baseInfoArr[1] = $("#base").find("input").eq(1).val();
						baseInfoArr[2] = $("#base").find("input").eq(2).val();
						baseInfoArr[3] = "";
						if($("#base").find("select").eq(0).val() == "SL"){
							baseInfoArr[3] = "세이프티로더";
						}else if($("#base").find("select").eq(0).val() == "CA"){
							baseInfoArr[3] = "캐리어";
						}else if($("#base").find("select").eq(0).val() == "RD"){
							baseInfoArr[3] = "로드탁송";
						}else if($("#base").find("select").eq(0).val() == "TR"){
							baseInfoArr[3] = "추레라";
						}else if($("#base").find("select").eq(0).val() == "FC"){
							baseInfoArr[3] = "풀카";
						}else if($("#base").find("select").eq(0).val() == "BC"){
							baseInfoArr[3] = "박스카";
						}else if($("#base").find("select").eq(0).val() == "NE"){
							baseInfoArr[3] = "상관없음";
						}
						
						baseInfoArr[4] = carCount;
						baseInfoArr[5] = "";
						if($("#base").find("select").eq(1).val() == "PR"){
							baseInfoArr[5] = "우선순위결정";
						}else if($("#base").find("select").eq(1).val() == "OA"){
							baseInfoArr[5] = "선착순결정";
						}
						baseInfoArr[6] = $("#base").find("select").eq(2).val()+"분";

						var setBaseInfo = $(baseInfo);
						
						$(setBaseInfo).find("p").each(function(index,element){
							$(this).html(baseInfoArr[index]);
						});
						var departureIsChecked = $("#sameDeparture").is(':checked');
						var arrivalIsChecked = $("#sameArrival").is(':checked');
						$("#confirm").append($(setBaseInfo));
						
						//차량 관련
						var inputCount = $("#car").children().length;
						var carInfoArr = new Array(inputCount); 
						$("#car").children().each(function(index,element){
							var setInfo = $(carInfo);
							$(this).find("input").each(function(innerindex,innerelement){
								$(setInfo).find("P").eq(innerindex).html($(this).val())
							});
							$(setInfo).find("P").last().html($(this).find("select").eq(0).val()=="OC"?"중고차":"신차");
							carInfoArr[index] = $(setInfo);
						});

						//주소 관련						
						var addressCount = $("#innerLocationDetail").find(".product-use-item").length;
						addressInfoArr = new Array(addressCount);
						var innerDetail = $("#innerLocationDetail").clone();
						if(Number(addressCount) > 0){
							$(innerDetail).find(".product-use-item").each(function(index,element){
								addressInfoArr[index] = new Array(2);
								$(this).find("input").each(function(innerindex,innerelement){
									addressInfoArr[index][innerindex] = $(this).val();
								});
							});
						}
						
						if(departureIsChecked && arrivalIsChecked){
							//출발지와 도착지가 같은경우
							for(var i = 0; i < Number(inputCount); i++){
								$("#confirm").append(carInfoArr[i]);
								$("#confirm").append($(address));
							}
							$("#confirm").find(".product-use-item").each(function(index,element){
								$(this).find("p").each(function(innerindex,innerelement){
									$(this).html(addressInfoArr[index%2][innerindex]);
								});
							});
														
						}else if(departureIsChecked && !arrivalIsChecked){
							//출발지가 같고 도착지가 다른경우
							for(var i = 0; i < Number(inputCount); i++){
								$("#confirm").append(carInfoArr[i]);
								$("#confirm").append($(address));
							}
							var count = 0;
							$("#confirm").find(".product-use-item").each(function(index,element){							
								$(this).find("p").each(function(innerindex,innerelement){
									if(index%2 == 0){
										$(this).html(addressInfoArr[0][innerindex]);
									}else{
										$(this).html(addressInfoArr[index-count][innerindex]);
									}
								});
								if(index%2 != 0){
									count++;
								}
							});
						
						}else if(!departureIsChecked && arrivalIsChecked){
							//출발지가 다르고 도착지가 같은경우
							for(var i = 0; i < Number(inputCount); i++){
								$("#confirm").append(carInfoArr[i]);
								$("#confirm").append($(address));
							}
							var count = 0;
							$("#confirm").find(".product-use-item").each(function(index,element){							
								$(this).find("p").each(function(innerindex,innerelement){
									if(index%2 == 0){
										$(this).html(addressInfoArr[index-count][innerindex]);
									}else{
										$(this).html(addressInfoArr[addressInfoArr.length-1][innerindex]);
									}
								});
								if(index%2 == 0){
									count++;
								}
							});
						
						}else if(!departureIsChecked && !arrivalIsChecked){
							//출발지와 도착지가 다른경우
							for(var i = 0; i < Number(inputCount); i++){
								$("#confirm").append(carInfoArr[i]);
								$("#confirm").append($(address));
							}
							$("#confirm").find(".product-use-item").each(function(index,element){
								$(this).find("p").each(function(innerindex,innerelement){
									$(this).html(addressInfoArr[index][innerindex]);
								});
							});
						} 
						$("#"+categoryId).css("display","");
					}else{
						$("#"+categoryId).css("display","");
					}
				}
							
				
			}else{

			}
			

	currentStatus = categoryId;
			
   }
    
    

function showMessage(obj){


	//alert($(obj).next().val());

	
	if($(obj).next().val() == "PR" || $(obj).next().val() == "OA"){

		if($(obj).next().val() == "PR"){
			$.alert("우선순위 결정 : 입찰 후 낙찰 결정시 1순위~5순위까지 결정 하고 1순위부터 차례로 낙찰 확정기회를 부여 하여 해당 차례에 낙찰 확정 하는 입찰 건에 최종 낙찰되는 경매 방식 입니다.",function(a){
			});
		}else if($(obj).next().val() == "OA"){
			$.alert("선착순 결정 : 입찰 후 낙찰 결정시 1~5개의 입찰 건에 낙찰 확정 기회를 부여 하고 가장 먼저 낙찰 확정 하는 입찰 건에 최종 낙찰되는 경매 방식 입니다.",function(a){
			});
		}

	}else if($(obj).next().find("input").attr("id") == "sameDeparture"){

		$.alert("탁송하는 모든 차량의 출발지가 동일한경우에 사용 합니다. 출발지가 다른경우 각각 입력 할 수 있습니다.",function(a){
		});

	}else if($(obj).next().find("input").attr("id") == "sameArrival"){

		$.alert("탁송하는 모든 차량의 도착지가 동일한경우에 사용 합니다. 도착지가 다른경우 각각 입력 할 수 있습니다.",function(a){
		});

	}else{

		$.alert("우선순위 결정의 경우 후 순위 입찰에 기회가 부여 되는 시간이고 선착순 결정의 경우 낙찰 확정기회가 부여 되는 시간을 의미합니다.",function(a){

		});
		
	}
	
}



function getCheckBoxVal(obj){


	var addressCount = $("#innerLocationDetail").find(".product-use-item").length;
	var innerDetail = $("#innerLocationDetail").clone();

	var isInput = false;

	
	if(Number(addressCount) > 0){
		addressInfoArr = new Array(addressCount);			
		$(innerDetail).find(".product-use-item").each(function(index,element){
			addressInfoArr[index] = new Array(2);
			$(this).find("input").each(function(innerindex,innerelement){
				addressInfoArr[index][innerindex] = $(this).val();
				if($(this).val() != ""){
					isInput = true;
				}
			});
		});
	}

	if(obj != ""){

		if(isInput){
			$.confirm("주소 정보가 초기화 됩니다. 적용 하시겠습니까?",function(a){
				if(a){

					$("#innerLocationDetail").html("");
					
					var inputCount = $("#car").children().length;
					var carInfoArr = new Array(inputCount); 

					$("#car").children().each(function(index,element){
						var setInfo = $(carInfo);
						$(this).find("input").each(function(innerindex,innerelement){
							$(setInfo).find("P").eq(innerindex).html($(this).val())
						});
						//$(this).find("select").each(function(innerindex,innerelement){
						//	$(setInfo).find("P").last().html($(this).val())
						//});
						$(setInfo).find("P").last().html($(this).find("select").eq(0).val()=="OC"?"중고차":"신차");
						carInfoArr[index] = $(setInfo);
						//$.alert($(carInfoArr[index]).html());
					});
					

						var departureIsChecked = $("#sameDeparture").is(':checked');
						var arrivalIsChecked = $("#sameArrival").is(':checked');

						if(departureIsChecked && arrivalIsChecked){
						//상차지와 하차지가 같은경우
							//$.alert(0);
							for(var i = 0; i < Number(inputCount); i++){
								$("#innerLocationDetail").append(carInfoArr[i]);
							}
							$("#innerLocationDetail").append(departure);
							$("#innerLocationDetail").append(arrival);
						}else if(departureIsChecked && !arrivalIsChecked){
						//상차지는 같고 하차지가 다른경우
							//$.alert(1);
							$("#innerLocationDetail").append(departure);
							for(var i = 0; i < Number(inputCount); i++){
								$("#innerLocationDetail").append(carInfoArr[i]);
								$("#innerLocationDetail").append(arrival);
							}
						}else if(!departureIsChecked && arrivalIsChecked){
						//상차지가 다르고 하차지가 같은경우
							//$.alert(2);
							for(var i = 0; i < Number(inputCount); i++){
								$("#innerLocationDetail").append(carInfoArr[i]);
								$("#innerLocationDetail").append(departure);
							}
							$("#innerLocationDetail").append(arrival);
						}else if(!departureIsChecked && !arrivalIsChecked){
						//상차지도 다르고 하차지도 다른경우
							//$.alert(3);
							for(var i = 0; i < Number(inputCount); i++){
								$("#innerLocationDetail").append(carInfoArr[i]);
								$("#innerLocationDetail").append(departure);
								$("#innerLocationDetail").append(arrival);
							}
						}


						if(obj == ""){
							//생성이 끝났으면 입력 되어 잇던 주소를 입력 한다.		
							if(Number(addressCount) > 0){
								$("#innerLocationDetail").find(".product-use-item").each(function(index,element){
									$(this).find("input").each(function(innerindex,innerelement){
										$(this).val(addressInfoArr[index][innerindex]);
									});
								});
							}
						}


							
				}else{

					var isChecked = $(obj).is(':checked');
					
					if(isChecked){
						$(obj).prop("checked",false);
					}else{
						$(obj).prop("checked",true);
					}
				}
			});

		}else{

			$("#innerLocationDetail").html("");
			
			var inputCount = $("#car").children().length;
			var carInfoArr = new Array(inputCount); 

			$("#car").children().each(function(index,element){
				var setInfo = $(carInfo);
				$(this).find("input").each(function(innerindex,innerelement){
					$(setInfo).find("P").eq(innerindex).html($(this).val())
				});
				$(setInfo).find("P").last().html($(this).find("select").eq(0).val()=="OC"?"중고차":"신차");
				carInfoArr[index] = $(setInfo);
				//$.alert($(carInfoArr[index]).html());
			});
			

				var departureIsChecked = $("#sameDeparture").is(':checked');
				var arrivalIsChecked = $("#sameArrival").is(':checked');

				if(departureIsChecked && arrivalIsChecked){
				//상차지와 하차지가 같은경우
					//$.alert(0);
					for(var i = 0; i < Number(inputCount); i++){
						$("#innerLocationDetail").append(carInfoArr[i]);
					}
					$("#innerLocationDetail").append(departure);
					$("#innerLocationDetail").append(arrival);
				}else if(departureIsChecked && !arrivalIsChecked){
				//상차지는 같고 하차지가 다른경우
					//$.alert(1);
					$("#innerLocationDetail").append(departure);
					for(var i = 0; i < Number(inputCount); i++){
						$("#innerLocationDetail").append(carInfoArr[i]);
						$("#innerLocationDetail").append(arrival);
					}
				}else if(!departureIsChecked && arrivalIsChecked){
				//상차지가 다르고 하차지가 같은경우
					//$.alert(2);
					for(var i = 0; i < Number(inputCount); i++){
						$("#innerLocationDetail").append(carInfoArr[i]);
						$("#innerLocationDetail").append(departure);
					}
					$("#innerLocationDetail").append(arrival);
				}else if(!departureIsChecked && !arrivalIsChecked){
				//상차지도 다르고 하차지도 다른경우
					//$.alert(3);
					for(var i = 0; i < Number(inputCount); i++){
						$("#innerLocationDetail").append(carInfoArr[i]);
						$("#innerLocationDetail").append(departure);
						$("#innerLocationDetail").append(arrival);
					}
				}


		}
				
	}else{

	
		$("#innerLocationDetail").html("");
		var inputCount = $("#car").children().length;
		var carInfoArr = new Array(inputCount); 

		$("#car").children().each(function(index,element){
			var setInfo = $(carInfo);
			$(this).find("input").each(function(innerindex,innerelement){
				$(setInfo).find("P").eq(innerindex).html($(this).val())
			});
			$(setInfo).find("P").last().html($(this).find("select").eq(0).val()=="OC"?"중고차":"신차");
			carInfoArr[index] = $(setInfo);
			//$.alert($(carInfoArr[index]).html());
		});
		

			var departureIsChecked = $("#sameDeparture").is(':checked');
			var arrivalIsChecked = $("#sameArrival").is(':checked');

			if(departureIsChecked && arrivalIsChecked){
			//상차지와 하차지가 같은경우
				//$.alert(0);
				for(var i = 0; i < Number(inputCount); i++){
					$("#innerLocationDetail").append(carInfoArr[i]);
				}
				$("#innerLocationDetail").append(departure);
				$("#innerLocationDetail").append(arrival);
			}else if(departureIsChecked && !arrivalIsChecked){
			//상차지는 같고 하차지가 다른경우
				//$.alert(1);
				$("#innerLocationDetail").append(departure);
				for(var i = 0; i < Number(inputCount); i++){
					$("#innerLocationDetail").append(carInfoArr[i]);
					$("#innerLocationDetail").append(arrival);
				}
			}else if(!departureIsChecked && arrivalIsChecked){
			//상차지가 다르고 하차지가 같은경우
				//$.alert(2);
				for(var i = 0; i < Number(inputCount); i++){
					$("#innerLocationDetail").append(carInfoArr[i]);
					$("#innerLocationDetail").append(departure);
				}
				$("#innerLocationDetail").append(arrival);
			}else if(!departureIsChecked && !arrivalIsChecked){
			//상차지도 다르고 하차지도 다른경우
				//$.alert(3);
				for(var i = 0; i < Number(inputCount); i++){
					$("#innerLocationDetail").append(carInfoArr[i]);
					$("#innerLocationDetail").append(departure);
					$("#innerLocationDetail").append(arrival);
				}
			}

			if(!isChange){
				if(Number(addressCount) > 0){
					$("#innerLocationDetail").find(".product-use-item").each(function(index,element){
						$(this).find("input").each(function(innerindex,innerelement){
							$(this).val(addressInfoArr[index][innerindex]);
						});
					});
				}
			}

			
	}

	//공통으로 설정 되는 부분은 경매진행 하는 차량이 2대 이상인 경우에만...
	if(carCount > 1){
		if(departureIsChecked && arrivalIsChecked){
			$(".product-use-item").find("span").each(function(index,element){
				if($(this).html() == "출발지" || $(this).html() == "도착지"){
					$(this).html("공통"+$(this).html()+"(동일한 "+$(this).html()+"로 설정 됩니다.)");
				} 
			});
		}else if(departureIsChecked && !arrivalIsChecked){
			$(".product-use-item").find("span").each(function(index,element){
				if($(this).html() == "출발지"){
					$(this).html("공통"+$(this).html()+"(동일한 "+$(this).html()+"로 설정 됩니다.)");
				} 
			});
		}else if(!departureIsChecked && arrivalIsChecked){
			$(".product-use-item").find("span").each(function(index,element){
				if($(this).html() == "도착지"){
					$(this).html("공통"+$(this).html()+"(동일한 "+$(this).html()+"로 설정 됩니다.)");
				} 
			});
		}
	}
	
}
    


var map, marker;
var markerArr = [];
var selectedObj;

function goShowModal(obj){


	//return false;

	
	if($(obj).prev().prev().val() == ""){

		$.alert("주소가 입력 되지 않았습니다.",function(a){
		
		});
		return false;
	}else{

		selectedObj = $(obj);
		
		$(selectedObj).parent().next().show();
		$(selectedObj).parent().next().find("ul").css("display","block");
		
		$.ajax({
			method:"GET",
			url:"https://apis.openapi.sk.com/tmap/pois?version=1&format=json&callback=result",
			async:false,
			data:{
				"appKey" : "${appKey}",
				"searchKeyword" : $(obj).prev().prev().val(),
				"resCoordType" : "EPSG3857",
				"reqCoordType" : "WGS84GEO",
				"count" : 10
			},
			success:function(response){

				
				var resultpoisData = response.searchPoiInfo.pois.poi;
				
				// 기존 마커, 팝업 제거
				if(markerArr.length > 0){
					for(var i in markerArr){
						markerArr[i].setMap(null);
					}
				}
				var innerHtml ="";	// Search Reulsts 결과값 노출 위한 변수
				var positionBounds = new Tmapv2.LatLngBounds();		//맵에 결과물 확인 하기 위한 LatLngBounds객체 생성
				
				for(var k in resultpoisData){
					
					var noorLat = Number(resultpoisData[k].noorLat);
					var noorLon = Number(resultpoisData[k].noorLon);
					var name = resultpoisData[k].name;

					var upperAddrName = resultpoisData[k].upperAddrName;
					var middleAddrName = resultpoisData[k].middleAddrName;
					var lowerAddrName = resultpoisData[k].lowerAddrName;
					var detailAddrName = resultpoisData[k].detailAddrName;
					var firstNo = resultpoisData[k].firstNo;
					var secondNo = resultpoisData[k].secondNo == "" ? "" : "-"+resultpoisData[k].secondNo;

					var pointCng = new Tmapv2.Point(noorLon, noorLat);
					var projectionCng = new Tmapv2.Projection.convertEPSG3857ToWGS84GEO(pointCng);
					
					var lat = projectionCng._lat;
					var lon = projectionCng._lng;
					
					var markerPosition = new Tmapv2.LatLng(lat, lon);
					
					marker = new Tmapv2.Marker({
				 		position : markerPosition,
				 		icon : "http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_" + k + ".png",
						iconSize : new Tmapv2.Size(24, 38),
						title : name,
				 	});
					
					innerHtml += '<li  onclick="javascript:inputValue(\''+name+" ("+upperAddrName+" "+middleAddrName+" "+lowerAddrName+" "+detailAddrName+" "+firstNo+secondNo+")"+'\',\''+lon+'\',\''+lat+'\')">';
					innerHtml += '<img src="http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_' + k + '.png" style="vertical-align:middle;"/>';
					innerHtml += '<span>'+name+'</span>';
					innerHtml += '<span style="display:block; color:#3f3f3f;" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;('+upperAddrName+" "+middleAddrName+" "+lowerAddrName+" "+detailAddrName+" "+firstNo+secondNo+')</span>';
					innerHtml += '</li>';

					markerArr.push(marker);
					positionBounds.extend(markerPosition);	// LatLngBounds의 객체 확장
				}
				
				$(selectedObj).parent().next().find("ul").html(innerHtml);
				
			},
			error:function(request,status,error){
				console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
			}
		});
		
	}
	
}

	function inputValue(address,px,py){
		
		$(selectedObj).prev().prev().val(address);
		$(selectedObj).parent().next().find("ul").css("display","none");
		$(selectedObj).prev().val(px+","+py);
		$(selectedObj).parent().next().css('display','none');
	    			
	}

	function prevPageConfirm(){
		
		//document.location.href = "/allocation/main.do";
		homeLoader.show();
		history.go(-1);
		
	}


	function removeCarInfo(obj){

		if($("#car").children().length > 1){

			$.confirm("이 차량의 정보를 삭제 하시겠습니까?",function(a){
				if(a){
					$(obj).parent().parent().parent().parent().parent().remove();
					$("#carCount").val($("#carCount").val()-1);
					setCarCategory($("#category").children().eq(1),"car");
					$.alert("삭제되었습니다.",function(a){
					});		
				}else{
					$.alert("삭제가 취소 되었습니다.",function(a){
					});
				}
			});
		}else{
			$.alert("차량정보를 삭제 할 수 없습니다.",function(a){
				
			});

		}

	}


	  function setCarCategory(obj,categoryId){

	    	var inputCount = $("#car").children().length;
			$("#category").children().each(function(index,element){
				$(this).removeClass("active");
			});
			$(obj).addClass("active");

        	carCount = $("#carCount").val();
       	
			isChange = true;
			var carLen = $("#car").children().length;
			if(carLen < carCount){
				//기존의 차량대수보다 많아지는경우 많아진만큼 추가 한다.
				for(var i = 0; i < carCount-carLen; i++){
					$("#car").append(contents);
				}
			}else if(carLen > carCount){
				//기존의 차량대수보다 적어지는 경우
				$("#car").children().each(function(index,element){
					if(index > carCount-1){
						$(this).remove();
					}
				});
			}else{
				
			}
			
			$("#"+categoryId).css("display","");
		
		currentStatus = categoryId;
				
	   }
	    
	 
	  function getNumberOnly (str) {			//저장할때는 콤마 제거 후 저장한다.
		    var len = str.length;
		    var sReturn = "";

		    for (var i=0; i<len; i++){
		        if ( (str.charAt(i) >= "0") && (str.charAt(i) <= "9") ){
		            sReturn += str.charAt(i);
		        }
		    }
		    return sReturn;
		}


function goBid(obj,corId,corStatus){

	if(corStatus != "II"){
		$.alert("입찰 진행중인 건에 대해서만 입찰이 가능 합니다.",function(a){
		});
		return false;
	}
	
	var amount = getNumberOnly($(obj).prev().val());
	if(amount == "" || Number(amount) == 0){
		$.alert("입찰액이 입력 되지 않았습니다.",function(a){
		});
		return false;
	}else if(Number(amount) < 50000){
		$.alert("입찰에 참여 할 수 있는 최소 금액은 50,000원 입니다.",function(a){
		});
		return false;
	}


	var latlng = "";
	latlng = getDeviceLocation();
	var result = "";
	if(latlng != ""){
		//$.alert("현재 좌표는   ="+ latlng);
		//$.alert("현재 좌표는   ="+ latlng.split(",")[1]);
		//$.alert("현재 좌표는   ="+ latlng.split(",")[0]);
		result = reverseGeo(latlng.split(",")[1], latlng.split(",")[0]);
		//var addr = result.split("^")[0];
		//var startX = result.split("^")[1];
		//var startY = result.split("^")[2];
		//getDistance(startX, startY, latlng.split(",")[1],latlng.split(",")[0]);
		
	}
	
	$.confirm("이 경매건에 입찰 하시겠습니까?",function(a){
		
		if(a){

			$.ajax({ 
	    		type: 'post' ,
	    		url : "/bid/insert-bid.do" ,
	    		dataType : 'json' ,
	    		data : {
	    			corId : corId,
	    			amount : amount,
	    			crbDvrAddress : result.split("^")[0],
	    			crbDvrLng : result.split("^")[1],
	    			crbDvrLat : result.split("^")[2]
	    		},
	    		success : function(data, textStatus, jqXHR)
	    		{
	    			if(data.resultCode == "0000"){
	    				$.alert("입찰에 성공 했습니다.",function(a){
	    					window.location.reload();
		    			});
	    			}else{
	    				if(data.resultCode == "E010" || data.resultCode == "E011" || data.resultCode == "E012" ){
	    					$.alert(data.resultMsg,function(a){
		    					window.location.reload();
			    			});
	    				}else{
	    					$.alert("입찰하는데 실패 했습니다. 관리자에게 문의 하세요.",function(a){
		    					window.location.reload();
			    			});
			    		}
	    				
	    			}
	    		} ,
	    		error : function(xhRequest, ErrorText, thrownError) {
	    		}
	    	}); 
			
		}
	});


	}



function reverseGeo(lon, lat) {
	$.ajax({
				method : "GET",
				url : "https://apis.openapi.sk.com/tmap/geo/reversegeocoding?version=1&format=json&callback=result",
				async : false,
				data : {
					"appKey" : "${appKey}",
					"coordType" : "WGS84GEO",
					"addressType" : "A10",
					"lon" : lon,
					"lat" : lat
				},
				success : function(response) {
					// 3. json에서 주소 파싱
					var arrResult = response.addressInfo;

					//법정동 마지막 문자 
					var lastLegal = arrResult.legalDong
							.charAt(arrResult.legalDong.length - 1);

					// 새주소
					newRoadAddr = arrResult.city_do + ' '
							+ arrResult.gu_gun + ' ';

					if (arrResult.eup_myun == ''
							&& (lastLegal == "읍" || lastLegal == "면")) {//읍면
						newRoadAddr += arrResult.legalDong;
					} else {
						newRoadAddr += arrResult.eup_myun;
					}
					newRoadAddr += ' ' + arrResult.roadName + ' '
							+ arrResult.buildingIndex;

					// 새주소 법정동& 건물명 체크
					if (arrResult.legalDong != ''
							&& (lastLegal != "읍" && lastLegal != "면")) {//법정동과 읍면이 같은 경우

						if (arrResult.buildingName != '') {//빌딩명 존재하는 경우
							newRoadAddr += (' (' + arrResult.legalDong
									+ ', ' + arrResult.buildingName + ') ');
						} else {
							newRoadAddr += (' (' + arrResult.legalDong + ')');
						}
					} else if (arrResult.buildingName != '') {//빌딩명만 존재하는 경우
						newRoadAddr += (' (' + arrResult.buildingName + ') ');
					}

					// 구주소
					jibunAddr = arrResult.city_do + ' '
							+ arrResult.gu_gun + ' '
							+ arrResult.legalDong + ' ' + arrResult.ri
							+ ' ' + arrResult.bunji;
					//구주소 빌딩명 존재
					if (arrResult.buildingName != '') {//빌딩명만 존재하는 경우
						jibunAddr += (' ' + arrResult.buildingName);
					}

					result = "새주소 : " + newRoadAddr + "</br>";
					result += "지번주소 : " + jibunAddr + "</br>";
					result += "위경도좌표 : " + lat + ", " + lon;

					//var resultDiv = document.getElementById("result");
					//resultDiv.innerHTML = result;
					//$.alert(result);

				},
				error : function(request, status, error) {
					console.log("code:" + request.status + "\n"
							+ "message:" + request.responseText + "\n"
							+ "error:" + error);
				}
			});


		return newRoadAddr+"^"+lon + "^" + lat;
	
}




function getDistance(startX, startY, endX, endY) {


	$.ajax({
		method : "GET",
		url : "https://apis.openapi.sk.com/tmap/routes/distance?version=1&format=json&callback=result",//
		async : false,
		data : {
			"appKey" : "${appKey}",
			"startX" : startX,
			"startY" : startY,
			"endX" : endX,
			"endY" : endY,
			"reqCoordType" : "WGS84GEO"
		},
		success : function(response) {

			//console.log(response);

			var distance = response.distanceInfo.distance;

			$("#result").text("두점의 직선거리 : " + distance + "m");
		},
		error : function(request, status, error) {
			console.log("code:" + request.status + "\n"
					+ "message:" + request.responseText + "\n"
					+ "error:" + error);
		}
	});




	
}












function bidCancel(crbId,status){



	$.confirm("해당 입찰을 취소 하시겠습니까?(이 동작은 취소 할 수 없습니다.)",function(a){
		
		if(a){

			$.ajax({ 
	    		type: 'post' ,
	    		url : "/bid/update-bid-status.do" ,
	    		dataType : 'json' ,
	    		data : {
	    			crbId : crbId,
	    			crbStatus : status
	    		},
	    		success : function(data, textStatus, jqXHR)
	    		{
	    			if(data.resultCode == "0000"){
	    				$.alert("입찰을 취소 했습니다.",function(a){
	    					window.location.reload();
		    			});
	    			}else{
	    				$.alert("입찰을 취소  하는데 실패 했습니다. 관리자에게 문의 하세요.",function(a){
	    					window.location.reload();
		    			});
	    				
	    			}
	    		} ,
	    		error : function(xhRequest, ErrorText, thrownError) {
	    		}
	    	}); 
			
		}
	});

	


	
}






function getDeviceLocation(){
	 
	 var latlng = "";
		try{
			if(window.Android != null){
				latlng = window.Android.getDeviceLocation("${kakaoId}");
				//alert(latlng);
			}	
		}catch(exception){
			
		}finally{
			
		}
		return latlng; 
	 
}



function nextProcess(corId,crbId,finishStatus){


	if(finishStatus == "N"){
		$.confirm("해당 경매건에 대해 탁송을 진행 하시겠습니까?",function(a){
			if(a){
				homeLoader.show();
				document.location.href = "/allocation/consignRequestProcess.do?corId="+corId+"&crbId="+crbId;
			}
		});
	}else{
		homeLoader.show();
		document.location.href = "/allocation/consignRequestProcess.do?corId="+corId+"&crbId="+crbId;
	}
	

	
	
}



function backKeyController(str){
	
	homeLoader.show();
	prevPageConfirm();
	
}


	

	
    </script>

	<div
		class="content-container withads themedetails companydetails writing pb100 homeadvisordetails">

		<header class="clearfix nb">
			<div class="search-icon">
				<!-- <a style="cursor:pointer;" onclick="javascript:history.go(-1);" ><img src="/img/back-icon.png" alt=""></a> -->
				<!-- <a style="cursor:pointer;" href="/menu/menu.do" ><img src="/img/back-icon.png" alt=""></a> -->
				<a style="cursor: pointer;" onclick="javascript:prevPageConfirm();"><img
					src="/img/back-icon-2.png" alt=""></a>
			</div>
			<div class="page-title txt-medium">고객 및 낙찰 정보</div>
			<div class="menu-bar pull-right">

				<!-- <a href="#"><img src="/img/open-icon.png" alt=""></a> -->
				<!-- <a href="/adviser/adviser.do"><img src="/img/home-pink-icon.png" alt=""></a> -->
				<!-- <a href="/allocation/main.do"><img src="/img/delete-x-icon.png" alt=""></a> -->
				<a onclick="javascript:prevPageConfirm();"><img
					src="/img/delete-x-icon-1.png" alt=""></a>

			</div>
		</header>

		<div class="company-details-container">
			<div class="allocation-views">
				<!-- <ul id="category">
                        <li class="active" onclick="javascript:changeCategory(this,'base');"><a>기본정보입력</a></li>
                        <li onclick="javascript:changeCategory(this,'car');"><a>차량정보입력</a></li>
                        <li onclick="javascript:changeCategory(this,'location');"><a>주소정보입력</a></li>
                        <li onclick="javascript:changeCategory(this,'confirm');"><a>내용확인</a></li>
                        <li onclick="javascript:changeCategory(this,'request');"><a>경매요청</a></li>
                    </ul> -->
				<!-- <ul id="category">
                        <li class="active"><a>기본정보입력</a></li>
                        <li><a>차량정보입력</a></li>
                        <li><a>주소정보입력</a></li>
                        <li><a>내용확인</a></li>
                        <li><a>경매요청</a></li>
                    </ul> -->
			</div>

			<div class="divider black"></div>

			<div class="heading-area"
				style="text-align: center; background-image: none; padding-top: 5px;">
				<a href="#" class="addme"> <img style="border-radius: 100%;"
					src="${member.mem_photo}" alt="">
				</a>
			</div>


			<div id="total" style="margin-top: 10%;">


				<div id="baseInfo">
					<div style="padding: 3%;">
						<div
							style="width: 100%; overflow: hidden; border: 1px solid #eaeaea; border-bottom: none;">
							<div style="float: left; width: 50%;">
								<div class="certNum" style="margin-top: 5%; width: 100%;">
									<div style="text-align: right;">
										<div style="text-align: left; margin-left: 2.5%;">
											<span class="d-tbc" style="font-weight: bold;">고객명</span>
										</div>
										<p
											style="width: 90%; padding: 0%; margin: 0%; margin-top: 5%;"
											onfocus="">${member.mem_user_name}</p>
									</div>
								</div>
							</div>
							<div style="float: left; width: 50%;">
								<div class="certNum" style="margin-top: 5%; width: 100%;">
									<div style="text-align: right;">
										<div style="text-align: left; margin-left: 2.5%;">
											<span class="d-tbc" style="font-weight: bold;">연락처
												(터치시 전화연결)</span>
										</div>
										<p
											style="width: 90%; padding: 0%; margin: 0%; margin-top: 5%;"
											onfocus="">
											<a href="tel:${member.mem_phone}">${member.mem_phone}</a>
										</p>
									</div>
								</div>
							</div>
						</div>
						<div
							style="width: 100%; overflow: hidden; border: 1px solid #eaeaea; border-top: none; border-bottom: none; margin-top: 0%;">
							<div class="certNum" style="margin-top: 5%; width: 100%;">
								<div style="text-align: right;">
									<div style="text-align: left; margin-left: 2.5%;">
										<span style="color: #F00;"></span> <span class="d-tbc"
											style="font-weight: bold;">제목</span>
									</div>
									<p style="width: 90%; padding: 0%; margin: 0%; margin-top: 5%;"
										onfocus="">${consignRequest.cor_title}</p>
								</div>
							</div>
						</div>
						<div
							style="width: 100%; overflow: hidden; border: 1px solid #eaeaea; border-top: none; border-bottom: none; margin-top: 0%;">
							<div style="float: left; width: 50%;">
								<div class="certNum" style="margin-top: 5%; width: 100%;">
									<div style="text-align: right;">
										<div style="text-align: left; margin-left: 2.5%;">
											<span class="d-tbc" style="font-weight: bold;">탁송방식</span>
										</div>
										<p
											style="width: 90%; padding: 0%; margin: 0%; margin-top: 5%;"
											onfocus="">${consignRequest.carrier_type_desc}</p>
									</div>
								</div>
							</div>
							<div style="float: left; width: 50%;">
								<div class="certNum" style="margin-top: 5%; width: 100%;">
									<div style="text-align: right;">
										<div style="text-align: left; margin-left: 2.5%;">
											<span class="d-tbc" style="font-weight: bold;">차량대수</span>
										</div>
										<p
											style="width: 90%; padding: 0%; margin: 0%; margin-top: 5%;"
											onfocus="">${consignRequest.cor_car_cnt}</p>
									</div>
								</div>
							</div>
						</div>
						<div
							style="width: 100%; overflow: hidden; padding-bottom: 3%; border: 1px solid #eaeaea; border-top: none; margin-top: 0%;">
							<div style="float: left; width: 50%;">
								<div class="certNum" style="margin-top: 5%; width: 100%;">
									<div style="text-align: right;">
										<div style="text-align: left; margin-left: 2.5%;">
											<span style="color: #F00;"></span> <span class="d-tbc"
												style="font-weight: bold;">낙찰금액</span>
										</div>
										<p
											style="width: 90%; padding: 0%; margin: 0%; margin-top: 5%;"
											onfocus="">${consignRequestBid.crb_amount}</p>
									</div>
								</div>
							</div>
							<%-- <div style="float: left; width: 50%;" >
											<div class="certNum" style="margin-top: 5%; width: 100%;">
												<div style="text-align:right;">
													<div style="text-align: left; margin-left: 2.5%;">
													<span style="color: #F00;"></span> <span class="d-tbc" style="font-weight: bold;">낙찰금액</span>
												</div>
													<p style="width: 90%; padding: 0%; margin: 0%;" onfocus="">${consignRequestBid.crb_amount}원</p>
												</div>
											</div>
										</div> --%>
						</div>
					</div>
				</div>



				<div class="add-ticker-container ">
					<div class="divider"></div>
					<div class="ticker-list-container content-box"
						style="text-align: center;">

						<div class="ticker-list" style="text-align: center;">

							<c:if test="${consignRequest.cor_status != 'SS'}">
								<div class="productsinuse" style="">
									<div class="certNum product-use-list"
										style="margin-top: 10%; width: 100%;">
										<div class="product-use-item"
											style="text-align: center; border: none;">
											<a
												onclick="javascript:nextProcess('${paramMap.corId}','${data.crb_id}','N');"
												class="product-use-search inlined"
												style="width: 100px; position: static; line-height: 50px;">탁송진행</a>
										</div>
									</div>
								</div>
							</c:if>
							<c:if test="${consignRequest.cor_status == 'SS'}">
								<div class="productsinuse" style="">
									<div class="certNum product-use-list"
										style="margin-top: 10%; width: 100%;">
										<div class="product-use-item"
											style="text-align: center; border: none;">
											<a
												onclick="javascript:nextProcess('${paramMap.corId}','${data.crb_id}','Y');"
												class="product-use-search inlined"
												style="width: 100px; position: static; line-height: 50px;">탁송완료</a>
										</div>
									</div>
								</div>
							</c:if>




						</div>
					</div>



				</div>









			</div>


		</div>
	</div>




	<script src="/js/vendor/jquery-3.2.1.min.js"></script>
	<!-- <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script> -->
	<script src="/js/vendor/bootstrap.min.js"></script>
	<!-- veiwport for countnumber -->
	<script src="/js/jquery.viewportchecker.js"></script>
	<script src="/js/main.js"></script>
	<script src="/js/alert.js"></script>
	<script
		src="https://apis.openapi.sk.com/tmap/jsv2?version=1&appKey=${appKey}"></script>
	<script src="/js/vendor/jquery.loading-indicator.min.js"></script>

	<script>

    $(document).ready(function(){
    
    	homeLoader = $('body').loadingIndicator({
			useImage: false,
			showOnInit : false
		}).data("loadingIndicator");
	
    	//homeLoader.show();
    	//homeLoader.hide();
    	var scroll_top=$(document).scrollTop();
    	$(".loading-indicator-wrapper").css("top",scroll_top);
    	$(".loading-indicator-wrapper").css("height","100%");
    	
    });

    

    

    </script>
</body>
</html>
