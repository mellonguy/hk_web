<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko">
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>

<body>

	<script type="text/javascript">  
  
function kakaoLogin(){

	/* document.location.href = "https://kauth.kakao.com/oauth/authorize?client_id=e3a057034847accf088ffd0b3ad83801&redirect_uri=http://localhost:8080/kakao/kakaoLogin.do&response_type=code"; */

	//아래는 테스트 키 이므로 변경 해야함... 컨트롤러도 변경 해야 함....

	//document.location.href = "https://kauth.kakao.com/oauth/authorize?client_id=63f484a87cbebd51c62a6c6c93501a01&redirect_uri=http://localhost:8091/kakao/kakaoLogin.do&response_type=code&auth_type=reauthenticate";
	document.location.href = "https://kauth.kakao.com/oauth/authorize?client_id=63f484a87cbebd51c62a6c6c93501a01&redirect_uri=${Redirect_URI}&response_type=code&auth_type=reauthenticate";
	
}
    
</script>


	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

	<div class="content-container">
		<header class="bg-blue clearfix">
			<div class="search-icon">
				<a href="#"></a>
			</div>
			<div class="menu-bar pull-right">
				<!-- <a style="cursor:pointer;" href="/adviser/adviser.do"><img src="/img/close-white-icon.png" alt=""></a> -->
				<!-- <a style="cursor:pointer;" onclick="javascript:history.go(-1);"><img src="/img/close-white-icon.png" alt=""></a> -->
			</div>
		</header>
		<div class="menu-container bg-blue">
			<span class="img-holder"> <img src="/img/user-img.png" alt="">
			</span> <a class="kakao txt-bold" style="cursor: pointer;"
				onclick="javascript:kakaoLogin();"> 카카오계정으로 로그인 </a>
		</div>
		<!-- <div class="main-menu-container">
                <a href="primary.do" class="menu-link">
                    <img src="/img/coin-icon.png" alt="">
                    <span class="text">포인트충전</span>
                </a>
                <a href="notification.do" class="menu-link">
                    <img src="/img/gear-icon.png" alt="">
                    <span class="text">알림설정</span>
                </a>
                <a href="roboadvisor.do" class="menu-link">
                    <img src="/img/list-icon.png" alt="">
                    <span class="text">이용중 상품</span>
                </a>
                <a href="widgetsetting.do" class="menu-link">
                    <img src="/img/layer-icon.png" alt="">
                    <span class="text">위젯설정</span>
                </a>
                <a href="coupon.do" class="menu-link">
                    <img src="/img/cut-icon.png" alt="">
                    <span class="text">쿠폰함</span>
                </a>
                <a href="support.do" class="menu-link">
                    <img src="/img/support-icon.png" alt="">
                    <span class="text">고객지원센터</span>
                </a>
            </div> -->
	</div>

	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
	<script src="/js/vendor/bootstrap.min.js"></script>
	<!-- veiwport for countnumber -->
	<script src="/js/jquery.viewportchecker.js"></script>
	<script src="/js/main.js"></script>
</body>
</html>
