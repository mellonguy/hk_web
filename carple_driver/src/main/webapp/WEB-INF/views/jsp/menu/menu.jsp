<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko">
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
<body>

	<script type="text/javascript">  
var homeLoader;
var deviceToken = "${dvrDeviceKey}";
$(document).ready(function(){  

	try {

		if(window.Android != null && (typeof deviceToken == "undefined" || deviceToken == "")){
    		deviceToken = getDeviceToken();
    		
			if(typeof deviceToken == "undefined" || deviceToken == ""){
				window.location.reload();
			}

			if(window.Android != null && deviceToken != ""){
				updateDeviceToken(deviceToken,"aos");
				pageMoveStop = false;
			}
			
    	}else{
    	
    	}
	}catch(exception){
	    
	}finally {
		
	}
	
	
  
});




function getDeviceToken(){
	
	var token = "";
	try{
		if(window.Android != null){
			token = window.Android.getDeviceToken("test");
		}else if(window.webkit.messageHandlers != null){
			window.webkit.messageHandlers.scriptHandler.postMessage("getDeviceToken");
		}
	}catch(exception){

		
	}finally{
		
	}
	return token;
}  

$(window).on("scroll", function(){ 
	var scroll_top=$(this).scrollTop();
	$(".loading-indicator-wrapper").css("top",scroll_top);
	$(".loading-indicator-wrapper").css("height","100%");
});




function logout(){

	$.confirm("로그아웃 하시겠습니까?",function(a){
		if(a){

			try {
				if(window.GerionJrpr != null){
					window.GerionJrpr.logoutForApp("");
				}
				if(window.external != null){
		            window.external.logoutForPC("");
		        }
			}catch(exception){
			    
			}finally {
				document.location.href = "/login/logout.do";	
			}
		}


	});

}
    

function loginForApp(userId){
	window.GerionJrpr.loginForApp(userId, "");
}



function logoutForApp(){
	window.GerionJrpr.logoutForApp("");
}    


var resultData;
var dataLength;


function getApiData(){

/* 
	$.alert("testtest",function(a){

	});
 */

 $.alert("시작",function(a){

	 $.ajax({ 
			type: 'post' ,
			url : "/menu/getapidata.do" ,
			dataType : 'json' ,
			async : true,
			data : {
				
			},
			success : function(data, textStatus, jqXHR)
			{

				resultData = data.resultData;
				dataLength = resultData.length;

				startInterval(dataLength);
				
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});

});

	

	
}


var intervalVal;

function startInterval(max){


	intervalVal = setInterval( function() {

		var randomVal = Math.floor((Math.random()*max));
		var resultStr = "["+resultData[randomVal].accType+"]\n";
		resultStr += resultData[randomVal].accDate +"\t"+resultData[randomVal].accHour+"\n"
		resultStr += resultData[randomVal].roadNM +"\n"
		resultStr += resultData[randomVal].startEndTypeCode+"\n";
		resultStr += resultData[randomVal].accPointNM +"\n";
		
		$.alert(resultStr,function(a){
					
		});
			
	}, 10000);


	
}


function stopInterval(){


	clearInterval(intervalVal);
	$.alert("중지",function(a){
		
	});
	
}


function goMyPage(){


	document.location.href = "/menu/mypage.do";

}


function movePage(addr){

	homeLoader.show();
	document.location.href = addr;
	
}


function backKeyController(str){
	
	//document.location.href="/carrier/group-list.do?allocationId=${paramMap.groupId}&allocationStatus=Y";
	if(window.Android != null){
		window.Android.toastShort("뒤로가기 버튼으로 앱을 종료 할 수 없습니다.");
	}
	
	
}



function updateDeviceToken(dvrDeviceKey,dvrLoginEnv){


	if(dvrDeviceKey != null && dvrDeviceKey != ""){
		
		$.ajax({ 
			type: 'post' ,
			url : "/driver/updateDvrDeviceKey.do" ,
			dataType : 'json' ,
			async : true,
			data : {
				dvrDeviceKey : dvrDeviceKey,
				dvrLoginEnv : dvrLoginEnv
			},
			success : function(data, textStatus, jqXHR)
			{
				//$.alert("success");
				resultData = data.resultData;
			} ,
			error : function(xhRequest, ErrorText, thrownError) {

				//$.alert("fail");
				
			}
		});

	}

}

    
</script>



	<div class="content-container">
		<header class="bg-blue clearfix">
			<div class="search-icon">
				<a href="#"></a>
			</div>
			<div class="menu-bar pull-right">
				<!-- <a style="cursor:pointer;" href="/adviser/adviser.do"><img src="/img/close-white-icon.png" alt=""></a> -->
				<!-- <a style="cursor:pointer;" onclick="javascript:history.go(-1);"><img src="/img/close-white-icon.png" alt=""></a> -->
			</div>
		</header>
		<div class="menu-container bg-blue">
			<span class="img-holder"> <img src="${thumbnail}" alt="">
			</span>
			<div class="qoute-emotion-holder">
				<span class="qoute txt-bold"> 안녕하세요. <br> ${driverName}님<br>
					잡아! 에 오신것을 환영 합니다.
				</span> <span class="emotion txt-bold"> <%-- ${usernick} --%>
				</span>
			</div>

			<div class="clearfix">
				<!-- <span class="label-text">보유포인트</span> -->
				<%-- <a href="purchase-history.do" style="width:80%;">
                        <span class="points roboto">
                            ${userPoint}
                            <img src="/img/arrow-white-right-icon.png" alt="">
                        </span>
                    </a> --%>
				<a onclick="javascript:logout();" class="log-out">로그아웃</a>
			</div>
		</div>
		<div class="main-menu-container">
			<!-- <a href="/allocation/main.do" class="menu-link"> -->
			<a onclick="javascript:movePage('/allocation/main.do')"
				class="menu-link"> <img src="/img/list-icon.png" alt=""> <span
				class="text">차량탁송</span>
			</a>

			<!-- <a href="getapidata.do" class="menu-link"> -->
			<a onclick="javascript:getApiData();" class="menu-link"> <img
				src="/img/coin-icon.png" alt=""> <span class="text">한국도로공사
					API 호출</span>
			</a>
			<!-- <a href="notification.do" class="menu-link"> -->
			<a onclick="javascript:stopInterval();" class="menu-link"> <img
				src="/img/gear-icon.png" alt=""> <span class="text">정지</span>
			</a> <a onclick="javascript:movePage('/menu/mypage.do');"
				class="menu-link"> <img src="/img/gear-icon.png" alt=""> <span
				class="text">마이페이지</span>
			</a>

			<!-- <a href="widgetsetting.do" class="menu-link">
                    <img src="/img/layer-icon.png" alt="">
                    <span class="text">위젯설정</span>
                </a>
                <a href="coupon.do" class="menu-link">
                    <img src="/img/cut-icon.png" alt="">
                    <span class="text">쿠폰함</span>
                </a>
                <a href="support.do" class="menu-link">
                    <img src="/img/support-icon.png" alt="">
                    <span class="text">고객지원센터</span>
                </a> -->
		</div>
	</div>




	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
	<script src="/js/vendor/bootstrap.min.js"></script>
	<!-- veiwport for countnumber -->
	<script src="/js/jquery.viewportchecker.js"></script>
	<script src="/js/alert.js"></script>
	<script src="/js/main.js"></script>
	<script src="/js/vendor/jquery.loading-indicator.min.js"></script>
	<script>

    $(document).ready(function(){
    
    	homeLoader = $('body').loadingIndicator({
			useImage: false,
			showOnInit : false
		}).data("loadingIndicator");
	
    	//homeLoader.show();
    	//homeLoader.hide();
    	var scroll_top=$(document).scrollTop();
    	$(".loading-indicator-wrapper").css("top",scroll_top);
    	$(".loading-indicator-wrapper").css("height","100%");

    	
    });


    </script>
</html>
