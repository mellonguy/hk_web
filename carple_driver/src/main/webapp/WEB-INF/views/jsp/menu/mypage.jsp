<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko">
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>

<body class="">

	<script>

var homeLoader;
$(document).ready(function(){  

	try {
		if(window.GerionJrpr != null){
			window.GerionJrpr.loginForApp("${kakaoId}", "");
		}
		if(window.external != null){
	        window.external.loginForPC("${kakaoId}", "");
	    }
	}catch(exception){
	    
	}finally {
		
	}
	
	
	
  
});

$(window).on("scroll", function(){ 
	var scroll_top=$(this).scrollTop();
	$(".loading-indicator-wrapper").css("top",scroll_top);
	$(".loading-indicator-wrapper").css("height","100%");
});


function backKeyController(str){
	
	homeLoader.show();
	document.location.href="/menu/menu.do";
	
}


function driverMod(obj,str){

	$.confirm("회원 정보를 수정 하시겠습니까?",function(a){

		if(a){
			homeLoader.show();
			document.location.href="/login/driver-mod.do";
		}
		
	});
	
}



</script>



	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

	<div class="content-container homeadvisordetails writing ">
		<header class="clearfix">
			<div class="search-icon">
				<a
					onclick="javascript:homeLoader.show(); document.location.href='/menu/menu.do';"><img
					src="/img/back-icon-2.png" alt=""></a>
			</div>
			<div class="page-title txt-medium">마이페이지</div>
			<div class="menu-bar pull-right">
				<a
					onclick="javascript:homeLoader.show(); document.location.href='/menu/menu.do';"><img
					src="/img/home-blue-icon.png" alt=""></a>
			</div>
		</header>

		<div class="divider black"></div>


		<!-- content-container black homeadvisordetails -->
		<div style="padding: 5%;">


			<div class="heading-area"
				style="text-align:center; background:url(${driver_server_path}${driver_thumb_image_path}<c:forEach var="data" items="${driverPic}" varStatus="status"><c:if test="${data.drf_category_type eq 'background' }">${data.drf_file_path}</c:if></c:forEach>) no-repeat; background-size:100% 100%; background-position:center;">
				<!-- <div class="heading-area" style=""> -->
				<!--  <span class="lbl">주락펴락 시그널 3호</span>
	                <span class="desc">
	                    거래량, 볼린저밴드, RSI, 주가이평비교 등 <br>
	                    5개의 척도로 단기성 중소형주를 발굴합니다.
	                </span> -->
				<a href="#" class="addme"> <img style="border-radius: 100%;"
					src="${driver_server_path}${driver_thumb_image_path}<c:forEach var="data" items="${driverPic}" varStatus="status"><c:if test="${data.drf_category_type eq 'profile' }">${data.drf_file_path}</c:if></c:forEach>"
					alt="">
				</a>
				<div class="productsinuse" style="display: inline-block;">
					<div class="certNum product-use-list"
						style="margin-top: 10%; width: 100%;">
						<div class="product-use-item"
							style="text-align: center; border: none; position: static;">
							<%-- <a onclick="javascript:bidAccept('${data.cor_id}','${data.crb_id}','Y');" class="product-use-search inlined" style="line-height:22px; right:11%; width:100px; top:83%; color:#FFF; border:1px solid #1477ff;">사진편집</a> --%>
						</div>
					</div>
				</div>

				<div class="no-result content-box">
					<div class="notfound" style="margin-top: 0px;">
						<span
							style="font-size: 20px; margin-bottom: 0px; font-weight: bold;">${driver.dvr_user_name}
							기사님</span>
					</div>
				</div>
				<div class="no-result content-box">
					<div class="notfound" style="margin-top: 0px;">
						<span style="font-size: 13px; margin-bottom: 20px;">${driver.dvr_car_kind}</span>
					</div>
				</div>
				<div class="no-result content-box"
					style="text-align: center; border-bottom: 1px solid #1477ff;">
					<div class="notfound"
						style="width: 100%; text-align: center; margin-top: 0px;">
						<div style="width: 60%; text-align: center; margin: 0 auto;">${driver.dvr_simple_introduce}</div>
					</div>
					<div class="productsinuse"
						style="display: inline-block; position: relative; top: -41px; right: -160px;">
						<div class="certNum product-use-list"
							style="margin-top: 10%; width: 100%;">
							<div class="product-use-item"
								style="text-align: center; border: none; position: static;">
								<!-- <img src="/img/gear-icon.png" alt="" style="width:25px; height:25px;"> -->
							</div>
						</div>
					</div>
				</div>

				<div style="width: 100%; overflow: hidden; border: none;">
					<div style="float: left; width: 50%;">
						<div class="certNum" style="margin-top: 5%; width: 100%;">
							<div style="text-align: right;">
								<div style="text-align: left; margin-left: 2.5%;">
									<span class="d-tbc" style="font-weight: bold;">기사명</span>
								</div>
								<p style="width: 90%; padding: 0%; margin-top: 10%;" onfocus="">${driver.dvr_user_name}</p>
							</div>
						</div>
					</div>
					<div style="float: left; width: 50%;">
						<div class="certNum" style="margin-top: 5%; width: 100%;">
							<div style="text-align: right;">
								<div style="text-align: left; margin-left: 2.5%;">
									<span class="d-tbc" style="font-weight: bold;">연락처</span>
								</div>
								<p style="width: 90%; padding: 0%; margin-top: 10%;" onfocus="">${driver.dvr_phone}</p>
							</div>
						</div>
					</div>
				</div>

				<div style="width: 100%; overflow: hidden; border: none;">
					<div style="float: left; width: 50%;">
						<div class="certNum" style="margin-top: 5%; width: 100%;">
							<div style="text-align: right;">
								<div style="text-align: left; margin-left: 2.5%;">
									<span class="d-tbc" style="font-weight: bold;">차종</span>
								</div>
								<p style="width: 90%; padding: 0%; margin-top: 10%;" onfocus="">${driver.dvr_car_kind}</p>
							</div>
						</div>
					</div>
					<div style="float: left; width: 50%;">
						<div class="certNum" style="margin-top: 5%; width: 100%;">
							<div style="text-align: right;">
								<div style="text-align: left; margin-left: 2.5%;">
									<span class="d-tbc" style="font-weight: bold;">차량번호</span>
								</div>
								<p style="width: 90%; padding: 0%; margin-top: 10%;" onfocus="">${driver.dvr_car_num}</p>
							</div>
						</div>
					</div>
				</div>






			</div>









		</div>

		<div class="product-use-list">
			<c:forEach var="data" items="${couponList}" varStatus="status">
				<div class="product-use-item">
					<div class="product-use-info inlined">
						<span class="name">${data.coupon_name}</span> <span class="name">${data.coupon_use_possible}<c:if
								test="${data.coupon_use_kind eq 'C' }">회</c:if>
							<c:if test="${data.coupon_use_kind eq 'P' }">일</c:if> 사용권
						</span> <span class="date">${data.reg_dt_str} ~
							${data.expire_dt_str} 사용가능</span>
					</div>
					<a href="/adviser/adviserRecommendItem.do"
						class="product-use-search inlined">사용</a>
				</div>
			</c:forEach>

			<!--  <div class="product-use-item">
                    <div class="product-use-info inlined">
                        <span class="name">[S등급] 알파시그널 3호</span>
                        <span class="date">2018/05/25~2018/06/24</span>
                    </div>
                    <a href="#" class="product-use-search inlined">검색</a>
                </div>
                <div class="product-use-item">
                    <div class="product-use-info inlined">
                        <span class="name">[S등급] 알파시그널 3호</span>
                        <span class="date">2018/05/25~2018/06/24</span>
                    </div>
                    <a href="#" class="product-use-search inlined">검색</a>
                </div>
                <div class="product-use-item">
                    <div class="product-use-info inlined">
                        <span class="name">[S등급] 알파시그널 3호</span>
                        <span class="date">2018/05/25~2018/06/24</span>
                    </div>
                    <a href="#" class="product-use-search inlined">검색</a>
                </div>
                <div class="product-use-item">
                    <div class="product-use-info inlined">
                        <span class="name">[S등급] 알파시그널 3호</span>
                        <span class="date">2018/05/25~2018/06/24</span>
                    </div>
                    <a href="#" class="product-use-search inlined">검색</a>
                </div>
                <div class="product-use-item">
                    <div class="product-use-info inlined">
                        <span class="name">[S등급] 알파시그널 3호</span>
                        <span class="date">2018/05/25~2018/06/24</span>
                    </div>
                    <a href="#" class="product-use-search inlined">검색</a>
                </div> -->

		</div>

		<div class="fixed-bottomarea">
			<div style="width: 100%; display: inline-block;">
				<a href="javascript:driverMod(this,'')">정보 수정</a>
			</div>
		</div>

	</div>



	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
	<script src="/js/vendor/bootstrap.min.js"></script>
	<!-- veiwport for countnumber -->
	<script src="/js/jquery.viewportchecker.js"></script>
	<script src="/js/main.js"></script>
	<script src="/js/alert.js"></script>
	<script src="/js/vendor/jquery.loading-indicator.min.js"></script>
	<script>

    $(document).ready(function(){
    
    	homeLoader = $('body').loadingIndicator({
			useImage: false,
			showOnInit : false
		}).data("loadingIndicator");
	
    	//homeLoader.show();
    	//homeLoader.hide();
    	var scroll_top=$(document).scrollTop();
    	$(".loading-indicator-wrapper").css("top",scroll_top);
    	$(".loading-indicator-wrapper").css("height","100%");
    	
    });

    

    

    </script>
</body>
</html>
