<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko">
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>

<body class="bg-gray">
	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

	<div class="content-container pointsused">
		<header class="clearfix">
			<div class="search-icon">
				<a style="cursor: pointer;" onclick="javascript:history.go(-1);"><img
					src="/img/back-icon.png" alt=""></a>
			</div>
			<div class="page-title txt-medium">포인트 사용 내역</div>
			<div class="menu-bar pull-right">
				<a href="/adviser/adviser.do"><img src="/img/home-pink-icon.png"
					alt=""></a>
			</div>
		</header>
		<div class="pointsused-box">
			<span class="heading">포인트 사용 내역</span>
			<div class="lbl-box clearfix">
				<c:if test="${fn:length(pointHistoryList) > 0}">
					<span class="pull-left">사용 내역/날짜</span>
					<span class="pull-right text-right">포인트</span>
				</c:if>
			</div>
			<div class="pointsused-list-box">
				<c:forEach var="data" items="${pointHistoryList}" varStatus="status">
					<div class="item">
						<div class="box">
							<span class="name">${data.contents }</span> <span class="date">${data.reg_dt_str}</span>
						</div>
						<div class="box text-right">
							<c:if test="${data.paid_point >= 0}">
								<span class="points red">+${data.paid_point}P</span>
							</c:if>
							<c:if test="${data.paid_point < 0}">
								<span class="points blue"> ${data.paid_point}P</span>
							</c:if>
						</div>
					</div>
				</c:forEach>
				<!-- <div class="item">
                        <div class="box">
                            <span class="name">1차 목표가 확인 1회권</span>
                            <span class="date">2018/04/26</span>
                        </div>
                        <div class="box text-right">
                            <span class="points blue">- 100P</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="box">
                            <span class="name">포인트 충전</span>
                            <span class="date">2018/04/26</span>
                        </div>
                        <div class="box text-right">
                            <span class="points red">+ 1000P</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="box">
                            <span class="name">1차 목표가 확인 1회권</span>
                            <span class="date">2018/04/26</span>
                        </div>
                        <div class="box text-right">
                            <span class="points blue">- 100P</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="box">
                            <span class="name">1차 목표가 확인 1회권</span>
                            <span class="date">2018/04/26</span>
                        </div>
                        <div class="box text-right">
                            <span class="points blue">- 100P</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="box">
                            <span class="name">1차 목표가 확인 1회권</span>
                            <span class="date">2018/04/26</span>
                        </div>
                        <div class="box text-right">
                            <span class="points blue">- 100P</span>
                        </div>
                    </div> -->
			</div>
		</div>
	</div>


	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
	<script src="/js/vendor/bootstrap.min.js"></script>
	<!-- veiwport for countnumber -->
	<script src="/js/jquery.viewportchecker.js"></script>
	<script src="/js/main.js"></script>
</body>
</html>
