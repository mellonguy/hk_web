<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko">
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>

<body class="bg-gray">
	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

	<div class="content-container productsinuse">
		<header class="clearfix">
			<div class="search-icon">
				<a href="/menu/menu.do"><img src="/img/back-icon.png" alt=""></a>
			</div>
			<div class="page-title txt-medium">쿠폰함</div>
			<div class="menu-bar pull-right">
				<a href="/adviser/adviser.do"><img src="/img/home-pink-icon.png"
					alt=""></a>
			</div>
		</header>
		<!-- <div class="header-links clearfix">
                <ul>
                    <li  class="active"><a href="/menu/roboadvisor.do">로보어드바이저</a></li>
                    <li><a href="/menu/confirmrecomm.do">추천가 확인</a></li>
                    <li>
                        <a href="/menu/premiumsignalnotif.do">
                            <span>프리미엄</span>  
                            <span>신호알림</span>  
                        </a>
                    </li>
                </ul>
            </div> -->
		<div class="divider"></div>
		<div class="product-use-list">
			<c:forEach var="data" items="${couponList}" varStatus="status">
				<div class="product-use-item">
					<div class="product-use-info inlined">
						<span class="name">${data.coupon_name}</span> <span class="name">${data.coupon_use_possible}<c:if
								test="${data.coupon_use_kind eq 'C' }">회</c:if>
							<c:if test="${data.coupon_use_kind eq 'P' }">일</c:if> 사용권
						</span> <span class="date">${data.reg_dt_str} ~
							${data.expire_dt_str} 사용가능</span>
					</div>
					<a href="/adviser/adviserRecommendItem.do"
						class="product-use-search inlined">사용</a>
				</div>
			</c:forEach>

			<!--  <div class="product-use-item">
                    <div class="product-use-info inlined">
                        <span class="name">[S등급] 알파시그널 3호</span>
                        <span class="date">2018/05/25~2018/06/24</span>
                    </div>
                    <a href="#" class="product-use-search inlined">검색</a>
                </div>
                <div class="product-use-item">
                    <div class="product-use-info inlined">
                        <span class="name">[S등급] 알파시그널 3호</span>
                        <span class="date">2018/05/25~2018/06/24</span>
                    </div>
                    <a href="#" class="product-use-search inlined">검색</a>
                </div>
                <div class="product-use-item">
                    <div class="product-use-info inlined">
                        <span class="name">[S등급] 알파시그널 3호</span>
                        <span class="date">2018/05/25~2018/06/24</span>
                    </div>
                    <a href="#" class="product-use-search inlined">검색</a>
                </div>
                <div class="product-use-item">
                    <div class="product-use-info inlined">
                        <span class="name">[S등급] 알파시그널 3호</span>
                        <span class="date">2018/05/25~2018/06/24</span>
                    </div>
                    <a href="#" class="product-use-search inlined">검색</a>
                </div>
                <div class="product-use-item">
                    <div class="product-use-info inlined">
                        <span class="name">[S등급] 알파시그널 3호</span>
                        <span class="date">2018/05/25~2018/06/24</span>
                    </div>
                    <a href="#" class="product-use-search inlined">검색</a>
                </div> -->
		</div>
	</div>


	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
	<script src="/js/vendor/bootstrap.min.js"></script>
	<!-- veiwport for countnumber -->
	<script src="/js/jquery.viewportchecker.js"></script>
	<script src="/js/main.js"></script>
</body>
</html>
