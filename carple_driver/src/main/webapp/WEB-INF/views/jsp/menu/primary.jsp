<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko">
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>

<body class="bg-gray">

	<script type="text/javascript">  
  
function goPurchasePage(){

	document.location.href = "/menu/purchase-history.do";	
	
}
    
    
    
    
    
    
function purchasePoint(reason,point){
	
	//alert(reason);
	//alert(point);
	if(confirm(point+" 포인트를 구매 하시겠습니까?")){
		
		document.location.href = "/payment/order_pay.do?reason="+reason+"&point="+point;
		
		//document.location.href = "/menu/purchase-point.do?reason="+reason+"&point="+point;	
	}else{
		alert("구매가 취소 되었습니다.");
	}
	
	
	
}    
    
    
</script>

	<div class="content-container purchasepoints">
		<header class="clearfix">
			<div class="search-icon">
				<a style="cursor: pointer;" onclick="javascript:history.go(-1);"><img
					src="/img/back-icon.png" alt=""></a>
			</div>
			<div class="page-title txt-medium">포인트 충전</div>
			<div class="menu-bar pull-right">
				<a href="/adviser/adviser.do"><img src="/img/home-pink-icon.png"
					alt=""></a>
			</div>
		</header>
		<div class="points-box-holder">
			<div class="points-box">
				<span class="lbl">보유포인트</span> <span
					class="points roboto txt-medium">${userPoint}</span> <span
					class="pointsused" onclick="javascript:goPurchasePage();">포인트사용내역</span>
			</div>
		</div>
		<div class="content-box">
			<div class="primary-goal-items purchase-points">
				<div class="item">
					<div class="img-holder-box">
						<span class="img-holder"><img src="/img/100p.png" alt=""></span>
					</div>
					<div class="ticket txt-bold">
						<span>100P</span>
					</div>
					<a onclick="javascript:purchasePoint('P','100');" class="btn-price">9,900
						원</a>
				</div>
				<div class="item">
					<div class="img-holder-box">
						<span class="img-holder"><img src="/img/300p.png" alt=""></span>
					</div>
					<div class="ticket txt-bold">
						<span>300P</span>
					</div>
					<a onclick="javascript:purchasePoint('P','300');" class="btn-price">29,900
						원</a>
				</div>
				<div class="item">
					<div class="img-holder-box">
						<span class="img-holder"><img src="/img/500p.png" alt=""></span>
					</div>
					<div class="ticket txt-bold">
						<span>500P</span>
					</div>
					<a onclick="javascript:purchasePoint('P','500');" class="btn-price">49,900
						원</a>
				</div>
				<div class="item">
					<div class="img-holder-box">
						<span class="img-holder"><img src="/img/1100p.png" alt=""></span>
					</div>
					<div class="ticket txt-bold">
						<span>1000P + 100P</span>
					</div>
					<a onclick="javascript:purchasePoint('P','1100');"
						class="btn-price">99,900 원</a>
				</div>
				<div class="item ribboned">
					<div class="img-holder-box">
						<span class="img-holder"><img src="/img/2200p.png" alt=""></span>
					</div>
					<div class="ticket txt-bold">
						<span>2000P + 200P</span>
					</div>
					<a onclick="javascript:purchasePoint('P','2200');"
						class="btn-price">199,000 원</a>
				</div>
				<div class="item">
					<div class="img-holder-box">
						<span class="img-holder"><img src="/img/3300p.png" alt=""></span>
					</div>
					<div class="ticket txt-bold">
						<span>3000P + 300P</span>
					</div>
					<a onclick="javascript:purchasePoint('P','3300');"
						class="btn-price">299,000 원</a>
				</div>
				<div class="item">
					<div class="img-holder-box">
						<span class="img-holder"><img src="/img/6000p.png" alt=""></span>
					</div>
					<div class="ticket txt-bold">
						<span>5000P + 1000P</span>
					</div>
					<a onclick="javascript:purchasePoint('P','6000');"
						class="btn-price">499,000 원</a>
				</div>
				<div class="item">
					<div class="img-holder-box">
						<span class="img-holder"><img src="/img/13000p.png" alt=""></span>
					</div>
					<div class="ticket txt-bold">
						<span>10000P + 3000P</span>
					</div>
					<a onclick="javascript:purchasePoint('P','13000');"
						class="btn-price">999,000 원</a>
				</div>
			</div>
		</div>
	</div>


	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
	<script src="/js/vendor/bootstrap.min.js"></script>
	<!-- veiwport for countnumber -->
	<script src="/js/jquery.viewportchecker.js"></script>
	<script src="/js/main.js"></script>
</body>
</html>
