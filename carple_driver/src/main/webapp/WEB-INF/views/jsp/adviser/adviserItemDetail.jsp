<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
<title>종목발굴 일지</title>
<link href="/css/layout.css" rel="stylesheet">
<script src="/js/vendor/jquery-1.11.2.min.js"></script>
</head>
<body onload="getCalendar();">
	<input type="hidden" id="year">
	<input type="hidden" id="month">
	<!-- wrap -->
	<div id="wrap">
		<!-- header -->
		<header id="header">
			<div class="head">
				<h1 class="subtitle">종목발굴 일지</h1>
				<a onclick="javascript:history.go(-1); return false;"
					class="h_btn back"><span class="blind">뒤로</span></a> <a
					href="/adviser/adviser.do" class="h_btn r"><span class="blind">홈</span></a>
			</div>
		</header>
		<!-- //header -->
		<!-- container -->
		<section class="container" id="container">
			<div id="calendar"></div>
		</section>
	</div>
	<!-- //wrap -->

</body>
<script type="text/javascript">
	var today = new Date();    
	var year = today.getFullYear();   
	var month = today.getMonth();  
 
	document.getElementById("year").value = year;  
	document.getElementById("month").value = month+1; 
 
 	function getCalendar(){
		var yy = document.getElementById("year").value;  
  		var mm = document.getElementById("month").value;
  		var calendar = document.getElementById("calendar"); // 나중에 출력할 곳 div태그
  		
  		var html = '<div class="calendar_top"><div class="inner">';
   			html += '<button type="button" class="btn_pn prev" onclick="prevMonth()"><span class="blind">이전</span></button>';
   			html += '<strong class="date">'+ yy + "." + (mm < 10 ? '0' + mm : mm) + "</strong>";
   			html += '<button type="button" class="btn_pn next" onclick="nextMonth()"><span class="blind">다음</span></button>';
   			html += '</div></div>';
   			html += '<div class="calendar_w">';
		   	html += '<table class="tbl_calendar">';
		   	html += '<thead><tr>';
		   	html += "<th>일</th><th>월</th><th>화</th><th>수</th><th>목</th><th>금</th><th>토</th>";  
		   	html += '</tr></thead>';
   			html += '<tbody><tr>';
   
	   	var w = new Date(yy,mm-1,1).getDay();
	   	for( var i = 0 ; i < w ; i++){
	   		html += "<td>&nbsp;</td>";
	   	}
      
		var d = [31,28,31,30,31,30,31,31,30,31,30,31]; 
   			d[1] = (yy%400==0 || yy%4==0 && yy%100!=0) ? 29 : 28;
   			
   			//위 코드까지 윤달.... 아래에서 하면 되네..
   			$.ajax({ 
   				type: 'post' ,
   				url : "/adviser/getAdviserItemDetail.do" ,
   				dataType : 'json' ,
   				data : {
   					adviserCd : '${adviser.adviser_cd}',
   					createdAt : yy+"-"+(mm < 10 ? '0' + mm : mm)
   				},
   				success : function(data, textStatus, jqXHR)
   				{
   					
   					if(data.resultCode == "0000"){
   					
   						var result = "";
   						var resultData = data.resultData;
   						
   						for( i = 1 ; i <= d[mm-1] ; i++ ){
   							var tmpVal = '<td><span class="day">' + i +'</span></td>';
   							if(resultData.length > 0){
   	   							for(var j = 0; j < resultData.length; j++){
   	   								if((yy+"-"+(mm < 10 ? '0' + mm : mm)+"-"+(i < 10 ? '0' + i : i)) == resultData[j].created_at_str){
   	   									tmpVal = '<td onclick="javascript:goDetailPage(\''+(yy+"-"+(mm < 10 ? '0' + mm : mm)+"-"+(i < 10 ? '0' + i : i))+'\')"><span class="day point">' + i +'<span class="percent">'+resultData[j].per+'%</span></span></td>';
   	   									
   	   							//	result += '<a style="cursor:pointer;"   onclick="javascript:getPersonInChargeInfo(\''+list[i].person_in_charge_id+'\',\''+list[i].name+'\',\''+list[i].phone_num+'\',\''+list[i].address+'\');" class="result-sub"><span>'+list[i].name+'</span></a>';
   	   									
   	   									
   	   									
   	   								}
   	   								if((yy+"-"+(mm < 10 ? '0' + mm : mm)+"-"+(i < 10 ? '0' + i : i)) == resultData[j].created_at_str && (yy+"-"+(mm < 10 ? '0' + mm : mm)+"-"+(i < 10 ? '0' + i : i)) == resultData[j].now_str){
   	   									tmpVal = '<td onclick="javascript:goDetailPage(\''+(yy+"-"+(mm < 10 ? '0' + mm : mm)+"-"+(i < 10 ? '0' + i : i))+'\')"><span class="day point on">' + i +'<span class="percent">'+resultData[j].per+'%</span></span></td>';
   	   								}
   	   							}	
   	   						}else{
   	   						  
   	   						}   							
   							html += tmpVal;
   					    	if(new Date(yy,mm-1,i).getDay() == 6){  
   					     		html += '</tr>';
   						     	if(i != d[d-1]){ 
   						      		html += '<tr>';
   						     	}
   							}
   						}
   						
   						w = new Date(yy,mm,1).getDay(); 
   						if(w != 0){
   							for(var i = w ; i <= 6 ; i++){ 
   				 				html += '<td>&nbsp;</td>';
   							}
   						}
   						html += '</tr>';
   						html += "</tbody></table>";
   				  		calendar.innerHTML = html;
   						
   					}else{
   						alert("조회 실패");	
   					}
   					
   				} ,
   				error : function(xhRequest, ErrorText, thrownError) {
   				}
   			}); 
   	
 	}
 	
 	function prevMonth() {
		var yy = document.getElementById("year").value;
		var mm = document.getElementById("month").value;
	  	mm--;
	  	if(mm <= 0){
	   		mm = 12;
	   		yy--;
	  	}
	  	document.getElementById("year").value = yy;
	 	document.getElementById("month").value = mm;
	    getCalendar();
	 }
	function nextMonth() {
  		var yy = document.getElementById("year").value;
  		var mm = document.getElementById("month").value;
  		mm++;
  		if(mm > 12){
   			mm = 1;
   			yy++;
  		}
  		document.getElementById("year").value = yy;
     	document.getElementById("month").value = mm;
     	getCalendar();
	}
	
	
	function goDetailPage(date){
		
		//alert('${adviser.adviser_cd}');
		document.location.href = "/adviser/eventLog.do?adviserCd=${adviser.adviser_cd}&createdAt="+date;
	}
	
	
	
</script>
</html>
