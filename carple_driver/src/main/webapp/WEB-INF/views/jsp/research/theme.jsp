<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko">
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>

<body class="black">


	<script type="text/javascript">  

$(document).ready(function(){
	
	//getNewsData("${searchWord}");
	
});


 function getNewsData(obj,keyword){
	
	 
	 $('#selectedStatus').find("li").each(function(index,element) {
    		$(this).removeClass('selected');
    	});
    	$(obj).parent().addClass('selected');
	 
	 
/*	 var cnt = 0;
        if($(obj).parent().find("a").html() == "전체"){
  
   	    	$('#selectedStatus').find("li").each(function(index,element) {
           		$(this).removeClass('selected');
           	});
   	    	$(obj).parent().addClass('selected');
        }else{
  
        	$('#selectedStatus').find("li").each(function(index,element) {
        		 if($(this).find("a").html() == "전체"){
        			 $(this).removeClass('selected');	 
        		 }
        	});
        	if($(obj).parent().hasClass('selected')){
		        $(obj).parent().removeClass('selected');
		        $('#selectedStatus').find("li").each(function(index,element) {
	    			if($(this).hasClass("selected") === true){
	    					cnt++;
	    			}
	    		 });
	        	if(cnt == 0){
	        		$('#selectedStatus').find("li").each(function(index,element) {
		           		 if($(this).find("a").html() == "전체"){
		           			 $(this).addClass('selected');	 
		           		 }
	           		});
	        	}    
		    }else{
		    	$(obj).parent().addClass('selected');
		    }
        	
        }
        
	*/ 
	 
	 
/* 	  if($(obj).hasClass('selected')){
	        $(obj).removeClass('selected');
	    }else{ 
	        if($(obj).parent().find("a").html() == "전체"){
	        	alert(1);
	        	$('#selectedStatus').find("li").each(function(index,element) {
	        		$(this).removeClass('selected');
	        	});
	        }else{
	        	alert(2);
	        	$('#selectedStatus').find("li").each(function(index,element) {
	        		 if($(this).find("a").html() == "전체"){
	        			 $(this).removeClass('selected');	 
	        		 }else{
	        			 		 
	        		 }
	        	});
	        }
	        $(obj).addClass('selected');
	     } 
 */	
	    setTimeout( function() {
	    	getNewsDataSub(obj,keyword);
        }, 100);
	    
} 
 
 var userCategoryList = new Array();
 function getNewsDataSub(obj,keyword){
 
 
		userCategoryList.length = 0;
		$('#selectedStatus').find("li").each(function(index,element) {
			
			if($(this).hasClass("selected") === true){
				if($(this).find("a").html() != ""){
					var categoryObject = new Object();
					categoryObject.category_name = $(this).find("a").html()	
					userCategoryList.push(categoryObject);
				}	
			}
		 });

//	 alert(userCategoryList.length);
 
		$.ajax({ 
			type: 'post' ,
			url : "/research/newsSearch.do",
			dataType : 'json' ,
			data : {
				userCategoryList : JSON.stringify({userCategoryList : userCategoryList})
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				var str = "";
				if(result == "0000"){

					if(resultData.length > 0){
						$("#newsList").html("");
						for(var i = 0; i < resultData.length; i++){
							
							if(resultData[i].thumbnailUrl != ""){
								//썸네일 있을때
								str += '<div class="news" linkUrl="\''+resultData[i].link+'\'" onclick="javascript:goNewsPage(\''+resultData[i].link+'\');">';
								str += '<div class="news-content">';
								str += '<span class="news-title">';
								str += resultData[i].title;
								str += '</span>';
								str += '<div class="news-category">';
								str += '<span class="category">'+resultData[i].author+'</span>';
								str += '<span class="timepost">'+resultData[i].pubdate+'</span>';
								str += '</div>';
								str += '</div>';
								str += '<img src=\''+resultData[i].thumbnailUrl+'\' alt="">';
								str += '</div>';
							}else{
								str += '<div class="news" linkUrl="\''+resultData[i].link+'\'" onclick="javascript:goNewsPage(\''+resultData[i].link+'\');">';
								str += '<div class="news-content noimg">';
								str += '<span class="news-title">';
								str += resultData[i].title;
								str += '</span>';
								str += '<div class="news-category">';
								str += '<span class="category">'+resultData[i].author+'</span>';
								str += '<span class="timepost">'+resultData[i].pubdate+'</span>';
								str += '</div>';
								str += '</div>';
								str += '</div>';
							}
						}	
						$("#newsList").append(str);
					}else{
						
					}
					
				}
				
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
 
 }
 
 
 

 function goNewsPage(uri){
		
		//var replaceUrlStr = replaceAll(url, "?", "^");
		//replaceUrlStr = replaceAll(replaceUrlStr, "=", "+");
		
		var replaceUrlStr = encodeURIComponent(uri)
		document.location.href = "/news/newsPage.do?prevPage=theme&newsUri="+replaceUrlStr;
	}  


 
 
 
 function goSearch(){

		
		document.location.href = "/search/search.do";
		
		
	}
 
    
</script>


	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
	<div
		class="content-container black interest-page loadingthemepage news">
		<header
			style="background-color: #1e212a; position: fixed; top: 0px; z-index: 10000;">
			<div class="menu-bar">
				<a href="/menu/menu.do"><img src="/img/bar-icon.png" alt=""></a>
			</div>
			<div class="notif-icon">
				<a href="#"><img src="/img/bell2-icon.png" alt=""></a>
			</div>
			<div class="search-container">
				<a href="#" class="search-input-icon"> <img
					src="/img/search-input-icon.png" alt="">
				</a> <input type="text" name="" id="" onclick="javascript:goSearch();"
					placeholder="종목 혹은 테마를 검색해주세요">
			</div>
		</header>
		<div class="moveTop">
			<a href="#"> <img src="/img/arrow-top-icon.png" alt=""> TOP
			</a>
		</div>
		<div class="view-option" style="margin-top: 55px;">
			<ul>
				<li class="option interest "><a href="/interest/interest.do">시세알림</a>
				</li>
				<li class="option advisor"><a href="/adviser/adviser.do">로보어드바이저</a>
				</li>
				<li class="option active research "><a
					href="/research/theme.do">테마</a></li>
			</ul>
		</div>
		<div class="cat-list">
			<ul class="clearfix">
				<li><a href="/research/theme.do" class="active">뉴스</a></li>
				<li><a href="/research/emphasis.do">장전강세테마</a></li>
				<li><a href="/research/themeRank.do">테마 순위</a></li>
				<!-- <li><a href="#">스크리너</a></li> -->
			</ul>
		</div>
		<div class="view-category">
			<a href="/research/keywords.do" class="plus"><img
				src="/img/plus-red-icon.png" alt=""></a>
			<div class="cactegory-list-container">
				<ul id="selectedStatus">
					<li class="selected"><a
						onclick="javascript:getNewsData(this,'${searchWord}');">전체</a></li>
					<%-- <c:forEach var="data" items="${newsCategoryList}" varStatus="status">
							<li>
                            	<a onclick="javascript:getNewsData(this,'${data.news_category_name}');">${data.news_category_name}</a>
                      		</li>		
						</c:forEach> --%>
					<c:forEach var="data" items="${userCategoryList}"
						varStatus="status">
						<li><a
							onclick="javascript:getNewsData(this,'${data.category_name}');">${data.category_name}</a>
						</li>
					</c:forEach>


					<!-- <li>
                            <a href="#">코스피</a>
                        </li>
                        <li>
                            <a href="#">코스닥</a>
                        </li>
                        <li>
                            <a href="#">삼성전자</a>
                        </li>
                        <li>
                            <a href="#">IT</a>
                        </li> -->
				</ul>
			</div>
		</div>
		<div class="searchresult">
			<div class="recent-searches suggestion-box">
				<div class="suggestion">
					<div class="news-list" id="newsList">
						<c:forEach var="data" items="${feedList}" varStatus="status">
							<c:if test="${data.thumbnailUrl != ''}">
								<div class="news" linkUrl="${data.link}"
									onclick="javascript:goNewsPage('${data.link}');">
									<div class="news-content">
										<span class="news-title"> ${data.title} </span>
										<div class="news-category">
											<span class="category">${data.author}</span> <span
												class="timepost">${data.pubdate}</span>
										</div>
									</div>
									<img src="${data.thumbnailUrl}" alt="">
								</div>
							</c:if>
							<c:if test="${data.thumbnailUrl == ''}">
								<div class="news" linkUrl="${data.link}"
									onclick="javascript:goNewsPage('${data.link}');">
									<div class="news-content noimg">
										<span class="news-title"> ${data.title} </span>
										<div class="news-category">
											<span class="category">${data.author}</span> <span
												class="timepost">${data.pubdate}</span>
										</div>
									</div>
								</div>
							</c:if>
						</c:forEach>

						<!--     <div class="news">
                                <div class="news-content">
                                    <span class="news-title">
                                        [집단행동 나선 중소소상공인]
                                        주물공업동조합, 생산중단 오늘 결론
                                    </span>
                                    <div class="news-category">
                                        <span class="category">아시아경제</span>
                                        <span class="timepost">38분전</span>
                                    </div>
                                </div>
                                <img src="/img/default-img.png" alt="">
                            </div>
                             -->





						<!--  <div class="news">
                                <div class="news-content noimg">
                                    <span class="news-title">
                                            [집단행동 나선 중소소상공인] 주물공 <br>
                                            업동조합, 생산중단 오늘 결론                                            
                                    </span>
                                    <div class="news-category">
                                        <span class="category">아시아경제</span>
                                        <span class="timepost">38분전</span>
                                    </div>
                                </div>
                            </div>
                            <div class="news">
                                <div class="news-content">
                                    <span class="news-title">
                                        [집단행동 나선 중소소상공인]
                                        주물공업동조합, 생산중단 오늘 결론
                                    </span>
                                    <div class="news-category">
                                        <span class="category">아시아경제</span>
                                        <span class="timepost">38분전</span>
                                    </div>
                                </div>
                                <img src="/img/news-pic.png" alt="">
                            </div>
                            <div class="news">
                                <div class="news-content noimg">
                                    <span class="news-title">
                                        [집단행동 나선 중소소상공인] 주물공 <br>
                                        업동조합, 생산중단 오늘 결론  
                                    </span>
                                    <div class="news-category">
                                        <span class="category">아시아경제</span>
                                        <span class="timepost">38분전</span>
                                    </div>
                                </div>
                            </div> -->
					</div>
				</div>
			</div>
		</div>

	</div>


	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
	<script src="/js/vendor/bootstrap.min.js"></script>
	<!-- veiwport for countnumber -->
	<script src="/js/main.js"></script>
</body>
</html>
