<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko">
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>

<body class="black">


	<script type="text/javascript">  

$(document).ready(function(){
	
	//getNewsData("${searchWord}");
	
});


 function getNewsData(obj,keyword){
	
	 var cnt = 0;
        if($(obj).parent().find("a").html() == "전체"){
   	    	$('#selectedStatus').find("li").each(function(index,element) {
           		$(this).removeClass('selected');
           	});
   	    	$(obj).addClass('selected');
        }else{
        	$('#selectedStatus').find("li").each(function(index,element) {
        		 if($(this).find("a").html() == "전체"){
        			 $(this).removeClass('selected');	 
        		 }
        	});
        	if($(obj).hasClass('selected')){
		        $(obj).removeClass('selected');
		        $('#selectedStatus').find("li").each(function(index,element) {
	    			if($(this).hasClass("selected") === true){
	    					cnt++;
	    			}
	    		 });
	        	if(cnt == 1){
	        		$('#selectedStatus').find("li").each(function(index,element) {
		           		 if($(this).find("a").html() == "전체"){
		           			 $(this).addClass('selected');	 
		           		 }
	           		});
	        	}    
		    }else{
		    	$(obj).addClass('selected');
		    }
        	
        }
        
	 
	 
	 
/* 	  if($(obj).hasClass('selected')){
	        $(obj).removeClass('selected');
	    }else{ 
	        if($(obj).parent().find("a").html() == "전체"){
	        	alert(1);
	        	$('#selectedStatus').find("li").each(function(index,element) {
	        		$(this).removeClass('selected');
	        	});
	        }else{
	        	alert(2);
	        	$('#selectedStatus').find("li").each(function(index,element) {
	        		 if($(this).find("a").html() == "전체"){
	        			 $(this).removeClass('selected');	 
	        		 }else{
	        			 		 
	        		 }
	        	});
	        }
	        $(obj).addClass('selected');
	     } 
 */	
	    setTimeout( function() {
	    	getNewsDataSub(obj,keyword);
        }, 100);
	    
} 
 
 var userCategoryList = new Array();
 function getNewsDataSub(obj,keyword){
 
 
		userCategoryList.length = 0;
		$('#selectedStatus').find("li").each(function(index,element) {
			
			if($(this).hasClass("selected") === true){
				if($(this).find("a").html() != ""){
					var categoryObject = new Object();
					categoryObject.category_name = $(this).find("a").html()	
					userCategoryList.push(categoryObject);
				}	
			}
		 });

	 //alert(userCategoryList.length);
 
		$.ajax({ 
			type: 'post' ,
			url : "/research/newsSearch.do",
			dataType : 'json' ,
			data : {
				userCategoryList : JSON.stringify({userCategoryList : userCategoryList})
			},
			success : function(data, textStatus, jqXHR)
			{
				var result = data.resultCode;
				var resultData = data.resultData;
				var str = "";
				if(result == "0000"){

					if(resultData.length > 0){
						$("#newsList").html("");
						for(var i = 0; i < resultData.length; i++){
							
							if(resultData[i].thumbnailUrl != ""){
								//썸네일 있을때
								str += '<div class="news">';
								str += '<div class="news-content" linkUrl="\''+resultData[i].link+'\'" onclick="javascript:goNewsPage(\''+resultData[i].link+'\');">';
								str += '<span class="news-title">';
								str += resultData[i].title;
								str += '</span>';
								str += '<div class="news-category">';
								str += '<span class="category">'+resultData[i].author+'</span>';
								str += '<span class="timepost">'+resultData[i].pubdate+'</span>';
								str += '</div>';
								str += '</div>';
								str += '<img src=\''+resultData[i].thumbnailUrl+'\' alt="">';
								str += '</div>';
							}else{
								str += '<div class="news">';
								str += '<div class="news-content noimg" linkUrl="\''+resultData[i].link+'\'" onclick="javascript:goNewsPage(\''+resultData[i].link+'\');">';
								str += '<span class="news-title">';
								str += resultData[i].title;
								str += '</span>';
								str += '<div class="news-category">';
								str += '<span class="category">'+resultData[i].author+'</span>';
								str += '<span class="timepost">'+resultData[i].pubdate+'</span>';
								str += '</div>';
								str += '</div>';
								str += '</div>';
							}
						}	
						$("#newsList").append(str);
					}else{
						
					}
					
				}
				
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		});
 
 }
 
 
 

 function goNewsPage(uri){
		
		//var replaceUrlStr = replaceAll(url, "?", "^");
		//replaceUrlStr = replaceAll(replaceUrlStr, "=", "+");
		
		var replaceUrlStr = encodeURIComponent(uri)
		document.location.href = "/news/newsPage.do?newsUri="+replaceUrlStr;
	}  


 
 
 
 function goSearch(){

		
		document.location.href = "/search/search.do";
		
		
	}
 
   
 
 function goThemeDetail(themeCd){
	 
	alert(themeCd);	 
	 
 }
 
 
 function selectInterestItem(itemCd,marketCd,obj){
		
		$.ajax({ 
			type: 'post' ,
			url : "/interest/selectInterestItem.do" ,
			dataType : 'json' ,
			data : {
				itemCd : itemCd,
				marketCd : marketCd
			},
			success : function(data, textStatus, jqXHR)
			{
				if(data.resultCode == "0000"){
					insertInterestItem(itemCd,marketCd,obj);
				}else{
					deleteInterestItem(itemCd,marketCd,obj);
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		}); 
		
		
	}    
	    
	function insertInterestItem(itemCd,marketCd,obj){
		
		$.ajax({ 
			type: 'post' ,
			url : "/interest/insertInterestItem.do" ,
			dataType : 'json' ,
			data : {
				itemSrtCd : itemCd,
				marketCd : marketCd
			},
			success : function(data, textStatus, jqXHR)
			{
				if(data.resultCode == "0000"){
					$(obj).addClass('heart-full');
				}else if(data.resultCode == "E002"){
						alert("로그인 되지 않음");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		}); 
	}    
	    
	  

	function deleteInterestItem(itemCd,marketCd,obj){
		
		$.ajax({ 
			type: 'post' ,
			url : "/interest/deleteInterestItem.do" ,
			dataType : 'json' ,
			data : {
				itemSrtCd : itemCd,
				marketCd : marketCd
			},
			success : function(data, textStatus, jqXHR)
			{
				if(data.resultCode == "0000"){
					$(obj).removeClass('heart-full');
				}else if(data.resultCode == "E002"){
						alert("로그인 되지 않음");
				}
			} ,
			error : function(xhRequest, ErrorText, thrownError) {
			}
		}); 
	}    

 
 
 
</script>


	<div class="content-container black interest-page themedetails">
		<header class="clearfix nb">
			<div class="search-icon">
				<a style="cursor: pointer;" onclick="javascript:history.go(-1);"><img
					src="/img/back-icon.png" alt=""></a>
			</div>
			<div class="page-title txt-medium white">${theme.theme_name}</div>
			<div class="menu-bar pull-right">
				<a href="#"><img src="/img/open-icon.png" alt=""></a> <a
					href="/adviser/adviser.do"><img src="/img/home-pink-icon.png"
					alt=""></a>
			</div>
		</header>
		<div class="headerarea">
			<a href="#" class="absolute">주락펴락 시그널 3호</a> <a href="#"
				class="text-right">5G 테마 관련 4종목 발굴</a>
		</div>
		<div class="result-list">
			<c:forEach var="data" items="${themeItemList}" varStatus="status">
				<div class="desc-box">
					<div class="interest-item">
						<div class="interest-info">
							<span class="name">${data.item_name}</span>
						</div>
						<c:if test="${data.cmpprevddPrc >= 0}">
							<div class="value red">${data.trdPrc}</div>
							<div class="percent-value red">
								<span class="percent">${data.cmpprevddPer}%</span> <span
									class="value up">${data.cmpprevddPrc}</span>
							</div>
						</c:if>
						<c:if test="${data.cmpprevddPrc < 0}">
							<div class="value blue">${data.trdPrc}</div>
							<div class="percent-value blue">
								<span class="percent">${data.cmpprevddPer}%</span> <span
									class="value down">${data.cmpprevddPrc}</span>
							</div>
						</c:if>
						<div class="bell-holder">
							<c:if test="${data.interest_item_cd != null}">
								<a
									onclick="javascript:selectInterestItem('${data.item_srt_cd}','${data.market_cd}',this)"
									class="heart heart-full"></a>
							</c:if>
							<c:if test="${data.interest_item_cd == null}">
								<a
									onclick="javascript:selectInterestItem('${data.item_srt_cd}','${data.market_cd}',this)"
									class="heart"></a>
							</c:if>
						</div>

					</div>
					<div class="contentArea">
						<div class="otherBox" style="padding-left: 0px;">
							<span>강세순위 1위</span> <span class="red">97점</span>
						</div>
						<p class="fs23">${data.company_summary}</p>
					</div>
				</div>
			</c:forEach>

			<!--  <div class="desc-box">
                    <div class="interest-item">
                        <div class="interest-info">
                            <span class="name">케이엠더블유</span>
                        </div>
                        <div class="value red">
                            1,910
                        </div>
                        <div class="percent-value red">
                            <span class="percent">+300.85%</span>
                            <span class="value up">200,750</span>
                        </div>
                        <div class="bell-holder">
                            <a href="#" class="heart"></a>
                        </div>
                    </div>
                    <div class="contentArea">
                        <div class="otherBox">
                            <span>강세순위 1위</span>
                            <span class="red">97점</span>
                        </div>
                        <p class="fs23">
                            동사는 안테나 및 RF 설계기술과 LED 조명
                            기술을 융합하여 차별화된 조명 제품을 개발
                            함으로써 차세대 광원으로 주목 받고 있는
                            LED조명 사업 및 LED 조명 응용분야에서
                            확고한 기반을 구축할 계획임 
                        </p>
                    </div>
                </div>
                <div class="desc-box">
                    <div class="interest-item">
                        <div class="interest-info">
                            <span class="name">대한광통신</span>
                        </div>
                        <div class="value blue">
                            300,000
                        </div>
                        <div class="percent-value blue">
                            <span class="percent">+300.85%</span>
                            <span class="value down">200,750</span>
                        </div>
                        <div class="bell-holder">
                            <a href="#" class="heart"></a>
                        </div>
                    </div>
                    <div class="contentArea">
                        <div class="otherBox">
                            <span>강세순위 1위</span>
                            <span class="red">97점</span>
                        </div>
                        <p class="fs23">
                            국내 통신장비 시장에서 동사 및 종속회사는
                            국내 주요 이동통신사업자인 SK텔레콤, KT,
                            LGU+를 주요 고객으로 하고 있음
                        </p>
                    </div>                   
                </div>
                <div class="desc-box">
                    <div class="interest-item">
                        <div class="interest-info">
                            <span class="name">케이엠더블유</span>
                        </div>
                        <div class="value red">
                            1,910
                        </div>
                        <div class="percent-value red">
                            <span class="percent">+300.85%</span>
                            <span class="value up">200,750</span>
                        </div>
                        <div class="bell-holder">
                            <a href="#" class="heart"></a>
                        </div>
                    </div>
                    <div class="contentArea">
                        <div class="otherBox">
                            <span>강세순위 1위</span>
                            <span class="red">97점</span>
                        </div>
                        <p class="fs23">
                            지역별로는 본사를 거점으로 한국 및 CE, IM
                            부문 산하 해외 9개 지역총괄과 DS부문 산하
                            해외 5개 지역총괄의 생산, 판매법인 등
                            276개의 동종업종을 영위하는 종속기업으로
                            구성되어 있음
                        </p>
                    </div>      

                </div> -->
		</div>
	</div>












	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
	<script src="/js/vendor/bootstrap.min.js"></script>
	<!-- veiwport for countnumber -->
	<script src="/js/main.js"></script>
</body>
</html>
