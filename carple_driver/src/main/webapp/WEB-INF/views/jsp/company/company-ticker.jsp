<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
<body class="black">

	<script type="text/javascript">
 
 
 	$(document).ready(function(){
		
		//getNewsData("${searchWord}");
		
	}); 
 
 	var rgx1 = /\D/g;  // /[^0-9]/g 와 같은 표현
 	var rgx2 = /(\d+)(\d{3})/; 

 	
 	function setComma(inNum){
 	     
 	     var outNum;
 	     outNum = inNum; 
 	     while (rgx2.test(outNum)) {
 	          outNum = outNum.replace(rgx2, '$1' + ',' + '$2');
 	      }
 	     return outNum;

 	}
 	
 	function comma(str) {
 	    str = String(str);
 	    return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
 	}
 	
 	function getNumber(obj){
 		
 	     var num01;
 	     var num02;
 	     num01 = obj.value;
 	     num02 = num01.replace(rgx1,"");
 	      num01 = setComma(num02); 
 	     //num01 = num02;
 	     obj.value =  num01;
 	     
 	}	
 
 	
 	
 function removeComma(str){
	 
		n = parseInt(str.replace(/,/g,""));
		return n;
		
	}

 	
 	
  function setAmount(val){
	  
	  if(val != 0){
		  var value = removeComma($("#trdPrc").html());
		  $("#priceOrder").val(comma(parseInt(Number(value * (1 + val / 100)))));  
	  }
	 
	  
  }
 
 
 
 function priceOrderPlus(){
	 
	 var marketCd = "${marketCd}";
	 
	 if($("#priceOrder").val() != ""){
		 var val = removeComma($("#priceOrder").val());
		 
		 if(marketCd == "kospi"){
			 if(val < 1000){
				 val += 1;
			 }else if(val >= 1000 && val < 5000){
				 val += 5;
			 }else if(val >= 5000 && val < 10000){
				 val += 10;
			 }else if(val >= 10000 && val < 50000){
				 val += 50;
			 }else if(val >= 50000 && val < 100000){
				 val += 100;
			 }else if(val >= 100000 && val < 500000){
				 val += 500;
			 }else if(val >= 500000){
				 val += 1000;
			 } 
		 }else{
			 if(val < 1000){
				 val += 1;
			 }else if(val >= 1000 && val < 5000){
				 val += 5;
			 }else if(val >= 5000 && val < 10000){
				 val += 10;
			 }else if(val >= 10000 && val < 50000){
				 val += 50;
			 }else if(val >= 50000){
				 val += 100;
			 }
		 }
		 
	 	$("#priceOrder").val(comma(val));
	 }
	 
 }
 
 
 function priceOrderMinus(){
	 
	 var marketCd = "${marketCd}";
	 
	 if($("#priceOrder").val() != ""){
		 var val = removeComma($("#priceOrder").val());
		 if(marketCd == "kospi"){
			 if(val < 1000){
				 val -= 1;
			 }else if(val >= 1000 && val < 5000){
				 val -= 5;
			 }else if(val >= 5000 && val < 10000){
				 val -= 10;
			 }else if(val >= 10000 && val < 50000){
				 val -= 50;
			 }else if(val >= 50000 && val < 100000){
				 val -= 100;
			 }else if(val >= 100000 && val < 500000){
				 val -= 500;
			 }else if(val >= 500000){
				 val -= 1000;
			 } 
		 }else{
			 if(val < 1000){
				 val -= 1;
			 }else if(val >= 1000 && val < 5000){
				 val -= 5;
			 }else if(val >= 5000 && val < 10000){
				 val -= 10;
			 }else if(val >= 10000 && val < 50000){
				 val -= 50;
			 }else if(val >= 50000){
				 val -= 100;
			 }
		 }
	 	$("#priceOrder").val(comma(val));	 
	 }
	 
 }
 
 
 function deleteTargetPriceAlarm(targetPriceAlarmId,obj){
	 
	 
	 //alert(targetPriceAlarmId);
	 
	  $.ajax({ 
	 		type: 'post' ,
	 		url : "/company/deleteTargetPriceAlarm.do" ,
	 		dataType : 'json' ,
	 		data : {
	 			targetPriceAlarmId : targetPriceAlarmId
	 		},
	 		success : function(data, textStatus, jqXHR)
	 		{
	 			if(data.resultCode == "0000"){
	 				$(obj).parent().remove();
	 			}else if(data.resultCode == "E002"){
	 				//	alert("로그인 되지 않음");
	 			}
	 		} ,
	 		error : function(xhRequest, ErrorText, thrownError) {
	 		}
	 	});  
	 
 }
 
 
 function selectInterestItem(itemCd,marketCd,obj){
 	
	 if(obj == null){
		 obj = $("#forHeart"); 
	 }
	 	 
 	$.ajax({ 
 		type: 'post' ,
 		url : "/interest/selectInterestItem.do" ,
 		dataType : 'json' ,
 		data : {
 			itemCd : itemCd,
 			marketCd : marketCd
 		},
 		success : function(data, textStatus, jqXHR)
 		{
 			if(data.resultCode == "0000"){
 				insertInterestItem(itemCd,marketCd,obj);
 			}else{
 				insertTargetPriceAlarm(itemCd,marketCd,obj);
 			}
 		} ,
 		error : function(xhRequest, ErrorText, thrownError) {
 		}
 	}); 
 	
 	
 }    
     
 function insertInterestItem(itemCd,marketCd,obj){
 	
 	$.ajax({ 
 		type: 'post' ,
 		url : "/interest/insertInterestItem.do" ,
 		dataType : 'json' ,
 		data : {
 			itemSrtCd : itemCd,
 			marketCd : marketCd
 		},
 		success : function(data, textStatus, jqXHR)
 		{
 			if(data.resultCode == "0000"){
 				$(obj).addClass('heart-full');
 				insertTargetPriceAlarm(itemCd,marketCd,obj);
 			}else if(data.resultCode == "E002"){
 					alert("로그인 되지 않음");
 			}
 		} ,
 		error : function(xhRequest, ErrorText, thrownError) {
 		}
 	}); 
 }    
     
 
 function insertTargetPriceAlarm(itemCd,marketCd,obj){
	 
	 
	 //removeComma($("#trdPrc").html()); 현재가
	 //removeComma($("#priceOrder").val()) 설정가
	 
	 //alert(removeComma($("#trdPrc").html()));
	 //alert(removeComma($("#priceOrder").val()));
	 
	 var nowPrice = Number(removeComma($("#trdPrc").html()));
	 var targetPrice = Number(removeComma($("#priceOrder").val())); 
	 var achievementType = "";
	 
	 if(nowPrice == targetPrice){
		 alert("현재가와 지정가가 같습니다.");
		 return false;
	 }else{
		 if(targetPrice > nowPrice){
			 achievementType = "+";
		 }else{
			 achievementType = "-";
		 }
		 
		 	$.ajax({ 
		 		type: 'post' ,
		 		url : "/company/insertTargetPriceAlarm.do" ,
		 		dataType : 'json' ,
		 		data : {
		 			itemSrtCd : itemCd,
		 			marketCd : marketCd,
		 			targetPrice : removeComma($("#priceOrder").val()),
		 			achievementType : achievementType
		 		},
		 		success : function(data, textStatus, jqXHR)
		 		{
		 			if(data.resultCode == "0000"){
		 				alert("알림 목록에 추가 되었습니다.");
		 				var result = "";
		 				result += '<div class="ticker-item">';
		 				result += '<span>목표가</span>';
		 				result += '<span>'+$("#priceOrder").val()+'원 </span>';
		 				result += '<a onclick="javascript:deleteTargetPriceAlarm(\''+data.resultData.targetPriceAlarmId+'\',this);" class="delete"></a>';
		 				result += '</div>';
		 				$("#ticker-list").append(result);
		 				
		 			}else if(data.resultCode == "E002"){
		 				//	alert("로그인 되지 않음");
		 			}else if(data.resultCode == "E003"){
		 				alert("동일한 지정가가 이미 설정 되어 있습니다.");
		 			}
		 			
		 		} ,
		 		error : function(xhRequest, ErrorText, thrownError) {
		 		}
		 	});   
		 
	 }
	 
	
	 
	 
	 
 }
 
 
 
 function addAlarm(itemCd,marketCd){

	 if($("#priceOrder").val() == ""){
		 alert("지정가가 설정 되지 않았습니다.");
		 return false;
	 }else{
		 //관심종목이 아닌경우 관심종목으로 등록 한다.
		 selectInterestItem(itemCd,marketCd,null);		 
	 }
	 
 }
 
 
 function insertPremiumSignal(itemCd,marketCd){
	 
	 
	 if(Number("${userPoint}") < 490){
		 if(confirm("보유한 포인트가 부족 합니다. \r\n 포인트를 구매 화면으로 이동 하시겠습니까?")){
			 document.location.href = "/menu/primary.do";
		 }else{
			 return false;
		 }
	 }else{
		if(confirm("490P 를 사용하여 해당 서비스를 신청 하시겠습니까?")){
			
			$.ajax({ 
		 		type: 'post' ,
		 		url : "/company/insertPremiumSignal.do" ,
		 		dataType : 'json' ,
		 		data : {
		 			itemSrtCd : itemCd,
		 			marketCd : marketCd
		 		},
		 		success : function(data, textStatus, jqXHR)
		 		{
		 			if(data.resultCode == "0000"){
		 				alert("정상적으로 구매 되었습니다.");
		 			}else if(data.resultCode == "E002"){
		 					alert("로그인이 필요 합니다.");
		 					document.location.href = "/menu/menu.do";
		 			}else if(data.resultCode == "E003"){
		 					alert("포인트가 부족 합니다.");
		 					document.location.href = "/menu/primary.do";
		 			}else if(data.resultCode == "E004"){
		 					alert("이미 구매한 종목입니다.");
		 					return false;
		 			}
		 		} ,
		 		error : function(xhRequest, ErrorText, thrownError) {
		 		}
		 	});  
			
			
			
			
		}else{
			return false;
		}	 
	 }
	 
	 
	 
	 
	 
 }
 
 
 
   
 </script>

	<div
		class="content-container black themedetails pb60 tickernotification">
		<header class="clearfix nb">
			<div class="search-icon">
				<a style="cursor: pointer;" onclick="javascript:history.go(-1);"><img
					src="/img/back-icon.png" alt=""></a>
			</div>
			<div class="page-title txt-medium white">기업상세</div>
			<div class="menu-bar pull-right">
				<a href="#"><img src="/img/open-icon.png" alt=""></a> <a
					href="/adviser/adviser.do"><img src="/img/home-pink-icon.png"
					alt=""></a>
			</div>
		</header>
		<div class="company-details-container">
			<div class="company-name">
				<span class="name txt-bold">${itemMap.item_name}
					${itemMap.item_srt_cd}</span>
				<c:if test="${itemMap.interest_item_cd != null}">
					<a
						onclick="javascript:selectInterestItem('${itemMap.item_srt_cd}','${itemMap.market_cd}',this)"
						id="forHeart" class="heart heart-full"></a>
				</c:if>
				<c:if test="${itemMap.interest_item_cd == null}">
					<a
						onclick="javascript:selectInterestItem('${itemMap.item_srt_cd}','${itemMap.market_cd}',this)"
						id="forHeart" class="heart"></a>
				</c:if>
			</div>
			<div class="newsdetails">
				<div class="headerarea">
					<a href="#" class="absolute">주락펴락 시그널 3호</a> <a href="#"
						class="text-right">5G 테마 관련 4종목 발굴</a>
				</div>
			</div>
			<div class="company-views">
				<ul>
					<li class="active"><a
						href="company-ticker.do?itemSrtCd=${itemMap.item_srt_cd}&marketCd=${itemMap.market_cd}">시세알림설정</a></li>
					<li><a
						href="company.do?itemSrtCd=${itemMap.item_srt_cd}&marketCd=${itemMap.market_cd}">기업개요</a></li>
					<li><a
						href="company-news.do?itemSrtCd=${itemMap.item_srt_cd}&marketCd=${itemMap.market_cd}">뉴스</a></li>
					<li><a
						href="theme.do?itemSrtCd=${itemMap.item_srt_cd}&marketCd=${itemMap.market_cd}">테마</a></li>
					<li><a
						href="discussion.do?itemSrtCd=${itemMap.item_srt_cd}&marketCd=${itemMap.market_cd}">토론실</a></li>
				</ul>
			</div>
			<div class="overview-box active content-view">
				<div class="view-details">
					<div class="overview detail-box">
						<span class="price">현재가</span>
						<div class="price-info-box">

							<span
								class="<c:if test="${cmpprevddPrc >= 0}">price-val</c:if><c:if test="${cmpprevddPrc < 0}">price-val-blue</c:if>"
								id="trdPrc">${trdPrc}</span>
							<div
								class="<c:if test="${cmpprevddPrc >= 0}">percent-value</c:if><c:if test="${cmpprevddPrc < 0}">percent-value-blue</c:if>">
								<span><c:if test="${cmpprevddPrc >= 0}">+</c:if>${cmpprevddPer}%</span>
								<span class="up">${cmpprevddPrc}</span>
							</div>

							<%-- 
	                           
	                            	<span class=""  id="trdPrc">${trdPrc}</span>
                                   <div class="">
                                       <span>${cmpprevddPer}%</span>
                                       <span class="up">${cmpprevddPrc}</span>
                                   </div>
	                            --%>

							<%-- <c:if test="${cmpprevddTpCd == '1' || cmpprevddTpCd == '2'}">
                                   <span class="price-val"  id="trdPrc">${trdPrc}</span>
                                   <div class="percent-value">
                                       <span>+${cmpprevddPer}%</span>
                                       <span class="up">${cmpprevddPrc}</span>
                                   </div>
                                  </c:if>
                                  <c:if test="${cmpprevddTpCd == '3' || cmpprevddTpCd == '6' || cmpprevddTpCd == '7' || cmpprevddTpCd == '8' || cmpprevddTpCd == '9'}">
                                   <span class="price-val"  id="trdPrc">${trdPrc}</span>
                                   <div class="percent-value">
                                       <span>${cmpprevddPer}%</span>
                                       <span class="up">${cmpprevddPrc}</span>
                                   </div>
                                  </c:if>
                                  <c:if test="${cmpprevddTpCd == '4' || cmpprevddTpCd == '5'}">
                                   <span class="price-val-blue"  id="trdPrc">${trdPrc}</span>
                                   <div class="percent-value-blue">
                                       <span>-${cmpprevddPer}%</span>
                                       <span class="up">${cmpprevddPrc}</span>
                                   </div>
                                  </c:if> --%>
						</div>
					</div>
				</div>
				<div class="add-ticker-container white">
					<div class="add-ticker">
						<div class="ticker">
							<span>시세 알림 추가</span> <img src="/img/qm-icon.png" alt="">
						</div>
						<div class="note">
							<span>지정가격 도달 시 알림이 발송됩니다.</span> <span>최초 도달 후 한 시간 동안은
								알림이</span> <span>재발송되지 않습니다.</span>
							<!-- 매수가를 입력하면 실시간으로
                                <span>수익률을 알려드립니다.</span>  -->
						</div>
					</div>
					<!-- <div class="number-sheets">
                            <span class="head">매수가 입력</span>
                            <div class="label-input">
                                <input type="text" placeholder="매수가 입력">
                            </div>
                            <div class="checker">
                                <img src="img/check-sheet-icon.png" alt="">
                                <span>해당없음</span>
                            </div>
                        </div>     -->
					<div class="assigned-setting">
						<span class="title">지정가 설정 </span>
						<div class="price-input-box">
							<div class="direct-input">
								<div class="has_amb">
									<input type="text" onkeyup="javascript:getNumber(this);"
										placeholder="직접 입력" id="priceOrder">
									<div class="add-minus-box">
										<a onclick="javascript:priceOrderMinus();"></a> <a
											onclick="javascript:priceOrderPlus();"></a>
									</div>
								</div>
								<select onchange="javascript:setAmount(this.value)">
									<c:forEach var="val" begin="0" end="60" step="1"
										varStatus="status">
										<c:if test="${val == 30}">
											<option value="${30-val}" selected>직접입력</option>
										</c:if>
										<c:if test="${val != 30}">
											<option value="${30-val}">현재가 대비 ${30-val}%</option>
										</c:if>
									</c:forEach>
								</select> <a
									onclick="javascript:addAlarm('${itemMap.item_srt_cd}','${itemMap.market_cd}');"
									class="btn-addnotif">알림 추가</a>
							</div>
						</div>

					</div>
					<div class="divider"></div>
					<div class="ticker-list-container content-box">
						<span class="title white">시세 알림 목록</span>
						<div class="ticker-list" id="ticker-list">
							<c:forEach var="data" items="${targetPriceAlarmList}"
								varStatus="status">
								<div class="ticker-item">
									<span>목표가</span> <span><fmt:formatNumber
											value="${data.target_price}" groupingUsed="true" />원 <!-- ( -3% ) --></span>
									<a
										onclick="javascript:deleteTargetPriceAlarm('${data.target_price_alarm_id}',this);"
										class="delete"></a>
								</div>
							</c:forEach>

							<!-- <div class="ticker-item">
                                    <span>목표가</span>
                                    <span>300원 ( +3% )</span>
                                    <a href="#" class="delete"></a>
                                </div>
                                <div class="ticker-item">
                                    <span>손절가</span>
                                    <span>200원 ( -3% )</span>
                                    <a href="#" class="delete"></a>
                                </div>
                                <div class="ticker-item">
                                    <span>손절가</span>
                                    <span>200원 ( -3% )</span>
                                    <a href="#" class="delete"></a>
                                </div> -->

						</div>
					</div>
				</div>
			</div>
		</div>
		<c:if
			test="${adviserGrade != null && (adviserGrade == 'S' || adviserGrade == 'A')}">
			<div class="fixed-bottomarea">
				<span>인공지능 차트분석 프리미엄 신호알림</span>
				<c:if test="${useYn =='Y'}">
					<a
						href="javascript:insertPremiumSignal('${itemMap.item_srt_cd}','${itemMap.market_cd}');">서비스
						신청하기</a>
				</c:if>
				<c:if test="${useYn =='N'}">
					<a href="/adviser/adviser.do">어드바이저 구매</a>
				</c:if>
			</div>
		</c:if>
	</div>

	<%-- <div class="content-container">
            <header class="clearfix">
                <div class="search-icon">
                    <a style="cursor:pointer;" onclick="javascript:history.go(-1);" class="goback"><img src="/img/back-icon.png" alt=""></a> 
                </div>
                <div class="page-title txt-medium">
                    기업상세
                </div>
                <div class="menu-bar pull-right withsearch">
                    <a href="/search/search.do"><img src="/img/search-icon.png" alt=""></a>
                    <a href="/adviser/adviser.do"><img src="/img/home-pink-icon.png" alt=""></a>
                </div>
            </header>
            <div class="divider"></div>
            <div class="company-details-container">
                <div class="company-name">
                    <span class="name txt-bold">${itemMap.item_name} (${itemMap.item_srt_cd})</span>
                    <c:if test="${itemMap.interest_item_cd != null}">
                    	<a onclick="javascript:selectInterestItem('${itemMap.item_srt_cd}','${itemMap.market_cd}',this)" id="forHeart" class="heart heart-full"></a>
                    </c:if>
                    <c:if test="${itemMap.interest_item_cd == null}">
                    	<a onclick="javascript:selectInterestItem('${itemMap.item_srt_cd}','${itemMap.market_cd}',this)" id="forHeart" class="heart"></a>
                    </c:if>
                </div>
                <div class="company-views">
                    <ul>
                        <li class="active"><a href="company-ticker.do?itemSrtCd=${itemMap.item_srt_cd}&marketCd=${itemMap.market_cd}">시세알림설정</a></li>
                        <li><a href="company.do?itemSrtCd=${itemMap.item_srt_cd}&marketCd=${itemMap.market_cd}">기업개요</a></li>
                        <li><a href="company-news.do?itemSrtCd=${itemMap.item_srt_cd}&marketCd=${itemMap.market_cd}">뉴스</a></li>
                        <li><a href="company-analysis.do?itemSrtCd=${itemMap.item_srt_cd}&marketCd=${itemMap.market_cd}">프리미엄 종목 분석</a></li>
                    </ul>
                </div>         
                <div class="ticker-notifications view-details">
                    <div class="overview detail-box">
                        <span class="price">현재가</span>
                        <div class="price-info-box">
                                	<c:if test="${cmpprevddTpCd == '1' || cmpprevddTpCd == '2'}">
	                                    <span class="price-val"  id="trdPrc">${trdPrc}</span>
	                                    <div class="percent-value">
	                                        <span>${cmpprevddPer}%</span>
	                                        <span>${cmpprevddPrc}</span>
	                                    </div>
                                    </c:if>
                                    <c:if test="${cmpprevddTpCd == '3' || cmpprevddTpCd == '6' || cmpprevddTpCd == '7' || cmpprevddTpCd == '8' || cmpprevddTpCd == '9'}">
	                                    <span class="price-val"  id="trdPrc">${trdPrc}</span>
	                                    <div class="percent-value">
	                                        <span>${cmpprevddPer}%</span>
	                                        <span>${cmpprevddPrc}</span>
	                                    </div>
                                    </c:if>
                                    <c:if test="${cmpprevddTpCd == '4' || cmpprevddTpCd == '5'}">
	                                    <span class="price-val-blue" id="trdPrc">${trdPrc}</span>
	                                    <div class="percent-value-blue">
	                                        <span>${cmpprevddPer}%</span>
	                                        <span>${cmpprevddPrc}</span>
	                                    </div>
                                    </c:if>
                                </div>
                    </div>
                </div>
                <div class="divider"></div>
                <div class="add-ticker-container">
                    <div class="add-ticker">
                        <div class="ticker">
                            <span>시세 알림 추가</span>  
                            <img src="/img/qm-icon.png" alt="">
                        </div>
                        <div class="note">
                            <span>매수가를 입력하면 실시간으로</span> 
                            <span>수익률을 알려드립니다.</span> 
                        </div>          
                    </div>
                    <div class="number-sheets">
                        <span class="head">매수가 입력</span>
                        <div class="label-input">
                            <input type="text" placeholder="매수가 입력" id="purchasePrice">
                        </div>
                        <div class="checker" onclick="javascript:purchaseNone();">
                            <img src="/img/check-sheet-icon.png" alt="">
                            <span>해당없음</span>
                        </div>
                    </div>    
                    <div class="divider"></div>
                    <div class="assigned-setting">
                        <span class="title">지정가 설정 </span>
                        <div class="price-input-box">
                            <select onchange="javascript:setAmount(this.value)">
                            	<c:forEach var="val" begin="0" end="60" step="1" varStatus="status">
                            		<c:if test="${val == 30}">
								    	<option value="${30-val}"  selected>현재가 대비 ${30-val}%</option>
								    </c:if>
								    <c:if test="${val != 30}">
								    	<option value="${30-val}">현재가 대비 ${30-val}%</option>
								    </c:if>
								</c:forEach>
                            </select>
                            <div class="direct-input">
                                <input type="text"  onkeyup="javascript:getNumber(this);" placeholder="직접 입력" id="priceOrder">
                                <div class="add-minus-box">
                                    <a onclick="javascript:priceOrderMinus();"></a>
                                    <a onclick="javascript:priceOrderPlus();"></a>
                                </div>
                                <a onclick="javascript:addAlarm('${itemMap.item_srt_cd}','${itemMap.market_cd}');" class="btn-addnotif">알림 추가</a>
                            </div>
                        </div>
                    </div>
                    <div class="divider"></div>
                    <div class="ticker-list-container content-box">
                        <span class="title">시세 알림 목록</span>
                        <div class="ticker-list" id="ticker-list">
                        	<c:forEach var="data" items="${targetPriceAlarmList}" varStatus="status">
                        		<div class="ticker-item">
	                                <span><fmt:formatNumber value="${data.target_price}" groupingUsed="true"/>원 <!-- ( -3% ) --></span>
	                                <a onclick="javascript:deleteTargetPriceAlarm('${data.target_price_alarm_id}',this);" class="delete"></a>
	                            </div>
                        	</c:forEach>
                        </div>
                    </div>
                </div>
        </div>
	</div> --%>

	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
	<script src="/js/vendor/bootstrap.min.js"></script>
	<!-- veiwport for countnumber -->
	<script src="/js/jquery.viewportchecker.js"></script>
	<script src="/js/main.js"></script>
</body>
</html>
