<%--
	 /*
	  * ================================================================================================
	  * Filename	:	result_bnoti.jsp
	  * Function	:	결제결과 공통통보 페이지. 
	  							더존페이(PG사)에서 승인 결과를 받아 쇼핑몰 DB에 처리하는 페이지
	  * Author		:	All contents Copyright DuzonPay all rights reserved
	  * ================================================================================================
	  */
	 
	 /*
	  * ================================================================================================
	  *	result_bnoti.php를 호출할때 인자로 넘어오는 구성요소
	  
	  * 결제수단과 상관없이 공통적으로 넘어오는 변수	 	 
	   . BKW_RESULTCD			  : 응답코드로 '0000'이면 정상 그외는 에러처리한다.
	   . BKW_RESULTMSG			: 응답메시지
	   . BKW_TRADENO				: 더존페이(PG사) 고유 거래번호
	   . BKW_AUTHDATE			  : 승인일시(YYYYMMDDhhmmss)
	   . BKW_AMOUNT				  : 승인금액
	   . BKW_PAYTYPE				: 결제수단(PA11:신용카드, PA03:계좌이체, PA10:가상계좌, PA08:휴대폰소액결제, PA01:전화결제_폰빌)
	   . BKW_SHOP_ORDER_NO	: 쇼핑몰 주문번호
	   . BKW_SITECD				  : 쇼핑몰 사이트코드
	   . BKW_SHA256				  : 거래금액검증 hash 값
	    
	   * 가상계좌 관련 변수
	   . BKW_BANKNM				  : 가상계좌 발급은행명
	   . BKW_BANKACCOUNT		: 가상계좌 발급계좌번호
	   . BKW_IPKUMSTATUS		: 가상계좌 입금상태(IM03: 입금대기, IM01:입금완료)
	   . BKW_IPKUMUSERNM		: 주문자/입금자명
	   
	   * 소액결제(휴대폰,전화) 관련 변수
	   . BKW_PHONENO				: 결제 전화번호
	 	 . BKW_PHONECOMMTYPE	: 통신사(CI01:SKT, CI02:KT, CI03:LG U+) 
	 	
	 	 * 신용카드 관련 변수
	   . BKW_AUTHNO				  : 카드 승인번호
	   . BKW_CARDNAME			  : 카드명
	 	 . BKW_QUOTA				  : 할부개월(00:일시불, 02:2개월...)
	 	 
	 	 * 기타변수                       
	 	 . BKW_ETC1					  : 쇼핑몰에서 사용하는 여유필드1
	 	 . BKW_ETC2					  : 쇼핑몰에서 사용하는 여유필드2
	 	 . BKW_ETC3					  : 쇼핑몰에서 사용하는 여유필드3
	 	 . BKW_ETC4					  : 쇼핑몰에서 사용하는 여유필드4
	 	 . BKW_ETC5					  : 쇼핑몰에서 사용하는 여유필드5
	 	 	   
	  * ================================================================================================
	  */
--%>
<%@ page contentType="text/html;charset=euc-kr" pageEncoding="euc-kr"%>
<%

	 /*
    * 1. 결제관련 변수 받아오기
    * 쇼핑몰 자체변수는 넘어오지않으므로 유의하시기 바랍니다.
    * 단, 결제시 쇼핑몰에서 사용하도록 정의한 여유필드1~5에 setting한 경우 전송가능함.
    */
     
   String BKW_RESULTCD   				= request.getParameter("BKW_RESULTCD");
	 String BKW_RESULTMSG   			= request.getParameter("BKW_RESULTMSG");
	 String BKW_TRADENO   				= request.getParameter("BKW_TRADENO");
	 String BKW_AUTHDATE   				= request.getParameter("BKW_AUTHDATE");
	 String BKW_AMOUNT   					= request.getParameter("BKW_AMOUNT");
	 String BKW_PAYTYPE   				= request.getParameter("BKW_PAYTYPE");
   String BKW_SHOP_ORDER_NO   	= request.getParameter("BKW_SHOP_ORDER_NO");
	 String BKW_SITECD   					= request.getParameter("BKW_SITECD");
	 String BKW_SHA256   					= request.getParameter("BKW_SHA256");
	 
	 String BKW_BANKNM   					= request.getParameter("BKW_BANKNM");
	 String BKW_BANKACCOUNT   		= request.getParameter("BKW_BANKACCOUNT");
	 String BKW_IPKUMSTATUS   		= request.getParameter("BKW_IPKUMSTATUS");
	 String BKW_IPKUMUSERNM   		= request.getParameter("BKW_IPKUMUSERNM");
	 
	 String BKW_PHONENO   				= request.getParameter("BKW_PHONENO");
	 String BKW_PHONECOMMTYPE  		= request.getParameter("BKW_PHONECOMMTYPE");
	 
	 String BKW_AUTHNO   					= request.getParameter("BKW_AUTHNO");
	 String BKW_CARDNAME   				= request.getParameter("BKW_CARDNAME");
	 String BKW_QUOTA   					= request.getParameter("BKW_QUOTA");
	 
	 String BKW_ETC1   					  = request.getParameter("BKW_ETC1");
	 String BKW_ETC2   					  = request.getParameter("BKW_ETC2");
	 String BKW_ETC3   					  = request.getParameter("BKW_ETC3");
	 String BKW_ETC4   					  = request.getParameter("BKW_ETC4");
	 String BKW_ETC5   					  = request.getParameter("BKW_ETC5");
	 
	 /*
    * 2. 결제성공시 DB처리
    */     
   if(BKW_RESULTCD.equals("0000")){
   		// 결제 성공시 처리 작업
     	// 이곳에서 데이터 베이스 작업을 하시면 됩니다.
     		
     	// 데이터 베이스 처리 후, 아래 print구문을 꼭 삽입해 주셔야합니다.
     	out.println("<TID>" + BKW_TRADENO + "</TID>");
   }    
%>

