
<%@ page contentType="text/html;charset=euc-kr" pageEncoding="euc-kr"%>
<%
		/*
     * 1. 입금통보URL로 전송되는 주요변수 받아오기
     * 상점에서 구현한 입금통보 페이지는 더존페이 상점관리자 페이지에 등록하셔야 입금완료 시 입금통보를 받으실 수 있습니다.
     * 등록할 위치 : 상점관리자페이지 HOME > 사이트 정보조회 > 사이트 정보관리 메뉴에서 가상계좌 입금통지URL
     */     
    String BKW_TRADENO					= request.getParameter("BKW_TRADENO");						// 원거래에 대한 더존페이(PG사) 고유 거래번호
    String BKW_RESULTCD  				= request.getParameter("BKW_RESULTCD");						// 거래결과코드(0000인 경우 성공. 그 외는 실패를 나타냅니다)
         
     if(BKW_RESULTCD.equals("0000")){
     		
     		// 데이터 베이스 처리 후, 아래 print구문을 꼭 삽입해 주셔야합니다.
     		out.println("<TID>" + BKW_TRADENO + "</TID>");
     }
%>

