<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

%>

<!DOCTYPE html>
<html lang="ko" style="height: 100%;">
<jsp:include page="/WEB-INF/views/jsp/common-header.jsp"></jsp:include>
<body style="height: 100%; text-align: center;">

	<script type="text/javascript">  
var homeLoader;
$(document).ready(function(){  

	var userInputId = getCookie("userInputId");
	$("#loginId").val(userInputId);

	var userInputPw = getCookie("userInputPw"); 
	$("#loginPwd").val(userInputPw);

	try {
		if(window.carrierJrpr != null){
			window.carrierJrpr.loginForApp("${kakaoId}", "");
		}
		if(window.external != null){
	        window.external.loginForPC("${kakaoId}", "");
	    }
	}catch(exception){
	    
	}finally {
		
	}

	//아이디와 패스워드가 
	if($("#loginId").val() != "" && $("#loginPwd").val() != "" && getParameter("status") != "logout"){
		//loginProcess();
	}	
	
  
});


$(window).on("scroll", function(){ 
	var scroll_top=$(this).scrollTop();
	$(".loading-indicator-wrapper").css("top",scroll_top);
	$(".loading-indicator-wrapper").css("height","100%");
});



function getParameter(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}



function getCookie(cookieName) {
    cookieName = cookieName + '=';
    var cookieData = document.cookie;
    var start = cookieData.indexOf(cookieName);
    var cookieValue = '';
    if(start != -1){
        start += cookieName.length;
        var end = cookieData.indexOf(';', start);
        if(end == -1)end = cookieData.length;
        cookieValue = cookieData.substring(start, end);
    }
    return unescape(cookieValue);
}

function setCookie(cookieName, value, exdays){
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var cookieValue = escape(value) + ((exdays==null) ? "" : "; expires=" + exdate.toGMTString());
    document.cookie = cookieName + "=" + cookieValue;
}

function logout(){

	if(confirm("로그아웃 하시겠습니까?")){
		
		try {
			if(window.carrierJrpr != null){
				window.carrierJrpr.logoutForApp("");
			}
			if(window.external != null){
	            window.external.logoutForPC("");
	        }
		}catch(exception){
		    
		}finally {
			document.location.href = "/kakao/logout.do";	
		}
	}
}
    

function loginForApp(userId){
	window.carrierJrpr.loginForApp(userId, "");
}



function logoutForApp(){
	window.carrierJrpr.logoutForApp("");
}    
    
    
    
function loginProcess(){
	
	if($("#loginId").val() == ""){
		$.alert("아이디가 입력 되지 않았습니다.",function(a){
	    });
		return;
	}
	if($("#loginPwd").val() == ""){
		$.alert("패스워드가 입력 되지 않았습니다.",function(a){
	    });
		return;
	}

	homeLoader.show();
	$.ajax({ 
        type: 'post' ,
        url : '/login/loginPersist.do' ,
        dataType : 'json' ,
        data : $('#loginForm').serialize(), 
        
        success : function(jsonData, textStatus, jqXHR) {
        	if(jsonData.resultCode =="0000"){
        		setCookie("userInputId", $("#loginId").val(), 365);
        		setCookie("userInputPw", $("#loginPwd").val(), 365);
            	goMainPage();
        	}else{
        		if(jsonData.resultCode =="E002"){
        			setCookie("userInputId", $("#loginId").val(), 365);
        			setCookie("userInputPw", $("#loginPwd").val(), 365);
        		}
        		$.alert(jsonData.resultMsg,function(a){
        			
        	    });
        	}
        	homeLoader.hide();
        } ,
        error : function(xhRequest, ErrorText, thrownError) {
          //  alertEx("시스템 에러");
        }
    }); 
}

function goMainPage(){
	document.location.href = "/menu/menu.do";
}
    
    
function changePassWord(){
	$.alert("준비중입니다.",function(a){
    });
//	document.location.href = "/login/changePassWord.do"
}
    
  


function backKeyController(str){
	if(window.Android != null){
		window.Android.toastShort("뒤로가기 버튼으로 앱을 종료 할 수 없습니다.");
	}	
} 

function goRegPage(){

	$.confirm("회원 가입 페이지로 이동 하시겠습니까?",function(a){
			if(a){
				//document.location.href = "/login/driver-register.do";
				document.location.href = "/login/cert.do";
			}
	});
	
}

</script>


	<style>
* {
	margin: 0;
	padding: 0;
	box-sizing: border-box;
	font-family: 'Noto Sans KR', 'sans-serif';
}

html, body {
	height: 100%;
}

body {
	display: grid;
	place-items: center;
	text-align: center;
	background: #1E1D1D;
}

.content {
	width: 330px;
	background: #D9F2F7;
	margin-bottom: -55%;
	padding: 40px 30px;
	box-shadow: -3px -3px 7px #ffffff73, 2px 2px 5px
		rgba(94, 104, 121, 0.288);
}

.content .text {
	font-size: 33px;
	font-weight: 600;
	margin-bottom: 35px;
	color: #1478FF;
}

.forgot-pass {
	text-align: left;
	margin: 10px 0 10px 5px;
}

.forgot-pass a {
	font-size: 16px;
	color: #1478FF;
	text-decoration: none;
}

.forgot-pass:hover a {
	text-decoration: underline;
}

.content .field {
	height: 50px;
	width: 100%;
	display: flex;
	position: relative;
}

.field input {
	height: 100%;
	width: 75%;
	padding-left: 45px;
	font-size: 18px;
	outline: none;
	border: none;
	color: #595959;
	background: #fff;
	box-shadow: inset 2px 2px 5px #babecc, inset -5px -5px 10px #ffffff73;
}

.field input:focus ~ label {
	box-shadow: inset 2px 2px 5px #babecc, inset -1px -1px 2px #ffffff73;
}

.loginbtn {
	margin: 15px 0;
	width: 100%;
	height: 50px;
	color: #fff;
	font-size: 18px;
	font-weight: 600;
	background: #4D1B08;
	border: none;
	outline: none;
	cursor: pointer;
	box-shadow: 2px 2px 5px #7D8082, -5px -5px 10px #ffffff73;
}

.loginbtn:focus {
	color: #fff;
	box-shadow: inset 2px 2px 5px #babecc, inset -5px -5px 10px #ffffff73;
}

.signup {
	font-size: 16px;
	color: #041033;
	margin: 10px 0;
}

.signup a {
	color: #1478FF;
	text-decoration: none;
}

.signup a:hover {
	text-decoration: underline;
}
</style>




	<div class="content">


		<div class="text">
			<img style="width: 20%; height: 15%;" src="/img/nqlogoY.png">
			Login
		</div>
		<form id="loginForm">
			<div class="field">
				<span class="fas fa-user"></span> <label>&nbsp;아 이 디&nbsp;</label> <input
					id="loginId" name="dvrUserId" type="text">
			</div>
			<div class="field" style="margin-top: 5%;">
				<span class="fas fa-lock"></span> <label>비밀번호&nbsp;</label> <input
					id="loginPwd" name="loginPwd" type="password"
					onkeypress="if(event.keyCode=='13') loginProcess();">
			</div>
		</form>
		<div class="forgot-pass"></div>
		<button class="loginbtn" onclick="javascript:loginProcess();">로그인</button>
		<div class="signup">
			<a onclick="javascript:changePassWord();" style="color: #000;"><strong>비밀번호
					변경 /</strong></a> <a onclick="javascript:goRegPage()"><strong> 회원
					가입</strong></a>
		</div>

	</div>


	<div style="">
		<img style="width: 45%;" src="/img/nqlonglogo.jpg">
	</div>

	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
	<script src="/js/vendor/bootstrap.min.js"></script>
	<!-- veiwport for countnumber -->
	<script src="/js/jquery.viewportchecker.js"></script>
	<script src="/js/alert.js"></script>
	<script src="/js/main.js"></script>
	<script src="/js/vendor/jquery.loading-indicator.min.js"></script>
	<script>

    $(document).ready(function(){
    
    	homeLoader = $('body').loadingIndicator({
			useImage: false,
			showOnInit : false
		}).data("loadingIndicator");
	
    	//homeLoader.show();
    	//homeLoader.hide();
    	var scroll_top=$(document).scrollTop();
    	$(".loading-indicator-wrapper").css("top",scroll_top);
    	$(".loading-indicator-wrapper").css("height","100%");
    	
    });


    </script>
</body>
</html>
