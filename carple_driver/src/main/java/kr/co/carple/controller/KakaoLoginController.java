package kr.co.carple.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.JsonNode;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import kr.co.carple.utils.KakaoUtils;

@Controller
@RequestMapping(value="/kakao")
public class KakaoLoginController {

	//private String RestApiKey = "e3a057034847accf088ffd0b3ad83801";
	
	@Value("#{appProp['RestApiKey']}")
    private String RestApiKey;
    
    @Value("#{appProp['Redirect_URI']}")
    private String Redirect_URI;
        
  //  @Autowired
   // private UserService userService;
	
  
    
    
	@RequestMapping(value = "/kakaoLogin",produces="application/json", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView kakaoLogin(@RequestParam("code") String code,ModelAndView mav,HttpSession session, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			//JsonNode jsonToken  = KakaoUtils.getAccessToken(code,RestApiKey,Redirect_URI);			
			//JsonNode userInfo = KakaoUtils.getKakaoUserInfo(jsonToken.get("access_token").toString(),RestApiKey,Redirect_URI);
			JsonNode jsonToken  = KakaoUtils.getAccessToken(code,RestApiKey,Redirect_URI);			
			JsonNode userInfo = KakaoUtils.getKakaoUserInfo(jsonToken.get("access_token").toString(),RestApiKey,Redirect_URI);
			
			
			String id = userInfo.path("id").asText();
			String nickname = null;
			String thumbnailImage = null;
			String profileImage = null;
			
			
			
			JsonNode properties = userInfo.path("properties");
			JsonNode kakaoAccount = userInfo.path("kakao_account");
			JsonNode profile = kakaoAccount.path("profile");
			
			
			if (!properties.isMissingNode()) {
				nickname = profile.path("nickname").asText();
				thumbnailImage = profile.path("thumbnail_image_url").asText();
				profileImage = profile.path("profile_image_url").asText();
			}
			
				// id로 사용자를 select 하고 데이터가 없다면 insert를 진행 한다.  
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("kakaoId", id);
			//Map<String, Object> userMap = userService.selectUser(map);
			
			
			Map<String, Object> userMap = new HashMap<String, Object>();
			
			userMap.put("memUserId", id);
			userMap.put("usernick", nickname);
			userMap.put("thumbnail_image_url", thumbnailImage);
			userMap.put("access_token", jsonToken.get("access_token").toString());
			
			session.setAttribute("user", userMap);
			
			/*
				
			if(userMap == null){
				UserVO userVO = new UserVO();
				userVO.setAccessToken(jsonToken.get("access_token").toString());
				userVO.setAdviserYn("N");
				userVO.setExpiresIn(jsonToken.get("expires_in").toString());
				userVO.setMainMenu("");
				userVO.setRefreshToken(jsonToken.get("refresh_token").toString());
				userVO.setThumbnailImageUrl(thumbnailImage);
				userVO.setTokenType(jsonToken.get("token_type").toString());
				userVO.setUserGrade(BaseAppConstants.USER_GRADE_NORMAL);
				userVO.setKakaoId(id);
				userVO.setUserNick(nickname);
				userVO.setUserRole(BaseAppConstants.USER_ROLE_NORMAL);
				userVO.setUserStatus(BaseAppConstants.USER_STATUS_NORMAL);
				userService.insertUser(userVO);
				userMap = userService.selectUser(map);
				session.setAttribute("user", userMap);
				
			}else{
				map.put("thumbnailImageUrl", thumbnailImage);
				userService.updateUserThumbNailImg(map);
				userMap = userService.selectUser(map);
				session.setAttribute("user", userMap);
			}
			
			//알림 설정 테이블에 사용자의 알림 설정이 있는지 확인 하고 없다면 insert를 진행 한다.
			Map<String, Object> notificationSettingMap = notificationSettingService.selectNotificationSetting(map);
			if(notificationSettingMap == null){
				//알림 설정
				NotificationSettingVO notificationSettingVO = new NotificationSettingVO();
				notificationSettingVO.setUniqueId("NSI"+UUID.randomUUID().toString().replaceAll("-", ""));
				notificationSettingVO.setKakaoId(id);	
				notificationSettingService.insertNotificationSetting(notificationSettingVO);
			}
			
			//
			List<Map<String, Object>> userCategoryList = userCategoryService.selectUserCategoryList(map);
			if(userCategoryList == null || userCategoryList.size() == 0){
				List<Map<String, Object>> newsCategoryList = newsCategoryService.selectNewsCategoryList(map);	
				for(int i = 0; i < newsCategoryList.size(); i++){
					Map<String, Object> newsCategory = newsCategoryList.get(i);
					UserCategoryVO userCategoryVO = new UserCategoryVO();
					userCategoryVO.setBasicYn("Y");
					userCategoryVO.setUniqueId("UCI"+UUID.randomUUID().toString().replaceAll("-", ""));
					userCategoryVO.setCategoryName(newsCategory.get("news_category_name").toString());
					userCategoryVO.setNewsCategoryCd(newsCategory.get("news_category_cd").toString());
					userCategoryVO.setKakaoId(id);
					userCategoryVO.setUseYn("Y");
					userCategoryService.insertUserCategory(userCategoryVO);
				}
			}
			
			//위젯설정 테이블에 사용자의 위젯 설정이 있는지 확인 하고 없다면 insert를 진행 한다.
			Map<String, Object> widgetSettingMap = widgetSettingService.selectWidgetSetting(map);
			if(widgetSettingMap == null){
				//위젯설정
				WidgetSettingVO widgetSettingVO = new WidgetSettingVO();
				widgetSettingVO.setKakaoId(id);
				widgetSettingVO.setUniqueId("WSI"+UUID.randomUUID().toString().replaceAll("-", ""));
				widgetSettingService.insertWidgetSetting(widgetSettingVO);
			}
		*/
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		mav.setViewName("redirect:/menu/menu.do");
		return mav;
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpSession session) {
	
		try{
			Map<String, Object> userSessionMap = (Map<String, Object>)session.getAttribute("user");
			JsonNode userInfo = KakaoUtils.kakaoUserLogout(userSessionMap.get("access_token").toString());
			session.invalidate();	
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return "redirect:/menu/menu.do?type=logout";
	}
	
	
	
}
