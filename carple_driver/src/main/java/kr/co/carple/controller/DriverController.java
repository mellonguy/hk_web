package kr.co.carple.controller;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.co.carple.service.DriverService;
import kr.co.carple.utils.ResultApi;
import kr.co.carple.vo.MemberVO;

@Controller
@RequestMapping(value="/driver")
public class DriverController {

	
	private static final Logger logger = LoggerFactory.getLogger(DriverController.class);
	
	
	@Autowired
	private DriverService driverService;
	
	
	
	
	
    @RequestMapping(value = "/updateDvrDeviceKey", method = {RequestMethod.POST,RequestMethod.GET})
	@ResponseBody
	public ResultApi updateDvrDeviceKey(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response, MemberVO memberVO) {
		ResultApi resultApi = new ResultApi();
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("driver");
			
			Map<String, Object> map = new HashMap<String, Object>();
			
			map.put("dvrDeviceKey", request.getParameter("dvrDeviceKey").toString());
			map.put("dvrLoginEnv", request.getParameter("dvrLoginEnv").toString());
			map.put("dvrUserId", userSessionMap.get("dvr_user_id").toString());
			session.setAttribute("dvrDeviceKey", request.getParameter("dvrDeviceKey").toString());
			resultApi = driverService.updateDvrDeviceKey(map);
			
		}catch(Exception e){
			e.printStackTrace();
			resultApi.setResultCode("0001");
			
		}
		
		
		return resultApi;
	}
    
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
