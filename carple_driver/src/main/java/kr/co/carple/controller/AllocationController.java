package kr.co.carple.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.co.carple.service.ConsignRequestBidService;
import kr.co.carple.service.ConsignRequestDetailService;
import kr.co.carple.service.ConsignRequestService;
import kr.co.carple.service.FileService;
import kr.co.carple.utils.BaseAppConstants;
import kr.co.carple.utils.ParamUtils;
import kr.co.carple.utils.ResultApi;
import kr.co.carple.utils.WebUtils;

@Controller
@RequestMapping(value="/allocation")
public class AllocationController {

	
	private static final Logger logger = LoggerFactory.getLogger(AllocationController.class);
	
	
	@Value("#{appProp['upload.base.path']}")
    private String rootDir;
	private String subDir;
	
	@Value("#{appProp['RestApiKey']}")
    private String RestApiKey;
    
    @Value("#{appProp['Redirect_URI']}")
    private String Redirect_URI;
	
    @Value("#{appProp['customer_server_path']}")
    private String customer_server_path;
	
	@Value("#{appProp['customer_image_path']}")
    private String customer_image_path;
	
	@Value("#{appProp['customer_thumb_image_path']}")
    private String customer_thumb_image_path;
		
	@Value("#{appProp['driver_server_path']}")
    private String driver_server_path;
	
	@Value("#{appProp['driver_image_path']}")
    private String driver_image_path;
	
	@Value("#{appProp['driver_thumb_image_path']}")
    private String driver_thumb_image_path;
    
    
    
    
	@Autowired 
	private ConsignRequestService consignRequestService;
	 
	@Autowired
	private ConsignRequestDetailService consignRequestDetailService;
	
	@Autowired
	private ConsignRequestBidService consignRequestBidService;
	
	
	@Autowired
	private FileService fileService;
	
	
	
	@RequestMapping(value = "/main", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView main(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("driver");
			
			if(userSessionMap != null){
				
				
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				//경매 진행 상태가 입찰 진행중이고 탁송 차량 구분이 기사의 탁송 차량과 같은 경매건을 불러온다.
				List<String> corStatusList = new ArrayList<String>();
				corStatusList.add(BaseAppConstants.BID_STATUS_ING);
				//corStatusList.add(BaseAppConstants.BID_STATUS_DONE);
				paramMap.put("corStatus",corStatusList);
				paramMap.put("dvrCarrierType",userSessionMap.get("dvr_carrier_type").toString());
				paramMap.put("dvrUserId",userSessionMap.get("dvr_user_id").toString());
				paramMap.put("startRownum",0);
				paramMap.put("numOfRows",2);
				
				//PagingUtils.setPageing(request, totalCount, paramMap);
				
				//입찰 진행중 목록(입찰 진행중이고 차량구분이 같은 모든 목록)
				List<Map<String, Object>> consignRequestList = consignRequestService.selectConsignRequestList(paramMap);
				
				//응찰 진행중 목록(차량구분이 같고 내가 입찰한 목록 중 입찰 진행중 목록)
				corStatusList = new ArrayList<String>();
				corStatusList.add(BaseAppConstants.BID_STATUS_ING);
				paramMap.put("corStatus",corStatusList);
				List<Map<String, Object>> consignRequestJoinList = consignRequestService.selectConsignRequestJoinList(paramMap);
				
				//낙찰 목록(입찰에 참여 하여 낙찰 받은 목록) 
				corStatusList = new ArrayList<String>();
				corStatusList.add(BaseAppConstants.BID_STATUS_DONE);
				paramMap.put("corStatus",corStatusList);
				List<Map<String, Object>> consignRequestJoinBidDoneList = consignRequestService.selectConsignRequestJoinList(paramMap);
								
				//완료목록(응찰 하여 낙찰 받고 완료까지 한 목록)
				corStatusList = new ArrayList<String>();
				corStatusList.add(BaseAppConstants.BID_STATUS_SUCCESS);
				paramMap.put("corStatus",corStatusList);
				List<Map<String, Object>> consignRequestJoinBidSuccessList = consignRequestService.selectConsignRequestJoinList(paramMap);
				
				
				
				//String today = WebUtils.getNow("yyyy-MM-dd");
				//String now = WebUtils.getNow("HH:mm");
				
				//mav.addObject("today", today);
				//mav.addObject("now", now);
				mav.addObject("listData", consignRequestList);
				mav.addObject("joinListData", consignRequestJoinList);
				mav.addObject("joinBidDoneListData", consignRequestJoinBidDoneList);
				mav.addObject("joinBidSuccessListData", consignRequestJoinBidSuccessList);
				mav.addObject("customer_server_path", customer_server_path);
				mav.addObject("customer_image_path", customer_image_path);
				mav.addObject("customer_thumb_image_path", customer_thumb_image_path);
				mav.addObject("driver_server_path", driver_server_path);
				mav.addObject("driver_image_path", driver_image_path);
				mav.addObject("driver_thumb_image_path", driver_thumb_image_path);
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	@RequestMapping(value = "/mainIngList", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView mainIngList(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("driver");
			
			if(userSessionMap != null){
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				//경매 진행 상태가 입찰 진행중이고 탁송 차량 구분이 기사의 탁송 차량과 같은 경매건을 불러온다.
				List<String> corStatusList = new ArrayList<String>();
				corStatusList.add(BaseAppConstants.BID_STATUS_ING);
				//corStatusList.add(BaseAppConstants.BID_STATUS_DONE);
				paramMap.put("corStatus",corStatusList);
				paramMap.put("dvrCarrierType",userSessionMap.get("dvr_carrier_type").toString());
				paramMap.put("startRownum",0);
				paramMap.put("numOfRows",10);
				
				//PagingUtils.setPageing(request, totalCount, paramMap);
				
				//입찰 진행중 목록(입찰 진행중이고 차량구분이 같은 모든 목록)
				List<Map<String, Object>> consignRequestList = consignRequestService.selectConsignRequestList(paramMap);
			
				mav.addObject("listData", consignRequestList);
				mav.addObject("customer_server_path", customer_server_path);
				mav.addObject("customer_image_path", customer_image_path);
				mav.addObject("customer_thumb_image_path", customer_thumb_image_path);
				mav.addObject("driver_server_path", driver_server_path);
				mav.addObject("driver_image_path", driver_image_path);
				mav.addObject("driver_thumb_image_path", driver_thumb_image_path);
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	@RequestMapping(value = "/mainJoinList", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView mainJoinList(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("driver");
			
			if(userSessionMap != null){
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				//경매 진행 상태가 입찰 진행중이고 탁송 차량 구분이 기사의 탁송 차량과 같은 경매건을 불러온다.
				List<String> corStatusList = new ArrayList<String>();
				corStatusList.add(BaseAppConstants.BID_STATUS_ING);
				//corStatusList.add(BaseAppConstants.BID_STATUS_DONE);
				paramMap.put("corStatus",corStatusList);
				paramMap.put("dvrCarrierType",userSessionMap.get("dvr_carrier_type").toString());
				paramMap.put("startRownum",0);
				paramMap.put("numOfRows",10);
				
				//PagingUtils.setPageing(request, totalCount, paramMap);
				
				//응찰 진행중 목록(차량구분이 같고 내가 입찰한 모든 목록)
				corStatusList = new ArrayList<String>();
				corStatusList.add(BaseAppConstants.BID_STATUS_ING);
				paramMap.put("corStatus",corStatusList);
				paramMap.put("dvrUserId",userSessionMap.get("dvr_user_id").toString());				
				List<Map<String, Object>> consignRequestJoinList = consignRequestService.selectConsignRequestJoinList(paramMap);
			
				mav.addObject("joinListData", consignRequestJoinList);
				mav.addObject("customer_server_path", customer_server_path);
				mav.addObject("customer_image_path", customer_image_path);
				mav.addObject("customer_thumb_image_path", customer_thumb_image_path);
				mav.addObject("driver_server_path", driver_server_path);
				mav.addObject("driver_image_path", driver_image_path);
				mav.addObject("driver_thumb_image_path", driver_thumb_image_path);
				
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}

	
	
	@RequestMapping(value = "/mainBidDoneList", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView mainBidDone(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("driver");
			
			if(userSessionMap != null){
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				//경매 진행 상태가 입찰 진행중이고 탁송 차량 구분이 기사의 탁송 차량과 같은 경매건을 불러온다.
				List<String> corStatusList = new ArrayList<String>();
				corStatusList.add(BaseAppConstants.BID_STATUS_ING);
				//corStatusList.add(BaseAppConstants.BID_STATUS_DONE);
				paramMap.put("corStatus",corStatusList);
				paramMap.put("dvrCarrierType",userSessionMap.get("dvr_carrier_type").toString());
				paramMap.put("startRownum",0);
				paramMap.put("numOfRows",10);
				
				//PagingUtils.setPageing(request, totalCount, paramMap);
								
				//낙찰 목록(입찰에 참여 하여 낙찰 받은 목록) 
				corStatusList = new ArrayList<String>();
				corStatusList.add(BaseAppConstants.BID_STATUS_DONE);
				paramMap.put("corStatus",corStatusList);
				paramMap.put("dvrUserId",userSessionMap.get("dvr_user_id").toString());
				List<Map<String, Object>> consignRequestJoinBidDoneList = consignRequestService.selectConsignRequestJoinList(paramMap);
								
				mav.addObject("joinBidDoneListData", consignRequestJoinBidDoneList);
				mav.addObject("customer_server_path", customer_server_path);
				mav.addObject("customer_image_path", customer_image_path);
				mav.addObject("customer_thumb_image_path", customer_thumb_image_path);
				mav.addObject("driver_server_path", driver_server_path);
				mav.addObject("driver_image_path", driver_image_path);
				mav.addObject("driver_thumb_image_path", driver_thumb_image_path);
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	@RequestMapping(value = "/mainBidSuccessList", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView mainBidSuccess(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("driver");
			
			if(userSessionMap != null){
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				//경매 진행 상태가 입찰 진행중이고 탁송 차량 구분이 기사의 탁송 차량과 같은 경매건을 불러온다.
				List<String> corStatusList = new ArrayList<String>();
				corStatusList.add(BaseAppConstants.BID_STATUS_ING);
				//corStatusList.add(BaseAppConstants.BID_STATUS_DONE);
				paramMap.put("corStatus",corStatusList);
				paramMap.put("dvrCarrierType",userSessionMap.get("dvr_carrier_type").toString());
				paramMap.put("startRownum",0);
				paramMap.put("numOfRows",10);
				
				//PagingUtils.setPageing(request, totalCount, paramMap);
				
				//완료목록(응찰 하여 낙찰 받고 완료까지 한 목록)
				corStatusList = new ArrayList<String>();
				corStatusList.add(BaseAppConstants.BID_STATUS_SUCCESS);
				paramMap.put("corStatus",corStatusList);
				paramMap.put("dvrUserId",userSessionMap.get("dvr_user_id").toString());
				List<Map<String, Object>> consignRequestJoinBidSuccessList = consignRequestService.selectConsignRequestJoinList(paramMap);

				mav.addObject("joinBidSuccessListData", consignRequestJoinBidSuccessList);
				mav.addObject("customer_server_path", customer_server_path);
				mav.addObject("customer_image_path", customer_image_path);
				mav.addObject("customer_thumb_image_path", customer_thumb_image_path);
				mav.addObject("driver_server_path", driver_server_path);
				mav.addObject("driver_image_path", driver_image_path);
				mav.addObject("driver_thumb_image_path", driver_thumb_image_path);
				
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	@RequestMapping(value = "/register", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView register(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			//if(userSessionMap != null){
			
				//String adviserCd = request.getParameter("adviserCd").toString();
				
				Map<String, Object> map = new HashMap<String, Object>();
				//map.put("adviserCd", adviserCd);
				//map.put("kakaoId", userSessionMap.get("kakao_id").toString());
				
				String today = WebUtils.getNow("yyyy-MM-dd");
				String now = WebUtils.getNow("HH:mm");
				mav.addObject("today", today);
				mav.addObject("now", now);
				
				/*
				mav.addObject("adviser", adviser);
				mav.addObject("msg", msg);
				mav.addObject("adviserGrade", adviserGrade);
				*/
			//}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	@RequestMapping(value = "/main-bak", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView mainBackup(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null){
			
				String adviserCd = request.getParameter("adviserCd").toString();
				
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("adviserCd", adviserCd);
				map.put("kakaoId", userSessionMap.get("kakao_id").toString());
				
				/*
				mav.addObject("adviser", adviser);
				mav.addObject("msg", msg);
				mav.addObject("adviserGrade", adviserGrade);
				*/
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	
	@RequestMapping(value = "/selectAdviserList", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi selectAdviserList(ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception{
		
		ResultApi result = new ResultApi();
		 
		 try{
			 
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			String adviserGrade = request.getParameter("adviserGrade").toString();
			
				
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("adviserGrade", adviserGrade==null?"":adviserGrade);
				
			//List<Map<String, Object>> adviserList = adviserService.selectAdviserList(map);
			//result.setResultData(adviserService.selectAdviserList(map));
			
		 }catch(Exception e){
			 result.setResultCode("E001");
			 e.printStackTrace();
		 }
		
		return result;
	}
	
	
	
	
	
	
	
	
	
	@RequestMapping(value = "/insert-allocation", method = {RequestMethod.POST,RequestMethod.GET})
	@ResponseBody
	public ResultApi insertAllocation(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response ) throws Exception {
		
		ResultApi resultApi = null;
		
		try{
			
			
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("user");
			Map<String, Object> map = new HashMap<String, Object>();
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
 						
			String[] contentArr =request.getParameterValues("contentArr");
			
			resultApi =  consignRequestService.insertConsignRequest(session,contentArr);
			
		}catch(Exception e){
			e.printStackTrace();
			resultApi.setResultCode("0001");
			
		}
		
		return resultApi;
	}
	
	
	
	
	
	
	
	
	@RequestMapping(value = "/viewConsignRequestDetail", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView viewConsignRequestDetail(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("driver");
			
			if(userSessionMap != null){
			
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				//paramMap.put("corMemUserId", userSessionMap.get("memUserId").toString());
				Map<String, Object> consignRequest = consignRequestService.selectConsignRequest(paramMap);
				paramMap.put("crdCorId", consignRequest.get("cor_id").toString());
				paramMap.put("corStatus", consignRequest.get("cor_status").toString());
				List<Map<String, Object>> consignRequestDetailList = consignRequestDetailService.selectConsignRequestDetailList(paramMap);
				paramMap.put("dvrUserId", userSessionMap.get("dvr_user_id").toString());
				List<Map<String, Object>> consignRequestBidList = consignRequestBidService.selectConsignRequestBidList(paramMap);
				
				subDir = "carPic";
				paramMap.put("crfCategoryType", subDir);
				paramMap.put("crfCorId", consignRequest.get("cor_id").toString());
				List<Map<String, Object>> consignRequestFileList = fileService.selectFileList(paramMap);
				
				mav.addObject("consignRequest", consignRequest);
				mav.addObject("consignRequestDetailList", consignRequestDetailList);
				mav.addObject("consignRequestBidList", consignRequestBidList);
				mav.addObject("consignRequestFileList", consignRequestFileList);
				mav.addObject("rootDir", rootDir);
				mav.addObject("paramMap", paramMap);
				mav.addObject("customer_server_path", customer_server_path);
				mav.addObject("customer_image_path", customer_image_path);
				mav.addObject("customer_thumb_image_path", customer_thumb_image_path);
				mav.addObject("driver_server_path", driver_server_path);
				mav.addObject("driver_image_path", driver_image_path);
				mav.addObject("driver_thumb_image_path", driver_thumb_image_path);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	
	
	
	
	@RequestMapping(value = "/consignRequestProcess", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView consignRequestProcess(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("driver");
			
			if(userSessionMap != null){
			
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				
				
				Map<String, Object> consignRequest = consignRequestService.selectConsignRequest(paramMap);
				paramMap.put("crdCorId", consignRequest.get("cor_id").toString());
				List<Map<String, Object>> consignRequestDetailList = consignRequestDetailService.selectConsignRequestDetailList(paramMap);
				paramMap.put("dvrUserId", userSessionMap.get("dvr_user_id").toString());
				paramMap.put("corStatus", BaseAppConstants.BID_STATUS_DONE);
				
				//해당 기사의 낙찰건이 맞는지 확인 하고...
				Map<String, Object> consignRequestForNextProcess = consignRequestService.selectConsignRequestForNextProcess(paramMap);
				
				if(consignRequestForNextProcess != null) {
					
					subDir = "carPic";
					paramMap.put("crfCategoryType", subDir);
					paramMap.put("crfCorId", consignRequest.get("cor_id").toString());
					List<Map<String, Object>> consignRequestFileList = fileService.selectFileList(paramMap);
					mav.addObject("consignRequest", consignRequest);
					mav.addObject("consignRequestDetailList", consignRequestDetailList);
					mav.addObject("consignRequestFileList", consignRequestFileList);
					mav.addObject("rootDir", rootDir);
					mav.addObject("paramMap", paramMap);
					mav.addObject("customer_server_path", customer_server_path);
					mav.addObject("customer_image_path", customer_image_path);
					mav.addObject("customer_thumb_image_path", customer_thumb_image_path);
					mav.addObject("driver_server_path", driver_server_path);
					mav.addObject("driver_image_path", driver_image_path);
					mav.addObject("driver_thumb_image_path", driver_thumb_image_path);	
					
				}else {
					WebUtils.messageAndRedirectUrl(mav, "해당 경매건에 대해 진행 할 수 있는 권한이 없습니다. 관리자에게 문의 하세요.", "/allocation/viewConsignRequestDetail.do?corId="+paramMap.get("crdCorId").toString());
				}
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	@RequestMapping(value = "/updateCrdStatus", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updateCrdStatus(ModelAndView mav, HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		ResultApi result = null;
		 
		 try{
			 
			 	HttpSession session = request.getSession();
				Map userSessionMap = (Map)session.getAttribute("driver");
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
								
				if(userSessionMap != null){
					result = consignRequestDetailService.updateConsignRequestDetailCrdStatus(paramMap);	
				}else {
					result.setResultCode("E003");
				}
				
		 }catch(Exception e){
			 result.setResultCode("E001");
			 e.printStackTrace();
		 }
		
		return result;
	}
	
	
	
	
	@RequestMapping(value = "/updateCorStatus", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updateCorStatus(ModelAndView mav, HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		ResultApi result = new ResultApi();
		 
		 try{
			 
			 	HttpSession session = request.getSession();
				Map userSessionMap = (Map)session.getAttribute("driver");
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
								
				if(userSessionMap != null){
					consignRequestService.updateCorStatus(paramMap);	
				}
				
		 }catch(Exception e){
			 result.setResultCode("E001");
			 e.printStackTrace();
		 }
		
		return result;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
