package kr.co.carple.controller;

import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.co.carple.service.ConsignRequestBidService;
import kr.co.carple.utils.ParamUtils;
import kr.co.carple.utils.ResultApi;
import kr.co.carple.vo.ConsignRequestBidVO;

@Controller
@RequestMapping(value="/bid")
public class BidController {

	private static final Logger logger = LoggerFactory.getLogger(BidController.class);
	
	@Value("#{appProp['RestApiKey']}")
    private String RestApiKey;
    
    @Value("#{appProp['Redirect_URI']}")
    private String Redirect_URI;
	
    
    @Autowired
    private ConsignRequestBidService consignRequestBidService;
	
    
    
    
    
	@RequestMapping(value = "/insert-bid", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi insertBid(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response ) throws Exception {
		
		ResultApi resultApi = new ResultApi();
		
		try{
			
			
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("driver");
			
			if(userMap != null) {
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				paramMap.put("dvrUserId", userMap.get("dvr_user_id").toString());
				resultApi =  consignRequestBidService.insertConsignRequestBid(paramMap);	
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
			resultApi.setResultCode("0001");
			
		}
		
		return resultApi;
	}
	
    
	
	@RequestMapping(value = "/update-bid-status", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi updateBidStatus(Locale locale,ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response ) throws Exception {
		
		ResultApi resultApi = new ResultApi();
		
		try{
			
			
			HttpSession session = request.getSession(); 
			Map userMap = (Map)session.getAttribute("driver");
			
			if(userMap != null) {
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				paramMap.put("dvrUserId", userMap.get("dvr_user_id").toString());
				resultApi =  consignRequestBidService.updateConsignRequestBidCrbStatus(paramMap);	
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
			resultApi.setResultCode("0001");
			
		}
		
		return resultApi;
	}
    
    
    
	
	
	
	
}
