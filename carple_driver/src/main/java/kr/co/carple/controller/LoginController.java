package kr.co.carple.controller;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.co.carple.service.DriverService;
import kr.co.carple.utils.ParamUtils;
import kr.co.carple.utils.ResultApi;
import kr.co.carple.utils.WebUtils;
import kr.co.carple.vo.DriverVO;

@Controller
@RequestMapping(value="/login")
public class LoginController {

	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	@Value("#{appProp['RestApiKey']}")
    private String RestApiKey;
    
    @Value("#{appProp['Redirect_URI']}")
    private String Redirect_URI;
	
    @Value("#{appProp['dbEncKey']}")
	private String dbEncKey;
  
    @Value("#{appProp['appKey']}")
    private String appKey;
    
    
    @Autowired
    private DriverService driverService;
    
    
	
    @RequestMapping(value = "/login-page", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView login_page(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("driver");
			
			if(userSessionMap != null) {
				response.sendRedirect(request.getContextPath()+"/menu/menu.do");
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
    
    @RequestMapping(value = "/cert", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView cert(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			session.setAttribute("appKey", appKey);
			

			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
    
    
    
    @RequestMapping(value = "/driver-register", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView driver_register(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			session.setAttribute("appKey", appKey);
			
			String dvrPhone = request.getParameter("dvrPhone") != null ?  request.getParameter("dvrPhone").toString():"";
			String dvrUserName = request.getParameter("dvrUserName") != null ?  request.getParameter("dvrUserName").toString():"";

			dvrPhone = WebUtils.phoneNum(dvrPhone);
			
			mav.addObject("dvrPhone", dvrPhone);
			mav.addObject("dvrUserName", dvrUserName);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
    
    
    @RequestMapping(value = "/driver-mod", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView driver_mod(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("driver");
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("dvrUserId", userSessionMap.get("dvr_user_id").toString());
			
			Map<String, Object> driverMap = driverService.selectDriver(map);
			mav.addObject("driverMap", driverMap);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
    
    
    
    
    
    @RequestMapping(value = "/duplicateChk", method = {RequestMethod.POST,RequestMethod.GET})
	@ResponseBody
	public ResultApi duplicateChk(ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception{
		
		ResultApi result = new ResultApi();
		 
		 try{
			 
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
			Map<String, Object> driverMap = driverService.selectDriverDuplicateChk(paramMap);
			
			if(driverMap != null) {
				result.setResultCode("E101");
				result.setResultMsg("이미 등록된 아이디 입니다.");
			}else {
				result.setResultMsg("이 아이디는 사용이 가능 합니다.");
			}
			
		 }catch(Exception e){
			 result.setResultCode("E001");
			 result.setResultMsg("오류가 발생 했습니다. 관리자에게 문의 하세요.");
			 e.printStackTrace();
		 }
		
		return result;
	}
	
    
    
    @RequestMapping(value = "/insertDriver", method = {RequestMethod.POST,RequestMethod.GET})
	@ResponseBody
	public ResultApi insertDriver(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response, DriverVO driverVO) {
		ResultApi resultApi = new ResultApi();
		try{
			
			
			HttpSession session = request.getSession();
			//Map userSessionMap = (Map)session.getAttribute("driver");
			
			resultApi = driverService.insertDriver(request, session, driverVO);
			
		}catch(Exception e){
			e.printStackTrace();
			resultApi.setResultCode("0001");
			
		}
		
		
		return resultApi;
	}
    
    
    @RequestMapping(value = "/updateDriver", method = {RequestMethod.POST,RequestMethod.GET})
	@ResponseBody
	public ResultApi updateDriver(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response, DriverVO driverVO) {
		ResultApi resultApi = new ResultApi();
		try{
			
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("driver");
			
			if(userSessionMap != null) {
				
				resultApi = driverService.updateDriver(driverVO);	
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
			resultApi.setResultCode("0001");
			
		}
		
		
		return resultApi;
	}
    
    
    
    
    
    @RequestMapping(value = "/loginPersist", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi loginPersist(HttpServletRequest request,HttpServletResponse response,HttpSession session,@RequestParam String dvrUserId,@RequestParam String loginPwd) throws Exception{
    	
    	ResultApi result = new ResultApi();
			
			try{

				Map<String, Object> map = new HashMap<String, Object>();
		    	map.put("dvrUserId", dvrUserId);
		    	map.put("loginPwd", loginPwd);
		    	
		    	Map<String, Object> driverMap = driverService.selectDriver(map);
		    	
		    	map.put("forLogin", "Y");
		    	map.put("dbEncKey", dbEncKey);
		    	
		    	Map<String, Object> driverMapSub = driverService.selectDriver(map);
		    	
		    	//guid
		    	if(driverMap == null){
		    		result.setResultCode("E001");
		    		result.setResultMsg("로그인 정보를 찾을 수 없습니다. 관리자에게 문의 하세요.");
		    	}else if(driverMap != null && driverMapSub == null){
		    		result.setResultCode("E002");
		    		result.setResultMsg("패스워드가 잘못 입력 되었습니다.");
		    	}else{
		    		
		    		if(driverMapSub.get("dvr_approve_status") != null && !driverMapSub.get("dvr_approve_status").toString().equals("")){
		    		
		    			if(driverMapSub.get("dvr_approve_status").toString().equals("Y")) {
		    				result.setResultCode("0000");
						    session.setAttribute(dvrUserId, driverMapSub);	//로그인 성공
					    	session.setAttribute("driver", driverMapSub);		
					    	session.setAttribute("appKey", appKey);
		    			}else {
		    				result.setResultCode("E002");
		    				result.setResultMsg("승인 대기중인 아이디 입니다. 승인이 완료될때 까지 기다려 주세요.");
		    			}
		    		}else {
		    			result.setResultCode("E003");
	    				result.setResultMsg("로그인중 오류가 발생 했습니다. 관리자에게 문의 하세요.");
		    		}
		    			
		    	}
		    	
			}catch( Exception e){
				result.setResultCode("E999");
				e.printStackTrace();
			}
			
			return result;
	}
	
	@RequestMapping(value = "/nopermission", method = RequestMethod.GET)
	public ModelAndView nopermission(ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception{
		
		try{
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return mav;
	}
    
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpSession session) {
    	
    	try {    	
    		session.invalidate();	
    	}catch(Exception e) {
    		e.printStackTrace();    	
    	}
		return "redirect:/?&status=logout";
	}
    
	
	
	
}
