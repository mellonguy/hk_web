package kr.co.carple.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.co.carple.service.DriverFileService;
import kr.co.carple.service.DriverService;
import kr.co.carple.service.FileService;
import kr.co.carple.utils.ResultApi;

@Controller
@RequestMapping(value="/menu")
public class MenuController {

	private static final Logger logger = LoggerFactory.getLogger(MenuController.class);
	
	@Value("#{appProp['RestApiKey']}")
    private String RestApiKey;
    
    @Value("#{appProp['Redirect_URI']}")
    private String Redirect_URI;
	
	@Value("#{appProp['customer_server_path']}")
    private String customer_server_path;
	
	@Value("#{appProp['customer_image_path']}")
    private String customer_image_path;
	
	@Value("#{appProp['customer_thumb_image_path']}")
    private String customer_thumb_image_path;
		
	@Value("#{appProp['driver_server_path']}")
    private String driver_server_path;
	
	@Value("#{appProp['driver_image_path']}")
    private String driver_image_path;
	
	@Value("#{appProp['driver_thumb_image_path']}")
    private String driver_thumb_image_path;
    
	@Value("#{appProp['appKey']}")
    private String appKey;
    
	
    @Autowired
    private DriverService driverService;

    @Autowired
    private FileService fileService;
    
    @Autowired
    private DriverFileService driverFileService;
    
	
	@RequestMapping(value = "/menu", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView menu(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) throws Exception {
		
		try{
			HttpSession session = request.getSession();
			
			Map userSessionMap = (Map)session.getAttribute("driver");
			
			if(userSessionMap == null){
				mav.setViewName("/menu/menu-sub");	
				mav.addObject("Redirect_URI", Redirect_URI);
				//WebUtils.messageAndRedirectUrl(mav, "이미 사용중인 아이디 입니다.", "/baseinfo/add-employee.do");
			}else{
				
				mav.addObject("driverName", userSessionMap.get("dvr_user_name"));
				mav.addObject("thumbnail", "/img/user-img.png");
				
			}
			
			
			//WsClient.main(null);
			
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return mav;
	}
	
	
	
	@RequestMapping(value = "/menu-sub", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView menuSub(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response)  throws Exception{

		try{
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			if(userSessionMap != null){
				//포인트 사용/적립 내역 조회
				Map<String,Object> userMap = new HashMap<String, Object>();
				userMap.put("kakaoId", userSessionMap.get("kakao_id"));
				
	//			List<Map<String, Object>> pointHistoryList = pointHistoryService.selectPointHistoryList(userMap);
	//			mav.addObject("pointHistoryList", pointHistoryList);				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return mav;
	}
	
	
	@RequestMapping(value = "/getapidata", method = {RequestMethod.POST,RequestMethod.GET})
	@ResponseBody
	public ResultApi getapidata(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response)  throws Exception{

		ResultApi result = new ResultApi();
		
		try{
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("driver");
			if(userSessionMap != null){
				
				URL url = new URL("http://data.ex.co.kr/openapi/burstInfo/realTimeSms?key=9318324770&sortType=desc&pagingYn=Y&type=json&numOfRows=40&pageNo=1");
				HttpURLConnection con = (HttpURLConnection) url.openConnection(); 
				con.setConnectTimeout(5000);
				con.setReadTimeout(5000);
				con.setRequestMethod("GET");
	            con.setDoOutput(false); 
				
	            JSONParser jsonParser = null;
				JSONObject jsonObject = null;
	            
				if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
					
					ArrayList<JSONObject> finalList = new ArrayList<JSONObject>();
					
					String line = "";
					StringBuilder sb = new StringBuilder();
					BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
					
					while ((line = br.readLine()) != null) {
						sb.append(line);
					}
					br.close();
					
					jsonParser = new JSONParser();
					jsonObject = (JSONObject) jsonParser.parse(sb.toString());
				
					JSONArray list = (JSONArray)jsonParser.parse(jsonObject.get("realTimeSMSList").toString());
					list = (JSONArray)jsonParser.parse(jsonObject.get("realTimeSMSList").toString());
					
					for(int i = 0; i < list.size(); i++) {
						JSONObject jsonData = (JSONObject) list.get(i);
						System.out.println(jsonData.toString());
						finalList.add(jsonData);
					}
					
					result.setResultData(finalList);
					
				} else {
					System.out.println(con.getResponseMessage());
				}
				
				
				
				/*
				URL url = new URL("http://data.ex.co.kr/openapi/burstInfo/realTimeSms?key=9318324770&type=json&numOfRows=20&pageNo=1");
				HttpURLConnection con = (HttpURLConnection) url.openConnection(); 
				con.setConnectTimeout(5000);
				con.setReadTimeout(5000);
				con.setRequestMethod("GET");
	            con.setDoOutput(false); 

	            //Thread.sleep(500);
	            
				StringBuilder sb = new StringBuilder();
				if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
					 
					JSONParser jsonParser = null;
					JSONObject jsonObject = null;
					
					ArrayList<JSONObject> finalList = new ArrayList<JSONObject>();
					
					
					BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
					String line;
					while ((line = br.readLine()) != null) {
						sb.append(line);
					}
					br.close();
					
					jsonParser = new JSONParser();
					jsonObject = (JSONObject) jsonParser.parse(sb.toString());
					String count = jsonObject.get("count").toString();
					String pageSize = jsonObject.get("pageSize").toString();
					
					//마지막-1 전페이지
					String pageNo = String.valueOf(Integer.parseInt(pageSize)-1);
					
					//카운트와 페이지 사이즈로 최근 목록을 가져온다.
					url = new URL("http://data.ex.co.kr/openapi/burstInfo/realTimeSms?key=9318324770&type=json&numOfRows=20&pageNo="+pageNo);
					con = (HttpURLConnection) url.openConnection(); 
					con.setConnectTimeout(5000);
					con.setReadTimeout(5000);
					con.setRequestMethod("GET");
		            con.setDoOutput(false); 

					sb = new StringBuilder();
					if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
						 
						br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
						line="";
						while ((line = br.readLine()) != null) {
							sb.append(line);
						}
						br.close();
						
						jsonParser = new JSONParser();
						jsonObject = (JSONObject) jsonParser.parse(sb.toString());
					
						JSONArray list = (JSONArray)jsonParser.parse(jsonObject.get("realTimeSMSList").toString());
						
						for(int i = 0; i < list.size(); i++) {
							JSONObject jsonData = (JSONObject) list.get(i);
							finalList.add(jsonData);					
							
						}
					
						//마지막페이지
						url = new URL("http://data.ex.co.kr/openapi/burstInfo/realTimeSms?key=9318324770&type=json&numOfRows=20&pageNo="+pageSize);
						con = (HttpURLConnection) url.openConnection(); 
						con.setConnectTimeout(5000);
						con.setReadTimeout(5000);
						con.setRequestMethod("GET");
			            con.setDoOutput(false); 

						sb = new StringBuilder();
						if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
							 
							br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
							line="";
							while ((line = br.readLine()) != null) {
								sb.append(line);
							}
							br.close();
							
							jsonParser = new JSONParser();
							jsonObject = (JSONObject) jsonParser.parse(sb.toString());
						
							list = (JSONArray)jsonParser.parse(jsonObject.get("realTimeSMSList").toString());
							
							for(int i = 0; i < list.size(); i++) {
								JSONObject jsonData = (JSONObject) list.get(i);
								finalList.add(jsonData);
							}
							
							
						} else {
							System.out.println(con.getResponseMessage());
						}
						
						result.setResultData(finalList);
						
					} else {
						System.out.println(con.getResponseMessage());
					}
					
				} else {
					System.out.println(con.getResponseMessage());
				}

				*/

				
				
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return result;
	}
	
	
	

	
	@RequestMapping(value = "/mypage", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView mypage(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) throws Exception{
		
		
		try{
				HttpSession session = request.getSession();
				Map userSessionMap = (Map)session.getAttribute("driver");
			
				if(userSessionMap != null){
					 
					Map<String,Object> map = new HashMap<String, Object>();
					map.put("dvrUserId", userSessionMap.get("dvr_user_id"));
					
					Map<String, Object> driver = driverService.selectDriver(map);
					List<Map<String, Object>> driverPic = driverFileService.selectDriverFileList(map);
					
					mav.addObject("driver", driver);
					mav.addObject("driverPic", driverPic);
					mav.addObject("customer_server_path", customer_server_path);
					mav.addObject("customer_image_path", customer_image_path);
					mav.addObject("customer_thumb_image_path", customer_thumb_image_path);
					mav.addObject("driver_server_path", driver_server_path);
					mav.addObject("driver_image_path", driver_image_path);
					mav.addObject("driver_thumb_image_path", driver_thumb_image_path);
				}
			
			}catch(Exception e){
				e.printStackTrace();
			}
		
		return mav;
	}
	
	
	@RequestMapping(value = "/notification", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView notification(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) throws Exception{
		
		
		try{
				HttpSession session = request.getSession();
				Map userSessionMap = (Map)session.getAttribute("user");
			
				if(userSessionMap != null){
					//사용자의 알람 설정정보 
					Map<String,Object> map = new HashMap<String, Object>();
					map.put("kakaoId", userSessionMap.get("kakao_id"));
//					Map<String, Object> notificationSettingMap = notificationSettingService.selectNotificationSetting(map);
//					mav.addObject("notificationSettingMap", notificationSettingMap );
				}
			
			}catch(Exception e){
				e.printStackTrace();
			}
		
		return mav;
	}
	
	
	
	@RequestMapping(value = "/notification-list", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView notificationList(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) throws Exception {

		try{
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			if(userSessionMap != null){
				//포인트 사용/적립 내역 조회
				Map<String,Object> userMap = new HashMap<String, Object>();
				userMap.put("kakaoId", userSessionMap.get("kakao_id"));
				
//				List<Map<String, Object>> pointHistoryList = pointHistoryService.selectPointHistoryList(userMap);
//				mav.addObject("pointHistoryList", pointHistoryList);				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return mav;
	}
	
	
	
	@RequestMapping(value = "/support", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView support(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) throws Exception {

		try{
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			if(userSessionMap != null){
				//포인트 사용/적립 내역 조회
				Map<String,Object> userMap = new HashMap<String, Object>();
				userMap.put("kakaoId", userSessionMap.get("kakao_id"));
				
//				List<Map<String, Object>> pointHistoryList = pointHistoryService.selectPointHistoryList(userMap);
//				mav.addObject("pointHistoryList", pointHistoryList);				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return mav;
	}
	
	
	@RequestMapping(value = "/selectSupportList", method = RequestMethod.POST)
	@ResponseBody
	public ResultApi selectAdviserList(ModelAndView mav, HttpServletRequest request, 
			HttpServletResponse response) throws Exception{
		
		ResultApi result = new ResultApi();
		 
		 try{
			 
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("user");
			String categoryCd = request.getParameter("categoryCd").toString();
			
				
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("categoryCd", categoryCd==null?"":categoryCd);
				
			//List<Map<String, Object>> adviserList = adviserService.selectAdviserList(map);
//			result.setResultData(customerSupportService.selectCustomerSupportList(map));
			
		 }catch(Exception e){
			 result.setResultCode("E001");
			 e.printStackTrace();
		 }
		
		return result;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
