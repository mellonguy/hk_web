package kr.co.carple.controller;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import kr.co.carple.service.ConsignRequestBidService;
import kr.co.carple.service.ConsignRequestService;
import kr.co.carple.service.MemberService;
import kr.co.carple.utils.ParamUtils;
import kr.co.carple.utils.WebUtils;

@Controller
@RequestMapping(value="/member")
public class MemberController {

	
	private static final Logger logger = LoggerFactory.getLogger(MemberController.class);
	
	
	@Autowired
	private ConsignRequestBidService consignRequestBidService;
	
	@Autowired
	private ConsignRequestService consignRequestService;
	
	@Autowired
	private MemberService memberService;
	
	
    @RequestMapping(value = "/viewMemberInfo", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView viewMemberInfo(Locale locale,ModelAndView mav, HttpServletRequest request,HttpServletResponse response) {
		
		try{
			
			HttpSession session = request.getSession();
			Map userSessionMap = (Map)session.getAttribute("driver");
			
			if(userSessionMap != null) {
				
				Map<String,Object> paramMap = ParamUtils.getParamToMap(request);
				
				Map<String, Object> consignRequestBid = consignRequestBidService.selectConsignRequestBid(paramMap);
				Map<String, Object> consignRequest = consignRequestService.selectConsignRequest(paramMap);
				
				if(!userSessionMap.get("dvr_user_id").toString().equals(consignRequestBid.get("crb_dvr_user_id").toString())) {
					WebUtils.messageAndRedirectUrl(mav, "해당 입찰건에 대해 권한이 없습니다. 관리자에게 문의 하세요.", "/allocation/viewConsignRequestDetail.do?corId="+paramMap.get("crdCorId").toString());
				}else {
					String memberId = consignRequest.get("cor_mem_uuid").toString();
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("memUuid", memberId);
					Map<String, Object> member = memberService.selectMember(map);
					mav.addObject("member", member);
					mav.addObject("paramMap", paramMap);
					mav.addObject("consignRequest", consignRequest);
					mav.addObject("consignRequestBid", consignRequestBid);
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return mav;
	}
	
	
	
	
	
	
	
	
	
	
}
