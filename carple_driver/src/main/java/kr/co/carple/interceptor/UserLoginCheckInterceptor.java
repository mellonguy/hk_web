package kr.co.carple.interceptor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import kr.co.carple.utils.WebUtils;

public class UserLoginCheckInterceptor extends HandlerInterceptorAdapter  {

	
	@SuppressWarnings("unchecked")
	@Override
	 public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object hadler) throws Exception {

		boolean isMobile = WebUtils.isPhone(request.getHeader("User-Agent"));			
		
		Map<String, Object> userSessionMap = (Map<String, Object>)request.getSession().getAttribute("driver");
		Map<String, Object> userMap = null;
		
		try{
			
			//System.out.println("request.getRequestURL()="+request.getRequestURL());
			if(userSessionMap == null){
				
				HttpSession session = request.getSession();
				response.sendRedirect(request.getContextPath()+"/login/nopermission.do");
				return false;
				
			}else{
				
				//디바이스 키가 없는경우 빈문자열로
				if(request.getSession().getAttribute("dvrDeviceKey") == null || request.getSession().getAttribute("dvrDeviceKey").toString().equals("")) {
					request.getSession().setAttribute("dvrDeviceKey", "");
				}
				
			}
			
		}catch(Exception e){
			System.out.println("===================preHandle======================");
			e.printStackTrace();
			return false;
		}
	
		return true;
		
	 }
	
	
	@SuppressWarnings("unchecked")
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView mav)throws Exception{
		
		
		Map<String, Object> userSessionMap = (Map<String, Object>)request.getSession().getAttribute("driver");
		Map<String, Object> userMap = null;
		
		try {
			
			if(userSessionMap != null){
				HttpSession session = request.getSession();
				
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("today", WebUtils.getNow("yyyy-MM-dd"));
			
			}
			
			
		}catch(Exception e) {
			System.out.println("===================postHandle======================");
			e.printStackTrace();
		}
		
	}



	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	private boolean isAjaxRequest(HttpServletRequest req) {
        return req.getHeader("AJAX") != null && req.getHeader("AJAX").equals(Boolean.TRUE.toString());
	}
	
	
	
	
	
	
	
	
	
	
}
