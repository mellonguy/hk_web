package kr.co.carple.mapper;

import java.util.List;
import java.util.Map;

import kr.co.carple.vo.MemberVO;

public interface MemberMapper {

	
	public List<Map<String, Object>> selectMemberList(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectMember(Map<String, Object> map) throws Exception;
	public int insertMember(MemberVO memberVO) throws Exception;
	public void deleteMember(Map<String, Object> map) throws Exception;
	
	
}
