package kr.co.carple.mapper;

import java.util.List;
import java.util.Map;

import kr.co.carple.vo.DriverVO;

public interface DriverMapper {

	
	
	public List<Map<String, Object>> selectDriverList(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectDriver(Map<String, Object> map) throws Exception;
	public int insertDriver(DriverVO driverVO) throws Exception;
	public void deleteDriver(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectDriverDuplicateChk(Map<String, Object> map) throws Exception;
	public void updateDvrDeviceKey(Map<String, Object> map) throws Exception;
	public void updateDriver(DriverVO driverVO) throws Exception;
	
	
	
	
}
