package kr.co.carple.mapper;

import java.util.List;
import java.util.Map;

import kr.co.carple.vo.ConsignRequestDetailVO;

public interface ConsignRequestDetailMapper {

	
	public List<Map<String, Object>> selectConsignRequestDetailList(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectConsignRequestDetail(Map<String, Object> map) throws Exception;
	public int insertConsignRequestDetail(ConsignRequestDetailVO consignRequestDetailVO) throws Exception;
	public void deleteConsignRequestDetail(Map<String, Object> map) throws Exception;
	public void updateConsignRequestDetailCrdStatus(Map<String, Object> map) throws Exception;
	
	
	
	
	
	
	
}
