package kr.co.carple.mapper;

import java.util.List;
import java.util.Map;

import kr.co.carple.vo.DriverFileVO;

public interface DriverFileMapper {

	public int insertDriverFile(DriverFileVO driverFileVO) throws Exception;	
	public List<Map<String, Object>> selectDriverFileList(Map<String, Object> map) throws Exception;
	public Map<String,Object>selectDriverFile(Map<String,Object>map) throws Exception;
	public void deleteDriverFile(Map<String, Object> map) throws Exception;
	
	
	
}
