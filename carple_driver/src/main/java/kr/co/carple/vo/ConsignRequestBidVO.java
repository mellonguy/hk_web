package kr.co.carple.vo;

public class ConsignRequestBidVO {

	
	private String idx;					//인덱스
	private String crbId;              //입찰 아이디
	private String corId;              //탁송 요청 아이디
	private String crbType;            //입찰유형 N:일반,
	private String crbDvrUserId;     //기사 아이디(승인 완료된 기사만 입찰 할 수 있다)
	private String crbStatus;          //입찰상태 N:입찰중,C:입찰취소,Y:낙찰(낙찰 이후에는 입찰을 취소 할 수 없다.)
	private String crbAmount;          //입찰금액
	private String crbRegDt;          //등록일
	private String crbDvrLat;         //기사 입찰시 위도
	private String crbDvrLng;         //기사 입찰시 경도
	private String crbDvrAddress;     //입찰시 기사 위치(address)
	private String crbDvrDistance;    //입찰시 기사의 위치와 상차지까지의 거리
	
	
	public String getIdx() {
		return idx;
	}
	public void setIdx(String idx) {
		this.idx = idx;
	}
	public String getCrbId() {
		return crbId;
	}
	public void setCrbId(String crbId) {
		this.crbId = crbId;
	}
	public String getCorId() {
		return corId;
	}
	public void setCorId(String corId) {
		this.corId = corId;
	}
	public String getCrbType() {
		return crbType;
	}
	public void setCrbType(String crbType) {
		this.crbType = crbType;
	}
	public String getCrbDvrUserId() {
		return crbDvrUserId;
	}
	public void setCrbDvrUserId(String crbDvrUserId) {
		this.crbDvrUserId = crbDvrUserId;
	}
	public String getCrbStatus() {
		return crbStatus;
	}
	public void setCrbStatus(String crbStatus) {
		this.crbStatus = crbStatus;
	}
	public String getCrbAmount() {
		return crbAmount;
	}
	public void setCrbAmount(String crbAmount) {
		this.crbAmount = crbAmount;
	}
	public String getCrbRegDt() {
		return crbRegDt;
	}
	public void setCrbRegDt(String crbRegDt) {
		this.crbRegDt = crbRegDt;
	}
	public String getCrbDvrLat() {
		return crbDvrLat;
	}
	public void setCrbDvrLat(String crbDvrLat) {
		this.crbDvrLat = crbDvrLat;
	}
	public String getCrbDvrLng() {
		return crbDvrLng;
	}
	public void setCrbDvrLng(String crbDvrLng) {
		this.crbDvrLng = crbDvrLng;
	}
	public String getCrbDvrAddress() {
		return crbDvrAddress;
	}
	public void setCrbDvrAddress(String crbDvrAddress) {
		this.crbDvrAddress = crbDvrAddress;
	}
	public String getCrbDvrDistance() {
		return crbDvrDistance;
	}
	public void setCrbDvrDistance(String crbDvrDistance) {
		this.crbDvrDistance = crbDvrDistance;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
