package kr.co.carple.vo;

public class ConsignRequestAcceptVO {

	
	private String idx;					//인덱스
	private String craId;              //낙찰 아이디
	private String corId;              //탁송 요청 아이디
	private String crbId;              //입찰 아이디
	private String craCrbAmount;      //낙찰금액
	private String craRegDt;          //낙찰시간
	
	
	
	public String getIdx() {
		return idx;
	}
	public void setIdx(String idx) {
		this.idx = idx;
	}
	public String getCraId() {
		return craId;
	}
	public void setCraId(String craId) {
		this.craId = craId;
	}
	public String getCorId() {
		return corId;
	}
	public void setCorId(String corId) {
		this.corId = corId;
	}
	public String getCrbId() {
		return crbId;
	}
	public void setCrbId(String crbId) {
		this.crbId = crbId;
	}
	public String getCraCrbAmount() {
		return craCrbAmount;
	}
	public void setCraCrbAmount(String craCrbAmount) {
		this.craCrbAmount = craCrbAmount;
	}
	public String getCraRegDt() {
		return craRegDt;
	}
	public void setCraRegDt(String craRegDt) {
		this.craRegDt = craRegDt;
	}
	
	
	
	
	
	
}
