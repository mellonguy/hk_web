package kr.co.carple.vo;

public class DriverFileVO {

	
	

	private String idx;					//아이디
	private String drfId;              //파일 아이디
	private String drfDriverId;       //기사 아이디
	private String drfFilePath;       //파일 경로
	private String drfFileNm;         //파일명
	private String drfCategoryType;   //카테고리 구분
	private String drfRegDt;          //등록일
	public String getIdx() {
		return idx;
	}
	public void setIdx(String idx) {
		this.idx = idx;
	}
	public String getDrfId() {
		return drfId;
	}
	public void setDrfId(String drfId) {
		this.drfId = drfId;
	}
	public String getDrfDriverId() {
		return drfDriverId;
	}
	public void setDrfDriverId(String drfDriverId) {
		this.drfDriverId = drfDriverId;
	}
	public String getDrfFilePath() {
		return drfFilePath;
	}
	public void setDrfFilePath(String drfFilePath) {
		this.drfFilePath = drfFilePath;
	}
	public String getDrfFileNm() {
		return drfFileNm;
	}
	public void setDrfFileNm(String drfFileNm) {
		this.drfFileNm = drfFileNm;
	}
	public String getDrfCategoryType() {
		return drfCategoryType;
	}
	public void setDrfCategoryType(String drfCategoryType) {
		this.drfCategoryType = drfCategoryType;
	}
	public String getDrfRegDt() {
		return drfRegDt;
	}
	public void setDrfRegDt(String drfRegDt) {
		this.drfRegDt = drfRegDt;
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
