package kr.co.carple.vo;

public class ConsignRequestVO {
	
	private String idx;
	private String corId;				//탁송 요청 아이디
	private String corMemUuid;        //회원 아이디
	private String corCarCnt;           //차량 대수(1~10까지 숫자만 입력)
	private String corCarrierType;      //탁송차량 구분 S:세이프티로더,C:5톤캐리어,T:추레라,F:풀카,B:박스카,N:상관없음
	private String corBatchYn;          //일괄 여부(1대 이상인경우 반드시 설정 되어야 함 N:개별등록, Y:일괄등록) 일괄 등록된 배차건의 경우 상,하차지 정보를 하나만 입력 해도 자동으로 입력 되도록 함.
	private String corTitle;            //탁송요청 제목
	private String corStatus;           //탁송요청 상태 W:입찰 대기중(작성중) I:입찰 진행중(작성완료),Y:낙찰,D:유찰,Q:재입찰,G:재입찰 후 유찰,K:재입찰 유찰 후 입찰취소,C:입찰취소,F:낙찰후완료(낙찰 된 경매건에 대하여 취소 할 수 없도록 하여야 할것 같음.)
	private String corDepartureDt;      //출발일
	private String corDepartureTime;    //출발요청시간
	private String corRegDt;            //등록일
	private String corBidDecisionType;  //낙찰결정 방식 PR:우선순위결정,OA:선착순결정
	private String corBidDecisionTime;  //낙찰결정 시간 단위:분
	
	
	public String getIdx() {
		return idx;
	}
	public void setIdx(String idx) {
		this.idx = idx;
	}
	public String getCorId() {
		return corId;
	}
	public void setCorId(String corId) {
		this.corId = corId;
	}
	public String getCorMemUuid() {
		return corMemUuid;
	}
	public void setCorMemUuid(String corMemUuid) {
		this.corMemUuid = corMemUuid;
	}
	public String getCorCarCnt() {
		return corCarCnt;
	}
	public void setCorCarCnt(String corCarCnt) {
		this.corCarCnt = corCarCnt;
	}
	public String getCorCarrierType() {
		return corCarrierType;
	}
	public void setCorCarrierType(String corCarrierType) {
		this.corCarrierType = corCarrierType;
	}
	public String getCorBatchYn() {
		return corBatchYn;
	}
	public void setCorBatchYn(String corBatchYn) {
		this.corBatchYn = corBatchYn;
	}
	public String getCorTitle() {
		return corTitle;
	}
	public void setCorTitle(String corTitle) {
		this.corTitle = corTitle;
	}
	public String getCorStatus() {
		return corStatus;
	}
	public void setCorStatus(String corStatus) {
		this.corStatus = corStatus;
	}
	public String getCorDepartureDt() {
		return corDepartureDt;
	}
	public void setCorDepartureDt(String corDepartureDt) {
		this.corDepartureDt = corDepartureDt;
	}
	public String getCorDepartureTime() {
		return corDepartureTime;
	}
	public void setCorDepartureTime(String corDepartureTime) {
		this.corDepartureTime = corDepartureTime;
	}
	public String getCorRegDt() {
		return corRegDt;
	}
	public void setCorRegDt(String corRegDt) {
		this.corRegDt = corRegDt;
	}
	public String getCorBidDecisionType() {
		return corBidDecisionType;
	}
	public void setCorBidDecisionType(String corBidDecisionType) {
		this.corBidDecisionType = corBidDecisionType;
	}
	public String getCorBidDecisionTime() {
		return corBidDecisionTime;
	}
	public void setCorBidDecisionTime(String corBidDecisionTime) {
		this.corBidDecisionTime = corBidDecisionTime;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
