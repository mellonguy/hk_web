package kr.co.carple.service.impl;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import kr.co.carple.mapper.DriverMapper;
import kr.co.carple.service.DriverFileService;
import kr.co.carple.service.DriverService;
import kr.co.carple.service.FileService;
import kr.co.carple.service.FileUploadService;
import kr.co.carple.utils.BaseAppConstants;
import kr.co.carple.utils.ResultApi;
import kr.co.carple.vo.DriverFileVO;
import kr.co.carple.vo.DriverVO;

@Service("driverService")
public class DriverServiceImpl implements DriverService{

	
	@Value("#{appProp['dbEncKey']}")
	private String dbEncKey;
	    
	@Value("#{appProp['upload.base.path']}")
    private String rootDir;
	private String subDir;
	
	@Resource(name="driverMapper")
	private DriverMapper driverMapper;
	
	@Autowired
	private FileUploadService fileUploadService;
		
	@Autowired
	private FileService fileService;
	
	@Autowired
	private DriverFileService driverFileService;
	
	
	public List<Map<String, Object>> selectDriverList(Map<String, Object> map) throws Exception{
		return driverMapper.selectDriverList(map);		
	}
	
	public Map<String, Object> selectDriver(Map<String, Object> map) throws Exception{
		return driverMapper.selectDriver(map);
	}
	
	
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor={Exception.class})
	public ResultApi insertDriver(HttpServletRequest request, HttpSession session, DriverVO driverVO) throws Exception{
		
		ResultApi resultApi = new ResultApi();
		
		try {
			
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
			List<MultipartFile> fileList = multipartRequest.getFiles("driverPicture");
			
			if(fileList != null && fileList.size() > 0){
				
				//for(MultipartFile mFile : fileList){
				for(int i = 0; i < fileList.size(); i++){
					
					MultipartFile mFile = fileList.get(i);
					if(mFile != null && mFile.getSize() > 0){
						subDir = "driverPic";
						Map<String, Object> fileMap = fileUploadService.upload(rootDir, subDir, mFile);
						DriverFileVO driverFileVO = new DriverFileVO();
						if(i == 0){//사업자등록증
							driverFileVO.setDrfCategoryType("businessLicense");								
						}else if(i == 1){//보험납입증명서
							driverFileVO.setDrfCategoryType("insurance");
						}else if(i == 2){//화물운송자격
							driverFileVO.setDrfCategoryType("transportLicense");
						}else if(i == 3){//자동차운전면허
							driverFileVO.setDrfCategoryType("driverLicense");
						}else if(i == 4){//배경사진
							driverFileVO.setDrfCategoryType("background");
						}else if(i == 5){//프로필사진
							driverFileVO.setDrfCategoryType("profile");
						}
						driverFileVO.setDrfDriverId(driverVO.getDvrUserId());
						driverFileVO.setDrfFileNm(fileMap.get("ORG_NAME").toString());
						driverFileVO.setDrfFilePath(fileMap.get("SAVE_NAME").toString());
						driverFileVO.setDrfId("DRF"+UUID.randomUUID().toString().replaceAll("-", ""));
	    				driverFileService.insertDriverFile(driverFileVO);
					}
					
				}
				
			}
			
			driverVO.setDbEncKey(dbEncKey);
			driverVO.setDvrApproveStatus("N");
			driverVO.setDvrStatus("0");
			driverVO.setDvrGrade("N");
			driverVO.setDvrLoginEnv("aos");
			int status = driverMapper.insertDriver(driverVO);
			
		}catch(Exception e) {
			e.printStackTrace();
			resultApi.setResultCode("E0004");
			throw new Exception();
		}
		
		return resultApi;
	}
	
	public void deleteDriver(Map<String, Object> map) throws Exception{
		driverMapper.deleteDriver(map);		
	}
	
	public Map<String, Object> selectDriverDuplicateChk(Map<String, Object> map) throws Exception{
		return driverMapper.selectDriverDuplicateChk(map);
	}
	
	public ResultApi updateDvrDeviceKey(Map<String, Object> map) throws Exception{
		
		ResultApi result = new ResultApi();
		
		try {
			
			driverMapper.updateDvrDeviceKey(map);
			
			
		}catch(Exception e) {
			
			e.printStackTrace();
			result.setResultCode("E0004");
			
		}
		
		return result;
	
	}
	
	
public ResultApi updateDriver(DriverVO driverVO) throws Exception{
		
		ResultApi result = new ResultApi();
		
		try {
			
			driverMapper.updateDriver(driverVO);
			
		}catch(Exception e) {
			
			e.printStackTrace();
			result.setResultCode("E0004");
			
		}
		
		return result;
	
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
