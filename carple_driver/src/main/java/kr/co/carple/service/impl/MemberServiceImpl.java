package kr.co.carple.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.carple.mapper.MemberMapper;
import kr.co.carple.service.MemberService;
import kr.co.carple.vo.MemberVO;

@Service("memberService")
public class MemberServiceImpl implements MemberService{

	@Resource(name="memberMapper")
	private MemberMapper memberMapper;
	
	
	public List<Map<String, Object>> selectMemberList(Map<String, Object> map) throws Exception{
		return memberMapper.selectMemberList(map);
	}
	
	public Map<String, Object> selectMember(Map<String, Object> map) throws Exception{
		return memberMapper.selectMember(map);
	}
	
	public int insertMember(MemberVO memberVO) throws Exception{
		return memberMapper.insertMember(memberVO);
	}
	
	public void deleteMember(Map<String, Object> map) throws Exception{
		memberMapper.deleteMember(map);
	}
	
	
	
}
