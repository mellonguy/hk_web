package kr.co.carple.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import kr.co.carple.mapper.ConsignRequestMapper;
import kr.co.carple.service.ConsignRequestDetailService;
import kr.co.carple.service.ConsignRequestService;
import kr.co.carple.service.FcmService;
import kr.co.carple.service.MemberService;
import kr.co.carple.timer.StatusUpdateTimerTask;
import kr.co.carple.utils.BaseAppConstants;
import kr.co.carple.utils.ResultApi;
import kr.co.carple.vo.ConsignRequestDetailVO;
import kr.co.carple.vo.ConsignRequestVO;

@Service("consignRequestService")
public class ConsignRequestServiceImpl implements ConsignRequestService{
	
	
	
	@Resource(name="consignRequestMapper")
	private ConsignRequestMapper consignRequestMapper;
	
	
	@Autowired
	private ConsignRequestDetailService consignRequestDetailService;
	
	@Autowired
	private MemberService memService;
	
	@Autowired
	private FcmService fcmService;
	
	public List<Map<String, Object>> selectConsignRequestList(Map<String, Object> map) throws Exception{
		return consignRequestMapper.selectConsignRequestList(map);
	}
	
	public Map<String, Object> selectConsignRequest(Map<String, Object> map) throws Exception{
		return consignRequestMapper.selectConsignRequest(map);
	}
	
	
	
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor={Exception.class})
	public ResultApi insertConsignRequest(HttpSession session, String[] contentArr) throws Exception{
		
		ResultApi result = new ResultApi();
		
		try {
		
			Map userSessionMap = (Map)session.getAttribute("user");
			
			if(userSessionMap != null) {
						
				int carCount = Integer.valueOf(contentArr[4]);
				ConsignRequestVO consignRequestVO = new ConsignRequestVO();
				String corId ="COR"+UUID.randomUUID().toString().replaceAll("-","");
	
				consignRequestVO.setCorId(corId);
				consignRequestVO.setCorMemUuid(userSessionMap.get("mem_uuid").toString());
				consignRequestVO.setCorBatchYn("N");
				consignRequestVO.setCorStatus(BaseAppConstants.BID_STATUS_WAIT);
				consignRequestVO.setCorDepartureDt(contentArr[0]); //출발일
				consignRequestVO.setCorDepartureTime(contentArr[1]); //출발시간
				consignRequestVO.setCorTitle(contentArr[2]); //제목
				
				if(contentArr[3].equals(BaseAppConstants.CARRIER_TYPE_SL)) {
					consignRequestVO.setCorCarrierType(BaseAppConstants.CARRIER_CODE_SL);
				}else if(contentArr[3].equals(BaseAppConstants.CARRIER_TYPE_CA)) {
					consignRequestVO.setCorCarrierType(BaseAppConstants.CARRIER_CODE_CA);
				}else if(contentArr[3].equals(BaseAppConstants.CARRIER_TYPE_RD)) {
					consignRequestVO.setCorCarrierType(BaseAppConstants.CARRIER_CODE_RD);
				}else if(contentArr[3].equals(BaseAppConstants.CARRIER_TYPE_TR)) {
					consignRequestVO.setCorCarrierType(BaseAppConstants.CARRIER_CODE_TR);
				}else if(contentArr[3].equals(BaseAppConstants.CARRIER_TYPE_FC)) {
					consignRequestVO.setCorCarrierType(BaseAppConstants.CARRIER_CODE_FC);
				}else if(contentArr[3].equals(BaseAppConstants.CARRIER_TYPE_BC)) {
					consignRequestVO.setCorCarrierType(BaseAppConstants.CARRIER_CODE_BC);
				}else if(contentArr[3].equals(BaseAppConstants.CARRIER_TYPE_NE)) {
					consignRequestVO.setCorCarrierType(BaseAppConstants.CARRIER_CODE_NE);
				}
				consignRequestVO.setCorCarCnt(contentArr[4]);
				if(contentArr[5].equals(BaseAppConstants.BID_DECISION_TYPE_PR)) {
					consignRequestVO.setCorBidDecisionType(BaseAppConstants.BID_DECISION_CODE_PR);
				}else if(contentArr[5].equals(BaseAppConstants.BID_DECISION_TYPE_OA)) {
					consignRequestVO.setCorBidDecisionType(BaseAppConstants.BID_DECISION_CODE_OA);
				}
				if(contentArr[6].equals(BaseAppConstants.BID_DECISION_TIME_10)) {
					consignRequestVO.setCorBidDecisionTime(BaseAppConstants.BID_DECISION_CODE_10);
				}else if(contentArr[6].equals(BaseAppConstants.BID_DECISION_TIME_20)) {
					consignRequestVO.setCorBidDecisionTime(BaseAppConstants.BID_DECISION_CODE_20);
				}else if(contentArr[6].equals(BaseAppConstants.BID_DECISION_TIME_30)) {
					consignRequestVO.setCorBidDecisionTime(BaseAppConstants.BID_DECISION_CODE_30);
				}
				consignRequestMapper.insertConsignRequest(consignRequestVO);
				for(int v =0; v<carCount; v++ ) {
					int gap = 7;				
					ConsignRequestDetailVO consignRequestDetailVO = new ConsignRequestDetailVO();
					consignRequestDetailVO.setCrdCorId(corId);
					consignRequestDetailVO.setCrdCarMaker(contentArr[(v*10)+gap]);	//제조사
					consignRequestDetailVO.setCrdCarKind(contentArr[(v*10)+1+gap]);	//차종
					consignRequestDetailVO.setCrdCarTrim(contentArr[(v*10)+2+gap]);	//트림
					consignRequestDetailVO.setCrdCarNum(contentArr[(v*10)+3+gap]);	//차량번호
					consignRequestDetailVO.setCrdCarIdNum(contentArr[(v*10)+4+gap]);//차대번호
					if(contentArr[(v*10)+5+gap].equals(BaseAppConstants.CAR_STATUS_TYPE_NC)) {
						consignRequestDetailVO.setCrdCarType(BaseAppConstants.CAR_STATUS_CODE_NC);
					}else if(contentArr[(v*10)+5+gap].equals(BaseAppConstants.CAR_STATUS_TYPE_OC)) {
						consignRequestDetailVO.setCrdCarType(BaseAppConstants.CAR_STATUS_CODE_OC);
					}
					consignRequestDetailVO.setCrdDeparture(contentArr[(v*10)+6+gap]);								//출발지
					consignRequestDetailVO.setCrdDepartureLatlng(contentArr[(v*10)+7+gap]);						//출발지 좌표
					consignRequestDetailVO.setCrdDeparturePersonInCharge("");			//출발지 담당자
					consignRequestDetailVO.setCrdDeparturePhone("");						//출발지 연락처
					consignRequestDetailVO.setCrdArrival(contentArr[(v*10)+8+gap]);										//도착지
					consignRequestDetailVO.setCrdArrivalLatlng(contentArr[(v*10)+9+gap]);								//도착지 좌표
					consignRequestDetailVO.setCrdArrivalPersonInCharge("");					//도착지 담당자
					consignRequestDetailVO.setCrdArrivalPhone("");								//도착지 연락처
					consignRequestDetailVO.setCrdDistance("");									//출발지와 도착지의 거리
					consignRequestDetailVO.setCrdSignificantData("");							//중요사함.
					consignRequestDetailService.insertConsignRequestDetail(consignRequestDetailVO);
				}
	
				//경매건이 전부 입력 되고 10분이 지난 후에 경매 대기 상태에서 진행중으로 변경 한다.
				Timer test = new Timer();
				long minute = 60*1000;
				StatusUpdateTimerTask task = new StatusUpdateTimerTask(session,corId,consignRequestVO.getCorStatus());	
				test.schedule(task,minute*BaseAppConstants.STATUS_CHANGE_WW_TO_II);

			}else {
				
				result.setResultCode("E000");
				result.setResultMsg("로그인이 되어 있지 않습니다.");
			}
			
			
		}catch(Exception e) {
			e.printStackTrace();
			result.setResultCode("E001");
			throw new Exception();
		}
		
		
		
		return result;
	}
	
	public void deleteConsignRequest(Map<String, Object> map) throws Exception{
		consignRequestMapper.deleteConsignRequest(map);		
	}
	
	public ResultApi updateCorStatus(Map<String, Object> map) throws Exception{
		
		ResultApi result = new ResultApi();
		
		try {
			
			System.out.println("updateCorStatus 실행됨");
						
			//완료 처리가 되는 경우 알림 발송
			if(map.get("corStatus").toString().equals(BaseAppConstants.BID_STATUS_SUCCESS)) {
				
				Map<String, Object> consignRequest = this.selectConsignRequest(map);
				
				//null일리는 없겠지만.....
				if(consignRequest != null) {
					
					Map<String, Object> memSelMap = new HashMap<String, Object>();
					memSelMap.put("memUuid", consignRequest.get("cor_mem_uuid").toString());
					Map<String, Object> memberMap = memService.selectMember(memSelMap);
						
					Map<String, Object> sendMessageMap = new HashMap<String, Object>();
					
					sendMessageMap.put("title", "낙찰된 경매가 탁송 완료 되었습니다.");
					sendMessageMap.put("body", "견적 제목:"+consignRequest.get("cor_title").toString());
					sendMessageMap.put("fcm_token", memberMap.get("mem_device_key") != null && !memberMap.get("mem_device_key").toString().equals("") ? memberMap.get("mem_device_key").toString() : "");
					sendMessageMap.put("device_os", memberMap.get("mem_login_env").toString());
					
					if(!sendMessageMap.get("fcm_token").toString().equals("")) {
						fcmService.fcmSendMessage(sendMessageMap);	
					}
					
				}
				
			}
			
			consignRequestMapper.updateCorStatus(map);	
			
		}catch(Exception e) {
			result.setResultCode("E003");
			e.printStackTrace();
			
		}
		
		return result;
	}
	
	
	public List<Map<String, Object>> selectConsignRequestJoinList(Map<String, Object> map) throws Exception{
		return consignRequestMapper.selectConsignRequestJoinList(map);
	}
	
	
	public Map<String, Object> selectConsignRequestForNextProcess(Map<String, Object> map) throws Exception{
		return consignRequestMapper.selectConsignRequestForNextProcess(map);
	}
	
	public Map<String, Object> selectConsignRequestByCrdId(Map<String, Object> map) throws Exception{
		return consignRequestMapper.selectConsignRequestByCrdId(map);
	}
	
	
	

}
