package kr.co.carple.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import kr.co.carple.mapper.ConsignRequestBidMapper;
import kr.co.carple.service.ConsignRequestBidService;
import kr.co.carple.service.ConsignRequestService;
import kr.co.carple.service.FcmService;
import kr.co.carple.service.MemberService;
import kr.co.carple.utils.BaseAppConstants;
import kr.co.carple.utils.ResultApi;
import kr.co.carple.vo.ConsignRequestBidVO;

@Service("consignRequestBidService")
public class ConsignRequestBidServiceImpl implements ConsignRequestBidService{

	
	@Resource(name="consignRequestBidMapper")
	private ConsignRequestBidMapper consignRequestBidMapper;
	
	@Autowired
	private ConsignRequestService consignRequestService;
	
	@Autowired
	private FcmService fcmService;
	
	@Autowired
	private MemberService memService;
	
	
	public List<Map<String, Object>> selectConsignRequestBidList(Map<String, Object> map) throws Exception{
		return consignRequestBidMapper.selectConsignRequestBidList(map);
	}
	
	public Map<String, Object> selectConsignRequestBid(Map<String, Object> map) throws Exception{
		return consignRequestBidMapper.selectConsignRequestBid(map);
	}
	
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor={Exception.class})
	public ResultApi insertConsignRequestBid(Map<String,Object> map) throws Exception{
		
		ResultApi result = new ResultApi();
		try {
			
			ConsignRequestBidVO consignRequestBidVO = new ConsignRequestBidVO();
			String crbId ="CRB"+UUID.randomUUID().toString().replaceAll("-","");
			consignRequestBidVO.setCrbId(crbId);
			consignRequestBidVO.setCorId(map.get("corId").toString());
			consignRequestBidVO.setCrbDvrUserId(map.get("dvrUserId").toString());
			consignRequestBidVO.setCrbType("N");
			consignRequestBidVO.setCrbStatus("N");
			consignRequestBidVO.setCrbAmount(map.get("amount").toString());
			consignRequestBidVO.setCrbDvrLat(map.get("crbDvrLat") != null && !map.get("crbDvrLat").toString().equals("") ? map.get("crbDvrLat").toString():"");
			consignRequestBidVO.setCrbDvrLng(map.get("crbDvrLng") != null && !map.get("crbDvrLng").toString().equals("") ? map.get("crbDvrLng").toString():"");
			consignRequestBidVO.setCrbDvrAddress(map.get("crbDvrAddress") != null && !map.get("crbDvrAddress").toString().equals("") ? map.get("crbDvrAddress").toString():"");
			consignRequestBidVO.setCrbDvrDistance("");
			
			Map<String, Object> consignRequest = consignRequestService.selectConsignRequest(map);
			
			//기존에 입력 되어 있는 최저가 입찰건
			Map<String, Object> lowestBid = this.selectConsignRequestLowestBid(map);
			
			//입찰 진행중인건에 대해서만 입찰을 할 수 있도록 한다.
			if(consignRequest != null && !consignRequest.get("cor_status").toString().equals(BaseAppConstants.BID_STATUS_ING)) {
				result.setResultCode("E012");
				result.setResultMsg("입찰 진행중인 건에 대해서만 입찰이 가능 합니다.");
			}else {
				
				
				//입찰 진행중인 건에 대해....
				List<Map<String, Object>> allBidList = this.selectConsignRequestBidList(map);
				map.put("validation", "Y");
				List<Map<String, Object>> bidList = this.selectConsignRequestBidList(map);
				
				if(bidList.size() > 0) {
					
					//같은건에 5회까지만 참여 할 수 있도록 한다.
					if(allBidList.size() >= BaseAppConstants.BID_MAX_CNT) {
						result.setResultCode("E011");
						result.setResultMsg("동일한 건에 참여 할 수 있는 최대 입찰 건수는 "+String.valueOf(BaseAppConstants.BID_MAX_CNT)+" 건 입니다.");
					}else {
						
						//입찰가 오름차순이므로 가장 마지막 리스트를 가져온다.
						Map<String, Object> bidMap = bidList.get(bidList.size()-1);
						int amount = Integer.parseInt(bidMap.get("crb_amount").toString().replaceAll(",", ""));
						
						//같은 입찰건에 참여 하는경우 기존의 입찰가보다 높거나 같은 금액은 작성 할 수 없도록 한다. 
						if(Integer.parseInt(map.get("amount").toString()) >= amount){
							result.setResultCode("E010");
							result.setResultMsg("기존에 참여한 입찰가보다 높거나 같은 금액으로 참여 할 수 없습니다.");
						}else {
							consignRequestBidMapper.insertConsignRequestBid(consignRequestBidVO);		
						}	
						
					}
					
				}else {
					consignRequestBidMapper.insertConsignRequestBid(consignRequestBidVO);
				}
				
				//정상적으로 입찰액이 입력 된 경우이고 기존에 입력되어 있던 입찰액보다 낮은 금액인 경우 해당 배차건을 등록한 고객에게 푸시 알림을 보낸다.
				if(lowestBid == null || (Integer.parseInt(map.get("amount").toString()) < Integer.parseInt(lowestBid.get("crb_amount").toString()))) {
				
					Map<String, Object> memSelMap = new HashMap<String, Object>();
					memSelMap.put("memUuid", consignRequest.get("cor_mem_uuid").toString());
					Map<String, Object> memberMap = memService.selectMember(memSelMap);
					
					if(memberMap != null && memberMap.get("mem_login_env") != null && !memberMap.get("mem_login_env").toString().equals("")) {
						
						Map<String, Object> sendMessageMap = new HashMap<String, Object>();
						sendMessageMap.put("title", "최저가 입찰가가 등록 되었습니다.");
						sendMessageMap.put("body", "견적 제목:"+consignRequest.get("cor_title").toString());
						sendMessageMap.put("fcm_token", memberMap.get("mem_device_key").toString());
						sendMessageMap.put("device_os", memberMap.get("mem_login_env").toString());
						fcmService.fcmSendMessage(sendMessageMap);
						
					}
					
				}
				
			}
					
		
		}catch(Exception e) {
			e.printStackTrace();	
			result.setResultCode("E000");
			throw new Exception();
		}
		return result;
	}
	
	public void deleteConsignRequestBid(Map<String, Object> map) throws Exception{
		consignRequestBidMapper.deleteConsignRequestBid(map);
	}
	
	
	
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor={Exception.class})
	public ResultApi updateConsignRequestBidCrbStatus(Map<String, Object> map) throws Exception{
		
		ResultApi result = new ResultApi();
		try {
			
			consignRequestBidMapper.updateConsignRequestBidCrbStatus(map);		
			
		}catch(Exception e) {
			result.setResultCode("E001");
			e.printStackTrace();
			throw new Exception();
		}
		return result;
	}
	
	
	public Map<String, Object> selectConsignRequestLowestBid(Map<String, Object> map) throws Exception{
		return consignRequestBidMapper.selectConsignRequestLowestBid(map);
		
		
	}
	
	
	
	
	
	
}
