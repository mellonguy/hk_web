package kr.co.carple.service.impl;

import java.io.File;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import kr.co.carple.mapper.DriverFileMapper;
import kr.co.carple.service.DriverFileService;
import kr.co.carple.service.FileUploadService;
import kr.co.carple.utils.ResultApi;
import kr.co.carple.vo.DriverFileVO;

@Service("driverFileService")
public class DriverFileServiceImpl implements DriverFileService{

	
	@Resource(name="driverFileMapper")
	private DriverFileMapper driverFileMapper;
	
	
	@Autowired
	private FileUploadService fileUploadService;
	
	
	public ResultApi insertDriverFile(DriverFileVO driverFileVO) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			driverFileMapper.insertDriverFile(driverFileVO);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return result; 
		
	}
	public ResultApi insertDriverFile(String rootDir, String subDir,String contentId,HttpServletRequest request) throws Exception{
		
		ResultApi result = new ResultApi();
		try{
			
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
	    	List<MultipartFile> fileList = multipartRequest.getFiles("bbsFile");
	    	List<MultipartFile> driverFileList = multipartRequest.getFiles("driverFile");
	    	List<MultipartFile> driverSealFileList = multipartRequest.getFiles("driverSealFile");
	    	if(fileList != null && fileList.size() > 0){
	    		result.setResultCode("0000");
	    		for(MultipartFile mFile : fileList){	
	    			if(mFile != null && mFile.getSize() > 0){
	    				DriverFileVO driverFileVO = new DriverFileVO();
	    				Map<String, Object> fileMap = fileUploadService.upload(rootDir, subDir, mFile);
	    				
	    		/*		fileVO.setContentId(contentId);
	    				fileVO.setCategoryType(subDir);
	    				fileVO.setFileId("FLE"+UUID.randomUUID().toString().replaceAll("-", ""));
	    				fileVO.setFileNm(fileMap.get("ORG_NAME").toString());
	    				fileVO.setFilePath(fileMap.get("SAVE_NAME").toString());*/
	    				driverFileMapper.insertDriverFile(driverFileVO);		
	    				
	    			}
	    		}
	    	}else{
	    		result.setResultCode("1111"); 	//file 없음
	    	}
	    	
	    	if(driverFileList != null && driverFileList.size() > 0) {
	    		result.setResultCode("0000");
	    		for(MultipartFile mFile : driverFileList){	
	    			if(mFile != null && mFile.getSize() > 0){
	    				DriverFileVO driverFileVO = new DriverFileVO();
	    				Map<String, Object> fileMap = fileUploadService.upload(rootDir, subDir, mFile);
	    		/*		fileVO.setContentId(contentId);
	    				fileVO.setCategoryType("driverPic");
	    				fileVO.setFileId("FLE"+UUID.randomUUID().toString().replaceAll("-", ""));
	    				fileVO.setFileNm(fileMap.get("ORG_NAME").toString());
	    				fileVO.setFilePath(fileMap.get("SAVE_NAME").toString());*/
	    				driverFileMapper.insertDriverFile(driverFileVO);		
	    			}
	    		}
	    	}else{
	    		result.setResultCode("1111"); 	//file 없음
	    	}
	    	
	    	
	    	if(driverSealFileList != null && driverSealFileList.size() > 0) {
	    		result.setResultCode("0000");
	    		for(MultipartFile mFile : driverSealFileList){	
	    			if(mFile != null && mFile.getSize() > 0){
	    				DriverFileVO driverFileVO = new DriverFileVO();
	    				Map<String, Object> fileMap = fileUploadService.upload(rootDir, subDir, mFile);
	    				/*fileVO.setContentId(contentId);
	    				fileVO.setCategoryType("driverSeal");
	    				fileVO.setFileId("FLE"+UUID.randomUUID().toString().replaceAll("-", ""));
	    				fileVO.setFileNm(fileMap.get("ORG_NAME").toString());
	    				fileVO.setFilePath(fileMap.get("SAVE_NAME").toString());*/
	    				driverFileMapper.insertDriverFile(driverFileVO);		
	    			}
	    		}
	    	}else{
	    		result.setResultCode("1111"); 	//file 없음
	    	}
	    	
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return result; 
		
		
		
	}
	
	public List<Map<String, Object>> selectDriverFileList(Map<String, Object> map) throws Exception{
		return driverFileMapper.selectDriverFileList(map);
		
	}
	
	public void deleteDriverFile(Map<String, Object> map) throws Exception{
		driverFileMapper.deleteDriverFile(map);
	}
	
	public ResultApi insertDriverFile(String rootDir, String subDir,File file) throws Exception{
		
		ResultApi result = new ResultApi();
		try {
			
			
			
			
		}catch(Exception e) {
			
			e.printStackTrace();
			
		}
		
		return result;
		
		
	}
	
	public Map<String,Object>selectDriverFile(Map<String,Object>map) throws Exception{
		return driverFileMapper.selectDriverFile(map);
	}
	
	
	
	
	
	
	
	
	
}
