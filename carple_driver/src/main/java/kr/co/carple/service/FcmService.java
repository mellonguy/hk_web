package kr.co.carple.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.ui.Model;

public interface FcmService {

	
	public String fcmSendMessage(Map<String, Object> map)throws Exception;
	
	
	
}
