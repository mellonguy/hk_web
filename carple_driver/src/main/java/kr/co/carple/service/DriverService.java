package kr.co.carple.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import kr.co.carple.utils.ResultApi;
import kr.co.carple.vo.DriverVO;

public interface DriverService {

	
	
	
	public List<Map<String, Object>> selectDriverList(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectDriver(Map<String, Object> map) throws Exception;
	public ResultApi insertDriver(HttpServletRequest request, HttpSession session, DriverVO driverVO) throws Exception;
	public void deleteDriver(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectDriverDuplicateChk(Map<String, Object> map) throws Exception;
	public ResultApi updateDvrDeviceKey(Map<String, Object> map) throws Exception;
	public ResultApi updateDriver(DriverVO driverVO) throws Exception;
	
	
}
