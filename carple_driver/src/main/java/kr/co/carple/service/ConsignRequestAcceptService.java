package kr.co.carple.service;

import java.util.List;
import java.util.Map;

import kr.co.carple.vo.ConsignRequestAcceptVO;

public interface ConsignRequestAcceptService {

	public List<Map<String, Object>> selectConsignRequestAcceptList(Map<String, Object> map) throws Exception;
	public Map<String, Object> selectConsignRequestAccept(Map<String, Object> map) throws Exception;
	public int insertConsignRequestDetail(ConsignRequestAcceptVO consignRequestAcceptVO) throws Exception;
	public void deleteConsignRequestAccept(Map<String, Object> map) throws Exception;
	
	
	
}
