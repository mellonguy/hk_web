package kr.co.carple.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.carple.mapper.ConsignRequestDetailMapper;
import kr.co.carple.service.ConsignRequestDetailService;
import kr.co.carple.service.ConsignRequestService;
import kr.co.carple.service.FcmService;
import kr.co.carple.service.MemberService;
import kr.co.carple.utils.ResultApi;
import kr.co.carple.vo.ConsignRequestDetailVO;

@Service("consignRequestDetail")
public class ConsignRequestDetailServiceImpl implements ConsignRequestDetailService{

	
	@Resource(name="consignRequestDetailMapper")
	private ConsignRequestDetailMapper consignRequestDetailMapper;
	
	
	@Autowired
	private ConsignRequestService consignRequestService;
	
	@Autowired
	private MemberService memService;
	
	@Autowired
	private FcmService fcmService;
	
	public List<Map<String, Object>> selectConsignRequestDetailList(Map<String, Object> map) throws Exception{
		return consignRequestDetailMapper.selectConsignRequestDetailList(map);
	}
	
	public Map<String, Object> selectConsignRequestDetail(Map<String, Object> map) throws Exception{
		return consignRequestDetailMapper.selectConsignRequestDetail(map);
	}
	
	public int insertConsignRequestDetail(ConsignRequestDetailVO consignRequestDetailVO) throws Exception{
		return consignRequestDetailMapper.insertConsignRequestDetail(consignRequestDetailVO);
	}
	
	public void deleteConsignRequestDetail(Map<String, Object> map) throws Exception{
		consignRequestDetailMapper.deleteConsignRequestDetail(map);
	}
	
	public ResultApi updateConsignRequestDetailCrdStatus(Map<String, Object> map) throws Exception{
		
		ResultApi result = new ResultApi();
		
		try {
			
			
			
			//경매건의 일부 내역이 완료 처리 되는 경우 알림을 보내 준다.
			Map<String, Object> consignRequest = consignRequestService.selectConsignRequestByCrdId(map);
			Map<String, Object> consignRequestDetail = this.selectConsignRequestDetail(map);
			
			//null일리는 없겠지만.....
			if(consignRequest != null) {
				
				Map<String, Object> memSelMap = new HashMap<String, Object>();
				memSelMap.put("memUuid", consignRequest.get("cor_mem_uuid").toString());
				Map<String, Object> memberMap = memService.selectMember(memSelMap);
					
				Map<String, Object> sendMessageMap = new HashMap<String, Object>();
				sendMessageMap.put("title", "탁송이 완료 되었습니다.");
				
				String detail = "제조사 : "+consignRequestDetail.get("crd_car_maker").toString()+"\r\n";
				detail += "차종 : "+consignRequestDetail.get("crd_car_kind").toString()+"\r\n";
				
				String carInfo = "";
				carInfo = consignRequestDetail.get("crd_car_id_num") != null && !consignRequestDetail.get("crd_car_id_num").toString().equals("") ? consignRequestDetail.get("crd_car_id_num").toString() : "";
				
				if(!carInfo.equals("")) {
					carInfo = "차대번호 : "+carInfo+"\r\n";
				}
				if(consignRequestDetail.get("crd_car_num") != null && !consignRequestDetail.get("crd_car_num").toString().equals("")) {
					carInfo +=  "차량번호 : "+consignRequestDetail.get("crd_car_num").toString();
				}
				
				detail += "차량정보 : "+carInfo+"\r\n";
				sendMessageMap.put("body", detail);
				sendMessageMap.put("fcm_token", memberMap.get("mem_device_key") != null && !memberMap.get("mem_device_key").toString().equals("") ? memberMap.get("mem_device_key").toString() : "");
				sendMessageMap.put("device_os", memberMap.get("mem_login_env").toString());
				
				if(!sendMessageMap.get("fcm_token").toString().equals("")) {
					fcmService.fcmSendMessage(sendMessageMap);	
				}
				
			}
			
			consignRequestDetailMapper.updateConsignRequestDetailCrdStatus(map);	
			
		}catch(Exception e) {
			result.setResultCode("E002");
			e.printStackTrace();
		}
		return result;
	}
	
	
	
}
